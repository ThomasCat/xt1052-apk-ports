# XT1052 APK ports
The Moto X (1st Gen) had some pretty hyped features that were ahead of their time ([MKBHD review](https://www.youtube.com/watch?v=3ZX8graseUQ)). The Moto G (1st Gen) was one of the best selling phones of all time. Naturally, people on dev forums like XDA wanted these features on their $179 Moto G, but nobody was ready to do it, so here was my mostly successful attempt. 

So I extracted some APKs from the Moto X (1st Gen) KitKat 4.4.2 system dump, decompiled and modded for the XT1033/Moto G (1st Gen).

Touchless Control obviously needs the special Moto X8 system with the special Qualcomm Snapdragon S4 Pro, so it keeps crashing. Active Display needs a hardware gyroscope, which isn't present on any Moto G model, except the Moto G 4G (XT1039). So expect crashing.

#### To install on your device:
1. Make sure you're using an XT1033 running Motorola's stock Android 4.4.2.
2. On your device, open Settings → About Phone, scroll down to "Build Number" and tap on it several times.
3. Go back to Settings → Developer Options → USB Debugging and enable it.
4. Connect your device to your computer, open Terminal and type the following:

```git clone https://gitlab.com/5b43f91c61170e88de567951ebbec765/xt1052-apk-ports```

```cd xt1052-apk-ports```

```adb install -r "Active Display/com.motorola.aon.apk"```

```adb install -r "Assist/com.motorola.assist.apk"```

```adb install -r "Droid Zap/com.motorola.zap.apk"```

```adb install -r "Modality Services/com.motorola.slpc.apk"```

```adb install -r "Touchless Control/com.motorola.audiomonitor.apk"```

<hr>

Original XDA posts (archived): 
---
Original thread: https://web.archive.org/save/https://forum.xda-developers.com/showthread.php?t=2687141

Thread for Motorola Assist: https://web.archive.org/web/20190713195108/https://forum.xda-developers.com/showthread.php?t=2764635

Thread for Motorola Connect: https://web.archive.org/web/20190713195618/https://forum.xda-developers.com/showthread.php?t=2763630

Motorola Connect Chrome Extension: https://chrome.google.com/webstore/detail/motorola-connect/kigmoblgooahdmdibodmcnffgnejlndh



ALL FILES ARE INTELLECTUAL PROPERTY OF MOTOROLA MOBILITY LLC. I've merely decompiled them for homebrew purposes and make no profit from any of this.
