package com.nuance.a.a.a.c;

import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.a.b.a.b;
import com.nuance.a.a.a.a.b.a.b.C0002b;
import java.util.Vector;

public class d implements b {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final C0001a f955a = com.nuance.a.a.a.a.b.a.a.a(d.class);
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public Object f956b = new Object();
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public Vector f957c = new Vector();

    /* renamed from: d reason: collision with root package name */
    private g f958d = new g();

    public final class a {

        /* renamed from: a reason: collision with root package name */
        public Thread f960a;

        /* renamed from: b reason: collision with root package name */
        public Thread f961b;

        /* renamed from: c reason: collision with root package name */
        public C0002b f962c;

        /* renamed from: d reason: collision with root package name */
        public Object f963d;

        public a(Object obj, C0002b bVar) {
            this.f963d = obj;
            this.f962c = bVar;
        }
    }

    public final void a(com.nuance.a.a.a.a.b.a.e.a aVar, long j) {
        boolean z = false;
        synchronized (this.f956b) {
            if (this.f958d != null) {
                this.f958d.a(aVar, j);
            } else {
                z = true;
            }
        }
        if (z && f955a.b()) {
            f955a.b((Object) "MessageSystemOEM.scheduleTask() timer already stopped!");
        }
    }

    public final void a(Object obj, C0002b bVar, Object obj2, Object obj3) {
        a aVar = new a(obj, bVar);
        aVar.f961b = (Thread) obj3;
        aVar.f960a = (Thread) obj2;
        AnonymousClass1 r2 = new com.nuance.a.a.a.a.b.a.e.a() {
            /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
            public final void run() {
                a aVar = null;
                try {
                    synchronized (d.this.f956b) {
                        if (!d.this.f957c.isEmpty()) {
                            aVar = (a) d.this.f957c.elementAt(0);
                            d.this.f957c.removeElementAt(0);
                        }
                    }
                    if (aVar == null) {
                        return;
                    }
                    if (aVar.f962c != null) {
                        if (d.f955a.a()) {
                            d.f955a.a((Object) "Executing Message");
                        }
                        aVar.f962c.a(aVar.f963d, aVar.f960a);
                        if (d.f955a.a()) {
                            d.f955a.a((Object) "Done Executing Message");
                        }
                    } else if (d.f955a.e()) {
                        d.f955a.e("MessageSystemOEM.send() msg.msgHandler is null");
                    }
                } catch (Exception e2) {
                    if (d.f955a.e()) {
                        d.f955a.e("MessageSystemOEM.send() " + e2.getClass().getName() + " " + e2.getMessage());
                    }
                }
            }
        };
        boolean z = false;
        synchronized (this.f956b) {
            if (this.f958d != null) {
                this.f957c.addElement(aVar);
                a(r2, 0);
            } else {
                z = true;
            }
        }
        if (z && f955a.b()) {
            f955a.b((Object) "MessageSystemOEM.send() timer already stopped!");
        }
    }

    public final boolean a(com.nuance.a.a.a.a.b.a.e.a aVar) {
        boolean z;
        boolean z2 = false;
        synchronized (this.f956b) {
            if (this.f958d != null) {
                z = this.f958d.a(aVar);
            } else {
                z2 = true;
                z = false;
            }
        }
        if (z2 && f955a.b()) {
            f955a.b((Object) "MessageSystemOEM.cancelTask() timer already stopped!");
        }
        return z;
    }

    public final Object[] a() {
        return new Object[]{Thread.currentThread()};
    }

    public final Object b() {
        return Thread.currentThread();
    }

    public final void c() {
        boolean z = false;
        synchronized (this.f956b) {
            if (this.f958d != null) {
                this.f958d.a();
                this.f958d = null;
                this.f957c.removeAllElements();
                this.f957c = null;
            } else {
                z = true;
            }
        }
        if (z && f955a.b()) {
            f955a.b((Object) "MessageSystemOEM.stop() timer already stopped!");
        }
    }
}
