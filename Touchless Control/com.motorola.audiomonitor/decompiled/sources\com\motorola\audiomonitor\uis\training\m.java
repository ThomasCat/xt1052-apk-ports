package com.motorola.audiomonitor.uis.training;

import android.view.View;
import android.view.View.OnClickListener;

class m implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ TrainingTriggerRecordingActivity f752a;

    m(TrainingTriggerRecordingActivity trainingTriggerRecordingActivity) {
        this.f752a = trainingTriggerRecordingActivity;
    }

    public void onClick(View view) {
        this.f752a.i();
        this.f752a.a(true, false);
        this.f752a.j.b(true);
        this.f752a.n = 0;
        this.f752a.a(1);
        this.f752a.a(this.f752a.V);
    }
}
