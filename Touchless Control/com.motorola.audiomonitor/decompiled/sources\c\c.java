package c;

import android.util.Log;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class c {

    /* renamed from: a reason: collision with root package name */
    private static final String f110a = ("AMonitor" + c.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static boolean f111b;

    /* renamed from: c reason: collision with root package name */
    private static Constructor f112c;

    /* renamed from: d reason: collision with root package name */
    private static Method f113d;

    /* renamed from: e reason: collision with root package name */
    private static Method f114e;

    /* renamed from: f reason: collision with root package name */
    private static Method f115f;

    /* renamed from: g reason: collision with root package name */
    private static Method f116g;

    /* renamed from: h reason: collision with root package name */
    private static Method f117h;
    private static Method i;
    private final Object j;

    static {
        f111b = false;
        try {
            Class cls = Class.forName("com.motorola.data.event.api.Segment");
            f112c = cls.getDeclaredConstructor(new Class[]{String.class});
            f113d = cls.getDeclaredMethod("getSegmentName", new Class[0]);
            f114e = cls.getDeclaredMethod("setValue", new Class[]{String.class, Boolean.TYPE});
            f115f = cls.getDeclaredMethod("setValue", new Class[]{String.class, Double.TYPE});
            f116g = cls.getDeclaredMethod("setValue", new Class[]{String.class, Integer.TYPE});
            f117h = cls.getDeclaredMethod("setValue", new Class[]{String.class, Long.TYPE});
            i = cls.getDeclaredMethod("setValue", new Class[]{String.class, String.class});
            f111b = true;
        } catch (Throwable th) {
            Log.w(f110a, "Unable to get segment class.");
            f111b = false;
        }
    }

    public c(String str) {
        Object obj = null;
        if (f111b && str != null && !str.isEmpty()) {
            try {
                obj = f112c.newInstance(new Object[]{str});
            } catch (Throwable th) {
                Log.w(f110a, "Unable to instantiate Segment.");
            }
        }
        this.j = obj;
    }

    /* access modifiers changed from: 0000 */
    public Object a() {
        return this.j;
    }

    public void a(String str, String str2) {
        if (f111b && this.j != null) {
            if (str2 == null) {
                str2 = "null";
            }
            try {
                i.invoke(this.j, new Object[]{str, str2});
            } catch (Throwable th) {
                Log.w(f110a, "Unable to set value.");
            }
        }
    }
}
