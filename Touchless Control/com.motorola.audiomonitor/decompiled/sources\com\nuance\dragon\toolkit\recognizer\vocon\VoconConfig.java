package com.nuance.dragon.toolkit.recognizer.vocon;

import com.nuance.dragon.toolkit.language.Language;
import com.nuance.dragon.toolkit.recognizer.NMTLanguage;
import com.nuance.dragon.toolkit.util.internal.d;

public class VoconConfig {

    /* renamed from: a reason: collision with root package name */
    private String f2005a;

    /* renamed from: b reason: collision with root package name */
    private int f2006b;

    /* renamed from: c reason: collision with root package name */
    private String f2007c;

    /* renamed from: d reason: collision with root package name */
    private String f2008d;

    public VoconConfig(Language language, int i) {
        d.a("language", (Object) language);
        this.f2005a = language.getVoconLanguage().name;
        this.f2006b = i;
        this.f2007c = null;
        this.f2008d = null;
    }

    public VoconConfig(NMTLanguage nMTLanguage, int i) {
        d.a("language", (Object) nMTLanguage);
        this.f2005a = nMTLanguage.name;
        this.f2006b = i;
        this.f2007c = null;
        this.f2008d = null;
    }

    public VoconConfig(String str, String str2) {
        d.a("acModFileName", str);
        this.f2007c = str;
        this.f2008d = str2;
    }

    /* access modifiers changed from: 0000 */
    public final String a() {
        return this.f2007c;
    }

    /* access modifiers changed from: 0000 */
    public final String b() {
        return this.f2008d;
    }

    /* access modifiers changed from: 0000 */
    public final String c() {
        return this.f2005a;
    }

    /* access modifiers changed from: 0000 */
    public final int d() {
        return this.f2006b;
    }
}
