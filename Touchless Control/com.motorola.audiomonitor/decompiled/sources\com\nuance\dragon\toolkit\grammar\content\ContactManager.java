package com.nuance.dragon.toolkit.grammar.content;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.Data;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.grammar.Word;
import com.nuance.dragon.toolkit.grammar.WordAction;
import com.nuance.dragon.toolkit.grammar.content.ContentManager.PlatformFullIterator;
import com.nuance.dragon.toolkit.grammar.content.UriContentManager.UriPlatformFullIterator;

public class ContactManager extends UriContentManager {

    /* renamed from: a reason: collision with root package name */
    private static final Uri f1748a = Data.CONTENT_URI;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final String[] f1749b = {"lookup", "display_name", "in_visible_group", "data7", "data8", "data9"};

    public ContactManager(String str, FileManager fileManager, Context context) {
        this(str, fileManager, false, false, context);
    }

    public ContactManager(String str, FileManager fileManager, boolean z, Context context) {
        this(str, fileManager, false, z, context);
    }

    public ContactManager(String str, FileManager fileManager, boolean z, boolean z2, Context context) {
        super(str, fileManager, f1748a, z, z2, context);
    }

    /* access modifiers changed from: protected */
    public PlatformFullIterator getPlatformFullIterator() {
        return new UriPlatformFullIterator(this._context, this._uri, f1749b, "mimetype='vnd.android.cursor.item/name'") {

            /* renamed from: b reason: collision with root package name */
            private int f1751b;

            /* renamed from: c reason: collision with root package name */
            private int f1752c;

            /* renamed from: d reason: collision with root package name */
            private int f1753d;

            /* renamed from: e reason: collision with root package name */
            private int f1754e;

            /* renamed from: f reason: collision with root package name */
            private int f1755f;

            /* renamed from: g reason: collision with root package name */
            private int f1756g;

            /* access modifiers changed from: protected */
            public final WordAction getNext(Cursor cursor) {
                while (cursor.moveToNext()) {
                    try {
                        if (cursor.getInt(this.f1753d) != 0) {
                            String string = cursor.getString(this.f1751b);
                            String string2 = cursor.getString(this.f1752c);
                            if (!(string2 == null || string2.length() == 0 || string == null)) {
                                String str = "";
                                String string3 = cursor.getString(this.f1754e);
                                String string4 = cursor.getString(this.f1755f);
                                String string5 = cursor.getString(this.f1756g);
                                if (string5 == null || string5.length() == 0) {
                                    string5 = str;
                                }
                                if (!(string4 == null || string4.length() == 0)) {
                                    string5 = string5 + string4;
                                }
                                if (!(string3 == null || string3.length() == 0)) {
                                    string5 = string5 + string3;
                                }
                                if (string5.length() == 0) {
                                    string5 = null;
                                }
                                return new WordAction(new Word(string2, string5), true);
                            }
                        }
                    } catch (RuntimeException e2) {
                    }
                }
                return null;
            }

            /* access modifiers changed from: protected */
            public final void init(Cursor cursor) {
                this.f1751b = cursor.getColumnIndex(ContactManager.f1749b[0]);
                this.f1752c = cursor.getColumnIndex(ContactManager.f1749b[1]);
                this.f1753d = cursor.getColumnIndex(ContactManager.f1749b[2]);
                this.f1754e = cursor.getColumnIndex(ContactManager.f1749b[3]);
                this.f1755f = cursor.getColumnIndex(ContactManager.f1749b[4]);
                this.f1756g = cursor.getColumnIndex(ContactManager.f1749b[5]);
            }
        };
    }
}
