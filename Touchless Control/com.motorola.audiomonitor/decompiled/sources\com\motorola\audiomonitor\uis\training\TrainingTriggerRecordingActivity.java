package com.motorola.audiomonitor.uis.training;

import android.animation.TimeAnimator;
import android.animation.TimeAnimator.TimeListener;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.au;
import com.motorola.audiomonitor.aw;
import h.k;

public class TrainingTriggerRecordingActivity extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f730a = ("AMonitor." + TrainingTriggerRecordingActivity.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f731b = a.f160b;
    private static final int q = Math.abs(-60);
    private static final int r = Math.abs(-51);
    private static final int s = Math.abs(-39);
    /* access modifiers changed from: private */
    public static final int[] x = {1200, 600, 300, 150, 0};
    /* access modifiers changed from: private */
    public static final int[] y = {170, 150, 120, 60, 0};
    /* access modifiers changed from: private */
    public static final int[] z = {180, 160, 130, 70, 50};
    /* access modifiers changed from: private */
    public View A;
    private View B;
    /* access modifiers changed from: private */
    public ImageView C;
    /* access modifiers changed from: private */
    public ImageView D;
    /* access modifiers changed from: private */
    public ImageView E;
    private ImageView F;
    /* access modifiers changed from: private */
    public int G = 0;
    /* access modifiers changed from: private */
    public int H = 0;
    /* access modifiers changed from: private */
    public int I = 0;
    /* access modifiers changed from: private */
    public int J = 0;
    /* access modifiers changed from: private */
    public int K = 0;
    /* access modifiers changed from: private */
    public LayoutParams L;
    /* access modifiers changed from: private */
    public LayoutParams M;
    private TimeAnimator N;
    private WakeLock O;
    private final TimeListener P = new d(this);
    private final aw Q = new h(this);
    private final Runnable R = new p(this);
    /* access modifiers changed from: private */
    public final Runnable S = new q(this);
    /* access modifiers changed from: private */
    public final s T = new r(this);
    /* access modifiers changed from: private */
    public final s U = new e(this);
    /* access modifiers changed from: private */
    public final s V = new f(this);
    private final BroadcastReceiver W = new g(this);
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public Context f732c;

    /* renamed from: d reason: collision with root package name */
    private TextView f733d;

    /* renamed from: e reason: collision with root package name */
    private TextView f734e;

    /* renamed from: f reason: collision with root package name */
    private TextView f735f;

    /* renamed from: g reason: collision with root package name */
    private TextView f736g;

    /* renamed from: h reason: collision with root package name */
    private ProgressBar f737h;
    private ImageView i;
    /* access modifiers changed from: private */
    public au j;
    /* access modifiers changed from: private */
    public String k;
    /* access modifiers changed from: private */
    public final Handler l = new Handler();
    /* access modifiers changed from: private */
    public boolean m = false;
    /* access modifiers changed from: private */
    public int n;
    /* access modifiers changed from: private */
    public int o = 1;
    /* access modifiers changed from: private */
    public int p = 0;
    /* access modifiers changed from: private */
    public int t = 0;
    /* access modifiers changed from: private */
    public int u = 0;
    /* access modifiers changed from: private */
    public s v = null;
    /* access modifiers changed from: private */
    public final ImageView[] w = new ImageView[4];

    /* access modifiers changed from: private */
    public void a(int i2) {
        if (f731b) {
            Log.d(f730a, "UI control is " + i2 + " and mNumEnroll is " + this.n);
        }
        this.o = i2;
        switch (i2) {
            case 1:
                for (int i3 = 0; i3 < 3; i3++) {
                    this.w[i3].setImageResource(C0014R.drawable.vc_circle_training);
                }
                this.i.setVisibility(8);
                this.f737h.setVisibility(4);
                this.A.setVisibility(0);
                c((int) C0014R.string.training_say_hello_moto);
                a(false);
                g();
                return;
            case 2:
                this.w[this.n].setImageResource(C0014R.drawable.vc_circle_training_pass);
                return;
            case 3:
                d((int) C0014R.string.training_gotit_touch_retry);
                a(true);
                if (this.p != 26) {
                    a(true, true, (int) C0014R.string.training_retry);
                    return;
                }
                return;
            case 4:
                d((int) C0014R.string.training_perfect_touch_again);
                a(true);
                return;
            case 5:
                this.w[this.n].setImageResource(C0014R.drawable.vc_circle_training_fail);
                a(true);
                return;
            case 6:
                this.i.setVisibility(8);
                this.f737h.setVisibility(4);
                this.A.setVisibility(0);
                a(true);
                d((int) C0014R.string.training_great_say_lasttime);
                return;
            case 7:
                int i4 = 0;
                while (i4 < 3) {
                    this.w[i4].setImageResource(i4 < this.n ? C0014R.drawable.vc_circle_training_pass : C0014R.drawable.vc_circle_training);
                    i4++;
                }
                if (this.p != 0) {
                    this.w[this.n].setImageResource(C0014R.drawable.vc_circle_training_fail);
                }
                a(true);
                return;
            case 8:
                d((int) C0014R.string.training_check_then_one_more_time);
                this.i.setVisibility(8);
                this.f737h.setVisibility(0);
                this.A.setVisibility(4);
                return;
            case 9:
                if (this.n == 0) {
                    c((int) C0014R.string.training_say_hello_moto);
                } else if (this.n == 1) {
                    c((int) C0014R.string.training_second_hello_moto);
                } else if (this.n == 2) {
                    c((int) C0014R.string.training_third_hello_moto);
                } else if (this.n == 3) {
                    c((int) C0014R.string.training_final_hello_moto);
                }
                this.i.setVisibility(8);
                this.f737h.setVisibility(4);
                this.A.setVisibility(0);
                return;
            case 10:
                this.w[this.n].setImageResource(C0014R.drawable.vc_circle_training_fail);
                this.i.setVisibility(8);
                this.f737h.setVisibility(4);
                this.A.setVisibility(0);
                a(true);
                g();
                return;
            case 11:
                c((int) C0014R.string.training_final_hello_moto);
                return;
            case 12:
                for (int i5 = 0; i5 < 3; i5++) {
                    this.w[i5].setImageResource(C0014R.drawable.vc_circle_training_error);
                }
                this.i.setVisibility(8);
                this.f737h.setVisibility(4);
                this.A.setVisibility(0);
                a(true, true, (int) C0014R.string.training_start_again);
                a(false);
                g();
                return;
            case 13:
                this.w[this.n].setImageResource(C0014R.drawable.vc_circle_training_fail);
                this.i.setVisibility(8);
                this.f737h.setVisibility(4);
                this.A.setVisibility(0);
                a(true, true, (int) C0014R.string.training_start_again);
                a(false);
                g();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3) {
        if (f731b) {
            Log.d(f730a, "event=" + i2 + ", error=" + i3);
        }
        i();
        this.p = i3;
        switch (i2) {
            case 0:
                f();
                a(9);
                return;
            case 1:
                a(2);
                this.n++;
                if (this.n == 1) {
                    g();
                    a(3);
                    return;
                } else if (this.n < 3) {
                    g();
                    a(4);
                    return;
                } else if (this.n == 3) {
                    g();
                    this.j.g();
                    return;
                } else {
                    return;
                }
            case 2:
                g();
                a(5);
                b(i3);
                return;
            case 3:
                a(8);
                return;
            case 4:
                if (this.n < 3) {
                    a(6);
                    return;
                }
                g();
                this.j.c();
                this.j.a(true);
                k.g((Context) this);
                h();
                return;
            case 5:
                g();
                this.j.b(true);
                this.n = 0;
                a(12);
                b(i3);
                return;
            case 6:
                f();
                a(11);
                this.l.postDelayed(this.R, 10000);
                return;
            case 7:
                this.l.removeCallbacks(this.R);
                g();
                a(2);
                this.n++;
                if (this.n == 3) {
                    a(true, (int) C0014R.string.training_finished);
                    this.j.c();
                    this.j.a(true);
                    k.g((Context) this);
                    h();
                    return;
                }
                return;
            case 8:
                this.l.removeCallbacks(this.R);
                break;
            case 9:
                this.l.removeCallbacks(this.R);
                g();
                this.j.b(false);
                a(13);
                b(i3);
                return;
            case 10:
                b(i3);
                return;
            case 11:
                break;
            default:
                Log.e(f730a, "Unsupported Enrollment Event: " + i2);
                return;
        }
        g();
        this.j.b(false);
        a(10);
        b(i3);
    }

    /* access modifiers changed from: private */
    public void a(s sVar) {
        this.v = sVar;
        this.j.i();
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        this.f735f.setEnabled(z2);
        this.f735f.setTextColor(getResources().getColor(z2 ? C0014R.color.Active_Button : C0014R.color.Reg));
        this.f735f.setVisibility(0);
    }

    private void a(boolean z2, int i2) {
        this.f735f.setText(i2);
        a(z2);
    }

    /* access modifiers changed from: private */
    public void a(boolean z2, boolean z3) {
        this.f736g.setEnabled(z3);
        this.f736g.setTextColor(getResources().getColor(z3 ? C0014R.color.Active_Button : C0014R.color.Reg));
        this.f736g.setVisibility(z2 ? 0 : 8);
    }

    private void a(boolean z2, boolean z3, int i2) {
        this.f736g.setText(i2);
        a(z2, z3);
    }

    private void b(int i2) {
        if (i2 != 0) {
            if (f731b) {
                Log.d(f730a, "error=" + i2);
            }
            Resources resources = this.f732c.getResources();
            String[] stringArray = resources.getStringArray(C0014R.array.training_errors);
            if (i2 >= 0 && i2 < stringArray.length) {
                Spanned fromHtml = Html.fromHtml(stringArray[i2]);
                this.f733d.setText("");
                this.f734e.setText(fromHtml);
            }
            TypedArray obtainTypedArray = resources.obtainTypedArray(C0014R.array.training_error_images);
            Drawable drawable = obtainTypedArray.getDrawable(i2);
            if (drawable != null) {
                this.i.setImageDrawable(drawable);
                this.i.setVisibility(0);
                this.f737h.setVisibility(4);
                this.A.setVisibility(4);
            }
            obtainTypedArray.recycle();
        }
    }

    /* access modifiers changed from: private */
    public void b(int i2, int i3) {
        if (this.m) {
            Resources resources = this.f732c.getResources();
            String[] stringArray = resources.getStringArray(C0014R.array.noise_errors);
            int c2 = c(i2, i3);
            if (c2 >= 0 && c2 < stringArray.length) {
                this.f734e.setText(Html.fromHtml(stringArray[c2]));
            }
            TypedArray obtainTypedArray = resources.obtainTypedArray(C0014R.array.sound_meter_images);
            if (obtainTypedArray != null) {
                int i4 = q + i3;
                if (i4 < 0) {
                    i4 = 0;
                } else if (i4 >= obtainTypedArray.length()) {
                    i4 = obtainTypedArray.length() - 1;
                }
                if (s + i3 < 0 && i2 == 2) {
                    i4 = (q - s) + 1;
                } else if (r + i3 < 0 && i2 == 1) {
                    i4 = (q - r) + 1;
                }
                if (i4 < obtainTypedArray.length()) {
                    Drawable drawable = obtainTypedArray.getDrawable(i4);
                    if (drawable != null) {
                        this.i.setImageDrawable(drawable);
                        this.i.setVisibility(0);
                        this.f737h.setVisibility(4);
                        this.A.setVisibility(4);
                    }
                }
                obtainTypedArray.recycle();
            }
        }
    }

    /* access modifiers changed from: private */
    public int c(int i2, int i3) {
        int i4 = s + i3 >= 0 ? 2 : r + i3 >= 0 ? 1 : 0;
        return Math.max(i2, i4);
    }

    static /* synthetic */ int c(TrainingTriggerRecordingActivity trainingTriggerRecordingActivity, int i2) {
        int i3 = trainingTriggerRecordingActivity.I - i2;
        trainingTriggerRecordingActivity.I = i3;
        return i3;
    }

    private void c(int i2) {
        this.f734e.setText("");
        this.f733d.setText(k.a(this, i2, new int[]{C0014R.string.hello_moto}, C0014R.color.UDTHighlightColor));
    }

    static /* synthetic */ int d(TrainingTriggerRecordingActivity trainingTriggerRecordingActivity, int i2) {
        int i3 = trainingTriggerRecordingActivity.J - i2;
        trainingTriggerRecordingActivity.J = i3;
        return i3;
    }

    private void d(int i2) {
        this.f733d.setText("");
        this.f734e.setText(i2);
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.N != null) {
            this.N.setCurrentPlayTime(0);
            this.N.start();
            this.C.setImageResource(C0014R.drawable.vc_micbtn_on);
            this.D.setVisibility(0);
            this.E.setVisibility(0);
            this.F.setVisibility(0);
        }
    }

    private void g() {
        this.I = 0;
        this.J = 0;
        if (this.N != null) {
            this.N.cancel();
        }
        this.C.setImageResource(C0014R.drawable.vc_micbtn_off);
        this.D.setVisibility(4);
        this.E.setVisibility(4);
        this.F.setVisibility(4);
    }

    private void h() {
        Intent intent = new Intent(this, TrainingCompletedActivity.class);
        intent.putExtras(getIntent());
        startActivity(intent);
        finish();
    }

    /* access modifiers changed from: private */
    @SuppressLint({"Wakelock"})
    public void i() {
        j();
        this.O = ((PowerManager) getSystemService("power")).newWakeLock(536870922, f730a);
        this.O.acquire(180000);
        if (f731b) {
            Log.d(f730a, "acquire wakelock");
        }
    }

    private void j() {
        if (this.O != null && this.O.isHeld()) {
            this.O.release();
            if (f731b) {
                Log.d(f730a, "release wakelock");
            }
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        this.f733d.setVisibility(0);
        this.B.setVisibility(4);
        this.m = false;
    }

    /* access modifiers changed from: private */
    public void l() {
        Resources resources = this.f732c.getResources();
        this.f733d.setVisibility(4);
        this.B.setVisibility(0);
        ((TextView) findViewById(C0014R.id.quiet_prompt)).setText(Html.fromHtml(resources.getString(C0014R.string.training_quiet)));
        ((TextView) findViewById(C0014R.id.noisy_prompt)).setText(Html.fromHtml(resources.getString(C0014R.string.training_noisy)));
        ((TextView) findViewById(C0014R.id.loud_prompt)).setText(Html.fromHtml(resources.getString(C0014R.string.training_loud)));
        this.m = true;
    }

    private void m() {
        registerReceiver(this.W, new IntentFilter("com.motorola.audiomonitor.NOISE_ESTIMATE"));
    }

    private void n() {
        unregisterReceiver(this.W);
    }

    public void finish() {
        super.finish();
        overridePendingTransition(0, C0014R.anim.activity_close_exit);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(C0014R.layout.training_trigger_recording_new);
        ActionBar actionBar = getActionBar();
        actionBar.setIcon(k.b());
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(C0014R.string.personalization_title);
        this.f732c = this;
        this.k = null;
        Intent intent = getIntent();
        if (intent != null) {
            this.k = intent.getStringExtra("selection");
        }
        this.n = 0;
        this.o = 1;
        this.p = 0;
        if (bundle != null) {
            this.n = bundle.getInt("mNumEnroll");
            this.o = bundle.getInt("mStage");
            this.p = bundle.getInt("mLastError");
        }
        this.j = new au(this, this.Q);
        this.j.c();
        this.f735f = (TextView) findViewById(C0014R.id.training_recording_record);
        this.f735f.setOnClickListener(new l(this));
        this.f736g = (TextView) findViewById(C0014R.id.training_recording_retry);
        this.f736g.setOnClickListener(new m(this));
        ((TextView) findViewById(C0014R.id.training_recording_quit)).setOnClickListener(new n(this));
        this.f737h = (ProgressBar) findViewById(C0014R.id.training_progress);
        this.i = (ImageView) findViewById(C0014R.id.training_illustration);
        this.f734e = (TextView) findViewById(C0014R.id.recording_information);
        this.f733d = (TextView) findViewById(C0014R.id.recording_prompt);
        this.w[0] = (ImageView) findViewById(C0014R.id.recording_1_icon);
        this.w[0].setImageResource(C0014R.drawable.vc_circle_training);
        this.w[1] = (ImageView) findViewById(C0014R.id.recording_2_icon);
        this.w[1].setImageResource(C0014R.drawable.vc_circle_training);
        this.w[2] = (ImageView) findViewById(C0014R.id.recording_3_icon);
        this.w[2].setImageResource(C0014R.drawable.vc_circle_training);
        this.w[3] = (ImageView) findViewById(C0014R.id.recording_4_icon);
        this.w[3].setImageResource(C0014R.drawable.vc_circle_training);
        View[] viewArr = {findViewById(C0014R.id.recording_1_icon_layout), findViewById(C0014R.id.recording_2_icon_layout), findViewById(C0014R.id.recording_3_icon_layout), findViewById(C0014R.id.recording_4_icon_layout)};
        if (viewArr.length > 3) {
            for (int i2 = 3; i2 < viewArr.length; i2++) {
                viewArr[i2].setVisibility(8);
            }
        }
        this.D = (ImageView) findViewById(C0014R.id.training_mic_shadow);
        this.E = (ImageView) findViewById(C0014R.id.training_mic_reactive);
        this.F = (ImageView) findViewById(C0014R.id.training_levels_guideline);
        this.C = (ImageView) findViewById(C0014R.id.training_voiceicon);
        this.B = findViewById(C0014R.id.quiet_noisy_loud_layout);
        this.A = findViewById(C0014R.id.training_mic_frame);
        this.A.addOnLayoutChangeListener(new o(this));
        this.N = new TimeAnimator();
        this.N.setTimeListener(this.P);
        this.N.start();
        this.j.a();
    }

    public void onDestroy() {
        this.j.c();
        this.j.a(false);
        k.g((Context) this);
        this.j.b();
        if (this.N != null) {
            this.N.removeAllListeners();
            if (this.N.isRunning()) {
                this.N.cancel();
            }
            this.N = null;
        }
        super.onDestroy();
    }

    public boolean onNavigateUp() {
        finish();
        return true;
    }

    public void onPause() {
        super.onPause();
        j();
        if (this.p == 0) {
            this.p = 26;
        }
        this.l.removeCallbacksAndMessages(null);
        this.j.b(isFinishing());
        n();
    }

    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.n = bundle.getInt("mNumEnroll");
        this.o = bundle.getInt("mStage");
        this.p = bundle.getInt("mLastError");
    }

    public void onResume() {
        super.onResume();
        i();
        if (this.p == 0) {
            a(this.o);
        } else if (this.p == 7) {
            l();
            b(this.t, this.u);
        } else {
            a(7);
            b(this.p);
            a(this.o);
        }
        m();
        a((s) null);
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("mNumEnroll", this.n);
        bundle.putInt("mStage", this.o);
        bundle.putInt("mLastError", this.p);
    }
}
