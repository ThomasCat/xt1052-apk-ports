package com.motorola.audiomonitor.pluginservice;

import android.os.RemoteException;
import android.util.Log;
import com.motorola.audiomonitor.ProtectedByteBuffer;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.j;

public class b extends j {

    /* renamed from: b reason: collision with root package name */
    private static final String f536b = ("AMonitor." + b.class.getSimpleName());

    /* renamed from: c reason: collision with root package name */
    private static final boolean f537c = a.f160b;

    /* renamed from: d reason: collision with root package name */
    private g.a f538d;

    public b() {
        super(b.class.getSimpleName());
    }

    public synchronized void a() {
        if (this.f538d != null) {
            try {
                this.f538d.c();
                if (f537c) {
                    Log.d(f536b, "Started Audio");
                }
            } catch (RemoteException e2) {
                Log.e(f536b, "Failed to start Audio");
            }
        }
        return;
    }

    public void a(int i, int i2) {
        a();
    }

    public synchronized void a(g.a aVar) {
        this.f538d = aVar;
    }

    public synchronized void b() {
        if (this.f538d != null) {
            try {
                this.f538d.d();
                if (f537c) {
                    Log.d(f536b, "Stopped Audio");
                }
            } catch (RemoteException e2) {
                Log.e(f536b, e2.toString());
            }
        }
        return;
    }

    public void c() {
        super.c();
    }

    public void c(ProtectedByteBuffer protectedByteBuffer) {
        a(protectedByteBuffer);
    }
}
