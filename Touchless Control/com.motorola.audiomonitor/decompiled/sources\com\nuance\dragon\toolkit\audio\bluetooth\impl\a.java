package com.nuance.dragon.toolkit.audio.bluetooth.impl;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.util.Log;
import com.nuance.dragon.toolkit.audio.bluetooth.impl.Bluetooth.HeadsetStateListener;
import com.nuance.dragon.toolkit.audio.bluetooth.impl.BluetoothHeadset.ServiceListener;
import java.util.Set;

final class a extends Bluetooth {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public BluetoothHeadset f1044a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public HeadsetStateListener f1045b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public boolean f1046c;

    /* renamed from: d reason: collision with root package name */
    private boolean f1047d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public boolean f1048e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public boolean f1049f;

    /* renamed from: g reason: collision with root package name */
    private AudioManager f1050g;

    /* renamed from: h reason: collision with root package name */
    private final ServiceListener f1051h = new ServiceListener() {
        public final void onServiceConnected() {
            boolean z = false;
            Bluetooth.a("BluetoothHeadset service connected");
            if (a.this.f1044a == null) {
                a.this.f1045b.onServiceConnected(false);
                return;
            }
            a.this.f1046c = true;
            a aVar = a.this;
            if (a.this.f1044a.getState() == BluetoothHeadset.STATE_CONNECTED) {
                z = true;
            }
            aVar.f1048e = z;
            if (a.this.f1045b != null) {
                a.this.f1045b.onServiceConnected(true);
                a.this.f1045b.onConnectionStateChanged(a.this.getHeadsetState());
            }
        }

        public final void onServiceDisconnected() {
            Bluetooth.a("BluetoothHeadset service disconnected");
            a.this.f1045b.onServiceDisconnected();
            a.this.f1046c = false;
        }
    };
    private final BroadcastReceiver i = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            boolean z = true;
            String action = intent.getAction();
            Bluetooth.a("onReceive(), action = " + action);
            if (action.equals(BluetoothHeadset.ACTION_STATE_CHANGED)) {
                int intExtra = intent.getIntExtra(BluetoothHeadset.EXTRA_STATE, -1);
                Bluetooth.a("Bluetooth state changed: " + intExtra);
                if (intExtra != BluetoothHeadset.STATE_CONNECTED) {
                    z = false;
                }
                if (a.this.f1048e != z) {
                    a.this.f1048e = z;
                    if (a.this.f1045b != null) {
                        a.this.f1045b.onConnectionStateChanged(a.this.getHeadsetState());
                    }
                }
            } else if (action.equals("android.media.SCO_AUDIO_STATE_CHANGED")) {
                int intExtra2 = intent.getIntExtra("android.media.extra.SCO_AUDIO_STATE", -1);
                Bluetooth.a("Bluetooth audio state changed: " + intExtra2);
                if (intExtra2 == 1 || intExtra2 == 0 || intExtra2 == -1) {
                    if (intExtra2 != 1) {
                        z = false;
                    }
                    if (a.this.f1049f != z) {
                        a.this.f1049f = z;
                        if (a.this.f1045b != null) {
                            a.this.f1045b.onAudioStateChanged(a.this.getAudioState());
                        }
                    }
                }
            }
        }
    };

    public a(Context context) {
        super(context);
    }

    public final void close() {
        if (this.f1047d) {
            this.f1047d = false;
            this.mContext.unregisterReceiver(this.i);
        }
        if (this.f1044a != null) {
            try {
                this.f1044a.close();
            } catch (Throwable th) {
            }
            this.f1044a = null;
        }
    }

    public final int getAudioState() {
        return this.f1049f ? 1 : 0;
    }

    public final String[] getBondedDevices() {
        Set<BluetoothDevice> bondedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
        if (bondedDevices == null) {
            return null;
        }
        String[] strArr = new String[bondedDevices.size()];
        int i2 = 0;
        for (BluetoothDevice bluetoothDevice : bondedDevices) {
            String name = bluetoothDevice.getName();
            int i3 = i2 + 1;
            strArr[i2] = name != null ? name : bluetoothDevice.getAddress();
            i2 = i3;
        }
        return strArr;
    }

    public final int getHeadsetState() {
        return this.f1048e ? 1 : 0;
    }

    public final int getPlaybackStream() {
        return Audio.VOICE_CALL_STREAM;
    }

    public final int getRecordingSource() {
        return Audio.DEFAULT_AUDIO_SOURCE;
    }

    public final boolean isEnabled() {
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter != null) {
            return defaultAdapter.isEnabled();
        }
        return false;
    }

    public final void setHeadsetStateListener(HeadsetStateListener headsetStateListener) {
        this.f1045b = headsetStateListener;
        if (this.f1044a == null) {
            this.f1044a = new BluetoothHeadset(this.mContext, this.f1051h);
            IntentFilter intentFilter = new IntentFilter(BluetoothHeadset.ACTION_STATE_CHANGED);
            intentFilter.addAction("android.media.SCO_AUDIO_STATE_CHANGED");
            this.mContext.registerReceiver(this.i, intentFilter);
            this.f1047d = true;
            this.f1050g = (AudioManager) this.mContext.getSystemService("audio");
        }
        if (this.f1046c) {
            this.f1045b.onConnectionStateChanged(getHeadsetState());
        }
    }

    public final boolean startBluetoothSco() {
        this.f1050g.startBluetoothSco();
        Log.d("DMT-Bluetooth", "called void mAudioManager.startBluetoothSco");
        return true;
    }

    public final void stopBluetoothSco() {
        this.f1050g.stopBluetoothSco();
    }
}
