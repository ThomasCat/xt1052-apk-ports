package com.motorola.audiomonitor.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import com.motorola.audiomonitor.a;
import com.nuance.dragon.toolkit.audio.bluetooth.impl.AndroidVersion;
import com.nuance.dragon.toolkit.audio.pipes.SpeexEncoderPipe;
import com.nuance.dragon.toolkit.elvis.ElvisResult;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;

public class d {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f563a = ("AMonitor." + d.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f564b = a.f160b;
    /* access modifiers changed from: private */
    public short[][] A = {null, null};
    private String B = "";
    private int[] C = {0, 0, 0, 0, 0};
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final p f565c = new e(this, "disconnected", "disabled");

    /* renamed from: d reason: collision with root package name */
    private final p f566d = new f(this, "connected", "configuring");
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public final p f567e = new g(this, "connected", "idle");
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public final p f568f = new h(this, "connected", "armed");
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public final p f569g = new i(this, "connected", "stopping");
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public final p f570h = new j(this, "connected", "passthrough");
    private final p i = new k(this, "disconnected", "retry");
    private final p j = new l(this, "disconnected", "probation");
    private HandlerThread k;
    /* access modifiers changed from: private */
    public Handler l;
    private final OnSharedPreferenceChangeListener m = new m(this);
    private Context n;
    private SharedPreferences o;
    private BroadcastReceiver p;
    /* access modifiers changed from: private */
    public n q;
    private List r = new CopyOnWriteArrayList();
    /* access modifiers changed from: private */
    public p s = this.f565c;
    /* access modifiers changed from: private */
    public int t;
    /* access modifiers changed from: private */
    public boolean u;
    private Queue v;
    /* access modifiers changed from: private */
    public Message w;
    /* access modifiers changed from: private */
    public c x;
    private int y = 0;
    /* access modifiers changed from: private */
    public int[] z = {-1, -1};

    public static final String a(int i2) {
        switch (i2) {
            case 0:
                return "MSG_FIRMWARE_LOADED";
            case 1:
                return "MSG_PHRASE_RECOGNIZED";
            case 2:
                return "MSG_ACOUSTIC_MODEL_LOADED";
            case 3:
                return "MSG_LANGUAGE_MODEL_LOADED";
            case 4:
                return "MSG_MIC_TEST";
            case 5:
                return "MSG_SENSOR_HUB_TEST";
            case 6:
                return "MSG_INTERRUPT_TEST";
            case 7:
                return "MSG_EVENT_LOG_CTRL";
            case 8:
                return "MSG_AUDIO_LOG_CTRL";
            case 9:
                return "MSG_AUDIO_BUF_CTRL";
            case 10:
                return "MSG_DOCK_STATE";
            case 11:
                return "MSG_RECOGNITION_CTRL";
            case 12:
                return "MSG_NOISE_ESTIMATE_COMLETE";
            case 13:
                return "MSG_DSP_VERSION";
            case AndroidVersion.ICE_CREAM_SANDWICH /*14*/:
                return "MSG_NOISE_MODE_TIMES";
            case SpeexEncoderPipe.DEFAULT_VAD_BEGIN_LEN /*15*/:
                return "MSG_DISCONNECT";
            case ElvisResult.MAX_CONFIDENCE /*100*/:
                return "EVENT_ENABLE";
            case 101:
                return "EVENT_DISABLE";
            case 102:
                return "EVENT_START";
            case 103:
                return "EVENT_STOP";
            case 104:
                return "EVENT_EVENT_LOG";
            case 105:
                return "EVENT_AUDIO_LOG";
            case 106:
                return "EVENT_AUDIO_BUF_CTRL";
            case 107:
                return "EVENT_START_PASSTHROUGH";
            case 108:
                return "EVENT_STOP_PASSTHROUGH";
            case 109:
                return "EVENT_GET_NOISE_ESTIMATE";
            case 110:
                return "EVENT_GET_DSP_VERSION";
            case 111:
                return "EVENT_GET_NOISE_MODE_TIMES";
            case 200:
                return "EVENT_RETRY_CONNECTION";
            case 201:
                return "EVENT_REQUEST_TIMEOUT";
            case 202:
                return "EVENT_DOCK_STATE_CHANGED";
            case 203:
                return "EVENT_LOAD_ACOUSTIC_MODEL";
            case 204:
                return "EVENT_LOAD_LANGUAGE_MODEL";
            case 205:
                return "EVENT_CONFIG_COMPLETE";
            default:
                return "Unknown " + i2;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(int i2, int i3, short[] sArr) {
        for (o oVar : this.r) {
            if (oVar != null) {
                oVar.a(i2, i3, sArr);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(Intent intent) {
        b(intent);
        a(202, this.y, 0, (Object) null);
    }

    /* access modifiers changed from: private */
    public void a(Message message) {
        u();
    }

    /* access modifiers changed from: private */
    public void a(Message message, p pVar, p pVar2) {
        if (message.what != 109 && message.what != 12) {
            Log.d(f563a, a(message.what) + " (" + message.arg1 + ", " + message.arg2 + ", " + message.obj + ")" + ", from=" + pVar.a() + "." + pVar.b() + ", to=" + pVar2.a() + "." + pVar2.b());
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(p pVar) {
        if (f564b) {
            Log.d(f563a, "onStateChanged: state=" + pVar.a() + "." + pVar.b());
        }
        for (o oVar : this.r) {
            if (oVar != null) {
                oVar.a(pVar.a(), pVar.b());
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        this.B = str;
        Log.i(f563a, "DSP version=" + str);
    }

    /* access modifiers changed from: private */
    public void a(int[] iArr) {
        this.C = iArr;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("noiseModes: ");
        for (int i2 = 0; i2 < iArr.length; i2++) {
            stringBuffer.append(i2 + "=" + iArr[i2] + " ");
            c.d.a(this.n, "noise_mode" + i2, (long) iArr[i2]);
        }
        if (f564b) {
            Log.d(f563a, stringBuffer.toString());
        }
    }

    /* access modifiers changed from: private */
    public synchronized boolean a(int i2, int i3, int i4, Object obj) {
        boolean z2 = false;
        synchronized (this) {
            if (this.l == null) {
                Log.e(f563a, "addRequest called with an invalid handler.");
            } else {
                Message obtainMessage = this.l.obtainMessage(i2, i3, i4, obj);
                if (i2 == 101 || "connected".equals(this.s.a())) {
                    if (this.w == null) {
                        c(obtainMessage);
                    } else {
                        b(obtainMessage);
                    }
                    z2 = true;
                }
            }
        }
        return z2;
    }

    private boolean a(p pVar, int i2, int i3, int i4, Object obj) {
        boolean z2 = true;
        synchronized (this) {
            if (this.s != pVar) {
                q qVar = new q(pVar.a(), pVar.b());
                this.r.add(qVar);
                synchronized (qVar) {
                    boolean a2 = a(i2, i3, i4, obj);
                    if (a2) {
                        try {
                            qVar.b();
                        } catch (InterruptedException e2) {
                            Log.e(f563a, "Interrupted while waiting for desired state.");
                        }
                    }
                    z2 = a2;
                }
                synchronized (this) {
                    this.r.remove(qVar);
                }
            }
        }
        return z2;
    }

    private boolean a(p pVar, p pVar2, int i2, int i3, int i4, Object obj) {
        boolean z2 = false;
        synchronized (this) {
            if (this.s == pVar) {
                q qVar = new q(pVar2.a(), pVar2.b());
                this.r.add(qVar);
                synchronized (qVar) {
                    z2 = a(i2, i3, i4, obj);
                    if (z2) {
                        try {
                            qVar.b();
                            z2 = true;
                        } catch (InterruptedException e2) {
                            Log.e(f563a, "Interrupted while waiting for desired state.");
                        }
                    }
                }
                synchronized (this) {
                    this.r.remove(qVar);
                }
            }
        }
        return z2;
    }

    private void b(Intent intent) {
        switch (intent.getIntExtra("android.intent.extra.DOCK_STATE", 0)) {
            case 1:
            case 3:
            case 4:
                this.y = 1;
                return;
            case 2:
                this.y = 2;
                return;
            default:
                this.y = 0;
                return;
        }
    }

    private void b(Message message) {
        this.v.add(message);
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        this.l.removeMessages(201);
        if (z2) {
            s();
        }
        r();
    }

    private void c(Message message) {
        this.w = Message.obtain(message);
        this.l.sendMessage(message);
        this.l.sendEmptyMessageDelayed(201, (long) this.q.f583d);
    }

    /* access modifiers changed from: private */
    public void o() {
        int b2 = this.x.b();
        if (b2 == 0) {
            this.s = this.f566d;
            t();
            q();
            return;
        }
        if (f564b) {
            Log.w(f563a, "connect failed: status=" + b2);
        }
        r();
    }

    /* access modifiers changed from: private */
    public void p() {
        if (!this.q.f584e.equals("")) {
            a(203, 0, 0, (Object) this.q.f584e);
        }
        if (!this.q.f585f.equals("")) {
            a(204, 0, 0, (Object) this.q.f585f);
        }
    }

    private void q() {
        e();
        g();
        a(110, 0, 0, (Object) null);
        p();
        a(202, this.y, 0, (Object) null);
        a(205, 0, 0, (Object) null);
    }

    private void r() {
        if (f564b) {
            Log.w(f563a, "retryConnection: retries=" + this.t + ", max=" + this.q.f582c);
        }
        if (this.t < this.q.f582c) {
            this.t++;
            this.s = this.i;
            this.l.sendEmptyMessageDelayed(200, (long) this.q.f581b);
            return;
        }
        this.s = this.j;
    }

    /* access modifiers changed from: private */
    public void s() {
        this.l.removeMessages(201);
        if (this.x != null) {
            this.x.c();
        }
        this.s = this.f565c;
    }

    private void t() {
        while (true) {
            Message message = (Message) this.v.poll();
            if (message == null) {
                break;
            }
            message.recycle();
        }
        this.v.clear();
        if (this.w != null) {
            this.w.recycle();
            this.w = null;
        }
    }

    /* access modifiers changed from: private */
    public void u() {
        this.l.removeMessages(201);
        Message message = (Message) this.v.poll();
        if (message != null) {
            c(message);
        } else if (this.w != null) {
            this.w.recycle();
            this.w = null;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void v() {
        for (o oVar : this.r) {
            if (oVar != null) {
                oVar.a();
            }
        }
    }

    public void a() {
        synchronized (this) {
            if (this.w != null) {
                Log.w(f563a, "Uninitializing with an outstanding request: " + this.w);
            }
            if (this.s != this.f565c) {
                Log.w(f563a, "Uninitializing while not disabled: state=" + this.s.a() + "." + this.s.b());
                if (this.x != null) {
                    this.x.c();
                }
            }
        }
        if (this.k != null) {
            this.k.quit();
            while (true) {
                try {
                    this.k.join(5000);
                    break;
                } catch (InterruptedException e2) {
                    if (f564b) {
                        Log.d(f563a, "Join interrupted");
                    }
                }
            }
            if (this.k.isAlive()) {
                Log.e(f563a, "Failed to stop the handler thread");
            }
            this.k = null;
        }
        synchronized (this) {
            if (this.l != null) {
                this.l.removeCallbacksAndMessages(null);
                this.l = null;
            }
            if (this.r != null) {
                this.r.clear();
                this.r = null;
            }
            if (this.o != null) {
                this.o.unregisterOnSharedPreferenceChangeListener(this.m);
                this.o = null;
            }
            if (this.w != null) {
                this.w.recycle();
                this.w = null;
            }
            if (this.x != null) {
                this.x.a();
                this.x = null;
            }
            if (!(this.n == null || this.p == null)) {
                this.n.unregisterReceiver(this.p);
                this.p = null;
            }
        }
    }

    public synchronized void a(Context context, SharedPreferences sharedPreferences) {
        n nVar = new n();
        this.o = sharedPreferences;
        if (this.o != null) {
            nVar.a(this.o);
        }
        a(context, nVar, (c) new AudioDspControl());
        if (this.o != null) {
            this.o.registerOnSharedPreferenceChangeListener(this.m);
        }
    }

    public synchronized void a(Context context, n nVar, c cVar) {
        this.n = context;
        this.q = nVar;
        if (this.q == null) {
            this.q = new n();
        }
        this.v = new LinkedList();
        this.w = null;
        this.k = new HandlerThread(f563a);
        this.k.start();
        this.l = new s(this, this.k.getLooper());
        if (this.n != null) {
            this.p = new r(this, null);
            Intent registerReceiver = this.n.registerReceiver(this.p, new IntentFilter("android.intent.action.DOCK_EVENT"), null, this.l);
            if (registerReceiver != null) {
                b(registerReceiver);
            }
        }
        this.x = cVar;
        this.x.a(this.l);
        if (this.q.f580a) {
            c();
        }
    }

    public synchronized void a(o oVar) {
        if (oVar == null) {
            Log.e(f563a, "addListener: Invalid listener");
        } else {
            this.r.add(oVar);
            oVar.a(i(), j());
        }
    }

    public boolean a(boolean z2) {
        return a(102, z2 ? 1 : 0, 0, (Object) null);
    }

    public void b() {
        d();
        a();
    }

    public synchronized void b(o oVar) {
        if (oVar == null) {
            Log.e(f563a, "removeListener: Invalid listener");
        } else {
            this.r.remove(oVar);
        }
    }

    public boolean b(int i2) {
        return a(109, i2, 0, (Object) null);
    }

    public int c(int i2) {
        if (i2 < this.z.length) {
            return this.z[i2];
        }
        return -1;
    }

    public boolean c() {
        this.l.sendEmptyMessage(100);
        return true;
    }

    public boolean d() {
        this.l.sendEmptyMessage(103);
        return a(this.f565c, 101, 0, 0, (Object) null);
    }

    public short[] d(int i2) {
        if (i2 < this.A.length) {
            return this.A[i2];
        }
        return null;
    }

    public boolean e() {
        return a(103, 0, 0, (Object) null);
    }

    public boolean f() {
        return a(this.f567e, this.f570h, 107, 0, 0, null);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        try {
            a();
        } finally {
            super.finalize();
        }
    }

    public boolean g() {
        return a(108, 0, 0, (Object) null);
    }

    public boolean h() {
        return a(this.f570h, this.f567e, 108, 0, 0, null);
    }

    public String i() {
        return this.s.a();
    }

    public String j() {
        return this.s.b();
    }

    public String k() {
        return this.B;
    }

    public int[] l() {
        return this.C;
    }
}
