package com.nuance.dragon.toolkit.util.internal;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public final class c extends AbstractList {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final ArrayList f2128a = new ArrayList();
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Object f2129b = new Object();

    /* renamed from: c reason: collision with root package name */
    private final int f2130c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public boolean f2131d;

    final class a implements ListIterator {

        /* renamed from: b reason: collision with root package name */
        private int f2133b;

        a(int i) {
            this.f2133b = i;
        }

        public final void add(Object obj) {
            throw new UnsupportedOperationException("Blocking iterators don't support add");
        }

        public final boolean hasNext() {
            boolean z;
            synchronized (c.this.f2129b) {
                z = !c.this.f2131d || this.f2133b < c.this.f2128a.size();
            }
            return z;
        }

        public final boolean hasPrevious() {
            return this.f2133b > 0;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
            r0 = com.nuance.dragon.toolkit.util.internal.c.c(r4.f2132a);
            r2 = r4.f2133b;
            r4.f2133b = r2 + 1;
            r0 = r0.get(r2);
         */
        public final Object next() {
            Object obj;
            synchronized (c.this.f2129b) {
                while (true) {
                    if (c.this.f2131d) {
                        break;
                    } else if (this.f2133b < c.this.f2128a.size()) {
                        ArrayList c2 = c.this.f2128a;
                        int i = this.f2133b;
                        this.f2133b = i + 1;
                        obj = c2.get(i);
                        break;
                    } else {
                        try {
                            c.this.f2129b.wait();
                        } catch (InterruptedException e2) {
                        }
                    }
                }
            }
            return obj;
        }

        public final int nextIndex() {
            return this.f2133b;
        }

        public final Object previous() {
            Object obj;
            if (this.f2133b <= 0) {
                throw new NoSuchElementException();
            }
            synchronized (c.this.f2129b) {
                ArrayList c2 = c.this.f2128a;
                int i = this.f2133b - 1;
                this.f2133b = i;
                obj = c2.get(i);
            }
            return obj;
        }

        public final int previousIndex() {
            if (this.f2133b > 0) {
                return this.f2133b - 1;
            }
            throw new NoSuchElementException();
        }

        public final void remove() {
            throw new UnsupportedOperationException("Blocking iterators don't support remove");
        }

        public final void set(Object obj) {
            throw new UnsupportedOperationException("Blocking iterators don't support set");
        }
    }

    public c(int i) {
        this.f2130c = i;
    }

    public final void a(Object obj, boolean z) {
        synchronized (this.f2129b) {
            this.f2128a.add(obj);
            this.f2131d = z;
            this.f2129b.notifyAll();
        }
    }

    public final Object get(int i) {
        Object obj;
        synchronized (this.f2129b) {
            obj = this.f2128a.get(i);
        }
        return obj;
    }

    public final Iterator iterator() {
        return new a(0);
    }

    public final ListIterator listIterator() {
        return new a(0);
    }

    public final ListIterator listIterator(int i) {
        synchronized (this.f2129b) {
            if (i >= 0) {
                if (i < this.f2128a.size()) {
                }
            }
            throw new IndexOutOfBoundsException();
        }
        return new a(i);
    }

    public final int size() {
        int i;
        synchronized (this.f2129b) {
            i = this.f2131d ? this.f2128a.size() : this.f2130c;
        }
        return i;
    }
}
