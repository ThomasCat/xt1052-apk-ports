package com.nuance.dragon.toolkit.elvis;

import com.nuance.dragon.toolkit.elvis.ElvisNbestList.Entry;
import com.nuance.dragon.toolkit.elvis.ElvisNbestList.Entry.Word;
import java.util.ArrayList;
import java.util.List;

public class DefaultResultFilter implements ElvisResultFilter {
    private static boolean a(Entry entry, Entry entry2) {
        String constraint = entry.getConstraint();
        String constraint2 = entry2.getConstraint();
        if ((constraint != null && constraint2 == null) || ((constraint == null && constraint2 != null) || (constraint != null && constraint2 != null && !constraint2.equals(constraint)))) {
            return false;
        }
        List words = entry.getWords();
        List words2 = entry2.getWords();
        int size = words.size();
        if (size != words2.size()) {
            return false;
        }
        for (int i = 0; i < size; i++) {
            if (!(((Word) words.get(i)).equals((Word) words2.get(i)))) {
                return false;
            }
        }
        return true;
    }

    public ElvisResult applyFilter(ElvisResult elvisResult) {
        if (elvisResult.getChoiceCount() <= 0) {
            return null;
        }
        ElvisNbestList choiceList = elvisResult.getChoiceList();
        ArrayList arrayList = new ArrayList(choiceList.size());
        arrayList.add((Entry) choiceList.get(0));
        int i = 1;
        while (true) {
            int i2 = i;
            if (i2 >= choiceList.size()) {
                break;
            }
            Entry entry = (Entry) choiceList.get(i2);
            int i3 = 0;
            while (true) {
                if (i3 >= arrayList.size()) {
                    break;
                } else if (a((Entry) arrayList.get(i3), entry)) {
                    entry = null;
                    break;
                } else {
                    i3++;
                }
            }
            if (entry != null) {
                arrayList.add(entry);
            }
            i = i2 + 1;
        }
        ElvisNbestList elvisNbestList = new ElvisNbestList((List) arrayList);
        return elvisNbestList.size() > 0 ? new ElvisResult(elvisNbestList, elvisResult.getGateConfidence()) : null;
    }
}
