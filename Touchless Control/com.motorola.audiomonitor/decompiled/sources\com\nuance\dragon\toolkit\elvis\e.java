package com.nuance.dragon.toolkit.elvis;

import com.nuance.dragon.toolkit.grammar.Word;
import java.util.List;

interface e {
    int a(WordSlot wordSlot);

    int a(Word word);

    int a(String str, int i, int i2, boolean z, boolean z2);

    int a(String str, Word word, boolean z);

    int a(String str, String str2, String str3, int i);

    int a(short[] sArr);

    ElvisResult a(Grammar grammar);

    Grammar a(String str);

    boolean a();

    boolean a(String str, int i);

    boolean a(String str, int i, String str2);

    boolean a(String str, Grammar grammar);

    boolean a(String str, String str2, String str3);

    boolean a(List list);

    boolean a(List list, int i);

    boolean a(boolean z);

    int b(short[] sArr);

    void b();

    void b(boolean z);

    boolean b(String str);

    boolean b(List list);

    boolean c();

    boolean c(String str);

    boolean d();

    boolean e();

    ElvisResult f();

    void g();

    int h();
}
