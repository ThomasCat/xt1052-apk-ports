package com.nuance.dragon.toolkit.util.internal;

public class NativeUtils {
    static {
        System.loadLibrary("dmt_native_utils");
    }

    public static native boolean isArmV7();

    public static native boolean isNeonSupported();
}
