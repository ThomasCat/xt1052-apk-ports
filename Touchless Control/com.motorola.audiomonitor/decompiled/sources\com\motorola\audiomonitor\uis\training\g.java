package com.motorola.audiomonitor.uis.training;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

class g extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ TrainingTriggerRecordingActivity f744a;

    g(TrainingTriggerRecordingActivity trainingTriggerRecordingActivity) {
        this.f744a = trainingTriggerRecordingActivity;
    }

    public void onReceive(Context context, Intent intent) {
        if (!isInitialStickyBroadcast()) {
            Bundle extras = intent.getExtras();
            if (extras != null && extras.containsKey("NOISE_DB") && extras.containsKey("NOISE_ESTIMATE")) {
                int i = extras.getInt("NOISE_ESTIMATE");
                int i2 = extras.getInt("NOISE_DB");
                if (TrainingTriggerRecordingActivity.f731b) {
                    Log.d(TrainingTriggerRecordingActivity.f730a, "onReceive: noiseDb=" + i2 + ", lastNoiseDb" + this.f744a.u);
                }
                if (i == -1) {
                    i = 2;
                }
                boolean z = (this.f744a.u == i2 && this.f744a.t == i) ? false : true;
                this.f744a.t = i;
                this.f744a.u = i2;
                if (this.f744a.o != 12 && this.f744a.o != 13) {
                    if (this.f744a.c(i, i2) >= 1) {
                        if (this.f744a.v != null) {
                            this.f744a.v.c();
                        }
                        this.f744a.l();
                        this.f744a.j.b(false);
                        this.f744a.p = 7;
                    } else if (this.f744a.v != null) {
                        if (this.f744a.t == i) {
                            this.f744a.v.a();
                        } else {
                            this.f744a.v.b();
                        }
                        this.f744a.v = null;
                    }
                    if (z) {
                        this.f744a.b(i, i2);
                    }
                    if (this.f744a.m) {
                        this.f744a.v = this.f744a.T;
                    } else {
                        this.f744a.v = null;
                    }
                    this.f744a.l.removeCallbacks(this.f744a.S);
                    this.f744a.l.postDelayed(this.f744a.S, 500);
                }
            }
        }
    }
}
