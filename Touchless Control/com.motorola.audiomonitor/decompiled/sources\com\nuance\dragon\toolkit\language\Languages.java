package com.nuance.dragon.toolkit.language;

import com.nuance.dragon.toolkit.elvis.ElvisLanguage;
import com.nuance.dragon.toolkit.recognizer.NMTLanguage;
import com.nuance.dragon.toolkit.vocalizer.VocalizerLanguage;

public class Languages extends a {
    public static Language lookupFromCloudLanguage(String str) {
        Language[] languageArr;
        for (Language language : ALL_LANGUAGES) {
            if (language.getCloudLanguage().equals(str)) {
                return language;
            }
        }
        return null;
    }

    public static Language lookupFromElvisLanguage(ElvisLanguage elvisLanguage) {
        Language[] languageArr;
        for (Language language : ALL_LANGUAGES) {
            if (language.getElvisLanguage().equals(elvisLanguage)) {
                return language;
            }
        }
        return null;
    }

    public static Language lookupFromGrammarLanguage(String str) {
        Language[] languageArr;
        for (Language language : ALL_LANGUAGES) {
            if (language.getGrammarLanguage().equals(str)) {
                return language;
            }
        }
        return null;
    }

    public static Language lookupFromVocalizerLanguage(VocalizerLanguage vocalizerLanguage) {
        Language[] languageArr;
        for (Language language : ALL_LANGUAGES) {
            if (language.getVocalizerLanguage().equals(vocalizerLanguage)) {
                return language;
            }
        }
        return null;
    }

    public static Language lookupFromVoconLanguage(NMTLanguage nMTLanguage) {
        Language[] languageArr;
        for (Language language : ALL_LANGUAGES) {
            if (language.getVoconLanguage().equals(nMTLanguage)) {
                return language;
            }
        }
        return null;
    }
}
