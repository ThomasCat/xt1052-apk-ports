package com.motorola.audiomonitor.pluginservice;

import android.os.IBinder.DeathRecipient;

class g implements DeathRecipient {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ InputPluginService f545a;

    g(InputPluginService inputPluginService) {
        this.f545a = inputPluginService;
    }

    public void binderDied() {
        synchronized (InputPluginService.class) {
            this.f545a.f531f = null;
            this.f545a.f();
        }
    }
}
