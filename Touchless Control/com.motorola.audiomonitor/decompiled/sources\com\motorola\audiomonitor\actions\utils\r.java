package com.motorola.audiomonitor.actions.utils;

import android.content.Context;
import android.util.Log;
import b.b;
import b.c;
import com.motorola.audiomonitor.a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class r implements d {

    /* renamed from: a reason: collision with root package name */
    private static final String f262a = ("AMonitor." + r.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f263b = a.f160b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final Object f264c = new Object();
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public final List f265d = new CopyOnWriteArrayList();

    /* renamed from: e reason: collision with root package name */
    private final p f266e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public Map f267f = new HashMap();

    public r(Context context, t tVar) {
        this.f265d.add(tVar);
        ArrayList arrayList = new ArrayList();
        arrayList.add(this);
        this.f266e = new p(context, arrayList);
    }

    private void a(Map map, String str, b bVar) {
        String[] split = str.split("\\s+");
        if (split.length > 2) {
            String str2 = null;
            int length = split.length;
            int i = 0;
            while (i < length) {
                String str3 = split[i];
                if (str2 != null) {
                    c(map, str2 + " " + str3, bVar);
                }
                i++;
                str2 = str3;
            }
        }
    }

    private boolean a(String str) {
        if (str.length() <= 50) {
            String replace = str.replace(".", "");
            if (replace.length() > 1) {
                return true;
            }
            if (!f263b) {
                return false;
            }
            Log.w(f262a, "skipping short name: " + replace);
            return false;
        } else if (!f263b) {
            return false;
        } else {
            Log.w(f262a, "skipping long name: " + str);
            return false;
        }
    }

    private void b(Map map, String str, b bVar) {
        String[] split = str.split("\\s+");
        if (split.length > 2) {
            c(map, split[0] + " " + split[split.length - 1], bVar);
        }
    }

    private void c(Map map, String str, b bVar) {
        String replaceAll = str.trim().replaceAll(" +", " ");
        if (a(replaceAll)) {
            List list = (List) map.get(replaceAll);
            if (list == null) {
                list = new ArrayList();
                map.put(replaceAll, list);
            }
            if (!list.contains(bVar)) {
                list.add(bVar);
            }
        }
    }

    public List a(b bVar) {
        synchronized (this.f264c) {
            if (this.f267f == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            for (c cVar : bVar.a()) {
                List<b> list = (List) this.f267f.get(cVar.a());
                if (list != null) {
                    for (b bVar2 : list) {
                        arrayList.add(new b(bVar2.a(), bVar2.b(), cVar.b(), (float) cVar.c()));
                    }
                }
            }
            return arrayList;
        }
    }

    public void a() {
        synchronized (this.f264c) {
            this.f267f.clear();
            this.f267f = null;
        }
        this.f266e.b();
        this.f265d.clear();
    }

    public void a(t tVar) {
        new Thread(new s(this, tVar)).start();
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.motorola.audiomonitor.actions.utils.b>, for r10v0, types: [java.util.List, java.util.List<com.motorola.audiomonitor.actions.utils.b>] */
    public void a(List<b> list) {
        Map map;
        if (list.size() != 0) {
            HashMap hashMap = new HashMap();
            for (b bVar : list) {
                String lowerCase = bVar.b().toLowerCase(Locale.getDefault());
                if (lowerCase.contains(",")) {
                    for (String c2 : lowerCase.split(",")) {
                        c(hashMap, c2, bVar);
                        b(hashMap, lowerCase, bVar);
                    }
                    lowerCase = lowerCase.replace(",", " ");
                }
                c(hashMap, lowerCase, bVar);
                b(hashMap, lowerCase, bVar);
                for (String c3 : lowerCase.split("\\s+")) {
                    c(hashMap, c3, bVar);
                }
                a(hashMap, lowerCase, bVar);
            }
            ArrayList arrayList = new ArrayList();
            synchronized (this.f264c) {
                map = this.f267f;
                this.f267f = hashMap;
                arrayList.addAll(this.f267f.keySet());
            }
            if (f263b) {
                Log.d(f262a, "onUpdate: contacts = " + list.size() + " names =" + arrayList.size());
            }
            for (t a2 : this.f265d) {
                a2.a(arrayList);
            }
            if (map != null) {
                map.clear();
            }
        }
    }

    public void b(t tVar) {
        this.f265d.remove(tVar);
    }
}
