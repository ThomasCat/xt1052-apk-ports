package com.nuance.a.a.a.b.c.b.b;

import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.dragon.toolkit.data.Data;

public class s extends l {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f877a = a.a(s.class);

    public s(o oVar) {
        super(515);
        if (f877a.b()) {
            f877a.b((Object) "PDXQueryParameter()");
        }
        a("name", oVar.b(), 193);
        if (oVar.c() == Byte.MAX_VALUE) {
            a("type", 5);
        } else {
            a("type", (int) oVar.c());
        }
        switch (oVar.c()) {
            case 1:
                a("buffer_id", ((c) oVar).a());
                return;
            case 2:
                a("text", ((y) oVar).a(), 193);
                return;
            case 3:
                a("text", ((e) oVar).a(), 193);
                return;
            case 4:
                a("data", ((g) oVar).a(), 193);
                return;
            case 5:
                a("dict", ((h) oVar).a(), (short) Data.TYPE_DICT);
                return;
            case 6:
            case 7:
            case 8:
                a("dict", ((v) oVar).a(), (short) Data.TYPE_DICT);
                return;
            case Byte.MAX_VALUE:
                a("dict", ((x) oVar).d(), (short) Data.TYPE_DICT);
                return;
            default:
                f877a.e("PDXQueryParameter() Unknown parameter type: " + oVar.c() + ". ");
                return;
        }
    }
}
