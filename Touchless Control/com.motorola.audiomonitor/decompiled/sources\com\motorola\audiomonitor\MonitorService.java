package com.motorola.audiomonitor;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import c.a;
import c.f;
import com.motorola.audiomonitor.actions.utils.ak;
import com.motorola.audiomonitor.pluginservice.InputPluginService;
import com.motorola.audiomonitor.pluginservice.h;
import com.motorola.audiomonitor.service.d;
import com.motorola.audiomonitor.service.o;
import f.i;
import h.g;
import h.k;
import java.util.Locale;
import java.util.Vector;

public class MonitorService extends Service {
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final String f149b = ("AMonitor." + MonitorService.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public static final boolean f150c = a.f160b;

    /* renamed from: a reason: collision with root package name */
    cl f151a = new as(this);
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public ab f152d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public cc f153e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public d f154f;

    /* renamed from: g reason: collision with root package name */
    private h f155g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public final Handler f156h = new Handler();
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public final RemoteCallbackList j = new RemoteCallbackList();
    private final o k = new ag(this);
    private final u l = new aj(this);

    private void a(int i2) {
        int beginBroadcast = this.j.beginBroadcast();
        for (int i3 = 0; i3 < beginBroadcast; i3++) {
            try {
                ((w) this.j.getBroadcastItem(i3)).a(i2);
            } catch (RemoteException e2) {
                Log.e(f149b, "Remote exception while notifying service state");
            }
        }
        this.j.finishBroadcast();
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3) {
        int beginBroadcast = this.j.beginBroadcast();
        for (int i4 = 0; i4 < beginBroadcast; i4++) {
            try {
                ((w) this.j.getBroadcastItem(i4)).a(i2, i3);
            } catch (RemoteException e2) {
                Log.e(f149b, "Remote exception while notifying enrollment event");
            }
        }
        this.j.finishBroadcast();
    }

    private static void a(Context context, boolean z) {
        Vector vector = new Vector();
        vector.add(new f("aov_enabled", z + ""));
        vector.add(new f("com_bypass", a.m + ""));
        vector.add(new f("language", Locale.getDefault() + ""));
        a.a(context, "SETTINGS", vector.iterator());
    }

    /* access modifiers changed from: private */
    public synchronized void a(String str) {
        if (this.f153e == null) {
            this.f153e = new cc(this, str, this.f151a, this.f154f);
            this.f153e.a();
        }
        a.i = true;
        b(1);
    }

    /* access modifiers changed from: private */
    public synchronized void a(boolean z) {
        if (this.f153e != null) {
            if (z) {
                this.f153e.f();
            }
            this.f153e.b();
            this.f153e = null;
        }
        a.i = false;
        b(0);
    }

    private void b(int i2) {
        int beginBroadcast = this.j.beginBroadcast();
        for (int i3 = 0; i3 < beginBroadcast; i3++) {
            try {
                ((w) this.j.getBroadcastItem(i3)).b(i2);
            } catch (RemoteException e2) {
                Log.e(f149b, "Remote exception while notifying training mode");
            }
        }
        this.j.finishBroadcast();
    }

    /* access modifiers changed from: private */
    public synchronized void b(boolean z) {
        if (this.f153e != null) {
            this.f153e.a(z);
        }
    }

    private void c() {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        a.n = Locale.getDefault().toString();
        a.k = k.a(Locale.getDefault());
        a.l = defaultSharedPreferences.getBoolean("disp_off_trig", a.l);
        a.m = defaultSharedPreferences.getBoolean("lock_bypass", a.m);
        a.r = defaultSharedPreferences.getString("call_confirmation", a.r);
        i.a(getApplicationContext());
    }

    /* access modifiers changed from: private */
    public void c(int i2) {
        int beginBroadcast = this.j.beginBroadcast();
        for (int i3 = 0; i3 < beginBroadcast; i3++) {
            try {
                ((w) this.j.getBroadcastItem(i3)).c(i2);
            } catch (RemoteException e2) {
                Log.e(f149b, "Remote exception while notifying enrollment audio");
            }
        }
        this.j.finishBroadcast();
    }

    /* access modifiers changed from: private */
    public synchronized void d() {
        if (!k.a(Locale.getDefault())) {
            if (f150c) {
                Log.w(f149b, "Unsupported language. Do not start monitoring.");
            }
            k.b((Context) this, 3);
            a.f166h = false;
            k.a((Context) this, "monitor_status", false);
            stopSelf();
        } else {
            if (!a.j) {
                if (!k.a("customphrase")) {
                    k.b((Context) this, 1);
                    a.f166h = false;
                    k.a((Context) this, "monitor_status", false);
                    stopSelf();
                } else if (!k.a((Context) this, PreferenceManager.getDefaultSharedPreferences(this))) {
                    k.b((Context) this, 1);
                    a.f166h = false;
                    k.a((Context) this, "monitor_status", false);
                    stopSelf();
                }
            } else if (!k.a((Context) this, "hellomoto")) {
                a.f166h = false;
                k.a((Context) this, "monitor_status", false);
                stopSelf();
            }
            if (this.i && this.f152d == null) {
                c();
                g.a().d();
                if (this.f155g != null) {
                    this.f152d = new ba((Context) this, this.f155g);
                } else if (a.k) {
                    this.f152d = new ba((Context) this, this.f154f);
                } else {
                    this.f152d = new bu(this, this.f154f);
                }
                this.f152d.b();
                k.g(getApplicationContext());
            }
            com.motorola.audiomonitor.actions.utils.g.a(getApplicationContext(), true);
            a.f166h = true;
            k.a((Context) this, "monitor_status", true);
            k.b((Context) this, 2);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void e() {
        if (this.f152d != null) {
            this.f152d.c();
            this.f152d = null;
            g.a().e();
        }
        com.motorola.audiomonitor.actions.utils.g.a(getApplicationContext(), false);
        a.f166h = false;
        k.a((Context) this, "monitor_status", false);
        k.b((Context) this, 3);
    }

    /* access modifiers changed from: private */
    public synchronized void f() {
        if (this.f153e != null) {
            this.f153e.c();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void g() {
        if (this.f153e != null) {
            this.f153e.d();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void h() {
        if (this.f153e != null) {
            this.f153e.e();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void i() {
        if (this.i) {
            this.i = false;
            a(false);
            e();
            if (this.f155g != null) {
                this.f155g = null;
            } else {
                this.f154f.b();
                this.f154f = null;
            }
            g.a().r();
            a(getApplicationContext(), false);
            stopSelf();
            a(0);
        }
    }

    public IBinder onBind(Intent intent) {
        return this.l;
    }

    public void onCreate() {
        super.onCreate();
        ak.a((Context) this);
        g.a().a(getApplicationContext());
        g.a().c();
    }

    public void onDestroy() {
        if (f150c) {
            Log.d(f149b, "Destroying Service");
        }
        i();
        ak.c();
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        boolean z;
        boolean z2;
        boolean z3 = false;
        if (!MonitorApplication.a()) {
            if (f150c) {
                Log.d(f149b, "App ain't initialized, delay service start");
            }
            this.f156h.postDelayed(new at(this, intent, i2, i3), 100);
        } else {
            if (f150c) {
                Log.d(f149b, "Starting Service");
            }
            if (intent != null) {
                String stringExtra = intent.getStringExtra("MONITOR_STATE");
                boolean z4 = TextUtils.equals(stringExtra, "restart");
                if (TextUtils.equals(stringExtra, "stop")) {
                    z2 = z4;
                    z = true;
                } else {
                    z2 = z4;
                    z = false;
                }
            } else {
                z = false;
                z2 = false;
            }
            synchronized (this) {
                if (this.i || z) {
                    if (z2) {
                        if (f150c) {
                            Log.d(f149b, "Restart Recognition");
                        }
                        e();
                        d();
                    }
                    if (z) {
                        if (f150c) {
                            Log.d(f149b, "Stop Monitoring");
                        }
                        i();
                    }
                } else {
                    this.i = true;
                    this.f155g = InputPluginService.a();
                    if (this.f155g != null) {
                        d();
                    } else {
                        this.f154f = new d();
                        this.f154f.a(this.k);
                        this.f154f.a((Context) this, PreferenceManager.getDefaultSharedPreferences(this));
                    }
                    g a2 = g.a();
                    if (this.f155g != null) {
                        z3 = true;
                    }
                    a2.b(z3);
                    g.a().b();
                    a(getApplicationContext(), true);
                    a(1);
                }
            }
        }
        return 1;
    }
}
