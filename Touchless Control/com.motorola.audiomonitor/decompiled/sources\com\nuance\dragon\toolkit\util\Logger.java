package com.nuance.dragon.toolkit.util;

import android.util.Log;
import com.nuance.dragon.toolkit.util.internal.e;

public final class Logger {

    final class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public static final boolean f2108a = (!"Obf".equals(a.class.getSimpleName()));

        private a() {
        }
    }

    private static String a(Object obj, String str) {
        if (obj == null) {
            return str;
        }
        String a2 = a.f2108a ? null : obj instanceof String ? (String) obj : obj instanceof Class ? e.a((Class) obj) : e.a(obj.getClass());
        return (a2 == null || a2.length() == 0) ? str : a2 + ": " + str;
    }

    public static void debug(Object obj, String str) {
        Log.d("NMT", a(obj, str));
    }

    public static void error(Object obj, String str) {
        Log.e("NMT", a(obj, str));
    }

    public static void error(Object obj, String str, Throwable th) {
        Log.e("NMT", a(obj, str), th);
    }

    public static void info(Object obj, String str) {
        Log.i("NMT", a(obj, str));
    }

    public static void warn(Object obj, String str) {
        Log.w("NMT", a(obj, str));
    }
}
