package com.motorola.audiomonitor.pluginservice;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.util.Log;
import com.motorola.audiomonitor.MonitorService;
import com.motorola.audiomonitor.a;
import g.e;

public class InputPluginService extends Service {
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final String f526b = ("AMonitor." + InputPluginService.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public static final boolean f527c = a.f160b;

    /* renamed from: d reason: collision with root package name */
    private static i f528d = new e();
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public static h f529e;

    /* renamed from: a reason: collision with root package name */
    e f530a = new f(this);
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public g.a f531f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public final DeathRecipient f532g = new g(this);

    public static h a() {
        h hVar;
        synchronized (InputPluginService.class) {
            hVar = f529e;
        }
        return hVar;
    }

    public static i b() {
        h a2 = a();
        return a2 == null ? f528d : a2;
    }

    /* access modifiers changed from: private */
    public void f() {
        if (f529e != null) {
            f529e.f546a.a((g.a) null);
            f529e.f547b.a(null);
            f529e.f549d.a((g.a) null);
            f529e = null;
        }
        if (this.f531f != null) {
            this.f531f.asBinder().unlinkToDeath(this.f532g, 0);
            this.f531f = null;
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        Intent intent = new Intent(this, MonitorService.class);
        stopService(intent);
        startService(intent);
    }

    public IBinder onBind(Intent intent) {
        if (f527c) {
            Log.d(f526b, "A plugin bound");
        }
        return this.f530a;
    }

    public void onDestroy() {
        synchronized (InputPluginService.class) {
            f();
        }
        if (f527c) {
            Log.d(f526b, "Destroyed");
        }
    }
}
