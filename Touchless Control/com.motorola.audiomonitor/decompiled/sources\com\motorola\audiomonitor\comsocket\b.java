package com.motorola.audiomonitor.comsocket;

import android.net.Credentials;
import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;
import com.motorola.audiomonitor.GenericRecognizer.UdtEnrollResult;
import com.motorola.audiomonitor.ProtectedByteBuffer;
import com.motorola.audiomonitor.a;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;

public class b extends Thread {

    /* renamed from: a reason: collision with root package name */
    private static final String f442a = ("AMonitor." + b.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f443b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private LocalServerSocket f444c;

    /* renamed from: d reason: collision with root package name */
    private final LinkedList f445d = new LinkedList();
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public final a f446e;

    /* renamed from: f reason: collision with root package name */
    private boolean f447f = true;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public e f448g;

    /* renamed from: h reason: collision with root package name */
    private final AtomicBoolean f449h = new AtomicBoolean(false);

    public b(e eVar) {
        super("MediaCommandManager");
        this.f448g = eVar;
        this.f446e = new a();
        this.f446e.start();
    }

    private void a(LocalSocket localSocket, d dVar) {
        f fVar = new f(localSocket, new c(this), dVar);
        synchronized (this.f445d) {
            this.f445d.add(fVar);
        }
        fVar.start();
    }

    /* access modifiers changed from: private */
    public void a(f fVar) {
        synchronized (this.f445d) {
            this.f445d.remove(fVar);
        }
    }

    private boolean a(int i) {
        byte[] bArr = new byte[UdtEnrollResult.CHECKRECORDING_POOR_RECORDINGS];
        File file = new File("/proc/" + i + "/cmdline");
        if (!file.exists() || !file.canRead()) {
            return false;
        }
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            int read = fileInputStream.read(bArr);
            int i2 = 0;
            while (i2 < read && bArr[i2] != 0) {
                i2++;
            }
            if (f443b) {
                Log.d(f442a, "pid file read count=" + read + " last null position=" + i2);
            }
            String str = new String(bArr, 0, i2);
            if (f443b) {
                Log.d(f442a, "Command Line for " + i + " : " + str);
            }
            boolean equals = TextUtils.equals("com.google.android.googlequicksearchbox", str);
            fileInputStream.close();
            return equals;
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            return false;
        } catch (IOException e3) {
            return false;
        }
    }

    private void d() {
        LocalSocket localSocket = new LocalSocket();
        try {
            localSocket.connect(new LocalSocketAddress("com.motorola.audiomonitor"));
            localSocket.close();
        } catch (IOException e2) {
            Log.e(f442a, "Error cleaning up server socket" + e2.toString());
        }
    }

    public void a() {
        this.f447f = false;
        d();
        try {
            join(3000);
        } catch (InterruptedException e2) {
        }
        this.f448g = null;
        this.f446e.c();
        if (f443b) {
            Log.d(f442a, "MediaCommandManager cleanUp completed.");
        }
    }

    public void a(ProtectedByteBuffer protectedByteBuffer) {
        this.f446e.a(protectedByteBuffer);
    }

    public void a(boolean z) {
        this.f449h.set(z);
    }

    public void b() {
        this.f446e.b();
    }

    public void c() {
        f d2 = this.f446e.d();
        if (d2 != null) {
            d2.b();
        }
    }

    public void run() {
        try {
            this.f444c = new LocalServerSocket("com.motorola.audiomonitor");
        } catch (IOException e2) {
            Log.e(f442a, "LocalServer Sock creation failure");
            this.f447f = false;
        }
        while (this.f447f) {
            try {
                LocalSocket accept = this.f444c.accept();
                if (this.f447f) {
                    Credentials peerCredentials = accept.getPeerCredentials();
                    if (f443b) {
                        Log.d(f442a, "New Connection: pid=" + peerCredentials.getPid());
                    }
                    if (a(peerCredentials.getPid())) {
                        if (!this.f449h.get()) {
                            a(accept, d.CONFLICT_LINK);
                        } else {
                            a(accept, d.RECOG_LINK);
                        }
                    } else if (peerCredentials.getPid() == Process.myPid()) {
                        a(accept, d.OTHER_LINK);
                    } else {
                        a(accept, d.CONFLICT_LINK);
                    }
                }
            } catch (IOException e3) {
                Log.e(f442a, "LocalServer Sock I/O failure");
                this.f447f = false;
            }
        }
        try {
            if (this.f444c != null) {
                this.f444c.close();
            }
        } catch (IOException e4) {
        }
        if (f443b) {
            Log.d(f442a, "Exiting CommandBufferManager thread.");
        }
        synchronized (this.f445d) {
            Iterator it = this.f445d.iterator();
            while (it.hasNext()) {
                ((f) it.next()).b();
            }
            this.f445d.clear();
        }
    }
}
