package com.motorola.audiomonitor;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

public abstract class x extends Binder implements w {
    public x() {
        attachInterface(this, "com.motorola.audiomonitor.IMonitorServiceListener");
    }

    public static w a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.motorola.audiomonitor.IMonitorServiceListener");
        return (queryLocalInterface == null || !(queryLocalInterface instanceof w)) ? new y(iBinder) : (w) queryLocalInterface;
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                parcel.enforceInterface("com.motorola.audiomonitor.IMonitorServiceListener");
                a(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 2:
                parcel.enforceInterface("com.motorola.audiomonitor.IMonitorServiceListener");
                b(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 3:
                parcel.enforceInterface("com.motorola.audiomonitor.IMonitorServiceListener");
                a(parcel.readInt(), parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 4:
                parcel.enforceInterface("com.motorola.audiomonitor.IMonitorServiceListener");
                c(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 1598968902:
                parcel2.writeString("com.motorola.audiomonitor.IMonitorServiceListener");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
