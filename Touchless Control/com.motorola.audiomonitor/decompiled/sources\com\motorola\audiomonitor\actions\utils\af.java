package com.motorola.audiomonitor.actions.utils;

import android.util.Log;

class af implements Runnable {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ ae f195a;

    af(ae aeVar) {
        this.f195a = aeVar;
    }

    public void run() {
        if (ae.f188b) {
            Log.d(ae.f187a, "Network TTS Timeout and switch to Local TTS");
        }
        if (this.f195a.f192f != null) {
            this.f195a.f192f.stop();
            this.f195a.a();
            if (!this.f195a.c(0)) {
                this.f195a.b(this.f195a.f193g);
            }
        }
    }
}
