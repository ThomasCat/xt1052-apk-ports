package com.motorola.audiomonitor.service;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;

public class n implements OnSharedPreferenceChangeListener {

    /* renamed from: a reason: collision with root package name */
    public boolean f580a = true;

    /* renamed from: b reason: collision with root package name */
    public int f581b = 5000;

    /* renamed from: c reason: collision with root package name */
    public int f582c = 100;

    /* renamed from: d reason: collision with root package name */
    public int f583d = 60000;

    /* renamed from: e reason: collision with root package name */
    public String f584e = "";

    /* renamed from: f reason: collision with root package name */
    public String f585f = "";

    public void a(SharedPreferences sharedPreferences) {
        this.f580a = sharedPreferences.getBoolean("autoenable", true);
        this.f581b = sharedPreferences.getInt("connectionretrytimeout", 5000);
        this.f582c = sharedPreferences.getInt("connectionmaxretries", 100);
        this.f583d = sharedPreferences.getInt("requesttimeout", 60000);
        this.f584e = sharedPreferences.getString("acousticmodel", "");
        this.f585f = sharedPreferences.getString("languagemodel", "");
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        if ("autoenable".equals(str)) {
            this.f580a = sharedPreferences.getBoolean("autoenable", true);
        } else if ("connectionretrytimeout".equals(str)) {
            this.f581b = sharedPreferences.getInt("connectionretrytimeout", 5000);
        } else if ("connectionmaxretries".equals(str)) {
            this.f582c = sharedPreferences.getInt("connectionmaxretries", 100);
        } else if ("requesttimeout".equals(str)) {
            this.f583d = sharedPreferences.getInt("requesttimeout", 60000);
        } else if ("acousticmodel".equals(str)) {
            this.f584e = sharedPreferences.getString("acousticmodel", "");
        } else if ("languagemodel".equals(str)) {
            this.f585f = sharedPreferences.getString("languagemodel", "");
        }
    }
}
