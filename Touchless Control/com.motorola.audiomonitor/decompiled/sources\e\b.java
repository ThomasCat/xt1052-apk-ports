package e;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

class b extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ a f2205a;

    b(a aVar) {
        this.f2205a = aVar;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null) {
            if (action.equals("android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED")) {
                this.f2205a.m();
            } else if (action.equals("android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED")) {
                int intExtra = intent.getIntExtra("android.bluetooth.profile.extra.STATE", -1);
                int intExtra2 = intent.getIntExtra("android.bluetooth.profile.extra.PREVIOUS_STATE", -1);
                BluetoothDevice bluetoothDevice = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                if (a.f2198b) {
                    Log.d(a.f2197a, "SCO Audio State change: " + intExtra + " prev: " + intExtra2);
                }
                if (this.f2205a.f2200d != null && this.f2205a.f2201e != null && this.f2205a.f2201e.equals(bluetoothDevice)) {
                    if (intExtra2 != 12 && intExtra == 12) {
                        this.f2205a.a(true);
                    } else if (intExtra2 == 12 && intExtra != 12) {
                        this.f2205a.a(false);
                    }
                }
            } else if (action.equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                int intExtra3 = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", -1);
                int intExtra4 = intent.getIntExtra("android.bluetooth.adapter.extra.PREVIOUS_STATE", -1);
                if (a.f2198b) {
                    Log.d(a.f2197a, "Bluetooth State change: " + intExtra3 + " prev: " + intExtra4);
                }
                if (intExtra3 == 12 && this.f2205a.f2200d == null) {
                    this.f2205a.k();
                }
            }
        }
    }
}
