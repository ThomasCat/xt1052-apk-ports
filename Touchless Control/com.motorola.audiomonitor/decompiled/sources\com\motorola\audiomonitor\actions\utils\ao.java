package com.motorola.audiomonitor.actions.utils;

import android.database.ContentObserver;
import android.os.Handler;
import android.text.TextUtils;
import java.util.Map;

class ao extends ContentObserver {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ an f218a;

    public ao(an anVar, Handler handler) {
        this.f218a = anVar;
        super(handler);
        a();
    }

    private void a() {
        Map a2 = this.f218a.f217f.a();
        String str = (String) a2.get("data2");
        if (!TextUtils.isEmpty(str)) {
            this.f218a.f215d = str;
            return;
        }
        String str2 = (String) a2.get("display_name");
        if (!TextUtils.isEmpty(str2)) {
            this.f218a.f215d = str2;
        } else {
            this.f218a.f215d = null;
        }
    }

    public boolean deliverSelfNotifications() {
        return true;
    }

    public void onChange(boolean z) {
        super.onChange(z);
        a();
    }
}
