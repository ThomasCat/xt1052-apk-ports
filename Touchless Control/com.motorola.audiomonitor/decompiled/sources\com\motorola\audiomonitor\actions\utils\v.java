package com.motorola.audiomonitor.actions.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.ProtectedByteBuffer;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.k;
import e.e;
import f.f;
import f.h;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class v implements k, e {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f273a = ("AMonitor." + v.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f274b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private final Context f275c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public final Handler f276d;

    /* renamed from: e reason: collision with root package name */
    private final Object f277e = new Object();

    /* renamed from: f reason: collision with root package name */
    private boolean f278f = false;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public ac f279g = ac.NO_PROMPT;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public long f280h;
    private k i;
    private ad j;
    private final ae k;
    private final List l;
    private final Object m = new Object();
    private final f n;
    private f o;
    private int p = 16000;
    /* access modifiers changed from: private */
    public final String q;
    /* access modifiers changed from: private */
    public final String r;
    private final h s = new w(this);
    private final ab t;
    /* access modifiers changed from: private */
    public ai u;
    private final ai v = new x(this);
    private final l w = new y(this);
    private final Runnable x = new z(this);
    /* access modifiers changed from: private */
    public final Runnable y = new aa(this);

    public v(Context context, Handler handler, ab abVar) {
        this.f275c = context;
        this.f276d = handler;
        this.t = abVar;
        this.k = new ae(context);
        Resources resources = this.f275c.getResources();
        this.q = resources.getString(C0014R.string.spotter_call).toLowerCase(Locale.getDefault());
        this.r = resources.getString(C0014R.string.spotter_cancel).toLowerCase(Locale.getDefault());
        this.l = Arrays.asList(new String[]{this.q, this.r});
        this.n = new f(16000, this.s, this.l, true);
    }

    private void a(ad adVar) {
        i();
        this.k.c();
        if (this.i != null && this.f279g.f180g == 2) {
            this.i.b();
        }
        this.j = adVar;
        this.f276d.removeCallbacks(this.y);
    }

    /* access modifiers changed from: private */
    public void b(ad adVar) {
        if (f274b) {
            Log.d(f273a, "handleUserResponse: " + adVar);
        }
        synchronized (this.f277e) {
            a(adVar);
            this.f277e.notify();
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        synchronized (this.m) {
            f h2 = h();
            if (h2 != null) {
                if (z) {
                    h2.b();
                }
                h2.a();
            }
        }
    }

    private f h() {
        f fVar = null;
        if (16000 == this.p) {
            fVar = this.n;
        } else if (this.p == 8000) {
            fVar = this.o;
        }
        if (fVar == null && f274b) {
            Log.w(f273a, "No suitable spotter found for rate = " + this.p);
        }
        return fVar;
    }

    private void i() {
        synchronized (this.m) {
            f h2 = h();
            if (h2 != null) {
                h2.b();
            }
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.f279g.f180g == 1) {
            b(ad.RESPONSE_POSITIVE);
        } else if (this.f279g.f181h == 2) {
            if (this.f280h > 0) {
                this.f276d.postDelayed(this.y, this.f280h);
            }
            this.f276d.postDelayed(this.x, 550);
            this.i.c();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00ef, code lost:
        r7.f278f = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        return r7.j;
     */
    @SuppressLint({"Wakelock"})
    public ad a(ac acVar, long j2, k kVar, List list, ai aiVar) {
        WakeLock wakeLock;
        if (acVar == ac.NO_PROMPT) {
            Log.e(f273a, "Invalid arguments for prompter.");
            return ad.RESPONSE_NEGATIVE;
        }
        synchronized (this.f277e) {
            if (this.f278f) {
                if (f274b) {
                    Log.d(f273a, "Abort promptUser");
                }
                this.f278f = false;
                ad adVar = ad.RESPONSE_ABORT;
                return adVar;
            }
            this.f279g = acVar;
            this.f280h = j2;
            this.i = kVar;
            this.u = aiVar;
            if (this.i != null) {
                if (f274b) {
                    Log.d(f273a, this.i.getClass().getSimpleName() + "\n" + this.i.toString());
                }
                this.i.a(this.f276d);
                this.i.a(this.w);
                this.i.a();
            }
            PowerManager powerManager = (PowerManager) this.f275c.getSystemService("power");
            if (powerManager != null) {
                wakeLock = powerManager.newWakeLock(536870922, f273a);
                wakeLock.acquire(60000);
            } else {
                wakeLock = null;
            }
            if (list != null && list.size() > 0) {
                this.k.a(this.v);
                this.k.a(list);
                if (this.f279g.f180g != 0) {
                    this.f276d.postDelayed(this.y, 20000);
                }
            }
            if (acVar.f181h == 1) {
                a();
            }
            try {
                this.f277e.wait();
            } catch (InterruptedException e2) {
                Log.e(f273a, "Prompt interrupted.");
                this.j = ad.RESPONSE_NEGATIVE;
            }
            if (this.f278f) {
                Log.e(f273a, "Prompt Aborted.");
                this.j = ad.RESPONSE_ABORT;
            }
            this.t.a(false);
            this.f279g = ac.NO_PROMPT;
            if (wakeLock != null && wakeLock.isHeld()) {
                wakeLock.release();
            }
        }
    }

    public void a() {
        synchronized (this.f277e) {
            b(false);
            if (f274b) {
                Log.d(f273a, "request audio");
            }
            this.t.a(true);
        }
    }

    public void a(int i2) {
        synchronized (this.m) {
            this.p = i2;
        }
    }

    public void a(boolean z) {
        synchronized (this.m) {
            if (z) {
                this.o = new f(8000, this.s, this.l, true);
            } else if (this.o != null) {
                this.o.c();
                this.o = null;
            }
        }
    }

    public boolean a(ProtectedByteBuffer protectedByteBuffer) {
        synchronized (this.m) {
            f h2 = h();
            if (h2 != null) {
                h2.a(protectedByteBuffer);
            }
        }
        return true;
    }

    public void b() {
        this.f278f = false;
    }

    public void c() {
        synchronized (this.f277e) {
            this.f278f = true;
            a(ad.RESPONSE_ABORT);
            this.f277e.notify();
        }
    }

    public int d() {
        return this.p;
    }

    public void e() {
        if (this.i != null) {
            this.i.b();
        }
        synchronized (this.f277e) {
            this.j = ad.RESPONSE_NEGATIVE;
            this.f277e.notify();
        }
        synchronized (this.m) {
            this.n.c();
            if (this.o != null) {
                this.o.c();
                this.o = null;
            }
        }
        this.k.d();
    }
}
