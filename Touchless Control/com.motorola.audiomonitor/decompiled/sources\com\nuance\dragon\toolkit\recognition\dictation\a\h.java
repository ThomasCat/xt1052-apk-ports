package com.nuance.dragon.toolkit.recognition.dictation.a;

import com.nuance.dragon.toolkit.recognition.dictation.EditorItem.Type;
import com.nuance.dragon.toolkit.recognition.dictation.Token;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import org.json.JSONObject;

public final class h implements Token, JSONCompliant {

    /* renamed from: a reason: collision with root package name */
    private String f1948a;

    /* renamed from: b reason: collision with root package name */
    private long f1949b;

    /* renamed from: c reason: collision with root package name */
    private long f1950c;

    /* renamed from: d reason: collision with root package name */
    private double f1951d;

    /* renamed from: e reason: collision with root package name */
    private boolean f1952e = false;

    /* renamed from: f reason: collision with root package name */
    private boolean f1953f = false;

    public h(String str, long j, long j2, double d2) {
        this.f1948a = new String(str);
        this.f1949b = j;
        this.f1950c = j2;
        this.f1951d = d2;
        if (this.f1948a.indexOf("\\*no-space-before") != -1) {
            this.f1952e = true;
            int indexOf = this.f1948a.indexOf("\\*no-space-before");
            if (indexOf + 17 == this.f1948a.length()) {
                this.f1948a = this.f1948a.substring(0, indexOf);
            } else {
                this.f1948a = this.f1948a.substring(0, indexOf) + this.f1948a.substring(indexOf + 17);
            }
        }
        if (this.f1948a.indexOf("\\*no-space-after") != -1) {
            this.f1953f = true;
            int indexOf2 = this.f1948a.indexOf("\\*no-space-after");
            if (indexOf2 + 16 == this.f1948a.length()) {
                this.f1948a = this.f1948a.substring(0, indexOf2);
            } else {
                this.f1948a = this.f1948a.substring(0, indexOf2) + this.f1948a.substring(indexOf2 + 16);
            }
        }
    }

    static h a(JSONObject jSONObject) {
        return new h(jSONObject.getString("word"), jSONObject.getLong("start"), jSONObject.getLong("end"), jSONObject.getDouble("conf"));
    }

    public final String a() {
        return this.f1948a;
    }

    public final void a(long j) {
        this.f1949b = j;
    }

    public final void a(String str) {
        this.f1948a = str;
    }

    /* renamed from: b */
    public final h clone() {
        h hVar = new h(this.f1948a, this.f1949b, this.f1950c, this.f1951d);
        hVar.f1953f = this.f1953f;
        hVar.f1952e = this.f1952e;
        return hVar;
    }

    public final void b(long j) {
        this.f1950c = j;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof h)) {
            return false;
        }
        h hVar = (h) obj;
        return this.f1948a.equals(hVar.f1948a) && this.f1949b == hVar.f1949b && this.f1950c == hVar.f1950c && this.f1951d == hVar.f1951d;
    }

    public final double getConfidenceScore() {
        return this.f1951d;
    }

    public final long getEndTime() {
        return this.f1950c;
    }

    public final long getStartTime() {
        return this.f1949b;
    }

    public final Type getType() {
        return Type.TOKEN;
    }

    public final boolean hasNoSpaceAfterDirective() {
        return this.f1953f;
    }

    public final boolean hasNoSpaceBeforeDirective() {
        return this.f1952e;
    }

    public final int hashCode() {
        return this.f1948a.hashCode() + ((int) this.f1949b) + ((int) this.f1950c) + ((int) (this.f1951d * 1000.0d));
    }

    public final int length() {
        return this.f1948a.length();
    }

    public final JSONObject toJSON() {
        b bVar = new b();
        bVar.a("word", (Object) this.f1948a);
        bVar.a("start", (Object) Long.valueOf(this.f1949b));
        bVar.a("end", (Object) Long.valueOf(this.f1950c));
        bVar.a("conf", (Object) Double.valueOf(this.f1951d));
        return bVar;
    }

    public final String toString() {
        return new String(this.f1948a);
    }
}
