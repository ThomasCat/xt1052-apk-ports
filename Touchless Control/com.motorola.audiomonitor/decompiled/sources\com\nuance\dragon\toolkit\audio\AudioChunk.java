package com.nuance.dragon.toolkit.audio;

import android.os.SystemClock;
import com.nuance.dragon.toolkit.audio.AbstractAudioChunk.SpeechStatus;

public class AudioChunk extends AbstractAudioChunk {
    public AudioChunk(AudioType audioType, byte[] bArr, int i) {
        this(audioType, null, bArr, 0, i, SpeechStatus.UNKNOWN);
    }

    public AudioChunk(AudioType audioType, byte[] bArr, int i, SpeechStatus speechStatus) {
        this(audioType, null, bArr, SystemClock.uptimeMillis(), i, speechStatus);
    }

    public AudioChunk(AudioType audioType, short[] sArr) {
        this(audioType, sArr, null, SystemClock.uptimeMillis(), audioType.getDuration(sArr), SpeechStatus.UNKNOWN);
    }

    public AudioChunk(AudioType audioType, short[] sArr, long j) {
        this(audioType, sArr, null, j, audioType.getDuration(sArr), SpeechStatus.UNKNOWN);
    }

    public AudioChunk(AudioType audioType, short[] sArr, SpeechStatus speechStatus) {
        this(audioType, sArr, null, SystemClock.uptimeMillis(), audioType.getDuration(sArr), speechStatus);
    }

    private AudioChunk(AudioType audioType, short[] sArr, byte[] bArr, long j, int i, SpeechStatus speechStatus) {
        super(audioType, sArr, bArr, j, i, speechStatus);
    }

    public float getAudioEnergyLevel() {
        double log10;
        long j = 0;
        int i = 0;
        if (this.audioShorts != null) {
            short[] sArr = this.audioShorts;
            while (i < sArr.length) {
                long j2 = (long) sArr[i];
                j += (j2 * j2) >> 9;
                i++;
            }
        } else {
            byte[] bArr = this.audioBytes;
            while (i < bArr.length) {
                long j3 = (long) bArr[i];
                j += (j3 * j3) >> 9;
                i++;
            }
        }
        double d2 = ((double) j) / 1.073741824E9d;
        if (d2 < 1.0E-9d) {
            log10 = -90.0d;
        } else {
            log10 = Math.log10(d2) * 10.0d;
            if (log10 > 0.0d) {
                log10 = 0.0d;
            }
        }
        return (float) ((long) (((float) log10) + 90.0f));
    }
}
