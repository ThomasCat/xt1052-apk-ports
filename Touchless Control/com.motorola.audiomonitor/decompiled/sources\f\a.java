package f;

import android.os.Handler;
import android.util.Log;
import com.motorola.audiomonitor.ProtectedByteBuffer;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.sources.RecorderSource;

public class a extends RecorderSource {

    /* renamed from: a reason: collision with root package name */
    private static final String f2208a = ("AMonitor." + a.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f2209b = com.motorola.audiomonitor.a.f160b;

    /* renamed from: e reason: collision with root package name */
    private static final short[] f2210e = new short[16000];

    /* renamed from: c reason: collision with root package name */
    private final Object f2211c = new Object();

    /* renamed from: d reason: collision with root package name */
    private final AudioChunk f2212d;

    /* renamed from: f reason: collision with root package name */
    private boolean f2213f = false;

    static {
        for (int i = 0; i < 16000; i++) {
            f2210e[i] = 0;
        }
    }

    public a(AudioType audioType, Handler handler) {
        super(audioType, handler);
        this.f2212d = new AudioChunk(audioType, f2210e);
    }

    public void a() {
        handleNewAudio(this.f2212d);
    }

    public boolean a(ProtectedByteBuffer protectedByteBuffer) {
        handleNewAudio(new AudioChunk(getAudioType(), protectedByteBuffer.getShortArray()));
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean isCodecSupported(AudioType audioType) {
        return audioType == AudioType.PCM_16k || audioType == AudioType.PCM_8k;
    }

    public void startRecording() {
        synchronized (this.f2211c) {
            if (!this.f2213f) {
                super.startRecording();
                try {
                    this.f2211c.wait();
                } catch (InterruptedException e2) {
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean startRecordingInternal(AudioType audioType) {
        if (f2209b) {
            Log.d(f2208a, "startRecordingInternal");
        }
        synchronized (this.f2211c) {
            this.f2213f = true;
            this.f2211c.notifyAll();
        }
        return true;
    }

    public void stopRecording() {
        synchronized (this.f2211c) {
            if (this.f2213f) {
                super.stopRecording();
                try {
                    this.f2211c.wait();
                } catch (InterruptedException e2) {
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void stopRecordingInternal() {
        if (f2209b) {
            Log.d(f2208a, "stopRecordingInternal");
        }
        synchronized (this.f2211c) {
            this.f2213f = false;
            this.f2211c.notify();
        }
    }
}
