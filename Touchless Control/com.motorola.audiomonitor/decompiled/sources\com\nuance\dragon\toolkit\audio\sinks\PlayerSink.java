package com.nuance.dragon.toolkit.audio.sinks;

import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AbstractAudioChunk;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.pipes.DuplicatorPipe;
import com.nuance.dragon.toolkit.audio.sources.SingleSinkSource;
import com.nuance.dragon.toolkit.util.WorkerThread;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public abstract class PlayerSink extends AudioSink {
    protected Handler _workerThreadHandler;
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final AudioType f1211a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public WorkerThread f1212b;

    /* renamed from: c reason: collision with root package name */
    private final Handler f1213c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public Listener f1214d;

    /* renamed from: e reason: collision with root package name */
    private boolean f1215e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public boolean f1216f;

    /* renamed from: g reason: collision with root package name */
    private boolean f1217g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public int f1218h;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public a j;
    /* access modifiers changed from: private */
    public DuplicatorPipe k;

    public interface Listener {
        void onStarted(PlayerSink playerSink);

        void onStopped(PlayerSink playerSink);
    }

    final class a extends SingleSinkSource {

        /* renamed from: b reason: collision with root package name */
        private final LinkedList f1236b = new LinkedList();

        a() {
        }

        /* access modifiers changed from: 0000 */
        public final void a() {
            notifySourceClosed();
        }

        /* access modifiers changed from: 0000 */
        public final void a(List list) {
            this.f1236b.addAll(list);
            notifyChunksAvailable();
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ AbstractAudioChunk getAudioChunk() {
            if (!this.f1236b.isEmpty()) {
                return (AudioChunk) this.f1236b.remove();
            }
            return null;
        }

        public final AudioType getAudioType() {
            return PlayerSink.this.f1211a;
        }

        public final int getChunksAvailable() {
            return this.f1236b.size();
        }

        public final boolean isActive() {
            return this == PlayerSink.this.j;
        }
    }

    public PlayerSink(AudioType audioType, Handler handler) {
        d.a("audioType", (Object) audioType);
        d.a("audioType", "a type supported by this player", isCodecSupported(audioType));
        this.f1211a = audioType;
        if (handler == null) {
            this.f1215e = true;
            this.f1212b = null;
        } else {
            this._workerThreadHandler = handler;
            this.f1212b = null;
        }
        this.f1213c = new Handler();
        this.j = new a();
        this.k = new DuplicatorPipe();
        this.k.connectAudioSource(this.j);
        this.j = null;
    }

    /* access modifiers changed from: private */
    public int a(AudioSource audioSource, int i2, List list) {
        int i3 = 0;
        do {
            AudioChunk audioChunk = (AudioChunk) audioSource.getAudioChunkForSink(this);
            if (audioChunk == null) {
                break;
            }
            i3 += audioChunk.audioDuration;
            list.add(audioChunk);
        } while (i3 < i2);
        return i3;
    }

    static /* synthetic */ int b(PlayerSink playerSink, int i2) {
        int i3 = playerSink.i - i2;
        playerSink.i = i3;
        return i3;
    }

    /* access modifiers changed from: protected */
    public void audioSourceDisconnected(AudioSource audioSource) {
        if (this.f1216f) {
            this._workerThreadHandler.post(new Runnable() {
                public final void run() {
                    PlayerSink.this.noNewChunksInternal();
                }
            });
            super.audioSourceDisconnected(audioSource);
        }
    }

    public void chunksAvailable(AudioSource audioSource) {
        if (this.f1216f && this.f1218h > 0) {
            final ArrayList arrayList = new ArrayList();
            this.f1218h -= a(audioSource, this.f1218h, arrayList);
            this._workerThreadHandler.post(new Runnable() {
                public final void run() {
                    PlayerSink.this.newChunksInternal(arrayList);
                }
            });
        }
    }

    public void framesDropped(AudioSource audioSource) {
    }

    /* access modifiers changed from: protected */
    public AudioType getAudioType() {
        return this.f1211a;
    }

    public AudioSource getReferenceSource() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    public void handleAudioNeeded(final int i2) {
        if (this.i > 0) {
            i2 -= this.i;
        }
        if (i2 > 0) {
            this.i += i2;
            this.f1213c.post(new Runnable() {
                public final void run() {
                    final ArrayList arrayList;
                    int i;
                    final boolean z = false;
                    if (PlayerSink.this.f1216f) {
                        AudioSource e2 = PlayerSink.this.getConnectedSource();
                        if (e2 != null) {
                            arrayList = new ArrayList();
                            i = PlayerSink.this.a(e2, i2, arrayList);
                        } else {
                            arrayList = null;
                            i = 0;
                        }
                        PlayerSink.this.f1218h = i2 - i;
                        if (PlayerSink.this.f1218h > 0 && (e2 == null || !e2.isActive())) {
                            PlayerSink.this.f1218h = 0;
                            z = true;
                        }
                        PlayerSink.this._workerThreadHandler.post(new Runnable() {
                            public final void run() {
                                PlayerSink.b(PlayerSink.this, i2);
                                PlayerSink.this.i;
                                if (arrayList != null && !arrayList.isEmpty()) {
                                    PlayerSink.this.newChunksInternal(arrayList);
                                }
                                if (z) {
                                    PlayerSink.this.noNewChunksInternal();
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void handleChunksPlayed(final List list) {
        this.f1213c.post(new Runnable() {
            public final void run() {
                PlayerSink.this.j.a(list);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void handleStarted() {
        this.f1213c.post(new Runnable() {
            public final void run() {
                if (PlayerSink.this.f1214d != null) {
                    PlayerSink.this.f1214d.onStarted(PlayerSink.this);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void handleStopped() {
        this.f1213c.post(new Runnable() {
            public final void run() {
                PlayerSink.this.disconnectAudioSource();
                if (PlayerSink.this.f1216f) {
                    PlayerSink.this.f1216f = false;
                    if (PlayerSink.this.f1212b != null) {
                        PlayerSink.this.f1212b.stop();
                        PlayerSink.this.f1212b = null;
                    }
                }
                if (PlayerSink.this.f1214d != null) {
                    PlayerSink.this.f1214d.onStopped(PlayerSink.this);
                    PlayerSink.this.f1214d = null;
                }
                if (PlayerSink.this.j != null) {
                    a b2 = PlayerSink.this.j;
                    PlayerSink.this.j = null;
                    PlayerSink.this.k = null;
                    b2.a();
                }
            }
        });
    }

    public final boolean isAudioSourceTypeSupported(AudioType audioType) {
        return this.f1211a.encoding == audioType.encoding && this.f1211a.frequency == audioType.frequency;
    }

    /* access modifiers changed from: protected */
    public abstract boolean isCodecSupported(AudioType audioType);

    /* access modifiers changed from: protected */
    public abstract void newChunksInternal(List list);

    /* access modifiers changed from: protected */
    public abstract void noNewChunksInternal();

    public void sourceClosed(AudioSource audioSource) {
        if (this.f1216f) {
            final ArrayList arrayList = new ArrayList();
            while (audioSource.getChunksAvailableForSink(this) > 0) {
                arrayList.add(audioSource.getAudioChunkForSink(this));
            }
            this._workerThreadHandler.post(new Runnable() {
                public final void run() {
                    if (!arrayList.isEmpty()) {
                        PlayerSink.this.newChunksInternal(arrayList);
                    }
                    PlayerSink.this.noNewChunksInternal();
                }
            });
        }
    }

    public void startPlaying() {
        startPlaying(null);
    }

    public void startPlaying(Listener listener) {
        d.a((Object) this, !this.f1216f, "Already started.");
        if (this.f1215e) {
            this.f1212b = new WorkerThread();
            this.f1212b.start();
            this._workerThreadHandler = this.f1212b.getHandler();
        }
        this.f1216f = true;
        this.f1214d = listener;
        this.f1218h = 0;
        this.j = new a();
        this.k = new DuplicatorPipe();
        this.k.connectAudioSource(this.j);
        this._workerThreadHandler.post(new Runnable() {
            public final void run() {
                PlayerSink.this.i = 0;
                if (!PlayerSink.this.startPlayingInternal(PlayerSink.this.f1211a)) {
                    PlayerSink.this.handleStopped();
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public abstract boolean startPlayingInternal(AudioType audioType);

    public void stopPlaying() {
        if (this.f1216f && !this.f1217g) {
            this.f1217g = true;
            this._workerThreadHandler.post(new Runnable() {
                public final void run() {
                    PlayerSink.this.stopPlayingInternal();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public abstract void stopPlayingInternal();
}
