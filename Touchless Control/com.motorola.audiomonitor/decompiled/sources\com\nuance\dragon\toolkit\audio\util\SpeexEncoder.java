package com.nuance.dragon.toolkit.audio.util;

import com.nuance.dragon.toolkit.audio.AbstractAudioChunk.SpeechStatus;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.util.Logger;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class SpeexEncoder {

    /* renamed from: a reason: collision with root package name */
    private AudioType f1312a;

    /* renamed from: b reason: collision with root package name */
    private AudioType f1313b;

    /* renamed from: c reason: collision with root package name */
    private int f1314c;

    /* renamed from: d reason: collision with root package name */
    private SpeechStatus f1315d = SpeechStatus.NO_SPEECH;

    /* renamed from: e reason: collision with root package name */
    private long f1316e;

    static {
        System.loadLibrary("dmt_speex");
    }

    private static native int encodeNative(long j, short[] sArr, int i, int i2, OutputStream outputStream, int[] iArr, int[] iArr2);

    private static native long initializeNative(int[] iArr, int[] iArr2, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12, int i13);

    private static native void releaseNative(long j);

    public final void a() {
        if (this.f1316e != 0) {
            releaseNative(this.f1316e);
            this.f1316e = 0;
        }
    }

    public final void a(AudioType audioType, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        int i10;
        int i11;
        int i12;
        Encoding encoding = audioType.encoding;
        Encoding encoding2 = Encoding.SPEEX;
        this.f1312a = audioType;
        this.f1313b = new AudioType(Encoding.PCM_16, this.f1312a.frequency);
        if (audioType.frequency == 8000) {
            i10 = 0;
            i11 = 8000;
            i12 = 6;
        } else {
            i10 = 1;
            i11 = 16000;
            i12 = 8;
        }
        int[] iArr = new int[1];
        int[] iArr2 = new int[1];
        if (this.f1316e != 0) {
            releaseNative(this.f1316e);
        }
        this.f1316e = initializeNative(iArr2, iArr, i10, i11, 3, i12, i, i2, i3, i4, i5, i6, i7, i8, i9);
        this.f1314c = iArr2[0];
    }

    public final AudioChunk[] a(short[] sArr, int i, int i2) {
        if (i2 % this.f1314c != 0) {
            Logger.error(this, "the buffer length (" + i2 + ") is NOT divisible by Speex encoder frame size (" + this.f1314c + ")!!!");
            return new AudioChunk[0];
        } else if (this.f1316e == 0) {
            Logger.error(this, "Speex encoder isn't initialized");
            return new AudioChunk[0];
        } else {
            short[] sArr2 = new short[sArr.length];
            System.arraycopy(sArr, 0, sArr2, 0, i2);
            ArrayList arrayList = new ArrayList();
            int[] iArr = new int[1];
            int[] iArr2 = new int[1];
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            int i3 = i2;
            int i4 = i;
            do {
                byteArrayOutputStream.reset();
                if (encodeNative(this.f1316e, sArr2, i4, i3, byteArrayOutputStream, iArr, iArr2) >= 0) {
                    i3 -= iArr[0];
                    i4 += iArr[0];
                    arrayList.add(new AudioChunk(this.f1312a, byteArrayOutputStream.toByteArray(), this.f1313b.getDuration(iArr[0]), this.f1315d));
                    int i5 = iArr2[0];
                    if (i5 == 1) {
                        SpeechStatus speechStatus = this.f1315d;
                        SpeechStatus speechStatus2 = SpeechStatus.NO_SPEECH;
                        this.f1315d = SpeechStatus.SPEECH;
                        continue;
                    } else if (i5 == 2) {
                        SpeechStatus speechStatus3 = this.f1315d;
                        SpeechStatus speechStatus4 = SpeechStatus.SPEECH;
                        this.f1315d = SpeechStatus.NO_SPEECH;
                        continue;
                    } else {
                        continue;
                    }
                } else {
                    i3 = 0;
                    continue;
                }
            } while (i3 > 0);
            return (AudioChunk[]) arrayList.toArray(new AudioChunk[arrayList.size()]);
        }
    }
}
