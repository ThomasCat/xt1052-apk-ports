package com.motorola.audiomonitor.uis;

import android.content.ActivityNotFoundException;
import android.util.Log;
import h.k;

class at implements Runnable {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ boolean f676a;

    /* renamed from: b reason: collision with root package name */
    final /* synthetic */ as f677b;

    at(as asVar, boolean z) {
        this.f677b = asVar;
        this.f676a = z;
    }

    public void run() {
        if (this.f677b.f671c == null) {
            this.f677b.f675g = false;
            if (this.f676a) {
                k.a(this.f677b.f669a);
            }
            if (this.f677b.j != null) {
                this.f677b.j.a();
                this.f677b.j = null;
            }
            if (this.f677b.k != null) {
                this.f677b.k.b();
                this.f677b.k = null;
            }
            if (this.f677b.f674f != null) {
                try {
                    this.f677b.f669a.startActivity(this.f677b.f674f);
                } catch (ActivityNotFoundException e2) {
                    Log.e("AMonitor", e2.toString());
                    this.f677b.f673e.e();
                }
            }
        }
    }
}
