package h;

import android.os.Build;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONObject;

class i extends Thread {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ g f2302a;

    i(g gVar) {
        this.f2302a = gVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:108:0x020b  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0215 A[LOOP:6: B:111:0x0215->B:114:0x021b, LOOP_START, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x01bc A[SYNTHETIC] */
    public void run() {
        int size;
        Throwable th;
        HttpURLConnection httpURLConnection;
        if (g.f2294b) {
            Log.d(g.f2293a, "Uploader started.");
        }
        while (true) {
            synchronized (this.f2302a.f2300h) {
                try {
                    this.f2302a.f2300h.wait(900000);
                } catch (InterruptedException e2) {
                }
            }
            synchronized (this.f2302a.f2296d) {
                size = this.f2302a.f2296d.size();
            }
            if (size == 0) {
                if (g.f2294b) {
                    Log.d(g.f2293a, "No data to upload.");
                }
            } else if (!this.f2302a.v()) {
                if (g.f2294b) {
                    Log.d(g.f2293a, "Upload is turned off.");
                }
                this.f2302a.f2296d.clear();
            } else {
                if (g.f2294b) {
                    Log.d(g.f2293a, "Uploading " + size + " records.");
                }
                try {
                    URL url = new URL("https://weylandut.appspot.com/myprojectstats?serial=" + Build.SERIAL);
                    HttpURLConnection httpURLConnection2 = null;
                    LinkedList linkedList = new LinkedList();
                    try {
                        httpURLConnection = (HttpURLConnection) url.openConnection();
                        try {
                            httpURLConnection.setDoOutput(true);
                            httpURLConnection.setChunkedStreamingMode(0);
                            httpURLConnection.setRequestProperty("Content-Type", "application/json");
                            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
                            synchronized (this.f2302a.f2296d) {
                                linkedList.addAll(this.f2302a.f2296d);
                                this.f2302a.f2296d.clear();
                            }
                            this.f2302a.a((OutputStream) bufferedOutputStream, (List) linkedList);
                            if (g.f2294b) {
                                Log.d(g.f2293a, "Receiving Response");
                            }
                            this.f2302a.a((InputStream) new BufferedInputStream(httpURLConnection.getInputStream()));
                            int responseCode = httpURLConnection.getResponseCode();
                            if (g.f2294b) {
                                Log.d(g.f2293a, "Response Code=" + responseCode);
                            }
                            if (responseCode == 200) {
                                linkedList.clear();
                            }
                            if (httpURLConnection != null) {
                                httpURLConnection.disconnect();
                            }
                            synchronized (this.f2302a.f2296d) {
                                while (!linkedList.isEmpty()) {
                                    this.f2302a.f2296d.addFirst((JSONObject) linkedList.removeLast());
                                }
                            }
                        } catch (IOException e3) {
                            IOException iOException = e3;
                            httpURLConnection2 = httpURLConnection;
                            e = iOException;
                        } catch (Throwable th2) {
                            th = th2;
                            if (httpURLConnection != null) {
                            }
                            synchronized (this.f2302a.f2296d) {
                            }
                            throw th;
                        }
                    } catch (IOException e4) {
                        e = e4;
                        try {
                            Log.e(g.f2293a, "Error during POST" + e.toString());
                            if (httpURLConnection2 != null) {
                                httpURLConnection2.disconnect();
                            }
                            synchronized (this.f2302a.f2296d) {
                                while (!linkedList.isEmpty()) {
                                    this.f2302a.f2296d.addFirst((JSONObject) linkedList.removeLast());
                                }
                            }
                            synchronized (this.f2302a.f2296d) {
                            }
                        } catch (Throwable th3) {
                            Throwable th4 = th3;
                            httpURLConnection = httpURLConnection2;
                            th = th4;
                            if (httpURLConnection != null) {
                                httpURLConnection.disconnect();
                            }
                            synchronized (this.f2302a.f2296d) {
                                while (!linkedList.isEmpty()) {
                                    this.f2302a.f2296d.addFirst((JSONObject) linkedList.removeLast());
                                }
                            }
                            throw th;
                        }
                    } catch (Throwable th5) {
                        Throwable th6 = th5;
                        httpURLConnection = null;
                        th = th6;
                        if (httpURLConnection != null) {
                        }
                        synchronized (this.f2302a.f2296d) {
                        }
                        throw th;
                    }
                    synchronized (this.f2302a.f2296d) {
                        if (this.f2302a.f2296d.size() > 500) {
                            Log.e(g.f2293a, "Discarding " + this.f2302a.f2296d.size() + " records...");
                            this.f2302a.f2296d.clear();
                        }
                    }
                } catch (MalformedURLException e5) {
                    Log.e(g.f2293a, "Malformed URL. Do not upload.");
                }
            }
        }
        while (true) {
        }
    }
}
