package com.nuance.a.a.a.b.c.b.b;

import com.nuance.dragon.toolkit.data.Data;

public final class k extends f {

    /* renamed from: a reason: collision with root package name */
    private int f868a;

    public k(int i) {
        super(Data.TYPE_INT);
        this.f868a = i;
    }

    public k(byte[] bArr) {
        super(Data.TYPE_INT);
        if (bArr.length == 1) {
            this.f868a = bArr[0] & 255;
        } else if (bArr.length == 2) {
            this.f868a = ((bArr[1] & 255) << 8) + (bArr[0] & 255);
        } else if (bArr.length == 3) {
            this.f868a = ((bArr[2] & 255) << 16) + ((bArr[1] & 255) << 8) + (bArr[0] & 255);
        } else {
            this.f868a = ((bArr[3] & 255) << 24) + ((bArr[2] & 255) << 16) + ((bArr[1] & 255) << 8) + (bArr[0] & 255);
        }
    }

    public final int a() {
        return this.f868a;
    }

    public final byte[] b() {
        byte[] bArr = Math.abs(this.f868a) < 128 ? new byte[]{(byte) (this.f868a & 255)} : Math.abs(this.f868a) < 32768 ? new byte[]{(byte) (this.f868a & 255), (byte) ((this.f868a >> 8) & 255)} : new byte[]{(byte) (this.f868a & 255), (byte) ((this.f868a >> 8) & 255), (byte) ((this.f868a >> 16) & 255), (byte) ((this.f868a >> 24) & 255)};
        return super.a(bArr);
    }
}
