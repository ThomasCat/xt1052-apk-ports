package com.nuance.dragon.toolkit.recognition.dictation.a;

import com.nuance.dragon.toolkit.util.Logger;
import java.io.UnsupportedEncodingException;

public final class i {
    public static int a(byte[] bArr, int i) {
        return (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8);
    }

    public static String a(byte[] bArr, int i, int i2, String str) {
        try {
            return new String(bArr, i, i2, str);
        } catch (UnsupportedEncodingException e2) {
            Logger.warn("dictation.Util", str + " character encoding is not available in your VM. Using the default one.");
            return new String(bArr, i, i2);
        }
    }

    public static int b(byte[] bArr, int i) {
        return (a(bArr, i) & 65535) | ((a(bArr, i + 2) & 65535) << 16);
    }

    public static long c(byte[] bArr, int i) {
        return ((long) (((bArr[i + 3] & 255) << 24) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 1] & 255) << 8) | (bArr[i] & 255))) & 4294967295L;
    }

    public static int d(byte[] bArr, int i) {
        int i2 = i;
        while (bArr[i2] != 0) {
            i2++;
        }
        int i3 = i2 - i;
        new StringBuilder("String length: [").append(i3).append("]");
        return i3;
    }
}
