package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.a.a.a.b.c.c.j;
import com.nuance.dragon.toolkit.util.Logger;
import java.util.ArrayList;
import java.util.Iterator;

final class i extends f {
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final ArrayList f1469c = new ArrayList();

    /* renamed from: d reason: collision with root package name */
    private final a f1470d = new a() {
        public final void a(Param param, boolean z) {
            i.this.f1469c.remove(param);
            if (z) {
                i.this.a(new TransactionError(i.this.f1465b, 4, null, param.f1355a, null, 0));
            } else if (i.this.f1469c.isEmpty() && i.this.f1465b.isFinished() && !i.this.f1471e) {
                i.this.f1471e = true;
                try {
                    i.this.f1464a.a();
                } catch (Exception e2) {
                    Logger.error(this, "Error ending command " + i.this.f1465b.a(), e2);
                    i.this.a(new TransactionError(i.this.f1465b));
                }
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public boolean f1471e = false;

    i(d dVar, Transaction transaction) {
        super(dVar, true, false, transaction);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        super.a();
        try {
            this.f1464a.a(this.f1465b.a(), this.f1465b.b(), this.f1465b.c());
            for (Param param : this.f1465b.e()) {
                try {
                    if (!param.a(this.f1464a, this.f1470d)) {
                        this.f1469c.add(param);
                    }
                } catch (Exception e2) {
                    Exception exc = e2;
                    Logger.error(this, "Failed to send param [" + param.f1355a + "] command [" + this.f1465b.a() + "]", exc);
                    a(new TransactionError(this.f1465b, 4, exc.getMessage(), param.f1355a, null, 0));
                    return;
                }
            }
            if (this.f1469c.isEmpty() && this.f1465b.isFinished() && !this.f1471e) {
                this.f1471e = true;
                try {
                    this.f1464a.a();
                } catch (Exception e3) {
                    Logger.error(this, "Error ending command " + this.f1465b.a(), e3);
                    a(new TransactionError(this.f1465b));
                }
            }
        } catch (Exception e4) {
            Logger.error(this, "Failed to create command [" + this.f1465b.a() + "]", e4);
            g();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(j jVar) {
        super.a(jVar);
        TransactionResult transactionResult = new TransactionResult(this.f1465b, jVar);
        boolean isFinal = transactionResult.isFinal();
        if (isFinal) {
            a((f) new g(this.f1464a, this.f1465b));
        }
        this.f1465b.a(transactionResult, isFinal);
    }

    public final void a(Param param) {
        try {
            if (!param.a(this.f1464a, this.f1470d)) {
                this.f1469c.add(param);
            }
        } catch (Exception e2) {
            Logger.error(this, "Failed to send param " + param.f1355a, e2);
            a(new TransactionError(this.f1465b, 4, null, param.f1355a, null, 0));
        }
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        Iterator it = this.f1469c.iterator();
        while (it.hasNext()) {
            ((Param) it.next()).a();
        }
        this.f1469c.clear();
        super.b();
    }

    /* access modifiers changed from: 0000 */
    public final void f() {
        if (this.f1469c.isEmpty() && !this.f1471e) {
            this.f1471e = true;
            try {
                this.f1464a.a();
            } catch (Exception e2) {
                Logger.error(this, "Error ending command " + this.f1465b.a(), e2);
                a(new TransactionError(this.f1465b));
            }
        }
    }
}
