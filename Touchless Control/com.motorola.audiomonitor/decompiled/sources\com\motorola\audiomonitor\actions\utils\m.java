package com.motorola.audiomonitor.actions.utils;

import a.e;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.util.Log;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.uis.ag;
import com.motorola.audiomonitor.uis.ah;
import com.motorola.audiomonitor.uis.as;
import com.motorola.audiomonitor.uis.z;
import h.g;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicBoolean;

public class m implements ag {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f242a = ("AMonitor." + m.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f243b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private static m f244c;

    /* renamed from: d reason: collision with root package name */
    private static Object f245d = new Object();
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public final Context f246e;

    /* renamed from: f reason: collision with root package name */
    private final Handler f247f;

    /* renamed from: g reason: collision with root package name */
    private final z f248g;

    /* renamed from: h reason: collision with root package name */
    private final KeyguardManager f249h;
    /* access modifiers changed from: private */
    public final AtomicBoolean i = new AtomicBoolean(false);
    private final BroadcastReceiver j = new n(this);

    private m(Context context, Handler handler) {
        this.f246e = context;
        this.f247f = handler;
        this.f248g = new z(this.f246e);
        this.f249h = (KeyguardManager) this.f246e.getSystemService("keyguard");
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        this.f246e.registerReceiver(this.j, intentFilter);
    }

    public static m a() {
        m mVar;
        synchronized (f245d) {
            if (f244c == null) {
                throw new AssertionError("Not initialized");
            }
            mVar = f244c;
        }
        return mVar;
    }

    private void a(KeyguardManager keyguardManager) {
        try {
            Field declaredField = keyguardManager.getClass().getDeclaredField("mWM");
            if (declaredField != null) {
                declaredField.setAccessible(true);
                Object obj = declaredField.get(keyguardManager);
                Method declaredMethod = declaredField.getType().getDeclaredMethod("dismissKeyguard", null);
                if (declaredMethod != null && obj != null) {
                    declaredMethod.setAccessible(true);
                    declaredMethod.invoke(obj, new Object[0]);
                }
            }
        } catch (NoSuchFieldException e2) {
            Log.d(f242a, "dismissKeyguard:" + e2.toString());
        } catch (IllegalArgumentException e3) {
            Log.d(f242a, "dismissKeyguard:" + e3.toString());
        } catch (IllegalAccessException e4) {
            Log.d(f242a, "dismissKeyguard:" + e4.toString());
        } catch (NoSuchMethodException e5) {
            Log.d(f242a, "dismissKeyguard:" + e5.toString());
        } catch (InvocationTargetException e6) {
            Log.d(f242a, "dismissKeyguard:" + e6.toString());
        }
    }

    public static void a(Context context, Handler handler) {
        synchronized (f245d) {
            f244c = new m(context, handler);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, e eVar) {
        if (this.f248g == null || eVar == null) {
            this.f248g.a(ah.NO_OPTION, (ag) null);
            if (!as.a((ag) this)) {
                return;
            }
        } else {
            this.f248g.a(ah.CANCEL_OPTION, (ag) this);
        }
        this.f248g.a(str, eVar, 0, 0);
    }

    public static boolean a(Context context) {
        return ((KeyguardManager) context.getSystemService("keyguard")).inKeyguardRestrictedInputMode();
    }

    public boolean a(boolean z) {
        return a(z, (e) null);
    }

    public boolean a(boolean z, e eVar) {
        if (!this.f249h.isKeyguardLocked()) {
            return false;
        }
        g.a().j();
        if (!this.f249h.isKeyguardSecure()) {
            a(this.f249h);
            synchronized (this.i) {
                try {
                    this.i.wait(500);
                } catch (InterruptedException e2) {
                }
            }
        } else if (!z || !a.m) {
            this.f247f.post(new o(this, eVar));
            synchronized (this.i) {
                this.i.set(true);
                try {
                    this.i.wait(a.f165g);
                } catch (InterruptedException e3) {
                }
            }
            this.f248g.a();
            if (this.i.get()) {
                g.a().i();
                return true;
            }
            g.a().h();
        } else {
            g.a().q();
            return false;
        }
        try {
            Thread.sleep(150);
            return false;
        } catch (InterruptedException e4) {
            return false;
        }
    }

    public void b() {
        synchronized (this.i) {
            this.i.set(true);
            this.i.notify();
        }
    }

    public void c() {
        this.f248g.b();
        this.f246e.unregisterReceiver(this.j);
        f244c = null;
    }
}
