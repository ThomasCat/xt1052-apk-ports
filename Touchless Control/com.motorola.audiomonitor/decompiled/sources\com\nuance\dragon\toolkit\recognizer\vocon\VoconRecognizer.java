package com.nuance.dragon.toolkit.recognizer.vocon;

import android.os.Handler;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.recognizer.IRecognizer;
import com.nuance.dragon.toolkit.recognizer.IRecognizer.InitializeListener;

public abstract class VoconRecognizer implements IRecognizer {

    public final class Frequencies {
        public static final int FREQ_16KHZ = 16000;
    }

    public final class Languages extends b {
    }

    public static VoconRecognizer createVoconRecognizer(FileManager fileManager) {
        return new c(fileManager);
    }

    public static VoconRecognizer createVoconRecognizer(FileManager fileManager, Handler handler) {
        return new c(fileManager, handler);
    }

    public abstract boolean addVoconContext(String str);

    /* access modifiers changed from: protected */
    public abstract Handler getWorkerThreadHandler();

    public abstract void initialize(VoconConfig voconConfig);

    public abstract void initialize(VoconConfig voconConfig, InitializeListener initializeListener);

    public abstract boolean removeVoconContext();
}
