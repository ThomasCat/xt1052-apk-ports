package com.nuance.dragon.toolkit.audio.bluetooth.impl;

public interface BtServiceListener {
    void onServiceConnected(BtServiceListenerParams btServiceListenerParams);

    void onServiceDisconnected(BtServiceListenerParams btServiceListenerParams);
}
