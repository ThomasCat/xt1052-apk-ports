package com.nuance.dragon.toolkit.recognition.dictation.a;

import com.nuance.dragon.toolkit.recognition.dictation.Alternatives;
import com.nuance.dragon.toolkit.recognition.dictation.DictationResult;
import com.nuance.dragon.toolkit.recognition.dictation.Sentence;
import com.nuance.dragon.toolkit.util.a.a;
import com.nuance.dragon.toolkit.util.a.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONObject;

public final class f extends DictationResult {

    /* renamed from: a reason: collision with root package name */
    private ArrayList f1944a;

    /* renamed from: b reason: collision with root package name */
    private Map f1945b;

    public f(ArrayList arrayList, Map map) {
        new StringBuilder("Received ").append(arrayList.size()).append(" sentences constituting the dictation result.");
        this.f1944a = arrayList;
        this.f1945b = map;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f1944a.size()) {
                ((g) arrayList.get(i2)).c();
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public static f a(JSONObject jSONObject) {
        HashMap hashMap;
        JSONArray jSONArray = jSONObject.getJSONArray("sentences");
        ArrayList arrayList = new ArrayList(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(g.a(jSONArray.getJSONObject(i)));
        }
        JSONObject optJSONObject = jSONObject.optJSONObject("extra");
        if (optJSONObject != null) {
            hashMap = new HashMap();
            JSONArray names = optJSONObject.names();
            for (int i2 = 0; i2 < names.length(); i2++) {
                String string = names.getString(i2);
                hashMap.put(string, optJSONObject.getString(string));
            }
        } else {
            hashMap = null;
        }
        return new f(arrayList, hashMap);
    }

    private void a(Map map) {
        String str = (String) this.f1945b.get("utterance-length");
        String str2 = (String) map.get("utterance-length");
        if (!(str == null || str2 == null)) {
            this.f1945b.put("utterance-length", new Integer(Integer.parseInt(str) + Integer.parseInt(str2)).toString());
        }
        String str3 = (String) this.f1945b.get("processed-audio-frame-count");
        String str4 = (String) map.get("processed-audio-frame-count");
        if (!(str3 == null || str4 == null)) {
            this.f1945b.put("processed-audio-frame-count", new Integer(Integer.parseInt(str3) + Integer.parseInt(str4)).toString());
        }
        String str5 = (String) map.get("InputAudioLength");
        if (str5 != null) {
            this.f1945b.put("InputAudioLength", str5);
        }
    }

    /* access modifiers changed from: 0000 */
    public final g a(int i) {
        if (i < 0 || i >= this.f1944a.size()) {
            return null;
        }
        return (g) this.f1944a.get(i);
    }

    public final void append(DictationResult dictationResult) {
        if (dictationResult == null) {
            throw new IllegalArgumentException("newDR cannot be null!");
        }
        long j = 0;
        for (int i = 0; i < size(); i++) {
            long endTime = a(i).getEndTime();
            if (endTime > j) {
                j = endTime;
            }
        }
        int i2 = 0;
        while (i2 < size()) {
            a(i2).a(i2 < dictationResult.size() ? (g) dictationResult.sentenceAt(i2) : null, j, j, 0);
            i2++;
        }
        int size = size();
        while (true) {
            int i3 = size;
            if (i3 < dictationResult.size()) {
                g gVar = (g) ((g) dictationResult.sentenceAt(i3)).clone();
                gVar.a(j, 0);
                this.f1944a.add(gVar);
                size = i3 + 1;
            } else {
                a(dictationResult.getExtraInformation());
                return;
            }
        }
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        f fVar = (f) obj;
        if (this.f1945b == null) {
            if (fVar.f1945b != null) {
                return false;
            }
        } else if (!this.f1945b.equals(fVar.f1945b)) {
            return false;
        }
        return this.f1944a == null ? fVar.f1944a == null : this.f1944a.equals(fVar.f1944a);
    }

    public final Alternatives getAlternatives(long j, long j2) {
        long j3 = 0;
        new StringBuilder("Getting alternatives of at times [").append(j).append(", ").append(j2).append("]");
        if (j2 < 0 || j2 < j) {
            return null;
        }
        for (int i = 0; i < size(); i++) {
            long endTime = a(i).getEndTime();
            if (endTime > j3) {
                j3 = endTime;
            }
        }
        if (j >= j3) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (int i2 = 0; i2 < this.f1944a.size(); i2++) {
            c cVar = new c(((g) this.f1944a.get(i2)).a(j, j2));
            new StringBuilder("Got alternative [").append(cVar).append("] for sentence at index ").append(i2);
            if (cVar.size() == 0) {
                new StringBuilder("Got no alternative for sentence at index ").append(i2);
            } else {
                String cVar2 = cVar.toString();
                if (!arrayList2.contains(cVar2)) {
                    arrayList.add(cVar);
                    arrayList2.add(cVar2);
                }
            }
        }
        if (arrayList.size() == 0) {
            return null;
        }
        return new d(arrayList);
    }

    public final Map getExtraInformation() {
        return this.f1945b;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f1945b == null ? 0 : this.f1945b.hashCode()) + 31) * 31;
        if (this.f1944a != null) {
            i = this.f1944a.hashCode();
        }
        return hashCode + i;
    }

    public final void merge(DictationResult dictationResult, long j, long j2) {
        if (j < 0) {
            throw new IllegalArgumentException("Timings cannot be negative");
        } else if (j > j2) {
            throw new IllegalArgumentException("The timings are corrupted, the timingBegin is greater than the timingEnd");
        } else {
            long j3 = 0;
            for (int i = 0; i < size(); i++) {
                long endTime = a(i).getEndTime();
                if (endTime > j3) {
                    j3 = endTime;
                }
            }
            if (j2 > j3) {
                throw new IllegalArgumentException("timingEnd is greater than maximum time");
            }
            long j4 = 0;
            for (int i2 = 0; i2 < dictationResult.size(); i2++) {
                long endTime2 = ((g) dictationResult.sentenceAt(i2)).getEndTime();
                if (endTime2 > j4) {
                    j4 = endTime2;
                }
            }
            int i3 = 0;
            while (i3 < size()) {
                a(i3).a(i3 < dictationResult.size() ? ((f) dictationResult).a(i3) : null, j, j2, j4);
                i3++;
            }
            int size = size();
            while (true) {
                int i4 = size;
                if (i4 < dictationResult.size()) {
                    g gVar = (g) ((g) dictationResult.sentenceAt(i4)).clone();
                    gVar.a(j, 0);
                    this.f1944a.add(gVar);
                    size = i4 + 1;
                } else {
                    a(dictationResult.getExtraInformation());
                    return;
                }
            }
        }
    }

    public final Sentence sentenceAt(int i) {
        return a(i);
    }

    public final int size() {
        return this.f1944a.size();
    }

    public final JSONObject toJSON() {
        b bVar = new b();
        a aVar = new a();
        Iterator it = this.f1944a.iterator();
        while (it.hasNext()) {
            aVar.a(((g) it.next()).toJSON());
        }
        bVar.a("sentences", (Object) aVar);
        if (this.f1945b != null) {
            b bVar2 = new b();
            for (Entry entry : this.f1945b.entrySet()) {
                bVar2.a((String) entry.getKey(), entry.getValue());
            }
            bVar.a("extra", (Object) bVar2);
        }
        return bVar;
    }

    public final String toString() {
        return this.f1944a.size() > 0 ? ((g) this.f1944a.get(0)).toString() : "";
    }
}
