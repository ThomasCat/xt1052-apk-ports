package com.motorola.audiomonitor.uis;

import a.e;
import a.g;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.actions.utils.a;
import com.motorola.audiomonitor.actions.utils.aj;
import com.motorola.audiomonitor.actions.utils.an;
import com.motorola.audiomonitor.actions.utils.m;

public class AOVActivity extends Activity implements bc {

    /* renamed from: a reason: collision with root package name */
    public Handler f594a = new Handler(new a(this));

    /* renamed from: b reason: collision with root package name */
    private e f595b = null;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public AOVMicFragment f596c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public AOVSpeakNowStatusFragment f597d;

    /* renamed from: e reason: collision with root package name */
    private boolean f598e;

    /* renamed from: f reason: collision with root package name */
    private int f599f;

    private void b() {
        String a2 = this.f595b.b().a("command");
        if (a2.equals("call")) {
            d(AOVMicFragment.f604b);
            if (this.f595b.c().size() == 1) {
                if (g.a((a) this.f595b.c().get(0))) {
                    d(AOVMicFragment.i);
                }
                b bVar = new b();
                bVar.a(this.f595b);
                getFragmentManager().beginTransaction().replace(C0014R.id.aov_top_prompt_framelayout, bVar, null).commit();
            } else {
                if (d.f690a < 0) {
                    d(AOVMicFragment.f604b);
                } else {
                    d(AOVMicFragment.f610h);
                }
                d dVar = new d();
                dVar.a(this.f595b);
                getFragmentManager().beginTransaction().replace(C0014R.id.aov_top_prompt_framelayout, dVar, null).commit();
            }
            this.f597d.a("");
        } else if (a2.equals("voicemail")) {
            b bVar2 = new b();
            bVar2.a(this.f595b);
            getFragmentManager().beginTransaction().replace(C0014R.id.aov_top_prompt_framelayout, bVar2, null).commit();
            d(AOVMicFragment.f604b);
            this.f597d.a("");
        } else if (a2.equals("navigate")) {
            d(AOVMicFragment.f604b);
            this.f597d.a(getResources().getString(C0014R.string.navigate_please_wait));
        }
    }

    private void d(int i) {
        Message message = new Message();
        message.what = 0;
        message.arg1 = i;
        this.f594a.sendMessage(message);
    }

    public void a() {
        this.f594a.removeCallbacksAndMessages(null);
        finish();
    }

    public void a(int i) {
        if (!this.f598e) {
            this.f596c.b(i);
        }
    }

    public void a(e eVar) {
        if (!this.f598e) {
            this.f595b = eVar;
            b();
        }
    }

    public void b(int i) {
        if (!this.f598e) {
            switch (i) {
                case 1:
                    an a2 = an.a();
                    this.f597d.a(a2.a(a2.a((int) C0014R.array.array_speakup_name), a2.a((int) C0014R.array.array_speakup)));
                    return;
                case 2:
                    this.f597d.a(an.a().b());
                    return;
                case 3:
                    this.f594a.removeMessages(1);
                    this.f596c.a(AOVMicFragment.f607e);
                    return;
                case 4:
                    this.f597d.a("");
                    this.f596c.a(AOVMicFragment.f605c);
                    this.f594a.sendEmptyMessageDelayed(2, 3000);
                    return;
                default:
                    return;
            }
        }
    }

    public void c(int i) {
        if (!this.f598e) {
            this.f596c.c(i);
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        if (m.a((Context) this)) {
            as.a(false, this.f599f);
            this.f598e = true;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        FrameLayout frameLayout = (FrameLayout) findViewById(C0014R.id.aov_top_prompt_framelayout);
        LayoutParams layoutParams = frameLayout.getLayoutParams();
        layoutParams.height = getResources().getDimensionPixelSize(C0014R.dimen.top_prompt_height);
        frameLayout.setLayoutParams(layoutParams);
        if (!this.f598e && this.f595b != null) {
            b();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().addFlags(524288);
        setContentView(C0014R.layout.aov);
        this.f596c = (AOVMicFragment) getFragmentManager().findFragmentById(C0014R.id.aov_mic_fragment);
        this.f596c.setRetainInstance(true);
        this.f597d = (AOVSpeakNowStatusFragment) getFragmentManager().findFragmentById(C0014R.id.aov_top_prompt_fragment);
        this.f597d.setRetainInstance(true);
        this.f594a.sendEmptyMessageDelayed(1, 3500);
        int a2 = as.a((bc) this);
        this.f599f = a2;
        if (a2 < 0) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        as.a((bc) this, this.f599f);
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.f598e = false;
        aj.a(this);
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.f598e = true;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.f594a.removeCallbacksAndMessages(null);
        if (!m.a((Context) this) || isFinishing()) {
            as.a(false, this.f599f);
            this.f598e = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onUserLeaveHint() {
        super.onUserLeaveHint();
        if (m.a((Context) this)) {
            as.a(false, this.f599f);
            this.f598e = true;
        }
    }
}
