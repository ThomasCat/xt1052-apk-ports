package com.nuance.dragon.toolkit.elvis;

import com.nuance.dragon.toolkit.recognition.CloudInterpreter;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import com.nuance.dragon.toolkit.util.internal.f;
import com.nuance.dragon.toolkit.util.internal.g;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ElvisNbestList extends g implements JSONCompliant {

    public class Entry implements JSONCompliant {

        /* renamed from: a reason: collision with root package name */
        private final List f1539a = new ArrayList();

        /* renamed from: b reason: collision with root package name */
        private final String f1540b;

        /* renamed from: c reason: collision with root package name */
        private final int f1541c;

        /* renamed from: d reason: collision with root package name */
        private boolean f1542d;

        public class Word implements JSONCompliant {
            public final String fullPhrase;
            public final boolean isGenericSpeech;
            public final String spokenForm;
            public final String surfaceForm;
            public final int[] wordIds;
            public final String wordSlot;

            Word(String str, String str2, String str3, String str4, int[] iArr, boolean z) {
                this.wordSlot = str;
                this.surfaceForm = str2;
                if (str3 == null || str3.length() == 0) {
                    str3 = null;
                }
                this.spokenForm = str3;
                this.fullPhrase = str4;
                this.wordIds = iArr;
                this.isGenericSpeech = z;
            }

            public static Word createFromJSON(JSONObject jSONObject) {
                int[] iArr;
                String string = jSONObject.getString("slot");
                String string2 = jSONObject.getString("surface");
                String optString = jSONObject.optString("spoken", null);
                String string3 = jSONObject.getString("phrase");
                boolean z = jSONObject.getBoolean("generic");
                JSONArray optJSONArray = jSONObject.optJSONArray("wordids");
                if (optJSONArray != null) {
                    iArr = new int[optJSONArray.length()];
                    for (int i = 0; i < iArr.length; i++) {
                        iArr[i] = optJSONArray.getInt(i);
                    }
                } else {
                    iArr = new int[0];
                }
                return new Word(string, string2, optString, string3, iArr, z);
            }

            public final boolean equals(Object obj) {
                if (!(obj instanceof Word)) {
                    return false;
                }
                Word word = (Word) obj;
                if (!f.a(this.wordSlot, word.wordSlot)) {
                    return false;
                }
                if (!this.isGenericSpeech || !word.isGenericSpeech) {
                    return this.isGenericSpeech == word.isGenericSpeech && f.a(this.surfaceForm, word.surfaceForm) && f.a(this.spokenForm, word.spokenForm) && f.a(this.fullPhrase, word.fullPhrase) && Arrays.equals(this.wordIds, word.wordIds);
                }
                return true;
            }

            public final int hashCode() {
                int i = 0;
                int hashCode = ((((this.surfaceForm == null ? 0 : this.surfaceForm.hashCode()) + (((this.spokenForm == null ? 0 : this.spokenForm.hashCode()) + (((this.isGenericSpeech ? 1231 : 1237) + (((this.fullPhrase == null ? 0 : this.fullPhrase.hashCode()) + 31) * 31)) * 31)) * 31)) * 31) + Arrays.hashCode(this.wordIds)) * 31;
                if (this.wordSlot != null) {
                    i = this.wordSlot.hashCode();
                }
                return hashCode + i;
            }

            public JSONObject toJSON() {
                b bVar = new b();
                bVar.a("slot", (Object) this.wordSlot);
                bVar.a("surface", (Object) this.surfaceForm);
                bVar.a("spoken", (Object) this.spokenForm);
                bVar.a("phrase", (Object) this.fullPhrase);
                bVar.a("generic", (Object) Boolean.valueOf(this.isGenericSpeech));
                if (this.wordIds.length > 0) {
                    JSONArray jSONArray = new JSONArray();
                    for (int put : this.wordIds) {
                        jSONArray.put(put);
                    }
                    bVar.a("wordids", (Object) jSONArray);
                }
                return bVar;
            }

            public final String toString() {
                return this.isGenericSpeech ? "[?]" : this.fullPhrase;
            }
        }

        Entry(String str, int i) {
            this.f1540b = str;
            this.f1541c = i;
        }

        public static Entry createFromJSON(JSONObject jSONObject) {
            Entry entry = new Entry(jSONObject.getString("constraint"), jSONObject.getInt(CloudInterpreter.KEY_SCORE));
            JSONArray jSONArray = jSONObject.getJSONArray("words");
            for (int i = 0; i < jSONArray.length(); i++) {
                entry.a(Word.createFromJSON(jSONArray.getJSONObject(i)));
            }
            return entry;
        }

        /* access modifiers changed from: 0000 */
        public final void a(Word word) {
            this.f1539a.add(word);
            if (!this.f1542d && word.isGenericSpeech) {
                this.f1542d = true;
            }
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Entry entry = (Entry) obj;
            if (this.f1541c != entry.f1541c) {
                return false;
            }
            if (this.f1540b == null) {
                if (entry.f1540b != null) {
                    return false;
                }
            } else if (!this.f1540b.equals(entry.f1540b)) {
                return false;
            }
            if (this.f1542d != entry.f1542d) {
                return false;
            }
            return this.f1539a == null ? entry.f1539a == null : this.f1539a.equals(entry.f1539a);
        }

        public String getConstraint() {
            return this.f1540b;
        }

        public int getScore() {
            return this.f1541c;
        }

        public List getWords() {
            return new g(this.f1539a);
        }

        public boolean hasGenericSpeech() {
            return this.f1542d;
        }

        public final int hashCode() {
            int i = 0;
            int hashCode = ((this.f1542d ? 1231 : 1237) + (((this.f1540b == null ? 0 : this.f1540b.hashCode()) + ((this.f1541c + 31) * 31)) * 31)) * 31;
            if (this.f1539a != null) {
                i = this.f1539a.hashCode();
            }
            return hashCode + i;
        }

        public JSONObject toJSON() {
            b bVar = new b();
            bVar.a("constraint", (Object) this.f1540b);
            bVar.a(CloudInterpreter.KEY_SCORE, (Object) Integer.valueOf(this.f1541c));
            JSONArray jSONArray = new JSONArray();
            for (Word json : this.f1539a) {
                jSONArray.put(json.toJSON());
            }
            bVar.a("words", (Object) jSONArray);
            return bVar;
        }

        public final String toString() {
            StringBuilder sb = new StringBuilder();
            String str = "";
            Iterator it = this.f1539a.iterator();
            while (true) {
                String str2 = str;
                if (!it.hasNext()) {
                    return sb.toString();
                }
                Word word = (Word) it.next();
                sb.append(str2);
                sb.append(word.toString());
                str = " ";
            }
        }
    }

    private ElvisNbestList(int i) {
        super(i);
    }

    ElvisNbestList(List list) {
        super(list);
    }

    ElvisNbestList(Entry... entryArr) {
        super((Object[]) entryArr);
    }

    public static ElvisNbestList createFromJSON(JSONObject jSONObject) {
        JSONArray jSONArray = jSONObject.getJSONArray("entries");
        ElvisNbestList elvisNbestList = new ElvisNbestList(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            elvisNbestList.addInternal(Entry.createFromJSON(jSONArray.getJSONObject(i)));
        }
        return elvisNbestList;
    }

    public JSONObject toJSON() {
        b bVar = new b();
        JSONArray jSONArray = new JSONArray();
        Iterator it = iterator();
        while (it.hasNext()) {
            jSONArray.put(((Entry) it.next()).toJSON());
        }
        bVar.a("entries", (Object) jSONArray);
        return bVar;
    }
}
