package com.motorola.audiomonitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import com.motorola.audiomonitor.actions.utils.h;
import com.motorola.audiomonitor.actions.utils.m;
import com.motorola.audiomonitor.service.a;
import com.motorola.audiomonitor.service.d;
import h.k;
import java.util.concurrent.atomic.AtomicBoolean;

public class bu extends HandlerThread implements aa, ab {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f374a = ("AMonitor." + bu.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f375b = a.f160b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final Context f376c;

    /* renamed from: d reason: collision with root package name */
    private final d f377d;

    /* renamed from: e reason: collision with root package name */
    private s f378e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public final Handler f379f;

    /* renamed from: g reason: collision with root package name */
    private final Handler f380g = new Handler();
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public final AtomicBoolean f381h = new AtomicBoolean(false);
    private BroadcastReceiver i = new bv(this);

    public bu(Context context, d dVar) {
        super(ba.class.getSimpleName());
        if (f375b) {
            Log.d(f374a, "HW Control");
        }
        this.f376c = context;
        h.a(this.f376c);
        this.f377d = dVar;
        IntentFilter intentFilter = new IntentFilter("android.intent.action.SCREEN_ON");
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        this.f376c.registerReceiver(this.i, intentFilter);
        this.f381h.set(k.b(this.f376c));
        start();
        this.f379f = new Handler(getLooper());
    }

    /* access modifiers changed from: private */
    public void e() {
        this.f378e = new a(this.f376c, this.f377d, this);
        m.a(this.f376c, this.f380g);
        this.f378e.e_();
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.f378e != null) {
            this.f378e.b();
            this.f378e.d();
            this.f378e = null;
        }
        try {
            m.a().c();
        } catch (AssertionError e2) {
        }
    }

    public void a(float f2, s sVar) {
        if (f375b) {
            Log.d(f374a, "onPhraseSpotted: score=" + f2);
        }
        this.f379f.post(new by(this, sVar, f2));
    }

    public void a(s sVar) {
    }

    public void b() {
        this.f379f.post(new bw(this));
    }

    public void c() {
        this.f379f.post(new bx(this));
        while (true) {
            try {
                join(3000);
                break;
            } catch (InterruptedException e2) {
            }
        }
        if (this.i != null) {
            this.f376c.unregisterReceiver(this.i);
            this.i = null;
        }
        h.a().i();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        if (f375b) {
            Log.d(f374a, "finalize");
        }
    }
}
