package com.motorola.audiomonitor.uis;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.util.Log;
import android.view.View;

class aj extends al {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ PrivacyActivity f652a;

    aj(PrivacyActivity privacyActivity) {
        this.f652a = privacyActivity;
        super(privacyActivity, null);
    }

    public void onClick(View view) {
        try {
            this.f652a.startActivity(new Intent("com.motorola.intent.action.MOTOROLA_FULL_PRIVACY_POLICY"));
        } catch (ActivityNotFoundException e2) {
            Log.e(PrivacyActivity.f626a, "Error starting privacy policy activity");
        }
    }
}
