package com.nuance.dragon.toolkit.recognition;

public class InterpretException extends Exception {
    public InterpretException(String str) {
        super(str);
    }
}
