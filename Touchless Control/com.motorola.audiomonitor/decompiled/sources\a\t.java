package a;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.util.Log;
import b.b;
import b.f;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.ProtectedByteBuffer;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.actions.utils.ac;
import com.motorola.audiomonitor.actions.utils.ad;
import com.motorola.audiomonitor.actions.utils.ai;
import com.motorola.audiomonitor.actions.utils.h;
import com.motorola.audiomonitor.actions.utils.m;
import com.motorola.audiomonitor.ay;
import com.motorola.audiomonitor.uis.am;
import com.motorola.audiomonitor.z;
import java.util.Arrays;
import java.util.List;

public class t extends r implements ai, z {

    /* renamed from: a reason: collision with root package name */
    private static final String f62a = ("AMonitor." + t.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f63b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private int f64c = -1;

    /* renamed from: d reason: collision with root package name */
    private ay f65d = null;

    /* renamed from: e reason: collision with root package name */
    private am f66e = null;

    /* renamed from: f reason: collision with root package name */
    private String f67f = null;

    /* renamed from: g reason: collision with root package name */
    private final Object f68g = new Object();

    /* renamed from: h reason: collision with root package name */
    private boolean f69h = false;

    t(int i, Context context) {
        super(i, context);
    }

    private o a(String str) {
        o oVar = o.CONSUMED;
        if (f63b) {
            Log.d(f62a, "navigate address {" + str + "}");
        }
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("google.navigation:q=" + Uri.encode(str)));
        intent.addFlags(268435456);
        try {
            c().startActivity(intent);
            return oVar;
        } catch (ActivityNotFoundException e2) {
            return o.ABORTED;
        }
    }

    private String b(String str) {
        String[] stringArray = c().getResources().getStringArray(C0014R.array.commands_navigate_and_extra);
        if (str == null) {
            return null;
        }
        for (String str2 : stringArray) {
            if (str2 != null) {
                int indexOf = str.indexOf(str2);
                if (f63b) {
                    Log.d(f62a, "navigate address index {" + indexOf + "}");
                }
                if (indexOf >= 0 && str.length() - indexOf > str2.length()) {
                    return str.substring(str2.length() + indexOf);
                }
            }
        }
        return null;
    }

    public o a(b bVar, e eVar) {
        o oVar;
        this.f64c = -1;
        o oVar2 = o.NOT_CONSUMED;
        this.f69h = false;
        this.f65d = new ay(this, eVar.d());
        for (ProtectedByteBuffer a2 : eVar.a()) {
            this.f65d.a(a2);
        }
        this.f65d.a();
        ad adVar = ad.RESPONSE_POSITIVE;
        if (this.f64c == -1) {
            this.f66e = new am(c(), eVar, 20000, true, true);
            Resources resources = c().getResources();
            adVar = eVar.a(ac.EOS_DISMISS_BY_CONFIRM_CANCEL, 20000, this.f66e, Arrays.asList(new String[]{resources.getString(C0014R.string.navigate_please_wait)}), this);
        }
        if (this.f64c == 0) {
            if (m.a().a(false, eVar)) {
                return o.ABORTED;
            }
            oVar = a(this.f67f);
        } else if (adVar == ad.RESPONSE_NEGATIVE || adVar == ad.RESPONSE_ABORT) {
            oVar = o.ABORTED;
        } else if (!h.b(c()) || this.f64c == 3) {
            oVar = oVar2;
        } else {
            a.a(c());
            oVar = o.ABORTED;
        }
        a();
        return oVar;
    }

    public void a() {
        if (this.f65d != null) {
            this.f65d.b();
            this.f65d = null;
        }
        if (this.f66e != null) {
            this.f66e.b();
            this.f66e = null;
        }
        this.f67f = null;
    }

    public void a(float f2) {
    }

    public void a(int i) {
        if (f63b) {
            Log.d(f62a, "STT error happened");
        }
        synchronized (this.f68g) {
            if (!this.f69h) {
                try {
                    this.f68g.wait(20000);
                } catch (InterruptedException e2) {
                    Log.e(f62a, e2.toString());
                }
            }
            this.f64c = i;
            if (this.f66e != null) {
                this.f66e.a(true);
            }
        }
    }

    public void a(int i, int i2) {
        synchronized (this.f68g) {
            this.f69h = true;
            this.f68g.notify();
        }
    }

    public void a(com.motorola.audiomonitor.ac acVar) {
        List a2 = acVar.a();
        if (f63b) {
            Log.d(f62a, "result=" + a2);
        }
        this.f67f = b(((f) a2.get(0)).a());
        if (f63b) {
            Log.d(f62a, "navigate address {" + this.f67f + "}");
        }
        if (this.f67f != null) {
            this.f64c = 0;
        } else {
            this.f64c = 3;
        }
        synchronized (this.f68g) {
            if (!this.f69h) {
                try {
                    this.f68g.wait(20000);
                } catch (InterruptedException e2) {
                    Log.e(f62a, e2.toString());
                }
            }
            if (this.f66e != null) {
                this.f66e.a(true);
            }
        }
    }

    public void d_() {
    }
}
