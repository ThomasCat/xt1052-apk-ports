package com.nuance.dragon.toolkit.audio.sinks;

import com.motorola.audiomonitor.GenericRecognizer.UdtEnrollResult;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.util.Logger;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class FilePlayerSink extends PlayerSink {

    /* renamed from: a reason: collision with root package name */
    private String f1206a;

    /* renamed from: b reason: collision with root package name */
    private boolean f1207b;

    /* renamed from: c reason: collision with root package name */
    private AudioType f1208c;

    /* renamed from: d reason: collision with root package name */
    private FileOutputStream f1209d;

    /* renamed from: e reason: collision with root package name */
    private boolean f1210e;

    public FilePlayerSink(AudioType audioType, String str) {
        this(audioType, str, true);
    }

    public FilePlayerSink(AudioType audioType, String str, boolean z) {
        super(audioType, null);
        this.f1209d = null;
        this.f1210e = false;
        this.f1206a = str;
        this.f1207b = z;
    }

    private void a() {
        if (!this.f1210e) {
            try {
                this.f1209d.close();
            } catch (IOException e2) {
            }
            this.f1210e = true;
            handleStopped();
        }
    }

    /* access modifiers changed from: protected */
    public boolean isCodecSupported(AudioType audioType) {
        return true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.audio.AudioChunk>, for r8v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.audio.AudioChunk>] */
    public void newChunksInternal(List<AudioChunk> list) {
        byte[] bArr;
        if (!this.f1210e) {
            handleAudioNeeded(1000);
            for (AudioChunk audioChunk : list) {
                if (this.f1208c.encoding == Encoding.PCM_16) {
                    short[] sArr = audioChunk.audioShorts;
                    byte[] bArr2 = new byte[(sArr.length * 2)];
                    for (int i = 0; i < sArr.length; i++) {
                        short s = sArr[i];
                        if (this.f1207b) {
                            bArr2[i * 2] = (byte) s;
                            bArr2[(i * 2) + 1] = (byte) (s >> 8);
                        } else {
                            bArr2[(i * 2) + 1] = (byte) s;
                            bArr2[i * 2] = (byte) (s >> 8);
                        }
                    }
                    bArr = bArr2;
                } else {
                    bArr = audioChunk.audioBytes;
                }
                try {
                    if (this.f1208c.encoding == Encoding.SPEEX) {
                        if (bArr.length < 128) {
                            this.f1209d.write(bArr.length & 127);
                        } else if (bArr.length < 16384) {
                            this.f1209d.write(((bArr.length >> 7) & 127) | UdtEnrollResult.CHECKRECORDING_CLIPPING_MSEC);
                            this.f1209d.write(bArr.length & 127);
                        } else if (bArr.length < 2097152) {
                            this.f1209d.write(((bArr.length >> 14) & 127) | UdtEnrollResult.CHECKRECORDING_CLIPPING_MSEC);
                            this.f1209d.write(((bArr.length >> 7) & 127) | UdtEnrollResult.CHECKRECORDING_CLIPPING_MSEC);
                            this.f1209d.write(bArr.length & 127);
                        } else if (bArr.length < 268435456) {
                            this.f1209d.write(((bArr.length >> 21) & 127) | UdtEnrollResult.CHECKRECORDING_CLIPPING_MSEC);
                            this.f1209d.write(((bArr.length >> 14) & 127) | UdtEnrollResult.CHECKRECORDING_CLIPPING_MSEC);
                            this.f1209d.write(((bArr.length >> 7) & 127) | UdtEnrollResult.CHECKRECORDING_CLIPPING_MSEC);
                            this.f1209d.write(bArr.length & 127);
                        } else {
                            Logger.error(this, "chunksAvailableInternal() buffer size is too big!!!");
                            a();
                            return;
                        }
                    }
                    this.f1209d.write(bArr);
                } catch (IOException e2) {
                    Logger.error(this, "chunksAvailableInternal() exception thrown!!!");
                    a();
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void noNewChunksInternal() {
        a();
    }

    /* access modifiers changed from: protected */
    public boolean startPlayingInternal(AudioType audioType) {
        this.f1208c = audioType;
        try {
            this.f1209d = new FileOutputStream(new File(this.f1206a));
            handleStarted();
            handleAudioNeeded(1000);
            return true;
        } catch (FileNotFoundException e2) {
            this.f1210e = true;
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void stopPlayingInternal() {
        a();
    }
}
