package com.nuance.dragon.toolkit.elvis;

import com.nuance.dragon.toolkit.grammar.WordList;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.a.b;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Grammar implements JSONCompliant {

    /* renamed from: a reason: collision with root package name */
    private final HashMap f1548a = new HashMap();

    /* renamed from: b reason: collision with root package name */
    private final HashMap f1549b = new HashMap();

    Grammar() {
    }

    public static Grammar createFromJSON(JSONObject jSONObject) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        JSONArray jSONArray = jSONObject.getJSONArray("slots");
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(WordSlot.createFromJSON(jSONArray.getJSONObject(i)));
        }
        JSONArray jSONArray2 = jSONObject.getJSONArray("constraints");
        for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
            arrayList2.add(Constraint.createFromJSON(jSONArray2.getJSONObject(i2)));
        }
        Grammar createGrammar = createGrammar(arrayList, arrayList2);
        if (createGrammar != null) {
            return createGrammar;
        }
        throw new JSONException("Error is WordSlot or Constraint list.  Unable to create grammar.");
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.elvis.Constraint>, for r8v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.elvis.Constraint>] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.elvis.WordSlot>, for r7v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.elvis.WordSlot>] */
    public static Grammar createGrammar(List<WordSlot> list, List<Constraint> list2) {
        boolean z = true;
        d.a("constraints", "not null or empty", list2 != null && !list2.isEmpty());
        String str = "slots";
        String str2 = "not null or empty";
        if (list == null || list.isEmpty()) {
            z = false;
        }
        d.a(str, str2, z);
        Grammar grammar = new Grammar();
        for (WordSlot wordSlot : list) {
            if (grammar.f1549b.put(wordSlot.getId(), wordSlot.copy()) != null) {
                Logger.error(grammar, "Duplicate WordSlot ID in slots: " + wordSlot.getId());
                return null;
            }
        }
        for (Constraint constraint : list2) {
            if (grammar.f1548a.put(constraint.getId(), constraint.copy()) != null) {
                Logger.error(grammar, "Duplicate Constraint ID in slots: " + constraint.getId());
                return null;
            }
        }
        return grammar;
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        this.f1549b.clear();
    }

    /* access modifiers changed from: 0000 */
    public final void a(Constraint constraint) {
        if (constraint != null) {
            this.f1548a.put(constraint.getId(), constraint.copy());
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(WordSlot wordSlot) {
        if (wordSlot != null) {
            this.f1549b.put(wordSlot.getId(), wordSlot.copy());
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.elvis.WordSlot>, for r5v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.elvis.WordSlot>] */
    public final void a(List<WordSlot> list) {
        if (list != null) {
            for (WordSlot wordSlot : list) {
                this.f1549b.put(wordSlot.getId(), wordSlot.copy());
            }
        }
    }

    public boolean attachWordList(WordList wordList, String str) {
        d.b("id", str);
        d.a("wordList", (Object) wordList);
        WordSlot wordSlot = (WordSlot) this.f1549b.get(str);
        if (wordSlot == null) {
            return false;
        }
        wordSlot.attachWordList(wordList);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        this.f1548a.clear();
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.elvis.Constraint>, for r5v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.elvis.Constraint>] */
    public final void b(List<Constraint> list) {
        if (list != null) {
            for (Constraint constraint : list) {
                this.f1548a.put(constraint.getId(), constraint.copy());
            }
        }
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Grammar)) {
            return false;
        }
        Grammar grammar = (Grammar) obj;
        if (this.f1548a == null) {
            if (grammar.f1548a != null) {
                return false;
            }
        } else if (!this.f1548a.equals(grammar.f1548a)) {
            return false;
        }
        return this.f1549b == null ? grammar.f1549b == null : this.f1549b.equals(grammar.f1549b);
    }

    public Constraint getConstraintByName(String str) {
        return (Constraint) this.f1548a.get(str);
    }

    public List getConstraints() {
        ArrayList arrayList = new ArrayList();
        for (Constraint add : this.f1548a.values()) {
            arrayList.add(add);
        }
        return arrayList;
    }

    public Set getRequiredWordListIds() {
        HashSet hashSet = new HashSet();
        for (Entry entry : this.f1549b.entrySet()) {
            String str = (String) entry.getKey();
            if (((WordSlot) entry.getValue()).getWordListRequired()) {
                hashSet.add(str);
            }
        }
        return hashSet;
    }

    public List getSlots() {
        ArrayList arrayList = new ArrayList();
        for (WordSlot add : this.f1549b.values()) {
            arrayList.add(add);
        }
        return arrayList;
    }

    public WordSlot getWordSlotByName(String str) {
        return (WordSlot) this.f1549b.get(str);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f1548a == null ? 0 : this.f1548a.hashCode()) + 31) * 31;
        if (this.f1549b != null) {
            i = this.f1549b.hashCode();
        }
        return hashCode + i;
    }

    public JSONObject toJSON() {
        b bVar = new b();
        bVar.a("slots", getSlots());
        bVar.a("constraints", getConstraints());
        return bVar;
    }
}
