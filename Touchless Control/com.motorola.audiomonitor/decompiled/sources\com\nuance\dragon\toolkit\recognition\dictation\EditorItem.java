package com.nuance.dragon.toolkit.recognition.dictation;

public interface EditorItem {

    public enum Type {
        TOKEN,
        WORD,
        WHITE_SPACE
    }

    Type getType();

    int length();

    String toString();
}
