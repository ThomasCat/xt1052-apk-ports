package a;

import com.motorola.audiomonitor.actions.utils.a;
import com.motorola.audiomonitor.actions.utils.f;
import com.motorola.audiomonitor.actions.utils.u;
import java.util.Comparator;

class ab implements Comparator {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ z f3a;

    ab(z zVar) {
        this.f3a = zVar;
    }

    /* renamed from: a */
    public int compare(a aVar, a aVar2) {
        u a2 = f.a(this.f3a.f86a.o);
        if (aVar == null || aVar2 == null) {
            return -1;
        }
        return aVar.b().equals(aVar2.b()) ? a2.a(aVar2.e()) - a2.a(aVar.e()) : ((int) aVar2.d()) - ((int) aVar.d());
    }
}
