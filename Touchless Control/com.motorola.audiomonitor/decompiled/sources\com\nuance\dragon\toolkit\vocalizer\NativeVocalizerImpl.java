package com.nuance.dragon.toolkit.vocalizer;

import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.vocalizer.NativeVocalizer.SpeakListener;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.Frequencies;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

class NativeVocalizerImpl implements NativeVocalizer {

    /* renamed from: a reason: collision with root package name */
    private final FileManager f2135a;

    /* renamed from: b reason: collision with root package name */
    private long f2136b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public AudioType f2137c;

    /* renamed from: d reason: collision with root package name */
    private final HashMap f2138d = new HashMap();
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public SpeakListener f2139e;

    public interface OutputDeviceListener {
        void notify(short[] sArr, boolean z);
    }

    public class Status {
        public static final int FILE_ERROR = 3;
        public static final int INVALID_MODEL = 2;
        public static final int OK = 0;
        public static final int OOM = 1;
        public static final int OTHER_ERROR = 4;

        public Status() {
        }
    }

    static {
        System.loadLibrary("dmt_vocalizer");
    }

    NativeVocalizerImpl(FileManager fileManager) {
        this.f2135a = fileManager;
    }

    static /* synthetic */ AudioChunk a(AudioType audioType, short[] sArr) {
        if (sArr == null || sArr.length <= 0) {
            return null;
        }
        short[] sArr2 = new short[sArr.length];
        System.arraycopy(sArr, 0, sArr2, 0, sArr2.length);
        return new AudioChunk(audioType, sArr2);
    }

    public final AudioType a(String str, String str2, int i) {
        a();
        this.f2137c = null;
        if ((str != null && str.length() > 0) || (str2 != null && str2.length() > 0)) {
            long[] jArr = new long[1];
            int vocalizerCreate = vocalizerCreate(jArr, this.f2135a, str, str2, i, new OutputDeviceListener() {
                public final void notify(short[] sArr, boolean z) {
                    if (NativeVocalizerImpl.this.f2139e != null) {
                        NativeVocalizerImpl.this.f2139e.onNewChunk(NativeVocalizerImpl.a(NativeVocalizerImpl.this.f2137c, sArr), z);
                    }
                }
            });
            if (vocalizerCreate == 0 && jArr[0] != 0) {
                this.f2136b = jArr[0];
                switch (vocalizerGetAudioFrequency(this.f2136b)) {
                    case 8:
                        this.f2137c = new AudioType(Encoding.PCM_16, 8000);
                        break;
                    case 11:
                        this.f2137c = new AudioType(Encoding.PCM_16, 11025);
                        break;
                    case 16:
                        this.f2137c = new AudioType(Encoding.PCM_16, 16000);
                        break;
                    case Frequencies.FREQ_22KHZ /*22*/:
                        this.f2137c = new AudioType(Encoding.PCM_16, 22050);
                        break;
                    default:
                        Logger.error(this, "Unable to create native Vocalizer (invalid model frequency).");
                        vocalizerDestroy(this.f2136b);
                        this.f2136b = 0;
                        break;
                }
            } else {
                Logger.error(this, "Unable to create native Vocalizer (error status = " + vocalizerCreate + ").");
            }
        }
        return this.f2137c;
    }

    public final void a() {
        if (this.f2136b != 0) {
            for (Entry value : this.f2138d.entrySet()) {
                vocalizerReleaseResource(this.f2136b, ((Long) value.getValue()).longValue());
            }
            this.f2138d.clear();
            vocalizerDestroy(this.f2136b);
            this.f2136b = 0;
        }
    }

    public final void a(String str) {
        if (this.f2136b != 0) {
            Long l = (Long) this.f2138d.remove(str);
            if (l != null) {
                vocalizerReleaseResource(this.f2136b, l.longValue());
            } else {
                Logger.warn(this, "The vocalizer resource was never loaded or has already been released: " + str);
            }
        }
    }

    public final boolean a(String str, int i, int i2, int i3, SpeakListener speakListener) {
        if (this.f2136b != 0) {
            this.f2139e = speakListener;
            if (vocalizerSpeak(this.f2136b, str, i, i2, i3) == 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean a(String str, String str2) {
        if (this.f2138d.containsKey(str)) {
            Logger.warn(this, "This resource is already loaded.");
        } else if (this.f2136b != 0) {
            long[] jArr = new long[1];
            int vocalizerLoadResource = vocalizerLoadResource(this.f2136b, str, str2, jArr);
            if (vocalizerLoadResource != 0 || jArr[0] == 0) {
                Logger.warn(this, "Unable to load resource (error status = " + vocalizerLoadResource + ").");
                return false;
            }
            this.f2138d.put(str, Long.valueOf(jArr[0]));
            return true;
        }
        return false;
    }

    public final boolean a(List list) {
        return getVocalizerModels(this.f2135a, list) == 0;
    }

    public final void b() {
        if (this.f2136b != 0) {
            vocalizerCancel(this.f2136b);
        }
    }

    public native int getVocalizerModels(FileManager fileManager, List list);

    public native void vocalizerCancel(long j);

    public native int vocalizerCreate(long[] jArr, FileManager fileManager, String str, String str2, int i, OutputDeviceListener outputDeviceListener);

    public native void vocalizerDestroy(long j);

    public native int vocalizerGetAudioFrequency(long j);

    public native int vocalizerLoadResource(long j, String str, String str2, long[] jArr);

    public native void vocalizerReleaseResource(long j, long j2);

    public native int vocalizerSpeak(long j, String str, int i, int i2, int i3);
}
