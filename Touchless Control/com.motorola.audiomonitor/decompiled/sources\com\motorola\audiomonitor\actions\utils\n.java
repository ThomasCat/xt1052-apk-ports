package com.motorola.audiomonitor.actions.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

class n extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ m f250a;

    n(m mVar) {
        this.f250a = mVar;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (!TextUtils.isEmpty(action)) {
            if (m.f243b) {
                Log.d(m.f242a, "action received " + action);
            }
            if (action.equals("android.intent.action.USER_PRESENT")) {
                synchronized (this.f250a.i) {
                    this.f250a.i.set(false);
                    this.f250a.i.notify();
                }
            } else if (action.equals("android.intent.action.SCREEN_OFF")) {
                synchronized (this.f250a.i) {
                    this.f250a.i.set(true);
                    this.f250a.i.notify();
                }
            }
        }
    }
}
