package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.util.internal.d;
import org.json.JSONObject;

public class DictionaryParam extends DataParam {
    protected final Dictionary _dict;

    DictionaryParam(int i, String str, Dictionary dictionary) {
        super(i, str);
        d.a("dict", (Object) dictionary);
        this._dict = dictionary;
    }

    public DictionaryParam(String str, Dictionary dictionary) {
        this(6, str, dictionary);
    }

    DictionaryParam(JSONObject jSONObject) {
        super((JSONObject) d.a("json", (Object) jSONObject));
        this._dict = Dictionary.createFromJSON(jSONObject.getJSONObject("value"));
    }

    /* access modifiers changed from: 0000 */
    public boolean a(d dVar, a aVar) {
        dVar.a(this.f1355a, this._dict);
        return true;
    }
}
