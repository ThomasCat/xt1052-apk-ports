package com.nuance.dragon.toolkit.recognition.a;

import com.nuance.dragon.toolkit.recognition.dictation.a.f;
import com.nuance.dragon.toolkit.util.internal.g;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class c {

    /* renamed from: a reason: collision with root package name */
    private final f f1918a;

    /* renamed from: b reason: collision with root package name */
    private final List f1919b;

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.recognition.a.b>, for r4v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.recognition.a.b>] */
    public c(List<b> list, Map map) {
        ArrayList arrayList = new ArrayList();
        for (b d2 : list) {
            arrayList.add(d2.d());
        }
        this.f1918a = new f(arrayList, map);
        this.f1919b = new g((List) list);
    }

    public final List a() {
        return this.f1919b;
    }
}
