package com.nuance.dragon.toolkit.recognition;

import java.util.List;

public interface RecognitionInterpreter {
    InterpretedRecognition getInterpretedResult(Object obj);

    List getUpdateRequiredList(Object obj);

    boolean processForCloud(Object obj, List list);
}
