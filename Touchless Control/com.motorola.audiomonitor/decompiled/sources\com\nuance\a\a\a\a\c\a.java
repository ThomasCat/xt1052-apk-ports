package com.nuance.a.a.a.a.c;

import com.nuance.a.a.a.a.b.a.a.C0001a;

public class a {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f791a = com.nuance.a.a.a.a.b.a.a.a(a.class);

    public static byte[] a(byte b2, byte b3, short s, byte[] bArr) {
        if (f791a.b()) {
            f791a.b((Object) "Building XMode buffer: protocol=" + b2 + " version=" + b3 + " cmd=" + s + " payload len:" + bArr.length);
            f791a.a(bArr);
        }
        int length = bArr.length;
        byte[] bArr2 = new byte[(length + 8)];
        System.arraycopy(new b(b2, b3, s, length).a(), 0, bArr2, 0, 8);
        System.arraycopy(bArr, 0, bArr2, 8, length);
        return bArr2;
    }

    public static byte[] a(byte[] bArr, byte[] bArr2) {
        if (f791a.b()) {
            f791a.b((Object) "Appending session ID");
            f791a.a(bArr2);
            f791a.a(bArr);
        }
        byte[] bArr3 = new byte[(bArr.length + 16)];
        System.arraycopy(bArr2, 0, bArr3, 0, 16);
        System.arraycopy(bArr, 0, bArr3, 16, bArr.length);
        return bArr3;
    }
}
