package com.motorola.audiomonitor.uis.training;

import android.view.View;
import android.view.View.OnClickListener;

class b implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ StartTrainingActivity f739a;

    b(StartTrainingActivity startTrainingActivity) {
        this.f739a = startTrainingActivity;
    }

    public void onClick(View view) {
        if (view.isEnabled()) {
            view.setEnabled(false);
            this.f739a.b();
            this.f739a.finish();
        }
    }
}
