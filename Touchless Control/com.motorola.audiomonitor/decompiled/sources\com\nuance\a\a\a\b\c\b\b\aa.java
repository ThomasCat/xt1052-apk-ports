package com.nuance.a.a.a.b.c.b.b;

import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;

public class aa extends f {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f853a = a.a(aa.class);

    /* renamed from: b reason: collision with root package name */
    private String f854b;

    public aa(String str) {
        super(193);
        this.f854b = str;
    }

    public aa(byte[] bArr) {
        super(193);
        try {
            this.f854b = new String(bArr, "UTF-8");
        } catch (Exception e2) {
            f853a.e("PDXUTF8String() " + e2.getMessage());
            this.f854b = new String(bArr);
        }
    }

    public final String a() {
        return this.f854b;
    }

    public final byte[] b() {
        try {
            return super.a(this.f854b.getBytes("UTF-8"));
        } catch (Exception e2) {
            f853a.e("PDXUTF8String().toByteArray() " + e2.getMessage());
            return super.a(this.f854b.getBytes());
        }
    }
}
