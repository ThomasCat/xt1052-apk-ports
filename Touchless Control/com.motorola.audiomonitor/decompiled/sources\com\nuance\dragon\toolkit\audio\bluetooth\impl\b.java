package com.nuance.dragon.toolkit.audio.bluetooth.impl;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import com.nuance.dragon.toolkit.audio.bluetooth.impl.Bluetooth.HeadsetStateListener;
import com.nuance.dragon.toolkit.audio.bluetooth.impl.BluetoothHeadset_3_0.ServiceListener;
import java.util.Set;

final class b extends Bluetooth {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public BluetoothHeadset_3_0 f1054a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public HeadsetStateListener f1055b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public boolean f1056c;

    /* renamed from: d reason: collision with root package name */
    private boolean f1057d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public boolean f1058e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public boolean f1059f;

    /* renamed from: g reason: collision with root package name */
    private final ServiceListener f1060g = new ServiceListener() {
        public final void onServiceConnected(boolean z) {
            boolean z2 = false;
            Bluetooth.a("onServiceConnected(), BluetoothHeadset_3_0 service connected");
            Bluetooth.a("onServiceConnected(), mHeadset = " + b.this.f1054a + ", mListener = " + b.this.f1055b);
            if (b.this.f1054a == null || !z) {
                b.this.f1055b.onServiceConnected(false);
                return;
            }
            b.this.f1056c = true;
            b bVar = b.this;
            if (b.this.f1054a.getState() == BluetoothHeadset_3_0.STATE_CONNECTED) {
                z2 = true;
            }
            bVar.f1058e = z2;
            if (b.this.f1055b != null) {
                b.this.f1055b.onServiceConnected(true);
                b.this.f1055b.onConnectionStateChanged(b.this.getHeadsetState());
            }
        }

        public final void onServiceDisconnected() {
            Bluetooth.a("BluetoothHeadset_3_0 service disconnected");
            b.this.f1055b.onServiceDisconnected();
            b.this.f1056c = false;
        }
    };

    /* renamed from: h reason: collision with root package name */
    private final BroadcastReceiver f1061h = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            boolean z = true;
            String action = intent.getAction();
            if (action.equals(BluetoothHeadset_3_0.ACTION_STATE_CHANGED)) {
                int intExtra = intent.getIntExtra(BluetoothHeadset_3_0.EXTRA_STATE, -1);
                Bluetooth.a("Bluetooth state changed: " + intExtra);
                if (intExtra != BluetoothHeadset_3_0.STATE_CONNECTED) {
                    z = false;
                }
                if (b.this.f1058e != z) {
                    b.this.f1058e = z;
                    if (b.this.f1055b != null) {
                        b.this.f1055b.onConnectionStateChanged(b.this.getHeadsetState());
                    }
                }
            } else if (action.equals(BluetoothHeadset_3_0.ACTION_AUDIO_STATE_CHANGED)) {
                int intExtra2 = intent.getIntExtra(BluetoothHeadset_3_0.EXTRA_STATE, -1);
                Bluetooth.a("Bluetooth audio state changed: " + intExtra2);
                if (intExtra2 == BluetoothHeadset_3_0.AUDIO_STATE_DISCONNECTED || intExtra2 == BluetoothHeadset_3_0.AUDIO_STATE_CONNECTED || intExtra2 == BluetoothHeadset_3_0.AUDIO_STATE_CONNECTING) {
                    if (intExtra2 != BluetoothHeadset_3_0.AUDIO_STATE_CONNECTED) {
                        z = false;
                    }
                    if (b.this.f1059f != z) {
                        b.this.f1059f = z;
                        if (b.this.f1055b != null) {
                            b.this.f1055b.onAudioStateChanged(b.this.getAudioState());
                        }
                    }
                }
            }
        }
    };

    public b(Context context) {
        super(context);
    }

    public final void close() {
        if (this.f1057d) {
            this.f1057d = false;
            this.mContext.unregisterReceiver(this.f1061h);
        }
        if (this.f1054a != null) {
            try {
                this.f1054a.close();
            } catch (Throwable th) {
            }
            this.f1054a = null;
        }
    }

    public final int getAudioState() {
        return this.f1059f ? 1 : 0;
    }

    public final String[] getBondedDevices() {
        Set<BluetoothDevice> bondedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
        if (bondedDevices == null) {
            return null;
        }
        String[] strArr = new String[bondedDevices.size()];
        int i = 0;
        for (BluetoothDevice bluetoothDevice : bondedDevices) {
            String name = bluetoothDevice.getName();
            int i2 = i + 1;
            strArr[i] = name != null ? name : bluetoothDevice.getAddress();
            i = i2;
        }
        return strArr;
    }

    public final int getHeadsetState() {
        this.f1058e = this.f1054a.getState() == BluetoothHeadset_3_0.STATE_CONNECTED;
        return this.f1058e ? 1 : 0;
    }

    public final int getPlaybackStream() {
        int i = Audio.VOICE_CALL_STREAM;
        try {
            i = ReflectionFactory.getField("android.media.AudioManager", "STREAM_BLUETOOTH_SCO").getInt(null);
        } catch (Exception e2) {
            Log.d("DMT-Bluetooth", "getPlaybackStream, problem finding field STREAM_BLUETOOTH_SCO in class AudioManager");
        }
        Log.d("DMT-Bluetooth", "getPlaybackStream returns = " + i);
        return i;
    }

    public final int getRecordingSource() {
        return Audio.DEFAULT_AUDIO_SOURCE;
    }

    public final boolean isEnabled() {
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter != null) {
            return defaultAdapter.isEnabled();
        }
        return false;
    }

    public final void setHeadsetStateListener(HeadsetStateListener headsetStateListener) {
        this.f1055b = headsetStateListener;
        if (this.f1054a == null) {
            Log.d("DMT-Bluetooth", "setHeadsetStateListener(), mHeadset is null, initializing...");
            this.f1054a = new BluetoothHeadset_3_0(this.mContext, this.f1060g);
            IntentFilter intentFilter = new IntentFilter(BluetoothHeadset_3_0.ACTION_STATE_CHANGED);
            intentFilter.addAction(BluetoothHeadset_3_0.ACTION_AUDIO_STATE_CHANGED);
            this.mContext.registerReceiver(this.f1061h, intentFilter);
            this.f1057d = true;
        }
        Log.d("DMT-Bluetooth", "setHeadsetStateListener(), mServiceConnected = " + this.f1056c);
        if (this.f1056c) {
            this.f1055b.onConnectionStateChanged(getHeadsetState());
        }
    }

    public final boolean startBluetoothSco() {
        boolean startVoiceRecognition = this.f1054a.startVoiceRecognition();
        Log.d("DMT-Bluetooth", "mHeadset.startVoiceRecognition() returned " + startVoiceRecognition);
        return startVoiceRecognition;
    }

    public final void stopBluetoothSco() {
        String str = "DMT-Bluetooth";
        Log.d(str, "mHeadset.stopVoiceRecognition() returned " + this.f1054a.stopVoiceRecognition());
    }
}
