package f;

import android.util.Log;
import org.json.JSONObject;

class g implements t {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ f f2235a;

    g(f fVar) {
        this.f2235a = fVar;
    }

    public void a() {
    }

    public void a(int i, JSONObject jSONObject) {
        if (f.f2229b) {
            Log.d(f.f2228a, "onComplete");
        }
        this.f2235a.a(i, jSONObject);
    }

    public void a(i iVar, boolean z) {
        if (f.f2229b) {
            Log.d(f.f2228a, "onInitComplete");
        }
    }

    public void a(String str) {
        boolean b2;
        Log.e(f.f2228a, "Error during audio parsing: " + str);
        synchronized (this.f2235a.f2233f) {
            b2 = this.f2235a.f2234g;
            this.f2235a.f2234g = false;
            this.f2235a.f2233f.notify();
        }
        if (b2) {
            this.f2235a.f2232e.a(null, -1, -1);
        }
    }

    public void b() {
    }
}
