package com.nuance.dragon.toolkit.elvis;

public interface ElvisResultFilter {
    ElvisResult applyFilter(ElvisResult elvisResult);
}
