package com.motorola.audiomonitor.uis;

import a.e;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.motorola.audiomonitor.C0014R;
import java.util.List;

public class d extends Fragment {

    /* renamed from: a reason: collision with root package name */
    public static int f690a = 0;

    /* renamed from: b reason: collision with root package name */
    public static int f691b = -1;

    /* renamed from: c reason: collision with root package name */
    public static boolean f692c = false;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public static List f693g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public static Fragment f694h;

    /* renamed from: d reason: collision with root package name */
    private View f695d;

    /* renamed from: e reason: collision with root package name */
    private boolean f696e;

    /* renamed from: f reason: collision with root package name */
    private e f697f;

    /* access modifiers changed from: private */
    public void a(Context context) {
        if (this.f697f != null) {
            DetailContactListActivity.f623a = this.f697f;
            Intent intent = new Intent(context, DetailContactListActivity.class);
            intent.setFlags(268435456);
            intent.addFlags(65536);
            intent.addFlags(67108864);
            context.startActivity(intent);
        }
    }

    public void a(e eVar) {
        f693g = eVar.c();
        if (f692c) {
            this.f696e = false;
        } else if (f693g.size() > 3) {
            f693g = f693g.subList(0, 3);
            this.f696e = true;
            this.f697f = eVar;
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        int i = 0;
        super.onCreateView(layoutInflater, viewGroup, bundle);
        f694h = this;
        this.f695d = layoutInflater.inflate(C0014R.layout.aov_call_action_multi_contacts_view, null);
        ListView listView = (ListView) this.f695d.findViewById(C0014R.id.call_contacts_list);
        FrameLayout frameLayout = (FrameLayout) this.f695d.findViewById(C0014R.id.show_framelayout);
        TextView textView = (TextView) this.f695d.findViewById(C0014R.id.show_all_matches);
        textView.setOnClickListener(new e(this));
        listView.setAdapter(new f(getActivity()));
        if (f690a >= 0) {
            listView.setSelection(f690a);
        }
        frameLayout.setVisibility(f692c ? 8 : 0);
        if (!this.f696e) {
            i = 8;
        }
        textView.setVisibility(i);
        return this.f695d;
    }
}
