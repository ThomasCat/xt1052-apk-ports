package com.motorola.audiomonitor.uis.training;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import com.motorola.audiomonitor.C0014R;
import h.k;

public class TrainingCompletedActivity extends Activity {
    public void finish() {
        super.finish();
        overridePendingTransition(0, C0014R.anim.activity_close_exit);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(C0014R.layout.training_completed);
        TextView textView = (TextView) findViewById(C0014R.id.training_finished);
        if (getIntent().getStringExtra("show_voice_search_setting") != null) {
            textView.setText(C0014R.string.training_continue);
        }
        ((TextView) findViewById(C0014R.id.training_complete_note)).setText(Html.fromHtml(getResources().getString(C0014R.string.training_complete_notes)));
        ActionBar actionBar = getActionBar();
        actionBar.setIcon(k.b());
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(C0014R.string.personalization_title);
        getApplicationContext().sendBroadcast(new Intent("com.motorola.audiomonitor.CANCEL_NOTIFY"));
    }

    public void onFinishedClick(View view) {
        String stringExtra = getIntent().getStringExtra("show_voice_search_setting");
        if (stringExtra != null) {
            Intent intent = new Intent();
            intent.setClassName(this, stringExtra);
            startActivity(intent);
        }
        finish();
    }

    public boolean onNavigateUp() {
        finish();
        return true;
    }
}
