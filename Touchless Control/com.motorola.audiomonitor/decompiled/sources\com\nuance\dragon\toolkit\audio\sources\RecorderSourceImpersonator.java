package com.nuance.dragon.toolkit.audio.sources;

import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;

public class RecorderSourceImpersonator extends RecorderSource {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public boolean f1292a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public boolean f1293b;

    public RecorderSourceImpersonator(AudioType audioType) {
        super(audioType, new Handler());
    }

    public boolean isActive() {
        return this.f1292a;
    }

    /* access modifiers changed from: protected */
    public boolean isCodecSupported(AudioType audioType) {
        return audioType.equals(AudioType.PCM_16k);
    }

    /* access modifiers changed from: protected */
    public boolean startRecordingInternal(AudioType audioType) {
        if (!audioType.equals(AudioType.PCM_16k)) {
            return false;
        }
        this.f1292a = true;
        this.f1293b = false;
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public final void run() {
                RecorderSourceImpersonator.this.handleNewAudio(new AudioChunk(AudioType.PCM_16k, new short[320]));
                if (!RecorderSourceImpersonator.this.f1293b) {
                    handler.postDelayed(this, 100);
                    return;
                }
                RecorderSourceImpersonator recorderSourceImpersonator = RecorderSourceImpersonator.this;
                RecorderSourceImpersonator.this.f1293b = false;
                RecorderSourceImpersonator.this.f1292a = false;
                RecorderSourceImpersonator.this.handleSourceClosed();
            }
        }, 100);
        return true;
    }

    /* access modifiers changed from: protected */
    public void stopRecordingInternal() {
        this.f1293b = true;
    }
}
