package com.nuance.dragon.toolkit.grammar.content;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.grammar.WordAction;
import com.nuance.dragon.toolkit.grammar.content.ContentManager.PlatformFullIterator;
import com.nuance.dragon.toolkit.util.Logger;

public abstract class UriContentManager extends ContentManager {
    protected final Uri _uri;

    /* renamed from: a reason: collision with root package name */
    private ContentObserver f1805a;

    public abstract class UriPlatformFullIterator implements PlatformFullIterator {

        /* renamed from: a reason: collision with root package name */
        private Cursor f1807a;

        /* renamed from: b reason: collision with root package name */
        private WordAction f1808b = null;

        /* renamed from: c reason: collision with root package name */
        private final int f1809c;

        protected UriPlatformFullIterator(Context context, Uri uri, String[] strArr) {
            try {
                this.f1807a = context.getContentResolver().query(uri, strArr, null, null, null);
            } catch (Exception e2) {
                Logger.error(this, "Error getting cursor for URI " + uri.toString());
            }
            this.f1809c = a();
        }

        protected UriPlatformFullIterator(Context context, Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
            try {
                this.f1807a = context.getContentResolver().query(uri, strArr, str, strArr2, str2);
            } catch (Exception e2) {
                Logger.error(this, "Error getting cursor for URI " + uri.toString());
            }
            this.f1809c = a();
        }

        protected UriPlatformFullIterator(Cursor cursor) {
            this.f1807a = cursor;
            this.f1809c = a();
        }

        private int a() {
            if (this.f1807a == null) {
                return 0;
            }
            init(this.f1807a);
            int count = this.f1807a.getCount();
            if (this.f1807a.moveToFirst()) {
                this.f1807a.moveToPrevious();
                return count;
            }
            this.f1807a.close();
            this.f1807a = null;
            return count;
        }

        /* access modifiers changed from: protected */
        public abstract WordAction getNext(Cursor cursor);

        public final int getSize() {
            return this.f1809c;
        }

        public final boolean hasNext() {
            if (this.f1808b != null) {
                return true;
            }
            this.f1808b = this.f1807a == null ? null : getNext(this.f1807a);
            if (this.f1808b == null && this.f1807a != null) {
                this.f1807a.close();
                this.f1807a = null;
            }
            return this.f1808b != null;
        }

        /* access modifiers changed from: protected */
        public abstract void init(Cursor cursor);

        public final WordAction next() {
            if (this.f1808b != null) {
                WordAction wordAction = this.f1808b;
                this.f1808b = null;
                return wordAction;
            }
            WordAction next = this.f1807a == null ? null : getNext(this.f1807a);
            if (this.f1808b != null || this.f1807a == null) {
                return next;
            }
            this.f1807a.close();
            this.f1807a = null;
            return next;
        }

        public final void remove() {
        }
    }

    protected UriContentManager(String str, FileManager fileManager, Uri uri, boolean z, boolean z2, Context context) {
        super(str, fileManager, z2, context);
        this._uri = uri;
        initialize(z);
    }

    /* access modifiers changed from: protected */
    public void stopWatchingForContentUpdates(Context context) {
        if (this.f1805a != null) {
            context.getContentResolver().unregisterContentObserver(this.f1805a);
            this.f1805a = null;
        }
    }

    /* access modifiers changed from: protected */
    public void watchForContentUpdates(Context context) {
        this.f1805a = new ContentObserver(new Handler()) {
            public final boolean deliverSelfNotifications() {
                return false;
            }

            public final void onChange(boolean z) {
                if (!z) {
                    UriContentManager.this.onContentUpdate();
                }
            }
        };
        context.getContentResolver().registerContentObserver(this._uri, true, this.f1805a);
    }
}
