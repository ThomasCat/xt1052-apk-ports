package com.motorola.audiomonitor.comsocket;

import android.net.LocalSocket;
import android.text.TextUtils;
import android.util.Log;
import com.motorola.audiomonitor.ProtectedByteBuffer;
import com.motorola.audiomonitor.a;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;

public class f extends Thread {

    /* renamed from: a reason: collision with root package name */
    private static final String f455a = ("AMonitor." + f.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f456b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private LocalSocket f457c;

    /* renamed from: d reason: collision with root package name */
    private boolean f458d = true;

    /* renamed from: e reason: collision with root package name */
    private g f459e;

    /* renamed from: f reason: collision with root package name */
    private byte[] f460f = new byte[4];

    /* renamed from: g reason: collision with root package name */
    private BufferedInputStream f461g;

    /* renamed from: h reason: collision with root package name */
    private BufferedOutputStream f462h;
    private boolean i = false;
    private d j = d.OTHER_LINK;

    f(LocalSocket localSocket, g gVar, d dVar) {
        super("MediaCommandSocket");
        this.f457c = localSocket;
        this.f459e = gVar;
        this.j = dVar;
    }

    private boolean a(byte[] bArr) {
        if (this.f462h == null) {
            return false;
        }
        try {
            this.f462h.write(bArr);
            this.f462h.flush();
            return true;
        } catch (IOException e2) {
            Log.e(f455a, "I/O exception on writing to a stream.");
            return false;
        }
    }

    private boolean b(ProtectedByteBuffer protectedByteBuffer) {
        if (this.f462h == null) {
            return false;
        }
        try {
            protectedByteBuffer.writeTo(this.f462h);
            this.f462h.flush();
            return true;
        } catch (IOException e2) {
            Log.e(f455a, "I/O exception on writing to a stream.");
            return false;
        }
    }

    public d a() {
        return this.j;
    }

    public boolean a(ProtectedByteBuffer protectedByteBuffer) {
        boolean b2;
        synchronized (this) {
            b2 = b(protectedByteBuffer);
        }
        return b2;
    }

    public boolean a(ProtectedByteBuffer protectedByteBuffer, int i2, int i3) {
        boolean z = false;
        synchronized (this) {
            if (this.f462h != null) {
                try {
                    protectedByteBuffer.writeTo(this.f462h, i2, i3);
                    this.f462h.flush();
                    z = true;
                } catch (IOException e2) {
                    Log.e(f455a, "I/O exception on writing to a stream.");
                }
            }
        }
        return z;
    }

    public void b() {
        this.f458d = false;
    }

    public void run() {
        try {
            synchronized (this) {
                this.f461g = new BufferedInputStream(this.f457c.getInputStream());
                this.f462h = new BufferedOutputStream(this.f457c.getOutputStream());
            }
            if (this.f459e != null) {
                this.f459e.c(this);
            }
            if (this.i) {
                a(new byte[]{10, 11, 12, 5});
            }
            byte[] bArr = {10, 11, 12, 4};
            if (this.j == d.RECOG_LINK) {
                bArr[3] = 1;
            }
            a(bArr);
            this.f457c.setSoTimeout(1000);
            while (this.f458d) {
                try {
                    int read = this.f461g.read(this.f460f);
                    if (read == 4) {
                        if (this.f460f[0] == 10 && this.f460f[1] == 11 && this.f460f[2] == 12) {
                            switch (this.f460f[3]) {
                                case 2:
                                    if (f456b) {
                                        Log.d(f455a, "Read Ready:");
                                    }
                                    if (this.f459e == null) {
                                        break;
                                    } else {
                                        this.f459e.b(this);
                                        break;
                                    }
                                case 3:
                                    if (f456b) {
                                        Log.d(f455a, "Stopped:");
                                    }
                                    this.f458d = false;
                                    break;
                                default:
                                    if (!f456b) {
                                        break;
                                    } else {
                                        Log.d(f455a, "Unknown command received: " + this.f460f[3]);
                                        break;
                                    }
                            }
                        } else if (f456b) {
                            Log.d(f455a, "Corrupted command received?");
                        }
                    } else {
                        if (f456b) {
                            Log.d(f455a, "Closing socket (count = " + read + ")");
                        }
                        this.f458d = false;
                    }
                } catch (IOException e2) {
                    if (!TextUtils.equals(e2.getMessage(), "Try again")) {
                        Log.e(f455a, "I/O exception:" + e2.toString());
                        this.f458d = false;
                    }
                }
            }
            try {
                synchronized (this) {
                    if (this.f461g != null) {
                        this.f461g.close();
                        this.f461g = null;
                    }
                    if (this.f462h != null) {
                        this.f462h.close();
                        this.f462h = null;
                    }
                    if (this.f457c != null) {
                        this.f457c.close();
                        this.f457c = null;
                    }
                }
            } catch (IOException e3) {
            }
            if (this.f459e != null) {
                this.f459e.a(this);
            }
            if (f456b) {
                Log.d(f455a, "MediaCommandSocket... done");
            }
        } catch (IOException e4) {
            Log.e(f455a, e4.toString());
        }
    }
}
