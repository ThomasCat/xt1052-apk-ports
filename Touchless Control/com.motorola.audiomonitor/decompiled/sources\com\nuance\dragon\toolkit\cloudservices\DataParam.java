package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class DataParam extends Param implements JSONCompliant {
    DataParam(int i, String str) {
        super(i, str);
    }

    DataParam(JSONObject jSONObject) {
        super(jSONObject.getInt("type"), jSONObject.getString("key"));
    }

    public static DataParam createFromJSON(JSONObject jSONObject) {
        int i = jSONObject.getInt("type");
        switch (i) {
            case 3:
                return new StringParam(jSONObject);
            case 6:
                return new DictionaryParam(jSONObject);
            default:
                throw new JSONException("Can't create from JSON, type=" + i);
        }
    }

    public JSONObject toJSON() {
        b bVar = new b();
        bVar.a("key", (Object) this.f1355a);
        bVar.a("type", (Object) Integer.valueOf(this.f1356b));
        return bVar;
    }
}
