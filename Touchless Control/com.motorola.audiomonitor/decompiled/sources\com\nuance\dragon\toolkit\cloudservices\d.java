package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.a.a.a.b.b.a;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.data.Data.Dictionary;

interface d {
    void a();

    void a(String str);

    void a(String str, AudioChunk audioChunk, boolean z);

    void a(String str, Dictionary dictionary);

    void a(String str, Dictionary dictionary, int i);

    void a(String str, Dictionary dictionary, a aVar);

    void a(String str, String str2);

    void a(String str, byte[] bArr);

    void b();

    void b(String str, Dictionary dictionary);

    void b(String str, String str2);

    void c(String str, Dictionary dictionary);

    void d(String str, Dictionary dictionary);
}
