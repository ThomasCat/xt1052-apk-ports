package com.nuance.dragon.toolkit.elvis;

import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import org.json.JSONException;
import org.json.JSONObject;

public class ElvisLanguage implements JSONCompliant {
    public final String description;
    public final String name;

    ElvisLanguage(String str, String str2) {
        this.name = str;
        this.description = str2;
    }

    public static ElvisLanguage createFromJSON(JSONObject jSONObject) {
        ElvisLanguage[] elvisLanguageArr;
        String string = jSONObject.getString("name");
        for (ElvisLanguage elvisLanguage : d.ALL_LANGUAGES) {
            if (elvisLanguage.name.equals(string)) {
                return elvisLanguage;
            }
        }
        throw new JSONException("Invalid language name: " + string);
    }

    public final boolean equals(Object obj) {
        return obj != null && (obj instanceof ElvisLanguage) && ((ElvisLanguage) obj).name.equals(this.name);
    }

    public JSONObject toJSON() {
        b bVar = new b();
        bVar.a("name", (Object) this.name);
        return bVar;
    }

    public final String toString() {
        return this.description;
    }
}
