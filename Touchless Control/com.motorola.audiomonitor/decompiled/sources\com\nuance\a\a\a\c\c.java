package com.nuance.a.a.a.c;

import android.util.Log;
import com.nuance.a.a.a.a.b.a.a.C0001a;

public final class c extends C0001a {

    /* renamed from: a reason: collision with root package name */
    String f954a;

    public c(Class cls) {
        this.f954a = cls.getSimpleName();
    }

    public final void a(Object obj) {
        Log.v("NMSP_", "[" + this.f954a + "] " + obj.toString());
    }

    public final void a(Object obj, Throwable th) {
        th.printStackTrace();
        Log.w("NMSP", "[" + this.f954a + "] " + obj.toString(), th);
    }

    public final boolean a() {
        return Log.isLoggable("NMSP_", 2);
    }

    public final void b(Object obj) {
        Log.d("NMSP_", "[" + this.f954a + "] " + obj.toString());
    }

    public final boolean b() {
        return Log.isLoggable("NMSP_", 3);
    }

    public final void c(Object obj) {
        Log.i("NMSP", "[" + this.f954a + "] " + obj.toString());
    }

    public final boolean c() {
        return Log.isLoggable("NMSP", 4);
    }

    public final void d(Object obj) {
        Log.w("NMSP", "[" + this.f954a + "] " + obj.toString());
    }

    public final boolean d() {
        return Log.isLoggable("NMSP", 5);
    }

    public final void e(Object obj) {
        Log.e("NMSP", "[" + this.f954a + "] " + obj.toString());
    }

    public final boolean e() {
        return Log.isLoggable("NMSP", 6);
    }

    public final void f() {
    }

    public final void g() {
    }
}
