package com.motorola.audiomonitor.actions.utils;

import android.util.Log;

class x implements ai {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ v f282a;

    x(v vVar) {
        this.f282a = vVar;
    }

    public void a(int i, int i2) {
        if (v.f274b) {
            Log.d(v.f273a, "speak onDone " + i2 + " of " + i);
        }
        this.f282a.f276d.removeCallbacks(this.f282a.y);
        if (this.f282a.f279g.f180g != 0) {
            this.f282a.f276d.postDelayed(this.f282a.y, 20000);
        }
        if (i == i2) {
            this.f282a.j();
        }
        if (this.f282a.u != null) {
            this.f282a.u.a(i, i2);
        }
    }
}
