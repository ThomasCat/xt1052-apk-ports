package f;

import android.util.Log;
import com.nuance.dragon.toolkit.audio.SpeechDetectionListener;

class o implements SpeechDetectionListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ i f2250a;

    o(i iVar) {
        this.f2250a = iVar;
    }

    public void onEndOfSpeech() {
        if (i.f2237b) {
            Log.d(i.f2236a, "End of speech detected.");
        }
        if (this.f2250a.m != null) {
            this.f2250a.m.b();
        }
    }

    public void onStartOfSpeech() {
        if (i.f2237b) {
            Log.d(i.f2236a, "Start of speech detected.");
        }
        if (this.f2250a.m != null) {
            this.f2250a.m.a();
        }
    }
}
