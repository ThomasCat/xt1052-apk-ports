package com.motorola.audiomonitor.uis;

import android.animation.TimeAnimator;
import android.animation.TimeAnimator.TimeListener;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.motorola.audiomonitor.C0014R;
import h.k;
import h.m;

public class AOVMicFragment extends Fragment {

    /* renamed from: a reason: collision with root package name */
    public static int f603a = 0;

    /* renamed from: b reason: collision with root package name */
    public static int f604b = 1;

    /* renamed from: c reason: collision with root package name */
    public static int f605c = 2;

    /* renamed from: d reason: collision with root package name */
    public static int f606d = 3;

    /* renamed from: e reason: collision with root package name */
    public static int f607e = 4;

    /* renamed from: f reason: collision with root package name */
    public static int f608f = 5;

    /* renamed from: g reason: collision with root package name */
    public static int f609g = 6;

    /* renamed from: h reason: collision with root package name */
    public static int f610h = 7;
    public static int i = 8;
    /* access modifiers changed from: private */
    public static final int[] j = {1200, 600, 300, 150, 100};
    /* access modifiers changed from: private */
    public static final int[] k = {170, 150, 120, 60, 20};
    /* access modifiers changed from: private */
    public static final int[] l = {180, 160, 130, 70, 30};
    private ProgressBar A;
    private View B;
    /* access modifiers changed from: private */
    public int C = 0;
    /* access modifiers changed from: private */
    public int D = 0;
    /* access modifiers changed from: private */
    public int E = 0;
    /* access modifiers changed from: private */
    public int F = 0;
    /* access modifiers changed from: private */
    public int G = 0;
    /* access modifiers changed from: private */
    public LayoutParams H;
    /* access modifiers changed from: private */
    public LayoutParams I;
    private TimeAnimator J;
    private boolean K = false;
    private final TimeListener L = new l(this);
    /* access modifiers changed from: private */
    public ImageView m;
    /* access modifiers changed from: private */
    public ImageView n;
    /* access modifiers changed from: private */
    public ImageView o;
    private ImageView p;
    private View q;
    /* access modifiers changed from: private */
    public View r;
    private View s;
    private ImageView t;
    private TextView u;
    private TextView v;
    private TextView w;
    private TextView x;
    private FrameLayout y;
    private ProgressBar z;

    static /* synthetic */ int c(AOVMicFragment aOVMicFragment, int i2) {
        int i3 = aOVMicFragment.D - i2;
        aOVMicFragment.D = i3;
        return i3;
    }

    static /* synthetic */ int d(AOVMicFragment aOVMicFragment, int i2) {
        int i3 = aOVMicFragment.C - i2;
        aOVMicFragment.C = i3;
        return i3;
    }

    private void d() {
        if (this.J != null && this.J.isRunning()) {
            this.J.cancel();
        }
        this.s.setVisibility(0);
        this.q.setVisibility(8);
        this.t.setImageResource(C0014R.drawable.ic_vc_cancel);
    }

    private void e() {
        if (this.J != null && this.J.isRunning()) {
            this.J.cancel();
        }
        this.s.setVisibility(0);
        this.q.setVisibility(8);
        this.t.setImageResource(C0014R.drawable.ic_vc_warning);
    }

    public void a(int i2) {
        if (!this.K) {
            this.A.setVisibility(8);
            if (i2 == i) {
                this.y.setVisibility(0);
                this.u.setVisibility(0);
                this.v.setVisibility(8);
                this.u.setClickable(true);
                this.u.setText(k.a((Context) getActivity(), (int) C0014R.string.say_call_to_confirm, new String[]{getResources().getString(C0014R.string.call_word)}, (int) C0014R.color.Active_Button, m.Normal));
            } else if (i2 == f610h) {
                this.y.setVisibility(0);
                this.u.setVisibility(0);
                this.v.setVisibility(8);
                this.u.setClickable(true);
                this.u.setText(k.a((Context) getActivity(), (int) C0014R.string.say_call_after_name, new String[]{getResources().getString(C0014R.string.call_word)}, (int) C0014R.color.Active_Button, m.Normal));
            } else if (i2 == f603a) {
                this.y.setVisibility(0);
                this.u.setText(k.a((Context) getActivity(), (int) C0014R.string.speak_yes_text, new String[]{getResources().getString(C0014R.string.dialog_yes)}, (int) C0014R.color.Active_Button, m.Normal));
                this.w.setText(k.a((Context) getActivity(), (int) C0014R.string.dialog_no, new String[]{getResources().getString(C0014R.string.dialog_no)}, (int) C0014R.color.Active_Button, m.Normal));
                this.v.setVisibility(0);
                this.w.setVisibility(0);
                this.x.setVisibility(8);
                d();
            } else if (i2 == f604b) {
                this.y.setVisibility(8);
                this.x.setVisibility(0);
                this.x.setText(k.a((Context) getActivity(), (int) C0014R.string.cancel_to_stop, new String[]{getResources().getString(C0014R.string.highlight_cancel)}, (int) C0014R.color.Active_Button, m.Normal));
                d();
            } else if (i2 == f605c) {
                this.y.setVisibility(8);
                this.x.setVisibility(8);
                this.r.setVisibility(8);
                this.A.setVisibility(0);
            } else if (i2 == f608f) {
                this.y.setVisibility(8);
                this.x.setVisibility(8);
                e();
            } else if (i2 == f609g) {
                this.y.setVisibility(8);
                this.x.setVisibility(8);
                this.n.setVisibility(8);
                this.o.setVisibility(8);
                this.p.setVisibility(8);
                this.A.setVisibility(8);
                e();
            } else if (i2 == f607e) {
                this.m.setImageResource(C0014R.drawable.vc_micbtn_rec);
            }
        }
    }

    public void b(int i2) {
        if (!this.K && i2 > this.G) {
            this.G = i2;
        }
    }

    public void c(int i2) {
        if (!this.K) {
            this.z.setProgress(i2);
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.B = layoutInflater.inflate(C0014R.layout.aov_mic_view, null);
        this.u = (TextView) this.B.findViewById(C0014R.id.speak_command_yes_prompt);
        this.v = (TextView) this.B.findViewById(C0014R.id.speak_command_or_prompt);
        this.w = (TextView) this.B.findViewById(C0014R.id.speak_command_no_prompt);
        this.y = (FrameLayout) this.B.findViewById(C0014R.id.speak_yesno_framelayout);
        this.A = (ProgressBar) this.B.findViewById(C0014R.id.vc_progress);
        this.y.setVisibility(0);
        this.u.setVisibility(8);
        this.v.setVisibility(0);
        this.w.setVisibility(8);
        this.v.setText(k.a((Context) getActivity(), (int) C0014R.string.hint_text, new String[]{getResources().getString(C0014R.string.highlight_help)}, (int) C0014R.color.UDTHighlightColor, m.Normal));
        this.u.setOnClickListener(new m(this));
        this.u.setClickable(false);
        this.x = (TextView) this.B.findViewById(C0014R.id.speak_cancel_action_prompt);
        this.w.setOnClickListener(new n(this));
        this.x.setOnClickListener(new o(this));
        this.x.setVisibility(8);
        this.m = (ImageView) this.B.findViewById(C0014R.id.voiceicon);
        this.n = (ImageView) this.B.findViewById(C0014R.id.mic_shadow);
        this.o = (ImageView) this.B.findViewById(C0014R.id.mic_reactive);
        this.p = (ImageView) this.B.findViewById(C0014R.id.levels_guideline);
        this.q = this.B.findViewById(C0014R.id.speaker_frame);
        this.r = this.B.findViewById(C0014R.id.mic_frame);
        this.s = this.B.findViewById(C0014R.id.progress_frame);
        this.z = (ProgressBar) this.B.findViewById(C0014R.id.progress);
        this.t = (ImageView) this.B.findViewById(C0014R.id.progress_cancel);
        this.z.setMax(100);
        this.m.setOnClickListener(new p(this));
        this.z.setOnClickListener(new q(this));
        this.r.addOnLayoutChangeListener(new r(this));
        this.J = new TimeAnimator();
        this.J.setTimeListener(this.L);
        this.J.start();
        return this.B;
    }

    public void onDestroyView() {
        this.K = true;
        if (this.J != null) {
            this.J.removeAllListeners();
            if (this.J.isRunning()) {
                this.J.cancel();
            }
            this.J = null;
        }
        this.m = null;
        this.n = null;
        this.o = null;
        this.r = null;
        this.u = null;
        this.v = null;
        this.w = null;
        this.x = null;
        this.y = null;
        this.A = null;
        this.B = null;
        super.onDestroyView();
    }
}
