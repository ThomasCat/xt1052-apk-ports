package a;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.util.Log;
import b.b;
import c.d;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.actions.utils.ac;
import com.motorola.audiomonitor.actions.utils.ad;
import com.motorola.audiomonitor.actions.utils.ae;
import com.motorola.audiomonitor.actions.utils.h;
import com.motorola.audiomonitor.actions.utils.j;
import com.motorola.audiomonitor.actions.utils.m;
import com.motorola.audiomonitor.uis.am;
import java.util.Arrays;

public class ag extends r {

    /* renamed from: a reason: collision with root package name */
    private static final boolean f15a = a.f160b;

    /* renamed from: b reason: collision with root package name */
    private static final String f16b = ("AMonitor." + ag.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final j f17c = new ah(this);

    ag(int i, Context context) {
        super(i, context);
    }

    public o a(b bVar, e eVar) {
        d.a(c(), "num_voicemail");
        TelephonyManager telephonyManager = (TelephonyManager) c().getSystemService("phone");
        Resources resources = c().getResources();
        String voiceMailNumber = telephonyManager.getVoiceMailNumber();
        StringBuilder sb = new StringBuilder();
        if (f15a) {
            Log.d(f16b, "voicenumber is " + voiceMailNumber);
        }
        if (voiceMailNumber == null || voiceMailNumber.isEmpty()) {
            if (eVar.a(ac.DISMISS_BY_EOS, 0, null, Arrays.asList(new String[]{c().getResources().getString(C0014R.string.voicemail_action_handler_no_voicemail_account)})) == ad.RESPONSE_POSITIVE) {
                if (m.a().a(true, eVar)) {
                    return o.ABORTED;
                }
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setAction("com.android.phone.CallFeaturesSetting.ADD_VOICEMAIL");
                intent.addFlags(268435456);
                c().startActivity(intent);
            }
        } else {
            sb.append(resources.getString(C0014R.string.voicemail_action_handler_call_confirmation));
            am amVar = new am(c(), eVar, 3000, true, true);
            ad a2 = eVar.a(ac.EOS_DISMISS_BY_CONFIRM_CALL, 4000, amVar, Arrays.asList(new String[]{sb.toString()}));
            if (a2 == ad.RESPONSE_POSITIVE || a2 == ad.RESPONSE_TIMEOUT) {
                if (m.a().a(true, eVar)) {
                    return o.ABORTED;
                }
                if (h.a().d() || h.a().e()) {
                    if (f15a) {
                        Log.d(f16b, "phone state is not idle so do not call");
                    }
                    return o.CONSUMED;
                }
                ae aeVar = new ae(c());
                aeVar.a();
                aeVar.b();
                aeVar.a((int) C0014R.string.call_action_handler_call_confirmation);
                h.a().a(this.f17c);
                Intent intent2 = new Intent("android.intent.action.CALL");
                intent2.addFlags(268435456);
                intent2.setData(Uri.fromParts("tel", voiceMailNumber, null));
                c().startActivity(intent2);
            }
        }
        return o.CONSUMED;
    }
}
