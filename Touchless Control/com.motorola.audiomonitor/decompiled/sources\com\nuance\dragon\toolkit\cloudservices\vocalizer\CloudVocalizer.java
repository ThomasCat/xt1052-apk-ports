package com.nuance.dragon.toolkit.cloudservices.vocalizer;

import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.bluetooth.impl.AndroidVersion;
import com.nuance.dragon.toolkit.audio.pipes.SimplePipe;
import com.nuance.dragon.toolkit.cloudservices.CloudServices;
import com.nuance.dragon.toolkit.cloudservices.TTSParam;
import com.nuance.dragon.toolkit.cloudservices.Transaction;
import com.nuance.dragon.toolkit.cloudservices.TransactionError;
import com.nuance.dragon.toolkit.cloudservices.TransactionResult;
import com.nuance.dragon.toolkit.util.internal.d;

public class CloudVocalizer {

    /* renamed from: a reason: collision with root package name */
    private final CloudServices f1500a;

    /* renamed from: b reason: collision with root package name */
    private SimplePipe f1501b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public a f1502c;

    public interface Listener {
        void onError(TransactionError transactionError);

        void onSuccess();
    }

    final class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public Transaction f1506a;

        private a() {
        }

        /* synthetic */ a(byte b2) {
            this();
        }
    }

    public CloudVocalizer(CloudServices cloudServices) {
        d.a("cloudServices", (Object) cloudServices);
        this.f1500a = cloudServices;
    }

    public void cancel() {
        if (this.f1501b != null) {
            this.f1501b.disconnectAudioSource();
            this.f1501b = null;
        }
        if (this.f1502c != null) {
            Transaction a2 = this.f1502c.f1506a;
            this.f1502c = null;
            a2.cancel();
        }
    }

    public AudioSource generateTts(TtsSpec ttsSpec, final Listener listener) {
        d.a("ttsSpec", (Object) ttsSpec);
        cancel();
        final a aVar = new a(0);
        this.f1502c = aVar;
        aVar.f1506a = new Transaction(ttsSpec.getCommand(), ttsSpec.getSettings(), new com.nuance.dragon.toolkit.cloudservices.Transaction.Listener() {
            public final void onTransactionError(Transaction transaction, TransactionError transactionError) {
                if (aVar == CloudVocalizer.this.f1502c) {
                    CloudVocalizer.this.f1502c = null;
                }
                if (listener != null) {
                    listener.onError(transactionError);
                }
            }

            public final void onTransactionResult(Transaction transaction, TransactionResult transactionResult, boolean z) {
                if (aVar == CloudVocalizer.this.f1502c && transactionResult.isFinal()) {
                    CloudVocalizer.this.f1502c = null;
                }
                if (listener != null) {
                    listener.onSuccess();
                }
            }

            public final void onTransactionStarted(Transaction transaction) {
            }
        }, AndroidVersion.CUR_DEVELOPMENT);
        TTSParam tTSParam = new TTSParam(ttsSpec.getTtsParamName(), ttsSpec.getTtsParamData(), ttsSpec.getAudioType());
        aVar.f1506a.addParam(tTSParam);
        aVar.f1506a.finish();
        this.f1501b = new SimplePipe();
        this.f1501b.connectAudioSource(tTSParam.getAudioSource());
        this.f1500a.addTransaction(aVar.f1506a, 5);
        return this.f1501b;
    }
}
