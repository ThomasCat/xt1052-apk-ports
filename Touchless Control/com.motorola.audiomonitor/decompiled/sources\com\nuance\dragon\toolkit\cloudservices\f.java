package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.a.a.a.b.c.c.j;
import com.nuance.dragon.toolkit.cloudservices.TransactionError.ConnectionErrorCodes;

abstract class f {

    /* renamed from: a reason: collision with root package name */
    protected final d f1464a;

    /* renamed from: b reason: collision with root package name */
    protected final Transaction f1465b;

    /* renamed from: c reason: collision with root package name */
    private final boolean f1466c;

    /* renamed from: d reason: collision with root package name */
    private final boolean f1467d;

    /* renamed from: e reason: collision with root package name */
    private boolean f1468e;

    f(d dVar, boolean z, boolean z2, Transaction transaction) {
        this.f1465b = transaction;
        this.f1466c = z;
        this.f1467d = z2;
        this.f1464a = dVar;
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        this.f1468e = true;
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i, String str) {
        a(new TransactionError(this.f1465b, 3, str, null, null, i));
    }

    /* access modifiers changed from: 0000 */
    public void a(j jVar) {
    }

    /* access modifiers changed from: 0000 */
    public void a(Param param) {
    }

    /* access modifiers changed from: 0000 */
    public final void a(TransactionError transactionError) {
        if (this.f1466c) {
            a((f) new g(this.f1464a, this.f1465b));
            this.f1465b.f().onTransactionError(this.f1465b, transactionError);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(d dVar) {
    }

    /* access modifiers changed from: protected */
    public final void a(f fVar) {
        this.f1465b.a(fVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(String str, String str2) {
        a(new TransactionError(this.f1465b, 2, null, str2, str, 0));
    }

    /* access modifiers changed from: 0000 */
    public final void a(short s) {
        a(new TransactionError(this.f1465b, (int) s));
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        this.f1468e = false;
    }

    /* access modifiers changed from: 0000 */
    public final boolean c() {
        return this.f1466c;
    }

    /* access modifiers changed from: 0000 */
    public final boolean d() {
        return this.f1467d;
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        a((f) new g(this.f1464a, this.f1465b));
        this.f1465b.f().onTransactionError(this.f1465b, new TransactionError(this.f1465b, 0));
    }

    /* access modifiers changed from: 0000 */
    public void f() {
    }

    /* access modifiers changed from: 0000 */
    public final void g() {
        a(new TransactionError(this.f1465b, (int) ConnectionErrorCodes.NETWORK_UNAVAILABLE));
    }
}
