package com.nuance.dragon.toolkit.cloudservices;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import com.nuance.a.a.a.a.a.a.C0000a;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;

final class b {

    /* renamed from: a reason: collision with root package name */
    private final String f1390a;

    /* renamed from: b reason: collision with root package name */
    private final byte[] f1391b;

    /* renamed from: c reason: collision with root package name */
    private final String f1392c;

    /* renamed from: d reason: collision with root package name */
    private final short f1393d;

    /* renamed from: e reason: collision with root package name */
    private final SSLConfig f1394e;

    /* renamed from: f reason: collision with root package name */
    private final Context f1395f;

    /* renamed from: g reason: collision with root package name */
    private final String f1396g;

    /* renamed from: h reason: collision with root package name */
    private final AudioType f1397h;
    private final AudioType i;

    public b(Context context, String str, int i2, SSLConfig sSLConfig, String str2, byte[] bArr, AudioType audioType, AudioType audioType2) {
        this.f1395f = context;
        this.f1392c = str;
        this.f1393d = (short) i2;
        this.f1394e = sSLConfig;
        this.f1390a = str2;
        this.f1391b = bArr;
        String a2 = new com.nuance.a.a.a.c.b(this.f1395f).a();
        if (a2 != null && a2.length() == 0) {
            a2 = null;
        }
        this.f1396g = a2;
        this.f1397h = audioType;
        this.i = audioType2;
    }

    private static C0000a a(AudioType audioType) {
        if (audioType.encoding == Encoding.PCM_16) {
            if (audioType.frequency == 8000) {
                return C0000a.A;
            }
            if (audioType.frequency == 16000) {
                return C0000a.C;
            }
        } else if (audioType.encoding == Encoding.SPEEX) {
            if (audioType.frequency == 8000) {
                return C0000a.x;
            }
            if (audioType.frequency == 16000) {
                return C0000a.y;
            }
        }
        return null;
    }

    public final String a() {
        return this.f1390a;
    }

    public final byte[] b() {
        return this.f1391b;
    }

    public final String c() {
        return this.f1392c;
    }

    public final short d() {
        return this.f1393d;
    }

    public final boolean e() {
        return this.f1394e != null;
    }

    public final SSLConfig f() {
        return this.f1394e;
    }

    public final String g() {
        String networkOperatorName = ((TelephonyManager) this.f1395f.getSystemService("phone")).getNetworkOperatorName();
        return (networkOperatorName == null || networkOperatorName.length() == 0) ? "carrier" : networkOperatorName;
    }

    public final String h() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.f1395f.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            String typeName = activeNetworkInfo.getTypeName();
            if (typeName != null && typeName.length() > 0) {
                return typeName;
            }
        }
        return "unknown";
    }

    public final String i() {
        return this.f1396g == null ? "unknown" : this.f1396g;
    }

    public final C0000a j() {
        return a(this.f1397h);
    }

    public final C0000a k() {
        return a(this.i);
    }

    public final AudioType l() {
        return this.i;
    }
}
