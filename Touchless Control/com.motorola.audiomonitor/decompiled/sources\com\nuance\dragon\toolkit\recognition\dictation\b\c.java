package com.nuance.dragon.toolkit.recognition.dictation.b;

import com.nuance.dragon.toolkit.recognition.a.a.C0010a;
import com.nuance.dragon.toolkit.recognition.a.a.C0010a.C0011a;
import com.nuance.dragon.toolkit.recognition.a.b;
import com.nuance.dragon.toolkit.recognition.dictation.DictationResult;
import com.nuance.dragon.toolkit.recognition.dictation.Sentence;
import com.nuance.dragon.toolkit.recognition.dictation.Token;
import com.nuance.dragon.toolkit.recognition.dictation.a.f;
import com.nuance.dragon.toolkit.recognition.dictation.a.g;
import com.nuance.dragon.toolkit.recognition.dictation.a.h;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public final class c extends DefaultHandler implements com.nuance.dragon.toolkit.recognition.dictation.b.f.a {

    /* renamed from: a reason: collision with root package name */
    private Stack f1973a = new Stack();

    /* renamed from: b reason: collision with root package name */
    private ArrayList f1974b = new ArrayList();

    /* renamed from: c reason: collision with root package name */
    private Map f1975c = new HashMap();

    /* renamed from: d reason: collision with root package name */
    private g f1976d = null;

    /* renamed from: e reason: collision with root package name */
    private g f1977e = null;

    /* renamed from: f reason: collision with root package name */
    private g f1978f = null;

    /* renamed from: g reason: collision with root package name */
    private double f1979g = 0.0d;

    /* renamed from: h reason: collision with root package name */
    private HashMap f1980h = new HashMap();
    private List i = new ArrayList();
    private b j = null;
    private g k = null;
    private String l = null;
    private String m = null;
    private String n = null;
    private String o = null;
    private double p = 0.0d;

    final class a {

        /* renamed from: a reason: collision with root package name */
        Token f1981a;

        a(Token token) {
            this.f1981a = token;
        }
    }

    private static void a(Sentence sentence, String str) {
        if (sentence.size() == 0) {
            throw new SAXException("sentence is empty in setWord!");
        }
        ((h) sentence.tokenAt(sentence.size() - 1)).a(str);
    }

    private static void a(Sentence sentence, Attributes attributes) {
        if (attributes.getLength() < 3) {
            throw new SAXException("sentences is empty or attributes is missing in createWord!");
        }
        g gVar = (g) sentence;
        gVar.a(new h("", Long.parseLong(attributes.getValue("start")), Long.parseLong(attributes.getValue("end")), Double.parseDouble(attributes.getValue("confidence"))));
    }

    private void c() {
        int i2;
        String str;
        int i3 = 0;
        if (this.m != null) {
            g d2 = this.j.d();
            ArrayList arrayList = new ArrayList();
            for (int i4 = 0; i4 < d2.size(); i4++) {
                Token token = d2.tokenAt(i4);
                int length = token.toString().split(" ").length;
                int i5 = 0;
                boolean z = true;
                while (i5 < length) {
                    arrayList.add(new a(z ? token : null));
                    i5++;
                    z = false;
                }
            }
            String[] split = this.m.split(" ");
            int length2 = split.length;
            int i6 = 0;
            String str2 = null;
            while (i6 < length2) {
                String str3 = split[i6];
                if (str3.startsWith("[") && str3.endsWith("]") && !str3.startsWith("[/")) {
                    int i7 = i3;
                    str = str3.substring(1, str3.length() - 1);
                    i2 = i7;
                } else if (!str3.startsWith("[/") || !str3.endsWith("]")) {
                    if (!str3.equals("*")) {
                        Token token2 = i3 < arrayList.size() ? ((a) arrayList.get(i3)).f1981a : null;
                        if (token2 != null) {
                            this.j.a(new C0010a(C0011a.TOKEN, "", token2));
                        }
                    }
                    i2 = i3 + 1;
                    str = str2;
                } else {
                    f fVar = new f((ArrayList) this.f1980h.get(str2), null);
                    this.j.a(new C0010a(C0011a.DICTATION_RESULT, str2, (DictationResult) fVar));
                    this.j.a(str2, fVar);
                    i2 = i3;
                    str = null;
                }
                i6++;
                str2 = str;
                i3 = i2;
            }
        } else {
            g d3 = this.j.d();
            if (d3 != null) {
                while (i3 < d3.size()) {
                    this.j.a(new C0010a(C0011a.TOKEN, "", d3.tokenAt(i3)));
                    i3++;
                }
            }
        }
        if (this.j.d() != null) {
            this.i.add(this.j);
        }
        this.j = null;
    }

    private int d() {
        int i2 = 0;
        for (int i3 = 0; i3 < this.f1973a.size(); i3++) {
            if (((String) this.f1973a.get(i3)).equals("result")) {
                i2 = 1;
            } else if (i2 != 1 || !((String) this.f1973a.get(i3)).equals("NSS_Audio_Statistics")) {
                if (i2 == 1 && ((String) this.f1973a.get(i3)).equals("interpretation")) {
                    i2 = 3;
                } else if (i2 == 3 && ((String) this.f1973a.get(i3)).equals("SWI_literalTimings")) {
                    i2 = 4;
                } else if (i2 == 3 && ((String) this.f1973a.get(i3)).equals("alternatives")) {
                    i2 = 4;
                } else if (i2 == 3 && ((String) this.f1973a.get(i3)).equals("SWI_meaning_fillers")) {
                    i2 = 16;
                } else if (i2 == 16 && ((String) this.f1973a.get(i3)).equals("slot")) {
                    i2 = 17;
                } else if (i2 == 4 && ((String) this.f1973a.get(i3)).equals("word")) {
                    i2 = 7;
                } else if ((i2 == 4 || i2 == 16) && ((String) this.f1973a.get(i3)).equals("input_postItn")) {
                    i2 = 5;
                } else if (i2 == 5 && ((String) this.f1973a.get(i3)).equals("word")) {
                    i2 = 8;
                } else if ((i2 == 4 || i2 == 16) && ((String) this.f1973a.get(i3)).equals("input_preItn")) {
                    i2 = 6;
                } else if (i2 == 6 && ((String) this.f1973a.get(i3)).equals("word")) {
                    i2 = 9;
                } else if (i2 == 17 && ((String) this.f1973a.get(i3)).equals("word")) {
                    i2 = 18;
                } else if (i2 == 3 && ((String) this.f1973a.get(i3)).equals("SLOT_TYPE")) {
                    i2 = 19;
                } else if (i2 == 3 && ((String) this.f1973a.get(i3)).equals("details")) {
                    i2 = 20;
                } else if (i2 == 20 && ((String) this.f1973a.get(i3)).equals("fill")) {
                    i2 = 21;
                }
            } else if (i3 == this.f1973a.size() - 2) {
                return 2;
            }
        }
        return i2;
    }

    public final f a() {
        return new f(this.f1974b, this.f1975c);
    }

    public final com.nuance.dragon.toolkit.recognition.a.c b() {
        if (this.i == null || this.i.size() <= 0) {
            return null;
        }
        return new com.nuance.dragon.toolkit.recognition.a.c(this.i, this.f1975c);
    }

    public final void characters(char[] cArr, int i2, int i3) {
        new StringBuilder("Received characters: ").append(new String(cArr, i2, i3));
        StringBuffer stringBuffer = new StringBuffer(i3);
        stringBuffer.append(cArr, i2, i3);
        int d2 = d();
        if (d2 == 2) {
            String str = (String) this.f1973a.peek();
            if (str.equals("InputAudioLength")) {
                this.f1975c.put("IAL", new String(stringBuffer));
            }
            this.f1975c.put(str, new String(stringBuffer));
        } else if (d2 == 7) {
            a((Sentence) this.f1976d, new String(stringBuffer));
        } else if (d2 == 8) {
            a((Sentence) this.f1977e, new String(stringBuffer));
        } else if (d2 == 9) {
            a((Sentence) this.f1978f, new String(stringBuffer));
        } else if (d2 == 18) {
            if (this.k != null) {
                a((Sentence) this.k, new String(stringBuffer));
            }
        } else if (d2 == 19) {
            this.j.a(new String(stringBuffer));
        } else if (d2 == 21) {
            this.m = new String(stringBuffer);
            this.o = new String(stringBuffer);
        } else if (d2 == 20) {
            this.o = new String(stringBuffer);
        }
    }

    public final void endElement(String str, String str2, String str3) {
        ArrayList arrayList;
        new StringBuilder("Received endElement ").append(str2);
        String str4 = (String) this.f1973a.pop();
        if (str4.equals("input_postItn")) {
            str4 = "input";
        } else if (str4.equals("input_preItn")) {
            str4 = "input";
        }
        if (!str4.equals(str2)) {
            this.f1974b = null;
            this.f1975c = null;
            return;
        }
        if (str2.equals("slot")) {
            if (!(this.l == null || this.k == null)) {
                if (!this.f1980h.containsKey(this.l)) {
                    arrayList = new ArrayList();
                    this.f1980h.put(this.l, arrayList);
                } else {
                    arrayList = (ArrayList) this.f1980h.get(this.l);
                }
                arrayList.add(this.k);
            }
            this.l = null;
            this.k = null;
            this.f1979g = 0.0d;
        }
        if (str2.equals("interpretation")) {
            c();
        }
        if (str2.equals("alternatives")) {
            g gVar = this.f1977e != null ? this.f1977e : this.f1976d != null ? this.f1976d : this.f1978f != null ? this.f1978f : null;
            this.j.a(gVar);
            this.f1974b.add(gVar);
            this.f1976d = null;
            this.f1977e = null;
            this.f1978f = null;
            this.f1979g = 0.0d;
        }
        if (d() == 20) {
            this.j.a(this.n, this.o, this.p);
        }
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        String str4;
        new StringBuilder("Received startElement ").append(str2);
        if (str2.equals("result")) {
            if (this.f1973a.size() != 0) {
                throw new SAXException("StartElement> Found result but it is not the first token.");
            }
            str4 = str2;
        } else if (str2.equals("interpretation")) {
            if (attributes.getLength() > 0 && attributes.getValue("confidence") != null) {
                this.f1979g = Double.parseDouble(attributes.getValue("confidence"));
            }
            this.j = new b(this.f1979g);
            str4 = str2;
        } else if (str2.equals("SWI_literalTimings")) {
            this.f1976d = new g();
            this.f1976d.a(this.f1979g);
            str4 = str2;
        } else {
            if (str2.equals("input")) {
                if (attributes.getLength() > 0 && attributes.getValue("type") != null) {
                    if (attributes.getValue("type").equals("post-itn")) {
                        str4 = "input_postItn";
                        this.f1977e = new g();
                        this.f1977e.a(this.f1979g);
                    } else if (attributes.getValue("type").equals("post-itn")) {
                        str4 = "input_postItn";
                        this.f1978f = new g();
                        this.f1978f.a(this.f1979g);
                    } else {
                        str4 = str2;
                    }
                    if (this.k != null) {
                        this.k.a(Double.parseDouble(attributes.getValue("confidence")));
                    }
                }
            } else if (str2.equals("slot")) {
                int length = attributes.getLength();
                String value = attributes.getValue("name");
                if (length > 0 && value != null && !value.equals("")) {
                    this.l = value;
                    this.k = new g();
                }
                str4 = str2;
            } else if (str2.equals("word")) {
                if (d() == 4) {
                    a((Sentence) this.f1976d, attributes);
                    str4 = str2;
                } else if (d() == 5) {
                    a((Sentence) this.f1977e, attributes);
                    str4 = str2;
                } else if (d() == 6) {
                    a((Sentence) this.f1978f, attributes);
                    str4 = str2;
                } else if (d() == 17) {
                    a((Sentence) this.k, attributes);
                }
            }
            str4 = str2;
        }
        if (d() == 20) {
            this.n = str2;
            if (attributes.getLength() > 0 && attributes.getValue("confidence") != null) {
                this.p = Double.parseDouble(attributes.getValue("confidence"));
            }
        }
        this.f1973a.push(str4);
    }
}
