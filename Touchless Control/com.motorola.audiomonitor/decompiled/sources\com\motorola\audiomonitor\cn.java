package com.motorola.audiomonitor;

import android.content.Context;
import android.media.AudioManager;
import android.os.DropBoxManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import c.e;
import com.motorola.audiomonitor.service.d;

public class cn extends cm {

    /* renamed from: a reason: collision with root package name */
    private static final String f422a = ("AMonitor." + cn.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f423b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private final Context f424c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public final String f425d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public boolean f426e = false;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public boolean f427f = false;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public final cl f428g;

    /* renamed from: h reason: collision with root package name */
    private final d f429h;
    /* access modifiers changed from: private */
    public cb i;
    private final StringBuilder j = new StringBuilder();
    /* access modifiers changed from: private */
    public final e k = new e();
    /* access modifiers changed from: private */
    public final h.e l;

    public cn(Context context, cl clVar, d dVar, String str) {
        if (clVar == null) {
            throw new IllegalArgumentException("Invalid callback");
        }
        this.f424c = context;
        this.f425d = str;
        this.f428g = clVar;
        this.f429h = dVar;
        this.l = new h.e(context, new co(this));
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        if (this.k.f125d.length() > 0) {
            StringBuilder sb = new StringBuilder();
            e eVar = this.k;
            eVar.f125d = sb.append(eVar.f125d).append(" ").toString();
        }
        StringBuilder sb2 = new StringBuilder();
        e eVar2 = this.k;
        eVar2.f125d = sb2.append(eVar2.f125d).append(Integer.toString(i2)).toString();
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        this.j.append(str);
        this.j.append("\n");
    }

    private void b(String str) {
        if (this.f429h != null) {
            int c2 = this.f429h.c(1);
            short[] d2 = this.f429h.d(1);
            if (d2 != null && d2.length >= 3) {
                short s = d2[0];
                short s2 = d2[1];
                a(str + ": level=" + c2 + ", noiseDb=" + s + ", noiseType=" + s2 + ", noiseSnr=" + d2[2]);
            }
            if (((float) c2) > this.k.f126e) {
                this.k.f126e = (float) c2;
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        if (f423b) {
            Log.d(f422a, "muteAudioStreams: state=" + z);
        }
        AudioManager audioManager = (AudioManager) this.f424c.getSystemService("audio");
        for (int streamMute : new int[]{4, 3, 5, 2, 1}) {
            audioManager.setStreamMute(streamMute, z);
        }
    }

    private void e() {
        if (this.i != null) {
            this.i.b();
            this.i.c();
            synchronized (this) {
                this.i = null;
            }
        }
    }

    /* access modifiers changed from: private */
    public int f() {
        AudioManager audioManager = (AudioManager) this.f424c.getSystemService("audio");
        if (audioManager.isWiredHeadsetOn()) {
            a("ERROR: Headset present during enrollment");
            return 25;
        } else if (audioManager.isMusicActive()) {
            return 29;
        } else {
            TelephonyManager telephonyManager = (TelephonyManager) this.f424c.getSystemService("phone");
            if (telephonyManager.getCallState() == 2 || telephonyManager.getCallState() == 1) {
                return 30;
            }
            return b.c() ? 31 : 0;
        }
    }

    public synchronized void a() {
        a("enroll complete");
        if (this.i != null) {
            this.i.b();
            this.f428g.a(3, 0);
            new cp(this).start();
        }
    }

    public synchronized void a(p pVar) {
        b("enroll");
        cb cbVar = this.i;
        if (this.i == null || !(this.i instanceof cq)) {
            this.f426e = false;
            this.f427f = false;
            this.i = new cq(this, pVar, this.f429h);
        } else {
            this.i = new cq(this, (cq) this.i);
        }
        if (cbVar != null) {
            cbVar.c();
        }
        this.i.a();
    }

    public void a(boolean z) {
        b("cancel: terminate=" + z);
        if (z) {
            c();
        } else if (this.i != null) {
            this.i.b();
        }
    }

    public synchronized boolean a(ProtectedByteBuffer protectedByteBuffer) {
        return this.i != null ? this.i.a(protectedByteBuffer) : true;
    }

    public synchronized void b(p pVar) {
        b("verify");
        if (!this.f426e) {
            a("ERROR: verify not ready");
            a(19);
            this.f428g.a(9, 19);
        } else {
            if (this.i != null) {
                this.i.c();
            }
            this.i = new cr(this, pVar, this.f429h);
            this.i.start();
        }
    }

    public boolean b() {
        return this.f426e;
    }

    public void c() {
        if (f423b && this.j.length() > 0) {
            Log.d(f422a, "Enrollment log:\n" + this.j);
            ((DropBoxManager) this.f424c.getSystemService("dropbox")).addText("AOV_REPORT", "Subtype: enrollment\nAP version=" + a.p + ", DSP version=" + (this.f429h != null ? this.f429h.k() : "unknown") + "\n" + this.j);
        }
        this.k.a(this.f424c);
        e();
        this.f427f = true;
    }
}
