package com.nuance.dragon.toolkit.vocalizer;

import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.d;
import com.nuance.dragon.toolkit.vocalizer.NativeVocalizer.SpeakListener;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.Frequencies;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.LoadListener;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.ReleaseListener;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.ResourceLoadListener;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.ResourceReleaseListener;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.TtsListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

abstract class b extends Vocalizer {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final NativeVocalizer f2157a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final LinkedList f2158b = new LinkedList();
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public TtsAudioSource f2159c;

    /* renamed from: d reason: collision with root package name */
    private int f2160d = 100;

    /* renamed from: e reason: collision with root package name */
    private int f2161e = 0;

    /* renamed from: f reason: collision with root package name */
    private int f2162f = 8;

    /* renamed from: g reason: collision with root package name */
    private int f2163g = 400;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public AudioType f2164h = AudioType.UNKNOWN;
    private boolean i;
    private boolean j = false;
    /* access modifiers changed from: private */
    public final Handler k = new Handler();
    private final Handler l;

    b(NativeVocalizer nativeVocalizer, Handler handler) {
        this.f2157a = nativeVocalizer;
        if (handler == null) {
            this.l = getWorkerThreadHandler();
        } else {
            this.l = handler;
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        if (!this.f2158b.isEmpty()) {
            final TtsAudioSource ttsAudioSource = (TtsAudioSource) this.f2158b.removeFirst();
            this.f2159c = ttsAudioSource;
            new StringBuilder("Generating TTS for '").append(ttsAudioSource.f2142a).append("'");
            ttsAudioSource.b();
            TtsAudioSource ttsAudioSource2 = this.f2159c;
            this.l.post(new Runnable() {
                public final void run() {
                    if (!b.this.f2157a.a(ttsAudioSource.f2142a, ttsAudioSource.f2146e, ttsAudioSource.f2147f, ttsAudioSource.f2143b, new SpeakListener() {
                        public final void onNewChunk(final AudioChunk audioChunk, final boolean z) {
                            b.this.k.post(new Runnable() {
                                public final void run() {
                                    if (ttsAudioSource == b.this.f2159c) {
                                        if (ttsAudioSource.a()) {
                                            Logger.error(b.this, "TTS job was completed, but still in queue!");
                                            b.this.f2159c = null;
                                            return;
                                        }
                                        if (audioChunk != null) {
                                            ttsAudioSource.a(audioChunk);
                                        }
                                        if (z) {
                                            b bVar = b.this;
                                            new StringBuilder("Finished generating TTS for '").append(ttsAudioSource.f2142a).append("'");
                                            ttsAudioSource.a(true);
                                        }
                                    }
                                }
                            });
                        }
                    })) {
                        b.this.k.post(new Runnable() {
                            public final void run() {
                                ttsAudioSource.cancel();
                            }
                        });
                    }
                }
            });
        }
    }

    public void cancelTts() {
        int i2 = 0;
        d.a((Object) this, !this.j);
        StringBuilder sb = new StringBuilder("Canceling ");
        int size = this.f2158b.size();
        if (this.f2159c != null) {
            i2 = 1;
        }
        sb.append(size + i2).append(" TTS jobs");
        ArrayList arrayList = new ArrayList(this.f2158b.size() + 1);
        if (this.f2159c != null) {
            arrayList.add(this.f2159c);
            this.f2159c = null;
            this.f2157a.b();
        }
        arrayList.addAll(this.f2158b);
        this.f2158b.clear();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            ((TtsAudioSource) it.next()).cancel();
        }
    }

    public TtsAudioSource generateTts(String str, TtsListener ttsListener, Object obj) {
        d.a("text", (Object) str);
        d.a((Object) this, !this.j);
        d.a((Object) this, this.i);
        AudioType audioType = this.f2164h;
        TtsAudioSource ttsAudioSource = new TtsAudioSource(this, str, this.f2160d, this.f2161e, this.f2163g, this.f2164h.getSampleCount(this.f2163g), this.f2162f, new a() {
            public final void a(TtsAudioSource ttsAudioSource) {
                if (b.this.f2159c == ttsAudioSource) {
                    b.this.f2159c = null;
                    b.this.f2157a.b();
                    b.this.a();
                    return;
                }
                b.this.f2158b.remove(ttsAudioSource);
            }
        }, ttsListener, obj);
        this.f2158b.add(ttsAudioSource);
        if (this.f2159c == null) {
            a();
        }
        return ttsAudioSource;
    }

    public AudioType getAudioType() {
        return this.f2164h;
    }

    public final List getAvailableLanguages(int i2) {
        List<VocalizerModelInfo> availableModels = getAvailableModels(i2);
        ArrayList arrayList = new ArrayList(availableModels.size());
        for (VocalizerModelInfo vocalizerModelInfo : availableModels) {
            if (!arrayList.contains(vocalizerModelInfo.voice.language)) {
                arrayList.add(vocalizerModelInfo.voice.language);
            }
        }
        return arrayList;
    }

    public List getAvailableModels() {
        return getAvailableModels(0);
    }

    public final List getAvailableModels(int i2) {
        ArrayList arrayList = new ArrayList();
        if (this.f2157a.a((List) arrayList)) {
            ArrayList arrayList2 = new ArrayList(arrayList.size());
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                VocalizerModelJni vocalizerModelJni = (VocalizerModelJni) it.next();
                if (i2 == 0 || i2 == vocalizerModelJni.f2156d) {
                    VocalizerVoice a2 = VocalizerVoice.a(vocalizerModelJni.f2154b);
                    if (a2 == null) {
                        Logger.warn(this, "Unable to find Vocalizer voice " + vocalizerModelJni.f2154b);
                    } else if (a2.language.name.equalsIgnoreCase(vocalizerModelJni.f2153a)) {
                        arrayList2.add(new VocalizerModelInfo(a2, vocalizerModelJni.f2156d, vocalizerModelJni.f2155c));
                    } else {
                        Logger.warn(this, "Language name " + vocalizerModelJni.f2153a + " doesn't match " + a2.name + "'s language " + a2.language.name);
                    }
                }
            }
            return arrayList2;
        }
        Logger.error(this, "Unable to detect supported Vocalizer models");
        return new ArrayList();
    }

    public final List getAvailableVoices(int i2) {
        return getAvailableVoices(i2, null);
    }

    public final List getAvailableVoices(int i2, VocalizerLanguage vocalizerLanguage) {
        boolean z;
        VocalizerVoice[] vocalizerVoiceArr = vocalizerLanguage != null ? vocalizerLanguage.voices : null;
        List<VocalizerModelInfo> availableModels = getAvailableModels(i2);
        ArrayList arrayList = new ArrayList(availableModels.size());
        for (VocalizerModelInfo vocalizerModelInfo : availableModels) {
            VocalizerVoice vocalizerVoice = vocalizerModelInfo.voice;
            if (!arrayList.contains(vocalizerVoice)) {
                if (vocalizerVoiceArr != null) {
                    int length = vocalizerVoiceArr.length;
                    int i3 = 0;
                    while (true) {
                        if (i3 >= length) {
                            z = false;
                            break;
                        }
                        VocalizerVoice vocalizerVoice2 = vocalizerVoiceArr[i3];
                        if (vocalizerVoice2 != null && vocalizerVoice2.equals(vocalizerVoice)) {
                            break;
                        }
                        i3++;
                    }
                }
                z = true;
                if (z) {
                    arrayList.add(vocalizerVoice);
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public abstract Handler getWorkerThreadHandler();

    public void load(VocalizerConfig vocalizerConfig) {
        load(vocalizerConfig, null);
    }

    public void load(final VocalizerConfig vocalizerConfig, final LoadListener loadListener) {
        int i2;
        d.a("config", (Object) vocalizerConfig);
        d.a((Object) this, !this.j);
        this.i = true;
        Encoding encoding = Encoding.PCM_16;
        switch (vocalizerConfig.f2152c) {
            case 8:
                i2 = 8000;
                break;
            case 11:
                i2 = 11025;
                break;
            case 16:
                i2 = 16000;
                break;
            case Frequencies.FREQ_22KHZ /*22*/:
                i2 = 22050;
                break;
            default:
                i2 = 16000;
                break;
        }
        this.f2164h = new AudioType(encoding, i2);
        this.l.post(new Runnable() {
            public final void run() {
                final AudioType a2 = b.this.f2157a.a(vocalizerConfig.f2150a, vocalizerConfig.f2151b, vocalizerConfig.f2152c);
                b.this.k.post(new Runnable() {
                    public final void run() {
                        if (a2 != null) {
                            b.this.f2164h = a2;
                        }
                        if (loadListener != null) {
                            loadListener.onLoaded(b.this, a2 != null);
                        }
                    }
                });
            }
        });
    }

    public void loadResource(final String str, final String str2, final ResourceLoadListener resourceLoadListener) {
        d.a((Object) this, !this.j);
        d.a("fileName", str);
        d.a("mimeType", str2);
        this.l.post(new Runnable() {
            public final void run() {
                final boolean a2 = b.this.f2157a.a(str, str2);
                b.this.k.post(new Runnable() {
                    public final void run() {
                        if (resourceLoadListener != null) {
                            resourceLoadListener.onLoaded(b.this, a2);
                        }
                    }
                });
            }
        });
    }

    public void release() {
        d.a((Object) this, !this.j);
        release(null);
    }

    public void release(final ReleaseListener releaseListener) {
        d.a((Object) this, !this.j);
        cancelTts();
        this.j = true;
        this.l.post(new Runnable() {
            public final void run() {
                b.this.f2157a.a();
                if (releaseListener != null) {
                    b.this.k.post(new Runnable() {
                        public final void run() {
                            releaseListener.onReleased(b.this);
                        }
                    });
                }
            }
        });
    }

    public void releaseResource(final String str, final ResourceReleaseListener resourceReleaseListener) {
        d.a((Object) this, !this.j);
        d.a("fileName", str);
        this.l.post(new Runnable() {
            public final void run() {
                b.this.f2157a.a(str);
                b.this.k.post(new Runnable() {
                    public final void run() {
                        if (resourceReleaseListener != null) {
                            resourceReleaseListener.onReleased(b.this);
                        }
                    }
                });
            }
        });
    }

    public void setAudioChunkBufferCount(int i2) {
        d.a((Object) this, !this.j);
        int i3 = i2 == 0 ? 1 : i2;
        if (i3 < -1) {
            i3 = -1;
        }
        this.f2162f = i3;
    }

    public void setAudioChunkSize(int i2) {
        d.a((Object) this, !this.j);
        if (i2 < 20) {
            i2 = 20;
        }
        this.f2163g = i2;
    }

    public void setTtsSpeed(int i2) {
        d.a((Object) this, !this.j);
        if (i2 < 50) {
            i2 = 50;
        } else if (i2 > 400) {
            i2 = 400;
        }
        this.f2160d = i2;
    }

    public void setTtsVolume(int i2) {
        d.a((Object) this, !this.j);
        if (i2 < -10) {
            i2 = -10;
        } else if (i2 > 10) {
            i2 = 10;
        }
        this.f2161e = i2;
    }
}
