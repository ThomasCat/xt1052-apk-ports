package c;

import android.content.Context;
import android.util.Log;
import com.motorola.audiomonitor.a;
import java.util.Locale;
import java.util.Vector;

public class e {

    /* renamed from: h reason: collision with root package name */
    private static final String f121h = ("AMonitor." + e.class.getSimpleName());
    private static final boolean i = a.f160b;

    /* renamed from: a reason: collision with root package name */
    public String f122a = "false";

    /* renamed from: b reason: collision with root package name */
    public int f123b;

    /* renamed from: c reason: collision with root package name */
    public int f124c;

    /* renamed from: d reason: collision with root package name */
    public String f125d = "";

    /* renamed from: e reason: collision with root package name */
    public float f126e;

    /* renamed from: f reason: collision with root package name */
    public String f127f = a.p;

    /* renamed from: g reason: collision with root package name */
    public String f128g = (Locale.getDefault() + "");

    private Vector a() {
        Vector vector = new Vector();
        vector.add(new f("comp", this.f122a));
        vector.add(new f("num_att", this.f123b + ""));
        vector.add(new f("tests_passed", this.f124c + ""));
        vector.add(new f("errors", this.f125d));
        vector.add(new f("noise_level", this.f126e + ""));
        vector.add(new f("aov_version", this.f127f));
        vector.add(new f("call_status", Boolean.FALSE + ""));
        vector.add(new f("spk_phone", Boolean.FALSE + ""));
        vector.add(new f("aud_playing", Boolean.FALSE + ""));
        vector.add(new f("aud_vol", "0"));
        vector.add(new f("docked", ""));
        vector.add(new f("language", this.f128g));
        vector.add(new f("headset", Boolean.FALSE + ""));
        return vector;
    }

    public synchronized void a(Context context) {
        Vector a2 = a();
        if (i) {
            Log.d(f121h, "report: kvp size=" + a2.size());
        }
        a.a(context, "UDT", a2.iterator());
    }
}
