package com.nuance.dragon.toolkit.recognition.dictation;

public interface Token extends EditorItem {
    double getConfidenceScore();

    long getEndTime();

    long getStartTime();

    boolean hasNoSpaceAfterDirective();

    boolean hasNoSpaceBeforeDirective();

    String toString();
}
