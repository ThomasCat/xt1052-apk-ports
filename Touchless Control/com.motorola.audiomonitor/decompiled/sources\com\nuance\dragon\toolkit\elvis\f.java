package com.nuance.dragon.toolkit.elvis;

import com.nuance.dragon.toolkit.grammar.Word;
import com.nuance.dragon.toolkit.util.Logger;
import java.util.ArrayList;
import java.util.List;

final class f implements e {

    /* renamed from: a reason: collision with root package name */
    private boolean f1666a;

    f() {
    }

    public final int a(WordSlot wordSlot) {
        return 0;
    }

    public final int a(Word word) {
        return 0;
    }

    public final int a(String str, int i, int i2, boolean z, boolean z2) {
        return 0;
    }

    public final int a(String str, Word word, boolean z) {
        return 0;
    }

    public final int a(String str, String str2, String str3, int i) {
        return 0;
    }

    public final int a(short[] sArr) {
        Logger.info(this, "Recorded " + sArr.length + " shorts");
        return this.f1666a ? 1 : 2;
    }

    public final ElvisResult a(Grammar grammar) {
        return new ElvisResult(new ElvisNbestList((List) new ArrayList(0)), -1);
    }

    public final Grammar a(String str) {
        return new Grammar();
    }

    public final boolean a() {
        return true;
    }

    public final boolean a(String str, int i) {
        return false;
    }

    public final boolean a(String str, int i, String str2) {
        return true;
    }

    public final boolean a(String str, Grammar grammar) {
        return true;
    }

    public final boolean a(String str, String str2, String str3) {
        return false;
    }

    public final boolean a(List list) {
        return true;
    }

    public final boolean a(List list, int i) {
        return true;
    }

    public final boolean a(boolean z) {
        this.f1666a = false;
        return true;
    }

    public final int b(short[] sArr) {
        return 0;
    }

    public final void b() {
    }

    public final void b(boolean z) {
    }

    public final boolean b(String str) {
        return false;
    }

    public final boolean b(List list) {
        this.f1666a = true;
        return true;
    }

    public final boolean c() {
        return false;
    }

    public final boolean c(String str) {
        return false;
    }

    public final boolean d() {
        this.f1666a = false;
        return true;
    }

    public final boolean e() {
        return true;
    }

    public final ElvisResult f() {
        return null;
    }

    public final void g() {
    }

    public final int h() {
        return 0;
    }
}
