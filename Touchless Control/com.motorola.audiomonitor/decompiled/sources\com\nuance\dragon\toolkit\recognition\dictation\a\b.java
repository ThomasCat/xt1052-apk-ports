package com.nuance.dragon.toolkit.recognition.dictation.a;

import com.nuance.dragon.toolkit.recognition.dictation.AlterChoice;
import com.nuance.dragon.toolkit.recognition.dictation.AlterChoices;
import com.nuance.dragon.toolkit.recognition.dictation.EditorItem;
import java.util.ArrayList;
import java.util.Iterator;

final class b implements AlterChoices {

    /* renamed from: a reason: collision with root package name */
    private ArrayList f1922a;

    /* renamed from: b reason: collision with root package name */
    private e f1923b;

    /* renamed from: c reason: collision with root package name */
    private long f1924c;

    /* renamed from: d reason: collision with root package name */
    private long f1925d;

    /* renamed from: e reason: collision with root package name */
    private boolean f1926e = false;

    /* renamed from: f reason: collision with root package name */
    private final long f1927f;

    public b(e eVar, int i, long j, long j2, long j3) {
        this.f1927f = j3;
        this.f1922a = new ArrayList(i);
        for (int i2 = 0; i2 < i; i2++) {
            this.f1922a.add(i2, new a());
        }
        this.f1923b = eVar;
        this.f1924c = j;
        this.f1925d = j2;
    }

    public final void a() {
        if (!this.f1926e && this.f1922a.size() > 1) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            arrayList.add(this.f1922a.get(0));
            arrayList2.add(((AlterChoice) this.f1922a.get(0)).toString());
            for (int i = 1; i < this.f1922a.size(); i++) {
                AlterChoice alterChoice = (AlterChoice) this.f1922a.get(i);
                if (!arrayList2.contains(alterChoice.toString())) {
                    arrayList.add(alterChoice);
                    arrayList2.add(alterChoice.toString());
                }
            }
            this.f1922a = arrayList;
            this.f1926e = true;
        }
    }

    public final void a(int i, ArrayList arrayList) {
        this.f1926e = false;
        ((a) this.f1922a.get(i)).a(arrayList);
    }

    public final void a(EditorItem editorItem) {
        this.f1926e = false;
        Iterator it = this.f1922a.iterator();
        while (it.hasNext()) {
            ((a) ((AlterChoice) it.next())).a(editorItem);
        }
    }

    public final void choose(int i) {
        if (i < 0 || i >= this.f1922a.size()) {
            throw new IndexOutOfBoundsException("Index provided is not within range.");
        }
        a aVar = (a) this.f1922a.get(i);
        if (this.f1923b.a(this.f1927f)) {
            this.f1923b.a(this.f1924c, this.f1925d, aVar.a());
            return;
        }
        throw new IllegalStateException("The DictationEditBuffer has been modified since this AlterChoices object was created.");
    }

    public final AlterChoice getChoiceAt(int i) {
        if (i < 0 || i >= this.f1922a.size()) {
            return null;
        }
        return (AlterChoice) this.f1922a.get(i);
    }

    public final int size() {
        return this.f1922a.size();
    }

    public final String[] toStringArray() {
        String[] strArr = new String[size()];
        for (int i = 0; i < size(); i++) {
            strArr[i] = getChoiceAt(i).toString();
        }
        return strArr;
    }
}
