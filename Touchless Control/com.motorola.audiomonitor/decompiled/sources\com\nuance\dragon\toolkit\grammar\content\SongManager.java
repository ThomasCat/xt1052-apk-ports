package com.nuance.dragon.toolkit.grammar.content;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore.Audio.Media;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.grammar.Word;
import com.nuance.dragon.toolkit.grammar.WordAction;
import com.nuance.dragon.toolkit.grammar.content.ContentManager.PlatformFullIterator;
import com.nuance.dragon.toolkit.grammar.content.UriContentManager.UriPlatformFullIterator;

public class SongManager extends UriContentManager {

    /* renamed from: a reason: collision with root package name */
    private static final Uri f1801a = Media.EXTERNAL_CONTENT_URI;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final String[] f1802b = {"title"};

    public SongManager(String str, FileManager fileManager, Context context) {
        this(str, fileManager, false, false, context);
    }

    public SongManager(String str, FileManager fileManager, boolean z, Context context) {
        this(str, fileManager, false, z, context);
    }

    public SongManager(String str, FileManager fileManager, boolean z, boolean z2, Context context) {
        super(str, fileManager, f1801a, z, z2, context);
    }

    /* access modifiers changed from: protected */
    public PlatformFullIterator getPlatformFullIterator() {
        return new UriPlatformFullIterator(this._context, this._uri, f1802b) {

            /* renamed from: b reason: collision with root package name */
            private int f1804b;

            /* access modifiers changed from: protected */
            public final WordAction getNext(Cursor cursor) {
                String str;
                while (cursor.moveToNext()) {
                    String string = cursor.getString(this.f1804b);
                    if (string == null || string.length() != 0) {
                        str = string;
                        continue;
                    } else {
                        str = null;
                        continue;
                    }
                    if (str != null) {
                        return new WordAction(new Word(str, null), true);
                    }
                }
                return null;
            }

            /* access modifiers changed from: protected */
            public final void init(Cursor cursor) {
                this.f1804b = cursor.getColumnIndex(SongManager.f1802b[0]);
            }
        };
    }
}
