package com.motorola.audiomonitor;

import android.util.Log;
import h.k;
import java.nio.ByteBuffer;

public class GenericRecognizer {
    private static final boolean DEBUG = a.f160b;
    private static final String TAG = ("AMonitor." + GenericRecognizer.class.getSimpleName());
    private RecognizerConfig mConfig;
    private long mJniContext;
    private ByteBuffer mLastAudio;
    private float mLastClip;
    private int mLastCount;
    private float mLastFrom;
    private float[] mLastScores;
    private final o mLastSvTuple;
    private float mLastTo;
    private String[] mLastWords;
    private String mSearchFile;

    public class UdtEnrollInfo {
        private final ByteBuffer mAudio;
        private final String mPronun;

        UdtEnrollInfo(String str, ByteBuffer byteBuffer) {
            this.mPronun = str;
            this.mAudio = byteBuffer;
        }

        public ByteBuffer getAudio() {
            return this.mAudio;
        }

        public String getPronun() {
            return this.mPronun;
        }
    }

    public class UdtEnrollResult {
        public static final int CHECKRECORDING_CLIPPING_MSEC = 128;
        public static final int CHECKRECORDING_CLIPPING_PERCENT = 64;
        public static final int CHECKRECORDING_ENERGY_MIN = 1;
        public static final int CHECKRECORDING_ENERGY_STD_DEV = 2;
        public static final int CHECKRECORDING_POOR_RECORDINGS = 256;
        public static final int CHECKRECORDING_RECORDING_VARIANCE = 32;
        public static final int CHECKRECORDING_SIL_BEG_MSEC = 4;
        public static final int CHECKRECORDING_SIL_END_MSEC = 8;
        public static final int CHECKRECORDING_SNR = 16;
        public static final int CHECKRECORDING_SPOT = 512;
        private int mGlobalFeedback;
        private float mPhraseQuality;

        public int getGlobalFeedback() {
            return this.mGlobalFeedback;
        }

        public float getPhraseQuality() {
            return this.mPhraseQuality;
        }

        public void setGlobalFeedback(int i) {
            this.mGlobalFeedback = i;
        }

        public void setPhraseQuality(float f2) {
            this.mPhraseQuality = f2;
        }
    }

    static {
        System.loadLibrary("recoglib");
    }

    public GenericRecognizer() {
        this("generic_acoustic.raw", "language.raw", null);
    }

    public GenericRecognizer(String str, String str2, String str3) {
        this.mJniContext = 0;
        this.mLastCount = 0;
        this.mLastSvTuple = new o();
        this.mJniContext = recogInitSession(a.f161c + str, a.f161c + str2, 300);
        if (str3 != null) {
            this.mSearchFile = new String(a.f161c + str3);
        }
    }

    private native int checkRecording(long j, ByteBuffer byteBuffer);

    private native String normalizeWord(long j, String str);

    private void processRecognizedAction(float[] fArr, String[] strArr, int i, float f2, float f3, float f4) {
        if (DEBUG) {
            Log.d(TAG, "[audio: from " + f2 + " to " + f3 + " clip=" + f4 + "]");
            for (int i2 = 0; i2 < i; i2++) {
                Log.d(TAG, "[" + i2 + "] Score=" + fArr[i2] + " Word=" + strArr[i2]);
            }
        }
        this.mLastScores = fArr;
        this.mLastWords = strArr;
        this.mLastCount = i;
        this.mLastFrom = f2;
        this.mLastTo = f3;
        this.mLastClip = f4;
    }

    private void processRecognizedAudio(ByteBuffer byteBuffer) {
        if (DEBUG) {
            Log.d(TAG, "Recognized Audio Len = " + byteBuffer.capacity());
        }
        this.mLastAudio = byteBuffer;
    }

    private void processVerifyResult(float f2, float f3, int i) {
        boolean z = true;
        if (DEBUG) {
            Log.d(TAG, "SV: relative=" + f2 + " raw=" + f3 + " recOk=" + i);
        }
        this.mLastSvTuple.f523a = f2;
        this.mLastSvTuple.f524b = f3;
        o oVar = this.mLastSvTuple;
        if (i != 1) {
            z = false;
        }
        oVar.f525c = z;
    }

    private native void recogClose(long j);

    private native boolean recogInitSearch(long j, RecognizerConfig recognizerConfig);

    private native boolean recogInitSearchByFile(long j, String str, RecognizerConfig recognizerConfig);

    private native long recogInitSession(String str, String str2, int i);

    private native void recogReset(long j);

    private native int recognize(long j, ProtectedByteBuffer protectedByteBuffer, int i);

    private native boolean speakerEnrollComplete(long j, UdtEnrollInfo[] udtEnrollInfoArr, String str, String str2, String str3, String str4, String str5, String str6, String str7, UdtEnrollResult udtEnrollResult);

    private native boolean udtEnrollComplete(long j, UdtEnrollInfo[] udtEnrollInfoArr, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, UdtEnrollResult udtEnrollResult);

    private native boolean udtRecordingInit(long j, String str, RecognizerConfig recognizerConfig);

    private native float udtVerify(long j);

    public int checkRecording(ByteBuffer byteBuffer) {
        return checkRecording(this.mJniContext, byteBuffer);
    }

    public void cleanUp() {
        if (this.mJniContext != 0) {
            recogClose(this.mJniContext);
            this.mJniContext = 0;
        }
    }

    public void configure(RecognizerConfig recognizerConfig) {
        this.mConfig = recognizerConfig;
        if (!(this.mSearchFile == null ? recogInitSearch(this.mJniContext, recognizerConfig) : recogInitSearchByFile(this.mJniContext, this.mSearchFile, recognizerConfig))) {
            Log.e(TAG, "Configuration Failure.");
            recogClose(this.mJniContext);
            this.mJniContext = 0;
        }
    }

    public boolean enrollCompleteSpeaker(UdtEnrollInfo[] udtEnrollInfoArr, String str, String str2, String str3, String str4, String str5, String str6, String str7, UdtEnrollResult udtEnrollResult) {
        return speakerEnrollComplete(this.mJniContext, udtEnrollInfoArr, str, a.f161c + str2, a.f161c + str3, a.f161c + str4, str5, a.f161c + str6, a.f161c + str7, udtEnrollResult);
    }

    public boolean enrollCompleteUdt(UdtEnrollInfo[] udtEnrollInfoArr, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, UdtEnrollResult udtEnrollResult) {
        return udtEnrollComplete(this.mJniContext, udtEnrollInfoArr, a.f161c + str, a.f161c + str2, k.a() + str3, a.f161c + str4, a.f161c + str5, str6, a.f161c + str7, a.f161c + str8, udtEnrollResult);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        if (this.mJniContext != 0) {
            Log.w(TAG, "finalize: this=" + this);
        }
        cleanUp();
    }

    public ByteBuffer getLastAudio() {
        return this.mLastAudio;
    }

    public float getLastClip() {
        return this.mLastClip;
    }

    public float getLastPhraseDuration() {
        return this.mLastTo - this.mLastFrom;
    }

    public float[] getLastPhraseTime() {
        return new float[]{this.mLastFrom, this.mLastTo};
    }

    public o getLastSvTuple() {
        return this.mLastSvTuple;
    }

    public int getRecognizedWords(float f2, float[] fArr, String[] strArr) {
        int i = 0;
        if (fArr.length == strArr.length) {
            while (i < this.mLastCount && i < fArr.length && this.mLastScores[i] >= f2) {
                fArr[i] = this.mLastScores[i];
                strArr[i] = this.mLastWords[i];
                i++;
            }
        }
        return i;
    }

    public String normalize(String str) {
        return normalizeWord(this.mJniContext, str);
    }

    public n recognize(ProtectedByteBuffer protectedByteBuffer) {
        this.mLastCount = 0;
        n a2 = n.a(recognize(this.mJniContext, protectedByteBuffer, protectedByteBuffer.getAffinityData()));
        if (a2 == n.ERROR) {
            Log.e(TAG, "Failure during recognition.");
            recogClose(this.mJniContext);
            this.mJniContext = 0;
        }
        return a2;
    }

    public void recordingInitUdt(RecognizerConfig recognizerConfig) {
        this.mSearchFile = new String(a.f161c + "phoneme_search.raw");
        if (!udtRecordingInit(this.mJniContext, this.mSearchFile, recognizerConfig)) {
            Log.e(TAG, "recordingInitUdt failure");
            recogClose(this.mJniContext);
            this.mJniContext = 0;
        }
    }

    public void reset() {
        if (this.mJniContext != 0) {
            recogReset(this.mJniContext);
        }
    }

    public float verifyUdt() {
        return udtVerify(this.mJniContext);
    }
}
