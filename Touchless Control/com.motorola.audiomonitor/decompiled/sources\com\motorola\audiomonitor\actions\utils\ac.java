package com.motorola.audiomonitor.actions.utils;

public enum ac {
    NO_PROMPT,
    DISMISS_BY_EOS(1, 0, 0),
    DISMISS_BY_CONFIRM_CANCEL(2, 1, 2),
    EOS_DISMISS_BY_CONFIRM_CALL(2, 2, 1),
    EOS_DISMISS_BY_CONFIRM_CANCEL(2, 2, 2),
    EOS_CONFIRM_CALL(0, 2, 1);
    
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public int f180g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public int f181h;
    /* access modifiers changed from: private */
    public int i;

    private ac(int i2, int i3, int i4) {
        this.f180g = i2;
        this.f181h = i3;
        this.i = i4;
    }
}
