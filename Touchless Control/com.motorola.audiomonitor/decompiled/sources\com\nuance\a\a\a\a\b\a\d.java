package com.nuance.a.a.a.a.b.a;

import java.util.Vector;

public interface d {

    public interface a {
        void a();
    }

    public final class b {

        /* renamed from: a reason: collision with root package name */
        public static final b f786a = new b();

        /* renamed from: b reason: collision with root package name */
        public static final b f787b = new b();

        private b() {
        }
    }

    public final class c {

        /* renamed from: a reason: collision with root package name */
        public static final c f788a = new c();

        /* renamed from: b reason: collision with root package name */
        public static final c f789b = new c();

        /* renamed from: c reason: collision with root package name */
        public static final c f790c = new c();

        private c() {
        }
    }

    /* renamed from: com.nuance.a.a.a.a.b.a.d$d reason: collision with other inner class name */
    public interface C0003d {
        void a(c cVar, Object obj);
    }

    public interface e {
        void a(c cVar, Object obj, byte[] bArr, int i, int i2, Object obj2);
    }

    public interface f {
        void a(c cVar, Object obj, int i, int i2, Object obj2);
    }

    c a(Object obj, b bVar, byte[] bArr, int i, e eVar, Object obj2);

    c a(Object obj, byte[] bArr, int i, f fVar, Object obj2);

    void a(Object obj);

    void a(String str, int i, C0003d dVar, a aVar);

    void a(String str, int i, Vector vector, C0003d dVar, a aVar);

    void b(Object obj);
}
