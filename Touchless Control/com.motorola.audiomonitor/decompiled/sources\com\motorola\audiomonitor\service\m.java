package com.motorola.audiomonitor.service;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;

class m implements OnSharedPreferenceChangeListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ d f579a;

    m(d dVar) {
        this.f579a = dVar;
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        this.f579a.q.onSharedPreferenceChanged(sharedPreferences, str);
        if ("acousticmodel".equals(str)) {
            if (!this.f579a.q.f584e.equals("")) {
                this.f579a.a(203, 0, 0, (Object) this.f579a.q.f584e);
            }
        } else if ("languagemodel".equals(str) && !this.f579a.q.f585f.equals("")) {
            this.f579a.a(204, 0, 0, (Object) this.f579a.q.f585f);
        }
    }
}
