package com.nuance.dragon.toolkit.recognition.dictation.a;

import com.nuance.dragon.toolkit.recognition.dictation.AlterChoices;
import com.nuance.dragon.toolkit.recognition.dictation.DictationEditBuffer;
import com.nuance.dragon.toolkit.recognition.dictation.DictationResult;
import com.nuance.dragon.toolkit.recognition.dictation.DictationResultManager;
import com.nuance.dragon.toolkit.recognition.dictation.EditorItem;
import com.nuance.dragon.toolkit.recognition.dictation.EditorItem.Type;
import com.nuance.dragon.toolkit.recognition.dictation.Token;
import java.util.ArrayList;
import java.util.List;

public final class e implements DictationEditBuffer {

    /* renamed from: a reason: collision with root package name */
    private ArrayList f1930a;

    /* renamed from: b reason: collision with root package name */
    private DictationResult f1931b;

    /* renamed from: c reason: collision with root package name */
    private final long f1932c;

    /* renamed from: d reason: collision with root package name */
    private long f1933d = 0;

    public final class a {

        /* renamed from: a reason: collision with root package name */
        public int f1935a = 0;

        /* renamed from: b reason: collision with root package name */
        public int f1936b = 0;

        public a() {
        }
    }

    public final class b {

        /* renamed from: a reason: collision with root package name */
        public a f1938a;

        /* renamed from: b reason: collision with root package name */
        public a f1939b;

        public b() {
            this.f1938a = new a();
            this.f1939b = new a();
        }
    }

    public final class c {

        /* renamed from: a reason: collision with root package name */
        public long f1941a = -1;

        /* renamed from: b reason: collision with root package name */
        public long f1942b = -1;

        public c() {
        }
    }

    public e(long j, f fVar) {
        this.f1932c = j;
        this.f1930a = new ArrayList();
        this.f1931b = fVar;
        if (this.f1931b.size() > 0) {
            a(fVar.a(0).b_(), false);
        }
    }

    private b a(long j, long j2) {
        if (j < 0 || j2 < j) {
            throw new IndexOutOfBoundsException("start is less than 0 or end is less than start!");
        }
        b bVar = new b();
        long j3 = 0;
        int i = 0;
        while (i < this.f1930a.size()) {
            EditorItem editorItem = (EditorItem) this.f1930a.get(i);
            if (j >= j3 && j < ((long) editorItem.length()) + j3) {
                bVar.f1938a.f1935a = i;
                bVar.f1938a.f1936b = (int) (j - j3);
            }
            if (j2 < j3 || j2 >= ((long) editorItem.length()) + j3) {
                j3 += (long) editorItem.length();
                i++;
            } else {
                bVar.f1939b.f1935a = i;
                bVar.f1939b.f1936b = (int) (j2 - j3);
                return bVar;
            }
        }
        if (j == j3) {
            bVar.f1938a.f1935a = this.f1930a.size();
            bVar.f1938a.f1936b = 0;
        }
        if (j2 == j3) {
            bVar.f1939b.f1935a = this.f1930a.size();
            bVar.f1939b.f1936b = 0;
        }
        if (j2 <= j3) {
            return bVar;
        }
        throw new IndexOutOfBoundsException("end is greater than buffer length!");
    }

    private void a() {
        this.f1933d++;
    }

    private void a(int i, ArrayList arrayList) {
        this.f1930a.addAll(i, arrayList);
    }

    private void a(int i, List list, boolean z) {
        int i2;
        int i3 = 0;
        boolean z2 = !z;
        if (list != null) {
            boolean z3 = z2;
            while (true) {
                int i4 = i3;
                if (i4 < list.size()) {
                    h hVar = (h) list.get(i4);
                    boolean hasNoSpaceBeforeDirective = hVar.hasNoSpaceBeforeDirective();
                    if (0 + ((long) hVar.length()) <= this.f1932c) {
                        if (hasNoSpaceBeforeDirective || z3) {
                            i2 = i;
                        } else {
                            i2 = i + 1;
                            this.f1930a.add(i, new j(" ", Type.WHITE_SPACE));
                        }
                        i = i2 + 1;
                        this.f1930a.add(i2, hVar);
                        z3 = hVar.hasNoSpaceAfterDirective();
                        i3 = i4 + 1;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
        }
    }

    private void a(List list, boolean z) {
        a(this.f1930a.size(), list, z);
    }

    private a b(long j) {
        if (j < 0) {
            throw new IndexOutOfBoundsException("Position is less than 0!");
        }
        a aVar = new a();
        long j2 = 0;
        int i = 0;
        while (i < this.f1930a.size()) {
            EditorItem editorItem = (EditorItem) this.f1930a.get(i);
            if (j < j2 || j >= ((long) editorItem.length()) + j2) {
                j2 += (long) editorItem.length();
                i++;
            } else {
                aVar.f1935a = i;
                aVar.f1936b = (int) (j - j2);
                return aVar;
            }
        }
        if (j == j2) {
            aVar.f1935a = this.f1930a.size();
            aVar.f1936b = 0;
            return aVar;
        }
        throw new IndexOutOfBoundsException("Position is greater than buffer length!");
    }

    private c b(long j, long j2) {
        Token token;
        Token token2;
        Token token3;
        Token token4;
        Token token5;
        if (j < 0 || j2 < j) {
            throw new IndexOutOfBoundsException("start is less than 0 or end is less than start!");
        }
        c cVar = new c();
        long j3 = 0;
        Token token6 = null;
        Token token7 = null;
        Token token8 = null;
        int i = 0;
        while (true) {
            if (i >= this.f1930a.size()) {
                token = null;
                token2 = token6;
                token3 = token7;
                break;
            }
            EditorItem editorItem = (EditorItem) this.f1930a.get(i);
            if (editorItem.getType() == Type.TOKEN) {
                token2 = j3 < j ? (Token) editorItem : token6;
                if (j3 >= j && token8 == null) {
                    token8 = (Token) editorItem;
                }
                token3 = j3 < j2 ? (Token) editorItem : token7;
                if (j3 >= j2) {
                    token = (Token) editorItem;
                    break;
                }
                token4 = token8;
                Token token9 = token3;
                token6 = token2;
                token5 = token9;
            } else {
                token5 = token7;
                token4 = token8;
            }
            j3 += (long) editorItem.length();
            i++;
            token8 = token4;
            token7 = token5;
        }
        if (j2 > j3) {
            throw new IndexOutOfBoundsException("end is greater than buffer length!");
        }
        if (token8 != null) {
            cVar.f1941a = token8.getStartTime();
        } else if (token2 != null) {
            cVar.f1941a = token2.getEndTime();
        }
        if (token3 != null) {
            cVar.f1942b = token3.getEndTime();
        } else if (token != null) {
            cVar.f1942b = token.getStartTime();
        }
        return cVar;
    }

    private AlterChoices c(long j, long j2) {
        int i;
        a b2 = b(j);
        a b3 = b(j2);
        b bVar = new b(this, this.f1931b.size(), j, j2, this.f1933d);
        if (b2.f1936b != 0) {
            int i2 = b2.f1935a + 1;
            bVar.a(new j(((EditorItem) this.f1930a.get(b2.f1935a)).toString().substring(b2.f1936b), Type.WORD));
            i = i2;
        } else {
            i = b2.f1935a;
        }
        int i3 = b3.f1935a - 1;
        for (int i4 = i; i4 <= i3; i4++) {
            switch (((EditorItem) this.f1930a.get(i4)).getType()) {
                case WORD:
                    bVar.a((EditorItem) this.f1930a.get(i4));
                    break;
                case WHITE_SPACE:
                    bVar.a((EditorItem) this.f1930a.get(i4));
                    break;
                case TOKEN:
                    Token token = (Token) this.f1930a.get(i4);
                    int i5 = 0;
                    while (true) {
                        int i6 = i5;
                        if (i6 >= this.f1931b.size()) {
                            break;
                        } else {
                            ArrayList a2 = ((g) this.f1931b.sentenceAt(i6)).a(token.getStartTime(), token.getEndTime());
                            if (!a2.isEmpty()) {
                                bVar.a(i6, a2);
                            }
                            i5 = i6 + 1;
                        }
                    }
            }
        }
        if (b3.f1936b != 0) {
            bVar.a(new j(((EditorItem) this.f1930a.get(b3.f1935a)).toString().substring(0, b3.f1936b), Type.WORD));
        }
        bVar.a();
        return bVar;
    }

    public final void a(long j, long j2, ArrayList arrayList) {
        a b2 = b(j);
        delete(j, j2);
        if (b2.f1936b == 0) {
            a(b2.f1935a, arrayList);
        } else {
            a(b2.f1935a + 1, arrayList);
        }
    }

    public final boolean a(long j) {
        return j == this.f1933d;
    }

    public final void append(byte[] bArr) {
        a();
        DictationResult createDictationResult = DictationResultManager.createDictationResult(bArr);
        if (createDictationResult.size() > 0) {
            long j = this.f1931b.size() > 0 ? this.f1931b.sentenceAt(0).getEndTime() : 0;
            this.f1931b.append(createDictationResult);
            g gVar = (g) this.f1931b.sentenceAt(0);
            if (gVar.size() > 0) {
                ArrayList a2 = gVar.a(j, gVar.getEndTime());
                if (this.f1930a.size() > 0) {
                    EditorItem editorItem = (EditorItem) this.f1930a.get(this.f1930a.size() - 1);
                    if (editorItem.getType() != Type.WHITE_SPACE) {
                        if (editorItem.getType() == Type.TOKEN) {
                            a((List) a2, !((Token) editorItem).hasNoSpaceAfterDirective());
                            return;
                        } else {
                            a((List) a2, true);
                            return;
                        }
                    }
                }
                a((List) a2, false);
            }
        }
    }

    public final void delete(long j) {
        delete(j, 1 + j);
    }

    public final void delete(long j, long j2) {
        if (j >= j2) {
            throw new IllegalArgumentException("The indicated range is invalid. The range is treated as [start, end), therefore start cannot be greater or equal to end.");
        }
        a();
        b a2 = a(j, j2);
        if (a2.f1938a.f1935a == this.f1930a.size()) {
            throw new IndexOutOfBoundsException("Index out of bounds!");
        } else if (a2.f1939b.f1935a == a2.f1938a.f1935a) {
            String obj = ((EditorItem) this.f1930a.get(a2.f1939b.f1935a)).toString();
            this.f1930a.set(a2.f1939b.f1935a, new j(obj.substring(0, a2.f1938a.f1936b) + obj.substring(a2.f1939b.f1936b), Type.WORD));
        } else {
            int i = a2.f1938a.f1935a;
            int i2 = a2.f1939b.f1935a;
            EditorItem editorItem = (EditorItem) this.f1930a.get(a2.f1938a.f1935a);
            if (a2.f1938a.f1936b != 0) {
                this.f1930a.set(a2.f1938a.f1935a, new j(editorItem.toString().substring(0, a2.f1938a.f1936b), Type.WORD));
                i++;
            }
            if (a2.f1939b.f1935a != this.f1930a.size()) {
                EditorItem editorItem2 = (EditorItem) this.f1930a.get(a2.f1939b.f1935a);
                if (a2.f1939b.f1936b != 0) {
                    this.f1930a.set(a2.f1939b.f1935a, new j(editorItem2.toString().substring(a2.f1939b.f1936b), Type.WORD));
                }
            }
            if (i < i2) {
                this.f1930a.subList(i, i2).clear();
            }
        }
    }

    public final AlterChoices getChoices(long j, long j2) {
        if (j >= j2) {
            throw new IllegalArgumentException("The indicated range is invalid. The range is treated as [start, end), therefore start cannot be greater or equal to end.");
        } else if (j >= 0 && j <= length() && j2 >= 0 && j2 <= length()) {
            return c(j, j2);
        } else {
            throw new IndexOutOfBoundsException("Indicated position is out of bounds.");
        }
    }

    public final EditorItem[] getItems() {
        return (EditorItem[]) this.f1930a.toArray(new EditorItem[this.f1930a.size()]);
    }

    public final void insert(long j, char c2) {
        if (j < 0 || j > length()) {
            throw new IndexOutOfBoundsException("Indicated position is out of bounds.");
        }
        insert(j, Character.toString(c2));
    }

    public final void insert(long j, String str) {
        int i;
        int i2;
        if (str != null && str.length() != 0) {
            a();
            a b2 = b(j);
            int i3 = b2.f1935a;
            if (b2.f1936b != 0) {
                String obj = ((EditorItem) this.f1930a.get(b2.f1935a)).toString();
                str = obj.substring(0, b2.f1936b) + str + obj.substring(b2.f1936b);
            }
            int i4 = 0;
            int i5 = i3;
            int indexOf = str.indexOf(" ");
            while (true) {
                i = i4;
                if (indexOf == -1) {
                    break;
                }
                if (indexOf == i) {
                    i2 = i5 + 1;
                    this.f1930a.add(i5, new j(" ", Type.WHITE_SPACE));
                } else if (indexOf > i) {
                    int i6 = i5 + 1;
                    this.f1930a.add(i5, new j(str.substring(i, indexOf), Type.WORD));
                    i2 = i6 + 1;
                    this.f1930a.add(i6, new j(" ", Type.WHITE_SPACE));
                } else {
                    i2 = i5;
                }
                int i7 = indexOf + 1;
                indexOf = str.indexOf(" ", i7);
                i4 = i7;
                i5 = i2;
            }
            if (i < str.length()) {
                this.f1930a.add(i5, new j(str.substring(i), Type.WORD));
            }
        }
    }

    public final long length() {
        return (long) toString().length();
    }

    public final void merge(byte[] bArr, long j, long j2) {
        if (j < 0 || j2 > length() || j >= j2) {
            throw new IllegalArgumentException("Invalid start or end position");
        }
        a();
        c b2 = b(j, j2);
        delete(j, j2);
        DictationResult createDictationResult = DictationResultManager.createDictationResult(bArr);
        if (createDictationResult.size() > 0) {
            long j3 = b2.f1941a;
            long endTime = b2.f1941a + createDictationResult.sentenceAt(0).getEndTime();
            this.f1931b.merge(createDictationResult, b2.f1941a, b2.f1942b);
            g gVar = (g) this.f1931b.sentenceAt(0);
            if (gVar.size() > 0) {
                ArrayList a2 = gVar.a(j3, endTime);
                int i = b(j).f1935a;
                if (i > 0) {
                    EditorItem editorItem = (EditorItem) this.f1930a.get(i - 1);
                    if (editorItem.getType() != Type.WHITE_SPACE) {
                        if (editorItem.getType() == Type.TOKEN) {
                            a(i, (List) a2, !((Token) editorItem).hasNoSpaceAfterDirective());
                        } else {
                            a(i, (List) a2, true);
                        }
                    }
                }
                a(i, (List) a2, false);
            }
            if (b2.f1941a == -1) {
                b2.f1941a = this.f1931b.sentenceAt(0).getStartTime();
            }
            if (b2.f1942b == -1) {
                b2.f1942b = this.f1931b.sentenceAt(0).getEndTime();
                return;
            }
            return;
        }
        b2.f1941a = 0;
        b2.f1942b = 0;
    }

    public final void replace(long j, long j2, String str) {
        long length = length();
        if (j < 0 || j > length || j2 < 0 || j2 > length) {
            throw new IndexOutOfBoundsException("Indicated position is out of bounds.");
        }
        a();
        delete(j, j2);
        insert(j, str);
    }

    public final String toString() {
        if (this.f1930a.size() == 0) {
            return "";
        }
        String str = new String();
        int i = 0;
        while (i < this.f1930a.size()) {
            String str2 = str + ((EditorItem) this.f1930a.get(i)).toString();
            i++;
            str = str2;
        }
        return str;
    }

    public final void update(String str) {
        if (str == null) {
            throw new IllegalArgumentException("text cannot be null.");
        }
        a();
        String eVar = toString();
        CharSequence subSequence = eVar.subSequence(0, eVar.length());
        CharSequence subSequence2 = str.subSequence(0, str.length());
        int length = subSequence.length();
        int length2 = subSequence2.length();
        int i = length > length2 ? length2 : length;
        int i2 = 0;
        while (i2 < i && subSequence.charAt(i2) == subSequence2.charAt(i2)) {
            i2++;
        }
        int i3 = 0;
        while (i3 < i && subSequence.charAt((length - i3) - 1) == subSequence2.charAt((length2 - i3) - 1)) {
            i3++;
        }
        int i4 = (length2 - i3) - 1;
        int i5 = (((length - i3) - 1) - i2) + 1;
        if (i5 < 0) {
            i5 = 0;
        }
        int i6 = (i4 - i2) + 1;
        if (i6 < 0) {
            i6 = 0;
        }
        if (length2 != (length - i5) + i6) {
            if (length > length2) {
                i5 = length - length2;
            } else {
                i6 = length2 - length;
            }
        }
        if (i5 != 0 || i6 != 0) {
            if (i6 == 0) {
                delete((long) i2, (long) (i2 + i5));
            } else if (i5 == 0) {
                insert((long) i2, str.substring(i2, i6 + i2));
            } else {
                replace((long) i2, (long) (i5 + i2), str.substring(i2, i6 + i2));
            }
        }
    }
}
