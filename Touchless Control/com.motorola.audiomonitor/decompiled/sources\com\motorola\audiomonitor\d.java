package com.motorola.audiomonitor;

import android.text.TextUtils;
import android.util.Log;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

class d {

    /* renamed from: a reason: collision with root package name */
    private static final LinkedList f473a = new LinkedList();

    /* renamed from: b reason: collision with root package name */
    private static OutputStream f474b = null;

    public static List a(String str) {
        List list;
        synchronized (f473a) {
            Iterator it = f473a.iterator();
            while (true) {
                if (!it.hasNext()) {
                    list = null;
                    break;
                }
                e eVar = (e) it.next();
                if (TextUtils.equals(eVar.f489a, str)) {
                    list = eVar.f490b;
                    break;
                }
            }
        }
        return list;
    }

    public static void a(String str, OutputStream outputStream) {
        synchronized (f473a) {
            f474b = outputStream;
        }
    }

    public static void a(String str, List list) {
        synchronized (f473a) {
            if (f474b != null) {
                if (AudioProvider.f132b) {
                    Log.w(AudioProvider.f131a, "Force clean up possibly leaking stream.");
                }
                try {
                    f474b.close();
                } catch (IOException e2) {
                }
                f474b = null;
            }
            if (f473a.size() == 3) {
                f473a.removeLast();
            }
            f473a.addFirst(new e(str, list));
        }
    }
}
