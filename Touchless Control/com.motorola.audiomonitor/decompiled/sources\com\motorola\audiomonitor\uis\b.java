package com.motorola.audiomonitor.uis;

import a.e;
import a.g;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.actions.utils.a;
import com.motorola.audiomonitor.actions.utils.f;
import h.k;
import java.util.List;

public class b extends Fragment {

    /* renamed from: a reason: collision with root package name */
    private Context f685a;

    /* renamed from: b reason: collision with root package name */
    private View f686b;

    /* renamed from: c reason: collision with root package name */
    private Bitmap f687c = null;

    /* renamed from: d reason: collision with root package name */
    private e f688d = null;

    public void a(e eVar) {
        this.f688d = eVar;
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.f685a = getActivity();
        this.f686b = layoutInflater.inflate(C0014R.layout.aov_call_action_view, null);
        TextView textView = (TextView) this.f686b.findViewById(C0014R.id.call_name_prompt);
        TextView textView2 = (TextView) this.f686b.findViewById(C0014R.id.call_number_prompt);
        ImageView imageView = (ImageView) this.f686b.findViewById(C0014R.id.call_contact_photo);
        TextView textView3 = (TextView) this.f686b.findViewById(C0014R.id.call_internal_prompt);
        textView3.setOnClickListener(new c(this));
        String a2 = this.f688d.b().a("command");
        if (a2.equals("call")) {
            List c2 = this.f688d.c();
            if (c2 != null && c2.size() == 1) {
                a aVar = (a) c2.get(0);
                String c3 = aVar.c();
                String a3 = f.a(this.f685a, aVar);
                String i = k.i(PhoneNumberUtils.formatNumber(aVar.g()));
                if (!TextUtils.isEmpty(c3)) {
                    textView.setText(c3);
                    textView2.setText(i + "  " + a3);
                } else {
                    textView.setText(i);
                }
                if (!TextUtils.isEmpty(aVar.b())) {
                    this.f687c = f.b(this.f685a, aVar.b());
                }
                if (imageView != null) {
                    if (this.f687c != null) {
                        imageView.setImageBitmap(this.f687c);
                    } else {
                        imageView.setImageResource(C0014R.drawable.vc_default_contact);
                    }
                }
                textView3.setVisibility(g.a(aVar) ? 8 : 0);
            }
        } else if (a2.equals("voicemail")) {
            if (imageView != null) {
                imageView.setImageResource(C0014R.drawable.vc_voicemail);
            }
            textView.setText(C0014R.string.IParseResult_command_voicemail);
            String voiceMailNumber = ((TelephonyManager) this.f685a.getSystemService("phone")).getVoiceMailNumber();
            if (voiceMailNumber != null) {
                textView.setText(voiceMailNumber);
            } else {
                textView.setText(getResources().getString(C0014R.string.voicemail_no_number));
            }
        }
        return this.f686b;
    }

    public void onDestroyView() {
        super.onDestroyView();
        if (this.f687c != null) {
            this.f687c.recycle();
            this.f687c = null;
        }
    }
}
