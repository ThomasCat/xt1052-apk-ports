package com.motorola.audiomonitor.uis;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.uis.training.StartTrainingActivity;
import h.d;
import h.k;

public class AOVWelcomeActivity extends Activity {

    /* renamed from: a reason: collision with root package name */
    private static final String f615a = ("AMonitor." + AOVWelcomeActivity.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private FragmentManager f616b;

    /* renamed from: c reason: collision with root package name */
    private t f617c;

    /* renamed from: d reason: collision with root package name */
    private TextView f618d;

    /* renamed from: e reason: collision with root package name */
    private TextView f619e;

    /* renamed from: f reason: collision with root package name */
    private int f620f;

    private void a() {
        startActivity(new Intent(this, StartTrainingActivity.class));
        finish();
    }

    public void a(int i) {
        if (i == 0) {
            this.f620f = 0;
            this.f617c.a(0);
        } else if (i == 1) {
            this.f620f = 1;
            this.f617c.a(1);
        } else if (i == 2) {
            this.f620f = 2;
            this.f617c.a(2);
        } else if (i == 3) {
            this.f620f = 3;
            this.f618d.setText(C0014R.string.iamin);
            this.f619e.setVisibility(0);
        }
    }

    public void finish() {
        super.finish();
        overridePendingTransition(0, C0014R.anim.activity_close_exit);
    }

    public void onBackPressed() {
        if (this.f620f == 1) {
            a(0);
        } else if (this.f620f == 2) {
            a(1);
        } else if (this.f620f == 3) {
            this.f616b.beginTransaction().replace(C0014R.id.welcome_body_layout, this.f617c, null).commit();
            this.f616b.executePendingTransactions();
            a(2);
            this.f619e.setVisibility(8);
            this.f618d.setText(C0014R.string.next);
            LinearLayout linearLayout = (LinearLayout) findViewById(C0014R.id.footer);
            LayoutParams layoutParams = (LayoutParams) linearLayout.getLayoutParams();
            layoutParams.removeRule(3);
            layoutParams.addRule(12);
            linearLayout.setLayoutParams(layoutParams);
        } else {
            finish();
        }
    }

    public void onCancelClick(View view) {
        if (this.f620f == 3) {
            k.a(getApplicationContext(), "opt_out", true);
            finish();
        }
    }

    public void onContinueClick(View view) {
        if (this.f620f == 0) {
            a(1);
            this.f617c.a(1);
        } else if (this.f620f == 1) {
            a(2);
            this.f617c.a(2);
        } else if (this.f620f == 2) {
            if (d.b(getApplicationContext())) {
                a();
                return;
            }
            this.f616b.beginTransaction().replace(C0014R.id.welcome_body_layout, new s(), null).commit();
            a(3);
        } else if (this.f620f == 3) {
            d.a(getApplicationContext(), 1);
            k.a(getApplicationContext(), "opt_out", false);
            a();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(C0014R.layout.aov_welcome);
        this.f618d = (TextView) findViewById(C0014R.id.confirm);
        this.f619e = (TextView) findViewById(C0014R.id.cancel);
        this.f616b = getFragmentManager();
        this.f617c = new t();
        this.f616b.beginTransaction().replace(C0014R.id.welcome_body_layout, this.f617c, null).commit();
        this.f620f = 0;
    }

    public void onPrivacyClick(View view) {
        Intent intent = new Intent(this, PrivacyActivity.class);
        intent.putExtra("PRIVACY_VIEW_ONLY", true);
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e2) {
            Log.e(f615a, "Error starting privacy policy activity");
        }
    }
}
