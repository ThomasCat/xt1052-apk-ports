package com.nuance.dragon.toolkit.sse;

import android.content.Context;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.NativeUtils;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

class NativeSseImpl implements a {

    /* renamed from: a reason: collision with root package name */
    private static boolean f2101a = false;

    /* renamed from: b reason: collision with root package name */
    private long f2102b;

    public NativeSseImpl(Context context) {
        a(context);
    }

    private void a(Context context) {
        if (!f2101a) {
            boolean isArmV7 = NativeUtils.isArmV7();
            boolean isNeonSupported = NativeUtils.isNeonSupported();
            new StringBuilder("armV7:").append(isArmV7).append(", neonSupported:").append(isNeonSupported);
            if (isArmV7) {
                FileManager fileManager = new FileManager(context, "extralibs/armeabi-v7a");
                if (fileManager.exists("libsse.so")) {
                    fileManager.delete("libsse.so");
                }
                FileManager fileManager2 = new FileManager(context, ".jpg", "extralibs/armeabi-v7a");
                new StringBuilder().append(isNeonSupported ? "neon" : "vfp").append(" sse core lib will be copied");
                FileInputStream openFileForReading = fileManager2.openFileForReading(isNeonSupported ? "libsse_neon.so" : "libsse_vfp.so");
                if (openFileForReading == null) {
                    throw new RuntimeException("source libsse_*.so NOT found in assets/extralibs/armeabi-v7a!");
                }
                FileOutputStream openFileForWriting = fileManager.openFileForWriting("libsse.so");
                if (openFileForWriting == null) {
                    throw new RuntimeException("target libsse.so could NOT be opened for writingextralibs/armeabi-v7a!");
                }
                byte[] bArr = new byte[1024];
                while (true) {
                    try {
                        int read = openFileForReading.read(bArr);
                        if (read < 0) {
                            break;
                        }
                        openFileForWriting.write(bArr, 0, read);
                    } catch (IOException e2) {
                        Logger.error(this, "Error copying file:" + e2.toString());
                        if (openFileForReading != null) {
                            try {
                                openFileForReading.close();
                            } catch (IOException e3) {
                                Logger.error(this, "Error closing file:" + e3.toString());
                            }
                        }
                        if (openFileForWriting != null) {
                            try {
                                openFileForWriting.close();
                            } catch (IOException e4) {
                                Logger.error(this, "Error closing file:" + e4.toString());
                            }
                        }
                        fileManager.delete("libsse.so");
                        throw new RuntimeException("libsse.so could NOT be copied from assets/!");
                    }
                }
                openFileForReading.close();
                openFileForReading = null;
                openFileForWriting.close();
                System.load(context.getFilesDir() + "/extralibs/armeabi-v7a/libsse.so");
            }
            System.loadLibrary("dmt_sse");
            f2101a = true;
        }
    }

    private static native long sseCreate();

    private static native void sseDestroy(long j);

    private static native int sseInitialize(long j, int i, int i2, int i3, byte[] bArr, boolean z, boolean z2, boolean z3);

    private static native int sseProcessWithRef(long j, short[] sArr, short[] sArr2, short[] sArr3);

    private static native int sseProcessWithoutRef(long j, short[] sArr, short[] sArr2);

    public final boolean a() {
        if (this.f2102b != 0) {
            sseDestroy(this.f2102b);
        }
        this.f2102b = sseCreate();
        return this.f2102b != 0;
    }

    public final boolean a(int i, byte[] bArr, boolean z, boolean z2, boolean z3) {
        return this.f2102b != 0 && sseInitialize(this.f2102b, i, 0, 0, bArr, z, z2, z3) == 0;
    }

    public final short[] a(short[] sArr, short[] sArr2) {
        if (this.f2102b != 0) {
            short[] sArr3 = new short[sArr.length];
            if ((sArr2 != null ? sseProcessWithRef(this.f2102b, sArr, sArr2, sArr3) : sseProcessWithoutRef(this.f2102b, sArr, sArr3)) == 0) {
                return sArr3;
            }
        }
        return null;
    }

    public final void b() {
        if (this.f2102b != 0) {
            sseDestroy(this.f2102b);
            this.f2102b = 0;
        }
    }
}
