package com.nuance.dragon.toolkit.recognition;

import com.nuance.dragon.toolkit.cloudservices.recognizer.CloudRecognitionResult;
import com.nuance.dragon.toolkit.data.Data;
import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.data.Data.Integer;
import com.nuance.dragon.toolkit.data.Data.Sequence;
import com.nuance.dragon.toolkit.data.Data.String;
import com.nuance.dragon.toolkit.data.DataSpec;
import com.nuance.dragon.toolkit.recognition.InterpretedRecognition.Phrase;
import com.nuance.dragon.toolkit.recognition.InterpretedRecognition.Word;
import java.util.ArrayList;
import java.util.List;

public class CloudInterpreter implements RecognitionInterpreter {
    public static final String KEY_CHOICES = "choices";
    public static final String KEY_PIECES = "pieces";
    public static final String KEY_SCORE = "score";
    public static final String KEY_TRANSCRIPTION = "trans";

    /* renamed from: a reason: collision with root package name */
    private final DataSpec f1879a;

    public CloudInterpreter(DataSpec dataSpec) {
        this.f1879a = dataSpec;
    }

    public InterpretedRecognition getInterpretedResult(CloudRecognitionResult cloudRecognitionResult) {
        InterpretedRecognition interpretedRecognition = new InterpretedRecognition();
        Dictionary dictionary = cloudRecognitionResult.getDictionary();
        if (dictionary == null) {
            throw new InterpretException("No result dictionary found");
        }
        if (this.f1879a != null) {
            Data process = this.f1879a.process(dictionary);
            dictionary = (process == null || process.getType() != 224) ? null : (Dictionary) process;
        }
        if (dictionary == null) {
            throw new InterpretException("Unable to process result dictionary according to data spec");
        }
        Sequence sequence = dictionary.getSequence(KEY_CHOICES);
        if (sequence == null) {
            throw new InterpretException("No choices found");
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= sequence.size()) {
                return interpretedRecognition;
            }
            Dictionary dictionary2 = sequence.getDictionary(i2);
            if (dictionary2 != null) {
                String string = dictionary2.getString(KEY_TRANSCRIPTION);
                Integer integer = dictionary2.getInt(KEY_SCORE);
                if (string == null) {
                    throw new InterpretException("No transcription found");
                }
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                Sequence sequence2 = dictionary2.getSequence(KEY_PIECES);
                if (sequence2 == null) {
                    arrayList2.add(new Word(string.value, null, null, 0, 0));
                    arrayList.add(new Phrase(null, string.value, arrayList2, null, true));
                } else {
                    int i3 = 0;
                    while (true) {
                        int i4 = i3;
                        if (i4 >= sequence2.size()) {
                            break;
                        }
                        arrayList2.add(new Word(sequence2.getDictionary(i4).getString(KEY_TRANSCRIPTION).value, null, null, 0, 0));
                        i3 = i4 + 1;
                    }
                    arrayList.add(new Phrase(null, string.value, arrayList2, null, true));
                }
                interpretedRecognition.addChoice(string.value, "CLOUD_RECOGNITION", integer == null ? 0 : integer.value, arrayList, null);
            }
            i = i2 + 1;
        }
    }

    public List getUpdateRequiredList(CloudRecognitionResult cloudRecognitionResult) {
        return new ArrayList(0);
    }

    public boolean processForCloud(CloudRecognitionResult cloudRecognitionResult, List list) {
        return false;
    }
}
