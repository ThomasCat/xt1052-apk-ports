package com.nuance.dragon.toolkit.audio.bluetooth.impl;

import android.util.Log;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BtListenerInvocationHandler implements InvocationHandler {
    public static final boolean DEBUG = true;
    public static final String TAG = "BtListenerInvocationHandler";
    protected BtServiceListener mBtServiceListener;

    public BtListenerInvocationHandler(BtServiceListener btServiceListener) {
        this.mBtServiceListener = btServiceListener;
    }

    public Object invoke(Object obj, Method method, Object[] objArr) {
        Log.d(TAG, "BtListenerInvocationHandler.invoke() mBtServiceListener = " + this.mBtServiceListener + " method = " + method);
        if (!(method == null || this.mBtServiceListener == null)) {
            if (method.getName().equals("onServiceConnected")) {
                this.mBtServiceListener.onServiceConnected(new BtServiceListenerParams(objArr));
            } else if (method.getName().equals("onServiceDisconnected")) {
                this.mBtServiceListener.onServiceDisconnected(new BtServiceListenerParams(objArr));
            } else {
                try {
                    method.invoke(this.mBtServiceListener, objArr);
                } catch (IllegalAccessException e2) {
                    Log.e(TAG, "ListenerInvocationHandler.invoke() IllegalAccessException calling " + method, e2);
                } catch (InvocationTargetException e3) {
                    Log.e(TAG, "ListenerInvocationHandler.invoke() InvocationTargetException calling " + method, e3);
                }
            }
        }
        return null;
    }
}
