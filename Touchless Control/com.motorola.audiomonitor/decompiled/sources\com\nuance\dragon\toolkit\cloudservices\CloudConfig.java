package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.util.internal.d;

public class CloudConfig {
    public final String appId;
    public final byte[] appKey;
    public final String host;
    public final AudioType playerCodec;
    public final int port;
    public final AudioType recorderCodec;
    public final SSLConfig sslConfig;

    public CloudConfig(String str, int i, SSLConfig sSLConfig, String str2, byte[] bArr, AudioType audioType, AudioType audioType2) {
        d.a("appId", str2);
        d.a("host", str);
        d.a("port", "greater than 0", i > 0);
        d.a("appKey", (Object) bArr);
        d.a("recorderCodec", (Object) audioType);
        d.a("playerCodec", (Object) audioType2);
        this.host = str;
        this.port = i;
        this.sslConfig = sSLConfig;
        this.appId = str2;
        this.recorderCodec = audioType;
        this.playerCodec = audioType2;
        this.appKey = new byte[bArr.length];
        System.arraycopy(bArr, 0, this.appKey, 0, bArr.length);
    }

    public CloudConfig(String str, int i, String str2, byte[] bArr, AudioType audioType, AudioType audioType2) {
        this(str, i, null, str2, bArr, audioType, audioType2);
    }
}
