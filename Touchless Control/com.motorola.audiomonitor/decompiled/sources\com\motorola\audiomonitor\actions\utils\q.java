package com.motorola.audiomonitor.actions.utils;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.database.ContentObserver;
import android.text.TextUtils;
import java.util.List;

class q extends ContentObserver {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ p f261a;

    public q(p pVar) {
        this.f261a = pVar;
        super(null);
    }

    private boolean a() {
        List runningTasks = ((ActivityManager) this.f261a.f255c.getSystemService("activity")).getRunningTasks(1);
        if (runningTasks == null || runningTasks.size() <= 0) {
            return false;
        }
        return TextUtils.equals(((RunningTaskInfo) runningTasks.get(0)).topActivity.getPackageName(), "com.android.contacts");
    }

    public boolean deliverSelfNotifications() {
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    public void onChange(boolean z) {
        super.onChange(z);
        synchronized (this.f261a.i) {
            if (this.f261a.f260h.isAlive()) {
                long currentTimeMillis = 120000 - (System.currentTimeMillis() - this.f261a.f258f);
                if (a() || currentTimeMillis <= 0) {
                    this.f261a.i.removeCallbacksAndMessages(null);
                    this.f261a.f259g = true;
                    this.f261a.i.post(this.f261a.j);
                } else if (!this.f261a.f259g) {
                    this.f261a.f259g = true;
                    this.f261a.i.postDelayed(this.f261a.j, currentTimeMillis);
                }
            }
        }
    }
}
