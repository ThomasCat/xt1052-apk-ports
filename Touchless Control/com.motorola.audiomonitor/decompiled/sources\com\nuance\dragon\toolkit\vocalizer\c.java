package com.nuance.dragon.toolkit.vocalizer;

import android.os.Handler;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.util.WorkerThread;
import com.nuance.dragon.toolkit.util.internal.d;

final class c extends b {

    /* renamed from: a reason: collision with root package name */
    private WorkerThread f2191a;

    public c(FileManager fileManager) {
        this(fileManager, null);
    }

    public c(FileManager fileManager, Handler handler) {
        super(new NativeVocalizerImpl(fileManager), handler);
        d.a("fileManager", (Object) fileManager);
    }

    /* access modifiers changed from: protected */
    public final Handler getWorkerThreadHandler() {
        this.f2191a = new WorkerThread();
        this.f2191a.start();
        return this.f2191a.getHandler();
    }

    public final void release() {
        super.release();
        if (this.f2191a != null) {
            this.f2191a.stop();
            this.f2191a = null;
        }
    }
}
