package com.nuance.dragon.toolkit.recognition.dictation;

public interface AlterChoices {
    void choose(int i);

    AlterChoice getChoiceAt(int i);

    int size();

    String[] toStringArray();
}
