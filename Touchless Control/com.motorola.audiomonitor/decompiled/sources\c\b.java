package c;

import android.content.ContentResolver;
import android.util.Log;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class b {

    /* renamed from: a reason: collision with root package name */
    private static final String f102a = ("AMonitor" + b.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static boolean f103b;

    /* renamed from: c reason: collision with root package name */
    private static Method f104c;

    /* renamed from: d reason: collision with root package name */
    private static Method f105d;

    /* renamed from: e reason: collision with root package name */
    private static Method f106e;

    /* renamed from: f reason: collision with root package name */
    private static Method f107f;

    /* renamed from: g reason: collision with root package name */
    private static Method f108g;

    /* renamed from: h reason: collision with root package name */
    private static Method f109h;
    private static Method i;
    private static Method j;
    private static Method k;
    private static Method l;
    private static Method m;
    private static Method n;
    private static Constructor o;
    private static Constructor p;
    private final Object q;

    static {
        f103b = false;
        try {
            Class cls = Class.forName("com.motorola.data.event.api.Event");
            Class cls2 = Class.forName("com.motorola.android.provider.CheckinEvent");
            Class cls3 = Class.forName("com.motorola.data.event.api.Segment");
            o = cls2.getDeclaredConstructor(new Class[]{String.class, String.class, String.class});
            p = cls2.getDeclaredConstructor(new Class[]{String.class, String.class, String.class, Long.TYPE});
            f104c = cls.getDeclaredMethod("addSegment", new Class[]{cls3});
            f105d = cls.getDeclaredMethod("getEventName", new Class[0]);
            f106e = cls.getDeclaredMethod("getTagName", new Class[0]);
            f107f = cls.getDeclaredMethod("getTimestamp", new Class[0]);
            f108g = cls.getDeclaredMethod("getVersion", new Class[0]);
            f109h = cls.getDeclaredMethod("serializeEvent", new Class[0]);
            i = cls.getDeclaredMethod("setValue", new Class[]{String.class, Boolean.TYPE});
            j = cls.getDeclaredMethod("setValue", new Class[]{String.class, Double.TYPE});
            k = cls.getDeclaredMethod("setValue", new Class[]{String.class, Integer.TYPE});
            l = cls.getDeclaredMethod("setValue", new Class[]{String.class, Long.TYPE});
            m = cls.getDeclaredMethod("setValue", new Class[]{String.class, String.class});
            n = cls2.getDeclaredMethod("publish", new Class[]{Object.class});
            f103b = true;
        } catch (Throwable th) {
            Log.w(f102a, "Unable to get checkin class.");
            f103b = false;
        }
    }

    public b(String str, String str2, String str3) {
        Object obj = null;
        if (f103b && str != null && !str.isEmpty() && str2 != null && !str2.isEmpty() && str3 != null && !str3.isEmpty()) {
            try {
                obj = o.newInstance(new Object[]{str, str2, str3});
            } catch (Throwable th) {
                Log.w(f102a, "Unable to instantiate CheckinEvent.");
            }
        }
        this.q = obj;
    }

    public void a(ContentResolver contentResolver) {
        if (f103b && this.q != null && contentResolver != null) {
            try {
                n.invoke(this.q, new Object[]{contentResolver});
            } catch (Throwable th) {
                Log.w(f102a, "Unable to publish.");
            }
        }
    }

    public void a(c cVar) {
        if (f103b && this.q != null && cVar != null) {
            try {
                f104c.invoke(this.q, new Object[]{cVar.a()});
            } catch (Throwable th) {
                Log.w(f102a, "Unable to add Segment.");
            }
        }
    }
}
