package com.nuance.dragon.toolkit.audio.bluetooth.impl;

public class BtServiceListenerParams {

    /* renamed from: a reason: collision with root package name */
    private int f1042a;

    /* renamed from: b reason: collision with root package name */
    private Object f1043b;

    BtServiceListenerParams(Object... objArr) {
        if (objArr != null) {
            if (objArr.length > 1) {
                this.f1043b = objArr[1];
            }
            if (objArr.length > 0) {
                this.f1042a = objArr[0].intValue();
            }
        }
    }

    public int getProfile() {
        return this.f1042a;
    }

    public Object getProxy() {
        return this.f1043b;
    }

    public final String toString() {
        return "BtServiceListenerParams{" + this.f1042a + ", " + this.f1043b + "}";
    }
}
