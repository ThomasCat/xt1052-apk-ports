package com.nuance.dragon.toolkit.audio.bluetooth.impl;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;

public class BluetoothHeadset_3_0 {
    public static final String ACTION_AUDIO_STATE_CHANGED;
    public static final String ACTION_STATE_CHANGED;
    protected static final String ADAPTER_CLASSNAME = "android.bluetooth.BluetoothAdapter";
    public static final int AUDIO_STATE_CONNECTED;
    public static final int AUDIO_STATE_CONNECTING;
    public static final int AUDIO_STATE_DISCONNECTED;
    protected static final String DEVICE_CLASSNAME = "android.bluetooth.BluetoothDevice";
    public static final String EXTRA_AUDIO_STATE;
    public static final String EXTRA_STATE;
    protected static final String HEADSET_CLASSNAME = "android.bluetooth.BluetoothHeadset";
    protected static final String LISTENER_CLASSNAME = "android.bluetooth.BluetoothProfile$ServiceListener";
    protected static final String PROFILE_CLASSNAME = "android.bluetooth.BluetoothProfile";
    public static final int STATE_CONNECTED;
    public static final int STATE_CONNECTING;
    public static final int STATE_DISCONNECTED;

    /* renamed from: b reason: collision with root package name */
    private static final Method f1030b;

    /* renamed from: c reason: collision with root package name */
    private static final Method f1031c = ReflectionFactory.getMethodFromStrings(HEADSET_CLASSNAME, "startVoiceRecognition", DEVICE_CLASSNAME);

    /* renamed from: d reason: collision with root package name */
    private static final Method f1032d = ReflectionFactory.getMethodFromStrings(HEADSET_CLASSNAME, "stopVoiceRecognition", DEVICE_CLASSNAME);

    /* renamed from: e reason: collision with root package name */
    private static final Method f1033e;

    /* renamed from: f reason: collision with root package name */
    private static final Method f1034f;

    /* renamed from: g reason: collision with root package name */
    private static final Method f1035g;

    /* renamed from: h reason: collision with root package name */
    private static final Method f1036h = ReflectionFactory.getMethodFromStrings(ADAPTER_CLASSNAME, "getProfileProxy", "android.content.Context", LISTENER_CLASSNAME, "int");
    private static final Method i;
    private static final Class j;
    private static final int k;

    /* renamed from: a reason: collision with root package name */
    Handler f1037a = new Handler();
    private BluetoothAdapter l = BluetoothAdapter.getDefaultAdapter();
    /* access modifiers changed from: private */
    public Object m;
    /* access modifiers changed from: private */
    public ServiceListener n;
    /* access modifiers changed from: private */
    public Runnable o = new Runnable() {
        public final void run() {
            BluetoothHeadset_3_0.this.n.onServiceConnected(true);
        }
    };
    /* access modifiers changed from: private */
    public Runnable p = new Runnable() {
        public final void run() {
            BluetoothHeadset_3_0.this.n.onServiceConnected(false);
        }
    };
    /* access modifiers changed from: private */
    public Runnable q = new Runnable() {
        public final void run() {
            BluetoothHeadset_3_0.this.n.onServiceDisconnected();
        }
    };

    public interface ServiceListener {
        void onServiceConnected(boolean z);

        void onServiceDisconnected();
    }

    static {
        Reflection reflection = new Reflection();
        Class classForName = reflection.getClassForName(HEADSET_CLASSNAME);
        Class classForName2 = reflection.getClassForName(LISTENER_CLASSNAME);
        Class classForName3 = reflection.getClassForName(PROFILE_CLASSNAME);
        f1030b = reflection.getMethod(classForName, "getConnectionState", BluetoothDevice.class);
        f1033e = reflection.getMethod(classForName, "getConnectedDevices", new Class[0]);
        f1035g = reflection.getMethod(classForName, "connect", BluetoothDevice.class);
        f1034f = reflection.getMethod(classForName, "disconnect", BluetoothDevice.class);
        i = ReflectionFactory.getMethod(ADAPTER_CLASSNAME, "closeProfileProxy", Integer.TYPE, classForName3);
        j = classForName2;
        k = ((Integer) reflection.getFieldValue(classForName3, "HEADSET")).intValue();
        ACTION_STATE_CHANGED = (String) reflection.getFieldValue(classForName, "ACTION_CONNECTION_STATE_CHANGED");
        ACTION_AUDIO_STATE_CHANGED = (String) reflection.getFieldValue(classForName, "ACTION_AUDIO_STATE_CHANGED");
        EXTRA_STATE = (String) reflection.getFieldValue(classForName, "EXTRA_STATE");
        EXTRA_AUDIO_STATE = (String) reflection.getFieldValue(classForName, "EXTRA_STATE");
        STATE_DISCONNECTED = ((Integer) reflection.getFieldValue(classForName3, "STATE_DISCONNECTED")).intValue();
        STATE_CONNECTING = ((Integer) reflection.getFieldValue(classForName3, "STATE_CONNECTING")).intValue();
        STATE_CONNECTED = ((Integer) reflection.getFieldValue(classForName3, "STATE_CONNECTED")).intValue();
        AUDIO_STATE_DISCONNECTED = ((Integer) reflection.getFieldValue(classForName, "STATE_AUDIO_DISCONNECTED")).intValue();
        AUDIO_STATE_CONNECTED = ((Integer) reflection.getFieldValue(classForName, "STATE_AUDIO_CONNECTED")).intValue();
        AUDIO_STATE_CONNECTING = ((Integer) reflection.getFieldValue(classForName, "STATE_AUDIO_CONNECTING")).intValue();
    }

    public BluetoothHeadset_3_0(Context context, ServiceListener serviceListener) {
        this.n = serviceListener;
        Object newProxyInstance = Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{j}, new InvocationHandler() {
            public final Object invoke(Object obj, Method method, Object[] objArr) {
                Log.i("BluetoothHeadset_3_0", "onService_Callback: method.getName() is <" + method.getName() + ">");
                if (method.getName().equals("onServiceConnected")) {
                    BluetoothHeadset_3_0.this.m = objArr[1];
                    Log.i("BluetoothHeadset_3_0", "onServiceConnected: mBluetoothHeadset<" + BluetoothHeadset_3_0.this.m + ">");
                    if (BluetoothHeadset_3_0.this.m != null) {
                        BluetoothHeadset_3_0.this.f1037a.post(BluetoothHeadset_3_0.this.o);
                    } else {
                        BluetoothHeadset_3_0.this.f1037a.post(BluetoothHeadset_3_0.this.p);
                    }
                } else if (method.getName().equals("onServiceDisconnected")) {
                    BluetoothHeadset_3_0.this.m = null;
                    Log.i("BluetoothHeadset_3_0", "onServiceDisconnected: mBluetoothHeadset<" + BluetoothHeadset_3_0.this.m + ">");
                    BluetoothHeadset_3_0.this.f1037a.post(BluetoothHeadset_3_0.this.q);
                }
                return null;
            }
        });
        try {
            if (!((Boolean) f1036h.invoke(this.l, new Object[]{context, newProxyInstance, Integer.valueOf(k)})).booleanValue()) {
                throw new RuntimeException("Failed to get BT profile proxy");
            }
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("IllegalAccessException calling sGetProfileProxy");
        } catch (InvocationTargetException e3) {
            throw new RuntimeException("InvocationTargetException calling sGetProfileProxy");
        }
    }

    public void close() {
        Log.d("BluetoothHeadset_3_0", "close()");
        try {
            if (this.m != null) {
                Log.d("BluetoothHeadset_3_0", "close(), calling closeProfileProxy()");
                i.invoke(this.l, new Object[]{Integer.valueOf(k), this.m});
            }
        } catch (Exception e2) {
            Log.d("BluetoothHeadset_3_0", "close(), exception()");
            e2.printStackTrace();
        }
    }

    public boolean connectHeadset(BluetoothDevice bluetoothDevice) {
        try {
            if (f1035g != null) {
                return ((Boolean) f1035g.invoke(this.m, new Object[]{bluetoothDevice})).booleanValue();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return false;
    }

    public void disconnectHeadset() {
        try {
            if (f1034f != null) {
                f1034f.invoke(this.m, new Object[0]);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void disconnectHeadset(BluetoothDevice bluetoothDevice) {
        try {
            if (f1034f != null) {
                f1034f.invoke(this.m, new Object[]{bluetoothDevice});
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public BluetoothDevice getConnectedDevice() {
        try {
            if (!(this.m == null || f1033e == null)) {
                List list = (List) f1033e.invoke(this.m, new Object[0]);
                if (list != null && list.size() > 0) {
                    return (BluetoothDevice) list.get(0);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public int getState() {
        Exception exc;
        Object obj;
        int i2;
        int i3 = STATE_DISCONNECTED;
        Object obj2 = null;
        try {
            BluetoothDevice connectedDevice = getConnectedDevice();
            try {
                if (this.m == null || connectedDevice == null) {
                    i2 = i3;
                } else {
                    i2 = ((Integer) f1030b.invoke(this.m, new Object[]{connectedDevice})).intValue();
                }
                i3 = i2;
                obj = connectedDevice;
            } catch (Exception e2) {
                Exception exc2 = e2;
                obj = connectedDevice;
                exc = exc2;
                exc.printStackTrace();
                Log.i("BluetoothHeadset_3_0", "getState: state = <" + i3 + ">, device = <" + obj + ">, mBluetoothHeadset = <" + this.m + ">");
                return i3;
            }
        } catch (Exception e3) {
            Exception exc3 = e3;
            obj = obj2;
            exc = exc3;
        }
        Log.i("BluetoothHeadset_3_0", "getState: state = <" + i3 + ">, device = <" + obj + ">, mBluetoothHeadset = <" + this.m + ">");
        return i3;
    }

    public boolean startVoiceRecognition() {
        try {
            return ((Boolean) f1031c.invoke(this.m, new Object[]{getConnectedDevice()})).booleanValue();
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public boolean stopVoiceRecognition() {
        try {
            return ((Boolean) f1032d.invoke(this.m, new Object[]{getConnectedDevice()})).booleanValue();
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }
}
