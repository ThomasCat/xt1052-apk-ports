package com.nuance.dragon.toolkit.cloudservices.recognizer;

import com.nuance.dragon.toolkit.cloudservices.TransactionResult;
import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import org.json.JSONObject;

public class CloudRecognitionResult implements JSONCompliant {
    protected static final String KEY_RESULT = "res";

    /* renamed from: a reason: collision with root package name */
    private final TransactionResult f1475a;

    public CloudRecognitionResult(TransactionResult transactionResult) {
        this.f1475a = transactionResult;
    }

    public static CloudRecognitionResult createFromJSON(JSONObject jSONObject) {
        return new CloudRecognitionResult(TransactionResult.createFromJSON(jSONObject.getJSONObject(KEY_RESULT)));
    }

    public Dictionary getDictionary() {
        return this.f1475a.getContents();
    }

    public String getType() {
        return this.f1475a.getResultType();
    }

    public boolean isFinal() {
        return this.f1475a.isFinal();
    }

    public JSONObject toJSON() {
        b bVar = new b();
        bVar.a(KEY_RESULT, (Object) this.f1475a.toJSON());
        return bVar;
    }
}
