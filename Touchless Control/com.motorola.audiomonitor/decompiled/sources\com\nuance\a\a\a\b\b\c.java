package com.nuance.a.a.a.b.b;

public final class c {

    /* renamed from: a reason: collision with root package name */
    private String f823a;

    /* renamed from: b reason: collision with root package name */
    private Object f824b;

    /* renamed from: c reason: collision with root package name */
    private a f825c;

    public final class a extends com.nuance.a.a.a.a.d.c {

        /* renamed from: a reason: collision with root package name */
        public static final a f826a = new a(0);

        /* renamed from: b reason: collision with root package name */
        public static final a f827b = new a(1);

        /* renamed from: c reason: collision with root package name */
        public static final a f828c = new a(2);

        /* renamed from: d reason: collision with root package name */
        public static final a f829d = new a(3);

        /* renamed from: e reason: collision with root package name */
        public static final a f830e = new a(4);

        /* renamed from: f reason: collision with root package name */
        public static final a f831f = new a(5);

        /* renamed from: g reason: collision with root package name */
        public static final a f832g = new a(6);

        /* renamed from: h reason: collision with root package name */
        public static final a f833h = new a(7);
        public static final a i = new a(8);
        public static final a j = new a(9);

        private a(short s) {
            super(s);
        }

        public final String toString() {
            if (a(f827b)) {
                return "nmsp";
            }
            if (a(f828c)) {
                return "app";
            }
            if (a(f829d)) {
                return "nss";
            }
            if (a(f830e)) {
                return "slog";
            }
            if (a(f831f)) {
                return "nsslog";
            }
            if (a(f832g)) {
                return "gwlog";
            }
            if (a(f833h)) {
                return "svsp";
            }
            if (a(i)) {
                return "sip";
            }
            if (a(j)) {
                return "sdp";
            }
            if (a(f826a)) {
                return "sdk";
            }
            return null;
        }
    }

    public c(String str, a aVar) {
        this.f823a = str;
        this.f824b = new byte[0];
        this.f825c = aVar;
    }

    public c(String str, Object obj, a aVar) {
        this.f823a = str;
        this.f824b = obj;
        this.f825c = aVar;
    }

    public final String a() {
        return this.f823a;
    }

    public final byte[] b() {
        return this.f824b instanceof byte[] ? (byte[]) this.f824b : "THIS IS NOT A STRING PARAMETER!!!".getBytes();
    }

    public final Object c() {
        return this.f824b;
    }

    public final a d() {
        return this.f825c;
    }

    /* renamed from: e */
    public final c clone() {
        Object obj = this.f824b;
        if (this.f824b instanceof byte[]) {
            byte[] bArr = (byte[]) this.f824b;
            Object obj2 = new byte[bArr.length];
            System.arraycopy(bArr, 0, obj2, 0, bArr.length);
            obj = obj2;
        }
        return new c(this.f823a, obj, this.f825c);
    }
}
