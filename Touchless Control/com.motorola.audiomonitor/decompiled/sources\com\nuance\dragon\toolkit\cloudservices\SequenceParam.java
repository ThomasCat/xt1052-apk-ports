package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.dragon.toolkit.data.Data.Sequence;
import com.nuance.dragon.toolkit.util.Logger;

public class SequenceParam extends Param {
    public SequenceParam(String str, Sequence sequence) {
        super(7, str);
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(d dVar, a aVar) {
        Logger.warn(this, "Sending single sequence param is not supported, sequence param should be wrapped by DictionaryParam");
        return false;
    }
}
