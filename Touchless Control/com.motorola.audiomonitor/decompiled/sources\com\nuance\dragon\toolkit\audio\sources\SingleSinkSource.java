package com.nuance.dragon.toolkit.audio.sources;

import com.nuance.dragon.toolkit.audio.AbstractAudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.util.Logger;

public abstract class SingleSinkSource extends AudioSource {

    /* renamed from: a reason: collision with root package name */
    private AudioSink f1296a;

    /* access modifiers changed from: protected */
    public final void audioSinkConnected(AudioSink audioSink) {
        this.f1296a = audioSink;
        onSinkConnected(audioSink);
    }

    /* access modifiers changed from: protected */
    public final void audioSinkDisconnected(AudioSink audioSink) {
        if (audioSink == this.f1296a) {
            this.f1296a = null;
            onSinkDisconnected(audioSink);
            return;
        }
        Logger.warn(this, "Wrong sink disconnected");
    }

    /* access modifiers changed from: protected */
    public abstract AbstractAudioChunk getAudioChunk();

    public final AbstractAudioChunk getAudioChunkForSink(AudioSink audioSink) {
        if (audioSink == this.f1296a) {
            return getAudioChunk();
        }
        return null;
    }

    public final int getChunksAvailableForSink(AudioSink audioSink) {
        if (audioSink == this.f1296a) {
            return getChunksAvailable();
        }
        return 0;
    }

    public final boolean isEmptyForSink(AudioSink audioSink) {
        if (audioSink == this.f1296a) {
            return isEmpty();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void notifyChunksAvailable() {
        if (this.f1296a != null) {
            this.f1296a.chunksAvailable(this);
        }
    }

    /* access modifiers changed from: protected */
    public void notifyFramesDropped() {
        if (this.f1296a != null) {
            this.f1296a.framesDropped(this);
        }
    }

    /* access modifiers changed from: protected */
    public void notifySourceClosed() {
        if (this.f1296a != null) {
            this.f1296a.sourceClosed(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onSinkConnected(AudioSink audioSink) {
    }

    /* access modifiers changed from: protected */
    public void onSinkDisconnected(AudioSink audioSink) {
    }
}
