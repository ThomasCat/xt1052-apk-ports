package d;

import android.content.Context;
import android.util.Log;
import b.b;
import b.d;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.GenericRecognizer;
import com.motorola.audiomonitor.ProtectedByteBuffer;
import com.motorola.audiomonitor.n;
import com.motorola.audiomonitor.pluginservice.InputPluginService;
import h.g;
import java.util.List;

public class a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f2192a = ("AMonitor." + a.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f2193b = com.motorola.audiomonitor.a.f160b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final GenericRecognizer f2194c = new GenericRecognizer();

    public a(Context context) {
        new b(this, context.getResources().getStringArray(C0014R.array.commands_call)).start();
    }

    public b a(b.a aVar) {
        d dVar;
        n nVar;
        d dVar2 = new d("call");
        float[] fArr = new float[1];
        String[] strArr = new String[1];
        synchronized (this.f2194c) {
            this.f2194c.reset();
            n nVar2 = n.NONE;
            List<ProtectedByteBuffer> a2 = aVar.a();
            String b2 = aVar.b();
            int i = 0;
            int i2 = 0;
            for (ProtectedByteBuffer protectedByteBuffer : a2) {
                if (nVar2 == n.NONE) {
                    nVar = this.f2194c.recognize(protectedByteBuffer);
                    i2 += protectedByteBuffer.capacity();
                    if (nVar == n.RECOGNIZED) {
                        i = protectedByteBuffer.capacity() + i;
                        nVar2 = nVar;
                    }
                }
                nVar = nVar2;
                i = protectedByteBuffer.capacity() + i;
                nVar2 = nVar;
            }
            if (nVar2 == n.RECOGNIZED) {
                int recognizedWords = this.f2194c.getRecognizedWords(0.0f, fArr, strArr);
                if (recognizedWords > 0) {
                    float f2 = ((float) i) / 32000.0f;
                    float f3 = ((float) (i - i2)) / 32000.0f;
                    if (f2193b) {
                        Log.d(f2192a, "call command [" + fArr[0] + "] " + strArr[0] + " total=" + f2 + " rest=" + f3 + " cmd=" + b2);
                    }
                    if (f3 > 5.0f) {
                        dVar = null;
                        g.a().a(recognizedWords, fArr, strArr);
                        InputPluginService.b().a("local_command_spotter_results", recognizedWords, fArr, strArr);
                    }
                }
                dVar = dVar2;
                g.a().a(recognizedWords, fArr, strArr);
                InputPluginService.b().a("local_command_spotter_results", recognizedWords, fArr, strArr);
            } else {
                dVar = dVar2;
            }
        }
        return dVar;
    }

    public void a() {
        synchronized (this.f2194c) {
            this.f2194c.cleanUp();
        }
    }
}
