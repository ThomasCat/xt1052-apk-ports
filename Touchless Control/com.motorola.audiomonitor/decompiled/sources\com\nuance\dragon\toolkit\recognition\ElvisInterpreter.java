package com.nuance.dragon.toolkit.recognition;

import com.nuance.dragon.toolkit.elvis.ElvisNbestList.Entry;
import com.nuance.dragon.toolkit.elvis.ElvisNbestList.Entry.Word;
import com.nuance.dragon.toolkit.elvis.ElvisResult;
import com.nuance.dragon.toolkit.recognition.InterpretedRecognition.Phrase;
import com.nuance.dragon.toolkit.util.internal.f;
import com.nuance.dragon.toolkit.util.internal.h;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ElvisInterpreter implements RecognitionInterpreter {
    public InterpretedRecognition getInterpretedResult(ElvisResult elvisResult) {
        boolean z;
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        InterpretedRecognition interpretedRecognition = new InterpretedRecognition();
        Iterator it = elvisResult.getChoiceList().iterator();
        while (it.hasNext()) {
            Entry entry = (Entry) it.next();
            ArrayList arrayList = new ArrayList();
            List words = entry.getWords();
            ArrayList arrayList2 = new ArrayList(words.size());
            if (words.isEmpty()) {
                throw new InterpretException("Elvis Nbest list can't be empty");
            }
            String str8 = null;
            boolean z2 = false;
            String str9 = null;
            String str10 = null;
            String str11 = null;
            int size = words.size();
            int i = -1;
            Word word = null;
            while (i < size) {
                Word word2 = i + 1 < size ? (Word) words.get(i + 1) : null;
                if (word2 != null) {
                    int indexOf = word2.wordSlot == null ? -1 : word2.wordSlot.indexOf(126);
                    boolean z3 = indexOf >= 0;
                    z = z3;
                    str = z3 ? word2.wordSlot.substring(0, indexOf) : word2.wordSlot;
                } else {
                    z = z2;
                    str = str8;
                }
                if (word != null) {
                    boolean z4 = word2 == null || word.isGenericSpeech || word2.isGenericSpeech || !f.a(str8, str);
                    String a2 = h.a(str9, word.surfaceForm);
                    String a3 = h.a(str10, word.fullPhrase);
                    String a4 = h.a(str11, word.spokenForm);
                    if (!z2 || z4) {
                        arrayList2.add(new InterpretedRecognition.Word(a2, a3, a4, 0, 0));
                        str7 = null;
                        str6 = null;
                        str5 = null;
                    } else {
                        str7 = a4;
                        str6 = a3;
                        str5 = a2;
                    }
                    if (z4) {
                        arrayList.add(new Phrase(str8, h.a((Iterable) arrayList2, " "), arrayList2, null, !word.isGenericSpeech));
                        arrayList2.clear();
                    }
                    str2 = str7;
                    str4 = str6;
                    str3 = str5;
                } else {
                    str2 = str11;
                    String str12 = str10;
                    str3 = str9;
                    str4 = str12;
                }
                i++;
                z2 = z;
                str8 = str;
                word = word2;
                str11 = str2;
                String str13 = str4;
                str9 = str3;
                str10 = str13;
            }
            interpretedRecognition.addChoice(entry.toString(), entry.getConstraint(), entry.getScore(), arrayList, null);
        }
        return interpretedRecognition;
    }

    public List getUpdateRequiredList(ElvisResult elvisResult) {
        return new ArrayList(0);
    }

    public boolean processForCloud(ElvisResult elvisResult, List list) {
        return !((Entry) elvisResult.getChoiceList().get(0)).hasGenericSpeech();
    }
}
