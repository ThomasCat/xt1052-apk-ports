package com.nuance.a.a.a.b.c.b.b;

import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;

public class m {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f871a = a.a(m.class);

    public static l a(byte[] bArr) {
        short s = (short) (((bArr[1] & 255) << 8) + (bArr[0] & 255));
        byte[] bArr2 = new byte[(bArr.length - 2)];
        System.arraycopy(bArr, 2, bArr2, 0, bArr2.length);
        switch (s) {
            case 29185:
                return new t(bArr2);
            case 29186:
                return new r(bArr2);
            case 29189:
                return new u(bArr2);
            default:
                f871a.e("PDXMessageFactory.createMessage() Unknown command: " + s + ". ");
                return null;
        }
    }
}
