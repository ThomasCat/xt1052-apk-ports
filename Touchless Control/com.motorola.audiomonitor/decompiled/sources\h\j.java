package h;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Profile;
import android.util.Log;
import com.motorola.audiomonitor.a;
import java.util.HashMap;
import java.util.Map;

public class j {

    /* renamed from: a reason: collision with root package name */
    private static final String f2303a = ("AMonitor." + j.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f2304b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private final Context f2305c;

    /* renamed from: d reason: collision with root package name */
    private final Map f2306d = new HashMap();

    public j(Context context) {
        this.f2305c = context;
    }

    public Map a() {
        if (this.f2305c == null) {
            if (f2304b) {
                Log.d(f2303a, "Null context when trying to get user's name");
            }
            return new HashMap();
        }
        synchronized (this.f2306d) {
            this.f2306d.clear();
        }
        Cursor query = this.f2305c.getContentResolver().query(Profile.CONTENT_URI, new String[]{"_id", "display_name"}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    while (true) {
                        if (query.isAfterLast()) {
                            break;
                        }
                        long j = query.getLong(query.getColumnIndex("_id"));
                        String string = query.getString(query.getColumnIndex("display_name"));
                        if (string != null) {
                            if (f2304b) {
                                Log.d(f2303a, "Storing display name: " + string);
                            }
                            synchronized (this.f2306d) {
                                this.f2306d.put("display_name", string);
                            }
                        }
                        String[] strArr = {"vnd.android.cursor.item/name"};
                        query = this.f2305c.getContentResolver().query(Uri.withAppendedPath(ContentUris.withAppendedId(Contacts.CONTENT_URI, j), "data"), null, "mimetype=?", strArr, null);
                        if (query != null) {
                            try {
                                if (query.moveToFirst()) {
                                    String string2 = query.getString(query.getColumnIndex("data4"));
                                    String string3 = query.getString(query.getColumnIndex("data2"));
                                    String string4 = query.getString(query.getColumnIndex("data5"));
                                    String string5 = query.getString(query.getColumnIndex("data3"));
                                    String string6 = query.getString(query.getColumnIndex("data6"));
                                    synchronized (this.f2306d) {
                                        if (string2 != null) {
                                            if (f2304b) {
                                                Log.d(f2303a, "Storing prefix: " + string2);
                                            }
                                            this.f2306d.put("data4", string2);
                                        }
                                        if (string3 != null) {
                                            if (f2304b) {
                                                Log.d(f2303a, "Storing given name: " + string3);
                                            }
                                            this.f2306d.put("data2", string3);
                                        }
                                        if (string4 != null) {
                                            if (f2304b) {
                                                Log.d(f2303a, "Storing middle name: " + string4);
                                            }
                                            this.f2306d.put("data5", string4);
                                        }
                                        if (string5 != null) {
                                            if (f2304b) {
                                                Log.d(f2303a, "Storing family name: " + string5);
                                            }
                                            this.f2306d.put("data3", string5);
                                        }
                                        if (string6 != null) {
                                            if (f2304b) {
                                                Log.d(f2303a, "Storing suffix: " + string6);
                                            }
                                            this.f2306d.put("data6", string6);
                                        }
                                    }
                                    if (string3 != null) {
                                        break;
                                    }
                                }
                                query.close();
                            } finally {
                                query.close();
                            }
                        }
                        query.moveToNext();
                    }
                    while (true) {
                    }
                }
                query.close();
            } finally {
                query.close();
            }
        }
        return new HashMap(this.f2306d);
    }
}
