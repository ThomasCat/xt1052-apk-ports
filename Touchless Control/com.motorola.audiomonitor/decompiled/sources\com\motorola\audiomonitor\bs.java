package com.motorola.audiomonitor;

import android.util.Log;

class bs implements Runnable {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ s f368a;

    /* renamed from: b reason: collision with root package name */
    final /* synthetic */ ba f369b;

    bs(ba baVar, s sVar) {
        this.f369b = baVar;
        this.f368a = sVar;
    }

    public void run() {
        if (ba.f339c) {
            Log.d(ba.f338b, "Handling a trigger from an other source. Reset");
        }
        this.f368a.c();
    }
}
