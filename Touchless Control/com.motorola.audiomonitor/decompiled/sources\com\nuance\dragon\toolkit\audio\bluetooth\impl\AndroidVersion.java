package com.nuance.dragon.toolkit.audio.bluetooth.impl;

import android.os.Build;
import android.os.Build.VERSION;

public class AndroidVersion {
    public static final int BASE = 1;
    public static final int BASE_1_1 = 2;
    public static final int CUPCAKE = 3;
    public static final int CUR_DEVELOPMENT = 10000;
    public static final int DONUT = 4;
    public static final int ECLAIR = 5;
    public static final int ECLAIR_0_1 = 6;
    public static final int ECLAIR_MR1 = 7;
    public static final int FROYO = 8;
    public static final int GINGERBREAD = 9;
    public static final boolean HAS_WORKING_A2DP_CONTROL;
    public static final int HONEYCOMB = 11;
    public static final int ICE_CREAM_SANDWICH = 14;
    public static final boolean IS_BROKEN_HTC;
    public static final boolean IS_BROKEN_MOTOROLA_i1;
    public static final boolean IS_BROKEN_SAMSUNG;
    public static final String MANUFACTURER;
    public static final int SDK = Integer.parseInt(VERSION.SDK);
    public static final boolean USE_8K_FOR_BLUETOOTH_PLAYBACK;
    public static final boolean USE_MUSIC_STREAM_FOR_BLUETOOTH = (IS_BROKEN_SAMSUNG || IS_BROKEN_MOTOROLA_i1);

    static {
        String str;
        boolean z = true;
        try {
            str = (String) Build.class.getField("MANUFACTURER").get(null);
        } catch (Exception e2) {
            str = "";
        }
        MANUFACTURER = str;
        String str2 = Build.MODEL;
        IS_BROKEN_SAMSUNG = str.equalsIgnoreCase("samsung") && (str2.equalsIgnoreCase("SGH-T959") || str2.equalsIgnoreCase("SAMSUNG-SGH-I897") || str2.equalsIgnoreCase("SGH-I897") || str2.equalsIgnoreCase("GT-I9000"));
        IS_BROKEN_HTC = str.equalsIgnoreCase("htc") && (str2.equalsIgnoreCase("PC36100") || str2.equalsIgnoreCase("ADR6300") || str2.equalsIgnoreCase("HTC Glacier") || str2.equalsIgnoreCase("T-Mobile myTouch 4G") || str2.equalsIgnoreCase("T-Mobile G2"));
        IS_BROKEN_MOTOROLA_i1 = str2.equalsIgnoreCase("Motorola_i1");
        USE_8K_FOR_BLUETOOTH_PLAYBACK = str.equalsIgnoreCase("Motorola") && str2.equalsIgnoreCase("MB860") && SDK == 8;
        if (IS_BROKEN_SAMSUNG) {
            z = false;
        }
        HAS_WORKING_A2DP_CONTROL = z;
    }
}
