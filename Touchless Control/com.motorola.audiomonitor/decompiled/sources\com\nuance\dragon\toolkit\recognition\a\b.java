package com.nuance.dragon.toolkit.recognition.a;

import com.nuance.dragon.toolkit.recognition.a.a.C0010a;
import com.nuance.dragon.toolkit.recognition.dictation.DictationResult;
import com.nuance.dragon.toolkit.recognition.dictation.a.g;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class b extends g implements a {

    /* renamed from: a reason: collision with root package name */
    private g f1912a;

    /* renamed from: b reason: collision with root package name */
    private final List f1913b = new ArrayList();

    /* renamed from: c reason: collision with root package name */
    private final List f1914c = new ArrayList();

    /* renamed from: d reason: collision with root package name */
    private final Map f1915d = new HashMap();

    /* renamed from: e reason: collision with root package name */
    private String f1916e;

    /* renamed from: f reason: collision with root package name */
    private final double f1917f;

    public b(double d2) {
        this.f1917f = d2;
    }

    public final List a() {
        return this.f1914c;
    }

    public final void a(C0010a aVar) {
        this.f1913b.add(aVar);
    }

    public final void a(g gVar) {
        this.f1912a = gVar;
    }

    public final void a(String str) {
        this.f1916e = str;
    }

    public final void a(String str, DictationResult dictationResult) {
        com.nuance.dragon.toolkit.recognition.a.a.b bVar = new com.nuance.dragon.toolkit.recognition.a.a.b(str, dictationResult);
        this.f1914c.add(bVar);
        this.f1915d.put(str, bVar);
    }

    public final void a(String str, String str2, double d2) {
        com.nuance.dragon.toolkit.recognition.a.a.b bVar = new com.nuance.dragon.toolkit.recognition.a.a.b(str, str2, d2);
        this.f1914c.add(bVar);
        this.f1915d.put(str, bVar);
    }

    public final String a_() {
        return this.f1916e;
    }

    public final g d() {
        return this.f1912a;
    }

    public final double getConfidenceScore() {
        return this.f1917f;
    }

    public final String toString() {
        return this.f1912a != null ? this.f1912a.toString() : "";
    }
}
