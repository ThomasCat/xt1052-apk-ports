package com.nuance.dragon.toolkit.vocalizer;

class a {
    public static final VocalizerLanguage[] ALL_LANGUAGES = {JAPANESE, ITALIAN, MANDARIN_TRADITIONAL, BRAZILIAN_PORTUGUESE, LATIN_AMERICAN_SPANISH, MANDARIN_SIMPLIFIED, EUROPEAN_SPANISH, EUROPEAN_PORTUGUESE, CANADIAN_FRENCH, AUSTRALIAN_ENGLISH, KOREAN, EUROPEAN_FRENCH, DUTCH, BRITISH_ENGLISH, GREEK, FINNISH, UNITED_STATES_ENGLISH, SWEDISH, DANISH, NORWEGIAN, POLISH, GERMAN, RUSSIAN, CANTONESE_TRADITIONAL, TURKISH};
    public static final VocalizerLanguage AUSTRALIAN_ENGLISH = new VocalizerLanguage("ena", "Australian English", new String[]{"karen", "lee"}, new String[]{"Karen", "Lee"});
    public static final VocalizerLanguage BRAZILIAN_PORTUGUESE = new VocalizerLanguage("ptb", "Brazilian Portuguese", new String[]{"raquel", "luciana"}, new String[]{"Raquel", "Luciana"});
    public static final VocalizerLanguage BRITISH_ENGLISH = new VocalizerLanguage("eng", "British English", new String[]{"emily", "serena", "daniel"}, new String[]{"Emily", "Serena", "Daniel"});
    public static final VocalizerLanguage CANADIAN_FRENCH = new VocalizerLanguage("frc", "Canadian French", new String[]{"julie", "amelie", "felix", "nicolas"}, new String[]{"Julie", "Amelie", "Felix", "Nicolas"});
    public static final VocalizerLanguage CANTONESE_TRADITIONAL = new VocalizerLanguage("cah", "Cantonese Traditional", new String[]{"sin-ji"}, new String[]{"Sin-ji"});
    public static final VocalizerLanguage DANISH = new VocalizerLanguage("dad", "Danish", new String[]{"ida", "sara"}, new String[]{"Ida", "Sara"});
    public static final VocalizerLanguage DUTCH = new VocalizerLanguage("dun", "Dutch", new String[]{"claire", "xander"}, new String[]{"Claire", "Xander"});
    public static final VocalizerLanguage EUROPEAN_FRENCH = new VocalizerLanguage("frf", "European French", new String[]{"virginie", "audrey", "sebastien", "thomas"}, new String[]{"Virginie", "Audrey", "Sebastien", "Thomas"});
    public static final VocalizerLanguage EUROPEAN_PORTUGUESE = new VocalizerLanguage("ptp", "European Portuguese", new String[]{"joana"}, new String[]{"Joana"});
    public static final VocalizerLanguage EUROPEAN_SPANISH = new VocalizerLanguage("spe", "European Spanish", new String[]{"monica", "jorge"}, new String[]{"Monica", "Jorge"});
    public static final VocalizerLanguage FINNISH = new VocalizerLanguage("fif", "Finnish", new String[]{"mikko", "satu"}, new String[]{"Mikko", "Satu"});
    public static final VocalizerLanguage GERMAN = new VocalizerLanguage("ged", "German", new String[]{"steffi", "anna", "petra", "yannick"}, new String[]{"Steffi", "Anna", "Petra", "Yannick"});
    public static final VocalizerLanguage GREEK = new VocalizerLanguage("grg", "Greek", new String[]{"melina", "alexandros"}, new String[]{"Melina", "Alexandros"});
    public static final VocalizerLanguage ITALIAN = new VocalizerLanguage("iti", "Italian", new String[]{"silvia", "alice", "federica", "paola", "paolo", "luca"}, new String[]{"Silvia", "Alice", "Federica", "Paola", "Paolo", "Luca"});
    public static final VocalizerLanguage JAPANESE = new VocalizerLanguage("jpj", "Japanese", new String[]{"otoya", "kyoko"}, new String[]{"Otoya", "Kyoko"});
    public static final VocalizerLanguage KOREAN = new VocalizerLanguage("kok", "Korean", new String[]{"narae", "yuna"}, new String[]{"Narae", "Yuna"});
    public static final VocalizerLanguage LATIN_AMERICAN_SPANISH = new VocalizerLanguage("spm", "Latin American Spanish", new String[]{"paulina", "javier", "carlos", "diego", "juan"}, new String[]{"Paulina", "Javier", "Carlos", "Diego", "Juan"});
    public static final VocalizerLanguage MANDARIN_SIMPLIFIED = new VocalizerLanguage("mnc", "Mandarin Simplified", new String[]{"mei-ling", "ting-ting", "tian-tian"}, new String[]{"Mei-ling", "Ting-ting", "Tian-tian"});
    public static final VocalizerLanguage MANDARIN_TRADITIONAL = new VocalizerLanguage("mnt", "Mandarin Traditional", new String[]{"ya-ling", "mei-jia"}, new String[]{"Ya-ling", "Mei-jia"});
    public static final VocalizerLanguage NORWEGIAN = new VocalizerLanguage("non", "Norwegian", new String[]{"nora", "stine"}, new String[]{"Nora", "Stine"});
    public static final VocalizerLanguage POLISH = new VocalizerLanguage("plp", "Polish", new String[]{"agata", "zosia"}, new String[]{"Agata", "Zosia"});
    public static final VocalizerLanguage RUSSIAN = new VocalizerLanguage("rur", "Russian", new String[]{"milena", "katerina", "yuri"}, new String[]{"Milena", "Katerina", "Yuri"});
    public static final VocalizerLanguage SWEDISH = new VocalizerLanguage("sws", "Swedish", new String[]{"alva", "oskar"}, new String[]{"Alva", "Oskar"});
    public static final VocalizerLanguage TURKISH = new VocalizerLanguage("trt", "Turkish", new String[]{"aylin"}, new String[]{"Aylin"});
    public static final VocalizerLanguage UNITED_STATES_ENGLISH = new VocalizerLanguage("enu", "United States English", new String[]{"samantha", "jill", "carol", "ava", "allison", "tom"}, new String[]{"Samantha", "Jill", "Carol", "Ava", "Allison", "Tom"});

    a() {
    }
}
