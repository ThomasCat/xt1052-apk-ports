package com.nuance.dragon.toolkit.recognizer.vocon;

import com.nuance.dragon.toolkit.recognizer.Entry;
import com.nuance.dragon.toolkit.recognizer.Entry.Word;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.internal.g;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public class VoconEntry implements Entry, JSONCompliant {

    /* renamed from: a reason: collision with root package name */
    private final int f2009a;

    /* renamed from: b reason: collision with root package name */
    private final int f2010b;

    /* renamed from: c reason: collision with root package name */
    private final List f2011c = new ArrayList();

    /* renamed from: d reason: collision with root package name */
    private final String f2012d;

    public class VoconWord implements Word, JSONCompliant {

        /* renamed from: a reason: collision with root package name */
        private final String f2013a;

        public VoconWord(String str) {
            this.f2013a = str;
        }

        public int getConfidence() {
            return 0;
        }

        public String getText() {
            return this.f2013a;
        }

        public JSONObject toJSON() {
            return null;
        }
    }

    public VoconEntry(int i, int i2, String str) {
        this.f2009a = i;
        this.f2010b = i2;
        this.f2012d = str;
    }

    public void addWord(VoconWord voconWord) {
        this.f2011c.add(voconWord);
    }

    public void addWord(String str) {
        this.f2011c.add(new VoconWord(str));
    }

    public int getConfidence() {
        return this.f2009a;
    }

    public String getStartRule() {
        return this.f2012d;
    }

    public int getTotalSearchScore() {
        return this.f2010b;
    }

    public g getWords() {
        return new g(this.f2011c);
    }

    public JSONObject toJSON() {
        return null;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        String str = "";
        Iterator it = this.f2011c.iterator();
        while (true) {
            String str2 = str;
            if (!it.hasNext()) {
                return sb.toString();
            }
            VoconWord voconWord = (VoconWord) it.next();
            sb.append(str2);
            sb.append(voconWord.getText());
            str = " ";
        }
    }
}
