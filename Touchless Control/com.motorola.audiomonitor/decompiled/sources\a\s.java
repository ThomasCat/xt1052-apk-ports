package a;

import android.content.Context;
import android.content.res.Resources;
import b.b;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.uis.z;
import com.nuance.dragon.toolkit.audio.bluetooth.impl.AndroidVersion;

final class s extends r {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ q f60a;

    /* renamed from: b reason: collision with root package name */
    private final z f61b;

    s(q qVar, Context context) {
        this.f60a = qVar;
        super(context);
        this.f61b = new z(context);
    }

    public o a(b bVar, e eVar) {
        Context c2 = c();
        Resources resources = c2.getResources();
        String string = resources.getString(C0014R.string.err_msg_not_supported_generic);
        int identifier = resources.getIdentifier("IParseResult_command_" + bVar.a("command"), "string", c2.getPackageName());
        if (identifier != 0) {
            string = resources.getString(C0014R.string.err_msg_not_supported_context, new Object[]{resources.getString(identifier)});
        }
        this.f61b.a(string, eVar, 0, AndroidVersion.CUR_DEVELOPMENT);
        return o.CONSUMED;
    }

    public void a() {
        this.f61b.b();
    }
}
