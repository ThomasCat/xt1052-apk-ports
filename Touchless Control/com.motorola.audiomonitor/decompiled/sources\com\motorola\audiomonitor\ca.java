package com.motorola.audiomonitor;

import android.util.Log;
import b.f;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class ca implements ac {

    /* renamed from: a reason: collision with root package name */
    private static final String f391a = ("AMonitor." + ca.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f392b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private final String f393c;

    /* renamed from: d reason: collision with root package name */
    private float[] f394d;

    /* renamed from: e reason: collision with root package name */
    private String[] f395e;

    /* renamed from: f reason: collision with root package name */
    private ArrayList f396f = new ArrayList();

    private ca(String str) {
        float f2;
        JSONArray jSONArray = ((JSONObject) new JSONTokener(str).nextValue()).getJSONArray("hypotheses");
        int length = jSONArray.length();
        this.f393c = str;
        this.f394d = new float[length];
        this.f395e = new String[length];
        if (length > 0) {
            int i = 0;
            while (i < length) {
                String str2 = "error";
                try {
                    JSONObject jSONObject = (JSONObject) jSONArray.get(i);
                    try {
                        str2 = jSONObject.getString("utterance");
                    } catch (JSONException e2) {
                    }
                    try {
                        f2 = (float) jSONObject.getDouble("confidence");
                    } catch (JSONException e3) {
                        f2 = 0.0f;
                    }
                    this.f395e[i] = str2;
                    this.f394d[i] = f2;
                    this.f396f.add(new f(f2, str2));
                    if (f392b) {
                        Log.i(f391a, str2 + " : " + f2);
                    }
                    i++;
                } catch (JSONException e4) {
                    return;
                }
            }
        }
    }

    public static ac a(String str) {
        return new ca(str);
    }

    public final List a() {
        return Collections.unmodifiableList(this.f396f);
    }

    public final boolean b() {
        return this.f396f != null && this.f396f.size() > 0;
    }

    public final String toString() {
        return this.f393c;
    }
}
