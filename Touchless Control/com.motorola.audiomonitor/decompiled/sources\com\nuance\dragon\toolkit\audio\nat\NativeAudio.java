package com.nuance.dragon.toolkit.audio.nat;

import com.nuance.dragon.toolkit.audio.AudioChunk;

public interface NativeAudio {

    public interface Player {
        AudioChunk getPlayedAudio();

        boolean isStalled();

        void release();

        boolean start();

        boolean stop();

        int write(short[] sArr);
    }

    public interface Recorder {
        int getBufferSizeMs();

        AudioChunk read();

        void release();

        boolean start();

        boolean stop();
    }

    Player createPlayer(int i);

    Recorder createRecorder(int i);

    boolean initialize();

    void release();
}
