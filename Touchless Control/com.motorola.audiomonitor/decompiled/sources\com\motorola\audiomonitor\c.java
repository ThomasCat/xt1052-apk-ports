package com.motorola.audiomonitor;

import android.content.ContentProvider.PipeDataWriter;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.ParcelFileDescriptor.AutoCloseOutputStream;
import android.util.Log;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

class c implements PipeDataWriter {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ AudioProvider f390a;

    c(AudioProvider audioProvider) {
        this.f390a = audioProvider;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c0 A[SYNTHETIC, Splitter:B:31:0x00c0] */
    /* JADX WARNING: Removed duplicated region for block: B:53:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    public void writeDataToPipe(ParcelFileDescriptor parcelFileDescriptor, Uri uri, String str, Bundle bundle, Void voidR) {
        OutputStream outputStream;
        OutputStream outputStream2;
        String a2 = this.f390a.a(uri.getPath());
        if (AudioProvider.f132b) {
            Log.d(AudioProvider.f131a, "processing audio: " + a2 + " start");
        }
        List<ProtectedByteBuffer> a3 = d.a(a2);
        if (a3 != null) {
            try {
                outputStream2 = new AutoCloseOutputStream(parcelFileDescriptor);
                try {
                    d.a(a2, outputStream2);
                    for (ProtectedByteBuffer protectedByteBuffer : a3) {
                        if (this.f390a.f134d == null || protectedByteBuffer.getAffinityData() == 8000) {
                            protectedByteBuffer.writeTo(outputStream2);
                        } else {
                            this.f390a.f135e.writeTo(outputStream2, 0, this.f390a.f134d.a(protectedByteBuffer, 0, protectedByteBuffer.capacity(), this.f390a.f135e));
                        }
                    }
                    if (outputStream2 != null) {
                        try {
                            outputStream2.close();
                        } catch (IOException e2) {
                        }
                    }
                    d.a(a2, (OutputStream) null);
                } catch (IOException e3) {
                    outputStream = outputStream2;
                } catch (Throwable th) {
                    th = th;
                    if (outputStream2 != null) {
                    }
                    d.a(a2, (OutputStream) null);
                    throw th;
                }
            } catch (IOException e4) {
                outputStream = null;
                try {
                    Log.e(AudioProvider.f131a, "Fail to write the command audio");
                    if (outputStream != null) {
                        try {
                            outputStream.close();
                        } catch (IOException e5) {
                        }
                    }
                    d.a(a2, (OutputStream) null);
                    if (!AudioProvider.f132b) {
                    }
                } catch (Throwable th2) {
                    Throwable th3 = th2;
                    outputStream2 = outputStream;
                    th = th3;
                    if (outputStream2 != null) {
                    }
                    d.a(a2, (OutputStream) null);
                    throw th;
                }
            } catch (Throwable th4) {
                th = th4;
                outputStream2 = null;
                if (outputStream2 != null) {
                    try {
                        outputStream2.close();
                    } catch (IOException e6) {
                    }
                }
                d.a(a2, (OutputStream) null);
                throw th;
            }
        }
        if (!AudioProvider.f132b) {
            Log.d(AudioProvider.f131a, "processing audio: " + a2 + " stop");
        }
    }
}
