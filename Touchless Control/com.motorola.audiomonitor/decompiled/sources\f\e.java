package f;

import android.util.Log;
import java.util.List;

class e implements Runnable {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ List f2226a;

    /* renamed from: b reason: collision with root package name */
    final /* synthetic */ b f2227b;

    e(b bVar, List list) {
        this.f2227b = bVar;
        this.f2226a = list;
    }

    public void run() {
        synchronized (this.f2227b.f2220g) {
            if (this.f2227b.f2217d == null) {
                if (b.f2215b) {
                    Log.d(b.f2214a, "updating list...");
                }
                this.f2227b.f2217d = this.f2227b.b(this.f2226a);
            } else if (b.f2215b) {
                Log.d(b.f2214a, "Another update already happening. Ignore.");
            }
        }
    }
}
