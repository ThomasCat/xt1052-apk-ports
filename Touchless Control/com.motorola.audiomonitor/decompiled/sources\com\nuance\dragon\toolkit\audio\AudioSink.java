package com.nuance.dragon.toolkit.audio;

import com.nuance.dragon.toolkit.util.internal.d;

public abstract class AudioSink {

    /* renamed from: a reason: collision with root package name */
    private AudioSource f1003a;

    /* access modifiers changed from: protected */
    public void audioSourceDisconnected(AudioSource audioSource) {
    }

    public abstract void chunksAvailable(AudioSource audioSource);

    public final void connectAudioSource(AudioSource audioSource) {
        d.a("source", (Object) audioSource);
        AudioType audioType = audioSource.getAudioType();
        d.a("source", "a supported audio type", audioType != null && isAudioSourceTypeSupported(audioType));
        AudioSource audioSource2 = this.f1003a;
        this.f1003a = audioSource;
        if (audioSource2 != null) {
            audioSource2.audioSinkDisconnected(this);
        }
        audioSource.audioSinkConnected(this);
    }

    public final AudioSource disconnectAudioSource() {
        AudioSource audioSource = this.f1003a;
        this.f1003a = null;
        if (audioSource == null) {
            return null;
        }
        audioSource.audioSinkDisconnected(this);
        audioSourceDisconnected(audioSource);
        return audioSource;
    }

    public abstract void framesDropped(AudioSource audioSource);

    /* access modifiers changed from: protected */
    public AudioSource getConnectedSource() {
        return this.f1003a;
    }

    /* access modifiers changed from: protected */
    public boolean isAudioSourceTypeSupported(AudioType audioType) {
        return true;
    }

    public abstract void sourceClosed(AudioSource audioSource);
}
