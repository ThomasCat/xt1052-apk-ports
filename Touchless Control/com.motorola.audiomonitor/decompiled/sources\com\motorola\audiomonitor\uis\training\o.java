package com.motorola.audiomonitor.uis.training;

import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup.LayoutParams;

class o implements OnLayoutChangeListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ TrainingTriggerRecordingActivity f754a;

    o(TrainingTriggerRecordingActivity trainingTriggerRecordingActivity) {
        this.f754a = trainingTriggerRecordingActivity;
    }

    public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        int measuredHeight = this.f754a.A.getMeasuredHeight();
        this.f754a.L = this.f754a.D.getLayoutParams();
        this.f754a.M = this.f754a.E.getLayoutParams();
        if (measuredHeight != this.f754a.G) {
            this.f754a.G = measuredHeight;
            this.f754a.H = this.f754a.C.getMeasuredHeight();
            this.f754a.I = 5;
            this.f754a.J = 5;
            LayoutParams h2 = this.f754a.L;
            int d2 = this.f754a.H + this.f754a.I;
            this.f754a.L.height = d2;
            h2.width = d2;
            LayoutParams f2 = this.f754a.M;
            int d3 = this.f754a.H + this.f754a.J;
            this.f754a.M.height = d3;
            f2.width = d3;
            this.f754a.D.invalidate();
            this.f754a.E.invalidate();
        }
    }
}
