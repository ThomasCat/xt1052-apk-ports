package com.nuance.dragon.toolkit.audio.nat;

import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.audio.nat.NativeAudio.Player;
import com.nuance.dragon.toolkit.audio.sinks.PlayerSink;
import com.nuance.dragon.toolkit.util.Logger;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

final class a extends PlayerSink {

    /* renamed from: a reason: collision with root package name */
    private final NativeAudio f1102a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public Player f1103b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public boolean f1104c;

    /* renamed from: d reason: collision with root package name */
    private boolean f1105d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public final LinkedList f1106e = new LinkedList();
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public boolean f1107f;

    a(AudioType audioType, NativeAudio nativeAudio, Handler handler) {
        super(audioType, handler);
        this.f1102a = nativeAudio;
    }

    /* access modifiers changed from: private */
    public boolean a() {
        AudioChunk playedAudio;
        if (!this.f1104c) {
            return false;
        }
        int i = 0;
        while (true) {
            if (this.f1106e.isEmpty()) {
                break;
            }
            short[] sArr = (short[]) this.f1106e.remove();
            int write = this.f1103b.write(sArr);
            if (write >= 0) {
                if (write != 0) {
                    i += write;
                    if (write < sArr.length) {
                        short[] sArr2 = new short[(sArr.length - write)];
                        System.arraycopy(sArr, write, sArr2, 0, sArr2.length);
                        this.f1106e.addFirst(sArr2);
                        break;
                    }
                } else if (sArr.length > 0) {
                    this.f1106e.addFirst(sArr);
                }
            } else {
                Logger.error(this, "Error writing audio buffer");
                break;
            }
        }
        if (this.f1106e.isEmpty()) {
            handleAudioNeeded(400);
        }
        if (!this.f1105d && i > 0) {
            this.f1105d = true;
            handleStarted();
        }
        if (i > 0) {
            ArrayList arrayList = new ArrayList();
            do {
                playedAudio = this.f1103b.getPlayedAudio();
                if (playedAudio != null) {
                    arrayList.add(playedAudio);
                    continue;
                }
            } while (playedAudio != null);
            if (!arrayList.isEmpty()) {
                handleChunksPlayed(arrayList);
            }
        }
        return i > 0;
    }

    /* access modifiers changed from: private */
    public void b() {
        this.f1107f = true;
        if (this.f1103b.isStalled() && this.f1106e.isEmpty()) {
            stopPlayingInternal();
        }
    }

    /* access modifiers changed from: protected */
    public final boolean isCodecSupported(AudioType audioType) {
        return audioType.encoding == Encoding.PCM_16 && audioType.frequency == 16000;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.audio.AudioChunk>, for r4v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.audio.AudioChunk>] */
    public final void newChunksInternal(List<AudioChunk> list) {
        for (AudioChunk audioChunk : list) {
            if (audioChunk.audioShorts.length > 0) {
                this.f1106e.add(audioChunk.audioShorts);
            } else {
                Logger.warn(this, "Skipping empty audio chunk");
            }
        }
        a();
    }

    /* access modifiers changed from: protected */
    public final void noNewChunksInternal() {
        if (this.f1104c) {
            b();
        }
    }

    /* access modifiers changed from: protected */
    public final boolean startPlayingInternal(AudioType audioType) {
        if (this.f1103b != null) {
            return false;
        }
        this.f1103b = this.f1102a.createPlayer(audioType.frequency);
        if (this.f1103b == null) {
            return false;
        }
        if (!this.f1103b.start()) {
            this.f1103b.release();
            this.f1103b = null;
            return false;
        }
        this.f1104c = true;
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            public final void run() {
                if (a.this.f1103b != null && a.this.f1104c) {
                    boolean c2 = a.this.a();
                    if (!a.this.f1104c) {
                        return;
                    }
                    if (c2 || !a.this.f1107f || !a.this.f1106e.isEmpty()) {
                        handler.postDelayed(this, 200);
                        return;
                    }
                    a.this.b();
                    if (a.this.f1104c) {
                        handler.postDelayed(this, 400);
                    }
                }
            }
        });
        return true;
    }

    /* access modifiers changed from: protected */
    public final void stopPlayingInternal() {
        if (this.f1104c) {
            this.f1103b.stop();
            this.f1103b.release();
            this.f1103b = null;
            this.f1104c = false;
            handleStopped();
        }
    }
}
