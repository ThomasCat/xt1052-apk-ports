package com.motorola.audiomonitor;

import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ProtectedByteBuffer {
    private final int mAffinityData;
    private final ByteBuffer mByteBuffer;

    public ProtectedByteBuffer(int i, int i2) {
        this.mByteBuffer = ByteBuffer.allocateDirect(i);
        this.mAffinityData = i2;
    }

    public ProtectedByteBuffer(ProtectedByteBuffer protectedByteBuffer, int i) {
        int capacity = protectedByteBuffer.capacity() - i;
        this.mByteBuffer = ByteBuffer.allocateDirect(capacity);
        this.mByteBuffer.put(protectedByteBuffer.mByteBuffer.array(), i, capacity);
        this.mByteBuffer.rewind();
        this.mAffinityData = protectedByteBuffer.mAffinityData;
    }

    public ProtectedByteBuffer(ByteBuffer byteBuffer, int i) {
        this.mByteBuffer = byteBuffer;
        this.mByteBuffer.rewind();
        this.mAffinityData = i;
    }

    public ProtectedByteBuffer(byte[] bArr, int i) {
        this.mByteBuffer = ByteBuffer.allocateDirect(bArr.length);
        this.mByteBuffer.put(bArr);
        this.mByteBuffer.rewind();
        this.mAffinityData = i;
    }

    public synchronized int capacity() {
        return this.mByteBuffer.capacity();
    }

    public int getAffinityData() {
        return this.mAffinityData;
    }

    public synchronized short getFirstShort() {
        this.mByteBuffer.rewind();
        this.mByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        return this.mByteBuffer.getShort();
    }

    public synchronized short[] getShortArray() {
        short[] sArr;
        this.mByteBuffer.rewind();
        int capacity = this.mByteBuffer.capacity() / 2;
        sArr = new short[capacity];
        this.mByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        for (int i = 0; i < capacity; i++) {
            sArr[i] = this.mByteBuffer.getShort();
        }
        return sArr;
    }

    public void writeTo(OutputStream outputStream) {
        outputStream.write(this.mByteBuffer.array());
    }

    public void writeTo(OutputStream outputStream, int i, int i2) {
        outputStream.write(this.mByteBuffer.array(), i, i2);
    }
}
