package com.nuance.dragon.toolkit.grammar;

import com.nuance.dragon.toolkit.cloudservices.recognizer.NvcAsrSpec;
import com.nuance.dragon.toolkit.cloudservices.recognizer.RecogSpec;
import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import com.nuance.dragon.toolkit.util.internal.d;
import com.nuance.dragon.toolkit.util.internal.g;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Scenario implements JSONCompliant {

    /* renamed from: a reason: collision with root package name */
    private final g f1722a;

    /* renamed from: b reason: collision with root package name */
    private NvcAsrSpec f1723b;

    /* renamed from: c reason: collision with root package name */
    private String f1724c;

    public Scenario(String str, List list, NvcAsrSpec nvcAsrSpec) {
        d.a("name", (Object) str);
        d.a("elvisConstraints and nvcAsrSpec", list, nvcAsrSpec);
        this.f1724c = str;
        if (list == null) {
            this.f1722a = new g();
        } else {
            this.f1722a = new g(list);
        }
        this.f1723b = nvcAsrSpec;
    }

    public static Scenario createFromJSON(JSONObject jSONObject) {
        ArrayList arrayList = null;
        String string = jSONObject.getString("name");
        JSONObject optJSONObject = jSONObject.optJSONObject("cmd");
        NvcAsrSpec createFromJSON = optJSONObject == null ? null : NvcAsrSpec.createFromJSON(optJSONObject);
        JSONArray optJSONArray = jSONObject.optJSONArray("constraints");
        if (optJSONArray != null) {
            ArrayList arrayList2 = new ArrayList(optJSONArray.length());
            for (int i = 0; i < optJSONArray.length(); i++) {
                arrayList2.add(optJSONArray.getString(i));
            }
            arrayList = arrayList2;
        }
        if (createFromJSON != null || arrayList != null) {
            return new Scenario(string, arrayList, createFromJSON);
        }
        throw new JSONException("Must have a cmd or constraints key");
    }

    public void attachSettings(Dictionary dictionary) {
        if (this.f1723b != null) {
            this.f1723b.attachSettings(dictionary);
        }
    }

    public boolean attachWordList(WordList wordList, String str) {
        d.b("id", str);
        d.a("wordList", (Object) wordList);
        if (this.f1723b != null) {
            return this.f1723b.attachWordList(wordList, str);
        }
        return false;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Scenario)) {
            return false;
        }
        Scenario scenario = (Scenario) obj;
        if (this.f1722a == null) {
            if (scenario.f1722a != null) {
                return false;
            }
        } else if (!this.f1722a.equals(scenario.f1722a)) {
            return false;
        }
        if (this.f1724c == null) {
            if (scenario.f1724c != null) {
                return false;
            }
        } else if (!this.f1724c.equals(scenario.f1724c)) {
            return false;
        }
        return this.f1723b == null ? scenario.f1723b == null : this.f1723b.equals(scenario.f1723b);
    }

    public RecogSpec getCloudRecogSpec() {
        if (this.f1723b != null) {
            return this.f1723b.createRecogSpec();
        }
        return null;
    }

    public List getElvisConstraints() {
        return this.f1722a;
    }

    public String getName() {
        return this.f1724c;
    }

    public String getRequiredContactListId() {
        if (this.f1723b != null) {
            return this.f1723b.getContactListId();
        }
        return null;
    }

    public Set getRequiredWordListIds() {
        return this.f1723b != null ? this.f1723b.getRequiredCustomWordListIds() : new HashSet();
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f1724c == null ? 0 : this.f1724c.hashCode()) + (((this.f1722a == null ? 0 : this.f1722a.hashCode()) + 31) * 31)) * 31;
        if (this.f1723b != null) {
            i = this.f1723b.hashCode();
        }
        return hashCode + i;
    }

    public JSONObject toJSON() {
        b bVar = new b();
        bVar.a("name", (Object) this.f1724c);
        bVar.a("cmd", (JSONCompliant) this.f1723b);
        bVar.b("constraints", this.f1722a);
        return bVar;
    }
}
