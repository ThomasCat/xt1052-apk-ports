package com.nuance.dragon.toolkit.recognizer;

public class RecogError {
    public static final int ERR_CODE_AUDIO_FRAMES_DROPPED = 1;
    public static final int ERR_CODE_CANCELED = 0;
    public static final int ERR_CODE_CANT_START = 3;
    public static final int ERR_CODE_CONSTRAINT_DEF = 8;
    public static final int ERR_CODE_FILE = 4;
    public static final int ERR_CODE_MISSING_WORDLIST = 10;
    public static final int ERR_CODE_NOT_INITIALIZED = 5;
    public static final int ERR_CODE_NO_RESULT = 2;
    public static final int ERR_CODE_TRANSITION_DEF = 7;
    public static final int ERR_CODE_WORDSLOT_DEF = 9;

    /* renamed from: a reason: collision with root package name */
    private static final String[] f1998a = {"Recognition canceled", "Audio frames dropped", "No recognition result", "Can't start", "File error", "Engine is not initialized", "Error", "Elvis Transition definition error", "Elvis Constraint definition error", "Elvis WordSlot definition error", "Elvis Missing wordlist"};

    /* renamed from: b reason: collision with root package name */
    private int f1999b;

    /* renamed from: c reason: collision with root package name */
    private String f2000c;

    public RecogError(int i) {
        this(i, null);
    }

    public RecogError(int i, String str) {
        this.f1999b = i;
        if (str != null) {
            this.f2000c = str;
        } else if (i < f1998a.length) {
            this.f2000c = f1998a[i];
        } else {
            this.f2000c = f1998a[6];
        }
    }

    public int getErrorCode() {
        return this.f1999b;
    }

    public String getErrorDescription() {
        return this.f2000c;
    }
}
