package com.nuance.dragon.toolkit.cloudservices;

import android.os.Build;
import android.os.Build.VERSION;
import android.os.Handler;
import com.nuance.a.a.a.b.b.c;
import com.nuance.a.a.a.b.c.a.a;
import com.nuance.a.a.a.b.c.c.b;
import com.nuance.a.a.a.b.c.c.d;
import com.nuance.a.a.a.b.c.c.f;
import com.nuance.a.a.a.b.c.c.g;
import com.nuance.a.a.a.b.c.c.h;
import com.nuance.a.a.a.b.c.c.i;
import com.nuance.a.a.a.b.c.c.j;
import com.nuance.a.a.a.b.c.c.k;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.language.Language;
import com.nuance.dragon.toolkit.util.Logger;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

class e implements d {

    /* renamed from: a reason: collision with root package name */
    static final /* synthetic */ boolean f1442a = (!e.class.desiredAssertionStatus());

    /* renamed from: b reason: collision with root package name */
    private final a f1443b;

    /* renamed from: c reason: collision with root package name */
    private final b f1444c;

    /* renamed from: d reason: collision with root package name */
    private final f f1445d = new f() {
        public final void a() {
            Logger.error(e.this, "Command Creation Failed [" + e.this.f1449h + "]");
            e.this.k.post(new Runnable() {
                public final void run() {
                    e.this.i.h().g();
                }
            });
        }

        public final void a(final String str) {
            e eVar = e.this;
            new StringBuilder("Command Created [").append(e.this.f1449h).append("] session id [").append(str).append("]");
            e.this.k.post(new Runnable() {
                public final void run() {
                    e.this.i.a(str);
                    e.this.i.h();
                    String str = str;
                }
            });
        }
    };

    /* renamed from: e reason: collision with root package name */
    private final g f1446e = new g() {
        public final void a(final i iVar) {
            Logger.info(e.this, "PDX Query Error Returned: " + iVar.g() + "(" + iVar.h() + ") [" + iVar.i() + "]");
            e.this.k.post(new Runnable() {
                public final void run() {
                    e.this.i.h().a(iVar.g(), iVar.h());
                }
            });
        }

        public final void a(final j jVar) {
            Logger.info(e.this, "PDX Query Result Returned");
            e.this.k.post(new Runnable() {
                public final void run() {
                    e.this.i.h().a(jVar);
                }
            });
        }

        public final void a(k kVar) {
            Logger.info(e.this, "PDX Query Retry Returned: " + kVar.h() + "(" + kVar.g() + ")");
            final String i = kVar.i();
            final String g2 = kVar.g();
            e.this.k.post(new Runnable() {
                public final void run() {
                    e.this.i.h().a(i, g2);
                }
            });
        }

        public final void a(final short s) {
            Logger.info(e.this, "Command Event: " + s);
            e.this.k.post(new Runnable() {
                public final void run() {
                    e.this.i.h().a(s);
                }
            });
        }
    };

    /* renamed from: f reason: collision with root package name */
    private d f1447f;

    /* renamed from: g reason: collision with root package name */
    private b f1448g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public String f1449h = "no_cmd_yet";
    /* access modifiers changed from: private */
    public final Transaction i;
    private final Map j = new HashMap();
    /* access modifiers changed from: private */
    public final Handler k = new Handler();

    public e(a aVar, b bVar, Transaction transaction) {
        this.f1443b = aVar;
        this.f1444c = bVar;
        this.i = transaction;
    }

    private void a(h hVar, String str) {
        new StringBuilder("Sending param ").append(str);
        this.f1448g.a(hVar);
    }

    public final void a() {
        if (this.f1448g != null) {
            this.f1448g.a();
        }
    }

    public final void a(String str) {
        if (this.f1448g == null) {
            return;
        }
        if (!this.j.containsKey(str)) {
            com.nuance.a.a.a.b.c.c.a a2 = this.f1447f.a(str);
            this.j.put(str, a2);
            a((h) a2, str);
            return;
        }
        Logger.warn(this, "This Audio Param [" + str + "] has already been sent, it will ignored");
    }

    public final void a(String str, AudioChunk audioChunk, boolean z) {
        if (this.f1448g != null) {
            if (!this.j.containsKey(str)) {
                a(str);
            }
            com.nuance.a.a.a.b.c.c.a aVar = (com.nuance.a.a.a.b.c.c.a) this.j.get(str);
            if (audioChunk != null) {
                if (audioChunk.audioBytes != null) {
                    aVar.a(audioChunk.audioBytes, 0, audioChunk.audioBytes.length, z);
                } else if (audioChunk.audioShorts != null) {
                    byte[] bArr = new byte[(audioChunk.audioShorts.length * 2)];
                    for (int i2 = 0; i2 < audioChunk.audioShorts.length; i2++) {
                        bArr[i2 * 2] = (byte) audioChunk.audioShorts[i2];
                        bArr[(i2 * 2) + 1] = (byte) (audioChunk.audioShorts[i2] >> 8);
                    }
                    aVar.a(bArr, 0, bArr.length, z);
                }
            } else if (f1442a || z) {
                aVar.a(null, 0, 0, z);
                new StringBuilder("stream audio last buffer [").append(z).append("]");
            } else {
                throw new AssertionError();
            }
        }
    }

    public final void a(String str, Dictionary dictionary) {
        if (this.f1448g != null) {
            a(this.f1447f.a(str, com.nuance.dragon.toolkit.data.a.a.a(dictionary, this.f1447f)), str);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0071, code lost:
        if (r1.length() > 0) goto L_0x0073;
     */
    public final void a(String str, Dictionary dictionary, int i2) {
        String str2;
        this.f1449h = str;
        Vector vector = new Vector();
        vector.add(new c("Disable_Bluetooth", "TRUE".getBytes(), c.a.f826a));
        a aVar = this.f1443b;
        f fVar = this.f1445d;
        this.f1444c.a();
        this.f1447f = com.nuance.a.a.a.b.c.c.e.a(aVar, fVar, vector);
        String str3 = this.f1449h;
        if (this.f1447f != null) {
            new StringBuilder("Creating command ").append(str3);
            com.nuance.a.a.a.b.c.c.c j2 = dictionary != null ? com.nuance.dragon.toolkit.data.a.a.a(dictionary, this.f1447f) : this.f1447f.j();
            String g2 = this.f1444c.g();
            b bVar = this.f1444c;
            String str4 = Build.MODEL;
            b bVar2 = this.f1444c;
            String str5 = "Android " + VERSION.RELEASE;
            b bVar3 = this.f1444c;
            Locale locale = Locale.getDefault();
            if (locale != null) {
                str2 = locale.getCountry();
                if (str2 != null) {
                }
            }
            str2 = "xxx-XXX";
            String h2 = this.f1444c.h();
            b bVar4 = this.f1444c;
            String str6 = "1.0.0.0";
            String a2 = this.f1444c.a();
            b bVar5 = this.f1444c;
            String grammarLanguage = Language.getGrammarLanguage(Locale.getDefault());
            if (grammarLanguage == null) {
                grammarLanguage = "xxx-XXX";
            }
            String str7 = j2.a("dictation_language") ? j2.f("dictation_language") : grammarLanguage;
            if (!j2.a("ui_language")) {
                j2.b("ui_language", str7.substring(0, 2).toLowerCase());
            }
            if (!j2.a("phone_submodel")) {
                j2.b("phone_submodel", str4);
            }
            if (!j2.a("phone_OS")) {
                j2.b("phone_OS", str5);
            }
            if (!j2.a("locale")) {
                j2.b("locale", str2);
            }
            if (!j2.a("nmaid")) {
                j2.b("nmaid", a2);
            }
            if (!j2.a("network_type")) {
                j2.b("network_type", h2);
            }
            this.f1448g = this.f1447f.a(this.f1446e, str3, str6, str7, g2, str4, j2, (long) i2);
        }
    }

    public final void a(String str, Dictionary dictionary, com.nuance.a.a.a.b.b.a aVar) {
        if (this.f1448g != null) {
            a(this.f1447f.a(str, com.nuance.dragon.toolkit.data.a.a.a(dictionary, this.f1447f), aVar), str);
        }
    }

    public final void a(String str, String str2) {
        if (this.f1448g != null) {
            a(this.f1447f.b(str, str2), str);
        }
    }

    public final void a(String str, byte[] bArr) {
        if (this.f1448g != null) {
            a(this.f1447f.a(str, bArr), str);
        }
    }

    public final void b() {
        if (this.f1447f != null) {
            try {
                this.f1447f.a();
            } catch (com.nuance.a.a.a.b.c.a.e e2) {
            }
            this.f1447f = null;
        }
    }

    public final void b(String str, Dictionary dictionary) {
        if (this.f1448g != null) {
            a(this.f1447f.b(str, com.nuance.dragon.toolkit.data.a.a.a(dictionary, this.f1447f)), str);
        }
    }

    public final void b(String str, String str2) {
        if (this.f1448g != null) {
            a(this.f1447f.a(str, str2), str);
        }
    }

    /* access modifiers changed from: protected */
    public final b c() {
        return this.f1444c;
    }

    public final void c(String str, Dictionary dictionary) {
        if (this.f1448g != null) {
            a(this.f1447f.c(str, com.nuance.dragon.toolkit.data.a.a.a(dictionary, this.f1447f)), str);
        }
    }

    public final void d(String str, Dictionary dictionary) {
        if (this.f1448g != null) {
            a(this.f1447f.d(str, com.nuance.dragon.toolkit.data.a.a.a(dictionary, this.f1447f)), str);
        }
    }
}
