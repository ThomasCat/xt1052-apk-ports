package com.motorola.audiomonitor.retailmode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.motorola.audiomonitor.MonitorService;
import com.motorola.audiomonitor.a;

public class RetailModeReceiver extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    private static final String f550a = ("AMonitor." + RetailModeReceiver.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f551b = a.f160b;

    public void onReceive(Context context, Intent intent) {
        if (f551b) {
            Log.d(f550a, "Received " + intent.toUri(0));
        }
        String action = intent.getAction();
        if (action != null && action.equals("com.motorola.audiomonitor.action.RETAILMODE")) {
            a.j = intent.getBooleanExtra("state", false);
            Intent intent2 = new Intent(context, MonitorService.class);
            intent2.putExtra("MONITOR_STATE", "restart");
            context.startService(intent2);
        }
    }
}
