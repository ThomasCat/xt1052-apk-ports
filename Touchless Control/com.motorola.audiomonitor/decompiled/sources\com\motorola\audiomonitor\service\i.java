package com.motorola.audiomonitor.service;

import android.os.Message;
import android.util.Log;
import com.nuance.dragon.toolkit.audio.pipes.SpeexEncoderPipe;

class i extends p {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ d f575a;

    i(d dVar, String str, String str2) {
        this.f575a = dVar;
        super(dVar, str, str2);
    }

    public void a(Message message) {
        switch (message.what) {
            case 1:
                this.f575a.a(message);
                return;
            case 2:
            case 3:
            case 7:
            case 8:
            case 9:
            case 10:
                if (message.arg1 != 0) {
                    this.f575a.b(true);
                    return;
                }
                return;
            case 11:
                if (message.arg1 != 0) {
                    this.f575a.b(true);
                    return;
                } else if (this.f575a.w != null && this.f575a.w.what == 103) {
                    this.f575a.s = this.f575a.f567e;
                    if (this.f575a.u) {
                        this.f575a.u = false;
                        this.f575a.p();
                        return;
                    }
                    return;
                } else {
                    return;
                }
            case 12:
                if (message.arg1 == 0) {
                    if (this.f575a.w.arg1 < this.f575a.z.length) {
                        this.f575a.z[this.f575a.w.arg1] = message.arg2;
                    }
                    if (this.f575a.w.arg1 < this.f575a.A.length) {
                        this.f575a.A[this.f575a.w.arg1] = (short[]) message.obj;
                    }
                    this.f575a.a(this.f575a.w.arg1, message.arg2, (short[]) message.obj);
                    return;
                }
                return;
            case SpeexEncoderPipe.DEFAULT_VAD_BEGIN_LEN /*15*/:
                this.f575a.b(false);
                return;
            case 101:
                this.f575a.s();
                return;
            case 102:
                this.f575a.a(message);
                return;
            case 103:
                this.f575a.a(message);
                return;
            case 104:
                if (this.f575a.x.a(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to control event log");
                    this.f575a.a(message);
                    return;
                }
                return;
            case 105:
                if (this.f575a.x.b(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to control audio log");
                    this.f575a.a(message);
                    return;
                }
                return;
            case 106:
                if (this.f575a.x.c(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to control audio buffering");
                    this.f575a.a(message);
                    return;
                }
                return;
            case 107:
                this.f575a.a(message);
                return;
            case 108:
                this.f575a.a(message);
                return;
            case 109:
                if (this.f575a.x.e(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to request noise estimate");
                    this.f575a.a(message);
                    return;
                }
                return;
            case 110:
                this.f575a.a(message);
                return;
            case 111:
                this.f575a.a(message);
                return;
            case 201:
                this.f575a.b(true);
                return;
            case 202:
                if (this.f575a.x.d(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to send dock state change");
                    this.f575a.a(message);
                    return;
                }
                return;
            case 203:
            case 204:
                this.f575a.u = true;
                this.f575a.a(message);
                return;
            default:
                return;
        }
    }
}
