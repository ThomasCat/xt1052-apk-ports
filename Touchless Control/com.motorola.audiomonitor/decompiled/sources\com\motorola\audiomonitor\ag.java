package com.motorola.audiomonitor;

import android.content.Context;
import android.util.Log;
import com.motorola.audiomonitor.service.o;
import h.k;

class ag implements o {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ MonitorService f295a;

    ag(MonitorService monitorService) {
        this.f295a = monitorService;
    }

    public void a() {
    }

    public void a(int i, int i2, short[] sArr) {
        short s;
        short s2;
        short s3 = 0;
        if (i2 < 0 || i2 > 2) {
            i2 = -1;
        }
        if (sArr == null || sArr.length < 3) {
            s = 0;
            s2 = 0;
        } else {
            s2 = sArr[0];
            s = sArr[1];
            s3 = sArr[2];
        }
        k.a((Context) this.f295a, i2, (int) s2, (int) s, (int) s3);
    }

    public void a(String str, String str2) {
        if (MonitorService.f150c) {
            Log.d(MonitorService.f149b, "onStateChanged: state=" + str + ", substate=" + str2);
        }
        if ("connected".equals(str) && "idle".equals(str2) && this.f295a.f152d == null) {
            this.f295a.f156h.post(new ah(this));
        }
        if ("disconnected".equals(str)) {
            this.f295a.f156h.post(new ai(this));
        }
    }
}
