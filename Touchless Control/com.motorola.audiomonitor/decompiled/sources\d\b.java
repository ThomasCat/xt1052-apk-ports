package d;

import android.util.Log;
import com.motorola.audiomonitor.bz;

class b extends Thread {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ String[] f2195a;

    /* renamed from: b reason: collision with root package name */
    final /* synthetic */ a f2196b;

    b(a aVar, String[] strArr) {
        this.f2196b = aVar;
        this.f2195a = strArr;
    }

    public void run() {
        String[] strArr;
        StringBuffer stringBuffer = new StringBuffer();
        String str = "";
        for (String str2 : this.f2195a) {
            stringBuffer.append(str);
            stringBuffer.append(str2);
            str = "|";
        }
        String format = String.format("g=%s; paramA:$g 500; paramB:$g 740; paramC:$g 226;", new Object[]{stringBuffer.toString()});
        if (a.f2193b) {
            Log.d(a.f2192a, "call command grammar=" + format);
        }
        synchronized (this.f2196b.f2194c) {
            this.f2196b.f2194c.configure(bz.a(format, this.f2195a));
        }
    }
}
