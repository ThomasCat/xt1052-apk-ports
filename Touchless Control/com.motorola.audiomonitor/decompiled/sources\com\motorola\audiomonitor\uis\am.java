package com.motorola.audiomonitor.uis;

import a.e;
import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.actions.utils.k;
import com.motorola.audiomonitor.actions.utils.l;

public class am implements k, ba {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f655a = ("AMonitor." + am.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f656b = a.f160b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public boolean f657c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public as f658d;

    /* renamed from: e reason: collision with root package name */
    private final e f659e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public long f660f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public final boolean f661g;

    /* renamed from: h reason: collision with root package name */
    private l f662h;
    private Handler i;
    /* access modifiers changed from: private */
    public CountDownTimer j;

    public am(Context context, e eVar, long j2) {
        this(context, eVar, j2, false, false);
    }

    public am(Context context, e eVar, long j2, boolean z, boolean z2) {
        this.f659e = eVar;
        this.f660f = j2;
        this.f661g = z2;
        this.f657c = z;
    }

    public void a() {
        this.f658d = as.a();
        as.a((ba) this);
        this.f658d.a(this.f659e);
        this.i.post(new an(this));
    }

    public void a(long j2) {
        this.f660f = j2;
    }

    public void a(Handler handler) {
        this.i = handler;
    }

    public void a(l lVar) {
        this.f662h = lVar;
    }

    public void a(boolean z) {
        if (this.f662h != null) {
            this.f662h.a(z);
        }
    }

    public void b() {
        if (this.j != null) {
            this.j.cancel();
            this.j = null;
        }
        if (this.f658d != null) {
            this.f658d.b();
        }
    }

    public void b(boolean z) {
        if (f656b) {
            Log.d(f655a, "onUserResponseFromUI");
        }
        a(z);
    }

    public void c() {
        if (this.f661g) {
            this.i.post(new ao(this));
        }
    }

    /* access modifiers changed from: protected */
    public void d() {
        if (this.f660f > 0) {
            this.j = new ap(this, this.f660f, 10);
            this.j.start();
        }
    }

    public void e() {
        b();
    }
}
