package com.nuance.dragon.toolkit.cloudservices;

import android.content.Context;
import android.os.Handler;
import com.nuance.a.a.a.b.c.a.a;
import com.nuance.a.a.a.b.c.a.b;
import com.nuance.dragon.toolkit.cloudservices.CloudServices.ConnectionState;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.Vector;

final class c extends a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public ConnectionState f1398a = ConnectionState.DISCONNECTED;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public String f1399b;

    /* renamed from: c reason: collision with root package name */
    private a f1400c = a();

    /* renamed from: d reason: collision with root package name */
    private com.nuance.a.a.a.b.c.a.c f1401d = new com.nuance.a.a.a.b.c.a.c() {
        public final void a() {
            Logger.warn(c.this, "Connection failed");
        }

        public final void a(final String str) {
            Logger.info(c.this, "Connected with session ID " + str);
            c.this.f1404g.post(new Runnable() {
                public final void run() {
                    c.this.f1399b = str;
                    c.this.f1398a = ConnectionState.CONNECTED;
                }
            });
        }

        public final void a(short s) {
            Logger.info(c.this, "Disconnected reasonCode [" + s + "]");
            c.this.f1404g.post(new Runnable() {
                public final void run() {
                    c.this.f1398a = ConnectionState.DISCONNECTED;
                }
            });
        }
    };

    /* renamed from: e reason: collision with root package name */
    private b f1402e;

    /* renamed from: f reason: collision with root package name */
    private boolean f1403f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public Handler f1404g = new Handler();

    public c(Context context, CloudConfig cloudConfig) {
        d.a("appContext", (Object) context);
        d.a("config", (Object) cloudConfig);
        this.f1402e = new b(context.getApplicationContext(), cloudConfig.host, cloudConfig.port, cloudConfig.sslConfig, cloudConfig.appId, cloudConfig.appKey, cloudConfig.recorderCodec, cloudConfig.playerCodec);
        if (this.f1400c == null) {
            this.f1403f = false;
            this.f1402e = null;
            return;
        }
        this.f1403f = true;
    }

    private a a() {
        Vector vector;
        try {
            if (this.f1402e.e()) {
                vector = new Vector();
                vector.add(new com.nuance.a.a.a.b.b.c("SSL_Socket_Enable", "TRUE".getBytes(), com.nuance.a.a.a.b.b.c.a.f826a));
                SSLConfig f2 = this.f1402e.f();
                if (f2.isSelfSigned()) {
                    vector.add(new com.nuance.a.a.a.b.b.c("SSL_SelfSigned_Cert", "TRUE".getBytes(), com.nuance.a.a.a.b.b.c.a.f826a));
                }
                if (f2.certSummary() != null) {
                    vector.add(new com.nuance.a.a.a.b.b.c("SSL_Cert_Summary", f2.certSummary().getBytes(), com.nuance.a.a.a.b.b.c.a.f826a));
                }
                if (f2.certData() != null) {
                    vector.add(new com.nuance.a.a.a.b.b.c("SSL_Cert_Data", f2.certData().getBytes(), com.nuance.a.a.a.b.b.c.a.f826a));
                }
            } else {
                vector = null;
            }
            return b.a(this.f1402e.c(), this.f1402e.d(), this.f1402e.a(), this.f1402e.b(), this.f1402e.i(), this.f1402e.j(), this.f1402e.k(), vector, this.f1401d);
        } catch (Throwable th) {
            Logger.error(this, "Unable to create NMSP manager", th);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final boolean canStartTransaction() {
        return this.f1403f;
    }

    /* access modifiers changed from: protected */
    public final d createCommandContext(Transaction transaction) {
        if (this.f1403f) {
            return new e(this.f1400c, this.f1402e, transaction);
        }
        return null;
    }

    public final ConnectionState getConnectionState() {
        return this.f1403f ? this.f1398a : ConnectionState.DISCONNECTED;
    }

    public final String getSessionID() {
        if (this.f1403f) {
            return this.f1399b;
        }
        return null;
    }

    public final String getUniqueID() {
        if (this.f1402e != null) {
            return this.f1402e.i();
        }
        return null;
    }

    public final void release() {
        super.release();
        if (this.f1403f) {
            this.f1400c.c_();
            this.f1403f = false;
            this.f1402e = null;
        }
    }
}
