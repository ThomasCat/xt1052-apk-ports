package com.nuance.dragon.toolkit.data.a;

import com.nuance.a.a.a.b.c.c.c;
import com.nuance.a.a.a.b.c.c.d;
import com.nuance.a.a.a.b.c.c.l;
import com.nuance.dragon.toolkit.data.Data;
import com.nuance.dragon.toolkit.data.Data.Bytes;
import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.data.Data.Integer;
import com.nuance.dragon.toolkit.data.Data.Null;
import com.nuance.dragon.toolkit.data.Data.Sequence;
import com.nuance.dragon.toolkit.data.Data.String;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.Frequencies;
import java.util.Enumeration;
import java.util.Map.Entry;

public final class a {
    public static c a(Dictionary dictionary, d dVar) {
        c j = dVar.j();
        for (Entry entry : dictionary.getEntries()) {
            String str = (String) entry.getKey();
            Data data = (Data) entry.getValue();
            switch (data.getType()) {
                case 4:
                    j.a(str, ((Bytes) data).value);
                    break;
                case 16:
                    j.a(str, a((Sequence) data, dVar));
                    break;
                case Frequencies.FREQ_22KHZ /*22*/:
                    j.a(str, ((String) data).value);
                    break;
                case 192:
                    j.b(str, ((Integer) data).value);
                    break;
                case 193:
                    j.b(str, ((String) data).value);
                    break;
                case 224:
                    j.a(str, a((Dictionary) data, dVar));
                    break;
            }
        }
        return j;
    }

    private static l a(Sequence sequence, d dVar) {
        l k = dVar.k();
        for (Data data : sequence.getValues()) {
            switch (data.getType()) {
                case 4:
                    k.b(((Bytes) data).value);
                    break;
                case 16:
                    k.a(a((Sequence) data, dVar));
                    break;
                case Frequencies.FREQ_22KHZ /*22*/:
                    k.a(((String) data).value);
                    break;
                case 192:
                    k.i(((Integer) data).value);
                    break;
                case 193:
                    k.b(((String) data).value);
                    break;
                case 224:
                    k.a(a((Dictionary) data, dVar));
                    break;
            }
        }
        return k;
    }

    public static Dictionary a(c cVar) {
        Dictionary dictionary = new Dictionary();
        Enumeration a2 = cVar.a();
        while (a2.hasMoreElements()) {
            String str = (String) a2.nextElement();
            switch (cVar.c(str)) {
                case 4:
                    dictionary.put(str, (Data) new Bytes(cVar.e(str)));
                    break;
                case 5:
                    dictionary.put(str, (Data) new Null());
                    break;
                case 16:
                    dictionary.put(str, (Data) a(cVar.i(str)));
                    break;
                case Frequencies.FREQ_22KHZ /*22*/:
                    dictionary.put(str, (Data) new String(cVar.g(str)));
                    break;
                case 192:
                    dictionary.put(str, (Data) new Integer(cVar.d(str)));
                    break;
                case 193:
                    dictionary.put(str, (Data) new String(cVar.f(str)));
                    break;
                case 224:
                    dictionary.put(str, (Data) a(cVar.h(str)));
                    break;
                default:
                    Logger.warn("Data", "Unsupported PDX type found in dictionary, skipping");
                    break;
            }
        }
        return dictionary;
    }

    private static Sequence a(l lVar) {
        Sequence sequence = new Sequence();
        for (int i = 0; i < lVar.a(); i++) {
            switch (lVar.b(i)) {
                case 4:
                    sequence.add((Data) new Bytes(lVar.d(i)));
                    break;
                case 5:
                    sequence.add((Data) new Null());
                    break;
                case 16:
                    sequence.add((Data) a(lVar.h(i)));
                    break;
                case Frequencies.FREQ_22KHZ /*22*/:
                    sequence.add((Data) new String(lVar.f(i)));
                    break;
                case 192:
                    sequence.add((Data) new Integer(lVar.c(i)));
                    break;
                case 193:
                    sequence.add((Data) new String(lVar.e(i)));
                    break;
                case 224:
                    sequence.add((Data) a(lVar.g(i)));
                    break;
                default:
                    Logger.warn("PdxValue", "Unsupported PDX type found in sequence, skipping");
                    break;
            }
        }
        return sequence;
    }
}
