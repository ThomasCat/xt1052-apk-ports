package com.nuance.dragon.toolkit.elvis;

import android.os.Handler;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.util.WorkerThread;
import com.nuance.dragon.toolkit.util.internal.d;

final class c extends a {

    /* renamed from: c reason: collision with root package name */
    private static WorkerThread f1662c;

    /* renamed from: d reason: collision with root package name */
    private static int f1663d;

    /* renamed from: b reason: collision with root package name */
    private final boolean f1664b;

    /* renamed from: e reason: collision with root package name */
    private boolean f1665e;

    public c(FileManager fileManager) {
        this(fileManager, null);
    }

    public c(FileManager fileManager, Handler handler) {
        super(new NativeElvisImpl(fileManager), handler);
        d.a("fileManager", (Object) fileManager);
        this.f1664b = handler == null;
    }

    /* access modifiers changed from: protected */
    public final Handler a() {
        if (f1662c == null) {
            WorkerThread workerThread = new WorkerThread();
            f1662c = workerThread;
            workerThread.start();
        }
        f1663d++;
        return f1662c.getHandler();
    }

    public final void release() {
        if (!this.f1665e) {
            this.f1665e = true;
            super.release();
            if (this.f1664b) {
                int i = f1663d - 1;
                f1663d = i;
                if (i == 0) {
                    f1662c.stop();
                    f1662c = null;
                }
            }
        }
    }
}
