package com.nuance.dragon.toolkit.grammar.content;

import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.grammar.WordAction;
import com.nuance.dragon.toolkit.grammar.WordList;
import com.nuance.dragon.toolkit.grammar.WordList.WordIterator;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.d;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class a extends WordList {

    /* renamed from: a reason: collision with root package name */
    private static int f1810a = 0;

    /* renamed from: b reason: collision with root package name */
    private final FileManager f1811b;

    /* renamed from: c reason: collision with root package name */
    private final String f1812c;

    /* renamed from: d reason: collision with root package name */
    private final boolean f1813d;

    /* renamed from: e reason: collision with root package name */
    private final com.nuance.dragon.toolkit.util.internal.a f1814e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public final Object f1815f;

    /* renamed from: g reason: collision with root package name */
    private b f1816g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public List f1817h;
    /* access modifiers changed from: private */
    public List i;
    /* access modifiers changed from: private */
    public List j;
    /* access modifiers changed from: private */
    public List k;
    /* access modifiers changed from: private */
    public List l;
    /* access modifiers changed from: private */
    public String m;
    /* access modifiers changed from: private */
    public int n;
    /* access modifiers changed from: private */
    public boolean o;
    private final Object p = new Object();
    private boolean q;
    private boolean r;

    /* renamed from: com.nuance.dragon.toolkit.grammar.content.a$a reason: collision with other inner class name */
    final class C0008a {

        /* renamed from: a reason: collision with root package name */
        final List f1823a;

        /* renamed from: b reason: collision with root package name */
        final int f1824b;

        /* renamed from: c reason: collision with root package name */
        final String f1825c;

        C0008a(List list, int i, String str) {
            this.f1823a = list;
            this.f1824b = i;
            this.f1825c = str;
        }
    }

    final class b implements WordIterator {

        /* renamed from: b reason: collision with root package name */
        private final List f1827b;

        /* renamed from: c reason: collision with root package name */
        private final List f1828c;

        /* renamed from: d reason: collision with root package name */
        private final List f1829d;

        /* renamed from: e reason: collision with root package name */
        private final Iterator f1830e = this.f1828c.iterator();

        /* renamed from: f reason: collision with root package name */
        private final int f1831f = this.f1828c.size();

        /* renamed from: g reason: collision with root package name */
        private int f1832g = 0;

        /* renamed from: h reason: collision with root package name */
        private int f1833h;
        private boolean i;

        b(List list, List list2) {
            this.f1828c = list2;
            this.f1827b = list;
            this.f1829d = new ArrayList(list2.size() + list.size());
            this.f1829d.addAll(this.f1827b);
        }

        private void a(boolean z) {
            d.a((Object) this, !this.i);
            this.i = true;
            a.a(a.this, this, this.f1827b, z ? this.f1829d : null, getChecksum());
        }

        public final void acceptChanges() {
            a(this.f1832g > 0);
        }

        public final void discardChanges() {
            a(false);
        }

        public final int getChecksum() {
            return this.f1833h;
        }

        public final int getCount() {
            return this.f1831f;
        }

        public final boolean hasNext() {
            return this.f1830e.hasNext();
        }

        public final /* synthetic */ Object next() {
            WordAction wordAction = (WordAction) this.f1830e.next();
            this.f1832g++;
            if (wordAction.isAdded()) {
                this.f1833h += wordAction.getWord().hashCode();
                this.f1829d.add(wordAction);
            } else if (this.f1829d.remove(new WordAction(wordAction.getWord(), true))) {
                this.f1833h -= wordAction.getWord().hashCode();
            }
            return wordAction;
        }

        public final void remove() {
        }
    }

    a(FileManager fileManager, String str, boolean z, Object obj) {
        this.f1811b = fileManager;
        this.f1812c = str;
        this.f1813d = z;
        this.f1814e = new com.nuance.dragon.toolkit.util.internal.a();
        this.f1815f = obj;
        synchronized (this.p) {
            this.q = true;
        }
    }

    private WordIterator a(boolean z) {
        b bVar;
        a();
        synchronized (this.f1815f) {
            if (z) {
                this.f1816g = new b(new ArrayList(0), this.f1817h);
            } else if (this.o) {
                this.f1816g = new b(this.k, this.l);
            } else {
                this.f1816g = new b(this.i, this.j);
            }
            bVar = this.f1816g;
        }
        return bVar;
    }

    private void a() {
        synchronized (this.p) {
            while (true) {
                if (this.r) {
                    break;
                } else if (!this.q) {
                    Logger.error(this, "Not initialized or initializing!");
                    break;
                } else {
                    try {
                        this.p.wait();
                    } catch (InterruptedException e2) {
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return;
     */
    static /* synthetic */ void a(a aVar, WordIterator wordIterator, List list, final List list2, final int i2) {
        synchronized (aVar.f1815f) {
            if (wordIterator != aVar.f1816g) {
                Logger.error(aVar, "Can't accept changes on old iterator");
                return;
            }
            aVar.f1816g = null;
            if (list2 != null) {
                aVar.o = true;
                aVar.k = list2;
                aVar.l = b(list, list2);
                aVar.n = i2;
                aVar.f1814e.a((Runnable) new Runnable() {
                    public final void run() {
                        synchronized (a.this.f1815f) {
                            if (!a.this.o) {
                                Logger.error(a.this, "Accepting unexpectedly interrupted");
                                return;
                            }
                            a.this.n = i2;
                            a.this.i = list2;
                            a aVar = a.this;
                            a aVar2 = a.this;
                            aVar.j = a.b(a.this.f1817h, list2);
                            a.this.k = null;
                            a.this.l = null;
                            a.this.o = false;
                            a.this.a(new C0008a(list2, i2, a.this.m));
                        }
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean a(C0008a aVar) {
        Logger.info(this, "Writing to file " + this.f1812c);
        boolean z = false;
        ObjectOutputStream b2 = ContentManager.b(this.f1811b, this.f1812c);
        if (b2 != null) {
            try {
                b2.writeInt(f1810a);
                b2.writeUTF(aVar.f1825c);
                b2.writeInt(aVar.f1824b);
                WordAction.writeListTo(aVar.f1823a, b2);
                new StringBuilder("Saved ").append(aVar.f1823a.size()).append(" items");
                z = true;
            } catch (IOException e2) {
                Logger.error(this, "Error reading to file " + this.f1812c, e2);
            }
            try {
                b2.close();
            } catch (IOException e3) {
            }
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    private C0008a b() {
        String str;
        List list;
        List list2;
        int i2;
        String str2;
        Logger.info(this, "Reading from file " + this.f1812c);
        int i3 = 0;
        ObjectInputStream a2 = ContentManager.a(this.f1811b, this.f1812c);
        if (a2 != null) {
            try {
                if (a2.readInt() == f1810a) {
                    str = a2.readUTF();
                    try {
                        i3 = a2.readInt();
                        list = WordAction.readListFrom(a2);
                    } catch (IOException e2) {
                        list2 = null;
                        String str3 = str;
                        e = e2;
                        i2 = i3;
                        str2 = str3;
                    }
                    try {
                        new StringBuilder("Loaded ").append(list.size()).append(" items");
                    } catch (IOException e3) {
                        Throwable th = e3;
                        list2 = list;
                        i2 = i3;
                        str2 = str;
                        e = th;
                        Logger.error(this, "Error reading from file " + this.f1812c, e);
                        str = str2;
                        i3 = i2;
                        list = list2;
                        a2.close();
                        if (list == null) {
                        }
                    }
                } else {
                    str = null;
                    list = null;
                }
            } catch (IOException e4) {
                e = e4;
                i2 = 0;
                list2 = null;
                str2 = null;
                Logger.error(this, "Error reading from file " + this.f1812c, e);
                str = str2;
                i3 = i2;
                list = list2;
                a2.close();
                if (list == null) {
                }
            }
            try {
                a2.close();
            } catch (IOException e5) {
            }
        } else {
            str = null;
            list = null;
        }
        if (list == null) {
            return null;
        }
        return new C0008a(list, i3, str);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.grammar.WordAction>, for r5v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.grammar.WordAction>] */
    public static List b(List<WordAction> list, List list2) {
        if (list2.isEmpty()) {
            return list;
        }
        ArrayList arrayList = new ArrayList();
        ArrayList<WordAction> arrayList2 = new ArrayList<>(list2);
        for (WordAction wordAction : list) {
            if (arrayList2.contains(wordAction)) {
                arrayList2.remove(wordAction);
            } else {
                arrayList.add(wordAction);
            }
        }
        for (WordAction word : arrayList2) {
            arrayList.add(new WordAction(word.getWord(), false));
        }
        return arrayList;
    }

    /* access modifiers changed from: 0000 */
    public final void a(final List list) {
        this.f1814e.a((Runnable) new Runnable() {
            public final void run() {
                synchronized (a.this.f1815f) {
                    a.this.f1817h = list;
                    a aVar = a.this;
                    a aVar2 = a.this;
                    aVar.j = a.b(list, a.this.i);
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0031 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004e  */
    public final void a(List list, String str, boolean z) {
        int i2;
        List b2;
        List list2 = null;
        this.m = str;
        if (!z && !this.f1813d) {
            C0008a b3 = b();
            if (b3 != null && str.equals(b3.f1825c)) {
                List list3 = b3.f1823a;
                list2 = list3;
                i2 = b3.f1824b;
                if (list2 != null) {
                    list2 = new ArrayList(0);
                    this.f1811b.delete(this.f1812c);
                    b2 = list;
                } else {
                    b2 = b(list, list2);
                }
                synchronized (this.f1815f) {
                    this.n = i2;
                    this.i = list2;
                    this.f1817h = list;
                    this.j = b2;
                }
                synchronized (this.p) {
                    if (!this.r) {
                        this.q = false;
                        this.r = true;
                        this.p.notifyAll();
                    }
                }
            }
        }
        i2 = 0;
        if (list2 != null) {
        }
        synchronized (this.f1815f) {
        }
        synchronized (this.p) {
        }
    }

    public final boolean fullUpdateRequired() {
        a();
        synchronized (this.f1815f) {
            if (this.o) {
                return false;
            }
            if (!this.i.isEmpty() || this.f1817h.isEmpty()) {
                return this.j.size() > this.f1817h.size();
            }
            return true;
        }
    }

    public final int getAcceptedChecksum() {
        int i2;
        a();
        synchronized (this.f1815f) {
            i2 = this.n;
        }
        return i2;
    }

    public final WordIterator getFullIterator() {
        return a(true);
    }

    public final WordIterator getModifiedIterator() {
        return a(false);
    }
}
