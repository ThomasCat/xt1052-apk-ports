package b;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class d implements b {

    /* renamed from: a reason: collision with root package name */
    private final HashMap f95a = new HashMap();

    /* renamed from: b reason: collision with root package name */
    private final List f96b = new ArrayList();

    public d(String str) {
        a("command", str);
    }

    public String a(String str) {
        return (String) this.f95a.get(str);
    }

    public List a() {
        return this.f96b;
    }

    public void a(c cVar) {
        this.f96b.add(cVar);
    }

    public void a(String str, String str2) {
        if (str2 != null) {
            this.f95a.put(str, str2);
        } else {
            this.f95a.remove(str);
        }
    }

    public String toString() {
        return this.f95a.toString() + (this.f96b.size() > 0 ? ", Entries=" + this.f96b.toString() : "");
    }
}
