package com.nuance.dragon.toolkit.grammar;

import com.nuance.dragon.toolkit.cloudservices.CloudServices;
import com.nuance.dragon.toolkit.cloudservices.TransactionError;
import com.nuance.dragon.toolkit.cloudservices.TransactionResult;
import com.nuance.dragon.toolkit.cloudservices.datauploader.CloudDataUploader;
import com.nuance.dragon.toolkit.cloudservices.datauploader.CloudDataUploader.DeleteListener;
import com.nuance.dragon.toolkit.cloudservices.datauploader.CloudDataUploader.UploadListener;
import com.nuance.dragon.toolkit.elvis.Grammar;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.grammar.content.ContentManager;
import com.nuance.dragon.toolkit.grammar.content.ContentManager.SyncListener;
import com.nuance.dragon.toolkit.language.Language;
import com.nuance.dragon.toolkit.language.Languages;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.a.b;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GrammarDepot implements JSONCompliant {

    /* renamed from: a reason: collision with root package name */
    private final Map f1694a;

    /* renamed from: b reason: collision with root package name */
    private final List f1695b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public GrammarEntry f1696c;

    /* renamed from: d reason: collision with root package name */
    private final Map f1697d;

    /* renamed from: e reason: collision with root package name */
    private final FileManager f1698e;

    /* renamed from: f reason: collision with root package name */
    private final HashMap f1699f;

    /* renamed from: g reason: collision with root package name */
    private final HashMap f1700g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public HashMap f1701h;
    /* access modifiers changed from: private */
    public final HashMap i;
    /* access modifiers changed from: private */
    public final ArrayList j;
    /* access modifiers changed from: private */
    public int k;
    /* access modifiers changed from: private */
    public boolean l;
    private String m;

    public class ContentManagerEntry {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public String f1712a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public ContentManager f1713b;

        public ContentManagerEntry(String str, ContentManager contentManager) {
            d.a("name", str);
            d.a("contentManager", (Object) contentManager);
            this.f1712a = str;
            this.f1713b = contentManager;
        }
    }

    public class GrammarEntry {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final Language f1714a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public final Map f1715b = new HashMap();
        /* access modifiers changed from: private */

        /* renamed from: c reason: collision with root package name */
        public final Grammar f1716c;

        /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.grammar.Scenario>, for r6v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.grammar.Scenario>] */
        public GrammarEntry(Language language, List<Scenario> list, Grammar grammar) {
            this.f1714a = language;
            this.f1716c = grammar;
            for (Scenario scenario : list) {
                this.f1715b.put(scenario.getName(), scenario);
            }
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (!(obj instanceof GrammarEntry)) {
                return false;
            }
            GrammarEntry grammarEntry = (GrammarEntry) obj;
            if (this.f1716c == null) {
                if (grammarEntry.f1716c != null) {
                    return false;
                }
            } else if (!this.f1716c.equals(grammarEntry.f1716c)) {
                return false;
            }
            if (this.f1714a == null) {
                if (grammarEntry.f1714a != null) {
                    return false;
                }
            } else if (!this.f1714a.equals(grammarEntry.f1714a)) {
                return false;
            }
            return this.f1715b == null ? grammarEntry.f1715b == null : this.f1715b.equals(grammarEntry.f1715b);
        }

        public final int hashCode() {
            int i = 0;
            int hashCode = ((this.f1714a == null ? 0 : this.f1714a.hashCode()) + (((this.f1716c == null ? 0 : this.f1716c.hashCode()) + 31) * 31)) * 31;
            if (this.f1715b != null) {
                i = this.f1715b.hashCode();
            }
            return hashCode + i;
        }
    }

    public interface GrammarUploadListener {
        void onComplete(String str, int i);
    }

    public final class GrammarUploadStatus {
        public static final int INVALID_WORD_LIST_ID = 2;
        public static final int NOTHING_TO_UPLOAD = 1;
        public static final int SUCCESS = 0;
        public static final int UNKNOWN_ERROR = 5;
        public static final int UPLOAD_ERROR = 3;
        public static final int UPLOAD_IN_PROGRESS = 4;
    }

    final class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public String f1717a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public CloudServices f1718b;
        /* access modifiers changed from: private */

        /* renamed from: c reason: collision with root package name */
        public boolean f1719c;
        /* access modifiers changed from: private */

        /* renamed from: d reason: collision with root package name */
        public int f1720d;
        /* access modifiers changed from: private */

        /* renamed from: e reason: collision with root package name */
        public GrammarUploadListener f1721e;

        public a(String str, CloudServices cloudServices, boolean z, int i, GrammarUploadListener grammarUploadListener) {
            this.f1717a = str;
            this.f1718b = cloudServices;
            this.f1719c = z;
            this.f1720d = i;
            this.f1721e = grammarUploadListener;
        }
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.grammar.GrammarDepot$GrammarEntry>, for r7v0, types: [java.util.List, java.util.Collection, java.util.List<com.nuance.dragon.toolkit.grammar.GrammarDepot$GrammarEntry>, java.lang.Object] */
    public GrammarDepot(List<GrammarEntry> list) {
        d.a("grammars", (Object) list);
        d.a("grammars", "non-empty", !list.isEmpty());
        this.f1695b = new ArrayList(list.size());
        this.f1695b.addAll(list);
        this.f1694a = new HashMap();
        for (GrammarEntry grammarEntry : list) {
            this.f1694a.put(grammarEntry.f1714a.getGrammarLanguage(), grammarEntry);
        }
        this.f1696c = (GrammarEntry) list.get(0);
        this.f1697d = null;
        this.f1698e = null;
        this.f1699f = null;
        this.f1700g = null;
        this.i = new HashMap();
        this.j = new ArrayList();
        this.k = 0;
        this.l = false;
        this.m = "NVC_DATA_UPLOAD_CMD";
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.grammar.GrammarDepot$ContentManagerEntry>, for r8v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.grammar.GrammarDepot$ContentManagerEntry>, java.lang.Object] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.grammar.GrammarDepot$GrammarEntry>, for r7v0, types: [java.util.List, java.util.Collection, java.util.List<com.nuance.dragon.toolkit.grammar.GrammarDepot$GrammarEntry>, java.lang.Object] */
    public GrammarDepot(List<GrammarEntry> list, List<ContentManagerEntry> list2, FileManager fileManager) {
        boolean z = true;
        d.a("grammars", (Object) list);
        d.a("grammars", "non-empty", !list.isEmpty());
        d.a("contentManagers", (Object) list2);
        String str = "contentManagers";
        String str2 = "non-empty";
        if (list2.isEmpty()) {
            z = false;
        }
        d.a(str, str2, z);
        d.a("fileManager", (Object) fileManager);
        this.f1695b = new ArrayList(list.size());
        this.f1695b.addAll(list);
        this.f1694a = new HashMap();
        for (GrammarEntry grammarEntry : list) {
            this.f1694a.put(grammarEntry.f1714a.getGrammarLanguage(), grammarEntry);
        }
        this.f1696c = (GrammarEntry) list.get(0);
        this.f1697d = new HashMap();
        for (ContentManagerEntry contentManagerEntry : list2) {
            if (contentManagerEntry.f1712a == null || contentManagerEntry.f1712a.length() == 0 || contentManagerEntry.f1713b == null) {
                throw new IllegalArgumentException("ContentManagerEntry cannot have an empty or null name or ContentManager.");
            } else if (this.f1697d.containsKey(contentManagerEntry.f1712a)) {
                throw new IllegalArgumentException("contentManagers must not contain 2 ContentManagerEntry with the same name.");
            } else {
                final int size = list2.size();
                contentManagerEntry.f1713b.setInitListener("GrammarDepot", new SyncListener() {
                    public final void onDone(ContentManager contentManager) {
                        GrammarDepot.this.k = GrammarDepot.this.k + 1;
                        if (GrammarDepot.this.k >= size) {
                            GrammarDepot.this.l = true;
                            Iterator it = GrammarDepot.this.j.iterator();
                            while (it.hasNext()) {
                                a aVar = (a) it.next();
                                if (aVar.f1717a != null) {
                                    GrammarDepot.this.uploadServerWordList(aVar.f1717a, aVar.f1718b, aVar.f1719c, aVar.f1720d, aVar.f1721e);
                                } else {
                                    GrammarDepot.this.uploadServerWordLists(aVar.f1718b, aVar.f1719c, aVar.f1720d, aVar.f1721e);
                                }
                            }
                            GrammarDepot.this.j.clear();
                        }
                    }
                });
                this.f1697d.put(contentManagerEntry.f1712a, contentManagerEntry.f1713b);
            }
        }
        this.f1698e = fileManager;
        this.f1699f = new HashMap();
        this.f1700g = new HashMap();
        this.i = new HashMap();
        this.j = new ArrayList();
        this.k = 0;
        this.l = false;
        this.m = "NVC_DATA_UPLOAD_CMD";
    }

    /* access modifiers changed from: private */
    public WordList a(String str, boolean z) {
        String str2 = this.f1696c.f1714a + str;
        if (this.f1700g.containsKey(str2) && !z) {
            return (WordList) this.f1700g.get(str2);
        }
        if (this.f1697d == null || this.f1698e == null) {
            Logger.error(this, "Missing content manager or file manager.");
            return null;
        }
        ContentManager contentManager = (ContentManager) this.f1697d.get(str);
        if (contentManager == null) {
            Logger.error(this, "Cannot find content manager for word list: " + str);
            return null;
        }
        WordList createWordList = contentManager.createWordList(this.f1698e, str2 + ".lst", z);
        this.f1700g.put(str2, createWordList);
        return createWordList;
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.grammar.GrammarDepot$ContentManagerEntry>, for r19v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.grammar.GrammarDepot$ContentManagerEntry>] */
    public static GrammarDepot createFromJSON(JSONObject jSONObject, FileManager fileManager, List<ContentManagerEntry> list) {
        HashMap hashMap;
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            hashMap = new HashMap(list.size());
            for (ContentManagerEntry contentManagerEntry : list) {
                if (contentManagerEntry.f1712a == null || contentManagerEntry.f1712a.length() == 0 || contentManagerEntry.f1713b == null) {
                    throw new IllegalArgumentException("ContentManagerEntry cannot have an empty or null name or ContentManager.");
                } else if (hashMap.containsKey(contentManagerEntry.f1712a)) {
                    throw new IllegalArgumentException("contentManagers must not contain 2 ContentManagerEntry with the same name.");
                } else {
                    hashMap.put(contentManagerEntry.f1712a, contentManagerEntry.f1713b);
                }
            }
        } else {
            hashMap = new HashMap();
        }
        JSONArray jSONArray = jSONObject.getJSONArray("grammars");
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= jSONArray.length()) {
                return new GrammarDepot(arrayList, list, fileManager);
            }
            JSONObject jSONObject2 = jSONArray.getJSONObject(i3);
            String string = jSONObject2.getString("language");
            Language lookupFromGrammarLanguage = Languages.lookupFromGrammarLanguage(string);
            if (lookupFromGrammarLanguage == null) {
                throw new JSONException("Undefined grammar language: " + string);
            }
            JSONObject jSONObject3 = jSONObject2.getJSONObject("scenarios");
            Grammar createFromJSON = Grammar.createFromJSON(jSONObject2.getJSONObject("elvis_grammar"));
            Set<String> requiredWordListIds = createFromJSON.getRequiredWordListIds();
            HashSet hashSet = new HashSet();
            if (requiredWordListIds != null && !requiredWordListIds.isEmpty()) {
                for (String str : requiredWordListIds) {
                    if (!hashMap.containsKey(str)) {
                        throw new IllegalArgumentException("contentManagers must contain a ContentManagerEntry named: " + str);
                    }
                }
                hashSet.addAll(requiredWordListIds);
            }
            ArrayList arrayList2 = new ArrayList();
            Iterator keys = jSONObject3.keys();
            while (keys.hasNext()) {
                Scenario createFromJSON2 = Scenario.createFromJSON(jSONObject3.getJSONObject(keys.next().toString()));
                Set<String> requiredWordListIds2 = createFromJSON2.getRequiredWordListIds();
                if (requiredWordListIds2 != null && !requiredWordListIds2.isEmpty()) {
                    for (String str2 : requiredWordListIds2) {
                        if (!hashMap.containsKey(str2)) {
                            throw new IllegalArgumentException("contentManagers must contain a ContentManagerEntry named: " + str2);
                        }
                    }
                    hashSet.addAll(requiredWordListIds2);
                }
                arrayList2.add(createFromJSON2);
            }
            String str3 = "";
            for (Entry key : hashMap.entrySet()) {
                String str4 = (String) key.getKey();
                if (!hashSet.contains(str4)) {
                    if (str3.length() > 0) {
                        str3 = str3 + ", ";
                    }
                    str3 = str3 + str4;
                }
            }
            if (str3.length() > 0) {
                throw new IllegalArgumentException("The following entries in contentManagers are not referenced by this GrammarDepot: " + str3);
            }
            arrayList.add(new GrammarEntry(lookupFromGrammarLanguage, arrayList2, createFromJSON));
            i2 = i3 + 1;
        }
    }

    public static GrammarDepot createFromJSON(JSONObject jSONObject, FileManager fileManager, ContentManagerEntry... contentManagerEntryArr) {
        ArrayList arrayList = new ArrayList(contentManagerEntryArr.length);
        for (ContentManagerEntry add : contentManagerEntryArr) {
            arrayList.add(add);
        }
        return createFromJSON(jSONObject, fileManager, (List) arrayList);
    }

    public void cancelServerWordListsUpload() {
        if (this.f1701h != null) {
            HashMap hashMap = this.f1701h;
            this.f1701h = null;
            for (Entry value : hashMap.entrySet()) {
                ((CloudDataUploader) value.getValue()).cancel();
            }
        }
    }

    public void clearServerWordLists(CloudServices cloudServices, final GrammarUploadListener grammarUploadListener) {
        if (this.f1701h != null) {
            Logger.warn(this, "Upload is already in progress.  Cannot clear Word List.");
            return;
        }
        final HashMap hashMap = new HashMap(1);
        this.f1701h = hashMap;
        new CloudDataUploader(cloudServices, this.m).deleteAll(new DeleteListener() {
            public final void onError(CloudDataUploader cloudDataUploader, TransactionError transactionError) {
                if (GrammarDepot.this.f1701h == hashMap) {
                    GrammarDepot.this.f1701h = null;
                    if (grammarUploadListener != null) {
                        grammarUploadListener.onComplete("unknown", 3);
                    }
                }
            }

            public final void onSuccess(CloudDataUploader cloudDataUploader, TransactionResult transactionResult) {
                if (GrammarDepot.this.f1701h == hashMap) {
                    String str = null;
                    HashSet hashSet = new HashSet();
                    for (Entry value : GrammarDepot.this.f1696c.f1715b.entrySet()) {
                        Scenario scenario = (Scenario) value.getValue();
                        if (str == null) {
                            str = scenario.getRequiredContactListId();
                        } else {
                            scenario.getRequiredContactListId();
                        }
                        hashSet.addAll(scenario.getRequiredWordListIds());
                    }
                    if (str != null) {
                        GrammarDepot.this.a(str, true);
                    }
                    Iterator it = hashSet.iterator();
                    while (it.hasNext()) {
                        GrammarDepot.this.a((String) it.next(), true);
                    }
                    GrammarDepot.this.f1701h = null;
                    if (grammarUploadListener != null) {
                        grammarUploadListener.onComplete("unknown", 0);
                    }
                }
            }
        });
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof GrammarDepot)) {
            return false;
        }
        GrammarDepot grammarDepot = (GrammarDepot) obj;
        if (this.f1696c == null) {
            if (grammarDepot.f1696c != null) {
                return false;
            }
        } else if (!this.f1696c.equals(grammarDepot.f1696c)) {
            return false;
        }
        if (this.f1698e == null) {
            if (grammarDepot.f1698e != null) {
                return false;
            }
        } else if (!this.f1698e.equals(grammarDepot.f1698e)) {
            return false;
        }
        if (this.f1694a == null) {
            if (grammarDepot.f1694a != null) {
                return false;
            }
        } else if (!this.f1694a.equals(grammarDepot.f1694a)) {
            return false;
        }
        if (this.f1695b == null) {
            if (grammarDepot.f1695b != null) {
                return false;
            }
        } else if (!this.f1695b.equals(grammarDepot.f1695b)) {
            return false;
        }
        if (this.f1699f == null) {
            if (grammarDepot.f1699f != null) {
                return false;
            }
        } else if (!this.f1699f.equals(grammarDepot.f1699f)) {
            return false;
        }
        if (this.f1700g == null) {
            if (grammarDepot.f1700g != null) {
                return false;
            }
        } else if (!this.f1700g.equals(grammarDepot.f1700g)) {
            return false;
        }
        return this.f1701h == null ? grammarDepot.f1701h == null : this.f1701h.equals(grammarDepot.f1701h);
    }

    public int getAcceptedWordListChecksum(String str) {
        return a(str, false).getAcceptedChecksum();
    }

    public List getAvailableLanguages() {
        ArrayList arrayList = new ArrayList(this.f1695b.size());
        for (GrammarEntry a2 : this.f1695b) {
            arrayList.add(a2.f1714a);
        }
        return arrayList;
    }

    public Grammar getElvisGrammar() {
        WordList wordList;
        Grammar c2 = this.f1696c.f1716c;
        for (String str : c2.getRequiredWordListIds()) {
            String str2 = this.f1696c.f1714a + str;
            if (this.f1699f.containsKey(str2)) {
                wordList = (WordList) this.f1699f.get(str2);
            } else if (this.f1697d == null || this.f1698e == null) {
                wordList = null;
            } else {
                ContentManager contentManager = (ContentManager) this.f1697d.get(str);
                if (contentManager == null) {
                    wordList = null;
                } else {
                    wordList = contentManager.createWordList(this.f1698e, str2 + ".lst", false);
                    this.f1699f.put(str2, wordList);
                }
            }
            if (wordList == null) {
                Logger.error(this, "Missing required Elvis wordlist: " + str);
                return null;
            } else if (!c2.attachWordList(wordList, str)) {
                Logger.error(this, "Failed to attach required Elvis wordlist: " + str);
                return null;
            }
        }
        return c2;
    }

    public Language getLanguage() {
        return this.f1696c.f1714a;
    }

    public Scenario getScenario(String str) {
        Scenario scenario = (Scenario) this.f1696c.f1715b.get(str);
        String requiredContactListId = scenario.getRequiredContactListId();
        if (requiredContactListId != null) {
            WordList a2 = a(requiredContactListId, false);
            if (a2 == null) {
                Logger.error(this, "Missing required Server contact list: " + requiredContactListId);
                return null;
            } else if (!scenario.attachWordList(a2, requiredContactListId)) {
                Logger.error(this, "Failed to attach required Server contact list: " + requiredContactListId);
                return null;
            }
        }
        for (String str2 : scenario.getRequiredWordListIds()) {
            WordList a3 = a(str2, false);
            if (a3 == null) {
                Logger.error(this, "Missing required Server wordlist: " + str2);
                return null;
            } else if (!scenario.attachWordList(a3, str2)) {
                Logger.error(this, "Failed to attach required Server wordlist: " + str2);
                return null;
            }
        }
        return scenario;
    }

    public final int hashCode() {
        int i2 = 0;
        int hashCode = ((this.f1700g == null ? 0 : this.f1700g.hashCode()) + (((this.f1699f == null ? 0 : this.f1699f.hashCode()) + (((this.f1695b == null ? 0 : this.f1695b.hashCode()) + (((this.f1694a == null ? 0 : this.f1694a.hashCode()) + (((this.f1698e == null ? 0 : this.f1698e.hashCode()) + (((this.f1696c == null ? 0 : this.f1696c.hashCode()) + 31) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.f1701h != null) {
            i2 = this.f1701h.hashCode();
        }
        return hashCode + i2;
    }

    public List resetServerWordLists(List list) {
        ArrayList arrayList = new ArrayList();
        if (list == null || list.isEmpty()) {
            return arrayList;
        }
        HashSet hashSet = new HashSet(list.size());
        hashSet.addAll(list);
        HashSet hashSet2 = new HashSet();
        String str = null;
        for (Entry value : this.f1696c.f1715b.entrySet()) {
            Scenario scenario = (Scenario) value.getValue();
            if (str == null) {
                str = scenario.getRequiredContactListId();
            }
            hashSet2.addAll(scenario.getRequiredWordListIds());
        }
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            String str2 = (String) it.next();
            if (hashSet2.contains(str2)) {
                if (a(str2, true) == null) {
                    Logger.error(this, "Unable to reset custom word list: " + str2);
                    arrayList.add(str2);
                }
            } else if (!str2.equals("contacts") || str == null) {
                Logger.error(this, "Word list not found in grammar depot: " + str2);
                arrayList.add(str2);
            } else if (a(str, true) == null) {
                Logger.error(this, "Unable to reset contact list: " + str);
                arrayList.add(str2);
            }
        }
        return arrayList;
    }

    public void setLanguage(Language language) {
        GrammarEntry grammarEntry = (GrammarEntry) this.f1694a.get(language.getGrammarLanguage());
        d.a("language", "a configured language", grammarEntry != null);
        this.f1696c = grammarEntry;
    }

    public void setUploadCommandName(String str) {
        d.a("commandName", (Object) str);
        this.m = str;
    }

    public JSONObject toJSON() {
        b bVar = new b();
        com.nuance.dragon.toolkit.util.a.a aVar = new com.nuance.dragon.toolkit.util.a.a();
        for (GrammarEntry grammarEntry : this.f1695b) {
            b bVar2 = new b();
            b bVar3 = new b();
            for (Scenario scenario : grammarEntry.f1715b.values()) {
                bVar3.a(scenario.getName(), (JSONCompliant) scenario);
            }
            bVar2.a("language", (Object) grammarEntry.f1714a.getGrammarLanguage());
            bVar2.a("scenarios", (Object) bVar3);
            bVar2.a("elvis_grammar", (JSONCompliant) grammarEntry.f1716c);
            aVar.a(bVar2);
        }
        bVar.a("grammars", (Object) aVar);
        return bVar;
    }

    public void uploadServerWordList(String str, CloudServices cloudServices, boolean z, int i2, final GrammarUploadListener grammarUploadListener) {
        boolean z2;
        boolean z3;
        String str2;
        if (this.i.containsValue(str)) {
            Logger.warn(this, "Upload (\"" + str + "\") is already in progress. Not starting new upload.");
            if (grammarUploadListener != null) {
                grammarUploadListener.onComplete(str, 4);
            }
        } else if (this.l) {
            AnonymousClass2 r7 = new UploadListener() {
                public final void onError(CloudDataUploader cloudDataUploader, TransactionError transactionError, String str) {
                    if (GrammarDepot.this.f1701h != null) {
                        Logger.error(this, "Failed to upload word list.");
                        GrammarDepot.this.f1701h.remove(str);
                        if (GrammarDepot.this.f1701h.isEmpty()) {
                            GrammarDepot.this.f1701h = null;
                        }
                        if (grammarUploadListener != null) {
                            grammarUploadListener.onComplete(str, 3);
                        }
                    }
                }

                public final void onSuccess(CloudDataUploader cloudDataUploader, TransactionResult transactionResult, String str, boolean z) {
                    if (GrammarDepot.this.f1701h != null) {
                        GrammarDepot.this.f1701h.remove(str);
                        if (GrammarDepot.this.f1701h.isEmpty()) {
                            GrammarDepot.this.f1701h = null;
                        }
                        String str2 = (String) GrammarDepot.this.i.remove(str);
                        if (str2 == null) {
                            str2 = "unknown";
                        }
                        if (z) {
                            if (grammarUploadListener != null) {
                                grammarUploadListener.onComplete(str2, 1);
                            }
                        } else if (transactionResult != null) {
                            if (grammarUploadListener != null) {
                                grammarUploadListener.onComplete(str2, 0);
                            }
                        } else if (grammarUploadListener != null) {
                            grammarUploadListener.onComplete(str2, 5);
                        }
                    }
                }
            };
            if (this.f1701h == null) {
                this.f1701h = new HashMap();
            }
            Iterator it = this.f1696c.f1715b.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    z2 = false;
                    break;
                }
                String requiredContactListId = ((Scenario) ((Entry) it.next()).getValue()).getRequiredContactListId();
                if (requiredContactListId != null && requiredContactListId.equals(str)) {
                    z2 = true;
                    break;
                }
            }
            if (str != null) {
                WordList a2 = a(str, false);
                if (a2 == null) {
                    Logger.error(this, "Upload failed. Cannot load word list");
                    cancelServerWordListsUpload();
                    if (grammarUploadListener != null) {
                        grammarUploadListener.onComplete(str, 2);
                        return;
                    }
                    return;
                }
                String str3 = "custom_words";
                if (z2) {
                    str2 = "contacts";
                    str3 = "contacts";
                } else {
                    str2 = str;
                }
                CloudDataUploader cloudDataUploader = new CloudDataUploader(cloudServices, this.m);
                this.f1701h.put(str2, cloudDataUploader);
                this.i.put(str2, str);
                cloudDataUploader.uploadData(str2, str3, a2, i2, z, 1, r7);
                z3 = true;
            } else {
                z3 = false;
            }
            if (!z3 && grammarUploadListener != null) {
                grammarUploadListener.onComplete("unknown", 1);
            }
        } else {
            this.j.add(new a(str, cloudServices, z, i2, grammarUploadListener));
        }
    }

    public void uploadServerWordList(String str, CloudServices cloudServices, boolean z, GrammarUploadListener grammarUploadListener) {
        uploadServerWordList(str, cloudServices, z, 70, grammarUploadListener);
    }

    public void uploadServerWordLists(CloudServices cloudServices, GrammarUploadListener grammarUploadListener) {
        uploadServerWordLists(cloudServices, false, 70, grammarUploadListener);
    }

    public void uploadServerWordLists(CloudServices cloudServices, boolean z, int i2, final GrammarUploadListener grammarUploadListener) {
        if (this.f1701h != null) {
            Logger.warn(this, "Upload is already in progress. Not starting new upload.");
            if (grammarUploadListener != null) {
                grammarUploadListener.onComplete("unknown", 4);
            }
        } else if (this.l) {
            final HashMap hashMap = new HashMap();
            AnonymousClass3 r7 = new UploadListener() {
                public final void onError(CloudDataUploader cloudDataUploader, TransactionError transactionError, String str) {
                    if (GrammarDepot.this.f1701h == hashMap) {
                        Logger.error(this, "Failed to upload word list.");
                        GrammarDepot.this.f1701h = null;
                        hashMap.remove(str);
                        for (Entry value : hashMap.entrySet()) {
                            ((CloudDataUploader) value.getValue()).cancel();
                        }
                        if (grammarUploadListener != null) {
                            grammarUploadListener.onComplete("unknown", 3);
                        }
                    }
                }

                public final void onSuccess(CloudDataUploader cloudDataUploader, TransactionResult transactionResult, String str, boolean z) {
                    if (GrammarDepot.this.f1701h == hashMap) {
                        if (transactionResult != null || z) {
                            hashMap.remove(str);
                            if (hashMap.isEmpty()) {
                                GrammarDepot.this.f1701h = null;
                                if (grammarUploadListener != null) {
                                    grammarUploadListener.onComplete("unknown", 0);
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        Logger.error(this, "Upload command has not been constructed properly");
                        onError(cloudDataUploader, null, str);
                    }
                }
            };
            String str = null;
            HashSet hashSet = new HashSet();
            for (Entry value : this.f1696c.f1715b.entrySet()) {
                Scenario scenario = (Scenario) value.getValue();
                if (str == null) {
                    str = scenario.getRequiredContactListId();
                } else {
                    scenario.getRequiredContactListId();
                }
                hashSet.addAll(scenario.getRequiredWordListIds());
            }
            this.f1701h = hashMap;
            boolean z2 = false;
            if (str != null) {
                WordList a2 = a(str, false);
                if (a2 == null) {
                    Logger.error(this, "Upload failed.  Cannot load contact list");
                    cancelServerWordListsUpload();
                    if (grammarUploadListener != null) {
                        grammarUploadListener.onComplete(str, 2);
                        return;
                    }
                    return;
                }
                CloudDataUploader cloudDataUploader = new CloudDataUploader(cloudServices, this.m);
                hashMap.put("contacts", cloudDataUploader);
                cloudDataUploader.uploadData("contacts", "contacts", a2, i2, z, 1, r7);
                z2 = true;
            }
            Iterator it = hashSet.iterator();
            while (it.hasNext()) {
                String str2 = (String) it.next();
                WordList a3 = a(str2, false);
                if (a3 == null) {
                    Logger.error(this, "Upload failed.  Cannot load word list:" + str2);
                    cancelServerWordListsUpload();
                    if (grammarUploadListener != null) {
                        grammarUploadListener.onComplete(str2, 2);
                        return;
                    }
                    return;
                }
                CloudDataUploader cloudDataUploader2 = new CloudDataUploader(cloudServices, this.m);
                hashMap.put(str2, cloudDataUploader2);
                cloudDataUploader2.uploadData(str2, "custom_words", a3, i2, z, 1, r7);
                z2 = true;
            }
            if (!z2 && grammarUploadListener != null) {
                this.f1701h = null;
                grammarUploadListener.onComplete("unknown", 1);
            }
        } else {
            this.j.add(new a(null, cloudServices, z, i2, grammarUploadListener));
        }
    }

    public void uploadServerWordLists(CloudServices cloudServices, boolean z, GrammarUploadListener grammarUploadListener) {
        uploadServerWordLists(cloudServices, z, 70, grammarUploadListener);
    }
}
