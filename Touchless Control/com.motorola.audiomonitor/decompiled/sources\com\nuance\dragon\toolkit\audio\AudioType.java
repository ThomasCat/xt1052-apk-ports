package com.nuance.dragon.toolkit.audio;

import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.a.b;
import org.json.JSONException;
import org.json.JSONObject;

public class AudioType implements JSONCompliant {
    public static final AudioType PCM_11k = new AudioType(Encoding.PCM_16, 11025);
    public static final AudioType PCM_16k = new AudioType(Encoding.PCM_16, 16000);
    public static final AudioType PCM_22k = new AudioType(Encoding.PCM_16, 22050);
    public static final AudioType PCM_44k = new AudioType(Encoding.PCM_16, 44100);
    public static final AudioType PCM_8k = new AudioType(Encoding.PCM_16, 8000);
    public static final AudioType SPEEX_NB = new AudioType(Encoding.SPEEX, 8000);
    public static final AudioType SPEEX_WB = new AudioType(Encoding.SPEEX, 16000);
    public static final AudioType UNKNOWN = new AudioType(Encoding.UNKNOWN, 0);
    public final Encoding encoding;
    public final int frequency;

    public enum Encoding {
        PCM_16,
        SPEEX,
        UNKNOWN
    }

    public class Frequency {
        public static final int FREQ_11KHZ = 11025;
        public static final int FREQ_16KHZ = 16000;
        public static final int FREQ_22KHZ = 22050;
        public static final int FREQ_44KHZ = 44100;
        public static final int FREQ_8KHZ = 8000;
    }

    public AudioType(Encoding encoding2, int i) {
        this.frequency = i;
        this.encoding = encoding2;
    }

    public static AudioType fromJSON(JSONObject jSONObject) {
        Encoding[] values;
        int i = jSONObject.getInt("freq");
        String string = jSONObject.getString("enc");
        for (Encoding encoding2 : Encoding.values()) {
            if (encoding2.name().equals(string)) {
                return new AudioType(encoding2, i);
            }
        }
        throw new JSONException("Encoding '" + string + "' not valid");
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof AudioType)) {
            return false;
        }
        AudioType audioType = (AudioType) obj;
        return this.encoding.equals(audioType.encoding) && this.frequency == audioType.frequency;
    }

    public int getDuration(int i) {
        if (this.encoding == Encoding.PCM_16) {
            return (i * 1000) / this.frequency;
        }
        Logger.error(this, "Unable to detect duration for encoding " + this.encoding.name());
        return 0;
    }

    public int getDuration(short[] sArr) {
        return getDuration(sArr.length);
    }

    public int getSampleCount(int i) {
        return (this.frequency * i) / 1000;
    }

    public final int hashCode() {
        return this.encoding.hashCode() + this.frequency;
    }

    public JSONObject toJSON() {
        b bVar = new b();
        bVar.a("freq", (Object) Integer.valueOf(this.frequency));
        bVar.a("enc", (Object) this.encoding.name());
        return bVar;
    }
}
