package com.nuance.a.a.a.b.c.b.b;

import android.os.Build.VERSION;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.a.b.a.b;
import com.nuance.a.a.a.b.c.a.e;
import com.nuance.a.a.a.b.c.b.a.d;
import com.nuance.a.a.a.b.c.c.c;
import com.nuance.a.a.a.b.c.c.f;
import com.nuance.a.a.a.b.c.c.g;
import com.nuance.a.a.a.b.c.c.h;
import com.nuance.a.a.a.b.c.c.l;
import java.util.Vector;

public class a extends d implements com.nuance.a.a.a.b.c.c.d {
    private static final C0001a k = com.nuance.a.a.a.a.b.a.a.a(a.class);
    private static String o = VERSION.RELEASE;
    private b l;
    private f m;
    private z n = null;

    public a(com.nuance.a.a.a.b.c.b.a.a aVar, f fVar, Vector vector) {
        super(aVar, fVar, vector);
        this.m = fVar;
        this.l = aVar.a();
        this.f845a = aVar.g();
        this.f845a.a((com.nuance.a.a.a.b.c.a.d) this);
        this.l.a(new com.nuance.a.a.a.a.b.a.b.a(0, null), this, Thread.currentThread(), this.l.a()[0]);
    }

    private void a(byte b2) {
        if (b2 == this.n.f889a) {
            Vector vector = (Vector) ((com.nuance.a.a.a.b.c.b.a.a) g()).h();
            if (vector != null && vector.size() > 0) {
                k.b((Object) "clearResLogsToServer() before clean the log vector tranId[" + b2 + "] log list size [" + vector.size() + "]");
                int i = 0;
                while (i < vector.size()) {
                    com.nuance.a.a.a.b.c.b.b.z.a aVar = (com.nuance.a.a.a.b.c.b.b.z.a) vector.elementAt(i);
                    if (aVar.e()) {
                        vector.removeElement(aVar);
                    } else {
                        i++;
                    }
                }
                k.b((Object) "clearResLogsToServer() after clean the log vector tranId[" + b2 + "] log list size [" + vector.size() + "]");
            }
        }
    }

    public final com.nuance.a.a.a.b.c.c.a a(String str) {
        if (str != null) {
            return new c(str, this.f845a, this.l);
        }
        throw new NullPointerException("name can not be null");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005f, code lost:
        if (r30.equals("") != false) goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x007e, code lost:
        if (r32.equals("") != false) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0021, code lost:
        if (r28.equals("") != false) goto L_0x0023;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0040, code lost:
        if (r29.equals("") != false) goto L_0x0042;
     */
    public final com.nuance.a.a.a.b.c.c.b a(g gVar, String str, String str2, String str3, String str4, String str5, c cVar, long j) {
        z zVar;
        String str6 = "";
        if (gVar == null) {
            str6 = str6 + "commandListener is invalid; ";
        }
        if (str != null) {
        }
        str6 = str6 + "cmd should be non-null; ";
        if (str2 != null) {
        }
        str6 = str6 + "scriptVersion should be non-null; ";
        if (str3 != null) {
        }
        str6 = str6 + "dictationLanguage should be non-null; ";
        if (str5 != null) {
        }
        str6 = str6 + "phoneModel should be non-null; ";
        if (j <= 0) {
            str6 = str6 + "commandTimeout is invalid; ";
        }
        if (!str6.equals("")) {
            k.e("NMASResourceImpl.createCommand() " + str6);
            throw new IllegalArgumentException(str6);
        }
        synchronized (this.f851g) {
            if (this.n != null) {
                this.n.e();
            }
            this.f845a.b();
            if (this.f850f == 0) {
                this.l.a(new com.nuance.a.a.a.a.b.a.b.a(0, null), this, Thread.currentThread(), this.l.a()[0]);
            }
            this.i = this.f845a.f();
            this.n = new z(this.l, gVar, str, ((com.nuance.a.a.a.b.c.b.a.a) this.f849e).f836g, this.f845a, ((com.nuance.a.a.a.b.c.b.a.a) this.f849e).d(), "1", o, str2, "enus", "ne", ((com.nuance.a.a.a.b.c.b.a.a) this.f849e).e(), str3, str4, str5, ((com.nuance.a.a.a.b.c.b.a.a) this.f849e).d(), "", j, cVar, this, this.m, this.i);
            zVar = this.n;
        }
        return zVar;
    }

    public final h a(String str, c cVar) {
        if (str == null) {
            throw new NullPointerException("name can not be null");
        } else if (cVar != null) {
            return new h(str, (i) cVar);
        } else {
            throw new NullPointerException("dict can not be null");
        }
    }

    public final h a(String str, c cVar, com.nuance.a.a.a.b.b.a aVar) {
        if (str == null) {
            throw new NullPointerException("name can not be null");
        } else if (cVar == null) {
            throw new NullPointerException("tts_dict can not be null");
        } else if (aVar != null) {
            return new x(str, this.f845a, (i) cVar, aVar);
        } else {
            throw new NullPointerException("audioSink can not be null");
        }
    }

    public final h a(String str, String str2) {
        if (str == null) {
            throw new NullPointerException("name can not be null");
        } else if (str2 != null) {
            return new e(str, str2);
        } else {
            throw new NullPointerException("choicename can not be null");
        }
    }

    public final h a(String str, byte[] bArr) {
        if (str == null) {
            throw new NullPointerException("name can not be null");
        } else if (bArr != null) {
            return new g(str, bArr);
        } else {
            throw new NullPointerException("data can not be null");
        }
    }

    public final void a() {
        k.b((Object) "freeResource() disconnectTimeout:0");
        if (this.n != null) {
            this.n.e();
        }
        synchronized (this.f851g) {
            if (this.f850f == 2) {
                this.f850f = 0;
                this.l.a(new com.nuance.a.a.a.a.b.a.b.a(3, new Integer(0)), this, Thread.currentThread(), this.l.a()[0]);
            } else {
                throw new e("the resource was unloaded. ");
            }
        }
    }

    public final void a(byte b2, short s) {
        if (b2 == this.i) {
            if (this.n != null) {
                this.n.e();
            }
            super.a(b2, s);
        }
    }

    public final void a(byte b2, byte[] bArr) {
        l a2 = m.a(bArr);
        switch (a2.e()) {
            case 29185:
                a(b2);
                if (this.n != null) {
                    this.n.a((t) a2, b2);
                    return;
                }
                return;
            case 29186:
                if (this.n != null) {
                    this.n.a((r) a2, b2);
                    return;
                }
                return;
            case 29189:
                a(b2);
                if (this.n != null) {
                    this.n.a((u) a2, b2);
                    return;
                }
                return;
            default:
                k.e("Session.parseXModeMsgBcpData() Unknown command: " + a2.e() + ". ");
                return;
        }
    }

    public final void a(Object obj, Object obj2) {
        switch (((com.nuance.a.a.a.a.b.a.b.a) obj).f783a) {
            case 0:
                super.a((com.nuance.a.a.a.b.c.b.a.c) this);
                return;
            case 3:
                this.f845a.b((com.nuance.a.a.a.b.c.b.a.c) this);
                f fVar = this.m;
                return;
            default:
                super.a(obj, obj2);
                return;
        }
    }

    public final void a(short s) {
        k.b((Object) "onSessionDisconnected() reasonCode:" + s);
        if (this.n != null) {
            this.n.a(s);
        }
        super.a(s);
    }

    public final void a(byte[] bArr) {
        super.a(bArr);
        if (this.n != null) {
            this.n.a(bArr);
        }
    }

    public final h b(String str, c cVar) {
        if (str == null) {
            throw new NullPointerException("name can not be null");
        } else if (cVar != null) {
            return new v(str, (i) cVar, 2);
        } else {
            throw new NullPointerException("dict can not be null");
        }
    }

    public final h b(String str, String str2) {
        if (str == null) {
            throw new NullPointerException("name can not be null");
        } else if (str2 != null) {
            return new y(str, str2);
        } else {
            throw new NullPointerException("text can not be null");
        }
    }

    public final h c(String str, c cVar) {
        if (str == null) {
            throw new NullPointerException("name can not be null");
        } else if (cVar != null) {
            return new v(str, (i) cVar, 3);
        } else {
            throw new NullPointerException("dict can not be null");
        }
    }

    public final void c() {
        this.n.f();
    }

    public final h d(String str, c cVar) {
        if (str == null) {
            throw new NullPointerException("name can not be null");
        } else if (cVar != null) {
            return new v(str, (i) cVar, 1);
        } else {
            throw new NullPointerException("dict can not be null");
        }
    }

    public final void d() {
        this.n.b();
    }

    public final void e() {
        this.n.c();
    }

    public final void f() {
        this.n.d();
    }

    public final long h() {
        return super.h();
    }

    public final c j() {
        return new i();
    }

    public final l k() {
        return new w();
    }
}
