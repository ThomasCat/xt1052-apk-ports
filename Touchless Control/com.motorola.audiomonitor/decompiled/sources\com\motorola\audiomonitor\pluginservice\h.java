package com.motorola.audiomonitor.pluginservice;

import android.util.Log;
import com.nuance.dragon.toolkit.recognition.CloudInterpreter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class h implements i {

    /* renamed from: a reason: collision with root package name */
    public final d f546a;

    /* renamed from: b reason: collision with root package name */
    public final b f547b;

    /* renamed from: c reason: collision with root package name */
    public final c f548c;

    /* renamed from: d reason: collision with root package name */
    public final a f549d;

    h(d dVar, b bVar, c cVar, a aVar) {
        this.f546a = dVar;
        this.f547b = bVar;
        this.f548c = cVar;
        this.f549d = aVar;
    }

    private boolean a() {
        return this.f549d != null && this.f549d.a().startsWith("SttTest");
    }

    public void a(String str, int i, float[] fArr, String[] strArr) {
        if (a()) {
            JSONObject jSONObject = new JSONObject();
            JSONArray jSONArray = new JSONArray();
            int i2 = 0;
            while (i2 < i) {
                try {
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("text", strArr[i2]);
                    jSONObject2.put(CloudInterpreter.KEY_SCORE, (double) fArr[i2]);
                    jSONArray.put(jSONObject2);
                    i2++;
                } catch (JSONException e2) {
                    Log.e(InputPluginService.f526b, "Failed to set parameter for " + str);
                    return;
                }
            }
            jSONObject.put(str, jSONArray);
            a(jSONObject.toString());
        }
    }

    public boolean a(String str) {
        if (!a()) {
            return false;
        }
        this.f549d.a(str);
        return true;
    }

    public boolean a(String str, JSONObject jSONObject) {
        boolean z = false;
        if (!a() || jSONObject == null) {
            return z;
        }
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject2.put(str, jSONObject);
            return a(jSONObject2.toString());
        } catch (JSONException e2) {
            Log.e(InputPluginService.f526b, "Failed to set parameter for " + str);
            return z;
        }
    }
}
