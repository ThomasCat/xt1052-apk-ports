package com.motorola.audiomonitor.comsocket;

import android.media.AudioRecord;
import android.util.Log;
import com.motorola.audiomonitor.ProtectedByteBuffer;
import java.util.LinkedList;
import java.util.List;

public class a extends Thread {

    /* renamed from: a reason: collision with root package name */
    private static final String f434a = ("AMonitor." + a.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f435b = com.motorola.audiomonitor.a.f160b;

    /* renamed from: c reason: collision with root package name */
    private final List f436c = new LinkedList();

    /* renamed from: d reason: collision with root package name */
    private boolean f437d = true;

    /* renamed from: e reason: collision with root package name */
    private final Object f438e = new Object();

    /* renamed from: f reason: collision with root package name */
    private final Object f439f = new Object();

    /* renamed from: g reason: collision with root package name */
    private f f440g;

    /* renamed from: h reason: collision with root package name */
    private boolean f441h = false;
    private boolean i = false;
    private final int j = 16000;
    private final int k = 8000;
    private final DownSampler l = new DownSampler(16000, 8000);
    private final ProtectedByteBuffer m;

    public a() {
        super("Feeder");
        int minBufferSize = AudioRecord.getMinBufferSize(16000, 16, 2);
        if (minBufferSize < 0) {
            Log.e(f434a, "getMinBufferSize failed");
            throw new IllegalStateException("getMinBufferSize failed");
        } else {
            this.m = new ProtectedByteBuffer(minBufferSize * 2, 8000);
        }
    }

    public f a() {
        f fVar;
        synchronized (this.f439f) {
            fVar = this.f440g;
        }
        return fVar;
    }

    public void a(ProtectedByteBuffer protectedByteBuffer) {
        synchronized (this.f438e) {
            if (!this.f441h) {
                this.f436c.add(protectedByteBuffer);
                this.f438e.notify();
            }
        }
    }

    public void a(f fVar) {
        f fVar2;
        synchronized (this.f439f) {
            fVar2 = this.f440g;
            this.f440g = fVar;
        }
        if (fVar2 != null && fVar == null) {
            synchronized (this.f438e) {
                if (!this.i) {
                    if (f435b) {
                        Log.d(f434a, "set to ignore");
                    }
                    this.f441h = true;
                }
            }
        }
        if (fVar != null) {
            synchronized (this.f438e) {
                this.f438e.notify();
            }
        }
    }

    public void b() {
        synchronized (this.f438e) {
            this.f436c.clear();
            this.f441h = false;
            this.i = false;
        }
    }

    public void c() {
        this.f437d = false;
        interrupt();
    }

    public f d() {
        f fVar;
        synchronized (this.f439f) {
            fVar = this.f440g;
        }
        if (fVar != null) {
            this.i = true;
        }
        if (f435b) {
            Log.d(f434a, "Abort on Feeder. " + fVar);
        }
        return fVar;
    }

    /* JADX INFO: used method not loaded: com.motorola.audiomonitor.comsocket.f.a(com.motorola.audiomonitor.ProtectedByteBuffer):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0024, code lost:
        if (r7.f436c.size() == 0) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0083, code lost:
        if (r7.f440g.a(r7.m, 0, r7.l.a(r0, 0, r0.capacity(), r7.m)) != false) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0090, code lost:
        if (r7.f440g.a(r0) == false) goto L_0x004b;
     */
    public void run() {
        boolean z;
        if (f435b) {
            Log.d(f434a, "Feeder running");
        }
        while (this.f437d) {
            synchronized (this.f438e) {
                synchronized (this.f439f) {
                    z = this.f440g == null;
                }
                if (!z) {
                }
                try {
                    this.f438e.wait();
                } catch (InterruptedException e2) {
                    this.f437d = false;
                }
            }
            synchronized (this.f439f) {
                if (this.f440g != null) {
                    while (true) {
                        ProtectedByteBuffer protectedByteBuffer = null;
                        synchronized (this.f438e) {
                            if (this.f436c.size() > 0) {
                                protectedByteBuffer = (ProtectedByteBuffer) this.f436c.remove(0);
                            }
                        }
                        if (protectedByteBuffer != null) {
                            if (this.l == null || protectedByteBuffer.getAffinityData() == 8000) {
                            }
                            if (f435b) {
                            }
                        }
                    }
                    while (true) {
                    }
                }
            }
        }
        b();
        if (this.l != null) {
            this.l.a();
        }
    }
}
