package com.motorola.audiomonitor.uis.training;

import android.animation.TimeAnimator;
import android.animation.TimeAnimator.TimeListener;
import com.motorola.audiomonitor.C0014R;

class d implements TimeListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ TrainingTriggerRecordingActivity f741a;

    d(TrainingTriggerRecordingActivity trainingTriggerRecordingActivity) {
        this.f741a = trainingTriggerRecordingActivity;
    }

    public void onTimeUpdate(TimeAnimator timeAnimator, long j, long j2) {
        int i = 0;
        while (true) {
            if (i < TrainingTriggerRecordingActivity.x.length) {
                if (this.f741a.K > TrainingTriggerRecordingActivity.x[i] && this.f741a.I < TrainingTriggerRecordingActivity.z[i]) {
                    this.f741a.I = TrainingTriggerRecordingActivity.z[i];
                    this.f741a.J = TrainingTriggerRecordingActivity.y[i];
                    break;
                }
                i++;
            } else {
                break;
            }
        }
        if (this.f741a.K > 800) {
            this.f741a.C.setImageResource(C0014R.drawable.vc_micbtn_rec);
        }
        int d2 = this.f741a.H + this.f741a.J;
        int d3 = this.f741a.H + this.f741a.I;
        if (!(this.f741a.M == null || this.f741a.E == null || this.f741a.M.height == d2)) {
            this.f741a.M.height = d2;
            this.f741a.M.width = d2;
            this.f741a.E.setLayoutParams(this.f741a.M);
            this.f741a.E.invalidate();
        }
        if (!(this.f741a.L == null || this.f741a.D == null || this.f741a.L.height == d3)) {
            this.f741a.L.height = d3;
            this.f741a.L.width = d3;
            this.f741a.D.setLayoutParams(this.f741a.L);
            this.f741a.D.invalidate();
        }
        TrainingTriggerRecordingActivity.c(this.f741a, 1);
        TrainingTriggerRecordingActivity.d(this.f741a, 3);
        if (this.f741a.J < 0) {
            this.f741a.J = 0;
        }
        if (this.f741a.I < 0) {
            this.f741a.I = 0;
        }
        this.f741a.K = 0;
    }
}
