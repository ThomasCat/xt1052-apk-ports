package h;

import android.text.TextUtils;
import java.io.File;
import java.util.List;

class r {

    /* renamed from: a reason: collision with root package name */
    public final q f2330a;

    /* renamed from: b reason: collision with root package name */
    public final String f2331b;

    /* renamed from: c reason: collision with root package name */
    public final String f2332c;

    /* renamed from: d reason: collision with root package name */
    public final List f2333d;

    /* renamed from: e reason: collision with root package name */
    public final int f2334e;

    /* renamed from: f reason: collision with root package name */
    public final String f2335f;

    /* renamed from: g reason: collision with root package name */
    final /* synthetic */ o f2336g;

    public r(o oVar, q qVar, String str, List list, int i, String str2) {
        this.f2336g = oVar;
        this.f2330a = qVar;
        oVar.f2318c.setTime(System.currentTimeMillis());
        this.f2331b = oVar.f2319d.format(oVar.f2318c);
        this.f2332c = str;
        this.f2333d = list;
        this.f2334e = i;
        StringBuilder sb = new StringBuilder();
        sb.append(this.f2332c);
        sb.append(File.separator);
        sb.append(this.f2331b);
        sb.append("_");
        sb.append(this.f2330a.a());
        if (!TextUtils.isEmpty(str2)) {
            sb.append("_");
            sb.append(str2);
        }
        sb.append(".wav");
        this.f2335f = sb.toString();
    }
}
