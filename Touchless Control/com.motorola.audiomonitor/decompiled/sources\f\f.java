package f;

import android.util.Log;
import com.motorola.audiomonitor.ProtectedByteBuffer;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.k;
import com.nuance.dragon.toolkit.recognition.CloudInterpreter;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class f implements k {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f2228a = ("AMonitor." + f.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f2229b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private final i f2230c;

    /* renamed from: d reason: collision with root package name */
    private final t f2231d = new g(this);
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public final h f2232e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public final Object f2233f = new Object();
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public boolean f2234g = false;

    public f(int i, h hVar, List list, boolean z) {
        if (hVar == null) {
            throw new IllegalArgumentException("callback is null");
        }
        this.f2232e = hVar;
        this.f2230c = new i(i, this.f2231d, w.a(list, z));
    }

    /* access modifiers changed from: private */
    public void a(int i, JSONObject jSONObject) {
        int i2;
        boolean z;
        String str;
        boolean z2;
        String str2 = null;
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject("nbest");
            int i3 = jSONObject.getInt("gate_confidence");
            JSONArray jSONArray = jSONObject2.getJSONArray("entries");
            int length = jSONArray.length();
            if (f2229b) {
                Log.d(f2228a, "nbest = " + length + " gate_confidence = " + i3 + " confidence = " + i);
            }
            for (int i4 = 0; i4 < length; i4++) {
                JSONObject jSONObject3 = jSONArray.getJSONObject(i4);
                String string = jSONObject3.getString("constraint");
                int i5 = jSONObject3.getInt(CloudInterpreter.KEY_SCORE);
                JSONArray jSONArray2 = jSONObject3.getJSONArray("words");
                int length2 = jSONArray2.length();
                if ("spotCommand".equals(string)) {
                    boolean z3 = false;
                    String str3 = null;
                    int i6 = 0;
                    while (i6 < length2) {
                        JSONObject jSONObject4 = jSONArray2.getJSONObject(i6);
                        if ("spotCommands".equals(jSONObject4.get("slot"))) {
                            boolean z4 = z3;
                            str = jSONObject4.getString("phrase");
                            z2 = z4;
                        } else if ("garbage".equals(jSONObject4.get("slot"))) {
                            str = str3;
                            z2 = true;
                        } else {
                            boolean z5 = z3;
                            str = str3;
                            z2 = z5;
                        }
                        i6++;
                        boolean z6 = z2;
                        str3 = str;
                        z3 = z6;
                    }
                    if (f2229b) {
                        Log.d(f2228a, "[" + i5 + "] " + str3 + (z3 ? " + extra" : ""));
                    }
                    if (i4 != 0 || z3) {
                        str3 = str2;
                    }
                    str2 = str3;
                } else if ("garbageConst".equals(string) && f2229b) {
                    Log.d(f2228a, "[" + i5 + "] garbage");
                }
            }
            i2 = i3;
        } catch (JSONException e2) {
            i2 = 0;
        }
        synchronized (this.f2233f) {
            z = this.f2234g;
            this.f2234g = false;
            this.f2233f.notify();
        }
        if (z) {
            this.f2232e.a(str2, i2, i);
        }
    }

    public void a() {
        synchronized (this.f2233f) {
            if (!this.f2234g) {
                this.f2234g = true;
                this.f2230c.b();
            }
        }
    }

    public boolean a(ProtectedByteBuffer protectedByteBuffer) {
        return this.f2230c.a(protectedByteBuffer);
    }

    public void b() {
        synchronized (this.f2233f) {
            this.f2230c.c();
            if (this.f2234g) {
                this.f2234g = false;
                try {
                    this.f2233f.wait(3000);
                } catch (InterruptedException e2) {
                }
            }
        }
    }

    public void c() {
        this.f2230c.d();
        if (f2229b) {
            Log.d(f2228a, "cleaned up");
        }
    }
}
