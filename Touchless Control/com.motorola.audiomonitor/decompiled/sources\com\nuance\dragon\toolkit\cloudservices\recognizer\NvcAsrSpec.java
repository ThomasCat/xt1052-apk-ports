package com.nuance.dragon.toolkit.cloudservices.recognizer;

import com.nuance.dragon.toolkit.cloudservices.DictionaryParam;
import com.nuance.dragon.toolkit.data.Data;
import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.data.Data.Sequence;
import com.nuance.dragon.toolkit.grammar.WordList;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.a.b;
import com.nuance.dragon.toolkit.util.internal.d;
import com.nuance.dragon.toolkit.util.internal.g;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

public class NvcAsrSpec implements JSONCompliant {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final String f1486a;

    /* renamed from: b reason: collision with root package name */
    private final String f1487b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public String f1488c;

    /* renamed from: d reason: collision with root package name */
    private Dictionary f1489d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public String f1490e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public final HashMap f1491f = new HashMap();
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public String f1492g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public String f1493h;
    /* access modifiers changed from: private */
    public final HashSet i = new HashSet();

    public NvcAsrSpec(String str, String str2) {
        this.f1486a = str;
        this.f1487b = str2;
    }

    public static NvcAsrSpec createFromJSON(JSONObject jSONObject) {
        String string = jSONObject.getString("grammarid");
        String string2 = jSONObject.getString("language");
        String string3 = jSONObject.getString("server");
        String string4 = jSONObject.getString("recoggrammar");
        String string5 = jSONObject.getString("interpgrammar");
        NvcAsrSpec nvcAsrSpec = new NvcAsrSpec(string, string2);
        nvcAsrSpec.setServer(string3);
        nvcAsrSpec.setGrammarFiles(string4, string5);
        if (!jSONObject.isNull("contactlistid")) {
            String string6 = jSONObject.getString("contactlistid");
            if (string6 != null && !string6.equals("")) {
                nvcAsrSpec.setContactListId(string6);
            }
        }
        if (!jSONObject.isNull("reqcustwordlistids")) {
            JSONArray jSONArray = jSONObject.getJSONArray("reqcustwordlistids");
            if (jSONArray.length() > 0) {
                HashSet hashSet = new HashSet(jSONArray.length());
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    hashSet.add(jSONArray.getString(i2));
                }
                nvcAsrSpec.addRequiredCustomWordListIds(hashSet);
            }
        }
        return nvcAsrSpec;
    }

    public void addRequiredCustomWordListIds(Set set) {
        d.a("wordListIds", (Object) set);
        d.a("wordListIds", "not empty", !set.isEmpty());
        this.i.addAll(set);
        if (this.f1490e != null) {
            this.i.remove(this.f1490e);
        }
    }

    public void attachSettings(Dictionary dictionary) {
        this.f1489d = dictionary;
    }

    public boolean attachWordList(WordList wordList, String str) {
        d.a("wordList", (Object) wordList);
        d.a("id", str);
        if (!this.i.contains(str) && (this.f1490e == null || !this.f1490e.equals(str))) {
            return false;
        }
        this.f1491f.put(str, wordList);
        return true;
    }

    public RecogSpec createRecogSpec() {
        if (this.f1489d == null) {
            this.f1489d = new Dictionary();
        }
        this.f1489d.put("dictation_language", this.f1487b);
        this.f1489d.put("dictation_type", "OneShot");
        return new RecogSpec("NVC_ASR_CMD", this.f1489d, "AUDIO_INFO") {
            public final List getDelayedParams() {
                String str;
                String str2 = "";
                Sequence sequence = new Sequence();
                if (NvcAsrSpec.this.f1490e != null) {
                    WordList wordList = (WordList) NvcAsrSpec.this.f1491f.get(NvcAsrSpec.this.f1490e);
                    if (wordList == null) {
                        Logger.error(this, "Missing required contact list: " + NvcAsrSpec.this.f1490e);
                        return null;
                    }
                    String str3 = "contacts";
                    String str4 = wordList.getAcceptedChecksum();
                    str = str2 + "?SWI_import.contacts=nvc_builtin:{\"id\":\"" + str3 + "\",\"type\":\"contacts\",\"checksum\":\"" + str4 + "\"}";
                    Dictionary dictionary = new Dictionary();
                    dictionary.put("type", "contacts");
                    dictionary.put("id", str3);
                    dictionary.put("checksum", str4);
                    sequence.add((Data) dictionary);
                } else {
                    str = str2;
                }
                Iterator it = NvcAsrSpec.this.i.iterator();
                String str5 = str;
                while (it.hasNext()) {
                    String str6 = (String) it.next();
                    WordList wordList2 = (WordList) NvcAsrSpec.this.f1491f.get(str6);
                    if (wordList2 == null) {
                        Logger.error(this, "Missing required custom word list: " + str6);
                        return null;
                    }
                    String str7 = wordList2.getAcceptedChecksum();
                    String str8 = (str5.equals("") ? str5 + "?" : str5 + "&") + "SWI_import." + str6 + "=nvc_builtin:{\"id\":\"" + str6 + "\",\"type\":\"custom_words\",\"checksum\":\"" + str7 + "\"}";
                    Dictionary dictionary2 = new Dictionary();
                    dictionary2.put("type", "custom_words");
                    dictionary2.put("id", str6);
                    dictionary2.put("checksum", str7);
                    sequence.add((Data) dictionary2);
                    str5 = str8;
                }
                Dictionary dictionary3 = new Dictionary();
                dictionary3.put("type", "uri");
                dictionary3.put("id", NvcAsrSpec.this.f1486a);
                dictionary3.put("uri", NvcAsrSpec.this.f1488c + NvcAsrSpec.this.f1492g + str5);
                if (NvcAsrSpec.this.f1493h != null) {
                    dictionary3.put("interpretation_uri", NvcAsrSpec.this.f1488c + NvcAsrSpec.this.f1493h + str5);
                }
                sequence.add((Data) dictionary3);
                Dictionary dictionary4 = new Dictionary();
                dictionary4.put("start", 0);
                dictionary4.put("end", 0);
                dictionary4.put("text", "");
                dictionary4.put("grammar_list", (Data) sequence);
                return new g((Object) new DictionaryParam("REQUEST_INFO", dictionary4));
            }
        };
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof NvcAsrSpec)) {
            return false;
        }
        NvcAsrSpec nvcAsrSpec = (NvcAsrSpec) obj;
        if (this.f1490e == null) {
            if (nvcAsrSpec.f1490e != null) {
                return false;
            }
        } else if (!this.f1490e.equals(nvcAsrSpec.f1490e)) {
            return false;
        }
        if (this.f1486a == null) {
            if (nvcAsrSpec.f1486a != null) {
                return false;
            }
        } else if (!this.f1486a.equals(nvcAsrSpec.f1486a)) {
            return false;
        }
        if (this.f1493h == null) {
            if (nvcAsrSpec.f1493h != null) {
                return false;
            }
        } else if (!this.f1493h.equals(nvcAsrSpec.f1493h)) {
            return false;
        }
        if (this.f1487b == null) {
            if (nvcAsrSpec.f1487b != null) {
                return false;
            }
        } else if (!this.f1487b.equals(nvcAsrSpec.f1487b)) {
            return false;
        }
        if (this.f1492g == null) {
            if (nvcAsrSpec.f1492g != null) {
                return false;
            }
        } else if (!this.f1492g.equals(nvcAsrSpec.f1492g)) {
            return false;
        }
        if (this.i == null) {
            if (nvcAsrSpec.i != null) {
                return false;
            }
        } else if (!this.i.equals(nvcAsrSpec.i)) {
            return false;
        }
        if (this.f1488c == null) {
            if (nvcAsrSpec.f1488c != null) {
                return false;
            }
        } else if (!this.f1488c.equals(nvcAsrSpec.f1488c)) {
            return false;
        }
        if (this.f1489d == null) {
            if (nvcAsrSpec.f1489d != null) {
                return false;
            }
        } else if (!this.f1489d.equals(nvcAsrSpec.f1489d)) {
            return false;
        }
        return this.f1491f == null ? nvcAsrSpec.f1491f == null : this.f1491f.equals(nvcAsrSpec.f1491f);
    }

    public String getContactListId() {
        return this.f1490e;
    }

    public Set getRequiredCustomWordListIds() {
        HashSet hashSet = new HashSet(this.i.size());
        hashSet.addAll(this.i);
        return hashSet;
    }

    public final int hashCode() {
        int i2 = 0;
        int hashCode = ((this.f1489d == null ? 0 : this.f1489d.hashCode()) + (((this.f1488c == null ? 0 : this.f1488c.hashCode()) + (((this.i == null ? 0 : this.i.hashCode()) + (((this.f1492g == null ? 0 : this.f1492g.hashCode()) + (((this.f1487b == null ? 0 : this.f1487b.hashCode()) + (((this.f1493h == null ? 0 : this.f1493h.hashCode()) + (((this.f1486a == null ? 0 : this.f1486a.hashCode()) + (((this.f1490e == null ? 0 : this.f1490e.hashCode()) + 31) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.f1491f != null) {
            i2 = this.f1491f.hashCode();
        }
        return hashCode + i2;
    }

    public void setContactListId(String str) {
        d.a("contactListId", str);
        if (this.f1490e == null) {
            this.f1490e = str;
            this.i.remove(this.f1490e);
        }
    }

    public void setGrammarFiles(String str, String str2) {
        this.f1492g = str;
        this.f1493h = str2;
    }

    public void setServer(String str) {
        d.a("server", str);
        this.f1488c = str;
    }

    public JSONObject toJSON() {
        b bVar = new b();
        bVar.a("grammarid", (Object) this.f1486a);
        bVar.a("language", (Object) this.f1487b);
        bVar.a("server", (Object) this.f1488c);
        bVar.a("recoggrammar", (Object) this.f1492g);
        bVar.a("interpgrammar", (Object) this.f1493h);
        if (this.f1490e != null) {
            bVar.a("contactlistid", (Object) this.f1490e);
        }
        if (!this.i.isEmpty()) {
            ArrayList arrayList = new ArrayList(this.i.size());
            arrayList.addAll(this.i);
            bVar.b("reqcustwordlistids", arrayList);
        }
        return bVar;
    }
}
