package com.motorola.audiomonitor.actions.utils;

public class a {

    /* renamed from: a reason: collision with root package name */
    private final b f168a;

    /* renamed from: b reason: collision with root package name */
    private final String f169b;

    /* renamed from: c reason: collision with root package name */
    private final boolean f170c;

    /* renamed from: d reason: collision with root package name */
    private final float f171d;

    /* renamed from: e reason: collision with root package name */
    private final float f172e;

    public a(b bVar, String str, int i, String str2, boolean z, float f2, float f3) {
        if (bVar == null) {
            throw new IllegalArgumentException("InternalContact is null");
        }
        this.f168a = new b(bVar.a(), str, i, bVar.c());
        this.f169b = str2;
        this.f170c = z;
        this.f171d = f2;
        this.f172e = f3;
    }

    public a(String str, int i, String str2, int i2, String str3, boolean z, float f2, float f3) {
        this.f168a = new b(str, str2, i2, (float) i);
        this.f169b = str3;
        this.f170c = z;
        this.f171d = f2;
        this.f172e = f3;
    }

    public b a() {
        return this.f168a;
    }

    public String b() {
        return this.f168a.f219a;
    }

    public String c() {
        return this.f168a.b();
    }

    public float d() {
        return this.f168a.c();
    }

    public int e() {
        return this.f168a.d();
    }

    public boolean f() {
        return this.f170c;
    }

    public String g() {
        return this.f169b;
    }

    public float h() {
        return this.f171d;
    }

    public float i() {
        return this.f172e;
    }

    public String toString() {
        return this.f168a.toString();
    }
}
