package com.nuance.dragon.toolkit.cloudservices.datauploader;

import android.util.Pair;
import com.nuance.dragon.toolkit.cloudservices.CloudServices;
import com.nuance.dragon.toolkit.cloudservices.DictionaryParam;
import com.nuance.dragon.toolkit.cloudservices.SeqChunkParam;
import com.nuance.dragon.toolkit.cloudservices.SeqEndParam;
import com.nuance.dragon.toolkit.cloudservices.SeqStartParam;
import com.nuance.dragon.toolkit.cloudservices.Transaction;
import com.nuance.dragon.toolkit.cloudservices.Transaction.Listener;
import com.nuance.dragon.toolkit.cloudservices.TransactionError;
import com.nuance.dragon.toolkit.cloudservices.TransactionResult;
import com.nuance.dragon.toolkit.data.Data;
import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.data.Data.Sequence;
import com.nuance.dragon.toolkit.grammar.Word;
import com.nuance.dragon.toolkit.grammar.WordAction;
import com.nuance.dragon.toolkit.grammar.WordList;
import com.nuance.dragon.toolkit.grammar.WordList.WordIterator;
import com.nuance.dragon.toolkit.util.internal.a.b;
import com.nuance.dragon.toolkit.util.internal.d;

public class CloudDataUploader {

    /* renamed from: a reason: collision with root package name */
    private final CloudServices f1409a;

    /* renamed from: b reason: collision with root package name */
    private final String f1410b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final com.nuance.dragon.toolkit.util.internal.a f1411c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public a f1412d;

    public interface DeleteListener {
        void onError(CloudDataUploader cloudDataUploader, TransactionError transactionError);

        void onSuccess(CloudDataUploader cloudDataUploader, TransactionResult transactionResult);
    }

    public interface UploadListener {
        void onError(CloudDataUploader cloudDataUploader, TransactionError transactionError, String str);

        void onSuccess(CloudDataUploader cloudDataUploader, TransactionResult transactionResult, String str, boolean z);
    }

    final class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public Transaction f1436a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public WordIterator f1437b;
        /* access modifiers changed from: private */

        /* renamed from: c reason: collision with root package name */
        public boolean f1438c;
        /* access modifiers changed from: private */

        /* renamed from: d reason: collision with root package name */
        public boolean f1439d;
        /* access modifiers changed from: private */

        /* renamed from: e reason: collision with root package name */
        public boolean f1440e;
        /* access modifiers changed from: private */

        /* renamed from: f reason: collision with root package name */
        public int f1441f;

        private a() {
        }

        /* synthetic */ a(byte b2) {
            this();
        }
    }

    public CloudDataUploader(CloudServices cloudServices) {
        this(cloudServices, "NVC_DATA_UPLOAD_CMD");
    }

    public CloudDataUploader(CloudServices cloudServices, String str) {
        d.a("cloudServices", (Object) cloudServices);
        this.f1409a = cloudServices;
        this.f1411c = new com.nuance.dragon.toolkit.util.internal.a();
        this.f1410b = str;
    }

    private static Dictionary a(Sequence sequence, String str) {
        Dictionary dictionary = new Dictionary();
        dictionary.put("list", (Data) sequence);
        Dictionary dictionary2 = new Dictionary();
        dictionary2.put("content", (Data) dictionary);
        dictionary2.put("action", str);
        return dictionary2;
    }

    static /* synthetic */ Dictionary a(WordIterator wordIterator, String str, String str2, WordList wordList, int i) {
        Dictionary dictionary = new Dictionary();
        dictionary.put("num_data_blocks", i);
        Sequence sequence = new Sequence();
        Dictionary dictionary2 = new Dictionary();
        dictionary2.put("id", str);
        dictionary2.put("type", str2);
        dictionary2.put("current_checksum", wordList.getAcceptedChecksum());
        dictionary2.put("new_checksum", wordIterator.getChecksum());
        dictionary2.put("algorithm_id", "MD5");
        sequence.add((Data) dictionary2);
        dictionary.put("checksums", (Data) sequence);
        return dictionary;
    }

    static /* synthetic */ Dictionary a(String str, String str2, Sequence sequence) {
        Dictionary dictionary = new Dictionary();
        Sequence sequence2 = new Sequence();
        Dictionary dictionary2 = new Dictionary();
        dictionary2.put("id", str);
        dictionary2.put("type", str2);
        dictionary2.put("actions", (Data) sequence);
        sequence2.add((Data) dictionary2);
        dictionary.put("data_list", (Data) sequence2);
        return dictionary;
    }

    static /* synthetic */ Sequence a(WordIterator wordIterator, boolean z, String str, int i) {
        if (i == 0) {
            i = Integer.MAX_VALUE;
        }
        Sequence sequence = new Sequence();
        if (z) {
            Dictionary dictionary = new Dictionary();
            dictionary.put("action", "clear_all");
            sequence.add((Data) dictionary);
        }
        boolean z2 = true;
        Sequence sequence2 = new Sequence();
        while (wordIterator.hasNext()) {
            int i2 = r10 - 1;
            if (r10 <= 0) {
                break;
            }
            WordAction wordAction = (WordAction) wordIterator.next();
            if (z2 != wordAction.isAdded() && sequence2.size() > 0) {
                sequence.add((Data) a(sequence2, z2 ? "add" : "remove"));
                sequence2 = new Sequence();
            }
            Word word = wordAction.getWord();
            boolean isAdded = wordAction.isAdded();
            if (str.equals("contacts")) {
                Dictionary customForm = word.getCustomForm();
                if (customForm == null) {
                    customForm = new Dictionary();
                    customForm.put("full_name", word.getSurfaceForm());
                }
                sequence2.add((Data) customForm);
                r10 = i2;
                z2 = isAdded;
            } else {
                sequence2.add(word.getSurfaceForm());
                r10 = i2;
                z2 = isAdded;
            }
        }
        if (sequence2.size() > 0) {
            sequence.add((Data) a(sequence2, z2 ? "add" : "remove"));
        }
        return sequence;
    }

    public void cancel() {
        if (this.f1412d != null) {
            if (this.f1412d.f1436a != null) {
                this.f1412d.f1436a.cancel();
            }
            this.f1412d = null;
        }
    }

    public void deleteAll(final DeleteListener deleteListener) {
        cancel();
        final a aVar = new a(0);
        this.f1412d = aVar;
        Transaction transaction = new Transaction(this.f1410b, null, new Listener() {
            public final void onTransactionError(Transaction transaction, TransactionError transactionError) {
                if (aVar == CloudDataUploader.this.f1412d) {
                    CloudDataUploader.this.f1412d = null;
                    deleteListener.onError(CloudDataUploader.this, transactionError);
                }
            }

            public final void onTransactionResult(Transaction transaction, TransactionResult transactionResult, boolean z) {
                if (aVar == CloudDataUploader.this.f1412d) {
                    CloudDataUploader.this.f1412d = null;
                    deleteListener.onSuccess(CloudDataUploader.this, transactionResult);
                }
            }

            public final void onTransactionStarted(Transaction transaction) {
            }
        }, 40000);
        aVar.f1436a = transaction;
        Dictionary dictionary = new Dictionary();
        dictionary.put("delete_all", 1);
        dictionary.put("data_list", (Data) new Sequence());
        transaction.addParam(new DictionaryParam("DATA_BLOCK", dictionary));
        Dictionary dictionary2 = new Dictionary();
        dictionary2.put("num_data_blocks", 1);
        dictionary2.put("checksums", (Data) new Sequence());
        transaction.addParam(new DictionaryParam("UPLOAD_DONE", dictionary2));
        transaction.finish();
        this.f1409a.addTransaction(transaction, 0);
    }

    public void uploadData(String str, String str2, WordList wordList, int i, boolean z, int i2, UploadListener uploadListener) {
        d.a("grammarId", str);
        d.a("type", str2);
        d.a("list", (Object) wordList);
        d.a("listener", (Object) uploadListener);
        d.a("maxItemsToUpload", "greater than or equal to 0", i >= 0);
        cancel();
        final a aVar = new a(0);
        this.f1412d = aVar;
        final String str3 = str;
        final String str4 = str2;
        final WordList wordList2 = wordList;
        final int i3 = i;
        final UploadListener uploadListener2 = uploadListener;
        Transaction transaction = new Transaction(this.f1410b, null, new Listener() {
            public final void onTransactionError(Transaction transaction, TransactionError transactionError) {
                if (aVar.f1439d) {
                    CloudDataUploader.this.f1412d = null;
                    uploadListener2.onSuccess(CloudDataUploader.this, null, str3, true);
                    return;
                }
                if (aVar.f1437b != null) {
                    aVar.f1437b.discardChanges();
                }
                if (aVar == CloudDataUploader.this.f1412d) {
                    CloudDataUploader.this.f1412d = null;
                }
                uploadListener2.onError(CloudDataUploader.this, transactionError, str3);
            }

            public final void onTransactionResult(Transaction transaction, TransactionResult transactionResult, boolean z) {
                if (aVar == CloudDataUploader.this.f1412d) {
                    if (aVar.f1437b != null) {
                        aVar.f1437b.acceptChanges();
                    }
                    CloudDataUploader.this.f1412d = null;
                    uploadListener2.onSuccess(CloudDataUploader.this, transactionResult, str3, false);
                }
            }

            public final void onTransactionStarted(final Transaction transaction) {
                if (aVar == CloudDataUploader.this.f1412d) {
                    final WordIterator a2 = aVar.f1437b;
                    final boolean b2 = aVar.f1438c;
                    final AnonymousClass1 r5 = new b() {
                        public final /* synthetic */ Object a() {
                            return new DictionaryParam("UPLOAD_DONE", CloudDataUploader.a(a2, str3, str4, wordList2, aVar.f1441f));
                        }

                        public final /* synthetic */ void a(Object obj) {
                            DictionaryParam dictionaryParam = (DictionaryParam) obj;
                            if (CloudDataUploader.this.f1412d == aVar) {
                                transaction.addParam(dictionaryParam);
                                transaction.finish();
                            }
                        }
                    };
                    final Transaction transaction2 = transaction;
                    CloudDataUploader.this.f1411c.a((b) new b() {
                        public final /* synthetic */ Object a() {
                            if (CloudDataUploader.this.f1412d != aVar) {
                                return null;
                            }
                            Dictionary a2 = CloudDataUploader.a(str3, str4, CloudDataUploader.a(a2, b2 && !aVar.f1440e, str4, i3));
                            boolean z = aVar.f1437b != null ? aVar.f1437b.hasNext() : false;
                            if (aVar.f1440e) {
                                if (z) {
                                    return new SeqChunkParam("DATA_BLOCK", a2);
                                }
                                SeqEndParam seqEndParam = new SeqEndParam("DATA_BLOCK", a2);
                                aVar.f1440e = false;
                                return seqEndParam;
                            } else if (!z) {
                                return new DictionaryParam("DATA_BLOCK", a2);
                            } else {
                                SeqStartParam seqStartParam = new SeqStartParam("DATA_BLOCK", a2);
                                aVar.f1440e = true;
                                return seqStartParam;
                            }
                        }

                        public final /* synthetic */ void a(Object obj) {
                            DictionaryParam dictionaryParam = (DictionaryParam) obj;
                            if (CloudDataUploader.this.f1412d == aVar) {
                                aVar.f1441f = aVar.f1441f + 1;
                                transaction2.addParam(dictionaryParam);
                                if (aVar.f1440e) {
                                    CloudDataUploader.this.f1411c.a((b) this);
                                } else {
                                    CloudDataUploader.this.f1411c.a(r5);
                                }
                            }
                        }
                    });
                }
            }
        }, 40000, false);
        this.f1412d.f1436a = transaction;
        this.f1409a.addTransaction(transaction, i2);
        final boolean z2 = z;
        final WordList wordList3 = wordList;
        final a aVar2 = aVar;
        final Transaction transaction2 = transaction;
        this.f1411c.a((b) new b() {
            public final /* synthetic */ Object a() {
                boolean z = z2 || wordList3.fullUpdateRequired();
                WordIterator modifiedIterator = z ? wordList3.getFullIterator() : wordList3.getModifiedIterator();
                if (z || modifiedIterator.hasNext()) {
                    return new Pair(modifiedIterator, Boolean.valueOf(z));
                }
                modifiedIterator.discardChanges();
                return null;
            }

            public final /* synthetic */ void a(Object obj) {
                Pair pair = (Pair) obj;
                if (aVar2 != CloudDataUploader.this.f1412d) {
                    return;
                }
                if (pair == null) {
                    CloudDataUploader.this.f1412d.f1439d = true;
                    transaction2.cancel();
                    return;
                }
                CloudDataUploader.this.f1412d.f1437b = (WordIterator) pair.first;
                CloudDataUploader.this.f1412d.f1438c = ((Boolean) pair.second).booleanValue();
                CloudDataUploader.this.f1412d.f1436a.enable();
            }
        });
    }
}
