package com.nuance.dragon.toolkit.recognition.dictation.a;

import com.nuance.dragon.toolkit.recognition.dictation.Alternative;
import com.nuance.dragon.toolkit.recognition.dictation.Alternatives;
import java.util.ArrayList;

final class d implements Alternatives {

    /* renamed from: a reason: collision with root package name */
    private ArrayList f1929a;

    public d(ArrayList arrayList) {
        this.f1929a = arrayList;
    }

    public final Alternative getAlternativeAt(int i) {
        return (Alternative) this.f1929a.get(i);
    }

    public final int size() {
        return this.f1929a.size();
    }
}
