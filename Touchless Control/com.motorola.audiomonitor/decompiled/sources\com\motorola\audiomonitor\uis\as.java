package com.motorola.audiomonitor.uis;

import a.e;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import com.motorola.audiomonitor.ProtectedByteBuffer;
import com.motorola.audiomonitor.k;

public class as extends BroadcastReceiver implements k {

    /* renamed from: h reason: collision with root package name */
    private static int f668h = 0;
    private static as i = null;
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final Context f669a;

    /* renamed from: b reason: collision with root package name */
    private final Handler f670b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public bc f671c;

    /* renamed from: d reason: collision with root package name */
    private ba f672d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public final bb f673e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public final Intent f674f = new Intent(this.f669a, AOVActivity.class);
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public boolean f675g;
    /* access modifiers changed from: private */
    public az j;
    /* access modifiers changed from: private */
    public ag k;

    public as(Context context, Handler handler, bb bbVar) {
        this.f670b = handler;
        this.f669a = context;
        this.f673e = bbVar;
        this.f674f.addFlags(268435456);
        this.f674f.addFlags(65536);
    }

    public static synchronized int a(bc bcVar) {
        int i2;
        synchronized (as.class) {
            if (i != null) {
                i.f671c = bcVar;
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("android.intent.action.SCREEN_OFF");
                intentFilter.addAction("com.motorola.audiomonitor.NOISE_ESTIMATE");
                i.f669a.registerReceiver(i, intentFilter);
                i2 = f668h + 1;
                f668h = i2;
            } else {
                i2 = -1;
            }
        }
        return i2;
    }

    public static synchronized as a() {
        as asVar;
        synchronized (as.class) {
            asVar = i != null ? i : null;
        }
        return asVar;
    }

    public static synchronized as a(Context context, Handler handler, bb bbVar) {
        as asVar;
        synchronized (as.class) {
            if (i == null) {
                i = new as(context, handler, bbVar);
            }
            asVar = i;
        }
        return asVar;
    }

    public static synchronized void a(bc bcVar, int i2) {
        synchronized (as.class) {
            if (i2 == f668h) {
                if (i != null && i.f671c == bcVar) {
                    i.f669a.unregisterReceiver(i);
                    i.f671c = null;
                }
            }
        }
    }

    public static synchronized void a(boolean z, int i2) {
        synchronized (as.class) {
            if (i2 == f668h) {
                if (!(i == null || i.f671c == null)) {
                    i.e();
                    if (i.f672d != null) {
                        i.f672d.b(z);
                        i.f672d = null;
                    }
                }
            }
        }
    }

    public static boolean a(ag agVar) {
        if (i == null) {
            return false;
        }
        i.k = agVar;
        return true;
    }

    public static boolean a(az azVar) {
        if (i == null) {
            return false;
        }
        i.j = azVar;
        return true;
    }

    public static synchronized boolean a(ba baVar) {
        boolean z;
        synchronized (as.class) {
            if (i != null) {
                i.f672d = baVar;
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public static synchronized void b(boolean z) {
        synchronized (as.class) {
            if (!(i == null || i.f671c == null)) {
                i.e();
                if (i.f672d != null) {
                    i.f672d.b(z);
                    i.f672d = null;
                }
            }
        }
    }

    public static synchronized void c(boolean z) {
        synchronized (as.class) {
            if (!(i == null || i.f671c == null || i.f672d == null)) {
                i.f672d.b(z);
                i.f672d = null;
            }
        }
    }

    private void e() {
        this.f670b.post(new ax(this));
    }

    public void a(int i2) {
        if (this.f671c != null) {
            this.f671c.c(i2);
        }
    }

    public void a(e eVar) {
        this.f670b.post(new ay(this, eVar));
    }

    public void a(boolean z) {
        this.f670b.post(new at(this, z));
    }

    public boolean a(ProtectedByteBuffer protectedByteBuffer) {
        this.f670b.post(new av(this, protectedByteBuffer));
        return true;
    }

    public synchronized void b() {
        this.f670b.post(new au(this));
    }

    public void c() {
        this.f670b.post(new aw(this));
    }

    public synchronized void d() {
        this.f670b.removeCallbacksAndMessages(null);
        if (this.j != null) {
            this.j.a();
            this.j = null;
        }
        if (this.k != null) {
            this.k.b();
            this.k = null;
        }
        if (i != null) {
            i.b();
            i = null;
        }
    }

    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
            a(false, f668h);
        } else if (this.f671c != null && !this.f675g) {
            Bundle extras = intent.getExtras();
            if (extras != null && extras.containsKey("NOISE_ESTIMATE")) {
                if (extras.getInt("NOISE_ESTIMATE") >= 2) {
                    this.f671c.b(1);
                } else {
                    this.f671c.b(2);
                }
            }
        }
    }
}
