package com.nuance.dragon.toolkit.audio;

import com.nuance.dragon.toolkit.audio.AbstractAudioChunk.SpeechStatus;
import com.nuance.dragon.toolkit.util.internal.d;

public class MultiChannelAudioChunk extends AbstractAudioChunk {
    public final int channelCount;
    public final int[] channelOffsetsMs;

    public MultiChannelAudioChunk(AudioType audioType, byte[] bArr, long j, int i, int i2, SpeechStatus speechStatus) {
        this(audioType, null, bArr, i, i2, new int[i2], speechStatus);
    }

    public MultiChannelAudioChunk(AudioType audioType, short[] sArr, int i, SpeechStatus speechStatus) {
        this(audioType, sArr, null, audioType.getDuration(sArr) / i, i, new int[i], speechStatus);
    }

    private MultiChannelAudioChunk(AudioType audioType, short[] sArr, byte[] bArr, int i, int i2, int[] iArr, SpeechStatus speechStatus) {
        super(audioType, sArr, bArr, System.currentTimeMillis(), i, speechStatus);
        this.channelCount = i2;
        this.channelOffsetsMs = iArr;
    }

    public MultiChannelAudioChunk(AudioChunk... audioChunkArr) {
        AudioType audioType = audioChunkArr[0].audioType;
        short[] c2 = c(audioChunkArr);
        byte[] b2 = b(audioChunkArr);
        long j = audioChunkArr[0].audioTimestamp;
        this(audioType, c2, b2, audioChunkArr[0].audioDuration, (byte) audioChunkArr.length, a(audioChunkArr), audioChunkArr[0].audioSpeechStatus);
    }

    private static int[] a(AudioChunk... audioChunkArr) {
        int[] iArr = new int[audioChunkArr.length];
        long j = audioChunkArr[0].audioTimestamp;
        iArr[0] = 0;
        for (int i = 1; i < audioChunkArr.length; i++) {
            iArr[i] = (int) (audioChunkArr[i].audioTimestamp - j);
        }
        return iArr;
    }

    private static byte[] b(AudioChunk... audioChunkArr) {
        int i = 0;
        for (AudioChunk audioChunk : audioChunkArr) {
            if (audioChunk.audioBytes == null) {
                return null;
            }
            i += audioChunk.audioBytes.length;
        }
        byte[] bArr = new byte[i];
        int i2 = 0;
        for (AudioChunk audioChunk2 : audioChunkArr) {
            System.arraycopy(audioChunk2.audioBytes, 0, bArr, i2, audioChunk2.audioBytes.length);
            i2 += audioChunk2.audioBytes.length;
        }
        return bArr;
    }

    private static short[] c(AudioChunk... audioChunkArr) {
        int i = 0;
        for (AudioChunk audioChunk : audioChunkArr) {
            if (audioChunk.audioShorts == null) {
                return null;
            }
            i += audioChunk.audioShorts.length;
        }
        short[] sArr = new short[i];
        int i2 = 0;
        for (AudioChunk audioChunk2 : audioChunkArr) {
            System.arraycopy(audioChunk2.audioShorts, 0, sArr, i2, audioChunk2.audioShorts.length);
            i2 += audioChunk2.audioShorts.length;
        }
        return sArr;
    }

    public AudioChunk getSingleChannelChunk(int i) {
        d.a("channel", "valid", i >= 0 && i < this.channelCount);
        if (this.audioShorts != null) {
            int length = this.audioShorts.length / this.channelCount;
            short[] sArr = new short[length];
            System.arraycopy(this.audioShorts, length * i, sArr, 0, length);
            return new AudioChunk(this.audioType, sArr, this.audioTimestamp);
        }
        int length2 = this.audioBytes.length / this.channelCount;
        byte[] bArr = new byte[length2];
        System.arraycopy(this.audioBytes, length2 * i, bArr, 0, length2);
        return new AudioChunk(this.audioType, bArr, this.audioDuration, this.audioSpeechStatus);
    }
}
