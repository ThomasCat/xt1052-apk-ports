package com.motorola.audiomonitor.uis;

import android.os.CountDownTimer;
import android.util.Log;

class ap extends CountDownTimer {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ am f665a;

    ap(am amVar, long j, long j2) {
        this.f665a = amVar;
        super(j, j2);
    }

    public void onFinish() {
        if (am.f656b) {
            Log.d(am.f655a, "timeout: " + this.f665a.f660f);
        }
        this.f665a.j = null;
        if (!this.f665a.f657c) {
            this.f665a.f658d.a(100);
        } else {
            this.f665a.f658d.a(0);
        }
    }

    public void onTick(long j) {
        if (this.f665a.f658d != null) {
            this.f665a.f658d.a((int) (((this.f665a.f660f - j) * 100) / this.f665a.f660f));
        }
    }
}
