package com.nuance.dragon.toolkit.vocalizer;

public final class VocalizerModelInfo {
    public final int frequencyKhz;
    public final String model;
    public final VocalizerVoice voice;

    VocalizerModelInfo(VocalizerVoice vocalizerVoice, int i, String str) {
        this.voice = vocalizerVoice;
        this.frequencyKhz = i;
        this.model = str;
    }

    public final String toString() {
        return this.voice.toString() + ", " + this.frequencyKhz + "KHz, " + this.model;
    }
}
