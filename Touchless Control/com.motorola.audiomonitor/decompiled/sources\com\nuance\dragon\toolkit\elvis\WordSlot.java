package com.nuance.dragon.toolkit.elvis;

import com.nuance.dragon.toolkit.grammar.Word;
import com.nuance.dragon.toolkit.grammar.WordAction;
import com.nuance.dragon.toolkit.grammar.WordList;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.a;
import com.nuance.dragon.toolkit.util.a.b;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class WordSlot implements JSONCompliant {

    /* renamed from: a reason: collision with root package name */
    private final List f1559a;

    /* renamed from: b reason: collision with root package name */
    private final List f1560b;

    /* renamed from: c reason: collision with root package name */
    private final String f1561c;

    /* renamed from: d reason: collision with root package name */
    private int f1562d;

    /* renamed from: e reason: collision with root package name */
    private final int f1563e;

    /* renamed from: f reason: collision with root package name */
    private boolean f1564f;

    public final class RebuildModes {
        public static final int WORDSLOT_REBUILD_APPEND = 1;
        public static final int WORDSLOT_REBUILD_FULL = 0;
        public static final int WORDSLOT_REBUILD_SKIP_EXISTING = 2;
    }

    public final class Types {
        public static final int WORDSLOT_DEFAULT = 0;
        public static final int WORDSLOT_GENERIC_LONG = 5;
        public static final int WORDSLOT_GENERIC_SHORT = 4;
        public static final int WORDSLOT_MUSIC = 2;
        public static final int WORDSLOT_NAME = 1;
        public static final int WORDSLOT_PHONE_DIGIT = 3;
    }

    public class WordIterator {

        /* renamed from: a reason: collision with root package name */
        private final LinkedList f1565a;

        /* renamed from: b reason: collision with root package name */
        private final LinkedList f1566b;

        /* renamed from: c reason: collision with root package name */
        private final LinkedList f1567c;

        /* renamed from: d reason: collision with root package name */
        private Word f1568d;

        /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.grammar.WordList>, for r5v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.grammar.WordList>] */
        private WordIterator(List list, List<WordList> list2, boolean z) {
            this.f1565a = new LinkedList();
            this.f1566b = new LinkedList();
            this.f1567c = new LinkedList();
            if (list != null) {
                this.f1565a.addAll(list);
            }
            if (list2 != null) {
                for (WordList wordList : list2) {
                    this.f1566b.add(z ? wordList.getFullIterator() : wordList.getModifiedIterator());
                }
            }
            this.f1568d = a();
        }

        /* synthetic */ WordIterator(List list, List list2, boolean z, byte b2) {
            this(list, list2, z);
        }

        private Word a() {
            WordAction wordAction;
            if (!this.f1565a.isEmpty()) {
                return (Word) this.f1565a.remove();
            }
            while (!this.f1566b.isEmpty()) {
                WordAction wordAction2 = null;
                while (true) {
                    if (!((com.nuance.dragon.toolkit.grammar.WordList.WordIterator) this.f1566b.getFirst()).hasNext()) {
                        wordAction = wordAction2;
                        break;
                    }
                    wordAction = (WordAction) ((com.nuance.dragon.toolkit.grammar.WordList.WordIterator) this.f1566b.getFirst()).next();
                    if (wordAction.isAdded()) {
                        break;
                    }
                    wordAction2 = wordAction;
                }
                if (wordAction != null) {
                    return wordAction.getWord();
                }
                this.f1567c.add(this.f1566b.remove());
            }
            return null;
        }

        public void finished(boolean z) {
            LinkedList linkedList = this.f1566b;
            Iterator it = this.f1567c.iterator();
            while (it.hasNext()) {
                com.nuance.dragon.toolkit.grammar.WordList.WordIterator wordIterator = (com.nuance.dragon.toolkit.grammar.WordList.WordIterator) it.next();
                if (z) {
                    wordIterator.acceptChanges();
                } else {
                    wordIterator.discardChanges();
                }
            }
        }

        public Word getNext() {
            Word word = this.f1568d;
            if (this.f1568d != null) {
                this.f1568d = a();
            }
            return word;
        }

        public boolean hasNext() {
            return this.f1568d != null;
        }
    }

    public WordSlot(String str) {
        this(str, 0, 2);
    }

    public WordSlot(String str, int i) {
        this(str, i, 2);
    }

    public WordSlot(String str, int i, int i2) {
        d.a("id", str);
        this.f1561c = str;
        this.f1562d = i;
        this.f1559a = new ArrayList();
        this.f1560b = new ArrayList();
        if (this.f1562d == 4 || this.f1562d == 5) {
            this.f1563e = 2;
            this.f1559a.add(new Word(":GARBAGE:", null));
            this.f1559a.add(new Word("0", null));
            this.f1559a.add(new Word("1", null));
            this.f1559a.add(new Word("2", null));
            this.f1559a.add(new Word("3", null));
            this.f1559a.add(new Word("4", null));
            this.f1559a.add(new Word("5", null));
            this.f1559a.add(new Word("6", null));
            this.f1559a.add(new Word("7", null));
            this.f1559a.add(new Word("8", null));
            this.f1559a.add(new Word("9", null));
            return;
        }
        this.f1563e = i2;
    }

    public static WordSlot createFromJSON(JSONObject jSONObject) {
        WordSlot wordSlot = new WordSlot(jSONObject.getString("id"), jSONObject.getInt("slot_type"), jSONObject.getInt("rebuild_mode"));
        wordSlot.setWordListRequired(jSONObject.getBoolean("requires_wordlist"));
        if (!wordSlot.isGeneric()) {
            JSONArray jSONArray = jSONObject.getJSONArray("words");
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                wordSlot.addWord(Word.createFromJSON(jSONArray.getJSONObject(i)));
            }
        }
        return wordSlot;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a() {
        return this.f1562d == 1 || this.f1562d == 2;
    }

    public void addWord(Word word) {
        d.a("word", (Object) word);
        this.f1559a.add(word);
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.lang.Iterable, code=java.lang.Iterable<com.nuance.dragon.toolkit.grammar.Word>, for r4v0, types: [java.lang.Iterable<com.nuance.dragon.toolkit.grammar.Word>, java.lang.Object, java.lang.Iterable] */
    public void addWords(Iterable<Word> iterable) {
        d.a("words", (Object) iterable);
        for (Word add : iterable) {
            this.f1559a.add(add);
        }
    }

    public void attachWordList(WordList wordList) {
        if (!this.f1560b.contains(wordList)) {
            this.f1560b.add(wordList);
        }
    }

    public void clear() {
        this.f1559a.clear();
    }

    public WordSlot copy() {
        WordSlot wordSlot = new WordSlot(this.f1561c, this.f1562d, this.f1563e);
        wordSlot.f1559a.addAll(this.f1559a);
        wordSlot.f1560b.addAll(this.f1560b);
        wordSlot.f1564f = this.f1564f;
        return wordSlot;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof WordSlot)) {
            return false;
        }
        WordSlot wordSlot = (WordSlot) obj;
        if (this.f1561c == null) {
            if (wordSlot.f1561c != null) {
                return false;
            }
        } else if (!this.f1561c.equals(wordSlot.f1561c)) {
            return false;
        }
        if (this.f1563e != wordSlot.f1563e) {
            return false;
        }
        if (this.f1562d != wordSlot.f1562d) {
            return false;
        }
        if (this.f1564f != wordSlot.f1564f) {
            return false;
        }
        if (this.f1560b == null) {
            if (wordSlot.f1560b != null) {
                return false;
            }
        } else if (!this.f1560b.equals(wordSlot.f1560b)) {
            return false;
        }
        return this.f1559a == null ? wordSlot.f1559a == null : this.f1559a.equals(wordSlot.f1559a);
    }

    public String getId() {
        return this.f1561c;
    }

    public int getRebuildType() {
        if (this.f1563e != 0) {
            for (WordList fullUpdateRequired : this.f1560b) {
                if (fullUpdateRequired.fullUpdateRequired()) {
                    return 0;
                }
            }
        }
        return this.f1563e;
    }

    public int getType() {
        return this.f1562d;
    }

    public boolean getWordListRequired() {
        return this.f1564f;
    }

    public WordIterator getWords(boolean z) {
        return new WordIterator(this.f1559a, this.f1560b, z || getRebuildType() == 0, 0);
    }

    public boolean hasWordList() {
        return !this.f1560b.isEmpty();
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f1560b == null ? 0 : this.f1560b.hashCode()) + (((this.f1564f ? 1231 : 1237) + (((((((this.f1561c == null ? 0 : this.f1561c.hashCode()) + 31) * 31) + this.f1563e) * 31) + this.f1562d) * 31)) * 31)) * 31;
        if (this.f1559a != null) {
            i = this.f1559a.hashCode();
        }
        return hashCode + i;
    }

    public boolean isGeneric() {
        return this.f1562d == 4 || this.f1562d == 5;
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.lang.Iterable, code=java.lang.Iterable<com.nuance.dragon.toolkit.grammar.Word>, for r4v0, types: [java.lang.Iterable<com.nuance.dragon.toolkit.grammar.Word>, java.lang.Object, java.lang.Iterable] */
    public void removeWords(Iterable<Word> iterable) {
        d.a("words", (Object) iterable);
        for (Word remove : iterable) {
            this.f1559a.remove(remove);
        }
    }

    public void setWordListRequired(boolean z) {
        this.f1564f = z;
    }

    public JSONObject toJSON() {
        b bVar = new b();
        bVar.a("id", (Object) this.f1561c);
        bVar.a("slot_type", (Object) Integer.valueOf(this.f1562d));
        bVar.a("rebuild_mode", (Object) Integer.valueOf(this.f1563e));
        bVar.a("requires_wordlist", (Object) Boolean.valueOf(this.f1564f));
        if (!isGeneric()) {
            a aVar = new a();
            for (Word json : this.f1559a) {
                aVar.put(json.toJSON());
            }
            bVar.a("words", (Object) aVar);
        }
        return bVar;
    }
}
