package com.nuance.dragon.toolkit.speechkit;

import com.nuance.dragon.toolkit.audio.AudioEnergyListener;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.SpeechDetectionListener;
import com.nuance.dragon.toolkit.audio.pipes.AudioEnergyCalculatorPipe;
import com.nuance.dragon.toolkit.audio.pipes.EndPointerPipe;
import com.nuance.dragon.toolkit.audio.pipes.SpeexEncoderPipe;
import com.nuance.dragon.toolkit.audio.sources.MicrophoneRecorderSource;
import com.nuance.dragon.toolkit.audio.sources.RecorderSource;
import com.nuance.dragon.toolkit.audio.sources.StreamingFileRecorderSource;
import com.nuance.dragon.toolkit.cloudservices.CloudServices;
import com.nuance.dragon.toolkit.cloudservices.recognizer.CloudRecognitionError;
import com.nuance.dragon.toolkit.cloudservices.recognizer.CloudRecognitionResult;
import com.nuance.dragon.toolkit.cloudservices.recognizer.CloudRecognizer;
import com.nuance.dragon.toolkit.cloudservices.recognizer.RecogSpec;
import com.nuance.dragon.toolkit.util.internal.d;

public final class SKCloudRecognizer {

    /* renamed from: a reason: collision with root package name */
    private final CloudServices f2039a;

    /* renamed from: b reason: collision with root package name */
    private final CloudRecognizer f2040b = new CloudRecognizer(this.f2039a);

    /* renamed from: c reason: collision with root package name */
    private final RecorderSource f2041c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public final SpeexEncoderPipe f2042d;

    /* renamed from: e reason: collision with root package name */
    private final RecogSpec f2043e;

    /* renamed from: f reason: collision with root package name */
    private final int f2044f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public final Listener f2045g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public final AudioEnergyCalculatorPipe f2046h;
    /* access modifiers changed from: private */
    public float i;
    /* access modifiers changed from: private */
    public EndPointerPipe j;
    private boolean k;
    private boolean l;
    /* access modifiers changed from: private */
    public boolean m;
    /* access modifiers changed from: private */
    public SKPrompt n;
    private SKPrompt o;
    /* access modifiers changed from: private */
    public SKPrompt p;
    /* access modifiers changed from: private */
    public SKPrompt q;
    /* access modifiers changed from: private */
    public SKPrompt r;

    public final class EndPointerTypes {
        public static final int LONG = 1;
        public static final int NONE = 0;
    }

    public interface Listener {
        void onError(SKCloudRecognizer sKCloudRecognizer, CloudRecognitionError cloudRecognitionError);

        void onListeningStart(SKCloudRecognizer sKCloudRecognizer);

        void onListeningStop(SKCloudRecognizer sKCloudRecognizer);

        void onResult(SKCloudRecognizer sKCloudRecognizer, CloudRecognitionResult cloudRecognitionResult);
    }

    public final class PromptTypes {
        public static final int ERROR = 3;
        public static final int RESULT = 2;
        public static final int START_LISTENING = 0;
        public static final int STOP_LISTENING = 1;
    }

    SKCloudRecognizer(CloudServices cloudServices, RecogSpec recogSpec, int i2, String str, boolean z, Listener listener) {
        this.f2039a = cloudServices;
        if (str == null) {
            this.f2041c = new MicrophoneRecorderSource(AudioType.PCM_16k);
        } else {
            this.f2041c = new StreamingFileRecorderSource(AudioType.PCM_16k, str, z);
        }
        this.f2046h = new AudioEnergyCalculatorPipe(new AudioEnergyListener() {
            public final void onEnergyLevelAvaiable(float f2) {
                SKCloudRecognizer.this.i = f2;
            }
        });
        this.f2042d = new SpeexEncoderPipe();
        this.f2043e = recogSpec;
        this.f2044f = i2;
        this.f2045g = listener;
    }

    /* access modifiers changed from: private */
    public void a() {
        if (!this.m) {
            this.m = true;
            if (this.r != null) {
                this.r.a();
                this.r = null;
            }
            this.n = null;
            this.o = null;
            this.p = null;
            this.q = null;
            this.f2041c.stopRecording();
            this.f2040b.cancel();
            this.f2042d.release();
        }
    }

    static /* synthetic */ void d(SKCloudRecognizer sKCloudRecognizer) {
        sKCloudRecognizer.f2046h.connectAudioSource(sKCloudRecognizer.f2041c);
        sKCloudRecognizer.f2042d.connectAudioSource(sKCloudRecognizer.f2046h);
        sKCloudRecognizer.f2045g.onListeningStart(sKCloudRecognizer);
    }

    public final void cancel() {
        if (!this.m) {
            a();
        }
    }

    public final float getAudioLevel() {
        return this.i;
    }

    public final void setPrompt(int i2, SKPrompt sKPrompt) {
        switch (i2) {
            case 0:
                this.n = sKPrompt;
                return;
            case 1:
                this.o = sKPrompt;
                return;
            case 2:
                this.p = sKPrompt;
                return;
            case 3:
                this.q = sKPrompt;
                return;
            default:
                return;
        }
    }

    public final void start() {
        d.a((Object) this, !this.k, "Already started");
        d.a((Object) this, !this.m, "Already finished");
        this.k = true;
        this.l = false;
        this.f2041c.startRecording(new com.nuance.dragon.toolkit.audio.sources.RecorderSource.Listener() {
            public final void onStarted(RecorderSource recorderSource) {
                if (!SKCloudRecognizer.this.m) {
                    if (SKCloudRecognizer.this.n != null) {
                        SKCloudRecognizer sKCloudRecognizer = SKCloudRecognizer.this;
                        SKCloudRecognizer.this.r = SKCloudRecognizer.this.n;
                        SKCloudRecognizer.this.n = null;
                        SKCloudRecognizer.this.r.a(new a() {
                            public final void a(SKPrompt sKPrompt) {
                                if (!SKCloudRecognizer.this.m) {
                                    SKCloudRecognizer sKCloudRecognizer = SKCloudRecognizer.this;
                                    SKCloudRecognizer.this.r = null;
                                    SKCloudRecognizer.d(SKCloudRecognizer.this);
                                }
                            }
                        });
                        return;
                    }
                    SKCloudRecognizer sKCloudRecognizer2 = SKCloudRecognizer.this;
                    SKCloudRecognizer.d(SKCloudRecognizer.this);
                }
            }

            public final void onStopped(RecorderSource recorderSource) {
            }
        });
        if (this.f2044f != 0) {
            this.j = new EndPointerPipe(new SpeechDetectionListener() {
                public final void onEndOfSpeech() {
                    if (!SKCloudRecognizer.this.m) {
                        SKCloudRecognizer sKCloudRecognizer = SKCloudRecognizer.this;
                        if (SKCloudRecognizer.this.j != null) {
                            SKCloudRecognizer.this.j.disconnectAudioSource();
                        }
                        SKCloudRecognizer.this.f2042d.disconnectAudioSource();
                        SKCloudRecognizer.this.f2046h.disconnectAudioSource();
                        SKCloudRecognizer.this.stopListening();
                    }
                }

                public final void onStartOfSpeech() {
                    if (SKCloudRecognizer.this.m) {
                    }
                }
            });
            this.j.connectAudioSource(this.f2042d);
        }
        this.f2040b.startRecognition(this.f2043e, this.j != null ? this.j : this.f2042d, new com.nuance.dragon.toolkit.cloudservices.recognizer.CloudRecognizer.Listener() {
            public final void onError(CloudRecognitionError cloudRecognitionError) {
                SKCloudRecognizer sKCloudRecognizer = SKCloudRecognizer.this;
                if (!SKCloudRecognizer.this.m && SKCloudRecognizer.this.q != null) {
                    SKCloudRecognizer.this.q.a(null);
                    SKCloudRecognizer.this.q = null;
                }
                SKCloudRecognizer.this.a();
                SKCloudRecognizer.this.f2045g.onError(SKCloudRecognizer.this, cloudRecognitionError);
            }

            public final void onResult(CloudRecognitionResult cloudRecognitionResult) {
                if (!SKCloudRecognizer.this.m) {
                    SKCloudRecognizer sKCloudRecognizer = SKCloudRecognizer.this;
                    if (SKCloudRecognizer.this.p != null) {
                        SKCloudRecognizer.this.p.a(null);
                        SKCloudRecognizer.this.p = null;
                    }
                    SKCloudRecognizer.this.a();
                    SKCloudRecognizer.this.f2045g.onResult(SKCloudRecognizer.this, cloudRecognitionResult);
                }
            }
        });
    }

    public final void stopListening() {
        d.a((Object) this, this.k, "Not started yet");
        d.a((Object) this, !this.m, "Already finished");
        if (!this.l) {
            this.l = true;
            this.f2040b.processResult();
            this.f2041c.stopRecording();
            this.f2045g.onListeningStop(this);
            if (this.o != null) {
                if (this.r != null) {
                    this.r.a();
                }
                this.r = this.o;
                this.o = null;
                this.r.a(new a() {
                    public final void a(SKPrompt sKPrompt) {
                        if (!SKCloudRecognizer.this.m && sKPrompt == SKCloudRecognizer.this.r) {
                            SKCloudRecognizer.this.r = null;
                        }
                    }
                });
            }
        }
    }
}
