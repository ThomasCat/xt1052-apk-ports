package com.nuance.dragon.toolkit.recognition.dictation.a;

import com.nuance.dragon.toolkit.recognition.dictation.Sentence;
import com.nuance.dragon.toolkit.recognition.dictation.Token;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.a;
import com.nuance.dragon.toolkit.util.a.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class g implements Sentence, JSONCompliant {

    /* renamed from: a reason: collision with root package name */
    private final ArrayList f1946a;

    /* renamed from: b reason: collision with root package name */
    private double f1947b;

    public g() {
        this.f1946a = new ArrayList();
    }

    private g(ArrayList arrayList, double d2) {
        this.f1947b = d2;
        this.f1946a = arrayList;
    }

    private int a(long j) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f1946a.size()) {
                return -1;
            }
            if (((h) tokenAt(i2)).getStartTime() >= j) {
                return i2;
            }
            i = i2 + 1;
        }
    }

    static g a(JSONObject jSONObject) {
        double d2 = jSONObject.getDouble("conf");
        JSONArray jSONArray = jSONObject.getJSONArray("tokens");
        ArrayList arrayList = new ArrayList(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(h.a(jSONArray.getJSONObject(i)));
        }
        return new g(arrayList, d2);
    }

    private void a(int i, g gVar) {
        for (int size = gVar.f1946a.size() - 1; size >= 0; size--) {
            this.f1946a.add(i, gVar.f1946a.get(size));
        }
    }

    public final ArrayList a(long j, long j2) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f1946a.size()) {
                break;
            }
            h hVar = (h) tokenAt(i2);
            long startTime = hVar.getStartTime();
            long endTime = hVar.getEndTime();
            if (startTime < j2 && endTime > j) {
                arrayList.add(hVar);
            } else if (startTime > j2) {
                break;
            }
            i = i2 + 1;
        }
        return arrayList;
    }

    public final void a(double d2) {
        this.f1947b = d2;
    }

    /* access modifiers changed from: 0000 */
    public final void a(long j, int i) {
        while (i < this.f1946a.size()) {
            h hVar = (h) tokenAt(i);
            hVar.a(hVar.getStartTime() + j);
            hVar.b(hVar.getEndTime() + j);
            i++;
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(g gVar, long j, long j2, long j3) {
        if (j < 0 || j2 < 0) {
            throw new IllegalArgumentException("Timings cannot be negative");
        } else if (j > j2) {
            throw new IllegalArgumentException("The timings are corrupted, the timingBegin is greater than the timingEnd");
        } else {
            g gVar2 = null;
            if (gVar != null) {
                gVar2 = (g) gVar.clone();
            }
            if (this.f1946a.size() != 0) {
                ArrayList a2 = a(j, j2);
                if (a2.size() != 0) {
                    for (int i = 0; i < a2.size(); i++) {
                        this.f1946a.remove(a2.get(i));
                    }
                }
                int a3 = a(j);
                if (a3 != -1) {
                    a(j3, a3);
                }
                int a4 = a(j);
                if (a4 == -1) {
                    a4 = this.f1946a.size();
                }
                if (gVar2 != null && gVar2.f1946a.size() != 0) {
                    gVar2.a(j, 0);
                    a(a4, gVar2);
                }
            } else if (gVar2 != null) {
                this.f1946a.addAll(gVar2.f1946a);
                this.f1947b = gVar2.f1947b;
            }
        }
    }

    public final void a(h hVar) {
        this.f1946a.add(hVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public final Sentence clone() {
        g gVar = new g();
        gVar.f1947b = this.f1947b;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f1946a.size()) {
                return gVar;
            }
            gVar.f1946a.add(((h) tokenAt(i2)).clone());
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: 0000 */
    public final List b_() {
        return this.f1946a;
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        String property = System.getProperty("line.separator");
        String str = property == null ? "\n" : property;
        for (int i = 0; i < this.f1946a.size(); i++) {
            h hVar = (h) tokenAt(i);
            StringBuffer stringBuffer = new StringBuffer(hVar.a());
            int i2 = 0;
            while (i2 < stringBuffer.length()) {
                int indexOf = stringBuffer.toString().indexOf("\r\n", i2);
                if (indexOf == -1) {
                    break;
                }
                stringBuffer.deleteCharAt(indexOf);
                stringBuffer.deleteCharAt(indexOf);
                stringBuffer.insert(indexOf, 10);
                i2 = indexOf + 1;
            }
            int i3 = 0;
            while (i3 < stringBuffer.length()) {
                int indexOf2 = stringBuffer.toString().indexOf("\n", i3);
                if (indexOf2 == -1) {
                    break;
                }
                stringBuffer.deleteCharAt(indexOf2);
                stringBuffer.insert(indexOf2, str);
                i3 = indexOf2 + str.length();
            }
            hVar.a(stringBuffer.toString());
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof g)) {
            return false;
        }
        g gVar = (g) obj;
        return this.f1947b == gVar.f1947b && this.f1946a.equals(gVar.f1946a);
    }

    public double getConfidenceScore() {
        return this.f1947b;
    }

    public long getEndTime() {
        if (this.f1946a.isEmpty()) {
            return 0;
        }
        return ((Token) this.f1946a.get(this.f1946a.size() - 1)).getEndTime();
    }

    public long getStartTime() {
        if (this.f1946a.isEmpty()) {
            return 0;
        }
        return ((Token) this.f1946a.get(0)).getStartTime();
    }

    public final int hashCode() {
        return this.f1946a.hashCode() + ((int) (this.f1947b * 1000.0d));
    }

    public int size() {
        return this.f1946a.size();
    }

    public JSONObject toJSON() {
        b bVar = new b();
        bVar.a("conf", (Object) Double.valueOf(this.f1947b));
        a aVar = new a();
        Iterator it = this.f1946a.iterator();
        while (it.hasNext()) {
            aVar.a(((h) it.next()).toJSON());
        }
        bVar.a("tokens", (Object) aVar);
        return bVar;
    }

    public String toString() {
        if (this.f1946a.size() == 0) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        boolean z = true;
        while (true) {
            int i2 = i;
            if (i2 >= this.f1946a.size()) {
                return stringBuffer.toString();
            }
            h hVar = (h) this.f1946a.get(i2);
            if (!hVar.hasNoSpaceBeforeDirective() && !z) {
                stringBuffer.append(' ');
            }
            stringBuffer.append(tokenAt(i2).toString());
            z = hVar.hasNoSpaceAfterDirective();
            i = i2 + 1;
        }
    }

    public Token tokenAt(int i) {
        return (h) this.f1946a.get(i);
    }
}
