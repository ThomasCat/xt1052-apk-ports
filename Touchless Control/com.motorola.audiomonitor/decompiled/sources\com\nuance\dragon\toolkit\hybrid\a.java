package com.nuance.dragon.toolkit.hybrid;

import android.util.Pair;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.SpeechDetectionListener;
import com.nuance.dragon.toolkit.audio.pipes.DuplicatorPipe;
import com.nuance.dragon.toolkit.audio.pipes.SpeexEncoderPipe;
import com.nuance.dragon.toolkit.cloudservices.CloudServices;
import com.nuance.dragon.toolkit.cloudservices.DataParam;
import com.nuance.dragon.toolkit.cloudservices.recognizer.CloudRecognitionError;
import com.nuance.dragon.toolkit.cloudservices.recognizer.CloudRecognitionResult;
import com.nuance.dragon.toolkit.cloudservices.recognizer.CloudRecognizer;
import com.nuance.dragon.toolkit.cloudservices.recognizer.RecogSpec;
import com.nuance.dragon.toolkit.elvis.ElvisError;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.ResultListener;
import com.nuance.dragon.toolkit.elvis.ElvisResult;
import com.nuance.dragon.toolkit.grammar.Scenario;
import com.nuance.dragon.toolkit.hybrid.HybridRecognizer.Listener;
import com.nuance.dragon.toolkit.recognition.InterpretException;
import com.nuance.dragon.toolkit.recognition.InterpretedRecognition;
import com.nuance.dragon.toolkit.recognition.InterpretedRecognitionCombiner;
import com.nuance.dragon.toolkit.recognition.RecognitionInterpreter;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.a.b;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.ArrayList;
import java.util.List;

final class a extends HybridRecognizer {

    /* renamed from: a reason: collision with root package name */
    private DuplicatorPipe f1842a;

    /* renamed from: b reason: collision with root package name */
    private SpeexEncoderPipe f1843b;

    /* renamed from: c reason: collision with root package name */
    private AudioSource f1844c;

    /* renamed from: d reason: collision with root package name */
    private AudioSource f1845d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public final ElvisRecognizer f1846e;

    /* renamed from: f reason: collision with root package name */
    private final CloudServices f1847f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public final RecognitionInterpreter f1848g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public final RecognitionInterpreter f1849h;
    /* access modifiers changed from: private */
    public final InterpretedRecognitionCombiner i;
    private final com.nuance.dragon.toolkit.util.internal.a j = new com.nuance.dragon.toolkit.util.internal.a();
    /* access modifiers changed from: private */
    public C0009a k;

    /* renamed from: com.nuance.dragon.toolkit.hybrid.a$a reason: collision with other inner class name */
    final class C0009a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public CloudRecognizer f1867a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public InterpretedRecognition f1868b;
        /* access modifiers changed from: private */

        /* renamed from: c reason: collision with root package name */
        public boolean f1869c;
        /* access modifiers changed from: private */

        /* renamed from: d reason: collision with root package name */
        public InterpretedRecognition f1870d;
        /* access modifiers changed from: private */

        /* renamed from: e reason: collision with root package name */
        public ElvisError f1871e;
        /* access modifiers changed from: private */

        /* renamed from: f reason: collision with root package name */
        public b f1872f;
        /* access modifiers changed from: private */

        /* renamed from: g reason: collision with root package name */
        public List f1873g;

        private C0009a() {
        }

        /* synthetic */ C0009a(byte b2) {
            this();
        }
    }

    public a(ElvisRecognizer elvisRecognizer, CloudServices cloudServices, RecognitionInterpreter recognitionInterpreter, RecognitionInterpreter recognitionInterpreter2, InterpretedRecognitionCombiner interpretedRecognitionCombiner) {
        d.a("elvisRecognizer", (Object) elvisRecognizer);
        d.a("cloudServices", (Object) cloudServices);
        d.a("elvisInterpreter", (Object) recognitionInterpreter);
        d.a("cloudInterpreter", (Object) recognitionInterpreter2);
        d.a("hybridCombiner", (Object) interpretedRecognitionCombiner);
        this.f1846e = elvisRecognizer;
        this.f1847f = cloudServices;
        this.f1848g = recognitionInterpreter;
        this.f1849h = recognitionInterpreter2;
        this.i = interpretedRecognitionCombiner;
    }

    /* access modifiers changed from: private */
    public void a() {
        if (this.f1842a != null) {
            this.f1842a.disconnectAudioSource();
            this.f1842a = null;
        }
        if (this.f1843b != null) {
            this.f1843b.disconnectAudioSource();
            this.f1843b.release();
            this.f1843b = null;
        }
    }

    static /* synthetic */ void a(a aVar, C0009a aVar2, HybridRecognitionError hybridRecognitionError, Listener listener) {
        if (aVar2 == aVar.k) {
            aVar.k = null;
        }
        listener.onError(hybridRecognitionError);
    }

    static /* synthetic */ void a(a aVar, C0009a aVar2, InterpretedRecognition interpretedRecognition, InterpretedRecognition interpretedRecognition2, InterpretedRecognitionCombiner interpretedRecognitionCombiner, Listener listener) {
        final InterpretedRecognition interpretedRecognition3 = interpretedRecognition;
        final InterpretedRecognition interpretedRecognition4 = interpretedRecognition2;
        final InterpretedRecognitionCombiner interpretedRecognitionCombiner2 = interpretedRecognitionCombiner;
        final C0009a aVar3 = aVar2;
        final Listener listener2 = listener;
        aVar2.f1872f = new b() {
            /* access modifiers changed from: private */
            /* renamed from: b */
            public Pair a() {
                try {
                    return new Pair(new HybridRecognitionResult(interpretedRecognition3, interpretedRecognition4, interpretedRecognitionCombiner2, aVar3.f1873g), null);
                } catch (InterpretException e2) {
                    HybridRecognitionError hybridRecognitionError = new HybridRecognitionError();
                    hybridRecognitionError.b(e2);
                    hybridRecognitionError.a(e2);
                    return new Pair(null, hybridRecognitionError);
                }
            }

            public final /* synthetic */ void a(Object obj) {
                Pair pair = (Pair) obj;
                if (aVar3 == a.this.k) {
                    a.this.k = null;
                    if (pair.first != null) {
                        listener2.onResult((HybridRecognitionResult) pair.first);
                    } else {
                        listener2.onError((HybridRecognitionError) pair.second);
                    }
                }
            }
        };
        aVar.j.a(aVar.k.f1872f);
    }

    public final void cancel() {
        if (this.k != null) {
            this.k.f1870d = null;
            this.k.f1868b = null;
            CloudRecognizer c2 = this.k.f1867a;
            this.k = null;
            this.f1846e.cancelRecognition();
            if (c2 != null) {
                c2.cancel();
            }
        }
    }

    public final void startRecognition(AudioSource audioSource, Scenario scenario, final Listener listener) {
        d.a("audioSource", (Object) audioSource);
        d.a("scenario", (Object) scenario);
        d.a("listener", (Object) listener);
        RecogSpec cloudRecogSpec = scenario.getCloudRecogSpec();
        List elvisConstraints = scenario.getElvisConstraints();
        d.a("Cloud recognition spec and Elvis constraints ", cloudRecogSpec, elvisConstraints);
        cancel();
        final C0009a aVar = new C0009a(0);
        this.k = aVar;
        a();
        this.f1842a = new DuplicatorPipe();
        this.f1842a.connectAudioSource(audioSource);
        this.f1844c = this.f1842a;
        this.f1843b = new SpeexEncoderPipe();
        this.f1843b.connectAudioSource(this.f1842a);
        this.f1845d = this.f1843b;
        if (cloudRecogSpec != null) {
            aVar.f1867a = new CloudRecognizer(this.f1847f);
            aVar.f1867a.startRecognition(cloudRecogSpec, this.f1845d, new CloudRecognizer.Listener() {
                private void a(CloudRecognitionError cloudRecognitionError, InterpretException interpretException) {
                    if (aVar.f1867a == null) {
                        Logger.error(a.this, "Got unexpected Cloud error");
                        return;
                    }
                    aVar.f1867a = null;
                    if (!aVar.f1869c) {
                        a.this.a();
                        if (aVar.f1870d != null) {
                            a.a(a.this, aVar, aVar.f1870d, null, a.this.i, listener);
                            return;
                        }
                        HybridRecognitionError hybridRecognitionError = new HybridRecognitionError();
                        hybridRecognitionError.a(aVar.f1871e);
                        hybridRecognitionError.a(cloudRecognitionError);
                        hybridRecognitionError.b(interpretException);
                        a.a(a.this, aVar, hybridRecognitionError, listener);
                    }
                }

                public final void onError(CloudRecognitionError cloudRecognitionError) {
                    a aVar = a.this;
                    a(cloudRecognitionError, null);
                }

                public final void onResult(CloudRecognitionResult cloudRecognitionResult) {
                    a aVar = a.this;
                    if (aVar.f1867a == null) {
                        Logger.error(a.this, "Got unexpected Cloud result");
                        return;
                    }
                    try {
                        aVar.f1868b = a.this.f1849h.getInterpretedResult(cloudRecognitionResult);
                        aVar.f1873g = a.this.f1849h.getUpdateRequiredList(cloudRecognitionResult);
                        aVar.f1867a = null;
                        if (!aVar.f1869c) {
                            a.this.a();
                            a.a(a.this, aVar, aVar.f1870d, aVar.f1868b, a.this.i, listener);
                        }
                    } catch (InterpretException e2) {
                        a(null, e2);
                    }
                }
            });
        }
        if (aVar == this.k) {
            if (elvisConstraints == null || elvisConstraints.isEmpty()) {
                aVar.f1869c = true;
                this.f1846e.startSpeechDetection(this.f1844c, new SpeechDetectionListener() {
                    public final void onEndOfSpeech() {
                        if (!aVar.f1869c) {
                            Logger.error(a.this, "Got unexpected Elvis end-of-speech");
                            return;
                        }
                        aVar.f1869c = false;
                        a.this.f1846e.stopListening();
                        a.this.a();
                        listener.onEndOfSpeech();
                        if (aVar.f1867a != null) {
                            aVar.f1867a.processResult();
                        }
                    }

                    public final void onStartOfSpeech() {
                        listener.onStartOfSpeech();
                    }
                });
                return;
            }
            aVar.f1869c = true;
            this.f1846e.setActiveConstraintNames(elvisConstraints);
            this.f1846e.startRecognition(this.f1844c, new SpeechDetectionListener() {
                public final void onEndOfSpeech() {
                    a.this.f1846e.stopListening();
                    a.this.a();
                    listener.onEndOfSpeech();
                }

                public final void onStartOfSpeech() {
                    listener.onStartOfSpeech();
                }
            }, new ResultListener() {
                private void a(ElvisError elvisError, InterpretException interpretException) {
                    if (!aVar.f1869c) {
                        Logger.error(a.this, "Got unexpected Elvis error");
                        return;
                    }
                    aVar.f1869c = false;
                    a.this.a();
                    if (aVar.f1867a == null) {
                        HybridRecognitionError hybridRecognitionError = new HybridRecognitionError();
                        hybridRecognitionError.a(elvisError);
                        hybridRecognitionError.a(interpretException);
                        a.a(a.this, aVar, hybridRecognitionError, listener);
                        return;
                    }
                    aVar.f1871e = elvisError;
                    aVar.f1867a.processResult();
                }

                public final void onError(ElvisError elvisError) {
                    a aVar = a.this;
                    a(elvisError, null);
                }

                public final void onResult(ElvisResult elvisResult) {
                    a aVar = a.this;
                    if (!aVar.f1869c) {
                        Logger.error(a.this, "Got unexpected Elvis result");
                        return;
                    }
                    try {
                        aVar.f1870d = a.this.f1848g.getInterpretedResult(elvisResult);
                        if (aVar.f1867a != null) {
                            ArrayList<DataParam> arrayList = new ArrayList<>(3);
                            boolean z = !a.this.f1848g.processForCloud(elvisResult, arrayList);
                            for (DataParam sendParam : arrayList) {
                                aVar.f1867a.sendParam(sendParam);
                            }
                            aVar.f1867a.processResult();
                            if (!z) {
                                aVar.f1867a = null;
                            }
                        }
                        aVar.f1869c = false;
                        if (aVar.f1867a == null) {
                            a.this.a();
                            a.a(a.this, aVar, aVar.f1870d, aVar.f1868b, a.this.i, listener);
                        }
                    } catch (InterpretException e2) {
                        a(null, e2);
                    }
                }
            });
        }
    }

    public final void stopRecognition() {
        this.f1846e.stopListening();
    }
}
