package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.dragon.toolkit.util.internal.d;

public abstract class Param {
    public static final int TYPE_AUDIO = 1;
    public static final int TYPE_BYTES = 5;
    public static final int TYPE_DICTIONARY = 6;
    public static final int TYPE_INTEGER = 4;
    public static final int TYPE_SEQUENCE = 7;
    public static final int TYPE_SEQUENCE_CHUNK = 9;
    public static final int TYPE_SEQUENCE_END = 10;
    public static final int TYPE_SEQUENCE_START = 8;
    public static final int TYPE_STRING = 3;
    public static final int TYPE_TTS = 2;

    /* renamed from: a reason: collision with root package name */
    final String f1355a;

    /* renamed from: b reason: collision with root package name */
    final int f1356b;

    interface a {
        void a(Param param, boolean z);
    }

    Param(int i, String str) {
        d.a("type", "should be one of Param.TYPE_AUDIO, Param.TYPE_TTS, Param.TYPE_STRING, Param.TYPE_INTEGER, Param.TYPE_BYTES, Param.TYPE_DICTIONARY, Param.TYPE_SEQUENCE, Param.TYPE_SEQUENCE_START, Param.TYPE_SEQUENCE_CHUNK, Param.TYPE_SEQUENCE_END", i > 0 && i <= 10);
        d.a("key", str);
        this.f1356b = i;
        this.f1355a = str;
    }

    /* access modifiers changed from: 0000 */
    public void a() {
    }

    /* access modifiers changed from: 0000 */
    public abstract boolean a(d dVar, a aVar);
}
