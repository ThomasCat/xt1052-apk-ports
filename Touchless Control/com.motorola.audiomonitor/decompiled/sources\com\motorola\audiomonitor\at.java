package com.motorola.audiomonitor;

import android.content.Intent;
import android.util.Log;

class at implements Runnable {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ MonitorService f311a;

    /* renamed from: b reason: collision with root package name */
    private final Intent f312b;

    /* renamed from: c reason: collision with root package name */
    private final int f313c;

    /* renamed from: d reason: collision with root package name */
    private final int f314d;

    public at(MonitorService monitorService, Intent intent, int i, int i2) {
        this.f311a = monitorService;
        this.f312b = intent;
        this.f313c = i;
        this.f314d = i2;
    }

    public void run() {
        if (MonitorService.f150c) {
            Log.d(MonitorService.f149b, "Delayed start attempt");
        }
        this.f311a.onStartCommand(this.f312b, this.f313c, this.f314d);
    }
}
