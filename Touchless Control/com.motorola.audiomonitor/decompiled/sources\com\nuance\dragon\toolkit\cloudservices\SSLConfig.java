package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.dragon.toolkit.util.internal.d;

public class SSLConfig {

    /* renamed from: a reason: collision with root package name */
    private final boolean f1357a;

    /* renamed from: b reason: collision with root package name */
    private final String f1358b;

    /* renamed from: c reason: collision with root package name */
    private final String f1359c;

    public SSLConfig() {
        this.f1357a = false;
        this.f1358b = null;
        this.f1359c = null;
    }

    public SSLConfig(boolean z, String str, String str2) {
        this.f1357a = z;
        d.b("certSummary", str);
        d.b("certData", str2);
        this.f1358b = str;
        this.f1359c = str2;
    }

    public String certData() {
        return this.f1359c;
    }

    public String certSummary() {
        return this.f1358b;
    }

    public boolean isSelfSigned() {
        return this.f1357a;
    }
}
