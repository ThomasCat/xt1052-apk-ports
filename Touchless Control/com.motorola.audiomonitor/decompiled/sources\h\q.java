package h;

public enum q {
    TYPE_NORMAL(0),
    TYPE_ABORT(1),
    TYPE_ENROLL_RAW(2),
    TYPE_ENROLL_EOS(3),
    TYPE_ENROLL_OUT(4),
    TYPE_GOOD_TRIGGER(5),
    TYPE_BAD_TRIGGER(6);
    

    /* renamed from: h reason: collision with root package name */
    private final int f2329h;

    private q(int i2) {
        this.f2329h = i2;
    }

    public String a() {
        switch (this.f2329h) {
            case 0:
                return "normal";
            case 1:
                return "abort";
            case 2:
                return "enroll_raw";
            case 3:
                return "enroll_eos";
            case 4:
                return "enroll_out";
            case 5:
                return "good_trigger";
            case 6:
                return "bad_trigger";
            default:
                return "unknown";
        }
    }
}
