package com.nuance.dragon.toolkit.cloudservices.recognizer;

import com.nuance.dragon.toolkit.cloudservices.TransactionError;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import org.json.JSONObject;

public class CloudRecognitionError implements JSONCompliant {

    /* renamed from: a reason: collision with root package name */
    private final TransactionError f1473a;

    /* renamed from: b reason: collision with root package name */
    private final int f1474b;

    public final class Errors {
        public static final int TRANSACTION = 0;
    }

    private CloudRecognitionError(int i, TransactionError transactionError) {
        this.f1474b = i;
        this.f1473a = transactionError;
    }

    public CloudRecognitionError(TransactionError transactionError) {
        this(0, transactionError);
    }

    public static CloudRecognitionError createFromJSON(JSONObject jSONObject) {
        TransactionError transactionError = null;
        int i = jSONObject.getInt("type");
        if (i == 0) {
            transactionError = TransactionError.createFromJSON(jSONObject.getJSONObject("transaction_error"));
        }
        return new CloudRecognitionError(i, transactionError);
    }

    public TransactionError getTransactionError() {
        return this.f1473a;
    }

    public int getType() {
        return this.f1474b;
    }

    public JSONObject toJSON() {
        b bVar = new b();
        bVar.a("type", (Object) Integer.valueOf(this.f1474b));
        bVar.a("transaction_error", (JSONCompliant) this.f1473a);
        return bVar;
    }
}
