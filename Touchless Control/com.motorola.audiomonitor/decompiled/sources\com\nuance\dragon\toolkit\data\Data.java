package com.nuance.dragon.toolkit.data;

import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.d;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.Frequencies;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class Data implements JSONCompliant {
    public static final short TYPE_ASCII = 22;
    public static final short TYPE_BYTES = 4;
    public static final short TYPE_DICT = 224;
    public static final short TYPE_INT = 192;
    public static final short TYPE_NULL = 5;
    public static final short TYPE_SEQ = 16;
    public static final short TYPE_STRING = 193;
    public static final short TYPE_UTF8 = 193;
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public short f1512a;

    public final class Bytes extends Data {

        /* renamed from: a reason: collision with root package name */
        private static final char[] f1513a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        public byte[] value;

        public Bytes(byte[] bArr) {
            super(4);
            d.a("bytes", (Object) bArr);
            this.value = new byte[bArr.length];
            System.arraycopy(bArr, 0, this.value, 0, bArr.length);
        }

        public static Bytes createFromJSON(JSONObject jSONObject) {
            d.a("o", jSONObject);
            String[] split = jSONObject.getString("value").split(",");
            byte[] bArr = new byte[split.length];
            for (int i = 0; i < bArr.length; i++) {
                bArr[i] = Byte.parseByte(split[i]);
            }
            return new Bytes(bArr);
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!Data.super.equals(obj)) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            return Arrays.equals(this.value, ((Bytes) obj).value);
        }

        public final int hashCode() {
            return (Data.super.hashCode() * 31) + Arrays.hashCode(this.value);
        }

        /* access modifiers changed from: protected */
        public final void putInJSON(JSONObject jSONObject) {
            StringBuilder sb = new StringBuilder();
            byte[] bArr = this.value;
            int length = bArr.length;
            boolean z = true;
            int i = 0;
            while (i < length) {
                byte b2 = bArr[i];
                if (!z) {
                    sb.append(',');
                }
                sb.append(b2);
                i++;
                z = false;
            }
            jSONObject.put("value", sb.toString());
        }

        public final String toString(String str) {
            byte[] bArr;
            StringBuilder sb = new StringBuilder();
            sb.append("0x");
            for (byte b2 : this.value) {
                byte b3 = (byte) ((b2 >> 4) & 15);
                byte b4 = (byte) (b2 & 15);
                sb.append(f1513a[b3]);
                sb.append(f1513a[b4]);
            }
            return sb.toString();
        }
    }

    public final class Dictionary extends Data {

        /* renamed from: a reason: collision with root package name */
        private final Map f1514a;

        public Dictionary() {
            this(null);
        }

        public Dictionary(Map map) {
            super(Data.TYPE_DICT);
            this.f1514a = new LinkedHashMap();
            if (map != null) {
                this.f1514a.putAll(map);
            }
        }

        public static Dictionary createFromJSON(JSONObject jSONObject) {
            d.a("o", jSONObject);
            JSONObject jSONObject2 = jSONObject.getJSONObject("value");
            JSONArray names = jSONObject2.names();
            Dictionary dictionary = new Dictionary();
            for (int i = 0; i < names.length(); i++) {
                String string = names.getString(i);
                dictionary.put(string, Data.createFromJSON(jSONObject2.getJSONObject(string)));
            }
            return dictionary;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!Data.super.equals(obj)) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Dictionary dictionary = (Dictionary) obj;
            return this.f1514a == null ? dictionary.f1514a == null : this.f1514a.equals(dictionary.f1514a);
        }

        public final Data get(String str) {
            return (Data) this.f1514a.get(str);
        }

        public final Bytes getBytes(String str) {
            Data data = get(str);
            if (data == null || data.f1512a != 4) {
                return null;
            }
            return (Bytes) data;
        }

        public final Dictionary getDictionary(String str) {
            Data data = get(str);
            if (data == null || data.f1512a != 224) {
                return null;
            }
            return (Dictionary) data;
        }

        public final Set getEntries() {
            return this.f1514a.entrySet();
        }

        public final Integer getInt(String str) {
            Data data = get(str);
            if (data == null || data.f1512a != 192) {
                return null;
            }
            return (Integer) data;
        }

        public final Sequence getSequence(String str) {
            Data data = get(str);
            if (data == null || data.f1512a != 16) {
                return null;
            }
            return (Sequence) data;
        }

        public final String getString(String str) {
            Data data = get(str);
            if (data == null || data.f1512a != 193) {
                return null;
            }
            return (String) data;
        }

        public final int hashCode() {
            return (this.f1514a == null ? 0 : this.f1514a.hashCode()) + (Data.super.hashCode() * 31);
        }

        public final void put(String str, int i) {
            d.a("key", (Object) str);
            this.f1514a.put(str, new Integer(i));
        }

        public final void put(String str, Data data) {
            d.a("key", (Object) str);
            this.f1514a.put(str, data);
        }

        public final void put(String str, String str2) {
            d.a("key", (Object) str);
            this.f1514a.put(str, new String(str2));
        }

        public final void put(String str, byte[] bArr) {
            d.a("key", (Object) str);
            this.f1514a.put(str, new Bytes(bArr));
        }

        public final void putAll(Dictionary dictionary) {
            this.f1514a.putAll(dictionary.f1514a);
        }

        /* access modifiers changed from: protected */
        public final void putInJSON(JSONObject jSONObject) {
            JSONObject jSONObject2 = new JSONObject();
            try {
                for (Entry entry : this.f1514a.entrySet()) {
                    jSONObject2.put((String) entry.getKey(), ((Data) entry.getValue()).toJSON());
                }
                jSONObject.put("value", jSONObject2);
            } catch (JSONException e2) {
            }
        }

        /* access modifiers changed from: protected */
        public final String toString(String str) {
            StringBuilder sb = new StringBuilder();
            sb.append("{\n");
            String str2 = str + "  ";
            for (Entry entry : this.f1514a.entrySet()) {
                sb.append(str2);
                sb.append((String) entry.getKey());
                sb.append(" : ");
                sb.append(((Data) entry.getValue()).toString(str2 + "  "));
                sb.append(",\n");
            }
            sb.append(str);
            sb.append("}");
            return sb.toString();
        }
    }

    public final class Integer extends Data {
        public final int value;

        public Integer(int i) {
            super(Data.TYPE_INT);
            this.value = i;
        }

        public static Integer createFromJSON(JSONObject jSONObject) {
            d.a("o", jSONObject);
            return new Integer(jSONObject.getInt("value"));
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!Data.super.equals(obj)) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            return this.value == ((Integer) obj).value;
        }

        public final int hashCode() {
            return (Data.super.hashCode() * 31) + this.value;
        }

        /* access modifiers changed from: protected */
        public final void putInJSON(JSONObject jSONObject) {
            jSONObject.put("value", this.value);
        }

        public final String toString(String str) {
            return String.valueOf(this.value);
        }
    }

    public final class Null extends Data {
        public Null() {
            super(5);
        }

        /* access modifiers changed from: protected */
        public final void putInJSON(JSONObject jSONObject) {
        }

        public final String toString(String str) {
            return "[null]";
        }
    }

    public final class Sequence extends Data {

        /* renamed from: a reason: collision with root package name */
        private final List f1515a;

        public Sequence() {
            this(null);
        }

        public Sequence(List list) {
            super(16);
            this.f1515a = new ArrayList();
            if (list != null) {
                this.f1515a.addAll(list);
            }
        }

        public static Sequence createFromJSON(JSONObject jSONObject) {
            d.a("o", jSONObject);
            JSONArray jSONArray = jSONObject.getJSONArray("value");
            ArrayList arrayList = new ArrayList(jSONArray.length());
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(Data.createFromJSON(jSONArray.getJSONObject(i)));
            }
            return new Sequence(arrayList);
        }

        public final void add(int i) {
            this.f1515a.add(new Integer(i));
        }

        public final void add(Data data) {
            this.f1515a.add(data);
        }

        public final void add(String str) {
            this.f1515a.add(new String(str));
        }

        public final void add(byte[] bArr) {
            this.f1515a.add(new Bytes(bArr));
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!Data.super.equals(obj)) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Sequence sequence = (Sequence) obj;
            return this.f1515a == null ? sequence.f1515a == null : this.f1515a.equals(sequence.f1515a);
        }

        public final Data get(int i) {
            return (Data) this.f1515a.get(i);
        }

        public final Bytes getBytes(int i) {
            Data data = (Data) this.f1515a.get(i);
            if (data.f1512a == 4) {
                return (Bytes) data;
            }
            return null;
        }

        public final Dictionary getDictionary(int i) {
            Data data = (Data) this.f1515a.get(i);
            if (data.f1512a == 224) {
                return (Dictionary) data;
            }
            return null;
        }

        public final Integer getInt(int i) {
            Data data = (Data) this.f1515a.get(i);
            if (data.f1512a == 192) {
                return (Integer) data;
            }
            return null;
        }

        public final Sequence getSequence(int i) {
            Data data = (Data) this.f1515a.get(i);
            if (data.f1512a == 16) {
                return (Sequence) data;
            }
            return null;
        }

        public final String getString(int i) {
            Data data = (Data) this.f1515a.get(i);
            if (data.f1512a == 193) {
                return (String) data;
            }
            return null;
        }

        public final List getValues() {
            return this.f1515a;
        }

        public final int hashCode() {
            return (this.f1515a == null ? 0 : this.f1515a.hashCode()) + (Data.super.hashCode() * 31);
        }

        /* access modifiers changed from: protected */
        public final void putInJSON(JSONObject jSONObject) {
            JSONArray jSONArray = new JSONArray();
            for (Data json : this.f1515a) {
                jSONArray.put(json.toJSON());
            }
            jSONObject.put("value", jSONArray);
        }

        public final int size() {
            return this.f1515a.size();
        }

        public final String toString(String str) {
            StringBuilder sb = new StringBuilder();
            sb.append("[\n");
            String str2 = str + "  ";
            for (Data data : this.f1515a) {
                sb.append(str2);
                sb.append(data.toString(str2));
                sb.append(",\n");
            }
            sb.append(str);
            sb.append("]");
            return sb.toString();
        }
    }

    public final class String extends Data {
        public String value;

        public String(String str) {
            super(193);
            d.a("value", (Object) str);
            this.value = str;
        }

        public static String createFromJSON(JSONObject jSONObject) {
            d.a("o", jSONObject);
            return new String(jSONObject.getString("value"));
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!Data.super.equals(obj)) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            String string = (String) obj;
            return this.value == null ? string.value == null : this.value.equals(string.value);
        }

        public final int hashCode() {
            return (this.value == null ? 0 : this.value.hashCode()) + (Data.super.hashCode() * 31);
        }

        /* access modifiers changed from: protected */
        public final void putInJSON(JSONObject jSONObject) {
            jSONObject.put("value", this.value);
        }

        public final String toString(String str) {
            return this.value;
        }
    }

    Data(short s) {
        this.f1512a = s;
    }

    public static Data createFromJSON(JSONObject jSONObject) {
        d.a("json", jSONObject);
        switch (jSONObject.getInt("type")) {
            case 4:
                return Bytes.createFromJSON(jSONObject);
            case 16:
                return Sequence.createFromJSON(jSONObject);
            case Frequencies.FREQ_22KHZ /*22*/:
            case 193:
                return String.createFromJSON(jSONObject);
            case 192:
                return Integer.createFromJSON(jSONObject);
            case 224:
                return Dictionary.createFromJSON(jSONObject);
            default:
                return new Null();
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.f1512a == ((Data) obj).f1512a;
    }

    public final short getType() {
        return this.f1512a;
    }

    public int hashCode() {
        return this.f1512a + 31;
    }

    /* access modifiers changed from: protected */
    public abstract void putInJSON(JSONObject jSONObject);

    public JSONObject toJSON() {
        JSONObject jSONObject = new JSONObject();
        try {
            putInJSON(jSONObject);
            jSONObject.put("type", this.f1512a);
        } catch (JSONException e2) {
            Logger.error(this, "Error adding to JSON", e2);
        }
        return jSONObject;
    }

    public final String toString() {
        return toString("");
    }

    /* access modifiers changed from: protected */
    public abstract String toString(String str);
}
