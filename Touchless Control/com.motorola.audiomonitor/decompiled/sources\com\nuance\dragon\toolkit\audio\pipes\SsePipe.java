package com.nuance.dragon.toolkit.audio.pipes;

import android.content.Context;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.MultiChannelAudioChunk;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.sse.b;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.Hashtable;

public class SsePipe extends ConverterPipe {

    /* renamed from: a reason: collision with root package name */
    private final b f1196a;

    /* renamed from: b reason: collision with root package name */
    private boolean f1197b;

    /* renamed from: c reason: collision with root package name */
    private FileManager f1198c;

    /* renamed from: d reason: collision with root package name */
    private Hashtable f1199d;

    /* renamed from: e reason: collision with root package name */
    private SseTuningType f1200e;

    public SsePipe(Context context) {
        this(context, null, null, SseTuningType.ECHO_AND_NOISE);
    }

    public SsePipe(Context context, FileManager fileManager, Hashtable hashtable) {
        this(context, fileManager, hashtable, SseTuningType.ECHO_AND_NOISE);
    }

    public SsePipe(Context context, FileManager fileManager, Hashtable hashtable, SseTuningType sseTuningType) {
        super(0);
        d.a("context", (Object) context.getApplicationContext());
        this.f1196a = new b(context);
        this.f1198c = fileManager;
        this.f1199d = hashtable;
        this.f1200e = sseTuningType;
    }

    public static int getRequiredFrameSize() {
        return 10;
    }

    /* access modifiers changed from: protected */
    public void cleanup() {
        if (this.f1197b) {
            this.f1196a.a();
        }
        this.f1197b = false;
    }

    /* access modifiers changed from: protected */
    public AudioChunk[] convert(MultiChannelAudioChunk multiChannelAudioChunk) {
        AudioChunk a2 = this.f1196a.a(multiChannelAudioChunk);
        if (a2 != null) {
            return new AudioChunk[]{a2};
        }
        return new AudioChunk[]{multiChannelAudioChunk.getSingleChannelChunk(0)};
    }

    public AudioType getAudioType() {
        return getAudioTypeFromSource();
    }

    /* access modifiers changed from: protected */
    public void initialize(AudioType audioType) {
        if (!this.f1197b) {
            if (!this.f1196a.a(audioType, this.f1198c, this.f1199d != null ? (String) this.f1199d.get(audioType) : null, this.f1200e)) {
                Logger.error(this, "Failure initializing SSE");
            }
            this.f1197b = true;
        }
    }
}
