package com.nuance.dragon.toolkit.audio.pipes;

public enum SseTuningType {
    NONE,
    ECHO_CANCEL,
    NOISE_SUPPRESS,
    BEAM_FORMING,
    ECHO_AND_NOISE,
    ECHO_AND_BEAM,
    NOISE_AND_BEAM,
    ECHO_NOISE_BEAM
}
