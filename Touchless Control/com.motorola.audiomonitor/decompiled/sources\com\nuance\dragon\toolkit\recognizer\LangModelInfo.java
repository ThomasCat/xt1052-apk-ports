package com.nuance.dragon.toolkit.recognizer;

public final class LangModelInfo {
    public final int frequencyHz;
    public final NMTLanguage language;

    public LangModelInfo(NMTLanguage nMTLanguage, int i) {
        this.language = nMTLanguage;
        this.frequencyHz = i;
    }

    public final String toString() {
        return this.language.toString() + ", " + this.frequencyHz + "Hz";
    }
}
