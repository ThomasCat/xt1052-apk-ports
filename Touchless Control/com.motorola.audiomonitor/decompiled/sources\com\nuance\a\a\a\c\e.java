package com.nuance.a.a.a.c;

import com.nuance.a.a.a.a.b.a.c;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public final class e extends c {
    /* access modifiers changed from: protected */
    public final byte[] a(byte[] bArr, byte[] bArr2) {
        if (bArr == null || bArr2 == null) {
            return new byte[0];
        }
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "HmacSHA1");
            Mac instance = Mac.getInstance("HmacSHA1");
            instance.init(secretKeySpec);
            return instance.doFinal(bArr2);
        } catch (Exception e2) {
            return new byte[0];
        }
    }
}
