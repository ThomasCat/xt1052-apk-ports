package com.nuance.dragon.toolkit.speechkit;

import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.cloudservices.CloudConfig;
import com.nuance.dragon.toolkit.cloudservices.SSLConfig;

public final class SKCloudConfig extends CloudConfig {
    public SKCloudConfig(String str, int i, String str2, byte[] bArr, boolean z) {
        super(str, i, z ? new SSLConfig(true, null, null) : null, str2, bArr, AudioType.SPEEX_WB, AudioType.SPEEX_WB);
    }
}
