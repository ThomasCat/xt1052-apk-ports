package com.nuance.dragon.toolkit.util.internal;

import android.os.Handler;
import android.os.Message;
import java.util.ArrayList;
import java.util.Iterator;

public final class b extends Handler {

    /* renamed from: a reason: collision with root package name */
    private Handler f2123a;

    /* renamed from: b reason: collision with root package name */
    private final ArrayList f2124b = new ArrayList();

    final class a {

        /* renamed from: a reason: collision with root package name */
        final Message f2125a;

        /* renamed from: b reason: collision with root package name */
        final long f2126b;

        a(Message message, long j) {
            this.f2125a = message;
            this.f2126b = j;
        }
    }

    public final synchronized void a() {
        this.f2123a = new Handler();
        if (this.f2124b.size() > 0) {
            new StringBuilder("Attached handler to current thread, sending ").append(this.f2124b.size()).append(" queued messages");
            Iterator it = this.f2124b.iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                this.f2123a.sendMessageAtTime(aVar.f2125a, aVar.f2126b);
            }
            this.f2124b.clear();
        }
    }

    public final synchronized boolean sendMessageAtTime(Message message, long j) {
        boolean z = true;
        synchronized (this) {
            if (this.f2123a == null) {
                this.f2124b.add(new a(message, j));
            } else if (this.f2123a.getLooper().getThread().isAlive()) {
                z = this.f2123a.sendMessageAtTime(message, j);
            } else {
                new StringBuilder("this msg [").append(message).append("] will be ignore since the Looper Thread has been terminated already!");
            }
        }
        return z;
    }
}
