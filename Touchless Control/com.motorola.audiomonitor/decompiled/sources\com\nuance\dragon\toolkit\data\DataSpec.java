package com.nuance.dragon.toolkit.data;

import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.data.Data.Sequence;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.a.b;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONObject;

public class DataSpec implements JSONCompliant {

    /* renamed from: a reason: collision with root package name */
    private final short f1516a;

    /* renamed from: b reason: collision with root package name */
    private final boolean f1517b;

    /* renamed from: c reason: collision with root package name */
    private final String f1518c;

    /* renamed from: d reason: collision with root package name */
    private DataSpec f1519d;

    /* renamed from: e reason: collision with root package name */
    private Map f1520e;

    public DataSpec(short s, String str, boolean z) {
        this.f1516a = s;
        this.f1518c = str;
        this.f1517b = z;
        if (s == 224) {
            this.f1520e = new HashMap();
        } else if (s == 16) {
            this.f1519d = new DataSpec(5, null, false);
        }
    }

    private Data a(Data data) {
        Dictionary dictionary;
        Dictionary dictionary2;
        if (data == null || data.getType() != this.f1516a) {
            return null;
        }
        if (this.f1516a == 224) {
            Map map = this.f1520e;
            if (this.f1517b) {
                Dictionary dictionary3 = new Dictionary();
                dictionary = dictionary3;
                dictionary2 = dictionary3;
            } else {
                dictionary = null;
                dictionary2 = null;
            }
            Dictionary dictionary4 = (Dictionary) data;
            for (Entry entry : this.f1520e.entrySet()) {
                String str = (String) entry.getKey();
                DataSpec dataSpec = (DataSpec) entry.getValue();
                Data data2 = dictionary4.get(str);
                if (data2 != null) {
                    Data a2 = dataSpec.a(data2);
                    if (a2 == null) {
                        continue;
                    } else if (dictionary == null) {
                        return a2;
                    } else {
                        String str2 = dataSpec.f1518c == null ? str : dataSpec.f1518c;
                        Data data3 = dictionary.get(str2);
                        if (data3 == null) {
                            dictionary.put(str2, a2);
                        } else {
                            new StringBuilder("Attempting to merge into '").append(str2).append("'");
                            if (a2.getType() == 16 && data3.getType() == 16) {
                                Sequence sequence = (Sequence) data3;
                                Sequence sequence2 = (Sequence) a2;
                                if (sequence.size() == sequence2.size()) {
                                    int size = sequence.size();
                                    for (int i = 0; i < size; i++) {
                                        Data data4 = sequence2.get(i);
                                        Data data5 = sequence.get(i);
                                        if (data4.getType() == 224 && data5.getType() == 224) {
                                            ((Dictionary) data5).putAll((Dictionary) data4);
                                        } else {
                                            Logger.error(this, "Unable to merge sequence elements of '" + str2 + "' due to non-dictionary types");
                                        }
                                    }
                                }
                            } else if (a2.getType() == 224 && data3.getType() == 224) {
                                ((Dictionary) data3).putAll((Dictionary) a2);
                            } else {
                                Logger.error(this, "Unable to merge multiple data with key '" + str2 + "' due to different types");
                            }
                        }
                    }
                }
            }
            return dictionary2;
        } else if (this.f1516a == 16) {
            DataSpec dataSpec2 = this.f1519d;
            if (!this.f1517b) {
                return null;
            }
            Sequence sequence3 = new Sequence();
            for (Data a3 : ((Sequence) data).getValues()) {
                Data a4 = this.f1519d.a(a3);
                if (a4 != null) {
                    if (this.f1519d.f1518c == null) {
                        sequence3.add(a4);
                    } else {
                        Dictionary dictionary5 = new Dictionary();
                        dictionary5.put(this.f1519d.f1518c, a4);
                        sequence3.add((Data) dictionary5);
                    }
                }
            }
            return sequence3;
        } else if (this.f1517b) {
            return data;
        } else {
            return null;
        }
    }

    public static DataSpec createFromJSON(JSONObject jSONObject) {
        short s = (short) jSONObject.getInt("type");
        DataSpec dataSpec = new DataSpec(s, jSONObject.optString("alias"), true);
        if (s == 16) {
            dataSpec.setSequenceContent(createFromJSON(jSONObject.getJSONObject("content")));
        } else if (s == 224) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("content");
            Iterator keys = jSONObject2.keys();
            while (keys.hasNext()) {
                String obj = keys.next().toString();
                dataSpec.addDictContent(obj, createFromJSON(jSONObject2.getJSONObject(obj).getJSONObject("data")));
            }
        }
        return dataSpec;
    }

    public DataSpec addDictContent(String str, short s, String str2) {
        DataSpec dataSpec = new DataSpec(s, str2, true);
        addDictContent(str, dataSpec);
        return dataSpec;
    }

    public void addDictContent(String str, DataSpec dataSpec) {
        short s = this.f1516a;
        this.f1520e.put(str, dataSpec);
    }

    public DataSpec addDictContentToDiscard(String str, short s) {
        DataSpec dataSpec = new DataSpec(s, null, false);
        addDictContent(str, dataSpec);
        return dataSpec;
    }

    public short getType() {
        return this.f1516a;
    }

    public Data process(Data data) {
        Data a2 = a(data);
        if (a2 == null || this.f1518c == null) {
            return a2;
        }
        Dictionary dictionary = new Dictionary();
        dictionary.put(this.f1518c, a2);
        return dictionary;
    }

    public DataSpec setSequenceContent(short s, String str) {
        DataSpec dataSpec = new DataSpec(s, str, true);
        setSequenceContent(dataSpec);
        return dataSpec;
    }

    public void setSequenceContent(DataSpec dataSpec) {
        short s = this.f1516a;
        this.f1519d = dataSpec;
    }

    public JSONObject toJSON() {
        b bVar = new b();
        bVar.a("type", (Object) Integer.valueOf(this.f1516a));
        if (this.f1518c != null) {
            bVar.a("alias", (Object) this.f1518c);
        }
        if (this.f1516a == 16) {
            bVar.a("content", (Object) this.f1519d.toJSON());
        } else if (this.f1516a == 224) {
            b bVar2 = new b();
            for (Entry entry : this.f1520e.entrySet()) {
                b bVar3 = new b();
                bVar3.a("data", (Object) ((DataSpec) entry.getValue()).toJSON());
                bVar2.a((String) entry.getKey(), (Object) bVar3);
            }
            bVar.a("content", (Object) bVar2);
        }
        return bVar;
    }
}
