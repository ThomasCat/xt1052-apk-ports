package a;

import b.b;
import com.motorola.audiomonitor.actions.utils.ac;
import com.motorola.audiomonitor.actions.utils.ad;
import com.motorola.audiomonitor.actions.utils.ai;
import com.motorola.audiomonitor.actions.utils.k;
import com.motorola.audiomonitor.actions.utils.v;
import com.motorola.audiomonitor.pluginservice.a;
import java.util.List;

public final class e {

    /* renamed from: a reason: collision with root package name */
    private final List f25a;

    /* renamed from: b reason: collision with root package name */
    private final v f26b;

    /* renamed from: c reason: collision with root package name */
    private final b f27c;

    /* renamed from: d reason: collision with root package name */
    private final List f28d;

    /* renamed from: e reason: collision with root package name */
    private final a f29e;

    public e(List list, b bVar, v vVar, List list2, a aVar) {
        this.f25a = list;
        this.f26b = vVar;
        this.f27c = bVar;
        this.f28d = list2;
        this.f29e = aVar;
    }

    public ad a(ac acVar, long j, k kVar, List list) {
        return a(acVar, j, kVar, list, null);
    }

    public ad a(ac acVar, long j, k kVar, List list, ai aiVar) {
        return this.f26b.a(acVar, j, kVar, list, aiVar);
    }

    public List a() {
        return this.f25a;
    }

    public b b() {
        return this.f27c;
    }

    public List c() {
        return this.f28d;
    }

    public int d() {
        return this.f26b.d();
    }
}
