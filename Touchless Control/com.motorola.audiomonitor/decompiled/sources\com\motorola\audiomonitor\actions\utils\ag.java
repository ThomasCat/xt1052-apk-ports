package com.motorola.audiomonitor.actions.utils;

import android.speech.tts.TextToSpeech.OnInitListener;

class ag implements OnInitListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ ae f196a;

    ag(ae aeVar) {
        this.f196a = aeVar;
    }

    public void onInit(int i) {
        synchronized (this.f196a.f190d) {
            if (this.f196a.f192f != null) {
                this.f196a.i = i == 0;
                if (this.f196a.i) {
                    this.f196a.i = this.f196a.h();
                }
            }
            this.f196a.f190d.notify();
        }
    }
}
