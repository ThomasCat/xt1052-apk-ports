package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.dragon.toolkit.util.Logger;

final class g extends f {
    g(d dVar, Transaction transaction) {
        super(dVar, false, true, transaction);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        super.a();
        if (this.f1464a != null) {
            this.f1464a.b();
        }
        a g2 = this.f1465b.g();
        if (g2 != null) {
            g2.b(this.f1465b);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void e() {
        Logger.warn(this, "cancel() method called in finished state");
    }
}
