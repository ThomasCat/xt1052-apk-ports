package com.nuance.dragon.toolkit.audio.pipes;

import android.content.Context;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.sse.b;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.Hashtable;

public class NoiseSuppressionPipe extends ConverterPipe {

    /* renamed from: a reason: collision with root package name */
    private final b f1169a;

    /* renamed from: b reason: collision with root package name */
    private boolean f1170b;

    /* renamed from: c reason: collision with root package name */
    private FileManager f1171c;

    /* renamed from: d reason: collision with root package name */
    private Hashtable f1172d;

    /* renamed from: e reason: collision with root package name */
    private SseTuningType f1173e;

    public NoiseSuppressionPipe(Context context) {
        this(context, null, null, SseTuningType.ECHO_AND_NOISE);
    }

    public NoiseSuppressionPipe(Context context, FileManager fileManager, Hashtable hashtable) {
        this(context, fileManager, hashtable, SseTuningType.ECHO_AND_NOISE);
    }

    public NoiseSuppressionPipe(Context context, FileManager fileManager, Hashtable hashtable, SseTuningType sseTuningType) {
        super(0);
        d.a("context", (Object) context.getApplicationContext());
        this.f1169a = new b(context);
        this.f1171c = fileManager;
        this.f1172d = hashtable;
        this.f1173e = sseTuningType;
    }

    public static int getRequiredFrameSize() {
        return 10;
    }

    /* access modifiers changed from: protected */
    public void cleanup() {
        if (this.f1170b) {
            this.f1169a.a();
        }
        this.f1170b = false;
    }

    /* access modifiers changed from: protected */
    public AudioChunk[] convert(AudioChunk audioChunk) {
        AudioChunk a2 = this.f1169a.a(audioChunk);
        if (a2 != null) {
            return new AudioChunk[]{a2};
        }
        return new AudioChunk[]{audioChunk};
    }

    public AudioType getAudioType() {
        return getAudioTypeFromSource();
    }

    /* access modifiers changed from: protected */
    public void initialize(AudioType audioType) {
        if (!this.f1170b) {
            if (!this.f1169a.a(audioType, this.f1171c, this.f1172d != null ? (String) this.f1172d.get(audioType) : null, this.f1173e)) {
                Logger.error(this, "Failure initializing SSE");
            }
            this.f1170b = true;
        }
    }
}
