package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.dragon.toolkit.util.internal.d;

public class BytesParam extends Param {

    /* renamed from: c reason: collision with root package name */
    private final byte[] f1339c;

    public BytesParam(String str, byte[] bArr) {
        super(5, str);
        d.a("data", (Object) bArr);
        this.f1339c = bArr;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(d dVar, a aVar) {
        dVar.a(this.f1355a, this.f1339c);
        return true;
    }
}
