package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.dragon.toolkit.data.Data.Dictionary;

public class SeqStartParam extends DictionaryParam {
    public SeqStartParam(String str, Dictionary dictionary) {
        super(8, str, dictionary);
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(d dVar, a aVar) {
        dVar.d(this.f1355a, this._dict);
        return true;
    }
}
