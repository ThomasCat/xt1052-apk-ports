package com.motorola.audiomonitor.actions.utils;

import android.content.Context;
import android.media.AudioTrack;
import android.util.Log;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.a;
import h.k;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ak {

    /* renamed from: a reason: collision with root package name */
    private static final String f199a = ("AMonitor." + ak.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f200b = a.f160b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public static Object f201c = new Object();

    /* renamed from: d reason: collision with root package name */
    private static final Map f202d = new HashMap();

    /* renamed from: e reason: collision with root package name */
    private static final Map f203e = new HashMap();

    /* renamed from: f reason: collision with root package name */
    private float f204f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public boolean f205g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public AudioTrack f206h;
    private Runnable i;

    private ak(byte[] bArr, int i2, int i3) {
        this(bArr, i2, i3, 3);
    }

    private ak(byte[] bArr, int i2, int i3, int i4) {
        this.f204f = 0.0f;
        this.f205g = false;
        this.i = new al(this);
        this.f206h = new AudioTrack(i4, i3, i2 == 1 ? 4 : 12, 2, bArr.length, 0);
        this.f206h.write(bArr, 0, bArr.length);
        this.f205g = true;
        this.f204f = (((float) ((bArr.length / i2) / 2)) / ((float) i3)) * 1000.0f;
    }

    public static synchronized ak a(am amVar) {
        ak akVar;
        synchronized (ak.class) {
            akVar = !f203e.isEmpty() ? (ak) f203e.get(amVar) : (ak) f202d.get(amVar);
        }
        return akVar;
    }

    public static synchronized void a(Context context) {
        synchronized (ak.class) {
            try {
                f202d.put(am.PROCESS, new ak(k.a(context, (int) C0014R.raw.processing), 1, 44100));
                f202d.put(am.ERROR, new ak(k.a(context, (int) C0014R.raw.error), 1, 44100));
                f202d.put(am.NO_INPUT, new ak(k.a(context, (int) C0014R.raw.no_input), 1, 44100));
            } catch (IOException e2) {
                Log.e(f199a, "Failure to read necessary tones.");
            }
        }
        return;
    }

    public static synchronized void b(Context context) {
        synchronized (ak.class) {
            try {
                f203e.put(am.PROCESS, new ak(k.a(context, (int) C0014R.raw.processing), 1, 44100, 0));
                f203e.put(am.ERROR, new ak(k.a(context, (int) C0014R.raw.error), 1, 44100, 0));
                f203e.put(am.NO_INPUT, new ak(k.a(context, (int) C0014R.raw.no_input), 1, 44100, 0));
            } catch (IOException e2) {
                Log.e(f199a, "Failure to read necessary tones.");
            }
        }
        return;
    }

    public static synchronized void c() {
        synchronized (ak.class) {
            for (ak b2 : f202d.values()) {
                b2.b();
            }
            f202d.clear();
            d();
        }
    }

    public static synchronized void d() {
        synchronized (ak.class) {
            for (ak b2 : f203e.values()) {
                b2.b();
            }
            f203e.clear();
        }
    }

    public int a() {
        int audioSessionId = this.f206h.getAudioSessionId();
        new Thread(this.i).start();
        return audioSessionId;
    }

    public void b() {
        synchronized (f201c) {
            if (f200b) {
                Log.d(f199a, "TonePlayer Cleanup");
            }
            if (this.f206h != null) {
                int playState = this.f206h.getPlayState();
                if (playState == 2 || playState == 3) {
                    this.f206h.stop();
                }
                this.f206h.release();
                this.f206h = null;
                if (f200b) {
                    Log.d(f199a, "TonePlayer Clean up Done");
                }
            }
        }
    }

    public float e() {
        return this.f204f;
    }
}
