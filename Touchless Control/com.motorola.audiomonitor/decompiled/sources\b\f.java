package b;

public final class f implements Comparable {

    /* renamed from: a reason: collision with root package name */
    private final String f99a;

    /* renamed from: b reason: collision with root package name */
    private final float f100b;

    public f(float f2, String str) {
        if (str == null || str.length() == 0) {
            throw new AssertionError("word is " + (str == null ? "null" : "len " + str.length()));
        }
        this.f99a = str;
        this.f100b = f2;
    }

    /* renamed from: a */
    public int compareTo(f fVar) {
        if (this.f100b == fVar.b()) {
            return 0;
        }
        return this.f100b > fVar.b() ? 1 : -1;
    }

    public String a() {
        return this.f99a;
    }

    public float b() {
        return this.f100b;
    }

    public String toString() {
        return this.f99a + "[" + this.f100b + "]";
    }
}
