package com.nuance.dragon.toolkit.util.internal;

import android.os.AsyncTask;
import android.os.Handler;
import com.nuance.dragon.toolkit.util.Logger;
import java.util.LinkedList;

public final class a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final LinkedList f2113a = new LinkedList();
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Object f2114b = new Object();
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final Object f2115c = new Object();
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public C0013a f2116d;

    /* renamed from: com.nuance.dragon.toolkit.util.internal.a$a reason: collision with other inner class name */
    final class C0013a extends AsyncTask {
        private C0013a() {
        }

        /* synthetic */ C0013a(a aVar, byte b2) {
            this();
        }

        private Void a() {
            while (true) {
                synchronized (a.this.f2114b) {
                    if (a.this.f2116d != this) {
                        Logger.warn(a.this, "Task is not current, aborting");
                        break;
                    } else if (a.this.f2113a.isEmpty()) {
                        a.this.f2116d = null;
                        break;
                    } else {
                        Runnable runnable = (Runnable) a.this.f2113a.removeFirst();
                        if (isCancelled()) {
                            Logger.warn(a.this, "Task is canceled, aborting");
                            break;
                        }
                        synchronized (a.this.f2115c) {
                            runnable.run();
                        }
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            return a();
        }
    }

    public abstract class b {
        public abstract Object a();

        public abstract void a(Object obj);
    }

    public final void a() {
        synchronized (this.f2114b) {
            this.f2113a.clear();
            if (this.f2116d != null) {
                this.f2116d.cancel(false);
                this.f2116d = null;
            }
        }
    }

    public final void a(final b bVar) {
        final Handler handler = new Handler();
        synchronized (this.f2114b) {
            this.f2113a.add(new Runnable() {
                public final void run() {
                    b bVar = bVar;
                    handler.post(new Runnable(bVar.a()) {

                        /* renamed from: a reason: collision with root package name */
                        final /* synthetic */ Object f2121a;

                        {
                            this.f2121a = r2;
                        }

                        public final void run() {
                            b.this.a(this.f2121a);
                        }
                    });
                }
            });
            if (this.f2116d == null) {
                this.f2116d = new C0013a(this, 0);
                this.f2116d.execute(new Void[]{null});
            }
        }
    }

    public final void a(Runnable runnable) {
        synchronized (this.f2114b) {
            this.f2113a.add(runnable);
            if (this.f2116d == null) {
                this.f2116d = new C0013a(this, 0);
                this.f2116d.execute(new Void[]{null});
            }
        }
    }
}
