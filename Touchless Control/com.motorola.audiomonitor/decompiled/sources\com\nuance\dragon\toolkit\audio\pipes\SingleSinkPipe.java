package com.nuance.dragon.toolkit.audio.pipes;

import com.nuance.dragon.toolkit.audio.AbstractAudioChunk;
import com.nuance.dragon.toolkit.audio.AudioPipe;
import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.util.Logger;
import java.util.List;

public abstract class SingleSinkPipe extends AudioPipe {

    /* renamed from: a reason: collision with root package name */
    private AudioSink f1181a;

    /* renamed from: b reason: collision with root package name */
    private boolean f1182b;

    /* renamed from: c reason: collision with root package name */
    private boolean f1183c;

    /* access modifiers changed from: protected */
    public final void audioSinkConnected(AudioSink audioSink) {
        this.f1181a = audioSink;
        onSinkConnected(audioSink);
        if (audioSink == this.f1181a) {
            if (this.f1183c) {
                this.f1183c = false;
                audioSink.framesDropped(this);
            }
            if (audioSink == this.f1181a) {
                if (!isEmpty()) {
                    audioSink.chunksAvailable(this);
                }
                if (audioSink == this.f1181a && this.f1182b) {
                    audioSink.sourceClosed(this);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void audioSinkDisconnected(AudioSink audioSink) {
        if (audioSink == this.f1181a) {
            this.f1181a = null;
            onSinkDisconnected(audioSink);
            return;
        }
        Logger.warn(this, "Wrong sink disconnected");
    }

    public void connectAudioSource(AudioSource audioSource) {
        if (this.f1182b && audioSource.isActive()) {
            this.f1182b = false;
        }
        super.connectAudioSource(audioSource);
    }

    /* access modifiers changed from: protected */
    public void getAllAudioChunks(List list) {
        while (true) {
            AbstractAudioChunk audioChunk = getAudioChunk();
            if (audioChunk != null) {
                list.add(audioChunk);
            } else {
                return;
            }
        }
    }

    public final void getAllAudioChunksForSink(AudioSink audioSink, List list) {
        if (audioSink == this.f1181a) {
            getAllAudioChunks(list);
        }
    }

    /* access modifiers changed from: protected */
    public abstract AbstractAudioChunk getAudioChunk();

    public final AbstractAudioChunk getAudioChunkForSink(AudioSink audioSink) {
        if (audioSink == this.f1181a) {
            return getAudioChunk();
        }
        return null;
    }

    public final int getChunksAvailableForSink(AudioSink audioSink) {
        if (audioSink == this.f1181a) {
            return getChunksAvailable();
        }
        return 0;
    }

    public final boolean isEmptyForSink(AudioSink audioSink) {
        if (audioSink == this.f1181a) {
            return isEmpty();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void notifyChunksAvailable() {
        if (this.f1181a != null) {
            this.f1181a.chunksAvailable(this);
        }
    }

    /* access modifiers changed from: protected */
    public void notifyFramesDropped() {
        if (this.f1181a != null) {
            this.f1181a.framesDropped(this);
        } else {
            this.f1183c = true;
        }
    }

    /* access modifiers changed from: protected */
    public void notifySourceClosed() {
        if (this.f1182b) {
            return;
        }
        if (this.f1181a != null) {
            this.f1181a.sourceClosed(this);
        } else {
            this.f1182b = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onSinkConnected(AudioSink audioSink) {
    }

    /* access modifiers changed from: protected */
    public void onSinkDisconnected(AudioSink audioSink) {
    }
}
