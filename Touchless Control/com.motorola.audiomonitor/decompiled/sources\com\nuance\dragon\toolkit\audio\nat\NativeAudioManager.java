package com.nuance.dragon.toolkit.audio.nat;

import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.sinks.PlayerSink;
import com.nuance.dragon.toolkit.audio.sources.RecorderSource;
import com.nuance.dragon.toolkit.util.WorkerThread;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NativeAudioManager {

    /* renamed from: a reason: collision with root package name */
    private final WorkerThread f1095a = new WorkerThread();

    /* renamed from: b reason: collision with root package name */
    private final Handler f1096b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final NativeAudio f1097c = new NativeAudioImpl();

    /* renamed from: d reason: collision with root package name */
    private final List f1098d = new ArrayList();

    /* renamed from: e reason: collision with root package name */
    private final ArrayList f1099e = new ArrayList();

    public NativeAudioManager() {
        this.f1095a.start();
        this.f1096b = this.f1095a.getHandler();
        this.f1096b.post(new Runnable() {
            public final void run() {
                NativeAudioManager.this.f1097c.initialize();
            }
        });
    }

    public PlayerSink createPlayerSink(AudioType audioType) {
        a aVar = new a(audioType, this.f1097c, this.f1096b);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f1099e.size()) {
                if (((PlayerSink) ((WeakReference) this.f1099e.get(i2)).get()) == null) {
                    int i3 = i2 - 1;
                    this.f1099e.remove(i2);
                    i2 = i3;
                }
                i = i2 + 1;
            } else {
                this.f1099e.add(new WeakReference(aVar));
                return aVar;
            }
        }
    }

    public RecorderSource createRecorderSource(AudioType audioType) {
        b bVar = new b(AudioType.PCM_16k, this.f1097c, this.f1096b);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f1098d.size()) {
                if (((RecorderSource) ((WeakReference) this.f1098d.get(i2)).get()) == null) {
                    int i3 = i2 - 1;
                    this.f1098d.remove(i2);
                    i2 = i3;
                }
                i = i2 + 1;
            } else {
                this.f1098d.add(new WeakReference(bVar));
                return bVar;
            }
        }
    }

    public void release() {
        for (WeakReference weakReference : this.f1098d) {
            RecorderSource recorderSource = (RecorderSource) weakReference.get();
            if (recorderSource != null) {
                recorderSource.stopRecording();
            }
        }
        Iterator it = this.f1099e.iterator();
        while (it.hasNext()) {
            PlayerSink playerSink = (PlayerSink) ((WeakReference) it.next()).get();
            if (playerSink != null) {
                playerSink.stopPlaying();
            }
        }
        this.f1096b.post(new Runnable() {
            public final void run() {
                NativeAudioManager.this.f1097c.release();
            }
        });
        this.f1095a.stop();
    }
}
