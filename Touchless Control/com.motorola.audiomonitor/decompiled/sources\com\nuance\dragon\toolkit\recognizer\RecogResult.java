package com.nuance.dragon.toolkit.recognizer;

public class RecogResult {

    /* renamed from: a reason: collision with root package name */
    private NbestList f2001a;

    public RecogResult(NbestList nbestList) {
        this.f2001a = nbestList;
    }

    public int getChoiceCount() {
        return this.f2001a.size();
    }

    public NbestList getNbestList() {
        return this.f2001a;
    }

    public String getTopResult() {
        if (this.f2001a == null || this.f2001a.size() == 0) {
            return null;
        }
        return ((Entry) this.f2001a.get(0)).toString();
    }

    public int getTopResultConfidence() {
        return ((Entry) this.f2001a.get(0)).getConfidence();
    }
}
