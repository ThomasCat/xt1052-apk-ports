package com.motorola.audiomonitor.uis;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import com.motorola.audiomonitor.a;

public class BtStubActivity extends Activity {

    /* renamed from: a reason: collision with root package name */
    private static final String f621a = ("AMonitor." + BtStubActivity.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f622b = a.f160b;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (f622b) {
            Log.d(f621a, "on voice command from BT headset.");
        }
        e.a.f();
        finish();
    }
}
