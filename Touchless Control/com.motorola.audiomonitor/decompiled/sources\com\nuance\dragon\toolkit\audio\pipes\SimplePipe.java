package com.nuance.dragon.toolkit.audio.pipes;

import com.nuance.dragon.toolkit.audio.AbstractAudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.AudioType;

public class SimplePipe extends SingleSinkPipe {
    public void chunksAvailable(AudioSource audioSource, AudioSink audioSink) {
        notifyChunksAvailable();
    }

    public void framesDropped(AudioSource audioSource, AudioSink audioSink) {
        notifyFramesDropped();
    }

    /* access modifiers changed from: protected */
    public AbstractAudioChunk getAudioChunk() {
        return getAudioChunkFromSource();
    }

    public AudioType getAudioType() {
        return getAudioTypeFromSource();
    }

    public int getChunksAvailable() {
        return getChunksAvailableFromSource();
    }

    public boolean isActive() {
        AudioSource connectedSource = getConnectedSource();
        return connectedSource != null && connectedSource.isActive();
    }

    /* access modifiers changed from: protected */
    public void sourceClosed(AudioSource audioSource, AudioSink audioSink) {
        notifySourceClosed();
    }
}
