package com.nuance.dragon.toolkit.grammar;

import com.nuance.dragon.toolkit.data.Data;
import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.data.Data.Sequence;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import com.nuance.dragon.toolkit.util.internal.d;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import org.json.JSONObject;

public class Word implements JSONCompliant {

    /* renamed from: a reason: collision with root package name */
    private final boolean f1725a;

    /* renamed from: b reason: collision with root package name */
    private final String f1726b;

    /* renamed from: c reason: collision with root package name */
    private final String f1727c;

    /* renamed from: d reason: collision with root package name */
    private final String f1728d;

    /* renamed from: e reason: collision with root package name */
    private final LinkedHashMap f1729e;

    /* renamed from: f reason: collision with root package name */
    private final LinkedHashMap f1730f;

    Word(ObjectInputStream objectInputStream) {
        this.f1725a = objectInputStream.readBoolean();
        if (this.f1725a) {
            try {
                this.f1726b = null;
                this.f1727c = null;
                this.f1728d = objectInputStream.readBoolean() ? objectInputStream.readUTF() : null;
                this.f1729e = (LinkedHashMap) objectInputStream.readObject();
                this.f1730f = objectInputStream.readBoolean() ? (LinkedHashMap) objectInputStream.readObject() : null;
            } catch (ClassNotFoundException e2) {
                throw new IOException();
            }
        } else {
            this.f1726b = objectInputStream.readUTF();
            this.f1727c = objectInputStream.readBoolean() ? objectInputStream.readUTF() : null;
            this.f1728d = null;
            this.f1729e = null;
            this.f1730f = null;
        }
    }

    public Word(String str) {
        this(str, null);
    }

    public Word(String str, String str2) {
        boolean z = true;
        d.a("surfaceForm", (Object) str);
        d.a("surfaceForm", "less than 400 characters", str.length() <= 400);
        String str3 = "spokenForm";
        String str4 = "less than 400 characters";
        if (str2 != null && str2.length() > 400) {
            z = false;
        }
        d.a(str3, str4, z);
        this.f1725a = false;
        this.f1726b = str;
        this.f1727c = str2;
        this.f1728d = null;
        this.f1729e = null;
        this.f1730f = null;
    }

    public Word(LinkedHashMap linkedHashMap, LinkedHashMap linkedHashMap2, String str) {
        d.a("main", (Object) linkedHashMap);
        d.a("additional", "less than 2000 entries", linkedHashMap2 == null || linkedHashMap2.size() <= 2000);
        this.f1725a = true;
        this.f1726b = null;
        this.f1727c = null;
        this.f1728d = str;
        this.f1729e = linkedHashMap;
        this.f1730f = linkedHashMap2;
    }

    public static Word createFromJSON(JSONObject jSONObject) {
        return new Word(jSONObject.getString("surface"), jSONObject.optString("spoken", null));
    }

    /* access modifiers changed from: 0000 */
    public final void a(ObjectOutputStream objectOutputStream) {
        boolean z = true;
        objectOutputStream.writeBoolean(this.f1725a);
        if (this.f1725a) {
            objectOutputStream.writeBoolean(this.f1728d != null);
            if (this.f1728d != null) {
                objectOutputStream.writeUTF(this.f1728d);
            }
            objectOutputStream.writeObject(this.f1729e);
            if (this.f1730f == null) {
                z = false;
            }
            objectOutputStream.writeBoolean(z);
            if (this.f1730f != null) {
                objectOutputStream.writeObject(this.f1730f);
                return;
            }
            return;
        }
        objectOutputStream.writeUTF(this.f1726b);
        if (this.f1727c == null) {
            z = false;
        }
        objectOutputStream.writeBoolean(z);
        if (this.f1727c != null) {
            objectOutputStream.writeUTF(this.f1727c);
        }
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Word word = (Word) obj;
        if (this.f1730f == null) {
            if (word.f1730f != null) {
                return false;
            }
        } else if (!this.f1730f.equals(word.f1730f)) {
            return false;
        }
        if (this.f1725a != word.f1725a) {
            return false;
        }
        if (this.f1729e == null) {
            if (word.f1729e != null) {
                return false;
            }
        } else if (!this.f1729e.equals(word.f1729e)) {
            return false;
        }
        if (this.f1727c == null) {
            if (word.f1727c != null) {
                return false;
            }
        } else if (!this.f1727c.equals(word.f1727c)) {
            return false;
        }
        if (this.f1726b == null) {
            if (word.f1726b != null) {
                return false;
            }
        } else if (!this.f1726b.equals(word.f1726b)) {
            return false;
        }
        return this.f1728d == null ? word.f1728d == null : this.f1728d.equals(word.f1728d);
    }

    public Dictionary getCustomForm() {
        if (!this.f1725a) {
            return null;
        }
        Dictionary dictionary = new Dictionary();
        if (this.f1729e != null) {
            for (Entry entry : this.f1729e.entrySet()) {
                dictionary.put((String) entry.getKey(), (String) entry.getValue());
            }
        }
        if (this.f1730f != null) {
            Dictionary dictionary2 = new Dictionary();
            if (this.f1728d != null) {
                dictionary2.put("id", this.f1728d);
            }
            for (Entry entry2 : this.f1730f.entrySet()) {
                Sequence sequence = new Sequence();
                for (String add : (List) entry2.getValue()) {
                    sequence.add(add);
                }
                dictionary2.put((String) entry2.getKey(), (Data) sequence);
            }
            dictionary.put("additional_info", (Data) dictionary2);
        }
        return dictionary;
    }

    public String getCustomFormDigest() {
        String str = "";
        if (!this.f1725a) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        if (this.f1729e != null) {
            for (Entry entry : this.f1729e.entrySet()) {
                sb.append(((String) entry.getKey()) + ":" + ((String) entry.getValue()));
            }
        }
        if (this.f1730f != null) {
            if (this.f1728d != null) {
                sb.append(this.f1728d);
            }
            for (Entry entry2 : this.f1730f.entrySet()) {
                sb.append((String) entry2.getKey());
                for (String append : (List) entry2.getValue()) {
                    sb.append(append);
                }
            }
        }
        return sb.toString();
    }

    public String getSpokenForm() {
        return this.f1727c;
    }

    public String getSurfaceForm() {
        return this.f1726b;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f1726b == null ? 0 : this.f1726b.hashCode()) + (((this.f1727c == null ? 0 : this.f1727c.hashCode()) + (((this.f1729e == null ? 0 : this.f1729e.hashCode()) + (((this.f1725a ? 1231 : 1237) + (((this.f1730f == null ? 0 : this.f1730f.hashCode()) + 31) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.f1728d != null) {
            i = this.f1728d.hashCode();
        }
        return hashCode + i;
    }

    public JSONObject toJSON() {
        b bVar = new b();
        bVar.a("surface", (Object) this.f1726b);
        bVar.a("spoken", (Object) this.f1727c);
        return bVar;
    }

    public final String toString() {
        return "Word [_isCustomForm=" + this.f1725a + ", _surfaceForm=" + this.f1726b + ", _spokenForm=" + this.f1727c + ", _wordId=" + this.f1728d + ", _main=" + this.f1729e + ", _additional=" + this.f1730f + "]";
    }
}
