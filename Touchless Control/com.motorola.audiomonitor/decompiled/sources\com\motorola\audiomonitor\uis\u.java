package com.motorola.audiomonitor.uis;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.a;

public class u extends PreferenceFragment {

    /* renamed from: a reason: collision with root package name */
    private Dialog f758a;

    /* renamed from: b reason: collision with root package name */
    private Dialog f759b;

    private void b() {
        Preference findPreference = findPreference("app_version");
        if (findPreference != null) {
            findPreference.setSummary(a.p);
            findPreference.setEnabled(true);
            findPreference.setOnPreferenceClickListener(null);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        Builder builder = new Builder(getActivity());
        y yVar = new y(this);
        builder.setTitle(C0014R.string.lock_bypass_title).setMessage(C0014R.string.lock_bypass_warning).setPositiveButton(C0014R.string.dialog_ok, yVar).setNegativeButton(C0014R.string.dialog_cancel, yVar).setCancelable(false);
        this.f759b = builder.create();
        this.f759b.show();
    }

    public void a(boolean z) {
        boolean z2 = getPreferenceManager().getSharedPreferences().getBoolean("monitor_status", false);
        getPreferenceScreen().setEnabled(false);
        CheckBoxPreference checkBoxPreference = (CheckBoxPreference) findPreference("monitor_status");
        if (checkBoxPreference != null) {
            checkBoxPreference.setEnabled(true);
            checkBoxPreference.setChecked(z2);
        }
        Preference findPreference = findPreference("privacy_policy");
        if (findPreference != null) {
            findPreference.setEnabled(true);
        }
        Preference findPreference2 = findPreference("help");
        if (findPreference2 != null) {
            findPreference2.setEnabled(z2);
        }
        Preference findPreference3 = findPreference("personalization");
        if (findPreference3 != null) {
            findPreference3.setEnabled(z2);
        }
        CheckBoxPreference checkBoxPreference2 = (CheckBoxPreference) findPreference("disp_off_trig");
        if (checkBoxPreference2 != null) {
            checkBoxPreference2.setEnabled(z2);
            checkBoxPreference2.setChecked(getPreferenceManager().getSharedPreferences().getBoolean("disp_off_trig", a.l));
        }
        CheckBoxPreference checkBoxPreference3 = (CheckBoxPreference) findPreference("lock_bypass");
        if (checkBoxPreference3 != null) {
            checkBoxPreference3.setEnabled(z2);
            checkBoxPreference3.setChecked(getPreferenceManager().getSharedPreferences().getBoolean("lock_bypass", a.m));
        }
        ListPreference listPreference = (ListPreference) findPreference("call_confirmation");
        if (listPreference != null) {
            listPreference.setEnabled(z2);
            listPreference.setSummary(listPreference.getEntry());
        }
        b();
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (bundle != null && ((Bundle) bundle.getParcelable("dialogBundle")) != null) {
            a();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        addPreferencesFromResource(C0014R.xml.settings);
        a(false);
        Preference findPreference = findPreference("lock_bypass");
        if (findPreference != null) {
            findPreference.setOnPreferenceChangeListener(new v(this));
        }
        Preference findPreference2 = findPreference("personalization");
        if (findPreference2 != null) {
            findPreference2.setOnPreferenceClickListener(new w(this));
        }
        Preference findPreference3 = findPreference("privacy_policy");
        if (findPreference3 != null) {
            findPreference3.setOnPreferenceClickListener(new x(this));
        }
    }

    public void onPause() {
        super.onPause();
        if (this.f758a != null && this.f758a.isShowing()) {
            this.f758a.setOnDismissListener(null);
            this.f758a.dismiss();
            this.f758a = null;
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.f759b != null) {
            if (this.f759b.isShowing()) {
                bundle.putParcelable("dialogBundle", this.f759b.onSaveInstanceState());
                this.f759b.dismiss();
            }
            this.f759b = null;
        }
    }
}
