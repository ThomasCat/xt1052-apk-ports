package a;

import android.util.Log;

class k implements Runnable {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ i f43a;

    k(i iVar) {
        this.f43a = iVar;
    }

    public void run() {
        if (i.f35b) {
            Log.d(i.f34a, "Mic connection too long. ABORTING.");
        }
        if (this.f43a.f38e != null) {
            this.f43a.f38e.c();
            this.f43a.f38e.b();
        }
        if (this.f43a.n != null) {
            this.f43a.n.a();
        }
    }
}
