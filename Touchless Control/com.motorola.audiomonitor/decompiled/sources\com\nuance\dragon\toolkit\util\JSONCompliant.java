package com.nuance.dragon.toolkit.util;

import org.json.JSONObject;

public interface JSONCompliant {
    JSONObject toJSON();
}
