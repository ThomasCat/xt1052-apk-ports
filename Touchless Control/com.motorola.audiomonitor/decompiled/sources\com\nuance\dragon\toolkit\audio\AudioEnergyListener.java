package com.nuance.dragon.toolkit.audio;

public interface AudioEnergyListener {
    void onEnergyLevelAvaiable(float f2);
}
