package com.motorola.audiomonitor.actions.utils;

import android.content.Context;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;
import com.motorola.audiomonitor.a;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class ae {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f187a = ("AMonitor." + ae.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f188b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private final Context f189c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public final Object f190d = new Object();
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public final Handler f191e = new Handler();
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public TextToSpeech f192f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public int f193g;

    /* renamed from: h reason: collision with root package name */
    private List f194h = null;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public boolean j = false;
    /* access modifiers changed from: private */
    public boolean k = false;
    private boolean l = false;
    /* access modifiers changed from: private */
    public final Runnable m = new af(this);
    private ai n;
    private final OnInitListener o = new ag(this);
    private final UtteranceProgressListener p = new ah(this);

    public ae(Context context) {
        this.f189c = context;
    }

    private boolean a(Locale locale, String str) {
        int isLanguageAvailable = this.f192f.isLanguageAvailable(locale);
        if (isLanguageAvailable == 0 || isLanguageAvailable == 1 || isLanguageAvailable == 2) {
            return this.f192f.getFeatures(locale).contains(str);
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        if (i2 <= 100) {
            if (this.n != null) {
                this.n.a(this.f193g, i2);
            }
            if (i2 == this.f193g) {
                d();
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean c(int i2) {
        int i3;
        HashMap hashMap = new HashMap();
        hashMap.put("streamType", Integer.toString(((AudioManager) this.f189c.getSystemService("audio")).isBluetoothScoOn() ? 0 : j()));
        if (this.k || this.j || !i()) {
            hashMap.put("networkTts", "false");
            hashMap.put("embeddedTts", "true");
        } else {
            hashMap.put("networkTts", "true");
            hashMap.put("embeddedTts", "false");
            synchronized (this.m) {
                this.f191e.removeCallbacks(this.m);
                this.f191e.postDelayed(this.m, 2000);
            }
        }
        int i4 = i2;
        int i5 = -1;
        while (true) {
            if (i4 >= this.f194h.size()) {
                i3 = i5;
                break;
            }
            String str = (String) this.f194h.get(i4);
            if (i4 != i2) {
                hashMap.put("utteranceId", Integer.toString(i4 + 1 + 100));
                i3 = this.f192f != null ? this.f192f.playSilence(500, 1, hashMap) : -1;
                if (i3 == -1) {
                    break;
                }
            }
            hashMap.put("utteranceId", Integer.toString(i4 + 1));
            i3 = this.f192f != null ? this.f192f.speak(str, 1, hashMap) : -1;
            if (i3 == -1) {
                break;
            }
            i4++;
            i5 = i3;
        }
        return i3 == 0;
    }

    private void g() {
        synchronized (this.f190d) {
            this.f192f = new TextToSpeech(this.f189c, this.o);
            this.f192f.setOnUtteranceProgressListener(this.p);
            if (f188b) {
                Log.d(f187a, "Wait for TTS initialize");
            }
            try {
                this.f190d.wait(2000);
            } catch (InterruptedException e2) {
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean h() {
        boolean z = true;
        Locale locale = Locale.getDefault();
        if (a(locale, "networkTts")) {
            this.f192f.setLanguage(locale);
            if (f188b) {
                Log.d(f187a, "Set language to " + locale.getLanguage());
            }
        } else if (a(locale, "embeddedTts")) {
            this.f192f.setLanguage(locale);
            this.k = true;
            if (f188b) {
                Log.d(f187a, "Set language to " + locale.getLanguage() + " use local TTS only");
            }
        } else {
            z = false;
            if (f188b) {
                Log.d(f187a, locale.getLanguage() + " is not supported");
            }
        }
        return z;
    }

    private boolean i() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.f189c.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private static int j() {
        char c2 = 3;
        try {
            return AudioManager.class.getField("STREAM_TTS").getInt(null);
        } catch (IllegalAccessException e2) {
            Log.e(f187a, "IllegalAccessException: " + e2.toString());
            return c2;
        } catch (IllegalArgumentException e3) {
            Log.e(f187a, "IllegalArgumentException: " + e3.toString());
            return c2;
        } catch (NoSuchFieldException e4) {
            Log.e(f187a, "NoSuchFieldException: " + e4.toString());
            return c2;
        }
    }

    public void a() {
        this.k = true;
    }

    public void a(int i2) {
        a(this.f189c.getResources().getString(i2));
    }

    public void a(ai aiVar) {
        this.n = aiVar;
    }

    public void a(String str) {
        a(Arrays.asList(new String[]{str}));
    }

    public void a(List list) {
        boolean z = false;
        this.f193g = list.size();
        this.f194h = list;
        if (f188b) {
            Log.d(f187a, "speak total:" + this.f193g);
        }
        g();
        if (this.i) {
            z = c(0);
        }
        if (!z) {
            if (f188b) {
                Log.d(f187a, "initialization failure");
            }
            b(this.f193g);
        } else if (this.l) {
            if (f188b) {
                Log.d(f187a, "Wait for speak done");
            }
            synchronized (this.f190d) {
                try {
                    this.f190d.wait(60000);
                } catch (InterruptedException e2) {
                }
            }
        }
    }

    public void b() {
        this.l = true;
    }

    public void c() {
        if (this.i) {
            d();
        }
    }

    public void d() {
        synchronized (this.m) {
            this.f191e.removeCallbacksAndMessages(null);
        }
        this.k = false;
        this.n = null;
        synchronized (this.f190d) {
            if (this.f192f != null) {
                this.f192f.stop();
                this.f192f.shutdown();
                this.f192f = null;
            }
            this.i = false;
            this.f193g = 0;
            this.f194h = null;
            this.f190d.notify();
        }
        if (f188b) {
            Log.d(f187a, "clean up");
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        if (this.f192f != null) {
            Log.w(f187a, "finalize: this=" + this);
        }
    }
}
