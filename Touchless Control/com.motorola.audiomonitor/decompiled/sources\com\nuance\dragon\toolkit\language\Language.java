package com.nuance.dragon.toolkit.language;

import com.nuance.dragon.toolkit.elvis.ElvisLanguage;
import com.nuance.dragon.toolkit.recognizer.NMTLanguage;
import com.nuance.dragon.toolkit.util.internal.f;
import com.nuance.dragon.toolkit.vocalizer.VocalizerLanguage;
import java.util.Locale;

public class Language {

    /* renamed from: a reason: collision with root package name */
    private final ElvisLanguage f1874a;

    /* renamed from: b reason: collision with root package name */
    private final VocalizerLanguage f1875b;

    /* renamed from: c reason: collision with root package name */
    private final NMTLanguage f1876c;

    /* renamed from: d reason: collision with root package name */
    private final String f1877d;

    /* renamed from: e reason: collision with root package name */
    private final String f1878e;

    Language(ElvisLanguage elvisLanguage, VocalizerLanguage vocalizerLanguage, NMTLanguage nMTLanguage, String str, String str2) {
        this.f1874a = elvisLanguage;
        this.f1875b = vocalizerLanguage;
        this.f1876c = nMTLanguage;
        this.f1877d = str;
        this.f1878e = str2;
    }

    public static String getGrammarLanguage(Locale locale) {
        String str;
        if (locale == null) {
            return null;
        }
        String language = locale.getLanguage();
        String country = locale.getCountry();
        String str2 = "";
        if (language == null || language.length() != 2 || country == null || country.length() != 2) {
            return null;
        }
        if (language.equals("de")) {
            str = str2 + "deu";
        } else if (language.equals("en")) {
            str = str2 + "eng";
        } else if (language.equals("es")) {
            str = str2 + "spa";
        } else if (language.equals("fr")) {
            str = str2 + "fra";
        } else if (language.equals("it")) {
            str = str2 + "ita";
        } else if (language.equals("ja")) {
            str = str2 + "jpn";
        } else if (language.equals("ko")) {
            str = str2 + "kor";
        } else if (language.equals("zh")) {
            str = str2 + "cmn";
        } else if (!language.equals("tr")) {
            return null;
        } else {
            str = str2 + "tur";
        }
        String str3 = str + "-";
        if (country.equals("CA")) {
            return str3 + "CAN";
        }
        if (country.equals("CN")) {
            return str3 + "CHN";
        }
        if (country.equals("DE")) {
            return str3 + "DEU";
        }
        if (country.equals("FR")) {
            return str3 + "FRA";
        }
        if (country.equals("GB")) {
            return str3 + "GBR";
        }
        if (country.equals("IT")) {
            return str3 + "ITA";
        }
        if (country.equals("JP")) {
            return str3 + "JPN";
        }
        if (country.equals("KR")) {
            return str3 + "KOR";
        }
        if (country.equals("TW")) {
            return str3 + "TWN";
        }
        if (country.equals("US")) {
            return str3 + (language.equals("es") ? "XLA" : "USA");
        } else if (country.equals("TR")) {
            return str3 + "TUR";
        } else {
            return null;
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Language)) {
            return false;
        }
        Language language = (Language) obj;
        return this.f1877d.equals(language.f1877d) && f.a(this.f1874a, language.f1874a) && f.a(this.f1875b, language.f1875b) && f.a(this.f1876c, language.f1876c);
    }

    public String getCloudLanguage() {
        return this.f1877d;
    }

    public ElvisLanguage getElvisLanguage() {
        return this.f1874a;
    }

    public String getGrammarLanguage() {
        return this.f1877d;
    }

    public VocalizerLanguage getVocalizerLanguage() {
        return this.f1875b;
    }

    public NMTLanguage getVoconLanguage() {
        return this.f1876c;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (this.f1875b == null ? 0 : this.f1875b.hashCode()) + this.f1877d.hashCode() + (this.f1874a == null ? 0 : this.f1874a.hashCode());
        if (this.f1876c != null) {
            i = this.f1876c.hashCode();
        }
        return hashCode + i;
    }

    public final String toString() {
        return this.f1878e;
    }
}
