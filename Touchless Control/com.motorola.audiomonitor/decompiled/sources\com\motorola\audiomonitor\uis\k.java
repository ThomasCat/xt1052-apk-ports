package com.motorola.audiomonitor.uis;

import android.os.CountDownTimer;

class k extends CountDownTimer {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ AOVErrorActivity f711a;

    k(AOVErrorActivity aOVErrorActivity, long j, long j2) {
        this.f711a = aOVErrorActivity;
        super(j, j2);
    }

    public void onFinish() {
        this.f711a.f602c = null;
        if (!this.f711a.f600a) {
            this.f711a.f601b.c(100);
        }
        this.f711a.finish();
    }

    public void onTick(long j) {
        int i = (int) (((5000 - j) * 100) / 5000);
        if (!this.f711a.f600a) {
            this.f711a.f601b.c(i);
        }
    }
}
