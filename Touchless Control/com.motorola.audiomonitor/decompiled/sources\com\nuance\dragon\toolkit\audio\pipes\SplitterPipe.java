package com.nuance.dragon.toolkit.audio.pipes;

import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioPipe;
import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.MultiChannelAudioChunk;
import com.nuance.dragon.toolkit.audio.util.a;
import com.nuance.dragon.toolkit.audio.util.a.C0005a;
import com.nuance.dragon.toolkit.util.Logger;
import java.util.ArrayList;
import java.util.List;

public class SplitterPipe extends AudioPipe {

    /* renamed from: a reason: collision with root package name */
    private final List f1193a = new ArrayList();

    /* renamed from: b reason: collision with root package name */
    private final a f1194b = new a();

    /* renamed from: c reason: collision with root package name */
    private final List f1195c = new ArrayList();

    /* access modifiers changed from: protected */
    public void audioSinkConnected(AudioSink audioSink) {
        this.f1193a.add(audioSink);
        this.f1195c.add(this.f1194b.a());
        if (!isSourceActive()) {
            audioSink.sourceClosed(this);
        }
    }

    /* access modifiers changed from: protected */
    public void audioSinkDisconnected(AudioSink audioSink) {
        int indexOf = this.f1193a.indexOf(audioSink);
        if (indexOf >= 0) {
            this.f1193a.remove(indexOf);
            ((C0005a) this.f1195c.remove(indexOf)).c();
            return;
        }
        Logger.warn(this, "Couldn't find audio sink to remove");
    }

    /* access modifiers changed from: protected */
    public void chunksAvailable(AudioSource audioSource, AudioSink audioSink) {
        int chunksAvailableForSink = audioSource.getChunksAvailableForSink(audioSink);
        if (chunksAvailableForSink > 0) {
            while (true) {
                int i = chunksAvailableForSink - 1;
                if (chunksAvailableForSink <= 0) {
                    break;
                }
                this.f1194b.a((Object) (MultiChannelAudioChunk) audioSource.getAudioChunkForSink(audioSink));
                chunksAvailableForSink = i;
            }
            for (AudioSink chunksAvailable : this.f1193a) {
                chunksAvailable.chunksAvailable(this);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void framesDropped(AudioSource audioSource, AudioSink audioSink) {
        for (AudioSink framesDropped : this.f1193a) {
            framesDropped.framesDropped(this);
        }
    }

    public AudioChunk getAudioChunkForSink(AudioSink audioSink) {
        int indexOf = this.f1193a.indexOf(audioSink);
        if (indexOf != -1) {
            C0005a aVar = (C0005a) this.f1195c.get(indexOf);
            if (aVar.b() > 0) {
                MultiChannelAudioChunk multiChannelAudioChunk = (MultiChannelAudioChunk) aVar.a();
                if (multiChannelAudioChunk != null) {
                    return multiChannelAudioChunk.getSingleChannelChunk(indexOf);
                }
                Logger.error(this, "getAudioChunkForSink(): Expected non-null chunk");
            }
        } else {
            Logger.warn(this, "getAudioChunkForSink(): Can't find sink");
        }
        return null;
    }

    public AudioType getAudioType() {
        return getAudioTypeFromSource();
    }

    public int getChunksAvailable() {
        return 0;
    }

    public int getChunksAvailableForSink(AudioSink audioSink) {
        int indexOf = this.f1193a.indexOf(audioSink);
        if (indexOf != -1) {
            return ((C0005a) this.f1195c.get(indexOf)).b();
        }
        return 0;
    }

    public boolean isActive() {
        return isSourceActive();
    }

    /* access modifiers changed from: protected */
    public void sourceClosed(AudioSource audioSource, AudioSink audioSink) {
        for (int size = this.f1193a.size() - 1; size >= 0; size--) {
            ((AudioSink) this.f1193a.get(size)).sourceClosed(this);
        }
    }
}
