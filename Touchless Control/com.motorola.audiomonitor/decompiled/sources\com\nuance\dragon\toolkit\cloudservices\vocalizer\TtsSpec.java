package com.nuance.dragon.toolkit.cloudservices.vocalizer;

import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import com.nuance.dragon.toolkit.util.internal.d;
import org.json.JSONObject;

public class TtsSpec implements JSONCompliant {

    /* renamed from: a reason: collision with root package name */
    private final String f1507a;

    /* renamed from: b reason: collision with root package name */
    private final Dictionary f1508b;

    /* renamed from: c reason: collision with root package name */
    private final String f1509c;

    /* renamed from: d reason: collision with root package name */
    private final Dictionary f1510d;

    /* renamed from: e reason: collision with root package name */
    private final AudioType f1511e;

    public TtsSpec(String str, Dictionary dictionary, String str2, Dictionary dictionary2, AudioType audioType) {
        d.a("command", str);
        d.a("ttsParamName", str2);
        d.a("ttsParamData", (Object) dictionary2);
        d.a("ttsParamData", "not empty", dictionary2.getEntries().size() > 0);
        d.a("audioType", (Object) audioType);
        this.f1507a = str;
        this.f1508b = dictionary;
        this.f1509c = str2;
        this.f1510d = dictionary2;
        this.f1511e = audioType;
    }

    public static TtsSpec createFromJSON(JSONObject jSONObject) {
        return new TtsSpec(jSONObject.getString("cmd"), Dictionary.createFromJSON(jSONObject.getJSONObject("settings")), jSONObject.getString("pname"), Dictionary.createFromJSON(jSONObject.getJSONObject("pdata")), AudioType.fromJSON(jSONObject.getJSONObject("type")));
    }

    public AudioType getAudioType() {
        return this.f1511e;
    }

    public String getCommand() {
        return this.f1507a;
    }

    public Dictionary getSettings() {
        return this.f1508b;
    }

    public Dictionary getTtsParamData() {
        return this.f1510d;
    }

    public String getTtsParamName() {
        return this.f1509c;
    }

    public JSONObject toJSON() {
        b bVar = new b();
        bVar.a("cmd", (Object) this.f1507a);
        bVar.a("settings", (Object) this.f1508b.toJSON());
        bVar.a("pname", (Object) this.f1509c);
        bVar.a("pdata", (Object) this.f1510d.toJSON());
        bVar.a("type", (Object) this.f1511e.toJSON());
        return bVar;
    }
}
