package com.nuance.dragon.toolkit.elvis;

import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import org.json.JSONException;
import org.json.JSONObject;

public class ElvisError implements JSONCompliant {

    /* renamed from: a reason: collision with root package name */
    private final int f1536a;

    /* renamed from: b reason: collision with root package name */
    private final String f1537b;

    public class Reason {
        public static final int AUDIO_FRAMES_DROPPED = 1;
        public static final int CANCELED = 0;
        public static final int CANT_START = 3;
        public static final int CONSTRAINT_DEF_ERROR = 5;
        public static final int FILE_ERROR = 7;
        public static final int MISSING_WORDLIST = 8;
        public static final int NOT_INITIALIZED = 9;
        public static final int NO_RESULT = 2;
        public static final int TRANSITION_DEF_ERROR = 4;
        public static final int WORDSLOT_DEF_ERROR = 6;
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public static final String[] f1538a = {"Recognition canceled", "Audio frames dropped", "No recognition result", "Can't start", "Transition definition error", "Constraint definition error", "WordSlot definition error", "File error", "Missing wordlist", "Elvis is not initialized."};
    }

    ElvisError(int i) {
        this.f1536a = i;
        this.f1537b = Reason.f1538a[i];
    }

    public static ElvisError createFromJSON(JSONObject jSONObject) {
        int i = jSONObject.getInt("code");
        if (i >= 0 && i < Reason.f1538a.length) {
            return new ElvisError(i);
        }
        throw new JSONException("Invalid code");
    }

    public String getReason() {
        return this.f1537b;
    }

    public int getReasonCode() {
        return this.f1536a;
    }

    public JSONObject toJSON() {
        b bVar = new b();
        bVar.a("code", (Object) Integer.valueOf(this.f1536a));
        return bVar;
    }

    public final String toString() {
        return this.f1537b;
    }
}
