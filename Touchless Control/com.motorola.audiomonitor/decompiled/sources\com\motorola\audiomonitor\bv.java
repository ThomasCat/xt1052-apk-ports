package com.motorola.audiomonitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

class bv extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ bu f382a;

    bv(bu buVar) {
        this.f382a = buVar;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (!TextUtils.isEmpty(action)) {
            if (bu.f375b) {
                Log.d(bu.f374a, "action received " + action);
            }
            if (action.equals("android.intent.action.SCREEN_ON")) {
                synchronized (this.f382a.f381h) {
                    this.f382a.f381h.set(true);
                    this.f382a.f381h.notifyAll();
                }
            } else if (action.equals("android.intent.action.SCREEN_OFF")) {
                this.f382a.f381h.set(false);
            }
        }
    }
}
