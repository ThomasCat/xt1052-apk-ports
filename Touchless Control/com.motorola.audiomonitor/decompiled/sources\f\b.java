package f;

import a.p;
import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import b.c;
import b.d;
import b.e;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.actions.utils.f;
import com.motorola.audiomonitor.actions.utils.t;
import com.motorola.audiomonitor.pluginservice.InputPluginService;
import com.nuance.dragon.toolkit.elvis.Grammar;
import com.nuance.dragon.toolkit.recognition.CloudInterpreter;
import h.g;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class b implements t {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f2214a = ("AMonitor." + b.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f2215b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private i f2216c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public i f2217d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public final Handler f2218e = new Handler();

    /* renamed from: f reason: collision with root package name */
    private final Context f2219f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public final Object f2220g = new Object();
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public final Object f2221h = new Object();
    /* access modifiers changed from: private */
    public d i;
    private final Map j = new HashMap();
    private final List k = new ArrayList();
    private final String[] l;
    private final String m;
    private boolean n;
    private final int o;
    private final t p = new c(this);

    public b(Context context, int i2) {
        this.f2219f = context;
        Resources resources = this.f2219f.getResources();
        a("voicemail", resources.getStringArray(C0014R.array.commands_voicemail), false);
        a("navigate", resources.getStringArray(C0014R.array.commands_navigate_and_extra), true);
        a("intent", p.a(this.f2219f), false);
        a("google", resources.getStringArray(C0014R.array.commands_white_list), true);
        this.l = resources.getStringArray(C0014R.array.digitals_short_numbers);
        this.m = resources.getString(C0014R.string.voicemail_contact_name).toLowerCase(Locale.getDefault());
        this.o = i2;
        this.f2216c = b((List) null);
    }

    private c a(String str, String str2, int i2) {
        return new c(str, f.a(this.f2219f).a(str2), i2 * -1);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x01ad A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x01fc A[Catch:{ JSONException -> 0x00e0 }] */
    public void a(int i2, JSONObject jSONObject) {
        boolean z;
        boolean z2;
        String str;
        int i3;
        boolean z3;
        boolean z4 = false;
        int i4 = 0;
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject("nbest");
            int i5 = jSONObject.getInt("gate_confidence");
            JSONArray jSONArray = jSONObject2.getJSONArray("entries");
            int length = jSONArray.length();
            boolean z5 = i2 < 20;
            if (f2215b) {
                Log.d(f2214a, "nbest = " + length + " gate_confidence = " + i5 + " confidence = " + i2 + (z5 ? "  noise tolerance(20)" : ""));
            }
            int i6 = 0;
            String str2 = null;
            while (i6 < length) {
                JSONObject jSONObject3 = jSONArray.getJSONObject(i6);
                String string = jSONObject3.getString("constraint");
                int i7 = jSONObject3.getInt(CloudInterpreter.KEY_SCORE);
                JSONArray jSONArray2 = jSONObject3.getJSONArray("words");
                int length2 = jSONArray2.length();
                String str3 = i6 == 0 ? string : str2;
                if ("callConst".equals(string)) {
                    StringBuilder sb = new StringBuilder();
                    String str4 = null;
                    String str5 = null;
                    boolean z6 = false;
                    for (int i8 = 0; i8 < length2; i8++) {
                        JSONObject jSONObject4 = jSONArray2.getJSONObject(i8);
                        if ("callCmd".equals(jSONObject4.get("slot"))) {
                            str5 = jSONObject4.getString("phrase");
                        } else if ("name".equals(jSONObject4.get("slot"))) {
                            sb.append(jSONObject4.getString("phrase"));
                        } else {
                            if ("digit".equals(jSONObject4.get("slot"))) {
                                z6 = true;
                                sb.append(jSONObject4.getString("phrase"));
                            } else if ("location".equals(jSONObject4.get("slot"))) {
                                str4 = jSONObject4.getString("phrase");
                            }
                        }
                    }
                    if (f2215b) {
                        Log.d(f2214a, "[" + i7 + "] " + str5 + " " + (sb.length() != 0 ? sb.toString() : "empty") + (str4 != null ? " location:" + str4 : "") + (z6 ? "(digit)" : ""));
                    }
                    if (z4 || i5 >= 40) {
                        if (TextUtils.equals(str3, string)) {
                            if (z6) {
                                if (!z4) {
                                    if (f.a(this.f2219f, sb.toString(), this.l)) {
                                        this.i = new d("call");
                                        this.i.a("telephoneNumber", sb.toString());
                                    }
                                    z3 = true;
                                    i3 = i4;
                                    i4 = i3;
                                    z = z3;
                                    if (this.i != null) {
                                        this.i.a(CloudInterpreter.KEY_SCORE, Integer.toString(i7));
                                        this.i.a("confidence", Integer.toString(i2));
                                    }
                                }
                            } else if (!this.n || !TextUtils.equals(sb.toString().toLowerCase(Locale.getDefault()), this.m)) {
                                if (!z4) {
                                    if (sb.length() != 0) {
                                        this.i = new d("call");
                                        this.i.a(a(sb.toString(), str4, i7));
                                        a(str5);
                                    }
                                    z3 = true;
                                    i3 = i7;
                                    i4 = i3;
                                    z = z3;
                                    if (this.i != null) {
                                    }
                                } else if (this.i != null && TextUtils.equals(this.i.a("command"), "call") && sb.length() != 0 && ((long) (i7 - i4)) <= 10) {
                                    this.i.a(a(sb.toString(), str4, i7));
                                }
                            } else if (!z4) {
                                if (f2215b) {
                                    Log.d(f2214a, "Use system voicemail number");
                                }
                                this.i = new d("voicemail");
                                z3 = true;
                                i3 = i4;
                                i4 = i3;
                                z = z3;
                                if (this.i != null) {
                                }
                            }
                        }
                        i3 = i4;
                        z3 = z4;
                        i4 = i3;
                        z = z3;
                        if (this.i != null) {
                        }
                    } else {
                        if (f2215b) {
                            Log.d(f2214a, "Call command gate confidence threshold=40");
                        }
                        z = true;
                    }
                } else {
                    if ("localCommand".equals(string)) {
                        String str6 = null;
                        boolean z7 = false;
                        int i9 = 0;
                        while (i9 < length2) {
                            JSONObject jSONObject5 = jSONArray2.getJSONObject(i9);
                            if ("localCommands".equals(jSONObject5.get("slot"))) {
                                boolean z8 = z7;
                                str = jSONObject5.getString("phrase");
                                z2 = z8;
                            } else if ("garbage".equals(jSONObject5.get("slot"))) {
                                z2 = true;
                                str = str6;
                            } else {
                                z2 = z7;
                                str = str6;
                            }
                            i9++;
                            str6 = str;
                            z7 = z2;
                        }
                        if (f2215b) {
                            Log.d(f2214a, "[" + i7 + "] " + str6 + (z7 ? " + extra" : ""));
                        }
                        if (!z4) {
                            String str7 = (String) this.j.get(str6);
                            if ((!z7 || this.k.contains(str7)) && (i6 == 0 || (z5 && i6 <= 1))) {
                                if (str7 != null) {
                                    this.i = new d(str7);
                                    this.i.a("name", str6);
                                }
                                z4 = true;
                            } else if (!z5 || (z5 && i6 >= 1)) {
                                z4 = true;
                            }
                        }
                        z = z4;
                    } else {
                        if ("garbageConst".equals(string)) {
                            if (f2215b) {
                                Log.d(f2214a, "[" + i7 + "] garbage");
                            }
                            if (!z4 && (!z5 || (z5 && i6 >= 1))) {
                                z = true;
                            }
                        }
                        z = z4;
                    }
                    if (this.i != null) {
                    }
                }
                i6++;
                z4 = z;
                str2 = str3;
            }
        } catch (JSONException e2) {
        }
        g.a().b(jSONObject.toString());
        InputPluginService.b().a("nuance_results", jSONObject);
    }

    /* access modifiers changed from: private */
    public void a(i iVar, boolean z) {
        i iVar2 = null;
        if (iVar != null) {
            synchronized (this.f2220g) {
                if (iVar == this.f2217d) {
                    if (z) {
                        iVar2 = this.f2216c;
                        this.f2216c = this.f2217d;
                        this.f2217d = null;
                    } else {
                        this.f2217d.d();
                        this.f2217d = null;
                    }
                }
            }
            if (iVar2 != null) {
                iVar2.d();
            }
        }
    }

    private void a(String str) {
        this.i.a("CHECK_CALL_COMMAND", str);
    }

    private void a(String str, String[] strArr, boolean z) {
        if (str != null && strArr != null && strArr.length != 0) {
            for (String put : strArr) {
                this.j.put(put, str);
            }
            if (z) {
                this.k.add(str);
            }
        }
    }

    /* access modifiers changed from: private */
    public i b(List list) {
        ArrayList arrayList = new ArrayList(this.j.keySet());
        ArrayList arrayList2 = new ArrayList();
        for (String add : this.f2219f.getResources().getStringArray(C0014R.array.commands_call)) {
            arrayList2.add(add);
        }
        Grammar a2 = v.a(arrayList, arrayList2, list, Arrays.asList(f.a(this.f2219f).a()));
        if (f2215b) {
            Log.d(f2214a, "lang=" + v.a() + " commands=" + this.j);
        }
        return new i(this.o, this.p, a2);
    }

    public b.b a(b.a aVar) {
        synchronized (this.f2220g) {
            if (this.f2216c == null || !this.f2216c.a()) {
                throw new e("Not ready");
            }
            List a2 = aVar.a();
            synchronized (this.f2221h) {
                this.i = null;
                this.f2216c.a(a2);
                try {
                    this.f2221h.wait(3000);
                } catch (InterruptedException e2) {
                }
                if (this.i == null) {
                    if (f2215b) {
                        Log.d(f2214a, "Recognizer timeout");
                    }
                    this.f2216c.c();
                }
            }
        }
        if (this.i != null) {
            return this.i;
        }
        throw new e("No result found");
    }

    public void a() {
        synchronized (this.f2220g) {
            if (this.f2217d != null) {
                this.f2217d.d();
                this.f2217d = null;
            }
            if (this.f2216c != null) {
                this.f2216c.d();
                this.f2216c = null;
            }
        }
        if (f2215b) {
            Log.d(f2214a, "cleaned up");
        }
    }

    public void a(List list) {
        this.n = !list.contains(this.m);
        if (this.n) {
            list.add(this.m);
        }
        this.f2218e.post(new e(this, list));
    }
}
