package com.nuance.a.a.a.b.c.b.b;

import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.b.c.c.k;

public class u extends l implements k {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f880a = a.a(u.class);

    public u(byte[] bArr) {
        super(29189, bArr);
    }

    public final String g() {
        f880a.b((Object) "PDXQueryRetry.getName()");
        String f2 = f("name");
        return f2 != null ? f2 : "";
    }

    public final int h() {
        f880a.b((Object) "PDXQueryRetry.getCause()");
        try {
            return d("cause");
        } catch (Exception e2) {
            return 0;
        }
    }

    public final String i() {
        String str;
        f880a.b((Object) "PDXQueryRetry.getPrompt()");
        try {
            str = f("prompt");
        } catch (Exception e2) {
            str = "";
        }
        return str != null ? str : "";
    }
}
