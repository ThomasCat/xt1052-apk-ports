package com.nuance.a.a.a.b.c.c;

import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.b.b.c;
import java.util.Vector;

public class e {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f903a = a.a(e.class);

    private e() {
    }

    public static d a(com.nuance.a.a.a.b.c.a.a aVar, f fVar, Vector vector) {
        c cVar;
        if (f903a.b()) {
            f903a.b((Object) "createNMASResource");
        }
        if (aVar == null) {
            f903a.e("manager is null");
            throw new NullPointerException("manager can not be null!");
        } else if (fVar == null) {
            f903a.e("nmasListener is null");
            throw new NullPointerException("nmasListener can not be null!");
        } else {
            if (vector != null) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= vector.size()) {
                        break;
                    }
                    cVar = (c) vector.elementAt(i2);
                    if (cVar.d() == c.a.f828c || cVar.d() == c.a.f829d || cVar.d() == c.a.f830e || cVar.d() == c.a.f831f || cVar.d() == c.a.f832g || cVar.d() == c.a.f833h || cVar.d() == c.a.i || cVar.d() == c.a.j) {
                        f903a.e("IllegalArgumentException Parameter type: " + cVar.d() + " not allowed. ");
                    } else {
                        i = i2 + 1;
                    }
                }
                f903a.e("IllegalArgumentException Parameter type: " + cVar.d() + " not allowed. ");
                throw new IllegalArgumentException("Parameter type: " + cVar.d() + " not allowed. ");
            }
            return new com.nuance.a.a.a.b.c.b.b.a((com.nuance.a.a.a.b.c.b.a.a) aVar, fVar, vector);
        }
    }
}
