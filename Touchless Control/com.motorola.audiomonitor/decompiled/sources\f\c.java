package f;

import android.util.Log;
import b.d;
import org.json.JSONObject;

class c implements t {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ b f2222a;

    c(b bVar) {
        this.f2222a = bVar;
    }

    public void a() {
        if (b.f2215b) {
            Log.d(b.f2214a, "onStartOfSpeech");
        }
    }

    public void a(int i, JSONObject jSONObject) {
        if (b.f2215b) {
            Log.d(b.f2214a, "onComplete");
        }
        synchronized (this.f2222a.f2221h) {
            this.f2222a.a(i, jSONObject);
            if (this.f2222a.i == null) {
                this.f2222a.i = new d("google");
            }
            this.f2222a.f2221h.notify();
        }
    }

    public void a(i iVar, boolean z) {
        if (b.f2215b) {
            Log.d(b.f2214a, "onInitComplete");
        }
        this.f2222a.f2218e.post(new d(this, iVar, z));
    }

    public void a(String str) {
        Log.e(b.f2214a, "Error during audio parsing: " + str);
        synchronized (this.f2222a.f2221h) {
            this.f2222a.i = new d("google");
            this.f2222a.f2221h.notify();
        }
    }

    public void b() {
        if (b.f2215b) {
            Log.d(b.f2214a, "onEndOfSpeech");
        }
    }
}
