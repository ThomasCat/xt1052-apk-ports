package com.nuance.dragon.toolkit.audio.pipes;

import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.MultiChannelAudioChunk;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.LinkedList;
import java.util.List;

public class AlignedMergerPipe extends SingleSinkPipe {

    /* renamed from: a reason: collision with root package name */
    private final com.nuance.dragon.toolkit.util.internal.a f1118a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Handler f1119b;

    /* renamed from: c reason: collision with root package name */
    private AudioSource f1120c;

    /* renamed from: d reason: collision with root package name */
    private final AudioSink f1121d;

    /* renamed from: e reason: collision with root package name */
    private final LinkedList f1122e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public final LinkedList f1123f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public final LinkedList f1124g;

    /* renamed from: h reason: collision with root package name */
    private final LinkedList f1125h;
    /* access modifiers changed from: private */
    public final AudioType i;
    private final a j;
    private long k;
    private int l;
    private int m;
    private final a n;
    /* access modifiers changed from: private */
    public final a o;
    /* access modifiers changed from: private */
    public final a p;
    /* access modifiers changed from: private */
    public a q;
    /* access modifiers changed from: private */
    public boolean r;
    /* access modifiers changed from: private */
    public Object s;

    final class a {

        /* renamed from: a reason: collision with root package name */
        final int f1136a;

        /* renamed from: b reason: collision with root package name */
        final int f1137b;

        a(AudioType audioType, int i) {
            this.f1136a = i;
            this.f1137b = audioType.getSampleCount(i);
        }
    }

    static {
        System.loadLibrary("dmt_native_utils");
    }

    public AlignedMergerPipe(AudioType audioType, int i2, int i3) {
        this(audioType, i2, i3, i3, 0);
    }

    public AlignedMergerPipe(AudioType audioType, int i2, int i3, int i4, int i5) {
        boolean z = false;
        d.a("audioType", (Object) audioType);
        d.a("correlationWindowMs", "greater than or equal to 0", i5 >= 0);
        d.a("frameSizeMs", "greater than 0", i2 > 0);
        String str = "maxSecondaryOffsetMs";
        String str2 = "greater than or equal to minSecondaryOffsetMs";
        if (i4 >= i3) {
            z = true;
        }
        d.a(str, str2, z);
        this.f1118a = new com.nuance.dragon.toolkit.util.internal.a();
        this.f1119b = new Handler();
        this.n = new a(audioType, i5);
        this.o = new a(audioType, i3);
        this.p = new a(audioType, i4);
        this.q = this.p;
        this.i = audioType;
        this.j = new a(audioType, i2);
        this.f1122e = new LinkedList();
        this.f1123f = new LinkedList();
        this.f1124g = new LinkedList();
        this.f1125h = new LinkedList();
        if (i5 == 0) {
            this.r = true;
        }
        this.f1121d = new AudioSink() {
            public final void chunksAvailable(AudioSource audioSource) {
                boolean z = false;
                for (AudioChunk audioChunk : audioSource.getAllAudioChunksForSink(this)) {
                    int i = !AlignedMergerPipe.this.f1123f.isEmpty() ? (int) (audioChunk.audioTimestamp - ((AudioChunk) AlignedMergerPipe.this.f1123f.getLast()).audioEndTimestamp) : 0;
                    if (i <= 1) {
                        AlignedMergerPipe.a(AlignedMergerPipe.this, audioChunk.audioShorts.length);
                        AlignedMergerPipe.this.f1123f.add(audioChunk);
                    } else {
                        if (AlignedMergerPipe.this.f1124g.isEmpty()) {
                            Logger.warn(AlignedMergerPipe.this, "Got delayed secondary audio (" + i + " missing)");
                        }
                        AlignedMergerPipe.this.f1124g.add(audioChunk);
                    }
                    z = true;
                }
                if (z) {
                    AlignedMergerPipe.this.c();
                }
            }

            public final void framesDropped(AudioSource audioSource) {
            }

            public final void sourceClosed(AudioSource audioSource) {
            }
        };
    }

    static /* synthetic */ int a(AlignedMergerPipe alignedMergerPipe, int i2) {
        int i3 = alignedMergerPipe.m + i2;
        alignedMergerPipe.m = i3;
        return i3;
    }

    private static AudioChunk a(LinkedList linkedList, int i2) {
        return a(linkedList, ((AudioChunk) linkedList.peek()).audioType, i2, ((AudioChunk) linkedList.peek()).audioTimestamp);
    }

    private static AudioChunk a(LinkedList linkedList, AudioType audioType, int i2, long j2) {
        short[] sArr;
        short[] sArr2 = new short[i2];
        int i3 = 0;
        while (!linkedList.isEmpty() && i3 < i2) {
            int i4 = i2 - i3;
            AudioChunk audioChunk = (AudioChunk) linkedList.remove();
            short[] sArr3 = audioChunk.audioShorts;
            if (sArr3.length <= i4) {
                System.arraycopy(sArr3, 0, sArr2, i3, sArr3.length);
                i3 = sArr3.length + i3;
            } else {
                System.arraycopy(sArr3, 0, sArr2, i3, i4);
                short[] sArr4 = new short[(sArr3.length - i4)];
                System.arraycopy(sArr3, i4, sArr4, 0, sArr3.length - i4);
                linkedList.addFirst(new AudioChunk(audioChunk.audioType, sArr4, audioChunk.audioTimestamp + ((long) audioChunk.audioType.getDuration(i4))));
                i3 = i2;
            }
        }
        if (i3 < i2) {
            sArr = new short[i3];
            System.arraycopy(sArr2, 0, sArr, 0, sArr.length);
        } else {
            sArr = sArr2;
        }
        return new AudioChunk(audioType, sArr, j2);
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.audio.AudioChunk>, for r8v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.audio.AudioChunk>] */
    private static AudioChunk a(List<AudioChunk> list, int i2) {
        short[] sArr = new short[i2];
        AudioChunk audioChunk = (AudioChunk) list.get(0);
        int i3 = 0;
        for (AudioChunk audioChunk2 : list) {
            int i4 = i2 - i3;
            if (i4 > audioChunk2.audioShorts.length) {
                i4 = audioChunk2.audioShorts.length;
            }
            System.arraycopy(audioChunk2.audioShorts, 0, sArr, i3, i4);
            int i5 = i3 + i4;
            if (i5 >= i2) {
                break;
            }
            i3 = i5;
        }
        return new AudioChunk(audioChunk.audioType, sArr, audioChunk.audioTimestamp);
    }

    private boolean a() {
        return this.f1120c != null && this.f1120c.isActive();
    }

    private static int b(LinkedList linkedList, int i2) {
        int i3 = 0;
        while (!linkedList.isEmpty() && i3 < i2) {
            int i4 = i2 - i3;
            AudioChunk audioChunk = (AudioChunk) linkedList.remove();
            short[] sArr = audioChunk.audioShorts;
            if (sArr.length <= i4) {
                i3 = sArr.length + i3;
            } else {
                short[] sArr2 = new short[(sArr.length - i4)];
                System.arraycopy(sArr, i4, sArr2, 0, sArr.length - i4);
                linkedList.addFirst(new AudioChunk(audioChunk.audioType, sArr2, audioChunk.audioTimestamp + ((long) audioChunk.audioType.getDuration(i4))));
                i3 = i2;
            }
        }
        return i3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:111:0x02a8  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x008c  */
    private boolean b() {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        int i2;
        boolean z5 = false;
        do {
            if (this.m > 0) {
                int i3 = this.m;
                if (!this.f1123f.isEmpty()) {
                    if (!this.f1122e.isEmpty()) {
                        long j2 = ((AudioChunk) this.f1122e.peek()).audioTimestamp;
                        long j3 = ((AudioChunk) this.f1123f.peek()).audioTimestamp;
                        int i4 = (int) (j2 - (this.r ? j3 + ((long) this.q.f1136a) : j3 + ((long) this.p.f1136a)));
                        if (i4 > 0) {
                            long j4 = ((AudioChunk) this.f1123f.getLast()).audioEndTimestamp;
                            int sampleCount = this.i.getSampleCount(i4);
                            if (sampleCount >= this.m) {
                                sampleCount = this.m;
                            }
                            int b2 = b(this.f1123f, sampleCount);
                            if (!this.f1123f.isEmpty()) {
                                long j5 = ((AudioChunk) this.f1123f.getLast()).audioEndTimestamp;
                            }
                            Logger.warn(this, "Removing " + i4 + "ms from secondary audio");
                            i2 = b2;
                            this.m = i3 - i2;
                            int i5 = this.m;
                            if (this.m == 0) {
                                d();
                                z = true;
                                continue;
                            }
                        }
                    } else if (!isSourceActive()) {
                        int i6 = (int) (((this.f1124g.isEmpty() ? ((AudioChunk) this.f1123f.getLast()).audioEndTimestamp : ((AudioChunk) this.f1124g.getLast()).audioEndTimestamp) - ((long) (this.p.f1136a - this.o.f1136a))) - ((AudioChunk) this.f1123f.getFirst()).audioTimestamp);
                        if (i6 > 0) {
                            i2 = b(this.f1123f, this.i.getSampleCount(i6));
                            this.m = i3 - i2;
                            int i52 = this.m;
                            if (this.m == 0) {
                            }
                        }
                    }
                }
                i2 = 0;
                this.m = i3 - i2;
                int i522 = this.m;
                if (this.m == 0) {
                }
            }
            z = false;
            continue;
        } while (z);
        if (!this.r && this.s == null && !this.f1123f.isEmpty() && !this.f1122e.isEmpty()) {
            int i7 = this.n.f1137b;
            final int i8 = (int) (((AudioChunk) this.f1122e.peek()).audioTimestamp - ((AudioChunk) this.f1123f.peek()).audioTimestamp);
            final int i9 = this.o.f1136a - i8;
            final int i10 = this.p.f1136a - i8;
            int i11 = i9 < 0 ? i7 + this.i.getSampleCount(-i9) : i7;
            int i12 = i10 > 0 ? this.i.getSampleCount(i10) + i7 : i7;
            int i13 = (a() || this.m <= 0 || this.m >= i11) ? i11 : this.m;
            if (this.l >= i12 && this.m >= i13) {
                AudioChunk a2 = a((List) this.f1122e, i12);
                AudioChunk a3 = a((List) this.f1123f, i13);
                final short[] sArr = a2.audioShorts;
                final short[] sArr2 = a3.audioShorts;
                this.r = false;
                final Object obj = new Object();
                this.s = obj;
                this.f1118a.a((Runnable) new Runnable() {
                    public final void run() {
                        final int guessAudioAlignment = AlignedMergerPipe.guessAudioAlignment(sArr, sArr2, AlignedMergerPipe.this.i.frequency, i9, i10);
                        AlignedMergerPipe.this.f1119b.post(new Runnable() {
                            public final void run() {
                                if (guessAudioAlignment < i9 || guessAudioAlignment > i10) {
                                    Logger.error(AlignedMergerPipe.this, "Got invalid alignment " + guessAudioAlignment + " (expected between " + i9 + " and " + i10 + ")");
                                    AlignedMergerPipe.this.q = new a(AlignedMergerPipe.this.i, (i9 + i10) / 2);
                                } else if (obj == AlignedMergerPipe.this.s) {
                                    int i = i8 + guessAudioAlignment;
                                    if (AlignedMergerPipe.this.o.f1136a <= i) {
                                        int i2 = AlignedMergerPipe.this.p.f1136a;
                                    }
                                    AlignedMergerPipe alignedMergerPipe = AlignedMergerPipe.this;
                                    new StringBuilder("New secondary adjustment: ").append(i).append("ms");
                                    AlignedMergerPipe.this.q = new a(AlignedMergerPipe.this.i, i);
                                } else {
                                    return;
                                }
                                AlignedMergerPipe.this.r = true;
                                AlignedMergerPipe.this.s = null;
                                AlignedMergerPipe.this.c();
                                if (!AlignedMergerPipe.this.isSourceActive()) {
                                    AlignedMergerPipe.this.notifySourceClosed();
                                }
                            }
                        });
                    }
                });
            }
        }
        if (this.f1122e.isEmpty()) {
            z2 = false;
        } else if (this.f1123f.isEmpty() && a()) {
            z2 = false;
        } else if (!this.f1123f.isEmpty()) {
            long j6 = ((AudioChunk) this.f1122e.peek()).audioTimestamp;
            long j7 = ((AudioChunk) this.f1123f.peek()).audioTimestamp;
            int i14 = (int) ((this.r ? j7 + ((long) this.q.f1136a) : j7 + ((long) this.o.f1136a)) - j6);
            if (i14 > 0) {
                int sampleCount2 = this.i.getSampleCount(i14);
                int i15 = sampleCount2 % this.j.f1137b;
                if (i15 > 0) {
                    int i16 = this.j.f1137b - i15;
                    if (i16 <= this.m) {
                        int b3 = b(this.f1123f, i16);
                        this.m -= b3;
                        sampleCount2 += b3;
                    }
                }
                if (sampleCount2 > this.l) {
                    sampleCount2 = this.l - (this.l % this.j.f1137b);
                }
                if (sampleCount2 > 0) {
                    AudioChunk a4 = a(this.f1122e, sampleCount2);
                    this.l -= a4.audioShorts.length;
                    int i17 = this.l;
                    this.f1125h.add(new MultiChannelAudioChunk(a4));
                    Logger.warn(this, "Passing " + a4.audioDuration + "ms excess primary audio through");
                    z2 = true;
                    if (this.r) {
                        if (this.m < this.j.f1137b && !a()) {
                            this.f1123f.add(new AudioChunk(this.i, new short[(this.j.f1137b - this.m)], ((AudioChunk) this.f1123f.getLast()).audioEndTimestamp));
                            this.m = this.j.f1137b;
                        }
                        int i18 = this.l < this.m ? this.l : this.m;
                        if (i18 >= this.j.f1137b) {
                            int i19 = i18 - (i18 % this.j.f1137b);
                            AudioChunk a5 = a(this.f1122e, i19);
                            AudioChunk a6 = a(this.f1123f, a5.audioType, i19, a5.audioTimestamp - ((long) this.j.f1136a));
                            short[] sArr3 = a5.audioShorts;
                            short[] sArr4 = a6.audioShorts;
                            this.l -= i19;
                            this.m -= i19;
                            this.f1125h.add(new MultiChannelAudioChunk(a5, a6));
                            z2 = true;
                        }
                        int i20 = this.m;
                        int i21 = this.l;
                    }
                }
            }
            z2 = false;
            if (this.r) {
            }
        } else if (this.l > 0) {
            int i22 = this.l - (this.l % this.j.f1137b);
            if (i22 > 0) {
                AudioChunk a7 = a(this.f1122e, i22);
                this.l -= a7.audioShorts.length;
                this.f1125h.add(new MultiChannelAudioChunk(a7));
                Logger.warn(this, "Passing primary audio through due to empty secondary audio");
                z3 = true;
            } else {
                z3 = false;
            }
            z2 = z3;
        } else {
            z2 = false;
        }
        if (isSourceActive()) {
            z4 = false;
        } else if (this.f1122e.isEmpty() || this.s != null) {
            z4 = false;
        } else {
            this.f1125h.add(new MultiChannelAudioChunk(a(this.f1122e, this.l)));
            this.l = 0;
            z4 = true;
        }
        boolean z6 = z2 || z4;
        e();
        if (!z2 || this.m != 0) {
            return z6;
        }
        if (z6 || b()) {
            z5 = true;
        }
        return z5;
    }

    /* access modifiers changed from: private */
    public void c() {
        if (b()) {
            notifyChunksAvailable();
        }
    }

    private void d() {
        LinkedList linkedList = this.f1123f;
        while (!this.f1124g.isEmpty()) {
            AudioChunk audioChunk = (AudioChunk) this.f1124g.peek();
            if (this.f1123f.isEmpty() || audioChunk.audioTimestamp == ((AudioChunk) this.f1123f.getLast()).audioEndTimestamp) {
                this.f1123f.add(this.f1124g.removeFirst());
                this.m = audioChunk.audioShorts.length + this.m;
            } else {
                return;
            }
        }
    }

    private void e() {
        if (!isSourceActive() && !this.f1124g.isEmpty()) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.f1124g.size()) {
                    AudioChunk audioChunk = (AudioChunk) this.f1124g.get(i3);
                    if (((int) (((AudioChunk) this.f1124g.getLast()).audioEndTimestamp - audioChunk.audioEndTimestamp)) >= (-this.q.f1136a) && (this.f1122e.isEmpty() || ((int) (audioChunk.audioTimestamp - ((AudioChunk) this.f1122e.getLast()).audioEndTimestamp)) >= this.p.f1136a)) {
                        int i4 = i3 - 1;
                        this.f1124g.remove(i3);
                        i3 = i4;
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    static native int guessAudioAlignment(short[] sArr, short[] sArr2, int i2, int i3, int i4);

    /* access modifiers changed from: protected */
    public void chunksAvailable(AudioSource audioSource, AudioSink audioSink) {
        for (AudioChunk audioChunk : audioSource.getAllAudioChunksForSink(audioSink)) {
            if (this.f1122e.isEmpty()) {
                this.f1122e.add(audioChunk);
                this.l = audioChunk.audioShorts.length + this.l;
            } else {
                long j2 = ((AudioChunk) this.f1122e.getLast()).audioEndTimestamp;
                int i2 = (int) (audioChunk.audioTimestamp - j2);
                if (i2 == 0) {
                    this.f1122e.add(audioChunk);
                    this.l = audioChunk.audioShorts.length + this.l;
                } else if (i2 > 0) {
                    Logger.warn(this, "Inserting silence to preserve time alignment");
                    int sampleCount = this.i.getSampleCount(i2);
                    this.f1122e.add(new AudioChunk(this.i, new short[sampleCount], j2));
                    this.l = sampleCount + this.l;
                    this.f1122e.add(audioChunk);
                    this.l = audioChunk.audioShorts.length + this.l;
                } else {
                    Logger.error(this, "Discarding audio buffer because it overlaps previous buffer");
                    notifyFramesDropped();
                }
            }
        }
        c();
    }

    public void connectAudioSource(AudioSource audioSource) {
        this.f1122e.clear();
        this.l = 0;
        if (this.n.f1136a != 0) {
            this.r = false;
            this.s = null;
            this.q = this.p;
        }
        super.connectAudioSource(audioSource);
    }

    public void connectSecondarySource(AudioSource audioSource) {
        d.a("source", "the correct audio type", audioSource.getAudioType().equals(this.i));
        this.f1120c = audioSource;
        this.f1121d.connectAudioSource(this.f1120c);
    }

    public AudioSource disconnectSecondarySource() {
        this.f1120c = null;
        return this.f1121d.disconnectAudioSource();
    }

    /* access modifiers changed from: protected */
    public void framesDropped(AudioSource audioSource, AudioSink audioSink) {
    }

    /* access modifiers changed from: protected */
    public MultiChannelAudioChunk getAudioChunk() {
        if (!this.f1125h.isEmpty()) {
            return (MultiChannelAudioChunk) this.f1125h.remove();
        }
        return null;
    }

    public AudioType getAudioType() {
        return this.i;
    }

    public int getChunksAvailable() {
        return this.f1125h.size();
    }

    public boolean isActive() {
        return isSourceActive();
    }

    /* access modifiers changed from: protected */
    public void sourceClosed(AudioSource audioSource, AudioSink audioSink) {
        boolean z;
        int i2 = 0;
        boolean z2 = true;
        int i3 = this.l % this.j.f1137b;
        if (i3 > 0) {
            AudioChunk audioChunk = new AudioChunk(this.i, new short[i3], this.k);
            this.k += (long) audioChunk.audioDuration;
            this.f1122e.add(audioChunk);
            this.l = i3 + this.l;
            z = true;
        } else {
            z = false;
        }
        if (!this.f1122e.isEmpty()) {
            long j2 = ((AudioChunk) this.f1122e.getLast()).audioEndTimestamp;
            while (i2 < this.f1123f.size() && ((AudioChunk) this.f1123f.get(i2)).audioTimestamp - j2 <= 0) {
                i2++;
            }
            int size = this.f1123f.size() - 1;
            while (size >= i2) {
                AudioChunk audioChunk2 = (AudioChunk) this.f1123f.remove(size);
                this.m -= audioChunk2.audioShorts.length;
                this.f1124g.addFirst(audioChunk2);
                size--;
                z = true;
            }
            int i4 = this.m;
        }
        if (this.f1122e.isEmpty()) {
            z2 = z;
        }
        if (z2) {
            c();
        }
        if (this.s == null) {
            notifySourceClosed();
        }
    }
}
