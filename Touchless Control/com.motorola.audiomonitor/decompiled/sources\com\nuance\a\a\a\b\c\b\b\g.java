package com.nuance.a.a.a.b.c.b.b;

import com.nuance.a.a.a.b.c.c.h;

public final class g extends o implements h {

    /* renamed from: a reason: collision with root package name */
    private byte[] f864a;

    public g(String str, byte[] bArr) {
        super(str, 4);
        this.f864a = new byte[bArr.length];
        System.arraycopy(bArr, 0, this.f864a, 0, this.f864a.length);
    }

    /* access modifiers changed from: protected */
    public final byte[] a() {
        return this.f864a;
    }
}
