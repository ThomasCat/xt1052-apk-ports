package com.nuance.dragon.toolkit.audio.sinks;

import android.media.AudioTrack;
import android.os.Handler;
import android.os.SystemClock;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.d;
import com.nuance.dragon.toolkit.util.internal.f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class SpeakerPlayerSink extends PlayerSink {

    /* renamed from: a reason: collision with root package name */
    static final /* synthetic */ boolean f1237a = (!SpeakerPlayerSink.class.desiredAssertionStatus());

    /* renamed from: b reason: collision with root package name */
    private AudioTrack f1238b;

    /* renamed from: c reason: collision with root package name */
    private int f1239c;

    /* renamed from: d reason: collision with root package name */
    private int f1240d;

    /* renamed from: e reason: collision with root package name */
    private int f1241e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public boolean f1242f;

    /* renamed from: g reason: collision with root package name */
    private final LinkedList f1243g;

    /* renamed from: h reason: collision with root package name */
    private int f1244h;
    /* access modifiers changed from: private */
    public boolean i;
    private boolean j;
    /* access modifiers changed from: private */
    public Handler k;
    private int l;
    private long m;
    private final int n;
    private int o;
    private boolean p;
    private final ArrayList q;

    public SpeakerPlayerSink(int i2, AudioType audioType) {
        this(audioType);
        this.f1239c = i2;
    }

    public SpeakerPlayerSink(AudioType audioType) {
        this(audioType, (Handler) null);
    }

    public SpeakerPlayerSink(AudioType audioType, int i2) {
        this(audioType, i2, null);
    }

    public SpeakerPlayerSink(AudioType audioType, int i2, Handler handler) {
        boolean z = false;
        super(audioType, handler);
        this.f1239c = 3;
        this.f1242f = false;
        this.f1243g = new LinkedList();
        String str = "bufferLengthMs";
        String str2 = "greater than 0";
        if (i2 > 0) {
            z = true;
        }
        d.a(str, str2, z);
        this.n = i2;
        this.q = new ArrayList();
    }

    public SpeakerPlayerSink(AudioType audioType, int i2, Handler handler, int i3) {
        boolean z = false;
        super(audioType, handler);
        this.f1239c = 3;
        this.f1242f = false;
        this.f1243g = new LinkedList();
        String str = "bufferLengthMs";
        String str2 = "greater than 0";
        if (i2 > 0) {
            z = true;
        }
        d.a(str, str2, z);
        this.n = i2;
        this.q = new ArrayList();
        this.f1239c = i3;
    }

    public SpeakerPlayerSink(AudioType audioType, Handler handler) {
        this(audioType, 400, handler);
    }

    static /* synthetic */ int c(SpeakerPlayerSink speakerPlayerSink) {
        int i2;
        if (!speakerPlayerSink.i) {
            return 0;
        }
        if (speakerPlayerSink.p) {
            if (speakerPlayerSink.f1238b.getPlaybackHeadPosition() >= speakerPlayerSink.l) {
                speakerPlayerSink.p = false;
                Logger.warn(speakerPlayerSink, "Player stalled");
            }
        }
        int i3 = speakerPlayerSink.f1241e;
        ArrayList arrayList = new ArrayList();
        int i4 = 0;
        int i5 = i3;
        while (i5 > 0 && !speakerPlayerSink.f1243g.isEmpty()) {
            short[] sArr = (short[]) speakerPlayerSink.f1243g.getFirst();
            int length = sArr.length - speakerPlayerSink.f1244h;
            int i6 = length > i5 ? i5 : length;
            int write = speakerPlayerSink.f1238b.write(sArr, speakerPlayerSink.f1244h, i6);
            if (write < 0) {
                speakerPlayerSink.f1244h = 0;
                speakerPlayerSink.f1243g.removeFirst();
                i5 = 0;
            } else {
                if (write > 0) {
                    if (!speakerPlayerSink.p && speakerPlayerSink.o != speakerPlayerSink.f1238b.getPlaybackHeadPosition()) {
                        int i7 = speakerPlayerSink.o;
                        speakerPlayerSink.p = true;
                        speakerPlayerSink.m = SystemClock.uptimeMillis() - ((long) speakerPlayerSink.getAudioType().getDuration(speakerPlayerSink.f1238b.getPlaybackHeadPosition() - speakerPlayerSink.o));
                        Iterator it = speakerPlayerSink.q.iterator();
                        while (true) {
                            i2 = i7;
                            if (!it.hasNext()) {
                                break;
                            }
                            short[] sArr2 = (short[]) it.next();
                            arrayList.add(new AudioChunk(speakerPlayerSink.getAudioType(), sArr2, speakerPlayerSink.m + ((long) speakerPlayerSink.getAudioType().getDuration(i2))));
                            i7 = sArr2.length + i2;
                        }
                        speakerPlayerSink.q.clear();
                        if (f1237a || i2 == speakerPlayerSink.l) {
                            Logger.warn(speakerPlayerSink, "Player unstalled");
                        } else {
                            throw new AssertionError();
                        }
                    }
                    if (speakerPlayerSink.p) {
                        arrayList.add(new AudioChunk(speakerPlayerSink.getAudioType(), f.a(sArr, speakerPlayerSink.f1244h, write), speakerPlayerSink.m + ((long) speakerPlayerSink.getAudioType().getDuration(speakerPlayerSink.l))));
                    } else {
                        speakerPlayerSink.q.add(f.a(sArr, speakerPlayerSink.f1244h, write));
                    }
                    i4 += write;
                    speakerPlayerSink.l += write;
                }
                int i8 = write < i6 ? 0 : i5 - write;
                if (speakerPlayerSink.f1244h + write < sArr.length) {
                    speakerPlayerSink.f1244h += write;
                    i5 = i8;
                } else {
                    speakerPlayerSink.f1243g.removeFirst();
                    speakerPlayerSink.f1244h = 0;
                    i5 = i8;
                }
            }
        }
        if (!arrayList.isEmpty()) {
            speakerPlayerSink.handleChunksPlayed(arrayList);
        }
        if (i5 == 0) {
            speakerPlayerSink.i = false;
        }
        AudioSource connectedSource = speakerPlayerSink.getConnectedSource();
        if (!speakerPlayerSink.j || !speakerPlayerSink.f1243g.isEmpty() || !(connectedSource == null || connectedSource.getChunksAvailableForSink(speakerPlayerSink) == 0)) {
            if (speakerPlayerSink.f1243g.isEmpty()) {
                speakerPlayerSink.handleAudioNeeded(speakerPlayerSink.n);
            }
        } else if (i4 > 0 && i4 < speakerPlayerSink.f1241e) {
            short[] sArr3 = new short[(speakerPlayerSink.f1241e - i4)];
            speakerPlayerSink.f1238b.write(sArr3, 0, sArr3.length);
        } else if (speakerPlayerSink.l < speakerPlayerSink.f1241e || speakerPlayerSink.f1238b.getPlaybackHeadPosition() - speakerPlayerSink.l >= 0) {
            speakerPlayerSink.stopPlayingInternal();
        }
        return i4;
    }

    /* access modifiers changed from: protected */
    public boolean isCodecSupported(AudioType audioType) {
        return audioType.encoding == Encoding.PCM_16;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.audio.AudioChunk>, for r4v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.audio.AudioChunk>] */
    public void newChunksInternal(List<AudioChunk> list) {
        if (list != null) {
            for (AudioChunk audioChunk : list) {
                this.f1243g.add(audioChunk.audioShorts);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void noNewChunksInternal() {
        if (!this.j) {
            this.j = true;
        }
    }

    /* access modifiers changed from: protected */
    public boolean startPlayingInternal(AudioType audioType) {
        int i2 = 8000;
        Encoding encoding = audioType.encoding;
        Encoding encoding2 = Encoding.PCM_16;
        this.f1242f = false;
        this.j = false;
        this.l = 0;
        final int i3 = this.n / 2;
        if (audioType.frequency != 8000) {
            i2 = audioType.frequency == 11025 ? 11025 : audioType.frequency == 22050 ? 22050 : audioType.frequency == 44100 ? 44100 : 16000;
        }
        int i4 = (this.n * (i2 * 2)) / 1000;
        int minBufferSize = AudioTrack.getMinBufferSize(i2, 2, 2);
        if (minBufferSize == -2 || minBufferSize == -1 || minBufferSize <= i4) {
            minBufferSize = i4;
        }
        this.f1240d = minBufferSize;
        this.f1241e = this.f1240d / 2;
        this.f1238b = new AudioTrack(this.f1239c, i2, 2, 2, this.f1240d, 1);
        if (this.f1238b.getState() != 1) {
            Logger.error(this, "startPlayingInternal() instantiate AudioTrack failed!!!");
            stopPlayingInternal();
            return false;
        }
        this.o = 0;
        this.p = false;
        this.k = new Handler();
        this.k.post(new Runnable() {

            /* renamed from: c reason: collision with root package name */
            private boolean f1247c = false;

            public final void run() {
                if (!SpeakerPlayerSink.this.f1242f) {
                    SpeakerPlayerSink.this.i = true;
                    int c2 = SpeakerPlayerSink.c(SpeakerPlayerSink.this);
                    if (!this.f1247c && c2 > 0) {
                        SpeakerPlayerSink.this.handleStarted();
                        if (!SpeakerPlayerSink.this.f1242f) {
                            this.f1247c = true;
                        }
                    }
                    if (!SpeakerPlayerSink.this.f1242f) {
                        SpeakerPlayerSink.this.k.postDelayed(this, (long) i3);
                    }
                }
            }
        });
        this.f1238b.play();
        this.i = true;
        return true;
    }

    /* access modifiers changed from: protected */
    public void stopPlayingInternal() {
        if (!this.f1242f) {
            this.f1242f = true;
            if (this.k != null) {
                this.k.removeCallbacksAndMessages(null);
                this.k = null;
            }
            if (this.f1238b != null) {
                if (this.f1238b.getState() != 0) {
                    this.f1238b.stop();
                }
                this.f1238b.release();
                this.f1238b = null;
            }
            handleStopped();
        }
    }
}
