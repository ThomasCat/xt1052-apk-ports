package com.motorola.audiomonitor;

import android.os.IBinder;
import android.os.Parcel;

class y implements w {

    /* renamed from: a reason: collision with root package name */
    private IBinder f773a;

    y(IBinder iBinder) {
        this.f773a = iBinder;
    }

    public void a(int i) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.motorola.audiomonitor.IMonitorServiceListener");
            obtain.writeInt(i);
            this.f773a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }

    public void a(int i, int i2) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.motorola.audiomonitor.IMonitorServiceListener");
            obtain.writeInt(i);
            obtain.writeInt(i2);
            this.f773a.transact(3, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }

    public IBinder asBinder() {
        return this.f773a;
    }

    public void b(int i) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.motorola.audiomonitor.IMonitorServiceListener");
            obtain.writeInt(i);
            this.f773a.transact(2, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }

    public void c(int i) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.motorola.audiomonitor.IMonitorServiceListener");
            obtain.writeInt(i);
            this.f773a.transact(4, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }
}
