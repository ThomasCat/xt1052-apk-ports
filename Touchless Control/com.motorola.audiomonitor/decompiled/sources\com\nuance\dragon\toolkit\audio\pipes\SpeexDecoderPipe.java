package com.nuance.dragon.toolkit.audio.pipes;

import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.audio.util.SpeexDecoder;

public class SpeexDecoderPipe extends ConverterPipe {

    /* renamed from: a reason: collision with root package name */
    private final SpeexDecoder f1184a = new SpeexDecoder();

    /* access modifiers changed from: protected */
    public void cleanup() {
        this.f1184a.a();
    }

    /* access modifiers changed from: protected */
    public AudioChunk[] convert(AudioChunk audioChunk) {
        return this.f1184a.a(audioChunk.audioBytes, audioChunk.audioBytes.length, audioChunk.audioSpeechStatus);
    }

    public AudioType getAudioType() {
        AudioSource connectedSource = getConnectedSource();
        if (connectedSource != null) {
            AudioType audioType = connectedSource.getAudioType();
            if (audioType != null) {
                return audioType.frequency == 8000 ? AudioType.PCM_8k : AudioType.PCM_16k;
            }
        }
        return AudioType.UNKNOWN;
    }

    /* access modifiers changed from: protected */
    public void initialize(AudioType audioType) {
        this.f1184a.a(audioType);
    }

    /* access modifiers changed from: protected */
    public final boolean isAudioSourceTypeSupported(AudioType audioType) {
        return audioType.encoding == Encoding.SPEEX;
    }
}
