package com.nuance.dragon.toolkit.elvis;

import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.SpeechDetectionListener;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.InitializeListener;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.Languages;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.RebuildListener;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.ReleaseListener;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.ResultListener;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.SaveListener;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.WakeupCheckListener;
import com.nuance.dragon.toolkit.elvis.WordSlot.WordIterator;
import com.nuance.dragon.toolkit.grammar.Word;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

abstract class a extends ElvisRecognizer {

    /* renamed from: a reason: collision with root package name */
    protected final Handler f1569a = new Handler();
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final e f1570b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public Grammar f1571c = new Grammar();
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public final Handler f1572d;

    /* renamed from: e reason: collision with root package name */
    private final ArrayList f1573e = new ArrayList();

    /* renamed from: f reason: collision with root package name */
    private final List f1574f = new ArrayList(0);

    /* renamed from: g reason: collision with root package name */
    private int f1575g = 0;

    /* renamed from: h reason: collision with root package name */
    private int f1576h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public b k;
    /* access modifiers changed from: private */
    public C0006a l;
    /* access modifiers changed from: private */
    public String m;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public boolean o;
    private boolean p;

    /* renamed from: com.nuance.dragon.toolkit.elvis.a$a reason: collision with other inner class name */
    final class C0006a {
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public final Grammar f1619b;
        /* access modifiers changed from: private */

        /* renamed from: c reason: collision with root package name */
        public final Grammar f1620c = new Grammar();
        /* access modifiers changed from: private */

        /* renamed from: d reason: collision with root package name */
        public final ArrayList f1621d;
        /* access modifiers changed from: private */

        /* renamed from: e reason: collision with root package name */
        public final RebuildListener f1622e;
        /* access modifiers changed from: private */

        /* renamed from: f reason: collision with root package name */
        public boolean f1623f;
        /* access modifiers changed from: private */

        /* renamed from: g reason: collision with root package name */
        public final List f1624g;

        C0006a(Grammar grammar, Grammar grammar2, RebuildListener rebuildListener) {
            if (grammar2 != null) {
                this.f1620c.b(grammar2.getConstraints());
                this.f1620c.a(grammar2.getSlots());
            }
            this.f1619b = grammar;
            this.f1621d = new ArrayList();
            this.f1622e = rebuildListener;
            this.f1624g = new ArrayList();
        }

        /* access modifiers changed from: 0000 */
        public final void a() {
            if (this.f1623f) {
                synchronized (this) {
                    this.f1623f = false;
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public final void a(final boolean z) {
            boolean z2 = this.f1623f;
            synchronized (this) {
                this.f1623f = true;
            }
            final ArrayList arrayList = new ArrayList();
            final AnonymousClass1 r1 = new Runnable() {
                public final void run() {
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        Constraint constraint = (Constraint) it.next();
                        String id = constraint.getId();
                        Iterator it2 = constraint.a().iterator();
                        while (true) {
                            if (it2.hasNext()) {
                                a aVar = (a) it2.next();
                                if (a.this.f1570b.a(id, aVar.f1529a, aVar.f1530b, aVar.f1531c) != 0) {
                                    Logger.error(a.this, "Error adding transition to native Elvis: " + id + " from: " + aVar.f1529a + " to: " + aVar.f1530b + " penalty: " + aVar.f1531c);
                                    a.this.f1570b.b(id);
                                    a.this.f1569a.post(new Runnable(new ElvisError(4)) {
                                        public final void run() {
                                            if (C0006a.this.f1623f) {
                                                C0006a.this.f1623f = false;
                                                Iterator it = C0006a.this.f1621d.iterator();
                                                while (it.hasNext()) {
                                                    ((WordIterator) it.next()).finished(false);
                                                }
                                                C0006a.this.f1621d.clear();
                                                C0006a.this.f1622e.onError(r3);
                                                if (a.this.l == C0006a.this) {
                                                    a.this.l = null;
                                                }
                                            }
                                        }
                                    });
                                    return;
                                }
                            }
                        }
                    }
                    if (!a.this.f1570b.a(a.this.m, C0006a.this.f1620c)) {
                        Logger.error(a.this, "Error saving Elvis state to: " + a.this.m);
                        a.this.f1569a.post(new Runnable(new ElvisError(7)) {
                            public final void run() {
                                if (C0006a.this.f1623f) {
                                    C0006a.this.f1623f = false;
                                    Iterator it = C0006a.this.f1621d.iterator();
                                    while (it.hasNext()) {
                                        ((WordIterator) it.next()).finished(false);
                                    }
                                    C0006a.this.f1621d.clear();
                                    C0006a.this.f1622e.onError(r3);
                                    if (a.this.l == C0006a.this) {
                                        a.this.l = null;
                                    }
                                }
                            }
                        });
                        return;
                    }
                    a.this.f1569a.post(new Runnable() {
                        public final void run() {
                            if (C0006a.this.f1623f) {
                                C0006a.this.f1623f = false;
                                Iterator it = C0006a.this.f1621d.iterator();
                                while (it.hasNext()) {
                                    ((WordIterator) it.next()).finished(true);
                                }
                                C0006a.this.f1621d.clear();
                                a.this.f1571c.b(C0006a.this.f1620c.getConstraints());
                                a.this.f1571c.a(C0006a.this.f1620c.getSlots());
                                if (a.this.l == C0006a.this) {
                                    a.this.l = null;
                                }
                                C0006a.this.f1622e.onComplete(C0006a.this.f1619b, C0006a.this.f1624g);
                            }
                        }
                    });
                }
            };
            final AnonymousClass2 r2 = new Runnable() {

                /* renamed from: d reason: collision with root package name */
                private Iterator f1631d;

                /* renamed from: e reason: collision with root package name */
                private WordIterator f1632e;

                /* renamed from: f reason: collision with root package name */
                private String f1633f;

                {
                    ArrayList arrayList = new ArrayList(C0006a.this.f1619b.getSlots().size() + 1);
                    arrayList.add(new WordSlot("~", 5));
                    arrayList.addAll(C0006a.this.f1619b.getSlots());
                    this.f1631d = arrayList.iterator();
                }

                /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
                    if (r9.f1632e == null) goto L_0x0038;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:16:0x0036, code lost:
                    if (r9.f1632e.hasNext() != false) goto L_0x015e;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:18:0x003e, code lost:
                    if (r9.f1631d.hasNext() == false) goto L_0x014f;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:19:0x0040, code lost:
                    r0 = (com.nuance.dragon.toolkit.elvis.WordSlot) r9.f1631d.next();
                    r9.f1633f = r0.getId();
                    r3 = com.nuance.dragon.toolkit.elvis.a.C0006a.a(r9.f1630c).getWordSlotByName(r9.f1633f);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:20:0x005e, code lost:
                    if (r0.getWordListRequired() == false) goto L_0x0094;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:22:0x0064, code lost:
                    if (r0.hasWordList() != false) goto L_0x0094;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:23:0x0066, code lost:
                    com.nuance.dragon.toolkit.util.Logger.error(r9.f1630c.f1618a, "WordSlot " + r0.getId() + " requires an attached wordlist.");
                    com.nuance.dragon.toolkit.elvis.a.C0006a.a(r9.f1630c, new com.nuance.dragon.toolkit.elvis.ElvisError(8));
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:24:0x0094, code lost:
                    if (r3 == null) goto L_0x009c;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:26:0x009a, code lost:
                    if (r0.getRebuildType() != 0) goto L_0x00c3;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:27:0x009c, code lost:
                    com.nuance.dragon.toolkit.elvis.a.C0006a.a(r9.f1630c).a(r0.copy());
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a9, code lost:
                    r3 = com.nuance.dragon.toolkit.elvis.a.a(r9.f1630c.f1618a).a(r0);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:29:0x00b5, code lost:
                    if (r3 != 2) goto L_0x0113;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b7, code lost:
                    com.nuance.dragon.toolkit.elvis.a.C0006a.a(r9.f1630c, new com.nuance.dragon.toolkit.elvis.ElvisError(6));
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:32:0x00cb, code lost:
                    if (r3.getType() == r0.getType()) goto L_0x00a9;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:34:0x00d1, code lost:
                    if (r0.getType() == 0) goto L_0x00a9;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:35:0x00d3, code lost:
                    com.nuance.dragon.toolkit.util.Logger.error(r9.f1630c.f1618a, "WordSlot types do not match:" + r9.f1633f + ". Old: " + r3.getType() + " New: " + r0.getType());
                    com.nuance.dragon.toolkit.elvis.a.C0006a.a(r9.f1630c, new com.nuance.dragon.toolkit.elvis.ElvisError(6));
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:36:0x0113, code lost:
                    if (r3 == 0) goto L_0x0119;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:38:0x0117, code lost:
                    if (r4 == false) goto L_0x0137;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:39:0x0119, code lost:
                    r9.f1632e = r0.getWords(true);
                    com.nuance.dragon.toolkit.elvis.a.C0006a.d(r9.f1630c).add(r9.f1632e);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:40:0x012a, code lost:
                    com.nuance.dragon.toolkit.elvis.a.c(r9.f1630c.f1618a).post(r9);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:42:0x013b, code lost:
                    if (r0.getRebuildType() != 1) goto L_0x012a;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:43:0x013d, code lost:
                    r9.f1632e = r0.getWords(false);
                    com.nuance.dragon.toolkit.elvis.a.C0006a.d(r9.f1630c).add(r9.f1632e);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:44:0x014f, code lost:
                    com.nuance.dragon.toolkit.elvis.a.c(r9.f1630c.f1618a).post(r1);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:45:0x015e, code lost:
                    r0 = com.nuance.dragon.toolkit.elvis.a.C0006a.a(r9.f1630c).getWordSlotByName(r9.f1633f);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:46:0x016a, code lost:
                    if (r0 == null) goto L_0x01c3;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:48:0x0170, code lost:
                    if (r0.a() == false) goto L_0x01c3;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:49:0x0172, code lost:
                    r0 = true;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:51:0x0175, code lost:
                    if (r2 >= 100) goto L_0x01b6;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:53:0x017d, code lost:
                    if (r9.f1632e.hasNext() == false) goto L_0x01b6;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:54:0x017f, code lost:
                    r3 = r9.f1632e.getNext();
                    r4 = com.nuance.dragon.toolkit.elvis.a.a(r9.f1630c.f1618a).a(r9.f1633f, r3, r0);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:55:0x0193, code lost:
                    if (r4 != 2) goto L_0x01a1;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:56:0x0195, code lost:
                    com.nuance.dragon.toolkit.elvis.a.C0006a.a(r9.f1630c, new com.nuance.dragon.toolkit.elvis.ElvisError(6));
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:57:0x01a1, code lost:
                    if (r4 != 1) goto L_0x01b3;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:58:0x01a3, code lost:
                    com.nuance.dragon.toolkit.elvis.a.C0006a.f(r9.f1630c).add(new com.nuance.dragon.toolkit.elvis.ElvisRecognizer.RebuildListener.SkippedWord(r3, r9.f1633f));
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:59:0x01b3, code lost:
                    r2 = r2 + 1;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:60:0x01b6, code lost:
                    com.nuance.dragon.toolkit.elvis.a.c(r9.f1630c.f1618a).post(r9);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:61:0x01c3, code lost:
                    r0 = false;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:71:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:75:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
                    if (com.nuance.dragon.toolkit.elvis.a.i(r9.f1630c.f1618a) != false) goto L_0x002c;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
                    com.nuance.dragon.toolkit.elvis.a.C0006a.a(r9.f1630c, new com.nuance.dragon.toolkit.elvis.ElvisError(9));
                 */
                public final void run() {
                    int i = 0;
                    synchronized (C0006a.this) {
                        if (!C0006a.this.f1623f) {
                        }
                    }
                }
            };
            a.this.f1572d.post(new Runnable() {
                /* JADX WARNING: Code restructure failed: missing block: B:10:0x004a, code lost:
                    r0.addAll(com.nuance.dragon.toolkit.elvis.a.C0006a.e(r8.f1636c).getConstraints());
                    r7 = r0.iterator();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:12:0x005f, code lost:
                    if (r7.hasNext() == false) goto L_0x011c;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:13:0x0061, code lost:
                    r6 = (com.nuance.dragon.toolkit.elvis.Constraint) r7.next();
                    r0 = 2;
                    r1 = r6.getId();
                    r2 = com.nuance.dragon.toolkit.elvis.a.C0006a.a(r8.f1636c).getConstraintByName(r1);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:14:0x007b, code lost:
                    if (r6.getAlwaysRebuild() != false) goto L_0x007f;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:15:0x007d, code lost:
                    if (r2 != null) goto L_0x005b;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:17:0x0083, code lost:
                    if (r6.b() == false) goto L_0x00c9;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:18:0x0085, code lost:
                    r0 = com.nuance.dragon.toolkit.elvis.a.a(r8.f1636c.f1618a).a(r1, r6.d(), r6.c(), r6.e(), r6.getAlwaysRebuild());
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:19:0x00a1, code lost:
                    if (r0 != 0) goto L_0x00e6;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:20:0x00a3, code lost:
                    r0.add(r6);
                    com.nuance.dragon.toolkit.elvis.a.C0006a.a(r8.f1636c).a(r6);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:24:0x00b5, code lost:
                    r0 = new java.util.ArrayList(com.nuance.dragon.toolkit.elvis.a.C0006a.e(r8.f1636c).getConstraints().size());
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:25:0x00c9, code lost:
                    com.nuance.dragon.toolkit.util.Logger.error(r8, "Constraint not valid: " + r6.getId() + ".  Skipping.");
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:27:0x00e7, code lost:
                    if (r0 != 1) goto L_0x00f9;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:28:0x00e9, code lost:
                    r0 = r8.f1636c.f1618a;
                    new java.lang.StringBuilder("Constraint already exists in native Elvis: ").append(r1);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:29:0x00f9, code lost:
                    com.nuance.dragon.toolkit.util.Logger.error(r8.f1636c.f1618a, "Error adding constraint to native Elvis: " + r1);
                    com.nuance.dragon.toolkit.elvis.a.C0006a.a(r8.f1636c, new com.nuance.dragon.toolkit.elvis.ElvisError(5));
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:30:0x011c, code lost:
                    com.nuance.dragon.toolkit.elvis.a.c(r8.f1636c.f1618a).post(r2);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:8:0x001b, code lost:
                    if (com.nuance.dragon.toolkit.elvis.a.C0006a.e(r8.f1636c).getConstraintByName("~") != null) goto L_0x00b5;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:9:0x001d, code lost:
                    r0 = new java.util.ArrayList(com.nuance.dragon.toolkit.elvis.a.C0006a.e(r8.f1636c).getConstraints().size() + 1);
                    r1 = new com.nuance.dragon.toolkit.elvis.Constraint("~");
                    r1.addTransition(com.nuance.dragon.toolkit.elvis.Constraint.START, "~", 0);
                    r1.addTransition("~", com.nuance.dragon.toolkit.elvis.Constraint.END, 0);
                    r0.add(r1);
                 */
                public final void run() {
                    synchronized (C0006a.this) {
                        if (!C0006a.this.f1623f) {
                        }
                    }
                }
            });
        }
    }

    final class b extends AudioSink {
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public final Grammar f1640b;

        /* renamed from: c reason: collision with root package name */
        private ResultListener f1641c;
        /* access modifiers changed from: private */

        /* renamed from: d reason: collision with root package name */
        public SpeechDetectionListener f1642d;
        /* access modifiers changed from: private */

        /* renamed from: e reason: collision with root package name */
        public SpeechDetectionListener f1643e;
        /* access modifiers changed from: private */

        /* renamed from: f reason: collision with root package name */
        public boolean f1644f;
        /* access modifiers changed from: private */

        /* renamed from: g reason: collision with root package name */
        public boolean f1645g;
        /* access modifiers changed from: private */

        /* renamed from: h reason: collision with root package name */
        public boolean f1646h;
        /* access modifiers changed from: private */
        public boolean i;
        private short[] j;

        b(Grammar grammar, ResultListener resultListener, SpeechDetectionListener speechDetectionListener) {
            this.f1640b = grammar;
            this.f1641c = resultListener;
            this.f1642d = speechDetectionListener;
            this.f1643e = speechDetectionListener;
        }

        /* access modifiers changed from: private */
        public void a(ElvisError elvisError) {
            if (a.this.k == this) {
                a.this.k = null;
                if (this.f1641c != null) {
                    this.f1641c.onError(elvisError);
                    this.f1641c = null;
                }
            }
        }

        static /* synthetic */ void a(b bVar, ElvisResult elvisResult) {
            if (a.this.k == bVar) {
                a.this.k = null;
                if (bVar.f1641c != null) {
                    bVar.f1641c.onResult(elvisResult);
                    bVar.f1641c = null;
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public final void a() {
            a(true);
            a(new ElvisError(0));
        }

        /* access modifiers changed from: 0000 */
        public final void a(AudioSource audioSource, final List list) {
            boolean z = this.f1644f;
            if (!this.f1644f) {
                this.f1644f = true;
                this.j = null;
                connectAudioSource(audioSource);
                a.this.f1572d.post(new Runnable() {
                    public final void run() {
                        b.this.f1645g = false;
                        b.this.f1646h = false;
                        boolean b2 = a.this.f1570b.b(list);
                        a.b(a.this);
                        if (!b2) {
                            a.this.f1569a.post(new Runnable() {
                                public final void run() {
                                    b.this.f1644f = false;
                                    b.this.a(true);
                                    b.this.a(new ElvisError(3));
                                }
                            });
                        }
                    }
                });
            }
        }

        /* access modifiers changed from: 0000 */
        public final void a(AudioSource audioSource, final List list, final int i2) {
            boolean z = this.f1644f;
            if (!this.f1644f) {
                this.f1644f = true;
                this.j = null;
                connectAudioSource(audioSource);
                a.this.f1572d.post(new Runnable() {
                    public final void run() {
                        b.this.f1645g = false;
                        b.this.f1646h = true;
                        boolean a2 = a.this.f1570b.a(list, i2);
                        a.b(a.this);
                        if (!a2) {
                            a.this.f1569a.post(new Runnable() {
                                public final void run() {
                                    b.this.f1644f = false;
                                    b.this.a(true);
                                    b.this.a(new ElvisError(3));
                                }
                            });
                        }
                    }
                });
            }
        }

        /* access modifiers changed from: 0000 */
        public final void a(final boolean z) {
            this.f1642d = null;
            this.f1643e = null;
            disconnectAudioSource();
            if (this.f1644f) {
                this.f1644f = false;
                a.this.f1572d.post(new Runnable() {
                    public final void run() {
                        boolean z;
                        if (!b.this.i) {
                            if (b.this.f1645g) {
                                e a2 = a.this.f1570b;
                                boolean z2 = z;
                                z = a2.d();
                            } else {
                                z = !b.this.f1646h ? a.this.f1570b.a(z) : true;
                            }
                            b.this.i = true;
                            final ElvisResult f2 = (z || !z) ? null : b.this.f1646h ? a.this.f1570b.f() : a.this.f1570b.a(b.this.f1640b);
                            if (!b.this.f1646h) {
                                a.this.f1570b.g();
                            } else if (!a.this.f1570b.e()) {
                                f2 = null;
                            }
                            a.b(a.this);
                            if (!z) {
                                a.this.f1569a.post(new Runnable() {
                                    public final void run() {
                                        if (f2 == null) {
                                            b.this.a(new ElvisError(2));
                                        } else {
                                            b.a(b.this, f2);
                                        }
                                    }
                                });
                            }
                        }
                    }
                });
            }
        }

        public final void chunksAvailable(AudioSource audioSource) {
            final short[] sArr;
            if (a.this.k != this) {
                Logger.warn(this, "Got audio for old recognition");
                return;
            }
            List allAudioChunksForSink = audioSource.getAllAudioChunksForSink(this);
            for (int i2 = 0; i2 < allAudioChunksForSink.size(); i2++) {
                AudioChunk audioChunk = (AudioChunk) allAudioChunksForSink.get(i2);
                if (this.j != null) {
                    sArr = new short[(this.j.length + audioChunk.audioShorts.length)];
                    System.arraycopy(this.j, 0, sArr, 0, this.j.length);
                    System.arraycopy(audioChunk.audioShorts, 0, sArr, this.j.length, audioChunk.audioShorts.length);
                    this.j = null;
                } else {
                    sArr = audioChunk.audioShorts;
                }
                if (sArr.length < audioChunk.audioType.getSampleCount(40)) {
                    this.j = sArr;
                } else {
                    a.this.f1572d.post(new Runnable() {
                        public final void run() {
                            if (!b.this.i) {
                                final int a2 = b.this.f1646h ? a.this.f1570b.b(sArr) : a.this.f1570b.a(sArr);
                                b.this.i = a2 == 2;
                                final ElvisResult elvisResult = b.this.i ? b.this.f1646h ? a.this.f1570b.f() : a.this.f1570b.a(b.this.f1640b) : null;
                                if (b.this.f1646h && b.this.i) {
                                    a.this.f1570b.e();
                                }
                                a.b(a.this);
                                if (a2 != 0) {
                                    a.this.f1569a.post(new Runnable() {
                                        public final void run() {
                                            if (a.this.k == b.this) {
                                                if (a2 != 1) {
                                                    b.this.disconnectAudioSource();
                                                    if (a2 == 2) {
                                                        a aVar = a.this;
                                                        if (b.this.f1643e != null) {
                                                            b.this.f1643e.onEndOfSpeech();
                                                            b.this.f1643e = null;
                                                        }
                                                    } else {
                                                        a aVar2 = a.this;
                                                    }
                                                    if (elvisResult == null) {
                                                        b.this.a(new ElvisError(2));
                                                    } else {
                                                        b.a(b.this, elvisResult);
                                                    }
                                                    b.this.f1644f = false;
                                                } else if (b.this.f1642d != null) {
                                                    a aVar3 = a.this;
                                                    b.this.f1642d.onStartOfSpeech();
                                                    b.this.f1642d = null;
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
            }
        }

        public final void framesDropped(AudioSource audioSource) {
            if (a.this.k == this) {
                a(new ElvisError(1));
            }
        }

        public final void sourceClosed(AudioSource audioSource) {
            if (a.this.k == this) {
                a(false);
            }
        }
    }

    a(e eVar, Handler handler) {
        this.f1570b = eVar;
        if (handler == null) {
            this.f1572d = a();
        } else {
            this.f1572d = handler;
        }
    }

    private void b() {
        if (this.k != null) {
            b bVar = this.k;
            this.k.a();
            if (bVar == this.k) {
                this.k = null;
            }
        }
    }

    static /* synthetic */ void b(a aVar) {
        int i2 = aVar.f1576h;
        int h2 = aVar.f1570b.h();
        if (i2 != h2) {
            if (h2 == 3) {
                if (aVar.j != null) {
                    if (!aVar.f1570b.a("log", aVar.j, aVar.i)) {
                        aVar.j = null;
                    }
                }
                aVar.f1576h = 4;
                return;
            }
            aVar.f1576h = h2;
        }
    }

    /* access modifiers changed from: protected */
    public abstract Handler a();

    public void cancelRebuild() {
        if (this.l != null) {
            this.l.a();
            this.l = null;
        }
    }

    public final void cancelRecognition() {
        d.a((Object) this, !this.o);
        d.a((Object) this, this.p);
        b();
    }

    public void checkWakeupPhrases(final List list, final WakeupCheckListener wakeupCheckListener) {
        d.a("wakeupPhrases", (Object) list);
        d.a("wakeupCheckListener", (Object) wakeupCheckListener);
        d.a((Object) this, !this.o);
        d.a((Object) this, this.p);
        this.f1572d.post(new Runnable() {
            public final void run() {
                if (!a.this.n) {
                    a.this.f1569a.post(new Runnable() {
                        public final void run() {
                            wakeupCheckListener.onError(new ElvisError(9));
                        }
                    });
                    return;
                }
                final ArrayList arrayList = new ArrayList();
                for (String str : list) {
                    if (!a.this.f1570b.c(str)) {
                        arrayList.add(str);
                    }
                }
                a.this.f1569a.post(new Runnable() {
                    public final void run() {
                        wakeupCheckListener.onComplete(arrayList);
                    }
                });
            }
        });
    }

    public void clearGrammar(final RebuildListener rebuildListener) {
        cancelRebuild();
        cancelRecognition();
        final ArrayList arrayList = new ArrayList();
        this.f1571c = new Grammar();
        this.f1572d.post(new Runnable() {
            public final void run() {
                if (!a.this.n) {
                    a.this.f1569a.post(new Runnable() {
                        public final void run() {
                            if (rebuildListener != null) {
                                rebuildListener.onError(new ElvisError(9));
                            }
                        }
                    });
                }
                final boolean c2 = a.this.f1570b.c();
                a.this.f1569a.post(new Runnable() {
                    public final void run() {
                        if (rebuildListener == null) {
                            return;
                        }
                        if (c2) {
                            rebuildListener.onComplete(new Grammar(), arrayList);
                        } else {
                            rebuildListener.onError(new ElvisError(5));
                        }
                    }
                });
            }
        });
    }

    public void enableRecognitionLogging(final String str, final String str2, final int i2) {
        d.a((Object) this, !this.o);
        this.f1572d.post(new Runnable() {
            public final void run() {
                if (str2 == null || i2 <= 0) {
                    if (a.this.f1570b.a((String) null, 0)) {
                        a.this.j = null;
                        a.this.i = null;
                    }
                } else if (a.this.f1570b.a("log", i2)) {
                    a.this.j = str;
                    a.this.i = str2;
                }
                a.b(a.this);
            }
        });
    }

    public void enableVerboseAndroidLogging(boolean z) {
        this.f1570b.b(z);
    }

    public Constraint findConstraint(String str) {
        return this.f1571c.getConstraintByName(str);
    }

    public WordSlot findWordSlot(String str) {
        return this.f1571c.getWordSlotByName(str);
    }

    public List getAvailableModels() {
        ElvisLanguage[] elvisLanguageArr;
        ArrayList arrayList = new ArrayList();
        this.f1570b.a((List) arrayList);
        ArrayList arrayList2 = new ArrayList(arrayList.size());
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            g gVar = (g) it.next();
            int i2 = gVar.f1668b * 1000;
            if (i2 == 16000 || i2 == 8000) {
                for (ElvisLanguage elvisLanguage : Languages.ALL_LANGUAGES) {
                    if (elvisLanguage.name.equals(gVar.f1667a)) {
                        arrayList2.add(new ElvisModelInfo(elvisLanguage, i2));
                    }
                }
            }
        }
        return arrayList2;
    }

    public List getAvailableModels(int i2) {
        List availableModels = getAvailableModels();
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= availableModels.size()) {
                return availableModels;
            }
            if (((ElvisModelInfo) availableModels.get(i4)).frequencyHz != i2) {
                availableModels.remove(i4);
                i4--;
            }
            i3 = i4 + 1;
        }
    }

    public List getAvailableModels(ElvisLanguage elvisLanguage) {
        List availableModels = getAvailableModels();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= availableModels.size()) {
                return availableModels;
            }
            if (!((ElvisModelInfo) availableModels.get(i3)).language.equals(elvisLanguage)) {
                availableModels.remove(i3);
                i3--;
            }
            i2 = i3 + 1;
        }
    }

    public int getRecognitionLoggingStatus() {
        return this.f1576h;
    }

    public void initialize(ElvisConfig elvisConfig, String str) {
        initialize(elvisConfig, str, null);
    }

    public void initialize(final ElvisConfig elvisConfig, final String str, final InitializeListener initializeListener) {
        boolean z = false;
        d.a((Object) this, !this.o);
        if (!this.p) {
            z = true;
        }
        d.a((Object) this, z, "Already initialized");
        d.a("stateFileName", str);
        d.a("config", (Object) elvisConfig);
        this.p = true;
        this.f1572d.post(new Runnable() {
            public final void run() {
                a.this.m = str;
                String str = elvisConfig.f1533a;
                int i = elvisConfig.f1535c;
                String str2 = elvisConfig.f1534b;
                if (str2 == null && str != null) {
                    str2 = d.a(str, i / 1000);
                }
                final boolean a2 = a.this.f1570b.a(str, i, str2);
                if (a2) {
                    a.this.n = true;
                }
                a.this.f1569a.post(new Runnable() {
                    public final void run() {
                        if (initializeListener != null) {
                            initializeListener.onLoaded(a.this, a2);
                        }
                    }
                });
            }
        });
    }

    public void loadGrammar(Grammar grammar, RebuildListener rebuildListener) {
        d.a("grammar", (Object) grammar);
        d.a("listener", (Object) rebuildListener);
        d.a((Object) this, !this.o);
        d.a((Object) this, this.p);
        cancelRebuild();
        cancelRecognition();
        this.l = new C0006a(grammar, this.f1571c, rebuildListener);
        this.l.a(false);
    }

    public void loadState(RebuildListener rebuildListener) {
        loadStateAndGrammar(null, rebuildListener);
    }

    public void loadStateAndGrammar(final Grammar grammar, final RebuildListener rebuildListener) {
        d.a("rebuildListener", (Object) rebuildListener);
        d.a((Object) this, !this.o);
        d.a((Object) this, this.p);
        cancelRebuild();
        cancelRecognition();
        this.f1571c.b();
        this.f1571c.a();
        final C0006a aVar = grammar != null ? new C0006a(grammar, this.f1571c, rebuildListener) : null;
        this.l = aVar;
        this.f1572d.post(new Runnable() {
            public final void run() {
                if (!a.this.n) {
                    a.this.f1569a.post(new Runnable() {
                        public final void run() {
                            if (aVar == a.this.l) {
                                Logger.error(a.this, "Cannot load Elvis State or Grammar.  Elvis is not initialized. ");
                                rebuildListener.onError(new ElvisError(9));
                            }
                        }
                    });
                    return;
                }
                a.this.f1570b.a();
                final String f2 = a.this.m;
                final Grammar a2 = a.this.f1570b.a(a.this.m);
                final ArrayList arrayList = new ArrayList();
                a.this.f1569a.post(new Runnable() {
                    public final void run() {
                        if (aVar != a.this.l) {
                            return;
                        }
                        if (a2 == null && grammar == null) {
                            Logger.error(a.this, "Error loading Elvis state from: " + f2);
                            rebuildListener.onError(new ElvisError(7));
                        } else if (grammar != null) {
                            a.this.l = new C0006a(grammar, a2, rebuildListener);
                            if (a2 == null) {
                                Logger.warn(a.this, "Unable to load Elvis state from: " + f2 + ", doing a full grammar rebuild");
                                a.this.l.a(true);
                                return;
                            }
                            a aVar = a.this;
                            a.this.l.a(false);
                        } else {
                            a.this.f1571c = a2;
                            rebuildListener.onComplete(a2, arrayList);
                        }
                    }
                });
            }
        });
    }

    public void release() {
        release(null);
    }

    public void release(final ReleaseListener releaseListener) {
        d.a((Object) this, !this.o);
        if (!this.p) {
            this.o = true;
            if (releaseListener != null) {
                releaseListener.onReleased(this);
                return;
            }
            return;
        }
        b();
        this.f1572d.post(new Runnable() {
            public final void run() {
                if (a.this.n) {
                    a.this.f1570b.b();
                }
                a.this.n = false;
                a.this.o = true;
                if (releaseListener != null) {
                    a.this.f1569a.post(new Runnable() {
                        public final void run() {
                            releaseListener.onReleased(a.this);
                        }
                    });
                }
            }
        });
    }

    public void saveState() {
        saveState(null);
    }

    public void saveState(final SaveListener saveListener) {
        d.a((Object) this, !this.o);
        d.a((Object) this, this.p);
        final Grammar grammar = this.f1571c;
        this.f1572d.post(new Runnable() {
            public final void run() {
                final boolean a2 = a.this.f1570b.a(a.this.m, grammar);
                if (saveListener != null) {
                    a.this.f1569a.post(new Runnable() {
                        public final void run() {
                            saveListener.onSaved(a.this, a2);
                        }
                    });
                }
            }
        });
    }

    public void setActiveConstraintNames(List list) {
        d.a((Object) this, !this.o);
        d.a((Object) this, this.p);
        this.f1573e.clear();
        if (list != null) {
            this.f1573e.addAll(list);
        }
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.elvis.Constraint>, for r4v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.elvis.Constraint>] */
    public void setActiveConstraints(List<Constraint> list) {
        d.a((Object) this, !this.o);
        d.a((Object) this, this.p);
        this.f1573e.clear();
        if (list != null) {
            this.f1573e.ensureCapacity(list.size());
            for (Constraint id : list) {
                this.f1573e.add(id.getId());
            }
        }
    }

    public void setCustomPronunciations(final List list) {
        d.a("words", (Object) list);
        d.a((Object) this, !this.o);
        d.a((Object) this, this.p);
        this.f1572d.post(new Runnable() {
            public final void run() {
                if (a.this.n) {
                    for (Word word : list) {
                        if (a.this.f1570b.a(word) != 0) {
                            Logger.error(this, "Unable to set custom pronunciation for " + word.toString());
                        }
                    }
                    return;
                }
                Logger.error(this, "Unable to set custom pronunciations. Elvis is not created.");
            }
        });
    }

    public void setWakeupPenalty(int i2) {
        this.f1575g = i2;
    }

    public void setWakeupPhrases(List list) {
        this.f1574f.clear();
        if (list != null) {
            this.f1574f.addAll(list);
        }
    }

    public final void startRecognition(AudioSource audioSource, SpeechDetectionListener speechDetectionListener, ResultListener resultListener) {
        boolean z = true;
        d.a((Object) this, !this.o);
        d.a((Object) this, this.p);
        if (this.l != null) {
            z = false;
        }
        d.a((Object) this, z, "A rebuild is in progress");
        d.a("audioSource", (Object) audioSource);
        d.a("resultListener", (Object) resultListener);
        if (this.l == null) {
            cancelRecognition();
            this.k = new b(this.f1571c, resultListener, speechDetectionListener);
            this.k.a(audioSource, (List) this.f1573e);
        }
    }

    public final void startSpeechDetection(AudioSource audioSource, SpeechDetectionListener speechDetectionListener) {
        boolean z = false;
        d.a((Object) this, !this.o);
        d.a((Object) this, this.p);
        if (this.l == null) {
            z = true;
        }
        d.a((Object) this, z, "A rebuild is in progress");
        d.a("audioSource", (Object) audioSource);
        d.a("speechDetectionListener", (Object) speechDetectionListener);
        if (this.l == null) {
            cancelRecognition();
            ArrayList arrayList = new ArrayList(1);
            arrayList.add("~");
            this.k = new b(this.f1571c, null, speechDetectionListener);
            this.k.a(audioSource, (List) arrayList);
        }
    }

    public final void startWakeupMode(AudioSource audioSource, ResultListener resultListener) {
        boolean z = true;
        d.a((Object) this, !this.o);
        d.a((Object) this, this.p);
        if (this.l != null) {
            z = false;
        }
        d.a((Object) this, z, "A rebuild is in progress");
        d.a("audioSource", (Object) audioSource);
        d.a("resultListener", (Object) resultListener);
        if (this.l == null) {
            cancelRecognition();
            this.k = new b(this.f1571c, resultListener, null);
            this.k.a(audioSource, this.f1574f, this.f1575g);
        }
    }

    public final void stopListening() {
        d.a((Object) this, !this.o);
        d.a((Object) this, this.p);
        if (this.k != null) {
            this.k.a(false);
        }
    }
}
