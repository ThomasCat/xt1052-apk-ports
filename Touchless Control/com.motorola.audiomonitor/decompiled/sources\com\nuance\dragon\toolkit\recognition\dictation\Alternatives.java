package com.nuance.dragon.toolkit.recognition.dictation;

public interface Alternatives {
    Alternative getAlternativeAt(int i);

    int size();
}
