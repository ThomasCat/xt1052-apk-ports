package a;

import android.util.Log;
import com.motorola.audiomonitor.comsocket.d;
import com.motorola.audiomonitor.comsocket.e;

class j implements e {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ i f42a;

    j(i iVar) {
        this.f42a = iVar;
    }

    public void a(d dVar) {
        if (dVar == d.RECOG_LINK) {
            if (i.f35b) {
                Log.d(i.f34a, "AudioRecog - recog audio recording start");
            }
        } else if (dVar == d.CONFLICT_LINK) {
            if (i.f35b) {
                Log.d(i.f34a, "AudioRecog - conflicting audio recording start");
            }
            if (this.f42a.f37d != null) {
                this.f42a.f37d.b();
            }
        } else if (i.f35b) {
            Log.d(i.f34a, "AudioRecog - non-conflicting audio recording start");
        }
    }

    public void b(d dVar) {
        if (dVar == d.RECOG_LINK) {
            if (i.f35b) {
                Log.d(i.f34a, "AudioRecog - recog audio recording stopped");
            }
            this.f42a.f39f.removeCallbacks(this.f42a.l);
            if (this.f42a.n != null) {
                this.f42a.n.a();
            }
            if (this.f42a.f38e != null) {
                this.f42a.f38e.a(false);
            }
        } else if (dVar == d.CONFLICT_LINK) {
            if (i.f35b) {
                Log.d(i.f34a, "AudioRecog - conflicting audio recording stopped");
            }
            if (this.f42a.f37d != null) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e2) {
                }
                this.f42a.f37d.a();
            }
        } else {
            Log.d(i.f34a, "AudioRecog - non-conflicting audio recording stopped");
        }
    }
}
