package com.motorola.audiomonitor.discovery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.motorola.audiomonitor.a;

public class DiscoveryReceiver extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    private static final String f476a = ("AMonitor." + DiscoveryReceiver.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f477b = a.f160b;

    public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.getAction() != null) {
            if (f477b) {
                Log.w(f476a, "Received intent with action: " + intent.getAction());
            }
            Intent intent2 = new Intent(context, DiscoveryIntentService.class);
            intent2.putExtras(intent);
            intent2.putExtra("com.motorola.audiomonitor.extra.DISCOVERY_ACTION", intent.getAction());
            context.startService(intent2);
        }
    }
}
