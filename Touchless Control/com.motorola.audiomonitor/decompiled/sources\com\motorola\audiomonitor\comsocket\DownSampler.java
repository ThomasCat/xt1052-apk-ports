package com.motorola.audiomonitor.comsocket;

import android.util.Log;
import com.motorola.audiomonitor.ProtectedByteBuffer;
import com.motorola.audiomonitor.a;

public class DownSampler {

    /* renamed from: a reason: collision with root package name */
    private static final String f431a = ("AMonitor." + DownSampler.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f432b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private long f433c = 0;

    static {
        System.loadLibrary("rateconv");
    }

    public DownSampler(int i, int i2) {
        if (i > i2) {
            this.f433c = samplerInit(i, i2);
            if (this.f433c == 0) {
                throw new RuntimeException("Failed to init native sampler");
            }
            return;
        }
        Log.e(f431a, "No supported downsampling. src=" + i + " tgt=" + i2);
        throw new IllegalArgumentException("Invalid sampling rate");
    }

    private native int downsample(long j, ProtectedByteBuffer protectedByteBuffer, int i, int i2, ProtectedByteBuffer protectedByteBuffer2);

    private native void samplerClean(long j);

    private native long samplerInit(int i, int i2);

    public int a(ProtectedByteBuffer protectedByteBuffer, int i, int i2, ProtectedByteBuffer protectedByteBuffer2) {
        return downsample(this.f433c, protectedByteBuffer, i, i2 / 2, protectedByteBuffer2) * 2;
    }

    public void a() {
        if (this.f433c != 0) {
            samplerClean(this.f433c);
            this.f433c = 0;
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        if (this.f433c != 0) {
            Log.w(f431a, "finalize: this=" + this);
        }
        a();
    }
}
