package com.motorola.audiomonitor.uis;

import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;

class v implements OnPreferenceChangeListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ u f760a;

    v(u uVar) {
        this.f760a = uVar;
    }

    public boolean onPreferenceChange(Preference preference, Object obj) {
        if (obj instanceof Boolean) {
            Boolean bool = (Boolean) obj;
            if (!((CheckBoxPreference) preference).isChecked() && bool.booleanValue()) {
                this.f760a.a();
                return false;
            }
        }
        return true;
    }
}
