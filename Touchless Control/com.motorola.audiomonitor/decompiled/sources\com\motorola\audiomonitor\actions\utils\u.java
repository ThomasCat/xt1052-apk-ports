package com.motorola.audiomonitor.actions.utils;

import android.content.Context;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.util.SparseIntArray;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class u {

    /* renamed from: a reason: collision with root package name */
    private static int[] f270a = {2, 1, 3};

    /* renamed from: b reason: collision with root package name */
    private final Map f271b = new HashMap();

    /* renamed from: c reason: collision with root package name */
    private final SparseIntArray f272c;

    public u(Context context) {
        int[] iArr;
        int length = f270a.length;
        this.f272c = new SparseIntArray();
        int i = length;
        for (int i2 : f270a) {
            this.f271b.put(((String) Phone.getTypeLabel(context.getResources(), i2, "")).toLowerCase(Locale.getDefault()), Integer.valueOf(i2));
            this.f272c.put(i2, i);
            i--;
        }
    }

    public int a(int i) {
        Integer valueOf = Integer.valueOf(this.f272c.get(i));
        if (valueOf == null) {
            valueOf = Integer.valueOf(0);
        }
        return valueOf.intValue();
    }

    public int a(String str) {
        Integer num = (Integer) this.f271b.get(str);
        if (num != null) {
            return num.intValue();
        }
        return -1;
    }

    public String[] a() {
        return (String[]) this.f271b.keySet().toArray(new String[this.f271b.size()]);
    }

    public void b() {
        this.f271b.clear();
        this.f272c.clear();
    }
}
