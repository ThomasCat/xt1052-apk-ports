package com.nuance.dragon.toolkit.util.internal;

public final class e {
    public static String a(Class cls) {
        String simpleName = cls.getSimpleName();
        if (simpleName == null) {
            return null;
        }
        if (simpleName.length() > 0) {
            return simpleName;
        }
        String name = cls.getName();
        if (name == null) {
            return null;
        }
        int lastIndexOf = name.lastIndexOf(46);
        if (lastIndexOf < 0) {
            return name;
        }
        int i = lastIndexOf + 1;
        return i < name.length() ? name.substring(i) : name;
    }
}
