package com.nuance.a.a.a.b.a;

import com.motorola.audiomonitor.GenericRecognizer.UdtEnrollResult;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.a.b.a.b;
import com.nuance.a.a.a.a.b.a.b.C0002b;
import com.nuance.a.a.a.a.b.a.d;
import com.nuance.a.a.a.a.b.a.d.C0003d;
import com.nuance.a.a.a.a.b.a.d.e;
import com.nuance.a.a.a.a.b.a.d.f;
import com.nuance.a.a.a.b.b.c;
import com.nuance.dragon.toolkit.audio.pipes.SpeexEncoderPipe;
import java.util.Enumeration;
import java.util.Vector;

public class a implements C0002b, com.nuance.a.a.a.a.b.a.d.a, C0003d, e, f {
    /* access modifiers changed from: private */
    public static C0001a i = com.nuance.a.a.a.a.b.a.a.a(a.class);
    private C0004a A;

    /* renamed from: a reason: collision with root package name */
    protected String f801a = null;

    /* renamed from: b reason: collision with root package name */
    protected short f802b = 0;

    /* renamed from: c reason: collision with root package name */
    b f803c = null;

    /* renamed from: d reason: collision with root package name */
    protected byte f804d;

    /* renamed from: e reason: collision with root package name */
    public byte[] f805e;

    /* renamed from: f reason: collision with root package name */
    protected int f806f;

    /* renamed from: g reason: collision with root package name */
    protected short f807g;

    /* renamed from: h reason: collision with root package name */
    protected short f808h;
    private com.nuance.a.a.a.a.b.a.e.a j;
    private int k = 30;
    private com.nuance.a.a.a.a.b.a.e.a l;
    /* access modifiers changed from: private */
    public int m = 50;
    private com.nuance.a.a.a.a.b.a.e.a n;
    private Vector o;
    private boolean p = false;
    private Vector q = null;
    private Object r;
    private com.nuance.a.a.a.a.c.b s = null;
    /* access modifiers changed from: private */
    public short t = 0;
    private short u = 9;
    private String v = "Not specified";
    private String w = "Not specified";
    private byte[] x = null;
    private d y = null;
    private String z = "";

    /* renamed from: com.nuance.a.a.a.b.a.a$a reason: collision with other inner class name */
    public interface C0004a {
        void a(com.nuance.a.a.a.a.c.b bVar, byte[] bArr);

        void a(short s, short s2);
    }

    public a(String str, short s2, String str2, byte[] bArr, String str3, C0004a aVar, Vector vector, b bVar) {
        this.f801a = str;
        this.f802b = s2;
        this.v = str2;
        this.x = bArr;
        this.w = str3;
        this.A = aVar;
        if (vector != null) {
            this.o = vector;
        } else {
            this.o = new Vector();
        }
        this.f803c = bVar;
        if (i.b()) {
            i.b((Object) "XMode() server: " + str + " port: " + s2);
        }
        Enumeration elements = vector.elements();
        while (elements.hasMoreElements()) {
            c cVar = (c) elements.nextElement();
            if (i.b()) {
                i.b((Object) "XMode() " + cVar.d() + " : " + cVar.a() + " = " + new String(cVar.b()));
            }
            if (cVar.d() == com.nuance.a.a.a.b.b.c.a.f826a) {
                if (cVar.a().equals("IdleSessionTimeout")) {
                    int parseInt = Integer.parseInt(new String(cVar.b()));
                    if (parseInt > 0) {
                        this.m = parseInt;
                    }
                } else if (cVar.a().equals("ConnectionTimeout")) {
                    this.k = Integer.parseInt(new String(cVar.b()));
                } else if (cVar.a().equals("SSL_Socket_Enable") || cVar.a().equals("SSL_Cert_Summary") || cVar.a().equals("SSL_Cert_Data") || cVar.a().equals("SSL_SelfSigned_Cert")) {
                    if (cVar.a().equals("SSL_Socket_Enable") && (new String(cVar.b()).equals("TRUE") || new String(cVar.b()).equals("true"))) {
                        this.p = true;
                    }
                    if (this.q == null) {
                        this.q = new Vector();
                    }
                    this.q.addElement(cVar);
                }
            }
        }
        this.f804d = 3;
    }

    /* access modifiers changed from: private */
    public void a(byte b2) {
        try {
            this.f803c.a(new com.nuance.a.a.a.a.b.a.b.a(b2, null), this, Thread.currentThread(), this.f803c.a()[0]);
        } catch (Exception e2) {
            if (i.e()) {
                i.e("XMode.sendCmdMsg() " + e2.getMessage());
            }
        }
    }

    /* JADX INFO: finally extract failed */
    private void a(com.nuance.a.a.a.a.c.b bVar, byte[] bArr) {
        if (i.b()) {
            i.b((Object) "XMode.parseXModeMsg() protocol: " + bVar.f793a + " cmd: " + bVar.f795c);
        }
        switch (bVar.f793a) {
            case 1:
                if (this.n != null && this.f803c.a(this.n)) {
                    this.n = new com.nuance.a.a.a.a.b.a.e.a() {
                        public final void run() {
                            try {
                                a.this.t = 3;
                                if (a.i.e()) {
                                    a.i.e("Session Idle for too long, longer than [" + a.this.m + "] (initiated from XMode.parseVap)");
                                }
                                a.this.f804d = 2;
                                a.this.a(4);
                            } catch (Exception e2) {
                                if (a.i.e()) {
                                    a.i.e("XMode.parseXModeMsg() " + e2.getClass().getName() + " " + e2.getMessage());
                                }
                            }
                        }
                    };
                    this.f803c.a(this.n, (long) (this.m * 1000));
                }
                this.A.a(bVar, bArr);
                return;
            case 2:
                if (this.n != null && this.f803c.a(this.n)) {
                    this.n = new com.nuance.a.a.a.a.b.a.e.a() {
                        public final void run() {
                            try {
                                a.this.t = 3;
                                if (a.i.e()) {
                                    a.i.e("Session Idle for too long, longer than [" + a.this.m + "] (initiated from XMode.parseBcp)");
                                }
                                a.this.f804d = 2;
                                a.this.a(4);
                            } catch (Exception e2) {
                                if (a.i.e()) {
                                    a.i.e("XMode.parseXModeMsg() " + e2.getClass().getName() + " " + e2.getMessage());
                                }
                            }
                        }
                    };
                    this.f803c.a(this.n, (long) (this.m * 1000));
                }
                this.A.a(bVar, bArr);
                return;
            case 3:
                switch (bVar.f795c) {
                    case 257:
                        this.f803c.a(this.l);
                        this.f805e = new byte[16];
                        System.arraycopy(bArr, 0, this.f805e, 0, 16);
                        b bVar2 = this.f803c;
                        byte[] bArr2 = this.f805e;
                        d dVar = this.y;
                        Object obj = this.r;
                        byte[] bArr3 = this.f805e;
                        this.z = C0001a.b(this.f805e);
                        C0001a aVar = i;
                        String str = this.z;
                        aVar.f();
                        try {
                            if (i.b()) {
                                i.b((Object) "Received COP_Connected " + this.z);
                            }
                            a(10);
                            i.g();
                            this.n = new com.nuance.a.a.a.a.b.a.e.a() {
                                public final void run() {
                                    try {
                                        a.this.t = 3;
                                        if (a.i.e()) {
                                            a.i.e("Session Idle for too long, longer than [" + a.this.m + "] (initiated from XMode.parseCopConnected)");
                                        }
                                        a.this.f804d = 2;
                                        a.this.a(4);
                                    } catch (Exception e2) {
                                        if (a.i.e()) {
                                            a.i.e("XMode.parseXModeMsg() " + e2.getClass().getName() + " " + e2.getMessage());
                                        }
                                    }
                                }
                            };
                            this.f803c.a(this.n, (long) (this.m * 1000));
                            break;
                        } catch (Throwable th) {
                            i.g();
                            throw th;
                        }
                    case 258:
                        this.f806f = com.nuance.a.a.a.a.d.b.b(bArr, 0);
                        a(8);
                        break;
                    case UdtEnrollResult.CHECKRECORDING_SPOT /*512*/:
                        if (this.n != null) {
                            this.f803c.a(this.n);
                        }
                        this.u = com.nuance.a.a.a.a.d.b.a(bArr, 0);
                        this.f804d = 2;
                        this.t = 6;
                        if (i.d()) {
                            i.d("XMode.parseXModeMsgCopDisconnect() Received COP DISCONNECT. ");
                        }
                        a(4);
                        break;
                    case 768:
                        this.t = 7;
                        this.f804d = 2;
                        if (i.e()) {
                            i.e("XMode.parseXModeMsgCopConnectFailed() COP CONNECT failure. ");
                        }
                        a(4);
                        break;
                }
                this.A.a(bVar, bArr);
                return;
            case SpeexEncoderPipe.DEFAULT_VAD_BEGIN_LEN /*15*/:
                return;
            default:
                if (i.e()) {
                    i.e("XMode.parseXModeMsg() unknown protocol: " + Integer.toHexString(bVar.f793a));
                    return;
                }
                return;
        }
    }

    public final void a() {
        if (i.b()) {
            i.b((Object) "XMode.closeSocketCallback() " + this.z);
        }
        this.f804d = 3;
        if (this.j != null) {
            this.f803c.a(this.j);
            this.j = null;
        }
        if (this.n != null) {
            this.f803c.a(this.n);
            this.n = null;
        }
        if (this.r != null) {
            this.A.a(this.t, this.u);
        }
        this.r = null;
        this.y = null;
        this.f805e = null;
        this.z = "";
    }

    public final void a(int i2) {
        if (i.b()) {
            i.b((Object) "XMode.startStreaming() audio id: " + i2);
        }
        if (this.f804d == 1) {
            byte[] bArr = new byte[6];
            com.nuance.a.a.a.a.d.b.a(i2, bArr, 0);
            com.nuance.a.a.a.a.d.b.a(this.f807g, bArr, 4);
            a(com.nuance.a.a.a.a.c.a.a(1, 18, 257, bArr), (Object) "SEND_VAP_RECORD_BEGIN");
        }
    }

    public final void a(d.c cVar, Object obj) {
        if (i.b()) {
            i.b((Object) "XMode.openSocketCallback() ");
        }
        if (cVar == d.c.f788a) {
            this.r = obj;
            if (this.f804d == 0) {
                a(7);
                return;
            }
            this.f804d = 2;
            this.y.a(this.r);
        } else if (cVar == d.c.f789b) {
            if (i.e()) {
                i.e("XMode.openSocketCallback() NETWORK_ERROR");
            }
            this.f804d = 3;
            this.t = 4;
            this.A.a(this.t, this.u);
            this.r = null;
            this.y = null;
            this.f805e = null;
            this.z = "";
        } else if (cVar == d.c.f790c) {
            this.t = 4;
            if (i.e()) {
                i.e("XMode.openSocketCallback() NETWORK_MEMORY_ERROR");
            }
        }
    }

    public final void a(d.c cVar, Object obj, int i2, int i3, Object obj2) {
        String str = (String) obj2;
        if (cVar == d.c.f788a && i2 == i3) {
            if (str.equals("SEND_COP_CONNECT")) {
                a(5);
            } else if (str.equals("SEND_COP_DISCONNECT")) {
                this.y.a(obj);
            }
        } else if (cVar == d.c.f789b) {
            if (this.t != 1 && this.t != 3) {
                this.t = 8;
                if (i.e()) {
                    i.e("XMode.writeSocketCallback() NETWORK_ERROR");
                }
            }
        } else if (cVar == d.c.f790c && this.t != 1 && this.t != 3) {
            this.t = 8;
            if (i.e()) {
                i.e("XMode.writeSocketCallback() NETWORK_MEMORY_ERROR");
            }
        }
    }

    public final void a(d.c cVar, Object obj, byte[] bArr, int i2, int i3, Object obj2) {
        String str = (String) obj2;
        if (i.b()) {
            i.b((Object) "Read callback");
        }
        if (i.a()) {
            i.a(bArr);
        }
        if (cVar == d.c.f788a) {
            if (str.equals("READ_XMODE_HEADER")) {
                if (i3 == 0) {
                    this.j = new com.nuance.a.a.a.a.b.a.e.a() {
                        public final void run() {
                            try {
                                a.this.a(5);
                            } catch (Exception e2) {
                                if (a.i.e()) {
                                    a.i.e("XMode.readSocketCallback() " + e2.getClass().getName() + " " + e2.getMessage());
                                }
                            }
                        }
                    };
                    this.f803c.a(this.j, 20);
                } else if (i3 == i2) {
                    this.s = new com.nuance.a.a.a.a.c.b(bArr);
                    if (this.s.f796d == 0) {
                        a(this.s, (byte[]) null);
                        a(5);
                    } else if (this.s.f796d > 512000 || this.s.f796d < 0) {
                        this.y.b(obj);
                        a(5);
                    } else {
                        a(6);
                    }
                } else if (i.e()) {
                    i.e("----***---- readSocketCallback fatal error in readSocketCallback NET_CONTEXT_READ_XMODE_HEADER bytesRead:[" + i3 + "] bufferLen:[" + i2 + "]");
                }
            } else if (!str.equals("READ_XMODE_PAYLOAD")) {
            } else {
                if (i3 == 0) {
                    if (i.b()) {
                        i.b((Object) Integer.toHexString(this.s.f795c) + " payload not read bytesRead is 0");
                    }
                    this.j = new com.nuance.a.a.a.a.b.a.e.a() {
                        public final void run() {
                            try {
                                a.this.a(6);
                            } catch (Exception e2) {
                                if (a.i.e()) {
                                    a.i.e("XMode.readSocketCallback() " + e2.getClass().getName() + " " + e2.getMessage());
                                }
                            }
                        }
                    };
                    this.f803c.a(this.j, 20);
                } else if (i3 == i2) {
                    if (this.s.f796d <= i2) {
                        a(this.s, bArr);
                    }
                    a(5);
                } else if (i.e()) {
                    i.e("----***---- readSocketCallback fatal error in readSocketCallback NET_CONTEXT_READ_XMODE_PAYLOAD bytesRead:[" + i3 + "] bufferLen:[" + i2 + "]");
                }
            }
        } else if (cVar == d.c.f789b) {
            if (!(this.t == 1 || this.t == 3 || this.t == 6)) {
                this.t = 8;
                if (i.e()) {
                    i.e("XMode.readSocketCallback() NETWORK_ERROR");
                }
            }
            a(4);
        } else if (cVar == d.c.f790c && this.t != 1 && this.t != 3) {
            this.t = 8;
            if (i.e()) {
                i.e("XMode.readSocketCallback() NETWORK_MEMORY_ERROR");
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0129  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x02ea  */
    public final void a(Object obj, Object obj2) {
        boolean z2;
        String str;
        String str2;
        com.nuance.a.a.a.a.b.a.b.a aVar = (com.nuance.a.a.a.a.b.a.b.a) obj;
        switch (aVar.f783a) {
            case 1:
                if (i.b()) {
                    i.b((Object) "XMode.handleMessage() CMD_CONNECT");
                }
                if (this.f804d == 0) {
                    a(3);
                    return;
                } else {
                    this.A.a(this.t, this.u);
                    return;
                }
            case 2:
                if (i.b()) {
                    i.b((Object) "XMode.handleMessage() CMD_DISCONNECT");
                }
                byte[] bArr = new byte[2];
                com.nuance.a.a.a.a.d.b.a(0, bArr, 0);
                byte[] a2 = com.nuance.a.a.a.a.c.a.a(3, 23, 512, bArr);
                this.y.b(this.r);
                a(a2, (Object) "SEND_COP_DISCONNECT");
                return;
            case 3:
                if (i.b()) {
                    i.b((Object) "XMode.handleMessage() CMD_OPEN_SOCKET");
                }
                if (this.f804d == 0) {
                    this.y = new com.nuance.a.a.a.c.f(this.f803c);
                    if (this.p) {
                        this.y.a(this.f801a, (int) this.f802b, this.q, (C0003d) this, (com.nuance.a.a.a.a.b.a.d.a) this);
                        return;
                    } else {
                        this.y.a(this.f801a, this.f802b, this, this);
                        return;
                    }
                } else {
                    this.A.a(this.t, this.u);
                    return;
                }
            case 4:
                if (i.b()) {
                    i.b((Object) "XMode.handleMessage() CMD_CLOSE_SOCKET");
                }
                if (this.y != null && this.r != null) {
                    this.y.a(this.r);
                    return;
                }
                return;
            case 5:
                if (this.y != null && this.r != null) {
                    this.y.a(this.r, d.b.f787b, new byte[8], 8, this, "READ_XMODE_HEADER");
                    return;
                }
                return;
            case 6:
                if (this.s.f796d > 0 && this.s.f796d <= 512000) {
                    byte[] bArr2 = new byte[this.s.f796d];
                    if (this.y != null && this.r != null) {
                        this.y.a(this.r, d.b.f787b, bArr2, bArr2.length, this, "READ_XMODE_PAYLOAD");
                        return;
                    }
                    return;
                }
                return;
            case 7:
                if (i.b()) {
                    i.b((Object) "XMode.handleMessage() CMD_COP_CONNECT");
                }
                if (this.f804d == 0) {
                    String str3 = "<?xml version=\"1.0\"?><cc><s></s><t>7</t><b>20091023</b><tsc>" + this.f807g + "</tsc><fsc>" + this.f808h + "</fsc><nmaid>" + this.v + "</nmaid><uid>" + this.w + "</uid>";
                    Enumeration elements = this.o.elements();
                    String str4 = str3;
                    boolean z3 = false;
                    while (elements.hasMoreElements()) {
                        c cVar = (c) elements.nextElement();
                        if (cVar.d() == com.nuance.a.a.a.b.b.c.a.f827b) {
                            str4 = str4 + "<nmsp p=\"" + cVar.a() + "\" v=\"" + com.nuance.a.a.a.a.d.d.a(new String(cVar.b())) + "\"/>";
                            if (cVar.a().equals("Ping_IntervalSecs")) {
                                str = str4;
                                z2 = true;
                                if (cVar.d() != com.nuance.a.a.a.b.b.c.a.f828c) {
                                    str2 = str + "<app p=\"" + cVar.a() + "\" v=\"" + com.nuance.a.a.a.a.d.d.a(new String(cVar.b())) + "\"/>";
                                } else {
                                    str2 = str;
                                }
                                z3 = z2;
                                str4 = str2;
                            }
                        }
                        String str5 = str4;
                        z2 = z3;
                        str = str5;
                        if (cVar.d() != com.nuance.a.a.a.b.b.c.a.f828c) {
                        }
                        z3 = z2;
                        str4 = str2;
                    }
                    if (!z3) {
                        str4 = str4 + "<nmsp p=\"Ping_IntervalSecs\" v=\"0\"/>";
                    }
                    byte[] bytes = (str4 + "</cc>").getBytes();
                    int length = bytes.length;
                    int i2 = length + 4 + 1;
                    byte[] bArr3 = new byte[i2];
                    com.nuance.a.a.a.a.d.b.a(i2 - 4, bArr3, 0);
                    bArr3[4] = 0;
                    System.arraycopy(bytes, 0, bArr3, 5, length);
                    a(com.nuance.a.a.a.a.c.a.a(3, 23, 256, bArr3), (Object) "SEND_COP_CONNECT");
                    this.l = new com.nuance.a.a.a.a.b.a.e.a() {
                        public final void run() {
                            try {
                                if (a.i.e()) {
                                    a.i.e("XMode.handleSendCopConnect() COP CONNECT timed out. ");
                                }
                                a.this.a(9);
                            } catch (Exception e2) {
                                if (a.i.e()) {
                                    a.i.e("XMode.handleSendCopConnect() " + e2.getClass().getName() + " " + e2.getMessage());
                                }
                            }
                        }
                    };
                    this.f803c.a(this.l, (long) (this.k * 1000));
                    this.f804d = 1;
                    C0004a aVar2 = this.A;
                    return;
                }
                return;
            case 8:
                if (i.b()) {
                    i.b((Object) "XMode.handleMessage() CMD_COP_PING_RESPONSE");
                }
                byte[] bArr4 = new byte[8];
                com.nuance.a.a.a.a.d.b.a(this.f806f, bArr4, 0);
                com.nuance.a.a.a.a.d.b.a(0, bArr4, 4);
                a(com.nuance.a.a.a.a.c.a.a(3, 23, 259, bArr4), (Object) "SEND_COP_PING_RESPONSE");
                return;
            case 9:
                if (i.b()) {
                    i.b((Object) "XMode.handleMessage() CMD_COP_CONNECT_TIMED_OUT");
                }
                if (this.f804d == 1) {
                    this.f804d = 2;
                    this.t = 5;
                    if (i.e()) {
                        i.e("XMode.handleCopConnectTimeout() COP CONNECT timed out. ");
                    }
                    a(4);
                    return;
                }
                return;
            case 10:
                if (i.b()) {
                    i.b((Object) "XMode.handleMessage() CMD_COP_CONFIRM");
                }
                byte[] b2 = com.nuance.a.a.a.a.b.a.c.b(this.x, this.f805e);
                byte[] bArr5 = new byte[(b2.length + 4)];
                com.nuance.a.a.a.a.d.b.a(b2.length, bArr5, 0);
                System.arraycopy(b2, 0, bArr5, 4, b2.length);
                a(com.nuance.a.a.a.a.c.a.a(3, 23, 262, bArr5), (Object) "SEND_COP_CONFIRM");
                return;
            case 11:
                Object obj3 = aVar.f784b;
                C0004a aVar3 = this.A;
                return;
            case 12:
                Object obj4 = aVar.f784b;
                C0004a aVar4 = this.A;
                return;
            default:
                return;
        }
    }

    public final void a(short s2, short s3) {
        if (i.b()) {
            i.b((Object) "XMode.connect() codec: " + s3);
        }
        if (this.f804d != 1 && this.f804d != 0) {
            if (this.f804d == 2) {
                if (this.j != null) {
                    this.f803c.a(this.j);
                }
                if (this.n != null) {
                    this.f803c.a(this.n);
                }
                this.r = null;
                this.y = null;
                this.f805e = null;
                this.z = "";
                this.f807g = s2;
                this.f808h = s3;
                this.f804d = 0;
                a(1);
            } else if (this.f804d == 3) {
                this.f807g = s2;
                this.f808h = s3;
                this.f804d = 0;
                a(1);
            }
        }
    }

    public final void a(byte[] bArr, int i2) {
        if (i.b()) {
            i.b((Object) "XMode.sendVapRecordMsg() audio id: " + i2);
        }
        if (this.f804d == 1) {
            int length = bArr.length;
            byte[] bArr2 = new byte[(length + 8)];
            com.nuance.a.a.a.a.d.b.a(i2, bArr2, 0);
            com.nuance.a.a.a.a.d.b.a(length, bArr2, 4);
            System.arraycopy(bArr, 0, bArr2, 8, length);
            a(com.nuance.a.a.a.a.c.a.a(1, 18, 513, bArr2), (Object) "SEND_VAP_RECORD");
        }
    }

    public final void a(byte[] bArr, Object obj) {
        if (i.b()) {
            i.b((Object) "XMode.sendXModeMsg() " + obj);
        }
        if (this.n != null && this.f803c.a(this.n)) {
            this.n = new com.nuance.a.a.a.a.b.a.e.a() {
                public final void run() {
                    try {
                        a.this.t = 3;
                        if (a.i.e()) {
                            a.i.e("Session Idle for too long, longer than [" + a.this.m + "] ()");
                        }
                        a.this.f804d = 2;
                        a.this.a(4);
                    } catch (Exception e2) {
                        if (a.i.e()) {
                            a.i.e("XMode.sendXModeMsg() " + e2.getClass().getName() + " " + e2.getMessage());
                        }
                    }
                }
            };
            this.f803c.a(this.n, (long) (this.m * 1000));
        }
        if (this.y != null && this.r != null) {
            this.y.a(this.r, bArr, bArr.length, (f) this, obj);
        }
    }

    public final void b() {
        if (i.b()) {
            i.b((Object) "XMode.disconnect() state:" + this.f804d + ", socket:" + this.r);
        }
        if (this.f804d == 3) {
            this.t = 1;
            this.A.a(this.t, this.u);
        } else if (this.f804d == 2) {
        } else {
            if (this.f804d == 0) {
                this.t = 1;
                this.f804d = 2;
                if (this.r != null) {
                    this.y.a(this.r);
                }
            } else if (this.f804d == 1) {
                this.f804d = 2;
                this.t = 1;
                a(2);
            }
        }
    }

    public final void b(int i2) {
        if (i.b()) {
            i.b((Object) "XMode.sendVapRecordEnd() audio id: " + i2);
        }
        if (this.f804d == 1) {
            byte[] bArr = new byte[4];
            com.nuance.a.a.a.a.d.b.a(i2, bArr, 0);
            a(com.nuance.a.a.a.a.c.a.a(1, 18, 256, bArr), (Object) "SEND_VAP_RECORD_END");
        }
    }
}
