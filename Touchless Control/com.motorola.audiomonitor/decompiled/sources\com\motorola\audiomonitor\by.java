package com.motorola.audiomonitor;

import a.i;
import c.d;
import com.motorola.audiomonitor.actions.utils.h;
import com.motorola.audiomonitor.actions.utils.m;
import h.g;
import h.k;

class by implements Runnable {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ s f385a;

    /* renamed from: b reason: collision with root package name */
    final /* synthetic */ float f386b;

    /* renamed from: c reason: collision with root package name */
    final /* synthetic */ bu f387c;

    by(bu buVar, s sVar, float f2) {
        this.f387c = buVar;
        this.f385a = sVar;
        this.f386b = f2;
    }

    public void run() {
        if (h.a().d() || h.a().e() || h.a().b()) {
            this.f385a.c();
            return;
        }
        g.a().a(this.f386b);
        d.a(this.f387c.f376c, "triggers");
        if (!this.f387c.f381h.get()) {
            synchronized (this.f387c.f381h) {
                k.a(this.f387c.f376c);
                try {
                    this.f387c.f381h.wait(500);
                } catch (InterruptedException e2) {
                }
            }
        }
        if (!m.a().a(false)) {
            i.b(this.f387c.f376c);
        }
        g.a().a((String) null, (String) null);
        this.f385a.c();
    }
}
