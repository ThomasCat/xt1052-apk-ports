package com.motorola.audiomonitor.uis;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.text.TextUtils;
import android.util.Log;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.au;
import h.d;
import h.k;

public class SettingActivity extends Activity implements OnSharedPreferenceChangeListener {

    /* renamed from: a reason: collision with root package name */
    private static final String f630a = ("AMonitor." + SettingActivity.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f631b = a.f160b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public au f632c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public boolean f633d = false;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public final Handler f634e = new Handler();

    /* access modifiers changed from: private */
    public void a() {
        Fragment findFragmentById = getFragmentManager().findFragmentById(16908290);
        if (findFragmentById != null) {
            PreferenceScreen preferenceScreen = ((PreferenceFragment) findFragmentById).getPreferenceScreen();
            if (preferenceScreen != null) {
                preferenceScreen.setEnabled(this.f633d);
            }
            if (findFragmentById instanceof u) {
                ((u) findFragmentById).a(this.f633d);
            }
        }
    }

    public void finish() {
        super.finish();
        overridePendingTransition(0, C0014R.anim.activity_close_exit);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f632c = new au(this, new aq(this));
        Fragment findFragmentByTag = getFragmentManager().findFragmentByTag(f630a);
        if (findFragmentByTag == null) {
            getFragmentManager().beginTransaction().replace(16908290, new u(), f630a).commit();
        } else if (findFragmentByTag.isDetached()) {
            getFragmentManager().beginTransaction().attach(findFragmentByTag).commit();
        }
        ActionBar actionBar = getActionBar();
        actionBar.setIcon(k.b());
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(C0014R.string.app_name);
    }

    public boolean onNavigateUp() {
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
        if (this.f632c != null) {
            this.f632c.b();
        }
        this.f633d = false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
        this.f632c.a();
        a();
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        if (f631b) {
            Log.d(f630a, "pref_change=" + str);
        }
        if (TextUtils.equals("monitor_status", str)) {
            if (sharedPreferences.getBoolean(str, false)) {
                if (d.b(getApplicationContext()) && !a.f166h) {
                    this.f632c.c();
                }
            } else if (a.f166h) {
                this.f632c.d();
            }
        }
        if (TextUtils.equals("disp_off_trig", str)) {
            a.l = sharedPreferences.getBoolean(str, a.l);
        }
        if (TextUtils.equals("lock_bypass", str)) {
            a.m = sharedPreferences.getBoolean(str, a.m);
        }
        if (TextUtils.equals("call_confirmation", str)) {
            a.r = sharedPreferences.getString(str, a.r);
        }
        a();
    }
}
