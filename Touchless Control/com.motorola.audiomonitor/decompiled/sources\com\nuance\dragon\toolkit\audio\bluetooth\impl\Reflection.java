package com.nuance.dragon.toolkit.audio.bluetooth.impl;

import android.util.Log;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Reflection {
    private static void a(Exception exc) {
        Log.w("DMT-Reflection", "Exception received - " + exc);
    }

    public Class getClassForName(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e2) {
            e2.printStackTrace();
            throw new RuntimeException("getClassForName failed", e2);
        }
    }

    public Constructor getConstructor(Class cls, Class... clsArr) {
        try {
            return cls.getConstructor(clsArr);
        } catch (Exception e2) {
            e2.printStackTrace();
            throw new RuntimeException("getConstructor failed", e2);
        }
    }

    public Constructor getConstructorForName(String str, Class... clsArr) {
        Class classForName = getClassForName(str);
        if (classForName != null) {
            return getConstructor(classForName, clsArr);
        }
        return null;
    }

    public Object getFieldValue(Class cls, String str) {
        try {
            return cls.getField(str).get(null);
        } catch (Exception e2) {
            e2.printStackTrace();
            throw new RuntimeException("getFieldValue failed", e2);
        }
    }

    public Method getMethod(Class cls, String str, Class... clsArr) {
        try {
            return cls.getMethod(str, clsArr);
        } catch (Exception e2) {
            e2.printStackTrace();
            throw new RuntimeException("getMethod failed", e2);
        }
    }

    public Method getMethodOrNull(Class cls, String str, Class... clsArr) {
        boolean z = false;
        try {
            return cls.getMethod(str, clsArr);
        } catch (Exception e2) {
            return z;
        }
    }

    public Object newObject(String str) {
        Object obj = null;
        if (str == null) {
            return obj;
        }
        try {
            return Class.forName(str).newInstance();
        } catch (ClassNotFoundException e2) {
            a(e2);
            return obj;
        } catch (IllegalAccessException e3) {
            a(e3);
            return obj;
        } catch (InstantiationException e4) {
            a(e4);
            return obj;
        }
    }

    public Object newObject(Constructor constructor, Object... objArr) {
        Object obj = null;
        if (constructor == null) {
            return obj;
        }
        try {
            return constructor.newInstance(objArr);
        } catch (IllegalAccessException e2) {
            a(e2);
            return obj;
        } catch (InstantiationException e3) {
            a(e3);
            return obj;
        } catch (InvocationTargetException e4) {
            a(e4);
            return obj;
        }
    }
}
