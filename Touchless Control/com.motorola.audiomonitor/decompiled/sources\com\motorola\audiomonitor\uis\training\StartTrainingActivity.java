package com.motorola.audiomonitor.uis.training;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.au;
import com.motorola.audiomonitor.aw;
import h.k;

public class StartTrainingActivity extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public au f725a;

    /* renamed from: b reason: collision with root package name */
    private TextView f726b;

    /* renamed from: c reason: collision with root package name */
    private TextView f727c;

    /* renamed from: d reason: collision with root package name */
    private TextView f728d;

    /* renamed from: e reason: collision with root package name */
    private final aw f729e = new a(this);

    /* access modifiers changed from: private */
    public void a() {
        if (k.a("customphrase")) {
            this.f726b.setText(k.a(this, C0014R.string.training_retrain_comments, new int[]{C0014R.string.hello_moto}, C0014R.color.UDTHighlightColor));
            this.f727c.setText(C0014R.string.training_rerecord);
            return;
        }
        this.f726b.setText(k.a(this, C0014R.string.training_launch_comments, new int[]{C0014R.string.hello_moto, C0014R.string.training_launch_comments_highlight0, C0014R.string.training_launch_comments_highlight1}, C0014R.color.UDTHighlightColor));
        this.f727c.setText(C0014R.string.training_record);
    }

    /* access modifiers changed from: private */
    public void b() {
        Intent intent = new Intent(this, TrainingTriggerRecordingActivity.class);
        intent.putExtra("selection", "hellomoto");
        intent.putExtras(getIntent());
        startActivity(intent);
    }

    public void finish() {
        super.finish();
        overridePendingTransition(0, C0014R.anim.activity_close_exit);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(C0014R.layout.training_start);
        this.f725a = new au(this, this.f729e);
        this.f727c = (TextView) findViewById(C0014R.id.training_introduct_record);
        this.f727c.setOnClickListener(new b(this));
        this.f728d = (TextView) findViewById(C0014R.id.training_introduct_quit);
        this.f728d.setOnClickListener(new c(this));
        ActionBar actionBar = getActionBar();
        actionBar.setIcon(k.b());
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(C0014R.string.personalization_title);
        this.f726b = (TextView) findViewById(C0014R.id.training_start_notes);
        a();
    }

    public boolean onNavigateUp() {
        finish();
        return true;
    }
}
