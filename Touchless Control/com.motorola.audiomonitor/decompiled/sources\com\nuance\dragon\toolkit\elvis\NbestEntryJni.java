package com.nuance.dragon.toolkit.elvis;

import com.nuance.dragon.toolkit.elvis.ElvisNbestList.Entry;
import com.nuance.dragon.toolkit.elvis.ElvisNbestList.Entry.Word;
import java.util.ArrayList;
import java.util.Iterator;

class NbestEntryJni {

    /* renamed from: a reason: collision with root package name */
    final String f1556a;

    /* renamed from: b reason: collision with root package name */
    final int f1557b;

    /* renamed from: c reason: collision with root package name */
    private final ArrayList f1558c = new ArrayList();

    NbestEntryJni(String str, int i) {
        this.f1556a = str;
        this.f1557b = i;
    }

    /* access modifiers changed from: 0000 */
    public void addWord(String str, String str2, String str3, String str4, int[] iArr) {
        this.f1558c.add(new Word(str, str2, str3, str4, iArr, false));
    }

    /* access modifiers changed from: 0000 */
    public Entry createRealEntry(Grammar grammar) {
        Entry entry = new Entry(this.f1556a, this.f1557b);
        Iterator it = this.f1558c.iterator();
        while (it.hasNext()) {
            Word word = (Word) it.next();
            WordSlot wordSlot = null;
            if (grammar != null) {
                wordSlot = grammar.getWordSlotByName(word.wordSlot);
            }
            if (!(wordSlot != null && wordSlot.isGeneric())) {
                entry.a(word);
            } else {
                entry.a(new Word(word.wordSlot, word.surfaceForm, word.spokenForm, word.fullPhrase, word.wordIds, true));
            }
        }
        return entry;
    }

    /* access modifiers changed from: 0000 */
    public ArrayList getWords() {
        return this.f1558c;
    }
}
