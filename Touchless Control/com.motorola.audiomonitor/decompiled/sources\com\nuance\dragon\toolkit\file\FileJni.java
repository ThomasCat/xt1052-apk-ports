package com.nuance.dragon.toolkit.file;

import android.content.res.AssetFileDescriptor;
import com.nuance.dragon.toolkit.util.Logger;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;

final class FileJni {

    /* renamed from: a reason: collision with root package name */
    private final File f1669a;

    /* renamed from: b reason: collision with root package name */
    private AssetFileDescriptor f1670b;

    public FileJni(AssetFileDescriptor assetFileDescriptor) {
        this.f1670b = assetFileDescriptor;
        this.f1669a = null;
    }

    public FileJni(File file) {
        this.f1670b = null;
        this.f1669a = file;
    }

    public final void close() {
        if (this.f1670b != null) {
            try {
                this.f1670b.close();
            } catch (IOException e2) {
                Logger.error(this, "Error closing file", e2);
            }
            this.f1670b = null;
        }
    }

    public final FileDescriptor getFD() {
        if (this.f1670b != null) {
            return this.f1670b.getFileDescriptor();
        }
        return null;
    }

    public final String getFileName() {
        if (this.f1669a != null) {
            return this.f1669a.getAbsolutePath();
        }
        return null;
    }

    public final int getLength() {
        if (this.f1670b != null) {
            return (int) this.f1670b.getDeclaredLength();
        }
        return -1;
    }

    public final int getStartOffset() {
        if (this.f1670b != null) {
            return (int) this.f1670b.getStartOffset();
        }
        return 0;
    }
}
