package com.nuance.dragon.toolkit.recognition.dictation.b;

import com.nuance.dragon.toolkit.recognition.a.c;
import java.io.ByteArrayInputStream;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public abstract class f implements g {

    /* renamed from: a reason: collision with root package name */
    private byte[] f1983a;

    /* renamed from: b reason: collision with root package name */
    private com.nuance.dragon.toolkit.recognition.dictation.a.f f1984b;

    /* renamed from: c reason: collision with root package name */
    private c f1985c;

    /* renamed from: d reason: collision with root package name */
    private String f1986d = "";

    public interface a extends ContentHandler {
        com.nuance.dragon.toolkit.recognition.dictation.a.f a();

        c b();
    }

    public f(byte[] bArr) {
        if (bArr == null) {
            throw new IllegalArgumentException("Buffer cannot be null");
        }
        this.f1983a = bArr;
    }

    /* access modifiers changed from: protected */
    public abstract a a();

    public final void b() {
        try {
            System.setProperty("org.xml.sax.driver", "org.xmlpull.v1.sax2.Driver");
            InputSource inputSource = new InputSource(new ByteArrayInputStream(this.f1983a));
            XMLReader createXMLReader = XMLReaderFactory.createXMLReader();
            a a2 = a();
            createXMLReader.setContentHandler(a2);
            createXMLReader.parse(inputSource);
            this.f1984b = a2.a();
            this.f1985c = a2.b();
        } catch (SAXException e2) {
            this.f1986d = e2.getMessage();
        }
    }

    public final com.nuance.dragon.toolkit.recognition.dictation.a.f c() {
        return this.f1984b;
    }

    public final c d() {
        return this.f1985c;
    }

    public final String e() {
        return this.f1986d;
    }

    public final boolean f() {
        return this.f1984b != null && this.f1984b.size() > 0;
    }
}
