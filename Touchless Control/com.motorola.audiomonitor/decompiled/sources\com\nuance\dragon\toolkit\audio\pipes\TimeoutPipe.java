package com.nuance.dragon.toolkit.audio.pipes;

import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.util.internal.d;

public class TimeoutPipe extends BufferingPipe {

    /* renamed from: a reason: collision with root package name */
    private final int f1202a;

    /* renamed from: b reason: collision with root package name */
    private final Listener f1203b;

    /* renamed from: c reason: collision with root package name */
    private int f1204c;

    /* renamed from: d reason: collision with root package name */
    private boolean f1205d;

    public interface Listener {
        void onTimeout();
    }

    public TimeoutPipe(int i, Listener listener) {
        d.a("timeoutMs", "greater than 0", i > 0);
        d.a("listener", (Object) listener);
        this.f1202a = i;
        this.f1203b = listener;
    }

    public void connectAudioSource(AudioSource audioSource) {
        this.f1204c = 0;
        this.f1205d = false;
        super.connectAudioSource(audioSource);
    }

    /* access modifiers changed from: protected */
    public void onChunkBuffered(AudioChunk audioChunk) {
        super.onChunkBuffered(audioChunk);
        this.f1204c += audioChunk.audioDuration;
        if (this.f1204c >= this.f1202a && !this.f1205d) {
            this.f1205d = true;
            this.f1203b.onTimeout();
        }
    }
}
