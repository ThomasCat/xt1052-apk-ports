package com.nuance.dragon.toolkit.audio.bluetooth.impl;

import android.media.AudioManager;
import android.media.MediaRecorder.AudioSource;

public class Audio {
    public static final int BLUETOOTH_STREAM;
    public static final int DEFAULT_AUDIO_SOURCE;
    public static final int DEFAULT_AUDIO_STREAM;
    public static final int MUSIC_STREAM = 3;
    public static final int VOICE_CALL_STREAM = 0;

    static {
        Reflection reflection = new Reflection();
        Integer num = (Integer) reflection.getFieldValue(AudioSource.class, "VOICE_RECOGNITION");
        if (num != null) {
            num = Integer.valueOf(0);
        }
        DEFAULT_AUDIO_SOURCE = num.intValue();
        if (AndroidVersion.USE_MUSIC_STREAM_FOR_BLUETOOTH) {
            DEFAULT_AUDIO_STREAM = 3;
        } else {
            DEFAULT_AUDIO_STREAM = 2;
        }
        BLUETOOTH_STREAM = ((Integer) reflection.getFieldValue(AudioManager.class, "STREAM_BLUETOOTH_SCO")).intValue();
    }
}
