package f;

import android.text.TextUtils;
import com.nuance.dragon.toolkit.elvis.Constraint;
import com.nuance.dragon.toolkit.elvis.Grammar;
import com.nuance.dragon.toolkit.elvis.WordSlot;
import com.nuance.dragon.toolkit.grammar.Word;
import java.util.ArrayList;
import java.util.List;

public class u {
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<java.lang.String>, for r10v0, types: [java.util.List, java.util.List<java.lang.String>] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<java.lang.String>, for r7v0, types: [java.util.List, java.util.List<java.lang.String>] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<java.lang.String>, for r8v0, types: [java.util.List, java.util.List<java.lang.String>] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<java.lang.String>, for r9v0, types: [java.util.List, java.util.List<java.lang.String>] */
    public static Grammar a(List<String> list, List<String> list2, List<String> list3, List<String> list4) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        if (list != null && list.size() > 0) {
            WordSlot wordSlot = new WordSlot("localCommands");
            for (String word : list) {
                wordSlot.addWord(new Word(word));
            }
            arrayList.add(wordSlot);
        }
        WordSlot wordSlot2 = new WordSlot("callCmd");
        for (String word2 : list2) {
            wordSlot2.addWord(new Word(word2));
        }
        arrayList.add(wordSlot2);
        WordSlot wordSlot3 = new WordSlot("name", 1);
        if (list3 != null && list3.size() > 0) {
            for (String word3 : list3) {
                wordSlot3.addWord(new Word(word3));
            }
        }
        arrayList.add(wordSlot3);
        WordSlot wordSlot4 = new WordSlot("digit", 3);
        wordSlot4.addWord(new Word("0"));
        wordSlot4.addWord(new Word("1"));
        wordSlot4.addWord(new Word("2"));
        wordSlot4.addWord(new Word("3"));
        wordSlot4.addWord(new Word("4"));
        wordSlot4.addWord(new Word("5"));
        wordSlot4.addWord(new Word("6"));
        wordSlot4.addWord(new Word("7"));
        wordSlot4.addWord(new Word("8"));
        wordSlot4.addWord(new Word("9"));
        arrayList.add(wordSlot4);
        WordSlot wordSlot5 = new WordSlot("location");
        if (list4 != null && list4.size() > 0) {
            for (String str : list4) {
                if (!TextUtils.isEmpty(str)) {
                    wordSlot5.addWord(new Word(str));
                }
            }
        }
        arrayList.add(wordSlot5);
        arrayList.add(new WordSlot("garbage", 4));
        Constraint constraint = new Constraint("localCommand");
        constraint.addTransition(Constraint.START, "localCommands", 0);
        constraint.addTransition("localCommands", Constraint.END, 0);
        constraint.addTransition("localCommands", "garbage", 0);
        constraint.addTransition("garbage", Constraint.END, 0);
        arrayList2.add(constraint);
        Constraint constraint2 = new Constraint("callConst");
        constraint2.addTransition(Constraint.START, "callCmd", 0);
        constraint2.addTransition("callCmd", "name", 0);
        constraint2.addTransition("name", "location", 0);
        constraint2.addTransition("callCmd", "digit", 0);
        constraint2.addTransition("digit", "digit", 0);
        constraint2.addTransition("digit", Constraint.END, 0);
        constraint2.addTransition("callCmd", Constraint.END, 0);
        constraint2.addTransition("name", Constraint.END, 0);
        constraint2.addTransition("location", Constraint.END, 0);
        arrayList2.add(constraint2);
        Constraint constraint3 = new Constraint("garbageConst");
        constraint3.addTransition(Constraint.START, "garbage", 0);
        constraint3.addTransition("garbage", Constraint.END, 0);
        arrayList2.add(constraint3);
        return Grammar.createGrammar(arrayList, arrayList2);
    }
}
