package com.motorola.audiomonitor.actions.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class c {

    /* renamed from: a reason: collision with root package name */
    private final Object f223a = new Object();

    /* renamed from: b reason: collision with root package name */
    private final List f224b = new ArrayList();

    /* renamed from: c reason: collision with root package name */
    private final List f225c = new CopyOnWriteArrayList();

    public c(List list) {
        if (list == null) {
            throw new IllegalArgumentException("updateListener is null");
        }
        this.f225c.addAll(list);
    }

    private void b(List list) {
        if (this.f225c != null && list.size() > 0) {
            for (d a2 : this.f225c) {
                a2.a(list);
            }
        }
    }

    public void a() {
        ArrayList arrayList = new ArrayList();
        a(arrayList);
        synchronized (this.f223a) {
            this.f224b.clear();
            this.f224b.addAll(arrayList);
            b(arrayList);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(List list);

    public void b() {
        this.f225c.clear();
        if (this.f224b != null) {
            this.f224b.clear();
        }
    }
}
