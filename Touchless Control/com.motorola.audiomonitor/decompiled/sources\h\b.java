package h;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.motorola.audiomonitor.service.AudioDspControl;
import com.nuance.dragon.toolkit.audio.pipes.SpeexEncoderPipe;

class b extends Thread implements Callback {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ a f2270a;

    /* renamed from: b reason: collision with root package name */
    private Handler f2271b;

    public b(a aVar) {
        this.f2270a = aVar;
        super("DSP fw Updater");
    }

    private final void a() {
        if (this.f2270a.f2267f != null && this.f2270a.f2267f.isAlive()) {
            Looper myLooper = Looper.myLooper();
            if (myLooper != null && myLooper.getThread() == this.f2270a.f2267f) {
                myLooper.quit();
            }
        }
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 0:
                if (a.f2263b) {
                    Log.d(a.f2262a, "Firmware loaded, res=" + message.arg1);
                }
                this.f2270a.f2269h.c();
                a();
                break;
            case SpeexEncoderPipe.DEFAULT_VAD_BEGIN_LEN /*15*/:
                Log.e(a.f2262a, "Unexpected disconnect, failure is an option");
                a();
                break;
            default:
                if (a.f2263b) {
                    Log.d(a.f2262a, "Unknown message " + message.what);
                    break;
                }
                break;
        }
        return true;
    }

    public void run() {
        if (a.f2263b) {
            Log.d(a.f2262a, "Update thread started");
        }
        if (this.f2270a.f()) {
            if (a.f2263b) {
                Log.d(a.f2262a, "Detected fw file changes, reloading fw");
            }
            Looper.prepare();
            this.f2271b = new Handler(this);
            this.f2270a.f2269h = new AudioDspControl();
            this.f2270a.f2269h.a(this.f2271b);
            if (this.f2270a.f2269h.b() == 0) {
                if (this.f2270a.f2269h.d() == 0) {
                    Looper.loop();
                } else {
                    if (a.f2263b) {
                        Log.d(a.f2262a, "Connected but couldn't load fw");
                    }
                    this.f2270a.f2269h.c();
                }
            }
            this.f2270a.f2269h.a();
        }
        a.f2265d.set(true);
        a.i();
    }
}
