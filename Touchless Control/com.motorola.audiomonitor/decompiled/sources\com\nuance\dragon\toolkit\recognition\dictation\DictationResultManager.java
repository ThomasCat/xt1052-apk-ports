package com.nuance.dragon.toolkit.recognition.dictation;

import com.nuance.dragon.toolkit.recognition.a.c;
import com.nuance.dragon.toolkit.recognition.dictation.a.e;
import com.nuance.dragon.toolkit.recognition.dictation.a.f;
import com.nuance.dragon.toolkit.recognition.dictation.a.i;
import com.nuance.dragon.toolkit.recognition.dictation.b.a;
import com.nuance.dragon.toolkit.recognition.dictation.b.b;
import com.nuance.dragon.toolkit.recognition.dictation.b.d;
import com.nuance.dragon.toolkit.util.Logger;

public class DictationResultManager {
    private static f a(byte[] bArr) {
        d aVar;
        new StringBuilder("DictationResultImpl(buffer [size: ").append(bArr.length).append("] )");
        if (bArr == null || bArr.length < 4) {
            String str = "Cannot parse dictation results: The buffer length is too small to be containing any results.";
            Logger.error(DictationResultManager.class, str);
            throw new IllegalArgumentException(str);
        }
        try {
            if (bArr.length > 20) {
                String a2 = i.a(bArr, 0, 20, "ISO-8859-1");
                if (a2.startsWith("<?xml") || (a2.indexOf("<?") >= 0 && a2.indexOf("xml") >= 0)) {
                    aVar = new com.nuance.dragon.toolkit.recognition.dictation.b.i(bArr);
                    return aVar.a();
                }
            }
            aVar = new a(bArr);
            return aVar.a();
        } catch (Exception e2) {
            throw new IllegalArgumentException("Cannot parse dictation results: illegal format buffer.");
        }
    }

    public static DictationEditBuffer createDictationEditBuffer(DictationResult dictationResult, long j) {
        return new e(j, (f) dictationResult);
    }

    public static DictationEditBuffer createDictationEditBuffer(byte[] bArr, long j) {
        return new e(j, a(bArr));
    }

    public static DictationResult createDictationResult(byte[] bArr) {
        return a(bArr);
    }

    public static c createInterpretationResult(byte[] bArr) {
        new StringBuilder("createInterpretationResult(buffer [size: ").append(bArr.length).append("] )");
        if (bArr == null || bArr.length < 4) {
            String str = "Cannot parse interpretation results: The buffer length is too small to be containing any results.";
            Logger.error(DictationResultManager.class, str);
            throw new IllegalArgumentException(str);
        }
        try {
            b bVar = new b(bArr);
            bVar.b();
            return bVar.d();
        } catch (Exception e2) {
            Logger.error(DictationResultManager.class, "Error parsing interpretation result", e2);
            throw new IllegalArgumentException("Cannot parse interpretation results: illegal format buffer.");
        }
    }
}
