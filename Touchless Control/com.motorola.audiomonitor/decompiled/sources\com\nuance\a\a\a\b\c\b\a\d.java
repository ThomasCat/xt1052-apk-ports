package com.nuance.a.a.a.b.c.b.a;

import com.nuance.a.a.a.a.a.a.C0000a;
import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.a.b.a.b;
import com.nuance.a.a.a.a.b.a.b.C0002b;
import com.nuance.a.a.a.b.b.c;
import com.nuance.a.a.a.b.c.a.e;
import com.nuance.a.a.a.b.c.a.f;
import java.io.ByteArrayOutputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class d implements C0002b, com.nuance.a.a.a.b.c.a.d, c {
    private static final C0001a k = a.a(d.class);
    private static long p = 1;

    /* renamed from: a reason: collision with root package name */
    protected b f845a;

    /* renamed from: b reason: collision with root package name */
    protected C0000a f846b;

    /* renamed from: c reason: collision with root package name */
    protected C0000a f847c;

    /* renamed from: d reason: collision with root package name */
    public Vector f848d;

    /* renamed from: e reason: collision with root package name */
    protected com.nuance.a.a.a.b.c.a.a f849e;

    /* renamed from: f reason: collision with root package name */
    protected int f850f = 0;

    /* renamed from: g reason: collision with root package name */
    protected Object f851g;

    /* renamed from: h reason: collision with root package name */
    protected Hashtable f852h;
    protected byte i = 0;
    protected long j;
    private f l;
    private c m;
    private byte n;
    private b o;
    private Hashtable q;

    public d(a aVar, f fVar, Vector vector) {
        int i2 = 0;
        this.f849e = aVar;
        this.f845a = aVar.g();
        this.l = fVar;
        this.f846b = aVar.e();
        this.f847c = aVar.f();
        this.n = 5;
        this.o = aVar.a();
        this.f851g = new Object();
        this.i = this.f845a.f();
        this.f852h = new Hashtable();
        this.q = new Hashtable();
        this.j = this.f845a.e();
        this.f848d = new Vector();
        if (vector != null) {
            while (true) {
                int i3 = i2;
                if (i3 < vector.size()) {
                    this.f848d.addElement(((c) vector.elementAt(i3)).clone());
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    private static c.a a(String str) {
        if (str.equalsIgnoreCase("sdk")) {
            return c.a.f826a;
        }
        if (str.equalsIgnoreCase("nmsp")) {
            return c.a.f827b;
        }
        if (str.equalsIgnoreCase("app")) {
            return c.a.f828c;
        }
        if (str.equalsIgnoreCase("nss")) {
            return c.a.f829d;
        }
        if (str.equalsIgnoreCase("slog")) {
            return c.a.f830e;
        }
        if (str.equalsIgnoreCase("nsslog")) {
            return c.a.f831f;
        }
        if (str.equalsIgnoreCase("gwlog")) {
            return c.a.f832g;
        }
        if (str.equalsIgnoreCase("svsp")) {
            return c.a.f833h;
        }
        if (str.equalsIgnoreCase("sip")) {
            return c.a.i;
        }
        if (str.equalsIgnoreCase("sdp")) {
            return c.a.j;
        }
        return null;
    }

    private void a(Vector vector, long j2) {
        if (this.f850f != 2) {
            f fVar = this.l;
        }
        byte[] bArr = new byte[0];
        this.f852h.put(new Long(j2), new Byte(2));
        this.q.put(new Long(j2), vector);
        String str = "<gp><rid>" + j2 + "</rid>";
        for (int i2 = 0; i2 < vector.size(); i2++) {
            c cVar = (c) vector.elementAt(i2);
            if (cVar.d() == c.a.f828c || cVar.d() == c.a.f827b || cVar.d() == c.a.f829d || cVar.d() == c.a.f833h) {
                str = str + "<" + cVar.d() + " p=\"" + cVar.a() + "\"/>";
            }
        }
        long j3 = j2;
        this.f845a.a(2583, "SEND_BCP_GET_PARAMS", (str + "</gp>").getBytes(), bArr, this.i, j3, this.m, false);
    }

    public static long i() {
        return 0;
    }

    public void a() {
        k.b((Object) "freeResource, disconnect timeout: 0");
        synchronized (this.f851g) {
            if (this.f850f == 2) {
                this.f850f = 0;
                this.o.a(new b.a(3, new Integer(0)), this, Thread.currentThread(), this.o.a()[0]);
            } else {
                k.e("ResourceException the resource was unloaded. ");
                throw new e("the resource was unloaded. ");
            }
        }
    }

    public final void a(byte b2, long j2, short s, short s2, short s3) {
        k.b((Object) "onBcpResponse, TID: " + b2 + ", RID: " + j2 + ", status code: " + s + " , request state: " + s2 + ", completion cause: " + s3);
        if (s != 200) {
            Byte b3 = (Byte) this.f852h.get(new Long(j2));
            if (b3 != null) {
                switch (b3.byteValue()) {
                    case 1:
                        this.f852h.remove(new Long(j2));
                        f fVar = this.l;
                        return;
                    case 2:
                        this.f852h.remove(new Long(j2));
                        f fVar2 = this.l;
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public final void a(byte b2, long j2, short s, byte[] bArr) {
        k.b((Object) "onBcpSetParamsComplete, TID: " + b2 + ", RID: " + j2 + ", status code: " + s);
        if (this.f852h.remove(new Long(j2)) == null) {
            k.d("onBcpSetParamsComplete, RID: " + j2 + " already removed!");
            return;
        }
        Vector vector = new Vector();
        if (bArr != null) {
            String str = new String(bArr);
            int i2 = str.startsWith(";") ? 1 : 0;
            while (true) {
                int indexOf = str.indexOf(";", i2);
                if (indexOf == -1) {
                    break;
                }
                String substring = str.substring(i2, indexOf);
                int indexOf2 = substring.indexOf(".");
                if (indexOf2 != -1) {
                    vector.addElement(new c(substring.substring(indexOf2 + 1), a(substring.substring(0, indexOf2))));
                }
                i2 = indexOf + 1;
            }
            if (i2 < str.length()) {
                String substring2 = str.substring(i2);
                int indexOf3 = substring2.indexOf(".");
                if (indexOf3 != -1) {
                    vector.addElement(new c(substring2.substring(indexOf3 + 1), a(substring2.substring(0, indexOf3))));
                }
            }
        }
        if (s == 200 || s == 201) {
            f fVar = this.l;
        } else {
            f fVar2 = this.l;
        }
    }

    public void a(byte b2, short s) {
        k.b((Object) "onBcpEvent, TID: " + b2 + ", event code: " + s);
        if (this.f850f == 2) {
            this.f850f = 0;
            Enumeration keys = this.f852h.keys();
            while (keys.hasMoreElements()) {
                switch (((Byte) this.f852h.remove((Long) keys.nextElement())).byteValue()) {
                    case 1:
                        f fVar = this.l;
                        break;
                    case 2:
                        f fVar2 = this.l;
                        break;
                }
            }
            f fVar3 = this.l;
            this.f845a.b((c) this);
        }
    }

    public void a(byte b2, byte[] bArr) {
    }

    /* access modifiers changed from: protected */
    public final void a(c cVar) {
        k.b((Object) "loadResource");
        if (new com.nuance.a.a.a.c.a(this.f848d).a()) {
            this.f846b = com.nuance.a.a.a.a.d.d.b(this.f846b);
            this.f847c = com.nuance.a.a.a.a.d.d.b(this.f847c);
            ((a) this.f849e).a(this.f846b);
            ((a) this.f849e).b(this.f847c);
        }
        this.m = cVar;
        this.f845a.a(cVar);
        if (this.f850f == 0) {
            if (this.f845a.d() != null) {
                this.f850f = 2;
            } else {
                this.f845a.a(this.f846b, this.f847c);
                this.f850f = 1;
            }
            long h2 = h();
            String str = "<lr><rid>" + h2 + "</rid>";
            switch (this.n) {
                case 1:
                    str = str + "<nr9><reco/></nr9>";
                    break;
                case 2:
                    str = str + "<nr9><tts/></nr9>";
                    break;
                case 3:
                    str = str + "<oper></oper>";
                    break;
                case 4:
                    str = (str + "<dict>") + "</dict>";
                    break;
                case 5:
                    return;
            }
            this.f845a.a(2599, "SEND_BCP_LOAD_RESOURCE", (str + "</lr>").getBytes(), null, this.i, h2, cVar, false);
        }
    }

    public void a(Object obj, Object obj2) {
        b.a aVar = (b.a) obj;
        switch (aVar.f783a) {
            case 1:
                Object[] objArr = (Object[]) aVar.f784b;
                Vector vector = (Vector) objArr[0];
                long longValue = ((Long) objArr[1]).longValue();
                if (this.f850f != 2) {
                    f fVar = this.l;
                    return;
                }
                boolean z = true;
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr = new byte[4];
                this.f852h.put(new Long(longValue), new Byte(1));
                String str = "<sp><rid>" + longValue + "</rid>";
                int i2 = 0;
                int i3 = 1;
                String str2 = str;
                while (true) {
                    int i4 = i2;
                    if (i4 < vector.size()) {
                        c cVar = (c) vector.elementAt(i4);
                        if (cVar.d() == c.a.f828c || cVar.d() == c.a.f827b || cVar.d() == c.a.f829d) {
                            z = false;
                            str2 = str2 + "<" + cVar.d() + " p=\"" + cVar.a() + "\" v=\"" + com.nuance.a.a.a.a.d.d.a(new String(cVar.b())) + "\"/>";
                        } else {
                            if (cVar.d() == c.a.f833h) {
                                z = false;
                            }
                            StringBuilder append = new StringBuilder().append(str2).append("<").append(cVar.d().toString()).append(" p=\"").append(cVar.a()).append("\" v=\"");
                            int i5 = i3 + 1;
                            str2 = append.append(i3).append("\"/>").toString();
                            byte[] b2 = cVar.b();
                            com.nuance.a.a.a.a.d.b.a(b2.length, bArr, 0);
                            byteArrayOutputStream.write(bArr, 0, 4);
                            byteArrayOutputStream.write(b2, 0, b2.length);
                            i3 = i5;
                        }
                        i2 = i4 + 1;
                    } else {
                        this.f845a.a(2581, "SEND_BCP_SET_PARAMS", (str2 + "</sp>").getBytes(), byteArrayOutputStream.toByteArray(), this.i, longValue, this.m, z);
                        try {
                            byteArrayOutputStream.close();
                            return;
                        } catch (Exception e2) {
                            return;
                        }
                    }
                }
                break;
            case 2:
                Object[] objArr2 = (Object[]) aVar.f784b;
                a((Vector) objArr2[0], ((Long) objArr2[1]).longValue());
                return;
            case 3:
                this.f845a.a(this.i, ((Integer) aVar.f784b).intValue(), this.m);
                f fVar2 = this.l;
                return;
            case 4:
                int intValue = ((Integer) aVar.f784b).intValue();
                long h2 = h();
                this.f845a.a(2608, "SEND_BCP_FREE_RESOURCE_ID", ((((((("<fr><rid>" + h2 + "</rid>") + "<n>1</n>") + "<resids>") + "<res1><id>" + this.j + "</id>") + "<timeout>" + intValue + "<timeout></res1>") + "</resids>") + "</fr>").getBytes(), null, this.i, h2, this.m, true);
                return;
            default:
                return;
        }
    }

    public void a(short s) {
        if (k.b()) {
            k.b((Object) "onSessionDisconnected, reason code: " + s);
        }
        Enumeration keys = this.f852h.keys();
        while (keys.hasMoreElements()) {
            switch (((Byte) this.f852h.remove((Long) keys.nextElement())).byteValue()) {
                case 1:
                    f fVar = this.l;
                    break;
                case 2:
                    f fVar2 = this.l;
                    break;
            }
        }
        synchronized (this.f851g) {
            if (this.f850f == 2) {
                f fVar3 = this.l;
            }
            this.f850f = 0;
        }
    }

    public void a(byte[] bArr) {
        if (k.b()) {
            k.b((Object) "onSessionConnected, SID: " + bArr);
        }
        synchronized (this.f851g) {
            this.f850f = 2;
        }
    }

    public final void b() {
        f fVar = this.l;
    }

    public final void b(byte b2, long j2, short s, byte[] bArr) {
        int i2;
        int i3 = 0;
        k.b((Object) "onBcpGetParamsComplete, TID: " + b2 + ", RID: " + j2);
        if (this.f852h.remove(new Long(j2)) == null) {
            k.d("onBcpGetParamsComplete, RID: " + j2 + " already removed!");
            this.q.remove(new Long(j2));
            return;
        }
        Vector vector = (Vector) this.q.remove(new Long(j2));
        if (vector == null) {
            k.e("Could not find the grammars associated with RID: " + j2);
            return;
        }
        String str = new String(bArr);
        if (str.startsWith(";")) {
            i3 = 1;
            i2 = 0;
        } else {
            i2 = 0;
        }
        while (true) {
            int indexOf = str.indexOf(";", i3);
            if (indexOf == -1) {
                break;
            }
            String substring = str.substring(i3, indexOf);
            if (substring.indexOf(":") == -1) {
                c cVar = (c) vector.elementAt(i2);
                vector.setElementAt(new c(cVar.a(), substring.getBytes(), cVar.d()), i2);
            }
            i2++;
            i3 = indexOf + 1;
        }
        if (i3 < str.length()) {
            String substring2 = str.substring(i3);
            if (substring2.indexOf(":") == -1) {
                c cVar2 = (c) vector.elementAt(i2);
                vector.setElementAt(new c(cVar2.a(), substring2.getBytes(), cVar2.d()), i2);
            }
        }
        if (s == 200 || s == 201) {
            f fVar = this.l;
        } else {
            f fVar2 = this.l;
        }
    }

    public void c() {
    }

    public void d() {
    }

    public void e() {
    }

    public void f() {
    }

    public final com.nuance.a.a.a.b.c.a.a g() {
        return this.f849e;
    }

    /* access modifiers changed from: protected */
    public synchronized long h() {
        long j2;
        j2 = p;
        p = 1 + j2;
        if (p == Long.MIN_VALUE) {
            p = 1;
        }
        return j2;
    }
}
