package com.nuance.dragon.toolkit.util;

import android.os.Handler;
import android.os.Looper;
import com.nuance.dragon.toolkit.util.internal.b;

public class WorkerThread {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final b f2109a = new b();

    /* renamed from: b reason: collision with root package name */
    private final Thread f2110b = new Thread(new Runnable() {
        public final void run() {
            Looper.prepare();
            WorkerThread.this.f2109a.a();
            Looper.loop();
        }
    });

    public Handler getHandler() {
        return this.f2109a;
    }

    public Thread getThread() {
        return this.f2110b;
    }

    public void start() {
        this.f2110b.start();
    }

    public void stop() {
        this.f2109a.post(new Runnable() {
            public final void run() {
                Looper.myLooper().quit();
            }
        });
    }
}
