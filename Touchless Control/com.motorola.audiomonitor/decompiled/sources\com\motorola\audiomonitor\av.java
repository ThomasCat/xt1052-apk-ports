package com.motorola.audiomonitor;

import android.util.Log;

class av extends x {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ au f322a;

    av(au auVar) {
        this.f322a = auVar;
    }

    public void a(int i) {
        if (this.f322a.f320f != null) {
            if (au.f316b) {
                Log.d(au.f315a, "Service State Changed: " + i);
            }
            if (this.f322a.f320f != null) {
                this.f322a.f320f.a(i);
            }
        }
    }

    public void a(int i, int i2) {
        if (this.f322a.f320f != null) {
            if (au.f316b) {
                Log.d(au.f315a, "Enrollment Event: " + i + ", param0=" + i2);
            }
            this.f322a.f320f.a(i, i2);
        }
    }

    public void b(int i) {
        if (this.f322a.f320f != null) {
            if (au.f316b) {
                Log.d(au.f315a, "Training Mode: " + i);
            }
            this.f322a.f320f.b(i);
        }
    }

    public void c(int i) {
        if (this.f322a.f320f != null) {
            this.f322a.f320f.c(i);
        }
    }
}
