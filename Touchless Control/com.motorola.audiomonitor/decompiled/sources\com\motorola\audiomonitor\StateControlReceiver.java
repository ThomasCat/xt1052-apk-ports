package com.motorola.audiomonitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import com.motorola.audiomonitor.uis.SettingSwitchActivity;
import h.d;
import h.k;
import java.util.Locale;

public class StateControlReceiver extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    private static final String f157a = ("AMonitor." + StateControlReceiver.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f158b = a.f160b;

    private void a(Context context) {
        Intent intent = new Intent(context, SettingSwitchActivity.class);
        intent.setFlags(268435456);
        context.startActivity(intent);
    }

    private void a(Context context, String str) {
        if (f158b) {
            Log.d(f157a, "STATE_CONTROL broadcast: " + str);
        }
        if (str.equals("start")) {
            if (!d.b(context)) {
                a(context);
            } else if (a.f166h) {
            } else {
                if (!k.a(Locale.getDefault()) || (!k.a("customphrase") && !a.j)) {
                    a(context);
                } else {
                    a(context, false);
                }
            }
        } else if (str.equals("stop") && a.f166h) {
            a(context, true);
        }
    }

    private void a(Context context, boolean z) {
        Intent intent = new Intent(context, MonitorService.class);
        if (z) {
            intent.putExtra("MONITOR_STATE", "stop");
        }
        context.startService(intent);
    }

    public void onReceive(Context context, Intent intent) {
        if (intent != null && TextUtils.equals(intent.getAction(), "com.motorola.audiomonitor.AOV_STATE_CONTROL")) {
            String stringExtra = intent.getStringExtra("NEW_STATUS");
            if (TextUtils.equals(stringExtra, "start") || TextUtils.equals(stringExtra, "stop")) {
                a(context, stringExtra);
            } else if (f158b) {
                Log.e(f157a, "Invalid command.");
            }
        }
    }
}
