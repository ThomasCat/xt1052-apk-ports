package com.motorola.audiomonitor.uis;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import h.d;
import h.k;

class ak implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ PrivacyActivity f653a;

    ak(PrivacyActivity privacyActivity) {
        this.f653a = privacyActivity;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == -1) {
            Context applicationContext = this.f653a.getApplicationContext();
            d.a(applicationContext, 0);
            k.b(applicationContext, 0);
            k.a(applicationContext, "opt_out", true);
            k.c(applicationContext);
            k.d(applicationContext);
            this.f653a.finish();
        } else if (i == -2) {
            Context applicationContext2 = this.f653a.getApplicationContext();
            d.a(applicationContext2, 1);
            k.a(applicationContext2, "opt_out", false);
            this.f653a.f627b.setChecked(true);
        }
    }
}
