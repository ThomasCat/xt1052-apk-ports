package com.motorola.audiomonitor.actions.utils;

import android.content.Context;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;

public class aj {

    /* renamed from: a reason: collision with root package name */
    private static final String f198a = ("AMonitor." + aj.class.getSimpleName());

    public static void a(Context context) {
        try {
            Object systemService = context.getSystemService("statusbar");
            systemService.getClass().getDeclaredMethod("collapsePanels", new Class[0]).invoke(systemService, new Object[0]);
        } catch (IllegalAccessException e2) {
            Log.e(f198a, "statusbar collapse failed: " + e2.toString());
        } catch (InvocationTargetException e3) {
            Log.e(f198a, "statusbar collapse failed: " + e3.toString());
        } catch (NoSuchMethodException e4) {
            Log.e(f198a, "collapsePanels method not found");
        }
    }
}
