package com.nuance.dragon.toolkit.cloudservices.recognizer;

import com.nuance.dragon.toolkit.audio.bluetooth.impl.AndroidVersion;
import com.nuance.dragon.toolkit.cloudservices.DataParam;
import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.ArrayList;
import java.util.List;

public class RecogSpec {

    /* renamed from: a reason: collision with root package name */
    private String f1495a;

    /* renamed from: b reason: collision with root package name */
    private Dictionary f1496b;

    /* renamed from: c reason: collision with root package name */
    private String f1497c;

    /* renamed from: d reason: collision with root package name */
    private final ArrayList f1498d;

    /* renamed from: e reason: collision with root package name */
    private final int f1499e;

    public RecogSpec(String str, Dictionary dictionary, String str2) {
        d.a("command", (Object) str);
        d.a("audioParam", (Object) str2);
        this.f1495a = str;
        this.f1496b = dictionary;
        this.f1497c = str2;
        this.f1498d = new ArrayList();
        this.f1499e = AndroidVersion.CUR_DEVELOPMENT;
    }

    public RecogSpec(String str, Dictionary dictionary, String str2, int i) {
        d.a("command", (Object) str);
        d.a("audioParam", (Object) str2);
        d.a("timeoutMs", "greater than 0", i > 0);
        this.f1495a = str;
        this.f1496b = dictionary;
        this.f1497c = str2;
        this.f1498d = new ArrayList();
        this.f1499e = i;
    }

    public final void addParam(DataParam dataParam) {
        d.a("param", (Object) dataParam);
        this.f1498d.add(dataParam);
    }

    public final String getAudioParam() {
        return this.f1497c;
    }

    public final String getCommand() {
        return this.f1495a;
    }

    public List getDelayedParams() {
        return new ArrayList(0);
    }

    public final List getParams() {
        return this.f1498d;
    }

    public final Dictionary getSettings() {
        return this.f1496b;
    }

    public final int getTimeout() {
        return this.f1499e;
    }
}
