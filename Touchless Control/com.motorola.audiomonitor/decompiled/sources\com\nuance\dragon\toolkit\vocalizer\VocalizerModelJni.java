package com.nuance.dragon.toolkit.vocalizer;

final class VocalizerModelJni {

    /* renamed from: a reason: collision with root package name */
    final String f2153a;

    /* renamed from: b reason: collision with root package name */
    final String f2154b;

    /* renamed from: c reason: collision with root package name */
    final String f2155c;

    /* renamed from: d reason: collision with root package name */
    final int f2156d;

    VocalizerModelJni(String str, String str2, String str3, int i) {
        this.f2153a = str;
        this.f2154b = str2;
        this.f2155c = str3;
        this.f2156d = i;
    }

    public final String toString() {
        return this.f2153a + ", " + this.f2154b + ", " + this.f2155c + ", " + this.f2156d;
    }
}
