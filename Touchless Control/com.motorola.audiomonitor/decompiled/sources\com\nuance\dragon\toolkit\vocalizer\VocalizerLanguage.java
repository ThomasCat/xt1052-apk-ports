package com.nuance.dragon.toolkit.vocalizer;

import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import org.json.JSONException;
import org.json.JSONObject;

public final class VocalizerLanguage implements JSONCompliant {
    public final String description;
    public final String name;
    public final VocalizerVoice[] voices;

    VocalizerLanguage(String str, String str2, String[] strArr, String[] strArr2) {
        this.name = str;
        this.description = str2;
        this.voices = a(strArr, strArr2, this);
    }

    private static VocalizerVoice[] a(String[] strArr, String[] strArr2, VocalizerLanguage vocalizerLanguage) {
        VocalizerVoice[] vocalizerVoiceArr = new VocalizerVoice[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            vocalizerVoiceArr[i] = new VocalizerVoice(strArr[i], strArr2[i], vocalizerLanguage);
        }
        return vocalizerVoiceArr;
    }

    public static VocalizerLanguage createFromJSON(JSONObject jSONObject) {
        VocalizerLanguage[] vocalizerLanguageArr;
        String string = jSONObject.getString("name");
        for (VocalizerLanguage vocalizerLanguage : a.ALL_LANGUAGES) {
            if (vocalizerLanguage.name.equals(string)) {
                return vocalizerLanguage;
            }
        }
        throw new JSONException("Invalid language name: " + string);
    }

    public final boolean equals(Object obj) {
        if (obj == null || !(obj instanceof VocalizerLanguage)) {
            return false;
        }
        return this.name.equals(((VocalizerLanguage) obj).name);
    }

    public final int hashCode() {
        return this.name.hashCode();
    }

    public final JSONObject toJSON() {
        b bVar = new b();
        bVar.a("name", (Object) this.name);
        return bVar;
    }

    public final String toString() {
        return this.description;
    }
}
