package com.nuance.a.a.a.b.c.b.b;

import com.nuance.a.a.a.b.b.a;
import com.nuance.a.a.a.b.c.b.a.b;
import com.nuance.a.a.a.b.c.c.h;

public final class x extends o implements h {

    /* renamed from: a reason: collision with root package name */
    private i f884a;

    /* renamed from: b reason: collision with root package name */
    private a f885b;

    /* renamed from: c reason: collision with root package name */
    private int f886c;

    public x(String str, b bVar, i iVar, a aVar) {
        super(str, Byte.MAX_VALUE);
        this.f884a = iVar;
        this.f885b = aVar;
        this.f886c = bVar.a();
        iVar.b("audio_id", this.f886c);
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return this.f886c;
    }

    /* access modifiers changed from: protected */
    public final byte[] d() {
        return this.f884a.b();
    }

    /* access modifiers changed from: protected */
    public final a e() {
        return this.f885b;
    }
}
