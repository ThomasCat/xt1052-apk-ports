package com.nuance.dragon.toolkit.audio.sources;

import android.os.Handler;
import android.util.Pair;
import com.nuance.dragon.toolkit.audio.AbstractAudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class QueuingAudioSource extends AudioSource {

    /* renamed from: a reason: collision with root package name */
    private final LinkedList f1266a = new LinkedList();
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public AudioSource f1267b;

    /* renamed from: c reason: collision with root package name */
    private Listener f1268c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public AudioSink f1269d;

    /* renamed from: e reason: collision with root package name */
    private boolean f1270e = false;

    /* renamed from: f reason: collision with root package name */
    private final AudioType f1271f;

    /* renamed from: g reason: collision with root package name */
    private final boolean f1272g;

    /* renamed from: h reason: collision with root package name */
    private final AudioSink f1273h = new AudioSink() {
        public final void chunksAvailable(AudioSource audioSource) {
            if (QueuingAudioSource.this.f1269d != null && QueuingAudioSource.this.f1267b == audioSource) {
                QueuingAudioSource.this.f1269d.chunksAvailable(QueuingAudioSource.this);
            }
        }

        public final void framesDropped(AudioSource audioSource) {
            if (QueuingAudioSource.this.f1269d != null && QueuingAudioSource.this.f1267b == audioSource) {
                QueuingAudioSource.this.f1269d.framesDropped(QueuingAudioSource.this);
            }
        }

        public final void sourceClosed(AudioSource audioSource) {
            if (QueuingAudioSource.this.f1267b == audioSource && audioSource.isEmptyForSink(this)) {
                QueuingAudioSource.this.a();
            }
        }
    };
    private final Handler i = new Handler();

    public interface Listener {
        void onSourceDone(AudioSource audioSource, int i);
    }

    public QueuingAudioSource(AudioType audioType, boolean z) {
        d.a("audioType", (Object) audioType);
        this.f1271f = audioType;
        this.f1272g = z;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004d, code lost:
        r4.f1267b = (com.nuance.dragon.toolkit.audio.AudioSource) r0.first;
        r4.f1268c = (com.nuance.dragon.toolkit.audio.sources.QueuingAudioSource.Listener) r0.second;
        r4.f1273h.connectAudioSource(r4.f1267b);
     */
    public void a() {
        if (this.f1270e) {
            Logger.warn(this, "Can't begin next source because queuing source is closed");
            return;
        }
        if (this.f1267b != null) {
            this.f1273h.disconnectAudioSource();
            if (this.f1268c != null) {
                this.f1268c.onSourceDone(this.f1267b, this.f1266a.size());
                this.f1268c = null;
            }
            this.f1267b = null;
        }
        while (true) {
            if (this.f1266a.isEmpty()) {
                break;
            }
            Pair pair = (Pair) this.f1266a.remove();
            if (!((AudioSource) pair.first).isActive()) {
                if (!((AudioSource) pair.first).isEmpty()) {
                    break;
                }
            } else {
                break;
            }
        }
        if (this.f1267b == null && this.f1266a.isEmpty() && this.f1272g) {
            stop();
        }
    }

    private void b() {
        int size = this.f1266a.size();
        ArrayList arrayList = new ArrayList(size);
        arrayList.addAll(this.f1266a);
        this.f1266a.clear();
        if (this.f1267b != null) {
            AudioSource audioSource = this.f1267b;
            this.f1267b = null;
            if (this.f1268c != null) {
                this.f1268c.onSourceDone(audioSource, size);
            }
            this.f1268c = null;
            this.f1273h.disconnectAudioSource();
            Iterator it = arrayList.iterator();
            int i2 = size;
            while (it.hasNext()) {
                Pair pair = (Pair) it.next();
                i2--;
                this.f1273h.disconnectAudioSource();
                if (pair.second != null) {
                    ((Listener) pair.second).onSourceDone((AudioSource) pair.first, i2);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void audioSinkConnected(AudioSink audioSink) {
        this.f1269d = audioSink;
    }

    /* access modifiers changed from: protected */
    public void audioSinkDisconnected(AudioSink audioSink) {
        if (this.f1269d == audioSink) {
            this.f1269d = null;
            b();
        }
    }

    public AbstractAudioChunk getAudioChunkForSink(AudioSink audioSink) {
        if (this.f1269d != audioSink || this.f1267b == null) {
            return null;
        }
        AbstractAudioChunk audioChunkForSink = this.f1267b.getAudioChunkForSink(this.f1273h);
        if (this.f1267b.isActive() || !this.f1267b.isEmptyForSink(this.f1273h)) {
            return audioChunkForSink;
        }
        final AudioSource audioSource = this.f1267b;
        this.i.post(new Runnable() {
            public final void run() {
                if (QueuingAudioSource.this.f1267b == audioSource) {
                    QueuingAudioSource.this.a();
                }
            }
        });
        return audioChunkForSink;
    }

    public AudioType getAudioType() {
        return this.f1271f;
    }

    public int getChunksAvailable() {
        if (this.f1267b != null) {
            return this.f1267b.getChunksAvailable();
        }
        return 0;
    }

    public int getChunksAvailableForSink(AudioSink audioSink) {
        if (this.f1269d != audioSink || this.f1267b == null) {
            return 0;
        }
        return this.f1267b.getChunksAvailableForSink(this.f1273h);
    }

    public boolean isActive() {
        return !this.f1270e;
    }

    public void queue(AudioSource audioSource) {
        queue(audioSource, null);
    }

    public void queue(AudioSource audioSource, Listener listener) {
        d.a((Object) this, !this.f1270e, "Source is closed");
        d.a("audioSource", (Object) audioSource);
        d.a("audioSource", "a supported audio type", this.f1271f.equals(audioSource.getAudioType()));
        this.f1266a.add(new Pair(audioSource, listener));
        if (this.f1267b == null) {
            a();
        }
    }

    public void stop() {
        if (!this.f1270e) {
            this.f1270e = true;
            b();
            if (this.f1269d != null) {
                this.f1269d.sourceClosed(this);
            }
        }
    }
}
