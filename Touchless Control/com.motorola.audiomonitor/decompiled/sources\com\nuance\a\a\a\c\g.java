package com.nuance.a.a.a.c;

import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.a.b.a.e;
import java.util.Vector;

public class g implements e, Runnable {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f992a = com.nuance.a.a.a.a.b.a.a.a(g.class);

    /* renamed from: b reason: collision with root package name */
    private Vector f993b = new Vector();

    /* renamed from: c reason: collision with root package name */
    private Object f994c = new Object();

    /* renamed from: d reason: collision with root package name */
    private boolean f995d = false;

    final class a {

        /* renamed from: a reason: collision with root package name */
        public long f996a;

        /* renamed from: b reason: collision with root package name */
        public com.nuance.a.a.a.a.b.a.e.a f997b;

        public a(com.nuance.a.a.a.a.b.a.e.a aVar, long j) {
            this.f996a = j;
            this.f997b = aVar;
        }
    }

    public g() {
        new Thread(this).start();
    }

    public final void a() {
        this.f995d = true;
        synchronized (this.f994c) {
            this.f994c.notify();
        }
    }

    public final void a(com.nuance.a.a.a.a.b.a.e.a aVar, long j) {
        long currentTimeMillis = System.currentTimeMillis() + j;
        if (f992a.b()) {
            f992a.b((Object) "Scheduling TimerSystemTask for time: " + currentTimeMillis);
        }
        a aVar2 = new a(aVar, currentTimeMillis);
        synchronized (this.f994c) {
            int size = this.f993b.size();
            for (int i = 0; i < size; i++) {
                if (((a) this.f993b.elementAt(i)).f996a > aVar2.f996a) {
                    if (f992a.a()) {
                        f992a.a((Object) "Inserting new timer task at index " + i);
                    }
                    this.f993b.insertElementAt(aVar2, i);
                    this.f994c.notify();
                    return;
                }
            }
            if (f992a.a()) {
                f992a.a((Object) "Inserting new timer task at index 0");
            }
            this.f993b.addElement(aVar2);
            this.f994c.notify();
        }
    }

    public final boolean a(com.nuance.a.a.a.a.b.a.e.a aVar) {
        boolean z;
        if (f992a.b()) {
            f992a.b((Object) "Canceling task");
        }
        synchronized (this.f994c) {
            int i = 0;
            while (true) {
                if (i >= this.f993b.size()) {
                    z = false;
                    break;
                } else if (((a) this.f993b.elementAt(i)).f997b == aVar) {
                    z = true;
                    this.f993b.removeElementAt(i);
                    break;
                } else {
                    i++;
                }
            }
        }
        return z;
    }

    public void run() {
        while (!this.f995d) {
            synchronized (this.f994c) {
                if (this.f993b.size() > 0) {
                    a aVar = (a) this.f993b.elementAt(0);
                    if (aVar.f996a < System.currentTimeMillis()) {
                        this.f993b.removeElementAt(0);
                        if (f992a.a()) {
                            f992a.a((Object) "Removed a task from the queue");
                        }
                        if (aVar != null) {
                            try {
                                if (f992a.b()) {
                                    f992a.b((Object) "Executing timer task");
                                }
                                aVar.f997b.run();
                                if (f992a.b()) {
                                    f992a.b((Object) "Done executing timer task");
                                }
                            } catch (Exception e2) {
                                f992a.e("TimerSystemJ2me caught an exception:" + e2);
                            }
                        } else {
                            Thread.sleep(20);
                        }
                    } else {
                        try {
                            long currentTimeMillis = aVar.f996a - System.currentTimeMillis();
                            if (currentTimeMillis > 0) {
                                if (f992a.a()) {
                                    f992a.a((Object) "Waiting on condition for " + currentTimeMillis);
                                }
                                this.f994c.wait(currentTimeMillis);
                            }
                        } catch (InterruptedException e3) {
                            if (f992a.d()) {
                                f992a.a("Waiting on syncObj threw an exception: ", e3);
                            }
                        }
                    }
                } else {
                    try {
                        if (f992a.a()) {
                            f992a.a((Object) "No task in queue. Waiting.");
                        }
                        this.f994c.wait();
                    } catch (InterruptedException e4) {
                        if (f992a.d()) {
                            f992a.a("Waiting on syncObj threw an exception: ", e4);
                        }
                    }
                }
            }
        }
    }
}
