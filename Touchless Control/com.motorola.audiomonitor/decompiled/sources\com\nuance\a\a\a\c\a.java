package com.nuance.a.a.a.c;

import android.content.Context;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.b.b.c;
import com.nuance.a.a.a.c.a.b;
import java.util.Vector;

public final class a {

    /* renamed from: a reason: collision with root package name */
    private C0001a f904a = com.nuance.a.a.a.a.b.a.a.a(getClass());

    /* renamed from: b reason: collision with root package name */
    private Context f905b = null;

    /* renamed from: c reason: collision with root package name */
    private boolean f906c = false;

    public a(Vector vector) {
        int i = 0;
        if (vector != null) {
            while (true) {
                int i2 = i;
                if (i2 < vector.size()) {
                    c cVar = (c) vector.get(i2);
                    String a2 = cVar.a();
                    if (cVar.d() == com.nuance.a.a.a.b.b.c.a.f826a) {
                        if (a2.equals("Android_Context")) {
                            this.f905b = (Context) cVar.c();
                            if (this.f904a.b()) {
                                this.f904a.b((Object) "NMSP_DEFINES_ANDROID_CONTEXT is passed in as" + this.f905b);
                            }
                        } else if (a2.equals("Disable_Bluetooth") && new String(cVar.b()).equalsIgnoreCase("TRUE")) {
                            if (this.f904a.b()) {
                                this.f904a.b((Object) "Disable_Bluetooth is true.");
                            }
                            this.f906c = true;
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public final boolean a() {
        if (this.f906c) {
            return false;
        }
        if (this.f905b != null) {
            b a2 = b.a(this.f905b);
            boolean a3 = a2.a();
            a2.b();
            return a3;
        } else if (!this.f904a.e()) {
            return false;
        } else {
            this.f904a.e("ANDROID_CONTEXT parameter is not passed in!!!");
            return false;
        }
    }
}
