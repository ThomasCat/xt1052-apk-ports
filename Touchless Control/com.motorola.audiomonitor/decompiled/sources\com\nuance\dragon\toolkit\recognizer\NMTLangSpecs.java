package com.nuance.dragon.toolkit.recognizer;

public abstract class NMTLangSpecs {
    public static final NMTLanguage[] ALL_LANGUAGES = {CANTONESE_TRADITIONAL, EUROPEAN_FRENCH, FINNISH, BRITISH_ENGLISH, AUSTRALIAN_ENGLISH, JAPANESE, SWEDISH, RUSSIAN, EUROPEAN_PORTUGUESE, POLISH, GERMAN, DUTCH, MANDARIN_TRADITIONAL, CANADIAN_FRENCH, US_SPANISH, UNITED_STATES_ENGLISH, EUROPEAN_SPANISH, MEXICAN_SPANISH, BRAZILIAN_PORTUGUESE, UKRAINIAN, CANTONESE_SIMPLIFIED, DANISH, NORWEGIAN, ITALIAN, MANDARIN_SIMPLIFIED, GREEK, KOREAN, TURKISH};
    public static final NMTLanguage AUSTRALIAN_ENGLISH = new NMTLanguage("ena", "Australian English");
    public static final NMTLanguage BRAZILIAN_PORTUGUESE = new NMTLanguage("ptb", "Brazilian Portuguese");
    public static final NMTLanguage BRITISH_ENGLISH = new NMTLanguage("eng", "British English");
    public static final NMTLanguage CANADIAN_FRENCH = new NMTLanguage("frc", "Canadian French");
    public static final NMTLanguage CANTONESE_SIMPLIFIED = new NMTLanguage("cac", "Cantonese Simplified");
    public static final NMTLanguage CANTONESE_TRADITIONAL = new NMTLanguage("cah", "Cantonese Traditional");
    public static final NMTLanguage DANISH = new NMTLanguage("dad", "Danish");
    public static final NMTLanguage DUTCH = new NMTLanguage("dun", "Dutch");
    public static final NMTLanguage EUROPEAN_FRENCH = new NMTLanguage("frf", "European French");
    public static final NMTLanguage EUROPEAN_PORTUGUESE = new NMTLanguage("ptp", "European Portuguese");
    public static final NMTLanguage EUROPEAN_SPANISH = new NMTLanguage("spe", "European Spanish");
    public static final NMTLanguage FINNISH = new NMTLanguage("fif", "Finnish");
    public static final NMTLanguage GERMAN = new NMTLanguage("ged", "German");
    public static final NMTLanguage GREEK = new NMTLanguage("grg", "Greek");
    public static final NMTLanguage ITALIAN = new NMTLanguage("iti", "Italian");
    public static final NMTLanguage JAPANESE = new NMTLanguage("jpj", "Japanese");
    public static final NMTLanguage KOREAN = new NMTLanguage("kok", "Korean");
    public static final NMTLanguage MANDARIN_SIMPLIFIED = new NMTLanguage("mnc", "Mandarin Simplified");
    public static final NMTLanguage MANDARIN_TRADITIONAL = new NMTLanguage("mnt", "Mandarin Traditional");
    public static final NMTLanguage MEXICAN_SPANISH = new NMTLanguage("spm", "Spanish Mexico");
    public static final NMTLanguage NORWEGIAN = new NMTLanguage("non", "Norwegian");
    public static final NMTLanguage POLISH = new NMTLanguage("plp", "Polish");
    public static final NMTLanguage RUSSIAN = new NMTLanguage("rur", "Russian");
    public static final NMTLanguage SWEDISH = new NMTLanguage("sws", "Swedish");
    public static final NMTLanguage TURKISH = new NMTLanguage("trt", "Turkish");
    public static final NMTLanguage UKRAINIAN = new NMTLanguage("uku", "Ukrainian");
    public static final NMTLanguage UNITED_STATES_ENGLISH = new NMTLanguage("enu", "United States English");
    public static final NMTLanguage US_SPANISH = new NMTLanguage("spu", "US Spanish");
}
