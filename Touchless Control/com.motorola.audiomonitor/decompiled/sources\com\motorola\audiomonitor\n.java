package com.motorola.audiomonitor;

import com.nuance.dragon.toolkit.elvis.ElvisResult;

public enum n {
    ERROR(-1),
    NONE(0),
    RECOGNIZED(1),
    SPEECH(2),
    QUIET(3);
    

    /* renamed from: f reason: collision with root package name */
    int f522f;

    private n(int i) {
        this.f522f = i;
    }

    public static n a(int i) {
        switch (i) {
            case ElvisResult.GATE_CONFIDENCE_UNAVAILABLE /*-1*/:
                return ERROR;
            case 0:
                return NONE;
            case 1:
                return RECOGNIZED;
            case 2:
                return SPEECH;
            case 3:
                return QUIET;
            default:
                return ERROR;
        }
    }
}
