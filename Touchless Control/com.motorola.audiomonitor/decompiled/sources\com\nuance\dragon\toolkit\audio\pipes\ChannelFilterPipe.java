package com.nuance.dragon.toolkit.audio.pipes;

import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.audio.MultiChannelAudioChunk;
import java.util.LinkedList;

public class ChannelFilterPipe extends SingleSinkPipe {

    /* renamed from: a reason: collision with root package name */
    private final int f1146a;

    /* renamed from: b reason: collision with root package name */
    private final LinkedList f1147b;

    /* renamed from: c reason: collision with root package name */
    private final AudioType f1148c;

    /* renamed from: d reason: collision with root package name */
    private final boolean f1149d;

    public ChannelFilterPipe(int i, AudioType audioType) {
        this(i, audioType, false);
    }

    public ChannelFilterPipe(int i, AudioType audioType, boolean z) {
        this.f1146a = i;
        this.f1147b = new LinkedList();
        this.f1148c = audioType;
        this.f1149d = z;
    }

    /* access modifiers changed from: protected */
    public void chunksAvailable(AudioSource audioSource, AudioSink audioSink) {
        while (true) {
            MultiChannelAudioChunk multiChannelAudioChunk = (MultiChannelAudioChunk) audioSource.getAudioChunkForSink(audioSink);
            if (multiChannelAudioChunk == null) {
                return;
            }
            if (this.f1146a < multiChannelAudioChunk.channelCount) {
                this.f1147b.add(multiChannelAudioChunk.getSingleChannelChunk(this.f1146a));
                notifyChunksAvailable();
            } else if (this.f1149d) {
                Encoding encoding = this.f1148c.encoding;
                Encoding encoding2 = Encoding.PCM_16;
                AudioChunk singleChannelChunk = multiChannelAudioChunk.getSingleChannelChunk(0);
                this.f1147b.add(new AudioChunk(singleChannelChunk.audioType, new short[singleChannelChunk.audioShorts.length], singleChannelChunk.audioTimestamp));
                notifyChunksAvailable();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void framesDropped(AudioSource audioSource, AudioSink audioSink) {
        notifyFramesDropped();
    }

    /* access modifiers changed from: protected */
    public AudioChunk getAudioChunk() {
        if (!this.f1147b.isEmpty()) {
            return (AudioChunk) this.f1147b.remove();
        }
        return null;
    }

    public AudioType getAudioType() {
        return this.f1148c;
    }

    public int getChunksAvailable() {
        return getChunksAvailableFromSource();
    }

    public boolean isActive() {
        return isSourceActive();
    }

    /* access modifiers changed from: protected */
    public void sourceClosed(AudioSource audioSource, AudioSink audioSink) {
        notifySourceClosed();
    }
}
