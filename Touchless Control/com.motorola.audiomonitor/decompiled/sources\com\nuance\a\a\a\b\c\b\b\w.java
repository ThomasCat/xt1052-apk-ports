package com.nuance.a.a.a.b.c.b.b;

import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.b.c.c.c;
import com.nuance.a.a.a.b.c.c.l;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.Frequencies;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

public class w extends f implements l {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f882a = a.a(w.class);

    /* renamed from: b reason: collision with root package name */
    private Vector f883b = new Vector();

    public w() {
        super(16);
    }

    public w(byte[] bArr) {
        super(16);
        int i = 0;
        while (i < bArr.length) {
            int i2 = i + 1;
            byte b2 = bArr[i] & 255;
            int a2 = a(bArr, i2);
            int a3 = i2 + a(a2);
            byte[] bArr2 = new byte[a2];
            System.arraycopy(bArr, a3, bArr2, 0, bArr2.length);
            i = a2 + a3;
            switch (b2) {
                case 4:
                    this.f883b.addElement(new d(bArr2));
                    break;
                case 5:
                    this.f883b.addElement(new n());
                    break;
                case 16:
                    this.f883b.addElement(new w(bArr2));
                    break;
                case Frequencies.FREQ_22KHZ /*22*/:
                    this.f883b.addElement(new b(bArr2));
                    break;
                case 192:
                    this.f883b.addElement(new k(bArr2));
                    break;
                case 193:
                    this.f883b.addElement(new aa(bArr2));
                    break;
                case 224:
                    this.f883b.addElement(new i(bArr2, 0));
                    break;
                default:
                    f882a.e("PDXSequence() Unknown PDXClass type: " + b2 + ". ");
                    break;
            }
        }
    }

    public final int a() {
        return this.f883b.size();
    }

    public final void a(c cVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("value is null.");
        } else if (((f) cVar).c() != 224) {
            f882a.e("PDXSequence.addDictionary() value is not a valid dictionary.");
            throw new IllegalArgumentException("value is not a valid dictionary. ");
        } else {
            this.f883b.addElement(cVar);
        }
    }

    public final void a(l lVar) {
        if (lVar == null) {
            throw new IllegalArgumentException("value is null.");
        } else if (((f) lVar).c() != 16) {
            f882a.e("PDXSequence.addSequence() value is not a valid sequence.");
            throw new IllegalArgumentException("value is not a valid sequence. ");
        } else {
            this.f883b.addElement(lVar);
        }
    }

    public final void a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("value is null.");
        }
        this.f883b.addElement(new b(str));
    }

    public final short b(int i) {
        if (i < this.f883b.size()) {
            return ((f) this.f883b.elementAt(i)).c();
        }
        f882a.e("PDXSequence.getType() index " + i + " is out of range. ");
        throw new IndexOutOfBoundsException();
    }

    public final void b(String str) {
        if (str == null) {
            throw new IllegalArgumentException("value is null.");
        }
        this.f883b.addElement(new aa(str));
    }

    public final void b(byte[] bArr) {
        if (bArr == null) {
            throw new IllegalArgumentException("value is null.");
        }
        this.f883b.addElement(new d(bArr));
    }

    public final byte[] b() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Enumeration elements = this.f883b.elements();
        while (elements.hasMoreElements()) {
            f fVar = (f) elements.nextElement();
            try {
                switch (fVar.c()) {
                    case 4:
                        byteArrayOutputStream.write(((d) fVar).b());
                        break;
                    case 5:
                        byteArrayOutputStream.write(((n) fVar).a());
                        break;
                    case 16:
                        byteArrayOutputStream.write(((w) fVar).b());
                        break;
                    case Frequencies.FREQ_22KHZ /*22*/:
                        byteArrayOutputStream.write(((b) fVar).b());
                        break;
                    case 192:
                        byteArrayOutputStream.write(((k) fVar).b());
                        break;
                    case 193:
                        byteArrayOutputStream.write(((aa) fVar).b());
                        break;
                    case 224:
                        byteArrayOutputStream.write(((i) fVar).d());
                        break;
                }
            } catch (IOException e2) {
                f882a.e("PDXSequence.toByteArray() " + e2.toString() + ". ");
            }
        }
        return super.a(byteArrayOutputStream.toByteArray());
    }

    public final int c(int i) {
        if (i >= this.f883b.size()) {
            f882a.e("PDXSequence.getInteger() index " + i + " is out of range. ");
            throw new IndexOutOfBoundsException();
        }
        f fVar = (f) this.f883b.elementAt(i);
        if (fVar.c() == 192) {
            return ((k) fVar).a();
        }
        f882a.e("PDXSequence.getInteger() index " + i + " is not a PDXInteger. ");
        throw new RuntimeException("index is of wrong type.");
    }

    public final byte[] d(int i) {
        if (i >= this.f883b.size()) {
            f882a.e("PDXSequence.getByteString() index " + i + " is out of range. ");
            throw new IndexOutOfBoundsException();
        }
        f fVar = (f) this.f883b.elementAt(i);
        if (fVar.c() == 4) {
            return ((d) fVar).a();
        }
        f882a.e("PDXSequence.getByteString() index " + i + " is not a PDXByteString. ");
        throw new RuntimeException("index is of wrong type.");
    }

    public final String e(int i) {
        if (i >= this.f883b.size()) {
            f882a.e("PDXSequence.getUTF8String() index " + i + " is out of range. ");
            throw new IndexOutOfBoundsException();
        }
        f fVar = (f) this.f883b.elementAt(i);
        if (fVar.c() == 193) {
            return ((aa) fVar).a();
        }
        f882a.e("PDXSequence.getUTF8String() index " + i + " is not a PDXUTF8String. ");
        throw new RuntimeException("index is of wrong type.");
    }

    public final String f(int i) {
        if (i >= this.f883b.size()) {
            f882a.e("PDXSequence.getAsciiString() index " + i + " is out of range. ");
            throw new IndexOutOfBoundsException();
        }
        f fVar = (f) this.f883b.elementAt(i);
        if (fVar.c() == 22) {
            return ((b) fVar).a();
        }
        f882a.e("PDXSequence.getAsciiString() index " + i + " is not a PDXAsciiString. ");
        throw new RuntimeException("index is of wrong type.");
    }

    public final c g(int i) {
        if (i >= this.f883b.size()) {
            f882a.e("PDXSequence.getDictionary() index " + i + " is out of range. ");
            throw new IndexOutOfBoundsException();
        }
        f fVar = (f) this.f883b.elementAt(i);
        if (fVar.c() == 224) {
            return (i) fVar;
        }
        f882a.e("PDXSequence.getDictionary() index " + i + " is not a PDXDictionary. ");
        throw new RuntimeException("index is of wrong type.");
    }

    public final l h(int i) {
        if (i >= this.f883b.size()) {
            f882a.e("PDXSequence.getSequence() index " + i + " is out of range. ");
            throw new IndexOutOfBoundsException();
        }
        f fVar = (f) this.f883b.elementAt(i);
        if (fVar.c() == 16) {
            return (w) fVar;
        }
        f882a.e("PDXSequence.getSequence() index " + i + " is not a PDXSequence. ");
        throw new RuntimeException("index is of wrong type.");
    }

    public final void i(int i) {
        this.f883b.addElement(new k(i));
    }

    /* access modifiers changed from: protected */
    public final String j(int i) {
        String str;
        String str2 = "";
        String str3 = "";
        String str4 = "";
        int i2 = 0;
        while (true) {
            str = str3;
            if (i2 >= i - 1) {
                break;
            }
            str3 = str + "    ";
            i2++;
        }
        String str5 = i > 0 ? str + "    " : str4;
        Enumeration elements = this.f883b.elements();
        String str6 = str2 + "[ \n";
        while (true) {
            String str7 = str6;
            if (!elements.hasMoreElements()) {
                return str7 + str + "] ";
            }
            f fVar = (f) elements.nextElement();
            switch (fVar.c()) {
                case 4:
                    str7 = str7 + str5 + "<BYTES> \"" + ((d) fVar).a() + "\" ";
                    break;
                case 5:
                    str7 = str7 + str5 + "<NULL> ";
                    break;
                case 16:
                    str7 = str7 + str5 + ((w) fVar).j(i + 1);
                    break;
                case Frequencies.FREQ_22KHZ /*22*/:
                    str7 = str7 + str5 + "<ASCII> \"" + ((b) fVar).a() + "\" ";
                    break;
                case 192:
                    str7 = str7 + str5 + "<INT> " + ((k) fVar).a();
                    break;
                case 193:
                    str7 = str7 + str5 + "<UTF8> \"" + ((aa) fVar).a() + "\" ";
                    break;
                case 224:
                    str7 = str7 + str5 + ((i) fVar).b(i + 1);
                    break;
            }
            if (elements.hasMoreElements()) {
                str7 = str7 + ",";
            }
            str6 = str7 + "\n";
        }
    }

    public final String toString() {
        return j(0);
    }
}
