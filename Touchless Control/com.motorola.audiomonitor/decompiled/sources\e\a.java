package e;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile.ServiceListener;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.util.Log;
import c.d;
import com.motorola.audiomonitor.aa;
import com.motorola.audiomonitor.actions.utils.ak;
import com.motorola.audiomonitor.b;
import com.motorola.audiomonitor.s;
import h.c;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public class a implements s {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f2197a = ("AMonitor." + a.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f2198b = com.motorola.audiomonitor.a.f160b;
    private static a j;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final Context f2199c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public BluetoothHeadset f2200d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public BluetoothDevice f2201e;

    /* renamed from: f reason: collision with root package name */
    private boolean f2202f = false;

    /* renamed from: g reason: collision with root package name */
    private boolean f2203g = false;

    /* renamed from: h reason: collision with root package name */
    private aa f2204h;
    private final Handler i = new Handler();
    private final Set k = new CopyOnWriteArraySet();
    private final BroadcastReceiver l = new b(this);
    private final ServiceListener m = new c(this);
    private final c n = new c(this.i, new d(this));

    private a(Context context) {
        if (f2198b) {
            Log.d(f2197a, "Bluetooth SCO Enabler created.");
        }
        this.f2199c = context;
        i();
        k();
    }

    public static synchronized a a(Context context) {
        a aVar;
        synchronized (a.class) {
            if (j == null) {
                j = new a(context);
            }
            aVar = j;
        }
        return aVar;
    }

    /* access modifiers changed from: private */
    public synchronized void a(BluetoothHeadset bluetoothHeadset) {
        this.f2200d = bluetoothHeadset;
    }

    /* access modifiers changed from: private */
    public synchronized void a(boolean z) {
        if (f2198b) {
            Log.d(f2197a, "SCO Audio Connection State : " + z);
        }
        if (this.f2203g) {
            if (z) {
                this.n.a();
                d.a(this.f2199c, "bt_triggers");
                if (this.f2204h != null) {
                    this.f2204h.a(0.0f, this);
                }
            } else if (this.f2204h != null) {
                this.f2204h.a(this);
            }
        }
    }

    private void b(boolean z) {
        for (e eVar : this.k) {
            if (eVar != null) {
                eVar.a(z);
            }
        }
    }

    public static synchronized void e() {
        synchronized (a.class) {
            if (j != null) {
                j.d();
                j = null;
            }
        }
    }

    public static synchronized void f() {
        synchronized (a.class) {
            if (j != null) {
                j.n();
            } else if (f2198b) {
                Log.d(f2197a, "VR command not handled.");
            }
        }
    }

    private void i() {
        IntentFilter intentFilter = new IntentFilter("android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED");
        intentFilter.addAction("android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED");
        intentFilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        this.f2199c.registerReceiver(this.l, intentFilter);
    }

    private void j() {
        this.f2199c.unregisterReceiver(this.l);
    }

    /* access modifiers changed from: private */
    public void k() {
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter != null && defaultAdapter.isEnabled()) {
            defaultAdapter.getProfileProxy(this.f2199c, this.m, 1);
        }
    }

    private void l() {
        if (this.f2200d != null) {
            BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
            if (defaultAdapter != null) {
                defaultAdapter.closeProfileProxy(1, this.f2200d);
            }
            this.f2200d = null;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void m() {
        if (this.f2200d == null || this.f2200d.getConnectedDevices().isEmpty()) {
            if (f2198b) {
                Log.d(f2197a, "BT Headset Disconnected.");
            }
            ak.d();
            b(false);
        } else {
            if (f2198b) {
                Log.d(f2197a, "BT Headset Connected.");
            }
            ak.b(this.f2199c);
            b(true);
        }
    }

    private synchronized boolean n() {
        boolean z = false;
        synchronized (this) {
            if (this.f2202f) {
                Log.w(f2197a, "VR trigger currently being handled.");
            } else if (this.f2204h == null) {
                Log.w(f2197a, "VR trigger not requested yet.");
            } else if (this.f2200d == null) {
                Log.w(f2197a, "No BluetoothDevice available");
            } else {
                List connectedDevices = this.f2200d.getConnectedDevices();
                if (connectedDevices.isEmpty()) {
                    Log.w(f2197a, "No connected BT Headset Dvice");
                } else {
                    if (f2198b) {
                        Log.d(f2197a, "starting BT VR");
                    }
                    b.a().a(true, this.f2199c);
                    this.f2201e = (BluetoothDevice) connectedDevices.get(0);
                    this.f2202f = true;
                    this.f2203g = true;
                    this.f2200d.startVoiceRecognition(this.f2201e);
                    this.n.a(6000);
                    z = true;
                }
            }
        }
        return z;
    }

    private synchronized void o() {
        if (this.f2203g) {
            if (!(this.f2200d == null || this.f2201e == null)) {
                this.f2200d.stopVoiceRecognition(this.f2201e);
            }
            this.f2203g = false;
            if (f2198b) {
                Log.d(f2197a, "BT VR stopped");
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void p() {
        if (this.f2202f) {
            o();
            this.f2202f = false;
        }
    }

    public synchronized void a(aa aaVar) {
        this.f2204h = aaVar;
        if (this.f2204h == null && this.f2200d != null && this.f2201e != null && this.f2203g) {
            this.f2200d.stopVoiceRecognition(this.f2201e);
        }
    }

    public synchronized void a(e eVar) {
        if (eVar == null) {
            Log.w(f2197a, "add invalid audio listener");
        } else {
            this.k.add(eVar);
            if (this.f2200d != null && !this.f2200d.getConnectedDevices().isEmpty()) {
                eVar.a(true);
            }
        }
    }

    public void b() {
    }

    public void c() {
        p();
    }

    public synchronized void d() {
        p();
        j();
        l();
        this.n.a();
        this.f2204h = null;
        this.f2201e = null;
        ak.d();
        this.k.clear();
    }

    public void e_() {
    }
}
