package com.motorola.audiomonitor.uis;

import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup.LayoutParams;

class r implements OnLayoutChangeListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ AOVMicFragment f718a;

    r(AOVMicFragment aOVMicFragment) {
        this.f718a = aOVMicFragment;
    }

    public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        int measuredHeight = this.f718a.r.getMeasuredHeight();
        this.f718a.H = this.f718a.n.getLayoutParams();
        this.f718a.I = this.f718a.o.getLayoutParams();
        if (measuredHeight != this.f718a.E) {
            this.f718a.E = measuredHeight;
            this.f718a.F = this.f718a.m.getMeasuredHeight();
            this.f718a.D = 5;
            this.f718a.C = 5;
            LayoutParams g2 = this.f718a.H;
            int c2 = this.f718a.F + this.f718a.D;
            this.f718a.H.height = c2;
            g2.width = c2;
            LayoutParams e2 = this.f718a.I;
            int c3 = this.f718a.F + this.f718a.C;
            this.f718a.I.height = c3;
            e2.width = c3;
            this.f718a.n.invalidate();
            this.f718a.o.invalidate();
        }
    }
}
