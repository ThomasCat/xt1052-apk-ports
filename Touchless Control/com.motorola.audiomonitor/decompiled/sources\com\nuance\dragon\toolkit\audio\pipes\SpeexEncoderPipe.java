package com.nuance.dragon.toolkit.audio.pipes;

import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.audio.util.SpeexEncoder;

public class SpeexEncoderPipe extends ConverterPipe {
    public static final int DEFAULT_STOP_ON_END_OF_SPEECH = 1;
    public static final int DEFAULT_VAD_BEGIN_DELAY = 0;
    public static final int DEFAULT_VAD_BEGIN_LEN = 15;
    public static final int DEFAULT_VAD_BEGIN_THRESHOLD = 7;
    public static final int DEFAULT_VAD_END_LEN = 50;
    public static final int DEFAULT_VAD_END_THRESHOLD = 5;
    public static final int DEFAULT_VAD_HISTORY_LEN = 50;
    public static final int DEFAULT_VAD_INTERSPEECH_LEN = 35;
    public static final int DEFAULT_VAD_LONG_UTTERANCE = 0;

    /* renamed from: a reason: collision with root package name */
    private final SpeexEncoder f1185a;

    /* renamed from: b reason: collision with root package name */
    private final int f1186b;

    /* renamed from: c reason: collision with root package name */
    private final int f1187c;

    /* renamed from: d reason: collision with root package name */
    private final int f1188d;

    /* renamed from: e reason: collision with root package name */
    private final int f1189e;

    /* renamed from: f reason: collision with root package name */
    private final int f1190f;

    /* renamed from: g reason: collision with root package name */
    private final int f1191g;

    /* renamed from: h reason: collision with root package name */
    private final int f1192h;
    private final int i;
    private final int j;

    public SpeexEncoderPipe() {
        this(1, 0, 50, 15, 7, 0, 50, 5, 35);
    }

    public SpeexEncoderPipe(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10) {
        this.f1186b = i2;
        this.f1187c = i3;
        this.f1188d = i4;
        this.f1189e = i5;
        this.f1190f = i6;
        this.f1191g = i7;
        this.f1192h = i8;
        this.i = i9;
        this.j = i10;
        this.f1185a = new SpeexEncoder();
    }

    private static AudioType a(AudioType audioType) {
        return audioType.frequency == 8000 ? AudioType.SPEEX_NB : AudioType.SPEEX_WB;
    }

    /* access modifiers changed from: protected */
    public void cleanup() {
        this.f1185a.a();
    }

    /* access modifiers changed from: protected */
    public AudioChunk[] convert(AudioChunk audioChunk) {
        return this.f1185a.a(audioChunk.audioShorts, 0, audioChunk.audioShorts.length);
    }

    public AudioType getAudioType() {
        AudioSource connectedSource = getConnectedSource();
        if (connectedSource != null) {
            AudioType audioType = connectedSource.getAudioType();
            if (audioType != null) {
                return a(audioType);
            }
        }
        return AudioType.UNKNOWN;
    }

    /* access modifiers changed from: protected */
    public void initialize(AudioType audioType) {
        this.f1185a.a(a(audioType), this.f1186b, this.f1187c, this.f1188d, this.f1189e, this.f1190f, this.f1191g, this.f1192h, this.i, this.j);
    }

    /* access modifiers changed from: protected */
    public final boolean isAudioSourceTypeSupported(AudioType audioType) {
        return audioType.encoding == Encoding.PCM_16;
    }
}
