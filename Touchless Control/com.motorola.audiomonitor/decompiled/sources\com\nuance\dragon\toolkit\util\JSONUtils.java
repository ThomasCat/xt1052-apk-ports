package com.nuance.dragon.toolkit.util;

import com.nuance.dragon.toolkit.util.internal.d;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import org.json.JSONObject;

public class JSONUtils {
    public static JSONObject readFromFile(File file) {
        d.a("file", (Object) file);
        return readFromStream(new FileInputStream(file));
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0036  */
    public static JSONObject readFromStream(InputStream inputStream) {
        InputStreamReader inputStreamReader;
        d.a("is", (Object) inputStream);
        StringBuffer stringBuffer = new StringBuffer();
        try {
            inputStreamReader = new InputStreamReader(inputStream);
            try {
                char[] cArr = new char[1024];
                while (true) {
                    int read = inputStreamReader.read(cArr);
                    if (read > 0) {
                        stringBuffer.append(cArr, 0, read);
                    } else {
                        inputStreamReader.close();
                        inputStreamReader.close();
                        return new JSONObject(stringBuffer.toString());
                    }
                }
            } catch (Throwable th) {
                th = th;
                if (inputStreamReader == null) {
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            inputStreamReader = null;
            if (inputStreamReader == null) {
                inputStreamReader.close();
            } else if (inputStream != null) {
                inputStream.close();
            }
            throw th;
        }
    }

    public static void saveToFile(JSONObject jSONObject, File file) {
        d.a("file", (Object) file);
        saveToStream(jSONObject, new FileOutputStream(file));
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001f  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0023  */
    public static void saveToStream(JSONObject jSONObject, OutputStream outputStream) {
        OutputStreamWriter outputStreamWriter;
        d.a("json", (Object) jSONObject);
        d.a("os", (Object) outputStream);
        try {
            outputStreamWriter = new OutputStreamWriter(outputStream);
            try {
                outputStreamWriter.write(jSONObject.toString());
                outputStreamWriter.close();
            } catch (Throwable th) {
                th = th;
                if (outputStreamWriter == null) {
                    outputStreamWriter.close();
                } else if (outputStream != null) {
                    outputStream.close();
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            outputStreamWriter = null;
            if (outputStreamWriter == null) {
            }
            throw th;
        }
    }
}
