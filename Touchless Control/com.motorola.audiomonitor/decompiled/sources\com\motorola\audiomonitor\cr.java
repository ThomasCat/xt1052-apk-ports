package com.motorola.audiomonitor;

import com.motorola.audiomonitor.service.d;
import h.o;
import h.q;
import java.util.Arrays;
import java.util.List;

class cr extends cb {

    /* renamed from: c reason: collision with root package name */
    final /* synthetic */ cn f470c;

    /* renamed from: d reason: collision with root package name */
    private final float[] f471d = new float[1];

    /* renamed from: e reason: collision with root package name */
    private final String[] f472e = new String[1];

    public cr(cn cnVar, p pVar, d dVar) {
        this.f470c = cnVar;
        super(pVar, dVar);
    }

    public void a(int i) {
        this.f470c.f428g.a(i);
    }

    public void a(List list) {
        if (cn.f423b) {
            synchronized (this) {
                if (list != null) {
                    this.f470c.a(new o().a(q.TYPE_ENROLL_RAW, "/data/adspd/", list, 16000, "v0"));
                }
            }
        }
    }

    public boolean d() {
        this.f399a = new GenericRecognizer("custom_acoustic_22132.raw.tmp", "language.raw", "custom_search_22132.raw.tmp");
        this.f400b = bz.d();
        this.f399a.configure(this.f400b);
        this.f470c.f428g.a(6, 0);
        return true;
    }

    public boolean e() {
        if (this.f399a == null) {
            return false;
        }
        if (cn.f423b) {
            this.f470c.a("numWords=" + this.f399a.getRecognizedWords(0.0f, this.f471d, this.f472e) + ", scores=" + Arrays.toString(this.f471d) + ", words=" + Arrays.toString(this.f472e));
        }
        if (this.f399a.getRecognizedWords(this.f400b.getRecogThreshold(), this.f471d, this.f472e) > 0) {
            this.f470c.a("verifyScore=" + Arrays.toString(this.f472e) + ", id=" + Arrays.toString(this.f471d));
            float verifyUdt = this.f399a.verifyUdt();
            if (verifyUdt > this.f400b.getVerificationThreshold()) {
                this.f470c.a("UDT accepted: SV score=" + verifyUdt);
                this.f470c.f428g.a(7, 0);
                return false;
            }
            this.f470c.a("ERROR: SV verification failed, SV score=" + verifyUdt);
            this.f470c.a(13);
            this.f470c.f428g.a(8, 13);
            return false;
        }
        this.f470c.a("phrase not recognized");
        this.f470c.a(12);
        this.f470c.f428g.a(10, 12);
        return true;
    }

    public void f() {
        this.f470c.a("ERROR: verify recognition failed");
        this.f470c.a(20);
        this.f470c.f428g.a(9, 20);
    }

    public boolean g() {
        int g2 = this.f470c.f();
        if (g2 != 0) {
            this.f470c.f428g.a(5, g2);
            return false;
        }
        this.f470c.b(true);
        return true;
    }

    public void h() {
        this.f470c.b(false);
    }
}
