package com.motorola.audiomonitor.uis;

import android.text.Html;

class aa implements Runnable {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ int f636a;

    /* renamed from: b reason: collision with root package name */
    final /* synthetic */ int f637b;

    /* renamed from: c reason: collision with root package name */
    final /* synthetic */ int f638c;

    /* renamed from: d reason: collision with root package name */
    final /* synthetic */ String f639d;

    /* renamed from: e reason: collision with root package name */
    final /* synthetic */ int f640e;

    /* renamed from: f reason: collision with root package name */
    final /* synthetic */ z f641f;

    aa(z zVar, int i, int i2, int i3, String str, int i4) {
        this.f641f = zVar;
        this.f636a = i;
        this.f637b = i2;
        this.f638c = i3;
        this.f639d = str;
        this.f640e = i4;
    }

    public void run() {
        if (!this.f641f.f764a) {
            this.f641f.f764a = true;
            this.f641f.i.gravity = this.f636a;
            this.f641f.i.x = this.f637b;
            this.f641f.i.y = this.f638c;
            this.f641f.f769f.setText(Html.fromHtml(this.f639d));
            this.f641f.f767d.addView(this.f641f.f768e, this.f641f.i);
            if (this.f640e > 0) {
                this.f641f.f766c.postDelayed(new ab(this), (long) this.f640e);
            }
        }
    }
}
