package com.nuance.dragon.toolkit.util.internal;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

public class g extends AbstractList {

    /* renamed from: a reason: collision with root package name */
    private final ArrayList f2134a;

    public g() {
        this.f2134a = new ArrayList(0);
    }

    protected g(int i) {
        this.f2134a = new ArrayList(i);
    }

    public g(Object obj) {
        this.f2134a = new ArrayList(1);
        this.f2134a.add(obj);
    }

    public g(List list) {
        this.f2134a = new ArrayList(list.size());
        this.f2134a.addAll(list);
    }

    public g(Object... objArr) {
        this.f2134a = new ArrayList(objArr.length);
        for (Object add : objArr) {
            this.f2134a.add(add);
        }
    }

    /* access modifiers changed from: protected */
    public final void addInternal(Object obj) {
        this.f2134a.add(obj);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        g gVar = (g) obj;
        return this.f2134a == null ? gVar.f2134a == null : this.f2134a.equals(gVar.f2134a);
    }

    public Object get(int i) {
        return this.f2134a.get(i);
    }

    public final int hashCode() {
        return (this.f2134a == null ? 0 : this.f2134a.hashCode()) + (super.hashCode() * 31);
    }

    public final int size() {
        return this.f2134a.size();
    }
}
