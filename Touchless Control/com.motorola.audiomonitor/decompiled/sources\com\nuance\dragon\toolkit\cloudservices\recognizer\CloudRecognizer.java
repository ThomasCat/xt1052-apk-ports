package com.nuance.dragon.toolkit.cloudservices.recognizer;

import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.pipes.SimplePipe;
import com.nuance.dragon.toolkit.cloudservices.AudioParam;
import com.nuance.dragon.toolkit.cloudservices.CloudServices;
import com.nuance.dragon.toolkit.cloudservices.DataParam;
import com.nuance.dragon.toolkit.cloudservices.Param;
import com.nuance.dragon.toolkit.cloudservices.Transaction;
import com.nuance.dragon.toolkit.cloudservices.TransactionError;
import com.nuance.dragon.toolkit.cloudservices.TransactionResult;
import com.nuance.dragon.toolkit.util.internal.d;

public class CloudRecognizer {

    /* renamed from: a reason: collision with root package name */
    private final CloudServices f1476a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final SimplePipe f1477b = new SimplePipe();
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public a f1478c;

    public interface Listener {
        void onError(CloudRecognitionError cloudRecognitionError);

        void onResult(CloudRecognitionResult cloudRecognitionResult);
    }

    final class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public Transaction f1483a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public boolean f1484b;
        /* access modifiers changed from: private */

        /* renamed from: c reason: collision with root package name */
        public boolean f1485c;

        private a() {
        }

        /* synthetic */ a(byte b2) {
            this();
        }
    }

    public CloudRecognizer(CloudServices cloudServices) {
        d.a("cloudServices", (Object) cloudServices);
        this.f1476a = cloudServices;
    }

    public void cancel() {
        this.f1477b.disconnectAudioSource();
        if (this.f1478c != null) {
            Transaction b2 = this.f1478c.f1483a;
            this.f1478c = null;
            b2.cancel();
        }
    }

    public void processResult() {
        boolean z = true;
        d.a((Object) this, this.f1478c != null, "No recognition in progress");
        if (this.f1478c.f1485c) {
            z = false;
        }
        d.a((Object) this, z, "Already processing");
        if (this.f1478c != null) {
            this.f1478c.f1485c = true;
            if (this.f1478c.f1484b) {
                this.f1478c.f1483a.finish();
            }
        }
    }

    public void sendParam(Param param) {
        boolean z = true;
        d.a((Object) this, this.f1478c != null, "No recognition in progress");
        if (this.f1478c.f1485c) {
            z = false;
        }
        d.a((Object) this, z, "Can't add params after already processing");
        if (this.f1478c != null) {
            this.f1478c.f1483a.addParam(param);
        }
    }

    public void startRecognition(final RecogSpec recogSpec, AudioSource audioSource, final Listener listener) {
        d.a("recogSpec", (Object) recogSpec);
        d.a("audioSource", (Object) audioSource);
        d.a("resultListener", (Object) listener);
        cancel();
        final a aVar = new a(0);
        this.f1478c = aVar;
        aVar.f1483a = new Transaction(recogSpec.getCommand(), recogSpec.getSettings(), new com.nuance.dragon.toolkit.cloudservices.Transaction.Listener() {
            public final void onTransactionError(Transaction transaction, TransactionError transactionError) {
                if (aVar == CloudRecognizer.this.f1478c) {
                    CloudRecognizer.this.f1478c = null;
                }
                CloudRecognizer.this.f1477b.disconnectAudioSource();
                listener.onError(new CloudRecognitionError(transactionError));
            }

            public final void onTransactionResult(Transaction transaction, TransactionResult transactionResult, boolean z) {
                if (aVar == CloudRecognizer.this.f1478c) {
                    if (transactionResult.isFinal()) {
                        CloudRecognizer.this.f1478c = null;
                    }
                    CloudRecognizer.this.f1477b.disconnectAudioSource();
                    listener.onResult(new CloudRecognitionResult(transactionResult));
                }
            }

            public final void onTransactionStarted(Transaction transaction) {
                if (aVar == CloudRecognizer.this.f1478c) {
                    aVar.f1484b = true;
                    for (DataParam addParam : recogSpec.getDelayedParams()) {
                        aVar.f1483a.addParam(addParam);
                    }
                    if (aVar.f1485c) {
                        aVar.f1483a.finish();
                    }
                }
            }
        }, recogSpec.getTimeout());
        this.f1477b.connectAudioSource(audioSource);
        for (DataParam addParam : recogSpec.getParams()) {
            aVar.f1483a.addParam(addParam);
        }
        aVar.f1483a.addParam(new AudioParam(recogSpec.getAudioParam(), this.f1477b));
        this.f1476a.addTransaction(aVar.f1483a, 6);
    }
}
