package f;

import android.util.Log;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.InitializeListener;
import com.nuance.dragon.toolkit.elvis.Grammar;

class j implements InitializeListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ Grammar f2244a;

    /* renamed from: b reason: collision with root package name */
    final /* synthetic */ i f2245b;

    j(i iVar, Grammar grammar) {
        this.f2245b = iVar;
        this.f2244a = grammar;
    }

    public void onLoaded(ElvisRecognizer elvisRecognizer, boolean z) {
        if (!z) {
            Log.e(i.f2236a, "Init failure " + this.f2245b.p);
            return;
        }
        if (i.f2237b) {
            Log.d(i.f2236a, "Init Success " + this.f2245b.p);
        }
        if (this.f2245b.f2238c != null) {
            this.f2245b.f2238c.loadGrammar(this.f2244a, this.f2245b.j);
        }
    }
}
