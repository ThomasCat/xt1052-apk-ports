package com.nuance.dragon.toolkit.audio;

import com.nuance.dragon.toolkit.util.internal.d;

public abstract class AudioPipe extends AudioSource {

    /* renamed from: a reason: collision with root package name */
    private final AudioSink f1000a = new AudioSink() {
        public final void chunksAvailable(AudioSource audioSource) {
            AudioPipe.this.chunksAvailable(audioSource, this);
        }

        public final void framesDropped(AudioSource audioSource) {
            AudioPipe.this.framesDropped(audioSource, this);
        }

        public final boolean isAudioSourceTypeSupported(AudioType audioType) {
            return AudioPipe.this.isAudioSourceTypeSupported(audioType);
        }

        public final void sourceClosed(AudioSource audioSource) {
            AudioPipe.this.sourceClosed(audioSource, this);
        }
    };

    /* renamed from: b reason: collision with root package name */
    private boolean f1001b;

    /* access modifiers changed from: protected */
    public abstract void chunksAvailable(AudioSource audioSource, AudioSink audioSink);

    public void connectAudioSource(AudioSource audioSource) {
        d.a((Object) this, !this.f1001b, "Source was already connected to this pipe");
        this.f1001b = true;
        this.f1000a.connectAudioSource(audioSource);
    }

    public AudioSource disconnectAudioSource() {
        AudioSource connectedSource = this.f1000a.getConnectedSource();
        if (connectedSource == null) {
            return null;
        }
        boolean isActive = connectedSource.isActive();
        this.f1000a.disconnectAudioSource();
        if (isActive) {
            this.f1000a.sourceClosed(connectedSource);
        }
        this.f1001b = false;
        return connectedSource;
    }

    /* access modifiers changed from: protected */
    public abstract void framesDropped(AudioSource audioSource, AudioSink audioSink);

    /* access modifiers changed from: protected */
    public final AbstractAudioChunk getAudioChunkFromSource() {
        AudioSource connectedSource = this.f1000a.getConnectedSource();
        if (connectedSource == null) {
            return null;
        }
        return connectedSource.getAudioChunkForSink(this.f1000a);
    }

    /* access modifiers changed from: protected */
    public final AudioType getAudioTypeFromSource() {
        AudioSource connectedSource = this.f1000a.getConnectedSource();
        return connectedSource == null ? AudioType.UNKNOWN : connectedSource.getAudioType();
    }

    /* access modifiers changed from: protected */
    public final int getChunksAvailableFromSource() {
        AudioSource connectedSource = this.f1000a.getConnectedSource();
        if (connectedSource == null) {
            return 0;
        }
        return connectedSource.getChunksAvailableForSink(this.f1000a);
    }

    /* access modifiers changed from: protected */
    public final AudioSource getConnectedSource() {
        return this.f1000a.getConnectedSource();
    }

    /* access modifiers changed from: protected */
    public boolean isAudioSourceTypeSupported(AudioType audioType) {
        return true;
    }

    /* access modifiers changed from: protected */
    public final boolean isSourceActive() {
        if (!this.f1001b) {
            return true;
        }
        AudioSource connectedSource = this.f1000a.getConnectedSource();
        if (connectedSource == null) {
            return false;
        }
        return connectedSource.isActive();
    }

    /* access modifiers changed from: protected */
    public abstract void sourceClosed(AudioSource audioSource, AudioSink audioSink);
}
