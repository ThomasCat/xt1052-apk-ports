package h;

import android.os.Handler;
import android.util.Log;

public class c {

    /* renamed from: a reason: collision with root package name */
    private static final String f2272a = ("AMonitor." + c.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private final Handler f2273b;

    /* renamed from: c reason: collision with root package name */
    private final Runnable f2274c;

    public c(Handler handler, Runnable runnable) {
        this.f2273b = handler;
        this.f2274c = runnable;
    }

    public synchronized void a() {
        if (this.f2274c != null) {
            this.f2273b.removeCallbacks(this.f2274c);
        }
    }

    public synchronized void a(long j) {
        if (this.f2274c != null) {
            this.f2273b.removeCallbacks(this.f2274c);
        }
        if (!this.f2273b.postDelayed(this.f2274c, j)) {
            Log.w(f2272a, "Failed to start");
        }
    }
}
