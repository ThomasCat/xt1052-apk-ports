package com.nuance.dragon.toolkit.file;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.os.Environment;
import android.os.Handler;
import com.nuance.dragon.toolkit.file.FileZip.ZipEntryRO;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.a;
import com.nuance.dragon.toolkit.util.internal.d;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class FileManager {
    public static final int MODE_READ = 0;
    public static final int MODE_WRITE = 1;

    /* renamed from: a reason: collision with root package name */
    private static final String f1671a = File.separator;

    /* renamed from: b reason: collision with root package name */
    private String f1672b;

    /* renamed from: c reason: collision with root package name */
    private String f1673c;

    /* renamed from: d reason: collision with root package name */
    private String f1674d;

    /* renamed from: e reason: collision with root package name */
    private TreeSet f1675e;

    /* renamed from: f reason: collision with root package name */
    private boolean f1676f;

    /* renamed from: g reason: collision with root package name */
    private FileZip f1677g;

    /* renamed from: h reason: collision with root package name */
    private FileZip f1678h;
    private String i;
    private String j;
    private String k;
    private String l;
    private final Context m;

    public interface Listener {
        void onInitialized();
    }

    public FileManager(Context context, String str) {
        this(context, null, null, 0, false, 0, false, null, str, null);
    }

    public FileManager(Context context, String str, String str2) {
        this(context, str, str2, 0, false, 0, false, null, null, null);
    }

    public FileManager(Context context, String str, String str2, int i2, boolean z, int i3, boolean z2, String str3) {
        this(context, str, str2, i2, z, i3, z2, str3, null, null);
    }

    public FileManager(Context context, String str, String str2, int i2, boolean z, int i3, boolean z2, String str3, String str4, String str5) {
        this(context, str, str2, i2, z, i3, z2, str3, str4, str5, null);
    }

    public FileManager(Context context, String str, String str2, int i2, boolean z, int i3, boolean z2, String str3, String str4, String str5, Listener listener) {
        d.a("context", (Object) context);
        this.m = context.getApplicationContext();
        if (listener != null) {
            final Handler handler = new Handler();
            final String str6 = str;
            final String str7 = str2;
            final int i4 = i2;
            final boolean z3 = z;
            final int i5 = i3;
            final boolean z4 = z2;
            final String str8 = str3;
            final String str9 = str4;
            final String str10 = str5;
            final Listener listener2 = listener;
            new a().a((Runnable) new Runnable() {
                public final void run() {
                    FileManager.this.a(str6, str7, i4, z3, i5, z4, str8, str9, str10);
                    handler.post(new Runnable() {
                        public final void run() {
                            listener2.onInitialized();
                        }
                    });
                }
            });
            return;
        }
        a(str, str2, i2, z, i3, z2, str3, str4, str5);
    }

    public FileManager(Context context, String str, String str2, String str3) {
        this(context, str, str2, 0, false, 0, false, null, str3, null);
    }

    private static void a(File file, File file2) {
        FileOutputStream fileOutputStream;
        Logger.warn(FileManager.class, "Unpacking " + file + " to " + file2);
        ZipFile zipFile = new ZipFile(file);
        file2.mkdirs();
        try {
            Enumeration entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                File file3 = new File(file2, zipEntry.getName());
                if (zipEntry.isDirectory()) {
                    file3.mkdirs();
                } else {
                    InputStream inputStream = zipFile.getInputStream(zipEntry);
                    try {
                        fileOutputStream = new FileOutputStream(file3);
                        byte[] bArr = new byte[4096];
                        while (true) {
                            int read = inputStream.read(bArr);
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream.write(bArr, 0, read);
                        }
                        fileOutputStream.close();
                        inputStream.close();
                    } catch (Throwable th) {
                        inputStream.close();
                        throw th;
                    }
                }
            }
        } finally {
            zipFile.close();
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, int i2, boolean z, int i3, boolean z2, String str3, String str4, String str5) {
        String[] list;
        boolean z3 = true;
        d.a("assetExt, assetPath, expansionAssetPath, internalFilePath and externalFilePath", str, str2, str3, str4, str5);
        d.a("mainExpansionAssetVersion", ">= 0", i2 >= 0);
        String str6 = "patchExpansionAssetVersion";
        String str7 = ">= 0";
        if (i3 < 0) {
            z3 = false;
        }
        d.a(str6, str7, z3);
        if (str4 == null) {
            this.k = null;
        } else if (str4.length() == 0) {
            this.k = this.m.getFilesDir().toString();
        } else {
            this.k = this.m.getFilesDir() + f1671a + str4;
        }
        if (str5 == null) {
            this.l = null;
        } else if (str5.length() == 0) {
            this.l = Environment.getExternalStorageDirectory().toString();
        } else {
            this.l = str5;
        }
        if (str == null) {
            str = "";
        }
        this.f1672b = str;
        this.f1673c = str2;
        this.f1675e = new TreeSet();
        if (str2 != null) {
            try {
                for (String str8 : this.m.getAssets().list(str2)) {
                    if (str8.endsWith(this.f1672b)) {
                        this.f1675e.add(str8);
                    }
                }
            } catch (IOException e2) {
                Logger.error(this, "Error listing assets in " + str2, e2);
            }
        }
        if (str3 == null) {
            str3 = "";
        }
        this.f1674d = str3;
        this.f1676f = a(i2, z, i3, z2);
    }

    private boolean a(int i2, boolean z, int i3, boolean z2) {
        String packageName = this.m.getPackageName();
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return true;
        }
        File file = new File(Environment.getExternalStorageDirectory().getPath() + "/Android/obb/" + packageName);
        if (!file.exists()) {
            return true;
        }
        if (i2 > 0) {
            try {
                String str = "main." + i2 + "." + packageName + ".obb";
                File file2 = new File(file + f1671a + str);
                if (file2.isFile()) {
                    if (z) {
                        File file3 = new File(this.m.getExternalFilesDir(null), str);
                        if (!file3.isDirectory()) {
                            a(file2, file3);
                        }
                        this.i = new File(file3, this.f1674d).toString();
                    } else {
                        this.f1677g = new FileZip(file2.toString());
                    }
                }
            } catch (Exception e2) {
                Logger.warn(this, "Unable to parse expansion files.");
                return false;
            }
        }
        if (i3 <= 0) {
            return true;
        }
        String str2 = "patch." + i3 + "." + packageName + ".obb";
        File file4 = new File(file + f1671a + str2);
        if (!file4.isFile()) {
            return true;
        }
        if (z2) {
            File file5 = new File(this.m.getExternalFilesDir(null), str2);
            if (!file5.isDirectory()) {
                a(file4, file5);
            }
            this.j = new File(file5, this.f1674d).toString();
            return true;
        }
        this.f1678h = new FileZip(file4.toString());
        return true;
    }

    private boolean a(File file) {
        if (file == null) {
            return false;
        }
        if (file.isDirectory()) {
            return true;
        }
        File parentFile = file.getParentFile();
        if (parentFile == null) {
            return false;
        }
        if (parentFile.isDirectory() || a(parentFile)) {
            return file.mkdir();
        }
        return false;
    }

    private String[] a(FilenameFilter filenameFilter) {
        try {
            ArrayList arrayList = new ArrayList();
            if (this.f1678h != null) {
                ZipEntryRO[] a2 = this.f1678h.a(this.f1674d + f1671a);
                if (a2 != null) {
                    for (ZipEntryRO zipEntryRO : a2) {
                        if (filenameFilter.accept(null, zipEntryRO.mFileName)) {
                            arrayList.add(zipEntryRO.mFileName);
                        }
                    }
                }
            }
            if (this.f1677g != null) {
                ZipEntryRO[] a3 = this.f1677g.a(this.f1674d + f1671a);
                if (a3 != null) {
                    for (ZipEntryRO zipEntryRO2 : a3) {
                        if (filenameFilter.accept(null, zipEntryRO2.mFileName)) {
                            arrayList.add(zipEntryRO2.mFileName);
                        }
                    }
                }
            }
            if (this.f1673c != null) {
                Iterator it = this.f1675e.iterator();
                while (it.hasNext()) {
                    String str = (String) it.next();
                    String substring = str.substring(0, str.endsWith(this.f1672b) ? str.length() - this.f1672b.length() : str.length());
                    if (filenameFilter.accept(null, substring)) {
                        arrayList.add(substring);
                    }
                }
            }
            if (this.k != null) {
                String[] list = new File(this.k).list(filenameFilter);
                if (list != null) {
                    for (String add : list) {
                        arrayList.add(add);
                    }
                }
            }
            if (this.l != null) {
                String[] list2 = new File(this.l).list(filenameFilter);
                if (list2 != null) {
                    for (String add2 : list2) {
                        arrayList.add(add2);
                    }
                }
            }
            if (this.j != null) {
                String[] list3 = new File(this.j).list(filenameFilter);
                if (list3 != null) {
                    for (String add3 : list3) {
                        arrayList.add(add3);
                    }
                }
            }
            if (this.i != null) {
                String[] list4 = new File(this.i).list(filenameFilter);
                if (list4 != null) {
                    for (String add4 : list4) {
                        arrayList.add(add4);
                    }
                }
            }
            if (arrayList.size() > 0) {
                String[] strArr = new String[arrayList.size()];
                arrayList.toArray(strArr);
                return strArr;
            }
            new StringBuilder("No file matches \"").append(filenameFilter.toString()).append("\" pattern.");
            return null;
        } catch (Exception e2) {
            Logger.error(this, "Error while searching for files with a pattern.", e2);
            return null;
        }
    }

    public boolean delete(String str) {
        if (this.k != null) {
            return new File(this.k, str).delete();
        }
        Logger.warn(this, "Unable to delete file " + str);
        return false;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FileManager fileManager = (FileManager) obj;
        if (this.f1672b == null) {
            if (fileManager.f1672b != null) {
                return false;
            }
        } else if (!this.f1672b.equals(fileManager.f1672b)) {
            return false;
        }
        if (this.f1673c == null) {
            if (fileManager.f1673c != null) {
                return false;
            }
        } else if (!this.f1673c.equals(fileManager.f1673c)) {
            return false;
        }
        if (this.f1675e == null) {
            if (fileManager.f1675e != null) {
                return false;
            }
        } else if (!this.f1675e.equals(fileManager.f1675e)) {
            return false;
        }
        if (this.f1674d == null) {
            if (fileManager.f1674d != null) {
                return false;
            }
        } else if (!this.f1674d.equals(fileManager.f1674d)) {
            return false;
        }
        if (this.f1676f != fileManager.f1676f) {
            return false;
        }
        if (this.k == null) {
            if (fileManager.k != null) {
                return false;
            }
        } else if (!this.k.equals(fileManager.k)) {
            return false;
        }
        if (this.f1677g == null) {
            if (fileManager.f1677g != null) {
                return false;
            }
        } else if (!this.f1677g.equals(fileManager.f1677g)) {
            return false;
        }
        if (this.i == null) {
            if (fileManager.i != null) {
                return false;
            }
        } else if (!this.i.equals(fileManager.i)) {
            return false;
        }
        if (this.f1678h == null) {
            if (fileManager.f1678h != null) {
                return false;
            }
        } else if (!this.f1678h.equals(fileManager.f1678h)) {
            return false;
        }
        return this.j == null ? fileManager.j == null : this.j.equals(fileManager.j);
    }

    public boolean exists(String str) {
        if (this.f1678h != null) {
            for (ZipEntryRO zipEntryRO : this.f1678h.a(this.f1674d + f1671a)) {
                if (zipEntryRO.mFileName.equals(str)) {
                    return true;
                }
            }
        }
        if (this.f1677g != null) {
            for (ZipEntryRO zipEntryRO2 : this.f1677g.a(this.f1674d + f1671a)) {
                if (zipEntryRO2.mFileName.equals(str)) {
                    return true;
                }
            }
        }
        if (this.f1673c != null && this.f1675e.contains(str + this.f1672b)) {
            return true;
        }
        if (this.k != null && new File(this.k, str).exists()) {
            return true;
        }
        if (this.l != null && new File(this.l, str).exists()) {
            return true;
        }
        if (this.j == null || !new File(this.j, str).exists()) {
            return this.i != null && new File(this.i, str).exists();
        }
        return true;
    }

    public String[] findFiles(final String str) {
        return a((FilenameFilter) new FilenameFilter() {
            public final boolean accept(File file, String str) {
                return str.contains(str);
            }

            public final String toString() {
                return str;
            }
        });
    }

    public String[] findFiles(final Pattern pattern) {
        return a((FilenameFilter) new FilenameFilter() {
            public final boolean accept(File file, String str) {
                return pattern.matcher(str).matches();
            }

            public final String toString() {
                return pattern.pattern();
            }
        });
    }

    /* access modifiers changed from: protected */
    public AssetFileDescriptor getAssetFile(String str, String str2) {
        if (this.f1678h != null) {
            AssetFileDescriptor assetFileDescriptor = this.f1678h.getAssetFileDescriptor(this.f1674d + f1671a + str.replace(this.f1674d + f1671a, ""));
            if (assetFileDescriptor != null) {
                new StringBuilder("Found asset file (in patch expansion file) ").append(str);
                return assetFileDescriptor;
            }
        }
        if (this.f1677g != null) {
            AssetFileDescriptor assetFileDescriptor2 = this.f1677g.getAssetFileDescriptor(this.f1674d + f1671a + str.replace(this.f1674d + f1671a, ""));
            if (assetFileDescriptor2 != null) {
                new StringBuilder("Found asset file (in main expansion file) ").append(str);
                return assetFileDescriptor2;
            }
        }
        if (this.f1673c != null) {
            String str3 = str + str2;
            if (this.f1675e.contains(str3)) {
                try {
                    AssetFileDescriptor openFd = this.m.getAssets().openFd(this.f1673c.length() == 0 ? str3 : this.f1673c + f1671a + str3);
                    if (openFd != null) {
                        new StringBuilder("Found asset file ").append(str3);
                        return openFd;
                    }
                    Logger.error(this, "Unexpected error opening asset file " + str3);
                } catch (IOException e2) {
                    Logger.error(this, "Exception opening asset file " + str3, e2);
                }
            } else {
                new StringBuilder("Asset file ").append(str).append(" not found");
            }
        }
        return null;
    }

    public boolean getExpansionValidity() {
        return this.f1676f;
    }

    /* access modifiers changed from: 0000 */
    public FileJni getFileJni(String str, int i2) {
        if (i2 == 0) {
            File realFile = getRealFile(this.l, str, 0);
            if (realFile != null) {
                return new FileJni(realFile);
            }
            File realFile2 = getRealFile(this.j, str, 0);
            if (realFile2 != null) {
                return new FileJni(realFile2);
            }
            File realFile3 = getRealFile(this.i, str, 0);
            if (realFile3 != null) {
                return new FileJni(realFile3);
            }
        }
        File realFile4 = getRealFile(this.k, str, i2);
        if (realFile4 != null) {
            return new FileJni(realFile4);
        }
        AssetFileDescriptor assetFileDescriptor = i2 == 0 ? getAssetFile(str, this.f1672b) : null;
        if (assetFileDescriptor != null) {
            return new FileJni(assetFileDescriptor);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public File getRealFile(String str, String str2, int i2) {
        if (str == null) {
            return null;
        }
        File file = new File(str, str2);
        if (!file.exists()) {
            if (i2 == 1) {
                try {
                    if (!a(new File(str))) {
                        Logger.warn(this, "Target folder: " + str + " is not a directory.  Cannot create " + str2);
                        return null;
                    }
                    file.createNewFile();
                } catch (IOException e2) {
                    Logger.error(this, "File " + str + f1671a + str2 + " could not be created: " + e2);
                    return null;
                }
            } else {
                new StringBuilder("File ").append(str2).append(" not found");
                return null;
            }
        }
        new StringBuilder("Found file ").append(str).append(f1671a).append(str2);
        return file;
    }

    public int getSize(String str) {
        if (this.l != null) {
            File file = new File(this.l, str);
            if (file.exists()) {
                return (int) file.length();
            }
        }
        if (this.j != null) {
            File file2 = new File(this.j, str);
            if (file2.exists()) {
                return (int) file2.length();
            }
        }
        if (this.i != null) {
            File file3 = new File(this.i, str);
            if (file3.exists()) {
                return (int) file3.length();
            }
        }
        if (this.k != null) {
            File file4 = new File(this.k, str);
            if (file4.exists()) {
                return (int) file4.length();
            }
        }
        AssetFileDescriptor assetFile = getAssetFile(str, this.f1672b);
        if (assetFile == null) {
            return -1;
        }
        FileJni fileJni = new FileJni(assetFile);
        int length = fileJni.getLength();
        fileJni.close();
        return length;
    }

    public final int hashCode() {
        int i2 = 0;
        int hashCode = ((this.f1678h == null ? 0 : this.f1678h.hashCode()) + (((this.i == null ? 0 : this.i.hashCode()) + (((this.f1677g == null ? 0 : this.f1677g.hashCode()) + (((this.k == null ? 0 : this.k.hashCode()) + (((this.l == null ? 0 : this.l.hashCode()) + (((this.f1676f ? 1231 : 1237) + (((this.f1674d == null ? 0 : this.f1674d.hashCode()) + (((this.f1675e == null ? 0 : this.f1675e.hashCode()) + (((this.f1673c == null ? 0 : this.f1673c.hashCode()) + (((this.f1672b == null ? 0 : this.f1672b.hashCode()) + 31) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.j != null) {
            i2 = this.j.hashCode();
        }
        return hashCode + i2;
    }

    public FileInputStream openFileForReading(String str) {
        File file;
        File file2;
        d.a("fileName", (Object) str);
        if (this.l != null) {
            file = new File(this.l, str);
            if (!file.exists()) {
                file = null;
            }
        } else {
            file = null;
        }
        if (this.j != null && file == null) {
            file = new File(this.j, str);
            if (!file.exists()) {
                file = null;
            }
        }
        if (this.i != null && file == null) {
            file = new File(this.i, str);
            if (!file.exists()) {
                file = null;
            }
        }
        if (this.k == null || file != null) {
            file2 = file;
        } else {
            File file3 = new File(this.k, str);
            file2 = !file3.exists() ? null : file3;
        }
        AssetFileDescriptor assetFileDescriptor = file2 == null ? getAssetFile(str, this.f1672b) : null;
        if (file2 == null && assetFileDescriptor == null) {
            return null;
        }
        if (file2 == null) {
            return assetFileDescriptor.createInputStream();
        }
        try {
            return new FileInputStream(file2);
        } catch (Exception e2) {
            Logger.error(this, "Unable to open input stream for file " + str);
            return null;
        }
    }

    public FileOutputStream openFileForWriting(String str) {
        d.a((Object) this, this.k != null, "File manager isn't configured with an internal file path");
        d.a("fileName", (Object) str);
        File file = new File(this.k, str);
        if (a(file.getParentFile())) {
            try {
                return new FileOutputStream(file);
            } catch (Exception e2) {
                Logger.error(this, "Unable to open output stream for file " + str);
            }
        }
        return null;
    }

    public boolean rename(String str, String str2) {
        if (this.k != null) {
            return new File(this.k, str).renameTo(new File(this.k, str2));
        }
        Logger.warn(this, "Unable to rename file " + str + " to " + str2);
        return false;
    }
}
