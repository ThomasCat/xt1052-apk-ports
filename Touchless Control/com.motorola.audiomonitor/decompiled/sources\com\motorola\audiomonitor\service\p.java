package com.motorola.audiomonitor.service;

import android.os.Message;

abstract class p {

    /* renamed from: a reason: collision with root package name */
    private final String f586a;

    /* renamed from: b reason: collision with root package name */
    final /* synthetic */ d f587b;

    /* renamed from: c reason: collision with root package name */
    private final String f588c;

    public p(d dVar, String str, String str2) {
        this.f587b = dVar;
        this.f586a = str;
        this.f588c = str2;
    }

    public String a() {
        return this.f586a;
    }

    public abstract void a(Message message);

    public String b() {
        return this.f588c;
    }
}
