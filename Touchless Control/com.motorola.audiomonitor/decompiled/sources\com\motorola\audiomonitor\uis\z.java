package com.motorola.audiomonitor.uis;

import a.e;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;
import com.motorola.audiomonitor.C0014R;
import h.k;
import h.m;

public class z extends HandlerThread {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public boolean f764a = false;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Context f765b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final Handler f766c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public final WindowManager f767d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public final View f768e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public final TextView f769f;

    /* renamed from: g reason: collision with root package name */
    private final TextView f770g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public ag f771h;
    /* access modifiers changed from: private */
    public final LayoutParams i = new LayoutParams(-2, -2, 2010, 8, 1);
    private ah j = ah.NO_OPTION;

    public z(Context context) {
        super(z.class.getSimpleName());
        this.f765b = context;
        this.f767d = (WindowManager) this.f765b.getSystemService("window");
        this.f768e = ((LayoutInflater) this.f765b.getSystemService("layout_inflater")).inflate(C0014R.layout.notifdialog, null);
        this.f769f = (TextView) this.f768e.findViewById(C0014R.id.message);
        this.f770g = (TextView) this.f768e.findViewById(C0014R.id.option_prompt);
        start();
        this.f766c = new Handler(getLooper());
    }

    public void a() {
        this.f766c.post(new ae(this));
    }

    public void a(ah ahVar, ag agVar) {
        this.j = ahVar;
        if (ahVar != ah.CANCEL_OPTION || agVar == null) {
            this.f770g.setVisibility(8);
            this.f771h = null;
            return;
        }
        this.f771h = agVar;
        this.f770g.setVisibility(0);
        this.f770g.setText(k.a(this.f765b, (int) C0014R.string.cancel_to_stop, new String[]{this.f765b.getResources().getString(C0014R.string.highlight_cancel)}, (int) C0014R.color.Active_Button, m.Normal));
        this.f770g.setOnClickListener(new ad(this));
    }

    public void a(String str, e eVar, int i2, int i3) {
        if (i2 == 0) {
            a(str, eVar, 48, 0, 80, i3);
        } else if (i2 == 1) {
            a(str, eVar, 80, 0, 0, i3);
        }
    }

    public void a(String str, e eVar, int i2, int i3, int i4, int i5) {
        this.f766c.post(new aa(this, i2, i3, i4, str, i5));
        if (this.j == ah.CANCEL_OPTION) {
            new ac(this, eVar).start();
        }
    }

    public void b() {
        this.f766c.removeCallbacksAndMessages(null);
        a();
        this.f766c.post(new af(this));
        try {
            join();
        } catch (InterruptedException e2) {
        }
    }
}
