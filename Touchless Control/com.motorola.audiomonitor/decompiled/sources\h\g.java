package h;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Process;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import com.motorola.audiomonitor.a;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class g {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f2293a = ("AMonitor." + g.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f2294b = a.f160b;
    private static g k = null;
    private static String[] m = {"UNKNOWN", "GPRS", "EDGE", "UMTS", "CDMA", "DO", "DO_A", "1X", "HSDPA", "HSUPA", "HSPA", "IDEN", "DO_B", "LTE", "EHRPD", "HSPAP"};

    /* renamed from: c reason: collision with root package name */
    private Context f2295c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public final LinkedList f2296d = new LinkedList();

    /* renamed from: e reason: collision with root package name */
    private long f2297e = 0;

    /* renamed from: f reason: collision with root package name */
    private JSONObject f2298f;

    /* renamed from: g reason: collision with root package name */
    private JSONObject f2299g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public final Object f2300h = new Object();
    private boolean i = false;
    private boolean j = false;
    /* access modifiers changed from: private */
    public int l = 0;
    private final BroadcastReceiver n = new h(this);
    private final Thread o = new i(this);

    private g() {
        if (!a.a()) {
            this.o.start();
        }
    }

    public static g a() {
        if (k == null) {
            k = new g();
        }
        return k;
    }

    /* access modifiers changed from: private */
    public void a(InputStream inputStream) {
    }

    /* access modifiers changed from: private */
    public void a(OutputStream outputStream, List list) {
        outputStream.write("[".getBytes());
        JSONObject jSONObject = null;
        Iterator it = list.iterator();
        while (true) {
            JSONObject jSONObject2 = jSONObject;
            if (it.hasNext()) {
                jSONObject = (JSONObject) it.next();
                if (jSONObject2 != null) {
                    outputStream.write(",".getBytes());
                }
                outputStream.write(jSONObject.toString().getBytes());
            } else {
                outputStream.write("]".getBytes());
                outputStream.flush();
                return;
            }
        }
    }

    private void a(String str, int i2, float[] fArr, String[] strArr) {
        JSONArray jSONArray;
        if (this.f2295c != null && this.f2299g != null && i2 > 0) {
            try {
                jSONArray = this.f2299g.getJSONArray(str);
                jSONArray.put("========");
            } catch (JSONException e2) {
                jSONArray = new JSONArray();
            }
            int i3 = 0;
            while (i3 < i2) {
                try {
                    jSONArray.put(new String(fArr[i3] + " : " + strArr[i3]));
                    i3++;
                } catch (JSONException e3) {
                    Log.e("TAG", "Failed to set parameter for " + str);
                    return;
                }
            }
            this.f2299g.put(str, jSONArray);
        }
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.motorola.audiomonitor.actions.utils.a>, for r7v0, types: [java.util.List<com.motorola.audiomonitor.actions.utils.a>, java.util.List] */
    private void a(String str, String str2, List<com.motorola.audiomonitor.actions.utils.a> list) {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("[");
                stringBuffer.append(str2);
                stringBuffer.append("]");
                for (com.motorola.audiomonitor.actions.utils.a aVar : list) {
                    stringBuffer.append(aVar.c());
                    stringBuffer.append("(");
                    stringBuffer.append(aVar.e());
                    stringBuffer.append(")");
                    stringBuffer.append(aVar.d());
                    stringBuffer.append(":");
                    stringBuffer.append(aVar.i());
                    stringBuffer.append(":");
                    stringBuffer.append(aVar.h());
                    stringBuffer.append(";");
                }
                this.f2299g.put(str, stringBuffer.toString());
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in recordContactMatch");
            }
        }
    }

    private String d(String str) {
        if ("wifi".equalsIgnoreCase(str)) {
            WifiManager wifiManager = (WifiManager) this.f2295c.getSystemService("wifi");
            if (wifiManager != null) {
                return wifiManager.getConnectionInfo().getSSID();
            }
        } else if ("mobile".equalsIgnoreCase(str)) {
            TelephonyManager telephonyManager = (TelephonyManager) this.f2295c.getSystemService("phone");
            if (telephonyManager != null) {
                int networkType = telephonyManager.getNetworkType();
                if (networkType >= 0 && networkType < m.length) {
                    return m[networkType];
                }
            }
        }
        return null;
    }

    private void u() {
        synchronized (this.f2300h) {
            this.f2300h.notify();
        }
    }

    /* access modifiers changed from: private */
    public boolean v() {
        return !this.j && !a.a();
    }

    public void a(float f2) {
        if (this.f2295c != null) {
            double d2 = (double) f2;
            this.f2298f = new JSONObject();
            this.f2299g = new JSONObject();
            try {
                this.f2298f.put("phraseSpot", this.f2299g);
                this.f2299g.put("spotTime", System.currentTimeMillis());
                this.f2299g.put("spotScore", d2);
                this.f2299g.put("startId", this.f2297e);
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in recordPhrase");
            }
        }
    }

    public void a(int i2) {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                this.f2299g.put("noise", i2);
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in recordNoiseEstimate");
            }
        }
    }

    public void a(int i2, float[] fArr, String[] strArr) {
        a("localCommandMatch", i2, fArr, strArr);
    }

    public void a(Context context) {
        if (!a.a() && this.f2295c == null) {
            this.f2295c = context;
        }
    }

    public void a(String str) {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                this.f2299g.put("localAction", str);
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in recordLocalAction");
            }
        }
    }

    public void a(String str, String str2) {
        if (this.f2295c != null && this.f2298f != null) {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.f2295c.getSystemService("connectivity")).getActiveNetworkInfo();
            try {
                if (!TextUtils.isEmpty(str)) {
                    this.f2299g.put("triggerWavName", str);
                }
                if (!TextUtils.isEmpty(str2)) {
                    this.f2299g.put("commandWavName", str2);
                }
                if (activeNetworkInfo != null) {
                    String typeName = activeNetworkInfo.getTypeName();
                    StringBuilder sb = new StringBuilder(typeName);
                    String d2 = d(typeName);
                    if (d2 != null) {
                        sb.append("(");
                        sb.append(d2);
                        sb.append(")");
                    }
                    this.f2299g.put("network", sb.toString());
                    this.f2299g.put("connected", activeNetworkInfo.isConnected());
                } else {
                    this.f2299g.put("network", "NONE");
                    this.f2299g.put("connected", false);
                }
                this.f2299g.put("inet", this.l);
                this.f2299g.put("finishedTime", System.currentTimeMillis());
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter for NetworkCondition");
            }
            synchronized (this.f2296d) {
                this.f2296d.add(this.f2298f);
            }
            this.f2298f = null;
        }
    }

    public void a(String str, List list) {
        a("contacts-filtered", str, list);
    }

    public void a(List list) {
        a("contacts-vip", "vip", list);
    }

    public void a(boolean z) {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                this.f2299g.put("confirm", z);
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in recordConfirm");
            }
        }
    }

    public void a(boolean z, int i2) {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                this.f2299g.put("triggerCheck", z);
                this.f2299g.put("triggerConfidence", i2);
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in recordTriggerCheck");
            }
        }
    }

    public void a(int[] iArr) {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                StringBuilder sb = new StringBuilder();
                for (int append : iArr) {
                    sb.append(append);
                    sb.append(",");
                }
                this.f2299g.put("noisemodes", sb.toString());
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in recordNoiseModeTimes");
            }
        }
    }

    public void b() {
        if (this.f2295c != null && !this.i) {
            this.f2295c.registerReceiver(this.n, new IntentFilter("android.net.conn.INET_CONDITION_ACTION"));
            this.i = true;
        }
    }

    public void b(String str) {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                this.f2299g.put("nuance", str);
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in recordNuanceResult");
            }
        }
    }

    public void b(boolean z) {
        this.j = z;
    }

    public void c() {
        if (this.f2295c != null) {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject.put("device", jSONObject2);
                jSONObject2.put("serial", Build.SERIAL);
                jSONObject2.put("model", Build.MODEL);
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in recordDevice");
            }
            synchronized (this.f2296d) {
                this.f2296d.add(jSONObject);
            }
        }
    }

    public void c(String str) {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                this.f2299g.put("dspversion", str);
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in recordDspVersion");
            }
        }
    }

    public void d() {
        if (this.f2295c != null) {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject.put("start", jSONObject2);
                long currentTimeMillis = System.currentTimeMillis();
                this.f2297e = currentTimeMillis;
                jSONObject2.put("startId", currentTimeMillis);
                jSONObject2.put("time", currentTimeMillis);
                Account[] accountsByType = AccountManager.get(this.f2295c).getAccountsByType("com.google");
                if (accountsByType != null) {
                    JSONArray jSONArray = new JSONArray();
                    for (Account account : accountsByType) {
                        jSONArray.put(account.name);
                    }
                    jSONObject2.put("emails", jSONArray);
                }
                TelephonyManager telephonyManager = (TelephonyManager) this.f2295c.getSystemService("phone");
                if (telephonyManager != null) {
                    String line1Number = telephonyManager.getLine1Number();
                    if (line1Number != null) {
                        jSONObject2.put("telno", line1Number);
                    }
                }
                jSONObject2.put("pid", Process.myPid());
                jSONObject2.put("appver", a.p);
                jSONObject2.put("keyphrase", PreferenceManager.getDefaultSharedPreferences(this.f2295c).getString("key_phrase", "na"));
                jSONObject2.put("language", a.n);
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in recordStart");
            }
            synchronized (this.f2296d) {
                this.f2296d.add(jSONObject);
            }
        }
    }

    public void e() {
        if (this.f2295c != null) {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject.put("stop", jSONObject2);
                jSONObject2.put("startId", this.f2297e);
                jSONObject2.put("time", System.currentTimeMillis());
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in recordStop");
            }
            synchronized (this.f2296d) {
                this.f2296d.add(jSONObject);
            }
            u();
        }
    }

    public void f() {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                this.f2299g.put("beginningOfSpeech", System.currentTimeMillis());
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter: beginningOfSpeech");
            }
        }
    }

    public void g() {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                this.f2299g.put("endOfSpeech", System.currentTimeMillis());
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter: endOfSpeech");
            }
        }
    }

    public void h() {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                this.f2299g.put("securedLockGone", System.currentTimeMillis());
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter: securedLockUnblocked");
            }
        }
    }

    public void i() {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                this.f2299g.put("securedLockTimedOut", System.currentTimeMillis());
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter: securedLockTimedOut");
            }
        }
    }

    public void j() {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                this.f2299g.put("lockScreen", true);
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter: lockScreen");
            }
        }
    }

    public void k() {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                this.f2299g.put("localActionTime", System.currentTimeMillis());
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in recordLocalActionTime");
            }
        }
    }

    public String l() {
        String str = null;
        if (this.f2295c == null || this.f2299g == null) {
            return str;
        }
        try {
            return this.f2299g.getString("localAction");
        } catch (JSONException e2) {
            Log.e("TAG", "Failed to get parameter in getLocalCommand");
            return str;
        }
    }

    public void m() {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                this.f2299g.put("actionStartTime", System.currentTimeMillis());
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in actionStart");
            }
        }
    }

    public void n() {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                this.f2299g.put("googleNowTime", System.currentTimeMillis());
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in recordGoogleNow");
            }
        }
    }

    public void o() {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                this.f2299g.put("promptTime", System.currentTimeMillis());
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in recordPrompt");
            }
        }
    }

    public void p() {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                this.f2299g.put("bttrigger", true);
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in recordBtTrigger");
            }
        }
    }

    public void q() {
        if (this.f2295c != null && this.f2299g != null) {
            try {
                this.f2299g.put("lockscreenbypass", true);
            } catch (JSONException e2) {
                Log.e("TAG", "Failed to set parameter in recordLockScreenBypass");
            }
        }
    }

    public void r() {
        if (this.i) {
            this.f2295c.unregisterReceiver(this.n);
            this.i = false;
        }
    }
}
