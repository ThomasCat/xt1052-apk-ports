package com.motorola.audiomonitor.actions.utils;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.motorola.audiomonitor.a;
import com.nuance.dragon.toolkit.recognition.CloudInterpreter;
import java.util.ArrayList;
import java.util.List;

public class g {

    /* renamed from: a reason: collision with root package name */
    private static final String f231a = ("AMonitor." + g.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f232b = a.f160b;

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0151, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0155, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x015a, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x015b, code lost:
        r2 = r1;
        r3 = r9;
        r1 = null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:7:0x0088, B:31:0x0101] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0151  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0155 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:31:0x0101] */
    private static a a(Context context, b bVar, int i) {
        Cursor cursor;
        Cursor cursor2;
        Object obj;
        a aVar;
        a aVar2;
        a aVar3;
        Uri parse = Uri.parse("content://com.motorola.context.provider/context_likely_number");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("type=1");
        stringBuffer.append(" AND ");
        stringBuffer.append("name='" + bVar.b() + "'");
        if (i >= 0) {
            stringBuffer.append(" AND ");
            stringBuffer.append("address_type=" + i);
        }
        StringBuffer stringBuffer2 = new StringBuffer();
        stringBuffer2.append("probability");
        stringBuffer2.append(",");
        stringBuffer2.append("confidence");
        stringBuffer2.append(" ");
        stringBuffer2.append("DESC");
        try {
            cursor = context.getContentResolver().query(parse, null, stringBuffer.toString(), null, stringBuffer2.toString());
            if (cursor == null) {
                if (f232b) {
                    Log.d(f231a, "cursor is null");
                }
                aVar = null;
                if (cursor != null) {
                    cursor.close();
                }
                return aVar;
            }
            if (f232b) {
                Log.d(f231a, "retrieveNumber where(" + stringBuffer + ") sort(" + stringBuffer2 + ")");
            }
            int columnIndexOrThrow = cursor.getColumnIndexOrThrow("address");
            int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("address_type");
            int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("probability");
            int columnIndexOrThrow4 = cursor.getColumnIndexOrThrow("confidence");
            if (cursor.getCount() <= 0 || !cursor.moveToFirst()) {
                aVar = null;
            } else {
                a aVar4 = null;
                while (true) {
                    try {
                        String string = cursor.getString(columnIndexOrThrow);
                        if (!TextUtils.isEmpty(string)) {
                            b bVar2 = bVar;
                            aVar = new a(bVar2, bVar.b(), cursor.getInt(columnIndexOrThrow2), string, false, cursor.getFloat(columnIndexOrThrow3), cursor.getFloat(columnIndexOrThrow4));
                            if (aVar != null) {
                                break;
                            }
                            aVar3 = aVar;
                        } else if (f232b) {
                            Log.d(f231a, "returned null number");
                            aVar3 = aVar4;
                        } else {
                            aVar3 = aVar4;
                        }
                        try {
                            if (!cursor.moveToNext()) {
                                aVar = aVar3;
                                break;
                            }
                            aVar4 = aVar3;
                        } catch (SecurityException e2) {
                            cursor2 = cursor;
                            Object obj2 = e2;
                            aVar2 = aVar3;
                            obj = obj2;
                        } catch (Throwable th) {
                        }
                    } catch (SecurityException e3) {
                        obj = e3;
                        cursor2 = cursor;
                        aVar2 = aVar4;
                    } catch (Throwable th2) {
                    }
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            return aVar;
        } catch (SecurityException e4) {
            obj = e4;
            cursor2 = null;
            aVar2 = null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
        try {
            Log.e(f231a, "context engine exception: " + obj);
            if (cursor2 != null) {
                cursor2.close();
            }
            return aVar;
        } catch (Throwable th4) {
            th = th4;
            cursor = cursor2;
            if (cursor != null) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.motorola.audiomonitor.actions.utils.b>, for r13v0, types: [java.util.List, java.util.List<com.motorola.audiomonitor.actions.utils.b>] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0119  */
    public static List a(Context context, List<b> list) {
        Cursor cursor;
        Cursor cursor2 = null;
        ArrayList<b> arrayList = new ArrayList<>();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList<a> arrayList4 = new ArrayList<>();
        String str = "type=1";
        try {
            cursor = context.getContentResolver().query(Uri.parse("content://com.motorola.context.provider/context_list_vip"), null, str, null, "score DESC");
            if (cursor == null) {
                try {
                    if (f232b) {
                        Log.d(f231a, "cursor is null");
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                } catch (SecurityException e2) {
                    e = e2;
                    cursor2 = cursor;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            } else {
                if (f232b) {
                    Log.d(f231a, "getVIPList where(" + str + ")");
                }
                int columnIndexOrThrow = cursor.getColumnIndexOrThrow("lookup_key");
                int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow(CloudInterpreter.KEY_SCORE);
                if (cursor.getCount() <= 0 || !cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    for (b bVar : arrayList) {
                        a a2 = a(context, bVar, bVar.d());
                        if (a2 != null) {
                            arrayList4.add(a2);
                            if (a(a2)) {
                                arrayList2.add(a2);
                            } else if (b(a2)) {
                                arrayList3.add(a2);
                            }
                        }
                    }
                    if (arrayList2.size() > 0 && arrayList3.size() > 0) {
                        arrayList2.addAll(arrayList3);
                    }
                    if (arrayList4.size() > 0) {
                        if (f232b) {
                            for (a aVar : arrayList4) {
                                Log.d(f231a, "vip=" + aVar + " : " + aVar.d() + " " + aVar.h() + " " + aVar.i());
                            }
                        }
                        h.g.a().a((List) arrayList4);
                    }
                    return arrayList2;
                }
                do {
                    String string = cursor.getString(columnIndexOrThrow);
                    float f2 = cursor.getFloat(columnIndexOrThrow2);
                    for (b bVar2 : list) {
                        if (string.equals(bVar2.a())) {
                            bVar2.a(f2);
                            arrayList.add(bVar2);
                        }
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                }
                for (b bVar3 : arrayList) {
                }
                arrayList2.addAll(arrayList3);
                if (arrayList4.size() > 0) {
                }
                return arrayList2;
            }
        } catch (SecurityException e3) {
            e = e3;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
        try {
            Log.e(f231a, "context engine exception: " + e);
            if (cursor2 != null) {
                cursor2.close();
            }
            for (b bVar32 : arrayList) {
            }
            arrayList2.addAll(arrayList3);
            if (arrayList4.size() > 0) {
            }
            return arrayList2;
        } catch (Throwable th3) {
            th = th3;
            cursor = cursor2;
            if (cursor != null) {
            }
            throw th;
        }
    }

    public static void a(Context context, boolean z) {
        if (f232b) {
            Log.d(f231a, "sendSubscribeIntent: " + context.getPackageName() + (z ? " subscribe" : " unsubscribe"));
        }
        Intent intent = new Intent("com.motorola.context.engine.REQUEST");
        intent.putExtra("com.motorola.context.engine.intent.extra.EVENT", z ? "subscribe" : "unsubscribe");
        intent.putExtra("com.motorola.context.engine.intent.extra.REQUEST_ID", "123com.motorola.context.engine.test");
        intent.putExtra("com.motorola.context.engine.intent.extra.PUBLISHER_KEY", "com.motorola.context.publisher.vip");
        intent.putExtra("com.motorola.context.engine.intent.extra.CONSUMER", "com.motorola.context.TEST_RESPONSE");
        intent.putExtra("com.motorola.context.engine.intent.extra.CONSUMER_PACKAGE", context.getPackageName());
        context.getApplicationContext().sendBroadcast(intent);
    }

    public static boolean a(a aVar) {
        return aVar.h() > 60.0f && aVar.i() > 60.0f;
    }

    private static boolean b(a aVar) {
        return aVar.d() >= 25.0f && aVar.h() > 0.0f && aVar.i() > 0.0f;
    }
}
