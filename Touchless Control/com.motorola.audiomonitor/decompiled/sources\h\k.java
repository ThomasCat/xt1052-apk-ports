package h;

import android.annotation.SuppressLint;
import android.app.Notification.BigTextStyle;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.uis.training.TrainingTriggerRecordingActivity;
import f.v;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class k {

    /* renamed from: a reason: collision with root package name */
    private static final String f2307a = ("AMonitor." + k.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f2308b = a.f160b;

    public static SpannableStringBuilder a(Context context, int i, int[] iArr, int i2) {
        String string = context.getResources().getString(i);
        String[] strArr = new String[iArr.length];
        for (int i3 = 0; i3 < iArr.length; i3++) {
            strArr[i3] = context.getResources().getString(iArr[i3]);
        }
        return a(context, string, strArr, i2, m.Bold);
    }

    public static SpannableStringBuilder a(Context context, int i, String[] strArr, int i2, m mVar) {
        return a(context, context.getResources().getString(i), strArr, i2, mVar);
    }

    public static SpannableStringBuilder a(Context context, String str, String[] strArr, int i, m mVar) {
        StringBuilder sb = new StringBuilder();
        int parseColor = Color.parseColor(context.getResources().getString(i));
        String str2 = "";
        for (String append : strArr) {
            sb.append(str2);
            sb.append(append);
            str2 = "|";
        }
        Matcher matcher = Pattern.compile(sb.toString()).matcher(str);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
        while (matcher.find()) {
            switch (l.f2309a[mVar.ordinal()]) {
                case 1:
                    spannableStringBuilder.setSpan(Typeface.create("sans-serif", 0), matcher.start(), matcher.end(), 33);
                    break;
                case 2:
                    spannableStringBuilder.setSpan(new StyleSpan(1), matcher.start(), matcher.end(), 33);
                    break;
            }
            spannableStringBuilder.setSpan(new ForegroundColorSpan(parseColor), matcher.start(), matcher.end(), 33);
        }
        return spannableStringBuilder;
    }

    public static String a() {
        File file = new File("/data/aov-data/");
        return (!file.exists() || !file.isDirectory()) ? "/system/vendor/moto/audiomonitor/" : "/data/aov-data/";
    }

    public static String a(String str, n nVar, boolean z) {
        if (str != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            if (nVar == n.ACOUSTIC) {
                sb.append("_acoustic");
            } else {
                sb.append("_search");
            }
            if (z) {
                sb.append("_sv");
            }
            sb.append("_");
            sb.append("22132");
            sb.append("_");
            sb.append("ma36BP");
            sb.append(".bin");
            return sb.toString();
        }
        throw new IllegalArgumentException("invalid phrase");
    }

    public static String a(String str, String str2) {
        String str3 = new String(str2);
        try {
            Class cls = Class.forName("android.os.SystemProperties");
            return (String) cls.getDeclaredMethod("get", new Class[]{String.class, String.class}).invoke(cls, new Object[]{str, str2});
        } catch (IllegalArgumentException e2) {
            throw e2;
        } catch (Exception e3) {
            Log.e(f2307a, "Failed to get system property, " + str);
            return str3;
        }
    }

    public static ByteBuffer a(ByteBuffer byteBuffer) {
        ByteBuffer allocate = ByteBuffer.allocate(byteBuffer.capacity());
        byteBuffer.rewind();
        allocate.put(byteBuffer);
        byteBuffer.rewind();
        allocate.flip();
        return allocate;
    }

    @SuppressLint({"Wakelock"})
    public static void a(Context context) {
        ((PowerManager) context.getSystemService("power")).newWakeLock(805306394, "AudioMonitorLock").acquire(10000);
    }

    public static void a(Context context, int i, int i2, int i3, int i4) {
        Intent intent = new Intent("com.motorola.audiomonitor.NOISE_ESTIMATE");
        intent.putExtra("NOISE_ESTIMATE", i);
        intent.putExtra("NOISE_DB", i2);
        intent.putExtra("NOISE_TYPE", i3);
        intent.putExtra("NOISE_SNR", i4);
        if (f2308b) {
            Log.d(f2307a, "Sending Broadcast " + intent.toUri(0));
        }
        context.sendStickyBroadcast(intent);
    }

    public static void a(Context context, String str, long j) {
        Editor edit = PreferenceManager.getDefaultSharedPreferences(context).edit();
        edit.putLong(str, j);
        edit.apply();
    }

    public static void a(Context context, String str, boolean z) {
        Editor edit = PreferenceManager.getDefaultSharedPreferences(context).edit();
        edit.putBoolean(str, z);
        edit.apply();
    }

    public static void a(boolean z, Context context, Class cls) {
        context.getPackageManager().setComponentEnabledSetting(new ComponentName(context, cls), z ? 1 : 2, 1);
    }

    public static boolean a(Context context, int i, String str) {
        try {
            InputStream openRawResource = context.getResources().openRawResource(i);
            byte[] bArr = new byte[openRawResource.available()];
            openRawResource.read(bArr);
            openRawResource.close();
            boolean a2 = a(context, str, bArr);
            if (a2) {
                return a2;
            }
            Log.e(f2307a, "copyData failed: file=" + str);
            return a2;
        } catch (Exception e2) {
            Log.e(f2307a, "Failed to open resource");
            return false;
        }
    }

    public static boolean a(Context context, SharedPreferences sharedPreferences) {
        boolean a2 = a(context, sharedPreferences.getString("key_phrase", "customphrase"));
        if (!a2) {
            Log.e(f2307a, "DSP models not found");
        }
        return a2;
    }

    public static boolean a(Context context, String str) {
        String d2 = d(str);
        if (d2 == null) {
            Log.i(f2307a, "Search file not valid for phrase, " + str);
            return false;
        } else if (!new File(context.getFilesDir().getAbsolutePath() + "/" + d2).exists()) {
            Log.i(f2307a, "Search file not found for phrase, " + str);
            return false;
        } else {
            String e2 = e(str);
            if (e2 == null) {
                Log.i(f2307a, "Grammar file not valid for phrase, " + str);
                return false;
            } else if (!new File(context.getFilesDir().getAbsolutePath() + "/" + e2).exists()) {
                Log.i(f2307a, "Grammar file not found for phrase, " + str);
                return false;
            } else {
                SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                Editor edit = defaultSharedPreferences.edit();
                edit.putString("acousticmodel", "");
                edit.putString("languagemodel", "");
                edit.apply();
                Editor edit2 = defaultSharedPreferences.edit();
                edit2.putString("acousticmodel", context.getFilesDir().getAbsolutePath() + "/" + d2);
                edit2.putString("languagemodel", context.getFilesDir().getAbsolutePath() + "/" + e2);
                edit2.apply();
                if (f2308b) {
                    Log.d(f2307a, "searchFile=" + context.getFilesDir().getAbsolutePath() + "/" + d2);
                    Log.d(f2307a, "grammarFile=" + context.getFilesDir().getAbsolutePath() + "/" + e2);
                }
                return true;
            }
        }
    }

    public static boolean a(Context context, String str, byte[] bArr) {
        FileOutputStream openFileOutput;
        boolean z = false;
        try {
            File file = new File(str);
            z = file.isAbsolute();
            openFileOutput.write(bArr);
            if (openFileOutput != null) {
                try {
                    openFileOutput.close();
                } catch (IOException e2) {
                }
            }
            return true;
        } catch (Exception e3) {
            Log.e(f2307a, "writeStorage: " + e3.getMessage());
            if (!z) {
                return false;
            }
            try {
                z.close();
                return false;
            } catch (IOException e4) {
                return false;
            }
        } finally {
            if (z) {
                try {
                    z.close();
                } catch (IOException e5) {
                }
            }
        }
    }

    public static boolean a(String str) {
        if (!str.equals("customphrase")) {
            return new File(a.f161c + j(str)).exists();
        }
        return new File(new StringBuilder().append(a.f161c).append("custom_acoustic_22132.raw").toString()).exists() && new File(new StringBuilder().append(a.f161c).append("custom_search_22132.raw").toString()).exists();
    }

    public static boolean a(Locale locale) {
        if (locale == v.a(locale)) {
            return true;
        }
        Log.i(f2307a, "Parser not found for system locale, " + locale + ".  Disabling local commands");
        return false;
    }

    public static byte[] a(Context context, int i) {
        InputStream openRawResource = context.getResources().openRawResource(i);
        try {
            byte[] bArr = new byte[openRawResource.available()];
            openRawResource.read(bArr);
            openRawResource.close();
            return bArr;
        } catch (IOException e2) {
            throw e2;
        } catch (Throwable th) {
            openRawResource.close();
            throw th;
        }
    }

    public static int b() {
        return a("ro.build.product", "").contains("obake") ? C0014R.drawable.zz_moto_settings : C0014R.drawable.vc_settings;
    }

    public static long b(Context context, String str, long j) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong(str, j);
    }

    public static String b(String str) {
        if (str != null) {
            if (str.startsWith("hellomoto")) {
                return "ma_1_okgooglenow_en_us_adults_sfs12_delivery3_am.raw";
            }
            if (str.equals("customphrase")) {
                return "custom_acoustic_22132.raw";
            }
        }
        throw new IllegalArgumentException("invalid phrase");
    }

    public static String b(Locale locale) {
        return locale.getLanguage().equals(Locale.ENGLISH.getLanguage()) ? "udt_speaker.raw" : "udt_speaker.raw";
    }

    public static void b(Context context, int i) {
        Intent intent = new Intent("com.motorola.audiomonitor.AOV_UPDATE");
        intent.putExtra("MONITOR_STATUS", i);
        if (f2308b) {
            Log.d(f2307a, "Sending Broadcast " + intent.toUri(0));
        }
        context.sendStickyBroadcast(intent);
    }

    public static boolean b(Context context) {
        return ((PowerManager) context.getSystemService("power")).isScreenOn();
    }

    public static boolean b(Context context, String str, boolean z) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(str, z);
    }

    /* JADX WARNING: type inference failed for: r3v0 */
    /* JADX WARNING: type inference failed for: r0v4 */
    /* JADX WARNING: type inference failed for: r4v0, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r3v1, types: [java.io.OutputStream] */
    /* JADX WARNING: type inference failed for: r4v1 */
    /* JADX WARNING: type inference failed for: r4v2, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r3v2, types: [java.io.OutputStream] */
    /* JADX WARNING: type inference failed for: r4v3 */
    /* JADX WARNING: type inference failed for: r3v3, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r0v12, types: [java.io.OutputStream] */
    /* JADX WARNING: type inference failed for: r4v4, types: [java.io.FileInputStream, java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r0v17 */
    /* JADX WARNING: type inference failed for: r3v4 */
    /* JADX WARNING: type inference failed for: r2v4, types: [java.io.OutputStream, java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r3v6 */
    /* JADX WARNING: type inference failed for: r3v7 */
    /* JADX WARNING: type inference failed for: r0v23 */
    /* JADX WARNING: type inference failed for: r3v8 */
    /* JADX WARNING: type inference failed for: r3v10 */
    /* JADX WARNING: type inference failed for: r3v11 */
    /* JADX WARNING: type inference failed for: r3v12 */
    /* JADX WARNING: type inference failed for: r3v13 */
    /* JADX WARNING: type inference failed for: r3v14 */
    /* JADX WARNING: type inference failed for: r4v5 */
    /* JADX WARNING: type inference failed for: r4v6 */
    /* JADX WARNING: type inference failed for: r4v7 */
    /* JADX WARNING: type inference failed for: r4v8 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0039 A[SYNTHETIC, Splitter:B:18:0x0039] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003e A[SYNTHETIC, Splitter:B:21:0x003e] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0061 A[SYNTHETIC, Splitter:B:39:0x0061] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0066 A[SYNTHETIC, Splitter:B:42:0x0066] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0072 A[SYNTHETIC, Splitter:B:50:0x0072] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0077 A[SYNTHETIC, Splitter:B:53:0x0077] */
    /* JADX WARNING: Unknown variable types count: 9 */
    public static boolean b(String str, String str2) {
        ? r0;
        ? r4;
        ? r3;
        ? r42;
        ? r32;
        ? r33;
        ? r34 = 0;
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return false;
        }
        try {
            File file = new File(str);
            File file2 = new File(str2);
            ? fileInputStream = new FileInputStream(file);
            try {
                ? fileOutputStream = new FileOutputStream(file2);
                try {
                    byte[] bArr = new byte[4096];
                    while (true) {
                        int read = fileInputStream.read(bArr);
                        if (read <= 0) {
                            break;
                        }
                        fileOutputStream.write(bArr, 0, read);
                    }
                    file2.setReadable(true, false);
                    file2.setWritable(true, false);
                    if (fileInputStream != 0) {
                        try {
                            fileInputStream.close();
                        } catch (IOException e2) {
                        }
                    }
                    if (fileOutputStream == 0) {
                        return true;
                    }
                    try {
                        fileOutputStream.close();
                        return true;
                    } catch (IOException e3) {
                        return true;
                    }
                } catch (FileNotFoundException e4) {
                    r0 = fileOutputStream;
                    r33 = fileInputStream;
                    if (r33 != 0) {
                        try {
                            r33.close();
                        } catch (IOException e5) {
                        }
                    }
                    if (r0 != 0) {
                        try {
                            r0.close();
                            return false;
                        } catch (IOException e6) {
                            return false;
                        }
                    }
                    return false;
                } catch (IOException e7) {
                    r32 = fileOutputStream;
                    r42 = fileInputStream;
                    if (r42 != 0) {
                        try {
                            r42.close();
                        } catch (IOException e8) {
                        }
                    }
                    if (r32 != 0) {
                        try {
                            r32.close();
                            return false;
                        } catch (IOException e9) {
                            return false;
                        }
                    }
                    return false;
                } catch (Throwable th) {
                    th = th;
                    r3 = fileOutputStream;
                    r4 = fileInputStream;
                    if (r4 != 0) {
                        try {
                            r4.close();
                        } catch (IOException e10) {
                        }
                    }
                    if (r3 != 0) {
                        try {
                            r3.close();
                        } catch (IOException e11) {
                        }
                    }
                    throw th;
                }
            } catch (FileNotFoundException e12) {
                r0 = 0;
                r33 = fileInputStream;
                if (r33 != 0) {
                }
                if (r0 != 0) {
                }
                return false;
            } catch (IOException e13) {
                r32 = r34;
                r42 = fileInputStream;
                if (r42 != 0) {
                }
                if (r32 != 0) {
                }
                return false;
            } catch (Throwable th2) {
                th = th2;
                r3 = r34;
                r4 = fileInputStream;
                if (r4 != 0) {
                }
                if (r3 != 0) {
                }
                throw th;
            }
        } catch (FileNotFoundException e14) {
            r0 = 0;
            r33 = r34;
            if (r33 != 0) {
            }
            if (r0 != 0) {
            }
            return false;
        } catch (IOException e15) {
            r42 = 0;
            r32 = r34;
            if (r42 != 0) {
            }
            if (r32 != 0) {
            }
            return false;
        } catch (Throwable th3) {
            th = th3;
            r4 = 0;
            r3 = r34;
            if (r4 != 0) {
            }
            if (r3 != 0) {
            }
            throw th;
        }
    }

    public static String c(String str) {
        if (str != null) {
            if (str.equals("hellomoto")) {
                return "ma_1_okgooglenow_en_us_adults_sfs12_delivery3_search_7.raw";
            }
            if (str.equals("customphrase")) {
                return "custom_search_22132.raw";
            }
        }
        throw new IllegalArgumentException("invalid phrase");
    }

    public static void c(Context context) {
        String[] strArr = {b("customphrase"), c("customphrase"), d("customphrase"), e("customphrase")};
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            f(context.getFilesDir().getAbsolutePath() + "/" + strArr[i]);
        }
    }

    public static boolean c(String str, String str2) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return false;
        }
        boolean renameTo = new File(str).renameTo(new File(str2));
        if (renameTo) {
            return renameTo;
        }
        Log.e(f2307a, "Failed to rename " + str + " to " + str2);
        return renameTo;
    }

    public static String d(String str) {
        if (str != null) {
            if (str.startsWith("hellomoto")) {
                return "ma_1_okgooglenow_en_us_adults_ma36_delivery3_am.bin";
            }
            if (str.equals("customphrase")) {
                return a("customphrase", n.ACOUSTIC, true);
            }
        }
        throw new IllegalArgumentException("invalid phrase");
    }

    public static void d(Context context) {
        Editor edit = PreferenceManager.getDefaultSharedPreferences(context).edit();
        edit.putString("key_phrase", "customphrase");
        edit.apply();
    }

    public static String e(String str) {
        if (str != null) {
            if (TextUtils.equals(str, "hellomoto")) {
                return "ma_1_okgooglenow_en_us_adults_ma36_delivery3_search_7.bin";
            }
            if (str.equals("customphrase")) {
                return a("customphrase", n.SEARCH, true);
            }
        }
        throw new IllegalArgumentException("invalid phrase");
    }

    public static void e(Context context) {
    }

    public static void f(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
        Intent intent = new Intent(context.getApplicationContext(), TrainingTriggerRecordingActivity.class);
        intent.putExtra("selection", "customphrase");
        intent.putExtra("fromInterruption", true);
        String string = context.getResources().getString(C0014R.string.personalization_update_notify);
        BigTextStyle bigTextStyle = new BigTextStyle(new Builder(context.getApplicationContext()).setSmallIcon(C0014R.drawable.vc_notify_discovery).setContentTitle(context.getResources().getString(C0014R.string.app_name)).setContentText(string).setContentIntent(PendingIntent.getActivity(context.getApplicationContext(), 0, intent, 0)).setOngoing(true));
        bigTextStyle.bigText(string);
        notificationManager.notify(1, bigTextStyle.build());
    }

    public static boolean f(String str) {
        if (str == null) {
            return false;
        }
        boolean delete = new File(str).delete();
        if (delete) {
            return delete;
        }
        Log.e(f2307a, "Failed to delete " + str);
        return delete;
    }

    public static void g(Context context) {
        ((NotificationManager) context.getSystemService("notification")).cancel(1);
    }

    public static boolean g(String str) {
        return str.length() > 0 && new File(str).length() < 24000;
    }

    public static String h(String str) {
        char[] charArray;
        StringBuffer stringBuffer = new StringBuffer();
        for (char c2 : str.toCharArray()) {
            if (c2 == '-') {
                stringBuffer.append(',');
            } else {
                stringBuffer.append(c2);
            }
            stringBuffer.append(' ');
        }
        return stringBuffer.toString().trim();
    }

    public static void h(Context context) {
        String[] strArr = {"com.google.android.googlequicksearchbox", "com.google.android.apps.maps"};
        String str = "aov-tts-user";
        AudioManager audioManager = (AudioManager) context.getSystemService("audio");
        StringBuilder sb = new StringBuilder();
        String str2 = "";
        for (String str3 : strArr) {
            sb.append(str2);
            sb.append("aov-tts-user");
            sb.append("=");
            sb.append(str3);
            str2 = ";";
        }
        audioManager.setParameters(sb.toString());
    }

    public static String i(String str) {
        return str != null ? str.length() > 16 ? str.substring(0, 16) : str : "";
    }

    private static String j(String str) {
        if (str != null && str.startsWith("hellomoto")) {
            return "svsid_triggerOGN_enUS_3_1.raw";
        }
        throw new IllegalArgumentException("invalid phrase");
    }
}
