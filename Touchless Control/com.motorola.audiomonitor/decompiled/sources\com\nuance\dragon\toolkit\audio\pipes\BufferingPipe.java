package com.nuance.dragon.toolkit.audio.pipes;

import com.nuance.dragon.toolkit.audio.AbstractAudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.AudioType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class BufferingPipe extends SingleSinkPipe {

    /* renamed from: a reason: collision with root package name */
    private final LinkedList f1142a = new LinkedList();

    /* renamed from: b reason: collision with root package name */
    private final ArrayList f1143b = new ArrayList();

    /* renamed from: c reason: collision with root package name */
    private AudioType f1144c = AudioType.UNKNOWN;

    /* renamed from: d reason: collision with root package name */
    private int f1145d;

    /* access modifiers changed from: protected */
    public void chunksAvailable(AudioSource audioSource, AudioSink audioSink) {
        this.f1143b.ensureCapacity(audioSource.getChunksAvailableForSink(audioSink));
        audioSource.getAllAudioChunksForSink(audioSink, this.f1143b);
        Iterator it = this.f1143b.iterator();
        while (it.hasNext()) {
            AbstractAudioChunk abstractAudioChunk = (AbstractAudioChunk) it.next();
            this.f1142a.add(abstractAudioChunk);
            this.f1145d += abstractAudioChunk.audioDuration;
            onChunkBuffered(abstractAudioChunk);
        }
        this.f1143b.clear();
        notifyChunksAvailable();
    }

    public void clearBuffers() {
        this.f1142a.clear();
        this.f1145d = 0;
    }

    public void connectAudioSource(AudioSource audioSource) {
        this.f1142a.clear();
        this.f1144c = audioSource.getAudioType();
        super.connectAudioSource(audioSource);
    }

    /* access modifiers changed from: protected */
    public void framesDropped(AudioSource audioSource, AudioSink audioSink) {
        notifyFramesDropped();
    }

    /* access modifiers changed from: protected */
    public AbstractAudioChunk getAudioChunk() {
        if (this.f1142a.isEmpty()) {
            return null;
        }
        AbstractAudioChunk abstractAudioChunk = (AbstractAudioChunk) this.f1142a.remove();
        this.f1145d -= abstractAudioChunk.audioDuration;
        return abstractAudioChunk;
    }

    public AudioType getAudioType() {
        return this.f1144c;
    }

    public int getBufferedDuration() {
        return this.f1145d;
    }

    public int getChunksAvailable() {
        return this.f1142a.size();
    }

    public boolean isActive() {
        return isSourceActive();
    }

    /* access modifiers changed from: protected */
    public void onChunkBuffered(AbstractAudioChunk abstractAudioChunk) {
    }

    /* access modifiers changed from: protected */
    public void sourceClosed(AudioSource audioSource, AudioSink audioSink) {
        notifySourceClosed();
    }
}
