package com.nuance.a.a.a.b.c.b.b;

import com.nuance.a.a.a.a.a.a.C0000a;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.a.b.a.b.C0002b;
import com.nuance.a.a.a.b.b.d;
import com.nuance.a.a.a.b.b.e;
import com.nuance.a.a.a.b.c.c.b;
import com.nuance.a.a.a.b.c.c.c;
import com.nuance.a.a.a.b.c.c.f;
import com.nuance.a.a.a.b.c.c.g;
import com.nuance.a.a.a.b.c.c.h;
import com.nuance.a.a.a.b.c.c.i;
import com.nuance.a.a.a.b.c.c.j;
import com.nuance.a.a.a.b.c.c.k;
import com.nuance.a.a.a.b.c.c.l;
import java.util.Vector;

public class z implements C0002b, b {
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public static final C0001a f888g = com.nuance.a.a.a.a.b.a.a.a(z.class);

    /* renamed from: a reason: collision with root package name */
    protected byte f889a;

    /* renamed from: h reason: collision with root package name */
    private a f890h;
    private f i;
    /* access modifiers changed from: private */
    public g j = null;
    /* access modifiers changed from: private */
    public com.nuance.a.a.a.b.c.b.a.b k = null;
    /* access modifiers changed from: private */
    public short l = -1;
    private com.nuance.a.a.a.a.b.a.b m;
    private com.nuance.a.a.a.a.b.a.e.a n;
    private long o;
    private boolean p = false;
    /* access modifiers changed from: private */
    public a q;
    /* access modifiers changed from: private */
    public Object r;

    public final class a {

        /* renamed from: a reason: collision with root package name */
        private int f892a;

        /* renamed from: b reason: collision with root package name */
        private String f893b;

        /* renamed from: c reason: collision with root package name */
        private String f894c;

        /* renamed from: d reason: collision with root package name */
        private z f895d;

        /* renamed from: e reason: collision with root package name */
        private boolean f896e;

        protected a(int i, z zVar) {
            this.f892a = i;
            if (zVar.k.d() != null) {
                this.f893b = com.nuance.a.a.a.b.c.b.a.b.a(zVar.k.d());
            } else {
                this.f893b = "";
            }
            this.f894c = "INTERNAL_ERROR";
            this.f895d = zVar;
            this.f896e = false;
        }

        static /* synthetic */ void a(a aVar, String str) {
            if (aVar.f894c.equals("INTERNAL_ERROR")) {
                aVar.f894c = str;
            } else {
                aVar.f894c += ":" + str;
            }
            z.a(aVar.f895d, aVar);
        }

        /* access modifiers changed from: protected */
        public final int a() {
            return this.f892a;
        }

        /* access modifiers changed from: protected */
        public final void a(com.nuance.a.a.a.b.c.c.b.a aVar) {
            if (this.f894c.equals("INTERNAL_ERROR")) {
                this.f894c = aVar.toString();
            } else {
                this.f894c += ":" + aVar.toString();
            }
            z.a(this.f895d, this);
        }

        /* access modifiers changed from: protected */
        public final void a(String str) {
            this.f893b = str;
        }

        /* access modifiers changed from: protected */
        public final String b() {
            return this.f893b;
        }

        /* access modifiers changed from: protected */
        public final String c() {
            return this.f894c;
        }

        /* access modifiers changed from: protected */
        public final void d() {
            this.f896e = true;
        }

        /* access modifiers changed from: protected */
        public final boolean e() {
            return this.f896e;
        }
    }

    protected z(com.nuance.a.a.a.a.b.a.b bVar, g gVar, String str, String str2, com.nuance.a.a.a.b.c.b.a.b bVar2, String str3, String str4, String str5, String str6, String str7, String str8, C0000a aVar, String str9, String str10, String str11, String str12, String str13, long j2, c cVar, a aVar2, f fVar, byte b2) {
        if (f888g.b()) {
            f888g.b((Object) "PDXTransactionImpl()");
        }
        this.r = new Object();
        this.m = bVar;
        this.k = bVar2;
        this.j = gVar;
        this.o = j2;
        this.f890h = aVar2;
        this.i = fVar;
        this.f889a = b2;
        if (((com.nuance.a.a.a.b.c.b.a.a) aVar2.g()).h() != null) {
            this.q = new a(this.f889a, this);
            b(this.q, "INTERNAL_ERROR");
        }
        synchronized (this.r) {
            this.l = 0;
        }
        Object[] objArr = new Object[18];
        objArr[0] = str3;
        objArr[1] = str4;
        objArr[2] = str5;
        objArr[3] = str6;
        objArr[4] = str7;
        objArr[5] = str8;
        objArr[6] = aVar;
        objArr[7] = str9;
        objArr[8] = new Short(1);
        objArr[9] = new Short(1);
        objArr[10] = str10;
        objArr[11] = str11;
        objArr[12] = str12;
        objArr[13] = str13;
        objArr[14] = str2;
        objArr[15] = str;
        objArr[16] = cVar;
        bVar.a(new com.nuance.a.a.a.a.b.a.b.a(1, objArr), this, Thread.currentThread(), bVar.a()[0]);
    }

    static /* synthetic */ void a(z zVar, a aVar) {
        Vector vector = (Vector) ((com.nuance.a.a.a.b.c.b.a.a) zVar.f890h.g()).h();
        if (vector == null) {
            f888g.c((Object) "appendLogToResLogs: NMSPDefines.DEVICE_CMD_LOG_TO_SERVER_ENABLED is disabled.");
        } else if (!vector.contains(aVar)) {
            vector.addElement(aVar);
        }
    }

    private static String b(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < bArr.length; i2++) {
            String hexString = Integer.toHexString(bArr[i2]);
            if (hexString.length() > 1) {
                stringBuffer.append(hexString.substring(hexString.length() - 2));
            } else {
                stringBuffer.append(hexString);
            }
            if (i2 == 3 || i2 == 5 || i2 == 7 || i2 == 9) {
                stringBuffer.append('-');
            }
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: private */
    public static void b(a aVar, String str) {
        if (aVar != null) {
            a.a(aVar, str);
        }
    }

    public final void a() {
        if (f888g.b()) {
            f888g.b((Object) "PDXTransactionImpl.end()");
        }
        synchronized (this.r) {
            if (this.l == -1) {
                f888g.e("PDXTransactionImpl.end() transaction already finished!");
                throw new d("transaction already finished!");
            } else if (this.l == 0) {
                this.l = 1;
                this.m.a(new com.nuance.a.a.a.a.b.a.b.a(4, null), this, Thread.currentThread(), this.m.a()[0]);
            } else if (this.l == 1) {
                f888g.e("PDXTransactionImpl.end() transaction already finished!");
                throw new d("transaction already finished!");
            } else if (this.l == 2) {
                f888g.e("PDXTransactionImpl.end() transaction already expired!");
                throw new e("transaction already expired!");
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    public final void a(r rVar, byte b2) {
        f888g.b((Object) "PDXTransactionImpl.onQueryError()");
        if (b2 == this.f889a) {
            synchronized (this.r) {
                if (this.l != -1 && this.l != 2) {
                    this.l = -1;
                    if (this.n != null) {
                        this.m.a(this.n);
                    }
                    if (this.j != null) {
                        try {
                            b(this.q, "QUERY_ERROR");
                            this.j.a((i) rVar);
                        } catch (Throwable th) {
                            f888g.e("got exp in PDXCommandListener.PDXQueryErrorReturned() [" + th.getClass().getName() + "] msg [" + th.getMessage() + "]");
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return;
     */
    public final void a(t tVar, byte b2) {
        if (f888g.b()) {
            f888g.b((Object) "PDXTransactionImpl.onQueryResults()");
        }
        if (b2 == this.f889a) {
            synchronized (this.r) {
                if (this.l != -1 && this.l != 2) {
                    if (this.n != null && tVar.g()) {
                        this.m.a(this.n);
                    }
                    if (this.j != null) {
                        try {
                            if (tVar.g()) {
                                b(this.q, "FINAL_RESULT");
                            }
                            this.j.a((j) tVar);
                        } catch (Throwable th) {
                            f888g.e("got exp in PDXCommandListener.PDXQueryResultReturned() [" + th.getClass().getName() + "] msg [" + th.getMessage() + "]");
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    public final void a(u uVar, byte b2) {
        f888g.b((Object) "PDXTransactionImpl.onQueryRetry()");
        if (b2 == this.f889a) {
            synchronized (this.r) {
                if (this.l != -1 && this.l != 2) {
                    this.l = -1;
                    if (this.n != null) {
                        this.m.a(this.n);
                    }
                    if (this.j != null) {
                        try {
                            b(this.q, "QUERY_RETRY");
                            this.j.a((k) uVar);
                        } catch (Throwable th) {
                            f888g.e("got exp in PDXCommandListener.PDXQueryRetryReturned() [" + th.getClass().getName() + "] msg [" + th.getMessage() + "]");
                        }
                    }
                }
            }
        }
    }

    public final void a(h hVar) {
        if (f888g.b()) {
            f888g.b((Object) "PDXTransactionImpl.sendParam()");
        }
        if (hVar == null) {
            throw new NullPointerException("Parameter cannot be null");
        }
        synchronized (this.r) {
            if (this.l == -1) {
                f888g.e("PDXTransactionImpl.sendParam() transaction already finished!");
                throw new d("transaction already finished!");
            } else if (this.l == 0) {
                this.m.a(new com.nuance.a.a.a.a.b.a.b.a(2, hVar), this, Thread.currentThread(), this.m.a()[0]);
            } else if (this.l == 1) {
                f888g.e("PDXTransactionImpl.sendParam() transaction already finished!");
                throw new d("transaction already finished!");
            } else if (this.l == 2) {
                f888g.e("PDXTransactionImpl.sendParam() transaction already expired!");
                throw new e("transaction already expired!");
            }
        }
    }

    public final void a(Object obj, Object obj2) {
        com.nuance.a.a.a.a.b.a.b.a aVar = (com.nuance.a.a.a.a.b.a.b.a) obj;
        Object obj3 = aVar.f784b;
        switch (aVar.f783a) {
            case 1:
                Object[] objArr = (Object[]) obj3;
                String str = (String) objArr[0];
                String str2 = (String) objArr[1];
                String str3 = (String) objArr[2];
                String str4 = (String) objArr[3];
                String str5 = (String) objArr[4];
                String str6 = (String) objArr[5];
                C0000a aVar2 = (C0000a) objArr[6];
                String str7 = (String) objArr[7];
                short shortValue = ((Short) objArr[8]).shortValue();
                short shortValue2 = ((Short) objArr[9]).shortValue();
                String str8 = (String) objArr[10];
                String str9 = (String) objArr[11];
                String str10 = (String) objArr[12];
                String str11 = (String) objArr[13];
                byte[] d2 = this.k.d();
                String str12 = (String) objArr[14];
                String str13 = (String) objArr[15];
                c cVar = (c) objArr[16];
                if (new com.nuance.a.a.a.c.a(this.f890h.f848d).a()) {
                    aVar2 = com.nuance.a.a.a.a.d.d.b(aVar2);
                }
                com.nuance.a.a.a.b.c.b.a.d.i();
                p pVar = new p(str, str2, str3, str4, str5, str6, aVar2, str7, shortValue, shortValue2, str8, str9, str10, str11, d2, str12, str13, cVar);
                Vector vector = (Vector) ((com.nuance.a.a.a.b.c.b.a.a) this.f890h.g()).h();
                if (vector == null) {
                    f888g.c((Object) "appendLogToQueryBegin: NMSPDefines.DEVICE_CMD_LOG_TO_SERVER_ENABLED is disabled");
                } else if (vector.size() == 0) {
                    f888g.c((Object) "appendLogToQueryBegin: nmasResLogsToServer is empty, nothing to log to server");
                } else {
                    int size = vector.size();
                    i iVar = new i();
                    w wVar = new w();
                    for (int i2 = 0; i2 < size; i2++) {
                        a aVar3 = (a) vector.elementAt(i2);
                        if (aVar3.a() != this.f889a) {
                            i iVar2 = new i();
                            iVar2.a("id", aVar3.b() + ":" + aVar3.a(), 193);
                            iVar2.a("status", aVar3.c().toString(), 193);
                            wVar.a((c) iVar2);
                            aVar3.d();
                        }
                    }
                    iVar.a("device_log", (l) wVar);
                    pVar.a("app_info", (c) iVar);
                }
                this.k.a(2597, "SEND_BCP_BEGIN" + this.f889a, pVar.f(), null, this.f889a, 0, this.f890h, false);
                if (d2 != null) {
                    try {
                        if (!this.p) {
                            this.p = true;
                            f888g.b((Object) "PDXCommandCreated() called from handleInit()" + b(d2) + ":" + this.f889a + " (" + this + "," + this.i + ")");
                            this.i.a(b(d2) + ":" + this.f889a);
                            return;
                        }
                        return;
                    } catch (Throwable th) {
                        f888g.e("got exp in PDXCommandListener.PDXCommandCreated() [" + th.getClass().getName() + "] msg [" + th.getMessage() + "]");
                        return;
                    }
                } else {
                    return;
                }
            case 2:
                h hVar = (h) obj3;
                if (((o) hVar).c() == Byte.MAX_VALUE) {
                    this.k.a(((x) hVar).a(), ((x) hVar).e(), (com.nuance.a.a.a.b.c.b.a.c) this.f890h);
                }
                com.nuance.a.a.a.b.c.b.a.d.i();
                this.k.a(2585, "SEND_BCP_DATA", new s((o) hVar).d(), null, this.f889a, 0, this.f890h, false);
                return;
            case 3:
                byte[] bArr = (byte[]) obj3;
                com.nuance.a.a.a.b.c.b.a.d.i();
                this.k.a(2585, "SEND_BCP_DATA", new j(bArr).d(), null, this.f889a, 0, this.f890h, false);
                return;
            case 4:
                com.nuance.a.a.a.b.c.b.a.d.i();
                this.k.a(2585, "SEND_BCP_DATA", new q().d(), null, this.f889a, 0, this.f890h, false);
                this.n = new com.nuance.a.a.a.a.b.a.e.a() {
                    public final void run() {
                        try {
                            synchronized (z.this.r) {
                                z.this.l = -1;
                            }
                            if (z.this.j != null) {
                                z.f888g.b((Object) "PDXTransactionImpl.end() timed out waiting for results. ");
                                try {
                                    z zVar = z.this;
                                    z.b(z.this.q, "TIMEOUT_CMD");
                                    z.this.j.a(1);
                                } catch (Throwable th) {
                                    z.f888g.e("got exp in PDXCommandListener.PDXCommandEvent(TIMED_OUT_WAITING_FOR_RESULT) [" + th.getClass().getName() + "] msg [" + th.getMessage() + "]");
                                }
                            }
                        } catch (Exception e2) {
                            z.f888g.e("PDXTransactionImpl.run() " + e2.getClass().getName() + " " + e2.getMessage());
                        }
                    }
                };
                this.m.a(this.n, this.o);
                return;
            case 5:
                this.j.a(4);
                return;
            case 6:
                com.nuance.a.a.a.b.c.c.b.a aVar4 = (com.nuance.a.a.a.b.c.c.b.a) obj3;
                a aVar5 = this.q;
                if (aVar5 != null) {
                    aVar5.a(aVar4);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public final void a(short s) {
        if (f888g.b()) {
            f888g.b((Object) "PDXTransactionImpl.onSessionDisconnected() " + s);
        }
        synchronized (this.r) {
            if (this.l == 1 && this.n != null) {
                this.m.a(this.n);
            }
            if (this.l != 2) {
                if (s == 0) {
                    try {
                        if (this.l != -1) {
                            b(this.q, "REMOTE_DISC");
                            this.j.a(3);
                        }
                    } catch (Throwable th) {
                        f888g.e("got exp in PDXCommandListener.PDXCommandEvent() or PDXManagerListener.PDXManagerError() or PDXManagerListener.PDXManagerDisconnected() reasonCode [" + s + "] [" + th.getClass().getName() + "] msg [" + th.getMessage() + "]");
                    }
                } else if (s == 1) {
                    if (this.l != -1) {
                        this.j.a(4);
                    }
                } else if (s == 3) {
                    if (this.l != -1) {
                        b(this.q, "TIMEOUT_IDLE");
                        this.j.a(5);
                    }
                } else if (s == 4) {
                    if (this.l != -1) {
                        b(this.q, "CONN_FAILED");
                        this.i.a();
                    }
                } else if (s == 5) {
                    if (this.l != -1) {
                        b(this.q, "CONN_FAILED");
                        this.i.a();
                    }
                } else if (s == 6) {
                    if (this.l != -1) {
                        b(this.q, "REMOTE_DISC");
                        this.j.a(3);
                    }
                } else if (s == 7) {
                    if (this.l != -1) {
                        b(this.q, "CONN_FAILED");
                        this.i.a();
                    }
                } else if (s == 8 && this.l != -1) {
                    b(this.q, "REMOTE_DISC");
                    this.j.a(3);
                }
                this.l = 2;
                return;
            }
            return;
        }
    }

    public final void a(byte[] bArr) {
        if (f888g.b()) {
            f888g.b((Object) "PDXTransactionImpl.onSessionConnected()");
        }
        synchronized (this.r) {
            if (this.l == 0 || this.l == 1) {
                try {
                    String b2 = b(bArr);
                    a aVar = this.q;
                    if (aVar != null) {
                        aVar.a(b2);
                    }
                    if (!this.p) {
                        this.p = true;
                        f888g.b((Object) "PDXCommandCreated() called from onSessionConnected()" + b2 + ":" + this.f889a + " (" + this + "," + this.i + ")");
                        this.i.a(b2 + ":" + this.f889a);
                    }
                } catch (Throwable th) {
                    f888g.e("got exp in PDXCommandListener.PDXCommandCreated() [" + th.getClass().getName() + "] msg [" + th.getMessage() + "]");
                }
            }
        }
    }

    public final void b() {
        f();
    }

    public final void c() {
        f();
    }

    public final void d() {
        f();
    }

    public final void e() {
        synchronized (this.r) {
            if (this.l == 1 && this.n != null) {
                this.m.a(this.n);
            }
            if (this.l != 2) {
                if (this.l != -1) {
                    this.m.a(new com.nuance.a.a.a.a.b.a.b.a(5, null), this, Thread.currentThread(), this.m.a()[0]);
                }
                this.l = 2;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void f() {
        if (this.n != null) {
            this.m.a(this.n);
            this.m.a(this.n, this.o);
        }
    }
}
