package com.nuance.dragon.toolkit.hybrid;

import com.nuance.dragon.toolkit.cloudservices.recognizer.CloudRecognitionError;
import com.nuance.dragon.toolkit.elvis.ElvisError;
import com.nuance.dragon.toolkit.recognition.InterpretException;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import org.json.JSONObject;

public final class HybridRecognitionError implements JSONCompliant {

    /* renamed from: a reason: collision with root package name */
    private ElvisError f1834a;

    /* renamed from: b reason: collision with root package name */
    private CloudRecognitionError f1835b;

    /* renamed from: c reason: collision with root package name */
    private InterpretException f1836c;

    /* renamed from: d reason: collision with root package name */
    private InterpretException f1837d;

    public static HybridRecognitionError createFromJSON(JSONObject jSONObject) {
        CloudRecognitionError cloudRecognitionError = null;
        JSONObject optJSONObject = jSONObject.optJSONObject("elvis");
        JSONObject optJSONObject2 = jSONObject.optJSONObject("cloud");
        String optString = jSONObject.optString("elvis_ex");
        String optString2 = jSONObject.optString("cloud_ex");
        ElvisError createFromJSON = optJSONObject == null ? null : ElvisError.createFromJSON(optJSONObject);
        if (optJSONObject2 != null) {
            cloudRecognitionError = CloudRecognitionError.createFromJSON(optJSONObject);
        }
        HybridRecognitionError hybridRecognitionError = new HybridRecognitionError();
        hybridRecognitionError.f1834a = createFromJSON;
        hybridRecognitionError.f1835b = cloudRecognitionError;
        if (optString != null) {
            hybridRecognitionError.f1836c = new InterpretException(optString);
        }
        if (optString2 != null) {
            hybridRecognitionError.f1837d = new InterpretException(optString2);
        }
        return hybridRecognitionError;
    }

    /* access modifiers changed from: 0000 */
    public final void a(CloudRecognitionError cloudRecognitionError) {
        this.f1835b = cloudRecognitionError;
    }

    /* access modifiers changed from: 0000 */
    public final void a(ElvisError elvisError) {
        this.f1834a = elvisError;
    }

    /* access modifiers changed from: 0000 */
    public final void a(InterpretException interpretException) {
        this.f1836c = interpretException;
    }

    /* access modifiers changed from: 0000 */
    public final void b(InterpretException interpretException) {
        this.f1837d = interpretException;
    }

    public final CloudRecognitionError getCloudError() {
        return this.f1835b;
    }

    public final String getCloudInterpretationError() {
        if (this.f1837d == null) {
            return null;
        }
        return this.f1837d.getMessage();
    }

    public final ElvisError getElvisError() {
        return this.f1834a;
    }

    public final String getElvisInterpretationError() {
        if (this.f1836c == null) {
            return null;
        }
        return this.f1836c.getMessage();
    }

    public final JSONObject toJSON() {
        b bVar = new b();
        bVar.a("elvis", (JSONCompliant) this.f1834a);
        bVar.a("cloud", (JSONCompliant) this.f1835b);
        if (this.f1836c != null) {
            bVar.a("elvis_ex", (Object) this.f1836c.getMessage());
        }
        if (this.f1837d != null) {
            bVar.a("cloud_ex", (Object) this.f1837d.getMessage());
        }
        return bVar;
    }
}
