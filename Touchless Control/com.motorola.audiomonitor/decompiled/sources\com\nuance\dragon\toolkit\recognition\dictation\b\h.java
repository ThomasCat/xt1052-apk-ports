package com.nuance.dragon.toolkit.recognition.dictation.b;

import com.nuance.dragon.toolkit.recognition.a.c;
import com.nuance.dragon.toolkit.recognition.dictation.a.f;
import com.nuance.dragon.toolkit.recognition.dictation.a.g;
import com.nuance.dragon.toolkit.recognition.dictation.b.f.a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.StringTokenizer;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public final class h extends DefaultHandler implements a {

    /* renamed from: a reason: collision with root package name */
    private Stack f1987a = new Stack();

    /* renamed from: b reason: collision with root package name */
    private StringBuffer f1988b = null;

    /* renamed from: c reason: collision with root package name */
    private StringBuffer f1989c = null;

    /* renamed from: d reason: collision with root package name */
    private String f1990d = null;

    /* renamed from: e reason: collision with root package name */
    private String f1991e = null;

    /* renamed from: f reason: collision with root package name */
    private double f1992f = 0.0d;

    /* renamed from: g reason: collision with root package name */
    private ArrayList f1993g = new ArrayList();

    /* renamed from: h reason: collision with root package name */
    private Map f1994h = new HashMap();

    private void a(g gVar) {
        if (!this.f1993g.isEmpty()) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.f1993g.size()) {
                    break;
                } else if (((g) this.f1993g.get(i2)).getConfidenceScore() < gVar.getConfidenceScore()) {
                    this.f1993g.add(i2, gVar);
                    return;
                } else {
                    i = i2 + 1;
                }
            }
        }
        this.f1993g.add(gVar);
    }

    private void c() {
        if (this.f1987a.isEmpty() || this.f1987a.peek() != "input") {
            throw new SAXException("End Element> The top of the stack does not contain the token interpretation");
        } else if (this.f1990d == null) {
            throw new SAXException("Did not get any timings from input");
        } else if (this.f1988b == null) {
            throw new SAXException("Did not get any character from input");
        } else {
            StringTokenizer stringTokenizer = new StringTokenizer(this.f1988b.toString(), " ");
            StringTokenizer stringTokenizer2 = new StringTokenizer(this.f1990d, ",");
            StringTokenizer stringTokenizer3 = this.f1991e != null ? new StringTokenizer(this.f1991e, ",") : null;
            this.f1988b = null;
            this.f1990d = null;
            this.f1991e = null;
            if (stringTokenizer.countTokens() != stringTokenizer2.countTokens()) {
                throw new SAXException("timing(" + stringTokenizer2.countTokens() + ") and inputs(" + stringTokenizer.countTokens() + ") information does not have the same number of items");
            }
            g gVar = new g();
            gVar.a(this.f1992f);
            while (stringTokenizer.hasMoreTokens()) {
                String nextToken = stringTokenizer.nextToken();
                String nextToken2 = stringTokenizer2.nextToken();
                double d2 = 0.0d;
                if (stringTokenizer3 != null) {
                    d2 = Double.parseDouble(stringTokenizer3.nextToken());
                }
                gVar.a(new com.nuance.dragon.toolkit.recognition.dictation.a.h(nextToken, Long.parseLong(nextToken2.substring(0, nextToken2.indexOf("-"))), Long.parseLong(nextToken2.substring(nextToken2.indexOf("-") + 1)), d2));
            }
            a(gVar);
        }
    }

    public final f a() {
        return new f(this.f1993g, this.f1994h);
    }

    public final c b() {
        return null;
    }

    public final void characters(char[] cArr, int i, int i2) {
        new StringBuilder("Received characters: ").append(new String(cArr, i, i2));
        if (!this.f1987a.isEmpty() && this.f1987a.peek() == "input") {
            if (this.f1988b == null) {
                this.f1988b = new StringBuffer(i2);
            }
            this.f1988b.append(cArr, i, i2);
        } else if (!this.f1987a.isEmpty() && this.f1987a.peek() == "NSS_Audio_Statistics") {
            if (this.f1989c == null) {
                this.f1989c = new StringBuffer(i2);
            }
            this.f1989c.append(cArr, i, i2);
        }
    }

    public final void endElement(String str, String str2, String str3) {
        new StringBuilder("Received endElement ").append(str2);
        if (str2.equals("result")) {
            if (this.f1987a.isEmpty() || this.f1987a.peek() != "result") {
                throw new SAXException("End Element> The top of the stack does not contain the token result");
            }
            this.f1987a.pop();
        } else if (str2.equals("interpretation")) {
            if (this.f1987a.isEmpty() || this.f1987a.peek() != "interpretation") {
                throw new SAXException("End Element> The top of the stack does not contain the token interpretation");
            }
            this.f1987a.pop();
        } else if (str2.equals("input")) {
            c();
            this.f1987a.pop();
        } else if (str2.equals("NSS_Audio_Statistics")) {
            if (this.f1987a.isEmpty() || this.f1987a.peek() != "NSS_Audio_Statistics") {
                throw new SAXException("End Element> The top of the stack does not contain the token NSS_Audio_Statistics");
            }
            this.f1987a.pop();
        } else if (!this.f1987a.isEmpty() && this.f1987a.peek() == "NSS_Audio_Statistics") {
            String trim = this.f1989c == null ? "" : this.f1989c.toString().trim();
            if (str2.equals("InputAudioLength")) {
                this.f1994h.put("IAL", trim);
            }
            this.f1994h.put(str2, trim);
            this.f1989c = null;
        }
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        new StringBuilder("Received startElement ").append(str2);
        if (str2.equals("result")) {
            if (this.f1987a.size() != 0) {
                throw new SAXException("StartElement> Found result but it is not the first token.");
            }
            this.f1987a.push("result");
        } else if (str2.equals("interpretation")) {
            if (this.f1987a.isEmpty() || this.f1987a.peek() != "result") {
                throw new SAXException("StartElement> The interpretation token was found without a result being opened before.");
            }
            this.f1987a.push("interpretation");
            String value = attributes.getValue("timing");
            String value2 = attributes.getValue("confidence");
            String value3 = attributes.getValue("word_confidence");
            if (value == null) {
                throw new SAXException("StartElement> There are no timings associated with this interpretation.");
            } else if (value2 == null) {
                throw new SAXException("StartElement> There is no confidence associated with this interpretation.");
            } else {
                this.f1990d = value;
                this.f1991e = value3;
                try {
                    this.f1992f = Double.parseDouble(value2);
                } catch (NumberFormatException e2) {
                    throw new SAXException("StartElement> Could not parse the confidence: " + value2, e2);
                }
            }
        } else if (str2.equals("input")) {
            if (this.f1987a.isEmpty() || this.f1987a.peek() != "interpretation") {
                throw new SAXException("StartElement> The input token was found without a interpretation being opened before.");
            }
            this.f1987a.push("input");
        } else if (!str2.equals("NSS_Audio_Statistics")) {
        } else {
            if (this.f1987a.isEmpty() || this.f1987a.peek() != "result") {
                throw new SAXException("StartElement> The NSS_Audio_Statistics token was found without a result being opened before.");
            }
            this.f1987a.push("NSS_Audio_Statistics");
        }
    }
}
