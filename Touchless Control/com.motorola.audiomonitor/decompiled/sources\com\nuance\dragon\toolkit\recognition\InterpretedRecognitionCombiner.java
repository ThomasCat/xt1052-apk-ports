package com.nuance.dragon.toolkit.recognition;

public interface InterpretedRecognitionCombiner {
    InterpretedRecognition combine(InterpretedRecognition interpretedRecognition, InterpretedRecognition interpretedRecognition2);
}
