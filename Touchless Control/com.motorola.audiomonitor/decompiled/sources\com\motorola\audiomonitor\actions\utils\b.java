package com.motorola.audiomonitor.actions.utils;

public class b {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final String f219a;

    /* renamed from: b reason: collision with root package name */
    private final String f220b;

    /* renamed from: c reason: collision with root package name */
    private final int f221c;

    /* renamed from: d reason: collision with root package name */
    private float f222d;

    public b(String str, String str2, int i, float f2) {
        if (str == null || str2 == null) {
            throw new IllegalArgumentException(str + ":" + str2);
        }
        this.f219a = str;
        this.f220b = str2;
        this.f221c = i;
        this.f222d = f2;
    }

    public String a() {
        return this.f219a;
    }

    public void a(float f2) {
        this.f222d = f2;
    }

    public String b() {
        return this.f220b;
    }

    public float c() {
        return this.f222d;
    }

    public int d() {
        return this.f221c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        b bVar = (b) obj;
        if (this.f219a == null) {
            if (bVar.f219a != null) {
                return false;
            }
        } else if (!this.f219a.equals(bVar.f219a)) {
            return false;
        }
        return this.f221c == bVar.f221c;
    }

    public int hashCode() {
        return (((this.f219a == null ? 0 : this.f219a.hashCode()) + 31) * 31) + this.f221c;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[" + String.format("%2.0f", new Object[]{Float.valueOf(this.f222d)}) + "] ");
        stringBuffer.append(this.f220b);
        if (this.f221c != -1) {
            stringBuffer.append("(");
            stringBuffer.append(this.f221c);
            stringBuffer.append(")");
        }
        return stringBuffer.toString();
    }
}
