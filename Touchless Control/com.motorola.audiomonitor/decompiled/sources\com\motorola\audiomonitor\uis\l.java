package com.motorola.audiomonitor.uis;

import android.animation.TimeAnimator;
import android.animation.TimeAnimator.TimeListener;

class l implements TimeListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ AOVMicFragment f712a;

    l(AOVMicFragment aOVMicFragment) {
        this.f712a = aOVMicFragment;
    }

    public void onTimeUpdate(TimeAnimator timeAnimator, long j, long j2) {
        int i = 0;
        while (true) {
            if (i < AOVMicFragment.j.length) {
                if (this.f712a.G > AOVMicFragment.j[i] && this.f712a.D < AOVMicFragment.l[i]) {
                    this.f712a.D = AOVMicFragment.l[i];
                    this.f712a.C = AOVMicFragment.k[i];
                    break;
                }
                i++;
            } else {
                break;
            }
        }
        int c2 = this.f712a.F + this.f712a.C;
        int c3 = this.f712a.F + this.f712a.D;
        if (!(this.f712a.I == null || this.f712a.o == null || this.f712a.I.height == c2)) {
            this.f712a.I.height = c2;
            this.f712a.I.width = c2;
            this.f712a.o.setLayoutParams(this.f712a.I);
            this.f712a.o.invalidate();
        }
        if (!(this.f712a.H == null || this.f712a.n == null || this.f712a.H.height == c3)) {
            this.f712a.H.height = c3;
            this.f712a.H.width = c3;
            this.f712a.n.setLayoutParams(this.f712a.H);
            this.f712a.n.invalidate();
        }
        AOVMicFragment.c(this.f712a, 1);
        AOVMicFragment.d(this.f712a, 3);
        if (this.f712a.C < 0) {
            this.f712a.C = 0;
        }
        if (this.f712a.D < 0) {
            this.f712a.D = 0;
        }
        this.f712a.G = 0;
    }
}
