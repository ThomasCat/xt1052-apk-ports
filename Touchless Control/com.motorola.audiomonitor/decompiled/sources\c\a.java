package c;

import android.content.Context;
import android.util.Log;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;

public class a {

    /* renamed from: a reason: collision with root package name */
    private static final String f101a = ("AMonitor" + a.class.getSimpleName());

    private static c a(String str, Iterator it) {
        c cVar;
        if (it != null) {
            try {
                cVar = new c(str);
            } catch (NoClassDefFoundError e2) {
                Log.e(f101a, "Check-in not available, logging disabled");
                cVar = null;
            }
            if (cVar == null) {
                return cVar;
            }
            try {
                a(cVar, "timestamp", new Timestamp(new Date().getTime()).toString());
                while (it.hasNext()) {
                    f fVar = (f) it.next();
                    a(cVar, fVar.a(), fVar.b());
                }
                return cVar;
            } catch (IllegalArgumentException e3) {
                Log.e(f101a, "Unable to set segment due to invalid parameters.");
            }
        }
        return null;
    }

    private static void a(c cVar, String str, String str2) {
        if (cVar == null || str == null || str.isEmpty()) {
            throw new IllegalArgumentException();
        }
        if (str2 == null || str2.isEmpty()) {
            str2 = "null";
        }
        cVar.a(str, str2);
    }

    public static boolean a(Context context, String str, String str2, Iterator it) {
        b bVar;
        if (context == null) {
            return false;
        }
        try {
            bVar = new b(str, str2, "1.0");
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
            bVar = null;
        } catch (NoClassDefFoundError e3) {
            Log.e(f101a, "Check-in not available, logging disabled");
            bVar = null;
        }
        if (bVar == null) {
            return false;
        }
        c a2 = a(str2, it);
        if (a2 == null) {
            return false;
        }
        bVar.a(a2);
        try {
            bVar.a(context.getContentResolver());
            return true;
        } catch (Exception e4) {
            Log.e(f101a, "Check-in failed to publish data");
            return false;
        }
    }

    public static boolean a(Context context, String str, Iterator it) {
        return a(context, "AOV", str, it);
    }
}
