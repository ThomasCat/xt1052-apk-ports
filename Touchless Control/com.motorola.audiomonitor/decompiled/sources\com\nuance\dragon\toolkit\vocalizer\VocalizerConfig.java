package com.nuance.dragon.toolkit.vocalizer;

import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import com.nuance.dragon.toolkit.util.internal.d;
import org.json.JSONObject;

public final class VocalizerConfig implements JSONCompliant {

    /* renamed from: a reason: collision with root package name */
    final String f2150a;

    /* renamed from: b reason: collision with root package name */
    final String f2151b;

    /* renamed from: c reason: collision with root package name */
    final int f2152c;

    public VocalizerConfig(VocalizerLanguage vocalizerLanguage) {
        d.a("language", (Object) vocalizerLanguage);
        this.f2150a = vocalizerLanguage.name;
        this.f2151b = null;
        this.f2152c = 0;
    }

    public VocalizerConfig(VocalizerVoice vocalizerVoice) {
        d.a("voice", (Object) vocalizerVoice);
        this.f2150a = vocalizerVoice.language.name;
        this.f2151b = vocalizerVoice.name;
        this.f2152c = 0;
    }

    public VocalizerConfig(String str) {
        d.a("language", str);
        this.f2150a = str;
        this.f2151b = null;
        this.f2152c = 0;
    }

    public VocalizerConfig(String str, int i) {
        d.a("language", str);
        d.a("freqKhz", "equals to one of the values from \"Vocalizer.Frequencies\" class", i == 0 || i == 8 || i == 11 || i == 16 || i == 22);
        this.f2150a = str;
        this.f2151b = null;
        this.f2152c = i;
    }

    public VocalizerConfig(String str, String str2) {
        d.a("language and voice", str, str2);
        this.f2150a = str;
        this.f2151b = str2;
        this.f2152c = 0;
    }

    public VocalizerConfig(String str, String str2, int i) {
        boolean z = false;
        d.a("language and voice", str, str2);
        String str3 = "freqKhz";
        String str4 = "equals to one of the values from \"Vocalizer.Frequencies\" class";
        if (i == 0 || i == 8 || i == 11 || i == 16 || i == 22) {
            z = true;
        }
        d.a(str3, str4, z);
        this.f2150a = str;
        this.f2151b = str2;
        this.f2152c = i;
    }

    public static VocalizerConfig createFromJSON(JSONObject jSONObject) {
        int optInt = jSONObject.optInt("frequency", 0);
        String optString = jSONObject.optString("language", null);
        return new VocalizerConfig(optString, optString == null ? jSONObject.getString("voice") : jSONObject.optString("voice", null), optInt);
    }

    public final JSONObject toJSON() {
        b bVar = new b();
        bVar.a("language", (Object) this.f2150a);
        bVar.a("voice", (Object) this.f2151b);
        bVar.a("frequency", (Object) Integer.valueOf(this.f2152c));
        return bVar;
    }
}
