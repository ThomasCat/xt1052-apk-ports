package com.nuance.dragon.toolkit.grammar;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class WordAction {

    /* renamed from: a reason: collision with root package name */
    private final Word f1731a;

    /* renamed from: b reason: collision with root package name */
    private final boolean f1732b;

    public WordAction(Word word, boolean z) {
        this.f1731a = word;
        this.f1732b = z;
    }

    private WordAction(ObjectInputStream objectInputStream) {
        this.f1732b = objectInputStream.readBoolean();
        this.f1731a = new Word(objectInputStream);
    }

    public static List readListFrom(ObjectInputStream objectInputStream) {
        int readInt = objectInputStream.readInt();
        ArrayList arrayList = new ArrayList(readInt);
        while (true) {
            int i = readInt - 1;
            if (readInt <= 0) {
                return arrayList;
            }
            arrayList.add(new WordAction(objectInputStream));
            readInt = i;
        }
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.grammar.WordAction>, for r3v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.grammar.WordAction>] */
    public static void writeListTo(List<WordAction> list, ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeInt(list.size());
        for (WordAction wordAction : list) {
            objectOutputStream.writeBoolean(wordAction.f1732b);
            wordAction.f1731a.a(objectOutputStream);
        }
    }

    public final boolean equals(Object obj) {
        WordAction wordAction = (WordAction) obj;
        return this.f1732b == wordAction.f1732b && this.f1731a.equals(wordAction.f1731a);
    }

    public Word getWord() {
        return this.f1731a;
    }

    public final int hashCode() {
        return this.f1732b ? this.f1731a.hashCode() : -this.f1731a.hashCode();
    }

    public boolean isAdded() {
        return this.f1732b;
    }

    public final String toString() {
        return "[Word:" + this.f1731a + ", Added:" + this.f1732b + "]";
    }
}
