package f;

import com.nuance.dragon.toolkit.elvis.Constraint;
import com.nuance.dragon.toolkit.elvis.Grammar;
import com.nuance.dragon.toolkit.elvis.WordSlot;
import com.nuance.dragon.toolkit.grammar.Word;
import java.util.ArrayList;
import java.util.List;

public class w {
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<java.lang.String>, for r7v0, types: [java.util.List, java.util.List<java.lang.String>] */
    public static Grammar a(List<String> list, boolean z) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        if (list != null && list.size() > 0) {
            WordSlot wordSlot = new WordSlot("spotCommands");
            for (String word : list) {
                wordSlot.addWord(new Word(word));
            }
            arrayList.add(wordSlot);
        }
        Constraint constraint = new Constraint("spotCommand");
        constraint.addTransition(Constraint.START, "spotCommands", 0);
        constraint.addTransition("spotCommands", Constraint.END, 0);
        arrayList2.add(constraint);
        if (z) {
            arrayList.add(new WordSlot("garbage", 4));
            Constraint constraint2 = new Constraint("garbageConst");
            constraint2.addTransition(Constraint.START, "garbage", 0);
            constraint2.addTransition("garbage", Constraint.END, 0);
            arrayList2.add(constraint2);
        }
        return Grammar.createGrammar(arrayList, arrayList2);
    }
}
