package com.motorola.audiomonitor.uis;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.actions.utils.an;
import com.motorola.audiomonitor.actions.utils.m;

public class AOVErrorActivity extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public boolean f600a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public AOVMicFragment f601b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public CountDownTimer f602c;

    /* access modifiers changed from: protected */
    public void a() {
        this.f602c = new k(this, 5000, 100);
        this.f602c.start();
    }

    public void onBackPressed() {
        super.onBackPressed();
        if (m.a((Context) this)) {
            this.f600a = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().addFlags(524288);
        setContentView(C0014R.layout.aov_error);
        this.f601b = (AOVMicFragment) getFragmentManager().findFragmentById(C0014R.id.aov_error_mic_fragment);
        AOVSpeakNowStatusFragment aOVSpeakNowStatusFragment = (AOVSpeakNowStatusFragment) getFragmentManager().findFragmentById(C0014R.id.aov_error_top_prompt_fragment);
        if (this.f601b != null && this.f601b.isInLayout()) {
            this.f601b.a(AOVMicFragment.f608f);
            a();
        }
        if (aOVSpeakNowStatusFragment != null && aOVSpeakNowStatusFragment.isInLayout()) {
            Bundle extras = getIntent().getExtras();
            if (extras == null || !extras.containsKey("ERROR_TYPE")) {
                aOVSpeakNowStatusFragment.b(an.a().a((int) C0014R.string.data_error_name, (int) C0014R.string.data_error));
            } else if (extras.getInt("ERROR_TYPE") == 1) {
                aOVSpeakNowStatusFragment.b(getResources().getString(C0014R.string.airplane_mode_error));
            } else {
                aOVSpeakNowStatusFragment.b(an.a().a((int) C0014R.string.data_error_name, (int) C0014R.string.data_error));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.f600a = true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.f600a = false;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (!m.a((Context) this) || isFinishing()) {
            this.f600a = true;
        }
        if (this.f602c != null) {
            this.f602c.cancel();
            this.f602c = null;
        }
    }
}
