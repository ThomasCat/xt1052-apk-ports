package com.motorola.audiomonitor;

public class RecognizerConfig {
    public static final int MODE_RECOGNITION_ONLY = 1;
    public static final int MODE_SPEECH_DETECTION_AND_RECOGNITION = 2;
    public static final int MODE_SPEECH_DETECTION_ONLY = 0;
    private final int mAudioSampleRate;
    private final float mESilenceT;
    private final float mESpeechT;
    private final float mEnrollThreshold;
    private final String mGrammar;
    private final float mLongTermMs;
    private final int mNBest;
    private final int mNumEnroll;
    private final int mNumTrainingSample;
    private final boolean mPhraseSpot;
    private final float mPhrasespotAbsBeam;
    private final float mPhrasespotBeam;
    private final float mPhrasespotDelay;
    private final float mPhrasespotParamAOffset;
    private final String[] mPronuns;
    private final float mRecogLSilence;
    private final float mRecogMaxRecord;
    private final float mRecogMinDuration;
    private final int mRecogMode;
    private final float mRecogTSilence;
    private final float mRecogThreshold;
    private final float mSearchAny;
    private final float mSearchBeam;
    private final float mSearchGarbage;
    private final float mSearchNota;
    private final float mShortTermMs;
    private final boolean mUdt;
    private final float mVerificationThreshold;
    private final String[] mWords;

    public class Builder {
        /* access modifiers changed from: private */
        public int mAudioSampleRate = 16000;
        /* access modifiers changed from: private */
        public float mESilenceT = Float.NaN;
        /* access modifiers changed from: private */
        public float mESpeechT = Float.NaN;
        /* access modifiers changed from: private */
        public float mEnrollThreshold = 1.0E-4f;
        /* access modifiers changed from: private */
        public String mGrammar;
        /* access modifiers changed from: private */
        public float mLongTermMs = Float.NaN;
        /* access modifiers changed from: private */
        public int mNBest = 1;
        /* access modifiers changed from: private */
        public boolean mPhraseSpot = false;
        /* access modifiers changed from: private */
        public float mPhrasespotDelay = 30.0f;
        /* access modifiers changed from: private */
        public float mPhrasespotParamAOffset = Float.NaN;
        /* access modifiers changed from: private */
        public String[] mPronuns;
        /* access modifiers changed from: private */
        public float mRecogMaxRecord = 10000.0f;
        /* access modifiers changed from: private */
        public float mRecogMinDuration = 100.0f;
        /* access modifiers changed from: private */
        public int mRecogMode = 0;
        /* access modifiers changed from: private */
        public float mRecogTSilence = 500.0f;
        /* access modifiers changed from: private */
        public float mRecogThreshold = 0.0f;
        /* access modifiers changed from: private */
        public float mSearchAny = 0.5f;
        /* access modifiers changed from: private */
        public float mSearchBeam = 100.0f;
        /* access modifiers changed from: private */
        public float mSearchGarbage = 0.5f;
        /* access modifiers changed from: private */
        public float mSearchNota = 0.5f;
        /* access modifiers changed from: private */
        public float mShortTermMs = Float.NaN;
        /* access modifiers changed from: private */
        public boolean mUdt = false;
        /* access modifiers changed from: private */
        public float mVerificationThreshold = 0.6f;
        /* access modifiers changed from: private */
        public String[] mWords;

        public RecognizerConfig build() {
            return new RecognizerConfig(this);
        }

        public void setESilenceT(float f2) {
            this.mESilenceT = f2;
        }

        public void setESpeechT(float f2) {
            this.mESpeechT = f2;
        }

        public void setEnrollThreshold(float f2) {
            this.mEnrollThreshold = f2;
        }

        public void setGrammar(String str) {
            this.mGrammar = str;
        }

        public void setLongTermMs(float f2) {
            this.mLongTermMs = f2;
        }

        public void setNBest(int i) {
            this.mNBest = i;
        }

        public void setPhraseSpot(boolean z) {
            this.mPhraseSpot = z;
        }

        public void setPhrasespotDelay(float f2) {
            this.mPhrasespotDelay = f2;
        }

        public void setPhrasespotParamAOffset(float f2) {
            this.mPhrasespotParamAOffset = f2;
        }

        public void setPronuns(String[] strArr) {
            this.mPronuns = strArr;
        }

        public void setRecogMaxRecord(float f2) {
            this.mRecogMaxRecord = f2;
        }

        public void setRecogMinDuration(float f2) {
            this.mRecogMinDuration = f2;
        }

        public void setRecogMode(int i) {
            this.mRecogMode = i;
        }

        public void setRecogTSilence(float f2) {
            this.mRecogTSilence = f2;
        }

        public void setRecogThreshold(float f2) {
            this.mRecogThreshold = f2;
        }

        public void setSampleRate(int i) {
            this.mAudioSampleRate = i;
        }

        public void setSearchAny(float f2) {
            this.mSearchAny = f2;
        }

        public void setSearchBeam(float f2) {
            this.mSearchBeam = f2;
        }

        public void setSearchGarbage(float f2) {
            this.mSearchGarbage = f2;
        }

        public void setSearchNota(float f2) {
            this.mSearchNota = f2;
        }

        public void setShortTermMs(float f2) {
            this.mShortTermMs = f2;
        }

        public void setUdt(boolean z) {
            this.mUdt = z;
        }

        public void setVerificationThreshold(float f2) {
            this.mVerificationThreshold = f2;
        }

        public void setWords(String[] strArr) {
            this.mWords = strArr;
        }
    }

    private RecognizerConfig(Builder builder) {
        this.mRecogLSilence = 20000.0f;
        this.mPhrasespotBeam = 50.0f;
        this.mPhrasespotAbsBeam = 100.0f;
        this.mNumEnroll = 3;
        this.mNumTrainingSample = 1024;
        this.mGrammar = builder.mGrammar;
        this.mWords = builder.mWords;
        this.mPronuns = builder.mPronuns;
        this.mPhraseSpot = builder.mPhraseSpot;
        this.mUdt = builder.mUdt;
        this.mNBest = builder.mNBest;
        this.mRecogMode = builder.mRecogMode;
        this.mAudioSampleRate = builder.mAudioSampleRate;
        this.mRecogThreshold = builder.mRecogThreshold;
        this.mEnrollThreshold = builder.mEnrollThreshold;
        this.mVerificationThreshold = builder.mVerificationThreshold;
        this.mPhrasespotParamAOffset = builder.mPhrasespotParamAOffset;
        this.mRecogMinDuration = builder.mRecogMinDuration;
        this.mRecogTSilence = builder.mRecogTSilence;
        this.mRecogMaxRecord = builder.mRecogMaxRecord;
        this.mPhrasespotDelay = builder.mPhrasespotDelay;
        this.mSearchBeam = builder.mSearchBeam;
        this.mSearchGarbage = builder.mSearchGarbage;
        this.mSearchAny = builder.mSearchAny;
        this.mSearchNota = builder.mSearchNota;
        this.mESilenceT = builder.mESilenceT;
        this.mESpeechT = builder.mESpeechT;
        this.mShortTermMs = builder.mShortTermMs;
        this.mLongTermMs = builder.mLongTermMs;
    }

    public int getAudioSampleRate() {
        return this.mAudioSampleRate;
    }

    public float getESilenceT() {
        return this.mESilenceT;
    }

    public float getESpeechT() {
        return this.mESpeechT;
    }

    public float getEnrollThreshold() {
        return this.mEnrollThreshold;
    }

    public String getGrammar() {
        return this.mGrammar;
    }

    public float getLongTermMs() {
        return this.mLongTermMs;
    }

    public int getNBest() {
        return this.mNBest;
    }

    public int getNumEnroll() {
        return 3;
    }

    public int getNumTrainingSample() {
        return 1024;
    }

    public float getPhrasespotAbsBeam() {
        return 100.0f;
    }

    public float getPhrasespotBeam() {
        return 50.0f;
    }

    public float getPhrasespotDelay() {
        return this.mPhrasespotDelay;
    }

    public float getPhrasespotParamAOffset() {
        return this.mPhrasespotParamAOffset;
    }

    public String[] getPronuns() {
        return this.mPronuns;
    }

    public float getRecogLSilence() {
        return 20000.0f;
    }

    public float getRecogMaxRecord() {
        return this.mRecogMaxRecord;
    }

    public float getRecogMinDuration() {
        return this.mRecogMinDuration;
    }

    public int getRecogMode() {
        return this.mRecogMode;
    }

    public float getRecogTSilence() {
        return this.mRecogTSilence;
    }

    public float getRecogThreshold() {
        return this.mRecogThreshold;
    }

    public float getSearchAny() {
        return this.mSearchAny;
    }

    public float getSearchBeam() {
        return this.mSearchBeam;
    }

    public float getSearchGarbage() {
        return this.mSearchGarbage;
    }

    public float getSearchNota() {
        return this.mSearchNota;
    }

    public float getShortTermMs() {
        return this.mShortTermMs;
    }

    public float getVerificationThreshold() {
        return this.mVerificationThreshold;
    }

    public String[] getWords() {
        return this.mWords;
    }

    public boolean isPhraseSpot() {
        return this.mPhraseSpot;
    }

    public boolean isUdt() {
        return this.mUdt;
    }
}
