package com.nuance.dragon.toolkit.audio.pipes;

import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioEnergyListener;
import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.LinkedList;

public class AudioEnergyCalculatorPipe extends BufferingPipe {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final AudioEnergyListener f1138a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final LinkedList f1139b = new LinkedList();
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final Handler f1140c = new Handler();

    public AudioEnergyCalculatorPipe(AudioEnergyListener audioEnergyListener) {
        d.a("AudioEnergyListener", (Object) audioEnergyListener);
        this.f1138a = audioEnergyListener;
    }

    /* access modifiers changed from: protected */
    public void onChunkBuffered(AudioChunk audioChunk) {
        boolean z = this.f1139b.size() == 0;
        d.a("big AudioChunk", (Object) audioChunk);
        d.a("big audio chunk's duration", "greater than 20ms", audioChunk.audioDuration > 20);
        int i = audioChunk.audioDuration / 20;
        if (audioChunk.audioDuration % 20 != 0) {
            Logger.warn(this, "the given big Chunk's duration is not the multipler of 20ms we may miss some audio in the end for calculating the audio energy level.");
        }
        AudioChunk[] audioChunkArr = new AudioChunk[i];
        int length = audioChunk.audioShorts.length / i;
        for (int i2 = 0; i2 < i; i2++) {
            short[] sArr = new short[length];
            System.arraycopy(audioChunk.audioShorts, i2 * length, sArr, 0, length);
            long j = 0;
            if (audioChunk.audioTimestamp > 0) {
                j = audioChunk.audioTimestamp + (((long) audioChunk.audioDuration) / ((long) i));
            }
            audioChunkArr[i2] = new AudioChunk(audioChunk.audioType, sArr, j);
        }
        for (AudioChunk add : audioChunkArr) {
            this.f1139b.add(add);
        }
        if (z) {
            this.f1140c.post(new Runnable() {
                public final void run() {
                    if (AudioEnergyCalculatorPipe.this.f1139b.size() > 0) {
                        AudioChunk audioChunk = (AudioChunk) AudioEnergyCalculatorPipe.this.f1139b.remove();
                        AudioEnergyCalculatorPipe.this.f1138a.onEnergyLevelAvaiable(audioChunk.getAudioEnergyLevel());
                        if (AudioEnergyCalculatorPipe.this.f1139b.size() > 0) {
                            AudioEnergyCalculatorPipe.this.f1140c.postDelayed(this, (long) audioChunk.audioDuration);
                        }
                    }
                }
            });
        }
        super.onChunkBuffered(audioChunk);
    }

    /* access modifiers changed from: protected */
    public void sourceClosed(AudioSource audioSource, AudioSink audioSink) {
        this.f1139b.clear();
        super.sourceClosed(audioSource, audioSink);
    }
}
