package com.motorola.audiomonitor.pluginservice;

import android.os.RemoteException;
import android.util.Log;

public class a {

    /* renamed from: a reason: collision with root package name */
    private static final String f533a = ("AMonitor." + a.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private g.a f534b;

    /* renamed from: c reason: collision with root package name */
    private final String f535c;

    public a(String str) {
        this.f535c = str;
    }

    public String a() {
        return this.f535c;
    }

    public synchronized void a(g.a aVar) {
        this.f534b = aVar;
    }

    public synchronized void a(String str) {
        if (this.f534b != null) {
            try {
                this.f534b.a(str);
            } catch (RemoteException e2) {
                Log.e(f533a, "Failure on infoNotify");
            }
        }
        return;
    }
}
