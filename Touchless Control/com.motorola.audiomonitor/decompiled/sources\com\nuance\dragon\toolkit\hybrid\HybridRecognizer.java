package com.nuance.dragon.toolkit.hybrid;

import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.SpeechDetectionListener;
import com.nuance.dragon.toolkit.cloudservices.CloudServices;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer;
import com.nuance.dragon.toolkit.grammar.Scenario;
import com.nuance.dragon.toolkit.recognition.InterpretedRecognitionCombiner;
import com.nuance.dragon.toolkit.recognition.RecognitionInterpreter;

public abstract class HybridRecognizer {

    public interface Listener extends SpeechDetectionListener {
        void onError(HybridRecognitionError hybridRecognitionError);

        void onResult(HybridRecognitionResult hybridRecognitionResult);
    }

    public static HybridRecognizer createHybridRecognizer(ElvisRecognizer elvisRecognizer, CloudServices cloudServices, RecognitionInterpreter recognitionInterpreter, RecognitionInterpreter recognitionInterpreter2, InterpretedRecognitionCombiner interpretedRecognitionCombiner) {
        return new a(elvisRecognizer, cloudServices, recognitionInterpreter, recognitionInterpreter2, interpretedRecognitionCombiner);
    }

    public abstract void cancel();

    public abstract void startRecognition(AudioSource audioSource, Scenario scenario, Listener listener);

    public abstract void stopRecognition();
}
