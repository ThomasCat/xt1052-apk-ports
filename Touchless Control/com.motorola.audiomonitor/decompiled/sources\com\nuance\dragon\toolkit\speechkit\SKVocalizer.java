package com.nuance.dragon.toolkit.speechkit;

import com.nuance.dragon.toolkit.audio.sinks.PlayerSink;
import com.nuance.dragon.toolkit.audio.sinks.SpeakerPlayerSink;
import com.nuance.dragon.toolkit.util.internal.d;
import com.nuance.dragon.toolkit.vocalizer.TtsAudioSource;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.TtsListener;
import java.util.Iterator;
import java.util.LinkedList;

public class SKVocalizer {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final Vocalizer f2086a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Listener f2087b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final LinkedList f2088c = new LinkedList();

    /* renamed from: d reason: collision with root package name */
    private PlayerSink f2089d;

    public interface Listener {
        void onSpeakingBegin(SKVocalizer sKVocalizer, Object obj);

        void onSpeakingDone(SKVocalizer sKVocalizer, boolean z, boolean z2, Object obj);
    }

    final class a {

        /* renamed from: a reason: collision with root package name */
        final String f2095a;

        /* renamed from: b reason: collision with root package name */
        final Object f2096b;

        public a(String str, Object obj) {
            this.f2095a = str;
            this.f2096b = obj;
        }
    }

    SKVocalizer(Vocalizer vocalizer, Listener listener) {
        this.f2086a = vocalizer;
        this.f2087b = listener;
    }

    /* access modifiers changed from: private */
    public void a() {
        if (this.f2086a != null) {
            this.f2086a.cancelTts();
        }
        if (this.f2089d != null) {
            this.f2089d.disconnectAudioSource();
            this.f2089d.stopPlaying();
            this.f2089d = null;
        }
        this.f2088c.clear();
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!this.f2088c.isEmpty()) {
            final Vocalizer vocalizer = this.f2086a;
            final a aVar = (a) this.f2088c.remove();
            TtsAudioSource generateTts = this.f2086a.generateTts(aVar.f2095a, new TtsListener() {
                public final void ttsGenerationFinished(String str, Object obj, Vocalizer vocalizer, boolean z) {
                    if (SKVocalizer.this.f2086a == vocalizer && !z) {
                        SKVocalizer.this.a();
                        if (SKVocalizer.this.f2087b != null) {
                            SKVocalizer.this.f2087b.onSpeakingDone(SKVocalizer.this, false, true, aVar.f2096b);
                            Iterator it = SKVocalizer.this.f2088c.iterator();
                            while (it.hasNext()) {
                                SKVocalizer.this.f2087b.onSpeakingDone(SKVocalizer.this, false, true, ((a) it.next()).f2096b);
                            }
                        }
                        SKVocalizer.this.f2088c.clear();
                    }
                }

                public final void ttsGenerationStarted(String str, Object obj, Vocalizer vocalizer) {
                    if (SKVocalizer.this.f2086a != vocalizer) {
                    }
                }

                public final void ttsStreamingFinished(String str, Object obj, Vocalizer vocalizer) {
                    if (SKVocalizer.this.f2086a != vocalizer) {
                    }
                }

                public final void ttsStreamingStarted(String str, Object obj, Vocalizer vocalizer) {
                    if (SKVocalizer.this.f2086a != vocalizer) {
                    }
                }
            }, aVar.f2096b);
            this.f2089d = new SpeakerPlayerSink(generateTts.getAudioType());
            this.f2089d.connectAudioSource(generateTts);
            this.f2089d.startPlaying(new com.nuance.dragon.toolkit.audio.sinks.PlayerSink.Listener() {
                public final void onStarted(PlayerSink playerSink) {
                    if (vocalizer == SKVocalizer.this.f2086a && SKVocalizer.this.f2087b != null) {
                        SKVocalizer.this.f2087b.onSpeakingBegin(SKVocalizer.this, aVar.f2096b);
                    }
                }

                public final void onStopped(PlayerSink playerSink) {
                    if (vocalizer == SKVocalizer.this.f2086a) {
                        SKVocalizer.this.a();
                        if (SKVocalizer.this.f2087b != null) {
                            SKVocalizer.this.f2087b.onSpeakingDone(SKVocalizer.this, !SKVocalizer.this.f2088c.isEmpty(), false, aVar.f2096b);
                        }
                        SKVocalizer.this.b();
                    }
                }
            });
        }
    }

    public void cancel() {
        a();
    }

    public void speakString(String str, Object obj) {
        d.a("text", (Object) str);
        this.f2088c.add(new a(str, obj));
        b();
    }
}
