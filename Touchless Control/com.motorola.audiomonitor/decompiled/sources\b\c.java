package b;

public class c {

    /* renamed from: a reason: collision with root package name */
    private final String f92a;

    /* renamed from: b reason: collision with root package name */
    private final int f93b;

    /* renamed from: c reason: collision with root package name */
    private final int f94c;

    public c(String str, int i, int i2) {
        this.f92a = str;
        this.f93b = i;
        this.f94c = i2;
    }

    public String a() {
        return this.f92a;
    }

    public int b() {
        return this.f93b;
    }

    public int c() {
        return this.f94c;
    }

    public String toString() {
        return "[" + this.f94c + "]" + this.f92a + (this.f93b != -1 ? "(" + this.f93b + ")" : "");
    }
}
