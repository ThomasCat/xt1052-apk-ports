package com.nuance.dragon.toolkit.grammar;

import java.util.Iterator;

public abstract class WordList {

    public interface WordIterator extends Iterator {
        void acceptChanges();

        void discardChanges();

        int getChecksum();

        int getCount();
    }

    public abstract boolean fullUpdateRequired();

    public abstract int getAcceptedChecksum();

    public abstract WordIterator getFullIterator();

    public abstract WordIterator getModifiedIterator();
}
