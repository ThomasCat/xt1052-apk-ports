package com.nuance.a.a.a.c.a;

import android.os.Build;
import android.os.Build.VERSION;

public final class a {

    /* renamed from: a reason: collision with root package name */
    public static final int f907a = Integer.parseInt(VERSION.SDK);

    /* renamed from: b reason: collision with root package name */
    public static final String f908b;

    /* renamed from: c reason: collision with root package name */
    public static final boolean f909c;

    /* renamed from: d reason: collision with root package name */
    public static final boolean f910d;

    /* renamed from: e reason: collision with root package name */
    public static final boolean f911e;

    /* renamed from: f reason: collision with root package name */
    public static final boolean f912f = (f909c || f911e);

    /* renamed from: g reason: collision with root package name */
    public static final boolean f913g;

    /* renamed from: h reason: collision with root package name */
    public static final boolean f914h = k;
    public static final boolean i;
    private static final boolean j;
    private static final boolean k;

    static {
        String str;
        boolean z = true;
        try {
            str = (String) Build.class.getField("MANUFACTURER").get(null);
        } catch (Exception e2) {
            str = "";
        }
        f908b = str;
        String str2 = Build.MODEL;
        f909c = str.equalsIgnoreCase("samsung") && (str2.equalsIgnoreCase("SGH-T959") || str2.equalsIgnoreCase("SAMSUNG-SGH-I897") || str2.equalsIgnoreCase("SGH-I897") || str2.equalsIgnoreCase("GT-I9000"));
        f910d = str.equalsIgnoreCase("htc") && (str2.equalsIgnoreCase("PC36100") || str2.equalsIgnoreCase("ADR6300") || str2.equalsIgnoreCase("HTC Glacier") || str2.equalsIgnoreCase("T-Mobile myTouch 4G") || str2.equalsIgnoreCase("T-Mobile G2"));
        f911e = str2.equalsIgnoreCase("Motorola_i1");
        k = str.equalsIgnoreCase("Motorola") && str2.equalsIgnoreCase("MB860");
        boolean z2 = str.equalsIgnoreCase("samsung") && f907a == 8;
        j = z2;
        i = z2;
        if (f909c) {
            z = false;
        }
        f913g = z;
    }
}
