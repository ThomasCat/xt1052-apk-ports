package a;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;

public abstract class d extends HandlerThread {

    /* renamed from: a reason: collision with root package name */
    protected Handler f22a;

    /* renamed from: b reason: collision with root package name */
    protected Context f23b;

    /* renamed from: c reason: collision with root package name */
    private f f24c;

    protected d(Context context, String str) {
        super(str);
        this.f23b = context;
    }

    public void a() {
        this.f24c = null;
        quit();
    }

    public abstract void a(e eVar);

    public void a(f fVar) {
        this.f24c = fVar;
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.f24c != null) {
            this.f24c.a();
        }
    }

    /* access modifiers changed from: protected */
    public void onLooperPrepared() {
        this.f22a = new Handler(getLooper());
    }
}
