package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.ArrayList;
import java.util.List;

public final class Transaction {

    /* renamed from: a reason: collision with root package name */
    private boolean f1369a;

    /* renamed from: b reason: collision with root package name */
    private boolean f1370b;

    /* renamed from: c reason: collision with root package name */
    private final List f1371c;

    /* renamed from: d reason: collision with root package name */
    private final String f1372d;

    /* renamed from: e reason: collision with root package name */
    private final Dictionary f1373e;

    /* renamed from: f reason: collision with root package name */
    private final Listener f1374f;

    /* renamed from: g reason: collision with root package name */
    private final int f1375g;

    /* renamed from: h reason: collision with root package name */
    private a f1376h;
    private f i;
    private int j;
    private String k;

    public final class DefaultPriorities {
        public static final int DATA_UPLOAD = 1;
        public static final int FIRST_DATA_UPLOAD = 10;
        public static final int HIGH = 10;
        public static final int LOW = 0;
        public static final int MEDIUM = 5;
        public static final int RECOGNITION = 6;
        public static final int TEXT_TO_SPEECH = 5;
    }

    public interface Listener {
        void onTransactionError(Transaction transaction, TransactionError transactionError);

        void onTransactionResult(Transaction transaction, TransactionResult transactionResult, boolean z);

        void onTransactionStarted(Transaction transaction);
    }

    interface a {
        void a(Transaction transaction);

        void b(Transaction transaction);
    }

    public Transaction(String str, Dictionary dictionary, Listener listener, int i2) {
        this(str, dictionary, listener, i2, true);
    }

    public Transaction(String str, Dictionary dictionary, Listener listener, int i2, boolean z) {
        d.a("commandName", str);
        d.a("listener", (Object) listener);
        d.a("timeout", "equal or greater than 0", i2 >= 0);
        this.f1371c = new ArrayList();
        this.f1372d = str;
        this.f1373e = dictionary;
        this.f1374f = listener;
        this.f1375g = i2;
        this.i = new h(this);
        this.j = 0;
        this.f1369a = z;
    }

    /* access modifiers changed from: 0000 */
    public final String a() {
        return this.f1372d;
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i2) {
        this.j = i2;
    }

    /* access modifiers changed from: 0000 */
    public final void a(a aVar) {
        this.f1376h = aVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(TransactionResult transactionResult, boolean z) {
        this.f1374f.onTransactionResult(this, transactionResult, z);
    }

    /* access modifiers changed from: 0000 */
    public final void a(d dVar) {
        this.i.a(dVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(f fVar) {
        this.i.b();
        this.i = fVar;
        this.i.a();
    }

    /* access modifiers changed from: 0000 */
    public final void a(String str) {
        this.k = str;
    }

    public final void addParam(Param param) {
        this.f1371c.add(param);
        this.i.a(param);
    }

    /* access modifiers changed from: 0000 */
    public final Dictionary b() {
        return this.f1373e;
    }

    /* access modifiers changed from: 0000 */
    public final int c() {
        return this.f1375g;
    }

    public final void cancel() {
        this.i.e();
    }

    /* access modifiers changed from: 0000 */
    public final int d() {
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    public final List e() {
        return this.f1371c;
    }

    public final void enable() {
        if (!this.f1369a) {
            this.f1369a = true;
            if (this.f1376h != null) {
                this.f1376h.a(this);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final Listener f() {
        return this.f1374f;
    }

    public final void finish() {
        this.f1370b = true;
        this.i.f();
    }

    /* access modifiers changed from: 0000 */
    public final a g() {
        return this.f1376h;
    }

    /* access modifiers changed from: 0000 */
    public final f h() {
        return this.i;
    }

    /* access modifiers changed from: 0000 */
    public final String i() {
        return this.k;
    }

    public final boolean isActive() {
        return this.i.c();
    }

    public final boolean isComplete() {
        return this.i.d();
    }

    public final boolean isFinished() {
        return this.f1370b;
    }

    /* access modifiers changed from: 0000 */
    public final boolean j() {
        return this.f1369a;
    }
}
