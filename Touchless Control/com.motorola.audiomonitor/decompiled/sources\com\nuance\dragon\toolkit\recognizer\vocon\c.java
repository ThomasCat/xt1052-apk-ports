package com.nuance.dragon.toolkit.recognizer.vocon;

import android.os.Handler;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.util.WorkerThread;
import com.nuance.dragon.toolkit.util.internal.d;

final class c extends VoconRecognizerBase {

    /* renamed from: b reason: collision with root package name */
    private static WorkerThread f2035b;

    /* renamed from: c reason: collision with root package name */
    private static int f2036c;

    /* renamed from: a reason: collision with root package name */
    private final boolean f2037a;

    /* renamed from: d reason: collision with root package name */
    private boolean f2038d;

    public c(FileManager fileManager) {
        this(fileManager, null);
    }

    public c(FileManager fileManager, Handler handler) {
        super(new NativeVoconImpl(fileManager), handler);
        d.a("fileManager", (Object) fileManager);
        this.f2037a = handler == null;
    }

    /* access modifiers changed from: protected */
    public final Handler getWorkerThreadHandler() {
        if (f2035b == null) {
            WorkerThread workerThread = new WorkerThread();
            f2035b = workerThread;
            workerThread.start();
        }
        f2036c++;
        return f2035b.getHandler();
    }

    public final void release() {
        if (!this.f2038d) {
            this.f2038d = true;
            super.release();
            if (this.f2037a) {
                int i = f2036c - 1;
                f2036c = i;
                if (i == 0) {
                    f2035b.stop();
                    f2035b = null;
                }
            }
        }
    }
}
