package com.nuance.dragon.toolkit.audio.bluetooth.impl;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Handler;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class BluetoothHeadset {
    public static final String ACTION_AUDIO_STATE_CHANGED;
    public static final String ACTION_STATE_CHANGED;
    public static final int AUDIO_STATE_CONNECTED;
    public static final int AUDIO_STATE_DISCONNECTED;
    public static final String EXTRA_AUDIO_STATE;
    public static final String EXTRA_STATE;
    public static final int STATE_CONNECTED;
    public static final int STATE_CONNECTING;
    public static final int STATE_DISCONNECTED;
    public static final int STATE_ERROR;

    /* renamed from: b reason: collision with root package name */
    private static final Constructor f1019b;

    /* renamed from: c reason: collision with root package name */
    private static final Method f1020c;

    /* renamed from: d reason: collision with root package name */
    private static final Method f1021d;

    /* renamed from: e reason: collision with root package name */
    private static final Method f1022e;

    /* renamed from: f reason: collision with root package name */
    private static final Method f1023f;

    /* renamed from: g reason: collision with root package name */
    private static final Method f1024g;

    /* renamed from: h reason: collision with root package name */
    private static final Method f1025h;
    private static final Method i;
    private static final Class j;
    private static final boolean k;

    /* renamed from: a reason: collision with root package name */
    Handler f1026a = new Handler();
    private Object l;
    /* access modifiers changed from: private */
    public ServiceListener m;
    /* access modifiers changed from: private */
    public Runnable n = new Runnable() {
        public final void run() {
            BluetoothHeadset.this.m.onServiceConnected();
        }
    };
    /* access modifiers changed from: private */
    public Runnable o = new Runnable() {
        public final void run() {
            BluetoothHeadset.this.m.onServiceDisconnected();
        }
    };

    public interface ServiceListener {
        void onServiceConnected();

        void onServiceDisconnected();
    }

    static {
        Method method;
        boolean z;
        Reflection reflection = new Reflection();
        Class classForName = reflection.getClassForName("android.bluetooth.BluetoothHeadset");
        Class classForName2 = reflection.getClassForName("android.bluetooth.BluetoothHeadset$ServiceListener");
        f1019b = reflection.getConstructor(classForName, Context.class, classForName2);
        Method methodOrNull = reflection.getMethodOrNull(classForName, "getState", new Class[0]);
        if (methodOrNull == null) {
            method = reflection.getMethod(classForName, "getState", BluetoothDevice.class);
            z = true;
        } else {
            method = methodOrNull;
            z = false;
        }
        f1020c = method;
        k = z;
        f1023f = reflection.getMethod(classForName, "getCurrentHeadset", new Class[0]);
        f1025h = reflection.getMethod(classForName, "connectHeadset", BluetoothDevice.class);
        Method methodOrNull2 = reflection.getMethodOrNull(classForName, "disconnectHeadset", new Class[0]);
        if (methodOrNull2 == null) {
            methodOrNull2 = reflection.getMethod(classForName, "disconnectHeadset", BluetoothDevice.class);
        }
        f1024g = methodOrNull2;
        f1021d = reflection.getMethod(classForName, "startVoiceRecognition", new Class[0]);
        f1022e = reflection.getMethod(classForName, "stopVoiceRecognition", new Class[0]);
        i = reflection.getMethod(classForName, "close", new Class[0]);
        j = classForName2;
        ACTION_STATE_CHANGED = (String) reflection.getFieldValue(classForName, "ACTION_STATE_CHANGED");
        ACTION_AUDIO_STATE_CHANGED = (String) reflection.getFieldValue(classForName, "ACTION_AUDIO_STATE_CHANGED");
        EXTRA_STATE = (String) reflection.getFieldValue(classForName, "EXTRA_STATE");
        EXTRA_AUDIO_STATE = (String) reflection.getFieldValue(classForName, "EXTRA_AUDIO_STATE");
        STATE_ERROR = ((Integer) reflection.getFieldValue(classForName, "STATE_ERROR")).intValue();
        STATE_DISCONNECTED = ((Integer) reflection.getFieldValue(classForName, "STATE_DISCONNECTED")).intValue();
        STATE_CONNECTING = ((Integer) reflection.getFieldValue(classForName, "STATE_CONNECTING")).intValue();
        STATE_CONNECTED = ((Integer) reflection.getFieldValue(classForName, "STATE_CONNECTED")).intValue();
        AUDIO_STATE_DISCONNECTED = ((Integer) reflection.getFieldValue(classForName, "AUDIO_STATE_DISCONNECTED")).intValue();
        AUDIO_STATE_CONNECTED = ((Integer) reflection.getFieldValue(classForName, "AUDIO_STATE_CONNECTED")).intValue();
    }

    public BluetoothHeadset(Context context, ServiceListener serviceListener) {
        this.m = serviceListener;
        Object newProxyInstance = Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{j}, new InvocationHandler() {
            public final Object invoke(Object obj, Method method, Object[] objArr) {
                if (method.getName().equals("onServiceConnected")) {
                    BluetoothHeadset.this.f1026a.post(BluetoothHeadset.this.n);
                } else if (method.getName().equals("onServiceDisconnected")) {
                    BluetoothHeadset.this.f1026a.post(BluetoothHeadset.this.o);
                }
                return null;
            }
        });
        try {
            this.l = f1019b.newInstance(new Object[]{context, newProxyInstance});
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void close() {
        try {
            i.invoke(this.l, new Object[0]);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean connectHeadset(BluetoothDevice bluetoothDevice) {
        try {
            if (f1025h != null) {
                return ((Boolean) f1025h.invoke(this.l, new Object[]{bluetoothDevice})).booleanValue();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return false;
    }

    public void disconnectHeadset() {
        try {
            if (f1024g != null) {
                f1024g.invoke(this.l, new Object[0]);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void disconnectHeadset(BluetoothDevice bluetoothDevice) {
        try {
            if (f1024g != null) {
                f1024g.invoke(this.l, new Object[]{bluetoothDevice});
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public BluetoothDevice getConnectedDevice() {
        try {
            if (f1023f != null) {
                return (BluetoothDevice) f1023f.invoke(this.l, new Object[0]);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public int getState() {
        try {
            if (!k) {
                return ((Integer) f1020c.invoke(this.l, new Object[0])).intValue();
            }
            Object invoke = f1023f.invoke(this.l, new Object[0]);
            return ((Integer) f1020c.invoke(this.l, new Object[]{invoke})).intValue();
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public boolean startVoiceRecognition() {
        try {
            return ((Boolean) f1021d.invoke(this.l, new Object[0])).booleanValue();
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public boolean stopVoiceRecognition() {
        try {
            return ((Boolean) f1022e.invoke(this.l, new Object[0])).booleanValue();
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }
}
