package h;

import android.util.Log;
import com.motorola.audiomonitor.ProtectedByteBuffer;
import com.motorola.audiomonitor.a;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class o {

    /* renamed from: a reason: collision with root package name */
    private static final String f2316a = ("AMonitor." + o.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f2317b = a.f160b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final Date f2318c = new Date();
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public final SimpleDateFormat f2319d = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss.SSSZ", Locale.getDefault());
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public final List f2320e = new ArrayList();

    private int a(List list) {
        int i = 0;
        Iterator it = list.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = ((ProtectedByteBuffer) it.next()).capacity() + i2;
        }
    }

    private ByteBuffer a(int i, int i2) {
        ByteBuffer allocate = ByteBuffer.allocate(44);
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        allocate.put("RIFF".getBytes());
        allocate.putInt(i + 36);
        allocate.put("WAVE".getBytes());
        allocate.put("fmt ".getBytes());
        allocate.putInt(16);
        allocate.putShort(1);
        allocate.putShort(1);
        allocate.putInt(i2);
        allocate.putInt(i2 * 2);
        allocate.putShort(2);
        allocate.putShort(16);
        allocate.put("data".getBytes());
        allocate.putInt(i);
        return allocate;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0068 A[SYNTHETIC, Splitter:B:16:0x0068] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x006f  */
    public void a(r rVar) {
        FileOutputStream fileOutputStream;
        File file = new File(rVar.f2332c);
        if (!file.exists()) {
            file.mkdir();
        }
        if (file.exists()) {
            ByteBuffer a2 = a(a(rVar.f2333d), rVar.f2334e);
            try {
                fileOutputStream = new FileOutputStream(rVar.f2335f);
                try {
                    fileOutputStream.write(a2.array());
                    for (ProtectedByteBuffer writeTo : rVar.f2333d) {
                        writeTo.writeTo(fileOutputStream);
                    }
                    fileOutputStream.flush();
                } catch (FileNotFoundException e2) {
                    e = e2;
                } catch (IOException e3) {
                    e = e3;
                    Log.e(f2316a, "Error writing wave. " + e.toString());
                    if (fileOutputStream != null) {
                    }
                    if (f2317b) {
                    }
                    File file2 = new File(rVar.f2335f);
                    file2.setReadable(true, false);
                    file2.setWritable(true, false);
                }
            } catch (FileNotFoundException e4) {
                e = e4;
                fileOutputStream = null;
                Log.e(f2316a, "Error writing wave. " + e.toString());
                if (fileOutputStream != null) {
                }
                if (f2317b) {
                }
                File file22 = new File(rVar.f2335f);
                file22.setReadable(true, false);
                file22.setWritable(true, false);
            } catch (IOException e5) {
                e = e5;
                fileOutputStream = null;
                Log.e(f2316a, "Error writing wave. " + e.toString());
                if (fileOutputStream != null) {
                }
                if (f2317b) {
                }
                File file222 = new File(rVar.f2335f);
                file222.setReadable(true, false);
                file222.setWritable(true, false);
            }
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (Exception e6) {
                    Log.e(f2316a, "Error closing Output Stream", e6);
                }
            }
            if (f2317b) {
                Log.d(f2316a, "Writing " + rVar.f2335f);
            }
            File file2222 = new File(rVar.f2335f);
            file2222.setReadable(true, false);
            file2222.setWritable(true, false);
        }
    }

    public String a(q qVar, String str, List list, int i) {
        return a(qVar, str, list, i, null);
    }

    public String a(q qVar, String str, List list, int i, String str2) {
        if (a.a()) {
            return null;
        }
        r rVar = new r(this, qVar, str, list, i, str2);
        synchronized (this.f2320e) {
            this.f2320e.add(rVar);
        }
        new Thread(new p(this)).start();
        return rVar.f2335f;
    }
}
