package b;

public class e extends Exception {

    /* renamed from: a reason: collision with root package name */
    final String f97a;

    /* renamed from: b reason: collision with root package name */
    final int f98b;

    public e() {
        this("Parse error");
    }

    public e(String str) {
        this(str, 0);
    }

    public e(String str, int i) {
        this(str, i, null);
    }

    public e(String str, int i, Exception exc) {
        super(exc);
        this.f97a = str;
        this.f98b = i;
    }
}
