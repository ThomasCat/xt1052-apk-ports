package com.motorola.audiomonitor.uis;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.motorola.audiomonitor.C0014R;

public class s extends Fragment {

    /* renamed from: a reason: collision with root package name */
    private View f719a;

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.f719a = layoutInflater.inflate(C0014R.layout.welcome_confirm, null);
        return this.f719a;
    }
}
