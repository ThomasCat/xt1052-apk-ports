package com.nuance.dragon.toolkit.elvis;

import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Constraint implements JSONCompliant {
    public static final String END = "_CONSTRAINT_CLASS_END";
    public static final String START = "_CONSTRAINT_CLASS_START";

    /* renamed from: a reason: collision with root package name */
    private boolean f1521a;

    /* renamed from: b reason: collision with root package name */
    private boolean f1522b;

    /* renamed from: c reason: collision with root package name */
    private int f1523c;

    /* renamed from: d reason: collision with root package name */
    private int f1524d;

    /* renamed from: e reason: collision with root package name */
    private boolean f1525e;

    /* renamed from: f reason: collision with root package name */
    private final HashMap f1526f = new HashMap();

    /* renamed from: g reason: collision with root package name */
    private final String f1527g;

    /* renamed from: h reason: collision with root package name */
    private boolean f1528h;

    final class a {

        /* renamed from: a reason: collision with root package name */
        final String f1529a;

        /* renamed from: b reason: collision with root package name */
        final String f1530b;

        /* renamed from: c reason: collision with root package name */
        final int f1531c;

        /* renamed from: d reason: collision with root package name */
        final /* synthetic */ Constraint f1532d;

        private a(Constraint constraint, String str, String str2, int i) {
            String str3 = null;
            this.f1532d = constraint;
            if (str.length() == 0) {
                str = null;
            }
            this.f1529a = str;
            if (str2.length() != 0) {
                str3 = str2;
            }
            this.f1530b = str3;
            this.f1531c = i;
        }

        /* synthetic */ a(Constraint constraint, String str, String str2, int i, byte b2) {
            this(constraint, str, str2, i);
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.f1529a == null) {
                if (aVar.f1529a != null) {
                    return false;
                }
            } else if (!this.f1529a.equals(aVar.f1529a)) {
                return false;
            }
            if (this.f1531c != aVar.f1531c) {
                return false;
            }
            return this.f1530b == null ? aVar.f1530b == null : this.f1530b.equals(aVar.f1530b);
        }

        public final int hashCode() {
            int i = 0;
            int hashCode = ((((this.f1529a == null ? 0 : this.f1529a.hashCode()) + 31) * 31) + this.f1531c) * 31;
            if (this.f1530b != null) {
                i = this.f1530b.hashCode();
            }
            return hashCode + i;
        }
    }

    public Constraint(String str) {
        d.a("id", str);
        this.f1527g = str;
        this.f1528h = false;
        this.f1521a = false;
    }

    public static Constraint createFromJSON(JSONObject jSONObject) {
        String string = jSONObject.getString("id");
        if (string == null || string.equals("")) {
            throw new JSONException("id not defined in JSON object.");
        }
        Constraint constraint = new Constraint(string);
        JSONArray jSONArray = jSONObject.getJSONArray("from");
        JSONArray jSONArray2 = jSONObject.getJSONArray("to");
        JSONArray jSONArray3 = jSONObject.getJSONArray("pen");
        int length = jSONArray.length();
        if (length == jSONArray.length() && length == jSONArray3.length()) {
            for (int i = 0; i < length; i++) {
                constraint.addTransition(jSONArray.getString(i), jSONArray2.getString(i), jSONArray3.getInt(i));
            }
            return constraint;
        }
        throw new JSONException("The 'from', 'to', and 'penalty' arrays must have the same length.");
    }

    private void f() {
        boolean z;
        boolean z2 = false;
        int size = this.f1526f.size();
        ArrayList arrayList = new ArrayList();
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        for (a aVar : this.f1526f.values()) {
            if (aVar.f1531c != 0) {
                z5 = true;
            }
            if (aVar.f1529a.equals(START)) {
                z4 = true;
            } else if (!arrayList.contains(aVar.f1529a)) {
                arrayList.add(aVar.f1529a);
            }
            if (aVar.f1530b.equals(END)) {
                z3 = true;
            } else if (!arrayList.contains(aVar.f1530b)) {
                arrayList.add(aVar.f1530b);
            }
        }
        int size2 = arrayList.size();
        arrayList.clear();
        if (!z4) {
            Logger.error(this, "Constraint is missing transition from Constraint.START. [" + this.f1527g + "]");
            z = false;
        } else {
            z = true;
        }
        if (!z3) {
            Logger.error(this, "Constraint is missing transition to Constraint.END. [" + this.f1527g + "]");
            z = false;
        }
        if (size < 2) {
            Logger.error(this, "Constraint must contain at least 2 transitions. [" + this.f1527g + "]");
            z = false;
        }
        if (size2 <= 0) {
            Logger.error(this, "Constraint must contain at least 1 word slot. [" + this.f1527g + "]");
        } else {
            z2 = z;
        }
        this.f1524d = size;
        this.f1523c = size2;
        this.f1525e = z5;
        this.f1522b = z2;
        this.f1521a = true;
    }

    /* access modifiers changed from: 0000 */
    public final List a() {
        ArrayList arrayList = new ArrayList(this.f1526f.size());
        arrayList.addAll(this.f1526f.values());
        return arrayList;
    }

    public void addTransition(String str, String str2, int i) {
        boolean z = true;
        d.a("fromWordSlot", str);
        d.a("toWordSlot", str2);
        d.a("fromWordSlot", "not equal to Constraint.END", !str.equals(END));
        String str3 = "toWordSlot";
        String str4 = "not equal to Constraint.START";
        if (str2.equals(START)) {
            z = false;
        }
        d.a(str3, str4, z);
        this.f1526f.put(str + ":" + str2, new a(this, str, str2, i, 0));
        this.f1521a = false;
    }

    /* access modifiers changed from: 0000 */
    public final boolean b() {
        if (!this.f1521a) {
            f();
        }
        return this.f1522b;
    }

    /* access modifiers changed from: 0000 */
    public final int c() {
        if (!this.f1521a) {
            f();
        }
        return this.f1524d;
    }

    public Constraint copy() {
        Constraint constraint = new Constraint(this.f1527g);
        constraint.f1526f.putAll(this.f1526f);
        constraint.f1521a = this.f1521a;
        constraint.f1522b = this.f1522b;
        constraint.f1523c = this.f1523c;
        constraint.f1524d = this.f1524d;
        constraint.f1525e = this.f1525e;
        return constraint;
    }

    /* access modifiers changed from: 0000 */
    public final int d() {
        if (!this.f1521a) {
            f();
        }
        return this.f1523c;
    }

    /* access modifiers changed from: 0000 */
    public final boolean e() {
        if (!this.f1521a) {
            f();
        }
        return this.f1525e;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Constraint)) {
            return false;
        }
        Constraint constraint = (Constraint) obj;
        if (this.f1528h != constraint.f1528h) {
            return false;
        }
        if (this.f1523c != constraint.f1523c) {
            return false;
        }
        if (this.f1525e != constraint.f1525e) {
            return false;
        }
        if (this.f1527g == null) {
            if (constraint.f1527g != null) {
                return false;
            }
        } else if (!this.f1527g.equals(constraint.f1527g)) {
            return false;
        }
        if (this.f1522b != constraint.f1522b) {
            return false;
        }
        if (this.f1524d != constraint.f1524d) {
            return false;
        }
        if (this.f1526f == null) {
            if (constraint.f1526f != null) {
                return false;
            }
        } else if (!this.f1526f.equals(constraint.f1526f)) {
            return false;
        }
        return this.f1521a == constraint.f1521a;
    }

    public boolean getAlwaysRebuild() {
        return this.f1528h;
    }

    public String getId() {
        return this.f1527g;
    }

    public final int hashCode() {
        int i = 0;
        int i2 = 1231;
        int hashCode = ((((this.f1522b ? 1231 : 1237) + (((this.f1527g == null ? 0 : this.f1527g.hashCode()) + (((this.f1525e ? 1231 : 1237) + (((((this.f1528h ? 1231 : 1237) + 31) * 31) + this.f1523c) * 31)) * 31)) * 31)) * 31) + this.f1524d) * 31;
        if (this.f1526f != null) {
            i = this.f1526f.hashCode();
        }
        int i3 = (hashCode + i) * 31;
        if (!this.f1521a) {
            i2 = 1237;
        }
        return i3 + i2;
    }

    public void setAlwaysRebuild(boolean z) {
        this.f1528h = z;
    }

    public JSONObject toJSON() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("id", this.f1527g);
            JSONArray jSONArray = new JSONArray();
            JSONArray jSONArray2 = new JSONArray();
            JSONArray jSONArray3 = new JSONArray();
            for (a aVar : this.f1526f.values()) {
                jSONArray.put(aVar.f1529a == null ? "" : aVar.f1529a);
                jSONArray2.put(aVar.f1530b == null ? "" : aVar.f1530b);
                jSONArray3.put(aVar.f1531c);
            }
            jSONObject.put("from", jSONArray);
            jSONObject.put("to", jSONArray2);
            jSONObject.put("pen", jSONArray3);
        } catch (JSONException e2) {
            Logger.error(this, "Exception converting to JSON", e2);
        }
        return jSONObject;
    }
}
