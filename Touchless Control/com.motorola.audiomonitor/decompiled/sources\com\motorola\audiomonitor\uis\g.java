package com.motorola.audiomonitor.uis;

import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import android.view.View;
import android.view.View.OnClickListener;
import com.motorola.audiomonitor.actions.utils.a;

class g implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ a f700a;

    /* renamed from: b reason: collision with root package name */
    final /* synthetic */ f f701b;

    g(f fVar, a aVar) {
        this.f701b = fVar;
        this.f700a = aVar;
    }

    public void onClick(View view) {
        as.c(false);
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.withAppendedPath(Contacts.CONTENT_LOOKUP_URI, String.valueOf(this.f700a.b())));
        intent.setFlags(268435456);
        intent.addFlags(67108864);
        this.f701b.f699a.startActivity(intent);
        d.f694h.getActivity().finish();
    }
}
