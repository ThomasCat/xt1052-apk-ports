package com.nuance.a.a.a.b.c.b.b;

import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.b.c.c.i;

public class r extends l implements i {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f876a = a.a(r.class);

    public r(byte[] bArr) {
        super(29186, bArr);
    }

    public final int g() {
        f876a.b((Object) "PDXQueryError.getError()");
        return d("error");
    }

    public final String h() {
        f876a.b((Object) "PDXQueryError.getDescription()");
        String f2 = f("description");
        return f2 != null ? f2 : "";
    }

    public final String i() {
        f876a.b((Object) "PDXQueryError.getParameter()");
        String f2 = f("parameter");
        return f2 != null ? f2 : "";
    }
}
