package h;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import com.motorola.audiomonitor.a;
import java.lang.reflect.Method;

public class d {

    /* renamed from: a reason: collision with root package name */
    private static final String f2275a = ("AMonitor." + d.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f2276b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private static final Uri f2277c = Uri.parse("content://com.motorola.android.providers.settings/settings");

    /* renamed from: d reason: collision with root package name */
    private static d f2278d;

    /* renamed from: e reason: collision with root package name */
    private final Context f2279e;

    /* renamed from: f reason: collision with root package name */
    private Method f2280f;

    /* renamed from: g reason: collision with root package name */
    private Method f2281g;

    /* renamed from: h reason: collision with root package name */
    private Method f2282h;

    private d(Context context) {
        this.f2279e = context;
        try {
            Class cls = Class.forName("com.motorola.android.provider.MotorolaSettings");
            this.f2280f = cls.getMethod("getInt", new Class[]{ContentResolver.class, String.class, Integer.TYPE});
            this.f2281g = cls.getMethod("putInt", new Class[]{ContentResolver.class, String.class, Integer.TYPE});
            this.f2282h = cls.getMethod("getInt", new Class[]{ContentResolver.class, String.class});
        } catch (Throwable th) {
            Log.e(f2275a, "Settings class error: ", th);
        }
    }

    public static final synchronized d a(Context context) {
        d dVar;
        synchronized (d.class) {
            if (f2278d == null) {
                f2278d = new d(context);
            }
            dVar = f2278d;
        }
        return dVar;
    }

    public static void a(Context context, int i) {
        a(context).b("privacy_always_on_voice", i);
        if (f2276b) {
            Log.d(f2275a, "setAoVPrivacySetting: " + i);
        }
    }

    public static boolean b(Context context) {
        int a2 = a(context).a("privacy_always_on_voice", 0);
        if (f2276b) {
            Log.d(f2275a, "isAoVOn: " + a2);
        }
        return a2 == 1;
    }

    public int a(String str, int i) {
        try {
            if (this.f2280f == null) {
                return i;
            }
            return ((Integer) this.f2280f.invoke(null, new Object[]{this.f2279e.getContentResolver(), str, Integer.valueOf(i)})).intValue();
        } catch (Throwable th) {
            Log.e(f2275a, "getInt error: ", th);
            return i;
        }
    }

    public boolean b(String str, int i) {
        try {
            if (this.f2281g != null) {
                return ((Boolean) this.f2281g.invoke(null, new Object[]{this.f2279e.getContentResolver(), str, Integer.valueOf(i)})).booleanValue();
            }
        } catch (Throwable th) {
            Log.e(f2275a, "putInt error: ", th);
        }
        return false;
    }
}
