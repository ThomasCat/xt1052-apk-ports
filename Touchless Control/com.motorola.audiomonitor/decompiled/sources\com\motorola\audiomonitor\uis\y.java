package com.motorola.audiomonitor.uis;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.preference.CheckBoxPreference;

class y implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ u f763a;

    y(u uVar) {
        this.f763a = uVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == -1) {
            CheckBoxPreference checkBoxPreference = (CheckBoxPreference) this.f763a.findPreference("lock_bypass");
            if (checkBoxPreference != null) {
                checkBoxPreference.setChecked(true);
            }
        }
    }
}
