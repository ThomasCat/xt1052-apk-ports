package com.motorola.audiomonitor.pluginservice;

import android.os.RemoteException;
import android.util.Log;
import com.motorola.audiomonitor.ProtectedByteBuffer;
import g.a;
import g.e;

class f extends e {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ InputPluginService f544a;

    f(InputPluginService inputPluginService) {
        this.f544a = inputPluginService;
    }

    public void a() {
        synchronized (InputPluginService.class) {
            if (this.f544a.f531f != null) {
                if (InputPluginService.f527c) {
                    Log.d(InputPluginService.f526b, "Plugin unset.");
                }
                this.f544a.f();
                this.f544a.g();
            }
        }
    }

    public void a(float f2) {
        synchronized (InputPluginService.class) {
            if (InputPluginService.f529e == null || InputPluginService.f529e.f548c == null) {
                throw new RemoteException("Plugin not initialized.");
            }
            if (InputPluginService.f527c) {
                Log.d(InputPluginService.f526b, "on end-of-speech");
            }
            InputPluginService.f529e.f548c.a(f2);
        }
    }

    public void a(String str, a aVar, boolean z) {
        if (str == null || aVar == null) {
            throw new RemoteException("Invalid arguments.");
        } else if (this.f544a.f531f != null) {
            throw new RemoteException("Another plugin already connected.");
        } else {
            if (InputPluginService.f527c) {
                Log.d(InputPluginService.f526b, "New Plugin. Name = " + str);
            }
            d dVar = new d();
            b bVar = new b();
            a aVar2 = new a(str);
            c cVar = z ? null : new c();
            if (dVar == null || bVar == null || aVar2 == null) {
                throw new RemoteException("Allocation failure.");
            }
            dVar.a(aVar);
            bVar.a(aVar);
            aVar2.a(aVar);
            synchronized (InputPluginService.class) {
                this.f544a.f531f = aVar;
                this.f544a.f531f.asBinder().linkToDeath(this.f544a.f532g, 0);
                InputPluginService.f529e = new h(dVar, bVar, cVar, aVar2);
            }
            this.f544a.g();
        }
    }

    public void a(byte[] bArr) {
        synchronized (InputPluginService.class) {
            if (InputPluginService.f529e != null) {
                ProtectedByteBuffer protectedByteBuffer = new ProtectedByteBuffer(bArr, 16000);
                InputPluginService.f529e.f547b.c(protectedByteBuffer);
                if (InputPluginService.f529e.f548c != null) {
                    InputPluginService.f529e.f548c.a(protectedByteBuffer);
                }
            } else {
                throw new RemoteException("Plugin not initialized.");
            }
        }
    }

    public void b() {
        synchronized (InputPluginService.class) {
            if (InputPluginService.f529e != null) {
                if (InputPluginService.f527c) {
                    Log.d(InputPluginService.f526b, "on trigger from plugin");
                }
                InputPluginService.f529e.f546a.e();
            } else {
                throw new RemoteException("Plugin not initialized.");
            }
        }
    }
}
