package com.nuance.dragon.toolkit.audio.bluetooth;

public interface BluetoothListener {
    void onAudioConnected(int i);

    void onAudioDisconnected(int i);

    void onBluetoothInitialized(int i);

    void onBluetoothReleased(int i);
}
