package com.motorola.audiomonitor;

import a.f;
import a.u;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import com.motorola.audiomonitor.actions.utils.ab;
import com.motorola.audiomonitor.actions.utils.ae;
import com.motorola.audiomonitor.actions.utils.ak;
import com.motorola.audiomonitor.actions.utils.am;
import com.motorola.audiomonitor.actions.utils.an;
import com.motorola.audiomonitor.actions.utils.j;
import com.motorola.audiomonitor.actions.utils.m;
import com.motorola.audiomonitor.actions.utils.v;
import com.motorola.audiomonitor.pluginservice.h;
import com.motorola.audiomonitor.service.d;
import com.motorola.audiomonitor.uis.BtStubActivity;
import com.motorola.audiomonitor.uis.as;
import com.motorola.audiomonitor.uis.bb;
import e.a;
import e.e;
import h.c;
import h.g;
import h.k;
import h.o;
import h.q;

public class ba extends HandlerThread implements aa, ab, ab, bb, z {
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final String f338b = ("AMonitor." + ba.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public static final boolean f339c = a.f160b;

    /* renamed from: a reason: collision with root package name */
    c f340a;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public Context f341d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public j f342e;

    /* renamed from: f reason: collision with root package name */
    private r f343f;

    /* renamed from: g reason: collision with root package name */
    private u f344g;

    /* renamed from: h reason: collision with root package name */
    private v f345h;
    private final d i;
    private final h j;
    private s k;
    private a l;
    private ad m;
    /* access modifiers changed from: private */
    public final Handler n;
    private final Handler o = new Handler();
    /* access modifiers changed from: private */
    public as p;
    private o q;
    /* access modifiers changed from: private */
    public s r = null;
    /* access modifiers changed from: private */
    public bt s = bt.PHRASE_DETECT;
    private final l t = new bb(this);
    private final f u = new bj(this);
    private final j v = new bl(this);

    public ba(Context context, h hVar) {
        super(ba.class.getSimpleName());
        if (f339c) {
            Log.d(f338b, "RecogController : Plugin mode");
        }
        this.i = null;
        this.j = hVar;
        a(context);
        start();
        this.n = new Handler(getLooper());
    }

    public ba(Context context, d dVar) {
        super(ba.class.getSimpleName());
        if (f339c) {
            Log.d(f338b, "RecogController : HW Control");
        }
        a(context);
        this.i = dVar;
        this.j = null;
        start();
        this.n = new Handler(getLooper());
    }

    private void a(Context context) {
        this.f341d = context;
        com.motorola.audiomonitor.actions.utils.h.a(context);
        an.a(context, this.n);
        if (this.j == null || this.j.f549d == null || !TextUtils.equals(this.j.f549d.a(), "SttTestNoUi")) {
            this.p = as.a(context, this.o, this);
        } else {
            this.p = null;
        }
        this.q = new o();
    }

    /* access modifiers changed from: private */
    public void a(bt btVar) {
        switch (bi.f357a[this.s.ordinal()]) {
            case 1:
                if (btVar == bt.SPEECH_DETECT) {
                    boolean h2 = com.motorola.audiomonitor.actions.utils.h.a().h();
                    if (this.r != this.l) {
                        b.a().a(true, this.f341d);
                    }
                    this.s = btVar;
                    this.f342e.d();
                    if (this.p != null) {
                        this.f342e.a((k) this.p);
                    }
                    if (this.f343f instanceof m) {
                        this.f342e.a((k) (m) this.f343f);
                    }
                    this.f343f.a();
                    if (this.m != null) {
                        this.f342e.c(this.m);
                        this.m.c();
                        this.m.e_();
                    }
                    if (this.p != null) {
                        this.p.a(true);
                    }
                    this.f345h.b();
                    if (this.f344g != null) {
                        this.f344g.a();
                    }
                    long n2 = n();
                    if (h2 && 300 > n2) {
                        n2 = 300;
                    }
                    if (n2 > 0) {
                        this.f343f.b(n2);
                    }
                    this.f340a.a(n2 + a.f162d);
                    break;
                }
                break;
            case 2:
                if (btVar != bt.PHRASE_DETECT) {
                    if (btVar == bt.ACTION_PROGRESS) {
                        this.f340a.a();
                        this.s = btVar;
                        this.f342e.d();
                        c.d.a(this.f341d, "commands");
                        break;
                    }
                } else {
                    this.f340a.a();
                    b(false);
                    break;
                }
                break;
            case 3:
                if (btVar == bt.PHRASE_DETECT) {
                    b(true);
                    break;
                }
                break;
        }
        if (f339c) {
            Log.d(f338b, "Current: " + this.s);
        }
    }

    private void a(String str) {
        if (this.j != null) {
            this.k = this.j.f546a;
            this.j.f546a.a((aa) this);
            return;
        }
        this.k = new com.motorola.audiomonitor.service.a(this.f341d, this.i, this);
    }

    /* access modifiers changed from: private */
    public void b(float f2) {
        g.a().g();
        if (this.s != bt.SPEECH_DETECT) {
            return;
        }
        if (f2 >= 1000.0f) {
            int i2 = this.r == this.l ? 8000 : 16000;
            this.f345h.a(i2);
            this.f344g.a(this.f343f.d(), i2, f2);
            a(bt.ACTION_PROGRESS);
        } else if (f339c) {
            Log.d(f338b, "Ignoring too short audio: len=" + f2);
        }
    }

    /* access modifiers changed from: private */
    public void b(float f2, s sVar) {
        if (com.motorola.audiomonitor.actions.utils.h.a().d() || com.motorola.audiomonitor.actions.utils.h.a().e() || com.motorola.audiomonitor.actions.utils.h.a().b()) {
            b(sVar);
        } else if (sVar == this.m) {
            this.f342e.e();
        } else if (this.r != null) {
            b(sVar);
        } else {
            this.r = sVar;
            if (this.s == bt.PHRASE_DETECT) {
                a(bt.SPEECH_DETECT);
                g.a().a(f2);
                c.d.a(this.f341d, "triggers");
                return;
            }
            Log.w(f338b, "Trigger ignored");
        }
    }

    private void b(s sVar) {
        this.n.post(new bs(this, sVar));
    }

    private void b(boolean z) {
        String str;
        String str2 = null;
        this.s = bt.PHRASE_DETECT;
        this.f342e.d();
        this.f342e.e();
        o();
        if (this.m != null) {
            this.m.b();
        }
        if (this.p != null) {
            this.p.b();
        }
        if (!z) {
            ak.a(am.NO_INPUT).a();
        }
        if (this.i != null) {
            g.a().a(this.i.c(1));
            g.a().c(this.i.k());
            g.a().a(this.i.l());
        }
        if (this.m != null) {
            g.a().a(this.m.e(), this.m.f());
        }
        b.a().a(false, this.f341d);
        this.f345h.c();
        if (this.j != null || this.m == null) {
            str = null;
        } else {
            str = this.q.a(this.m.e() ? q.TYPE_GOOD_TRIGGER : q.TYPE_BAD_TRIGGER, "/data/adspd", this.m.g(), 16000, Integer.toString(this.m.f()));
        }
        if (this.j == null && this.f343f != null && this.f343f.c() && (a.q == 2 || (a.q == 1 && !z))) {
            q qVar = z ? q.TYPE_NORMAL : q.TYPE_ABORT;
            String l2 = g.a().l();
            str2 = !TextUtils.isEmpty(l2) ? this.q.a(qVar, "/data/adspd", this.f343f.d(), 16000, a.n + "_" + l2) : this.q.a(qVar, "/data/adspd", this.f343f.d(), 16000);
        }
        g.a().a(str, str2);
    }

    /* access modifiers changed from: private */
    public void c(boolean z) {
        if (!z || this.s != bt.ACTION_PROGRESS) {
            this.f342e.b((k) this.f345h);
        } else {
            this.f342e.a((k) this.f345h);
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        a(PreferenceManager.getDefaultSharedPreferences(this.f341d).getString("key_phrase", "customphrase"));
        this.f343f = m();
        a.a aVar = new a.a(this.f341d, l());
        aVar.a(this.u);
        this.f345h = new v(this.f341d, this.o, this);
        this.f344g = new u(this.f341d, aVar, this.f345h, this.j);
        m.a(this.f341d, this.o);
        this.f342e.d();
        k.a(true, this.f341d, BtStubActivity.class);
        this.l = a.a(this.f341d);
        this.l.a((aa) this);
        this.l.a((e) this.f344g);
        this.l.a((e) this.f345h);
        this.s = bt.PHRASE_DETECT;
        this.k.e_();
        this.f343f.start();
        if (a.o && this.j == null && !a.a()) {
            this.m = new ad(this.f341d, this);
        }
        k();
        com.motorola.audiomonitor.actions.utils.h.a().a(this.v);
    }

    /* access modifiers changed from: private */
    public void i() {
        this.n.postDelayed(new bo(this), 1000);
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.f340a != null) {
            this.f340a.a();
            this.f340a = null;
        }
        a.e();
        this.l = null;
        k.a(false, this.f341d, BtStubActivity.class);
        if (this.f342e != null) {
            this.f342e.d();
            this.f342e.f();
            this.f342e.c();
            this.f342e = null;
        }
        if (this.k != null) {
            this.k.b();
            this.k.d();
            this.k = null;
        }
        if (this.f343f != null) {
            this.f343f.b();
            this.f343f = null;
        }
        if (this.m != null) {
            this.m.d();
            this.m = null;
        }
        if (this.f345h != null) {
            this.f345h.e();
            this.f345h = null;
        }
        if (this.p != null) {
            this.p.b();
        }
        if (this.f344g != null) {
            this.f344g.b();
            this.f344g = null;
        }
        if (this.j != null) {
            this.j.f546a.a((aa) null);
            if (this.j.f548c != null) {
                this.j.f548c.a((z) null);
            }
        }
        try {
            m.a().c();
        } catch (AssertionError e2) {
        }
        com.motorola.audiomonitor.actions.utils.h.a().b(this.v);
        com.motorola.audiomonitor.actions.utils.f.a();
    }

    private void k() {
        this.f340a = new c(this.n, new bf(this));
    }

    private p l() {
        if (this.j != null) {
            this.f342e = this.j.f547b;
        } else {
            this.f342e = new f(true);
        }
        this.f342e.a(this.t);
        return null;
    }

    private r m() {
        if (this.j == null || this.j.f548c == null) {
            return new m(this);
        }
        this.j.f548c.a((z) this);
        return this.j.f548c;
    }

    private long n() {
        if (this.r == null || this.r != this.l) {
            this.f342e.a(a.o);
            this.f342e.a();
        } else {
            ae aeVar = new ae(this.f341d);
            aeVar.a();
            aeVar.b();
            aeVar.a((int) C0014R.string.bluetooth_triggered);
            this.f342e.a(false);
            this.f342e.a(6, 8000);
        }
        return 0;
    }

    private void o() {
        if (this.r != null && this.r == this.l) {
            g.a().p();
        }
        if (this.f342e != null && this.r != null) {
            this.f342e.b();
            this.r.c();
            this.r = null;
        }
    }

    public void a(float f2) {
        if (f339c) {
            Log.d(f338b, "onEndOfSpeech: phraseDuration=" + f2);
        }
        this.n.post(new br(this, f2));
    }

    public void a(float f2, s sVar) {
        if (f339c) {
            Log.d(f338b, "onPhraseSpotted: score=" + f2);
        }
        this.n.post(new bc(this, f2, sVar));
    }

    public void a(int i2) {
        if (f339c) {
            Log.d(f338b, "onError:" + this.s + " reason=" + i2);
        }
    }

    public void a(ac acVar) {
    }

    public void a(s sVar) {
        if (f339c) {
            Log.d(f338b, "onCancelPhraseSpot");
        }
        this.n.post(new bd(this, sVar));
    }

    public void a(boolean z) {
        this.n.post(new be(this, z));
    }

    public void b() {
        this.n.post(new bn(this));
    }

    public void c() {
        this.n.post(new bp(this));
        try {
            join();
        } catch (InterruptedException e2) {
        }
        if (this.p != null) {
            this.p.d();
        }
        this.p = null;
        this.q = null;
        com.motorola.audiomonitor.actions.utils.h.a().i();
        an.a().c();
    }

    public void d() {
        if (f339c) {
            Log.d(f338b, "Cancel by User");
        }
        this.n.post(new bg(this));
    }

    public void d_() {
        if (f339c) {
            Log.d(f338b, "BeginningOfSpeech");
        }
        this.n.post(new bq(this));
    }

    public void e() {
        if (f339c) {
            Log.d(f338b, "onFatalError");
        }
        this.n.post(new bh(this));
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        if (f339c) {
            Log.d(f338b, "finalize");
        }
    }
}
