package com.nuance.a.a.a.c.a;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Looper;
import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;

public final class d {

    /* renamed from: a reason: collision with root package name */
    public static String f928a;

    /* renamed from: b reason: collision with root package name */
    public static String f929b;

    /* renamed from: c reason: collision with root package name */
    public static int f930c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public static final C0001a f931d = a.a(b.class);

    /* renamed from: e reason: collision with root package name */
    private Context f932e;

    /* renamed from: f reason: collision with root package name */
    private boolean f933f;

    /* renamed from: g reason: collision with root package name */
    private c f934g = null;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public boolean f935h = false;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public Object j = null;
    private final h k;
    /* access modifiers changed from: private */
    public Object l = new Object();

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public d(Context context) {
        BluetoothAdapter bluetoothAdapter;
        this.f932e = context;
        this.k = new h();
        if (a.f907a <= 10) {
            this.f933f = true;
            f928a = c.f920b;
            f929b = c.f922d;
            f930c = c.j;
            AnonymousClass1 r0 = new c.a() {
                public final void a() {
                    if (d.f931d.c()) {
                        d.f931d.c((Object) "BluetoothHeadsetOEM reflected onServiceConnected()");
                    }
                    synchronized (d.this.l) {
                        if (!d.this.f935h) {
                            d.this.i = true;
                            d.this.l.notify();
                        } else {
                            d.this.d();
                        }
                    }
                }

                public final void b() {
                    if (d.f931d.c()) {
                        d.f931d.c((Object) "BluetoothHeadsetOEM reflected onServiceDisconnected()");
                    }
                    synchronized (d.this.l) {
                        d.this.d();
                        if (!d.this.f935h) {
                            d.this.i = true;
                            d.this.l.notify();
                        }
                    }
                }
            };
            synchronized (this.l) {
                long currentTimeMillis = System.currentTimeMillis();
                this.f934g = new c(this.f932e, r0);
                try {
                    this.l.wait(1000);
                } catch (InterruptedException e2) {
                    if (f931d.e()) {
                        f931d.e("BluetoothHeadsetOEM reflected mHeadsetSyncObj.wait() threw exception:" + e2);
                    }
                }
                if (!this.i) {
                    if (f931d.e()) {
                        f931d.e("BluetoothHeadsetOEM reflected service NOT connected in time. Gave up!!!");
                    }
                    this.f935h = true;
                    d();
                } else if (f931d.c()) {
                    f931d.c((Object) "BluetoothHeadsetOEM reflected service connection took " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
                }
            }
            return;
        }
        this.f933f = false;
        h hVar = this.k;
        Class a2 = h.a("android.bluetooth.BluetoothHeadset");
        h hVar2 = this.k;
        f928a = (String) h.a(a2, "ACTION_AUDIO_STATE_CHANGED");
        h hVar3 = this.k;
        f929b = (String) h.a(a2, "EXTRA_STATE");
        h hVar4 = this.k;
        f930c = ((Integer) h.a(a2, "STATE_AUDIO_CONNECTED")).intValue();
        if (a.f907a == 11 || a.f907a == 12 || a.f907a == 13 || a.f907a == 14 || a.f907a == 15) {
            final Object[] objArr = new Object[2];
            new Thread(new Runnable() {
                public final void run() {
                    Looper.prepare();
                    objArr[1] = BluetoothAdapter.getDefaultAdapter();
                    objArr[0] = new Boolean(true);
                    Looper.loop();
                }
            }).start();
            while (objArr[0] == null) {
                try {
                    Thread.sleep(10);
                } catch (Exception e3) {
                }
            }
            bluetoothAdapter = (BluetoothAdapter) objArr[1];
        } else {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }
        h hVar5 = this.k;
        Class a3 = h.a("android.bluetooth.BluetoothProfile$ServiceListener");
        Object newProxyInstance = Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{a3}, new InvocationHandler() {
            public final Object invoke(Object obj, Method method, Object[] objArr) {
                if (method.getName().equals("onServiceConnected")) {
                    if (d.f931d.c()) {
                        d.f931d.c((Object) "BluetoothHeadsetOEM native onServiceConnected()");
                    }
                    synchronized (d.this.l) {
                        if (!d.this.f935h) {
                            d.this.i = true;
                            d.this.j = objArr[1];
                            d.this.l.notify();
                        } else {
                            d.this.a(objArr[1]);
                        }
                    }
                } else if (method.getName().equals("onServiceDisconnected")) {
                    if (d.f931d.c()) {
                        d.f931d.c((Object) "BluetoothHeadsetOEM native onServiceDisconnected()");
                    }
                    synchronized (d.this.l) {
                        if (!d.this.f935h) {
                            d.this.i = true;
                            d.this.l.notify();
                        }
                    }
                }
                return null;
            }
        });
        synchronized (this.l) {
            long currentTimeMillis2 = System.currentTimeMillis();
            h hVar6 = this.k;
            Class a4 = h.a("android.bluetooth.BluetoothProfile");
            h hVar7 = this.k;
            Class a5 = h.a("android.bluetooth.BluetoothAdapter");
            h hVar8 = this.k;
            Method a6 = h.a(a5, "getProfileProxy", Context.class, a3, Integer.TYPE);
            try {
                h hVar9 = this.k;
                a6.invoke(bluetoothAdapter, new Object[]{context, newProxyInstance, h.a(a4, "HEADSET")});
            } catch (Exception e4) {
                e4.printStackTrace();
            }
            try {
                this.l.wait(1000);
            } catch (InterruptedException e5) {
                if (f931d.e()) {
                    f931d.e("BluetoothHeadsetOEM native mHeadsetSyncObj.wait() threw exception:" + e5);
                }
            }
            if (!this.i) {
                if (f931d.e()) {
                    f931d.e("BluetoothHeadsetOEM native service NOT connected in time. Gave up!!!");
                }
                this.f935h = true;
            } else if (f931d.c()) {
                f931d.c((Object) "BluetoothHeadsetOEM native service connection took " + (System.currentTimeMillis() - currentTimeMillis2) + "ms");
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(Object obj) {
        h hVar = this.k;
        Class a2 = h.a("android.bluetooth.BluetoothProfile");
        h hVar2 = this.k;
        Class a3 = h.a("android.bluetooth.BluetoothAdapter");
        h hVar3 = this.k;
        Method b2 = h.b(a3, "closeProfileProxy", Integer.TYPE, a2);
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        try {
            h hVar4 = this.k;
            b2.invoke(defaultAdapter, new Object[]{(Integer) h.a(a2, "HEADSET"), obj});
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.f934g != null) {
            this.f934g.b();
            this.f934g = null;
        }
    }

    public final BluetoothDevice a() {
        List list;
        if (this.f933f && this.f934g != null) {
            return this.f934g.a();
        }
        if (this.f933f || this.j == null) {
            return null;
        }
        h hVar = this.k;
        Class a2 = h.a("android.bluetooth.BluetoothProfile");
        h hVar2 = this.k;
        try {
            list = (List) h.b(a2, "getConnectedDevices", new Class[0]).invoke(this.j, new Object[0]);
        } catch (Exception e2) {
            e2.printStackTrace();
            list = null;
        }
        if (list == null || list.size() <= 0) {
            return null;
        }
        return (BluetoothDevice) list.get(0);
    }

    public final void b() {
        if (this.f933f) {
            d();
        } else if (this.j != null) {
            a(this.j);
            this.j = null;
        }
    }
}
