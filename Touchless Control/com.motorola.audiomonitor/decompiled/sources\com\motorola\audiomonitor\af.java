package com.motorola.audiomonitor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import h.a;
import h.d;
import h.k;
import java.util.Locale;

class af extends AsyncTask {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ MonitorApplication f294a;

    af(MonitorApplication monitorApplication) {
        this.f294a = monitorApplication;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Context... contextArr) {
        Context context = contextArr[0];
        if (context == null) {
            Log.e(MonitorApplication.f144a, "Failed to copy resources");
        } else {
            k.a(context, (int) C0014R.raw.nn_en_us_mfcc_16k_15_250_v5_1_1, "generic_acoustic.raw");
            k.a(context, (int) C0014R.raw.lts_en_us_9_5_2, "language.raw");
            k.a(context, (int) C0014R.raw.svsid_triggerogn_enus_3_1, "svsid_triggerOGN_enUS_3_1.raw");
            k.a(context, (int) C0014R.raw.ma_1_okgooglenow_en_us_adults_sfs12_delivery3_am, "ma_1_okgooglenow_en_us_adults_sfs12_delivery3_am.raw");
            k.a(context, (int) C0014R.raw.ma_1_okgooglenow_en_us_adults_sfs12_delivery3_search_7, "ma_1_okgooglenow_en_us_adults_sfs12_delivery3_search_7.raw");
            k.a(context, (int) C0014R.raw.ma_1_okgooglenow_en_us_adults_ma36_delivery3_am, "ma_1_okgooglenow_en_us_adults_ma36_delivery3_am.bin");
            k.a(context, (int) C0014R.raw.ma_1_okgooglenow_en_us_adults_ma36_delivery3_search_7, "ma_1_okgooglenow_en_us_adults_ma36_delivery3_search_7.bin");
            k.a(context, (int) C0014R.raw.phoneme_search_1_2, "phoneme_search.raw");
            k.a(context, PreferenceManager.getDefaultSharedPreferences(context));
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            if (!defaultSharedPreferences.contains("disp_off_trig")) {
                k.a(context, "disp_off_trig", a.l);
            }
            a.a(5000);
            if (MonitorApplication.f145b) {
                Log.d(MonitorApplication.f144a, "App initialization completed");
            }
            MonitorApplication.f147d.set(true);
            if (!d.b(context)) {
                k.b(context, 0);
            } else if (!defaultSharedPreferences.getBoolean("monitor_status", false) || !k.a(Locale.getDefault())) {
                k.b(context, 3);
            } else {
                this.f294a.startService(new Intent(context, MonitorService.class));
            }
            this.f294a.a(context);
        }
        return null;
    }
}
