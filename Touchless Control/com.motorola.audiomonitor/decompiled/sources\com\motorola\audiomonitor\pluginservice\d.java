package com.motorola.audiomonitor.pluginservice;

import android.os.RemoteException;
import android.util.Log;
import com.motorola.audiomonitor.aa;
import com.motorola.audiomonitor.s;
import g.a;

public class d implements s {

    /* renamed from: a reason: collision with root package name */
    private static final String f541a = ("AMonitor." + d.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private a f542b;

    /* renamed from: c reason: collision with root package name */
    private aa f543c;

    public void a(aa aaVar) {
        this.f543c = aaVar;
    }

    public synchronized void a(a aVar) {
        this.f542b = aVar;
    }

    public synchronized void b() {
        if (this.f542b != null) {
            try {
                this.f542b.b();
            } catch (RemoteException e2) {
                Log.e(f541a, e2.toString());
            }
        }
        return;
    }

    public void c() {
        e_();
    }

    public void d() {
        b();
        this.f543c = null;
    }

    public void e() {
        if (this.f543c != null) {
            this.f543c.a(0.0f, this);
        }
    }

    public synchronized void e_() {
        if (this.f542b != null) {
            try {
                this.f542b.a();
            } catch (RemoteException e2) {
                Log.e(f541a, e2.toString());
            }
        }
        return;
    }
}
