package com.nuance.a.a.a.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.telephony.TelephonyManager;
import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import java.util.UUID;

public class b {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f943a = a.a(b.class);

    /* renamed from: b reason: collision with root package name */
    private TelephonyManager f944b;

    /* renamed from: c reason: collision with root package name */
    private Context f945c = null;

    public b(Context context) {
        this.f945c = context;
        this.f944b = (TelephonyManager) context.getSystemService("phone");
    }

    public final String a() {
        SharedPreferences sharedPreferences = this.f945c.getSharedPreferences("nuance_sdk_pref", 0);
        String string = sharedPreferences.getString("NUANCE_NMSP_UID", "");
        if (string.length() == 0) {
            string = UUID.randomUUID().toString();
            if (string != null) {
                string = string.replaceAll("-", "");
            }
            Editor edit = sharedPreferences.edit();
            edit.putString("NUANCE_NMSP_UID", string);
            if (!edit.commit()) {
                f943a.e("Storing nuance sdk uid has failed");
            }
        }
        return string == null ? "" : string;
    }
}
