package com.nuance.a.a.a.b.c.b.a;

import com.motorola.audiomonitor.GenericRecognizer.UdtEnrollResult;
import com.nuance.a.a.a.a.a.a.C0000a;
import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.a.b.a.b.C0002b;
import com.nuance.a.a.a.b.a.a.C0004a;
import com.nuance.a.a.a.b.b.f;
import com.nuance.a.a.a.b.c.a.c;
import com.nuance.a.a.a.b.c.a.d;
import java.io.ByteArrayOutputStream;
import java.util.Hashtable;
import java.util.Vector;

public class b implements C0002b, C0004a {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f837a = a.a(b.class);
    private static byte[] m = new byte[16];
    private static int p = 1;
    private static String[] v = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};

    /* renamed from: b reason: collision with root package name */
    private Hashtable f838b;

    /* renamed from: c reason: collision with root package name */
    private Hashtable f839c;

    /* renamed from: d reason: collision with root package name */
    private Hashtable f840d;

    /* renamed from: e reason: collision with root package name */
    private com.nuance.a.a.a.b.a.a f841e;

    /* renamed from: f reason: collision with root package name */
    private String f842f;

    /* renamed from: g reason: collision with root package name */
    private short f843g;

    /* renamed from: h reason: collision with root package name */
    private Vector f844h;
    private com.nuance.a.a.a.a.b.a.b i;
    private c j;
    private Vector k;
    private byte[] l = null;
    private C0000a n;
    private C0000a o;
    private long q;
    private boolean r = false;
    private d s;
    private long t = 1;
    private byte u = 1;

    public b(String str, short s2, String str2, byte[] bArr, String str3, Vector vector, com.nuance.a.a.a.a.b.a.b bVar, c cVar) {
        this.f842f = str;
        this.f843g = s2;
        this.i = bVar;
        this.j = cVar;
        this.s = null;
        this.k = new Vector();
        this.f838b = new Hashtable();
        this.f839c = new Hashtable();
        this.f840d = new Hashtable();
        this.f844h = new Vector();
        this.f841e = new com.nuance.a.a.a.b.a.a(this.f842f, this.f843g, str2, bArr, str3, this, vector, bVar);
    }

    public static String a(byte[] bArr) {
        if (bArr == null) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < 16; i2++) {
            byte b2 = bArr[i2];
            stringBuffer.append(v[(byte) (((byte) (((byte) (b2 & 240)) >>> 4)) & 15)] + v[(byte) (b2 & 15)]);
            if (i2 == 3 || i2 == 5 || i2 == 7 || i2 == 9) {
                stringBuffer.append("-");
            }
        }
        return stringBuffer.toString().toLowerCase();
    }

    private void a(byte b2, Object obj) {
        this.i.a(new com.nuance.a.a.a.a.b.a.b.a(b2, obj), this, Thread.currentThread(), this.i.a()[0]);
    }

    private void b(byte[] bArr) {
        byte b2 = bArr[16];
        short a2 = com.nuance.a.a.a.a.d.b.a(bArr, 17);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.f844h.size()) {
                ((c) this.f844h.elementAt(i3)).a(b2, a2);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private void c(byte[] bArr) {
        int b2 = com.nuance.a.a.a.a.d.b.b(bArr, 0);
        if (this.f839c.size() != 0) {
            com.nuance.a.a.a.b.b.a aVar = (com.nuance.a.a.a.b.b.a) this.f839c.get(new Integer(b2));
            if (aVar == null) {
                f837a.e("Could not find the audio sink associated with AID: " + b2);
                return;
            }
            int b3 = com.nuance.a.a.a.a.d.b.b(bArr, 4);
            int i2 = 8;
            if (com.nuance.a.a.a.a.d.d.a(this.o)) {
                while ((bArr[i2] & 128) > 0) {
                    i2++;
                    b3--;
                }
                i2++;
                b3--;
            }
            if (b3 > 0 && b3 <= bArr.length - i2) {
                try {
                    aVar.a(bArr, i2, b3, false);
                } catch (f e2) {
                    f837a.e(e2.getMessage());
                }
            }
            if (this.f840d.size() != 0) {
                c cVar = (c) this.f840d.get(new Integer(b2));
                if (cVar == null) {
                    f837a.e("parseXModeMsgVapPlay:: Could not find the session listener associated with AID: " + b2);
                } else {
                    cVar.e();
                }
            }
        }
    }

    private void h() {
        while (!this.k.isEmpty()) {
            com.nuance.a.a.a.a.b.a.b.a aVar = (com.nuance.a.a.a.a.b.a.b.a) this.k.firstElement();
            this.k.removeElementAt(0);
            switch (aVar.f783a) {
                case 1:
                    a(1, aVar.f784b);
                    break;
                case 2:
                    a(2, aVar.f784b);
                    break;
                case 4:
                    a(4, aVar.f784b);
                    break;
                case 5:
                    a(5, aVar.f784b);
                    break;
                case 6:
                    a(6, aVar.f784b);
                    break;
                case 7:
                    a(7, aVar.f784b);
                    break;
                case 8:
                    a(8, aVar.f784b);
                    break;
            }
        }
    }

    public final synchronized int a() {
        int i2;
        i2 = p;
        p = i2 + 1;
        if (p == Integer.MIN_VALUE) {
            p = 1;
        }
        return i2;
    }

    public final void a(byte b2, int i2, c cVar) {
        f837a.b((Object) "freeResource, TID: " + b2 + ", disconnect timeout: " + i2);
        this.f844h.removeElement(cVar);
        Object[] objArr = {new Byte(b2), new Integer(i2)};
        if (this.l == null || !this.k.isEmpty()) {
            this.k.addElement(new com.nuance.a.a.a.a.b.a.b.a(4, objArr));
        } else {
            a(4, (Object) objArr);
        }
    }

    public final void a(int i2) {
        if (f837a.b()) {
            f837a.b((Object) "vapRecordBegin, AID: " + i2);
        }
        Integer num = new Integer(i2);
        if (this.l == null || !this.k.isEmpty()) {
            this.k.addElement(new com.nuance.a.a.a.a.b.a.b.a(5, num));
        } else {
            a(5, (Object) num);
        }
    }

    public final void a(int i2, com.nuance.a.a.a.b.b.a aVar, c cVar) {
        this.f839c.put(new Integer(i2), aVar);
        this.f840d.put(new Integer(i2), cVar);
    }

    public final void a(C0000a aVar, C0000a aVar2) {
        if (f837a.b()) {
            f837a.b((Object) "connect()");
        }
        this.n = aVar;
        this.o = aVar2;
        a(1, (Object) null);
    }

    public final void a(com.nuance.a.a.a.a.c.b bVar, byte[] bArr) {
        int i2 = 0;
        if (f837a.b()) {
            f837a.b((Object) "xmodeMsgCallback, protocol: " + bVar.f793a + ", command: " + bVar.f795c);
        }
        switch (bVar.f793a) {
            case 1:
                switch (bVar.f795c) {
                    case UdtEnrollResult.CHECKRECORDING_SPOT /*512*/:
                        c(bArr);
                        return;
                    case 528:
                        int b2 = com.nuance.a.a.a.a.d.b.b(bArr, 0);
                        if (this.f840d.size() != 0) {
                            c cVar = (c) this.f840d.get(new Integer(b2));
                            if (cVar == null) {
                                f837a.e("parseVapPlayBegin:: Could not find the session listener associated with AID: " + b2);
                                return;
                            } else {
                                cVar.d();
                                return;
                            }
                        } else {
                            return;
                        }
                    case 1024:
                        int b3 = com.nuance.a.a.a.a.d.b.b(bArr, 0);
                        if (this.f839c.size() != 0) {
                            com.nuance.a.a.a.b.b.a aVar = (com.nuance.a.a.a.b.b.a) this.f839c.remove(new Integer(b3));
                            if (aVar == null) {
                                f837a.e("Could not find the audio sink associated with AID: " + b3);
                                return;
                            }
                            try {
                                aVar.a(null, 0, 0, true);
                            } catch (f e2) {
                                f837a.e(e2.getMessage());
                            }
                            c cVar2 = (c) this.f840d.remove(new Integer(b3));
                            if (cVar2 == null) {
                                f837a.e("parseXModeMsgVapPlayEnd:: Could not find the session listener associated with AID: " + b3);
                                return;
                            } else {
                                cVar2.f();
                                return;
                            }
                        } else {
                            return;
                        }
                    default:
                        return;
                }
            case 2:
                switch (bVar.f795c) {
                    case 2576:
                        byte b4 = bArr[16];
                        int b5 = com.nuance.a.a.a.a.d.b.b(bArr, 17);
                        short a2 = com.nuance.a.a.a.a.d.b.a(bArr, 21);
                        short a3 = com.nuance.a.a.a.a.d.b.a(bArr, 23);
                        short a4 = com.nuance.a.a.a.a.d.b.a(bArr, 25);
                        c cVar3 = (c) this.f838b.get(new Long((long) b5));
                        if (cVar3 != null) {
                            cVar3.a(b4, (long) b5, a2, a3, a4);
                            if (a2 != 200) {
                                this.f838b.remove(new Long((long) b5));
                                return;
                            }
                            return;
                        }
                        return;
                    case 2577:
                        long b6 = (long) com.nuance.a.a.a.a.d.b.b(bArr, 17);
                        com.nuance.a.a.a.a.d.b.b(bArr, 21);
                        com.nuance.a.a.a.a.d.b.a(bArr, 25);
                        this.f838b.remove(new Long(b6));
                        return;
                    case 2578:
                        return;
                    case 2579:
                        long b7 = (long) com.nuance.a.a.a.a.d.b.b(bArr, 17);
                        com.nuance.a.a.a.a.d.b.a(bArr, 21);
                        int b8 = com.nuance.a.a.a.a.d.b.b(bArr, 23);
                        if (b8 > 0 && b8 <= bArr.length - 27) {
                            System.arraycopy(bArr, 27, new byte[b8], 0, b8);
                        }
                        this.f838b.remove(new Long(b7));
                        return;
                    case 2580:
                        long b9 = (long) com.nuance.a.a.a.a.d.b.b(bArr, 17);
                        com.nuance.a.a.a.a.d.b.a(bArr, 21);
                        int b10 = com.nuance.a.a.a.a.d.b.b(bArr, 23);
                        if (b10 > 0 && b10 <= bArr.length - 27) {
                            System.arraycopy(bArr, 27, new byte[b10], 0, b10);
                        }
                        this.f838b.get(new Long(b9));
                        return;
                    case 2582:
                        byte[] bArr2 = null;
                        byte b11 = bArr[16];
                        int b12 = com.nuance.a.a.a.a.d.b.b(bArr, 17);
                        short a5 = com.nuance.a.a.a.a.d.b.a(bArr, 21);
                        int b13 = com.nuance.a.a.a.a.d.b.b(bArr, 23);
                        if (b13 > 0 && b13 <= bArr.length - 27) {
                            bArr2 = new byte[b13];
                            System.arraycopy(bArr, 27, bArr2, 0, b13);
                        }
                        c cVar4 = (c) this.f838b.remove(new Long((long) b12));
                        if (cVar4 != null) {
                            cVar4.a(b11, (long) b12, a5, bArr2);
                            return;
                        }
                        return;
                    case 2584:
                        byte[] bArr3 = null;
                        byte b14 = bArr[16];
                        int b15 = com.nuance.a.a.a.a.d.b.b(bArr, 17);
                        short a6 = com.nuance.a.a.a.a.d.b.a(bArr, 21);
                        int b16 = com.nuance.a.a.a.a.d.b.b(bArr, 23);
                        if (b16 > 0 && b16 <= bArr.length - 27) {
                            bArr3 = new byte[b16];
                            System.arraycopy(bArr, 27, bArr3, 0, b16);
                        }
                        c cVar5 = (c) this.f838b.remove(new Long((long) b15));
                        if (cVar5 != null) {
                            cVar5.b(b14, (long) b15, a6, bArr3);
                            return;
                        }
                        return;
                    case 2585:
                        byte b17 = bArr[16];
                        int b18 = com.nuance.a.a.a.a.d.b.b(bArr, 21);
                        if (b18 > 0 && b18 <= bArr.length - 25) {
                            byte[] bArr4 = new byte[b18];
                            System.arraycopy(bArr, 25, bArr4, 0, b18);
                            c cVar6 = (c) this.f838b.get(new Long(this.q));
                            if (cVar6 != null) {
                                long j2 = this.q;
                                cVar6.a(b17, bArr4);
                                return;
                            }
                            return;
                        }
                        return;
                    case 2600:
                        b(bArr);
                        return;
                    default:
                        f837a.e("Unknown BCP command");
                        return;
                }
            case 3:
                switch (bVar.f795c) {
                    case 257:
                        this.l = this.f841e.f805e;
                        if (f837a.b()) {
                            f837a.b((Object) "connected(" + a(this.l) + ") called on " + this.j);
                        }
                        c cVar7 = this.j;
                        String a7 = a(this.l);
                        d dVar = this.s;
                        cVar7.a(a7);
                        while (true) {
                            int i3 = i2;
                            if (i3 < this.f844h.size()) {
                                ((c) this.f844h.elementAt(i3)).a(this.l);
                                i2 = i3 + 1;
                            } else {
                                h();
                                return;
                            }
                        }
                    default:
                        return;
                }
            default:
                f837a.e("Unknown Xmode protocol");
                return;
        }
    }

    public final void a(d dVar) {
        this.s = dVar;
    }

    public final void a(c cVar) {
        if (!this.f844h.contains(cVar)) {
            this.f844h.addElement(cVar);
        }
    }

    public final void a(Object obj, Object obj2) {
        com.nuance.a.a.a.a.b.a.b.a aVar = (com.nuance.a.a.a.a.b.a.b.a) obj;
        switch (aVar.f783a) {
            case 1:
                this.f841e.a(this.n.a(), this.o.a());
                return;
            case 2:
                this.f841e.b();
                return;
            case 3:
                this.f841e.b();
                this.r = true;
                return;
            case 4:
                if (this.l != null) {
                    Object[] objArr = (Object[]) aVar.f784b;
                    byte byteValue = ((Byte) objArr[0]).byteValue();
                    int intValue = ((Integer) objArr[1]).intValue();
                    byte[] bArr = new byte[5];
                    bArr[0] = byteValue;
                    com.nuance.a.a.a.a.d.b.a(intValue, bArr, 1);
                    this.f841e.a(com.nuance.a.a.a.a.c.a.a(2, 34, 2601, com.nuance.a.a.a.a.c.a.a(bArr, this.l)), (Object) "SEND_BCP_FREE_RESOURCE");
                    return;
                }
                return;
            case 5:
                if (this.l != null) {
                    this.f841e.a(((Integer) aVar.f784b).intValue());
                    return;
                }
                return;
            case 6:
                if (this.l != null) {
                    Object[] objArr2 = (Object[]) aVar.f784b;
                    byte[] bArr2 = (byte[]) objArr2[0];
                    int intValue2 = ((Integer) objArr2[1]).intValue();
                    int i2 = 0;
                    while (true) {
                        int i3 = i2;
                        if (i3 < this.f844h.size()) {
                            ((c) this.f844h.elementAt(i3)).c();
                            i2 = i3 + 1;
                        } else {
                            this.f841e.a(bArr2, intValue2);
                            return;
                        }
                    }
                } else {
                    return;
                }
            case 7:
                if (this.l != null) {
                    this.f841e.b(((Integer) aVar.f784b).intValue());
                    return;
                }
                return;
            case 8:
                if (this.l != null) {
                    Object[] objArr3 = (Object[]) aVar.f784b;
                    short shortValue = ((Short) objArr3[0]).shortValue();
                    String str = (String) objArr3[1];
                    byte[] bArr3 = (byte[]) objArr3[2];
                    byte[] bArr4 = (byte[]) objArr3[3];
                    byte byteValue2 = ((Byte) objArr3[4]).byteValue();
                    long longValue = ((Long) objArr3[5]).longValue();
                    c cVar = (c) objArr3[6];
                    boolean booleanValue = ((Boolean) objArr3[7]).booleanValue();
                    this.f838b.put(new Long(longValue), cVar);
                    int length = bArr3.length + 5;
                    if (shortValue == 2585) {
                        length += 4;
                    }
                    byte[] bArr5 = new byte[length];
                    bArr5[0] = byteValue2;
                    int i4 = 1;
                    if (shortValue == 2585) {
                        com.nuance.a.a.a.a.d.b.a((int) longValue, bArr5, 1);
                        i4 = 5;
                    }
                    com.nuance.a.a.a.a.d.b.a(bArr3.length, bArr5, i4);
                    System.arraycopy(bArr3, 0, bArr5, i4 + 4, bArr3.length);
                    byte[] a2 = com.nuance.a.a.a.a.c.a.a(bArr5, this.l != null ? this.l : m);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    byteArrayOutputStream.write(a2, 0, a2.length);
                    if (bArr4 != null) {
                        byteArrayOutputStream.write(bArr4, 0, bArr4.length);
                    }
                    this.f841e.a(com.nuance.a.a.a.a.c.a.a(2, 34, shortValue, byteArrayOutputStream.toByteArray()), (Object) str);
                    if (!booleanValue) {
                        return;
                    }
                    if (shortValue == 2581) {
                        if (cVar != null) {
                            cVar.a(byteValue2, longValue, 200, null);
                            return;
                        }
                        return;
                    } else if (shortValue == 2608 && cVar != null) {
                        this.f844h.removeElement(cVar);
                        cVar.b();
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            default:
                f837a.e("Unknown command");
                return;
        }
    }

    public final void a(short s2, String str, byte[] bArr, byte[] bArr2, byte b2, long j2, c cVar, boolean z) {
        if (f837a.b()) {
            f837a.b((Object) "postBcpMessage, BCP: " + s2 + ", TID: " + b2 + ", RID: " + j2);
        }
        Object[] objArr = {new Short(s2), str, bArr, bArr2, new Byte(b2), new Long(j2), cVar, new Boolean(z)};
        if (this.l == null || !this.k.isEmpty()) {
            this.k.addElement(new com.nuance.a.a.a.a.b.a.b.a(8, objArr));
        } else {
            a(8, (Object) objArr);
        }
    }

    public final void a(short s2, short s3) {
        if (f837a.b()) {
            f837a.b((Object) "socketClosed, reason: " + s2);
        }
        if (s2 == 1 && this.r) {
            this.i.c();
        }
        if (f837a.b()) {
            f837a.b((Object) "socketClosed() sessionListeners.size():" + this.f844h.size());
        }
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.f844h.size()) {
                break;
            }
            ((c) this.f844h.elementAt(i3)).a(s2);
            i2 = i3 + 1;
        }
        if (!this.k.isEmpty()) {
            this.k.removeAllElements();
        }
        this.f844h.removeAllElements();
        if (s2 == 4 || s2 == 5 || s2 == 7) {
            c cVar = this.j;
            d dVar = this.s;
            cVar.a();
        } else if (s2 == 8 && this.l == null) {
            c cVar2 = this.j;
            d dVar2 = this.s;
            cVar2.a();
        } else if (this.l != null) {
            c cVar3 = this.j;
            d dVar3 = this.s;
            cVar3.a(s3);
        }
        this.l = null;
    }

    public final void a(byte[] bArr, int i2) {
        if (f837a.b()) {
            f837a.b((Object) "vapRecord, AID: " + i2);
        }
        Object[] objArr = {bArr, new Integer(i2)};
        if (this.l == null || !this.k.isEmpty()) {
            this.k.addElement(new com.nuance.a.a.a.a.b.a.b.a(6, objArr));
        } else {
            a(6, (Object) objArr);
        }
    }

    public final void b() {
        this.q = 0;
    }

    public final void b(int i2) {
        if (f837a.b()) {
            f837a.b((Object) "vapRecordEnd, AID: " + i2);
        }
        Integer num = new Integer(i2);
        if (this.l == null || !this.k.isEmpty()) {
            this.k.addElement(new com.nuance.a.a.a.a.b.a.b.a(7, num));
        } else {
            a(7, (Object) num);
        }
    }

    public final void b(c cVar) {
        this.f844h.removeElement(cVar);
    }

    public final void c() {
        f837a.b((Object) "disconnectAndShutdown");
        a(3, (Object) null);
    }

    public final byte[] d() {
        return this.l;
    }

    public final long e() {
        long j2 = this.t;
        this.t = j2 + 1;
        if (this.t == Long.MIN_VALUE) {
            this.t = 1;
        }
        return j2;
    }

    public final byte f() {
        byte b2 = this.u;
        this.u = (byte) (b2 + 1);
        if (this.u == Byte.MIN_VALUE) {
            this.u = 1;
        }
        return b2;
    }

    public final C0000a g() {
        return this.n;
    }
}
