package com.nuance.dragon.toolkit.recognition.dictation;

public interface AlterChoice {
    EditorItem editorItemAt(int i);

    int size();

    String toString();
}
