package com.motorola.audiomonitor;

import com.motorola.audiomonitor.RecognizerConfig.Builder;

public class bz {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f388a = {"*sil", "*any"};

    /* renamed from: b reason: collision with root package name */
    private static final String[] f389b = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "zero", "enter", "oh", "*sil", "*any"};

    public static RecognizerConfig a() {
        Builder builder = new Builder();
        builder.setRecogThreshold(0.0f);
        builder.setPhraseSpot(true);
        builder.setNBest(1);
        builder.setRecogMinDuration(500.0f);
        builder.setPhrasespotParamAOffset(-500.0f);
        builder.setRecogMode(1);
        return builder.build();
    }

    public static RecognizerConfig a(String str, String[] strArr) {
        Builder builder = new Builder();
        builder.setGrammar(str);
        builder.setWords(strArr);
        builder.setRecogThreshold(0.0f);
        builder.setPhraseSpot(true);
        builder.setNBest(1);
        builder.setPhrasespotParamAOffset(-300.0f);
        builder.setRecogMode(1);
        return builder.build();
    }

    public static RecognizerConfig b() {
        Builder builder = new Builder();
        builder.setGrammar("g=(*sil%%) *any%% (*sil%%);");
        builder.setWords(f388a);
        builder.setPhraseSpot(false);
        builder.setNBest(1);
        builder.setRecogMode(0);
        builder.setRecogTSilence(1000.0f);
        builder.setRecogMinDuration(300.0f);
        builder.setESilenceT(1.5f);
        builder.setESpeechT(1.0f);
        return builder.build();
    }

    public static RecognizerConfig c() {
        Builder builder = new Builder();
        builder.setUdt(true);
        builder.setNBest(1);
        builder.setRecogMinDuration(500.0f);
        builder.setRecogMode(2);
        return builder.build();
    }

    public static RecognizerConfig d() {
        Builder builder = new Builder();
        builder.setPhraseSpot(true);
        builder.setUdt(true);
        builder.setNBest(1);
        builder.setRecogThreshold(900.0f);
        builder.setVerificationThreshold(0.45f);
        builder.setPhrasespotDelay(30.0f);
        builder.setRecogMode(2);
        return builder.build();
    }
}
