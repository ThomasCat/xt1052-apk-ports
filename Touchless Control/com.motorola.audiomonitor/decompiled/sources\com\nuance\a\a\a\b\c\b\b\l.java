package com.nuance.a.a.a.b.c.b.b;

import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class l extends i {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f869a = a.a(l.class);

    /* renamed from: b reason: collision with root package name */
    private short f870b;

    public l(short s) {
        this.f870b = s;
    }

    public l(short s, byte[] bArr) {
        super(bArr);
        this.f870b = s;
    }

    public final byte[] d() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(this.f870b & 255);
        byteArrayOutputStream.write((this.f870b & 65280) >> 8);
        try {
            byteArrayOutputStream.write(super.d());
        } catch (IOException e2) {
            f869a.e("PDXMessage.toByteArray() " + e2.toString() + ". ");
        }
        return byteArrayOutputStream.toByteArray();
    }

    public final short e() {
        return this.f870b;
    }

    public final byte[] f() {
        return super.d();
    }
}
