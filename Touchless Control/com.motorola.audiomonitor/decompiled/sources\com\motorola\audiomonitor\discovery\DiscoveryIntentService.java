package com.motorola.audiomonitor.discovery;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class DiscoveryIntentService extends IntentService {

    /* renamed from: a reason: collision with root package name */
    private static final String f475a = ("AMonitor." + DiscoveryIntentService.class.getSimpleName());

    public DiscoveryIntentService() {
        super(DiscoveryIntentService.class.getSimpleName());
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        Context applicationContext = getApplicationContext();
        if (intent == null) {
            Log.w(f475a, "Intent is null, returning");
            return;
        }
        String stringExtra = intent.getStringExtra("com.motorola.audiomonitor.extra.DISCOVERY_ACTION");
        if (stringExtra == null) {
            Log.w(f475a, "Action is null, returning");
            return;
        }
        c a2 = c.a(applicationContext);
        if (stringExtra.equals("android.intent.action.BOOT_COMPLETED")) {
            a2.b();
        } else if (stringExtra.equals("com.motorola.blur.setupprovider.action.POST_SETUP")) {
            a2.d();
            a2.a(b.SETUP_WIZARD_COMPLETE);
        } else if (stringExtra.equals("com.motorola.audiomonitor.action.TRAINING_DELAYED")) {
            a2.e();
            a2.a(b.TRAINING_DELAYED);
        } else if (stringExtra.equals("com.motorola.audiomonitor.CANCEL_NOTIFY")) {
            a2.a();
        } else if (stringExtra.equals("com.motorola.context.CONTEXT_CHANGE")) {
            if ("InVehicle".equals(intent.getStringExtra("com.motorola.context.state"))) {
                a2.a(true);
            } else if (a2.c()) {
                a2.a(b.DRIVE_MODE_ACTIVATED);
            }
        } else if (stringExtra.equals("com.motorola.audiomonitor.action.SHOW_DISCOVERY_NOTIFICATION")) {
            String stringExtra2 = intent.getStringExtra("com.motorola.audiomonitor.extra.DISCOVERY_MODE");
            if (stringExtra2 == null) {
                Log.w(f475a, "Mode extra is null");
                return;
            }
            try {
                a2.b(b.valueOf(stringExtra2));
            } catch (Exception e2) {
                Log.w(f475a, "Could not convert mode string to mode");
            }
        }
    }
}
