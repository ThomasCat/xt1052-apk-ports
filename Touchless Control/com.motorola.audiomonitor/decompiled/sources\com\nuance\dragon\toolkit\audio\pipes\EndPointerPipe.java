package com.nuance.dragon.toolkit.audio.pipes;

import com.nuance.dragon.toolkit.audio.AbstractAudioChunk.SpeechStatus;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.SpeechDetectionListener;

public class EndPointerPipe extends BufferingPipe {

    /* renamed from: a reason: collision with root package name */
    private final SpeechDetectionListener f1166a;

    /* renamed from: b reason: collision with root package name */
    private boolean f1167b = false;

    /* renamed from: c reason: collision with root package name */
    private boolean f1168c = false;

    public EndPointerPipe(SpeechDetectionListener speechDetectionListener) {
        this.f1166a = speechDetectionListener;
    }

    /* access modifiers changed from: protected */
    public void onChunkBuffered(AudioChunk audioChunk) {
        if (!this.f1167b) {
            if (audioChunk.audioSpeechStatus == SpeechStatus.SPEECH) {
                this.f1167b = true;
                this.f1166a.onStartOfSpeech();
            }
        } else if (!this.f1168c && audioChunk.audioSpeechStatus == SpeechStatus.NO_SPEECH) {
            this.f1168c = true;
            this.f1166a.onEndOfSpeech();
        }
        super.onChunkBuffered(audioChunk);
    }
}
