package com.nuance.dragon.toolkit.audio.bluetooth.impl;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.nuance.dragon.toolkit.audio.bluetooth.impl.Bluetooth.HeadsetStateListener;
import com.nuance.dragon.toolkit.audio.bluetooth.impl.BluetoothHeadset.ServiceListener;
import java.util.Set;

final class c extends Bluetooth {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public BluetoothHeadset f1064a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public HeadsetStateListener f1065b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public boolean f1066c;

    /* renamed from: d reason: collision with root package name */
    private boolean f1067d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public boolean f1068e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public boolean f1069f;

    /* renamed from: g reason: collision with root package name */
    private final ServiceListener f1070g = new ServiceListener() {
        public final void onServiceConnected() {
            boolean z = false;
            Bluetooth.a("BluetoothHeadset service connected");
            if (c.this.f1064a == null) {
                c.this.f1065b.onServiceConnected(false);
                return;
            }
            c.this.f1066c = true;
            c cVar = c.this;
            if (c.this.f1064a.getState() == BluetoothHeadset.STATE_CONNECTED) {
                z = true;
            }
            cVar.f1068e = z;
            if (c.this.f1065b != null) {
                c.this.f1065b.onServiceConnected(true);
                c.this.f1065b.onConnectionStateChanged(c.this.getHeadsetState());
            }
        }

        public final void onServiceDisconnected() {
            Bluetooth.a("BluetoothHeadset service disconnected");
            c.this.f1065b.onServiceDisconnected();
            c.this.f1066c = false;
        }
    };

    /* renamed from: h reason: collision with root package name */
    private final BroadcastReceiver f1071h = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            boolean z = true;
            String action = intent.getAction();
            if (action.equals(BluetoothHeadset.ACTION_STATE_CHANGED)) {
                int intExtra = intent.getIntExtra(BluetoothHeadset.EXTRA_STATE, -1);
                Bluetooth.a("Bluetooth state changed: " + intExtra);
                if (intExtra != BluetoothHeadset.STATE_CONNECTED) {
                    z = false;
                }
                if (c.this.f1068e != z) {
                    c.this.f1068e = z;
                    if (c.this.f1065b != null) {
                        c.this.f1065b.onConnectionStateChanged(c.this.getHeadsetState());
                    }
                }
            } else if (action.equals(BluetoothHeadset.ACTION_AUDIO_STATE_CHANGED)) {
                int intExtra2 = intent.getIntExtra(BluetoothHeadset.EXTRA_AUDIO_STATE, -1);
                Bluetooth.a("Bluetooth audio state changed: " + intExtra2);
                if (intExtra2 == BluetoothHeadset.AUDIO_STATE_CONNECTED || intExtra2 == BluetoothHeadset.AUDIO_STATE_DISCONNECTED) {
                    if (intExtra2 != BluetoothHeadset.AUDIO_STATE_CONNECTED) {
                        z = false;
                    }
                    if (c.this.f1069f != z) {
                        c.this.f1069f = z;
                        if (c.this.f1065b != null) {
                            c.this.f1065b.onAudioStateChanged(c.this.getAudioState());
                        }
                    }
                }
            }
        }
    };

    public c(Context context) {
        super(context);
    }

    public final void close() {
        if (this.f1067d) {
            this.f1067d = false;
            this.mContext.unregisterReceiver(this.f1071h);
        }
        if (this.f1064a != null) {
            try {
                this.f1064a.close();
            } catch (Throwable th) {
            }
            this.f1064a = null;
        }
    }

    public final int getAudioState() {
        return this.f1069f ? 1 : 0;
    }

    public final String[] getBondedDevices() {
        Set<BluetoothDevice> bondedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
        if (bondedDevices == null) {
            return null;
        }
        String[] strArr = new String[bondedDevices.size()];
        int i = 0;
        for (BluetoothDevice bluetoothDevice : bondedDevices) {
            String name = bluetoothDevice.getName();
            int i2 = i + 1;
            strArr[i] = name != null ? name : bluetoothDevice.getAddress();
            i = i2;
        }
        return strArr;
    }

    public final int getHeadsetState() {
        return this.f1068e ? 1 : 0;
    }

    public final int getPlaybackStream() {
        return AndroidVersion.USE_MUSIC_STREAM_FOR_BLUETOOTH ? Audio.MUSIC_STREAM : Audio.BLUETOOTH_STREAM;
    }

    public final int getRecordingSource() {
        return Audio.DEFAULT_AUDIO_SOURCE;
    }

    public final boolean isEnabled() {
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter != null) {
            return defaultAdapter.isEnabled();
        }
        return false;
    }

    public final void setHeadsetStateListener(HeadsetStateListener headsetStateListener) {
        if (this.f1064a == null) {
            this.f1064a = new BluetoothHeadset(this.mContext, this.f1070g);
            IntentFilter intentFilter = new IntentFilter(BluetoothHeadset.ACTION_STATE_CHANGED);
            intentFilter.addAction(BluetoothHeadset.ACTION_AUDIO_STATE_CHANGED);
            this.mContext.registerReceiver(this.f1071h, intentFilter);
            this.f1067d = true;
        }
        this.f1065b = headsetStateListener;
        if (this.f1066c) {
            this.f1065b.onConnectionStateChanged(getHeadsetState());
        }
    }

    public final boolean startBluetoothSco() {
        if (!AndroidVersion.USE_MUSIC_STREAM_FOR_BLUETOOTH) {
            this.f1064a.startVoiceRecognition();
        }
        return true;
    }

    public final void stopBluetoothSco() {
        if (!AndroidVersion.USE_MUSIC_STREAM_FOR_BLUETOOTH && this.f1064a != null) {
            this.f1064a.stopVoiceRecognition();
        }
    }
}
