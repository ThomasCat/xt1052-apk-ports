package a;

import android.text.TextUtils;
import android.util.Log;
import b.b;
import b.d;
import com.motorola.audiomonitor.actions.utils.a;
import com.motorola.audiomonitor.actions.utils.f;
import com.motorola.audiomonitor.actions.utils.g;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

final class z extends af {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ u f86a;

    /* renamed from: g reason: collision with root package name */
    private final Comparator f87g;

    /* renamed from: h reason: collision with root package name */
    private final Comparator f88h;

    private z(u uVar) {
        this.f86a = uVar;
        super(uVar, null);
        this.f87g = new aa(this);
        this.f88h = new ab(this);
    }

    /* synthetic */ z(u uVar, v vVar) {
        this(uVar);
    }

    private List a(b bVar) {
        List arrayList = new ArrayList();
        String str = null;
        if (!TextUtils.isEmpty(bVar.a("telephoneNumber"))) {
            a a2 = f.a(this.f86a.o, bVar.a("telephoneNumber"));
            if (a2 != null) {
                arrayList.add(a2);
                str = "digital";
            }
        } else {
            List a3 = this.f86a.l.a(bVar);
            arrayList = g.a(this.f86a.o, a3);
            if (arrayList.size() == 0) {
                arrayList = f.a(this.f86a.o, a3);
                if (arrayList.size() > 0) {
                    str = "local";
                }
            } else {
                str = "vip";
            }
        }
        if (arrayList.size() > 0) {
            arrayList = a(arrayList);
            h.g.a().a(str, arrayList);
        }
        if (u.f71b) {
            Log.d(u.f70a, "findContacts=" + arrayList + (str != null ? " by " + str : ""));
        }
        return arrayList;
    }

    public List a(List list) {
        TreeSet treeSet = new TreeSet(this.f87g);
        ArrayList arrayList = new ArrayList();
        treeSet.addAll(list);
        arrayList.addAll(treeSet);
        Collections.sort(arrayList, this.f88h);
        if (u.f71b && list.size() != arrayList.size()) {
            Log.d(u.f70a, "removed duplicate from " + list + " to " + arrayList);
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void a() {
        b bVar;
        if (this.f86a.i == ac.PARSED) {
            bVar = this.f86a.j;
            this.f12d = ad.Nuance.name();
            this.f13e = ad.Nuance.name();
        } else {
            bVar = null;
        }
        if (bVar != null) {
            Log.d(u.f70a, "Parse Result: " + bVar.toString());
        } else {
            Log.d(u.f70a, "Parse Result: no audio match");
        }
        if (this.f86a.a(bVar)) {
            this.f11c = a(bVar);
            if (this.f11c == null || this.f11c.size() == 0) {
                bVar = null;
            }
        }
        if (bVar == null) {
            bVar = new d("google");
        }
        this.f10b = bVar;
    }

    public String b() {
        return getClass().getSimpleName();
    }
}
