package com.motorola.audiomonitor.service;

import android.os.Message;
import android.util.Log;
import com.nuance.dragon.toolkit.audio.bluetooth.impl.AndroidVersion;
import com.nuance.dragon.toolkit.audio.pipes.SpeexEncoderPipe;

class g extends p {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ d f573a;

    g(d dVar, String str, String str2) {
        this.f573a = dVar;
        super(dVar, str, str2);
    }

    public void a(Message message) {
        switch (message.what) {
            case 1:
                this.f573a.s = this.f573a.f567e;
                this.f573a.v();
                this.f573a.a(109, 1, 0, (Object) null);
                this.f573a.a(111, 0, 0, (Object) null);
                return;
            case 2:
            case 3:
            case 7:
            case 8:
            case 9:
            case 10:
                if (message.arg1 != 0) {
                    this.f573a.b(true);
                    return;
                }
                return;
            case 11:
                if (message.arg1 != 0) {
                    this.f573a.b(true);
                    return;
                } else if (this.f573a.w.what == 102) {
                    this.f573a.s = this.f573a.f568f;
                    return;
                } else if (this.f573a.w.what == 107) {
                    this.f573a.s = this.f573a.f570h;
                    this.f573a.a(109, 1, 0, (Object) null);
                    return;
                } else {
                    return;
                }
            case 12:
                if (message.arg1 == 0) {
                    if (this.f573a.w.arg1 < this.f573a.z.length) {
                        this.f573a.z[this.f573a.w.arg1] = message.arg2;
                    }
                    if (this.f573a.w.arg1 < this.f573a.A.length) {
                        this.f573a.A[this.f573a.w.arg1] = (short[]) message.obj;
                    }
                    this.f573a.a(this.f573a.w.arg1, message.arg2, (short[]) message.obj);
                    return;
                }
                return;
            case AndroidVersion.ICE_CREAM_SANDWICH /*14*/:
                if (message.arg1 == 0) {
                    this.f573a.a((int[]) message.obj);
                    return;
                }
                return;
            case SpeexEncoderPipe.DEFAULT_VAD_BEGIN_LEN /*15*/:
                this.f573a.b(false);
                return;
            case 101:
                this.f573a.s();
                return;
            case 102:
                if (this.f573a.x.a(1, message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to start recognition");
                    this.f573a.a(message);
                    return;
                }
                return;
            case 103:
                if (this.f573a.x.a(0, message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to stop recognition");
                    this.f573a.a(message);
                    return;
                }
                this.f573a.s = this.f573a.f569g;
                return;
            case 104:
                if (this.f573a.x.a(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to control event log");
                    this.f573a.a(message);
                    return;
                }
                return;
            case 105:
                if (this.f573a.x.b(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to control audio log");
                    this.f573a.a(message);
                    return;
                }
                return;
            case 106:
                if (this.f573a.x.c(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to control audio buffering");
                    this.f573a.a(message);
                    return;
                }
                return;
            case 107:
                if (this.f573a.x.a(2, 1) != 0) {
                    Log.e(d.f563a, "Failed to start pass-through");
                    this.f573a.a(message);
                    return;
                }
                return;
            case 108:
                this.f573a.a(message);
                return;
            case 109:
                if (this.f573a.x.e(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to request noise estimate");
                    this.f573a.a(message);
                    return;
                }
                return;
            case 110:
                this.f573a.a(message);
                return;
            case 111:
                if (this.f573a.x.f() != 0) {
                    Log.e(d.f563a, "Failed to request noise mode times");
                    this.f573a.a(message);
                    return;
                }
                return;
            case 201:
                this.f573a.b(true);
                return;
            case 202:
                if (this.f573a.x.d(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to send dock state change");
                    this.f573a.a(message);
                    return;
                }
                return;
            case 203:
                if (this.f573a.x.a((String) message.obj) != 0) {
                    Log.e(d.f563a, "Failed to load acoustic model");
                    this.f573a.a(message);
                    return;
                }
                return;
            case 204:
                if (this.f573a.x.b((String) message.obj) != 0) {
                    Log.e(d.f563a, "Failed to load language model");
                    this.f573a.a(message);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
