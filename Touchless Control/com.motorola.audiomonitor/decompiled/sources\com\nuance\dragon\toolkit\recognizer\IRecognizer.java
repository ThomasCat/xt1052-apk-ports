package com.nuance.dragon.toolkit.recognizer;

import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.SpeechDetectionListener;
import java.util.List;

public interface IRecognizer {

    public interface InitializeListener {
        void onLoaded(IRecognizer iRecognizer, boolean z);
    }

    public interface ReleaseListener {
        void onReleased(IRecognizer iRecognizer);
    }

    public interface ResultListener {
        void onError(RecogError recogError);

        void onResult(RecogResult recogResult);
    }

    void cancelLocalRecognition();

    List getAvailableModels();

    List getAvailableModels(int i);

    List getAvailableModels(NMTLanguage nMTLanguage);

    void release();

    void release(ReleaseListener releaseListener);

    void startLocalRecognition(AudioSource audioSource, SpeechDetectionListener speechDetectionListener, ResultListener resultListener);

    void stopLocalRecognition();
}
