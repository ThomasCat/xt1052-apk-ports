package h;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.Log;
import com.motorola.audiomonitor.service.AudioDspControl;
import java.io.File;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

public class a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f2262a = ("AMonitor." + a.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f2263b = com.motorola.audiomonitor.a.f160b;

    /* renamed from: c reason: collision with root package name */
    private static final String[] f2264c = {"aonvr1", "aonvr2", "VRGain"};
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public static final AtomicBoolean f2265d = new AtomicBoolean(false);

    /* renamed from: e reason: collision with root package name */
    private static a f2266e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public final b f2267f = new b(this);

    /* renamed from: g reason: collision with root package name */
    private final Context f2268g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public AudioDspControl f2269h;

    private a(Context context) {
        this.f2268g = context.getApplicationContext();
        this.f2267f.start();
    }

    public static boolean a() {
        return f2265d.get();
    }

    public static boolean a(long j) {
        if (!a()) {
            Thread h2 = h();
            if (h2 != null && h2.isAlive()) {
                try {
                    if (f2263b) {
                        Log.d(f2262a, "Blocking until finished with update for ms " + j);
                    }
                    h2.join(j);
                } catch (InterruptedException e2) {
                }
            }
        }
        return a();
    }

    public static synchronized boolean a(Context context) {
        boolean z;
        synchronized (a.class) {
            if (f2266e != null || context == null) {
                if (f2263b) {
                    Log.d(f2262a, "Update is ongoing or bad context");
                }
                z = false;
            } else {
                if (f2263b) {
                    Log.d(f2262a, "Starting update");
                }
                f2266e = new a(context);
                z = true;
            }
        }
        return z;
    }

    private boolean a(String str) {
        boolean a2 = k.a(this.f2268g, c(str), d(str));
        if (a2) {
            new File(d(str)).setReadable(true, false);
        }
        if (f2263b) {
            Log.d(f2262a, "Install of " + str + " success=" + a2);
        }
        return a2;
    }

    private boolean b(String str) {
        return c(str) != 0;
    }

    private int c(String str) {
        Resources resources = this.f2268g.getResources();
        String lowerCase = str.toLowerCase(Locale.US);
        int identifier = resources.getIdentifier((lowerCase + "_" + Build.DEVICE).replace('-', '_').toLowerCase(Locale.US), "raw", this.f2268g.getPackageName());
        return identifier == 0 ? resources.getIdentifier(lowerCase, "raw", this.f2268g.getPackageName()) : identifier;
    }

    private static String d(String str) {
        return "/data/adspd/1/" + str + "." + "bin";
    }

    /* access modifiers changed from: private */
    public boolean f() {
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        for (String str : f2264c) {
            z3 |= new File(d(str)).exists();
            z2 |= b(str);
        }
        if (f2263b) {
            Log.d(f2262a, "Scanned DSP files, files on /data " + z3 + " files in apk " + z2);
        }
        if (z3 && !z2) {
            String[] strArr = f2264c;
            int i = 0;
            while (i < strArr.length) {
                boolean f2 = k.f(d(strArr[i])) | z;
                i++;
                z = f2;
            }
        } else if (z2 && g()) {
            String[] strArr2 = f2264c;
            int i2 = 0;
            while (i2 < strArr2.length) {
                boolean a2 = a(strArr2[i2]) | z;
                i2++;
                z = a2;
            }
        }
        return z;
    }

    private static boolean g() {
        File file = new File("/data/adspd/1/");
        boolean exists = file.exists();
        if (!exists) {
            exists = file.mkdir();
            if (exists) {
                file.setExecutable(true, false);
                file.setReadable(true, false);
            }
        }
        return exists;
    }

    private static synchronized Thread h() {
        b bVar;
        synchronized (a.class) {
            bVar = f2266e != null ? f2266e.f2267f : null;
        }
        return bVar;
    }

    /* access modifiers changed from: private */
    public static synchronized void i() {
        synchronized (a.class) {
            f2266e = null;
        }
    }
}
