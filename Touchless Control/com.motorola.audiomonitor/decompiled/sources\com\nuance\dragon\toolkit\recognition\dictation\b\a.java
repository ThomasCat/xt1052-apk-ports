package com.nuance.dragon.toolkit.recognition.dictation.b;

import com.nuance.dragon.toolkit.recognition.dictation.a.f;
import com.nuance.dragon.toolkit.recognition.dictation.a.g;
import com.nuance.dragon.toolkit.recognition.dictation.a.h;
import com.nuance.dragon.toolkit.recognition.dictation.a.i;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class a implements d {

    /* renamed from: a reason: collision with root package name */
    private byte[] f1956a;

    /* renamed from: b reason: collision with root package name */
    private String f1957b = "Cp1252";

    /* renamed from: c reason: collision with root package name */
    private C0012a f1958c = null;

    /* renamed from: com.nuance.dragon.toolkit.recognition.dictation.b.a$a reason: collision with other inner class name */
    final class C0012a {

        /* renamed from: a reason: collision with root package name */
        public int f1959a;

        /* renamed from: b reason: collision with root package name */
        public int f1960b;

        /* renamed from: c reason: collision with root package name */
        public int f1961c;

        /* renamed from: d reason: collision with root package name */
        public int f1962d;

        /* renamed from: e reason: collision with root package name */
        public int f1963e;

        private C0012a() {
            this.f1959a = -1;
            this.f1960b = -1;
            this.f1961c = -1;
            this.f1962d = -1;
            this.f1963e = -1;
        }

        /* synthetic */ C0012a(a aVar, byte b2) {
            this();
        }
    }

    final class b {

        /* renamed from: a reason: collision with root package name */
        public g f1965a;

        /* renamed from: b reason: collision with root package name */
        public int f1966b;

        /* renamed from: c reason: collision with root package name */
        public int f1967c;

        private b() {
            this.f1965a = new g();
            this.f1966b = 0;
            this.f1967c = 0;
        }

        /* synthetic */ b(a aVar, byte b2) {
            this();
        }
    }

    final class c {

        /* renamed from: a reason: collision with root package name */
        public int f1969a;

        /* renamed from: b reason: collision with root package name */
        public int f1970b;

        /* renamed from: c reason: collision with root package name */
        public h f1971c;

        private c() {
            this.f1969a = 0;
            this.f1970b = 0;
            this.f1971c = null;
        }

        /* synthetic */ c(a aVar, byte b2) {
            this();
        }
    }

    public a(byte[] bArr) {
        String a2;
        this.f1956a = bArr;
        C0012a aVar = new C0012a(this, 0);
        int a3 = a(2, c());
        aVar.f1963e = a3;
        int b2 = b(a3 + 4, a(a3));
        aVar.f1962d = b2;
        int a4 = a(b2 + 2, e(b2));
        aVar.f1960b = a(3, a4, bArr.length);
        aVar.f1961c = a(4, a4, bArr.length);
        aVar.f1959a = a(1, a4, bArr.length);
        this.f1958c = aVar;
        C0012a aVar2 = this.f1958c;
        if (aVar2.f1961c == -1) {
            a2 = "Cp1252";
        } else {
            int i = aVar2.f1961c + 4;
            int c2 = ((int) i.c(this.f1956a, i)) - 1;
            a2 = i.a(this.f1956a, i + 4, c2, "Cp1252");
            if (!b()) {
                a2 = "Cp1252";
            }
        }
        this.f1957b = a2;
    }

    private int a(int i) {
        int b2 = i.b(this.f1956a, i);
        new StringBuilder("Number of sentences: ").append(b2);
        return b2;
    }

    private int a(int i, int i2) {
        int i3 = 0;
        while (i < this.f1956a.length && i3 < i2) {
            if (this.f1956a[i] == 0) {
                i3++;
            }
            i++;
        }
        return i;
    }

    private int a(int i, int i2, int i3) {
        int i4 = i2;
        while (i4 < i3) {
            long c2 = i.c(this.f1956a, i4);
            new StringBuilder("Received extension id=").append(c2);
            if (c2 == ((long) i)) {
                return i4;
            }
            int i5 = i4 + 4;
            long c3 = i.c(this.f1956a, i5);
            new StringBuilder("Received payload size: ").append(c3);
            i4 = i5 + ((int) c3) + 4;
        }
        return -1;
    }

    private b a(int i, int i2, ArrayList arrayList) {
        int b2 = b(i);
        int i3 = i + 4;
        g gVar = new g();
        b bVar = new b(this, 0);
        for (int i4 = 0; i4 < b2; i4++) {
            c a2 = a(i3, i2, 2, arrayList);
            gVar.a(a2.f1971c);
            i3 = a2.f1969a;
            i2 = a2.f1970b;
            h hVar = a2.f1971c;
            new StringBuilder("Extracted word: startTime [").append(hVar.getStartTime()).append("] endTime [").append(hVar.getEndTime()).append("] content [").append(hVar.a()).append("]");
        }
        bVar.f1966b = i3;
        bVar.f1967c = i2;
        bVar.f1965a = gVar;
        return bVar;
    }

    private c a(int i, int i2, int i3, ArrayList arrayList) {
        long c2;
        long c3;
        int i4;
        c cVar = new c(this, 0);
        int i5 = i + 2;
        String str = (String) arrayList.get(i.a(this.f1956a, i) - 1);
        if (i3 == 4) {
            c2 = d(i5);
            int i6 = i5 + i3;
            c3 = d(i6);
            i4 = i6 + i3;
        } else {
            c2 = (long) c(i5);
            int i7 = i5 + i3;
            c3 = (long) c(i7);
            i4 = i7 + i3;
        }
        double d2 = 0.0d;
        if (i2 != -1) {
            d2 = (double) i.a(this.f1956a, i2);
            i2 += 2;
        }
        cVar.f1971c = new h(str, c2, c3, d2);
        cVar.f1969a = i4;
        cVar.f1970b = i2;
        return cVar;
    }

    private ArrayList a(C0012a aVar, ArrayList arrayList) {
        int i = 0;
        boolean z = aVar.f1959a != -1;
        int i2 = z ? aVar.f1959a : aVar.f1963e;
        boolean z2 = aVar.f1960b != -1;
        if (z) {
            i2 = i2 + 4 + 4;
        }
        int a2 = a(i2);
        int i3 = i2 + 4;
        int i4 = aVar.f1960b;
        if (z2) {
            i4 = i4 + 4 + 4 + 4 + 4;
        }
        ArrayList arrayList2 = new ArrayList();
        int i5 = i3;
        int i6 = i4;
        int i7 = i5;
        while (i < a2) {
            b a3 = z ? b(i7, i6, arrayList) : a(i7, i6, arrayList);
            new StringBuilder("Number of words in Sentence ").append(i).append(": [").append(a3.f1965a.size()).append("]");
            arrayList2.add(a3.f1965a);
            int i8 = a3.f1966b;
            int i9 = a3.f1967c;
            new StringBuilder("Extracted sentence: [").append(a3.f1965a).append("]");
            i++;
            i7 = i8;
            i6 = i9;
        }
        return arrayList2;
    }

    private static void a(Map map) {
        for (int i = 0; map.remove("CFD" + i) != null; i++) {
        }
        if (map.containsKey("IAL")) {
            map.put("InputAudioLength", map.get("IAL"));
        }
    }

    private static void a(Map map, List list) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < list.size()) {
                String str = (String) map.get("CFD" + i2);
                if (str != null) {
                    try {
                        ((g) list.get(i2)).a(Double.parseDouble(str));
                    } catch (NumberFormatException e2) {
                    }
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private int b(int i) {
        return i.b(this.f1956a, i);
    }

    private int b(int i, int i2) {
        int i3 = 0;
        while (i3 < i2) {
            int i4 = i + 4;
            for (int i5 = 0; i5 < b(i); i5++) {
                i4 += 6;
            }
            i3++;
            i = i4;
        }
        return i;
    }

    private b b(int i, int i2, ArrayList arrayList) {
        int b2 = b(i);
        int i3 = i + 4;
        b bVar = new b(this, 0);
        for (int i4 = 0; i4 < b2; i4++) {
            c a2 = a(i3, i2, 4, arrayList);
            bVar.f1965a.a(a2.f1971c);
            i3 = a2.f1969a;
            i2 = a2.f1970b;
            h hVar = a2.f1971c;
            new StringBuilder("Extracted word: startTime [").append(hVar.getStartTime()).append("] endTime [").append(hVar.getEndTime()).append("] content [").append(hVar.a()).append("]");
        }
        bVar.f1966b = i3;
        bVar.f1967c = i2;
        return bVar;
    }

    private static boolean b() {
        try {
            new byte[1][0] = 20;
            return true;
        } catch (UnsupportedEncodingException e2) {
            return false;
        }
    }

    private int c() {
        int a2 = i.a(this.f1956a, 0);
        new StringBuilder("Number of words: ").append(a2);
        return a2;
    }

    private int c(int i) {
        return i.a(this.f1956a, i);
    }

    private ArrayList c(int i, int i2) {
        ArrayList arrayList = new ArrayList();
        for (int i3 = 0; i3 < i; i3++) {
            int d2 = i.d(this.f1956a, i2);
            String a2 = i.a(this.f1956a, i2, d2, this.f1957b.equals("Cp1252") ? "Windows-1252" : this.f1957b);
            arrayList.add(a2);
            new StringBuilder("Added a word to the list: [").append(a2).append("] offset [").append(i2).append("] len [").append(d2).append("]");
            i2 += d2 + 1;
        }
        return arrayList;
    }

    private long d(int i) {
        return i.c(this.f1956a, i);
    }

    private int e(int i) {
        int a2 = i.a(this.f1956a, i);
        new StringBuilder("Number of key-value pairs: ").append(a2);
        return a2;
    }

    private Map f(int i) {
        int e2 = e(i);
        int i2 = i + 2;
        HashMap hashMap = new HashMap();
        ArrayList c2 = c(e2, i2);
        new StringBuilder("Extracted ").append(c2.size()).append(" words from the set of key-value pairs.");
        for (int i3 = 0; i3 < c2.size(); i3++) {
            String str = (String) c2.get(i3);
            int indexOf = str.indexOf(61);
            if (indexOf == -1) {
                throw new IllegalArgumentException("Received an invalid key-value pair: " + ((String) c2.get(i3)));
            }
            hashMap.put(str.substring(0, indexOf), str.substring(indexOf + 1, str.length()));
        }
        return hashMap;
    }

    public final f a() {
        ArrayList c2 = c(c(), 2);
        new StringBuilder("Found ").append(c2.size()).append(" in word list");
        for (int i = 0; i < c2.size(); i++) {
            c2.get(i);
        }
        ArrayList a2 = a(this.f1958c, c2);
        new StringBuilder("Found ").append(a2.size()).append(" in n-best list");
        for (int i2 = 0; i2 < a2.size(); i2++) {
            a2.get(i2);
        }
        Map f2 = f(this.f1958c.f1962d);
        a(f2, (List) a2);
        a(f2);
        return new f(a2, f2);
    }
}
