package com.motorola.audiomonitor;

import android.os.SystemClock;
import android.util.Log;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class m extends Thread implements k, r {

    /* renamed from: a reason: collision with root package name */
    private static final String f508a = ("AMonitor." + m.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f509b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private final List f510c = new LinkedList();

    /* renamed from: d reason: collision with root package name */
    private final List f511d = new LinkedList();

    /* renamed from: e reason: collision with root package name */
    private boolean f512e = false;

    /* renamed from: f reason: collision with root package name */
    private final Object f513f = new Object();

    /* renamed from: g reason: collision with root package name */
    private final z f514g;

    /* renamed from: h reason: collision with root package name */
    private boolean f515h = false;
    private int i;
    private int j;
    private final AtomicLong k = new AtomicLong(0);
    private final AtomicLong l = new AtomicLong(0);

    public m(z zVar) {
        super("EndOfSpeechDetector");
        this.f514g = zVar;
    }

    private void a(int i2) {
        if (this.i != i2) {
            this.i = i2;
            this.j = 128000 / this.i;
        }
    }

    private void a(GenericRecognizer genericRecognizer) {
        synchronized (this.f510c) {
            ProtectedByteBuffer protectedByteBuffer = (ProtectedByteBuffer) this.f510c.remove(0);
            if (this.f515h) {
                genericRecognizer.reset();
                this.f515h = false;
            }
            n recognize = genericRecognizer.recognize(protectedByteBuffer);
            if (recognize == n.RECOGNIZED) {
                if (f509b) {
                    Log.d(f508a, "end of speech");
                }
                if (this.f514g != null) {
                    this.f514g.a(genericRecognizer.getLastPhraseDuration());
                }
            } else if (recognize == n.SPEECH) {
                if (f509b) {
                    Log.d(f508a, "beginning of speech");
                }
                if (this.f514g != null) {
                    this.f514g.d_();
                }
                synchronized (this.f511d) {
                    for (int size = this.f511d.size(); size > this.j; size--) {
                        this.f511d.remove(0);
                    }
                }
            }
        }
    }

    public void a() {
        if (f509b) {
            Log.d(f508a, "reset");
        }
        synchronized (this.f511d) {
            this.f511d.clear();
        }
        synchronized (this.f510c) {
            this.f510c.clear();
            this.f515h = true;
        }
        this.k.set(0);
        this.l.set(0);
    }

    public void a(long j2) {
        if (f509b) {
            Log.d(f508a, "pause processing for duration=" + j2);
        }
        this.k.set(SystemClock.elapsedRealtime() + j2);
        synchronized (this.f510c) {
            this.f510c.clear();
            this.f515h = true;
        }
    }

    public boolean a(ProtectedByteBuffer protectedByteBuffer) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (this.l.get() > 0) {
            if (this.l.get() < elapsedRealtime) {
                this.l.set(0);
                if (f509b) {
                    Log.d(f508a, "resume add buffer");
                }
            }
            return true;
        }
        synchronized (this.f511d) {
            a(protectedByteBuffer.capacity());
            this.f511d.add(protectedByteBuffer);
        }
        if (this.k.get() > 0) {
            if (this.k.get() < elapsedRealtime) {
                this.k.set(0);
                if (f509b) {
                    Log.d(f508a, "resume processing");
                }
            }
            return true;
        }
        synchronized (this.f510c) {
            if (this.f510c.size() == 50) {
                if (f509b) {
                    Log.w(f508a, "buffer overflow");
                }
                b();
            } else {
                this.f510c.add(protectedByteBuffer);
                synchronized (this.f513f) {
                    this.f513f.notify();
                }
            }
        }
        return true;
    }

    public void b() {
        if (f509b) {
            Log.d(f508a, "stop");
        }
        this.f512e = false;
        interrupt();
        try {
            join(3000);
        } catch (InterruptedException e2) {
        }
        if (f509b) {
            Log.d(f508a, "stop complete");
        }
    }

    public void b(long j2) {
        this.l.set(SystemClock.elapsedRealtime() + j2);
        a(j2);
    }

    public boolean c() {
        boolean z;
        synchronized (this.f511d) {
            z = this.f511d.size() > 0;
        }
        return z;
    }

    public List d() {
        LinkedList linkedList;
        synchronized (this.f511d) {
            linkedList = new LinkedList(this.f511d);
        }
        return linkedList;
    }

    public void run() {
        GenericRecognizer genericRecognizer = new GenericRecognizer();
        genericRecognizer.configure(bz.b());
        if (f509b) {
            Log.d(f508a, "Starting EOS detection");
        }
        this.f512e = true;
        while (this.f512e) {
            if (this.f510c.size() == 0) {
                synchronized (this.f513f) {
                    try {
                        this.f513f.wait();
                    } catch (InterruptedException e2) {
                        if (f509b) {
                            Log.d(f508a, "interrupted");
                        }
                        this.f512e = false;
                    }
                }
            } else {
                a(genericRecognizer);
            }
        }
        genericRecognizer.cleanUp();
        if (f509b) {
            Log.d(f508a, "EOS detection stopped.");
        }
    }
}
