package com.nuance.a.a.a.b.b;

import com.nuance.a.a.a.a.a.a.C0000a;
import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.c.d;

public abstract class b {

    /* renamed from: g reason: collision with root package name */
    private static final C0001a f816g = a.a(b.class);

    /* renamed from: a reason: collision with root package name */
    protected String f817a;

    /* renamed from: b reason: collision with root package name */
    protected short f818b;

    /* renamed from: c reason: collision with root package name */
    protected String f819c;

    /* renamed from: d reason: collision with root package name */
    protected com.nuance.a.a.a.a.b.a.b f820d;

    /* renamed from: e reason: collision with root package name */
    protected C0000a f821e;

    /* renamed from: f reason: collision with root package name */
    protected C0000a f822f;

    protected b(String str, short s, String str2, C0000a aVar, C0000a aVar2) {
        if (f816g.b()) {
            f816g.b((Object) "in NMSPManager() gateway IP [" + str + "] Port [" + s + "]");
        }
        String str3 = null;
        if (str == null) {
            str3 = " gatewayIP is null";
        } else if (str.length() == 0) {
            str3 = " gatewayIP is empty";
        }
        if (s <= 0) {
            str3 = " gatewayPort should be greater than 0";
        }
        if (str3 != null) {
            f816g.e("NMSPManager " + IllegalArgumentException.class.getName() + str3);
            throw new IllegalArgumentException(str3);
        }
        this.f817a = str;
        this.f818b = s;
        this.f819c = str2;
        this.f821e = aVar;
        this.f822f = aVar2;
        this.f820d = new d();
    }

    public final com.nuance.a.a.a.a.b.a.b a() {
        return this.f820d;
    }

    public final void a(C0000a aVar) {
        this.f821e = aVar;
    }

    public final String b() {
        return this.f817a;
    }

    public final void b(C0000a aVar) {
        this.f822f = aVar;
    }

    public final short c() {
        return this.f818b;
    }

    public final String d() {
        return this.f819c;
    }

    public final C0000a e() {
        return this.f821e;
    }

    public final C0000a f() {
        return this.f822f;
    }
}
