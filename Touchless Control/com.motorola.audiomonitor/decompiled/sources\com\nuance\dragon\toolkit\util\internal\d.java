package com.nuance.dragon.toolkit.util.internal;

import com.nuance.dragon.toolkit.util.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public final class d {
    public static Object a(String str, Object obj) {
        if (obj == null) {
            a(str + " must not be null");
        }
        return obj;
    }

    public static String a(String str, String str2) {
        if (str2 == null || str2.length() == 0) {
            a(str + " must not be null or empty");
        }
        return str2;
    }

    public static JSONObject a(String str, JSONObject jSONObject) {
        if (jSONObject != null) {
            return jSONObject;
        }
        throw new JSONException(str + " must not be null");
    }

    public static void a(Object obj, boolean z) {
        a(obj, z, (String) null);
    }

    public static void a(Object obj, boolean z, String str) {
        if (!z) {
            String str2 = obj instanceof String ? (String) obj : obj != null ? e.a(obj.getClass()) : null;
            IllegalStateException illegalStateException = str2 == null ? str == null ? new IllegalStateException() : new IllegalStateException(str) : str == null ? new IllegalStateException(str2 + " is in an invalid state") : new IllegalStateException(str2 + " is in an invalid state: " + str);
            Logger.error(d.class, "State check failed", illegalStateException);
            throw illegalStateException;
        }
    }

    private static void a(String str) {
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException(str);
        Logger.error(d.class, str);
        throw illegalArgumentException;
    }

    public static void a(String str, String str2, boolean z) {
        if (!z) {
            throw new IllegalArgumentException(str + " must be " + str2);
        }
    }

    public static void a(String str, Object... objArr) {
        int length = objArr.length;
        int i = 0;
        while (i < length) {
            if (objArr[i] == null) {
                i++;
            } else {
                return;
            }
        }
        a("At least one of " + str + " must not be null or empty");
    }

    public static void a(String str, String... strArr) {
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            String str2 = strArr[i];
            if (str2 == null || str2.length() <= 0) {
                i++;
            } else {
                return;
            }
        }
        a("At least one of " + str + " must not be null or empty");
    }

    public static String b(String str, String str2) {
        if (str2 != null && str2.length() == 0) {
            a(str + " could be null but must not be empty if non-null");
        }
        return str2;
    }
}
