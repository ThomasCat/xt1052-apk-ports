package com.motorola.audiomonitor;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.os.Handler;
import android.os.HandlerThread;
import android.preference.PreferenceManager;
import android.util.Log;
import com.motorola.audiomonitor.pluginservice.InputPluginService;
import com.motorola.audiomonitor.pluginservice.h;
import com.motorola.audiomonitor.service.d;
import h.k;

public class cc extends HandlerThread {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f405a = ("AMonitor." + cc.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f406b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private final Context f407c;

    /* renamed from: d reason: collision with root package name */
    private final String f408d;

    /* renamed from: e reason: collision with root package name */
    private final Handler f409e;

    /* renamed from: f reason: collision with root package name */
    private final cl f410f;

    /* renamed from: g reason: collision with root package name */
    private final d f411g;

    /* renamed from: h reason: collision with root package name */
    private cm f412h;
    private j i;
    private l j = new cd(this);

    public cc(Context context, String str, cl clVar, d dVar) {
        super(cc.class.getSimpleName());
        if (context == null || str == null || clVar == null) {
            throw new IllegalArgumentException("Invalid argument in TrainingController");
        }
        this.f407c = context;
        this.f408d = str;
        this.f410f = clVar;
        this.f411g = dVar;
        if (f406b) {
            Log.d(f405a, "phrase=" + this.f408d);
        }
        start();
        this.f409e = new Handler(getLooper());
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        if (this.f412h != null) {
            this.f412h.a(z);
            if (z) {
                this.f412h = null;
                if (this.i != null) {
                    this.i.d();
                }
                this.f412h = new cn(this.f407c, this.f410f, this.f411g, this.f408d);
                if (this.i != null) {
                    this.i.a((k) this.f412h);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        this.f412h = new cn(this.f407c, this.f410f, this.f411g, this.f408d);
        h a2 = InputPluginService.a();
        if (a2 == null || a2.f547b == null) {
            this.i = new f(this.f411g != null);
        } else {
            this.i = a2.f547b;
        }
        this.i.a(this.j);
        this.i.a((k) this.f412h);
    }

    /* access modifiers changed from: private */
    public void j() {
        if (f406b) {
            Log.d(f405a, "stopped");
        }
        if (this.i != null) {
            this.i.d();
            this.i.f();
            this.i.c();
            this.i = null;
        }
        if (this.f412h != null) {
            this.f412h.c();
            this.f412h = null;
        }
        this.f409e.removeCallbacksAndMessages(null);
        quit();
    }

    /* access modifiers changed from: private */
    public void k() {
        if (this.f412h != null && this.i != null) {
            this.f412h.a((p) this.i);
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        if (this.f412h != null) {
            this.f412h.a();
        }
    }

    /* access modifiers changed from: private */
    public void m() {
        if (this.f412h != null && this.i != null) {
            this.f412h.b(this.i);
        }
    }

    /* access modifiers changed from: private */
    public void n() {
        if (this.f412h == null || !this.f412h.b()) {
            Log.e(f405a, "No enrollment result available yet.");
            return;
        }
        String str = "customphrase";
        k.c(a.f161c + "custom_acoustic_22132.raw" + ".tmp", a.f161c + "custom_acoustic_22132.raw");
        k.c(a.f161c + "custom_search_22132.raw" + ".tmp", a.f161c + "custom_search_22132.raw");
        k.c(a.f161c + k.d(str) + ".tmp", a.f161c + k.d(str));
        k.c(a.f161c + k.e(str) + ".tmp", a.f161c + k.e(str));
        if (f406b) {
            k.b(a.f161c + "custom_acoustic_22132.raw", "/data/adspd/custom_acoustic_22132.raw");
            k.b(a.f161c + "custom_search_22132.raw", "/data/adspd/custom_search_22132.raw");
            k.b(a.f161c + k.d(str), "/data/adspd/" + k.d(str));
            k.b(a.f161c + k.e(str), "/data/adspd/" + k.e(str));
        }
        if (!k.a(str)) {
            Log.e(f405a, "User data is not available for some reason.");
            str = "customphrase";
        }
        Editor edit = PreferenceManager.getDefaultSharedPreferences(this.f407c).edit();
        edit.putString("key_phrase", str);
        edit.apply();
        if (f406b) {
            Log.d(f405a, "New speaker data committed.");
        }
    }

    public void a() {
        this.f409e.post(new ce(this));
    }

    public void a(boolean z) {
        this.f409e.post(new cj(this, z));
    }

    public void b() {
        this.f409e.post(new cf(this));
        try {
            join();
        } catch (InterruptedException e2) {
        }
    }

    public void c() {
        this.f409e.post(new cg(this));
    }

    public void d() {
        this.f409e.post(new ch(this));
    }

    public void e() {
        this.f409e.post(new ci(this));
    }

    public void f() {
        this.f409e.post(new ck(this));
    }
}
