package com.nuance.dragon.toolkit.recognizer;

import com.nuance.dragon.toolkit.util.internal.g;

public interface Entry {

    public interface Word {
        int getConfidence();

        String getText();
    }

    int getConfidence();

    String getStartRule();

    int getTotalSearchScore();

    g getWords();
}
