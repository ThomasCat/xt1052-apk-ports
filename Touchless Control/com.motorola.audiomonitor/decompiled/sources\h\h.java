package h;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

class h extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ g f2301a;

    h(g gVar) {
        this.f2301a = gVar;
    }

    public void onReceive(Context context, Intent intent) {
        if (intent != null && TextUtils.equals("android.net.conn.INET_CONDITION_ACTION", intent.getAction())) {
            this.f2301a.l = intent.getIntExtra("inetCondition", 0);
            if (g.f2294b) {
                Log.d(g.f2293a, "Inetcondition is :" + this.f2301a.l);
            }
        }
    }
}
