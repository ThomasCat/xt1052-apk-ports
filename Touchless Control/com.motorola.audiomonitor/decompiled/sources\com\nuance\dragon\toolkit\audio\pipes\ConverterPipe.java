package com.nuance.dragon.toolkit.audio.pipes;

import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AbstractAudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.util.WorkerThread;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public abstract class ConverterPipe extends SingleSinkPipe {

    /* renamed from: a reason: collision with root package name */
    private final LinkedList f1150a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final LinkedList f1151b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final LinkedList f1152c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public boolean f1153d;

    /* renamed from: e reason: collision with root package name */
    private boolean f1154e;

    /* renamed from: f reason: collision with root package name */
    private WorkerThread f1155f;

    /* renamed from: g reason: collision with root package name */
    private final Handler f1156g;

    /* renamed from: h reason: collision with root package name */
    private final Handler f1157h;
    private int i;
    private final Object j;
    private final ArrayList k;

    public ConverterPipe() {
        this(0);
    }

    public ConverterPipe(int i2) {
        this.f1150a = new LinkedList();
        this.f1151b = new LinkedList();
        this.f1152c = new LinkedList();
        this.f1155f = new WorkerThread();
        this.f1155f.start();
        this.f1156g = this.f1155f.getHandler();
        this.f1157h = new Handler();
        this.i = i2;
        this.j = new Object();
        this.k = new ArrayList();
    }

    static /* synthetic */ void f(ConverterPipe converterPipe) {
        final ArrayList arrayList;
        synchronized (converterPipe.j) {
            arrayList = new ArrayList(converterPipe.f1150a.size());
            arrayList.addAll(converterPipe.f1150a);
            converterPipe.f1150a.clear();
        }
        if (!arrayList.isEmpty()) {
            if (!converterPipe.f1153d) {
                converterPipe.initialize(((AbstractAudioChunk) arrayList.get(0)).audioType);
                converterPipe.f1153d = true;
            }
            final ArrayList arrayList2 = new ArrayList(arrayList.size());
            arrayList2.addAll(converterPipe.convert((List) arrayList));
            converterPipe.f1157h.post(new Runnable() {
                public final void run() {
                    ConverterPipe.this.f1152c.addAll(arrayList2);
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        it.next();
                        ConverterPipe.this.f1151b.remove(0);
                    }
                    ConverterPipe.this.notifyChunksAvailable();
                    AudioSource e2 = ConverterPipe.this.getConnectedSource();
                    if (!ConverterPipe.this.f1151b.isEmpty()) {
                        return;
                    }
                    if (e2 == null || !e2.isActive()) {
                        ConverterPipe.this.notifySourceClosed();
                    }
                }
            });
        }
    }

    public void chunksAvailable(AudioSource audioSource, AudioSink audioSink) {
        int chunksAvailableForSink = audioSource.getChunksAvailableForSink(audioSink);
        if (chunksAvailableForSink > 0) {
            this.k.ensureCapacity(chunksAvailableForSink);
            audioSource.getAllAudioChunksForSink(audioSink, this.k);
            synchronized (this.j) {
                Iterator it = this.k.iterator();
                while (it.hasNext()) {
                    AbstractAudioChunk abstractAudioChunk = (AbstractAudioChunk) it.next();
                    this.f1150a.add(abstractAudioChunk);
                    this.f1151b.add(abstractAudioChunk);
                    if (this.i > 0) {
                        this.i -= abstractAudioChunk.audioDuration;
                    }
                }
            }
            this.k.clear();
            if (this.i <= 0) {
                this.f1156g.post(new Runnable() {
                    public final void run() {
                        ConverterPipe.f(ConverterPipe.this);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void cleanup();

    /* access modifiers changed from: protected */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.audio.AbstractAudioChunk>, for r7v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.audio.AbstractAudioChunk>] */
    public List convert(List<AbstractAudioChunk> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (AbstractAudioChunk convert : list) {
            AbstractAudioChunk[] convert2 = convert(convert);
            if (convert2 != null) {
                for (AbstractAudioChunk add : convert2) {
                    arrayList.add(add);
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public abstract AbstractAudioChunk[] convert(AbstractAudioChunk abstractAudioChunk);

    public void framesDropped(AudioSource audioSource, AudioSink audioSink) {
        notifyFramesDropped();
    }

    /* access modifiers changed from: protected */
    public void getAllAudioChunks(List list) {
        list.addAll(this.f1152c);
        this.f1152c.clear();
    }

    /* access modifiers changed from: protected */
    public AbstractAudioChunk getAudioChunk() {
        if (this.f1152c.isEmpty()) {
            return null;
        }
        return (AbstractAudioChunk) this.f1152c.remove(0);
    }

    public int getChunksAvailable() {
        return this.f1152c.size();
    }

    /* access modifiers changed from: protected */
    public abstract void initialize(AudioType audioType);

    public boolean isActive() {
        return isSourceActive() || this.f1151b.size() > 0;
    }

    public final void release() {
        d.a((Object) this, !this.f1154e);
        this.f1154e = true;
        this.f1156g.post(new Runnable() {
            public final void run() {
                if (ConverterPipe.this.f1153d) {
                    ConverterPipe.this.f1153d = false;
                    ConverterPipe.this.cleanup();
                }
            }
        });
        if (this.f1155f != null) {
            this.f1155f.stop();
            this.f1155f = null;
        }
    }

    public void sourceClosed(AudioSource audioSource, AudioSink audioSink) {
        if (this.f1151b.isEmpty()) {
            notifySourceClosed();
        }
    }
}
