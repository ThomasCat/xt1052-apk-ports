package com.motorola.audiomonitor.actions.utils;

import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;

class i extends PhoneStateListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ h f241a;

    i(h hVar) {
        this.f241a = hVar;
    }

    public void onCallStateChanged(int i, String str) {
        switch (i) {
            case 1:
                this.f241a.f238f = str;
                break;
        }
        this.f241a.f239g = i;
        if (this.f241a.f239g == 2 && this.f241a.i != null) {
            for (j a2 : this.f241a.i) {
                a2.a();
            }
        } else if (this.f241a.f239g == 1 && this.f241a.i != null) {
            for (j b2 : this.f241a.i) {
                b2.b();
            }
        }
    }

    public void onServiceStateChanged(ServiceState serviceState) {
        this.f241a.f240h = serviceState;
    }
}
