package c;

import android.content.Context;
import android.util.Log;
import com.motorola.audiomonitor.a;
import h.k;
import java.util.Vector;

public class d {

    /* renamed from: a reason: collision with root package name */
    private static final String f118a = ("AMonitor." + d.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f119b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private static final String[] f120c = {"num_errors", "dsp_triggers", "bt_triggers", "triggers", "commands", "num_commands_successful", "num_call", "num_whats_up", "num_whats_that", "num_google", "num_help", "num_voicemail", "num_where_are_you", "noise_mode0", "noise_mode1", "noise_mode2", "noise_mode3", "noise_mode4"};

    public static synchronized void a(Context context) {
        String[] strArr;
        synchronized (d.class) {
            Vector vector = new Vector();
            for (String str : f120c) {
                if (!str.equals("dsp_triggers") && !str.equals("bt_triggers")) {
                    long b2 = k.b(context, "stats_" + str, 0);
                    vector.add(new f(str, Long.toString(b2)));
                    if (f119b) {
                        Log.d(f118a, str + "=" + b2);
                    }
                    k.a(context, "stats_" + str, 0);
                }
            }
            a.a(context, "DAILY_STATS", vector.iterator());
        }
    }

    public static boolean a(Context context, String str) {
        return a(context, str, 1);
    }

    public static synchronized boolean a(Context context, String str, long j) {
        boolean z = false;
        synchronized (d.class) {
            String[] strArr = f120c;
            int length = strArr.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                } else if (strArr[i].equals(str)) {
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
            if (z) {
                k.a(context, "stats_" + str, k.b(context, "stats_" + str, 0) + j);
            }
        }
        return z;
    }
}
