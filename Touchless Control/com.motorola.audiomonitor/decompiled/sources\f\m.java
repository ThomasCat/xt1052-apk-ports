package f;

import android.util.Log;
import com.nuance.dragon.toolkit.elvis.ElvisError;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.ResultListener;
import com.nuance.dragon.toolkit.elvis.ElvisResult;

class m implements ResultListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ i f2248a;

    m(i iVar) {
        this.f2248a = iVar;
    }

    public void onError(ElvisError elvisError) {
        Log.e(i.f2236a, "Recognition result failure : " + elvisError.getReason());
        if (this.f2248a.m != null) {
            this.f2248a.m.a(elvisError.getReason());
        }
    }

    public void onResult(ElvisResult elvisResult) {
        if (this.f2248a.m != null) {
            this.f2248a.m.a(elvisResult.getConfidence(), elvisResult.toJSON());
        }
    }
}
