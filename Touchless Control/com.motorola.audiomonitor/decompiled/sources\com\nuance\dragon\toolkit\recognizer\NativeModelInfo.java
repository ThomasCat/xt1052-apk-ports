package com.nuance.dragon.toolkit.recognizer;

public class NativeModelInfo {

    /* renamed from: a reason: collision with root package name */
    private final String f1996a;

    /* renamed from: b reason: collision with root package name */
    private final int f1997b;

    public NativeModelInfo(String str, int i) {
        this.f1996a = str;
        this.f1997b = i;
    }

    public String getLanguage() {
        return this.f1996a;
    }

    public int getSampleRate() {
        return this.f1997b;
    }
}
