package com.motorola.audiomonitor.service;

import android.os.Handler;
import android.util.Log;

public class AudioDspControl implements c {

    /* renamed from: a reason: collision with root package name */
    private static final String f552a = ("AMonitor." + AudioDspControl.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private Handler f553b;

    /* renamed from: c reason: collision with root package name */
    private boolean f554c = false;

    /* renamed from: d reason: collision with root package name */
    private boolean f555d = false;

    static {
        try {
            System.loadLibrary("adspd");
            System.loadLibrary("adspd-jni");
        } catch (Exception e2) {
            Log.e(f552a, "Failed to load library, exception=" + e2);
        }
    }

    private native int nativeAudioBufCtrl(int i);

    private native int nativeAudioLogCtrl(int i);

    private native synchronized int nativeConnect();

    private native synchronized void nativeDestroy();

    private native synchronized void nativeDisconnect();

    private native int nativeDockState(int i);

    private native int nativeDspVersion();

    private native int nativeEventLogCtrl(int i);

    private native int nativeGetNoiseEstimate(int i);

    private native synchronized void nativeInit();

    private native int nativeLoadAcousticModel(String str);

    private native int nativeLoadFirmware();

    private native int nativeLoadLanguageModel(String str);

    private native int nativeModeTimes();

    private native int nativeRecognitionCtrl(int i, int i2);

    public int a(int i) {
        return nativeEventLogCtrl(i);
    }

    public int a(int i, int i2) {
        return nativeRecognitionCtrl(i, i2);
    }

    public int a(String str) {
        return nativeLoadAcousticModel(str);
    }

    public synchronized void a() {
        if (this.f554c) {
            nativeDestroy();
            this.f554c = false;
        }
    }

    public synchronized void a(Handler handler) {
        this.f553b = handler;
        nativeInit();
        this.f554c = true;
    }

    public synchronized int b() {
        int nativeConnect;
        nativeConnect = nativeConnect();
        if (nativeConnect == 0) {
            this.f555d = true;
        }
        return nativeConnect;
    }

    public int b(int i) {
        return nativeAudioLogCtrl(i);
    }

    public int b(String str) {
        return nativeLoadLanguageModel(str);
    }

    public int c(int i) {
        return nativeAudioBufCtrl(i);
    }

    public synchronized void c() {
        if (this.f555d) {
            nativeDisconnect();
            this.f555d = false;
        }
    }

    public int d() {
        return nativeLoadFirmware();
    }

    public int d(int i) {
        return nativeDockState(i);
    }

    public int e() {
        return nativeDspVersion();
    }

    public int e(int i) {
        return nativeGetNoiseEstimate(i);
    }

    public int f() {
        return nativeModeTimes();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        try {
            c();
            a();
        } finally {
            super.finalize();
        }
    }

    public void handleAcousticModelLoaded(int i) {
        this.f553b.sendMessage(this.f553b.obtainMessage(2, i, 0, null));
    }

    public void handleAudioBufCtrl(int i) {
        this.f553b.sendMessage(this.f553b.obtainMessage(9, i, 0, null));
    }

    public void handleAudioLogCtrl(int i) {
        this.f553b.sendMessage(this.f553b.obtainMessage(8, i, 0, null));
    }

    public void handleDisconnected() {
        this.f553b.sendEmptyMessage(15);
    }

    public void handleDockState(int i) {
        this.f553b.sendMessage(this.f553b.obtainMessage(10, i, 0, null));
    }

    public void handleDspVersion(int i, String str) {
        this.f553b.sendMessage(this.f553b.obtainMessage(13, i, 0, str));
    }

    public void handleEventLogCtrl(int i) {
        this.f553b.sendMessage(this.f553b.obtainMessage(7, i, 0, null));
    }

    public void handleFirmwareLoaded(int i) {
        this.f553b.sendMessage(this.f553b.obtainMessage(0, i, 0, null));
    }

    public void handleInterruptTest(int i) {
        this.f553b.sendMessage(this.f553b.obtainMessage(6, i, 0, null));
    }

    public void handleKeyPhraseRecognized() {
        this.f553b.sendEmptyMessage(1);
    }

    public void handleLanguageModelLoaded(int i) {
        this.f553b.sendMessage(this.f553b.obtainMessage(3, i, 0, null));
    }

    public void handleMicTest(int i) {
        this.f553b.sendMessage(this.f553b.obtainMessage(4, i, 0, null));
    }

    public void handleModeTimes(int i, int[] iArr) {
        this.f553b.sendMessage(this.f553b.obtainMessage(14, i, 0, iArr));
    }

    public void handleNoiseEstimateComplete(int i, int i2, int i3, short[] sArr) {
        this.f553b.sendMessage(this.f553b.obtainMessage(12, i, i3, sArr));
    }

    public void handleRecognitionCtrl(int i) {
        this.f553b.sendMessage(this.f553b.obtainMessage(11, i, 0, null));
    }

    public void handleSensorHubTest(int i) {
        this.f553b.sendMessage(this.f553b.obtainMessage(5, i, 0, null));
    }
}
