package com.motorola.audiomonitor.uis.training;

import android.util.Log;

class q implements Runnable {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ TrainingTriggerRecordingActivity f756a;

    q(TrainingTriggerRecordingActivity trainingTriggerRecordingActivity) {
        this.f756a = trainingTriggerRecordingActivity;
    }

    public void run() {
        if (TrainingTriggerRecordingActivity.f731b) {
            Log.d(TrainingTriggerRecordingActivity.f730a, "Noise estimate requested");
        }
        this.f756a.a(this.f756a.v);
    }
}
