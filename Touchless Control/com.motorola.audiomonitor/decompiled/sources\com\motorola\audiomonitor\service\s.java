package com.motorola.audiomonitor.service;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

@SuppressLint({"HandlerLeak"})
class s extends Handler {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ d f593a;

    public s(d dVar, Looper looper) {
        this.f593a = dVar;
        super(looper);
    }

    public void handleMessage(Message message) {
        synchronized (this.f593a) {
            p q = this.f593a.s;
            this.f593a.s.a(message);
            if (d.f564b) {
                this.f593a.a(message, q, this.f593a.s);
            }
            if (!q.equals(this.f593a.s)) {
                this.f593a.a(this.f593a.s);
            }
            if (message.what >= 2 && message.what < 15 && "connected".equals(this.f593a.s.a())) {
                this.f593a.u();
            }
        }
    }
}
