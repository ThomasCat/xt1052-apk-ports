package com.nuance.dragon.toolkit.audio.nat;

import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.audio.nat.NativeAudio.Recorder;
import com.nuance.dragon.toolkit.audio.sources.RecorderSource;
import com.nuance.dragon.toolkit.util.Logger;

final class b extends RecorderSource {

    /* renamed from: a reason: collision with root package name */
    private final NativeAudio f1110a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public Recorder f1111b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public boolean f1112c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public boolean f1113d;

    b(AudioType audioType, NativeAudio nativeAudio, Handler handler) {
        super(audioType, handler);
        this.f1110a = nativeAudio;
    }

    /* access modifiers changed from: protected */
    public final boolean isCodecSupported(AudioType audioType) {
        return audioType.encoding == Encoding.PCM_16 && audioType.frequency == 16000;
    }

    /* access modifiers changed from: protected */
    public final boolean startRecordingInternal(AudioType audioType) {
        if (this.f1111b != null) {
            Logger.error(this, "Recorder already active");
            return false;
        }
        final Recorder createRecorder = this.f1110a.createRecorder(audioType.frequency);
        this.f1111b = createRecorder;
        final int bufferSizeMs = this.f1111b.getBufferSizeMs();
        final Handler handler = new Handler();
        this.f1111b.start();
        handler.postDelayed(new Runnable() {
            public final void run() {
                while (createRecorder == b.this.f1111b) {
                    AudioChunk read = b.this.f1111b.read();
                    if (read != null) {
                        if (!b.this.f1113d) {
                            b.this.f1113d = true;
                            b.this.handleStarted();
                        }
                        b.this.handleNewAudio(read);
                        continue;
                    }
                    if (read == null) {
                        if (b.this.f1112c) {
                            handler.postDelayed(this, (long) (bufferSizeMs / 2));
                            return;
                        }
                        return;
                    }
                }
            }
        }, (long) (bufferSizeMs / 2));
        this.f1112c = true;
        return true;
    }

    /* access modifiers changed from: protected */
    public final void stopRecordingInternal() {
        AudioChunk read;
        if (this.f1112c) {
            do {
                read = this.f1111b.read();
                if (read != null) {
                    handleNewAudio(read);
                    continue;
                }
            } while (read != null);
            this.f1111b.stop();
            this.f1111b.release();
            this.f1111b = null;
            this.f1112c = false;
            handleSourceClosed();
        }
    }
}
