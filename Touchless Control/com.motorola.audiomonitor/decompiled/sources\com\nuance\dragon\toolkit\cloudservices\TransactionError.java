package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import org.json.JSONObject;

public class TransactionError implements JSONCompliant {

    /* renamed from: a reason: collision with root package name */
    private final int f1377a;

    /* renamed from: b reason: collision with root package name */
    private final String f1378b;

    /* renamed from: c reason: collision with root package name */
    private final String f1379c;

    /* renamed from: d reason: collision with root package name */
    private final String f1380d;

    /* renamed from: e reason: collision with root package name */
    private final int f1381e;

    /* renamed from: f reason: collision with root package name */
    private final String f1382f;

    public class ConnectionErrorCodes {
        public static final int COMMAND_ENDED_UNEXPECTEDLY = 4;
        public static final int COMMAND_IDLE_FOR_TOO_LONG = 5;
        public static final int NETWORK_UNAVAILABLE = 268369920;
        public static final int REMOTE_DISCONNECTION = 3;
        public static final int TIMED_OUT_WAITING_FOR_RESULT = 1;
    }

    public class Types {
        public static final int CANCELED = 0;
        public static final int CONNECTION_ERROR = 1;
        public static final int OTHER_ERROR = 4;
        public static final int QUERY_ERROR = 3;
        public static final int RETRY_ERROR = 2;
    }

    TransactionError(Transaction transaction) {
        this(transaction, 4, null, null, null, 0);
    }

    TransactionError(Transaction transaction, byte b2) {
        this(transaction, 0, null, null, null, 0);
    }

    TransactionError(Transaction transaction, int i) {
        this(transaction, 1, null, null, null, i);
    }

    TransactionError(Transaction transaction, int i, String str, String str2, String str3, int i2) {
        this.f1377a = i;
        this.f1378b = str;
        this.f1379c = str2;
        this.f1380d = str3;
        this.f1381e = i2;
        this.f1382f = transaction.i();
    }

    private TransactionError(JSONObject jSONObject) {
        this.f1377a = jSONObject.getInt("type");
        this.f1378b = jSONObject.optString("text", null);
        this.f1379c = jSONObject.optString("param", null);
        this.f1380d = jSONObject.optString("prompt", null);
        this.f1381e = jSONObject.optInt("code", 0);
        this.f1382f = jSONObject.optString("session", null);
    }

    public static TransactionError createFromJSON(JSONObject jSONObject) {
        return new TransactionError(jSONObject);
    }

    public int getErrorCode() {
        return this.f1381e;
    }

    public String getErrorText() {
        return this.f1378b;
    }

    public String getParameterWithError() {
        return this.f1379c;
    }

    public String getPrompt() {
        return this.f1380d;
    }

    public String getSessionId() {
        return this.f1382f;
    }

    public int getType() {
        return this.f1377a;
    }

    public JSONObject toJSON() {
        b bVar = new b();
        bVar.a("type", (Object) Integer.valueOf(this.f1377a));
        bVar.a("text", (Object) this.f1378b);
        bVar.a("param", (Object) this.f1379c);
        bVar.a("prompt", (Object) this.f1380d);
        bVar.a("code", (Object) Integer.valueOf(this.f1381e));
        return bVar;
    }
}
