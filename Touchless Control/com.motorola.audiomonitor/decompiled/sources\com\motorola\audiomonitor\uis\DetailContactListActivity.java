package com.motorola.audiomonitor.uis;

import a.e;
import android.app.Activity;
import android.app.FragmentManager;
import android.os.Bundle;
import com.motorola.audiomonitor.C0014R;

public class DetailContactListActivity extends Activity implements az {

    /* renamed from: a reason: collision with root package name */
    public static e f623a;

    /* renamed from: b reason: collision with root package name */
    private FragmentManager f624b;

    /* renamed from: c reason: collision with root package name */
    private d f625c;

    public void a() {
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().addFlags(524288);
        setContentView(C0014R.layout.contact_list_view);
        this.f624b = getFragmentManager();
        if (!as.a((az) this) || f623a == null) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        d.f690a = -1;
        d.f692c = true;
        if (this.f625c == null) {
            this.f625c = new d();
            this.f625c.a(f623a);
            f623a = null;
        }
        this.f624b.beginTransaction().replace(C0014R.id.aov_full_contact_list_framelayout, this.f625c, null).commit();
    }
}
