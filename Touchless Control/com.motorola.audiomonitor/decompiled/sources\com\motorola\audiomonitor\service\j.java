package com.motorola.audiomonitor.service;

import android.os.Message;
import android.util.Log;
import com.nuance.dragon.toolkit.audio.pipes.SpeexEncoderPipe;

class j extends p {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ d f576a;

    j(d dVar, String str, String str2) {
        this.f576a = dVar;
        super(dVar, str, str2);
    }

    public void a(Message message) {
        switch (message.what) {
            case 2:
            case 3:
            case 7:
            case 8:
            case 9:
            case 10:
                if (message.arg1 != 0) {
                    this.f576a.b(true);
                    return;
                }
                return;
            case 11:
                if (message.arg1 != 0) {
                    this.f576a.b(true);
                    return;
                } else if (this.f576a.w.what == 108) {
                    this.f576a.s = this.f576a.f567e;
                    return;
                } else {
                    return;
                }
            case 12:
                if (message.arg1 == 0) {
                    if (this.f576a.w.arg1 < this.f576a.z.length) {
                        this.f576a.z[this.f576a.w.arg1] = message.arg2;
                    }
                    if (this.f576a.w.arg1 < this.f576a.A.length) {
                        this.f576a.A[this.f576a.w.arg1] = (short[]) message.obj;
                    }
                    this.f576a.a(this.f576a.w.arg1, message.arg2, (short[]) message.obj);
                    return;
                }
                return;
            case SpeexEncoderPipe.DEFAULT_VAD_BEGIN_LEN /*15*/:
                this.f576a.b(false);
                return;
            case 101:
                this.f576a.s();
                return;
            case 102:
                this.f576a.a(message);
                return;
            case 103:
                this.f576a.a(message);
                return;
            case 104:
                if (this.f576a.x.a(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to control event log");
                    this.f576a.a(message);
                    return;
                }
                return;
            case 105:
                if (this.f576a.x.b(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to control audio log");
                    this.f576a.a(message);
                    return;
                }
                return;
            case 106:
                if (this.f576a.x.c(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to control audio buffering");
                    this.f576a.a(message);
                    return;
                }
                return;
            case 107:
                this.f576a.a(message);
                return;
            case 108:
                if (this.f576a.x.a(3, 0) != 0) {
                    Log.e(d.f563a, "Failed to stop pass-through");
                    this.f576a.a(message);
                    return;
                }
                return;
            case 109:
                if (this.f576a.x.e(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to request noise estimate");
                    this.f576a.a(message);
                    return;
                }
                return;
            case 110:
                this.f576a.a(message);
                return;
            case 111:
                this.f576a.a(message);
                return;
            case 201:
                this.f576a.b(true);
                return;
            case 202:
                if (this.f576a.x.d(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to send dock state change");
                    this.f576a.a(message);
                    return;
                }
                return;
            case 203:
                if (this.f576a.x.a((String) message.obj) != 0) {
                    Log.e(d.f563a, "Failed to load acoustic model");
                    this.f576a.a(message);
                    return;
                }
                return;
            case 204:
                if (this.f576a.x.b((String) message.obj) != 0) {
                    Log.e(d.f563a, "Failed to load language model");
                    this.f576a.a(message);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
