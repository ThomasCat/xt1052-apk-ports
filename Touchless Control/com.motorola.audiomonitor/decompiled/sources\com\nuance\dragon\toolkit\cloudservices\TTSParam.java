package com.nuance.dragon.toolkit.cloudservices;

import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AbstractAudioChunk;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.audio.sources.SingleSinkSource;
import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.LinkedList;
import java.util.List;

public class TTSParam extends Param {

    /* renamed from: c reason: collision with root package name */
    private final a f1360c;

    /* renamed from: d reason: collision with root package name */
    private final Dictionary f1361d;

    final class a extends SingleSinkSource implements com.nuance.a.a.a.b.b.a {

        /* renamed from: b reason: collision with root package name */
        static final /* synthetic */ boolean f1362b = (!TTSParam.class.desiredAssertionStatus());

        /* renamed from: a reason: collision with root package name */
        protected final Handler f1363a;

        /* renamed from: c reason: collision with root package name */
        private final AudioType f1364c;
        /* access modifiers changed from: private */

        /* renamed from: d reason: collision with root package name */
        public final List f1365d;

        /* renamed from: e reason: collision with root package name */
        private boolean f1366e;

        a(AudioType audioType) {
            if (f1362b || audioType != null) {
                this.f1365d = new LinkedList();
                this.f1364c = audioType;
                this.f1366e = false;
                this.f1363a = new Handler();
                return;
            }
            throw new AssertionError();
        }

        private void a(final AudioChunk audioChunk) {
            this.f1363a.post(new Runnable() {
                public final void run() {
                    a.this.f1365d.add(audioChunk);
                    a.this.notifyChunksAvailable();
                }
            });
        }

        public final void a(byte[] bArr, int i, int i2, boolean z) {
            if (!this.f1366e) {
                if (z) {
                    this.f1366e = true;
                }
                if (bArr != null && i2 > 0) {
                    if (this.f1364c.encoding == Encoding.PCM_16) {
                        short[] sArr = new short[(i2 / 2)];
                        for (int i3 = 0; i3 < i2 / 2; i3++) {
                            sArr[i3] = (short) ((((short) bArr[(i3 * 2) + i]) & 255) | ((((short) bArr[((i3 * 2) + i) + 1]) << 8) & 65280));
                        }
                        a(new AudioChunk(this.f1364c, sArr));
                    } else if (this.f1364c.encoding == Encoding.SPEEX) {
                        byte[] bArr2 = new byte[i2];
                        System.arraycopy(bArr, i, bArr2, 0, i2);
                        a(new AudioChunk(this.f1364c, bArr2, 0));
                    }
                }
                if (z) {
                    notifySourceClosed();
                }
            }
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ AbstractAudioChunk getAudioChunk() {
            if (this.f1365d.isEmpty()) {
                return null;
            }
            return (AudioChunk) this.f1365d.remove(0);
        }

        public final AudioType getAudioType() {
            return this.f1364c;
        }

        public final int getChunksAvailable() {
            return this.f1365d.size();
        }

        public final boolean isActive() {
            return !this.f1366e;
        }
    }

    public TTSParam(String str, Dictionary dictionary, AudioType audioType) {
        super(2, str);
        d.a("dict", (Object) dictionary);
        this.f1361d = dictionary;
        this.f1360c = new a(audioType);
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(d dVar, a aVar) {
        d.a((Object) CloudServices.class, ((e) dVar).c().l().equals(this.f1360c.getAudioType()), "The audio type specified in the TTSSpec does not match the player codec specified in the CloudServices");
        dVar.a(this.f1355a, this.f1361d, (com.nuance.a.a.a.b.b.a) this.f1360c);
        return true;
    }

    public AudioSource getAudioSource() {
        return this.f1360c;
    }
}
