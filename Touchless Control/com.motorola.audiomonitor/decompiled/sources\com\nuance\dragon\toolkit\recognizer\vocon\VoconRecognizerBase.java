package com.nuance.dragon.toolkit.recognizer.vocon;

import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.SpeechDetectionListener;
import com.nuance.dragon.toolkit.recognizer.IRecognizer.InitializeListener;
import com.nuance.dragon.toolkit.recognizer.IRecognizer.ReleaseListener;
import com.nuance.dragon.toolkit.recognizer.IRecognizer.ResultListener;
import com.nuance.dragon.toolkit.recognizer.LangModelInfo;
import com.nuance.dragon.toolkit.recognizer.NMTLanguage;
import com.nuance.dragon.toolkit.recognizer.NativeModelInfo;
import com.nuance.dragon.toolkit.recognizer.RecogError;
import com.nuance.dragon.toolkit.recognizer.RecogResult;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

abstract class VoconRecognizerBase extends VoconRecognizer {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final a f2014a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public VoconSink f2015b;

    /* renamed from: c reason: collision with root package name */
    private boolean f2016c = false;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public boolean f2017d = false;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public boolean f2018e = false;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public boolean f2019f = false;

    /* renamed from: g reason: collision with root package name */
    private final Handler f2020g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public final Handler f2021h;
    /* access modifiers changed from: private */
    public ResultListener i;
    /* access modifiers changed from: private */
    public AudioChunk j;
    /* access modifiers changed from: private */
    public int k = 0;
    /* access modifiers changed from: private */
    public int l;

    public class VoconSink extends AudioSink {
        public VoconSink() {
        }

        /* access modifiers changed from: private */
        public void a(RecogError recogError) {
            if (VoconRecognizerBase.this.f2015b == this) {
                VoconRecognizerBase.this.f2015b = null;
                if (VoconRecognizerBase.this.i != null) {
                    VoconRecognizerBase.this.i.onError(recogError);
                    VoconRecognizerBase.this.i = null;
                }
            }
        }

        static /* synthetic */ void a(VoconSink voconSink, RecogResult recogResult) {
            if (VoconRecognizerBase.this.f2015b == voconSink) {
                VoconRecognizerBase.this.f2015b = null;
                if (VoconRecognizerBase.this.i != null) {
                    VoconRecognizerBase.this.i.onResult(recogResult);
                    VoconRecognizerBase.this.i = null;
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public final void a(boolean z) {
            if (VoconRecognizerBase.this.f2019f) {
                VoconRecognizerBase.this.f2019f = false;
                if (VoconRecognizerBase.this.f2017d) {
                    new StringBuilder("Stop current sink with abort: ").append(z);
                    VoconRecognizerBase.this.f2014a.a(z);
                    final RecogResult recogResult = null;
                    if (!z) {
                        recogResult = VoconRecognizerBase.this.f2014a.c();
                    }
                    if (!z) {
                        VoconRecognizerBase.this.f2021h.post(new Runnable() {
                            public final void run() {
                                if (recogResult == null) {
                                    VoconSink.this.a(new RecogError(2));
                                } else {
                                    VoconSink.a(VoconSink.this, recogResult);
                                }
                            }
                        });
                    }
                }
            }
        }

        public void chunksAvailable(AudioSource audioSource) {
        }

        public void framesDropped(AudioSource audioSource) {
            if (VoconRecognizerBase.this.f2015b == this) {
                a(new RecogError(1));
            }
        }

        public int getMoreData(short[] sArr, int i) {
            int i2 = 0;
            while (true) {
                if (i2 >= i) {
                    break;
                }
                if (VoconRecognizerBase.this.j == null) {
                    VoconRecognizerBase.this.j = (AudioChunk) getConnectedSource().getAudioChunkForSink(this);
                    if (VoconRecognizerBase.this.j == null) {
                        break;
                    }
                    VoconRecognizerBase.this.k = 0;
                }
                if (VoconRecognizerBase.this.j != null) {
                    if (VoconRecognizerBase.this.j.audioShorts == null) {
                        VoconRecognizerBase.this.j = null;
                        break;
                    }
                    int i3 = i - i2;
                    int length = VoconRecognizerBase.this.j.audioShorts.length - VoconRecognizerBase.this.k;
                    if (length < i3) {
                        System.arraycopy(VoconRecognizerBase.this.j.audioShorts, VoconRecognizerBase.this.k, sArr, i2, length);
                        int i4 = length + i2;
                        VoconRecognizerBase.this.j = null;
                        i2 = i4;
                    } else {
                        System.arraycopy(VoconRecognizerBase.this.j.audioShorts, VoconRecognizerBase.this.k, sArr, i2, i3);
                        int i5 = i2 + i3;
                        VoconRecognizerBase.a(VoconRecognizerBase.this, i3);
                        i2 = i5;
                    }
                }
            }
            return i2;
        }

        public void sourceClosed(AudioSource audioSource) {
            if (VoconRecognizerBase.this.f2015b == this) {
                a(false);
            }
        }

        public void stopAudio() {
            disconnectAudioSource();
        }
    }

    VoconRecognizerBase(a aVar, Handler handler) {
        this.f2014a = aVar;
        this.f2015b = new VoconSink();
        this.l = 0;
        this.f2021h = new Handler();
        if (handler == null) {
            this.f2020g = getWorkerThreadHandler();
        } else {
            this.f2020g = handler;
        }
    }

    static /* synthetic */ int a(VoconRecognizerBase voconRecognizerBase, int i2) {
        int i3 = voconRecognizerBase.k + i2;
        voconRecognizerBase.k = i3;
        return i3;
    }

    public boolean addVoconContext(String str) {
        d.a((Object) this, !this.f2018e);
        d.a((Object) this, this.f2016c);
        d.a("ctxFileName", (Object) str);
        return this.f2014a.c(str);
    }

    public void cancelLocalRecognition() {
        d.a((Object) this, !this.f2018e);
        d.a((Object) this, this.f2016c);
        if (this.f2015b != null) {
            VoconSink voconSink = this.f2015b;
            this.f2015b.a(true);
            this.f2015b.a(new RecogError(0));
            if (voconSink == this.f2015b) {
                this.f2015b = null;
            }
        }
    }

    public List getAvailableModels() {
        NMTLanguage[] nMTLanguageArr;
        ArrayList arrayList = new ArrayList();
        this.f2014a.a((List) arrayList);
        ArrayList arrayList2 = new ArrayList(arrayList.size());
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            NativeModelInfo nativeModelInfo = (NativeModelInfo) it.next();
            int sampleRate = nativeModelInfo.getSampleRate() * 1000;
            if (sampleRate == 16000) {
                for (NMTLanguage nMTLanguage : b.ALL_LANGUAGES) {
                    if (nMTLanguage.name.equals(nativeModelInfo.getLanguage())) {
                        arrayList2.add(new LangModelInfo(nMTLanguage, sampleRate));
                    }
                }
            }
        }
        return arrayList2;
    }

    public List getAvailableModels(int i2) {
        List availableModels = getAvailableModels();
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= availableModels.size()) {
                return availableModels;
            }
            if (((LangModelInfo) availableModels.get(i4)).frequencyHz != i2) {
                availableModels.remove(i4);
                i4--;
            }
            i3 = i4 + 1;
        }
    }

    public List getAvailableModels(NMTLanguage nMTLanguage) {
        List availableModels = getAvailableModels();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= availableModels.size()) {
                return availableModels;
            }
            if (!((LangModelInfo) availableModels.get(i3)).language.equals(nMTLanguage)) {
                availableModels.remove(i3);
                i3--;
            }
            i2 = i3 + 1;
        }
    }

    public void initialize(VoconConfig voconConfig) {
        initialize(voconConfig, null);
    }

    public void initialize(final VoconConfig voconConfig, final InitializeListener initializeListener) {
        boolean z = false;
        d.a((Object) this, !this.f2018e);
        if (!this.f2016c) {
            z = true;
        }
        d.a((Object) this, z, "Already initialized");
        d.a("config", (Object) voconConfig);
        this.f2016c = true;
        this.f2020g.post(new Runnable() {
            public final void run() {
                String c2 = voconConfig.c();
                int d2 = voconConfig.d();
                String a2 = voconConfig.a();
                String b2 = voconConfig.b();
                if (a2 == null && c2 != null) {
                    a2 = VoconRecognizerBase.this.f2014a.a(c2, d2 / 1000);
                }
                if (b2 == null && c2 != null) {
                    VoconRecognizerBase.this.f2014a.a(c2);
                }
                final boolean b3 = VoconRecognizerBase.this.f2014a.b(a2);
                if (b3) {
                    VoconRecognizerBase.this.f2017d = true;
                }
                VoconRecognizerBase.this.f2021h.post(new Runnable() {
                    public final void run() {
                        if (initializeListener != null) {
                            initializeListener.onLoaded(VoconRecognizerBase.this, b3);
                        }
                    }
                });
            }
        });
    }

    public void release() {
        release(null);
    }

    public void release(final ReleaseListener releaseListener) {
        d.a((Object) this, !this.f2018e);
        if (!this.f2016c) {
            this.f2018e = true;
            if (releaseListener != null) {
                releaseListener.onReleased(this);
                return;
            }
            return;
        }
        if (this.f2019f) {
            cancelLocalRecognition();
        }
        this.f2020g.post(new Runnable() {
            public final void run() {
                if (VoconRecognizerBase.this.f2017d) {
                    VoconRecognizerBase.this.f2014a.a();
                }
                VoconRecognizerBase.this.f2017d = false;
                VoconRecognizerBase.this.f2018e = true;
                if (releaseListener != null) {
                    VoconRecognizerBase.this.f2021h.post(new Runnable() {
                        public final void run() {
                            releaseListener.onReleased(VoconRecognizerBase.this);
                        }
                    });
                }
            }
        });
    }

    public boolean removeVoconContext() {
        d.a((Object) this, !this.f2018e);
        d.a((Object) this, this.f2016c);
        return this.f2014a.b();
    }

    public void startLocalRecognition(AudioSource audioSource, SpeechDetectionListener speechDetectionListener, ResultListener resultListener) {
        d.a((Object) this, !this.f2018e);
        d.a((Object) this, this.f2016c);
        d.a("audioSource", (Object) audioSource);
        d.a("speechDetectionListener", (Object) speechDetectionListener);
        this.i = resultListener;
        if (!this.f2019f) {
            this.f2019f = true;
            if (this.f2015b == null) {
                this.f2015b = new VoconSink();
            }
            this.f2015b.connectAudioSource(audioSource);
            this.f2020g.post(new Runnable() {
                public final void run() {
                    boolean z = false;
                    if (VoconRecognizerBase.this.f2017d) {
                        z = VoconRecognizerBase.this.f2014a.a((Object) VoconRecognizerBase.this.f2015b, VoconRecognizerBase.this.l);
                    }
                    if (VoconRecognizerBase.this.i != null && !z) {
                        VoconRecognizerBase.this.f2021h.post(new Runnable() {
                            public final void run() {
                                VoconRecognizerBase.this.f2019f = false;
                                VoconRecognizerBase.this.i.onError(new RecogError(3));
                            }
                        });
                    }
                }
            });
        }
    }

    public void stopLocalRecognition() {
        d.a((Object) this, !this.f2018e);
        d.a((Object) this, this.f2016c);
        if (this.f2015b != null) {
            this.f2015b.a(false);
        }
    }
}
