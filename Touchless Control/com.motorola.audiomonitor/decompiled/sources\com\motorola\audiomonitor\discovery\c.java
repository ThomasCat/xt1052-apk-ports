package com.motorola.audiomonitor.discovery;

import android.app.AlarmManager;
import android.app.Notification.BigTextStyle;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.util.Log;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.uis.SettingSwitchActivity;
import h.d;
import h.j;
import h.k;
import java.util.Map;

public class c {

    /* renamed from: a reason: collision with root package name */
    private static final String f482a = ("AMonitor." + c.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static c f483b = null;

    /* renamed from: c reason: collision with root package name */
    private static final Object f484c = new Object();

    /* renamed from: d reason: collision with root package name */
    private final Context f485d;

    /* renamed from: e reason: collision with root package name */
    private final SharedPreferences f486e = this.f485d.getSharedPreferences("discovery_prefs", 0);

    /* renamed from: f reason: collision with root package name */
    private final j f487f = new j(this.f485d);

    private c(Context context) {
        this.f485d = context;
    }

    public static c a(Context context) {
        c cVar;
        synchronized (f484c) {
            if (context == null) {
                cVar = null;
            } else {
                if (f483b == null) {
                    f483b = new c(context);
                }
                cVar = f483b;
            }
        }
        return cVar;
    }

    private void a(b bVar, long j) {
        AlarmManager alarmManager = (AlarmManager) this.f485d.getSystemService("alarm");
        Intent intent = new Intent("com.motorola.audiomonitor.action.SHOW_DISCOVERY_NOTIFICATION");
        intent.putExtra("com.motorola.audiomonitor.extra.DISCOVERY_MODE", bVar.name());
        alarmManager.set(1, j, PendingIntent.getBroadcast(this.f485d, h(), intent, 134217728));
    }

    private int c(b bVar) {
        return this.f486e.getInt(e(bVar), 0);
    }

    private void d(b bVar) {
        String e2 = e(bVar);
        int i = this.f486e.getInt(e2, 0) + 1;
        Editor edit = this.f486e.edit();
        edit.putInt(e2, i);
        edit.apply();
    }

    private String e(b bVar) {
        return bVar.name() + "_count";
    }

    private long f() {
        long j = this.f486e.getLong("setup_complete_time", -1);
        return j < 0 ? j : System.currentTimeMillis() - j;
    }

    private String f(b bVar) {
        Resources resources = this.f485d.getResources();
        String string = resources.getString(C0014R.string.discovery_greeting);
        String str = null;
        String string2 = resources.getString(C0014R.string.discovery_default);
        Map a2 = this.f487f.a();
        if (a2 != null) {
            str = (String) a2.get("data2");
            if (str == null) {
                str = (String) a2.get("display_name");
            }
        }
        switch (d.f488a[bVar.ordinal()]) {
            case 1:
                String string3 = resources.getString(C0014R.string.discovery_setup_wizard_complete);
                return (str == null || str.length() > 10) ? string3 : string + " " + str + ". " + string3;
            case 2:
                String string4 = resources.getString(C0014R.string.discovery_drive_mode_activated);
                return (str == null || str.length() > 10) ? string4 : string + " " + str + ". " + string4;
            case 3:
                return resources.getString(C0014R.string.discovery_training_delay);
            default:
                Log.w(f482a, "Unknown mode, sending back default text");
                return string2;
        }
    }

    private long g() {
        long j = this.f486e.getLong("training_delayed_time", -1);
        return j < 0 ? j : System.currentTimeMillis() - j;
    }

    private boolean g(b bVar) {
        if (k.b(this.f485d, "opt_out", false)) {
            return false;
        }
        return (!d.b(this.f485d.getApplicationContext()) || !k.a("customphrase")) && !h(bVar);
    }

    private int h() {
        int i = this.f486e.getInt("request_id", 0);
        Editor edit = this.f486e.edit();
        edit.putInt("request_id", i + 1);
        edit.apply();
        return i;
    }

    private boolean h(b bVar) {
        int c2 = c(bVar);
        switch (d.f488a[bVar.ordinal()]) {
            case 1:
                if (c2 < 3) {
                    return false;
                }
                break;
            case 2:
                if (c2 < 1) {
                    return false;
                }
                break;
            case 3:
                if (c2 < 1) {
                    return false;
                }
                break;
            default:
                Log.w(f482a, "Unknown mode, marking all notifications as shown");
                break;
        }
        return true;
    }

    public boolean a() {
        synchronized (f484c) {
            ((NotificationManager) this.f485d.getSystemService("notification")).cancel(C0014R.drawable.vc_notify_discovery);
        }
        return true;
    }

    public boolean a(b bVar) {
        boolean z;
        synchronized (f484c) {
            if (!g(bVar)) {
                z = false;
            } else {
                long currentTimeMillis = System.currentTimeMillis();
                int c2 = c(bVar);
                switch (d.f488a[bVar.ordinal()]) {
                    case 1:
                        long f2 = f();
                        switch (c2) {
                            case 0:
                                a(bVar, currentTimeMillis);
                            case 1:
                                if (f2 < 0) {
                                    Log.w(f482a, "No setup complete time, skipping notification");
                                    z = false;
                                    break;
                                } else if (f2 > 86400000) {
                                    a(bVar, currentTimeMillis);
                                } else {
                                    a(bVar, currentTimeMillis + (86400000 - f2));
                                }
                            case 2:
                                if (f2 < 0) {
                                    Log.w(f482a, "No setup complete time, skipping notification");
                                    z = false;
                                    break;
                                } else if (f2 > 604800000) {
                                    a(bVar, currentTimeMillis);
                                } else {
                                    a(bVar, currentTimeMillis + (604800000 - f2));
                                }
                        }
                        break;
                    case 2:
                        a(bVar, currentTimeMillis + 600000);
                    case 3:
                        long g2 = g();
                        if (g2 < 0) {
                            Log.w(f482a, "No training delayed time, skipping notification");
                            z = false;
                            break;
                        } else if (g2 > 43200000) {
                            a(bVar, currentTimeMillis);
                        } else {
                            a(bVar, currentTimeMillis + (43200000 - g2));
                        }
                    default:
                        Log.w(f482a, "Unknown discovery mode, skip scheduling notification");
                }
                z = true;
            }
        }
        return z;
    }

    public boolean a(boolean z) {
        Editor edit = this.f486e.edit();
        edit.putBoolean("drive_mode_activated", z);
        edit.apply();
        return true;
    }

    public boolean b() {
        synchronized (f484c) {
            if (f() > 0 && !h(b.SETUP_WIZARD_COMPLETE)) {
                a(b.SETUP_WIZARD_COMPLETE);
            }
            if (!h(b.DRIVE_MODE_ACTIVATED)) {
                a(false);
            }
            if (g() > 0 && !h(b.TRAINING_DELAYED)) {
                a(b.TRAINING_DELAYED);
            }
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return true;
     */
    public boolean b(b bVar) {
        synchronized (f484c) {
            if (!g(bVar)) {
                return false;
            }
            NotificationManager notificationManager = (NotificationManager) this.f485d.getSystemService("notification");
            PendingIntent activity = PendingIntent.getActivity(this.f485d.getApplicationContext(), 0, new Intent(this.f485d.getApplicationContext(), SettingSwitchActivity.class), 0);
            String string = this.f485d.getResources().getString(C0014R.string.app_name);
            String f2 = f(bVar);
            BigTextStyle bigTextStyle = new BigTextStyle(new Builder(this.f485d.getApplicationContext()).setSmallIcon(C0014R.drawable.vc_notify_discovery).setAutoCancel(true).setContentTitle(string).setContentText(f2).setTicker(f2).setContentIntent(activity));
            bigTextStyle.bigText(f2);
            notificationManager.notify(C0014R.drawable.vc_notify_discovery, bigTextStyle.build());
            d(bVar);
            if (bVar == b.SETUP_WIZARD_COMPLETE && !h(b.SETUP_WIZARD_COMPLETE)) {
                a(bVar);
            }
        }
    }

    public boolean c() {
        return this.f486e.getBoolean("drive_mode_activated", false);
    }

    public boolean d() {
        Editor edit = this.f486e.edit();
        edit.putLong("setup_complete_time", System.currentTimeMillis());
        edit.apply();
        return true;
    }

    public boolean e() {
        Editor edit = this.f486e.edit();
        edit.putLong("training_delayed_time", System.currentTimeMillis());
        edit.apply();
        return true;
    }
}
