package com.nuance.dragon.toolkit.speechkit;

import android.content.Context;
import com.nuance.dragon.toolkit.cloudservices.CloudConfig;
import com.nuance.dragon.toolkit.cloudservices.CloudServices;
import com.nuance.dragon.toolkit.cloudservices.recognizer.RecogSpec;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.speechkit.SKCloudRecognizer.Listener;
import com.nuance.dragon.toolkit.util.internal.d;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer;
import com.nuance.dragon.toolkit.vocalizer.VocalizerConfig;

public final class SpeechKit {

    /* renamed from: a reason: collision with root package name */
    private boolean f2097a;

    /* renamed from: b reason: collision with root package name */
    private final CloudServices f2098b;

    /* renamed from: c reason: collision with root package name */
    private final Vocalizer f2099c;

    /* renamed from: d reason: collision with root package name */
    private final CloudConfig f2100d;

    public SpeechKit(CloudConfig cloudConfig, VocalizerConfig vocalizerConfig, Context context) {
        d.a("context", (Object) context);
        d.a("cloudConfig and vocalizerConfig", cloudConfig, vocalizerConfig);
        Context applicationContext = context.getApplicationContext();
        if (cloudConfig != null) {
            this.f2100d = cloudConfig;
            this.f2098b = CloudServices.createCloudServices(applicationContext, cloudConfig);
        } else {
            this.f2100d = null;
            this.f2098b = null;
        }
        if (vocalizerConfig != null) {
            this.f2099c = Vocalizer.createVocalizer(new FileManager(applicationContext, ".jpg", "vocalizer", "vocalizer"));
            this.f2099c.load(vocalizerConfig);
            return;
        }
        this.f2099c = null;
    }

    public final SKCloudRecognizer createCloudRecognizer(RecogSpec recogSpec, int i, Listener listener) {
        boolean z = true;
        d.a((Object) this, !this.f2097a);
        if (this.f2098b == null) {
            z = false;
        }
        d.a((Object) this, z);
        d.a("recogSpec", (Object) recogSpec);
        d.a("listener", (Object) listener);
        return new SKCloudRecognizer(this.f2098b, recogSpec, i, null, false, listener);
    }

    public final SKCloudRecognizer createCloudRecognizer(RecogSpec recogSpec, int i, String str, boolean z, Listener listener) {
        boolean z2 = true;
        d.a((Object) this, !this.f2097a);
        if (this.f2098b == null) {
            z2 = false;
        }
        d.a((Object) this, z2);
        d.a("recogSpec", (Object) recogSpec);
        d.a("fileName", (Object) str);
        d.a("listener", (Object) listener);
        return new SKCloudRecognizer(this.f2098b, recogSpec, i, str, z, listener);
    }

    public final SKCloudVocalizer createCloudVocalizer(SKCloudVocalizer.Listener listener) {
        boolean z = true;
        d.a((Object) this, !this.f2097a);
        if (this.f2098b == null) {
            z = false;
        }
        d.a((Object) this, z);
        return new SKCloudVocalizer(this.f2098b, this.f2100d, listener);
    }

    public final SKVocalizer createVocalizer(SKVocalizer.Listener listener) {
        boolean z = true;
        d.a((Object) this, !this.f2097a);
        if (this.f2099c == null) {
            z = false;
        }
        d.a((Object) this, z);
        return new SKVocalizer(this.f2099c, listener);
    }

    public final SKPrompt createVocalizerPrompt(String str) {
        boolean z = true;
        d.a((Object) this, !this.f2097a);
        if (this.f2099c == null) {
            z = false;
        }
        d.a((Object) this, z);
        d.a("text", (Object) str);
        return new b(this.f2099c, str);
    }

    public final String getSessionID() {
        if (this.f2098b != null) {
            return this.f2098b.getSessionID();
        }
        return null;
    }

    public final void release() {
        d.a((Object) this, !this.f2097a);
        if (this.f2098b != null) {
            this.f2098b.release();
        }
        if (this.f2099c != null) {
            this.f2099c.release();
        }
        this.f2097a = true;
    }
}
