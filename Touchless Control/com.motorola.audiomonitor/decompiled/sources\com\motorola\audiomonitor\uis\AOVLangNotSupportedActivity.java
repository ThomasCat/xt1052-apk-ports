package com.motorola.audiomonitor.uis;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.motorola.audiomonitor.C0014R;

public class AOVLangNotSupportedActivity extends Activity implements OnClickListener {
    public void finish() {
        super.finish();
        overridePendingTransition(0, C0014R.anim.activity_close_exit);
    }

    public void onClick(View view) {
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(C0014R.layout.aov_welcome);
        TextView textView = (TextView) findViewById(C0014R.id.confirm);
        textView.setOnClickListener(this);
        textView.setText(17039370);
        View inflate = LayoutInflater.from(this).inflate(C0014R.layout.welcome_confirm, (FrameLayout) findViewById(C0014R.id.welcome_body_layout));
        ((TextView) inflate.findViewById(C0014R.id.text_body_title)).setText(C0014R.string.unsupported_language_title);
        ((TextView) inflate.findViewById(C0014R.id.text_body_desc)).setText(C0014R.string.unsupported_language_desc);
        ((TextView) inflate.findViewById(C0014R.id.privacy)).setVisibility(8);
    }
}
