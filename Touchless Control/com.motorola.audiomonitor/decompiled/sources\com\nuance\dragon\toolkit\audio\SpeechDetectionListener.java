package com.nuance.dragon.toolkit.audio;

public interface SpeechDetectionListener {
    void onEndOfSpeech();

    void onStartOfSpeech();
}
