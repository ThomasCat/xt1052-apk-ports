package com.motorola.audiomonitor;

import android.os.SystemClock;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.atomic.AtomicLong;
import org.json.JSONException;

public class ay implements k {

    /* renamed from: a reason: collision with root package name */
    private static final String f324a = ("AMonitor." + ay.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f325b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private static int f326c = 2000;

    /* renamed from: d reason: collision with root package name */
    private z f327d;

    /* renamed from: e reason: collision with root package name */
    private final q f328e;

    /* renamed from: f reason: collision with root package name */
    private final int f329f;

    /* renamed from: g reason: collision with root package name */
    private HttpURLConnection f330g;

    /* renamed from: h reason: collision with root package name */
    private final AtomicLong f331h;

    public ay(z zVar, int i) {
        this(zVar, i, new FlacEncoder(i, 1));
    }

    public ay(z zVar, int i, q qVar) {
        this.f331h = new AtomicLong(0);
        this.f327d = zVar;
        this.f329f = i;
        this.f328e = qVar;
        int a2 = this.f328e.a();
        if (f325b) {
            Log.i(f324a, "FLAC encoder max bufferSize = " + a2);
        }
        new Thread(new az(this)).start();
    }

    private void a(InputStream inputStream) {
        byte[] bArr = new byte[1024];
        StringBuilder sb = new StringBuilder();
        for (boolean z = true; z; z = false) {
            int read = inputStream.read(bArr);
            if (read == -1) {
            }
            sb.append(new String(bArr, 0, read));
        }
        String sb2 = sb.toString();
        if (this.f327d != null) {
            ac acVar = null;
            try {
                acVar = ca.a(sb2);
            } catch (JSONException e2) {
                Log.e(f324a, "JSON parsing failure");
            }
            if (acVar == null || !acVar.b()) {
                this.f327d.a(3);
            } else {
                this.f327d.a(acVar);
            }
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        try {
            URL url = new URL("https://www.google.com/speech-api/v1/recognize?xjerr=1&client=chromium&maxresults=5&lang=" + a.n);
            try {
                if (f325b) {
                    Log.d(f324a, "Create connection");
                }
                synchronized (this) {
                    this.f330g = (HttpURLConnection) url.openConnection();
                }
                this.f330g.setConnectTimeout(f326c);
                this.f330g.setDoOutput(true);
                this.f330g.setChunkedStreamingMode(0);
                this.f330g.setRequestProperty("Content-Type", "audio/x-flac; rate=" + this.f329f);
                if (f325b) {
                    Log.d(f324a, "Connecting...");
                }
                OutputStream outputStream = this.f330g.getOutputStream();
                if (f325b) {
                    Log.d(f324a, "Start encoding FLAC...");
                }
                this.f328e.a(outputStream);
                outputStream.flush();
                BufferedInputStream bufferedInputStream = new BufferedInputStream(this.f330g.getInputStream());
                int responseCode = this.f330g.getResponseCode();
                if (f325b) {
                    Log.d(f324a, "Response Code=" + responseCode);
                }
                if (responseCode == 200) {
                    a((InputStream) bufferedInputStream);
                } else if (this.f327d != null) {
                    this.f327d.a(2);
                }
                if (this.f330g != null) {
                    this.f330g.disconnect();
                }
                synchronized (this) {
                    this.f330g = null;
                }
            } catch (NullPointerException e2) {
                try {
                    Log.e(f324a, "Cancel during connect " + e2.toString());
                    if (this.f327d != null) {
                        this.f327d.a(1);
                    }
                    if (this.f330g != null) {
                        this.f330g.disconnect();
                    }
                    synchronized (this) {
                        this.f330g = null;
                    }
                } catch (Throwable th) {
                    if (this.f330g != null) {
                        this.f330g.disconnect();
                    }
                    synchronized (this) {
                        this.f330g = null;
                        throw th;
                    }
                }
            } catch (IOException e3) {
                Log.e(f324a, "Error during POST " + e3.toString());
                if (this.f327d != null) {
                    this.f327d.a(1);
                }
                if (this.f330g != null) {
                    this.f330g.disconnect();
                }
                synchronized (this) {
                    this.f330g = null;
                }
            }
        } catch (MalformedURLException e4) {
            Log.e(f324a, "Malformed URL. Do not upload.");
        }
    }

    public void a() {
        if (this.f328e != null) {
            this.f328e.b();
        }
    }

    public boolean a(ProtectedByteBuffer protectedByteBuffer) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (this.f331h.get() > 0) {
            if (this.f331h.get() < elapsedRealtime) {
                this.f331h.set(0);
                if (f325b) {
                    Log.d(f324a, "Network STT: resume add buffer");
                }
            }
            return true;
        }
        if (this.f328e != null) {
            this.f328e.a(protectedByteBuffer);
        }
        return true;
    }

    public void b() {
        synchronized (this) {
            if (this.f330g != null) {
                this.f330g.disconnect();
            }
        }
        if (this.f328e != null) {
            this.f328e.c();
            this.f327d = null;
        }
    }
}
