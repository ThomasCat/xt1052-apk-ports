package com.nuance.dragon.toolkit.elvis;

import java.util.regex.Matcher;

class d {
    public static final ElvisLanguage[] ALL_LANGUAGES = {CANTONESE_TRADITIONAL, EUROPEAN_FRENCH, FINNISH, BRITISH_ENGLISH, AUSTRALIAN_ENGLISH, TURKISH, JAPANESE, LATIN_AMERICAN_SPANISH, SWEDISH, RUSSIAN, EUROPEAN_PORTUGUESE, POLISH, GERMAN, CANTONESE_ENGLISH_TRADITIONAL, DUTCH, MANDARIN_TRADITIONAL, MANDARIN_ENGLISH_TRADITIONAL, CANADIAN_FRENCH, US_SPANISH, UNITED_STATES_ENGLISH, EUROPEAN_SPANISH, BRAZILIAN_PORTUGUESE, UKRAINIAN, CANTONESE_SIMPLIFIED, DANISH, NORWEGIAN, ITALIAN, MANDARIN_ENGLISH_SIMPLIFIED, CANTONESE_ENGLISH_SIMPLIFIED, GREEK, KOREAN};
    public static final ElvisLanguage AUSTRALIAN_ENGLISH = new ElvisLanguage("enau", "Australian English");
    public static final ElvisLanguage BRAZILIAN_PORTUGUESE = new ElvisLanguage("ptbr", "Brazilian Portuguese");
    public static final ElvisLanguage BRITISH_ENGLISH = new ElvisLanguage("engb", "British English");
    public static final ElvisLanguage CANADIAN_FRENCH = new ElvisLanguage("frca", "Canadian French");
    public static final ElvisLanguage CANTONESE_ENGLISH_SIMPLIFIED = new ElvisLanguage("zect", "Cantonese/English Simplified");
    public static final ElvisLanguage CANTONESE_ENGLISH_TRADITIONAL = new ElvisLanguage("zehk", "Cantonese/English Traditional");
    public static final ElvisLanguage CANTONESE_SIMPLIFIED = new ElvisLanguage("zhct", "Cantonese Simplified");
    public static final ElvisLanguage CANTONESE_TRADITIONAL = new ElvisLanguage("zhhk", "Cantonese Traditional");
    public static final ElvisLanguage DANISH = new ElvisLanguage("dadk", "Danish");
    public static final ElvisLanguage DUTCH = new ElvisLanguage("nlnl", "Dutch");
    public static final ElvisLanguage EUROPEAN_FRENCH = new ElvisLanguage("frfr", "European French");
    public static final ElvisLanguage EUROPEAN_PORTUGUESE = new ElvisLanguage("ptpt", "European Portuguese");
    public static final ElvisLanguage EUROPEAN_SPANISH = new ElvisLanguage("eses", "European Spanish");
    public static final ElvisLanguage FINNISH = new ElvisLanguage("fifi", "Finnish");
    public static final ElvisLanguage GERMAN = new ElvisLanguage("dede", "German");
    public static final ElvisLanguage GREEK = new ElvisLanguage("elgr", "Greek");
    public static final ElvisLanguage ITALIAN = new ElvisLanguage("itit", "Italian");
    public static final ElvisLanguage JAPANESE = new ElvisLanguage("jajp", "Japanese");
    public static final ElvisLanguage KOREAN = new ElvisLanguage("kokr", "Korean");
    public static final ElvisLanguage LATIN_AMERICAN_SPANISH = new ElvisLanguage("esmx", "Latin American Spanish");
    public static final ElvisLanguage MANDARIN_ENGLISH_SIMPLIFIED = new ElvisLanguage("zecn", "Mandarin/English Simplified");
    public static final ElvisLanguage MANDARIN_ENGLISH_TRADITIONAL = new ElvisLanguage("zetw", "Mandarin/English Traditional");
    public static final ElvisLanguage MANDARIN_TRADITIONAL = new ElvisLanguage("zhtw", "Mandarin Traditional");
    public static final ElvisLanguage NORWEGIAN = new ElvisLanguage("nono", "Norwegian");
    public static final ElvisLanguage POLISH = new ElvisLanguage("plpl", "Polish");
    public static final ElvisLanguage RUSSIAN = new ElvisLanguage("ruru", "Russian");
    public static final ElvisLanguage SWEDISH = new ElvisLanguage("svse", "Swedish");
    public static final ElvisLanguage TURKISH = new ElvisLanguage("trtr", "Turkish");
    public static final ElvisLanguage UKRAINIAN = new ElvisLanguage("ukua", "Ukrainian");
    public static final ElvisLanguage UNITED_STATES_ENGLISH = new ElvisLanguage("enus", "United States English");
    public static final ElvisLanguage US_SPANISH = new ElvisLanguage("esus", "US Spanish");

    d() {
    }

    static String a(String str, int i) {
        return "~type~_~language~~rate_khz~k.bin".replace("~type~", "lvr").replace("~language~", str).replace("~rate_khz~", String.valueOf(i));
    }

    static String a(Matcher matcher) {
        return matcher.group(2);
    }

    static String b(Matcher matcher) {
        return matcher.group(3);
    }
}
