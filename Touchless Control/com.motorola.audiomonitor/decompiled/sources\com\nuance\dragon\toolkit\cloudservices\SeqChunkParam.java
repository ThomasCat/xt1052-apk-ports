package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.dragon.toolkit.data.Data.Dictionary;

public class SeqChunkParam extends DictionaryParam {
    public SeqChunkParam(String str, Dictionary dictionary) {
        super(9, str, dictionary);
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(d dVar, a aVar) {
        dVar.b(this.f1355a, this._dict);
        return true;
    }
}
