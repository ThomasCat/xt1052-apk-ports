package com.motorola.audiomonitor.actions.utils;

import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

class ah extends UtteranceProgressListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ ae f197a;

    ah(ae aeVar) {
        this.f197a = aeVar;
    }

    private int a(String str) {
        this.f197a.f193g;
        try {
            return Integer.valueOf(str).intValue();
        } catch (NumberFormatException e2) {
            NumberFormatException numberFormatException = e2;
            int b2 = this.f197a.f193g;
            this.f197a.j = true;
            Log.w(ae.f187a, "NumberFormatException :" + str + " " + numberFormatException);
            return b2;
        }
    }

    public void onDone(String str) {
        this.f197a.j = false;
        int a2 = a(str);
        if (ae.f188b) {
            Log.w(ae.f187a, "onDone   :" + (a2 < 100 ? Integer.valueOf(a2) : "silence"));
        }
        this.f197a.b(a2);
    }

    public void onError(String str) {
        int a2 = a(str);
        if (ae.f188b) {
            Log.w(ae.f187a, "onError :" + str);
        }
        if (this.f197a.k || this.f197a.j || this.f197a.f192f == null) {
            this.f197a.b(this.f197a.f193g);
            return;
        }
        this.f197a.j = true;
        this.f197a.f192f.stop();
        int i = a2 - 1;
        if (i > 100) {
            i -= 100;
        }
        if (!this.f197a.c(i)) {
            this.f197a.b(this.f197a.f193g);
        }
    }

    public void onStart(String str) {
        this.f197a.f191e.removeCallbacks(this.f197a.m);
    }
}
