package com.nuance.dragon.toolkit.recognition.dictation;

public interface Alternative {
    int size();

    String toString();

    Token tokenAt(int i);
}
