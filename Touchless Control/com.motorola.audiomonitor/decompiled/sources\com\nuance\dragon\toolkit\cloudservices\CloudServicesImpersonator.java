package com.nuance.dragon.toolkit.cloudservices;

import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.cloudservices.CloudServices.ConnectionState;
import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.util.Logger;

public class CloudServicesImpersonator extends a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public ConnectionState f1341a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public a f1342b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public boolean f1343c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public final Runnable f1344d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public final Handler f1345e;

    enum a {
        IDLE,
        CONNECT,
        DISCONNECT
    }

    public CloudServicesImpersonator() {
        this.f1343c = true;
        this.f1342b = a.CONNECT;
        this.f1343c = false;
        this.f1341a = ConnectionState.DISCONNECTED;
        this.f1345e = new Handler();
        this.f1344d = new Runnable() {
            public final void run() {
                if (CloudServicesImpersonator.this.f1343c) {
                    CloudServicesImpersonator.this.f1343c = false;
                    CloudServicesImpersonator.this.f1342b = a.IDLE;
                    CloudServicesImpersonator.this.f1341a = ConnectionState.DISCONNECTED;
                    CloudServicesImpersonator.this.connectionStateChanged();
                    return;
                }
                if (CloudServicesImpersonator.this.f1342b == a.CONNECT && CloudServicesImpersonator.this.f1341a != ConnectionState.CONNECTED) {
                    CloudServicesImpersonator.this.f1341a = ConnectionState.CONNECTED;
                    CloudServicesImpersonator.this.f1342b = a.IDLE;
                    CloudServicesImpersonator.this.connectionStateChanged();
                } else if (CloudServicesImpersonator.this.f1342b != a.DISCONNECT || CloudServicesImpersonator.this.f1341a == ConnectionState.DISCONNECTED) {
                    CloudServicesImpersonator.this.f1341a;
                    ConnectionState connectionState = ConnectionState.CONNECTED;
                } else {
                    CloudServicesImpersonator.this.f1341a = ConnectionState.DISCONNECTED;
                    CloudServicesImpersonator.this.f1342b = a.IDLE;
                    CloudServicesImpersonator.this.connectionStateChanged();
                }
                CloudServicesImpersonator.this.f1345e.postDelayed(CloudServicesImpersonator.this.f1344d, 200);
            }
        };
        this.f1345e.postDelayed(this.f1344d, 200);
    }

    public /* bridge */ /* synthetic */ void addTransaction(Transaction transaction, int i) {
        super.addTransaction(transaction, i);
    }

    /* access modifiers changed from: protected */
    public boolean canStartTransaction() {
        return true;
    }

    /* access modifiers changed from: protected */
    public d createCommandContext(final Transaction transaction) {
        return new d() {

            /* renamed from: c reason: collision with root package name */
            private boolean f1349c = false;

            /* renamed from: d reason: collision with root package name */
            private int f1350d = 0;

            private void c() {
                transaction.a(null, true);
            }

            public final void a() {
                Logger.info(CloudServicesImpersonator.this, "Ending command");
                this.f1349c = true;
                if (this.f1350d == 0) {
                    c();
                }
            }

            public final void a(String str) {
                Logger.info(CloudServicesImpersonator.this, "Sending audio param " + str);
                this.f1350d++;
            }

            public final void a(String str, AudioChunk audioChunk, boolean z) {
                Logger.info(CloudServicesImpersonator.this, "Sending" + (z ? " final" : "") + " audio for param " + str);
                if (z) {
                    this.f1350d--;
                }
                if (this.f1350d == 0 && this.f1349c) {
                    c();
                }
            }

            public final void a(String str, Dictionary dictionary) {
                Logger.info(CloudServicesImpersonator.this, "Sending dict param " + str);
            }

            public final void a(String str, Dictionary dictionary, int i) {
                Logger.info(CloudServicesImpersonator.this, "Initializing command " + str);
            }

            public final void a(String str, Dictionary dictionary, com.nuance.a.a.a.b.b.a aVar) {
                Logger.info(CloudServicesImpersonator.this, "Sending tts param " + str);
            }

            public final void a(String str, String str2) {
                Logger.info(CloudServicesImpersonator.this, "Sending text param " + str);
            }

            public final void a(String str, byte[] bArr) {
                Logger.info(CloudServicesImpersonator.this, "Sending data param " + str);
            }

            public final void b() {
                Logger.info(CloudServicesImpersonator.this, "free resource");
            }

            public final void b(String str, Dictionary dictionary) {
                Logger.info(CloudServicesImpersonator.this, "Sending seq chunk param " + str);
            }

            public final void b(String str, String str2) {
                Logger.info(CloudServicesImpersonator.this, "Sending choice param " + str);
            }

            public final void c(String str, Dictionary dictionary) {
                Logger.info(CloudServicesImpersonator.this, "Sending seq end param " + str);
            }

            public final void d(String str, Dictionary dictionary) {
                Logger.info(CloudServicesImpersonator.this, "Sending seq start param " + str);
            }
        };
    }

    public /* bridge */ /* synthetic */ Transaction currentTransaction() {
        return super.currentTransaction();
    }

    public ConnectionState getConnectionState() {
        return this.f1341a;
    }

    public String getSessionID() {
        return "SESSIONID";
    }

    public /* bridge */ /* synthetic */ int getTransactionCount() {
        return super.getTransactionCount();
    }

    public String getUniqueID() {
        return "UNIQUEID";
    }

    public void release() {
        super.release();
        this.f1343c = true;
    }
}
