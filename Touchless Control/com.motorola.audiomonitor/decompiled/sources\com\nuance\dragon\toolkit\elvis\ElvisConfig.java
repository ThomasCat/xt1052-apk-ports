package com.nuance.dragon.toolkit.elvis;

import com.nuance.dragon.toolkit.language.Language;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import com.nuance.dragon.toolkit.util.internal.d;
import org.json.JSONObject;

public final class ElvisConfig implements JSONCompliant {

    /* renamed from: a reason: collision with root package name */
    final String f1533a;

    /* renamed from: b reason: collision with root package name */
    final String f1534b;

    /* renamed from: c reason: collision with root package name */
    final int f1535c;

    public ElvisConfig(ElvisLanguage elvisLanguage, int i) {
        d.a("language", (Object) elvisLanguage);
        this.f1533a = elvisLanguage.name;
        this.f1535c = i;
        this.f1534b = null;
    }

    public ElvisConfig(Language language, int i) {
        d.a("language", (Object) language);
        this.f1533a = language.getElvisLanguage().name;
        this.f1535c = i;
        this.f1534b = null;
    }

    public ElvisConfig(String str) {
        d.a("binfile", str);
        this.f1533a = null;
        this.f1535c = 0;
        this.f1534b = str;
    }

    private ElvisConfig(String str, int i) {
        this.f1533a = str;
        this.f1535c = i;
        this.f1534b = null;
    }

    public static ElvisConfig createFromJSON(JSONObject jSONObject) {
        String optString = jSONObject.optString("binfile", null);
        return optString != null ? new ElvisConfig(optString) : new ElvisConfig(jSONObject.getString("language"), jSONObject.getInt("sampling_rate"));
    }

    public final JSONObject toJSON() {
        b bVar = new b();
        if (this.f1533a != null) {
            bVar.a("language", (Object) this.f1533a);
            bVar.a("sampling_rate", (Object) Integer.valueOf(this.f1535c));
        } else {
            bVar.a("binfile", (Object) this.f1534b);
        }
        return bVar;
    }
}
