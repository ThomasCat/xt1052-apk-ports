package com.motorola.audiomonitor.pluginservice;

import com.motorola.audiomonitor.ProtectedByteBuffer;
import com.motorola.audiomonitor.r;
import com.motorola.audiomonitor.z;
import java.util.LinkedList;
import java.util.List;

public class c implements r {

    /* renamed from: a reason: collision with root package name */
    private z f539a;

    /* renamed from: b reason: collision with root package name */
    private final List f540b = new LinkedList();

    public synchronized void a() {
        this.f540b.clear();
    }

    public synchronized void a(float f2) {
        if (this.f539a != null) {
            this.f539a.a(f2);
        }
    }

    public synchronized void a(ProtectedByteBuffer protectedByteBuffer) {
        this.f540b.add(protectedByteBuffer);
    }

    public synchronized void a(z zVar) {
        this.f539a = zVar;
    }

    public void b() {
        this.f539a = null;
    }

    public void b(long j) {
    }

    public synchronized boolean c() {
        return this.f540b.size() > 0;
    }

    public synchronized List d() {
        return new LinkedList(this.f540b);
    }

    public synchronized void start() {
        this.f540b.clear();
    }
}
