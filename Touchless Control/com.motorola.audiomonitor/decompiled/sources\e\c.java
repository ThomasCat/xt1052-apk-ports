package e;

import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothProfile.ServiceListener;
import android.util.Log;

class c implements ServiceListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ a f2206a;

    c(a aVar) {
        this.f2206a = aVar;
    }

    public void onServiceConnected(int i, BluetoothProfile bluetoothProfile) {
        if (a.f2198b) {
            Log.d(a.f2197a, "Connected to a profile : " + i);
        }
        if (i == 1) {
            this.f2206a.a((BluetoothHeadset) bluetoothProfile);
            this.f2206a.m();
        }
    }

    public void onServiceDisconnected(int i) {
        if (a.f2198b) {
            Log.d(a.f2197a, "Disconnected from a profile : " + i);
        }
        if (i == 1) {
            this.f2206a.a((BluetoothHeadset) null);
            this.f2206a.m();
        }
    }
}
