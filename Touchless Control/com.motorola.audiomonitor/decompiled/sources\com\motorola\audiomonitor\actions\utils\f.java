package com.motorola.audiomonitor.actions.utils;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.PhoneLookup;
import android.provider.ContactsContract.RawContacts;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.a;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class f {

    /* renamed from: a reason: collision with root package name */
    private static final String f227a = ("AMonitor." + f.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f228b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private static int[] f229c = null;

    /* renamed from: d reason: collision with root package name */
    private static u f230d;

    public static a a(Context context, String str) {
        a aVar;
        Cursor cursor = null;
        try {
            Cursor query = context.getContentResolver().query(Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(str)), new String[]{"lookup", "display_name", "type"}, null, null, null);
            if (query == null) {
                try {
                    if (f228b) {
                        Log.d(f227a, "cursor is null");
                    }
                    if (query != null) {
                        query.close();
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    cursor = query;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            } else {
                int columnIndexOrThrow = query.getColumnIndexOrThrow("lookup");
                int columnIndex = query.getColumnIndex("display_name");
                int columnIndex2 = query.getColumnIndex("type");
                if (query.getCount() <= 0 || !query.moveToFirst()) {
                    aVar = null;
                } else {
                    String string = query.getString(columnIndexOrThrow);
                    String string2 = query.getString(columnIndex);
                    int i = query.getInt(columnIndex2);
                    aVar = new a(new b(string, string2, i, 0.0f), string2, i, str, false, 100.0f, 100.0f);
                }
                if (query != null) {
                    query.close();
                }
                return aVar == null ? new a(new b("", "", -1, 0.0f), "", -1, str, false, 1.0f, 1.0f) : aVar;
            }
        } catch (Throwable th2) {
            th = th2;
        }
    }

    public static u a(Context context) {
        if (f230d == null) {
            f230d = new u(context);
        }
        return f230d;
    }

    public static String a(Context context, a aVar) {
        String str = "";
        if (aVar.e() != 0) {
            return (String) Phone.getTypeLabel(context.getResources(), aVar.e(), "");
        }
        String a2 = a(context, aVar.b(), aVar.g());
        return a2 == null ? "" : a2;
    }

    public static String a(Context context, String str, String str2) {
        String str3 = null;
        Uri uri = Phone.CONTENT_URI;
        StringBuilder sb = new StringBuilder("lookup");
        sb.append('=');
        DatabaseUtils.appendEscapedSQLString(sb, str);
        sb.append(" AND ");
        sb.append("data2");
        sb.append('=');
        sb.append(0);
        sb.append(" AND ");
        sb.append("data1");
        sb.append('=');
        DatabaseUtils.appendEscapedSQLString(sb, str2);
        Cursor query = context.getContentResolver().query(uri, new String[]{"data3"}, sb.toString(), null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    str3 = query.getString(query.getColumnIndexOrThrow("data3"));
                } else {
                    query.close();
                }
            } finally {
                query.close();
            }
        }
        return str3;
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.motorola.audiomonitor.actions.utils.b>, for r20v0, types: [java.util.List, java.util.List<com.motorola.audiomonitor.actions.utils.b>] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0147  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0220  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x022e A[ADDED_TO_REGION] */
    public static List a(Context context, List<b> list) {
        Cursor cursor;
        ArrayList arrayList;
        a aVar;
        int i;
        List list2;
        HashMap hashMap = new HashMap();
        Uri uri = Phone.CONTENT_URI;
        StringBuffer stringBuffer = new StringBuffer();
        String str = "";
        Iterator it = list.iterator();
        while (true) {
            String str2 = str;
            if (!it.hasNext()) {
                break;
            }
            b bVar = (b) it.next();
            stringBuffer.append(str2);
            stringBuffer.append("lookup");
            stringBuffer.append("='");
            stringBuffer.append(bVar.a());
            stringBuffer.append("'");
            str = " OR ";
        }
        StringBuffer stringBuffer2 = new StringBuffer();
        stringBuffer2.append("lookup");
        stringBuffer2.append(",");
        stringBuffer2.append("is_primary");
        stringBuffer2.append(" DESC");
        try {
            Cursor query = context.getContentResolver().query(uri, new String[]{"lookup", "display_name", "data1", "data2", "is_primary", "contact_id"}, stringBuffer.toString(), null, stringBuffer2.toString());
            if (query == null) {
                try {
                    if (f228b) {
                        Log.d(f227a, "cursor is null");
                    }
                    if (query == null) {
                        return null;
                    }
                    query.close();
                    return null;
                } catch (Throwable th) {
                    th = th;
                    cursor = query;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            } else {
                int columnIndex = query.getColumnIndex("lookup");
                int columnIndex2 = query.getColumnIndex("display_name");
                int columnIndex3 = query.getColumnIndex("data1");
                int columnIndex4 = query.getColumnIndex("data2");
                int columnIndex5 = query.getColumnIndex("is_primary");
                int columnIndex6 = query.getColumnIndex("contact_id");
                if (query.getCount() <= 0 || !query.moveToFirst()) {
                    if (query != null) {
                        query.close();
                    }
                    arrayList = new ArrayList();
                    ArrayList<a> arrayList2 = new ArrayList<>();
                    HashSet hashSet = new HashSet();
                    boolean z = false;
                    for (b bVar2 : list) {
                        List<a> list3 = (List) hashMap.get(bVar2.a());
                        if (list3 != null) {
                            arrayList2.addAll(list3);
                            for (a a2 : list3) {
                                a2.a().a(bVar2.c());
                                hashSet.add(bVar2.a());
                            }
                            if (bVar2.d() == -1) {
                                ArrayList arrayList3 = new ArrayList();
                                int i2 = 0;
                                Iterator it2 = list3.iterator();
                                while (true) {
                                    if (!it2.hasNext()) {
                                        aVar = null;
                                        break;
                                    }
                                    aVar = (a) it2.next();
                                    if (aVar.f()) {
                                        break;
                                    }
                                    int a3 = a(context).a(aVar.e());
                                    if (a3 == i2) {
                                        arrayList3.add(aVar);
                                        i = i2;
                                    } else if (a3 > i2) {
                                        arrayList3.clear();
                                        arrayList3.add(aVar);
                                        i = a3;
                                    } else {
                                        i = i2;
                                    }
                                    i2 = i;
                                }
                                if (aVar != null) {
                                    arrayList.add(aVar);
                                } else {
                                    arrayList.addAll(arrayList3);
                                }
                            } else {
                                boolean z2 = z;
                                for (a aVar2 : list3) {
                                    if (bVar2.d() == aVar2.e()) {
                                        z2 = true;
                                        arrayList.add(aVar2);
                                    }
                                    z2 = z2;
                                }
                                z = z2;
                            }
                        }
                    }
                    if (arrayList.size() != 0) {
                        if (f228b) {
                            Log.d(f227a, "priority search is 0 so use all found contacts.");
                        }
                        return arrayList2;
                    }
                    if (!z && hashSet.size() == 1) {
                        if (f228b) {
                            Log.d(f227a, "unique lookup key is found.");
                        }
                        String str3 = (String) hashSet.iterator().next();
                        arrayList.clear();
                        for (a aVar3 : arrayList2) {
                            if (aVar3.b().equals(str3)) {
                                arrayList.add(aVar3);
                            }
                        }
                    }
                    return arrayList;
                }
                do {
                    String string = query.getString(columnIndex);
                    String string2 = query.getString(columnIndex2);
                    String string3 = query.getString(columnIndex3);
                    int i3 = query.getInt(columnIndex4);
                    boolean z3 = query.getInt(columnIndex5) == 1;
                    if (!a(context, query.getLong(columnIndex6))) {
                        List list4 = (List) hashMap.get(string);
                        if (list4 == null) {
                            ArrayList arrayList4 = new ArrayList();
                            hashMap.put(string, arrayList4);
                            list2 = arrayList4;
                        } else {
                            list2 = list4;
                        }
                        list2.add(new a(string, 1, string2, i3, string3, z3, 1.0f, 1.0f));
                    } else if (f228b) {
                        Log.d(f227a, "Deleted Contact " + string2 + "(" + string3 + ")");
                    }
                } while (query.moveToNext());
                if (query != null) {
                }
                arrayList = new ArrayList();
                ArrayList<a> arrayList22 = new ArrayList<>();
                HashSet hashSet2 = new HashSet();
                boolean z4 = false;
                for (b bVar22 : list) {
                }
                if (arrayList.size() != 0) {
                }
            }
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
        }
    }

    public static void a() {
        if (f230d != null) {
            f230d.b();
            f230d = null;
        }
    }

    private static boolean a(Context context, long j) {
        Cursor cursor;
        boolean z = false;
        try {
            cursor = context.getContentResolver().query(RawContacts.CONTENT_URI, new String[]{"deleted"}, "contact_id=?", new String[]{String.valueOf(j)}, null);
            if (cursor == null) {
                try {
                    if (f228b) {
                        Log.d(f227a, "cursor is null");
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            } else {
                int columnIndex = cursor.getColumnIndex("deleted");
                if (cursor.getCount() > 0 && cursor.moveToFirst()) {
                    z = cursor.getLong(columnIndex) == 1;
                }
                if (cursor != null) {
                    cursor.close();
                }
            }
            return z;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
        }
    }

    public static boolean a(Context context, String str, String[] strArr) {
        return a(context, b(context)) ? a(str, strArr) : str.length() >= 7;
    }

    private static boolean a(Context context, int[] iArr) {
        String networkOperator = ((TelephonyManager) context.getSystemService("phone")).getNetworkOperator();
        int i = (networkOperator == null || networkOperator.length() < 3) ? -1 : Integer.parseInt(networkOperator.substring(0, 3));
        if (i != -1) {
            for (int i2 : iArr) {
                if (i2 == i) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean a(String str, String[] strArr) {
        if (str.length() == 3) {
            return false;
        }
        if ((str.length() == 7 || str.length() == 10) && !str.startsWith("0") && !str.startsWith("1")) {
            return true;
        }
        return str.length() == 11 && str.startsWith("1");
    }

    public static Bitmap b(Context context, String str) {
        Bitmap bitmap = null;
        if (!TextUtils.isEmpty(str)) {
            InputStream openContactPhotoInputStream = Contacts.openContactPhotoInputStream(context.getContentResolver(), Uri.withAppendedPath(Contacts.CONTENT_LOOKUP_URI, str), true);
            if (openContactPhotoInputStream != null) {
                bitmap = BitmapFactory.decodeStream(openContactPhotoInputStream, null, new Options());
                try {
                    openContactPhotoInputStream.close();
                } catch (IOException e2) {
                }
            }
        }
        return bitmap;
    }

    private static int[] b(Context context) {
        if (f229c == null) {
            f229c = context.getResources().getIntArray(C0014R.array.mcc_na);
        }
        return f229c;
    }
}
