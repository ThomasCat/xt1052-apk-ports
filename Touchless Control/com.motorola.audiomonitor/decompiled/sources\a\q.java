package a;

import android.content.Context;
import android.util.Log;
import b.b;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.actions.utils.ak;
import com.motorola.audiomonitor.actions.utils.am;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public final class q {

    /* renamed from: a reason: collision with root package name */
    private static final String f52a = ("AMonitor." + q.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f53b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private final s f54c;

    /* renamed from: d reason: collision with root package name */
    private final HashMap f55d = new HashMap();

    /* renamed from: e reason: collision with root package name */
    private boolean f56e;

    public q(Context context) {
        this.f54c = new s(this, context);
        a(context);
        if (f53b) {
            Log.d(f52a, toString());
        }
    }

    private void b() {
        if (this.f56e) {
            ak a2 = ak.a(am.PROCESS);
            if (a2 != null) {
                a2.a();
            }
        }
        this.f56e = false;
    }

    public final o a(e eVar) {
        b b2 = eVar.b();
        o oVar = o.NOT_CONSUMED;
        if (f53b) {
            Log.d(f52a, "command: " + b2.a("command"));
        }
        this.f56e = true;
        ArrayList arrayList = (ArrayList) this.f55d.get(b2.a("command"));
        if (arrayList != null) {
            Iterator it = arrayList.iterator();
            while (true) {
                o oVar2 = oVar;
                if (!it.hasNext()) {
                    oVar = oVar2;
                    break;
                }
                n nVar = (n) it.next();
                b();
                oVar = nVar.a(b2, eVar);
                if (oVar == o.CONSUMED) {
                    if (f53b) {
                        Log.d(f52a, b2.a("command") + " is consumed by " + nVar.getClass().getSimpleName());
                    }
                }
            }
            this.f56e = false;
        } else if (f53b) {
            Log.d(f52a, b2.a("command") + " is not consumed");
        }
        return oVar;
    }

    public void a() {
        this.f54c.b();
        for (ArrayList<n> it : this.f55d.values()) {
            for (n b2 : it) {
                b2.b();
            }
        }
    }

    public void a(Context context) {
        a("call", new g(1, context), false);
        a("voicemail", new ag(1, context), false);
        if (a.n.equalsIgnoreCase("pt_BR")) {
            a("navigate", new t(1, context), false);
        }
        if (p.a(context).length > 0) {
            a("intent", new p(context), false);
        }
    }

    public void a(String str, n nVar, boolean z) {
        ArrayList arrayList = (ArrayList) this.f55d.get(str);
        if (arrayList == null) {
            arrayList = new ArrayList();
            if (z) {
                arrayList.add(this.f54c);
            }
            this.f55d.put(str, arrayList);
        }
        arrayList.add(0, nVar);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Entry entry : this.f55d.entrySet()) {
            sb.append(((String) entry.getKey()) + "[");
            Iterator it = ((ArrayList) entry.getValue()).iterator();
            while (it.hasNext()) {
                sb.append(((n) it.next()).getClass().getSimpleName() + ",");
            }
            sb.append("]\n");
        }
        return sb.toString();
    }
}
