package a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

class l extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ i f44a;

    l(i iVar) {
        this.f44a = iVar;
    }

    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            if ((TextUtils.equals(intent.getAction(), "android.intent.action.PACKAGE_CHANGED") || TextUtils.equals(intent.getAction(), "android.intent.action.PACKAGE_ADDED") || TextUtils.equals(intent.getAction(), "android.intent.action.PACKAGE_REMOVED")) && TextUtils.equals(intent.getData().getSchemeSpecificPart(), "com.google.android.googlequicksearchbox")) {
                this.f44a.f41h = this.f44a.e();
            }
        }
    }
}
