package a;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;
import c.d;
import com.motorola.audiomonitor.AudioProvider;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.ProtectedByteBuffer;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.actions.utils.aj;
import com.motorola.audiomonitor.actions.utils.m;
import com.motorola.audiomonitor.comsocket.b;
import com.motorola.audiomonitor.comsocket.e;
import com.motorola.audiomonitor.p;
import com.motorola.audiomonitor.uis.as;
import h.g;
import java.util.LinkedList;
import java.util.List;

public class i {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f34a = ("AMonitor." + i.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f35b = a.f160b;
    private static int i = 0;

    /* renamed from: c reason: collision with root package name */
    private final Context f36c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public final p f37d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public b f38e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public final Handler f39f = new Handler();

    /* renamed from: g reason: collision with root package name */
    private final ProtectedByteBuffer f40g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public boolean f41h = false;
    private final String j = "com.google.android.googlequicksearchbox.VOICE_SEARCH_RECORDED_AUDIO";
    private final e k = new j(this);
    /* access modifiers changed from: private */
    public final Runnable l = new k(this);
    private final BroadcastReceiver m = new l(this);
    /* access modifiers changed from: private */
    public final m n;

    public i(Context context, p pVar, m mVar) {
        this.f36c = context;
        this.f37d = pVar;
        this.n = mVar;
        byte[] bArr = new byte[3200];
        for (int i2 = 0; i2 < bArr.length; i2++) {
            bArr[i2] = 0;
        }
        this.f40g = new ProtectedByteBuffer(bArr, 16000);
        this.f38e = d();
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_CHANGED");
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addDataScheme("package");
        context.registerReceiver(this.m, intentFilter);
        this.f41h = e();
    }

    public static void a(Context context) {
        Toast makeText = Toast.makeText(context, C0014R.string.open_google_now, 1);
        makeText.getView().setBackgroundResource(C0014R.drawable.toast_frame_light);
        makeText.show();
    }

    private void b(e eVar) {
        this.f38e.b();
        for (ProtectedByteBuffer a2 : eVar.a()) {
            this.f38e.a(a2);
        }
        for (int i2 = 0; i2 < 10; i2++) {
            this.f38e.a(this.f40g);
        }
        this.f38e.a(true);
        this.f39f.postDelayed(this.l, a.f163e);
        b(this.f36c);
    }

    public static void b(Context context) {
        a(context);
        aj.a(context);
        Intent intent = new Intent();
        intent.setClassName("com.google.android.googlequicksearchbox", "com.google.android.googlequicksearchbox.VoiceSearchActivity");
        intent.addFlags(268435456);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e2) {
            Log.e(f34a, "ActivityNotFound");
        }
        g.a().n();
        d.a(context, "num_google");
    }

    private void c(e eVar) {
        int i2 = 0;
        a(this.f36c);
        aj.a(this.f36c);
        LinkedList linkedList = new LinkedList();
        linkedList.addAll(eVar.a());
        for (int i3 = 0; i3 < 10; i3++) {
            linkedList.add(this.f40g);
        }
        if (i < 100) {
            i2 = i + 1;
            i = i2;
        }
        i = i2;
        String num = Integer.toString(i);
        AudioProvider.a(num, (List) linkedList);
        Uri parse = Uri.parse("content://com.motorola.audiomonitor.raw_audio/command_audio/" + num);
        Intent intent = new Intent("com.google.android.googlequicksearchbox.VOICE_SEARCH_RECORDED_AUDIO");
        intent.setData(parse);
        intent.addFlags(268435456);
        try {
            this.f36c.startActivity(intent);
        } catch (ActivityNotFoundException e2) {
            Log.e(f34a, "ActivityNotFound");
        } catch (SecurityException e3) {
            Log.e(f34a, "No permission to launch Activity");
        }
        g.a().n();
        d.a(this.f36c, "num_google");
        if (this.n != null) {
            this.n.a();
        }
    }

    private b d() {
        try {
            b bVar = new b(this.k);
            bVar.start();
            return bVar;
        } catch (IllegalStateException e2) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public boolean e() {
        boolean z = false;
        PackageManager packageManager = this.f36c.getPackageManager();
        Intent intent = new Intent("com.google.android.googlequicksearchbox.VOICE_SEARCH_RECORDED_AUDIO");
        intent.setType("audio/l16");
        if (!packageManager.queryIntentActivities(intent, 0).isEmpty()) {
            z = true;
        }
        if (f35b) {
            Log.d(f34a, "Use Audio API: " + z);
        }
        return z;
    }

    public void a() {
        if (this.f38e != null) {
            this.f38e.a();
            this.f38e = null;
        }
        this.f36c.unregisterReceiver(this.m);
    }

    public void a(e eVar) {
        as a2 = as.a();
        if (a2 != null) {
            a2.b();
        }
        if (m.a().a(false, eVar)) {
            if (this.n != null) {
                this.n.a();
            }
        } else if (this.f41h) {
            c(eVar);
        } else if (this.f38e == null) {
            if (this.n != null) {
                this.n.a();
            }
            this.f38e = d();
        } else {
            b(eVar);
        }
    }
}
