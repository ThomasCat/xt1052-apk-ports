package com.nuance.dragon.toolkit.audio;

import com.nuance.dragon.toolkit.util.internal.g;
import java.util.ArrayList;
import java.util.List;

public abstract class AudioSource {

    /* renamed from: a reason: collision with root package name */
    private final g f1004a = new g();

    /* access modifiers changed from: protected */
    public abstract void audioSinkConnected(AudioSink audioSink);

    /* access modifiers changed from: protected */
    public abstract void audioSinkDisconnected(AudioSink audioSink);

    public final List getAllAudioChunksForSink(AudioSink audioSink) {
        int chunksAvailableForSink = getChunksAvailableForSink(audioSink);
        if (chunksAvailableForSink == 0) {
            return this.f1004a;
        }
        ArrayList arrayList = new ArrayList(chunksAvailableForSink);
        getAllAudioChunksForSink(audioSink, arrayList);
        return arrayList;
    }

    public void getAllAudioChunksForSink(AudioSink audioSink, List list) {
        while (true) {
            AbstractAudioChunk audioChunkForSink = getAudioChunkForSink(audioSink);
            if (audioChunkForSink != null) {
                list.add(audioChunkForSink);
            } else {
                return;
            }
        }
    }

    public abstract AbstractAudioChunk getAudioChunkForSink(AudioSink audioSink);

    public abstract AudioType getAudioType();

    public abstract int getChunksAvailable();

    public abstract int getChunksAvailableForSink(AudioSink audioSink);

    public abstract boolean isActive();

    public boolean isEmpty() {
        return getChunksAvailable() == 0;
    }

    public boolean isEmptyForSink(AudioSink audioSink) {
        return getChunksAvailableForSink(audioSink) == 0;
    }

    public final int readAllAudioChunksForSink(AudioSink audioSink, List list) {
        int i = 0;
        while (true) {
            AbstractAudioChunk audioChunkForSink = getAudioChunkForSink(audioSink);
            if (audioChunkForSink == null) {
                return i;
            }
            i++;
            list.add(audioChunkForSink);
        }
    }
}
