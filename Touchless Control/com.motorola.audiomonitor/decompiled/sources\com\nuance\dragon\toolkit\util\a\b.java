package com.nuance.dragon.toolkit.util.a;

import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.Logger;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class b extends JSONObject {
    public final boolean a(String str, JSONCompliant jSONCompliant) {
        if (jSONCompliant != null) {
            try {
                put(str, jSONCompliant.toJSON());
                return true;
            } catch (JSONException e2) {
                Logger.error(this, "Error adding to JSON");
            }
        }
        return false;
    }

    public final boolean a(String str, Object obj) {
        if (obj != null) {
            try {
                put(str, obj);
                return true;
            } catch (JSONException e2) {
                Logger.error(this, "Error adding to JSON");
            }
        }
        return false;
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.util.JSONCompliant>, for r6v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.util.JSONCompliant>] */
    public final boolean a(String str, List<JSONCompliant> list) {
        if (list == null) {
            return false;
        }
        JSONArray jSONArray = new JSONArray();
        for (JSONCompliant json : list) {
            JSONObject json2 = json.toJSON();
            if (json2 == null) {
                return false;
            }
            jSONArray.put(json2);
        }
        return a(str, (Object) jSONArray);
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<java.lang.String>, for r5v0, types: [java.util.List, java.util.List<java.lang.String>] */
    public final boolean b(String str, List<String> list) {
        if (list == null) {
            return false;
        }
        JSONArray jSONArray = new JSONArray();
        for (String put : list) {
            jSONArray.put(put);
        }
        return a(str, (Object) jSONArray);
    }
}
