package a;

import android.content.Context;

public abstract class r implements n {

    /* renamed from: a reason: collision with root package name */
    private final int f57a;

    /* renamed from: b reason: collision with root package name */
    private final Context f58b;

    /* renamed from: c reason: collision with root package name */
    private boolean f59c;

    r(int i, Context context) {
        this.f59c = false;
        this.f57a = i;
        this.f58b = context;
    }

    r(Context context) {
        this(0, context);
    }

    public void a() {
    }

    public final void b() {
        if (!this.f59c) {
            this.f59c = true;
            a();
        }
    }

    public Context c() {
        return this.f58b;
    }
}
