package com.motorola.audiomonitor.uis;

import android.os.Handler.Callback;
import android.os.Message;
import com.motorola.audiomonitor.C0014R;

class a implements Callback {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ AOVActivity f635a;

    a(AOVActivity aOVActivity) {
        this.f635a = aOVActivity;
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 0:
                this.f635a.f596c.a(message.arg1);
                break;
            case 1:
                this.f635a.f597d.b((int) C0014R.array.array_tap_stop);
                break;
            case 2:
                this.f635a.f597d.a((int) C0014R.string.connection_slow);
                break;
        }
        return true;
    }
}
