package com.motorola.audiomonitor;

import android.text.TextUtils;
import c.e;
import com.motorola.audiomonitor.GenericRecognizer.UdtEnrollInfo;
import com.motorola.audiomonitor.GenericRecognizer.UdtEnrollResult;
import com.motorola.audiomonitor.service.d;
import h.k;
import h.o;
import h.q;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

class cq extends cb {

    /* renamed from: c reason: collision with root package name */
    final /* synthetic */ cn f464c;

    /* renamed from: d reason: collision with root package name */
    private final float[] f465d = new float[1];

    /* renamed from: e reason: collision with root package name */
    private final String[] f466e = new String[1];

    /* renamed from: f reason: collision with root package name */
    private final ArrayList f467f = new ArrayList();

    /* renamed from: g reason: collision with root package name */
    private int f468g = 7;

    /* renamed from: h reason: collision with root package name */
    private GenericRecognizer f469h;

    public cq(cn cnVar, cq cqVar) {
        this.f464c = cnVar;
        super(cqVar);
        this.f467f.addAll(cqVar.f467f);
        this.f469h = cqVar.f469h;
        cqVar.f469h = null;
    }

    public cq(cn cnVar, p pVar, d dVar) {
        this.f464c = cnVar;
        super(pVar, dVar);
    }

    private int a(String str) {
        return this.f469h == null ? b(str) : c(str);
    }

    private boolean a(UdtEnrollInfo[] udtEnrollInfoArr, UdtEnrollResult udtEnrollResult) {
        this.f464c.a("Complete UDT");
        return this.f399a.enrollCompleteUdt((UdtEnrollInfo[]) this.f467f.toArray(udtEnrollInfoArr), "generic_acoustic.raw", "phoneme_search.raw", k.b(Locale.getDefault()), "custom_acoustic_22132.raw.tmp", "custom_search_22132.raw.tmp", "ma36BP", k.d("customphrase") + ".tmp", k.e("customphrase") + ".tmp", udtEnrollResult);
    }

    private int b(String str) {
        this.f464c.a("Validate UDT");
        int checkRecording = this.f399a.checkRecording(this.f399a.getLastAudio());
        this.f464c.a("feedback=" + Integer.toHexString(checkRecording));
        if (checkRecording != 0) {
            return b(checkRecording);
        }
        String str2 = " (tS|t_h|k_h|p_h|t_c|k_c|p_c|b_c|g_c|d_c|t|k|p|b|g|d[^Z]|f|s|S|T)";
        if (str == null || str.length() == 0) {
            this.f464c.a("ERROR: Invalid phrase");
            return 17;
        } else if (this.f399a.getLastClip() > 0.1f) {
            this.f464c.a("WARNING: Too much clipping, " + this.f399a.getLastClip());
            return 4;
        } else {
            float[] lastPhraseTime = this.f399a.getLastPhraseTime();
            if (lastPhraseTime == null || lastPhraseTime.length < 2) {
                this.f464c.a("WARNING: phrase times not available");
            } else {
                float f2 = lastPhraseTime[0];
                float f3 = lastPhraseTime[1];
                float f4 = f3 - f2;
                this.f464c.a("duration=" + f4 + ", start=" + f2 + ", end=" + f3);
                if (f4 < 900.0f) {
                    this.f464c.a("ERROR: Duration too fast, duration=" + f4);
                    return 2;
                } else if (f4 > 2100.0f) {
                    this.f464c.a("ERROR: Duration too short, duration=" + f4);
                    return 1;
                }
            }
            if (!str.startsWith(".pau")) {
                this.f464c.a("WARNING: Phrase does not start with a pause");
            }
            if (!str.endsWith(".pau")) {
                this.f464c.a("WARNING: Phrase does not end with a pause");
            }
            int length = str.split("\\.pau").length;
            String replaceAll = str.replaceAll(" (tS|t_h|k_h|p_h|t_c|k_c|p_c|b_c|g_c|d_c|t|k|p|b|g|d[^Z]|f|s|S|T)", "");
            this.f464c.a("Pause pronun='" + replaceAll + "', length=" + replaceAll.length());
            int i = 1;
            for (String trim : replaceAll.split("\\.pau")) {
                if (!TextUtils.isEmpty(trim.trim())) {
                    i++;
                }
            }
            this.f464c.a("numPauses=" + length + ", numInterestingPauses=" + i);
            if (length > 10) {
                this.f464c.a("WARNING: Too many pauses: " + length);
                return 8;
            } else if (i > 10) {
                this.f464c.a("WARNING: Too many interesting pauses: " + i);
                return 8;
            } else {
                String replaceAll2 = str.replaceAll(".pau", "").replaceAll(" (tS|t_h|k_h|p_h|t_c|k_c|p_c|b_c|g_c|d_c|t|k|p|b|g|d[^Z]|f|s|S|T)", "");
                this.f464c.a("Filtered pronun='" + replaceAll2 + "', length=" + replaceAll2.length());
                int length2 = replaceAll2.replaceAll("[^ ]+", "").length() - 1;
                this.f464c.a("Score=" + length2);
                if (length2 < 7) {
                    this.f464c.a("WARNING: Score, + " + length2 + ", too low");
                    return 5;
                } else if (length2 <= 20) {
                    return 0;
                } else {
                    this.f464c.a("WARNING: Score, " + length2 + ", too high");
                    return 6;
                }
            }
        }
    }

    private boolean b(UdtEnrollInfo[] udtEnrollInfoArr, UdtEnrollResult udtEnrollResult) {
        this.f464c.a("Complete fixed-trigger");
        boolean enrollCompleteSpeaker = this.f469h.enrollCompleteSpeaker((UdtEnrollInfo[]) this.f467f.toArray(udtEnrollInfoArr), "hellomoto", "svsid_triggerOGN_enUS_3_1.raw", "custom_acoustic_22132.raw.tmp", "custom_search_22132.raw.tmp", "ma36BP", k.d("customphrase") + ".tmp", k.e("customphrase") + ".tmp", udtEnrollResult);
        this.f469h.reset();
        return enrollCompleteSpeaker;
    }

    private int c(String str) {
        this.f464c.a("Validate fixed-trigger");
        n recognize = this.f469h.recognize(new ProtectedByteBuffer(this.f399a.getLastAudio(), 16000));
        int recognizedWords = this.f469h.getRecognizedWords(0.0f, this.f465d, this.f466e);
        if (cn.f423b) {
            this.f464c.a("numWords=" + recognizedWords + ", scores=" + Arrays.toString(this.f465d) + ", words=" + Arrays.toString(this.f466e));
        }
        if (recognizedWords != 1) {
            this.f469h.reset();
            this.f464c.a("ERROR: failed to match the fixed-trigger phrase");
            return 18;
        } else if (recognize == n.ERROR) {
            this.f469h.cleanUp();
            this.f469h = null;
            j();
            this.f464c.a("ERROR: error with the fixed-trigger phrase");
            return 18;
        } else if (recognize != n.RECOGNIZED) {
            this.f469h.reset();
            this.f464c.a("ERROR: failed to match the fixed-trigger phrase");
            return 18;
        } else {
            int checkRecording = this.f469h.checkRecording(this.f399a.getLastAudio());
            this.f469h.reset();
            this.f464c.a("feedback=" + Integer.toHexString(checkRecording));
            if (checkRecording != 0) {
                return b(checkRecording);
            }
            this.f464c.a("speech-detector clipping=" + this.f399a.getLastClip());
            float[] lastPhraseTime = this.f399a.getLastPhraseTime();
            if (lastPhraseTime == null || lastPhraseTime.length < 2) {
                this.f464c.a("WARNING: phrase times not available");
            } else {
                float f2 = lastPhraseTime[0];
                float f3 = lastPhraseTime[1];
                this.f464c.a("speech-detector duration=" + (f3 - f2) + ", start=" + f2 + ", end=" + f3);
            }
            return 0;
        }
    }

    private void j() {
        RecognizerConfig a2 = bz.a();
        this.f469h = new GenericRecognizer(k.b(this.f464c.f425d), "language.raw", k.c(this.f464c.f425d));
        this.f469h.configure(a2);
    }

    public void a(int i) {
        this.f464c.f428g.a(i);
    }

    public void a(List list) {
        if (cn.f423b) {
            synchronized (this) {
                if (list != null) {
                    this.f464c.a(new o().a(q.TYPE_ENROLL_RAW, "/data/adspd/", list, 16000, this.f468g == 0 ? "e" + (this.f467f.size() - 1) + "_accepted" : "e" + this.f467f.size() + "_rejected"));
                }
            }
        }
    }

    public int b(int i) {
        int i2 = i & -257;
        if (i2 == 0) {
            return 0;
        }
        if ((i2 & 1) != 0) {
            this.f464c.a("ERROR: Too quiet");
            return 3;
        } else if ((i2 & 2) != 0) {
            this.f464c.a("ERROR: Too quiet (stddev)");
            return 3;
        } else if ((i2 & 4) != 0) {
            this.f464c.a("ERROR: Insufficient leading silence");
            return 15;
        } else if ((i2 & 8) != 0) {
            this.f464c.a("ERROR: Insufficient ending silence");
            return 16;
        } else if ((i2 & 16) != 0) {
            this.f464c.a("ERROR: Insufficient SNR");
            return 24;
        } else if ((i2 & 32) != 0) {
            this.f464c.a("ERROR: Cross-correlation failed");
            return 10;
        } else if ((i2 & 64) != 0) {
            this.f464c.a("ERROR: Excessive clipping (percent)");
            return 4;
        } else if ((i2 & UdtEnrollResult.CHECKRECORDING_CLIPPING_MSEC) != 0) {
            this.f464c.a("ERROR: Excessive clipping (ms)");
            return 4;
        } else if ((i2 & UdtEnrollResult.CHECKRECORDING_POOR_RECORDINGS) != 0) {
            this.f464c.a("ERROR: Poor recordings");
            return 27;
        } else if ((i2 & UdtEnrollResult.CHECKRECORDING_SPOT) == 0) {
            return 11;
        } else {
            this.f464c.a("ERROR: Spot failed");
            return 28;
        }
    }

    public void c() {
        super.c();
        if (this.f469h != null) {
            this.f469h.cleanUp();
            this.f469h = null;
        }
    }

    public boolean d() {
        this.f399a = new GenericRecognizer();
        this.f400b = bz.c();
        this.f399a.recordingInitUdt(this.f400b);
        this.f399a.reset();
        this.f464c.f428g.a(0, 0);
        if (this.f464c.f425d != null && this.f464c.f425d.equals("hellomoto")) {
            j();
        }
        return true;
    }

    public boolean e() {
        String str;
        boolean z;
        String str2;
        if (this.f399a == null) {
            return false;
        }
        String str3 = "e" + this.f467f.size();
        e f2 = this.f464c.k;
        f2.f123b++;
        if (this.f399a.getRecognizedWords(this.f400b.getEnrollThreshold(), this.f465d, this.f466e) > 0) {
            this.f464c.a("enrollScore=" + this.f465d[0] + ", pronun=" + this.f466e[0] + ", clipping=" + this.f399a.getLastClip());
            this.f468g = a(this.f466e[0]);
            if (this.f468g == 0) {
                this.f467f.add(new UdtEnrollInfo(this.f466e[0], this.f399a.getLastAudio()));
                this.f464c.f428g.a(1, 0);
                str2 = str3 + "_accepted";
                e f3 = this.f464c.k;
                f3.f124c++;
            } else {
                this.f464c.a("ERROR: phrase validation failed, error=" + this.f468g);
                this.f464c.a(this.f468g);
                this.f464c.f428g.a(2, this.f468g);
                str2 = str3 + "_rejected";
            }
            str = str2;
            z = false;
        } else {
            str = str3 + "_ignored";
            z = true;
        }
        if (cn.f423b) {
            ProtectedByteBuffer protectedByteBuffer = new ProtectedByteBuffer(k.a(this.f399a.getLastAudio()), 16000);
            ArrayList arrayList = new ArrayList();
            arrayList.add(protectedByteBuffer);
            this.f464c.a(new o().a(q.TYPE_ENROLL_EOS, "/data/adspd/", arrayList, 16000, str));
        }
        return z;
    }

    public void f() {
        this.f464c.a("ERROR: enroll onProcessError");
        this.f464c.a(18);
        this.f464c.f428g.a(5, 18);
    }

    public boolean g() {
        int g2 = this.f464c.f();
        if (g2 != 0) {
            this.f464c.f428g.a(5, g2);
            return false;
        }
        this.f464c.b(true);
        this.f464c.l.a();
        return true;
    }

    public void h() {
        this.f464c.l.b();
        this.f464c.b(false);
    }

    public int i() {
        if (this.f467f.size() < 3) {
            this.f464c.a("ERROR: Not enough enrollment entries");
            return 21;
        }
        UdtEnrollInfo[] udtEnrollInfoArr = new UdtEnrollInfo[this.f467f.size()];
        UdtEnrollResult udtEnrollResult = new UdtEnrollResult();
        boolean b2 = this.f469h == null ? a(udtEnrollInfoArr, udtEnrollResult) : b(udtEnrollInfoArr, udtEnrollResult);
        if (cn.f423b) {
            for (int i = 0; i < this.f467f.size(); i++) {
                ProtectedByteBuffer protectedByteBuffer = new ProtectedByteBuffer(k.a(((UdtEnrollInfo) this.f467f.get(i)).getAudio()), 16000);
                ArrayList arrayList = new ArrayList();
                arrayList.add(protectedByteBuffer);
                this.f464c.a(new o().a(q.TYPE_ENROLL_OUT, "/data/adspd/", arrayList, 16000, "e" + i));
            }
        }
        float phraseQuality = udtEnrollResult.getPhraseQuality();
        this.f464c.a("phraseQuality=" + phraseQuality);
        if (phraseQuality < 0.3f) {
            this.f464c.a("ERROR: Insufficient phrase quatliy: quality=" + phraseQuality);
            return 22;
        }
        int globalFeedback = udtEnrollResult.getGlobalFeedback();
        this.f464c.a("globalFeedback=" + Integer.toHexString(globalFeedback));
        if (globalFeedback != 0) {
            return b(globalFeedback);
        }
        if (!k.g(a.f161c + k.d("customphrase") + ".tmp")) {
            this.f464c.a("ERROR: Validation check for DSP Custom Phrase failed");
            return 14;
        } else if (!b2) {
            this.f464c.a("ERROR: complete enrollment failed");
            return 23;
        } else {
            this.f464c.k.f122a = "true";
            this.f464c.a("enrollment succeeded");
            return 0;
        }
    }
}
