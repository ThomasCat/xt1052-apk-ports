package com.nuance.dragon.toolkit.recognizer;

import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.internal.g;
import java.util.List;
import org.json.JSONObject;

public class NbestList extends g implements JSONCompliant {
    public NbestList(List list) {
        super(list);
    }

    public NbestList(Entry... entryArr) {
        super((Object[]) entryArr);
    }

    public JSONObject toJSON() {
        return null;
    }
}
