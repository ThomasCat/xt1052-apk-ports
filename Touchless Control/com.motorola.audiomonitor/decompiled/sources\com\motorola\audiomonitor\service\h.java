package com.motorola.audiomonitor.service;

import android.os.Message;
import android.util.Log;
import com.nuance.dragon.toolkit.audio.bluetooth.impl.AndroidVersion;
import com.nuance.dragon.toolkit.audio.pipes.SpeexEncoderPipe;

class h extends p {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ d f574a;

    h(d dVar, String str, String str2) {
        this.f574a = dVar;
        super(dVar, str, str2);
    }

    public void a(Message message) {
        switch (message.what) {
            case 1:
                this.f574a.s = this.f574a.f567e;
                this.f574a.v();
                this.f574a.a(109, 1, 0, (Object) null);
                this.f574a.a(111, 0, 0, (Object) null);
                return;
            case 2:
            case 3:
            case 7:
            case 8:
            case 9:
            case 10:
                if (message.arg1 != 0) {
                    this.f574a.b(true);
                    return;
                }
                return;
            case 11:
                this.f574a.a(message);
                return;
            case 12:
                if (message.arg1 == 0) {
                    if (this.f574a.w.arg1 < this.f574a.z.length) {
                        this.f574a.z[this.f574a.w.arg1] = message.arg2;
                    }
                    if (this.f574a.w.arg1 < this.f574a.A.length) {
                        this.f574a.A[this.f574a.w.arg1] = (short[]) message.obj;
                    }
                    this.f574a.a(this.f574a.w.arg1, message.arg2, (short[]) message.obj);
                    return;
                }
                return;
            case AndroidVersion.ICE_CREAM_SANDWICH /*14*/:
                if (message.arg1 == 0) {
                    this.f574a.a((int[]) message.obj);
                    return;
                }
                return;
            case SpeexEncoderPipe.DEFAULT_VAD_BEGIN_LEN /*15*/:
                this.f574a.b(false);
                return;
            case 101:
                this.f574a.s();
                return;
            case 102:
                this.f574a.a(message);
                return;
            case 103:
                if (this.f574a.x.a(0, message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to stop recognition");
                    this.f574a.a(message);
                    return;
                }
                this.f574a.s = this.f574a.f569g;
                return;
            case 104:
                if (this.f574a.x.a(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to control event log");
                    this.f574a.a(message);
                    return;
                }
                return;
            case 105:
                if (this.f574a.x.b(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to control audio log");
                    this.f574a.a(message);
                    return;
                }
                return;
            case 106:
                if (this.f574a.x.c(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to control audio buffering");
                    this.f574a.a(message);
                    return;
                }
                return;
            case 107:
                this.f574a.a(message);
                return;
            case 108:
                this.f574a.a(message);
                return;
            case 109:
                if (this.f574a.x.e(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to request noise estimate");
                    this.f574a.a(message);
                    return;
                }
                return;
            case 110:
                this.f574a.a(message);
                return;
            case 111:
                if (this.f574a.x.f() != 0) {
                    Log.e(d.f563a, "Failed to request noise mode times");
                    this.f574a.a(message);
                    return;
                }
                return;
            case 201:
                this.f574a.b(true);
                return;
            case 202:
                if (this.f574a.x.d(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to send dock state change");
                    this.f574a.a(message);
                    return;
                }
                return;
            case 203:
            case 204:
                this.f574a.u = true;
                this.f574a.a(message);
                return;
            default:
                return;
        }
    }
}
