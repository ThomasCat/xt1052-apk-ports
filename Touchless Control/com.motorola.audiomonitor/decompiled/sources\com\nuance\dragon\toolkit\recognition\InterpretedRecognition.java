package com.nuance.dragon.toolkit.recognition;

import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.recognition.dictation.DictationResult;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.a.a;
import com.nuance.dragon.toolkit.util.a.b;
import com.nuance.dragon.toolkit.util.internal.f;
import com.nuance.dragon.toolkit.util.internal.g;
import com.nuance.dragon.toolkit.util.internal.h;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class InterpretedRecognition implements JSONCompliant {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final List f1880a;

    /* renamed from: b reason: collision with root package name */
    private final String f1881b;

    public final class Choice implements JSONCompliant {

        /* renamed from: a reason: collision with root package name */
        private final String f1882a;

        /* renamed from: b reason: collision with root package name */
        private final String f1883b;

        /* renamed from: c reason: collision with root package name */
        private final Map f1884c;

        /* renamed from: d reason: collision with root package name */
        private final List f1885d;

        /* renamed from: e reason: collision with root package name */
        private final int f1886e;

        /* renamed from: f reason: collision with root package name */
        private final Dictionary f1887f;

        /* renamed from: g reason: collision with root package name */
        private final boolean f1888g;

        /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.recognition.InterpretedRecognition$Phrase>, for r9v0, types: [java.util.List, java.util.Collection, java.util.List<com.nuance.dragon.toolkit.recognition.InterpretedRecognition$Phrase>] */
        private Choice(String str, String str2, int i, List<Phrase> list, Dictionary dictionary) {
            this.f1883b = str2;
            this.f1884c = new HashMap();
            this.f1885d = new ArrayList(list.size());
            this.f1882a = str;
            this.f1886e = i;
            this.f1887f = dictionary;
            boolean z = true;
            this.f1885d.addAll(list);
            for (Phrase phrase : list) {
                this.f1884c.put(phrase.getKey(), phrase);
                z = !phrase.isRecognized() ? false : z;
            }
            this.f1888g = z;
        }

        /* synthetic */ Choice(String str, String str2, int i, List list, Dictionary dictionary, byte b2) {
            this(str, str2, i, list, dictionary);
        }

        public static Choice createFromJSON(JSONObject jSONObject) {
            String string = jSONObject.getString(CloudInterpreter.KEY_TRANSCRIPTION);
            String string2 = jSONObject.getString("type");
            int i = jSONObject.getInt(CloudInterpreter.KEY_SCORE);
            JSONArray jSONArray = jSONObject.getJSONArray("phrases");
            ArrayList arrayList = new ArrayList(jSONArray.length());
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                arrayList.add(Phrase.createFromJSON(jSONArray.getJSONObject(i2)));
            }
            Dictionary dictionary = null;
            JSONObject optJSONObject = jSONObject.optJSONObject("extra");
            if (optJSONObject != null) {
                dictionary = Dictionary.createFromJSON(optJSONObject);
            }
            return new Choice(string, string2, i, arrayList, dictionary);
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Choice choice = (Choice) obj;
            if (this.f1887f == null) {
                if (choice.f1887f != null) {
                    return false;
                }
            } else if (!this.f1887f.equals(choice.f1887f)) {
                return false;
            }
            if (this.f1888g != choice.f1888g) {
                return false;
            }
            if (this.f1884c == null) {
                if (choice.f1884c != null) {
                    return false;
                }
            } else if (!this.f1884c.equals(choice.f1884c)) {
                return false;
            }
            if (this.f1885d == null) {
                if (choice.f1885d != null) {
                    return false;
                }
            } else if (!this.f1885d.equals(choice.f1885d)) {
                return false;
            }
            if (this.f1883b == null) {
                if (choice.f1883b != null) {
                    return false;
                }
            } else if (!this.f1883b.equals(choice.f1883b)) {
                return false;
            }
            if (this.f1886e != choice.f1886e) {
                return false;
            }
            return this.f1882a == null ? choice.f1882a == null : this.f1882a.equals(choice.f1882a);
        }

        public final Dictionary getExtra() {
            return this.f1887f;
        }

        public final Phrase getPhrase(String str) {
            return (Phrase) this.f1884c.get(str);
        }

        public final List getPhrases() {
            return this.f1885d;
        }

        public final String getRecognitionType() {
            return this.f1883b;
        }

        public final int getScore() {
            return this.f1886e;
        }

        public final int hashCode() {
            int i = 0;
            int hashCode = ((((this.f1883b == null ? 0 : this.f1883b.hashCode()) + (((this.f1885d == null ? 0 : this.f1885d.hashCode()) + (((this.f1884c == null ? 0 : this.f1884c.hashCode()) + (((this.f1888g ? 1231 : 1237) + (((this.f1887f == null ? 0 : this.f1887f.hashCode()) + 31) * 31)) * 31)) * 31)) * 31)) * 31) + this.f1886e) * 31;
            if (this.f1882a != null) {
                i = this.f1882a.hashCode();
            }
            return hashCode + i;
        }

        public final boolean isAllRecognized() {
            return this.f1888g;
        }

        public final boolean matches(Choice choice) {
            if (!f.a(choice.f1883b, this.f1883b)) {
                return false;
            }
            int size = this.f1885d.size();
            List list = choice.f1885d;
            if (list.size() != size) {
                return false;
            }
            for (int i = 0; i < size; i++) {
                if (!f.a(((Phrase) this.f1885d.get(i)).getKey(), ((Phrase) list.get(i)).getKey())) {
                    return false;
                }
            }
            return true;
        }

        public final Choice resolveUnrecognizedPhrases(InterpretedRecognition interpretedRecognition, boolean z) {
            Phrase phrase;
            if (isAllRecognized()) {
                return this;
            }
            ArrayList arrayList = new ArrayList(this.f1885d.size());
            Dictionary dictionary = null;
            for (Phrase phrase2 : this.f1885d) {
                if (!phrase2.f1894f) {
                    String key = phrase2.getKey();
                    if (key != null) {
                        Iterator it = interpretedRecognition.f1880a.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            Choice choice = (Choice) it.next();
                            if (matches(choice)) {
                                Phrase phrase3 = choice.getPhrase(key);
                                if (phrase3 != null && phrase3.f1894f) {
                                    if (dictionary == null) {
                                        dictionary = choice.f1887f;
                                        phrase = phrase3;
                                    } else if (choice.f1887f != null) {
                                        dictionary.putAll(choice.f1887f);
                                        phrase = phrase3;
                                    } else {
                                        phrase = phrase3;
                                    }
                                }
                            }
                        }
                    }
                    phrase = null;
                    if (phrase != null) {
                        phrase2 = phrase;
                    } else if (!z) {
                        Logger.warn(this, "Unable to resolve unrecognized phrases in " + this.f1882a);
                        return null;
                    }
                }
                arrayList.add(phrase2);
            }
            return new Choice(h.a((Iterable) arrayList, " "), this.f1883b, this.f1886e, arrayList, dictionary);
        }

        public final JSONObject toJSON() {
            b bVar = new b();
            bVar.a(CloudInterpreter.KEY_TRANSCRIPTION, (Object) this.f1882a);
            bVar.a("type", (Object) this.f1883b);
            bVar.a(CloudInterpreter.KEY_SCORE, (Object) Integer.valueOf(this.f1886e));
            if (this.f1887f != null) {
                bVar.a("extra", (JSONCompliant) this.f1887f);
            }
            a aVar = new a();
            for (Phrase json : this.f1885d) {
                aVar.a(json.toJSON());
            }
            bVar.a("phrases", (Object) aVar);
            return bVar;
        }

        public final String toString() {
            return this.f1882a;
        }
    }

    public final class Phrase implements JSONCompliant {

        /* renamed from: a reason: collision with root package name */
        private final String f1889a;

        /* renamed from: b reason: collision with root package name */
        private final String f1890b;

        /* renamed from: c reason: collision with root package name */
        private final g f1891c;

        /* renamed from: d reason: collision with root package name */
        private final Dictionary f1892d;

        /* renamed from: e reason: collision with root package name */
        private final DictationResult f1893e;
        /* access modifiers changed from: private */

        /* renamed from: f reason: collision with root package name */
        public final boolean f1894f;

        public Phrase(String str, DictationResult dictationResult, Dictionary dictionary) {
            this(str, dictationResult.toString(), null, dictationResult, dictionary, true);
        }

        public Phrase(String str, String str2, List list, Dictionary dictionary, boolean z) {
            this(str, str2, list, null, dictionary, z);
        }

        private Phrase(String str, String str2, List list, DictationResult dictationResult, Dictionary dictionary, boolean z) {
            this.f1889a = str;
            this.f1890b = str2;
            this.f1891c = list == null ? new g() : new g(list);
            this.f1892d = dictionary;
            this.f1893e = dictationResult;
            this.f1894f = z;
        }

        public static Phrase createFromJSON(JSONObject jSONObject) {
            com.nuance.dragon.toolkit.recognition.dictation.a.f fVar = null;
            String optString = jSONObject.optString("key");
            String string = jSONObject.getString(CloudInterpreter.KEY_TRANSCRIPTION);
            boolean z = jSONObject.getBoolean("isrecognized");
            JSONArray jSONArray = jSONObject.getJSONArray("words");
            ArrayList arrayList = new ArrayList(jSONArray.length());
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(Word.createFromJSON(jSONArray.getJSONObject(i)));
            }
            JSONObject optJSONObject = jSONObject.optJSONObject("extra");
            Dictionary dictionary = optJSONObject != null ? Dictionary.createFromJSON(optJSONObject) : null;
            JSONObject optJSONObject2 = jSONObject.optJSONObject("dictresult");
            if (optJSONObject2 != null) {
                fVar = com.nuance.dragon.toolkit.recognition.dictation.a.f.a(optJSONObject2);
            }
            return new Phrase(optString, string, arrayList, fVar, dictionary, z);
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Phrase phrase = (Phrase) obj;
            if (this.f1893e == null) {
                if (phrase.f1893e != null) {
                    return false;
                }
            } else if (!this.f1893e.equals(phrase.f1893e)) {
                return false;
            }
            if (this.f1892d == null) {
                if (phrase.f1892d != null) {
                    return false;
                }
            } else if (!this.f1892d.equals(phrase.f1892d)) {
                return false;
            }
            if (this.f1894f != phrase.f1894f) {
                return false;
            }
            if (this.f1889a == null) {
                if (phrase.f1889a != null) {
                    return false;
                }
            } else if (!this.f1889a.equals(phrase.f1889a)) {
                return false;
            }
            if (this.f1890b == null) {
                if (phrase.f1890b != null) {
                    return false;
                }
            } else if (!this.f1890b.equals(phrase.f1890b)) {
                return false;
            }
            return this.f1891c == null ? phrase.f1891c == null : this.f1891c.equals(phrase.f1891c);
        }

        public final DictationResult getDictationResult() {
            return this.f1893e;
        }

        public final long getEndTime() {
            if (!this.f1891c.isEmpty()) {
                return ((Word) this.f1891c.get(this.f1891c.size() - 1)).f1899e;
            }
            return 0;
        }

        public final Dictionary getExtra() {
            return this.f1892d;
        }

        public final String getKey() {
            return this.f1889a;
        }

        public final long getStartTime() {
            if (!this.f1891c.isEmpty()) {
                return ((Word) this.f1891c.get(0)).f1898d;
            }
            return 0;
        }

        public final List getWords() {
            return this.f1891c;
        }

        public final int hashCode() {
            int i = 0;
            int hashCode = ((this.f1890b == null ? 0 : this.f1890b.hashCode()) + (((this.f1889a == null ? 0 : this.f1889a.hashCode()) + (((this.f1894f ? 1231 : 1237) + (((this.f1892d == null ? 0 : this.f1892d.hashCode()) + (((this.f1893e == null ? 0 : this.f1893e.hashCode()) + 31) * 31)) * 31)) * 31)) * 31)) * 31;
            if (this.f1891c != null) {
                i = this.f1891c.hashCode();
            }
            return hashCode + i;
        }

        public final boolean isRecognized() {
            return this.f1894f;
        }

        public final JSONObject toJSON() {
            b bVar = new b();
            bVar.a("key", (Object) this.f1889a);
            bVar.a(CloudInterpreter.KEY_TRANSCRIPTION, (Object) this.f1890b);
            if (this.f1892d != null) {
                bVar.a("extra", (Object) this.f1892d.toJSON());
            }
            bVar.a("words", (List) this.f1891c);
            bVar.a("isrecognized", (Object) Boolean.valueOf(this.f1894f));
            if (this.f1893e != null) {
                bVar.a("dictresult", (Object) this.f1893e.toJSON());
            }
            return bVar;
        }

        public final String toString() {
            return this.f1890b;
        }
    }

    public final class Word implements JSONCompliant {
        public static final int TIMING_NOT_SUPPORTED = 0;

        /* renamed from: a reason: collision with root package name */
        private final String f1895a;

        /* renamed from: b reason: collision with root package name */
        private final String f1896b;

        /* renamed from: c reason: collision with root package name */
        private final String f1897c;
        /* access modifiers changed from: private */

        /* renamed from: d reason: collision with root package name */
        public final long f1898d;
        /* access modifiers changed from: private */

        /* renamed from: e reason: collision with root package name */
        public final long f1899e;

        public Word(String str, String str2, String str3, long j, long j2) {
            this.f1895a = str;
            this.f1896b = str2;
            this.f1897c = str3;
            this.f1898d = j;
            this.f1899e = j2;
        }

        public static Word createFromJSON(JSONObject jSONObject) {
            return new Word(jSONObject.getString("text"), jSONObject.optString("fulltext", null), jSONObject.optString("pron", null), (long) jSONObject.getInt("start"), (long) jSONObject.getInt("end"));
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Word word = (Word) obj;
            if (this.f1899e != word.f1899e) {
                return false;
            }
            if (this.f1896b == null) {
                if (word.f1896b != null) {
                    return false;
                }
            } else if (!this.f1896b.equals(word.f1896b)) {
                return false;
            }
            if (this.f1897c == null) {
                if (word.f1897c != null) {
                    return false;
                }
            } else if (!this.f1897c.equals(word.f1897c)) {
                return false;
            }
            if (this.f1898d != word.f1898d) {
                return false;
            }
            return this.f1895a == null ? word.f1895a == null : this.f1895a.equals(word.f1895a);
        }

        public final long getEndTime() {
            return this.f1899e;
        }

        public final String getFullText() {
            return this.f1896b == null ? this.f1895a : this.f1896b;
        }

        public final String getPronunciation() {
            return this.f1897c;
        }

        public final long getStartTime() {
            return this.f1898d;
        }

        public final String getText() {
            return this.f1895a;
        }

        public final int hashCode() {
            int i = 0;
            int hashCode = ((((this.f1897c == null ? 0 : this.f1897c.hashCode()) + (((this.f1896b == null ? 0 : this.f1896b.hashCode()) + ((((int) (this.f1899e ^ (this.f1899e >>> 32))) + 31) * 31)) * 31)) * 31) + ((int) (this.f1898d ^ (this.f1898d >>> 32)))) * 31;
            if (this.f1895a != null) {
                i = this.f1895a.hashCode();
            }
            return hashCode + i;
        }

        public final boolean isTimingSupported() {
            return (this.f1898d == 0 && this.f1899e == 0) ? false : true;
        }

        public final JSONObject toJSON() {
            b bVar = new b();
            bVar.a("text", (Object) this.f1895a);
            bVar.a("fulltext", (Object) this.f1896b);
            bVar.a("pron", (Object) this.f1897c);
            bVar.a("start", (Object) Long.valueOf(this.f1898d));
            bVar.a("end", (Object) Long.valueOf(this.f1899e));
            return bVar;
        }

        public final String toString() {
            return this.f1895a;
        }
    }

    public InterpretedRecognition() {
        this(null);
    }

    public InterpretedRecognition(String str) {
        this.f1880a = new ArrayList();
        this.f1881b = str;
    }

    private InterpretedRecognition(List list, String str) {
        this.f1880a = new ArrayList(list.size());
        this.f1880a.addAll(list);
        this.f1881b = str;
    }

    public static InterpretedRecognition createFromJSON(JSONObject jSONObject) {
        JSONArray jSONArray = jSONObject.getJSONArray(CloudInterpreter.KEY_CHOICES);
        ArrayList arrayList = new ArrayList(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(Choice.createFromJSON(jSONArray.getJSONObject(i)));
        }
        return new InterpretedRecognition(arrayList, jSONObject.optString("prompt", null));
    }

    public final void addChoice(String str, String str2, int i, List list, Dictionary dictionary) {
        this.f1880a.add(new Choice(str, str2, i, list, dictionary, 0));
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        InterpretedRecognition interpretedRecognition = (InterpretedRecognition) obj;
        if (this.f1880a == null) {
            if (interpretedRecognition.f1880a != null) {
                return false;
            }
        } else if (!this.f1880a.equals(interpretedRecognition.f1880a)) {
            return false;
        }
        return this.f1881b == null ? interpretedRecognition.f1881b == null : this.f1881b.equals(interpretedRecognition.f1881b);
    }

    public final Choice getChoice(int i) {
        return (Choice) this.f1880a.get(i);
    }

    public final int getChoiceCount() {
        return this.f1880a.size();
    }

    public final Choice getFirstChoice() {
        return (Choice) this.f1880a.get(0);
    }

    public final Phrase getPhrase(String str) {
        if (!this.f1880a.isEmpty()) {
            return ((Choice) this.f1880a.get(0)).getPhrase(str);
        }
        return null;
    }

    public final List getPhrases() {
        return !this.f1880a.isEmpty() ? ((Choice) this.f1880a.get(0)).getPhrases() : new ArrayList(0);
    }

    public final String getPrompt() {
        return this.f1881b;
    }

    public final int getScore() {
        if (!this.f1880a.isEmpty()) {
            return ((Choice) this.f1880a.get(0)).getScore();
        }
        return 0;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f1880a == null ? 0 : this.f1880a.hashCode()) + 31) * 31;
        if (this.f1881b != null) {
            i = this.f1881b.hashCode();
        }
        return hashCode + i;
    }

    public final JSONObject toJSON() {
        b bVar = new b();
        a aVar = new a();
        for (Choice json : this.f1880a) {
            aVar.a(json.toJSON());
        }
        bVar.a(CloudInterpreter.KEY_CHOICES, (Object) aVar);
        bVar.a("prompt", (Object) this.f1881b);
        return bVar;
    }

    public final String toString() {
        return !this.f1880a.isEmpty() ? ((Choice) this.f1880a.get(0)).toString() : "[no choices]";
    }
}
