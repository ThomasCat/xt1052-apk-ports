package com.motorola.audiomonitor;

import android.util.Log;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;

public class FlacEncoder implements q {

    /* renamed from: c reason: collision with root package name */
    private static final String f136c = ("AMonitor." + FlacEncoder.class.getSimpleName());

    /* renamed from: d reason: collision with root package name */
    private static final boolean f137d = a.f160b;

    /* renamed from: a reason: collision with root package name */
    int f138a = 0;

    /* renamed from: b reason: collision with root package name */
    int f139b = 0;

    /* renamed from: e reason: collision with root package name */
    private long f140e = 0;

    /* renamed from: f reason: collision with root package name */
    private int f141f = 0;

    /* renamed from: g reason: collision with root package name */
    private int f142g = 0;

    /* renamed from: h reason: collision with root package name */
    private final Object f143h = new Object();
    private final List i = new LinkedList();
    private final List j = new LinkedList();
    private boolean k = true;
    private boolean l = false;
    private boolean m = false;
    private byte[] n;

    static {
        System.loadLibrary("flacencoder");
    }

    public FlacEncoder(int i2, int i3) {
        this.f141f = i2;
        this.f142g = i3;
        this.f140e = encoderInit(i2, i3);
    }

    private boolean a(long j2, ProtectedByteBuffer protectedByteBuffer) {
        return encode(j2, protectedByteBuffer, protectedByteBuffer.capacity());
    }

    private void b(OutputStream outputStream) {
        if (!this.j.isEmpty()) {
            for (ByteBuffer byteBuffer : this.j) {
                if (this.n == null || this.n.length < byteBuffer.capacity()) {
                    this.n = new byte[byteBuffer.capacity()];
                }
                byteBuffer.get(this.n, 0, byteBuffer.capacity());
                outputStream.write(this.n, 0, byteBuffer.capacity());
                disposeBuffer(byteBuffer);
            }
            this.j.clear();
        }
    }

    private native void disposeBuffer(ByteBuffer byteBuffer);

    private native boolean encode(long j2, ProtectedByteBuffer protectedByteBuffer, int i2);

    private native long encoderInit(int i2, int i3);

    private native void finishEncode(long j2);

    private native int getMaxInputBufferSize(long j2);

    private void onAudioEncoded(ByteBuffer byteBuffer, int i2, int i3) {
        this.f139b += byteBuffer.capacity();
        this.j.add(byteBuffer);
    }

    public int a() {
        return getMaxInputBufferSize(this.f140e);
    }

    public void a(OutputStream outputStream) {
        while (this.k) {
            ProtectedByteBuffer protectedByteBuffer = null;
            synchronized (this.f143h) {
                if (!this.i.isEmpty()) {
                    protectedByteBuffer = (ProtectedByteBuffer) this.i.remove(0);
                } else if (this.l) {
                    this.k = false;
                } else {
                    try {
                        this.f143h.wait();
                    } catch (InterruptedException e2) {
                    }
                }
            }
            if (protectedByteBuffer != null) {
                this.f138a += protectedByteBuffer.capacity();
                if (a(this.f140e, protectedByteBuffer)) {
                    b(outputStream);
                } else {
                    Log.e(f136c, "Failed to queue audio for FLAC encoding.");
                }
            }
        }
        finishEncode(this.f140e);
        this.f140e = 0;
        if (!this.m) {
            b(outputStream);
            return;
        }
        throw new IOException("FLAC encoding interrupted");
    }

    public boolean a(ProtectedByteBuffer protectedByteBuffer) {
        if (protectedByteBuffer.capacity() > a()) {
            Log.e(f136c, "Exceeding buffer size FLAC encoder can support");
            return false;
        }
        synchronized (this.f143h) {
            this.i.add(protectedByteBuffer);
            this.f143h.notify();
        }
        return true;
    }

    public void b() {
        synchronized (this.f143h) {
            this.l = true;
            this.f143h.notify();
        }
    }

    public void c() {
        synchronized (this.f143h) {
            this.k = false;
            this.m = true;
            this.f143h.notify();
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        this.i.clear();
        for (ByteBuffer disposeBuffer : this.j) {
            disposeBuffer(disposeBuffer);
        }
        this.j.clear();
        if (this.f140e > 0) {
            Log.w(f136c, "finalize: encoder=" + this.f140e + ", this=" + this);
            finishEncode(this.f140e);
            this.f140e = 0;
        }
    }
}
