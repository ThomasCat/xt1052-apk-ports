package com.motorola.audiomonitor;

import android.util.Log;
import com.motorola.audiomonitor.service.d;
import java.util.LinkedList;
import java.util.List;

public abstract class cb extends Thread implements k {

    /* renamed from: c reason: collision with root package name */
    private static final String f397c = ("AMonitor." + cb.class.getSimpleName());

    /* renamed from: d reason: collision with root package name */
    private static final boolean f398d = a.f160b;

    /* renamed from: a reason: collision with root package name */
    protected GenericRecognizer f399a;

    /* renamed from: b reason: collision with root package name */
    protected RecognizerConfig f400b;

    /* renamed from: e reason: collision with root package name */
    private final List f401e = new LinkedList();

    /* renamed from: f reason: collision with root package name */
    private List f402f = new LinkedList();

    /* renamed from: g reason: collision with root package name */
    private boolean f403g = false;

    /* renamed from: h reason: collision with root package name */
    private final p f404h;
    private final d i;

    public cb(cb cbVar) {
        cbVar.b();
        this.f399a = cbVar.f399a;
        this.f400b = cbVar.f400b;
        this.f404h = cbVar.f404h;
        this.i = cbVar.i;
        cbVar.f399a = null;
        cbVar.f400b = null;
    }

    public cb(p pVar, d dVar) {
        this.f404h = pVar;
        this.i = dVar;
    }

    private void a(boolean z) {
        if (this.i != null) {
            if (z) {
                this.i.f();
            } else {
                this.i.h();
            }
        }
    }

    public void a() {
        start();
    }

    public abstract void a(int i2);

    public abstract void a(List list);

    public boolean a(ProtectedByteBuffer protectedByteBuffer) {
        synchronized (this) {
            if (this.f402f != null) {
                this.f402f.add(protectedByteBuffer);
            }
        }
        synchronized (this.f401e) {
            if (this.f401e.size() == 50) {
                Log.w(f397c, "**** OVERFLOW *****");
            } else {
                this.f401e.add(protectedByteBuffer);
                this.f401e.notify();
            }
        }
        return true;
    }

    public void b() {
        synchronized (this) {
            this.f403g = false;
            interrupt();
        }
        try {
            join(3000);
        } catch (InterruptedException e2) {
        }
    }

    public void c() {
        synchronized (this) {
            if (this.f399a != null) {
                this.f399a.cleanUp();
                this.f399a = null;
            }
            if (this.f402f != null) {
                this.f402f.clear();
                this.f402f = null;
            }
        }
        this.f400b = null;
    }

    public abstract boolean d();

    public abstract boolean e();

    public abstract void f();

    public abstract boolean g();

    public abstract void h();

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0033, code lost:
        if (r6.f403g == false) goto L_0x00ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0035, code lost:
        r2 = r6.f401e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0037, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x003e, code lost:
        if (r6.f401e.size() != 0) goto L_0x008d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r6.f401e.wait();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0045, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        android.util.Log.d(f397c, "Interrupted.");
        r6.f403g = false;
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x008d, code lost:
        r0 = (com.motorola.audiomonitor.ProtectedByteBuffer) r6.f401e.remove(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00ae, code lost:
        r6.f404h.b();
        a(false);
        a(r6.f402f);
        r6.f402f = null;
        h();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00c2, code lost:
        if (f398d == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00c4, code lost:
        android.util.Log.d(f397c, "Command processing stopped.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
        return;
     */
    public void run() {
        List list;
        ProtectedByteBuffer protectedByteBuffer;
        synchronized (this) {
            if (this.f399a != null || (d() && this.f399a != null)) {
                this.f399a.reset();
                if (g()) {
                    this.f404h.a();
                    a(true);
                    this.f403g = true;
                } else {
                    return;
                }
            }
        }
        if (!(protectedByteBuffer == null || this.f399a == null)) {
            n recognize = this.f399a.recognize(protectedByteBuffer);
            if (recognize == n.RECOGNIZED) {
                this.f404h.b();
                a(false);
                synchronized (this) {
                    this.f403g = e();
                    this.f399a.reset();
                }
                if (this.f403g) {
                    a(true);
                    this.f404h.a();
                }
            } else if (recognize == n.ERROR) {
                Log.e(f397c, "Error during Recognition");
                f();
                this.f403g = false;
            }
            a((int) protectedByteBuffer.getFirstShort());
        }
    }
}
