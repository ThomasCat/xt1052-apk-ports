package com.motorola.audiomonitor.uis;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.actions.utils.a;
import com.motorola.audiomonitor.actions.utils.h;
import com.motorola.audiomonitor.actions.utils.m;
import h.k;

class f extends BaseAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final Context f699a;

    public f(Context context) {
        this.f699a = context;
    }

    /* access modifiers changed from: private */
    public void a(a aVar, Context context) {
        if (!m.a().a(true) && !h.a().d() && !h.a().e()) {
            Intent intent = new Intent("android.intent.action.CALL");
            intent.addFlags(268435456);
            intent.setData(Uri.fromParts("tel", aVar.g(), null));
            context.startActivity(intent);
        }
    }

    public int getCount() {
        return d.f693g.size();
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        j jVar;
        Bitmap bitmap = null;
        if (view == null) {
            view = LayoutInflater.from(this.f699a).inflate(C0014R.layout.contact_view_item, null);
            jVar = new j();
            jVar.f708c = (ImageView) view.findViewById(C0014R.id.contact_icon);
            jVar.f709d = (ImageView) view.findViewById(C0014R.id.contact_call_icon);
            jVar.f706a = (TextView) view.findViewById(C0014R.id.call_item_name);
            jVar.f707b = (TextView) view.findViewById(C0014R.id.call_item_number);
            jVar.f710e = (ImageView) view.findViewById(C0014R.id.contact_highlight);
            view.setTag(jVar);
        } else {
            jVar = (j) view.getTag();
        }
        jVar.f709d.setVisibility(8);
        if (i == d.f690a) {
            jVar.f710e.setVisibility(0);
        } else {
            jVar.f710e.setVisibility(8);
        }
        a aVar = (a) d.f693g.get(i);
        String c2 = aVar.c();
        String a2 = com.motorola.audiomonitor.actions.utils.f.a(this.f699a, aVar);
        String i2 = k.i(PhoneNumberUtils.formatNumber(aVar.g()));
        jVar.f706a.setText(c2);
        jVar.f707b.setText(i2 + " " + a2);
        if (!TextUtils.isEmpty(aVar.b())) {
            bitmap = com.motorola.audiomonitor.actions.utils.f.b(this.f699a, aVar.b());
        }
        if (bitmap != null) {
            jVar.f708c.setImageBitmap(bitmap);
        } else {
            jVar.f708c.setImageResource(C0014R.drawable.vc_default_contact);
        }
        jVar.f708c.setOnClickListener(new g(this, aVar));
        view.setOnClickListener(new h(this, aVar, i));
        return view;
    }
}
