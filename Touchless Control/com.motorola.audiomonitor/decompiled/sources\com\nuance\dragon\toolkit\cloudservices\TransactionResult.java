package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.a.a.a.b.c.c.c;
import com.nuance.a.a.a.b.c.c.j;
import com.nuance.dragon.toolkit.data.Data;
import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.data.Data.Integer;
import com.nuance.dragon.toolkit.data.Data.String;
import com.nuance.dragon.toolkit.data.a.a;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import org.json.JSONObject;

public class TransactionResult implements JSONCompliant {
    protected static final String KEY_RESULT = "res";
    protected static final String KEY_SESSION = "session";

    /* renamed from: a reason: collision with root package name */
    private Dictionary f1383a;

    /* renamed from: b reason: collision with root package name */
    private String f1384b;

    /* renamed from: c reason: collision with root package name */
    private final String f1385c;

    TransactionResult(Transaction transaction, j jVar) {
        this.f1383a = a.a((c) jVar);
        this.f1384b = a(this.f1383a);
        this.f1385c = transaction.i();
    }

    private TransactionResult(JSONObject jSONObject) {
        JSONObject optJSONObject = jSONObject.optJSONObject(KEY_RESULT);
        if (optJSONObject != null) {
            this.f1383a = Dictionary.createFromJSON(optJSONObject);
        } else {
            this.f1383a = null;
        }
        this.f1384b = a(this.f1383a);
        this.f1385c = jSONObject.optString(KEY_SESSION, null);
    }

    private static String a(Dictionary dictionary) {
        if (dictionary != null) {
            Data data = dictionary.get("result_type");
            if (data != null && data.getType() == 193) {
                return ((String) data).value;
            }
        }
        return "no_result_type";
    }

    public static TransactionResult createFromJSON(JSONObject jSONObject) {
        return new TransactionResult(jSONObject);
    }

    public Dictionary getContents() {
        return this.f1383a;
    }

    public String getResultType() {
        return this.f1384b;
    }

    public String getSessionId() {
        return this.f1385c;
    }

    public boolean isFinal() {
        Data data = this.f1383a.get("final_response");
        if (data == null) {
            Dictionary dictionary = this.f1383a.getDictionary("appserver_results");
            if (dictionary != null) {
                data = dictionary.get("final_response");
            }
        }
        if (data == null || !(data instanceof Integer)) {
            return true;
        }
        return ((Integer) data).value == 1;
    }

    public JSONObject toJSON() {
        b bVar = new b();
        if (this.f1383a != null) {
            bVar.a(KEY_RESULT, (Object) this.f1383a.toJSON());
        }
        bVar.a(KEY_SESSION, (Object) this.f1385c);
        return bVar;
    }
}
