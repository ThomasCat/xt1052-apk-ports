package com.motorola.audiomonitor.uis;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.TextView;
import com.motorola.audiomonitor.C0014R;
import h.d;
import h.k;

public class PrivacyActivity extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f626a = ("AMonitor." + PrivacyActivity.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public CheckBox f627b;

    /* renamed from: c reason: collision with root package name */
    private Dialog f628c;

    /* renamed from: d reason: collision with root package name */
    private boolean f629d = false;

    /* access modifiers changed from: 0000 */
    public void a() {
        Builder builder = new Builder(this);
        ak akVar = new ak(this);
        builder.setTitle(C0014R.string.privacy_dialog_title).setMessage(C0014R.string.privacy_dialog_warning).setPositiveButton(C0014R.string.dialog_ok, akVar).setNegativeButton(C0014R.string.dialog_cancel, akVar).setCancelable(false);
        this.f628c = builder.create();
        this.f628c.show();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle extras = getIntent().getExtras();
        if (extras == null || !extras.containsKey("PRIVACY_VIEW_ONLY")) {
            this.f629d = false;
        } else {
            this.f629d = extras.getBoolean("PRIVACY_VIEW_ONLY");
        }
        setContentView(C0014R.layout.aov_privacy);
        ActionBar actionBar = getActionBar();
        actionBar.setIcon(k.b());
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(C0014R.string.privacy_title);
    }

    public boolean onNavigateUp() {
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.f628c != null && this.f628c.isShowing()) {
            this.f628c.dismiss();
            this.f628c = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.f627b = (CheckBox) findViewById(C0014R.id.chkPrivacy);
        if (this.f629d) {
            this.f627b.setVisibility(8);
        } else {
            this.f627b.setChecked(d.b(getApplicationContext()));
            this.f627b.setOnCheckedChangeListener(new ai(this));
        }
        String string = getResources().getString(C0014R.string.privacy_moreinfo);
        SpannableString spannableString = new SpannableString(string);
        aj ajVar = new aj(this);
        int indexOf = string.indexOf(getResources().getString(C0014R.string.privacy_link));
        int length = getResources().getString(C0014R.string.privacy_link).length() + indexOf;
        if (indexOf >= 0) {
            spannableString.setSpan(ajVar, indexOf, length, 33);
        } else {
            Log.e(f626a, "Error creating privacy policy link");
        }
        TextView textView = (TextView) findViewById(C0014R.id.privacy_learn_more);
        textView.setText(spannableString);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
