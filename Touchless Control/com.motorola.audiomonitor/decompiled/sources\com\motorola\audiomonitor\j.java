package com.motorola.audiomonitor;

import android.os.HandlerThread;
import android.util.Log;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public abstract class j extends HandlerThread implements p {

    /* renamed from: b reason: collision with root package name */
    private static final String f503b = ("AMonitor." + j.class.getSimpleName());

    /* renamed from: a reason: collision with root package name */
    protected boolean f504a = false;

    /* renamed from: c reason: collision with root package name */
    private final Set f505c = new CopyOnWriteArraySet();

    /* renamed from: d reason: collision with root package name */
    private final Set f506d = new CopyOnWriteArraySet();

    /* renamed from: e reason: collision with root package name */
    private final Set f507e = new CopyOnWriteArraySet();

    protected j(String str) {
        super(str);
    }

    public abstract void a();

    public abstract void a(int i, int i2);

    public void a(k kVar) {
        if (kVar == null) {
            Log.w(f503b, "add invalid audio listener");
        } else {
            this.f505c.add(kVar);
        }
    }

    public void a(l lVar) {
        if (lVar == null) {
            Log.w(f503b, "add invalid control listener");
        } else {
            this.f506d.add(lVar);
        }
    }

    public void a(boolean z) {
        this.f504a = z;
    }

    /* access modifiers changed from: protected */
    public boolean a(ProtectedByteBuffer protectedByteBuffer) {
        for (k kVar : this.f505c) {
            if (kVar == null) {
                Log.w(f503b, "invalid audio listener");
            } else {
                boolean a2 = kVar.a(protectedByteBuffer);
                if (!a2) {
                    return a2;
                }
            }
        }
        return true;
    }

    public abstract void b();

    public void b(k kVar) {
        if (kVar == null) {
            Log.w(f503b, "remove invalid audio listener");
        } else {
            this.f505c.remove(kVar);
        }
    }

    /* access modifiers changed from: protected */
    public boolean b(ProtectedByteBuffer protectedByteBuffer) {
        for (k kVar : this.f507e) {
            if (kVar == null) {
                Log.w(f503b, "invalid 2nd channel audio listener");
            } else {
                boolean a2 = kVar.a(protectedByteBuffer);
                if (!a2) {
                    return a2;
                }
            }
        }
        return true;
    }

    public void c() {
        d();
        e();
        f();
    }

    public void c(k kVar) {
        if (kVar == null) {
            Log.w(f503b, "add invalid 2nd channel audio listener");
        } else {
            this.f507e.add(kVar);
        }
    }

    public void d() {
        this.f505c.clear();
    }

    public void e() {
        this.f507e.clear();
    }

    public void f() {
        this.f506d.clear();
    }

    /* access modifiers changed from: protected */
    public void g() {
        for (l lVar : this.f506d) {
            if (lVar == null) {
                Log.e(f503b, "invalid control listener");
            } else {
                lVar.a();
            }
        }
    }
}
