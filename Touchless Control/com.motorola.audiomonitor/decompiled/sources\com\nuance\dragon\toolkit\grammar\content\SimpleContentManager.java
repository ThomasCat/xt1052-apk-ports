package com.nuance.dragon.toolkit.grammar.content;

import android.content.Context;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.grammar.Word;
import com.nuance.dragon.toolkit.grammar.WordAction;
import com.nuance.dragon.toolkit.grammar.content.ContentManager.PlatformFullIterator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class SimpleContentManager extends ContentManager {

    /* renamed from: a reason: collision with root package name */
    private boolean f1796a;

    /* renamed from: b reason: collision with root package name */
    private boolean f1797b;

    /* renamed from: c reason: collision with root package name */
    private final ArrayList f1798c;

    final class a implements PlatformFullIterator {

        /* renamed from: a reason: collision with root package name */
        private final Iterator f1799a;

        /* renamed from: b reason: collision with root package name */
        private final int f1800b;

        a(List list) {
            this.f1799a = new ArrayList(list).iterator();
            this.f1800b = list.size();
        }

        public final int getSize() {
            return this.f1800b;
        }

        public final boolean hasNext() {
            return this.f1799a.hasNext();
        }

        public final /* bridge */ /* synthetic */ Object next() {
            return (WordAction) this.f1799a.next();
        }

        public final void remove() {
            this.f1799a.remove();
        }
    }

    public SimpleContentManager(String str, FileManager fileManager, boolean z, boolean z2, Context context, Word... wordArr) {
        super(str, fileManager, z2, context);
        this.f1798c = new ArrayList(wordArr.length);
        for (Word wordAction : wordArr) {
            this.f1798c.add(new WordAction(wordAction, true));
        }
        initialize(z);
    }

    private void a() {
        if (this.f1797b) {
            this.f1796a = false;
            onContentUpdate();
            return;
        }
        this.f1796a = true;
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.Collection, code=java.util.Collection<com.nuance.dragon.toolkit.grammar.Word>, for r6v0, types: [java.util.Collection, java.util.Collection<com.nuance.dragon.toolkit.grammar.Word>] */
    public void addContent(Collection<Word> collection) {
        this.f1798c.ensureCapacity(this.f1798c.size() + collection.size());
        for (Word wordAction : collection) {
            this.f1798c.add(new WordAction(wordAction, true));
        }
        a();
    }

    public void addContent(Word... wordArr) {
        this.f1798c.ensureCapacity(this.f1798c.size() + wordArr.length);
        for (Word wordAction : wordArr) {
            this.f1798c.add(new WordAction(wordAction, true));
        }
        a();
    }

    public void clearContent() {
        this.f1798c.clear();
        a();
    }

    /* access modifiers changed from: protected */
    public PlatformFullIterator getPlatformFullIterator() {
        return new a(this.f1798c);
    }

    /* access modifiers changed from: protected */
    public void stopWatchingForContentUpdates(Context context) {
        this.f1797b = false;
    }

    /* access modifiers changed from: protected */
    public void watchForContentUpdates(Context context) {
        this.f1797b = true;
        if (this.f1796a) {
            a();
        }
    }
}
