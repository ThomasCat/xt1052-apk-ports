package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.dragon.toolkit.util.internal.d;
import org.json.JSONObject;

public class StringParam extends DataParam {
    protected final String _value;

    public StringParam(String str, String str2) {
        super(3, str);
        d.a("value", (Object) str2);
        this._value = str2;
    }

    StringParam(JSONObject jSONObject) {
        super(jSONObject);
        this._value = jSONObject.getString("value");
    }

    /* access modifiers changed from: 0000 */
    public boolean a(d dVar, a aVar) {
        dVar.a(this.f1355a, this._value);
        return true;
    }
}
