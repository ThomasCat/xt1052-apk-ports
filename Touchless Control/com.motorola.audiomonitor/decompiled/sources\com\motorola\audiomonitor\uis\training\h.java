package com.motorola.audiomonitor.uis.training;

import com.motorola.audiomonitor.aw;

class h extends aw {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ TrainingTriggerRecordingActivity f745a;

    h(TrainingTriggerRecordingActivity trainingTriggerRecordingActivity) {
        this.f745a = trainingTriggerRecordingActivity;
    }

    public void a(int i) {
        if (i == 1) {
            this.f745a.l.post(new i(this));
        }
    }

    public void a(int i, int i2) {
        this.f745a.l.post(new k(this, i, i2));
    }

    public void b(int i) {
        if (1 == i) {
            this.f745a.l.post(new j(this));
        }
    }

    public void c(int i) {
        int abs = Math.abs(i);
        if (abs > this.f745a.K) {
            this.f745a.K = abs;
        }
    }
}
