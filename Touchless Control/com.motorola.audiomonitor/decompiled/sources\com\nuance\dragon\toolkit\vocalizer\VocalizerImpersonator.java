package com.nuance.dragon.toolkit.vocalizer;

import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.LoadListener;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.ReleaseListener;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.ResourceLoadListener;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.ResourceReleaseListener;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.TtsListener;
import java.util.List;

public class VocalizerImpersonator extends b {
    public /* bridge */ /* synthetic */ void cancelTts() {
        super.cancelTts();
    }

    public /* bridge */ /* synthetic */ TtsAudioSource generateTts(String str, TtsListener ttsListener, Object obj) {
        return super.generateTts(str, ttsListener, obj);
    }

    public /* bridge */ /* synthetic */ AudioType getAudioType() {
        return super.getAudioType();
    }

    public /* bridge */ /* synthetic */ List getAvailableModels() {
        return super.getAvailableModels();
    }

    /* access modifiers changed from: protected */
    public Handler getWorkerThreadHandler() {
        return new Handler();
    }

    public /* bridge */ /* synthetic */ void load(VocalizerConfig vocalizerConfig) {
        super.load(vocalizerConfig);
    }

    public /* bridge */ /* synthetic */ void load(VocalizerConfig vocalizerConfig, LoadListener loadListener) {
        super.load(vocalizerConfig, loadListener);
    }

    public /* bridge */ /* synthetic */ void loadResource(String str, String str2, ResourceLoadListener resourceLoadListener) {
        super.loadResource(str, str2, resourceLoadListener);
    }

    public /* bridge */ /* synthetic */ void release() {
        super.release();
    }

    public /* bridge */ /* synthetic */ void release(ReleaseListener releaseListener) {
        super.release(releaseListener);
    }

    public /* bridge */ /* synthetic */ void releaseResource(String str, ResourceReleaseListener resourceReleaseListener) {
        super.releaseResource(str, resourceReleaseListener);
    }

    public /* bridge */ /* synthetic */ void setAudioChunkBufferCount(int i) {
        super.setAudioChunkBufferCount(i);
    }

    public /* bridge */ /* synthetic */ void setAudioChunkSize(int i) {
        super.setAudioChunkSize(i);
    }

    public /* bridge */ /* synthetic */ void setTtsSpeed(int i) {
        super.setTtsSpeed(i);
    }

    public /* bridge */ /* synthetic */ void setTtsVolume(int i) {
        super.setTtsVolume(i);
    }
}
