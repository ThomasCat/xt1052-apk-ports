package com.nuance.dragon.toolkit.recognition.dictation.a;

import com.nuance.dragon.toolkit.recognition.dictation.Alternative;
import com.nuance.dragon.toolkit.recognition.dictation.Token;
import java.util.ArrayList;

final class c implements Alternative {

    /* renamed from: a reason: collision with root package name */
    private ArrayList f1928a;

    public c() {
        this.f1928a = new ArrayList();
    }

    public c(ArrayList arrayList) {
        this.f1928a = arrayList;
    }

    public final int size() {
        return this.f1928a.size();
    }

    public final String toString() {
        if (this.f1928a.size() == 0) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer("");
        int i = 0;
        boolean z = true;
        while (true) {
            int i2 = i;
            if (i2 >= this.f1928a.size()) {
                return stringBuffer.toString();
            }
            h hVar = (h) this.f1928a.get(i2);
            if (!hVar.hasNoSpaceBeforeDirective() && !z) {
                stringBuffer.append(' ');
            }
            stringBuffer.append(tokenAt(i2).toString());
            z = hVar.hasNoSpaceAfterDirective();
            i = i2 + 1;
        }
    }

    public final Token tokenAt(int i) {
        return (h) this.f1928a.get(i);
    }
}
