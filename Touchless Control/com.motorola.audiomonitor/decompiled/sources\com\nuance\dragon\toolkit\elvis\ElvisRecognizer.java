package com.nuance.dragon.toolkit.elvis;

import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.SpeechDetectionListener;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.grammar.Word;
import java.util.ArrayList;
import java.util.List;

public abstract class ElvisRecognizer {

    public final class Frequencies {
        public static final int FREQ_16KHZ = 16000;
        public static final int FREQ_8KHZ = 8000;
    }

    public interface InitializeListener {
        void onLoaded(ElvisRecognizer elvisRecognizer, boolean z);
    }

    public final class Languages extends d {
    }

    public final class Logging {
        public static final int LOGGING_COMPLETE = 3;
        public static final int LOGGING_DISABLED = 0;
        public static final int LOGGING_ERROR = 4;
        public static final int LOGGING_IN_PROGRESS = 2;
        public static final int LOGGING_NOT_STARTED = 1;
    }

    public interface RebuildListener {

        public class SkippedWord {

            /* renamed from: a reason: collision with root package name */
            private Word f1543a;

            /* renamed from: b reason: collision with root package name */
            private String f1544b;

            SkippedWord(Word word, String str) {
                this.f1543a = word;
                this.f1544b = str;
            }

            public String getSlotId() {
                return this.f1544b;
            }

            public Word getWord() {
                return this.f1543a;
            }
        }

        void onComplete(Grammar grammar, List list);

        void onError(ElvisError elvisError);
    }

    public interface ReleaseListener {
        void onReleased(ElvisRecognizer elvisRecognizer);
    }

    public interface ResultListener {
        void onError(ElvisError elvisError);

        void onResult(ElvisResult elvisResult);
    }

    public interface SaveListener {
        void onSaved(ElvisRecognizer elvisRecognizer, boolean z);
    }

    public interface WakeupCheckListener {
        void onComplete(List list);

        void onError(ElvisError elvisError);
    }

    public static ElvisRecognizer createElvisImpersonator() {
        return new b();
    }

    public static ElvisRecognizer createElvisRecognizer(FileManager fileManager) {
        return new c(fileManager);
    }

    public static ElvisRecognizer createElvisRecognizer(FileManager fileManager, Handler handler) {
        return new c(fileManager, handler);
    }

    public abstract void cancelRebuild();

    public abstract void cancelRecognition();

    public abstract void checkWakeupPhrases(List list, WakeupCheckListener wakeupCheckListener);

    public abstract void clearGrammar(RebuildListener rebuildListener);

    public abstract void enableRecognitionLogging(String str, String str2, int i);

    public abstract void enableVerboseAndroidLogging(boolean z);

    public abstract Constraint findConstraint(String str);

    public abstract WordSlot findWordSlot(String str);

    public abstract List getAvailableModels();

    public abstract List getAvailableModels(int i);

    public abstract List getAvailableModels(ElvisLanguage elvisLanguage);

    public abstract int getRecognitionLoggingStatus();

    public abstract void initialize(ElvisConfig elvisConfig, String str);

    public abstract void initialize(ElvisConfig elvisConfig, String str, InitializeListener initializeListener);

    public abstract void loadGrammar(Grammar grammar, RebuildListener rebuildListener);

    public abstract void loadState(RebuildListener rebuildListener);

    public abstract void loadStateAndGrammar(Grammar grammar, RebuildListener rebuildListener);

    public abstract void release();

    public abstract void release(ReleaseListener releaseListener);

    public abstract void saveState();

    public abstract void saveState(SaveListener saveListener);

    public abstract void setActiveConstraintNames(List list);

    public abstract void setActiveConstraints(List list);

    public abstract void setCustomPronunciations(List list);

    public abstract void setWakeupPenalty(int i);

    public abstract void setWakeupPhrases(List list);

    public final void setWakeupPhrases(String... strArr) {
        ArrayList arrayList = new ArrayList(strArr.length);
        for (String add : strArr) {
            arrayList.add(add);
        }
        setWakeupPhrases((List) arrayList);
    }

    public abstract void startRecognition(AudioSource audioSource, SpeechDetectionListener speechDetectionListener, ResultListener resultListener);

    public abstract void startSpeechDetection(AudioSource audioSource, SpeechDetectionListener speechDetectionListener);

    public abstract void startWakeupMode(AudioSource audioSource, ResultListener resultListener);

    public abstract void stopListening();
}
