package com.nuance.dragon.toolkit.recognition.dictation.b;

import com.nuance.dragon.toolkit.recognition.dictation.a.f;
import com.nuance.dragon.toolkit.util.Logger;
import java.io.IOException;

public final class i implements d {

    /* renamed from: a reason: collision with root package name */
    private byte[] f1995a;

    public i(byte[] bArr) {
        this.f1995a = bArr;
    }

    public final f a() {
        try {
            b bVar = new b(this.f1995a);
            bVar.b();
            if (bVar.f()) {
                return bVar.c();
            }
            new StringBuilder("Could not parse NLSML dictation results: ").append(bVar.e()).append(". Trying to parse XML results.");
            e eVar = new e(this.f1995a);
            eVar.b();
            if (eVar.f()) {
                return eVar.c();
            }
            String str = "Could not parse XML neither NLSML dictation results. Error from XML Parser: " + eVar.e() + ". Error from NLSML Parser: " + bVar.e();
            Logger.error(this, str);
            throw new IllegalArgumentException(str);
        } catch (IOException e2) {
            String str2 = "Received IOException while parsing XML/NLSML.";
            Logger.error(this, str2, e2);
            throw new IllegalArgumentException(str2, e2);
        }
    }
}
