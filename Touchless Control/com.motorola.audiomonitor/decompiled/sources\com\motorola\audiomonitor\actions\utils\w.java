package com.motorola.audiomonitor.actions.utils;

import android.text.TextUtils;
import android.util.Log;
import f.h;

class w implements h {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ v f281a;

    w(v vVar) {
        this.f281a = vVar;
    }

    public void a(String str, int i, int i2) {
        if (v.f274b) {
            Log.d(v.f273a, "spotted = " + str);
        }
        if (TextUtils.equals(str, this.f281a.q) && this.f281a.f279g.i == 1) {
            this.f281a.b(ad.RESPONSE_POSITIVE);
        } else if (!TextUtils.equals(str, this.f281a.r) || !(this.f281a.f279g.i == 1 || this.f281a.f279g.i == 2)) {
            this.f281a.b(true);
        } else {
            this.f281a.b(ad.RESPONSE_NEGATIVE);
        }
    }
}
