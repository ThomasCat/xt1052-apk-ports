package com.nuance.dragon.toolkit;

public final class Build {
    public static final String VERSION = "1.1.3";

    public class COMPONENT {
        public static final String ELVIS = "19.48.vs4.v.01";
        public static final String NMSP = "3.9.10";
        public static final String SPEEX = "1.2";
        public static final String SSE = "4.3";
        public static final String VEX = "1.0.1";
        public static final String VFA = "5.6.1";
        public static final String VOCON = "4.4";

        public COMPONENT() {
        }
    }
}
