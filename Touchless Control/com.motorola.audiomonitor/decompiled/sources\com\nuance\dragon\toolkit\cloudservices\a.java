package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.dragon.toolkit.util.internal.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

abstract class a extends CloudServices {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final Queue f1386a = new LinkedList();
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public Transaction f1387b;

    /* renamed from: c reason: collision with root package name */
    private boolean f1388c;

    a() {
    }

    /* access modifiers changed from: private */
    public void a() {
        if (this.f1387b == null && canStartTransaction() && !this.f1386a.isEmpty() && ((Transaction) this.f1386a.peek()).j()) {
            this.f1387b = (Transaction) this.f1386a.remove();
            d createCommandContext = createCommandContext(this.f1387b);
            this.f1387b.f().onTransactionStarted(this.f1387b);
            this.f1387b.a(createCommandContext);
        }
    }

    public void addTransaction(Transaction transaction, int i) {
        int i2 = 0;
        d.a((Object) this, !this.f1388c);
        d.a("transaction", (Object) transaction);
        transaction.a(i);
        LinkedList linkedList = (LinkedList) this.f1386a;
        while (true) {
            int i3 = i2;
            if (i3 >= linkedList.size()) {
                linkedList.add(transaction);
                break;
            }
            if (transaction.d() > ((Transaction) linkedList.get(i3)).d()) {
                linkedList.add(i3, transaction);
                break;
            }
            i2 = i3 + 1;
        }
        transaction.a((a) new a() {
            public final void a(Transaction transaction) {
                if (!a.this.f1386a.isEmpty() && a.this.f1386a.peek() == transaction) {
                    a.this.a();
                }
            }

            public final void b(Transaction transaction) {
                transaction.a((a) null);
                if (a.this.f1387b == transaction) {
                    a.this.f1387b = null;
                } else {
                    a.this.f1386a.remove(transaction);
                }
                a.this.a();
            }
        });
        a();
    }

    /* access modifiers changed from: protected */
    public abstract boolean canStartTransaction();

    /* access modifiers changed from: protected */
    public void connectionStateChanged() {
        a();
    }

    /* access modifiers changed from: protected */
    public abstract d createCommandContext(Transaction transaction);

    public Transaction currentTransaction() {
        return this.f1387b;
    }

    public int getTransactionCount() {
        int size = this.f1386a.size();
        return this.f1387b != null ? size + 1 : size;
    }

    public void release() {
        d.a((Object) this, !this.f1388c);
        this.f1388c = true;
        ArrayList arrayList = new ArrayList(this.f1386a.size());
        arrayList.addAll(this.f1386a);
        this.f1386a.clear();
        if (this.f1387b != null) {
            this.f1387b.cancel();
            this.f1387b = null;
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            ((Transaction) it.next()).cancel();
        }
    }
}
