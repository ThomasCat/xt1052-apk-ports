package com.nuance.dragon.toolkit.speechkit;

import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.audio.pipes.SpeexDecoderPipe;
import com.nuance.dragon.toolkit.audio.sinks.PlayerSink;
import com.nuance.dragon.toolkit.audio.sinks.SpeakerPlayerSink;
import com.nuance.dragon.toolkit.cloudservices.CloudConfig;
import com.nuance.dragon.toolkit.cloudservices.CloudServices;
import com.nuance.dragon.toolkit.cloudservices.TransactionError;
import com.nuance.dragon.toolkit.cloudservices.vocalizer.CloudVocalizer;
import com.nuance.dragon.toolkit.cloudservices.vocalizer.TtsSpec;
import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class SKCloudVocalizer {

    /* renamed from: a reason: collision with root package name */
    private final CloudServices f2053a;

    /* renamed from: b reason: collision with root package name */
    private final CloudConfig f2054b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final Listener f2055c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public final LinkedList f2056d = new LinkedList();
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public CloudVocalizer f2057e;

    /* renamed from: f reason: collision with root package name */
    private SpeexDecoderPipe f2058f;

    /* renamed from: g reason: collision with root package name */
    private PlayerSink f2059g;

    public final class Language {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final String f2066a;

        public Language(String str) {
            this.f2066a = str;
        }
    }

    public interface Listener {
        void onSpeakingBegin(SKCloudVocalizer sKCloudVocalizer, Object obj);

        void onSpeakingDone(SKCloudVocalizer sKCloudVocalizer, boolean z, TransactionError transactionError, Object obj);
    }

    public final class Voice {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final String f2067a;

        public Voice(String str) {
            this.f2067a = str;
        }
    }

    final class a {

        /* renamed from: a reason: collision with root package name */
        final TtsSpec f2068a;

        /* renamed from: b reason: collision with root package name */
        final Object f2069b;

        public a(TtsSpec ttsSpec, Object obj) {
            this.f2068a = ttsSpec;
            this.f2069b = obj;
        }
    }

    SKCloudVocalizer(CloudServices cloudServices, CloudConfig cloudConfig, Listener listener) {
        this.f2053a = cloudServices;
        this.f2054b = cloudConfig;
        this.f2055c = listener;
    }

    /* access modifiers changed from: private */
    public void a() {
        if (this.f2057e != null) {
            this.f2057e.cancel();
        }
        if (this.f2059g != null) {
            this.f2059g.disconnectAudioSource();
            this.f2059g.stopPlaying();
            this.f2059g = null;
        }
        if (this.f2058f != null) {
            this.f2058f.disconnectAudioSource();
            this.f2058f.release();
            this.f2058f = null;
        }
    }

    private void a(String str, boolean z, String str2, String str3, Object obj) {
        d.a("text", str);
        Dictionary dictionary = new Dictionary();
        dictionary.put("tts_input", str);
        dictionary.put("tts_type", z ? "ssml" : "text");
        Dictionary dictionary2 = new Dictionary();
        if (str3 != null) {
            dictionary2.put("tts_voice", str3);
        } else {
            dictionary2.put("tts_language", str2);
        }
        speakString(new TtsSpec("NVC_TTS_CMD", dictionary2, "TEXT_TO_READ", dictionary, AudioType.SPEEX_WB), obj);
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!this.f2056d.isEmpty()) {
            this.f2057e = new CloudVocalizer(this.f2053a);
            final CloudVocalizer cloudVocalizer = this.f2057e;
            final a aVar = (a) this.f2056d.remove();
            AudioSource generateTts = this.f2057e.generateTts(aVar.f2068a, new com.nuance.dragon.toolkit.cloudservices.vocalizer.CloudVocalizer.Listener() {
                public final void onError(TransactionError transactionError) {
                    if (SKCloudVocalizer.this.f2057e == cloudVocalizer) {
                        SKCloudVocalizer.this.f2057e = null;
                        int size = SKCloudVocalizer.this.f2056d.size();
                        ArrayList arrayList = new ArrayList(size);
                        SKCloudVocalizer.this.f2056d.clear();
                        SKCloudVocalizer.this.a();
                        if (SKCloudVocalizer.this.f2055c != null) {
                            int i = size - 1;
                            SKCloudVocalizer.this.f2055c.onSpeakingDone(SKCloudVocalizer.this, size > 0, transactionError, aVar.f2069b);
                            Iterator it = arrayList.iterator();
                            while (it.hasNext()) {
                                int i2 = i - 1;
                                SKCloudVocalizer.this.f2055c.onSpeakingDone(SKCloudVocalizer.this, i > 0, transactionError, ((a) it.next()).f2069b);
                                i = i2;
                            }
                        }
                    }
                }

                public final void onSuccess() {
                }
            });
            if (aVar.f2068a.getAudioType().encoding == Encoding.SPEEX) {
                this.f2058f = new SpeexDecoderPipe();
                this.f2058f.connectAudioSource(generateTts);
                this.f2059g = new SpeakerPlayerSink(this.f2058f.getAudioType());
                this.f2059g.connectAudioSource(this.f2058f);
            } else {
                this.f2059g = new SpeakerPlayerSink(generateTts.getAudioType());
                this.f2059g.connectAudioSource(generateTts);
            }
            this.f2059g.startPlaying(new com.nuance.dragon.toolkit.audio.sinks.PlayerSink.Listener() {
                public final void onStarted(PlayerSink playerSink) {
                    if (cloudVocalizer == SKCloudVocalizer.this.f2057e && SKCloudVocalizer.this.f2055c != null) {
                        SKCloudVocalizer.this.f2055c.onSpeakingBegin(SKCloudVocalizer.this, aVar.f2069b);
                    }
                }

                public final void onStopped(PlayerSink playerSink) {
                    if (cloudVocalizer == SKCloudVocalizer.this.f2057e) {
                        SKCloudVocalizer.this.f2057e = null;
                        SKCloudVocalizer.this.a();
                        if (SKCloudVocalizer.this.f2055c != null) {
                            SKCloudVocalizer.this.f2055c.onSpeakingDone(SKCloudVocalizer.this, !SKCloudVocalizer.this.f2056d.isEmpty(), null, aVar.f2069b);
                        }
                        SKCloudVocalizer.this.b();
                    }
                }
            });
        }
    }

    public void cancel() {
        a();
    }

    public void speakString(TtsSpec ttsSpec, Object obj) {
        d.a("ttsSpec", (Object) ttsSpec);
        d.a("ttsSpec", "have the same audio output type of the SpeechKit object", ttsSpec.getAudioType().equals(this.f2054b.playerCodec));
        this.f2056d.add(new a(ttsSpec, obj));
        if (this.f2057e == null) {
            b();
        }
    }

    public void speakString(String str, boolean z, Language language, Object obj) {
        d.a("language", language.f2066a);
        a(str, z, language.f2066a, null, obj);
    }

    public void speakString(String str, boolean z, Voice voice, Object obj) {
        d.a("voice", voice.f2067a);
        a(str, z, null, voice.f2067a, obj);
    }
}
