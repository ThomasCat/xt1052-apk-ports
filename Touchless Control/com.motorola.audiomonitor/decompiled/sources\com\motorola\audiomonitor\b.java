package com.motorola.audiomonitor;

import android.content.Context;
import android.media.AudioManager;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;

public class b {

    /* renamed from: a reason: collision with root package name */
    private static final String f333a = ("AMonitor." + b.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f334b = a.f160b;

    /* renamed from: e reason: collision with root package name */
    private static b f335e;

    /* renamed from: c reason: collision with root package name */
    private AudioManager f336c;

    /* renamed from: d reason: collision with root package name */
    private boolean f337d = false;

    public static b a() {
        if (f335e == null) {
            f335e = new b();
        }
        return f335e;
    }

    public static boolean c() {
        try {
            Class cls = Class.forName("android.media.AudioSystem");
            return ((Boolean) cls.getDeclaredMethod("isStreamActive", new Class[]{Integer.TYPE, Integer.TYPE}).invoke(cls, new Object[]{Integer.valueOf(4), Integer.valueOf(0)})).booleanValue();
        } catch (IllegalArgumentException e2) {
            Log.e(f333a, "Exception isAlarmActive: " + e2);
            return false;
        } catch (ClassNotFoundException e3) {
            Log.e(f333a, "Exception isAlarmActive: " + e3);
            return false;
        } catch (NoSuchMethodException e4) {
            Log.e(f333a, "Exception isAlarmActive: " + e4);
            return false;
        } catch (IllegalAccessException e5) {
            Log.e(f333a, "Exception isAlarmActive: " + e5);
            return false;
        } catch (InvocationTargetException e6) {
            Log.e(f333a, "Exception isAlarmActive: " + e6);
            return false;
        }
    }

    public void a(boolean z, Context context) {
        this.f336c = (AudioManager) context.getSystemService("audio");
        if (z) {
            this.f337d = this.f336c.isMusicActive();
            int requestAudioFocus = this.f336c.requestAudioFocus(null, 3, 2);
            if (f334b) {
                Log.d(f333a, "requestAudioFocus result is " + requestAudioFocus);
                return;
            }
            return;
        }
        this.f336c.abandonAudioFocus(null);
    }

    public boolean b() {
        return this.f337d;
    }
}
