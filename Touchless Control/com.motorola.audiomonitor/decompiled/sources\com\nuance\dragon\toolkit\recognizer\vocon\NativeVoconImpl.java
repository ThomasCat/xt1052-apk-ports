package com.nuance.dragon.toolkit.recognizer.vocon;

import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.recognizer.NativeModelInfo;
import com.nuance.dragon.toolkit.recognizer.NbestList;
import com.nuance.dragon.toolkit.recognizer.RecogResult;
import com.nuance.dragon.toolkit.util.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class NativeVoconImpl implements a {

    /* renamed from: a reason: collision with root package name */
    FileManager f2003a;

    /* renamed from: b reason: collision with root package name */
    private long f2004b;

    static {
        System.loadLibrary("dmt_vocon");
    }

    public NativeVoconImpl(FileManager fileManager) {
        this.f2003a = fileManager;
    }

    private native int nativeGetNbestListEntry(long j, int i, List list);

    private native int nativeVoconAddContext(long j, FileManager fileManager, String str);

    private native long nativeVoconCreate(FileManager fileManager, String str, String str2);

    private native int nativeVoconDeleteContext(long j);

    private native void nativeVoconDestroy(long j);

    private native long nativeVoconGetChoiceCount(long j);

    private native int nativeVoconStartRecognizing(long j, AudioSink audioSink, int i);

    private native void nativeVoconStopRecognizing(long j, int i);

    public final String a(String str) {
        String[] findFiles = this.f2003a.findFiles(Pattern.compile(b.a(str)));
        if (findFiles == null || findFiles.length <= 0) {
            return null;
        }
        return findFiles[0];
    }

    public final String a(String str, int i) {
        String[] findFiles = this.f2003a.findFiles(Pattern.compile(b.a(str, i)));
        if (findFiles == null || findFiles.length <= 0) {
            return null;
        }
        return findFiles[0];
    }

    public final void a() {
        if (this.f2004b == 0) {
            Logger.error(this, "Error destroy Vocon: native Vocon does not exist!");
            return;
        }
        nativeVoconDestroy(this.f2004b);
        this.f2004b = 0;
    }

    public final void a(boolean z) {
        if (this.f2004b != 0) {
            nativeVoconStopRecognizing(this.f2004b, z ? 1 : 0);
        }
    }

    public final boolean a(Object obj, int i) {
        if (this.f2004b == 0) {
            Logger.error(this, "Error starting recognizer: native Vocon does not exist.");
            return false;
        } else if (obj instanceof AudioSink) {
            return nativeVoconStartRecognizing(this.f2004b, (AudioSink) obj, i) == 0;
        } else {
            Logger.error(this, "Error starting recognizer: invalid audio sink");
            return false;
        }
    }

    public final boolean a(List list) {
        Pattern compile = Pattern.compile("acmod(\\d+)_(\\d+)_([a-z]{3})_([a-z]+)_([a-z]+)_f(\\d+)(.*)\\.dat");
        String[] findFiles = this.f2003a.findFiles(compile);
        if (findFiles != null) {
            for (String matcher : findFiles) {
                Matcher matcher2 = compile.matcher(matcher);
                if (matcher2.matches()) {
                    String a2 = b.a(matcher2);
                    String b2 = b.b(matcher2);
                    if (!(a2 == null || b2 == null)) {
                        list.add(new NativeModelInfo(a2, Integer.parseInt(b2)));
                    }
                }
            }
        }
        return true;
    }

    public final boolean b() {
        if (this.f2004b != 0) {
            return nativeVoconDeleteContext(this.f2004b) == 0;
        }
        Logger.error(this, "deleteContext: native Vocon does not exist!");
        return false;
    }

    public final boolean b(String str) {
        if (this.f2004b != 0) {
            Logger.error(this, "Error create vocon: native Vocon already exists.");
            return false;
        }
        if (str != null) {
            this.f2004b = nativeVoconCreate(this.f2003a, str, null);
        }
        if (this.f2004b != 0) {
            return true;
        }
        Logger.error(this, "Unable to create native Vocon");
        return false;
    }

    public final RecogResult c() {
        NbestList nbestList;
        if (this.f2004b == 0) {
            Logger.error(this, "getResult: native Vocon does not exist.");
            return null;
        }
        long nativeVoconGetChoiceCount = nativeVoconGetChoiceCount(this.f2004b);
        if (nativeVoconGetChoiceCount < 0) {
            Logger.error(this, "getResult: error retrieving choices.");
            nbestList = null;
        } else if (nativeVoconGetChoiceCount == 0 || nativeVoconGetChoiceCount <= 0) {
            nbestList = null;
        } else {
            new StringBuilder("getResult: ").append(nativeVoconGetChoiceCount).append(" choices.");
            ArrayList arrayList = new ArrayList((int) nativeVoconGetChoiceCount);
            int i = 0;
            while (true) {
                if (((long) i) >= nativeVoconGetChoiceCount) {
                    break;
                } else if (nativeGetNbestListEntry(this.f2004b, i, arrayList) != 0) {
                    Logger.error(this, "vocon result error.");
                    break;
                } else {
                    i++;
                }
            }
            nbestList = new NbestList((List) arrayList);
        }
        if (nbestList != null && !nbestList.isEmpty()) {
            return new RecogResult(nbestList);
        }
        Logger.warn(this, "No nbest results.");
        return null;
    }

    public final boolean c(String str) {
        if (this.f2004b != 0) {
            return nativeVoconAddContext(this.f2004b, this.f2003a, str) == 0;
        }
        Logger.error(this, "addContext (" + str + "): native Vocon does not exist!");
        return false;
    }
}
