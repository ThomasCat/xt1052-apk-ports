package com.motorola.audiomonitor.actions.utils;

import android.content.Context;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings.Global;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.b;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public class h {

    /* renamed from: a reason: collision with root package name */
    private static final String f233a = ("AMonitor." + h.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f234b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private static h f235c;

    /* renamed from: d reason: collision with root package name */
    private final TelephonyManager f236d;

    /* renamed from: e reason: collision with root package name */
    private final AudioManager f237e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public String f238f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public int f239g = -1;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public ServiceState f240h = null;
    /* access modifiers changed from: private */
    public Set i = new CopyOnWriteArraySet();
    private final PhoneStateListener j = new i(this);

    private h(Context context) {
        this.f236d = (TelephonyManager) context.getSystemService("phone");
        this.f236d.listen(this.j, 33);
        this.f237e = (AudioManager) context.getSystemService("audio");
    }

    public static h a() {
        if (f235c != null) {
            return f235c;
        }
        throw new AssertionError("Not initialized");
    }

    public static void a(Context context) {
        f235c = new h(context);
    }

    public static boolean b(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean c(Context context) {
        return Global.getInt(context.getContentResolver(), "airplane_mode_on", 0) != 0;
    }

    private boolean j() {
        if (this.f237e.isBluetoothA2dpOn()) {
            if (!f234b) {
                return false;
            }
            Log.d(f233a, "using bluetooth");
            return false;
        } else if (!this.f237e.isWiredHeadsetOn()) {
            return true;
        } else {
            if (!f234b) {
                return false;
            }
            Log.d(f233a, "using wired hs");
            return false;
        }
    }

    public void a(j jVar) {
        if (this.i != null) {
            this.i.add(jVar);
        }
    }

    public void b(j jVar) {
        if (this.i != null) {
            this.i.remove(jVar);
        }
    }

    public boolean b() {
        return b.c();
    }

    public boolean c() {
        return b.a().b();
    }

    public boolean d() {
        return this.f236d.getCallState() == 2;
    }

    public boolean e() {
        return this.f236d.getCallState() == 1;
    }

    public boolean f() {
        return this.f240h == null || this.f240h.getState() == 0;
    }

    public String g() {
        if (e()) {
            return this.f238f;
        }
        return null;
    }

    public boolean h() {
        if (this.f237e.getMode() != 0 || !this.f237e.isMusicActive() || !j() || this.f237e.getStreamVolume(3) <= 0) {
            return false;
        }
        if (f234b) {
            Log.d(f233a, "music audio active");
        }
        return true;
    }

    public void i() {
        this.f236d.listen(this.j, 0);
        f235c = null;
        if (this.i != null) {
            this.i.clear();
            this.i = null;
        }
    }

    public String toString() {
        return "isAlarming[" + b() + "] isMusicActive[" + c() + "] isRinging[" + e() + "(" + g() + ")] isCalling[" + d() + "] isCallPossible[" + f() + "]";
    }
}
