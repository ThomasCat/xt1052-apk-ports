package com.nuance.dragon.toolkit.recognition.a;

import com.nuance.dragon.toolkit.recognition.dictation.DictationResult;
import com.nuance.dragon.toolkit.recognition.dictation.Token;
import java.util.List;

public interface a {

    /* renamed from: com.nuance.dragon.toolkit.recognition.a.a$a reason: collision with other inner class name */
    public final class C0010a {

        /* renamed from: a reason: collision with root package name */
        public final String f1900a;

        /* renamed from: b reason: collision with root package name */
        public final C0011a f1901b;

        /* renamed from: c reason: collision with root package name */
        public final Token f1902c;

        /* renamed from: d reason: collision with root package name */
        public final DictationResult f1903d;

        /* renamed from: e reason: collision with root package name */
        public final double f1904e;

        /* renamed from: com.nuance.dragon.toolkit.recognition.a.a$a$a reason: collision with other inner class name */
        public enum C0011a {
            TOKEN,
            DICTATION_RESULT
        }

        public C0010a(C0011a aVar, String str, DictationResult dictationResult) {
            this(aVar, str, null, dictationResult, dictationResult.sentenceAt(0).getConfidenceScore());
        }

        public C0010a(C0011a aVar, String str, Token token) {
            this(aVar, str, token, null, token.getConfidenceScore());
        }

        private C0010a(C0011a aVar, String str, Token token, DictationResult dictationResult, double d2) {
            this.f1901b = aVar;
            this.f1900a = str;
            this.f1902c = token;
            this.f1903d = dictationResult;
            this.f1904e = d2;
        }
    }

    public final class b {

        /* renamed from: a reason: collision with root package name */
        public final String f1908a;

        /* renamed from: b reason: collision with root package name */
        public final String f1909b;

        /* renamed from: c reason: collision with root package name */
        public final DictationResult f1910c;

        /* renamed from: d reason: collision with root package name */
        public final double f1911d;

        public b(String str, DictationResult dictationResult) {
            this(str, dictationResult.toString(), dictationResult, dictationResult.sentenceAt(0).getConfidenceScore());
        }

        public b(String str, String str2, double d2) {
            this(str, str2, null, d2);
        }

        private b(String str, String str2, DictationResult dictationResult, double d2) {
            this.f1908a = str;
            this.f1909b = str2;
            this.f1910c = dictationResult;
            this.f1911d = d2;
        }
    }

    List a();

    String a_();
}
