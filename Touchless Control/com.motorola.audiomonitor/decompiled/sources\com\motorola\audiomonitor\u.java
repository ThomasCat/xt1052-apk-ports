package com.motorola.audiomonitor;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

public abstract class u extends Binder implements t {
    public u() {
        attachInterface(this, "com.motorola.audiomonitor.IMonitorService");
    }

    public static t a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.motorola.audiomonitor.IMonitorService");
        return (queryLocalInterface == null || !(queryLocalInterface instanceof t)) ? new v(iBinder) : (t) queryLocalInterface;
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        int i3 = 0;
        switch (i) {
            case 1:
                parcel.enforceInterface("com.motorola.audiomonitor.IMonitorService");
                a(x.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 2:
                parcel.enforceInterface("com.motorola.audiomonitor.IMonitorService");
                b(x.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 3:
                parcel.enforceInterface("com.motorola.audiomonitor.IMonitorService");
                a();
                parcel2.writeNoException();
                return true;
            case 4:
                parcel.enforceInterface("com.motorola.audiomonitor.IMonitorService");
                b();
                parcel2.writeNoException();
                return true;
            case 5:
                parcel.enforceInterface("com.motorola.audiomonitor.IMonitorService");
                boolean c2 = c();
                parcel2.writeNoException();
                if (c2) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case 6:
                parcel.enforceInterface("com.motorola.audiomonitor.IMonitorService");
                boolean a2 = a(parcel.readString());
                parcel2.writeNoException();
                if (a2) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case 7:
                parcel.enforceInterface("com.motorola.audiomonitor.IMonitorService");
                boolean a3 = a(parcel.readInt() != 0);
                parcel2.writeNoException();
                if (a3) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case 8:
                parcel.enforceInterface("com.motorola.audiomonitor.IMonitorService");
                boolean d2 = d();
                parcel2.writeNoException();
                if (d2) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case 9:
                parcel.enforceInterface("com.motorola.audiomonitor.IMonitorService");
                boolean e2 = e();
                parcel2.writeNoException();
                if (e2) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case 10:
                parcel.enforceInterface("com.motorola.audiomonitor.IMonitorService");
                boolean f2 = f();
                parcel2.writeNoException();
                if (f2) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case 11:
                parcel.enforceInterface("com.motorola.audiomonitor.IMonitorService");
                boolean b2 = b(parcel.readInt() != 0);
                parcel2.writeNoException();
                if (b2) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case 12:
                parcel.enforceInterface("com.motorola.audiomonitor.IMonitorService");
                boolean g2 = g();
                parcel2.writeNoException();
                if (g2) {
                    i3 = 1;
                }
                parcel2.writeInt(i3);
                return true;
            case 1598968902:
                parcel2.writeString("com.motorola.audiomonitor.IMonitorService");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
