package com.motorola.audiomonitor.actions.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import h.k;

public class ContextEngineReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (!TextUtils.isEmpty(action) && action.equals("com.motorola.context.engine.INIT_COMPLETE") && k.b(context, "monitor_status", true)) {
            g.a(context, true);
        }
    }
}
