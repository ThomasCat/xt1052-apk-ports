package f;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import com.motorola.audiomonitor.ProtectedByteBuffer;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.k;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.SpeechDetectionListener;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.InitializeListener;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.RebuildListener;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.RebuildListener.SkippedWord;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.ReleaseListener;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.ResultListener;
import com.nuance.dragon.toolkit.elvis.Grammar;
import com.nuance.dragon.toolkit.file.FileManager;
import java.util.ArrayList;
import java.util.List;

public class i extends HandlerThread implements k {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f2236a = ("AMonitor." + i.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f2237b = a.f160b;
    private static final List o = new ArrayList();
    private static int q = 0;
    /* access modifiers changed from: private */
    public static FileManager r = null;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public ElvisRecognizer f2238c;

    /* renamed from: d reason: collision with root package name */
    private a f2239d;

    /* renamed from: e reason: collision with root package name */
    private final Handler f2240e;

    /* renamed from: f reason: collision with root package name */
    private final HandlerThread f2241f;

    /* renamed from: g reason: collision with root package name */
    private final Handler f2242g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public final InitializeListener f2243h;
    private final ResultListener i;
    /* access modifiers changed from: private */
    public final RebuildListener j;
    /* access modifiers changed from: private */
    public final ReleaseListener k;
    private final SpeechDetectionListener l;
    /* access modifiers changed from: private */
    public final t m;
    /* access modifiers changed from: private */
    public boolean n = false;
    /* access modifiers changed from: private */
    public final String p;

    public i(int i2, t tVar, Grammar grammar) {
        super(i.class.getSimpleName());
        if (r == null) {
            throw new RuntimeException("FileManager not initialized");
        }
        synchronized (o) {
            if (o.isEmpty()) {
                StringBuilder append = new StringBuilder().append("default-");
                int i3 = q + 1;
                q = i3;
                this.p = append.append(Integer.toString(i3)).toString();
            } else {
                this.p = (String) o.remove(0);
            }
        }
        this.m = tVar;
        start();
        this.f2240e = new Handler(getLooper());
        this.f2241f = new HandlerThread("ElvisAudioWorker");
        this.f2241f.start();
        this.f2242g = new Handler(this.f2241f.getLooper());
        this.f2243h = new j(this, grammar);
        this.j = new l(this);
        this.i = new m(this);
        this.k = new n(this);
        this.l = new o(this);
        this.f2240e.post(new p(this, i2));
    }

    /* access modifiers changed from: private */
    public int a(int i2) {
        return i2 == 16000 ? 16000 : 8000;
    }

    public static void a(Context context) {
        if (r == null) {
            r = new FileManager(context, ".jpg", "elvis", 0, false, 0, false, null, "elvis", "/system/vendor/moto/audiomonitor/");
        }
    }

    private AudioType b(int i2) {
        return i2 == 16000 ? AudioType.PCM_16k : AudioType.PCM_8k;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.nuance.dragon.toolkit.elvis.ElvisRecognizer$RebuildListener$SkippedWord>, for r6v0, types: [java.util.List, java.util.List<com.nuance.dragon.toolkit.elvis.ElvisRecognizer$RebuildListener$SkippedWord>] */
    public String b(List<SkippedWord> list) {
        StringBuilder sb = new StringBuilder();
        for (SkippedWord skippedWord : list) {
            sb.append("Slot: ").append(skippedWord.getSlotId());
            sb.append("(").append(skippedWord.getWord().getSurfaceForm()).append(",");
            sb.append(skippedWord.getWord().getSpokenForm()).append(")\n");
        }
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public synchronized void h() {
        if (!this.n) {
            Log.e(f2236a, "Recognizer not ready");
        } else {
            if (this.f2239d != null) {
                this.f2239d.stopRecording();
            }
            this.f2239d = new a(b(16000), this.f2242g);
            this.f2239d.startRecording();
            if (f2237b) {
                Log.d(f2236a, "StartRecognition");
            }
            this.f2238c.startRecognition(this.f2239d, this.l, this.i);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void i() {
        if (this.f2239d != null) {
            this.f2239d.a();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void j() {
        if (this.f2239d != null) {
            this.f2239d.stopRecording();
            this.f2239d = null;
        }
    }

    public void a(List list) {
        this.f2240e.post(new q(this, list));
    }

    public boolean a() {
        return this.n;
    }

    public synchronized boolean a(ProtectedByteBuffer protectedByteBuffer) {
        if (this.f2239d != null) {
            this.f2239d.a(protectedByteBuffer);
        }
        return true;
    }

    public void b() {
        this.f2240e.post(new r(this));
    }

    public void c() {
        this.f2240e.post(new s(this));
    }

    public void d() {
        j();
        this.f2240e.post(new k(this));
        while (true) {
            try {
                join(3000);
                break;
            } catch (InterruptedException e2) {
            }
        }
        this.f2241f.quit();
        while (true) {
            try {
                this.f2241f.join(3000);
                break;
            } catch (InterruptedException e3) {
            }
        }
        synchronized (o) {
            o.add(this.p);
        }
        if (f2237b) {
            Log.d(f2236a, "Cleaned up " + this.p);
        }
    }
}
