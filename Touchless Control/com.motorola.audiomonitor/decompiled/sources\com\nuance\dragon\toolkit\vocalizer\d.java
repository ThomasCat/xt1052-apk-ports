package com.nuance.dragon.toolkit.vocalizer;

class d {
    public static final VocalizerVoice AGATA = a.POLISH.voices[0];
    public static final VocalizerVoice ALEXANDROS = a.GREEK.voices[1];
    public static final VocalizerVoice ALICE = a.ITALIAN.voices[1];
    public static final VocalizerVoice ALLISON = a.UNITED_STATES_ENGLISH.voices[4];
    public static final VocalizerVoice[] ALL_VOICES = {OTOYA, KYOKO, SILVIA, ALICE, FEDERICA, PAOLA, PAOLO, LUCA, YA_LING, MEI_JIA, RAQUEL, LUCIANA, PAULINA, JAVIER, CARLOS, DIEGO, JUAN, MEI_LING, TING_TING, TIAN_TIAN, MONICA, JORGE, JOANA, JULIE, AMELIE, FELIX, NICOLAS, KAREN, LEE, NARAE, YUNA, VIRGINIE, AUDREY, SEBASTIEN, THOMAS, CLAIRE, XANDER, EMILY, SERENA, DANIEL, MELINA, ALEXANDROS, MIKKO, SATU, SAMANTHA, JILL, CAROL, AVA, ALLISON, TOM, ALVA, OSKAR, IDA, SARA, NORA, STINE, AGATA, ZOSIA, STEFFI, ANNA, PETRA, YANNICK, MILENA, KATERINA, YURI, SIN_JI, AYLIN};
    public static final VocalizerVoice ALVA = a.SWEDISH.voices[0];
    public static final VocalizerVoice AMELIE = a.CANADIAN_FRENCH.voices[1];
    public static final VocalizerVoice ANNA = a.GERMAN.voices[1];
    public static final VocalizerVoice AUDREY = a.EUROPEAN_FRENCH.voices[1];
    public static final VocalizerVoice AVA = a.UNITED_STATES_ENGLISH.voices[3];
    public static final VocalizerVoice AYLIN = a.TURKISH.voices[0];
    public static final VocalizerVoice CARLOS = a.LATIN_AMERICAN_SPANISH.voices[2];
    public static final VocalizerVoice CAROL = a.UNITED_STATES_ENGLISH.voices[2];
    public static final VocalizerVoice CLAIRE = a.DUTCH.voices[0];
    public static final VocalizerVoice DANIEL = a.BRITISH_ENGLISH.voices[2];
    public static final VocalizerVoice DIEGO = a.LATIN_AMERICAN_SPANISH.voices[3];
    public static final VocalizerVoice EMILY = a.BRITISH_ENGLISH.voices[0];
    public static final VocalizerVoice FEDERICA = a.ITALIAN.voices[2];
    public static final VocalizerVoice FELIX = a.CANADIAN_FRENCH.voices[2];
    public static final VocalizerVoice IDA = a.DANISH.voices[0];
    public static final VocalizerVoice JAVIER = a.LATIN_AMERICAN_SPANISH.voices[1];
    public static final VocalizerVoice JILL = a.UNITED_STATES_ENGLISH.voices[1];
    public static final VocalizerVoice JOANA = a.EUROPEAN_PORTUGUESE.voices[0];
    public static final VocalizerVoice JORGE = a.EUROPEAN_SPANISH.voices[1];
    public static final VocalizerVoice JUAN = a.LATIN_AMERICAN_SPANISH.voices[4];
    public static final VocalizerVoice JULIE = a.CANADIAN_FRENCH.voices[0];
    public static final VocalizerVoice KAREN = a.AUSTRALIAN_ENGLISH.voices[0];
    public static final VocalizerVoice KATERINA = a.RUSSIAN.voices[1];
    public static final VocalizerVoice KYOKO = a.JAPANESE.voices[1];
    public static final VocalizerVoice LEE = a.AUSTRALIAN_ENGLISH.voices[1];
    public static final VocalizerVoice LUCA = a.ITALIAN.voices[5];
    public static final VocalizerVoice LUCIANA = a.BRAZILIAN_PORTUGUESE.voices[1];
    public static final VocalizerVoice MEI_JIA = a.MANDARIN_TRADITIONAL.voices[1];
    public static final VocalizerVoice MEI_LING = a.MANDARIN_SIMPLIFIED.voices[0];
    public static final VocalizerVoice MELINA = a.GREEK.voices[0];
    public static final VocalizerVoice MIKKO = a.FINNISH.voices[0];
    public static final VocalizerVoice MILENA = a.RUSSIAN.voices[0];
    public static final VocalizerVoice MONICA = a.EUROPEAN_SPANISH.voices[0];
    public static final VocalizerVoice NARAE = a.KOREAN.voices[0];
    public static final VocalizerVoice NICOLAS = a.CANADIAN_FRENCH.voices[3];
    public static final VocalizerVoice NORA = a.NORWEGIAN.voices[0];
    public static final VocalizerVoice OSKAR = a.SWEDISH.voices[1];
    public static final VocalizerVoice OTOYA = a.JAPANESE.voices[0];
    public static final VocalizerVoice PAOLA = a.ITALIAN.voices[3];
    public static final VocalizerVoice PAOLO = a.ITALIAN.voices[4];
    public static final VocalizerVoice PAULINA = a.LATIN_AMERICAN_SPANISH.voices[0];
    public static final VocalizerVoice PETRA = a.GERMAN.voices[2];
    public static final VocalizerVoice RAQUEL = a.BRAZILIAN_PORTUGUESE.voices[0];
    public static final VocalizerVoice SAMANTHA = a.UNITED_STATES_ENGLISH.voices[0];
    public static final VocalizerVoice SARA = a.DANISH.voices[1];
    public static final VocalizerVoice SATU = a.FINNISH.voices[1];
    public static final VocalizerVoice SEBASTIEN = a.EUROPEAN_FRENCH.voices[2];
    public static final VocalizerVoice SERENA = a.BRITISH_ENGLISH.voices[1];
    public static final VocalizerVoice SILVIA = a.ITALIAN.voices[0];
    public static final VocalizerVoice SIN_JI = a.CANTONESE_TRADITIONAL.voices[0];
    public static final VocalizerVoice STEFFI = a.GERMAN.voices[0];
    public static final VocalizerVoice STINE = a.NORWEGIAN.voices[1];
    public static final VocalizerVoice THOMAS = a.EUROPEAN_FRENCH.voices[3];
    public static final VocalizerVoice TIAN_TIAN = a.MANDARIN_SIMPLIFIED.voices[2];
    public static final VocalizerVoice TING_TING = a.MANDARIN_SIMPLIFIED.voices[1];
    public static final VocalizerVoice TOM = a.UNITED_STATES_ENGLISH.voices[5];
    public static final VocalizerVoice VIRGINIE = a.EUROPEAN_FRENCH.voices[0];
    public static final VocalizerVoice XANDER = a.DUTCH.voices[1];
    public static final VocalizerVoice YANNICK = a.GERMAN.voices[3];
    public static final VocalizerVoice YA_LING = a.MANDARIN_TRADITIONAL.voices[0];
    public static final VocalizerVoice YUNA = a.KOREAN.voices[1];
    public static final VocalizerVoice YURI = a.RUSSIAN.voices[2];
    public static final VocalizerVoice ZOSIA = a.POLISH.voices[1];

    d() {
    }
}
