package com.nuance.dragon.toolkit.elvis;

import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.grammar.Word;
import com.nuance.dragon.toolkit.util.JSONUtils;
import com.nuance.dragon.toolkit.util.Logger;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class NativeElvisImpl implements e {

    /* renamed from: a reason: collision with root package name */
    private long f1550a;

    /* renamed from: b reason: collision with root package name */
    private final FileManager f1551b;

    /* renamed from: c reason: collision with root package name */
    private String f1552c;

    /* renamed from: d reason: collision with root package name */
    private int f1553d;

    /* renamed from: e reason: collision with root package name */
    private String f1554e;

    /* renamed from: f reason: collision with root package name */
    private boolean f1555f;

    static {
        System.loadLibrary("dmt_elvis");
    }

    NativeElvisImpl(FileManager fileManager) {
        this.f1551b = fileManager;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x005d A[SYNTHETIC, Splitter:B:13:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0062 A[SYNTHETIC, Splitter:B:16:0x0062] */
    /* JADX WARNING: Removed duplicated region for block: B:43:? A[RETURN, SYNTHETIC] */
    private boolean a(String str, String str2) {
        FileOutputStream fileOutputStream = null;
        FileInputStream openFileForReading = this.f1551b.openFileForReading(str);
        if (openFileForReading == null) {
            Logger.error(this, "Unable to read file:" + str);
            return false;
        }
        File file = new File(str2);
        try {
            if (file.exists() && !file.delete()) {
                Logger.error(this, "Can't delete file: " + str2);
                openFileForReading.close();
                return false;
            } else if (!file.createNewFile()) {
                Logger.error(this, "Could not create log output file:" + str2);
                openFileForReading.close();
                return false;
            } else {
                FileOutputStream fileOutputStream2 = new FileOutputStream(file);
                try {
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = openFileForReading.read(bArr);
                        if (read >= 0) {
                            fileOutputStream2.write(bArr, 0, read);
                        } else {
                            openFileForReading.close();
                            try {
                                fileOutputStream2.close();
                                return true;
                            } catch (IOException e2) {
                                e = e2;
                                openFileForReading = null;
                                fileOutputStream = fileOutputStream2;
                                Logger.error(this, "Error copying file:" + e.toString());
                                if (openFileForReading != null) {
                                }
                                if (fileOutputStream == null) {
                                }
                            }
                        }
                    }
                } catch (IOException e3) {
                    e = e3;
                    fileOutputStream = fileOutputStream2;
                    Logger.error(this, "Error copying file:" + e.toString());
                    if (openFileForReading != null) {
                    }
                    if (fileOutputStream == null) {
                    }
                }
            }
        } catch (IOException e4) {
            e = e4;
            Logger.error(this, "Error copying file:" + e.toString());
            if (openFileForReading != null) {
                try {
                    openFileForReading.close();
                } catch (IOException e5) {
                    Logger.error(this, "Error closing file:" + e5.toString());
                }
            }
            if (fileOutputStream == null) {
                return false;
            }
            try {
                fileOutputStream.close();
                return false;
            } catch (IOException e6) {
                Logger.error(this, "Error closing file:" + e6.toString());
                return false;
            }
        }
    }

    public static native int elvisGetParamsFromBinFile(FileManager fileManager, String str, BinFileParametersJni binFileParametersJni);

    public final int a(WordSlot wordSlot) {
        if (wordSlot == null) {
            Logger.error(this, "addWordClass (): invalid WordSlot!");
            return 2;
        }
        String id = wordSlot.getId();
        if (this.f1550a == 0) {
            Logger.error(this, "addWordClass (" + id + "): native Elvis does not exist!");
            return 2;
        }
        int elvisAddWordClass = elvisAddWordClass(this.f1550a, id, wordSlot.getType(), wordSlot.getRebuildType() == 0);
        if (elvisAddWordClass == 2) {
            Logger.error(this, "Error adding word class to native Elvis: " + id + ".");
        }
        this.f1555f = true;
        return elvisAddWordClass;
    }

    public final int a(Word word) {
        if (word == null) {
            Logger.error(this, "addExternalPronunciation (): invalid word!");
            return 2;
        } else if (this.f1550a == 0) {
            Logger.error(this, "addExternalPronunciation (" + word.toString() + "): native Elvis does not exist!");
            return 2;
        } else {
            this.f1555f = true;
            int elvisAddExternalPronunciation = elvisAddExternalPronunciation(this.f1550a, word.getSurfaceForm(), word.getSpokenForm());
            if (elvisAddExternalPronunciation == 6) {
                Logger.warn(this, "addExternalPronunciation (" + word.toString() + "): no pronunciation found.");
                return 1;
            } else if (elvisAddExternalPronunciation == 0) {
                return 0;
            } else {
                Logger.error(this, "addExternalPronunciation (" + word.toString() + "): native Elvis error.");
                return 2;
            }
        }
    }

    public final int a(String str, int i, int i2, boolean z, boolean z2) {
        if (this.f1550a == 0) {
            Logger.error(this, "addConstraint (" + str + "): native Elvis does not exist!");
            return 2;
        }
        this.f1555f = true;
        return elvisAddConstraint(this.f1550a, str, i, i2, z, z2);
    }

    public final int a(String str, Word word, boolean z) {
        if (this.f1550a == 0) {
            Logger.error(this, "addWordClassMembers (Class " + str + "): native Elvis does not exist!");
            return 2;
        } else if (word == null) {
            Logger.error(this, "addWordClassMembers (Class " + str + "): word list is null!");
            return 2;
        } else {
            String surfaceForm = word.getSurfaceForm();
            String spokenForm = word.getSpokenForm();
            int elvisAddClassMember = elvisAddClassMember(this.f1550a, str, surfaceForm, spokenForm, z, 10);
            if (elvisAddClassMember == 6) {
                Logger.warn(this, "No pronunciation found.  Cannot add member to word class (" + str + "): " + surfaceForm + "," + spokenForm);
                return 1;
            } else if (elvisAddClassMember == 0) {
                return 0;
            } else {
                Logger.error(this, "Could not add member to word class (" + str + "): " + surfaceForm + "," + spokenForm);
                return 2;
            }
        }
    }

    public final int a(String str, String str2, String str3, int i) {
        if (this.f1550a == 0) {
            Logger.error(this, "Error adding transitions to constraint (" + str + "): " + str2 + "," + str3 + ": native Elvis does not exist.");
            return 1;
        }
        String str4 = str2 == null ? Constraint.START : str2;
        String str5 = str3 == null ? Constraint.END : str3;
        if (elvisAddTransition(this.f1550a, str, str4, str5, i) == 0) {
            return 0;
        }
        Logger.error(this, "Error adding transitions to constraint (" + str + "): " + str4 + "," + str5);
        return 1;
    }

    public final int a(short[] sArr) {
        if (this.f1550a == 0) {
            Logger.error(this, "processAudio: native Elvis does not exist.");
            return 3;
        } else if (sArr != null) {
            return elvisProcessAudio(this.f1550a, sArr, sArr.length);
        } else {
            return 0;
        }
    }

    public final ElvisResult a(Grammar grammar) {
        ElvisNbestList elvisNbestList;
        ElvisResult elvisResult;
        int i = 0;
        if (this.f1550a == 0) {
            Logger.error(this, "getResult: native Elvis does not exist.");
            return null;
        }
        int elvisGetChoiceCount = elvisGetChoiceCount(this.f1550a);
        int elvisGetGateConfidence = elvisGetGateConfidence(this.f1550a);
        if (elvisGetChoiceCount < 0) {
            Logger.error(this, "getResult: error retrieving choices.");
            elvisNbestList = null;
        } else if (elvisGetChoiceCount == 0 || elvisGetChoiceCount <= 0) {
            elvisNbestList = null;
        } else {
            new StringBuilder("getResult: ").append(elvisGetChoiceCount).append(" choices.");
            ArrayList arrayList = new ArrayList(elvisGetChoiceCount);
            int i2 = 0;
            while (true) {
                if (i2 >= elvisGetChoiceCount) {
                    break;
                } else if (elvisGetChoiceListEntry(this.f1550a, i2, arrayList) != 0) {
                    Logger.error(this, "elvis result error.");
                    break;
                } else {
                    i2++;
                }
            }
            ArrayList arrayList2 = new ArrayList(elvisGetChoiceCount);
            while (true) {
                int i3 = i;
                if (i3 >= arrayList.size()) {
                    break;
                }
                arrayList2.add(((NbestEntryJni) arrayList.get(i3)).createRealEntry(grammar));
                i = i3 + 1;
            }
            elvisNbestList = new ElvisNbestList((List) arrayList2);
        }
        if (elvisNbestList == null || elvisNbestList.isEmpty()) {
            Logger.warn(this, "No nbest results.");
            elvisResult = null;
        } else {
            elvisResult = new DefaultResultFilter().applyFilter(new ElvisResult(elvisNbestList, elvisGetGateConfidence));
        }
        return elvisResult;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0072  */
    public final Grammar a(String str) {
        Grammar grammar;
        String str2 = str + this.f1552c + this.f1553d + ".psa";
        String str3 = str + this.f1552c + this.f1553d + ".grm";
        if (this.f1550a == 0) {
            Logger.error(this, "loadElvis(" + str + "): native Elvis does not exist.");
            return null;
        }
        FileInputStream openFileForReading = this.f1551b.openFileForReading(str3);
        if (openFileForReading != null) {
            try {
                grammar = Grammar.createFromJSON(JSONUtils.readFromStream(openFileForReading));
            } catch (Exception e2) {
                Logger.error(this, "Error reading Elvis grm file", e2);
            }
            if (grammar != null) {
                return null;
            }
            int elvisLoad = elvisLoad(this.f1550a, str2);
            if (elvisLoad != 0) {
                Logger.error(this, "loadElvis (" + str2 + ") error loading Elvis: " + elvisLoad);
                return null;
            }
            this.f1555f = true;
            return grammar;
        }
        grammar = null;
        if (grammar != null) {
        }
    }

    public final boolean a() {
        if (!this.f1555f) {
            return this.f1550a != 0;
        }
        if (this.f1550a != 0) {
            elvisDestroy(this.f1550a);
            this.f1550a = 0;
        }
        return a(this.f1552c, this.f1553d, this.f1554e);
    }

    public final boolean a(String str, int i) {
        if (this.f1550a != 0) {
            return elvisEnableRecognitionLogging(this.f1550a, str, i) == 0;
        }
        Logger.error(this, "Error enabling recognition logging: native Elvis does not exist.");
        return false;
    }

    public final boolean a(String str, int i, String str2) {
        if (this.f1550a != 0) {
            Logger.error(this, "createElvis: native Elvis already exists.");
            return false;
        }
        if (str2 != null) {
            FileManager fileManager = this.f1551b;
            BinFileParametersJni binFileParametersJni = new BinFileParametersJni();
            if (elvisGetParamsFromBinFile(fileManager, str2, binFileParametersJni) != 0) {
                binFileParametersJni = null;
            }
            if (binFileParametersJni == null) {
                Logger.warn(this, "createElvis: Elvis binfile not found: " + str2);
                if (str != null) {
                    this.f1550a = elvisCreate(this.f1551b, null, new String(str), i);
                }
            } else {
                if (str == null) {
                    str = binFileParametersJni._languageCode;
                    i = binFileParametersJni._sampleRate;
                } else if (!str.equals(binFileParametersJni._languageCode) || binFileParametersJni._sampleRate != i) {
                    Logger.error(this, "createElvis: wrong language / sampling rate detected for Elvis bin file");
                    Logger.error(this, "Found " + binFileParametersJni._languageCode + ":" + binFileParametersJni._sampleRate + ", expected " + str + ":" + i);
                    Logger.error(this, "Unable to create native Elvis with requested language and frequency.");
                    return false;
                }
                this.f1550a = elvisCreate(this.f1551b, new String(str2), null, 1234);
            }
        } else if (str != null) {
            this.f1550a = elvisCreate(this.f1551b, null, new String(str), i);
        } else {
            Logger.error(this, "No binfile or language specified.");
        }
        if (this.f1550a != 0) {
            this.f1552c = str;
            this.f1553d = i;
            this.f1554e = str2;
            this.f1555f = false;
            return true;
        }
        Logger.error(this, "Unable to create native Elvis");
        return false;
    }

    public final boolean a(String str, Grammar grammar) {
        String str2 = str + this.f1552c + this.f1553d + ".psa";
        String str3 = str + this.f1552c + this.f1553d + ".grm";
        if (this.f1550a == 0) {
            Logger.error(this, "saveElvis(" + str + "): native Elvis does not exist.");
            return false;
        }
        int elvisSave = elvisSave(this.f1550a, str2);
        if (elvisSave != 0) {
            Logger.error(this, "saveElvis (" + str2 + ") error saving Elvis: " + elvisSave);
            return false;
        }
        FileOutputStream openFileForWriting = this.f1551b.openFileForWriting(str3);
        if (openFileForWriting == null) {
            return false;
        }
        try {
            JSONUtils.saveToStream(grammar.toJSON(), openFileForWriting);
            return true;
        } catch (Exception e2) {
            Logger.error(this, "Error saving Elvis grammar", e2);
            return false;
        }
    }

    public final boolean a(String str, String str2, String str3) {
        String str4 = str + "elvisdump.psa";
        String str5 = str2 + File.separator + str3 + "log.psa";
        String str6 = str + "elvisdump.rpr";
        String str7 = str2 + File.separator + str3 + "log.rpr";
        String str8 = str + "elvisdump.utt";
        String str9 = str2 + File.separator + str3 + "log.utt";
        String str10 = str + "elvisdump.sph";
        String str11 = str2 + File.separator + str3 + "log.sph";
        File file = new File(str2);
        if (!file.exists()) {
            if (!file.mkdir()) {
                Logger.error(this, "Destination folder cannot be created.");
                return false;
            }
        } else if (!file.isDirectory()) {
            Logger.error(this, "Destination folder is not a valid directory.");
            return false;
        }
        if (!a(str4, str5) || !a(str6, str7) || !a(str8, str9)) {
            return false;
        }
        return a(str10, str11);
    }

    public final boolean a(List list) {
        Pattern compile = Pattern.compile("([a-z]+)_([a-z]+)(\\d+)k\\.bin");
        String[] findFiles = this.f1551b.findFiles(compile);
        if (findFiles != null) {
            for (String matcher : findFiles) {
                Matcher matcher2 = compile.matcher(matcher);
                if (matcher2.matches()) {
                    String a2 = d.a(matcher2);
                    String b2 = d.b(matcher2);
                    if (!(a2 == null || b2 == null)) {
                        list.add(new g(a2, Integer.parseInt(b2)));
                    }
                }
            }
        }
        return true;
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<java.lang.String>, for r7v0, types: [java.util.List, java.util.List<java.lang.String>] */
    public final boolean a(List<String> list, int i) {
        if (this.f1550a == 0) {
            Logger.error(this, "Error starting wakeup mode: native Elvis does not exist.");
            return false;
        } else if (list == null) {
            return false;
        } else {
            String[] strArr = new String[list.size()];
            int i2 = 0;
            for (String str : list) {
                strArr[i2] = str;
                new StringBuilder("Phrase [").append(i2).append("]: ").append(strArr[i2]);
                i2++;
            }
            return elvisStartWakeupMode(this.f1550a, strArr, i) == 0;
        }
    }

    public final boolean a(boolean z) {
        return this.f1550a != 0 && elvisStopRecognizing(this.f1550a, z) == 0;
    }

    public final int b(short[] sArr) {
        if (this.f1550a == 0) {
            Logger.error(this, "processWakeupAudio: native Elvis does not exist.");
            return 3;
        } else if (sArr != null) {
            return elvisProcessWakeupAudio(this.f1550a, sArr, sArr.length);
        } else {
            return 0;
        }
    }

    public final void b() {
        if (this.f1550a == 0) {
            Logger.error(this, "destroyElvis: native Elvis does not exist!");
            return;
        }
        elvisDestroy(this.f1550a);
        this.f1550a = 0;
    }

    public final void b(boolean z) {
        elvisEnableVerboseLogging(z);
    }

    public final boolean b(String str) {
        if (this.f1550a != 0) {
            return elvisDeleteConstraint(this.f1550a, str) == 0;
        }
        Logger.error(this, "deleteConstraint (" + str + "): native Elvis does not exist!");
        return false;
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<java.lang.String>, for r7v0, types: [java.util.List, java.util.List<java.lang.String>] */
    public final boolean b(List<String> list) {
        if (this.f1550a == 0) {
            Logger.error(this, "Error starting recognize: native Elvis does not exist.");
            return false;
        }
        String[] strArr = new String[list.size()];
        int i = 0;
        for (String str : list) {
            strArr[i] = str;
            new StringBuilder("Constraint [").append(i).append("]: ").append(strArr[i]);
            i++;
        }
        return elvisStartRecognizing(this.f1550a, strArr, i) == 0;
    }

    public final boolean c() {
        if (this.f1550a == 0) {
            Logger.error(this, "clearConstraints: native Elvis does not exist!");
            return false;
        } else if (elvisClearGrammar(this.f1550a) == 0) {
            return true;
        } else {
            Logger.error(this, "Erroring Clearing Elvis constraints.");
            return false;
        }
    }

    public final boolean c(String str) {
        if (this.f1550a != 0) {
            return elvisCheckWakeupPhrase(this.f1550a, str);
        }
        Logger.error(this, "Error checking wakeup phrase: native Elvis does not exist.");
        return false;
    }

    public final boolean d() {
        return false;
    }

    public final boolean e() {
        return (this.f1550a == 0 || 3 == elvisStopWakeupMode(this.f1550a)) ? false : true;
    }

    public native int elvisAddClassMember(long j, String str, String str2, String str3, boolean z, int i);

    public native int elvisAddConstraint(long j, String str, int i, int i2, boolean z, boolean z2);

    public native int elvisAddExternalPronunciation(long j, String str, String str2);

    public native int elvisAddTransition(long j, String str, String str2, String str3, int i);

    public native int elvisAddWordClass(long j, String str, int i, boolean z);

    public native boolean elvisCheckWakeupPhrase(long j, String str);

    public native int elvisClearGrammar(long j);

    public native long elvisCreate(FileManager fileManager, String str, String str2, int i);

    public native int elvisDeleteConstraint(long j, String str);

    public native void elvisDestroy(long j);

    public native int elvisEnableRecognitionLogging(long j, String str, int i);

    public native void elvisEnableVerboseLogging(boolean z);

    public native int elvisGetChoiceCount(long j);

    public native int elvisGetChoiceListEntry(long j, int i, List list);

    public native int elvisGetGateConfidence(long j);

    public native int elvisGetRecognitionLoggingStatus(long j);

    public native int elvisLoad(long j, String str);

    public native int elvisProcessAudio(long j, short[] sArr, int i);

    public native int elvisProcessWakeupAudio(long j, short[] sArr, int i);

    public native int elvisReleaseUnusedMemory(long j);

    public native int elvisSave(long j, String str);

    public native int elvisStartRecognizing(long j, String[] strArr, int i);

    public native int elvisStartWakeupMode(long j, String[] strArr, int i);

    public native int elvisStopRecognizing(long j, boolean z);

    public native int elvisStopWakeupMode(long j);

    public final ElvisResult f() {
        ElvisResult elvisResult;
        if (this.f1550a == 0) {
            Logger.error(this, "getWakeupResult: native Elvis does not exist.");
            return null;
        }
        int elvisGetChoiceCount = elvisGetChoiceCount(this.f1550a);
        int elvisGetGateConfidence = elvisGetGateConfidence(this.f1550a);
        if (elvisGetChoiceCount < 0) {
            Logger.error(this, "getWakeupResult: error retrieving choices.");
            elvisResult = null;
        } else if (elvisGetChoiceCount == 0 || elvisGetChoiceCount <= 0) {
            elvisResult = null;
        } else {
            new StringBuilder("getResult: ").append(elvisGetChoiceCount).append(" choices.");
            ArrayList arrayList = new ArrayList(1);
            if (elvisGetChoiceListEntry(this.f1550a, 0, arrayList) != 0 || arrayList.isEmpty()) {
                Logger.error(this, "elvis result error.");
                elvisResult = null;
            } else {
                elvisResult = new ElvisResult(new ElvisNbestList(((NbestEntryJni) arrayList.get(0)).createRealEntry(null)), elvisGetGateConfidence);
            }
        }
        return elvisResult;
    }

    public final void g() {
        if (this.f1550a == 0) {
            Logger.error(this, "Error releasing unused memory: native Elvis does not exist.");
        } else {
            elvisReleaseUnusedMemory(this.f1550a);
        }
    }

    public final int h() {
        if (this.f1550a != 0) {
            return elvisGetRecognitionLoggingStatus(this.f1550a);
        }
        Logger.error(this, "Error getting logging status: native Elvis does not exist.");
        return 4;
    }
}
