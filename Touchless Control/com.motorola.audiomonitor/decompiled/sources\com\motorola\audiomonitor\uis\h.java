package com.motorola.audiomonitor.uis;

import android.view.View;
import android.view.View.OnClickListener;
import com.motorola.audiomonitor.actions.utils.a;

class h implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ a f702a;

    /* renamed from: b reason: collision with root package name */
    final /* synthetic */ int f703b;

    /* renamed from: c reason: collision with root package name */
    final /* synthetic */ f f704c;

    h(f fVar, a aVar, int i) {
        this.f704c = fVar;
        this.f702a = aVar;
        this.f703b = i;
    }

    public void onClick(View view) {
        if (d.f692c) {
            new i(this).start();
            d.f694h.getActivity().finish();
            return;
        }
        d.f691b = this.f703b;
        as.c(true);
    }
}
