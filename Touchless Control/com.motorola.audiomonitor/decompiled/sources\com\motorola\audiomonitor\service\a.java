package com.motorola.audiomonitor.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import c.d;
import com.motorola.audiomonitor.aa;
import com.motorola.audiomonitor.s;
import h.k;

public class a implements s, o {

    /* renamed from: a reason: collision with root package name */
    private static final String f556a = ("AMonitor." + a.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private final aa f557b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final d f558c;

    /* renamed from: d reason: collision with root package name */
    private final Context f559d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public boolean f560e = false;

    /* renamed from: f reason: collision with root package name */
    private final BroadcastReceiver f561f = new b(this);

    public a(Context context, d dVar, aa aaVar) {
        this.f559d = context;
        this.f557b = aaVar;
        this.f558c = dVar;
        this.f558c.a((o) this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        this.f559d.registerReceiver(this.f561f, intentFilter);
    }

    public void a() {
        d.a(this.f559d, "dsp_triggers");
        if (this.f557b != null) {
            this.f557b.a(0.0f, this);
        }
    }

    public void a(int i, int i2, short[] sArr) {
    }

    public void a(String str, String str2) {
    }

    public void b() {
        this.f560e = false;
        this.f558c.e();
    }

    public void c() {
        b();
        e_();
    }

    public void d() {
        this.f559d.unregisterReceiver(this.f561f);
        this.f558c.b((o) this);
    }

    public void e_() {
        this.f560e = true;
        if (com.motorola.audiomonitor.a.l || k.b(this.f559d)) {
            this.f558c.a(true);
        }
    }
}
