package com.nuance.dragon.toolkit.recognizer;

import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import org.json.JSONException;
import org.json.JSONObject;

public class NMTLanguage implements JSONCompliant {
    public final String description;
    public final String name;

    public NMTLanguage(String str, String str2) {
        this.name = str;
        this.description = str2;
    }

    public static NMTLanguage createFromJSON(JSONObject jSONObject) {
        NMTLanguage[] nMTLanguageArr;
        String string = jSONObject.getString("name");
        for (NMTLanguage nMTLanguage : NMTLangSpecs.ALL_LANGUAGES) {
            if (nMTLanguage.name.equals(string)) {
                return nMTLanguage;
            }
        }
        throw new JSONException("Invalid language name: " + string);
    }

    public final boolean equals(Object obj) {
        return obj != null && (obj instanceof NMTLanguage) && ((NMTLanguage) obj).name.equals(this.name);
    }

    public JSONObject toJSON() {
        b bVar = new b();
        bVar.a("name", (Object) this.name);
        return bVar;
    }

    public final String toString() {
        return this.description;
    }
}
