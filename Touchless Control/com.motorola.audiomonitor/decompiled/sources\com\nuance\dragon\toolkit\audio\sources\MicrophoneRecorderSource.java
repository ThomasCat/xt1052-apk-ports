package com.nuance.dragon.toolkit.audio.sources;

import android.media.AudioRecord;
import android.os.Handler;
import android.os.SystemClock;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.d;
import com.nuance.dragon.toolkit.util.internal.f;

public class MicrophoneRecorderSource extends RecorderSource {

    /* renamed from: a reason: collision with root package name */
    private AudioType f1256a;

    /* renamed from: b reason: collision with root package name */
    private int f1257b;

    /* renamed from: c reason: collision with root package name */
    private AudioRecord f1258c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public boolean f1259d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public Handler f1260e;

    /* renamed from: f reason: collision with root package name */
    private int f1261f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public int f1262g;

    /* renamed from: h reason: collision with root package name */
    private int f1263h;
    private long i;
    private final int j;

    public MicrophoneRecorderSource(int i2, AudioType audioType) {
        this(i2, audioType, 400, null);
    }

    public MicrophoneRecorderSource(int i2, AudioType audioType, int i3, Handler handler) {
        super(audioType, handler);
        d.a("defaultBufferLengthInMs", "stricly greater than 0", i3 > 0);
        this.f1257b = i2;
        this.j = i3;
    }

    public MicrophoneRecorderSource(AudioType audioType) {
        this(audioType, 400, null);
    }

    public MicrophoneRecorderSource(AudioType audioType, int i2, Handler handler) {
        this(0, audioType, i2, handler);
    }

    public MicrophoneRecorderSource(AudioType audioType, Handler handler) {
        this(audioType, 400, handler);
    }

    private void a() {
        this.f1259d = true;
        if (this.f1258c != null) {
            if (this.f1258c.getState() == 1) {
                this.f1258c.stop();
            }
            this.f1258c.release();
        }
        if (this.f1260e != null) {
            this.f1260e.removeCallbacksAndMessages(null);
            this.f1260e = null;
        }
    }

    /* access modifiers changed from: private */
    public AudioChunk b() {
        short[] sArr;
        if (this.f1259d) {
            return null;
        }
        int i2 = this.f1261f;
        short[] sArr2 = new short[this.f1261f];
        int i3 = 0;
        do {
            int read = this.f1258c.read(sArr2, i3, i2);
            if (read < 0) {
                break;
            }
            i3 += read;
            i2 -= read;
        } while (i2 > 0);
        if (i3 <= 0) {
            Logger.error(this, "readBuffer() _recorder.read() didn't read any audio!!!");
            a();
            handleSourceClosed();
            return null;
        }
        if (i3 < this.f1261f) {
            Logger.warn(this, "readBuffer() _recorder.read() didn't read expected len!!! " + i3);
            sArr = f.a(sArr2, 0, i3);
        } else {
            sArr = sArr2;
        }
        if (this.f1263h == 0) {
            this.i = SystemClock.uptimeMillis() - ((long) this.f1256a.getDuration(i3));
        }
        long duration = this.i + ((long) this.f1256a.getDuration(this.f1263h));
        this.f1263h = i3 + this.f1263h;
        return new AudioChunk(this.f1256a, sArr, duration);
    }

    /* access modifiers changed from: protected */
    public boolean isCodecSupported(AudioType audioType) {
        return audioType.encoding == Encoding.PCM_16;
    }

    /* access modifiers changed from: protected */
    public boolean startRecordingInternal(AudioType audioType) {
        int i2 = 8000;
        Encoding encoding = audioType.encoding;
        Encoding encoding2 = Encoding.PCM_16;
        this.f1259d = false;
        this.f1256a = audioType;
        this.f1263h = 0;
        if (audioType.frequency != 8000) {
            i2 = audioType.frequency == 11025 ? 11025 : audioType.frequency == 22050 ? 22050 : audioType.frequency == 44100 ? 44100 : 16000;
        }
        int i3 = (this.j * (i2 * 2)) / 1000;
        int minBufferSize = AudioRecord.getMinBufferSize(i2, 2, 2);
        if (minBufferSize == -2 || minBufferSize == -1 || minBufferSize <= i3) {
            minBufferSize = i3;
        }
        this.f1261f = (minBufferSize / 2) / 2;
        this.f1262g = this.j / 4;
        this.f1258c = new AudioRecord(this.f1257b, i2, 2, 2, minBufferSize);
        if (this.f1258c.getState() != 1) {
            Logger.error(this, "startRecordingInternal() instaniate AudioRecord failed!!!");
            a();
            return false;
        }
        this.f1260e = new Handler();
        this.f1260e.postDelayed(new Runnable() {

            /* renamed from: b reason: collision with root package name */
            private boolean f1265b = false;

            public final void run() {
                if (!MicrophoneRecorderSource.this.f1259d) {
                    AudioChunk b2 = MicrophoneRecorderSource.this.b();
                    if (b2 != null) {
                        if (!this.f1265b) {
                            this.f1265b = true;
                            MicrophoneRecorderSource.this.handleStarted();
                        }
                        MicrophoneRecorderSource.this.handleNewAudio(b2);
                    }
                    if (!MicrophoneRecorderSource.this.f1259d) {
                        MicrophoneRecorderSource.this.f1260e.postDelayed(this, (long) MicrophoneRecorderSource.this.f1262g);
                    }
                }
            }
        }, (long) this.f1262g);
        this.f1258c.startRecording();
        return true;
    }

    /* access modifiers changed from: protected */
    public void stopRecordingInternal() {
        if (!this.f1259d) {
            AudioChunk b2 = b();
            if (b2 != null) {
                handleNewAudio(b2);
            }
            a();
            handleSourceClosed();
        }
    }
}
