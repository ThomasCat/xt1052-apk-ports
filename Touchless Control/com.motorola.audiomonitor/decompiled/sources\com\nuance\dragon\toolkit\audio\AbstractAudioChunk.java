package com.nuance.dragon.toolkit.audio;

public abstract class AbstractAudioChunk {
    public static final int PCM_11K_SAMPLE_RATE_IN_HZ = 11025;
    public static final int PCM_11K_SAMPLE_SIZE_IN_BYTES = 2;
    public static final int PCM_16K_SAMPLE_RATE_IN_HZ = 16000;
    public static final int PCM_16K_SAMPLE_SIZE_IN_BYTES = 2;
    public static final int PCM_22K_SAMPLE_RATE_IN_HZ = 22050;
    public static final int PCM_22K_SAMPLE_SIZE_IN_BYTES = 2;
    public static final int PCM_44K_SAMPLE_RATE_IN_HZ = 44100;
    public static final int PCM_44K_SAMPLE_SIZE_IN_BYTES = 2;
    public static final int PCM_8K_SAMPLE_RATE_IN_HZ = 8000;
    public static final int PCM_8K_SAMPLE_SIZE_IN_BYTES = 2;
    public final byte[] audioBytes;
    public final int audioDuration;
    public final long audioEndTimestamp;
    public final short[] audioShorts;
    public final SpeechStatus audioSpeechStatus;
    public final long audioTimestamp;
    public final AudioType audioType;

    public enum SpeechStatus {
        UNKNOWN,
        NO_SPEECH,
        SPEECH
    }

    protected AbstractAudioChunk(AudioType audioType2, short[] sArr, byte[] bArr, long j, int i, SpeechStatus speechStatus) {
        this.audioType = audioType2;
        this.audioShorts = sArr;
        this.audioBytes = bArr;
        this.audioTimestamp = j;
        this.audioEndTimestamp = ((long) i) + j;
        this.audioDuration = i;
        this.audioSpeechStatus = speechStatus;
    }
}
