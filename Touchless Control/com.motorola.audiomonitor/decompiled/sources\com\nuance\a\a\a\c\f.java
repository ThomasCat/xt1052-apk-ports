package com.nuance.a.a.a.c;

import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.a.b.a.b.C0002b;
import com.nuance.a.a.a.a.b.a.d;
import com.nuance.a.a.a.a.b.a.d.C0003d;
import com.nuance.a.a.a.a.b.a.d.e;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Vector;
import javax.net.ssl.SSLSocket;

public class f implements C0002b, d {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final C0001a f964a = com.nuance.a.a.a.a.b.a.a.a(f.class);
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public static final Integer f965c = new Integer(0);

    /* renamed from: d reason: collision with root package name */
    private static final Integer f966d = new Integer(1);
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public static final Integer f967e = new Integer(2);

    /* renamed from: f reason: collision with root package name */
    private static final Integer f968f = new Integer(3);

    /* renamed from: b reason: collision with root package name */
    private com.nuance.a.a.a.a.b.a.b f969b = null;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public Object f970g = new Object();

    final class a {

        /* renamed from: a reason: collision with root package name */
        Socket f971a;

        /* renamed from: b reason: collision with root package name */
        com.nuance.a.a.a.a.b.a.d.a f972b;

        /* renamed from: c reason: collision with root package name */
        InputStream f973c = null;

        /* renamed from: d reason: collision with root package name */
        OutputStream f974d = null;

        /* renamed from: e reason: collision with root package name */
        Object f975e = null;

        /* renamed from: f reason: collision with root package name */
        c f976f = null;

        public a(Socket socket, com.nuance.a.a.a.a.b.a.d.a aVar, Object obj, InputStream inputStream, OutputStream outputStream, c cVar) {
            this.f971a = socket;
            this.f972b = aVar;
            this.f975e = obj;
            this.f973c = inputStream;
            this.f974d = outputStream;
            this.f976f = cVar;
        }
    }

    final class b {

        /* renamed from: a reason: collision with root package name */
        com.nuance.a.a.a.a.b.a.d.b f977a;

        /* renamed from: b reason: collision with root package name */
        byte[] f978b;

        /* renamed from: c reason: collision with root package name */
        int f979c = 0;

        /* renamed from: d reason: collision with root package name */
        int f980d;

        /* renamed from: e reason: collision with root package name */
        e f981e;

        /* renamed from: f reason: collision with root package name */
        Object f982f;

        public b(com.nuance.a.a.a.a.b.a.d.b bVar, byte[] bArr, int i, e eVar, Object obj) {
            this.f977a = bVar;
            this.f978b = bArr;
            this.f980d = i;
            this.f981e = eVar;
            this.f982f = obj;
        }
    }

    final class c extends Thread {

        /* renamed from: b reason: collision with root package name */
        private String f985b = null;

        /* renamed from: c reason: collision with root package name */
        private int f986c;

        /* renamed from: d reason: collision with root package name */
        private C0003d f987d = null;

        /* renamed from: e reason: collision with root package name */
        private com.nuance.a.a.a.a.b.a.d.a f988e = null;

        /* renamed from: f reason: collision with root package name */
        private Object f989f = null;

        /* renamed from: g reason: collision with root package name */
        private com.nuance.a.a.a.a.b.a.d.c f990g = com.nuance.a.a.a.a.b.a.d.c.f788a;
        /* access modifiers changed from: private */

        /* renamed from: h reason: collision with root package name */
        public boolean f991h = false;
        private Vector i = null;
        private com.nuance.a.a.a.c.b.a.c j = null;
        private boolean k = false;

        public c(String str, int i2, C0003d dVar, com.nuance.a.a.a.a.b.a.d.a aVar) {
            this.f985b = str;
            this.f986c = i2;
            this.f987d = dVar;
            this.f988e = aVar;
            this.f989f = null;
            this.i = new Vector();
        }

        public c(String str, int i2, com.nuance.a.a.a.c.b.a.c cVar, C0003d dVar, com.nuance.a.a.a.a.b.a.d.a aVar) {
            this.f985b = str;
            this.f986c = i2;
            this.f987d = dVar;
            this.f988e = aVar;
            this.f989f = null;
            this.i = new Vector();
            this.k = true;
            this.j = cVar;
        }

        public final com.nuance.a.a.a.a.b.a.d.c a(b bVar) {
            com.nuance.a.a.a.a.b.a.d.c cVar;
            Object obj;
            if (!this.f991h) {
                com.nuance.a.a.a.a.b.a.d.c cVar2 = com.nuance.a.a.a.a.b.a.d.c.f788a;
                synchronized (f.this.f970g) {
                    if (this.i == null) {
                        cVar = com.nuance.a.a.a.a.b.a.d.c.f789b;
                        obj = "SocketReadThread: queue is null!!";
                    } else {
                        this.i.addElement(bVar);
                        cVar = cVar2;
                        obj = null;
                    }
                    f.this.f970g.notify();
                }
                if (!f.f964a.e() || obj == null) {
                    return cVar;
                }
                f.f964a.e(obj);
                return cVar;
            }
            if (f.f964a.e()) {
                f.f964a.e("SocketReadThread is already stopping!");
            }
            return com.nuance.a.a.a.a.b.a.d.c.f789b;
        }

        public final void a() {
            synchronized (f.this.f970g) {
                this.f991h = true;
                this.i.removeAllElements();
                this.i = null;
                f.this.f970g.notify();
            }
        }

        public final void b() {
            String str = null;
            synchronized (f.this.f970g) {
                if (this.i == null) {
                    str = "SocketReadThread.cleanPendingJobs(): queue is null!!";
                } else {
                    this.i.removeAllElements();
                }
            }
            if (f.f964a.e() && str != null) {
                f.f964a.e(str);
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:102:0x03e9, code lost:
            r3 = r3 + r4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:103:0x03eb, code lost:
            r3 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:105:0x03f4, code lost:
            if (com.nuance.a.a.a.c.f.a().e() != false) goto L_0x03f6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:106:0x03f6, code lost:
            com.nuance.a.a.a.c.f.a().e("Socket Read/Available Expception - [" + r3.getClass().getName() + "] Message - [" + r3.getMessage() + "]");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:107:0x0428, code lost:
            r13.f990g = com.nuance.a.a.a.a.b.a.d.c.f789b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:126:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:127:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:129:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:65:0x028e, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:67:0x0297, code lost:
            if (com.nuance.a.a.a.c.f.a().e() != false) goto L_0x0299;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:68:0x0299, code lost:
            com.nuance.a.a.a.c.f.a().e("NetworkSystemJ2me.run() " + r1.getClass().getName() + " " + r1.getMessage());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:89:0x0380, code lost:
            r3 = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:92:0x0383, code lost:
            if (r3 >= r2.f980d) goto L_0x039b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:93:0x0385, code lost:
            r4 = r5.read(r2.f978b, r2.f979c + r3, r2.f980d - r3);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:94:0x0391, code lost:
            if (r4 >= 0) goto L_0x03e9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:96:0x0395, code lost:
            if (r13.f991h != false) goto L_0x039b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:97:0x0397, code lost:
            r13.f990g = com.nuance.a.a.a.a.b.a.d.c.f789b;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        public final void run() {
            a aVar;
            b bVar;
            if (f.f964a.b()) {
                f.f964a.b((Object) "Open Socket connection on host:- [" + this.f985b + "] and port[" + this.f986c + "]");
            }
            try {
                Socket socket = this.k ? com.nuance.a.a.a.c.b.a.a.a(this.f985b, this.f986c, this.j) : new Socket(this.f985b, this.f986c);
                if (!socket.isConnected()) {
                    if (f.f964a.e()) {
                        f.f964a.e("SocketReadThread NETWORK_ERROR. !sock.isConnected()");
                    }
                    f.this.a(new Object[]{f.f965c, this.f987d, com.nuance.a.a.a.a.b.a.d.c.f789b, null, this.f989f});
                    return;
                }
                try {
                    if (this.k) {
                        ((SSLSocket) socket).setUseClientMode(true);
                    }
                    socket.setTcpNoDelay(true);
                    socket.setKeepAlive(true);
                    socket.setSoLinger(true, 40);
                } catch (Exception e2) {
                    if (f.f964a.e()) {
                        f.f964a.e("Set Socket Option Exception - [" + e2.getClass().getName() + "] Message - [" + e2.getMessage() + "]");
                    }
                }
                try {
                    InputStream inputStream = socket.getInputStream();
                    aVar = new a(socket, this.f988e, this.f989f, inputStream, socket.getOutputStream(), this);
                    f.this.a(new Object[]{f.f965c, this.f987d, com.nuance.a.a.a.a.b.a.d.c.f788a, aVar, this.f989f});
                    while (!this.f991h) {
                        synchronized (f.this.f970g) {
                            while (!this.f991h && this.i != null && this.i.isEmpty()) {
                                try {
                                    f.this.f970g.wait();
                                } catch (Exception e3) {
                                    if (f.f964a.e()) {
                                        f.f964a.e("InterruptedException ie:" + e3);
                                    }
                                }
                            }
                            if (!this.f991h && this.i != null) {
                                bVar = (b) this.i.elementAt(0);
                                this.i.removeElementAt(0);
                            }
                        }
                    }
                    return;
                } catch (IOException e4) {
                    try {
                        socket.close();
                    } catch (IOException e5) {
                    }
                    if (f.f964a.e()) {
                        f.f964a.e("Set Socket Option Exception - [" + e4.getClass().getName() + "] Message - [" + e4.getMessage() + "]");
                    }
                    f.this.a(new Object[]{f.f965c, this.f987d, com.nuance.a.a.a.a.b.a.d.c.f789b, null, this.f989f});
                    return;
                }
                if (this.f990g == com.nuance.a.a.a.a.b.a.d.c.f788a) {
                    f.this.a(new Object[]{f.f967e, bVar.f981e, com.nuance.a.a.a.a.b.a.d.c.f788a, aVar, bVar.f978b, new Integer(bVar.f979c), new Integer(bVar.f980d), new Integer(bVar.f980d), bVar.f982f});
                } else {
                    f.this.a(new Object[]{f.f967e, bVar.f981e, com.nuance.a.a.a.a.b.a.d.c.f789b, aVar, bVar.f978b, new Integer(bVar.f979c), new Integer(bVar.f980d), new Integer(0), bVar.f982f});
                    f.this.a((Object) aVar);
                }
            } catch (UnknownHostException e6) {
                if (f.f964a.e()) {
                    f.f964a.e("Open Streams Exception - [" + e6.getClass().getName() + "] Message - [" + e6.getMessage() + "]");
                }
                f.this.a(new Object[]{f.f965c, this.f987d, com.nuance.a.a.a.a.b.a.d.c.f789b, null, this.f989f});
            } catch (IOException e7) {
                if (f.f964a.e()) {
                    f.f964a.e("Open Streams Exception - [" + e7.getClass().getName() + "] Message - [" + e7.getMessage() + "]");
                }
                f.this.a(new Object[]{f.f965c, this.f987d, com.nuance.a.a.a.a.b.a.d.c.f789b, null, this.f989f});
            } catch (SecurityException e8) {
                if (f.f964a.e()) {
                    f.f964a.e("Open Streams Exception - [" + e8.getClass().getName() + "] Message - [" + e8.getMessage() + "]");
                }
                f.this.a(new Object[]{f.f965c, this.f987d, com.nuance.a.a.a.a.b.a.d.c.f789b, null, this.f989f});
            } catch (Throwable th) {
                if (f.f964a.e()) {
                    f.f964a.e("Open Streams Exception - [" + th.getClass().getName() + "] Message - [" + th.getMessage() + "]");
                }
                f.this.a(new Object[]{f.f965c, this.f987d, com.nuance.a.a.a.a.b.a.d.c.f789b, null, this.f989f});
            }
        }
    }

    public f(com.nuance.a.a.a.a.b.a.b bVar) {
        this.f969b = bVar;
    }

    /* access modifiers changed from: private */
    public void a(Object[] objArr) {
        this.f969b.a(objArr, this, this.f969b.b(), this.f969b.a()[0]);
    }

    public final com.nuance.a.a.a.a.b.a.d.c a(Object obj, com.nuance.a.a.a.a.b.a.d.b bVar, byte[] bArr, int i, e eVar, Object obj2) {
        if (bVar != com.nuance.a.a.a.a.b.a.d.b.f787b) {
            if (f964a.e()) {
                f964a.e("Blackberry NetworkSystem only supports NETWORK_READ_FULL");
            }
            return com.nuance.a.a.a.a.b.a.d.c.f789b;
        }
        a aVar = (a) obj;
        if (aVar.f976f == null) {
            if (f964a.b()) {
                f964a.b((Object) "SOCKET READ ERROR: socket read thread is null");
            }
            return com.nuance.a.a.a.a.b.a.d.c.f789b;
        } else if (!aVar.f976f.f991h) {
            return aVar.f976f.a(new b(bVar, bArr, i, eVar, obj2));
        } else {
            if (f964a.e()) {
                f964a.e("socket read thread is stopping");
            }
            return com.nuance.a.a.a.a.b.a.d.c.f789b;
        }
    }

    public final com.nuance.a.a.a.a.b.a.d.c a(Object obj, byte[] bArr, int i, com.nuance.a.a.a.a.b.a.d.f fVar, Object obj2) {
        a aVar = (a) obj;
        if (aVar.f971a == null || aVar.f974d == null) {
            return com.nuance.a.a.a.a.b.a.d.c.f789b;
        }
        OutputStream outputStream = aVar.f974d;
        try {
            outputStream.write(bArr, 0, i);
            outputStream.flush();
            a(new Object[]{f968f, fVar, com.nuance.a.a.a.a.b.a.d.c.f788a, obj, bArr, new Integer(0), new Integer(i), new Integer(i), obj2});
            return com.nuance.a.a.a.a.b.a.d.c.f788a;
        } catch (Exception e2) {
            if (f964a.e()) {
                f964a.e("Socket Write Exception - [" + e2.getClass().getName() + "] Message - [" + e2.getMessage() + "]");
            }
            a(new Object[]{f968f, fVar, com.nuance.a.a.a.a.b.a.d.c.f789b, obj, bArr, new Integer(0), new Integer(i), new Integer(0), obj2});
            a(obj);
            return com.nuance.a.a.a.a.b.a.d.c.f789b;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002a, code lost:
        if (r0.f971a == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002c, code lost:
        if (r1 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r0.f973c.close();
        r0.f973c = null;
        r0.f971a.close();
        r0.f971a = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003e, code lost:
        a(new java.lang.Object[]{f966d, r0.f972b, com.nuance.a.a.a.a.b.a.d.c.f788a, r10, r0.f975e});
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0058, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005f, code lost:
        if (f964a.e() != false) goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0061, code lost:
        f964a.e("Socket Close Expception - [" + r1.getClass().getName() + "] Message - [" + r1.getMessage() + "]");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0091, code lost:
        a(new java.lang.Object[]{f966d, r0.f972b, com.nuance.a.a.a.a.b.a.d.c.f789b, r10, r0.f975e});
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        return;
     */
    public final void a(Object obj) {
        boolean z;
        a aVar = (a) obj;
        if (aVar != null) {
            synchronized (this.f970g) {
                if (aVar.f976f != null) {
                    if (!aVar.f976f.f991h) {
                        aVar.f976f.a();
                        aVar.f976f = null;
                        z = true;
                    } else {
                        z = false;
                    }
                }
            }
        }
    }

    public final void a(Object obj, Object obj2) {
        Object[] objArr = (Object[]) obj;
        if (objArr[0] == f965c) {
            ((C0003d) objArr[1]).a((com.nuance.a.a.a.a.b.a.d.c) objArr[2], objArr[3]);
        } else if (objArr[0] == f966d) {
            ((com.nuance.a.a.a.a.b.a.d.a) objArr[1]).a();
        } else if (objArr[0] == f967e) {
            ((e) objArr[1]).a((com.nuance.a.a.a.a.b.a.d.c) objArr[2], objArr[3], (byte[]) objArr[4], ((Integer) objArr[6]).intValue(), ((Integer) objArr[7]).intValue(), objArr[8]);
        } else if (objArr[0] == f968f) {
            ((com.nuance.a.a.a.a.b.a.d.f) objArr[1]).a((com.nuance.a.a.a.a.b.a.d.c) objArr[2], objArr[3], ((Integer) objArr[6]).intValue(), ((Integer) objArr[7]).intValue(), objArr[8]);
        }
    }

    public final void a(String str, int i, C0003d dVar, com.nuance.a.a.a.a.b.a.d.a aVar) {
        try {
            new c(str, i, dVar, aVar).start();
        } catch (Exception e2) {
            if (f964a.e()) {
                f964a.e("Open Socket Exception - [" + e2.getClass().getName() + "] Message - [" + e2.getMessage() + "]");
            }
            a(new Object[]{f965c, dVar, com.nuance.a.a.a.a.b.a.d.c.f789b, null, null});
        }
    }

    public final void a(String str, int i, Vector vector, C0003d dVar, com.nuance.a.a.a.a.b.a.d.a aVar) {
        com.nuance.a.a.a.c.b.a.c cVar = new com.nuance.a.a.a.c.b.a.c();
        Enumeration elements = vector.elements();
        while (elements.hasMoreElements()) {
            com.nuance.a.a.a.b.b.c cVar2 = (com.nuance.a.a.a.b.b.c) elements.nextElement();
            if (cVar2.a().equals("SSL_SelfSigned_Cert") && (new String(cVar2.b()).equals("TRUE") || new String(cVar2.b()).equals("true"))) {
                cVar.f951a = true;
            }
            if (cVar2.a().equals("SSL_Cert_Summary")) {
                cVar.f952b = new String(cVar2.b());
            }
            if (cVar2.a().equals("SSL_Cert_Data")) {
                cVar.f953c = new String(cVar2.b());
            }
        }
        try {
            new c(str, i, cVar, dVar, aVar).start();
        } catch (Exception e2) {
            if (f964a.e()) {
                f964a.e("Open Socket Exception - [" + e2.getClass().getName() + "] Message - [" + e2.getMessage() + "]");
            }
            a(new Object[]{f965c, dVar, com.nuance.a.a.a.a.b.a.d.c.f789b, null, null});
        }
    }

    public final void b(Object obj) {
        a aVar = (a) obj;
        if (aVar.f976f != null) {
            aVar.f976f.b();
            return;
        }
        if (f964a.b()) {
            f964a.b((Object) "SOCKET WRITE ERROR: socket read thread is null");
        }
        a(obj);
    }
}
