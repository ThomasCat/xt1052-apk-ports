package com.nuance.dragon.toolkit.elvis;

import com.nuance.dragon.toolkit.elvis.ElvisNbestList.Entry;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import org.json.JSONException;
import org.json.JSONObject;

public final class ElvisResult implements JSONCompliant {
    public static final int GATE_CONFIDENCE_UNAVAILABLE = -1;
    public static final int MAX_CONFIDENCE = 100;
    public static final int MIN_CONFIDENCE = 0;

    /* renamed from: a reason: collision with root package name */
    private final ElvisNbestList f1545a;

    /* renamed from: b reason: collision with root package name */
    private final int f1546b;

    /* renamed from: c reason: collision with root package name */
    private final int f1547c;

    ElvisResult(ElvisNbestList elvisNbestList, int i) {
        int score;
        this.f1545a = elvisNbestList;
        if (elvisNbestList.size() < 2) {
            score = 100;
        } else {
            score = ((Entry) elvisNbestList.get(1)).getScore() - ((Entry) elvisNbestList.get(0)).getScore();
            if (score < 0) {
                score = 0;
            }
            if (score > 100) {
                score = 100;
            }
        }
        this.f1546b = score;
        this.f1547c = i;
    }

    public static ElvisResult createFromJSON(JSONObject jSONObject) {
        int i;
        try {
            i = jSONObject.getInt("gate_confidence");
        } catch (JSONException e2) {
            i = -1;
        }
        return new ElvisResult(ElvisNbestList.createFromJSON(jSONObject.getJSONObject("nbest")), i);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ElvisResult elvisResult = (ElvisResult) obj;
        if (this.f1546b != elvisResult.f1546b) {
            return false;
        }
        if (this.f1547c != elvisResult.f1547c) {
            return false;
        }
        return this.f1545a.equals(elvisResult.f1545a);
    }

    public final int getChoiceCount() {
        return this.f1545a.size();
    }

    public final ElvisNbestList getChoiceList() {
        return this.f1545a;
    }

    public final int getConfidence() {
        return this.f1546b;
    }

    public final int getGateConfidence() {
        return this.f1547c;
    }

    public final int hashCode() {
        return ((((this.f1546b + 31) * 31) + this.f1545a.hashCode()) * 31) + this.f1547c;
    }

    public final JSONObject toJSON() {
        b bVar = new b();
        bVar.a("nbest", (Object) this.f1545a.toJSON());
        bVar.a("gate_confidence", (Object) Integer.valueOf(this.f1547c));
        return bVar;
    }

    public final String toString() {
        return ((Entry) this.f1545a.get(0)).toString();
    }
}
