package com.nuance.dragon.toolkit.audio.pipes;

import com.nuance.dragon.toolkit.audio.AbstractAudioChunk;
import com.nuance.dragon.toolkit.audio.AudioPipe;
import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.util.a;
import com.nuance.dragon.toolkit.audio.util.a.C0005a;
import com.nuance.dragon.toolkit.util.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class DuplicatorPipe extends AudioPipe {

    /* renamed from: a reason: collision with root package name */
    private final List f1163a = new CopyOnWriteArrayList();

    /* renamed from: b reason: collision with root package name */
    private final a f1164b = new a();

    /* renamed from: c reason: collision with root package name */
    private final List f1165c = new ArrayList();

    /* access modifiers changed from: protected */
    public void audioSinkConnected(AudioSink audioSink) {
        this.f1163a.add(audioSink);
        this.f1165c.add(this.f1164b.a());
        if (!isSourceActive()) {
            audioSink.sourceClosed(this);
        }
    }

    /* access modifiers changed from: protected */
    public void audioSinkDisconnected(AudioSink audioSink) {
        int indexOf = this.f1163a.indexOf(audioSink);
        if (indexOf >= 0) {
            this.f1163a.remove(indexOf);
            ((C0005a) this.f1165c.remove(indexOf)).c();
            return;
        }
        Logger.warn(this, "Couldn't find audio sink to remove");
    }

    public void chunksAvailable(AudioSource audioSource, AudioSink audioSink) {
        if (audioSource.getChunksAvailableForSink(audioSink) > 0) {
            for (AbstractAudioChunk a2 : audioSource.getAllAudioChunksForSink(audioSink)) {
                this.f1164b.a((Object) a2);
            }
            for (AudioSink chunksAvailable : this.f1163a) {
                chunksAvailable.chunksAvailable(this);
            }
        }
    }

    public void framesDropped(AudioSource audioSource, AudioSink audioSink) {
        for (AudioSink framesDropped : this.f1163a) {
            framesDropped.framesDropped(this);
        }
    }

    public void getAllAudioChunksForSink(AudioSink audioSink, List list) {
        int indexOf = this.f1163a.indexOf(audioSink);
        if (indexOf != -1) {
            C0005a aVar = (C0005a) this.f1165c.get(indexOf);
            for (AbstractAudioChunk abstractAudioChunk = (AbstractAudioChunk) aVar.a(); abstractAudioChunk != null; abstractAudioChunk = (AbstractAudioChunk) aVar.a()) {
                list.add(abstractAudioChunk);
            }
            return;
        }
        Logger.warn(this, "getAudioChunkForSink(): Can't find sink");
    }

    public AbstractAudioChunk getAudioChunkForSink(AudioSink audioSink) {
        int indexOf = this.f1163a.indexOf(audioSink);
        if (indexOf != -1) {
            C0005a aVar = (C0005a) this.f1165c.get(indexOf);
            if (aVar.b() > 0) {
                AbstractAudioChunk abstractAudioChunk = (AbstractAudioChunk) aVar.a();
                if (abstractAudioChunk != null) {
                    return abstractAudioChunk;
                }
                Logger.error(this, "getAudioChunkForSink(): Expected non-null chunk");
            }
        } else {
            Logger.warn(this, "getAudioChunkForSink(): Can't find sink");
        }
        return null;
    }

    public AudioType getAudioType() {
        return getAudioTypeFromSource();
    }

    public int getChunksAvailable() {
        return 0;
    }

    public int getChunksAvailableForSink(AudioSink audioSink) {
        int indexOf = this.f1163a.indexOf(audioSink);
        if (indexOf != -1) {
            return ((C0005a) this.f1165c.get(indexOf)).b();
        }
        return 0;
    }

    public boolean isActive() {
        return isSourceActive();
    }

    public void sourceClosed(AudioSource audioSource, AudioSink audioSink) {
        for (AudioSink sourceClosed : this.f1163a) {
            sourceClosed.sourceClosed(audioSource);
        }
    }
}
