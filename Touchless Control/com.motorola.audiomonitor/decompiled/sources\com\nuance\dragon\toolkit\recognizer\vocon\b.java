package com.nuance.dragon.toolkit.recognizer.vocon;

import com.nuance.dragon.toolkit.recognizer.NMTLangSpecs;
import java.util.regex.Matcher;

class b extends NMTLangSpecs {
    b() {
    }

    static String a(String str) {
        return "clc_~language~_([a-z]+)(.*)\\.dat".replace("~language~", str);
    }

    static String a(String str, int i) {
        return "acmod(\\d+)_(\\d+)_~language~_([a-z]+)_([a-z]+)_f~rate_khz~(.*)\\.dat".replace("~language~", str).replace("~rate_khz~", String.valueOf(i));
    }

    static String a(Matcher matcher) {
        return matcher.group(3);
    }

    static String b(Matcher matcher) {
        return matcher.group(6);
    }
}
