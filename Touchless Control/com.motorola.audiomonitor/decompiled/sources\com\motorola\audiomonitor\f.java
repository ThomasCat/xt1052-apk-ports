package com.motorola.audiomonitor;

import android.media.AudioRecord;
import android.media.MediaRecorder.AudioSource;
import android.media.MediaSyncEvent;
import android.os.Handler;
import android.os.Process;
import android.util.Log;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class f extends j {

    /* renamed from: b reason: collision with root package name */
    private static final String f491b = ("AMonitor." + f.class.getSimpleName());

    /* renamed from: c reason: collision with root package name */
    private static final boolean f492c = a.f160b;

    /* renamed from: d reason: collision with root package name */
    private final int f493d;

    /* renamed from: e reason: collision with root package name */
    private boolean f494e = false;

    /* renamed from: f reason: collision with root package name */
    private boolean f495f = false;

    /* renamed from: g reason: collision with root package name */
    private final Handler f496g;

    public f(boolean z) {
        super("AudioRecorder");
        this.f493d = z ? a("MOT_VR_SOURCE", 6) : 1;
        start();
        this.f496g = new Handler(getLooper());
    }

    private static int a(String str, int i) {
        try {
            return ((Integer) AudioSource.class.getField(str).get(null)).intValue();
        } catch (Exception e2) {
            Log.e(f491b, "Failed to get audio source for " + str + ":" + e2);
            return i;
        }
    }

    private void a(int i, int i2, MediaSyncEvent mediaSyncEvent) {
        if (f492c) {
            Log.d(f491b, "resumeRecording");
        }
        if (this.f496g == null) {
            Log.e(f491b, "resumeRecording called before initialized.");
            return;
        }
        synchronized (this) {
            this.f494e = true;
            this.f496g.post(new g(this, i, i2, mediaSyncEvent));
        }
    }

    private ByteBuffer[] a(ByteBuffer byteBuffer) {
        ByteBuffer[] byteBufferArr = {ByteBuffer.allocateDirect(byteBuffer.capacity() / 2), ByteBuffer.allocateDirect(byteBuffer.capacity() / 2)};
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        byteBufferArr[0].order(ByteOrder.LITTLE_ENDIAN);
        byteBufferArr[1].order(ByteOrder.LITTLE_ENDIAN);
        int capacity = byteBuffer.capacity() / 4;
        for (int i = 0; i < capacity; i++) {
            byteBufferArr[0].putShort(byteBuffer.getShort());
            byteBufferArr[1].putShort(byteBuffer.getShort());
        }
        return byteBufferArr;
    }

    /* access modifiers changed from: private */
    public void b(int i, int i2, MediaSyncEvent mediaSyncEvent) {
        boolean a2;
        int i3 = this.f504a ? 12 : 16;
        int minBufferSize = AudioRecord.getMinBufferSize(i2, i3, 2);
        if (minBufferSize < 0) {
            Log.e(f491b, "getMinBufferSize failed.");
            g();
            return;
        }
        if (f492c) {
            Log.i(f491b, "source=" + i + " buffersize=" + minBufferSize + " sample_rate=" + i2);
        }
        try {
            AudioRecord audioRecord = new AudioRecord(i, i2, i3, 2, minBufferSize * 8);
            if (f492c) {
                Log.d(f491b, "Start audio recording.");
            }
            if (audioRecord.getState() == 1) {
                if (mediaSyncEvent != null) {
                    audioRecord.startRecording(mediaSyncEvent);
                } else {
                    audioRecord.startRecording();
                }
                while (true) {
                    if (!this.f494e) {
                        break;
                    }
                    ByteBuffer allocateDirect = ByteBuffer.allocateDirect(minBufferSize * 2);
                    int read = audioRecord.read(allocateDirect, minBufferSize * 2);
                    if (read == -3) {
                        throw new IllegalStateException("read() returned AudioRecord.ERROR_INVALID_OPERATION");
                    } else if (read == -2) {
                        throw new IllegalStateException("read() returned AudioRecord.ERROR_BAD_VALUE");
                    } else {
                        if (!this.f504a) {
                            a2 = a(new ProtectedByteBuffer(allocateDirect, i2));
                            continue;
                        } else {
                            ByteBuffer[] a3 = a(allocateDirect);
                            a2 = a(new ProtectedByteBuffer(a3[0], i2));
                            if (a2) {
                                a2 = b(new ProtectedByteBuffer(a3[1], i2));
                                continue;
                            } else {
                                continue;
                            }
                        }
                        if (!a2) {
                            if (f492c) {
                                Log.d(f491b, "stop recording.");
                            }
                        }
                    }
                }
                audioRecord.stop();
            } else {
                Log.e(f491b, "not initialized, state=" + audioRecord.getState());
            }
            audioRecord.release();
            if (f492c) {
                Log.d(f491b, "stopped.");
            }
            if (!this.f495f) {
                g();
            }
        } catch (IllegalStateException e2) {
            Log.e(f491b, "AudioRecord init failed.");
            g();
        }
    }

    public void a() {
        a(this.f493d, 16000, null);
    }

    public void a(int i, int i2) {
        a(i, i2, null);
    }

    public void b() {
        if (f492c) {
            Log.d(f491b, "pauseRecording");
        }
        if (this.f496g == null) {
            Log.e(f491b, "pauseRecording called before initialized.");
            return;
        }
        this.f495f = true;
        this.f494e = false;
        synchronized (this) {
            this.f496g.removeCallbacksAndMessages(null);
            this.f496g.post(new h(this));
            while (true) {
                try {
                    wait(3000);
                    break;
                } catch (InterruptedException e2) {
                }
            }
        }
        if (f492c) {
            Log.d(f491b, "pauseRecording complete.");
        }
    }

    public void c() {
        if (f492c) {
            Log.d(f491b, "cleanUp");
        }
        if (this.f496g == null) {
            Log.e(f491b, "cleanUp called before initialized.");
            return;
        }
        this.f495f = false;
        this.f494e = false;
        synchronized (this) {
            this.f496g.removeCallbacksAndMessages(null);
            this.f496g.post(new i(this));
        }
        while (true) {
            try {
                join(3000);
                break;
            } catch (InterruptedException e2) {
            }
        }
        if (f492c && isAlive()) {
            Log.d(f491b, "cleanUp timed out");
        }
        if (f492c) {
            Log.d(f491b, "AudioRecorder cleanUp completed.");
        }
        super.c();
    }

    public void onLooperPrepared() {
        if (f492c) {
            Log.d(f491b, "onLooperPrepared:");
        }
        Process.setThreadPriority(-19);
    }
}
