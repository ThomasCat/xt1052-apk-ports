package com.nuance.dragon.toolkit.audio.sources;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.util.Logger;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

public class StreamingFileRecorderSource extends RecorderSource {

    /* renamed from: a reason: collision with root package name */
    private String f1297a;

    /* renamed from: b reason: collision with root package name */
    private final int f1298b;

    /* renamed from: c reason: collision with root package name */
    private AssetFileDescriptor f1299c;

    /* renamed from: d reason: collision with root package name */
    private final Context f1300d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public boolean f1301e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public FileInputStream f1302f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public boolean f1303g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public Runnable f1304h;
    /* access modifiers changed from: private */
    public byte[] i;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public Handler k;

    public StreamingFileRecorderSource(AudioType audioType, int i2, Context context) {
        super(audioType, null);
        this.f1302f = null;
        this.f1303g = false;
        this.j = 0;
        this.f1298b = i2;
        this.f1300d = context;
        this.f1297a = null;
        this.f1301e = true;
    }

    public StreamingFileRecorderSource(AudioType audioType, String str) {
        this(audioType, str, true);
    }

    public StreamingFileRecorderSource(AudioType audioType, String str, boolean z) {
        super(audioType, null);
        this.f1302f = null;
        this.f1303g = false;
        this.j = 0;
        this.f1297a = str;
        this.f1301e = z;
        this.f1298b = -1;
        this.f1300d = null;
    }

    /* access modifiers changed from: private */
    public void a() {
        if (this.k != null) {
            this.k.removeCallbacks(this.f1304h);
        }
        if (this.f1302f != null) {
            try {
                this.f1302f.close();
            } catch (IOException e2) {
            }
        }
        if (this.f1299c != null) {
            try {
                this.f1299c.close();
            } catch (IOException e3) {
            }
        }
        stopRecording();
    }

    /* access modifiers changed from: protected */
    public boolean isCodecSupported(AudioType audioType) {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean startRecordingInternal(final AudioType audioType) {
        this.f1303g = false;
        this.k = new Handler();
        try {
            if (this.f1297a != null) {
                this.f1302f = new FileInputStream(new File(this.f1297a));
            } else if (this.f1298b == -1 || this.f1300d == null) {
                Logger.error(this, "startRecordingInternal() FileInputStream has not been created!!!");
                return false;
            } else {
                this.f1299c = this.f1300d.getResources().openRawResourceFd(this.f1298b);
                if (this.f1299c == null) {
                    throw new IllegalArgumentException("resourceId must refer to an uncompressed resource");
                }
                this.f1302f = this.f1299c.createInputStream();
            }
            handleStarted();
            int i2 = audioType.encoding == Encoding.PCM_16 ? audioType.frequency == 8000 ? 4800 : audioType.frequency == 11025 ? 6615 : audioType.frequency == 16000 ? 9600 : audioType.frequency == 22050 ? 13230 : audioType.frequency == 44100 ? 26460 : -1 : -1;
            if (i2 > 0) {
                this.i = new byte[i2];
            }
            this.f1304h = new Runnable() {

                /* renamed from: b reason: collision with root package name */
                final /* synthetic */ int f1306b = 300;

                public final void run() {
                    int i;
                    AudioChunk audioChunk;
                    int i2;
                    boolean z = true;
                    if (!StreamingFileRecorderSource.this.f1303g) {
                        if (audioType.encoding == Encoding.SPEEX) {
                            byte[] bArr = new byte[1];
                            byte b2 = 0;
                            while (true) {
                                try {
                                    i2 = StreamingFileRecorderSource.this.f1302f.read(bArr);
                                } catch (IOException e2) {
                                    Logger.error(this, "TimerRunnable.run() reading header _fis.read() threw " + e2 + "!!!");
                                    i2 = 0;
                                }
                                if (i2 != bArr.length) {
                                    StreamingFileRecorderSource.this.f1303g = true;
                                    StreamingFileRecorderSource.this.a();
                                    StreamingFileRecorderSource.this.handleSourceClosed();
                                    return;
                                }
                                byte b3 = (b2 << 7) | (bArr[0] & Byte.MAX_VALUE);
                                if ((bArr[0] & 128) != 0) {
                                    b2 = b3;
                                } else if (b3 <= 0) {
                                    Logger.error(this, "Read file length error: speexBufferLen =" + b3);
                                    StreamingFileRecorderSource.this.handleFramesDropped();
                                    StreamingFileRecorderSource.this.f1303g = true;
                                    StreamingFileRecorderSource.this.a();
                                    StreamingFileRecorderSource.this.handleSourceClosed();
                                    return;
                                } else {
                                    StreamingFileRecorderSource.this.i = new byte[b3];
                                }
                            }
                        }
                        try {
                            i = StreamingFileRecorderSource.this.f1302f.read(StreamingFileRecorderSource.this.i, 0, StreamingFileRecorderSource.this.i.length);
                        } catch (IOException e3) {
                            Logger.error(this, "TimerRunnable.run() _fis.read() threw " + e3 + "!!!");
                            i = 0;
                        }
                        if (i != -1) {
                            if (i == StreamingFileRecorderSource.this.i.length || audioType.encoding != Encoding.PCM_16) {
                                z = false;
                            } else {
                                Arrays.fill(StreamingFileRecorderSource.this.i, i, StreamingFileRecorderSource.this.i.length - 1, 0);
                                i = StreamingFileRecorderSource.this.i.length;
                                z = false;
                            }
                        }
                        if (i > 0) {
                            if (audioType.encoding == Encoding.PCM_16) {
                                short[] sArr = new short[(i / 2)];
                                for (int i3 = 0; i3 < i - 1; i3 += 2) {
                                    if (StreamingFileRecorderSource.this.f1301e) {
                                        sArr[i3 / 2] = (short) (((StreamingFileRecorderSource.this.i[i3 + 1] << 8) & 65280) | (StreamingFileRecorderSource.this.i[i3] & 255));
                                    } else {
                                        sArr[i3 / 2] = (short) (((StreamingFileRecorderSource.this.i[i3] << 8) & 65280) | (StreamingFileRecorderSource.this.i[i3 + 1] & 255));
                                    }
                                }
                                audioChunk = new AudioChunk(audioType, sArr, (long) (this.f1306b * StreamingFileRecorderSource.this.j = StreamingFileRecorderSource.this.j + 1));
                            } else {
                                byte[] bArr2 = new byte[i];
                                System.arraycopy(StreamingFileRecorderSource.this.i, 0, bArr2, 0, i);
                                audioChunk = new AudioChunk(audioType, bArr2, 100);
                            }
                            StreamingFileRecorderSource.this.handleNewAudio(audioChunk);
                        }
                        if (!z) {
                            StreamingFileRecorderSource.this.k.postDelayed(StreamingFileRecorderSource.this.f1304h, (long) this.f1306b);
                            return;
                        }
                        StreamingFileRecorderSource.this.f1303g = true;
                        StreamingFileRecorderSource.this.a();
                        StreamingFileRecorderSource.this.handleSourceClosed();
                    }
                }
            };
            this.k.postDelayed(this.f1304h, 300);
            return true;
        } catch (FileNotFoundException e2) {
            Logger.error(this, "startRecordingInternal() FileNotFoundException!!!");
            this.f1303g = true;
            return false;
        } catch (IOException e3) {
            Logger.error(this, "startRecordingInternal() IOException!!!");
            this.f1303g = true;
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void stopRecordingInternal() {
        if (!this.f1303g) {
            this.f1303g = true;
            a();
            handleSourceClosed();
        }
    }
}
