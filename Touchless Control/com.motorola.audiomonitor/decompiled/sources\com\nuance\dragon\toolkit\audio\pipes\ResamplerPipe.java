package com.nuance.dragon.toolkit.audio.pipes;

import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.audio.util.SpeexResampler;
import com.nuance.dragon.toolkit.util.internal.d;

public class ResamplerPipe extends ConverterPipe {

    /* renamed from: a reason: collision with root package name */
    private final AudioType f1179a;

    /* renamed from: b reason: collision with root package name */
    private final SpeexResampler f1180b = new SpeexResampler();

    public ResamplerPipe(AudioType audioType) {
        d.a("resampledAudioType", (Object) audioType);
        this.f1179a = audioType;
    }

    /* access modifiers changed from: protected */
    public void cleanup() {
        this.f1180b.a();
    }

    /* access modifiers changed from: protected */
    public AudioChunk[] convert(AudioChunk audioChunk) {
        return this.f1180b.a(audioChunk);
    }

    public AudioType getAudioType() {
        return this.f1179a;
    }

    /* access modifiers changed from: protected */
    public void initialize(AudioType audioType) {
        this.f1180b.a(audioType, this.f1179a);
    }

    /* access modifiers changed from: protected */
    public final boolean isAudioSourceTypeSupported(AudioType audioType) {
        return audioType.encoding == Encoding.PCM_16;
    }
}
