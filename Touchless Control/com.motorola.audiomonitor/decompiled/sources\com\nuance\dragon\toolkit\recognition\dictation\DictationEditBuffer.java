package com.nuance.dragon.toolkit.recognition.dictation;

public interface DictationEditBuffer {
    void append(byte[] bArr);

    void delete(long j);

    void delete(long j, long j2);

    AlterChoices getChoices(long j, long j2);

    EditorItem[] getItems();

    void insert(long j, char c2);

    void insert(long j, String str);

    long length();

    void merge(byte[] bArr, long j, long j2);

    void replace(long j, long j2, String str);

    String toString();

    void update(String str);
}
