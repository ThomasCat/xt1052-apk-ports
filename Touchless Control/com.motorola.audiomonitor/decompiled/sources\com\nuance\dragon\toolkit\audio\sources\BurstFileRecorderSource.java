package com.nuance.dragon.toolkit.audio.sources;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.d;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BurstFileRecorderSource extends RecorderSource {

    /* renamed from: a reason: collision with root package name */
    private String f1248a;

    /* renamed from: b reason: collision with root package name */
    private final int f1249b;

    /* renamed from: c reason: collision with root package name */
    private AssetFileDescriptor f1250c;

    /* renamed from: d reason: collision with root package name */
    private final Context f1251d;

    /* renamed from: e reason: collision with root package name */
    private boolean f1252e;

    /* renamed from: f reason: collision with root package name */
    private FileInputStream f1253f;

    /* renamed from: g reason: collision with root package name */
    private byte[] f1254g;

    /* renamed from: h reason: collision with root package name */
    private int f1255h;
    private List i;
    private AudioType j;

    public BurstFileRecorderSource(AudioType audioType, int i2, Context context) {
        super(audioType, null);
        this.f1253f = null;
        this.f1255h = 0;
        d.a("audioType", (Object) audioType);
        d.a("audioType", "a type supported by this player", isCodecSupported(audioType));
        d.a("Context", (Object) context);
        this.j = audioType;
        this.f1249b = i2;
        this.f1251d = context;
        this.f1248a = null;
        this.f1252e = true;
        this.i = new ArrayList();
        a();
    }

    public BurstFileRecorderSource(AudioType audioType, String str) {
        this(audioType, str, true);
    }

    public BurstFileRecorderSource(AudioType audioType, String str, boolean z) {
        super(audioType, null);
        this.f1253f = null;
        this.f1255h = 0;
        d.a("audioType", (Object) audioType);
        d.a("fileName", (Object) str);
        this.j = audioType;
        this.f1248a = str;
        this.f1252e = z;
        this.f1249b = -1;
        this.f1251d = null;
        this.i = new ArrayList();
        a();
    }

    private void a() {
        int i2;
        AudioChunk audioChunk;
        int i3;
        try {
            if (this.f1248a != null) {
                this.f1253f = new FileInputStream(new File(this.f1248a));
            } else if (this.f1249b == -1 || this.f1251d == null) {
                Logger.error(this, "prepareAudioChunks() FileInputStream has not been created!!!");
                return;
            } else {
                this.f1250c = this.f1251d.getResources().openRawResourceFd(this.f1249b);
                if (this.f1250c == null) {
                    throw new IllegalArgumentException("resourceId must refer to an uncompressed resource");
                }
                this.f1253f = this.f1250c.createInputStream();
            }
            int i4 = this.j.encoding == Encoding.PCM_16 ? this.j.frequency == 8000 ? 6400 : this.j.frequency == 11025 ? 8820 : this.j.frequency == 16000 ? 12800 : this.j.frequency == 22050 ? 17640 : this.j.frequency == 44100 ? 35280 : -1 : -1;
            if (i4 > 0) {
                this.f1254g = new byte[i4];
            }
            boolean z = false;
            while (!z) {
                if (this.j.encoding == Encoding.SPEEX) {
                    byte[] bArr = new byte[1];
                    byte b2 = 0;
                    while (true) {
                        try {
                            i3 = this.f1253f.read(bArr);
                        } catch (IOException e2) {
                            Logger.error(this, "prepareAudioChunks() reading header _fis.read() threw " + e2 + "!!!");
                            i3 = 0;
                        }
                        if (i3 != bArr.length) {
                            b();
                            return;
                        }
                        byte b3 = (b2 << 7) | (bArr[0] & Byte.MAX_VALUE);
                        if ((bArr[0] & 128) != 0) {
                            b2 = b3;
                        } else if (b3 <= 0) {
                            Logger.error(this, "prepareAudioChunks() Read file length error: speexBufferLen =" + b3);
                            b();
                            return;
                        } else {
                            this.f1254g = new byte[b3];
                        }
                    }
                }
                try {
                    i2 = this.f1253f.read(this.f1254g, 0, this.f1254g.length);
                } catch (IOException e3) {
                    Logger.error(this, "prepareAudioChunks() _fis.read() threw " + e3 + "!!!");
                    i2 = 0;
                }
                if (i2 == -1) {
                    z = true;
                } else {
                    byte[] bArr2 = this.f1254g;
                }
                if (i2 > 0) {
                    if (this.j.encoding == Encoding.PCM_16) {
                        short[] sArr = new short[(i2 / 2)];
                        for (int i5 = 0; i5 < i2 - 1; i5 += 2) {
                            if (this.f1252e) {
                                sArr[i5 / 2] = (short) (((this.f1254g[i5 + 1] << 8) & 65280) | (this.f1254g[i5] & 255));
                            } else {
                                sArr[i5 / 2] = (short) (((this.f1254g[i5] << 8) & 65280) | (this.f1254g[i5 + 1] & 255));
                            }
                        }
                        AudioType audioType = this.j;
                        int i6 = this.f1255h;
                        this.f1255h = i6 + 1;
                        audioChunk = new AudioChunk(audioType, sArr, (long) (i6 * 400));
                    } else {
                        byte[] bArr3 = new byte[i2];
                        System.arraycopy(this.f1254g, 0, bArr3, 0, i2);
                        audioChunk = new AudioChunk(this.j, bArr3, 100);
                    }
                    this.i.add(audioChunk);
                }
                if (z) {
                    b();
                }
            }
        } catch (FileNotFoundException e4) {
            Logger.error(this, "prepareAudioChunks() FileNotFoundException!!!");
        } catch (IOException e5) {
            Logger.error(this, "prepareAudioChunks() IOException!!!");
        }
    }

    private void b() {
        if (this.f1253f != null) {
            try {
                this.f1253f.close();
            } catch (IOException e2) {
            }
        }
        if (this.f1250c != null) {
            try {
                this.f1250c.close();
            } catch (IOException e3) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public AudioChunk getAudioChunk() {
        if (this.i.size() > 0) {
            return (AudioChunk) this.i.remove(0);
        }
        return null;
    }

    public AudioType getAudioType() {
        return this.j;
    }

    public int getChunksAvailable() {
        return this.i.size();
    }

    public boolean isActive() {
        return this.i.size() > 0;
    }

    /* access modifiers changed from: protected */
    public boolean isCodecSupported(AudioType audioType) {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean startRecordingInternal(AudioType audioType) {
        super.handleStarted();
        handleNewAudio(null);
        handleSourceClosed();
        return true;
    }

    /* access modifiers changed from: protected */
    public void stopRecordingInternal() {
    }
}
