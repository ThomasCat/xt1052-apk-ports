package com.nuance.a.a.a.c.a;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public final class c {

    /* renamed from: a reason: collision with root package name */
    public static final String f919a;

    /* renamed from: b reason: collision with root package name */
    public static final String f920b;

    /* renamed from: c reason: collision with root package name */
    public static final String f921c;

    /* renamed from: d reason: collision with root package name */
    public static final String f922d;

    /* renamed from: e reason: collision with root package name */
    public static final int f923e;

    /* renamed from: f reason: collision with root package name */
    public static final int f924f;

    /* renamed from: g reason: collision with root package name */
    public static final int f925g;

    /* renamed from: h reason: collision with root package name */
    public static final int f926h;
    public static final int i;
    public static final int j;
    public static final int k;
    public static final int l;
    private static final Constructor m;
    private static final Method n;
    private static final Method o;
    private static final Method p;
    private static final Method q;
    private static final Method r;
    private static final Method s;
    private static final Method t;
    private static final Class u;
    private static final boolean v;
    private static final Method w;
    private static final Method x;
    private Object y;
    /* access modifiers changed from: private */
    public a z;

    public interface a {
        void a();

        void b();
    }

    static {
        Method method;
        boolean z2;
        new h();
        Class a2 = h.a("android.bluetooth.BluetoothHeadset");
        Class a3 = h.a("android.bluetooth.BluetoothHeadset$ServiceListener");
        m = h.a(a2, Context.class, a3);
        Method b2 = h.b(a2, "getState", new Class[0]);
        if (b2 == null) {
            method = h.a(a2, "getState", BluetoothDevice.class);
            z2 = true;
        } else {
            method = b2;
            z2 = false;
        }
        n = method;
        v = z2;
        q = h.a(a2, "getCurrentHeadset", new Class[0]);
        s = h.a(a2, "connectHeadset", BluetoothDevice.class);
        Method b3 = h.b(a2, "disconnectHeadset", new Class[0]);
        if (b3 == null) {
            b3 = h.a(a2, "disconnectHeadset", BluetoothDevice.class);
        }
        r = b3;
        o = h.a(a2, "startVoiceRecognition", new Class[0]);
        p = h.a(a2, "stopVoiceRecognition", new Class[0]);
        t = h.a(a2, "close", new Class[0]);
        w = h.a(a2, "getPriority", BluetoothDevice.class);
        x = h.a(a2, "setPriority", BluetoothDevice.class, Integer.TYPE);
        u = a3;
        f919a = (String) h.a(a2, "ACTION_STATE_CHANGED");
        f920b = (String) h.a(a2, "ACTION_AUDIO_STATE_CHANGED");
        f921c = (String) h.a(a2, "EXTRA_STATE");
        f922d = (String) h.a(a2, "EXTRA_AUDIO_STATE");
        f923e = ((Integer) h.a(a2, "STATE_ERROR")).intValue();
        f924f = ((Integer) h.a(a2, "STATE_DISCONNECTED")).intValue();
        f925g = ((Integer) h.a(a2, "STATE_CONNECTING")).intValue();
        f926h = ((Integer) h.a(a2, "STATE_CONNECTED")).intValue();
        i = ((Integer) h.a(a2, "AUDIO_STATE_DISCONNECTED")).intValue();
        j = ((Integer) h.a(a2, "AUDIO_STATE_CONNECTED")).intValue();
        k = ((Integer) h.a(a2, "PRIORITY_OFF")).intValue();
        l = ((Integer) h.a(a2, "PRIORITY_UNDEFINED", (Object) Integer.valueOf(-1))).intValue();
    }

    public c(Context context, a aVar) {
        this.z = aVar;
        Object newProxyInstance = Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{u}, new InvocationHandler() {
            public final Object invoke(Object obj, Method method, Object[] objArr) {
                if (method.getName().equals("onServiceConnected")) {
                    if (c.this.z != null) {
                        c.this.z.a();
                    }
                } else if (method.getName().equals("onServiceDisconnected") && c.this.z != null) {
                    c.this.z.b();
                }
                return null;
            }
        });
        try {
            this.y = m.newInstance(new Object[]{context, newProxyInstance});
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final BluetoothDevice a() {
        try {
            if (q != null) {
                return (BluetoothDevice) q.invoke(this.y, new Object[0]);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public final void b() {
        try {
            t.invoke(this.y, new Object[0]);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
