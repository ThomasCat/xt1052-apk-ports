package com.motorola.audiomonitor.uis;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.uis.training.StartTrainingActivity;
import com.motorola.audiomonitor.uis.training.TrainingTriggerRecordingActivity;
import h.d;
import h.k;
import java.util.Locale;

public class SettingSwitchActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = !k.a(Locale.getDefault()) ? new Intent(this, AOVLangNotSupportedActivity.class) : !d.b(getApplicationContext()) ? new Intent(this, AOVWelcomeActivity.class) : a.i ? new Intent(this, TrainingTriggerRecordingActivity.class) : !k.a("customphrase") ? new Intent(this, StartTrainingActivity.class) : new Intent(this, SettingActivity.class);
        startActivity(intent);
        finish();
    }
}
