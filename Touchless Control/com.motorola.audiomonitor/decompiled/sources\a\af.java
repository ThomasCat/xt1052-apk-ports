package a;

import b.b;
import java.util.List;

abstract class af implements ae {

    /* renamed from: b reason: collision with root package name */
    protected b f10b;

    /* renamed from: c reason: collision with root package name */
    protected List f11c;

    /* renamed from: d reason: collision with root package name */
    protected String f12d;

    /* renamed from: e reason: collision with root package name */
    protected String f13e;

    /* renamed from: f reason: collision with root package name */
    final /* synthetic */ u f14f;

    private af(u uVar) {
        this.f14f = uVar;
    }

    /* synthetic */ af(u uVar, v vVar) {
        this(uVar);
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public b c() {
        if (this.f10b == null) {
            a();
        }
        return this.f10b;
    }

    public List d() {
        return this.f11c;
    }

    public String e() {
        return this.f12d;
    }

    public String f() {
        return this.f13e;
    }
}
