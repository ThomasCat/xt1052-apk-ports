package com.motorola.audiomonitor;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.RemoteException;
import android.util.Log;

public class au {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f315a = ("AMonitor." + au.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f316b = a.f160b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public t f317c;

    /* renamed from: d reason: collision with root package name */
    private final Context f318d;

    /* renamed from: e reason: collision with root package name */
    private ServiceConnection f319e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public final aw f320f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public w f321g = new av(this);

    public au(Context context, aw awVar) {
        this.f318d = context;
        this.f320f = awVar;
    }

    public void a() {
        if (this.f317c == null) {
            this.f319e = new ax(this, null);
            this.f318d.bindService(new Intent(this.f318d, MonitorService.class), this.f319e, 1);
        }
    }

    public boolean a(String str) {
        boolean z = false;
        if (this.f317c == null) {
            return z;
        }
        try {
            return this.f317c.a(str);
        } catch (RemoteException e2) {
            Log.e(f315a, "startTraining failure on remote");
            return z;
        }
    }

    public boolean a(boolean z) {
        boolean z2 = false;
        if (this.f317c == null) {
            return z2;
        }
        try {
            return this.f317c.a(z);
        } catch (RemoteException e2) {
            Log.e(f315a, "stopTraining failure on remote");
            return z2;
        }
    }

    public void b() {
        if (this.f317c != null) {
            if (f316b) {
                Log.d(f315a, "Disconnected from Monitor Service");
            }
            try {
                if (this.f320f != null) {
                    this.f317c.b(this.f321g);
                }
            } catch (RemoteException e2) {
                Log.e(f315a, "Failed to remove aidl listener");
            }
            this.f317c = null;
        }
        if (this.f319e != null) {
            this.f318d.unbindService(this.f319e);
            this.f319e = null;
        }
    }

    public boolean b(boolean z) {
        boolean z2 = false;
        if (this.f317c == null) {
            return z2;
        }
        try {
            return this.f317c.b(z);
        } catch (RemoteException e2) {
            Log.e(f315a, "cancelVerifyEnrollment failure on remote");
            return z2;
        }
    }

    public void c() {
        this.f318d.startService(new Intent(this.f318d, MonitorService.class));
    }

    public boolean d() {
        if (this.f317c == null) {
            return false;
        }
        try {
            this.f317c.a();
            return true;
        } catch (RemoteException e2) {
            Log.e(f315a, "Restart failure on stop");
            return false;
        }
    }

    public boolean e() {
        if (this.f317c == null) {
            return false;
        }
        try {
            this.f317c.b();
            return true;
        } catch (RemoteException e2) {
            Log.e(f315a, "Restart failure on remote");
            return false;
        }
    }

    public boolean f() {
        boolean z = false;
        if (this.f317c == null) {
            return z;
        }
        try {
            return this.f317c.d();
        } catch (RemoteException e2) {
            Log.e(f315a, "startEnrollment failure on remote");
            return z;
        }
    }

    public boolean g() {
        boolean z = false;
        if (this.f317c == null) {
            return z;
        }
        try {
            return this.f317c.e();
        } catch (RemoteException e2) {
            Log.e(f315a, "completeEnrollment failure on remote");
            return z;
        }
    }

    public boolean h() {
        boolean z = false;
        if (this.f317c == null) {
            return z;
        }
        try {
            return this.f317c.f();
        } catch (RemoteException e2) {
            Log.e(f315a, "verifyEnrollment failure on remote");
            return z;
        }
    }

    public boolean i() {
        boolean z = false;
        if (this.f317c == null) {
            return z;
        }
        try {
            return this.f317c.g();
        } catch (RemoteException e2) {
            Log.e(f315a, "requestCurrentNoiseEstimate failure on remote");
            return z;
        }
    }
}
