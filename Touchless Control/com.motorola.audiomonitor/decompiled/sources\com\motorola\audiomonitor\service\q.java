package com.motorola.audiomonitor.service;

import java.util.concurrent.atomic.AtomicBoolean;

public class q implements o {

    /* renamed from: a reason: collision with root package name */
    private AtomicBoolean f589a = new AtomicBoolean(false);

    /* renamed from: b reason: collision with root package name */
    private final String f590b;

    /* renamed from: c reason: collision with root package name */
    private final String f591c;

    public q(String str, String str2) {
        this.f590b = str;
        this.f591c = str2;
    }

    public void a() {
    }

    public void a(int i, int i2, short[] sArr) {
    }

    public void a(String str, String str2) {
        if (this.f590b.equals(str) && this.f591c.equals(str2)) {
            this.f589a.set(true);
            synchronized (this) {
                notifyAll();
            }
        }
    }

    public boolean b() {
        if (!this.f589a.get()) {
            wait();
        }
        return true;
    }
}
