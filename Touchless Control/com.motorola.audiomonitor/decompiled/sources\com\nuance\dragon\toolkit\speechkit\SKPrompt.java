package com.nuance.dragon.toolkit.speechkit;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.os.Handler;
import android.os.Vibrator;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.sinks.PlayerSink;
import com.nuance.dragon.toolkit.audio.sinks.PlayerSink.Listener;
import com.nuance.dragon.toolkit.audio.sinks.SpeakerPlayerSink;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.vocalizer.TtsAudioSource;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer;
import java.util.LinkedList;

public abstract class SKPrompt {

    /* renamed from: a reason: collision with root package name */
    private a f2070a;

    public class Composite extends SKPrompt {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final LinkedList f2071a = new LinkedList();
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public SKPrompt f2072b;

        /* access modifiers changed from: private */
        public void b() {
            this.f2072b = (SKPrompt) this.f2071a.remove();
            this.f2072b.a(new a() {
                public final void a(SKPrompt sKPrompt) {
                    Composite.this.f2072b = null;
                    if (Composite.this.f2071a.isEmpty()) {
                        Composite.this.done();
                    } else {
                        Composite.this.b();
                    }
                }
            });
        }

        public void addPrompt(SKPrompt sKPrompt) {
            this.f2071a.add(sKPrompt);
        }

        /* access modifiers changed from: protected */
        public void onCancel() {
            this.f2071a.clear();
            if (this.f2072b != null) {
                this.f2072b.a();
                this.f2072b = null;
            }
        }

        /* access modifiers changed from: protected */
        public void onStart() {
            if (this.f2071a.isEmpty()) {
                Logger.warn(this, "Composite prompt is empty");
                done();
                return;
            }
            b();
        }
    }

    public class MediaAudio extends SKPrompt {
        public MediaAudio(AssetFileDescriptor assetFileDescriptor) {
        }

        /* access modifiers changed from: protected */
        public void onCancel() {
        }

        /* access modifiers changed from: protected */
        public void onStart() {
            Logger.error(this, "Audio prompts not implemented");
            done();
        }
    }

    public class SourceAudio extends SKPrompt {

        /* renamed from: a reason: collision with root package name */
        private final AudioSource f2074a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public PlayerSink f2075b;

        public SourceAudio(AudioSource audioSource) {
            this.f2074a = audioSource;
            this.f2075b.connectAudioSource(this.f2074a);
        }

        /* access modifiers changed from: protected */
        public void onCancel() {
            if (this.f2075b != null) {
                this.f2075b.disconnectAudioSource();
                this.f2075b = null;
            }
        }

        /* access modifiers changed from: protected */
        public void onStart() {
            this.f2075b = new SpeakerPlayerSink(this.f2074a.getAudioType());
            this.f2075b.startPlaying(new Listener() {
                public final void onStarted(PlayerSink playerSink) {
                }

                public final void onStopped(PlayerSink playerSink) {
                    SourceAudio.this.f2075b = null;
                    SourceAudio.this.done();
                }
            });
        }
    }

    public class Vibrate extends SKPrompt {

        /* renamed from: a reason: collision with root package name */
        private Vibrator f2077a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public Runnable f2078b;
        /* access modifiers changed from: private */

        /* renamed from: c reason: collision with root package name */
        public Handler f2079c;

        /* renamed from: d reason: collision with root package name */
        private final int f2080d;

        public Vibrate(Context context, int i) {
            this.f2077a = (Vibrator) context.getSystemService("vibrator");
            this.f2080d = i;
        }

        /* access modifiers changed from: protected */
        public void onCancel() {
            if (this.f2077a != null) {
                this.f2077a.cancel();
                this.f2077a = null;
            }
            if (this.f2079c != null) {
                this.f2079c.removeCallbacks(this.f2078b);
                this.f2079c = null;
                this.f2078b = null;
            }
        }

        /* access modifiers changed from: protected */
        public void onStart() {
            if (this.f2077a == null) {
                Logger.error(this, "Unable to get vibrator service");
                done();
                return;
            }
            this.f2077a.vibrate((long) this.f2080d);
            this.f2079c = new Handler();
            this.f2078b = new Runnable() {
                public final void run() {
                    Vibrate.this.f2079c = null;
                    Vibrate.this.f2078b = null;
                    Vibrate.this.done();
                }
            };
            this.f2079c.postDelayed(this.f2078b, (long) this.f2080d);
        }
    }

    interface a {
        void a(SKPrompt sKPrompt);
    }

    final class b extends SKPrompt {

        /* renamed from: a reason: collision with root package name */
        private final Vocalizer f2082a;

        /* renamed from: b reason: collision with root package name */
        private final String f2083b;
        /* access modifiers changed from: private */

        /* renamed from: c reason: collision with root package name */
        public PlayerSink f2084c;

        b(Vocalizer vocalizer, String str) {
            this.f2082a = vocalizer;
            this.f2083b = str;
        }

        /* access modifiers changed from: protected */
        public final void onCancel() {
            if (this.f2084c != null) {
                this.f2084c.disconnectAudioSource();
                this.f2084c = null;
            }
        }

        /* access modifiers changed from: protected */
        public final void onStart() {
            TtsAudioSource generateTts = this.f2082a.generateTts(this.f2083b, null, null);
            this.f2084c = new SpeakerPlayerSink(generateTts.getAudioType());
            this.f2084c.connectAudioSource(generateTts);
            this.f2084c.startPlaying(new Listener() {
                public final void onStarted(PlayerSink playerSink) {
                }

                public final void onStopped(PlayerSink playerSink) {
                    b.this.f2084c = null;
                    b.this.done();
                }
            });
        }
    }

    SKPrompt() {
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        this.f2070a = null;
        onCancel();
    }

    /* access modifiers changed from: 0000 */
    public final void a(a aVar) {
        a aVar2 = this.f2070a;
        this.f2070a = aVar;
        onStart();
    }

    /* access modifiers changed from: protected */
    public final void done() {
        if (this.f2070a != null) {
            this.f2070a.a(this);
            this.f2070a = null;
        }
    }

    /* access modifiers changed from: protected */
    public abstract void onCancel();

    /* access modifiers changed from: protected */
    public abstract void onStart();
}
