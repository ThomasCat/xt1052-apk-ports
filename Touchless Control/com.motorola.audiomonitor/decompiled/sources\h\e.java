package h;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class e {

    /* renamed from: a reason: collision with root package name */
    private static final String f2283a = ("AMonitor." + e.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private final Context f2284b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final Runnable f2285c;

    /* renamed from: d reason: collision with root package name */
    private SensorEventListener f2286d;

    public e(Context context, Runnable runnable) {
        this.f2284b = context;
        this.f2285c = runnable;
    }

    private static int a(String str, int i) {
        try {
            return ((Integer) Sensor.class.getField(str).get(null)).intValue();
        } catch (Exception e2) {
            Log.e(f2283a, "Failed to get sensor for " + str + ":" + e2);
            return i;
        }
    }

    public void a() {
        if (this.f2286d != null) {
            b();
        }
        SensorManager sensorManager = (SensorManager) this.f2284b.getSystemService("sensor");
        int a2 = a("TYPE_FLAT_UP", -1);
        if (a2 != -1) {
            Sensor defaultSensor = sensorManager.getDefaultSensor(a2);
            Sensor defaultSensor2 = sensorManager.getDefaultSensor(1);
            this.f2286d = new f(this, defaultSensor, defaultSensor2);
            sensorManager.registerListener(this.f2286d, defaultSensor2, 3);
            sensorManager.registerListener(this.f2286d, defaultSensor, 3);
        }
    }

    public void b() {
        if (this.f2286d != null) {
            ((SensorManager) this.f2284b.getSystemService("sensor")).unregisterListener(this.f2286d);
            this.f2286d = null;
        }
    }
}
