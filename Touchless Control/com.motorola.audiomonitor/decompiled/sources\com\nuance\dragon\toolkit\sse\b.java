package com.nuance.dragon.toolkit.sse;

import android.content.Context;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.MultiChannelAudioChunk;
import com.nuance.dragon.toolkit.audio.pipes.SseTuningType;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.util.Logger;
import java.io.FileInputStream;
import java.io.IOException;

public final class b {

    /* renamed from: a reason: collision with root package name */
    private final a f2103a;

    /* renamed from: b reason: collision with root package name */
    private boolean f2104b = false;

    /* renamed from: c reason: collision with root package name */
    private boolean f2105c = false;

    /* renamed from: d reason: collision with root package name */
    private boolean f2106d = false;

    public b(Context context) {
        this.f2103a = new NativeSseImpl(context);
    }

    public final AudioChunk a(AudioChunk audioChunk) {
        short[] sArr = new short[audioChunk.audioShorts.length];
        System.arraycopy(audioChunk.audioShorts, 0, sArr, 0, sArr.length);
        short[] a2 = this.f2103a.a(sArr, null);
        if (a2 != null) {
            return new AudioChunk(audioChunk.audioType, a2, audioChunk.audioTimestamp);
        }
        return null;
    }

    public final AudioChunk a(MultiChannelAudioChunk multiChannelAudioChunk) {
        short[] sArr;
        short[] sArr2;
        if (multiChannelAudioChunk.channelCount != 1) {
            int i = multiChannelAudioChunk.channelCount;
        }
        if (multiChannelAudioChunk.channelCount == 1) {
            sArr = new short[multiChannelAudioChunk.audioShorts.length];
            sArr2 = null;
        } else {
            sArr = new short[(multiChannelAudioChunk.audioShorts.length / 2)];
            sArr2 = new short[(multiChannelAudioChunk.audioShorts.length / 2)];
            System.arraycopy(multiChannelAudioChunk.audioShorts, sArr.length, sArr2, 0, sArr2.length);
        }
        System.arraycopy(multiChannelAudioChunk.audioShorts, 0, sArr, 0, sArr.length);
        short[] a2 = this.f2103a.a(sArr, sArr2);
        if (a2 != null) {
            return new AudioChunk(multiChannelAudioChunk.audioType, a2, multiChannelAudioChunk.audioTimestamp);
        }
        return null;
    }

    public final void a() {
        this.f2103a.b();
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0089  */
    public final boolean a(AudioType audioType, FileManager fileManager, String str, SseTuningType sseTuningType) {
        byte[] bArr;
        if (!(fileManager == null || str == null)) {
            FileInputStream openFileForReading = fileManager.openFileForReading(str);
            if (openFileForReading != null) {
                int size = fileManager.getSize(str);
                if (size > 0) {
                    bArr = new byte[size];
                    try {
                        if (openFileForReading.read(bArr) != size) {
                            throw new IOException(".bsd file not read fully!");
                        }
                    } catch (IOException e2) {
                        Logger.error(this, ".bsd file:" + str + " could not be read. e:" + e2);
                        bArr = null;
                    }
                    switch (sseTuningType) {
                        case NONE:
                            this.f2104b = false;
                            this.f2105c = false;
                            this.f2106d = false;
                            break;
                        case ECHO_CANCEL:
                            this.f2104b = true;
                            break;
                        case NOISE_SUPPRESS:
                            this.f2105c = true;
                            break;
                        case BEAM_FORMING:
                            this.f2106d = true;
                            break;
                        case ECHO_AND_NOISE:
                            this.f2104b = true;
                            this.f2105c = true;
                            break;
                        case ECHO_AND_BEAM:
                            this.f2104b = true;
                            this.f2106d = true;
                            break;
                        case NOISE_AND_BEAM:
                            this.f2105c = true;
                            this.f2106d = true;
                            break;
                        case ECHO_NOISE_BEAM:
                            this.f2104b = true;
                            this.f2105c = true;
                            this.f2106d = true;
                            break;
                        default:
                            this.f2104b = true;
                            this.f2105c = true;
                            break;
                    }
                    return !this.f2103a.a() && this.f2103a.a(audioType.frequency, bArr, this.f2104b, this.f2105c, this.f2106d);
                }
            }
        }
        bArr = null;
        switch (sseTuningType) {
            case NONE:
                break;
            case ECHO_CANCEL:
                break;
            case NOISE_SUPPRESS:
                break;
            case BEAM_FORMING:
                break;
            case ECHO_AND_NOISE:
                break;
            case ECHO_AND_BEAM:
                break;
            case NOISE_AND_BEAM:
                break;
            case ECHO_NOISE_BEAM:
                break;
        }
        if (!this.f2103a.a()) {
        }
    }
}
