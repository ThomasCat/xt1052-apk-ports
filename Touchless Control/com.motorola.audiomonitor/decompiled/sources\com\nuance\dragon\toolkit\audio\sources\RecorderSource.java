package com.nuance.dragon.toolkit.audio.sources;

import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.util.WorkerThread;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.LinkedList;
import java.util.List;

public abstract class RecorderSource extends SingleSinkSource {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final AudioType f1277a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final List f1278b = new LinkedList();
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public WorkerThread f1279c;

    /* renamed from: d reason: collision with root package name */
    private Handler f1280d;

    /* renamed from: e reason: collision with root package name */
    private final Handler f1281e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public Listener f1282f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public boolean f1283g;

    /* renamed from: h reason: collision with root package name */
    private boolean f1284h;
    /* access modifiers changed from: private */
    public boolean i;
    private boolean j;

    public interface Listener {
        void onStarted(RecorderSource recorderSource);

        void onStopped(RecorderSource recorderSource);
    }

    public RecorderSource(AudioType audioType, Handler handler) {
        d.a("audioType", (Object) audioType);
        d.a("audioType", "a type supported by this player", isCodecSupported(audioType));
        this.f1277a = audioType;
        if (handler == null) {
            this.j = true;
            this.f1279c = null;
        } else {
            this.f1279c = null;
            this.f1280d = handler;
        }
        this.f1281e = new Handler();
        this.i = false;
    }

    /* access modifiers changed from: protected */
    public AudioChunk getAudioChunk() {
        if (!this.f1278b.isEmpty()) {
            return (AudioChunk) this.f1278b.remove(0);
        }
        return null;
    }

    public AudioType getAudioType() {
        return this.f1277a;
    }

    public int getChunksAvailable() {
        return this.f1278b.size();
    }

    /* access modifiers changed from: protected */
    public void handleFramesDropped() {
        this.f1281e.post(new Runnable() {
            public final void run() {
                RecorderSource.this.notifyFramesDropped();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void handleNewAudio(final AudioChunk audioChunk) {
        this.f1281e.post(new Runnable() {
            public final void run() {
                RecorderSource.this.f1278b.add(audioChunk);
                RecorderSource.this.notifyChunksAvailable();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void handleSourceClosed() {
        this.f1281e.post(new Runnable() {
            public final void run() {
                if (RecorderSource.this.f1283g) {
                    RecorderSource.this.f1283g = false;
                }
                if (!RecorderSource.this.i) {
                    RecorderSource.this.i = true;
                    RecorderSource.this.notifySourceClosed();
                    if (RecorderSource.this.f1279c != null) {
                        RecorderSource.this.f1279c.stop();
                        RecorderSource.this.f1279c = null;
                    }
                }
                if (RecorderSource.this.f1282f != null) {
                    RecorderSource.this.f1282f.onStopped(RecorderSource.this);
                    RecorderSource.this.f1282f = null;
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void handleStarted() {
        this.f1281e.post(new Runnable() {
            public final void run() {
                if (RecorderSource.this.f1282f != null) {
                    RecorderSource.this.f1282f.onStarted(RecorderSource.this);
                }
            }
        });
    }

    public boolean isActive() {
        return !this.i;
    }

    /* access modifiers changed from: protected */
    public abstract boolean isCodecSupported(AudioType audioType);

    public void startRecording() {
        startRecording(null);
    }

    public void startRecording(Listener listener) {
        d.a((Object) this, !this.f1283g, "Already started");
        if (this.j) {
            this.f1279c = new WorkerThread();
            this.f1279c.start();
            this.f1280d = this.f1279c.getHandler();
        }
        this.f1282f = listener;
        this.f1283g = true;
        this.f1284h = false;
        this.i = false;
        this.f1280d.post(new Runnable() {
            public final void run() {
                if (!RecorderSource.this.startRecordingInternal(RecorderSource.this.f1277a)) {
                    RecorderSource.this.handleSourceClosed();
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public abstract boolean startRecordingInternal(AudioType audioType);

    public void stopRecording() {
        if (this.f1283g && !this.f1284h) {
            this.f1284h = true;
            this.f1280d.post(new Runnable() {
                public final void run() {
                    RecorderSource.this.stopRecordingInternal();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public abstract void stopRecordingInternal();
}
