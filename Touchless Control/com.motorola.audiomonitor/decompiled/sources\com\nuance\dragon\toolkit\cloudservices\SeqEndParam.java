package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.dragon.toolkit.data.Data.Dictionary;

public class SeqEndParam extends DictionaryParam {
    public SeqEndParam(String str, Dictionary dictionary) {
        super(10, str, dictionary);
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(d dVar, a aVar) {
        dVar.c(this.f1355a, this._dict);
        return true;
    }
}
