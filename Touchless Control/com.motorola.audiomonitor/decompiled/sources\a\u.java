package a;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;
import android.util.Log;
import b.b;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.actions.utils.r;
import com.motorola.audiomonitor.actions.utils.v;
import com.motorola.audiomonitor.pluginservice.h;
import com.nuance.dragon.toolkit.recognition.CloudInterpreter;
import e.e;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class u extends HandlerThread implements e {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f70a = ("AMonitor." + u.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f71b = a.f160b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final d f72c;

    /* renamed from: d reason: collision with root package name */
    private final v f73d;

    /* renamed from: e reason: collision with root package name */
    private final h f74e;

    /* renamed from: f reason: collision with root package name */
    private final Handler f75f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public final Object f76g;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public List f77h;
    /* access modifiers changed from: private */
    public ac i = ac.NONE;
    /* access modifiers changed from: private */
    public b j;
    /* access modifiers changed from: private */
    public final f.b k;
    /* access modifiers changed from: private */
    public final r l;
    /* access modifiers changed from: private */
    public final d.a m;
    /* access modifiers changed from: private */
    public f.b n;
    /* access modifiers changed from: private */
    public final Context o;

    public u(Context context, d dVar, v vVar, h hVar) {
        super(u.class.getSimpleName());
        this.o = context;
        start();
        this.f75f = new Handler(getLooper());
        this.f76g = new Object();
        this.f72c = dVar;
        this.f73d = vVar;
        this.f74e = hVar;
        this.k = new f.b(context, 16000);
        this.l = new r(context, this.k);
        if (TextUtils.equals("en_US", a.n)) {
            this.m = new d.a(context);
        } else {
            this.m = null;
        }
    }

    private void a(String str, String str2, String str3) {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("strategy", str);
            jSONObject2.put("command", str2);
            jSONObject2.put("contact", str3);
            jSONObject.put("resolvers", jSONObject2);
        } catch (JSONException e2) {
            Log.e(f70a, "notifySubInfo", e2);
        }
        this.f74e.f549d.a(jSONObject.toString());
    }

    private void a(List list) {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        try {
            jSONObject.put("contacts", jSONArray);
            if (list == null || list.size() <= 0) {
                this.f74e.f549d.a("contact: not found");
            } else {
                this.f74e.f549d.a("contact:" + ((com.motorola.audiomonitor.actions.utils.a) list.get(0)).c() + "(" + ((com.motorola.audiomonitor.actions.utils.a) list.get(0)).g() + ")");
                for (int i2 = 0; i2 < list.size(); i2++) {
                    com.motorola.audiomonitor.actions.utils.a aVar = (com.motorola.audiomonitor.actions.utils.a) list.get(i2);
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("name", aVar.c());
                    jSONObject2.put("number", aVar.g());
                    jSONObject2.put("phone_type", aVar.e());
                    jSONObject2.put(CloudInterpreter.KEY_SCORE, (double) aVar.d());
                    jSONObject2.put("probability", (double) aVar.h());
                    jSONObject2.put("confidence", (double) aVar.i());
                    jSONArray.put(jSONObject2);
                }
            }
            this.f74e.f549d.a(jSONObject.toString());
        } catch (JSONException e2) {
            Log.e(f70a, "notifyContactInfo", e2);
        }
    }

    /* access modifiers changed from: private */
    public boolean a(b bVar) {
        return bVar != null && bVar.a("command").equals("call");
    }

    private boolean a(b bVar, List list, String str, String str2, String str3) {
        if (this.f74e == null || this.f74e.f549d == null || this.f74e.f549d.a() == null || !this.f74e.f549d.a().startsWith("SttTest")) {
            return false;
        }
        a(str, str2, str3);
        if (bVar != null) {
            this.f74e.f549d.a("parser:" + bVar.a("command"));
            if (bVar.a("command").equals("call")) {
                a(list);
            }
        } else {
            this.f74e.f549d.a("parser:RECOGNITION FAILURE");
        }
        return true;
    }

    private ae e() {
        return new z(this, null);
    }

    /* access modifiers changed from: private */
    public void f() {
        com.motorola.audiomonitor.pluginservice.a aVar = null;
        if (this.i != ac.NONE) {
            ae e2 = e();
            b c2 = e2.c();
            if (a(c2, e2.d(), e2.b(), e2.e(), e2.f())) {
                this.f72c.a((e) null);
                a();
                return;
            }
            List list = this.f77h;
            v vVar = this.f73d;
            List d2 = e2.d();
            if (this.f74e != null) {
                aVar = this.f74e.f549d;
            }
            this.f72c.a(new e(list, c2, vVar, d2, aVar));
            a();
        }
    }

    public void a() {
        this.f75f.post(new x(this));
    }

    public void a(List list, int i2, float f2) {
        this.f75f.post(new w(this, list, f2, i2));
    }

    public void a(boolean z) {
        this.f75f.post(new v(this, z));
    }

    public void b() {
        this.f75f.removeCallbacksAndMessages(null);
        synchronized (this.f76g) {
            this.f75f.post(new y(this));
            try {
                this.f76g.wait();
            } catch (InterruptedException e2) {
            }
        }
        quit();
    }
}
