package com.nuance.a.a.a.b.c.b.b;

import com.nuance.a.a.a.a.a.a.C0000a;
import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.b.c.c.c;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.Frequencies;
import java.util.Enumeration;

public class p extends l {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f874a = a.a(p.class);

    public p(String str, String str2, String str3, String str4, String str5, String str6, C0000a aVar, String str7, short s, short s2, String str8, String str9, String str10, String str11, byte[] bArr, String str12, String str13, c cVar) {
        super(514);
        if (f874a.b()) {
            f874a.b((Object) "PDXQueryBegin()");
        }
        a("uid", str, 193);
        a("pdx_version", str2, 193);
        a("client_version", str3, 193);
        a("script_version", str4, 193);
        a("language", str5, 193);
        a("region", str6, 193);
        a("device_codec", (int) aVar.a());
        a("dictation_language", str7, 193);
        a("lcd_width", (int) s);
        a("lcd_height", (int) s2);
        if (str8 == null) {
            a("carrier", new byte[0], 5);
        } else {
            a("carrier", str8, 193);
        }
        a("phone_model", str9, 193);
        a("phone_number", str10, 193);
        a("original_session_id", str11, 22);
        if (bArr != null) {
            a("new_session_id", bArr, 22);
        }
        a("application", str12, 22);
        a("nmaid", str12, 22);
        a("command", str13, 22);
        if (cVar != null) {
            Enumeration a2 = cVar.a();
            while (a2.hasMoreElements()) {
                String str14 = (String) a2.nextElement();
                switch (((i) cVar).b(str14).c()) {
                    case 4:
                        a(str14, cVar.e(str14), 4);
                        break;
                    case 5:
                        a(str14, (byte[]) null, 5);
                        break;
                    case 16:
                        f874a.e("PDXQueryBegin() Sequence not accepted in optionalKeys");
                        break;
                    case Frequencies.FREQ_22KHZ /*22*/:
                        a(str14, cVar.g(str14), 22);
                        break;
                    case 192:
                        a(str14, cVar.d(str14));
                        break;
                    case 193:
                        a(str14, cVar.f(str14), 193);
                        break;
                    case 224:
                        f874a.e("PDXQueryBegin() Dictionary not accepted in optionalKeys");
                        break;
                }
            }
        }
    }
}
