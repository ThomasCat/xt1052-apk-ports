package com.nuance.dragon.toolkit.audio.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import com.nuance.dragon.toolkit.audio.bluetooth.impl.Bluetooth;
import com.nuance.dragon.toolkit.audio.bluetooth.impl.Bluetooth.HeadsetStateListener;

public class BluetoothManager implements HeadsetStateListener {
    private static final BluetoothManager j = new BluetoothManager();

    /* renamed from: a reason: collision with root package name */
    private int f1006a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public Bluetooth f1007b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public BluetoothListener f1008c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public boolean f1009d;

    /* renamed from: e reason: collision with root package name */
    private boolean f1010e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public int f1011f = 0;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public Handler f1012g;

    /* renamed from: h reason: collision with root package name */
    private Runnable f1013h = new Runnable() {
        public final void run() {
            if (BluetoothManager.this.f1009d) {
                BluetoothManager.this.f1007b.stopBluetoothSco();
                BluetoothManager.this.f1011f = 0;
                BluetoothManager.this.b(1);
            }
        }
    };
    private Runnable i = new Runnable() {
        public final void run() {
            BluetoothManager.this.f1011f = 0;
            BluetoothManager.this.a(1);
        }
    };
    private Runnable k = new Runnable() {
        public final void run() {
            if (BluetoothManager.this.f1008c != null) {
                BluetoothManager.this.f1008c.onBluetoothReleased(0);
            }
            BluetoothManager bluetoothManager = BluetoothManager.this;
            BluetoothManager.this.f1008c;
            bluetoothManager.f1008c = null;
            BluetoothManager.this.f1007b = null;
            BluetoothManager.this.f1012g = null;
        }
    };

    public class Status {
        public static final int ABORTED_BY_CALLER = 4;
        public static final int DEVICE_NOT_CONNECTED = 3;
        public static final int SUCCESS = 0;
        public static final int TIMEOUT = 1;
        public static final int UNSPECIFIED_REASON = 2;

        public Status() {
        }
    }

    private BluetoothManager() {
    }

    private void a() {
        if (!this.f1009d) {
            throw new IllegalStateException("Attempt at using uninitialized BluetoothManager");
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        if (this.f1008c != null) {
            this.f1008c.onAudioDisconnected(i2);
        }
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        if (this.f1008c != null) {
            this.f1008c.onAudioConnected(i2);
        }
    }

    public static BluetoothManager getInstance() {
        return j;
    }

    public void connectAudio() {
        connectAudio(5);
    }

    public void connectAudio(int i2) {
        Log.d("DMT-BluetoothManager", "connectAudio()");
        a();
        if (i2 <= 0) {
            throw new IllegalArgumentException("Connection timeout cannot be 0 or negative");
        }
        this.f1006a = i2;
        boolean isDeviceConnected = isDeviceConnected();
        Log.d("DMT-BluetoothManager", "connectAudio(), deviceConnected = <" + isDeviceConnected + ">, audioConnected = <" + isAudioConnected() + ">");
        if (!isDeviceConnected) {
            b(3);
            return;
        }
        switch (this.f1011f) {
            case 0:
                this.f1011f = 1;
                if (this.f1007b.startBluetoothSco()) {
                    this.f1012g.postDelayed(this.f1013h, (long) (this.f1006a * 1000));
                    return;
                } else {
                    this.f1012g.post(this.f1013h);
                    return;
                }
            case 1:
                throw new IllegalStateException("connectAudio() called twice");
            case 2:
                throw new IllegalStateException("connectAudio() called twice, already connected");
            case 3:
                throw new IllegalStateException("connectAudio() should not be called until the disconnectAudio() has been acknowledged");
            default:
                throw new IllegalStateException("connectAudio() - illegal audio state, mAudioState = " + this.f1011f);
        }
    }

    public void disconnectAudio() {
        Log.d("DMT-BluetoothManager", "disconnectAudio()");
        a();
        switch (this.f1011f) {
            case 0:
                throw new IllegalStateException("disconnectAudio() - audio already disconnected");
            case 1:
                b(4);
                this.f1011f = 3;
                break;
            case 2:
                this.f1011f = 3;
                break;
            case 3:
                throw new IllegalStateException("calling disconnectAudio() twice");
            default:
                throw new IllegalStateException("disconnectAudio() - illegal audio state, mAudioState = " + this.f1011f);
        }
        this.f1007b.stopBluetoothSco();
        this.f1012g.postDelayed(this.i, 1000);
    }

    public int getPlaybackStream() {
        Log.d("DMT-BluetoothManager", "getPlaybackStream(), mBluetoothImpl = " + this.f1007b);
        a();
        return this.f1007b.getPlaybackStream();
    }

    public void initialize(Context context, BluetoothListener bluetoothListener) {
        Log.d("DMT-BluetoothManager", "init(), context = " + context);
        if (context == null) {
            throw new IllegalArgumentException("The context argument is null");
        } else if (bluetoothListener == null) {
            throw new IllegalArgumentException("The listener argument is null");
        } else if (this.f1009d) {
            throw new IllegalStateException("Attempt at double-initialization of Bluetooth manager, it has been already initialized");
        } else {
            this.f1008c = bluetoothListener;
            this.f1012g = new Handler();
            BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
            this.f1010e = defaultAdapter != null ? defaultAdapter.isEnabled() : false;
            if (this.f1010e) {
                this.f1007b = Bluetooth.createInstance(context);
                this.f1007b.setHeadsetStateListener(this);
            } else {
                this.f1012g.post(new Runnable() {
                    public final void run() {
                        BluetoothManager.this.onServiceConnected(false);
                    }
                });
            }
            this.f1011f = 0;
            Log.d("DMT-BluetoothManager", "init(), returning mBluetoothImpl = " + this.f1007b);
        }
    }

    public boolean isAudioConnected() {
        boolean z = true;
        Log.d("DMT-BluetoothManager", "isAudioConnected(), mBluetoothImpl = " + this.f1007b);
        a();
        if (this.f1007b.getAudioState() != 1) {
            z = false;
        }
        Log.d("DMT-BluetoothManager", "isAudioConnected() returning " + z);
        return z;
    }

    public boolean isDeviceConnected() {
        boolean z = true;
        a();
        if (this.f1007b.getHeadsetState() != 1) {
            z = false;
        }
        Log.d("DMT-BluetoothManager", "isDeviceConnected() returning " + z);
        return z;
    }

    public void onAudioStateChanged(int i2) {
        Log.d("DMT-BluetoothManager", "onAudioStateChanged(), state = " + i2 + ", mListener = " + this.f1008c);
        if (i2 == 1) {
            this.f1012g.removeCallbacks(this.f1013h);
            switch (this.f1011f) {
                case 0:
                case 2:
                case 3:
                    Log.w("DMT-BluetoothManager", "onAudioStateChanged(), possible state error, mAudioState = " + this.f1011f);
                    break;
                case 1:
                    this.f1011f = 2;
                    b(0);
                    break;
                default:
                    throw new IllegalStateException("onAudioConnected() - illegal audio state, mAudioState = " + this.f1011f);
            }
        }
        if (i2 == 0) {
            this.f1012g.removeCallbacks(this.i);
            switch (this.f1011f) {
                case 0:
                    return;
                case 1:
                case 2:
                    this.f1007b.stopBluetoothSco();
                    this.f1011f = 0;
                    a(2);
                    return;
                case 3:
                    this.f1011f = 0;
                    a(0);
                    return;
                default:
                    throw new IllegalStateException("onAudioDisconnected() - illegal audio state, mAudioState = " + this.f1011f);
            }
        }
    }

    public void onConnectionStateChanged(int i2) {
        Log.d("DMT-BluetoothManager", "onConnectionStateChanged(), state = " + i2);
    }

    public void onServiceConnected(boolean z) {
        if (z) {
            this.f1009d = true;
            if (this.f1008c != null) {
                this.f1008c.onBluetoothInitialized(0);
                return;
            }
            return;
        }
        this.f1009d = false;
        if (this.f1008c != null) {
            this.f1008c.onBluetoothInitialized(2);
        }
    }

    public void onServiceDisconnected() {
        this.f1009d = false;
        if (this.f1008c != null) {
            this.f1008c.onBluetoothReleased(0);
        }
    }

    public void release() {
        Log.d("DMT-BluetoothManager", "release()");
        if (this.f1009d) {
            this.f1009d = false;
            if (this.f1011f == 2 || this.f1011f == 1) {
                Log.w("DMT-BluetoothManager", "release(), disconnecting audio: recommended to disconnect BT audio before calling release()");
                this.f1007b.stopBluetoothSco();
            }
            this.f1011f = 0;
            this.f1007b.close();
            this.f1012g.postDelayed(this.k, 500);
        } else if (this.f1012g == null || this.f1010e) {
            throw new IllegalStateException("release() - BluetoothManager is not active.");
        } else {
            this.f1012g.post(this.k);
        }
    }
}
