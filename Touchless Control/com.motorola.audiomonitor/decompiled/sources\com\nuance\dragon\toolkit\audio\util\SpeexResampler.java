package com.nuance.dragon.toolkit.audio.util;

import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.d;

public class SpeexResampler {

    /* renamed from: a reason: collision with root package name */
    private long f1317a;

    /* renamed from: b reason: collision with root package name */
    private short[] f1318b;

    /* renamed from: c reason: collision with root package name */
    private AudioType f1319c;

    static {
        System.loadLibrary("dmt_speex");
    }

    private static native long initializeNative(int[] iArr, int i, int i2, int i3, int i4);

    private static native void releaseNative(long j);

    private static native int resampleNative(long j, int i, short[] sArr, int[] iArr, short[] sArr2, int[] iArr2);

    public final void a() {
        if (this.f1317a != 0) {
            releaseNative(this.f1317a);
            this.f1317a = 0;
        }
    }

    public final void a(AudioType audioType, AudioType audioType2) {
        d.a("inputAudioType", "PCM", audioType.encoding == Encoding.PCM_16);
        d.a("outputAudioType", "PCM", audioType2.encoding == Encoding.PCM_16);
        d.a("channels", "greater than 1", true);
        d.a("quality", "greater than 0", true);
        if (this.f1317a != 0) {
            releaseNative(this.f1317a);
        }
        int[] iArr = new int[1];
        this.f1317a = initializeNative(iArr, 3, 1, audioType.frequency, audioType2.frequency);
        if (iArr[0] != 0) {
            Logger.error(this, "Failed to initialize Speex resampler. Error code = " + iArr[0]);
        }
        this.f1319c = audioType2;
    }

    public final AudioChunk[] a(AudioChunk audioChunk) {
        d.a("raw", (Object) audioChunk);
        int length = audioChunk.audioShorts.length * 2;
        if (this.f1318b == null || this.f1318b.length < length) {
            this.f1318b = new short[length];
        }
        int[] iArr = {this.f1318b.length};
        resampleNative(this.f1317a, 0, audioChunk.audioShorts, new int[]{audioChunk.audioShorts.length}, this.f1318b, iArr);
        short[] sArr = new short[iArr[0]];
        System.arraycopy(this.f1318b, 0, sArr, 0, sArr.length);
        return new AudioChunk[]{new AudioChunk(this.f1319c, sArr)};
    }
}
