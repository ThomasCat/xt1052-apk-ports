package com.motorola.audiomonitor;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.util.Log;
import com.motorola.audiomonitor.actions.utils.UsageAlarmReceiver;
import com.motorola.audiomonitor.uis.BtStubActivity;
import h.a;
import h.d;
import h.k;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

public class MonitorApplication extends Application {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f144a = ("AMonitor." + MonitorApplication.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f145b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private static boolean f146c = false;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public static final AtomicBoolean f147d = new AtomicBoolean();

    /* renamed from: e reason: collision with root package name */
    private Configuration f148e;

    /* access modifiers changed from: private */
    public void a(Context context) {
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, new Intent(context, UsageAlarmReceiver.class), 134217728);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        long b2 = k.b(context, "last_usage_report", 0);
        if (b2 <= 0) {
            b2 = System.currentTimeMillis();
            k.a(context, "last_usage_report", b2);
        }
        alarmManager.setRepeating(1, b2 + 86400000, 86400000, broadcast);
    }

    private void a(String str) {
        Intent intent = new Intent(this, MonitorService.class);
        intent.putExtra("MONITOR_STATE", str);
        startService(intent);
    }

    public static boolean a() {
        return f147d.get();
    }

    public void onConfigurationChanged(Configuration configuration) {
        Configuration configuration2 = this.f148e;
        this.f148e = new Configuration(configuration);
        if (configuration2 == null || (configuration2.diff(configuration) & 4) != 0) {
            if (f145b) {
                Log.d(f144a, "current locale: " + a.n + " new locale: " + Locale.getDefault());
            }
            if (!d.b(this)) {
                return;
            }
            if (k.a(Locale.getDefault())) {
                if (a.f166h || k.b((Context) this, "force_disabled", false)) {
                    a("restart");
                    k.a((Context) this, "force_disabled", false);
                }
            } else if (a.f166h) {
                if (f145b) {
                    Log.d(f144a, "Language not supported. Stop recognition.");
                }
                a("stop");
                k.a((Context) this, "force_disabled", true);
            }
        }
    }

    public void onCreate() {
        super.onCreate();
        try {
            a.p = getPackageManager().getPackageInfo(getPackageName(), 0).versionName + " (" + "c0b2b72" + ")";
        } catch (NameNotFoundException e2) {
        }
        if (f145b) {
            Log.d(f144a, "AudioMonitor started: version " + a.p);
        }
        if (!f146c) {
            f146c = true;
            a.f161c = getFilesDir().toString() + "/";
            this.f148e = new Configuration(getResources().getConfiguration());
            k.a((Context) this, -1, 0, 0, 0);
            k.a(false, (Context) this, BtStubActivity.class);
            k.h((Context) this);
            a.a((Context) this);
            new af(this).execute(new Context[]{this});
        }
    }
}
