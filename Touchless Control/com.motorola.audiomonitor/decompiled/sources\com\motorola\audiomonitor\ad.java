package com.motorola.audiomonitor;

import android.content.Context;
import android.util.Log;
import f.f;
import f.h;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class ad implements k, s {

    /* renamed from: b reason: collision with root package name */
    private static final String f285b = ("AMonitor." + ad.class.getSimpleName());

    /* renamed from: c reason: collision with root package name */
    private static final boolean f286c = a.f160b;

    /* renamed from: a reason: collision with root package name */
    List f287a = new ArrayList();

    /* renamed from: d reason: collision with root package name */
    private final List f288d = new LinkedList();
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public final aa f289e;

    /* renamed from: f reason: collision with root package name */
    private final f f290f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public boolean f291g = false;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public int f292h = -1;
    /* access modifiers changed from: private */
    public int i = -1;
    private final h j = new ae(this);

    ad(Context context, aa aaVar) {
        this.f289e = aaVar;
        this.f287a.add(context.getString(C0014R.string.hello_moto).toLowerCase(Locale.getDefault()));
        this.f290f = new f(16000, this.j, this.f287a, false);
    }

    public boolean a(ProtectedByteBuffer protectedByteBuffer) {
        synchronized (this.f288d) {
            this.f288d.add(protectedByteBuffer);
        }
        this.f290f.a(protectedByteBuffer);
        return true;
    }

    public void b() {
        this.f290f.b();
        if (f286c) {
            Log.d(f285b, "Key Phrase Spotting stopped.");
        }
    }

    public void c() {
        synchronized (this.f288d) {
            this.f288d.clear();
        }
        this.f291g = false;
        this.f292h = -1;
    }

    public void d() {
        this.f290f.c();
    }

    public boolean e() {
        return this.f291g;
    }

    public void e_() {
        if (f286c) {
            Log.d(f285b, "Starting Key Phrase Spotting.");
        }
        this.f290f.a();
    }

    public int f() {
        return this.i;
    }

    public List g() {
        LinkedList linkedList;
        synchronized (this.f288d) {
            linkedList = new LinkedList(this.f288d);
        }
        return linkedList;
    }
}
