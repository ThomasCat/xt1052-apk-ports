package a;

import android.util.Log;
import b.a;
import b.d;
import b.e;
import java.util.List;

class w implements Runnable {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ List f80a;

    /* renamed from: b reason: collision with root package name */
    final /* synthetic */ float f81b;

    /* renamed from: c reason: collision with root package name */
    final /* synthetic */ int f82c;

    /* renamed from: d reason: collision with root package name */
    final /* synthetic */ u f83d;

    w(u uVar, List list, float f2, int i) {
        this.f83d = uVar;
        this.f80a = list;
        this.f81b = f2;
        this.f82c = i;
    }

    public void run() {
        this.f83d.f77h = this.f80a;
        a aVar = new a(this.f80a, this.f81b);
        try {
            if (this.f83d.k != null) {
                if (this.f82c == 16000) {
                    this.f83d.j = this.f83d.k.a(aVar);
                } else if (this.f82c != 8000 || this.f83d.n == null) {
                    if (u.f71b) {
                        Log.e(u.f70a, "Parser not available for sample rate = " + this.f82c);
                    }
                    throw new e();
                } else {
                    this.f83d.j = this.f83d.n.a(aVar);
                }
                if (!(!this.f83d.a(this.f83d.j) || this.f83d.m == null || this.f83d.j.a("CHECK_CALL_COMMAND") == null)) {
                    aVar.a(this.f83d.j.a("CHECK_CALL_COMMAND"));
                    if (!this.f83d.a(this.f83d.m.a(aVar))) {
                        this.f83d.j = null;
                        if (u.f71b) {
                            Log.d(u.f70a, "call command is not spotted");
                        }
                    } else {
                        ((d) this.f83d.j).a("CHECK_CALL_COMMAND", null);
                        if (u.f71b) {
                            Log.d(u.f70a, "call command is spotted");
                        }
                    }
                }
            }
            this.f83d.i = ac.PARSED;
        } catch (e e2) {
            this.f83d.i = ac.NOT_PARSED;
        }
        this.f83d.f();
    }
}
