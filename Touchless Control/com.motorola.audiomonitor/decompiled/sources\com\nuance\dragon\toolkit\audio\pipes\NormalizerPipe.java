package com.nuance.dragon.toolkit.audio.pipes;

import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.util.internal.d;

public class NormalizerPipe extends ConverterPipe {

    /* renamed from: a reason: collision with root package name */
    private static int f1174a = 8000;

    /* renamed from: b reason: collision with root package name */
    private static short f1175b = Short.MIN_VALUE;

    /* renamed from: c reason: collision with root package name */
    private static short f1176c = Short.MAX_VALUE;

    /* renamed from: d reason: collision with root package name */
    private final short f1177d;

    /* renamed from: e reason: collision with root package name */
    private boolean f1178e;

    public NormalizerPipe() {
        this(f1174a);
    }

    public NormalizerPipe(int i) {
        d.a("threshold", "between 32768 and 32767", i >= f1175b && i <= f1176c);
        this.f1177d = (short) Math.abs(i);
        this.f1178e = true;
    }

    /* access modifiers changed from: protected */
    public void cleanup() {
    }

    /* access modifiers changed from: protected */
    public AudioChunk[] convert(AudioChunk audioChunk) {
        AudioChunk audioChunk2;
        if (audioChunk != null) {
            short[] sArr = new short[audioChunk.audioShorts.length];
            System.arraycopy(audioChunk.audioShorts, 0, sArr, 0, sArr.length);
            AudioChunk audioChunk3 = new AudioChunk(audioChunk.audioType, sArr);
            if (this.f1178e) {
                short s = this.f1177d;
                short[] sArr2 = audioChunk3.audioShorts;
                short s2 = 0;
                for (short abs : sArr2) {
                    short abs2 = (short) Math.abs(abs);
                    if (abs2 >= s2) {
                        s2 = abs2;
                    }
                }
                if (s2 > 0 && s2 >= s) {
                    float f2 = (float) (f1176c / s2);
                    for (int i = 0; i < sArr2.length; i++) {
                        short s3 = (short) ((int) (((float) sArr2[i]) * f2));
                        if (s3 >= f1175b && s3 <= f1176c) {
                            sArr2[i] = s3;
                        } else if (s3 > f1176c) {
                            sArr2[i] = f1176c;
                        } else {
                            sArr2[i] = f1175b;
                        }
                    }
                }
                audioChunk2 = new AudioChunk(audioChunk3.audioType, sArr2);
            } else {
                audioChunk2 = audioChunk3;
            }
        } else {
            audioChunk2 = null;
        }
        if (audioChunk2 == null) {
            return null;
        }
        return new AudioChunk[]{new AudioChunk(audioChunk2.audioType, audioChunk2.audioShorts)};
    }

    public AudioType getAudioType() {
        return getAudioTypeFromSource();
    }

    /* access modifiers changed from: protected */
    public void initialize(AudioType audioType) {
    }

    /* access modifiers changed from: protected */
    public final boolean isAudioSourceTypeSupported(AudioType audioType) {
        return audioType.encoding == Encoding.PCM_16;
    }

    public void setActive(boolean z) {
        this.f1178e = z;
    }
}
