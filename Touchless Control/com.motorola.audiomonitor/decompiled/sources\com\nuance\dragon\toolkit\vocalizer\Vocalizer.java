package com.nuance.dragon.toolkit.vocalizer;

import android.os.Handler;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.file.FileManager;
import java.util.List;

public abstract class Vocalizer {

    public final class Frequencies {
        public static final int FREQ_11KHZ = 11;
        public static final int FREQ_16KHZ = 16;
        public static final int FREQ_22KHZ = 22;
        public static final int FREQ_8KHZ = 8;
        public static final int UNKNOWN = 0;
    }

    public final class Languages extends a {
    }

    public interface LoadListener {
        void onLoaded(Vocalizer vocalizer, boolean z);
    }

    public interface ReleaseListener {
        void onReleased(Vocalizer vocalizer);
    }

    public interface ResourceLoadListener {
        void onLoaded(Vocalizer vocalizer, boolean z);
    }

    public interface ResourceReleaseListener {
        void onReleased(Vocalizer vocalizer);
    }

    public interface TtsListener {
        void ttsGenerationFinished(String str, Object obj, Vocalizer vocalizer, boolean z);

        void ttsGenerationStarted(String str, Object obj, Vocalizer vocalizer);

        void ttsStreamingFinished(String str, Object obj, Vocalizer vocalizer);

        void ttsStreamingStarted(String str, Object obj, Vocalizer vocalizer);
    }

    public final class Voices extends d {
    }

    public static Vocalizer createVocalizer(FileManager fileManager) {
        return new c(fileManager);
    }

    public static Vocalizer createVocalizer(FileManager fileManager, Handler handler) {
        return new c(fileManager, handler);
    }

    public abstract void cancelTts();

    public abstract TtsAudioSource generateTts(String str, TtsListener ttsListener, Object obj);

    public abstract AudioType getAudioType();

    public abstract List getAvailableLanguages(int i);

    public abstract List getAvailableModels();

    public abstract List getAvailableModels(int i);

    public abstract List getAvailableVoices(int i);

    public abstract List getAvailableVoices(int i, VocalizerLanguage vocalizerLanguage);

    public abstract void load(VocalizerConfig vocalizerConfig);

    public abstract void load(VocalizerConfig vocalizerConfig, LoadListener loadListener);

    public abstract void loadResource(String str, String str2, ResourceLoadListener resourceLoadListener);

    public abstract void release();

    public abstract void release(ReleaseListener releaseListener);

    public abstract void releaseResource(String str, ResourceReleaseListener resourceReleaseListener);

    public abstract void setAudioChunkBufferCount(int i);

    public abstract void setAudioChunkSize(int i);

    public abstract void setTtsSpeed(int i);

    public abstract void setTtsVolume(int i);
}
