package com.nuance.a.a.a.b.c.b.b;

import com.motorola.audiomonitor.GenericRecognizer.UdtEnrollResult;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.a.b.a.b.C0002b;
import com.nuance.a.a.a.a.d.d;
import com.nuance.a.a.a.b.b.f;
import com.nuance.a.a.a.b.c.b.a.b;
import com.nuance.a.a.a.b.c.c.a;
import java.io.ByteArrayOutputStream;

public class c extends o implements C0002b, a {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f856a = com.nuance.a.a.a.a.b.a.a.a(c.class);

    /* renamed from: b reason: collision with root package name */
    private int f857b;

    /* renamed from: c reason: collision with root package name */
    private b f858c;

    /* renamed from: d reason: collision with root package name */
    private boolean f859d = false;

    /* renamed from: e reason: collision with root package name */
    private com.nuance.a.a.a.a.b.a.b f860e;

    public c(String str, b bVar, com.nuance.a.a.a.a.b.a.b bVar2) {
        super(str, 1);
        this.f858c = bVar;
        this.f860e = bVar2;
        this.f857b = bVar.a();
        bVar2.a(new com.nuance.a.a.a.a.b.a.b.a(1, null), this, Thread.currentThread(), bVar2.a()[0]);
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return this.f857b;
    }

    public final void a(Object obj, Object obj2) {
        com.nuance.a.a.a.a.b.a.b.a aVar = (com.nuance.a.a.a.a.b.a.b.a) obj;
        switch (aVar.f783a) {
            case 1:
                this.f858c.a(this.f857b);
                return;
            case 2:
                Object[] objArr = (Object[]) aVar.f784b;
                byte[] bArr = (byte[]) objArr[0];
                boolean booleanValue = ((Boolean) objArr[1]).booleanValue();
                if (bArr != null) {
                    if (d.a(this.f858c.g())) {
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        if (bArr.length < 128) {
                            byteArrayOutputStream.write(bArr.length & 127);
                        } else if (bArr.length < 16384) {
                            byteArrayOutputStream.write(((bArr.length >> 7) & 127) | UdtEnrollResult.CHECKRECORDING_CLIPPING_MSEC);
                            byteArrayOutputStream.write(bArr.length & 127);
                        } else if (bArr.length < 2097152) {
                            byteArrayOutputStream.write(((bArr.length >> 14) & 127) | UdtEnrollResult.CHECKRECORDING_CLIPPING_MSEC);
                            byteArrayOutputStream.write(((bArr.length >> 7) & 127) | UdtEnrollResult.CHECKRECORDING_CLIPPING_MSEC);
                            byteArrayOutputStream.write(bArr.length & 127);
                        } else if (bArr.length < 268435456) {
                            byteArrayOutputStream.write(((bArr.length >> 21) & 127) | UdtEnrollResult.CHECKRECORDING_CLIPPING_MSEC);
                            byteArrayOutputStream.write(((bArr.length >> 14) & 127) | UdtEnrollResult.CHECKRECORDING_CLIPPING_MSEC);
                            byteArrayOutputStream.write(((bArr.length >> 7) & 127) | UdtEnrollResult.CHECKRECORDING_CLIPPING_MSEC);
                            byteArrayOutputStream.write(bArr.length & 127);
                        } else {
                            f856a.e("buffer size is too big!");
                            return;
                        }
                        byteArrayOutputStream.write(bArr, 0, bArr.length);
                        this.f858c.a(byteArrayOutputStream.toByteArray(), this.f857b);
                    } else {
                        this.f858c.a(bArr, this.f857b);
                    }
                }
                if (booleanValue) {
                    this.f858c.b(this.f857b);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public final void a(byte[] bArr, int i, int i2, boolean z) {
        if (bArr == null && !z) {
            f856a.e("PDXAudioParam.addAudioBuf() in (NMSPAudioSink)PDXAudioParam.addAudioBuf(), the param \"buffer\" is null.");
            throw new NullPointerException("in (NMSPAudioSink)PDXAudioParam.addAudioBuf(), the param \"buffer\" is null.");
        } else if (bArr != null && i < 0) {
            f856a.e("PDXAudioParam.addAudioBuf() the offset of the \"buffer\" is less than 0");
            throw new IllegalArgumentException("the offset of the \"buffer\" is less than 0");
        } else if (bArr != null && i2 <= 0) {
            f856a.e("PDXAudioParam.addAudioBuf() the indicated length of the \"buffer\" is less than 1 byte");
            throw new IllegalArgumentException("the indicated length of the \"buffer\" is less than 1 byte");
        } else if (this.f859d) {
            f856a.e("PDXAudioParam.addAudioBuf() last audio buffer already added!");
            throw new f("last audio buffer already added!");
        } else {
            if (z) {
                this.f859d = true;
            }
            byte[] bArr2 = null;
            if (bArr != null) {
                bArr2 = new byte[i2];
                System.arraycopy(bArr, i, bArr2, 0, i2);
            }
            this.f860e.a(new com.nuance.a.a.a.a.b.a.b.a(2, new Object[]{bArr2, new Boolean(z)}), this, Thread.currentThread(), this.f860e.a()[0]);
        }
    }
}
