package g;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

public abstract class b extends Binder implements a {
    public static a a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.motorola.audiomonitor.inputplugin.IInputPlugin");
        return (queryLocalInterface == null || !(queryLocalInterface instanceof a)) ? new c(iBinder) : (a) queryLocalInterface;
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                parcel.enforceInterface("com.motorola.audiomonitor.inputplugin.IInputPlugin");
                a();
                parcel2.writeNoException();
                return true;
            case 2:
                parcel.enforceInterface("com.motorola.audiomonitor.inputplugin.IInputPlugin");
                b();
                parcel2.writeNoException();
                return true;
            case 3:
                parcel.enforceInterface("com.motorola.audiomonitor.inputplugin.IInputPlugin");
                c();
                parcel2.writeNoException();
                return true;
            case 4:
                parcel.enforceInterface("com.motorola.audiomonitor.inputplugin.IInputPlugin");
                d();
                parcel2.writeNoException();
                return true;
            case 5:
                parcel.enforceInterface("com.motorola.audiomonitor.inputplugin.IInputPlugin");
                a(parcel.readString());
                parcel2.writeNoException();
                return true;
            case 1598968902:
                parcel2.writeString("com.motorola.audiomonitor.inputplugin.IInputPlugin");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
