package com.nuance.a.a.a.c.a;

import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public final class h {

    /* renamed from: a reason: collision with root package name */
    private C0001a f942a = a.a(getClass());

    public static Class a(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e2) {
            e2.printStackTrace();
            throw new RuntimeException("getClassForName failed", e2);
        }
    }

    public static Object a(Class cls, String str) {
        try {
            return cls.getField(str).get(null);
        } catch (Exception e2) {
            e2.printStackTrace();
            throw new RuntimeException("getFieldValue failed", e2);
        }
    }

    public static Object a(Class cls, String str, Object obj) {
        try {
            return cls.getField(str).get(null);
        } catch (Exception e2) {
            return obj;
        }
    }

    public static Constructor a(Class cls, Class... clsArr) {
        try {
            return cls.getConstructor(clsArr);
        } catch (Exception e2) {
            e2.printStackTrace();
            throw new RuntimeException("getConstructor failed", e2);
        }
    }

    public static Method a(Class cls, String str, Class... clsArr) {
        try {
            return cls.getMethod(str, clsArr);
        } catch (Exception e2) {
            e2.printStackTrace();
            throw new RuntimeException("getMethod failed", e2);
        }
    }

    public static Method b(Class cls, String str, Class... clsArr) {
        boolean z = false;
        try {
            return cls.getMethod(str, clsArr);
        } catch (Exception e2) {
            return z;
        }
    }
}
