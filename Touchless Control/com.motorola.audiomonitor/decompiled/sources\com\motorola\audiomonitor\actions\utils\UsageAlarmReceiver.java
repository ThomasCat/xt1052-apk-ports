package com.motorola.audiomonitor.actions.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.DropBoxManager;
import android.util.Log;
import com.motorola.audiomonitor.a;
import h.d;
import h.k;

public class UsageAlarmReceiver extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    private static final String f167a = ("AMonitor." + UsageAlarmReceiver.class.getSimpleName());

    public void onReceive(Context context, Intent intent) {
        k.a(context, "last_usage_report", System.currentTimeMillis());
        if (a.f160b && d.b(context)) {
            Log.d(f167a, "Triggering AoV usage report");
            ((DropBoxManager) context.getSystemService("dropbox")).addText("AOV_DAILY_REPORT", "Subtype: daily\nversion=" + a.p + "\n");
        }
        c.d.a(context);
    }
}
