package com.motorola.audiomonitor.service;

import android.os.Message;
import android.util.Log;
import com.nuance.dragon.toolkit.audio.pipes.SpeexEncoderPipe;

class f extends p {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ d f572a;

    f(d dVar, String str, String str2) {
        this.f572a = dVar;
        super(dVar, str, str2);
    }

    public void a(Message message) {
        switch (message.what) {
            case 1:
                this.f572a.b(true);
                return;
            case 2:
            case 3:
            case 7:
            case 8:
            case 9:
            case 10:
                break;
            case 11:
                if (message.arg1 != 0) {
                    this.f572a.b(true);
                    return;
                }
                if (this.f572a.w.what == 103 || this.f572a.w.what == 108) {
                }
                return;
            case 13:
                if (message.arg1 == 0) {
                    this.f572a.a((String) message.obj);
                    break;
                }
                break;
            case SpeexEncoderPipe.DEFAULT_VAD_BEGIN_LEN /*15*/:
                this.f572a.b(false);
                return;
            case 101:
                this.f572a.s();
                return;
            case 102:
                this.f572a.a(message);
                return;
            case 103:
                if (this.f572a.x.a(0, message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to stop recognition");
                    this.f572a.a(message);
                    return;
                }
                return;
            case 104:
                if (this.f572a.x.a(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to control event log");
                    this.f572a.a(message);
                    return;
                }
                return;
            case 105:
                if (this.f572a.x.b(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to control audio log");
                    this.f572a.a(message);
                    return;
                }
                return;
            case 106:
                if (this.f572a.x.c(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to control audio buffering");
                    this.f572a.a(message);
                    return;
                }
                return;
            case 107:
                this.f572a.a(message);
                return;
            case 108:
                if (this.f572a.x.a(3, 0) != 0) {
                    Log.e(d.f563a, "Failed to stop pass-through");
                    this.f572a.a(message);
                    return;
                }
                return;
            case 109:
                this.f572a.a(message);
                return;
            case 110:
                if (this.f572a.x.e() != 0) {
                    Log.e(d.f563a, "Failed to request dsp version");
                    this.f572a.a(message);
                    return;
                }
                return;
            case 111:
                this.f572a.a(message);
                return;
            case 201:
                this.f572a.b(true);
                return;
            case 202:
                if (this.f572a.x.d(message.arg1) != 0) {
                    Log.e(d.f563a, "Failed to send dock state change");
                    this.f572a.a(message);
                    return;
                }
                return;
            case 203:
                if (this.f572a.x.a((String) message.obj) != 0) {
                    Log.e(d.f563a, "Failed to load acoustic model");
                    this.f572a.a(message);
                    return;
                }
                return;
            case 204:
                if (this.f572a.x.b((String) message.obj) != 0) {
                    Log.e(d.f563a, "Failed to load language model");
                    this.f572a.a(message);
                    return;
                }
                return;
            case 205:
                this.f572a.t = 0;
                this.f572a.s = this.f572a.f567e;
                this.f572a.u();
                return;
            default:
                return;
        }
        if (message.arg1 != 0) {
            this.f572a.b(true);
        }
    }
}
