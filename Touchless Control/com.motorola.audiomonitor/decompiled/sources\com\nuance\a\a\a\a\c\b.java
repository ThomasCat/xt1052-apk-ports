package com.nuance.a.a.a.a.c;

import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;

public class b {

    /* renamed from: e reason: collision with root package name */
    private static final C0001a f792e = a.a(b.class);

    /* renamed from: a reason: collision with root package name */
    public byte f793a;

    /* renamed from: b reason: collision with root package name */
    public byte f794b;

    /* renamed from: c reason: collision with root package name */
    public short f795c;

    /* renamed from: d reason: collision with root package name */
    public int f796d;

    public b(byte b2, byte b3, short s, int i) {
        if (f792e.b()) {
            f792e.b((Object) "Constructing XModeMsgHeader(protocol=" + b2 + ", version=" + b3 + ", cmd=" + s + ", len=" + i + ")");
        }
        this.f793a = b2;
        this.f794b = b3;
        this.f795c = s;
        this.f796d = i;
    }

    public b(byte[] bArr) {
        if (f792e.b()) {
            f792e.b((Object) "Constructing XModeMsgHeader(byte[])");
            f792e.a(bArr);
        }
        this.f793a = bArr[0];
        this.f794b = bArr[1];
        this.f795c = com.nuance.a.a.a.a.d.b.a(bArr, 2);
        this.f796d = com.nuance.a.a.a.a.d.b.b(bArr, 4);
    }

    public final byte[] a() {
        if (f792e.b()) {
            f792e.b((Object) "XModeMsgHeader.getBytes()");
        }
        byte[] bArr = new byte[8];
        bArr[0] = this.f793a;
        bArr[1] = this.f794b;
        com.nuance.a.a.a.a.d.b.a(this.f795c, bArr, 2);
        com.nuance.a.a.a.a.d.b.a(this.f796d, bArr, 4);
        if (f792e.b()) {
            f792e.a((Object) "Generated: ");
            f792e.a(bArr);
        }
        return bArr;
    }
}
