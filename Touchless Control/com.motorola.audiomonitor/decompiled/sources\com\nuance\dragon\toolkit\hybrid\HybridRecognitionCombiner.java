package com.nuance.dragon.toolkit.hybrid;

import com.nuance.dragon.toolkit.recognition.InterpretException;
import com.nuance.dragon.toolkit.recognition.InterpretedRecognition;
import com.nuance.dragon.toolkit.recognition.InterpretedRecognition.Choice;
import com.nuance.dragon.toolkit.recognition.InterpretedRecognitionCombiner;

public class HybridRecognitionCombiner implements InterpretedRecognitionCombiner {
    public InterpretedRecognition combine(InterpretedRecognition interpretedRecognition, InterpretedRecognition interpretedRecognition2) {
        if ((interpretedRecognition == null || interpretedRecognition.getChoiceCount() == 0) && (interpretedRecognition2 == null || interpretedRecognition2.getChoiceCount() == 0)) {
            throw new InterpretException("No results to combine");
        } else if (interpretedRecognition == null || interpretedRecognition.getChoiceCount() == 0) {
            return interpretedRecognition2;
        } else {
            if (interpretedRecognition2 == null || interpretedRecognition2.getChoiceCount() == 0) {
                return interpretedRecognition;
            }
            InterpretedRecognition interpretedRecognition3 = new InterpretedRecognition();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < interpretedRecognition.getChoiceCount() && interpretedRecognition3.getChoiceCount() < 10) {
                    Choice resolveUnrecognizedPhrases = interpretedRecognition.getChoice(i2).resolveUnrecognizedPhrases(interpretedRecognition2, true);
                    if (resolveUnrecognizedPhrases != null) {
                        interpretedRecognition3.addChoice(resolveUnrecognizedPhrases.toString(), resolveUnrecognizedPhrases.getRecognitionType(), resolveUnrecognizedPhrases.getScore(), resolveUnrecognizedPhrases.getPhrases(), resolveUnrecognizedPhrases.getExtra());
                    }
                    i = i2 + 1;
                }
            }
            return interpretedRecognition3.getChoiceCount() != 0 ? interpretedRecognition3 : interpretedRecognition;
        }
    }
}
