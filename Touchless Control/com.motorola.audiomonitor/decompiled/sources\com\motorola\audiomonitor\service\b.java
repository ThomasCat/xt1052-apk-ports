package com.motorola.audiomonitor.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.motorola.audiomonitor.a;

class b extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ a f562a;

    b(a aVar) {
        this.f562a = aVar;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (!a.l && !TextUtils.isEmpty(action)) {
            if (action.equals("android.intent.action.SCREEN_ON")) {
                if (this.f562a.f560e) {
                    this.f562a.f558c.a(true);
                }
            } else if (action.equals("android.intent.action.SCREEN_OFF")) {
                this.f562a.f558c.e();
            }
        }
    }
}
