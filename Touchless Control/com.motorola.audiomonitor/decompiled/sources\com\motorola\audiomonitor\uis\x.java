package com.motorola.audiomonitor.uis;

import android.content.Intent;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;

class x implements OnPreferenceClickListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ u f762a;

    x(u uVar) {
        this.f762a = uVar;
    }

    public boolean onPreferenceClick(Preference preference) {
        this.f762a.startActivity(new Intent(this.f762a.getActivity(), PrivacyActivity.class));
        return true;
    }
}
