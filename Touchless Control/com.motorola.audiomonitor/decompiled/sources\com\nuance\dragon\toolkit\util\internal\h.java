package com.nuance.dragon.toolkit.util.internal;

public final class h {
    public static String a(Iterable iterable, String str) {
        StringBuffer stringBuffer = new StringBuffer();
        boolean z = true;
        for (Object next : iterable) {
            if (!z) {
                stringBuffer.append(str);
            } else {
                z = false;
            }
            stringBuffer.append(next.toString());
        }
        return stringBuffer.toString();
    }

    public static String a(String str, String str2) {
        return str == null ? str2 : str2 == null ? str : str + str2;
    }
}
