package h;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.FloatMath;

class f implements SensorEventListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ e f2287a;

    /* renamed from: b reason: collision with root package name */
    private final Sensor f2288b;

    /* renamed from: c reason: collision with root package name */
    private final Sensor f2289c;

    /* renamed from: d reason: collision with root package name */
    private float[] f2290d = {0.0f, 0.0f, 0.0f};

    /* renamed from: e reason: collision with root package name */
    private int f2291e;

    /* renamed from: f reason: collision with root package name */
    private boolean f2292f;

    public f(e eVar, Sensor sensor, Sensor sensor2) {
        this.f2287a = eVar;
        this.f2288b = sensor;
        this.f2289c = sensor2;
    }

    private void a() {
        if (this.f2292f && this.f2291e > 3 && this.f2287a.f2285c != null) {
            this.f2287a.f2285c.run();
        }
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        boolean z = false;
        if (sensorEvent.sensor.equals(this.f2288b)) {
            if (sensorEvent.values.length > 0) {
                if (sensorEvent.values[0] != 0.0f) {
                    z = true;
                }
                this.f2292f = z;
                a();
            }
        } else if (sensorEvent.sensor.equals(this.f2289c)) {
            float f2 = 0.0f;
            for (int i = 0; i < sensorEvent.values.length; i++) {
                f2 += FloatMath.pow(this.f2290d[i] - sensorEvent.values[i], 2.0f);
                this.f2290d[i] = sensorEvent.values[i];
            }
            if (f2 > 0.01f) {
                this.f2291e = 0;
            } else {
                this.f2291e++;
            }
            a();
        }
    }
}
