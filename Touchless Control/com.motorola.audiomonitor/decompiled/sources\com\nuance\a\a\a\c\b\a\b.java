package com.nuance.a.a.a.c.b.a;

import com.nuance.a.a.a.a.d.a;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public final class b implements X509TrustManager {

    /* renamed from: b reason: collision with root package name */
    private static X509TrustManager f949b;

    /* renamed from: a reason: collision with root package name */
    private c f950a;

    public b(c cVar) {
        this.f950a = cVar;
        TrustManagerFactory instance = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        instance.init(null);
        f949b = (X509TrustManager) instance.getTrustManagers()[0];
    }

    public final void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
    }

    public final void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
        try {
            f949b.checkServerTrusted(x509CertificateArr, str);
        } catch (CertificateException e2) {
            if (!this.f950a.f951a) {
                throw e2;
            }
        }
        if (this.f950a.f952b != null) {
            boolean z = false;
            for (X509Certificate subjectDN : x509CertificateArr) {
                String name = subjectDN.getSubjectDN().getName();
                int indexOf = name.indexOf("CN=") + 3;
                if (this.f950a.f952b.equals(name.substring(indexOf, name.indexOf(44, indexOf)))) {
                    z = true;
                }
            }
            if (!z) {
                throw new CertificateException("certificate summary is not identical");
            }
        }
        if (this.f950a.f953c != null) {
            boolean z2 = false;
            for (X509Certificate encoded : x509CertificateArr) {
                if (this.f950a.f953c.equals(a.a(encoded.getEncoded()))) {
                    z2 = true;
                }
            }
            if (!z2) {
                throw new CertificateException("certificate data is not identical");
            }
        }
    }

    public final X509Certificate[] getAcceptedIssuers() {
        return f949b.getAcceptedIssuers();
    }
}
