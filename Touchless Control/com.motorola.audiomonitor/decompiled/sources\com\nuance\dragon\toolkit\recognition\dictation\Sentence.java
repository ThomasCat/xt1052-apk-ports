package com.nuance.dragon.toolkit.recognition.dictation;

public interface Sentence {
    double getConfidenceScore();

    long getEndTime();

    long getStartTime();

    int size();

    String toString();

    Token tokenAt(int i);
}
