package f;

import android.util.Log;
import com.motorola.audiomonitor.a;
import com.nuance.dragon.toolkit.elvis.ElvisLanguage;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.Languages;
import com.nuance.dragon.toolkit.elvis.Grammar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class v {

    /* renamed from: a reason: collision with root package name */
    private static final String f2257a = ("AMonitor." + v.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f2258b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private static final Map f2259c = new HashMap();

    /* renamed from: d reason: collision with root package name */
    private static final ElvisLanguage f2260d = Languages.UNITED_STATES_ENGLISH;

    static {
        f2259c.put("en_US", Languages.UNITED_STATES_ENGLISH);
        f2259c.put("es_US", Languages.US_SPANISH);
        f2259c.put("pt_BR", Languages.BRAZILIAN_PORTUGUESE);
        f2259c.put("fr_CA", Languages.CANADIAN_FRENCH);
        f2259c.put("en_CA", Languages.UNITED_STATES_ENGLISH);
    }

    public static ElvisLanguage a() {
        ElvisLanguage elvisLanguage = (ElvisLanguage) f2259c.get(a.n);
        if (elvisLanguage == null) {
            elvisLanguage = f2260d;
            if (f2258b) {
                Log.d(f2257a, "Default language is selected " + elvisLanguage);
            }
        }
        return elvisLanguage;
    }

    public static Grammar a(List list, List list2, List list3, List list4) {
        return u.a(list, list2, list3, list4);
    }

    public static Locale a(Locale locale) {
        String locale2 = locale.toString();
        for (String equalsIgnoreCase : f2259c.keySet()) {
            if (locale2.equalsIgnoreCase(equalsIgnoreCase)) {
                return locale;
            }
        }
        return new Locale("en_US");
    }
}
