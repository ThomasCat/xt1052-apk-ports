package com.nuance.dragon.toolkit.grammar.content;

import android.content.Context;
import android.os.Handler;
import android.util.Pair;
import com.nuance.dragon.toolkit.audio.bluetooth.impl.AndroidVersion;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.grammar.WordAction;
import com.nuance.dragon.toolkit.grammar.WordList;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.c;
import com.nuance.dragon.toolkit.util.internal.d;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public abstract class ContentManager {
    protected final Context _context;

    /* renamed from: a reason: collision with root package name */
    private final String f1757a;

    /* renamed from: b reason: collision with root package name */
    private final FileManager f1758b;

    /* renamed from: c reason: collision with root package name */
    private final Handler f1759c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public final com.nuance.dragon.toolkit.util.internal.a f1760d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public final com.nuance.dragon.toolkit.util.internal.a f1761e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public a f1762f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public final Object f1763g = new Object();

    /* renamed from: h reason: collision with root package name */
    private boolean f1764h;
    /* access modifiers changed from: private */
    public boolean i;
    /* access modifiers changed from: private */
    public boolean j;
    private boolean k;
    /* access modifiers changed from: private */
    public List l;
    /* access modifiers changed from: private */
    public String m;
    /* access modifiers changed from: private */
    public final List n;
    /* access modifiers changed from: private */
    public SyncListener o;
    /* access modifiers changed from: private */
    public SyncListener p;

    public interface PlatformFullIterator extends Iterator {
        int getSize();
    }

    public interface SyncListener {
        void onDone(ContentManager contentManager);
    }

    final class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final Runnable f1783a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public final Handler f1784b = new Handler();
        /* access modifiers changed from: private */

        /* renamed from: c reason: collision with root package name */
        public boolean f1785c;
        /* access modifiers changed from: private */

        /* renamed from: d reason: collision with root package name */
        public boolean f1786d;

        /* renamed from: com.nuance.dragon.toolkit.grammar.content.ContentManager$a$a reason: collision with other inner class name */
        interface C0007a {
            void a();
        }

        a(final C0007a aVar) {
            this.f1783a = new Runnable() {

                /* renamed from: a reason: collision with root package name */
                final /* synthetic */ int f1787a = AndroidVersion.CUR_DEVELOPMENT;

                /* renamed from: b reason: collision with root package name */
                final /* synthetic */ int f1788b = 60000;

                /* renamed from: e reason: collision with root package name */
                private int f1791e;

                public final void run() {
                    if (!a.this.f1786d) {
                        this.f1791e += this.f1787a;
                        if (!a.this.f1785c || this.f1791e >= this.f1788b) {
                            a.this.f1786d = true;
                            C0007a aVar = aVar;
                            a aVar2 = a.this;
                            aVar.a();
                            return;
                        }
                        a.this.f1785c = false;
                        a.this.f1784b.postDelayed(a.this.f1783a, (long) this.f1787a);
                    }
                }
            };
            this.f1784b.postDelayed(this.f1783a, 10000);
        }

        /* access modifiers changed from: 0000 */
        public final void a() {
            this.f1785c = true;
        }

        /* access modifiers changed from: 0000 */
        public final void b() {
            this.f1786d = true;
            this.f1784b.removeCallbacks(this.f1783a);
        }
    }

    public ContentManager(String str, FileManager fileManager, boolean z, Context context) {
        d.a("fileName", str);
        d.a("fileManager", (Object) fileManager);
        d.a("context", (Object) context);
        this.f1757a = str;
        this.f1758b = fileManager;
        this.k = z;
        this._context = context.getApplicationContext();
        this.n = new ArrayList();
        this.f1759c = new Handler();
        this.f1760d = new com.nuance.dragon.toolkit.util.internal.a();
        this.f1761e = new com.nuance.dragon.toolkit.util.internal.a();
        this.l = new ArrayList(0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0040 A[SYNTHETIC, Splitter:B:12:0x0040] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0045 A[SYNTHETIC, Splitter:B:15:0x0045] */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    static ObjectInputStream a(FileManager fileManager, String str) {
        GZIPInputStream gZIPInputStream;
        FileInputStream openFileForReading = fileManager.openFileForReading(str);
        if (openFileForReading == null) {
            Logger.warn(ContentManager.class, "Fail to open read-stream for file " + str);
            return null;
        }
        try {
            gZIPInputStream = new GZIPInputStream(openFileForReading);
            try {
                return new ObjectInputStream(gZIPInputStream);
            } catch (Exception e2) {
                e = e2;
                Logger.error(ContentManager.class, "Error creating read-stream for file " + str, e);
                if (gZIPInputStream != null) {
                    try {
                        gZIPInputStream.close();
                    } catch (IOException e3) {
                    }
                }
                if (openFileForReading != null) {
                    return null;
                }
                try {
                    openFileForReading.close();
                    return null;
                } catch (IOException e4) {
                    return null;
                }
            }
        } catch (Exception e5) {
            e = e5;
            gZIPInputStream = null;
            Logger.error(ContentManager.class, "Error creating read-stream for file " + str, e);
            if (gZIPInputStream != null) {
            }
            if (openFileForReading != null) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        this.f1761e.a((Runnable) new Runnable() {
            public final void run() {
                ContentManager.k(ContentManager.this);
            }
        });
    }

    static /* synthetic */ void a(ContentManager contentManager, List list, String str) {
        Logger.info(contentManager, "Writing to file " + contentManager.f1757a);
        ObjectOutputStream b2 = b(contentManager.f1758b, contentManager.f1757a);
        if (b2 != null) {
            try {
                b2.writeInt(12345);
                b2.writeUTF(str);
                WordAction.writeListTo(list, b2);
                new StringBuilder("Saved ").append(list.size()).append(" items");
            } catch (IOException e2) {
                Logger.error(contentManager, "Error writing to file " + contentManager.f1757a, e2);
            }
            try {
                b2.close();
            } catch (IOException e3) {
                Logger.error(contentManager, "Error saving file " + contentManager.f1757a, e3);
            }
        }
    }

    static /* synthetic */ void a(ContentManager contentManager, boolean z) {
        List list;
        boolean z2;
        String str = null;
        if (z) {
            contentManager.f1758b.delete(contentManager.f1757a);
            list = null;
        } else {
            Pair c2 = contentManager.c();
            if (c2 != null) {
                list = (List) c2.first;
                str = (String) c2.second;
            } else {
                list = null;
            }
        }
        if (list != null) {
            synchronized (contentManager.f1763g) {
                contentManager.l = list;
                contentManager.m = str;
            }
            contentManager.a(list, str, false);
            z2 = true;
        } else {
            z2 = false;
        }
        if (str == null) {
            str = UUID.randomUUID().toString();
        }
        if (contentManager.k && list == null) {
            synchronized (contentManager.f1763g) {
                contentManager.l = new ArrayList(0);
                contentManager.m = str;
            }
            contentManager.a(list, str, true);
            z2 = true;
        }
        if (!contentManager.k) {
            final PlatformFullIterator platformFullIterator = contentManager.getPlatformFullIterator();
            if (platformFullIterator == null || !platformFullIterator.hasNext()) {
                synchronized (contentManager.f1763g) {
                    contentManager.m = str;
                }
                if (!z2) {
                    contentManager.a((List) new ArrayList(0), str, false);
                    return;
                }
                return;
            }
            final c cVar = new c(platformFullIterator.getSize());
            if (list == null) {
                synchronized (contentManager.f1763g) {
                    contentManager.l = cVar;
                    contentManager.m = str;
                }
            }
            if (!z2) {
                contentManager.a((List) cVar, str, true);
            }
            final List list2 = contentManager.l;
            contentManager.f1760d.a((Runnable) new Runnable() {
                public final void run() {
                    boolean hasNext;
                    final ArrayList arrayList = new ArrayList();
                    do {
                        WordAction wordAction = (WordAction) platformFullIterator.next();
                        hasNext = platformFullIterator.hasNext();
                        cVar.a(wordAction, !hasNext);
                        arrayList.add(wordAction);
                    } while (hasNext);
                    ContentManager.this.f1761e.a((Runnable) new Runnable() {
                        public final void run() {
                            String c2;
                            if (ContentManager.this.l != list2) {
                                Logger.warn(ContentManager.this, "Rebuild finished, but appears to have been aborted");
                                return;
                            }
                            synchronized (ContentManager.this.f1763g) {
                                c2 = ContentManager.this.m;
                                if (cVar != ContentManager.this.l) {
                                    ContentManager.b(ContentManager.this, (List) arrayList);
                                }
                                ContentManager.this.l = arrayList;
                            }
                            ContentManager.a(ContentManager.this, (List) arrayList, c2);
                        }
                    });
                }
            });
        }
    }

    private void a(List list, String str, boolean z) {
        int i2;
        synchronized (this.f1763g) {
            this.j = true;
            int i3 = 0;
            while (i3 < this.n.size()) {
                a aVar = (a) ((WeakReference) this.n.get(i3)).get();
                if (aVar != null) {
                    aVar.a(list, str, z);
                    i2 = i3;
                } else {
                    this.n.remove(i3);
                    i2 = i3 - 1;
                }
                i3 = i2 + 1;
            }
        }
        b();
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0040 A[SYNTHETIC, Splitter:B:12:0x0040] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0045 A[SYNTHETIC, Splitter:B:15:0x0045] */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    static ObjectOutputStream b(FileManager fileManager, String str) {
        GZIPOutputStream gZIPOutputStream;
        FileOutputStream openFileForWriting = fileManager.openFileForWriting(str);
        if (openFileForWriting == null) {
            Logger.warn(ContentManager.class, "Fail to open write-stream for file " + str);
            return null;
        }
        try {
            gZIPOutputStream = new GZIPOutputStream(openFileForWriting);
            try {
                return new ObjectOutputStream(gZIPOutputStream);
            } catch (Exception e2) {
                e = e2;
                Logger.error(ContentManager.class, "Error creating write-stream for file " + str, e);
                if (gZIPOutputStream != null) {
                    try {
                        gZIPOutputStream.close();
                    } catch (IOException e3) {
                    }
                }
                if (openFileForWriting != null) {
                    return null;
                }
                try {
                    openFileForWriting.close();
                    return null;
                } catch (IOException e4) {
                    return null;
                }
            }
        } catch (Exception e5) {
            e = e5;
            gZIPOutputStream = null;
            Logger.error(ContentManager.class, "Error creating write-stream for file " + str, e);
            if (gZIPOutputStream != null) {
            }
            if (openFileForWriting != null) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        this.f1759c.post(new Runnable() {
            public final void run() {
                ContentManager.this.i = true;
                if (ContentManager.this.o != null) {
                    ContentManager.this.o.onDone(ContentManager.this);
                    ContentManager.this.o = null;
                }
                if (ContentManager.this.p != null) {
                    ContentManager.this.p.onDone(ContentManager.this);
                }
            }
        });
    }

    static /* synthetic */ void b(ContentManager contentManager, List list) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < contentManager.n.size()) {
                a aVar = (a) ((WeakReference) contentManager.n.get(i3)).get();
                if (aVar != null) {
                    aVar.a(list);
                } else {
                    contentManager.n.remove(i3);
                    i3--;
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    private Pair c() {
        Object obj;
        List list;
        List list2;
        Object obj2;
        Logger.info(this, "Reading from file " + this.f1757a);
        ObjectInputStream a2 = a(this.f1758b, this.f1757a);
        if (a2 != null) {
            try {
                if (a2.readInt() == 12345) {
                    obj = a2.readUTF();
                    try {
                        list = WordAction.readListFrom(a2);
                        try {
                            new StringBuilder("Loaded ").append(list.size()).append(" items");
                        } catch (IOException e2) {
                            Throwable th = e2;
                            list2 = list;
                            obj2 = obj;
                            e = th;
                            Logger.error(this, "Error reading from file " + this.f1757a, e);
                            obj = obj2;
                            list = list2;
                            a2.close();
                            if (list == null) {
                            }
                        }
                    } catch (IOException e3) {
                        list2 = null;
                        Object obj3 = obj;
                        e = e3;
                        obj2 = obj3;
                    }
                } else {
                    obj = null;
                    list = null;
                }
            } catch (IOException e4) {
                e = e4;
                obj2 = null;
                list2 = null;
                Logger.error(this, "Error reading from file " + this.f1757a, e);
                obj = obj2;
                list = list2;
                a2.close();
                if (list == null) {
                }
            }
            try {
                a2.close();
            } catch (IOException e5) {
                Logger.error(this, "Error closing file " + this.f1757a, e5);
            }
        } else {
            obj = null;
            list = null;
        }
        if (list == null) {
            return null;
        }
        return new Pair(list, obj);
    }

    static /* synthetic */ void k(ContentManager contentManager) {
        final PlatformFullIterator platformFullIterator = contentManager.getPlatformFullIterator();
        contentManager.f1760d.a((Runnable) new Runnable() {
            public final void run() {
                int size = platformFullIterator.getSize();
                if (size < 0) {
                    size = 0;
                }
                final ArrayList arrayList = new ArrayList(size);
                while (platformFullIterator.hasNext()) {
                    arrayList.add(platformFullIterator.next());
                }
                ContentManager.this.f1761e.a((Runnable) new Runnable() {
                    public final void run() {
                        synchronized (ContentManager.this.f1763g) {
                            if (ContentManager.this.l != null) {
                                ContentManager.this.l = arrayList;
                                String c2 = ContentManager.this.m;
                                ContentManager.b(ContentManager.this, arrayList);
                                ContentManager.a(ContentManager.this, arrayList, c2);
                                ContentManager.this.b();
                            }
                        }
                    }
                });
            }
        });
    }

    public WordList createWordList(FileManager fileManager, String str, boolean z) {
        d.a((Object) this, this.f1764h, "Not initialized");
        d.a("fileManager", (Object) fileManager);
        d.a("fileName", str);
        d.a(str, "different from the ContentManager's file name", !str.equals(this.f1757a));
        final a aVar = new a(fileManager, str, z, this.f1763g);
        this.f1761e.a((Runnable) new Runnable() {
            public final void run() {
                ContentManager.this.n.add(new WeakReference(aVar));
                if (ContentManager.this.j) {
                    aVar.a(ContentManager.this.l, ContentManager.this.m, false);
                }
            }
        });
        return aVar;
    }

    public void forceRefresh() {
        d.a((Object) this, this.f1764h, "Not initialized");
        a();
    }

    /* access modifiers changed from: protected */
    public abstract PlatformFullIterator getPlatformFullIterator();

    /* access modifiers changed from: protected */
    public void initialize(final boolean z) {
        d.a((Object) this, !this.f1764h, "Already initialized");
        this.f1764h = true;
        watchForContentUpdates(this._context);
        this.f1761e.a((Runnable) new Runnable() {
            public final void run() {
                ContentManager.a(ContentManager.this, z);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onContentUpdate() {
        if (this.k) {
            return;
        }
        if (this.f1762f != null) {
            this.f1762f.a();
        } else {
            this.f1762f = new a(new C0007a() {
                public final void a() {
                    ContentManager.this.f1762f;
                    ContentManager.this.f1762f = null;
                    ContentManager.this.a();
                }
            });
        }
    }

    public void release() {
        d.a((Object) this, this.f1764h, "Not initialized");
        this.f1764h = false;
        stopWatchingForContentUpdates(this._context);
        if (this.f1762f != null) {
            this.f1762f.b();
            this.f1762f = null;
        }
        this.f1761e.a((Runnable) new Runnable() {
            public final void run() {
                ContentManager.this.n.clear();
                ContentManager.a(ContentManager.this, ContentManager.this.l, ContentManager.this.m);
                ContentManager.this.l = null;
                ContentManager.this.f1760d.a();
                ContentManager.this.f1761e.a();
            }
        });
    }

    public void setInitListener(String str, SyncListener syncListener) {
        if (str != null && str.equals("GrammarDepot")) {
            if (!this.i) {
                this.o = syncListener;
            } else if (syncListener != null) {
                syncListener.onDone(this);
            }
        }
    }

    public void setUpdateListener(SyncListener syncListener) {
        this.p = syncListener;
    }

    /* access modifiers changed from: protected */
    public abstract void stopWatchingForContentUpdates(Context context);

    /* access modifiers changed from: protected */
    public abstract void watchForContentUpdates(Context context);
}
