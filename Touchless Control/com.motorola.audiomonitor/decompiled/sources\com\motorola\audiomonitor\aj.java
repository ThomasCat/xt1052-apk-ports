package com.motorola.audiomonitor;

import android.os.RemoteException;

class aj extends u {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ MonitorService f298a;

    aj(MonitorService monitorService) {
        this.f298a = monitorService;
    }

    public void a() {
        this.f298a.i();
    }

    public void a(w wVar) {
        this.f298a.j.register(wVar);
        wVar.a(this.f298a.i ? 1 : 0);
    }

    public boolean a(String str) {
        if (!this.f298a.i) {
            return false;
        }
        this.f298a.f156h.post(new al(this, str));
        return true;
    }

    public boolean a(boolean z) {
        if (!this.f298a.i) {
            return false;
        }
        this.f298a.f156h.post(new am(this, z));
        return true;
    }

    public void b() {
        if (!this.f298a.i) {
            throw new RemoteException("Monitor has not been started.");
        }
        this.f298a.f156h.post(new ak(this));
    }

    public void b(w wVar) {
        this.f298a.j.unregister(wVar);
    }

    public boolean b(boolean z) {
        if (!this.f298a.i) {
            return false;
        }
        this.f298a.f156h.post(new aq(this, z));
        return true;
    }

    public boolean c() {
        return this.f298a.i;
    }

    public boolean d() {
        if (!this.f298a.i) {
            return false;
        }
        this.f298a.f156h.post(new an(this));
        return true;
    }

    public boolean e() {
        if (!this.f298a.i) {
            return false;
        }
        this.f298a.f156h.post(new ao(this));
        return true;
    }

    public boolean f() {
        if (!this.f298a.i) {
            return false;
        }
        this.f298a.f156h.post(new ap(this));
        return true;
    }

    public boolean g() {
        if (!this.f298a.i) {
            return false;
        }
        this.f298a.f156h.post(new ar(this));
        return true;
    }
}
