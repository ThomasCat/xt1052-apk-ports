package com.nuance.dragon.toolkit.util.a;

import com.nuance.dragon.toolkit.util.JSONCompliant;
import org.json.JSONArray;

public final class a extends JSONArray {
    public final boolean a(Object obj) {
        if (obj instanceof JSONCompliant) {
            obj = ((JSONCompliant) obj).toJSON();
        }
        put(obj);
        return true;
    }
}
