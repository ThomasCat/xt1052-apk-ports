package com.motorola.audiomonitor.actions.utils;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Handler;
import android.provider.ContactsContract.Profile;
import android.text.TextUtils;
import com.motorola.audiomonitor.C0014R;
import h.j;
import java.util.Calendar;

public class an {

    /* renamed from: a reason: collision with root package name */
    private static an f212a;

    /* renamed from: b reason: collision with root package name */
    private static Object f213b = new Object();

    /* renamed from: c reason: collision with root package name */
    private ao f214c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public String f215d;

    /* renamed from: e reason: collision with root package name */
    private final Context f216e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public final j f217f = new j(this.f216e);

    private an(Context context, Handler handler) {
        this.f216e = context;
        this.f214c = new ao(this, handler);
        this.f216e.getContentResolver().registerContentObserver(Profile.CONTENT_URI, true, this.f214c);
    }

    public static an a() {
        an anVar;
        synchronized (f213b) {
            if (f212a == null) {
                throw new AssertionError("Not initialized");
            }
            anVar = f212a;
        }
        return anVar;
    }

    public static void a(Context context, Handler handler) {
        synchronized (f213b) {
            if (f212a != null) {
                f212a.c();
            }
            f212a = new an(context, handler);
        }
    }

    public int a(int i) {
        TypedArray obtainTypedArray = this.f216e.getResources().obtainTypedArray(i);
        try {
            return obtainTypedArray.getResourceId((int) (((double) obtainTypedArray.length()) * Math.random()), 0);
        } finally {
            obtainTypedArray.recycle();
        }
    }

    public String a(int i, int i2) {
        if (TextUtils.isEmpty(this.f215d) || this.f215d.length() > 10) {
            return this.f216e.getString(i2);
        }
        return this.f216e.getString(i, new Object[]{this.f215d});
    }

    public String b() {
        String string;
        boolean z = true;
        Resources resources = this.f216e.getResources();
        TypedArray obtainTypedArray = resources.obtainTypedArray(C0014R.array.array_greeting);
        TypedArray obtainTypedArray2 = resources.obtainTypedArray(C0014R.array.array_greeting_name);
        try {
            int length = obtainTypedArray.length();
            int i = length + 1;
            if (TextUtils.isEmpty(this.f215d) || this.f215d.length() > 10) {
                z = false;
            }
            if (z) {
                i += obtainTypedArray2.length();
            }
            int random = (int) (((double) i) * Math.random());
            if (random == i - 1) {
                int i2 = Calendar.getInstance().get(11);
                if (!z) {
                    if (i2 >= 4 && i2 < 12) {
                        string = resources.getString(C0014R.string.good_morning);
                    } else if (i2 < 12 || i2 >= 16) {
                        string = resources.getString(C0014R.string.good_evening);
                        obtainTypedArray.recycle();
                        obtainTypedArray2.recycle();
                    } else {
                        string = resources.getString(C0014R.string.good_afternoon);
                        obtainTypedArray.recycle();
                        obtainTypedArray2.recycle();
                    }
                } else if (i2 >= 4 && i2 < 12) {
                    string = resources.getString(C0014R.string.good_morning_name, new Object[]{this.f215d});
                    obtainTypedArray.recycle();
                    obtainTypedArray2.recycle();
                } else if (i2 < 12 || i2 >= 16) {
                    string = resources.getString(C0014R.string.good_evening_name, new Object[]{this.f215d});
                    obtainTypedArray.recycle();
                    obtainTypedArray2.recycle();
                } else {
                    string = resources.getString(C0014R.string.good_afternoon_name, new Object[]{this.f215d});
                    obtainTypedArray.recycle();
                    obtainTypedArray2.recycle();
                }
            } else if (random < length) {
                string = resources.getString(obtainTypedArray.getResourceId(random, 0));
                obtainTypedArray.recycle();
                obtainTypedArray2.recycle();
            } else {
                string = resources.getString(obtainTypedArray2.getResourceId(random - length, 0), new Object[]{this.f215d});
                obtainTypedArray.recycle();
                obtainTypedArray2.recycle();
            }
            return string;
        } finally {
            obtainTypedArray.recycle();
            obtainTypedArray2.recycle();
        }
    }

    public void c() {
        synchronized (f213b) {
            if (f212a != null) {
                if (this.f214c != null) {
                    this.f216e.getContentResolver().unregisterContentObserver(this.f214c);
                    this.f214c = null;
                }
                f212a = null;
            }
        }
    }
}
