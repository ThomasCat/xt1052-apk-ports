package com.nuance.dragon.toolkit.cloudservices;

import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.util.Logger;
import com.nuance.dragon.toolkit.util.internal.d;
import java.util.ArrayList;
import java.util.List;

public class AudioParam extends Param {
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final AudioSource f1331c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public final AudioSink f1332d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public a f1333e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public d f1334f;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public boolean f1335g = false;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public final List f1336h = new ArrayList();
    /* access modifiers changed from: private */
    public final Boolean[] i = new Boolean[1];

    public AudioParam(final String str, AudioSource audioSource) {
        super(1, str);
        d.a("source", (Object) audioSource);
        this.f1331c = audioSource;
        this.i[0] = new Boolean(false);
        this.f1332d = new AudioSink() {
            public final void chunksAvailable(AudioSource audioSource) {
                int chunksAvailableForSink = AudioParam.this.f1331c.getChunksAvailableForSink(this);
                while (true) {
                    int i = chunksAvailableForSink - 1;
                    if (chunksAvailableForSink > 0) {
                        try {
                            if (AudioParam.this.f1333e != null) {
                                AudioParam.this.f1334f.a(str, (AudioChunk) AudioParam.this.f1331c.getAudioChunkForSink(AudioParam.this.f1332d), false);
                                chunksAvailableForSink = i;
                            } else {
                                AudioParam.this.f1336h.add(AudioParam.this.f1331c.getAudioChunkForSink(AudioParam.this.f1332d));
                                chunksAvailableForSink = i;
                            }
                        } catch (Exception e2) {
                            Logger.error(AudioParam.this, "Error sending audio for param " + str, e2);
                            AudioParam.this.f1335g = true;
                            disconnectAudioSource();
                            if (AudioParam.this.f1333e != null) {
                                AudioParam.this.f1333e.a(AudioParam.this, true);
                                AudioParam.this.f1333e = null;
                            } else {
                                AudioParam.this.i[0] = new Boolean(true);
                            }
                            AudioParam.this.f1334f = null;
                            return;
                        }
                    } else {
                        return;
                    }
                }
            }

            public final void framesDropped(AudioSource audioSource) {
            }

            public final void sourceClosed(AudioSource audioSource) {
                boolean z = false;
                AudioParam.this.f1335g = true;
                disconnectAudioSource();
                try {
                    if (AudioParam.this.f1333e != null) {
                        AudioParam.this.f1334f.a(str, (AudioChunk) null, true);
                    }
                } catch (Exception e2) {
                    AudioParam.this.i[0] = new Boolean(true);
                    Logger.error(AudioParam.this, "Error sending audio for param " + str, e2);
                    z = true;
                }
                if (AudioParam.this.f1333e != null) {
                    AudioParam.this.f1333e.a(AudioParam.this, z);
                    AudioParam.this.f1333e = null;
                }
                AudioParam.this.f1334f = null;
            }
        };
        this.f1332d.connectAudioSource(audioSource);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        this.f1332d.disconnectAudioSource();
        this.f1333e = null;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(d dVar, a aVar) {
        dVar.a(this.f1355a);
        if (!this.f1335g) {
            this.f1333e = aVar;
            this.f1334f = dVar;
        }
        if (this.f1336h.size() > 0) {
            for (AudioChunk a2 : this.f1336h) {
                dVar.a(this.f1355a, a2, false);
            }
        }
        if (this.f1335g) {
            dVar.a(this.f1355a, (AudioChunk) null, true);
            aVar.a(this, this.i[0].booleanValue());
            return this.f1335g;
        }
        int chunksAvailableForSink = this.f1331c.getChunksAvailableForSink(this.f1332d);
        while (true) {
            int i2 = chunksAvailableForSink - 1;
            if (chunksAvailableForSink <= 0) {
                return this.f1335g;
            }
            dVar.a(this.f1355a, (AudioChunk) this.f1331c.getAudioChunkForSink(this.f1332d), this.f1335g && i2 == 0);
            chunksAvailableForSink = i2;
        }
    }
}
