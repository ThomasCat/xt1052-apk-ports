package com.motorola.audiomonitor;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.media.AudioRecord;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.motorola.audiomonitor.comsocket.DownSampler;
import java.io.FileNotFoundException;
import java.util.List;

public class AudioProvider extends ContentProvider {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f131a = ("AMonitor." + AudioProvider.class.getSimpleName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final boolean f132b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private static final UriMatcher f133c = new UriMatcher(-1);
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public DownSampler f134d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public ProtectedByteBuffer f135e;

    static {
        f133c.addURI("com.motorola.audiomonitor.raw_audio", "command_audio/*", 1);
    }

    private ParcelFileDescriptor a(Uri uri) {
        return openPipeHelper(uri, a(), null, null, new c(this));
    }

    static String a() {
        return "audio/l16";
    }

    /* access modifiers changed from: private */
    public String a(String str) {
        return str.substring(str.lastIndexOf("/") + 1);
    }

    public static void a(String str, List list) {
        d.a(str, list);
    }

    public int delete(Uri uri, String str, String[] strArr) {
        throw new UnsupportedOperationException();
    }

    public String getType(Uri uri) {
        if (f132b) {
            Log.i(f131a, "getType: " + uri.toString());
        }
        if (f133c.match(uri) == 1) {
            return a();
        }
        return null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        throw new UnsupportedOperationException();
    }

    public boolean onCreate() {
        this.f134d = new DownSampler(16000, 8000);
        int minBufferSize = AudioRecord.getMinBufferSize(16000, 16, 2);
        if (minBufferSize < 0) {
            Log.e(f131a, "getMinBufferSize failed");
            throw new IllegalStateException("getMinBufferSize failed");
        }
        this.f135e = new ProtectedByteBuffer(minBufferSize * 2, 8000);
        return true;
    }

    public ParcelFileDescriptor openFile(Uri uri, String str) {
        if (f132b) {
            Log.d(f131a, "Opening File: " + uri + ", mode=" + str);
        }
        if (f133c.match(uri) == 1 && "r".equals(str)) {
            return a(uri);
        }
        throw new FileNotFoundException("Unmatched URL: " + uri.getPath());
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        throw new UnsupportedOperationException();
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        throw new UnsupportedOperationException();
    }
}
