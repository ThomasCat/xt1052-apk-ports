package com.nuance.dragon.toolkit.audio.util;

public final class a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public c f1320a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public c f1321b;

    /* renamed from: c reason: collision with root package name */
    private int f1322c;
    /* access modifiers changed from: private */

    /* renamed from: d reason: collision with root package name */
    public int f1323d;

    /* renamed from: com.nuance.dragon.toolkit.audio.util.a$a reason: collision with other inner class name */
    public final class C0005a {

        /* renamed from: b reason: collision with root package name */
        private final b f1325b;

        /* renamed from: c reason: collision with root package name */
        private int f1326c;

        C0005a(b bVar, int i) {
            this.f1325b = bVar;
            this.f1326c = i;
        }

        public final Object a() {
            Object obj = null;
            c cVar = this.f1325b.f1329c;
            while (true) {
                if (cVar == null) {
                    cVar = null;
                    break;
                } else if (cVar.f1327a == 0) {
                    break;
                } else {
                    cVar = cVar.f1329c;
                }
            }
            if (cVar != null) {
                b();
                obj = cVar.f1330d;
                if (a.this.f1320a == this.f1325b) {
                    a.this.f1320a = this.f1325b.f1329c;
                }
                this.f1325b.a();
                cVar.a(this.f1325b);
                if (this.f1325b.f1329c == null) {
                    a.this.f1321b = this.f1325b;
                }
                a.b(a.this);
                this.f1326c++;
            } else {
                b();
            }
            return obj;
        }

        public final int b() {
            return a.this.f1323d - this.f1326c;
        }

        public final void c() {
            if (a.this.f1320a == this.f1325b) {
                a.this.f1320a = this.f1325b.f1329c;
            }
            if (a.this.f1321b == this.f1325b) {
                a.this.f1321b = this.f1325b.f1328b;
            }
            this.f1325b.a();
            a.b(a.this);
        }
    }

    final class b extends c {
        b() {
            super(1, null);
        }
    }

    abstract class c {

        /* renamed from: a reason: collision with root package name */
        final int f1327a;

        /* renamed from: b reason: collision with root package name */
        c f1328b;

        /* renamed from: c reason: collision with root package name */
        c f1329c;
        /* access modifiers changed from: private */

        /* renamed from: d reason: collision with root package name */
        public final Object f1330d;

        c(int i, Object obj) {
            this.f1327a = i;
            this.f1330d = obj;
        }

        /* access modifiers changed from: 0000 */
        public final void a() {
            if (this.f1328b != null) {
                this.f1328b.f1329c = this.f1329c;
            }
            if (this.f1329c != null) {
                this.f1329c.f1328b = this.f1328b;
            }
            this.f1328b = null;
            this.f1329c = null;
        }

        /* access modifiers changed from: 0000 */
        public final void a(c cVar) {
            cVar.f1328b = this;
            if (this.f1329c != null) {
                this.f1329c.f1328b = cVar;
            }
            cVar.f1329c = this.f1329c;
            this.f1329c = cVar;
        }
    }

    final class d extends c {
        d(Object obj) {
            super(0, obj);
        }
    }

    private void a(c cVar) {
        if (this.f1320a == null) {
            this.f1320a = cVar;
        } else {
            this.f1321b.a(cVar);
        }
        this.f1321b = cVar;
    }

    static /* synthetic */ void b(a aVar) {
        while (aVar.f1320a != null && aVar.f1320a.f1327a != 1) {
            c cVar = aVar.f1320a.f1329c;
            aVar.f1320a.a();
            aVar.f1320a = cVar;
            aVar.f1322c++;
        }
    }

    public final C0005a a() {
        b bVar = new b();
        a((c) bVar);
        return new C0005a(bVar, this.f1323d);
    }

    public final void a(Object obj) {
        if (this.f1320a != null) {
            a((c) new d(obj));
            this.f1323d++;
        }
    }
}
