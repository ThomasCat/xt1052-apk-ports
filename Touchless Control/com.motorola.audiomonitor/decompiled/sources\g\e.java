package g;

import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;

public abstract class e extends Binder implements d {
    public e() {
        attachInterface(this, "com.motorola.audiomonitor.inputplugin.IInputPluginService");
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                parcel.enforceInterface("com.motorola.audiomonitor.inputplugin.IInputPluginService");
                a(parcel.readString(), b.a(parcel.readStrongBinder()), parcel.readInt() != 0);
                parcel2.writeNoException();
                return true;
            case 2:
                parcel.enforceInterface("com.motorola.audiomonitor.inputplugin.IInputPluginService");
                a();
                parcel2.writeNoException();
                return true;
            case 3:
                parcel.enforceInterface("com.motorola.audiomonitor.inputplugin.IInputPluginService");
                b();
                parcel2.writeNoException();
                return true;
            case 4:
                parcel.enforceInterface("com.motorola.audiomonitor.inputplugin.IInputPluginService");
                a(parcel.createByteArray());
                parcel2.writeNoException();
                return true;
            case 5:
                parcel.enforceInterface("com.motorola.audiomonitor.inputplugin.IInputPluginService");
                a(parcel.readFloat());
                parcel2.writeNoException();
                return true;
            case 1598968902:
                parcel2.writeString("com.motorola.audiomonitor.inputplugin.IInputPluginService");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
