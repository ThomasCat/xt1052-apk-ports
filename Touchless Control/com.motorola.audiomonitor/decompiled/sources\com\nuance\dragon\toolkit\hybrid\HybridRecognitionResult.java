package com.nuance.dragon.toolkit.hybrid;

import com.nuance.dragon.toolkit.recognition.InterpretedRecognition;
import com.nuance.dragon.toolkit.recognition.InterpretedRecognitionCombiner;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class HybridRecognitionResult implements JSONCompliant {

    /* renamed from: a reason: collision with root package name */
    private final InterpretedRecognition f1838a;

    /* renamed from: b reason: collision with root package name */
    private final InterpretedRecognition f1839b;

    /* renamed from: c reason: collision with root package name */
    private final InterpretedRecognition f1840c;

    /* renamed from: d reason: collision with root package name */
    private final ArrayList f1841d = new ArrayList();

    private HybridRecognitionResult(InterpretedRecognition interpretedRecognition, InterpretedRecognition interpretedRecognition2, InterpretedRecognition interpretedRecognition3, List list) {
        this.f1838a = interpretedRecognition;
        this.f1839b = interpretedRecognition2;
        this.f1840c = interpretedRecognition3;
        if (list != null) {
            this.f1841d.addAll(list);
        }
    }

    HybridRecognitionResult(InterpretedRecognition interpretedRecognition, InterpretedRecognition interpretedRecognition2, InterpretedRecognitionCombiner interpretedRecognitionCombiner, List list) {
        this.f1838a = interpretedRecognition;
        this.f1839b = interpretedRecognition2;
        this.f1840c = interpretedRecognitionCombiner.combine(interpretedRecognition, interpretedRecognition2);
        if (list != null) {
            this.f1841d.addAll(list);
        }
    }

    public static HybridRecognitionResult createFromJSON(JSONObject jSONObject) {
        ArrayList arrayList;
        JSONObject optJSONObject = jSONObject.optJSONObject("result");
        JSONObject optJSONObject2 = jSONObject.optJSONObject("local");
        JSONObject optJSONObject3 = jSONObject.optJSONObject("remote");
        JSONArray optJSONArray = jSONObject.optJSONArray("slots_to_update");
        if (optJSONArray != null) {
            ArrayList arrayList2 = new ArrayList(optJSONArray.length());
            for (int i = 0; i < optJSONArray.length(); i++) {
                arrayList2.add(optJSONArray.getString(i));
            }
            arrayList = arrayList2;
        } else {
            arrayList = null;
        }
        return new HybridRecognitionResult(optJSONObject2 == null ? null : InterpretedRecognition.createFromJSON(optJSONObject2), optJSONObject3 == null ? null : InterpretedRecognition.createFromJSON(optJSONObject3), optJSONObject == null ? null : InterpretedRecognition.createFromJSON(optJSONObject), (List) arrayList);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        HybridRecognitionResult hybridRecognitionResult = (HybridRecognitionResult) obj;
        if (this.f1838a == null) {
            if (hybridRecognitionResult.f1838a != null) {
                return false;
            }
        } else if (!this.f1838a.equals(hybridRecognitionResult.f1838a)) {
            return false;
        }
        if (this.f1839b == null) {
            if (hybridRecognitionResult.f1839b != null) {
                return false;
            }
        } else if (!this.f1839b.equals(hybridRecognitionResult.f1839b)) {
            return false;
        }
        if (this.f1840c == null) {
            if (hybridRecognitionResult.f1840c != null) {
                return false;
            }
        } else if (!this.f1840c.equals(hybridRecognitionResult.f1840c)) {
            return false;
        }
        return this.f1841d == null ? hybridRecognitionResult.f1841d == null : this.f1841d.equals(hybridRecognitionResult.f1841d);
    }

    public InterpretedRecognition getLocalResult() {
        return this.f1838a;
    }

    public InterpretedRecognition getRemoteResult() {
        return this.f1839b;
    }

    public InterpretedRecognition getResult() {
        return this.f1840c;
    }

    public List getSlotsToUpdate() {
        ArrayList arrayList = new ArrayList(this.f1841d.size());
        arrayList.addAll(this.f1841d);
        return arrayList;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f1839b == null ? 0 : this.f1839b.hashCode()) + (((this.f1838a == null ? 0 : this.f1838a.hashCode()) + 31) * 31)) * 31;
        if (this.f1840c != null) {
            i = this.f1840c.hashCode();
        }
        return hashCode + i;
    }

    public JSONObject toJSON() {
        b bVar = new b();
        bVar.a("result", (JSONCompliant) this.f1840c);
        if (this.f1838a != null) {
            bVar.a("local", (Object) this.f1838a.toJSON());
        }
        if (this.f1839b != null) {
            bVar.a("remote", (Object) this.f1839b.toJSON());
        }
        if (this.f1841d.size() > 0) {
            JSONArray jSONArray = new JSONArray();
            for (int i = 0; i < this.f1841d.size(); i++) {
                jSONArray.put(this.f1841d.get(i));
            }
            bVar.a("slots_to_update", (Object) jSONArray);
        }
        return bVar;
    }
}
