package com.motorola.audiomonitor.uis;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.actions.utils.an;

public class AOVSpeakNowStatusFragment extends Fragment {

    /* renamed from: a reason: collision with root package name */
    private View f611a;

    /* renamed from: b reason: collision with root package name */
    private TextView f612b;

    /* renamed from: c reason: collision with root package name */
    private int f613c = 0;

    /* renamed from: d reason: collision with root package name */
    private int f614d = 0;

    public synchronized void a(int i) {
        if (this.f612b != null) {
            this.f612b.setVisibility(0);
            this.f612b.setText(i);
            LayoutParams layoutParams = (LayoutParams) this.f612b.getLayoutParams();
            layoutParams.bottomMargin = this.f613c;
            this.f612b.setLayoutParams(layoutParams);
        }
    }

    public synchronized void a(String str) {
        if (this.f612b != null) {
            this.f612b.setVisibility(0);
            this.f612b.setText(str);
            LayoutParams layoutParams = (LayoutParams) this.f612b.getLayoutParams();
            layoutParams.bottomMargin = this.f613c;
            this.f612b.setLayoutParams(layoutParams);
        }
    }

    public int b(int i) {
        int a2 = an.a().a(i);
        a(a2);
        return a2;
    }

    public synchronized void b(String str) {
        if (this.f612b != null) {
            this.f612b.setVisibility(0);
            this.f612b.setText(str);
            LayoutParams layoutParams = (LayoutParams) this.f612b.getLayoutParams();
            layoutParams.bottomMargin = this.f614d;
            this.f612b.setLayoutParams(layoutParams);
        }
    }

    public synchronized View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.f611a = layoutInflater.inflate(C0014R.layout.aov_speaknow_status_view, null);
        this.f612b = (TextView) this.f611a.findViewById(C0014R.id.speak_status_information);
        this.f613c = getActivity().getResources().getDimensionPixelSize(C0014R.dimen.status_info_marginBottom);
        this.f614d = getActivity().getResources().getDimensionPixelSize(C0014R.dimen.status_error_marginBottom);
        return this.f611a;
    }

    public void onDestroy() {
        super.onDestroy();
    }
}
