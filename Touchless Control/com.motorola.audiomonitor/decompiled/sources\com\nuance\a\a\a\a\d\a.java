package com.nuance.a.a.a.a.d;

public final class a {

    /* renamed from: a reason: collision with root package name */
    static byte[] f797a = new byte[64];

    /* renamed from: b reason: collision with root package name */
    static String f798b = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    static {
        for (int i = 0; i < 64; i++) {
            f797a[i] = (byte) f798b.charAt(i);
        }
    }

    public static String a(byte[] bArr) {
        int i;
        int length = bArr.length;
        byte[] bArr2 = new byte[(((length + 2) / 3) * 4)];
        int i2 = length + 0;
        int i3 = 0;
        byte b2 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i3 < i2) {
            byte b3 = bArr[i3];
            i4++;
            switch (i4) {
                case 1:
                    i = i5 + 1;
                    bArr2[i5] = f797a[(b3 >> 2) & 63];
                    break;
                case 2:
                    i = i5 + 1;
                    bArr2[i5] = f797a[((b2 << 4) & 48) | ((b3 >> 4) & 15)];
                    break;
                case 3:
                    int i6 = i5 + 1;
                    bArr2[i5] = f797a[((b2 << 2) & 60) | ((b3 >> 6) & 3)];
                    int i7 = i6 + 1;
                    bArr2[i6] = f797a[b3 & 63];
                    i = i7;
                    i4 = 0;
                    break;
                default:
                    i = i5;
                    break;
            }
            i3++;
            b2 = b3;
            i5 = i;
        }
        switch (i4) {
            case 1:
                int i8 = i5 + 1;
                bArr2[i5] = f797a[(b2 << 4) & 48];
                int i9 = i8 + 1;
                bArr2[i8] = 61;
                bArr2[i9] = 61;
                break;
            case 2:
                int i10 = i5 + 1;
                bArr2[i5] = f797a[(b2 << 2) & 60];
                bArr2[i10] = 61;
                break;
        }
        return new String(bArr2);
    }
}
