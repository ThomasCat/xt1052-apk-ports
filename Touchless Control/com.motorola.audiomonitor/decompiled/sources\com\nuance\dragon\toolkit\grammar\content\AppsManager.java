package com.nuance.dragon.toolkit.grammar.content;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import com.nuance.dragon.toolkit.file.FileManager;
import com.nuance.dragon.toolkit.grammar.Word;
import com.nuance.dragon.toolkit.grammar.WordAction;
import com.nuance.dragon.toolkit.grammar.content.ContentManager.PlatformFullIterator;
import java.util.List;

public class AppsManager extends ContentManager {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final Context f1737a;

    /* renamed from: b reason: collision with root package name */
    private final BroadcastReceiver f1738b;

    public AppsManager(String str, FileManager fileManager, Context context) {
        this(str, fileManager, false, false, context);
    }

    public AppsManager(String str, FileManager fileManager, boolean z, Context context) {
        this(str, fileManager, false, z, context);
    }

    public AppsManager(String str, FileManager fileManager, boolean z, boolean z2, Context context) {
        super(str, fileManager, z2, context);
        this.f1737a = context.getApplicationContext();
        this.f1738b = new BroadcastReceiver() {
            public final void onReceive(Context context, Intent intent) {
                AppsManager.this.onContentUpdate();
            }
        };
        initialize(false);
    }

    /* access modifiers changed from: protected */
    public PlatformFullIterator getPlatformFullIterator() {
        PackageManager packageManager = this.f1737a.getPackageManager();
        Intent intent = new Intent();
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.setAction("android.intent.action.MAIN");
        final List queryIntentActivities = packageManager.queryIntentActivities(intent, 268435456);
        return new PlatformFullIterator() {

            /* renamed from: c reason: collision with root package name */
            private final List f1742c = queryIntentActivities;

            /* renamed from: d reason: collision with root package name */
            private int f1743d;

            public final int getSize() {
                return this.f1742c.size();
            }

            public final boolean hasNext() {
                return this.f1743d < this.f1742c.size();
            }

            public final /* synthetic */ Object next() {
                List list = this.f1742c;
                int i = this.f1743d;
                this.f1743d = i + 1;
                return new WordAction(new Word(((ResolveInfo) list.get(i)).loadLabel(AppsManager.this.f1737a.getPackageManager()).toString()), true);
            }

            public final void remove() {
            }
        };
    }

    /* access modifiers changed from: protected */
    public void stopWatchingForContentUpdates(Context context) {
        context.unregisterReceiver(this.f1738b);
    }

    /* access modifiers changed from: protected */
    public void watchForContentUpdates(Context context) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addDataScheme("package");
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addAction("android.intent.action.PACKAGE_CHANGED");
        intentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
        context.registerReceiver(this.f1738b, intentFilter);
    }
}
