package com.nuance.a.a.a.b.c.b.b;

import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.b.c.c.c;
import com.nuance.a.a.a.b.c.c.l;
import com.nuance.dragon.toolkit.data.Data;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.Frequencies;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

public class i extends f implements c {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f866a = a.a(i.class);

    /* renamed from: b reason: collision with root package name */
    private Hashtable f867b = new Hashtable();

    public i() {
        super(Data.TYPE_DICT);
    }

    public i(byte[] bArr) {
        super(Data.TYPE_DICT);
        if (bArr.length <= 0) {
            return;
        }
        if ((bArr[0] & 255) != 224) {
            f866a.e("PDXDictionary() Expected a dictionary. ");
            return;
        }
        int a2 = a(bArr, 1);
        byte[] bArr2 = new byte[a2];
        System.arraycopy(bArr, bArr.length - a2, bArr2, 0, bArr2.length);
        b(bArr2);
    }

    public i(byte[] bArr, byte b2) {
        super(Data.TYPE_DICT);
        b(bArr);
    }

    private void b(byte[] bArr) {
        int i = 0;
        while (i < bArr.length) {
            int i2 = i + 1;
            byte b2 = bArr[i] & 255;
            if (b2 == 22) {
                int a2 = a(bArr, i2);
                int a3 = i2 + a(a2);
                byte[] bArr2 = new byte[a2];
                System.arraycopy(bArr, a3, bArr2, 0, bArr2.length);
                int i3 = a2 + a3;
                String str = new String(bArr2);
                int i4 = i3 + 1;
                byte b3 = bArr[i3] & 255;
                int a4 = a(bArr, i4);
                int a5 = i4 + a(a4);
                byte[] bArr3 = new byte[a4];
                System.arraycopy(bArr, a5, bArr3, 0, bArr3.length);
                i = a4 + a5;
                switch (b3) {
                    case 4:
                        this.f867b.put(str, new d(bArr3));
                        break;
                    case 5:
                        this.f867b.put(str, new n());
                        break;
                    case 16:
                        this.f867b.put(str, new w(bArr3));
                        break;
                    case Frequencies.FREQ_22KHZ /*22*/:
                        this.f867b.put(str, new b(bArr3));
                        break;
                    case 192:
                        this.f867b.put(str, new k(bArr3));
                        break;
                    case 193:
                        this.f867b.put(str, new aa(bArr3));
                        break;
                    case 224:
                        this.f867b.put(str, new i(bArr3, 0));
                        break;
                    default:
                        f866a.e("PDXDictionary.setContent() Unknown PDXClass type: " + b3 + ". ");
                        break;
                }
            } else {
                f866a.e("PDXDictionary.setContent() Expected an ASCII string but got " + b2 + ". ");
                return;
            }
        }
    }

    public final Enumeration a() {
        return this.f867b.keys();
    }

    /* access modifiers changed from: protected */
    public final void a(String str, int i) {
        this.f867b.put(str, new k(i));
    }

    public final void a(String str, c cVar) {
        if (str == null || cVar == null) {
            throw new IllegalArgumentException("key or value is null.");
        } else if (((f) cVar).c() != 224) {
            f866a.e("PDXDictionary.addDictionary() value is not a valid dictionary.");
            throw new IllegalArgumentException("value is not a valid dictionary. ");
        } else {
            this.f867b.put(str, cVar);
        }
    }

    public final void a(String str, l lVar) {
        if (str == null || lVar == null) {
            throw new IllegalArgumentException("key or value is null.");
        } else if (((f) lVar).c() != 16) {
            f866a.e("PDXDictionary.addSequence() value is not a valid sequence.");
            throw new IllegalArgumentException("value is not a valid sequence. ");
        } else {
            this.f867b.put(str, lVar);
        }
    }

    public final void a(String str, String str2) {
        if (str == null || str2 == null) {
            throw new IllegalArgumentException("key or value is null.");
        }
        this.f867b.put(str, new b(str2));
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2, short s) {
        switch (s) {
            case Frequencies.FREQ_22KHZ /*22*/:
                this.f867b.put(str, new b(str2));
                return;
            case 193:
                this.f867b.put(str, new aa(str2));
                return;
            default:
                return;
        }
    }

    public final void a(String str, byte[] bArr) {
        if (str == null || bArr == null) {
            throw new IllegalArgumentException("key or value is null.");
        }
        this.f867b.put(str, new d(bArr));
    }

    /* access modifiers changed from: protected */
    public final void a(String str, byte[] bArr, short s) {
        switch (s) {
            case 4:
                this.f867b.put(str, new d(bArr));
                return;
            case 5:
                this.f867b.put(str, new n());
                return;
            case 16:
                this.f867b.put(str, new w(bArr));
                return;
            case Frequencies.FREQ_22KHZ /*22*/:
                this.f867b.put(str, new b(bArr));
                return;
            case 192:
                this.f867b.put(str, new k(bArr));
                return;
            case 193:
                this.f867b.put(str, new aa(bArr));
                return;
            case 224:
                this.f867b.put(str, new i(bArr, 0));
                return;
            default:
                f866a.e("PDXDictionary.put() Unknown PDXClass type: " + s + ". ");
                return;
        }
    }

    public final boolean a(String str) {
        if (str != null) {
            return this.f867b.containsKey(str);
        }
        throw new NullPointerException("PDXDictionary.containsKey key is null");
    }

    /* access modifiers changed from: protected */
    public final f b(String str) {
        return (f) this.f867b.get(str);
    }

    /* access modifiers changed from: protected */
    public final String b(int i) {
        String str;
        String str2;
        String str3;
        String str4 = "";
        String str5 = "";
        String str6 = "";
        int i2 = 0;
        while (true) {
            str = str5;
            if (i2 >= i - 1) {
                break;
            }
            str5 = str + "    ";
            i2++;
        }
        String str7 = i > 0 ? str + "    " : str6;
        Enumeration keys = this.f867b.keys();
        String str8 = i != 0 ? str4 + "{ \n" : str4;
        while (keys.hasMoreElements()) {
            String str9 = (String) keys.nextElement();
            f fVar = (f) this.f867b.get(str9);
            switch (fVar.c()) {
                case 4:
                    str3 = str8 + str7 + str9 + ": <BYTES> \"" + ((d) fVar).a() + "\"\n";
                    continue;
                case 5:
                    str3 = str8 + str7 + str9 + ": <NULL> \n";
                    continue;
                case 16:
                    str2 = str8 + str7 + str9 + ": " + ((w) fVar).j(i + 1) + "\n";
                    break;
                case Frequencies.FREQ_22KHZ /*22*/:
                    str3 = str8 + str7 + str9 + ": <ASCII> \"" + ((b) fVar).a() + "\"\n";
                    continue;
                case 192:
                    str3 = str8 + str7 + str9 + ": <INT> " + ((k) fVar).a() + "\n";
                    continue;
                case 193:
                    str3 = str8 + str7 + str9 + ": <UTF8> \"" + ((aa) fVar).a() + "\"\n";
                    continue;
                case 224:
                    str3 = str8 + str7 + str9 + ": " + ((i) fVar).b(i + 1) + "\n";
                    continue;
                default:
                    str2 = str8;
                    break;
            }
            str3 = str2;
        }
        return i != 0 ? str8 + str + "} " : str8;
    }

    public final void b(String str, int i) {
        if (str == null) {
            throw new IllegalArgumentException("key or value is null.");
        }
        this.f867b.put(str, new k(i));
    }

    public final void b(String str, String str2) {
        if (str == null || str2 == null) {
            throw new IllegalArgumentException("key or value is null.");
        }
        this.f867b.put(str, new aa(str2));
    }

    public final byte[] b() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Enumeration keys = this.f867b.keys();
        while (keys.hasMoreElements()) {
            String str = (String) keys.nextElement();
            try {
                byteArrayOutputStream.write(new b(str).b());
                f fVar = (f) this.f867b.get(str);
                switch (fVar.c()) {
                    case 4:
                        byteArrayOutputStream.write(((d) fVar).b());
                        break;
                    case 5:
                        byteArrayOutputStream.write(((n) fVar).a());
                        break;
                    case 16:
                        byteArrayOutputStream.write(((w) fVar).b());
                        break;
                    case Frequencies.FREQ_22KHZ /*22*/:
                        byteArrayOutputStream.write(((b) fVar).b());
                        break;
                    case 192:
                        byteArrayOutputStream.write(((k) fVar).b());
                        break;
                    case 193:
                        byteArrayOutputStream.write(((aa) fVar).b());
                        break;
                    case 224:
                        byteArrayOutputStream.write(((i) fVar).d());
                        break;
                }
            } catch (IOException e2) {
                f866a.e("PDXDictionary.getContent() " + e2.toString() + ". ");
            }
        }
        return byteArrayOutputStream.toByteArray();
    }

    public final short c(String str) {
        if (str != null) {
            return ((f) this.f867b.get(str)).c();
        }
        throw new IllegalArgumentException("key is null.");
    }

    public final int d(String str) {
        if (str == null) {
            throw new IllegalArgumentException("key is null.");
        }
        f fVar = (f) this.f867b.get(str);
        if (fVar == null) {
            if (f866a.e()) {
                f866a.e("PDXDictionary.getInteger() " + str + " does not exist. ");
            }
            throw new RuntimeException("key does not exist. ");
        } else if (fVar.c() == 192) {
            return ((k) fVar).a();
        } else {
            if (f866a.e()) {
                f866a.e("PDXDictionary.getInteger() " + str + " is not a PDXInteger. ");
            }
            throw new RuntimeException("key is of wrong type. ");
        }
    }

    public byte[] d() {
        return super.a(b());
    }

    public final byte[] e(String str) {
        if (str == null) {
            throw new IllegalArgumentException("key is null.");
        }
        f fVar = (f) this.f867b.get(str);
        if (fVar == null) {
            f866a.e("PDXDictionary.getByteString() " + str + " does not exist. ");
            throw new RuntimeException("key does not exist. ");
        } else if (fVar.c() == 4) {
            return ((d) fVar).a();
        } else {
            f866a.e("PDXDictionary.getByteString() " + str + " is not a PDXByteString. ");
            throw new RuntimeException("key is of wrong type. ");
        }
    }

    public final String f(String str) {
        if (str == null) {
            throw new IllegalArgumentException("key is null.");
        }
        f fVar = (f) this.f867b.get(str);
        if (fVar == null) {
            f866a.e("PDXDictionary.getUTF8String() " + str + " does not exist. ");
            throw new RuntimeException("key does not exist. ");
        } else if (fVar.c() == 193) {
            return ((aa) fVar).a();
        } else {
            f866a.e("PDXDictionary.getUTF8String() " + str + " is not a PDXUTF8String. ");
            throw new RuntimeException("key is of wrong type. ");
        }
    }

    public final String g(String str) {
        if (str == null) {
            throw new IllegalArgumentException("key is null.");
        }
        f fVar = (f) this.f867b.get(str);
        if (fVar == null) {
            f866a.e("PDXDictionary.getAsciiString() " + str + " does not exist. ");
            throw new RuntimeException("key does not exist. ");
        } else if (fVar.c() == 22) {
            return ((b) fVar).a();
        } else {
            f866a.e("PDXDictionary.getAsciiString() " + str + " is not a PDXAsciiString. ");
            throw new RuntimeException("key is of wrong type. ");
        }
    }

    public final c h(String str) {
        if (str == null) {
            throw new IllegalArgumentException("key is null.");
        }
        f fVar = (f) this.f867b.get(str);
        if (fVar == null) {
            f866a.e("PDXDictionary.getDictionary() " + str + " does not exist. ");
            throw new RuntimeException("key does not exist. ");
        } else if (fVar.c() == 224) {
            return (i) fVar;
        } else {
            f866a.e("PDXDictionary.getDictionary() " + str + " is not a PDXDictionary. ");
            throw new RuntimeException("key is of wrong type. ");
        }
    }

    public final l i(String str) {
        if (str == null) {
            throw new IllegalArgumentException("key is null.");
        }
        f fVar = (f) this.f867b.get(str);
        if (fVar == null) {
            f866a.e("PDXDictionary.getSequence() " + str + " does not exist. ");
            throw new RuntimeException("key does not exist. ");
        } else if (fVar.c() == 16) {
            return (w) fVar;
        } else {
            f866a.e("PDXDictionary.getSequence() " + str + " is not a PDXSequence. ");
            throw new RuntimeException("key is of wrong type. ");
        }
    }

    public final String toString() {
        return b(0);
    }
}
