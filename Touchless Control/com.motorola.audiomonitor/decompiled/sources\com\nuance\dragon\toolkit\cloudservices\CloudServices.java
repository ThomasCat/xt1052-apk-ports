package com.nuance.dragon.toolkit.cloudservices;

import android.content.Context;

public abstract class CloudServices {

    public enum ConnectionState {
        CONNECTED,
        DISCONNECTED
    }

    public static final CloudServices createCloudImpersonator() {
        return new CloudServicesImpersonator();
    }

    public static final CloudServices createCloudServices(Context context, CloudConfig cloudConfig) {
        return new c(context, cloudConfig);
    }

    public abstract void addTransaction(Transaction transaction, int i);

    public abstract Transaction currentTransaction();

    public abstract ConnectionState getConnectionState();

    public abstract String getSessionID();

    public abstract int getTransactionCount();

    public abstract String getUniqueID();

    public abstract void release();
}
