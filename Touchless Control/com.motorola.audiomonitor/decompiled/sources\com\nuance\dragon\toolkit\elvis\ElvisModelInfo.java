package com.nuance.dragon.toolkit.elvis;

public final class ElvisModelInfo {
    public final int frequencyHz;
    public final ElvisLanguage language;

    ElvisModelInfo(ElvisLanguage elvisLanguage, int i) {
        this.language = elvisLanguage;
        this.frequencyHz = i;
    }

    public final String toString() {
        return this.language.toString() + ", " + this.frequencyHz + "Hz";
    }
}
