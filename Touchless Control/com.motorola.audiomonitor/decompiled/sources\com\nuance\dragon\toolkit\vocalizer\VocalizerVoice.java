package com.nuance.dragon.toolkit.vocalizer;

import com.nuance.dragon.toolkit.util.JSONCompliant;
import com.nuance.dragon.toolkit.util.a.b;
import org.json.JSONException;
import org.json.JSONObject;

public final class VocalizerVoice implements JSONCompliant {
    public final String description;
    public final VocalizerLanguage language;
    public final String name;

    VocalizerVoice(String str, String str2, VocalizerLanguage vocalizerLanguage) {
        this.name = str;
        this.description = str2;
        this.language = vocalizerLanguage;
    }

    static VocalizerVoice a(String str) {
        VocalizerVoice[] vocalizerVoiceArr;
        for (VocalizerVoice vocalizerVoice : d.ALL_VOICES) {
            if (vocalizerVoice.name.equalsIgnoreCase(str)) {
                return vocalizerVoice;
            }
        }
        return null;
    }

    public static VocalizerVoice createFromJSON(JSONObject jSONObject) {
        String string = jSONObject.getString("name");
        VocalizerVoice a2 = a(string);
        if (a2 != null) {
            return a2;
        }
        throw new JSONException("Invalid voice name: " + string);
    }

    public final boolean equals(Object obj) {
        if (obj == null || !(obj instanceof VocalizerVoice)) {
            return false;
        }
        return this.name.equals(((VocalizerVoice) obj).name);
    }

    public final int hashCode() {
        return this.name.hashCode();
    }

    public final JSONObject toJSON() {
        b bVar = new b();
        bVar.a("name", (Object) this.name);
        return bVar;
    }

    public final String toString() {
        return this.description;
    }
}
