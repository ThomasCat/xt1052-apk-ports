package a;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import b.b;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.a;
import com.motorola.audiomonitor.actions.utils.ac;
import com.motorola.audiomonitor.actions.utils.ad;
import com.motorola.audiomonitor.actions.utils.ae;
import com.motorola.audiomonitor.actions.utils.f;
import com.motorola.audiomonitor.actions.utils.h;
import com.motorola.audiomonitor.actions.utils.j;
import com.motorola.audiomonitor.actions.utils.m;
import com.motorola.audiomonitor.uis.am;
import com.motorola.audiomonitor.uis.d;
import com.nuance.dragon.toolkit.elvis.ElvisResult;
import h.k;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class g extends r {

    /* renamed from: a reason: collision with root package name */
    private static final String f30a = ("AMonitor." + g.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f31b = a.f160b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final j f32c = new h(this);

    public g(int i, Context context) {
        super(i, context);
    }

    private o a(e eVar) {
        List c2 = eVar.c();
        Resources resources = c().getResources();
        StringBuilder sb = new StringBuilder();
        ad adVar = ad.RESPONSE_POSITIVE;
        if (c2.size() == 1) {
            com.motorola.audiomonitor.actions.utils.a aVar = (com.motorola.audiomonitor.actions.utils.a) c2.get(0);
            boolean a2 = a(aVar);
            if (f31b) {
                Log.d(f30a, " con=" + aVar.i() + " pro=" + aVar.h());
            }
            String c3 = aVar.c();
            int e2 = aVar.e();
            String formatNumber = PhoneNumberUtils.formatNumber(aVar.g());
            String a3 = f.a(c(), aVar);
            sb.append(resources.getString(a2 ? C0014R.string.say_call_to_phone : C0014R.string.call_action_handler_call_confirmation));
            sb.append(" ");
            if (!TextUtils.isEmpty(c3)) {
                switch (e2) {
                    case ElvisResult.GATE_CONFIDENCE_UNAVAILABLE /*-1*/:
                    case 19:
                        sb.append(a(resources, C0014R.string.call_action_handler_contact_confirm_notype, c3, a3));
                        break;
                    case 1:
                        sb.append(a(resources, C0014R.string.call_action_handler_contact_confirm_home, c3, a3));
                        break;
                    case 3:
                        sb.append(a(resources, C0014R.string.call_action_handler_contact_confirm_work, c3, a3));
                        break;
                    default:
                        sb.append(a(resources, C0014R.string.call_action_handler_contact_confirm, c3, a3));
                        break;
                }
            } else {
                sb.append(a(resources, C0014R.string.call_action_handler_contact_confirm_notype, k.h(formatNumber), null));
            }
            h.g.a().o();
            ad a4 = eVar.a(ac.EOS_DISMISS_BY_CONFIRM_CALL, (a2 ? 10000 : 3000) + 1000, new am(c(), eVar, a2 ? 10000 : 3000, false, true), Arrays.asList(new String[]{sb.toString()}));
            if (a4 == ad.RESPONSE_POSITIVE || (!a2 && a4 == ad.RESPONSE_TIMEOUT)) {
                if (m.a().a(true, eVar)) {
                    if (f31b) {
                        Log.d(f30a, "call response : key guard cancel");
                    }
                    return o.ABORTED;
                } else if (h.a().d() || h.a().e()) {
                    if (f31b) {
                        Log.d(f30a, "call response : incoming call cancel");
                    }
                    return o.ABORTED;
                } else {
                    b(aVar);
                    return o.CONSUMED;
                }
            } else if (f31b) {
                Log.d(f30a, "call response : user cancel");
            }
        } else {
            am amVar = new am(c(), eVar, 3000, true, true);
            d.f691b = -1;
            d.f692c = false;
            Iterator it = c2.iterator();
            int i = 0;
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                com.motorola.audiomonitor.actions.utils.a aVar2 = (com.motorola.audiomonitor.actions.utils.a) it.next();
                ArrayList arrayList = new ArrayList();
                if (i == 0) {
                    arrayList.add(resources.getString(C0014R.string.call_speech_multiple_contacts, new Object[]{Integer.valueOf(c2.size())}));
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append(aVar2.c());
                if (a(aVar2, c2)) {
                    String a5 = f.a(c(), aVar2);
                    if (!a5.isEmpty()) {
                        sb2.append(" ");
                        sb2.append(a5);
                    }
                }
                arrayList.add(sb2.toString());
                h.g.a().o();
                d.f690a = i;
                ad a6 = eVar.a(ac.EOS_CONFIRM_CALL, 4000, amVar, arrayList);
                if (a6 != ad.RESPONSE_TIMEOUT) {
                    amVar.e();
                }
                if (a6 == ad.RESPONSE_POSITIVE) {
                    if (m.a().a(true, eVar)) {
                        if (f31b) {
                            Log.d(f30a, "call response : key guard cancel");
                        }
                        return o.ABORTED;
                    } else if (h.a().d() || h.a().e()) {
                        if (f31b) {
                            Log.d(f30a, "call response : incoming call cancel");
                        }
                        return o.ABORTED;
                    } else {
                        b((d.f691b < 0 || d.f691b >= c2.size()) ? aVar2 : (com.motorola.audiomonitor.actions.utils.a) c2.get(d.f691b));
                        return o.CONSUMED;
                    }
                } else if (a6 == ad.RESPONSE_NEGATIVE) {
                    if (f31b) {
                        Log.d(f30a, "call response : user cancel");
                    }
                    return o.ABORTED;
                } else if (a6 == ad.RESPONSE_ABORT) {
                    if (f31b) {
                        Log.d(f30a, "call response : aborted");
                    }
                    return o.ABORTED;
                } else {
                    if (a6 == ad.RESPONSE_TIMEOUT && f31b) {
                        Log.d(f30a, "prompt timeout" + i);
                    }
                    int i2 = i + 1;
                    if (i2 != 3) {
                        i = i2;
                    } else if (f31b) {
                        Log.d(f30a, "call response : user timeout");
                    }
                }
            }
            if (c2.size() > 3) {
                d.f690a = -1;
                String string = resources.getString(C0014R.string.say_touch_more);
                amVar.a(10000);
                eVar.a(ac.EOS_DISMISS_BY_CONFIRM_CANCEL, 10000, amVar, Arrays.asList(new String[]{string}));
            }
            amVar.e();
        }
        return o.ABORTED;
    }

    private String a(Resources resources, int i, String str, String str2) {
        if (TextUtils.isEmpty(str2)) {
            return resources.getString(C0014R.string.call_action_handler_contact_confirm_notype, new Object[]{str});
        }
        return resources.getString(i, new Object[]{str, str2});
    }

    public static boolean a(com.motorola.audiomonitor.actions.utils.a aVar) {
        String str = a.r;
        if (str.equals("0")) {
            return false;
        }
        return str.equals("2") || !com.motorola.audiomonitor.actions.utils.g.a(aVar);
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.motorola.audiomonitor.actions.utils.a>, for r5v0, types: [java.util.List<com.motorola.audiomonitor.actions.utils.a>, java.util.List] */
    private boolean a(com.motorola.audiomonitor.actions.utils.a aVar, List<com.motorola.audiomonitor.actions.utils.a> list) {
        for (com.motorola.audiomonitor.actions.utils.a aVar2 : list) {
            if (aVar != aVar2 && aVar.c().equals(aVar2.c())) {
                return true;
            }
        }
        return false;
    }

    private void b(com.motorola.audiomonitor.actions.utils.a aVar) {
        h.g.a().a(true);
        ae aeVar = new ae(c());
        aeVar.a();
        aeVar.b();
        aeVar.a((int) C0014R.string.call_action_handler_call_confirmation);
        h.a().a(this.f32c);
        if (f31b) {
            Log.d(f30a, "call response : place call");
        }
        Intent intent = new Intent("android.intent.action.CALL");
        intent.addFlags(268435456);
        intent.setData(Uri.fromParts("tel", aVar.g(), null));
        c().startActivity(intent);
    }

    public o a(b bVar, e eVar) {
        c.d.a(c(), "num_call");
        if (eVar.c() != null) {
            return a(eVar);
        }
        Intent intent = new Intent("android.intent.action.DIAL");
        intent.addFlags(268435456);
        c().startActivity(intent);
        return o.CONSUMED;
    }
}
