package com.motorola.audiomonitor.uis.training;

import android.view.View;
import android.view.View.OnClickListener;

class n implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ TrainingTriggerRecordingActivity f753a;

    n(TrainingTriggerRecordingActivity trainingTriggerRecordingActivity) {
        this.f753a = trainingTriggerRecordingActivity;
    }

    public void onClick(View view) {
        view.setEnabled(false);
        this.f753a.l.removeCallbacks(this.f753a.S);
        this.f753a.v = null;
        this.f753a.finish();
    }
}
