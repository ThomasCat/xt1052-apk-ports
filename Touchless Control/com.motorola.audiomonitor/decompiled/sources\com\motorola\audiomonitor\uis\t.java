package com.motorola.audiomonitor.uis;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.motorola.audiomonitor.C0014R;

public class t extends Fragment {

    /* renamed from: a reason: collision with root package name */
    private View f720a;

    /* renamed from: b reason: collision with root package name */
    private TextView f721b;

    /* renamed from: c reason: collision with root package name */
    private TextView f722c;

    /* renamed from: d reason: collision with root package name */
    private TextView f723d;

    /* renamed from: e reason: collision with root package name */
    private ImageView f724e;

    public void a(int i) {
        if (i == 0) {
            this.f721b.setText(C0014R.string.welcome_title1);
            this.f722c.setText(C0014R.string.welcome_desc1);
            this.f723d.setText(C0014R.string.help_text_1);
            this.f724e.setImageResource(C0014R.drawable.vc_intro_example1);
        } else if (i == 1) {
            this.f721b.setText(C0014R.string.welcome_title2);
            this.f722c.setText(C0014R.string.welcome_desc2);
            this.f723d.setText(C0014R.string.help_text_2);
            this.f724e.setImageResource(C0014R.drawable.vc_intro_example2);
        } else if (i == 2) {
            this.f721b.setText(C0014R.string.welcome_title3);
            this.f722c.setText(C0014R.string.welcome_desc3);
            this.f723d.setText(C0014R.string.help_text_3);
            this.f724e.setImageResource(C0014R.drawable.vc_intro_example3);
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.f720a = layoutInflater.inflate(C0014R.layout.welcome_message, null);
        this.f721b = (TextView) this.f720a.findViewById(C0014R.id.text_body_title);
        this.f722c = (TextView) this.f720a.findViewById(C0014R.id.text_body_desc);
        this.f723d = (TextView) this.f720a.findViewById(C0014R.id.help_balloon_text_row2);
        this.f724e = (ImageView) this.f720a.findViewById(C0014R.id.welcome_image);
        return this.f720a;
    }
}
