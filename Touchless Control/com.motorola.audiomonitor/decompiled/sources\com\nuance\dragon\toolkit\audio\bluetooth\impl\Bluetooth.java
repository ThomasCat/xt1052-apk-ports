package com.nuance.dragon.toolkit.audio.bluetooth.impl;

import android.content.Context;
import android.util.Log;

public abstract class Bluetooth {
    public static final int HEADSET_AUDIO_STATE_CONNECTED = 1;
    public static final int HEADSET_AUDIO_STATE_DISCONNECTED = 0;
    public static final int HEADSET_STATE_CONNECTED = 1;
    public static final int HEADSET_STATE_DISCONNECTED = 0;
    protected Context mContext;

    public interface HeadsetStateListener {
        void onAudioStateChanged(int i);

        void onConnectionStateChanged(int i);

        void onServiceConnected(boolean z);

        void onServiceDisconnected();
    }

    protected Bluetooth(Context context) {
        this.mContext = context;
    }

    static void a(String str) {
        Log.d("DMT-Bluetooth", str);
    }

    public static Bluetooth createInstance(Context context) {
        int i = AndroidVersion.SDK;
        Bluetooth bluetooth = (i < 8 || AndroidVersion.IS_BROKEN_HTC) ? new c(context) : i < 9 ? new d(context) : i < 11 ? new a(context) : new b(context);
        Log.i("DMT-Bluetooth", "createInstance() returns " + bluetooth + "> for SDK version " + i);
        return bluetooth;
    }

    public abstract void close();

    public abstract int getAudioState();

    public abstract String[] getBondedDevices();

    public abstract int getHeadsetState();

    public abstract int getPlaybackStream();

    public abstract int getRecordingSource();

    public abstract boolean isEnabled();

    public abstract void setHeadsetStateListener(HeadsetStateListener headsetStateListener);

    public abstract boolean startBluetoothSco();

    public abstract void stopBluetoothSco();
}
