package com.nuance.dragon.toolkit.audio.util;

import com.nuance.dragon.toolkit.audio.AbstractAudioChunk.SpeechStatus;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.AudioType.Encoding;
import com.nuance.dragon.toolkit.util.Logger;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class SpeexDecoder {

    /* renamed from: a reason: collision with root package name */
    private AudioType f1308a;

    /* renamed from: b reason: collision with root package name */
    private AudioType f1309b;

    /* renamed from: c reason: collision with root package name */
    private int f1310c;

    /* renamed from: d reason: collision with root package name */
    private long f1311d;

    static {
        System.loadLibrary("dmt_speex");
    }

    private static native int decodeNative(long j, byte[] bArr, int i, int i2, OutputStream outputStream);

    private static native long initializeNative(int[] iArr, int[] iArr2, int i, int i2);

    private static native void releaseNative(long j);

    public final void a() {
        if (this.f1311d != 0) {
            releaseNative(this.f1311d);
            this.f1311d = 0;
        }
    }

    public final void a(AudioType audioType) {
        Encoding encoding = audioType.encoding;
        Encoding encoding2 = Encoding.SPEEX;
        if (audioType.frequency != 16000) {
            int i = audioType.frequency;
        }
        this.f1308a = audioType;
        this.f1309b = new AudioType(Encoding.PCM_16, this.f1308a.frequency);
        int i2 = audioType.frequency == 16000 ? 1 : 0;
        int i3 = audioType.frequency;
        int[] iArr = new int[1];
        int[] iArr2 = new int[1];
        if (this.f1311d != 0) {
            releaseNative(this.f1311d);
        }
        this.f1311d = initializeNative(iArr2, iArr, i2, i3);
        this.f1310c = iArr2[0];
    }

    public final AudioChunk[] a(byte[] bArr, int i, SpeechStatus speechStatus) {
        if (this.f1311d == 0) {
            Logger.error(this, "Speex decoder isn't initialized");
            return new AudioChunk[0];
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (decodeNative(this.f1311d, bArr, 0, i, byteArrayOutputStream) < 0) {
            return new AudioChunk[0];
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        ByteBuffer wrap = ByteBuffer.wrap(byteArray);
        wrap.order(ByteOrder.LITTLE_ENDIAN);
        short[] sArr = new short[(byteArray.length / 2)];
        wrap.asShortBuffer().get(sArr);
        return new AudioChunk[]{new AudioChunk(this.f1309b, sArr, speechStatus)};
    }
}
