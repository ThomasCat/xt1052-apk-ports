package com.nuance.a.a.a.b.c.b.b;

import java.io.ByteArrayOutputStream;

public class f {

    /* renamed from: a reason: collision with root package name */
    private short f863a;

    public f(short s) {
        this.f863a = s;
    }

    public static int a(int i) {
        if (i <= 127) {
            return 1;
        }
        if (i <= 255) {
            return 2;
        }
        return i <= 65535 ? 3 : 5;
    }

    public static int a(byte[] bArr, int i) {
        byte b2 = bArr[i] & 255;
        return b2 == 129 ? bArr[i + 1] & 255 : b2 == 130 ? ((bArr[i + 1] & 255) << 8) + (bArr[i + 2] & 255) : b2 == 132 ? ((bArr[i + 1] & 255) << 24) + ((bArr[i + 2] & 255) << 16) + ((bArr[i + 3] & 255) << 8) + (bArr[i + 4] & 255) : b2;
    }

    public final byte[] a(byte[] bArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(this.f863a & 255);
        if (bArr.length > 65535) {
            byteArrayOutputStream.write(132);
            byteArrayOutputStream.write((bArr.length >> 24) & 255);
            byteArrayOutputStream.write((bArr.length >> 16) & 255);
            byteArrayOutputStream.write((bArr.length >> 8) & 255);
            byteArrayOutputStream.write(bArr.length & 255);
        } else if (bArr.length > 255) {
            byteArrayOutputStream.write(130);
            byteArrayOutputStream.write((bArr.length >> 8) & 255);
            byteArrayOutputStream.write(bArr.length & 255);
        } else if (bArr.length > 127) {
            byteArrayOutputStream.write(129);
            byteArrayOutputStream.write(bArr.length);
        } else if (bArr.length >= 0) {
            byteArrayOutputStream.write(bArr.length);
        }
        byteArrayOutputStream.write(bArr, 0, bArr.length);
        return byteArrayOutputStream.toByteArray();
    }

    public final short c() {
        return this.f863a;
    }
}
