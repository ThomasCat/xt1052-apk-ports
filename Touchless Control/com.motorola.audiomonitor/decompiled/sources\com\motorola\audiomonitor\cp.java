package com.motorola.audiomonitor;

class cp extends Thread {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ cn f463a;

    cp(cn cnVar) {
        this.f463a = cnVar;
    }

    public void run() {
        cq cqVar;
        int i;
        synchronized (this.f463a) {
            if (this.f463a.i == null || !(this.f463a.i instanceof cq)) {
                cqVar = null;
            } else {
                cq cqVar2 = (cq) this.f463a.i;
                this.f463a.i = null;
                cqVar = cqVar2;
            }
        }
        if (cqVar != null) {
            i = cqVar.i();
            cqVar.c();
        } else {
            i = 0;
        }
        synchronized (this.f463a) {
            if (!this.f463a.f427f) {
                if (i == 0) {
                    this.f463a.f426e = true;
                    this.f463a.f428g.a(4, 0);
                } else {
                    this.f463a.a("ERROR: enrollment error=" + i);
                    this.f463a.a(i);
                    this.f463a.f428g.a(5, i);
                }
            }
        }
    }
}
