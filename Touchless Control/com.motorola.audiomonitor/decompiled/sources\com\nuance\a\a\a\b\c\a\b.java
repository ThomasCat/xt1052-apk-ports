package com.nuance.a.a.a.b.c.a;

import com.nuance.a.a.a.a.a.a.C0000a;
import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.b.b.c;
import java.util.Vector;

public class b {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f834a = a.a(b.class);

    private b() {
    }

    public static a a(String str, short s, String str2, byte[] bArr, String str3, C0000a aVar, C0000a aVar2, Vector vector, c cVar) {
        c cVar2;
        if (f834a.b()) {
            f834a.b((Object) "createManager");
        }
        if (str == null || str.length() == 0) {
            f834a.e("NullPointerException gatewayIP is NULL. ");
            throw new NullPointerException("gatewayIP must be provided!");
        } else if (s <= 0) {
            f834a.e("IllegalArgumentException gatewayPort is invalid. ");
            throw new IllegalArgumentException("gatewayPort invalid value!");
        } else if (str2 == null) {
            f834a.e("NullPointerException applicationId is NULL. ");
            throw new NullPointerException("Application id can not be null!");
        } else if (bArr == null) {
            f834a.e("NullPointerException appKey is NULL. ");
            throw new NullPointerException("Application key can not be null!");
        } else if (str3 == null) {
            f834a.e("NullPointerException uid is NULL. ");
            throw new NullPointerException("uid can not be null!");
        } else if (aVar == null) {
            f834a.e("NullPointerException inputCodec is NULL. ");
            throw new NullPointerException("inputCodec can not be null!");
        } else if (aVar2 == null) {
            f834a.e("NullPointerException outputCodec is NULL. ");
            throw new NullPointerException("outputCodec can not be null!");
        } else if (cVar == null) {
            f834a.e("NullPointerException managerListener is NULL. ");
            throw new NullPointerException("managerListener can not be null!");
        } else {
            if (vector != null) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= vector.size()) {
                        break;
                    }
                    cVar2 = (c) vector.elementAt(i2);
                    if (cVar2.d() == c.a.f830e || cVar2.d() == c.a.f831f || cVar2.d() == c.a.f832g || cVar2.d() == c.a.f833h || cVar2.d() == c.a.f829d || cVar2.d() == c.a.i || cVar2.d() == c.a.j) {
                        f834a.e("IllegalArgumentException Parameter type: " + cVar2.d() + " not allowed. ");
                    } else {
                        i = i2 + 1;
                    }
                }
                f834a.e("IllegalArgumentException Parameter type: " + cVar2.d() + " not allowed. ");
                throw new IllegalArgumentException("Parameter type: " + cVar2.d() + " not allowed. ");
            }
            return new com.nuance.a.a.a.b.c.b.a.a(str, s, str2, bArr, str3, aVar, aVar2, vector, cVar);
        }
    }
}
