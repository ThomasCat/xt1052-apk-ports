package com.nuance.a.a.a.c.a;

import android.content.Context;
import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;

public abstract class b {

    /* renamed from: c reason: collision with root package name */
    private static final C0001a f915c = a.a(b.class);

    /* renamed from: a reason: collision with root package name */
    protected Context f916a;

    /* renamed from: b reason: collision with root package name */
    protected d f917b;

    /* renamed from: d reason: collision with root package name */
    private Object f918d = new Object();

    protected b(Context context) {
        this.f916a = context;
        this.f917b = new d(this.f916a);
    }

    public static b a(Context context) {
        int i = a.f907a;
        return (i < 8 || a.f910d) ? new f(context) : i < 9 ? new g(context) : new e(context);
    }

    public final boolean a() {
        return this.f917b.a() != null;
    }

    public final void b() {
        if (this.f917b != null) {
            try {
                this.f917b.b();
            } catch (Throwable th) {
            }
            this.f917b = null;
        }
    }
}
