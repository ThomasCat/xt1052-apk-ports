package com.nuance.a.a.a.b.c.b.a;

import com.nuance.a.a.a.a.a.a.C0000a;
import com.nuance.a.a.a.a.b.a.a.C0001a;
import com.nuance.a.a.a.b.b.b;
import com.nuance.a.a.a.b.c.a.c;
import java.util.Vector;

public class a extends b implements com.nuance.a.a.a.b.c.a.a {

    /* renamed from: h reason: collision with root package name */
    private static final C0001a f835h = com.nuance.a.a.a.a.b.a.a.a(a.class);

    /* renamed from: g reason: collision with root package name */
    public String f836g;
    private b i;
    private Object j;

    public a(String str, short s, String str2, byte[] bArr, String str3, C0000a aVar, C0000a aVar2, Vector vector, c cVar) {
        Vector vector2;
        super(str, s, str3, aVar, aVar2);
        if (str2 == null) {
            throw new IllegalArgumentException(" application id is null.");
        }
        this.f836g = str2;
        if (bArr == null) {
            throw new IllegalArgumentException(" application key is null");
        }
        if (vector != null && vector.size() != 0) {
            vector2 = new Vector(vector.size());
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= vector.size()) {
                    break;
                }
                com.nuance.a.a.a.b.b.c cVar2 = (com.nuance.a.a.a.b.b.c) vector.elementAt(i3);
                if (cVar2.d() == com.nuance.a.a.a.b.b.c.a.f826a && cVar2.a().equals("DEVICE_CMD_LOG_TO_SERVER_ENABLED")) {
                    this.j = new Vector();
                }
                vector2.addElement(cVar2);
                i2 = i3 + 1;
            }
        } else {
            vector2 = new Vector();
        }
        this.i = new b(b(), c(), str2, bArr, str3, vector2, a(), cVar);
    }

    public final void c_() {
        f835h.b((Object) "shutdown");
        this.i.c();
    }

    public final b g() {
        return this.i;
    }

    public final Object h() {
        return this.j;
    }
}
