package com.nuance.dragon.toolkit.util.internal;

public final class f {
    public static boolean a(Object obj, Object obj2) {
        if (obj == null) {
            return obj == obj2;
        }
        if (obj2 != null) {
            return obj.equals(obj2);
        }
        return false;
    }

    public static short[] a(short[] sArr, int i, int i2) {
        if (i + i2 > sArr.length) {
            i2 = sArr.length - i;
        }
        short[] sArr2 = new short[i2];
        System.arraycopy(sArr, i, sArr2, 0, i2);
        return sArr2;
    }
}
