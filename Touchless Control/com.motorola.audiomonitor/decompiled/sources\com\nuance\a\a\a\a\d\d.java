package com.nuance.a.a.a.a.d;

import com.nuance.a.a.a.a.a.a.C0000a;
import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;

public class d {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f800a = a.a(d.class);

    public static String a(String str) {
        if (f800a.a()) {
            f800a.a((Object) "Escaping XML reserved tokens (&, <, >, \" and ') of: " + str);
        }
        int i = 0;
        StringBuffer stringBuffer = new StringBuffer(str);
        while (i < stringBuffer.length()) {
            char charAt = stringBuffer.charAt(i);
            if (charAt == '&') {
                int i2 = i + 1;
                stringBuffer.insert(i2, "amp;");
                i = i2 + 4;
            } else if (charAt == '<') {
                stringBuffer.deleteCharAt(i);
                stringBuffer.insert(i, "&lt;");
                i += 4;
            } else if (charAt == '>') {
                stringBuffer.deleteCharAt(i);
                stringBuffer.insert(i, "&gt;");
                i += 4;
            } else if (charAt == '\"') {
                stringBuffer.deleteCharAt(i);
                stringBuffer.insert(i, "&quot;");
                i += 6;
            } else if (charAt == '\'') {
                stringBuffer.deleteCharAt(i);
                stringBuffer.insert(i, "&apos;");
                i += 6;
            } else {
                i++;
            }
        }
        String stringBuffer2 = stringBuffer.toString();
        if (f800a.a()) {
            f800a.a((Object) "Final output: " + stringBuffer2);
        }
        return stringBuffer2;
    }

    public static boolean a(C0000a aVar) {
        short a2 = aVar.a();
        return a2 == C0000a.x.a() || a2 == C0000a.z.a() || a2 == C0000a.y.a();
    }

    public static C0000a b(C0000a aVar) {
        C0000a aVar2 = (aVar == C0000a.y || aVar == C0000a.z) ? C0000a.x : (aVar == C0000a.B || aVar == C0000a.C || aVar == C0000a.E || aVar == C0000a.D) ? C0000a.A : aVar;
        if (f800a.c()) {
            f800a.c((Object) "adjustCodecForBluetooth() " + aVar.a() + " -> " + aVar2.a());
        }
        return aVar2;
    }
}
