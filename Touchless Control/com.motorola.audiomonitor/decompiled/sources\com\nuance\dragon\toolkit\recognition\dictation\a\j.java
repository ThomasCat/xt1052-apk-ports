package com.nuance.dragon.toolkit.recognition.dictation.a;

import com.nuance.dragon.toolkit.recognition.dictation.EditorItem;
import com.nuance.dragon.toolkit.recognition.dictation.EditorItem.Type;

final class j implements EditorItem {

    /* renamed from: a reason: collision with root package name */
    private String f1954a;

    /* renamed from: b reason: collision with root package name */
    private Type f1955b;

    public j(String str, Type type) {
        if (str == null && type == Type.WORD) {
            throw new IllegalArgumentException("Text cannot be null!");
        }
        this.f1955b = type;
        if (this.f1955b == Type.WHITE_SPACE) {
            this.f1954a = " ";
        } else {
            this.f1954a = str;
        }
    }

    public final Type getType() {
        return this.f1955b;
    }

    public final int length() {
        return this.f1954a.length();
    }

    public final String toString() {
        return this.f1954a;
    }
}
