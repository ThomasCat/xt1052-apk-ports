package com.nuance.dragon.toolkit.language;

import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.Languages;
import com.nuance.dragon.toolkit.recognizer.vocon.VoconRecognizer;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer;

class a {
    public static final Language[] ALL_LANGUAGES = {EUROPEAN_FRENCH, JAPANESE, GERMAN, EUROPEAN_SPANISH, NORWEGIAN, SWEDISH, AUSTRALIAN_ENGLISH, POLISH, BRAZILIAN_PORTUGUESE, EUROPEAN_PORTUGUESE, CHINESE_MANDARIN, FINNISH, DANISH, DUTCH, TAIWANESE_MANDARIN, UKRANIAN, LATIN_AMERICAN_SPANISH, KOREAN, ITALIAN, CANADIAN_FRENCH, UNITED_STATES_ENGLISH, GREEK, RUSSIAN, CHINESE_CANTONESE, BRITISH_ENGLISH, TURKISH};
    public static final Language AUSTRALIAN_ENGLISH = new Language(Languages.AUSTRALIAN_ENGLISH, Vocalizer.Languages.AUSTRALIAN_ENGLISH, VoconRecognizer.Languages.AUSTRALIAN_ENGLISH, "eng-AUS", "Australian English");
    public static final Language BRAZILIAN_PORTUGUESE = new Language(Languages.BRAZILIAN_PORTUGUESE, Vocalizer.Languages.BRAZILIAN_PORTUGUESE, VoconRecognizer.Languages.BRAZILIAN_PORTUGUESE, "por-BRA", "Brazilian Portuguese");
    public static final Language BRITISH_ENGLISH = new Language(Languages.BRITISH_ENGLISH, Vocalizer.Languages.BRITISH_ENGLISH, VoconRecognizer.Languages.BRITISH_ENGLISH, "eng-GBR", "British English");
    public static final Language CANADIAN_FRENCH = new Language(Languages.CANADIAN_FRENCH, Vocalizer.Languages.CANADIAN_FRENCH, VoconRecognizer.Languages.CANADIAN_FRENCH, "fra-CAN", "Canadian French");
    public static final Language CHINESE_CANTONESE = new Language(Languages.CANTONESE_ENGLISH_TRADITIONAL, Vocalizer.Languages.CANTONESE_TRADITIONAL, VoconRecognizer.Languages.CANTONESE_TRADITIONAL, "yue-CHN", "Chinese Cantonese");
    public static final Language CHINESE_MANDARIN = new Language(Languages.MANDARIN_ENGLISH_SIMPLIFIED, Vocalizer.Languages.MANDARIN_SIMPLIFIED, VoconRecognizer.Languages.MANDARIN_SIMPLIFIED, "cmn-CHN", "Chinese Mandarin");
    public static final Language DANISH = new Language(Languages.DANISH, Vocalizer.Languages.DANISH, VoconRecognizer.Languages.DANISH, "dan-DNK", "Danish");
    public static final Language DUTCH = new Language(Languages.DUTCH, Vocalizer.Languages.DUTCH, VoconRecognizer.Languages.DUTCH, "nld-NLD", "Dutch");
    public static final Language EUROPEAN_FRENCH = new Language(Languages.EUROPEAN_FRENCH, Vocalizer.Languages.EUROPEAN_FRENCH, VoconRecognizer.Languages.EUROPEAN_FRENCH, "fra-FRA", "European French");
    public static final Language EUROPEAN_PORTUGUESE = new Language(Languages.EUROPEAN_PORTUGUESE, Vocalizer.Languages.EUROPEAN_PORTUGUESE, VoconRecognizer.Languages.EUROPEAN_PORTUGUESE, "por-PRT", "European Portuguese");
    public static final Language EUROPEAN_SPANISH = new Language(Languages.EUROPEAN_SPANISH, Vocalizer.Languages.EUROPEAN_SPANISH, VoconRecognizer.Languages.EUROPEAN_SPANISH, "spa-ESP", "European Spanish");
    public static final Language FINNISH = new Language(Languages.FINNISH, Vocalizer.Languages.FINNISH, VoconRecognizer.Languages.FINNISH, "fin-FIN", "Finnish");
    public static final Language GERMAN = new Language(Languages.GERMAN, Vocalizer.Languages.GERMAN, VoconRecognizer.Languages.GERMAN, "deu-DEU", "German");
    public static final Language GREEK = new Language(Languages.GREEK, Vocalizer.Languages.GREEK, VoconRecognizer.Languages.GREEK, "ell-GRC", "Greek");
    public static final Language ITALIAN = new Language(Languages.ITALIAN, Vocalizer.Languages.ITALIAN, VoconRecognizer.Languages.ITALIAN, "ita-ITA", "Italian");
    public static final Language JAPANESE = new Language(Languages.JAPANESE, Vocalizer.Languages.JAPANESE, VoconRecognizer.Languages.JAPANESE, "jpn-JPN", "Japanese");
    public static final Language KOREAN = new Language(Languages.KOREAN, Vocalizer.Languages.KOREAN, VoconRecognizer.Languages.KOREAN, "kor-KOR", "Korean");
    public static final Language LATIN_AMERICAN_SPANISH = new Language(Languages.LATIN_AMERICAN_SPANISH, Vocalizer.Languages.LATIN_AMERICAN_SPANISH, VoconRecognizer.Languages.MEXICAN_SPANISH, "spa-XLA", "Latin American Spanish");
    public static final Language NORWEGIAN = new Language(Languages.NORWEGIAN, Vocalizer.Languages.NORWEGIAN, VoconRecognizer.Languages.NORWEGIAN, "nor-NOR", "Norwegian");
    public static final Language POLISH = new Language(Languages.POLISH, Vocalizer.Languages.POLISH, VoconRecognizer.Languages.POLISH, "pol-POL", "Polish");
    public static final Language RUSSIAN = new Language(Languages.RUSSIAN, Vocalizer.Languages.RUSSIAN, VoconRecognizer.Languages.RUSSIAN, "rus-RUS", "Russian");
    public static final Language SWEDISH = new Language(Languages.SWEDISH, Vocalizer.Languages.SWEDISH, VoconRecognizer.Languages.SWEDISH, "swe-SWE", "Swedish");
    public static final Language TAIWANESE_MANDARIN = new Language(Languages.MANDARIN_ENGLISH_TRADITIONAL, Vocalizer.Languages.MANDARIN_TRADITIONAL, VoconRecognizer.Languages.MANDARIN_TRADITIONAL, "cmn-TWN", "Taiwanese Mandarin");
    public static final Language TURKISH = new Language(Languages.TURKISH, Vocalizer.Languages.TURKISH, VoconRecognizer.Languages.TURKISH, "tur-TUR", "Turkish");
    public static final Language UKRANIAN = new Language(Languages.UKRAINIAN, null, VoconRecognizer.Languages.UKRAINIAN, "ukr-UKR", "Ukranian");
    public static final Language UNITED_STATES_ENGLISH = new Language(Languages.UNITED_STATES_ENGLISH, Vocalizer.Languages.UNITED_STATES_ENGLISH, VoconRecognizer.Languages.UNITED_STATES_ENGLISH, "eng-USA", "United States English");

    a() {
    }
}
