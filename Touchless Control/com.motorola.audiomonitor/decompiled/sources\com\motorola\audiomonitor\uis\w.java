package com.motorola.audiomonitor.uis;

import android.content.Intent;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import com.motorola.audiomonitor.uis.training.StartTrainingActivity;

class w implements OnPreferenceClickListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ u f761a;

    w(u uVar) {
        this.f761a = uVar;
    }

    public boolean onPreferenceClick(Preference preference) {
        this.f761a.getActivity().startActivity(new Intent(this.f761a.getActivity(), StartTrainingActivity.class));
        return true;
    }
}
