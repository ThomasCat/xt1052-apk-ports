package com.nuance.dragon.toolkit.file;

import android.content.res.AssetFileDescriptor;
import android.os.ParcelFileDescriptor;
import com.nuance.dragon.toolkit.util.Logger;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel.MapMode;
import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class FileZip {

    /* renamed from: a reason: collision with root package name */
    ByteBuffer f1692a = ByteBuffer.allocate(4);

    /* renamed from: b reason: collision with root package name */
    private HashMap f1693b = new HashMap();
    public HashMap mZipFiles = new HashMap();

    public final class ZipEntryRO {
        public long mCRC32;
        public long mCompressedLength;
        public final File mFile;
        public final String mFileName;
        public long mLocalHdrOffset;
        public int mMethod;
        public long mOffset = -1;
        public long mUncompressedLength;
        public long mWhenModified;
        public final String mZipFileName;

        public ZipEntryRO(String str, File file, String str2) {
            this.mFileName = str2;
            this.mZipFileName = str;
            this.mFile = file;
        }

        public final AssetFileDescriptor getAssetFileDescriptor() {
            if (this.mMethod == 0) {
                try {
                    return new AssetFileDescriptor(ParcelFileDescriptor.open(this.mFile, 268435456), getOffset(), this.mUncompressedLength);
                } catch (FileNotFoundException e2) {
                    e2.printStackTrace();
                }
            }
            return null;
        }

        public final long getOffset() {
            return this.mOffset;
        }

        public final File getZipFile() {
            return this.mFile;
        }

        public final String getZipFileName() {
            return this.mZipFileName;
        }

        public final boolean isUncompressed() {
            return this.mMethod == 0;
        }

        public final void setOffsetFromFile(RandomAccessFile randomAccessFile, ByteBuffer byteBuffer) {
            long j = this.mLocalHdrOffset;
            try {
                randomAccessFile.seek(j);
                randomAccessFile.readFully(byteBuffer.array());
                if (byteBuffer.getInt(0) != 67324752) {
                    Logger.warn(this, "didn't find signature at start of lfh");
                    throw new IOException();
                }
                this.mOffset = j + 30 + ((long) (byteBuffer.getShort(26) & 65535)) + ((long) (byteBuffer.getShort(28) & 65535));
            } catch (FileNotFoundException e2) {
                e2.printStackTrace();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        }
    }

    public FileZip(String str) {
        b(str);
    }

    private void b(String str) {
        File file = new File(str);
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
        long length = randomAccessFile.length();
        if (length < 22) {
            throw new IOException();
        }
        long j = 65557;
        if (65557 > length) {
            j = length;
        }
        randomAccessFile.seek(0);
        int readInt = randomAccessFile.readInt();
        int i = ((readInt >>> 24) & 255) + ((readInt & 255) << 24) + ((65280 & readInt) << 8) + ((16711680 & readInt) >>> 8);
        if (i == 101010256) {
            throw new IOException();
        } else if (i != 67324752) {
            throw new IOException();
        } else {
            randomAccessFile.seek(length - j);
            ByteBuffer allocate = ByteBuffer.allocate((int) j);
            byte[] array = allocate.array();
            randomAccessFile.readFully(array);
            allocate.order(ByteOrder.LITTLE_ENDIAN);
            int length2 = array.length - 22;
            while (true) {
                if (length2 >= 0) {
                    if (array[length2] == 80 && allocate.getInt(length2) == 101010256) {
                        new StringBuilder("+++ Found EOCD at index: ").append(length2);
                        break;
                    }
                    length2--;
                } else {
                    break;
                }
            }
            if (length2 < 0) {
                new StringBuilder("Zip: EOCD not found, ").append(str).append(" is not zip");
            }
            short s = allocate.getShort(length2 + 8);
            long j2 = 4294967295L & ((long) allocate.getInt(length2 + 12));
            long j3 = ((long) allocate.getInt(length2 + 16)) & 4294967295L;
            if (j3 + j2 > length) {
                Logger.warn(this, "bad offsets (dir " + j3 + ", size " + j2 + ", eocd " + length2 + ")");
                throw new IOException();
            } else if (s == 0) {
                Logger.warn(this, "empty archive?");
                throw new IOException();
            } else {
                new StringBuilder("+++ numEntries=").append(s).append(" dirSize=").append(j2).append(" dirOffset=").append(j3);
                MappedByteBuffer map = randomAccessFile.getChannel().map(MapMode.READ_ONLY, j3, j2);
                map.order(ByteOrder.LITTLE_ENDIAN);
                byte[] bArr = new byte[65535];
                int i2 = 0;
                ByteBuffer allocate2 = ByteBuffer.allocate(30);
                allocate2.order(ByteOrder.LITTLE_ENDIAN);
                for (int i3 = 0; i3 < s; i3++) {
                    if (map.getInt(i2) != 33639248) {
                        Logger.warn(this, "Missed a central dir sig (at " + i2 + ")");
                        throw new IOException();
                    }
                    short s2 = map.getShort(i2 + 28) & 65535;
                    short s3 = map.getShort(i2 + 30) & 65535;
                    short s4 = map.getShort(i2 + 32) & 65535;
                    map.position(i2 + 46);
                    map.get(bArr, 0, s2);
                    map.position(0);
                    String str2 = new String(bArr, 0, s2);
                    new StringBuilder("Filename: ").append(str2);
                    ZipEntryRO zipEntryRO = new ZipEntryRO(str, file, str2);
                    zipEntryRO.mMethod = map.getShort(i2 + 10) & 65535;
                    zipEntryRO.mWhenModified = ((long) map.getInt(i2 + 12)) & 4294967295L;
                    zipEntryRO.mCRC32 = map.getLong(i2 + 16) & 4294967295L;
                    zipEntryRO.mCompressedLength = map.getLong(i2 + 20) & 4294967295L;
                    zipEntryRO.mUncompressedLength = map.getLong(i2 + 24) & 4294967295L;
                    zipEntryRO.mLocalHdrOffset = ((long) map.getInt(i2 + 42)) & 4294967295L;
                    allocate2.clear();
                    zipEntryRO.setOffsetFromFile(randomAccessFile, allocate2);
                    this.f1693b.put(str2, zipEntryRO);
                    i2 += s2 + 46 + s3 + s4;
                }
                new StringBuilder("+++ zip good scan ").append(s).append(" entries");
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final ZipEntryRO[] a(String str) {
        Vector vector = new Vector();
        Collection<ZipEntryRO> values = this.f1693b.values();
        if (str == null) {
            str = "";
        }
        int length = str.length();
        for (ZipEntryRO zipEntryRO : values) {
            if (zipEntryRO.mFileName.startsWith(str) && -1 == zipEntryRO.mFileName.indexOf(47, length)) {
                vector.add(zipEntryRO);
            }
        }
        return (ZipEntryRO[]) vector.toArray(new ZipEntryRO[vector.size()]);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FileZip fileZip = (FileZip) obj;
        if (this.f1693b == null) {
            if (fileZip.f1693b != null) {
                return false;
            }
        } else if (!this.f1693b.equals(fileZip.f1693b)) {
            return false;
        }
        if (this.f1692a == null) {
            if (fileZip.f1692a != null) {
                return false;
            }
        } else if (!this.f1692a.equals(fileZip.f1692a)) {
            return false;
        }
        return this.mZipFiles == null ? fileZip.mZipFiles == null : this.mZipFiles.equals(fileZip.mZipFiles);
    }

    public ZipEntryRO[] getAllEntries() {
        Collection values = this.f1693b.values();
        return (ZipEntryRO[]) values.toArray(new ZipEntryRO[values.size()]);
    }

    public AssetFileDescriptor getAssetFileDescriptor(String str) {
        ZipEntryRO zipEntryRO = (ZipEntryRO) this.f1693b.get(str);
        if (zipEntryRO != null) {
            return zipEntryRO.getAssetFileDescriptor();
        }
        return null;
    }

    public InputStream getInputStream(String str) {
        ZipEntryRO zipEntryRO = (ZipEntryRO) this.f1693b.get(str);
        if (zipEntryRO != null) {
            if (zipEntryRO.isUncompressed()) {
                return zipEntryRO.getAssetFileDescriptor().createInputStream();
            }
            ZipFile zipFile = (ZipFile) this.mZipFiles.get(zipEntryRO.getZipFile());
            if (zipFile == null) {
                zipFile = new ZipFile(zipEntryRO.getZipFile(), 1);
                this.mZipFiles.put(zipEntryRO.getZipFile(), zipFile);
            }
            ZipEntry entry = zipFile.getEntry(str);
            if (entry != null) {
                return zipFile.getInputStream(entry);
            }
        }
        return null;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f1692a == null ? 0 : this.f1692a.hashCode()) + (((this.f1693b == null ? 0 : this.f1693b.hashCode()) + 31) * 31)) * 31;
        if (this.mZipFiles != null) {
            i = this.mZipFiles.hashCode();
        }
        return hashCode + i;
    }
}
