package com.motorola.audiomonitor;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

class ax implements ServiceConnection {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ au f323a;

    private ax(au auVar) {
        this.f323a = auVar;
    }

    /* synthetic */ ax(au auVar, av avVar) {
        this(auVar);
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (au.f316b) {
            Log.d(au.f315a, "Connected to Monitor Service");
        }
        this.f323a.f317c = u.a(iBinder);
        if (this.f323a.f320f != null) {
            this.f323a.f320f.a(true);
        }
        try {
            if (this.f323a.f320f != null) {
                this.f323a.f317c.a(this.f323a.f321g);
            }
        } catch (RemoteException e2) {
            Log.e(au.f315a, "Failed to add aidl listener");
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        if (au.f316b) {
            Log.d(au.f315a, "Disconnected from Monitor Service");
        }
        this.f323a.f317c = null;
        if (this.f323a.f320f != null) {
            this.f323a.f320f.a(false);
        }
    }
}
