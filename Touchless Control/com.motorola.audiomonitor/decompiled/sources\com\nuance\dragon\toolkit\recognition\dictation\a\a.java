package com.nuance.dragon.toolkit.recognition.dictation.a;

import com.nuance.dragon.toolkit.recognition.dictation.AlterChoice;
import com.nuance.dragon.toolkit.recognition.dictation.EditorItem;
import com.nuance.dragon.toolkit.recognition.dictation.EditorItem.Type;
import com.nuance.dragon.toolkit.recognition.dictation.Token;
import java.util.ArrayList;

final class a implements AlterChoice {

    /* renamed from: a reason: collision with root package name */
    private ArrayList f1921a = new ArrayList();

    public final ArrayList a() {
        return this.f1921a;
    }

    public final void a(EditorItem editorItem) {
        this.f1921a.add(editorItem);
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c4  */
    public final void a(ArrayList arrayList) {
        boolean z;
        int i = 0;
        int lastIndexOf = this.f1921a.lastIndexOf(arrayList.get(0));
        if (lastIndexOf != -1) {
            int i2 = 0;
            int i3 = lastIndexOf;
            while (true) {
                if (i3 >= this.f1921a.size()) {
                    i = i2;
                    break;
                }
                EditorItem editorItem = (EditorItem) this.f1921a.get(i3);
                if (editorItem.getType() != Type.WHITE_SPACE) {
                    if (editorItem.getType() == Type.WORD || i2 >= arrayList.size() || !editorItem.equals(arrayList.get(i2))) {
                        break;
                    }
                    i3++;
                    i2++;
                } else {
                    i3++;
                }
            }
            if (i == arrayList.size() && this.f1921a.size() > 1 && ((EditorItem) this.f1921a.get(this.f1921a.size() - 1)).getType() == Type.WHITE_SPACE && ((EditorItem) this.f1921a.get(this.f1921a.size() - 2)).getType() == Type.TOKEN) {
                this.f1921a.remove(this.f1921a.size() - 1);
            }
        }
        if (this.f1921a.size() > 0) {
            EditorItem editorItem2 = (EditorItem) this.f1921a.get(this.f1921a.size() - 1);
            if (editorItem2.getType() == Type.TOKEN) {
                z = ((Token) editorItem2).hasNoSpaceAfterDirective();
                while (i < arrayList.size()) {
                    h hVar = (h) arrayList.get(i);
                    if (!hVar.hasNoSpaceBeforeDirective() && !z) {
                        this.f1921a.add(new j(" ", Type.WHITE_SPACE));
                    }
                    this.f1921a.add(hVar);
                    i++;
                    z = hVar.hasNoSpaceAfterDirective();
                }
            }
        }
        z = true;
        while (i < arrayList.size()) {
        }
    }

    public final EditorItem editorItemAt(int i) {
        if (i < 0 || i >= this.f1921a.size()) {
            return null;
        }
        return (EditorItem) this.f1921a.get(i);
    }

    public final int size() {
        return this.f1921a.size();
    }

    public final String toString() {
        if (this.f1921a == null || this.f1921a.size() == 0) {
            return "";
        }
        String str = new String();
        int i = 0;
        while (i < this.f1921a.size()) {
            String str2 = str + ((EditorItem) this.f1921a.get(i)).toString();
            i++;
            str = str2;
        }
        return str;
    }
}
