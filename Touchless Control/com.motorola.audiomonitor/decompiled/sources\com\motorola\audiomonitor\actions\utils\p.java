package com.motorola.audiomonitor.actions.utils;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.text.TextUtils;
import android.util.Log;
import com.motorola.audiomonitor.a;
import java.util.List;

public class p extends c {

    /* renamed from: a reason: collision with root package name */
    private static final String f253a = ("AMonitor." + p.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f254b = a.f160b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public final Context f255c;

    /* renamed from: d reason: collision with root package name */
    private final q f256d;

    /* renamed from: e reason: collision with root package name */
    private final long f257e = 120000;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public long f258f = 0;
    /* access modifiers changed from: private */

    /* renamed from: g reason: collision with root package name */
    public boolean f259g = false;
    /* access modifiers changed from: private */

    /* renamed from: h reason: collision with root package name */
    public final HandlerThread f260h;
    /* access modifiers changed from: private */
    public final Handler i;
    /* access modifiers changed from: private */
    public final e j = new e(this);

    public p(Context context, List list) {
        super(list);
        this.f255c = context;
        this.f256d = new q(this);
        this.f260h = new HandlerThread(p.class.getSimpleName());
        this.f260h.start();
        this.i = new Handler(this.f260h.getLooper());
        this.f259g = true;
        this.i.post(this.j);
        this.f255c.getContentResolver().registerContentObserver(Contacts.CONTENT_URI, true, this.f256d);
    }

    private boolean a(String str) {
        if (str.length() > 50) {
            return false;
        }
        boolean z = true;
        if (TextUtils.isEmpty(str.replaceAll("[^a-zA-Z \\.']", "").trim()) || str.contains("#")) {
            z = false;
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    private void b(List list) {
        Cursor cursor;
        try {
            cursor = this.f255c.getContentResolver().query(Phone.CONTENT_URI, new String[]{"lookup", "display_name"}, "has_phone_number=?", new String[]{"1"}, null);
            if (cursor == null) {
                try {
                    if (f254b) {
                        Log.d(f253a, "cursor is null");
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            } else {
                int columnIndexOrThrow = cursor.getColumnIndexOrThrow("lookup");
                int columnIndex = cursor.getColumnIndex("display_name");
                if (cursor.getCount() > 0 && cursor.moveToFirst()) {
                    do {
                        String string = cursor.getString(columnIndexOrThrow);
                        String string2 = cursor.getString(columnIndex);
                        if (a(string2)) {
                            b bVar = new b(string, string2, -1, 1.0f);
                            if (!list.contains(bVar)) {
                                list.add(bVar);
                            }
                        }
                    } while (cursor.moveToNext());
                    if (cursor == null) {
                    }
                } else if (cursor == null) {
                    cursor.close();
                }
            }
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
        }
    }

    /* access modifiers changed from: protected */
    public void a(List list) {
        synchronized (this.i) {
            if (this.f260h.isAlive()) {
                this.f258f = System.currentTimeMillis();
                this.f259g = false;
                this.i.removeCallbacksAndMessages(null);
                b(list);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0031, code lost:
        if (r4.f260h.isAlive() == false) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0033, code lost:
        android.util.Log.e(f253a, "Failed to stop the handler thread");
     */
    public void b() {
        this.f255c.getContentResolver().unregisterContentObserver(this.f256d);
        synchronized (this.i) {
            if (this.f260h.isAlive()) {
                this.f259g = false;
                this.i.removeCallbacksAndMessages(null);
                this.f260h.quit();
                while (true) {
                    try {
                        this.f260h.join(3000);
                        break;
                    } catch (InterruptedException e2) {
                        if (f254b) {
                            Log.d(f253a, "Join interrupted");
                        }
                    }
                }
            }
        }
        super.b();
    }
}
