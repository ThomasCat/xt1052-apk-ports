package com.nuance.dragon.toolkit.audio.nat;

import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.nat.NativeAudio.Player;
import com.nuance.dragon.toolkit.audio.nat.NativeAudio.Recorder;
import com.nuance.dragon.toolkit.util.Logger;
import java.util.Iterator;
import java.util.LinkedList;

class NativeAudioImpl implements NativeAudio {

    /* renamed from: a reason: collision with root package name */
    private long f1084a;

    final class a implements Player {

        /* renamed from: a reason: collision with root package name */
        private long f1085a;

        /* renamed from: b reason: collision with root package name */
        private boolean f1086b;

        /* renamed from: c reason: collision with root package name */
        private int f1087c;

        /* renamed from: d reason: collision with root package name */
        private final LinkedList f1088d;

        /* renamed from: e reason: collision with root package name */
        private final LinkedList f1089e;

        /* renamed from: f reason: collision with root package name */
        private int f1090f;

        private a(long j) {
            this.f1085a = j;
            this.f1088d = new LinkedList();
            this.f1089e = new LinkedList();
        }

        /* synthetic */ a(long j, byte b2) {
            this(j);
        }

        public final AudioChunk getPlayedAudio() {
            if (!this.f1089e.isEmpty()) {
                return (AudioChunk) this.f1089e.remove();
            }
            return null;
        }

        public final boolean isStalled() {
            long j = this.f1085a;
            return NativeAudioImpl.playerStalled(this.f1085a);
        }

        public final void release() {
            long j = this.f1085a;
            NativeAudioImpl.playerDestroy(this.f1085a);
            this.f1085a = 0;
        }

        public final boolean start() {
            long j = this.f1085a;
            return NativeAudioImpl.playerStart(this.f1085a);
        }

        public final boolean stop() {
            long j = this.f1085a;
            return NativeAudioImpl.playerStop(this.f1085a);
        }

        public final int write(short[] sArr) {
            long j = this.f1085a;
            int playerWrite = NativeAudioImpl.playerWrite(this.f1085a, sArr);
            if (playerWrite > 0) {
                if (playerWrite < sArr.length) {
                    short[] sArr2 = new short[playerWrite];
                    System.arraycopy(sArr, 0, sArr2, 0, playerWrite);
                    sArr = sArr2;
                }
                this.f1088d.add(sArr);
            }
            if (!this.f1086b) {
                if (NativeAudioImpl.playerHasStartTime(this.f1085a)) {
                    this.f1086b = true;
                    this.f1087c = NativeAudioImpl.getPlayerStartTime(this.f1085a);
                    this.f1090f = 0;
                }
            } else if (!NativeAudioImpl.playerHasStartTime(this.f1085a)) {
                this.f1086b = false;
            }
            if (this.f1086b) {
                Iterator it = this.f1088d.iterator();
                while (it.hasNext()) {
                    AudioChunk audioChunk = new AudioChunk(AudioType.PCM_16k, (short[]) it.next(), (long) (this.f1087c + this.f1090f));
                    this.f1090f += audioChunk.audioDuration;
                    this.f1089e.add(audioChunk);
                }
                this.f1088d.clear();
            }
            return playerWrite;
        }
    }

    final class b implements Recorder {

        /* renamed from: a reason: collision with root package name */
        private long f1091a;

        /* renamed from: b reason: collision with root package name */
        private final short[] f1092b;

        /* renamed from: c reason: collision with root package name */
        private final int f1093c;

        /* renamed from: d reason: collision with root package name */
        private AudioChunk f1094d;

        private b(long j) {
            this.f1091a = j;
            this.f1092b = new short[NativeAudioImpl.getRecorderBufferSize(this.f1091a)];
            this.f1093c = NativeAudioImpl.getRecorderBufferDuration(this.f1091a);
        }

        /* synthetic */ b(long j, byte b2) {
            this(j);
        }

        public final int getBufferSizeMs() {
            return this.f1093c;
        }

        public final AudioChunk read() {
            long j = this.f1091a;
            int recorderRead = NativeAudioImpl.recorderRead(this.f1091a, this.f1092b);
            if (recorderRead == 0) {
                return null;
            }
            short[] sArr = new short[this.f1092b.length];
            System.arraycopy(this.f1092b, 0, sArr, 0, sArr.length);
            AudioChunk audioChunk = new AudioChunk(AudioType.PCM_16k, sArr, (long) recorderRead);
            if (!(this.f1094d == null || this.f1094d.audioEndTimestamp == ((long) recorderRead))) {
                Logger.warn(this, "Audio timestamp mismatch: " + this.f1094d.audioTimestamp + " != " + recorderRead);
            }
            this.f1094d = audioChunk;
            return audioChunk;
        }

        public final void release() {
            long j = this.f1091a;
            NativeAudioImpl.recorderDestroy(this.f1091a);
            this.f1091a = 0;
        }

        public final boolean start() {
            long j = this.f1091a;
            return NativeAudioImpl.recorderStart(this.f1091a) == 0;
        }

        public final boolean stop() {
            long j = this.f1091a;
            return NativeAudioImpl.recorderStop(this.f1091a) == 0;
        }
    }

    static {
        System.loadLibrary("dmt_audio");
    }

    NativeAudioImpl() {
    }

    static native long audioCreate();

    static native void audioDestroy(long j);

    static native int getPlayerStartTime(long j);

    static native int getRecorderBufferDuration(long j);

    static native int getRecorderBufferSize(long j);

    static native long playerCreate(long j, int i, int i2);

    static native void playerDestroy(long j);

    static native boolean playerHasStartTime(long j);

    static native boolean playerStalled(long j);

    static native boolean playerStart(long j);

    static native boolean playerStop(long j);

    static native int playerWrite(long j, short[] sArr);

    static native long recorderCreate(long j, int i, int i2);

    static native void recorderDestroy(long j);

    static native int recorderRead(long j, short[] sArr);

    static native int recorderStart(long j);

    static native int recorderStop(long j);

    public Player createPlayer(int i) {
        if (this.f1084a != 0) {
            long playerCreate = playerCreate(this.f1084a, i, 400);
            if (playerCreate != 0) {
                return new a(playerCreate, 0);
            }
        }
        return null;
    }

    public Recorder createRecorder(int i) {
        if (this.f1084a != 0) {
            return new b(recorderCreate(this.f1084a, i, 400), 0);
        }
        return null;
    }

    public boolean initialize() {
        if (this.f1084a != 0) {
            audioDestroy(this.f1084a);
        }
        this.f1084a = audioCreate();
        return this.f1084a != 0;
    }

    public void release() {
        if (this.f1084a != 0) {
            audioDestroy(this.f1084a);
            this.f1084a = 0;
        }
    }
}
