package com.nuance.dragon.toolkit.vocalizer;

import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.sources.SingleSinkSource;
import com.nuance.dragon.toolkit.vocalizer.Vocalizer.TtsListener;
import java.util.LinkedList;

public class TtsAudioSource extends SingleSinkSource {

    /* renamed from: a reason: collision with root package name */
    final String f2142a;

    /* renamed from: b reason: collision with root package name */
    final int f2143b;

    /* renamed from: c reason: collision with root package name */
    final int f2144c;

    /* renamed from: d reason: collision with root package name */
    final int f2145d;

    /* renamed from: e reason: collision with root package name */
    final int f2146e;

    /* renamed from: f reason: collision with root package name */
    final int f2147f;

    /* renamed from: g reason: collision with root package name */
    final Object f2148g;

    /* renamed from: h reason: collision with root package name */
    private final Vocalizer f2149h;
    private final TtsListener i;
    private final a j;
    private boolean k;
    private boolean l;
    private boolean m;
    private boolean n;
    private int o;
    private final LinkedList p = new LinkedList();

    interface a {
        void a(TtsAudioSource ttsAudioSource);
    }

    TtsAudioSource(Vocalizer vocalizer, String str, int i2, int i3, int i4, int i5, int i6, a aVar, TtsListener ttsListener, Object obj) {
        this.f2149h = vocalizer;
        this.f2142a = str;
        this.f2146e = i2;
        this.f2147f = i3;
        this.f2143b = i4;
        this.f2144c = i5;
        this.f2145d = i6;
        this.j = aVar;
        this.i = ttsListener;
        this.f2148g = obj;
    }

    /* access modifiers changed from: 0000 */
    public final void a(AudioChunk audioChunk) {
        this.p.add(audioChunk);
        this.o += audioChunk.audioDuration;
        if (this.l && this.p.size() >= this.f2145d) {
            this.l = false;
        }
        if (!this.l) {
            notifyChunksAvailable();
        }
    }

    /* access modifiers changed from: 0000 */
    public final synchronized void a(boolean z) {
        if (!this.m) {
            this.m = true;
            if (this.n) {
                if (this.i != null) {
                    this.i.ttsGenerationFinished(this.f2142a, this.f2148g, this.f2149h, z);
                }
                this.n = false;
                if (this.l && !this.p.isEmpty()) {
                    notifyChunksAvailable();
                }
                notifySourceClosed();
            }
            this.j.a(this);
        }
    }

    /* access modifiers changed from: 0000 */
    public final synchronized boolean a() {
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        boolean z = this.n;
        if (this.i != null) {
            this.i.ttsGenerationStarted(this.f2142a, this.f2148g, this.f2149h);
        }
        this.n = true;
        if (this.f2145d > 0) {
            this.l = true;
        }
    }

    public synchronized void cancel() {
        a(false);
    }

    public AudioChunk getAudioChunk() {
        if (this.p.isEmpty()) {
            return null;
        }
        if (!this.k) {
            this.k = true;
            if (this.i != null) {
                this.i.ttsStreamingStarted(this.f2142a, this.f2148g, this.f2149h);
            }
        }
        return (AudioChunk) this.p.removeFirst();
    }

    public AudioType getAudioType() {
        Vocalizer vocalizer = this.f2149h;
        return this.f2149h.getAudioType();
    }

    public int getChunksAvailable() {
        return this.p.size();
    }

    public boolean isActive() {
        return this.n;
    }

    /* access modifiers changed from: protected */
    public void onSinkDisconnected(AudioSink audioSink) {
        super.onSinkDisconnected(audioSink);
        if (this.k) {
            this.k = false;
            if (this.i != null) {
                this.i.ttsStreamingFinished(this.f2142a, this.f2148g, this.f2149h);
            }
        }
        cancel();
    }
}
