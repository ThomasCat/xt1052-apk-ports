package com.nuance.dragon.toolkit.audio.bluetooth.impl;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.util.Log;
import com.nuance.dragon.toolkit.audio.bluetooth.impl.Bluetooth.HeadsetStateListener;
import com.nuance.dragon.toolkit.audio.bluetooth.impl.BluetoothHeadset.ServiceListener;
import java.util.Set;

final class d extends Bluetooth {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public BluetoothHeadset f1074a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public HeadsetStateListener f1075b;
    /* access modifiers changed from: private */

    /* renamed from: c reason: collision with root package name */
    public boolean f1076c;

    /* renamed from: d reason: collision with root package name */
    private boolean f1077d;
    /* access modifiers changed from: private */

    /* renamed from: e reason: collision with root package name */
    public boolean f1078e;
    /* access modifiers changed from: private */

    /* renamed from: f reason: collision with root package name */
    public boolean f1079f;

    /* renamed from: g reason: collision with root package name */
    private AudioManager f1080g;

    /* renamed from: h reason: collision with root package name */
    private final ServiceListener f1081h = new ServiceListener() {
        public final void onServiceConnected() {
            boolean z = false;
            Bluetooth.a("BluetoothHeadset service connected mHeadset = " + d.this.f1074a);
            if (d.this.f1074a == null) {
                d.this.f1075b.onServiceConnected(false);
                return;
            }
            d.this.f1076c = true;
            d dVar = d.this;
            if (d.this.f1074a.getState() == BluetoothHeadset.STATE_CONNECTED) {
                z = true;
            }
            dVar.f1078e = z;
            if (d.this.f1075b != null) {
                d.this.f1075b.onServiceConnected(true);
                d.this.f1075b.onConnectionStateChanged(d.this.getHeadsetState());
            }
        }

        public final void onServiceDisconnected() {
            Bluetooth.a("BluetoothHeadset service disconnected");
            d.this.f1075b.onServiceDisconnected();
            d.this.f1076c = false;
        }
    };
    private final BroadcastReceiver i = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            boolean z = true;
            String action = intent.getAction();
            if (action.equals(BluetoothHeadset.ACTION_STATE_CHANGED)) {
                int intExtra = intent.getIntExtra(BluetoothHeadset.EXTRA_STATE, -1);
                Bluetooth.a("Bluetooth state changed: " + intExtra);
                if (intExtra != BluetoothHeadset.STATE_CONNECTED) {
                    z = false;
                }
                if (d.this.f1078e != z) {
                    d.this.f1078e = z;
                    if (d.this.f1075b != null) {
                        d.this.f1075b.onConnectionStateChanged(d.this.getHeadsetState());
                    }
                }
            } else if (action.equals("android.media.SCO_AUDIO_STATE_CHANGED")) {
                int intExtra2 = intent.getIntExtra("android.media.extra.SCO_AUDIO_STATE", -1);
                Bluetooth.a("Bluetooth audio state changed: " + intExtra2);
                if (intExtra2 == 1 || intExtra2 == 0 || intExtra2 == -1) {
                    if (intExtra2 != 1) {
                        z = false;
                    }
                    if (d.this.f1079f != z) {
                        d.this.f1079f = z;
                        if (d.this.f1075b != null) {
                            d.this.f1075b.onAudioStateChanged(d.this.getAudioState());
                        }
                    }
                }
            }
        }
    };

    public d(Context context) {
        super(context);
        Log.d("DMT-Bluetooth", "Bluetooth 2.2 created");
    }

    public final void close() {
        if (this.f1077d) {
            this.f1077d = false;
            this.mContext.unregisterReceiver(this.i);
        }
        if (this.f1074a != null) {
            try {
                this.f1074a.close();
            } catch (Throwable th) {
            }
            this.f1074a = null;
        }
    }

    public final int getAudioState() {
        return this.f1079f ? 1 : 0;
    }

    public final String[] getBondedDevices() {
        Set<BluetoothDevice> bondedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
        if (bondedDevices == null) {
            return null;
        }
        String[] strArr = new String[bondedDevices.size()];
        int i2 = 0;
        for (BluetoothDevice bluetoothDevice : bondedDevices) {
            String name = bluetoothDevice.getName();
            int i3 = i2 + 1;
            strArr[i2] = name != null ? name : bluetoothDevice.getAddress();
            i2 = i3;
        }
        return strArr;
    }

    public final int getHeadsetState() {
        return this.f1078e ? 1 : 0;
    }

    public final int getPlaybackStream() {
        if (AndroidVersion.USE_MUSIC_STREAM_FOR_BLUETOOTH) {
            Log.d("DMT-Bluetooth", "Bluetooth 2.2 playback stream = MUSIC_STREAM");
            return Audio.MUSIC_STREAM;
        }
        Log.d("DMT-Bluetooth", "Bluetooth 2.2 playback stream = VOICE_CALL_STREAM");
        return Audio.VOICE_CALL_STREAM;
    }

    public final int getRecordingSource() {
        return Audio.DEFAULT_AUDIO_SOURCE;
    }

    public final boolean isEnabled() {
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter != null) {
            return defaultAdapter.isEnabled();
        }
        return false;
    }

    public final void setHeadsetStateListener(HeadsetStateListener headsetStateListener) {
        this.f1075b = headsetStateListener;
        if (this.f1074a == null) {
            this.f1074a = new BluetoothHeadset(this.mContext, this.f1081h);
            IntentFilter intentFilter = new IntentFilter(BluetoothHeadset.ACTION_STATE_CHANGED);
            intentFilter.addAction("android.media.SCO_AUDIO_STATE_CHANGED");
            this.mContext.registerReceiver(this.i, intentFilter);
            this.f1077d = true;
            this.f1080g = (AudioManager) this.mContext.getSystemService("audio");
        }
        if (this.f1076c) {
            this.f1075b.onConnectionStateChanged(getHeadsetState());
        }
    }

    public final boolean startBluetoothSco() {
        if (!AndroidVersion.USE_MUSIC_STREAM_FOR_BLUETOOTH) {
            this.f1080g.startBluetoothSco();
        }
        return true;
    }

    public final void stopBluetoothSco() {
        if (!AndroidVersion.USE_MUSIC_STREAM_FOR_BLUETOOTH) {
            this.f1080g.stopBluetoothSco();
        }
    }
}
