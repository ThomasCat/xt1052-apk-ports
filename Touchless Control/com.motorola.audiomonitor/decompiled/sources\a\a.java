package a;

import android.content.Context;
import android.content.Intent;
import c.d;
import com.motorola.audiomonitor.actions.utils.h;
import com.motorola.audiomonitor.p;
import com.motorola.audiomonitor.uis.AOVErrorActivity;
import h.g;

public final class a extends d {

    /* renamed from: c reason: collision with root package name */
    private final q f0c;

    /* renamed from: d reason: collision with root package name */
    private final i f1d;

    public a(Context context, p pVar) {
        super(context, a.class.getSimpleName());
        this.f23b = context;
        this.f0c = new q(context);
        this.f1d = new i(context, pVar, new b(this));
        start();
    }

    public static void a(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, AOVErrorActivity.class);
        intent.addFlags(268435456);
        if (h.c(context)) {
            intent.putExtra("ERROR_TYPE", 1);
            context.startActivity(intent);
            return;
        }
        intent.putExtra("ERROR_TYPE", 0);
        context.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void b(e eVar) {
        if (eVar == null) {
            b();
            return;
        }
        String a2 = eVar.b().a("command");
        if ((a2.equals("call") || a2.equals("voicemail")) && !h.a().f()) {
            a(this.f23b);
            b();
            return;
        }
        g.a().m();
        o oVar = o.NOT_CONSUMED;
        if (eVar.b() != null) {
            g.a().a(eVar.b().a("command"));
            oVar = this.f0c.a(eVar);
            if (oVar == o.CONSUMED) {
                g.a().k();
                d.a(this.f23b, "num_commands_successful");
                b();
                return;
            }
        }
        if (oVar == o.ABORTED) {
            b();
        } else if (!h.b(this.f23b)) {
            a(this.f23b);
            b();
        } else {
            this.f1d.a(eVar);
            d.a(this.f23b, "num_commands_successful");
        }
    }

    public void a() {
        this.f0c.a();
        this.f1d.a();
        super.a();
    }

    public void a(e eVar) {
        if (this.f22a != null) {
            this.f22a.post(new c(this, eVar));
        }
    }
}
