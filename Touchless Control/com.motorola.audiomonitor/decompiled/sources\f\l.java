package f;

import android.util.Log;
import com.nuance.dragon.toolkit.elvis.ElvisError;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer.RebuildListener;
import com.nuance.dragon.toolkit.elvis.Grammar;
import java.util.List;

class l implements RebuildListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ i f2247a;

    l(i iVar) {
        this.f2247a = iVar;
    }

    public void onComplete(Grammar grammar, List list) {
        List constraints = grammar.getConstraints();
        if (constraints == null || this.f2247a.f2238c == null) {
            Log.e(i.f2236a, "Init not completed");
            return;
        }
        this.f2247a.f2238c.setActiveConstraints(constraints);
        if (i.f2237b) {
            Log.d(i.f2236a, "Rebuild completed");
            Log.d(i.f2236a, "skipped words : " + this.f2247a.b(list));
        }
        this.f2247a.n = true;
        this.f2247a.m.a(this.f2247a, true);
    }

    public void onError(ElvisError elvisError) {
        Log.e(i.f2236a, "Failed to build the grammar: " + elvisError.getReason());
        this.f2247a.m.a(this.f2247a, false);
    }
}
