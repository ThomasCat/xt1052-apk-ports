package com.motorola.audiomonitor.uis;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

class al extends ClickableSpan {

    /* renamed from: b reason: collision with root package name */
    final /* synthetic */ PrivacyActivity f654b;

    private al(PrivacyActivity privacyActivity) {
        this.f654b = privacyActivity;
    }

    /* synthetic */ al(PrivacyActivity privacyActivity, ai aiVar) {
        this(privacyActivity);
    }

    public void onClick(View view) {
    }

    public void updateDrawState(TextPaint textPaint) {
        textPaint.setColor(textPaint.linkColor);
        textPaint.setUnderlineText(false);
    }
}
