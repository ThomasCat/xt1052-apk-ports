package com.nuance.dragon.toolkit.audio.bluetooth.impl;

public interface BtHeadsetListener {
    void onAudioStateConnected();

    void onAudioStateDisconnected();

    void onHeadsetConnected();

    void onHeadsetDisconnected();
}
