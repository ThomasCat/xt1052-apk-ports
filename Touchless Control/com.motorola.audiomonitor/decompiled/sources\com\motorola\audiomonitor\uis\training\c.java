package com.motorola.audiomonitor.uis.training;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

class c implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ StartTrainingActivity f740a;

    c(StartTrainingActivity startTrainingActivity) {
        this.f740a = startTrainingActivity;
    }

    public void onClick(View view) {
        this.f740a.getApplicationContext().sendBroadcast(new Intent("com.motorola.audiomonitor.action.TRAINING_DELAYED"));
        this.f740a.finish();
    }
}
