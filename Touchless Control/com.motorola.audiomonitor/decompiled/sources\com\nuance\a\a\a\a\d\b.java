package com.nuance.a.a.a.a.d;

import com.nuance.a.a.a.a.b.a.a;
import com.nuance.a.a.a.a.b.a.a.C0001a;

public class b {

    /* renamed from: a reason: collision with root package name */
    private static final C0001a f799a = a.a(b.class);

    public static short a(byte[] bArr, int i) {
        short s = (short) ((bArr[i] & 255) | ((bArr[i + 1] & 255) << 8));
        if (f799a.a()) {
            f799a.a((Object) "Converted [" + bArr[i] + ", " + bArr[i + 1] + "] to short " + s);
        }
        return s;
    }

    public static void a(int i, byte[] bArr, int i2) {
        short s = (short) ((i >> 16) & 65535);
        a((short) (i & 65535), bArr, i2);
        a(s, bArr, i2 + 2);
        if (f799a.a()) {
            f799a.a((Object) "Converted int " + i + " to [" + bArr[i2] + ", " + bArr[i2 + 1] + ", " + bArr[i2 + 2] + ", " + bArr[i2 + 3] + "]");
        }
    }

    public static void a(short s, byte[] bArr, int i) {
        byte b2 = (byte) ((s >> 8) & 255);
        bArr[i] = (byte) (s & 255);
        bArr[i + 1] = b2;
        if (f799a.a()) {
            f799a.a((Object) "Converted short " + s + " to [" + bArr[i] + ", " + bArr[i + 1] + "]");
        }
    }

    public static int b(byte[] bArr, int i) {
        short a2 = (a(bArr, i) & 65535) | ((a(bArr, i + 2) & 65535) << 16);
        if (f799a.a()) {
            f799a.a((Object) "Converted [" + bArr[i] + ", " + bArr[i + 1] + ", " + bArr[i + 2] + ", " + bArr[i + 3] + "] to int " + a2);
        }
        return a2;
    }
}
