package com.nuance.dragon.toolkit.vocalizer;

import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import java.util.List;

interface NativeVocalizer {

    public interface SpeakListener {
        void onNewChunk(AudioChunk audioChunk, boolean z);
    }

    AudioType a(String str, String str2, int i);

    void a();

    void a(String str);

    boolean a(String str, int i, int i2, int i3, SpeakListener speakListener);

    boolean a(String str, String str2);

    boolean a(List list);

    void b();
}
