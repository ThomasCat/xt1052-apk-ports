package a;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import b.b;
import com.motorola.audiomonitor.C0014R;
import com.motorola.audiomonitor.a;
import java.util.HashMap;
import java.util.Map;

public class p extends r {

    /* renamed from: a reason: collision with root package name */
    private static final String f49a = ("AMonitor." + p.class.getSimpleName());

    /* renamed from: b reason: collision with root package name */
    private static final boolean f50b = a.f160b;

    /* renamed from: c reason: collision with root package name */
    private static Map f51c = null;

    public p(Context context) {
        super(context);
    }

    private static boolean a(Context context, Intent intent) {
        return context.getPackageManager().queryBroadcastReceivers(intent, 0).size() > 0;
    }

    public static String[] a(Context context) {
        if (f51c == null) {
            b(context);
        }
        return (String[]) f51c.keySet().toArray(new String[f51c.size()]);
    }

    private static void b(Context context) {
        f51c = new HashMap();
        String[] stringArray = context.getResources().getStringArray(C0014R.array.commands_intent);
        String[] stringArray2 = context.getResources().getStringArray(C0014R.array.commands_intent_action);
        if (stringArray.length == stringArray2.length) {
            for (int i = 0; i < stringArray.length; i++) {
                Intent intent = new Intent();
                intent.setAction(stringArray2[i]);
                if (a(context, intent)) {
                    f51c.put(stringArray[i], intent);
                }
            }
        } else if (f50b) {
            Log.d(f49a, "names and actions len does not match");
        }
        if (f50b) {
            Log.d(f49a, "intents=" + f51c);
        }
    }

    public o a(b bVar, e eVar) {
        o oVar = o.NOT_CONSUMED;
        if (f51c != null) {
            Intent intent = (Intent) f51c.get(bVar.a("name"));
            if (intent != null) {
                c().sendBroadcast(intent, "com.motorola.audiomonitor.permission.BROADCAST_RECEIVER");
                oVar = o.CONSUMED;
                if (f50b) {
                    Log.d(f49a, "broadcast intent = " + intent);
                }
            }
        }
        return oVar;
    }

    public void a() {
        super.a();
        if (f51c != null) {
            f51c.clear();
            f51c = null;
        }
    }
}
