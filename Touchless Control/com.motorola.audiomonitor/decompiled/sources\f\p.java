package f;

import com.nuance.dragon.toolkit.elvis.ElvisConfig;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer;

class p implements Runnable {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ int f2251a;

    /* renamed from: b reason: collision with root package name */
    final /* synthetic */ i f2252b;

    p(i iVar, int i) {
        this.f2252b = iVar;
        this.f2251a = i;
    }

    public void run() {
        this.f2252b.f2238c = ElvisRecognizer.createElvisRecognizer(i.r);
        this.f2252b.f2238c.enableVerboseAndroidLogging(false);
        this.f2252b.f2238c.initialize(new ElvisConfig(v.a(), this.f2252b.a(this.f2251a)), this.f2252b.p, this.f2252b.f2243h);
    }
}
