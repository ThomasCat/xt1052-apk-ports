package com.nuance.dragon.toolkit.recognition;

import com.nuance.dragon.toolkit.cloudservices.recognizer.CloudRecognitionResult;
import com.nuance.dragon.toolkit.data.Data;
import com.nuance.dragon.toolkit.data.Data.Dictionary;
import com.nuance.dragon.toolkit.data.Data.Integer;
import com.nuance.dragon.toolkit.data.Data.Sequence;
import com.nuance.dragon.toolkit.data.Data.String;
import com.nuance.dragon.toolkit.recognition.InterpretedRecognition.Phrase;
import com.nuance.dragon.toolkit.recognition.InterpretedRecognition.Word;
import com.nuance.dragon.toolkit.recognition.a.a;
import com.nuance.dragon.toolkit.recognition.a.a.b;
import com.nuance.dragon.toolkit.recognition.a.c;
import com.nuance.dragon.toolkit.recognition.dictation.DictationResult;
import com.nuance.dragon.toolkit.recognition.dictation.DictationResultManager;
import java.util.ArrayList;
import java.util.List;

public class NvcAsrCloudInterpreter implements RecognitionInterpreter {
    public InterpretedRecognition getInterpretedResult(CloudRecognitionResult cloudRecognitionResult) {
        Dictionary dictionary = cloudRecognitionResult.getDictionary();
        if (dictionary == null || dictionary.getType() != 224) {
            throw new InterpretException("Couldn't find top-level result dictionary");
        }
        new StringBuilder("Trying to parse result: ").append(dictionary.toString());
        Dictionary dictionary2 = dictionary;
        String string = dictionary2.getString("prompt");
        String string2 = dictionary2.getString("result_format");
        InterpretedRecognition interpretedRecognition = string != null ? new InterpretedRecognition(string.value) : new InterpretedRecognition();
        if (string2 == null || !string2.value.equals("interp_xml_results")) {
            throw new InterpretException("Expected result_format == interp_xml_results");
        }
        String string3 = dictionary2.getString("xml_results");
        if (string3 == null) {
            throw new InterpretException("Expected xml_results");
        }
        c createInterpretationResult = DictationResultManager.createInterpretationResult(string3.value.getBytes());
        if (createInterpretationResult == null) {
            throw new InterpretException("No interpretation result!");
        }
        List<a> a2 = createInterpretationResult.a();
        if (a2.isEmpty()) {
            throw new InterpretException("No interpretations found");
        }
        for (a aVar : a2) {
            ArrayList arrayList = new ArrayList();
            for (b bVar : aVar.a()) {
                if (!"fill".equals(bVar.f1908a)) {
                    ArrayList arrayList2 = new ArrayList();
                    arrayList2.add(new Word(bVar.f1909b, null, null, 0, 0));
                    DictationResult dictationResult = bVar.f1910c;
                    arrayList.add(dictationResult != null ? new Phrase(bVar.f1908a, dictationResult, null) : new Phrase(bVar.f1908a, bVar.f1909b, arrayList2, null, true));
                }
            }
            interpretedRecognition.addChoice(aVar.toString(), aVar.a_(), 0, arrayList, null);
        }
        return interpretedRecognition;
    }

    public List getUpdateRequiredList(CloudRecognitionResult cloudRecognitionResult) {
        int i = 0;
        Dictionary dictionary = cloudRecognitionResult.getDictionary();
        if (dictionary == null || dictionary.getType() != 224) {
            return new ArrayList(0);
        }
        Data data = dictionary.get("force_upload_flags");
        if (data == null || data.getType() != 16) {
            return new ArrayList(0);
        }
        Sequence sequence = (Sequence) data;
        ArrayList arrayList = new ArrayList(sequence.size());
        while (true) {
            int i2 = i;
            if (i2 >= sequence.size()) {
                return arrayList;
            }
            Data data2 = sequence.get(i2);
            if (data2.getType() == 224) {
                Dictionary dictionary2 = (Dictionary) data2;
                Integer integer = dictionary2.getInt("force_upload");
                if (integer != null && integer.value == 1) {
                    String string = dictionary2.getString("id");
                    if (!arrayList.contains(string.value)) {
                        arrayList.add(string.value);
                    }
                }
            }
            i = i2 + 1;
        }
    }

    public boolean processForCloud(CloudRecognitionResult cloudRecognitionResult, List list) {
        return false;
    }
}
