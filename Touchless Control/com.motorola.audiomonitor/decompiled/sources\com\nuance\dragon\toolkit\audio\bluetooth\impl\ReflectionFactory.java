package com.nuance.dragon.toolkit.audio.bluetooth.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionFactory {
    private ReflectionFactory() {
    }

    public static Class getClassForName(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e2) {
            if ("int".equals(str)) {
                return Integer.TYPE;
            }
            if ("boolean".equals(str)) {
                return Boolean.TYPE;
            }
            if ("float".equals(str)) {
                return Float.TYPE;
            }
            if ("double".equals(str)) {
                return Double.TYPE;
            }
            if ("char".equals(str)) {
                return Character.TYPE;
            }
            if ("byte".equals(str)) {
                return Byte.TYPE;
            }
            if ("void".equals(str)) {
                return Void.TYPE;
            }
            if ("short".equals(str)) {
                return Short.TYPE;
            }
            if ("long".equals(str)) {
                return Long.TYPE;
            }
            return null;
        }
    }

    public static Constructor getConstructorFromStrings(String str, String... strArr) {
        Class[] clsArr = new Class[strArr.length];
        for (int i = 0; i < clsArr.length; i++) {
            clsArr[i] = getClassForName(strArr[i]);
        }
        if (str != null) {
            try {
                return Class.forName(str).getConstructor(clsArr);
            } catch (ClassNotFoundException e2) {
                return null;
            } catch (IllegalArgumentException e3) {
                return null;
            } catch (NoSuchMethodException e4) {
            }
        }
        return null;
    }

    public static Field getField(String str, String str2) {
        Field field = null;
        if (str == null || str2 == null) {
            return field;
        }
        try {
            return Class.forName(str).getField(str2);
        } catch (ClassNotFoundException | NoSuchFieldException e2) {
            return field;
        }
    }

    public static Method getMethod(String str, String str2, Class... clsArr) {
        Method method = null;
        if (str == null || str2 == null) {
            return method;
        }
        try {
            return Class.forName(str).getMethod(str2, clsArr);
        } catch (ClassNotFoundException | IllegalArgumentException | NoSuchMethodException e2) {
            return method;
        }
    }

    public static Method getMethodFromStrings(String str, String str2, String... strArr) {
        Class[] clsArr = new Class[strArr.length];
        for (int i = 0; i < clsArr.length; i++) {
            clsArr[i] = getClassForName(strArr[i]);
        }
        if (!(str == null || str2 == null)) {
            try {
                return Class.forName(str).getMethod(str2, clsArr);
            } catch (ClassNotFoundException e2) {
                return null;
            } catch (IllegalArgumentException e3) {
                return null;
            } catch (NoSuchMethodException e4) {
            }
        }
        return null;
    }

    public static Object newObject(String str) {
        Object obj = null;
        if (str == null) {
            return obj;
        }
        try {
            return Class.forName(str).newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e2) {
            return obj;
        }
    }

    public static Object newObject(Constructor constructor, Object... objArr) {
        Object obj = null;
        if (constructor == null) {
            return obj;
        }
        try {
            return constructor.newInstance(objArr);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e2) {
            return obj;
        }
    }
}
