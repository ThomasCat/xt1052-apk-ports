package com.nuance.dragon.toolkit.recognition.dictation;

import com.nuance.dragon.toolkit.recognition.dictation.a.f;
import com.nuance.dragon.toolkit.util.JSONCompliant;
import java.util.Map;
import org.json.JSONObject;

public abstract class DictationResult implements JSONCompliant {
    public static DictationResult createFromJSON(JSONObject jSONObject) {
        return f.a(jSONObject);
    }

    public abstract void append(DictationResult dictationResult);

    public abstract Alternatives getAlternatives(long j, long j2);

    public abstract Map getExtraInformation();

    public abstract void merge(DictationResult dictationResult, long j, long j2);

    public abstract Sentence sentenceAt(int i);

    public abstract int size();

    public abstract String toString();
}
