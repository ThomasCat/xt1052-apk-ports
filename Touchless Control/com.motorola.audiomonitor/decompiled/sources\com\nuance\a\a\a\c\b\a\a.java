package com.nuance.a.a.a.c.b.a;

import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

public final class a {

    /* renamed from: a reason: collision with root package name */
    private static SSLSocketFactory f946a;

    /* renamed from: b reason: collision with root package name */
    private static SSLContext f947b;

    /* renamed from: c reason: collision with root package name */
    private static b[] f948c = new b[1];

    public static Socket a(String str, int i, c cVar) {
        try {
            f948c[0] = new b(cVar);
            SSLContext instance = SSLContext.getInstance("TLS");
            f947b = instance;
            instance.init(null, (TrustManager[]) f948c, null);
            f946a = f947b.getSocketFactory();
            return f946a.createSocket(str, i);
        } catch (NoSuchAlgorithmException e2) {
            throw new SecurityException("No such algorithm exception " + e2);
        } catch (KeyManagementException e3) {
            throw new SecurityException("Failed to initialize the client-side SSLContext " + e3);
        } catch (GeneralSecurityException e4) {
            throw new SecurityException("General security exception " + e4);
        }
    }
}
