package com.google.android.gms.tagmanager;

import android.content.Context;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.concurrent.LinkedBlockingQueue;

class as extends Thread implements ar {
    private static as Vc;
    private final LinkedBlockingQueue<Runnable> Vb = new LinkedBlockingQueue<>();
    /* access modifiers changed from: private */
    public volatile at Vd;
    private volatile boolean mClosed = false;
    /* access modifiers changed from: private */
    public final Context mContext;
    private volatile boolean sa = false;

    private as(Context context) {
        super("GAThread");
        if (context != null) {
            this.mContext = context.getApplicationContext();
        } else {
            this.mContext = context;
        }
        start();
    }

    static as H(Context context) {
        if (Vc == null) {
            Vc = new as(context);
        }
        return Vc;
    }

    private String a(Throwable th) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(byteArrayOutputStream);
        th.printStackTrace(printStream);
        printStream.flush();
        return new String(byteArrayOutputStream.toByteArray());
    }

    public void a(Runnable runnable) {
        this.Vb.add(runnable);
    }

    /* access modifiers changed from: 0000 */
    public void b(String str, long j) {
        final long j2 = j;
        final String str2 = str;
        a((Runnable) new Runnable() {
            public void run() {
                if (as.this.Vd == null) {
                    cy kh = cy.kh();
                    kh.a(as.this.mContext, this);
                    as.this.Vd = kh.ki();
                }
                as.this.Vd.e(j2, str2);
            }
        });
    }

    public void bn(String str) {
        b(str, System.currentTimeMillis());
    }

    public void run() {
        while (!this.mClosed) {
            try {
                Runnable runnable = (Runnable) this.Vb.take();
                if (!this.sa) {
                    runnable.run();
                }
            } catch (InterruptedException e) {
                bh.u(e.toString());
            } catch (Throwable th) {
                bh.t("Error on GAThread: " + a(th));
                bh.t("Google Analytics is shutting down.");
                this.sa = true;
            }
        }
    }
}
