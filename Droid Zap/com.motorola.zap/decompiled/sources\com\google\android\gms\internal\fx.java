package com.google.android.gms.internal;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameBuffer;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.GamesMetadata.LoadGamesResult;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerBuffer;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.Players.LoadPlayersResult;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements.LoadAchievementsResult;
import com.google.android.gms.games.achievement.Achievements.UpdateAchievementResult;
import com.google.android.gms.games.leaderboard.Leaderboard;
import com.google.android.gms.games.leaderboard.LeaderboardBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer;
import com.google.android.gms.games.leaderboard.Leaderboards.LeaderboardMetadataResult;
import com.google.android.gms.games.leaderboard.Leaderboards.LoadPlayerScoreResult;
import com.google.android.gms.games.leaderboard.Leaderboards.LoadScoresResult;
import com.google.android.gms.games.leaderboard.Leaderboards.SubmitScoreResult;
import com.google.android.gms.games.leaderboard.ScoreSubmissionData;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.InvitationBuffer;
import com.google.android.gms.games.multiplayer.Invitations.LoadInvitationsResult;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.ParticipantResult;
import com.google.android.gms.games.multiplayer.ParticipantUtils;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMultiplayer.ReliableMessageSentCallback;
import com.google.android.gms.games.multiplayer.realtime.RealTimeSocket;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomEntity;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;
import com.google.android.gms.games.multiplayer.turnbased.LoadMatchesResponse;
import com.google.android.gms.games.multiplayer.turnbased.OnTurnBasedMatchUpdateReceivedListener;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchBuffer;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchConfig;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.CancelMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.InitiateMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.LeaveMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.LoadMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.LoadMatchesResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.UpdateMatchResult;
import com.google.android.gms.games.request.GameRequest;
import com.google.android.gms.games.request.GameRequestBuffer;
import com.google.android.gms.games.request.OnRequestReceivedListener;
import com.google.android.gms.games.request.Requests.LoadRequestsResult;
import com.google.android.gms.games.request.Requests.UpdateRequestsResult;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public final class fx extends eh<gb> implements ConnectionCallbacks, OnConnectionFailedListener {
    private boolean GA = false;
    private boolean GB = false;
    private int GC;
    private final Binder GD;
    private final long GE;
    private final boolean GF;
    private final int GG;
    private final boolean GH;
    private final String Gv;
    private final Map<String, RealTimeSocket> Gw;
    private PlayerEntity Gx;
    private GameEntity Gy;
    private final gd Gz;
    private final String vi;

    abstract class a extends c {
        private final ArrayList<String> GI = new ArrayList<>();

        a(RoomStatusUpdateListener roomStatusUpdateListener, DataHolder dataHolder, String[] strArr) {
            super(roomStatusUpdateListener, dataHolder);
            for (String add : strArr) {
                this.GI.add(add);
            }
        }

        /* access modifiers changed from: protected */
        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            a(roomStatusUpdateListener, room, this.GI);
        }

        /* access modifiers changed from: protected */
        public abstract void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList);
    }

    final class aa extends b<RoomStatusUpdateListener> {
        private final String GZ;

        aa(RoomStatusUpdateListener roomStatusUpdateListener, String str) {
            super(roomStatusUpdateListener);
            this.GZ = str;
        }

        public void a(RoomStatusUpdateListener roomStatusUpdateListener) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onP2PConnected(this.GZ);
            }
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }
    }

    final class ab extends b<RoomStatusUpdateListener> {
        private final String GZ;

        ab(RoomStatusUpdateListener roomStatusUpdateListener, String str) {
            super(roomStatusUpdateListener);
            this.GZ = str;
        }

        public void a(RoomStatusUpdateListener roomStatusUpdateListener) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onP2PDisconnected(this.GZ);
            }
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }
    }

    final class ac extends a {
        ac(RoomStatusUpdateListener roomStatusUpdateListener, DataHolder dataHolder, String[] strArr) {
            super(roomStatusUpdateListener, dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeersConnected(room, arrayList);
        }
    }

    final class ad extends a {
        ad(RoomStatusUpdateListener roomStatusUpdateListener, DataHolder dataHolder, String[] strArr) {
            super(roomStatusUpdateListener, dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeerDeclined(room, arrayList);
        }
    }

    final class ae extends a {
        ae(RoomStatusUpdateListener roomStatusUpdateListener, DataHolder dataHolder, String[] strArr) {
            super(roomStatusUpdateListener, dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeersDisconnected(room, arrayList);
        }
    }

    final class af extends a {
        af(RoomStatusUpdateListener roomStatusUpdateListener, DataHolder dataHolder, String[] strArr) {
            super(roomStatusUpdateListener, dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeerInvitedToRoom(room, arrayList);
        }
    }

    final class ag extends a {
        ag(RoomStatusUpdateListener roomStatusUpdateListener, DataHolder dataHolder, String[] strArr) {
            super(roomStatusUpdateListener, dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeerJoined(room, arrayList);
        }
    }

    final class ah extends a {
        ah(RoomStatusUpdateListener roomStatusUpdateListener, DataHolder dataHolder, String[] strArr) {
            super(roomStatusUpdateListener, dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeerLeft(room, arrayList);
        }
    }

    final class ai extends fw {
        private final com.google.android.gms.common.api.a.c<LoadPlayerScoreResult> vj;

        ai(com.google.android.gms.common.api.a.c<LoadPlayerScoreResult> cVar) {
            this.vj = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void C(DataHolder dataHolder) {
            fx.this.a((b<?>) new aj<Object>(this.vj, dataHolder));
        }
    }

    final class aj extends com.google.android.gms.internal.eh.d<com.google.android.gms.common.api.a.c<LoadPlayerScoreResult>> implements LoadPlayerScoreResult {
        private final com.google.android.gms.games.leaderboard.d Ha;
        private final Status vl;

        aj(com.google.android.gms.common.api.a.c<LoadPlayerScoreResult> cVar, DataHolder dataHolder) {
            super(cVar, dataHolder);
            this.vl = new Status(dataHolder.getStatusCode());
            LeaderboardScoreBuffer leaderboardScoreBuffer = new LeaderboardScoreBuffer(dataHolder);
            try {
                if (leaderboardScoreBuffer.getCount() > 0) {
                    this.Ha = (com.google.android.gms.games.leaderboard.d) leaderboardScoreBuffer.get(0).freeze();
                } else {
                    this.Ha = null;
                }
            } finally {
                leaderboardScoreBuffer.close();
            }
        }

        /* access modifiers changed from: protected */
        public void a(com.google.android.gms.common.api.a.c<LoadPlayerScoreResult> cVar, DataHolder dataHolder) {
            cVar.b(this);
        }

        public LeaderboardScore getScore() {
            return this.Ha;
        }

        public Status getStatus() {
            return this.vl;
        }
    }

    final class ak extends fw {
        private final com.google.android.gms.common.api.a.c<LoadPlayersResult> vj;

        ak(com.google.android.gms.common.api.a.c<LoadPlayersResult> cVar) {
            this.vj = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void e(DataHolder dataHolder) {
            fx.this.a((b<?>) new al<Object>(this.vj, dataHolder));
        }
    }

    final class al extends av<com.google.android.gms.common.api.a.c<LoadPlayersResult>> implements LoadPlayersResult {
        private final PlayerBuffer Hb;

        al(com.google.android.gms.common.api.a.c<LoadPlayersResult> cVar, DataHolder dataHolder) {
            super(cVar, dataHolder);
            this.Hb = new PlayerBuffer(dataHolder);
        }

        /* access modifiers changed from: protected */
        public void a(com.google.android.gms.common.api.a.c<LoadPlayersResult> cVar, DataHolder dataHolder) {
            cVar.b(this);
        }

        public PlayerBuffer getPlayers() {
            return this.Hb;
        }
    }

    final class am extends b<ReliableMessageSentCallback> {
        private final String Hc;
        private final int Hd;
        private final int yJ;

        am(ReliableMessageSentCallback reliableMessageSentCallback, int i, int i2, String str) {
            super(reliableMessageSentCallback);
            this.yJ = i;
            this.Hd = i2;
            this.Hc = str;
        }

        public void a(ReliableMessageSentCallback reliableMessageSentCallback) {
            if (reliableMessageSentCallback != null) {
                reliableMessageSentCallback.onRealTimeMessageSent(this.yJ, this.Hd, this.Hc);
            }
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }
    }

    final class an extends fw {
        final ReliableMessageSentCallback He;

        public an(ReliableMessageSentCallback reliableMessageSentCallback) {
            this.He = reliableMessageSentCallback;
        }

        public void b(int i, int i2, String str) {
            fx.this.a((b<?>) new am<Object>(this.He, i, i2, str));
        }
    }

    final class ao extends fw {
        private final OnRequestReceivedListener Hf;

        ao(OnRequestReceivedListener onRequestReceivedListener) {
            this.Hf = onRequestReceivedListener;
        }

        public void m(DataHolder dataHolder) {
            GameRequestBuffer gameRequestBuffer = new GameRequestBuffer(dataHolder);
            GameRequest gameRequest = null;
            try {
                if (gameRequestBuffer.getCount() > 0) {
                    gameRequest = (GameRequest) ((GameRequest) gameRequestBuffer.get(0)).freeze();
                }
                if (gameRequest != null) {
                    fx.this.a((b<?>) new ap<Object>(this.Hf, gameRequest));
                }
            } finally {
                gameRequestBuffer.close();
            }
        }

        public void onRequestRemoved(String requestId) {
            fx.this.a((b<?>) new aq<Object>(this.Hf, requestId));
        }
    }

    final class ap extends b<OnRequestReceivedListener> {
        private final GameRequest Hg;

        ap(OnRequestReceivedListener onRequestReceivedListener, GameRequest gameRequest) {
            super(onRequestReceivedListener);
            this.Hg = gameRequest;
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void a(OnRequestReceivedListener onRequestReceivedListener) {
            onRequestReceivedListener.onRequestReceived(this.Hg);
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }
    }

    final class aq extends b<OnRequestReceivedListener> {
        private final String Hh;

        aq(OnRequestReceivedListener onRequestReceivedListener, String str) {
            super(onRequestReceivedListener);
            this.Hh = str;
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void a(OnRequestReceivedListener onRequestReceivedListener) {
            onRequestReceivedListener.onRequestRemoved(this.Hh);
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }
    }

    final class ar extends fw {
        private final com.google.android.gms.common.api.a.c<LoadRequestsResult> Hi;

        public ar(com.google.android.gms.common.api.a.c<LoadRequestsResult> cVar) {
            this.Hi = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void b(int i, Bundle bundle) {
            bundle.setClassLoader(getClass().getClassLoader());
            fx.this.a((b<?>) new as<Object>(this.Hi, new Status(i), bundle));
        }
    }

    final class as extends b<com.google.android.gms.common.api.a.c<LoadRequestsResult>> implements LoadRequestsResult {
        private final Bundle Hj;
        private final Status vl;

        as(com.google.android.gms.common.api.a.c<LoadRequestsResult> cVar, Status status, Bundle bundle) {
            super(cVar);
            this.vl = status;
            this.Hj = bundle;
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void a(com.google.android.gms.common.api.a.c<LoadRequestsResult> cVar) {
            cVar.b(this);
        }

        /* access modifiers changed from: protected */
        public void cP() {
            release();
        }

        public GameRequestBuffer getRequests(int requestType) {
            String aW = gs.aW(requestType);
            if (!this.Hj.containsKey(aW)) {
                return null;
            }
            return new GameRequestBuffer((DataHolder) this.Hj.get(aW));
        }

        public Status getStatus() {
            return this.vl;
        }

        public void release() {
            for (String parcelable : this.Hj.keySet()) {
                DataHolder dataHolder = (DataHolder) this.Hj.getParcelable(parcelable);
                if (dataHolder != null) {
                    dataHolder.close();
                }
            }
        }
    }

    final class at extends fw {
        private final com.google.android.gms.common.api.a.c<UpdateRequestsResult> Hk;

        public at(com.google.android.gms.common.api.a.c<UpdateRequestsResult> cVar) {
            this.Hk = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void D(DataHolder dataHolder) {
            fx.this.a((b<?>) new au<Object>(this.Hk, dataHolder));
        }
    }

    final class au extends av<com.google.android.gms.common.api.a.c<UpdateRequestsResult>> implements UpdateRequestsResult {
        private final hb Hl;

        au(com.google.android.gms.common.api.a.c<UpdateRequestsResult> cVar, DataHolder dataHolder) {
            super(cVar, dataHolder);
            this.Hl = hb.H(dataHolder);
        }

        /* access modifiers changed from: protected */
        public void a(com.google.android.gms.common.api.a.c<UpdateRequestsResult> cVar, DataHolder dataHolder) {
            cVar.b(this);
        }

        public Set<String> getRequestIds() {
            return this.Hl.getRequestIds();
        }

        public int getRequestOutcome(String requestId) {
            return this.Hl.getRequestOutcome(requestId);
        }
    }

    abstract class av<R extends com.google.android.gms.common.api.a.c<?>> extends com.google.android.gms.internal.eh.d<R> implements Releasable, Result {
        final Status vl;
        final DataHolder zU;

        public av(R r, DataHolder dataHolder) {
            super(r, dataHolder);
            this.vl = new Status(dataHolder.getStatusCode());
            this.zU = dataHolder;
        }

        public Status getStatus() {
            return this.vl;
        }

        public void release() {
            if (this.zU != null) {
                this.zU.close();
            }
        }
    }

    final class aw extends c {
        aw(RoomStatusUpdateListener roomStatusUpdateListener, DataHolder dataHolder) {
            super(roomStatusUpdateListener, dataHolder);
        }

        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            roomStatusUpdateListener.onRoomAutoMatching(room);
        }
    }

    final class ax extends fw {
        private final RoomUpdateListener Hm;
        private final RoomStatusUpdateListener Hn;
        private final RealTimeMessageReceivedListener Ho;

        public ax(RoomUpdateListener roomUpdateListener) {
            this.Hm = (RoomUpdateListener) er.b(roomUpdateListener, (Object) "Callbacks must not be null");
            this.Hn = null;
            this.Ho = null;
        }

        public ax(RoomUpdateListener roomUpdateListener, RoomStatusUpdateListener roomStatusUpdateListener, RealTimeMessageReceivedListener realTimeMessageReceivedListener) {
            this.Hm = (RoomUpdateListener) er.b(roomUpdateListener, (Object) "Callbacks must not be null");
            this.Hn = roomStatusUpdateListener;
            this.Ho = realTimeMessageReceivedListener;
        }

        public void a(DataHolder dataHolder, String[] strArr) {
            fx.this.a((b<?>) new af<Object>(this.Hn, dataHolder, strArr));
        }

        public void b(DataHolder dataHolder, String[] strArr) {
            fx.this.a((b<?>) new ag<Object>(this.Hn, dataHolder, strArr));
        }

        public void c(DataHolder dataHolder, String[] strArr) {
            fx.this.a((b<?>) new ah<Object>(this.Hn, dataHolder, strArr));
        }

        public void d(DataHolder dataHolder, String[] strArr) {
            fx.this.a((b<?>) new ad<Object>(this.Hn, dataHolder, strArr));
        }

        public void e(DataHolder dataHolder, String[] strArr) {
            fx.this.a((b<?>) new ac<Object>(this.Hn, dataHolder, strArr));
        }

        public void f(DataHolder dataHolder, String[] strArr) {
            fx.this.a((b<?>) new ae<Object>(this.Hn, dataHolder, strArr));
        }

        public void onLeftRoom(int statusCode, String externalRoomId) {
            fx.this.a((b<?>) new v<Object>(this.Hm, statusCode, externalRoomId));
        }

        public void onP2PConnected(String participantId) {
            fx.this.a((b<?>) new aa<Object>(this.Hn, participantId));
        }

        public void onP2PDisconnected(String participantId) {
            fx.this.a((b<?>) new ab<Object>(this.Hn, participantId));
        }

        public void onRealTimeMessageReceived(RealTimeMessage message) {
            fx.this.a((b<?>) new z<Object>(this.Ho, message));
        }

        public void s(DataHolder dataHolder) {
            fx.this.a((b<?>) new ba<Object>(this.Hm, dataHolder));
        }

        public void t(DataHolder dataHolder) {
            fx.this.a((b<?>) new q<Object>(this.Hm, dataHolder));
        }

        public void u(DataHolder dataHolder) {
            fx.this.a((b<?>) new az<Object>(this.Hn, dataHolder));
        }

        public void v(DataHolder dataHolder) {
            fx.this.a((b<?>) new aw<Object>(this.Hn, dataHolder));
        }

        public void w(DataHolder dataHolder) {
            fx.this.a((b<?>) new ay<Object>(this.Hm, dataHolder));
        }

        public void x(DataHolder dataHolder) {
            fx.this.a((b<?>) new h<Object>(this.Hn, dataHolder));
        }

        public void y(DataHolder dataHolder) {
            fx.this.a((b<?>) new i<Object>(this.Hn, dataHolder));
        }
    }

    final class ay extends b {
        ay(RoomUpdateListener roomUpdateListener, DataHolder dataHolder) {
            super(roomUpdateListener, dataHolder);
        }

        public void a(RoomUpdateListener roomUpdateListener, Room room, int i) {
            roomUpdateListener.onRoomConnected(i, room);
        }
    }

    final class az extends c {
        az(RoomStatusUpdateListener roomStatusUpdateListener, DataHolder dataHolder) {
            super(roomStatusUpdateListener, dataHolder);
        }

        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            roomStatusUpdateListener.onRoomConnecting(room);
        }
    }

    abstract class b extends com.google.android.gms.internal.eh.d<RoomUpdateListener> {
        b(RoomUpdateListener roomUpdateListener, DataHolder dataHolder) {
            super(roomUpdateListener, dataHolder);
        }

        /* access modifiers changed from: protected */
        public void a(RoomUpdateListener roomUpdateListener, DataHolder dataHolder) {
            a(roomUpdateListener, fx.this.G(dataHolder), dataHolder.getStatusCode());
        }

        /* access modifiers changed from: protected */
        public abstract void a(RoomUpdateListener roomUpdateListener, Room room, int i);
    }

    final class ba extends b {
        public ba(RoomUpdateListener roomUpdateListener, DataHolder dataHolder) {
            super(roomUpdateListener, dataHolder);
        }

        public void a(RoomUpdateListener roomUpdateListener, Room room, int i) {
            roomUpdateListener.onRoomCreated(i, room);
        }
    }

    final class bb extends fw {
        private final com.google.android.gms.common.api.a.c<Status> vj;

        public bb(com.google.android.gms.common.api.a.c<Status> cVar) {
            this.vj = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void cM() {
            fx.this.a((b<?>) new bc<Object>(this.vj, new Status(0)));
        }
    }

    final class bc extends b<com.google.android.gms.common.api.a.c<Status>> {
        private final Status vl;

        public bc(com.google.android.gms.common.api.a.c<Status> cVar, Status status) {
            super(cVar);
            this.vl = status;
        }

        /* renamed from: c */
        public void a(com.google.android.gms.common.api.a.c<Status> cVar) {
            cVar.b(this.vl);
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }
    }

    final class bd extends fw {
        private final com.google.android.gms.common.api.a.c<SubmitScoreResult> vj;

        public bd(com.google.android.gms.common.api.a.c<SubmitScoreResult> cVar) {
            this.vj = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void d(DataHolder dataHolder) {
            fx.this.a((b<?>) new be<Object>(this.vj, dataHolder));
        }
    }

    final class be extends av<com.google.android.gms.common.api.a.c<SubmitScoreResult>> implements SubmitScoreResult {
        private final ScoreSubmissionData Hp;

        public be(com.google.android.gms.common.api.a.c<SubmitScoreResult> cVar, DataHolder dataHolder) {
            super(cVar, dataHolder);
            try {
                this.Hp = new ScoreSubmissionData(dataHolder);
            } finally {
                dataHolder.close();
            }
        }

        public void a(com.google.android.gms.common.api.a.c<SubmitScoreResult> cVar, DataHolder dataHolder) {
            cVar.b(this);
        }

        public ScoreSubmissionData getScoreData() {
            return this.Hp;
        }
    }

    abstract class bf<T extends com.google.android.gms.common.api.a.c<?>> extends av<T> {
        final TurnBasedMatch GX;

        bf(T t, DataHolder dataHolder) {
            super(t, dataHolder);
            TurnBasedMatchBuffer turnBasedMatchBuffer = new TurnBasedMatchBuffer(dataHolder);
            try {
                if (turnBasedMatchBuffer.getCount() > 0) {
                    this.GX = (TurnBasedMatch) ((TurnBasedMatch) turnBasedMatchBuffer.get(0)).freeze();
                } else {
                    this.GX = null;
                }
            } finally {
                turnBasedMatchBuffer.close();
            }
        }

        /* access modifiers changed from: protected */
        public void a(T t, DataHolder dataHolder) {
            h(t);
        }

        public TurnBasedMatch getMatch() {
            return this.GX;
        }

        /* access modifiers changed from: 0000 */
        public abstract void h(T t);
    }

    final class bg extends fw {
        private final com.google.android.gms.common.api.a.c<CancelMatchResult> Hq;

        public bg(com.google.android.gms.common.api.a.c<CancelMatchResult> cVar) {
            this.Hq = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void f(int i, String str) {
            fx.this.a((b<?>) new bh<Object>(this.Hq, new Status(i), str));
        }
    }

    final class bh extends b<com.google.android.gms.common.api.a.c<CancelMatchResult>> implements CancelMatchResult {
        private final String Hr;
        private final Status vl;

        bh(com.google.android.gms.common.api.a.c<CancelMatchResult> cVar, Status status, String str) {
            super(cVar);
            this.vl = status;
            this.Hr = str;
        }

        /* renamed from: c */
        public void a(com.google.android.gms.common.api.a.c<CancelMatchResult> cVar) {
            cVar.b(this);
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }

        public String getMatchId() {
            return this.Hr;
        }

        public Status getStatus() {
            return this.vl;
        }
    }

    final class bi extends fw {
        private final com.google.android.gms.common.api.a.c<InitiateMatchResult> Hs;

        public bi(com.google.android.gms.common.api.a.c<InitiateMatchResult> cVar) {
            this.Hs = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void o(DataHolder dataHolder) {
            fx.this.a((b<?>) new bj<Object>(this.Hs, dataHolder));
        }
    }

    final class bj extends bf<com.google.android.gms.common.api.a.c<InitiateMatchResult>> implements InitiateMatchResult {
        bj(com.google.android.gms.common.api.a.c<InitiateMatchResult> cVar, DataHolder dataHolder) {
            super(cVar, dataHolder);
        }

        /* access modifiers changed from: protected */
        public void h(com.google.android.gms.common.api.a.c<InitiateMatchResult> cVar) {
            cVar.b(this);
        }
    }

    final class bk extends fw {
        private final com.google.android.gms.common.api.a.c<LeaveMatchResult> Ht;

        public bk(com.google.android.gms.common.api.a.c<LeaveMatchResult> cVar) {
            this.Ht = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void q(DataHolder dataHolder) {
            fx.this.a((b<?>) new bl<Object>(this.Ht, dataHolder));
        }
    }

    final class bl extends bf<com.google.android.gms.common.api.a.c<LeaveMatchResult>> implements LeaveMatchResult {
        bl(com.google.android.gms.common.api.a.c<LeaveMatchResult> cVar, DataHolder dataHolder) {
            super(cVar, dataHolder);
        }

        /* access modifiers changed from: protected */
        public void h(com.google.android.gms.common.api.a.c<LeaveMatchResult> cVar) {
            cVar.b(this);
        }
    }

    final class bm extends fw {
        private final com.google.android.gms.common.api.a.c<LoadMatchResult> Hu;

        public bm(com.google.android.gms.common.api.a.c<LoadMatchResult> cVar) {
            this.Hu = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void n(DataHolder dataHolder) {
            fx.this.a((b<?>) new bn<Object>(this.Hu, dataHolder));
        }
    }

    final class bn extends bf<com.google.android.gms.common.api.a.c<LoadMatchResult>> implements LoadMatchResult {
        bn(com.google.android.gms.common.api.a.c<LoadMatchResult> cVar, DataHolder dataHolder) {
            super(cVar, dataHolder);
        }

        /* access modifiers changed from: protected */
        public void h(com.google.android.gms.common.api.a.c<LoadMatchResult> cVar) {
            cVar.b(this);
        }
    }

    final class bo extends fw {
        private final com.google.android.gms.common.api.a.c<UpdateMatchResult> Hv;

        public bo(com.google.android.gms.common.api.a.c<UpdateMatchResult> cVar) {
            this.Hv = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void p(DataHolder dataHolder) {
            fx.this.a((b<?>) new bp<Object>(this.Hv, dataHolder));
        }
    }

    final class bp extends bf<com.google.android.gms.common.api.a.c<UpdateMatchResult>> implements UpdateMatchResult {
        bp(com.google.android.gms.common.api.a.c<UpdateMatchResult> cVar, DataHolder dataHolder) {
            super(cVar, dataHolder);
        }

        /* access modifiers changed from: protected */
        public void h(com.google.android.gms.common.api.a.c<UpdateMatchResult> cVar) {
            cVar.b(this);
        }
    }

    final class bq extends fw {
        private final com.google.android.gms.common.api.a.c<LoadMatchesResult> Hw;

        public bq(com.google.android.gms.common.api.a.c<LoadMatchesResult> cVar) {
            this.Hw = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void a(int i, Bundle bundle) {
            bundle.setClassLoader(getClass().getClassLoader());
            fx.this.a((b<?>) new br<Object>(this.Hw, new Status(i), bundle));
        }
    }

    final class br extends b<com.google.android.gms.common.api.a.c<LoadMatchesResult>> implements LoadMatchesResult {
        private final LoadMatchesResponse Hx;
        private final Status vl;

        br(com.google.android.gms.common.api.a.c<LoadMatchesResult> cVar, Status status, Bundle bundle) {
            super(cVar);
            this.vl = status;
            this.Hx = new LoadMatchesResponse(bundle);
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void a(com.google.android.gms.common.api.a.c<LoadMatchesResult> cVar) {
            cVar.b(this);
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }

        public LoadMatchesResponse getMatches() {
            return this.Hx;
        }

        public Status getStatus() {
            return this.vl;
        }

        public void release() {
            this.Hx.close();
        }
    }

    abstract class c extends com.google.android.gms.internal.eh.d<RoomStatusUpdateListener> {
        c(RoomStatusUpdateListener roomStatusUpdateListener, DataHolder dataHolder) {
            super(roomStatusUpdateListener, dataHolder);
        }

        /* access modifiers changed from: protected */
        public void a(RoomStatusUpdateListener roomStatusUpdateListener, DataHolder dataHolder) {
            a(roomStatusUpdateListener, fx.this.G(dataHolder));
        }

        /* access modifiers changed from: protected */
        public abstract void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room);
    }

    final class d extends fw {
        private final com.google.android.gms.common.api.a.c<UpdateAchievementResult> vj;

        d(com.google.android.gms.common.api.a.c<UpdateAchievementResult> cVar) {
            this.vj = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void e(int i, String str) {
            fx.this.a((b<?>) new e<Object>(this.vj, i, str));
        }
    }

    final class e extends b<com.google.android.gms.common.api.a.c<UpdateAchievementResult>> implements UpdateAchievementResult {
        private final String GK;
        private final Status vl;

        e(com.google.android.gms.common.api.a.c<UpdateAchievementResult> cVar, int i, String str) {
            super(cVar);
            this.vl = new Status(i);
            this.GK = str;
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void a(com.google.android.gms.common.api.a.c<UpdateAchievementResult> cVar) {
            cVar.b(this);
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }

        public String getAchievementId() {
            return this.GK;
        }

        public Status getStatus() {
            return this.vl;
        }
    }

    final class f extends fw {
        private final com.google.android.gms.common.api.a.c<LoadAchievementsResult> vj;

        f(com.google.android.gms.common.api.a.c<LoadAchievementsResult> cVar) {
            this.vj = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void b(DataHolder dataHolder) {
            fx.this.a((b<?>) new g<Object>(this.vj, dataHolder));
        }
    }

    final class g extends av<com.google.android.gms.common.api.a.c<LoadAchievementsResult>> implements LoadAchievementsResult {
        private final AchievementBuffer GL;

        g(com.google.android.gms.common.api.a.c<LoadAchievementsResult> cVar, DataHolder dataHolder) {
            super(cVar, dataHolder);
            this.GL = new AchievementBuffer(dataHolder);
        }

        /* access modifiers changed from: protected */
        public void a(com.google.android.gms.common.api.a.c<LoadAchievementsResult> cVar, DataHolder dataHolder) {
            cVar.b(this);
        }

        public AchievementBuffer getAchievements() {
            return this.GL;
        }
    }

    final class h extends c {
        h(RoomStatusUpdateListener roomStatusUpdateListener, DataHolder dataHolder) {
            super(roomStatusUpdateListener, dataHolder);
        }

        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            roomStatusUpdateListener.onConnectedToRoom(room);
        }
    }

    final class i extends c {
        i(RoomStatusUpdateListener roomStatusUpdateListener, DataHolder dataHolder) {
            super(roomStatusUpdateListener, dataHolder);
        }

        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            roomStatusUpdateListener.onDisconnectedFromRoom(room);
        }
    }

    final class j extends fw {
        private final com.google.android.gms.common.api.a.c<LoadGamesResult> vj;

        j(com.google.android.gms.common.api.a.c<LoadGamesResult> cVar) {
            this.vj = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void g(DataHolder dataHolder) {
            fx.this.a((b<?>) new k<Object>(this.vj, dataHolder));
        }
    }

    final class k extends av<com.google.android.gms.common.api.a.c<LoadGamesResult>> implements LoadGamesResult {
        private final GameBuffer GM;

        k(com.google.android.gms.common.api.a.c<LoadGamesResult> cVar, DataHolder dataHolder) {
            super(cVar, dataHolder);
            this.GM = new GameBuffer(dataHolder);
        }

        /* access modifiers changed from: protected */
        public void a(com.google.android.gms.common.api.a.c<LoadGamesResult> cVar, DataHolder dataHolder) {
            cVar.b(this);
        }

        public GameBuffer getGames() {
            return this.GM;
        }
    }

    final class l extends fw {
        private final OnInvitationReceivedListener GN;

        l(OnInvitationReceivedListener onInvitationReceivedListener) {
            this.GN = onInvitationReceivedListener;
        }

        public void l(DataHolder dataHolder) {
            InvitationBuffer invitationBuffer = new InvitationBuffer(dataHolder);
            Invitation invitation = null;
            try {
                if (invitationBuffer.getCount() > 0) {
                    invitation = (Invitation) ((Invitation) invitationBuffer.get(0)).freeze();
                }
                if (invitation != null) {
                    fx.this.a((b<?>) new m<Object>(this.GN, invitation));
                }
            } finally {
                invitationBuffer.close();
            }
        }

        public void onInvitationRemoved(String invitationId) {
            fx.this.a((b<?>) new n<Object>(this.GN, invitationId));
        }
    }

    final class m extends b<OnInvitationReceivedListener> {
        private final Invitation GO;

        m(OnInvitationReceivedListener onInvitationReceivedListener, Invitation invitation) {
            super(onInvitationReceivedListener);
            this.GO = invitation;
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void a(OnInvitationReceivedListener onInvitationReceivedListener) {
            onInvitationReceivedListener.onInvitationReceived(this.GO);
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }
    }

    final class n extends b<OnInvitationReceivedListener> {
        private final String GP;

        n(OnInvitationReceivedListener onInvitationReceivedListener, String str) {
            super(onInvitationReceivedListener);
            this.GP = str;
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void a(OnInvitationReceivedListener onInvitationReceivedListener) {
            onInvitationReceivedListener.onInvitationRemoved(this.GP);
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }
    }

    final class o extends fw {
        private final com.google.android.gms.common.api.a.c<LoadInvitationsResult> vj;

        o(com.google.android.gms.common.api.a.c<LoadInvitationsResult> cVar) {
            this.vj = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void k(DataHolder dataHolder) {
            fx.this.a((b<?>) new p<Object>(this.vj, dataHolder));
        }
    }

    final class p extends av<com.google.android.gms.common.api.a.c<LoadInvitationsResult>> implements LoadInvitationsResult {
        private final InvitationBuffer GQ;

        p(com.google.android.gms.common.api.a.c<LoadInvitationsResult> cVar, DataHolder dataHolder) {
            super(cVar, dataHolder);
            this.GQ = new InvitationBuffer(dataHolder);
        }

        /* access modifiers changed from: protected */
        public void a(com.google.android.gms.common.api.a.c<LoadInvitationsResult> cVar, DataHolder dataHolder) {
            cVar.b(this);
        }

        public InvitationBuffer getInvitations() {
            return this.GQ;
        }
    }

    final class q extends b {
        public q(RoomUpdateListener roomUpdateListener, DataHolder dataHolder) {
            super(roomUpdateListener, dataHolder);
        }

        public void a(RoomUpdateListener roomUpdateListener, Room room, int i) {
            roomUpdateListener.onJoinedRoom(i, room);
        }
    }

    final class r extends fw {
        private final com.google.android.gms.common.api.a.c<LoadScoresResult> vj;

        r(com.google.android.gms.common.api.a.c<LoadScoresResult> cVar) {
            this.vj = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void a(DataHolder dataHolder, DataHolder dataHolder2) {
            fx.this.a((b<?>) new s<Object>(this.vj, dataHolder, dataHolder2));
        }
    }

    final class s extends av<com.google.android.gms.common.api.a.c<LoadScoresResult>> implements LoadScoresResult {
        private final com.google.android.gms.games.leaderboard.a GR;
        private final LeaderboardScoreBuffer GS;

        /* JADX INFO: finally extract failed */
        s(com.google.android.gms.common.api.a.c<LoadScoresResult> cVar, DataHolder dataHolder, DataHolder dataHolder2) {
            super(cVar, dataHolder2);
            LeaderboardBuffer leaderboardBuffer = new LeaderboardBuffer(dataHolder);
            try {
                if (leaderboardBuffer.getCount() > 0) {
                    this.GR = (com.google.android.gms.games.leaderboard.a) ((Leaderboard) leaderboardBuffer.get(0)).freeze();
                } else {
                    this.GR = null;
                }
                leaderboardBuffer.close();
                this.GS = new LeaderboardScoreBuffer(dataHolder2);
            } catch (Throwable th) {
                leaderboardBuffer.close();
                throw th;
            }
        }

        /* access modifiers changed from: protected */
        public void a(com.google.android.gms.common.api.a.c<LoadScoresResult> cVar, DataHolder dataHolder) {
            cVar.b(this);
        }

        public Leaderboard getLeaderboard() {
            return this.GR;
        }

        public LeaderboardScoreBuffer getScores() {
            return this.GS;
        }
    }

    final class t extends fw {
        private final com.google.android.gms.common.api.a.c<LeaderboardMetadataResult> vj;

        t(com.google.android.gms.common.api.a.c<LeaderboardMetadataResult> cVar) {
            this.vj = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void c(DataHolder dataHolder) {
            fx.this.a((b<?>) new u<Object>(this.vj, dataHolder));
        }
    }

    final class u extends av<com.google.android.gms.common.api.a.c<LeaderboardMetadataResult>> implements LeaderboardMetadataResult {
        private final LeaderboardBuffer GT;

        u(com.google.android.gms.common.api.a.c<LeaderboardMetadataResult> cVar, DataHolder dataHolder) {
            super(cVar, dataHolder);
            this.GT = new LeaderboardBuffer(dataHolder);
        }

        /* access modifiers changed from: protected */
        public void a(com.google.android.gms.common.api.a.c<LeaderboardMetadataResult> cVar, DataHolder dataHolder) {
            cVar.b(this);
        }

        public LeaderboardBuffer getLeaderboards() {
            return this.GT;
        }
    }

    final class v extends b<RoomUpdateListener> {
        private final String GU;
        private final int yJ;

        v(RoomUpdateListener roomUpdateListener, int i, String str) {
            super(roomUpdateListener);
            this.yJ = i;
            this.GU = str;
        }

        public void a(RoomUpdateListener roomUpdateListener) {
            roomUpdateListener.onLeftRoom(this.yJ, this.GU);
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }
    }

    final class w extends b<OnTurnBasedMatchUpdateReceivedListener> {
        private final String GV;

        w(OnTurnBasedMatchUpdateReceivedListener onTurnBasedMatchUpdateReceivedListener, String str) {
            super(onTurnBasedMatchUpdateReceivedListener);
            this.GV = str;
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void a(OnTurnBasedMatchUpdateReceivedListener onTurnBasedMatchUpdateReceivedListener) {
            onTurnBasedMatchUpdateReceivedListener.onTurnBasedMatchRemoved(this.GV);
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }
    }

    final class x extends fw {
        private final OnTurnBasedMatchUpdateReceivedListener GW;

        x(OnTurnBasedMatchUpdateReceivedListener onTurnBasedMatchUpdateReceivedListener) {
            this.GW = onTurnBasedMatchUpdateReceivedListener;
        }

        public void onTurnBasedMatchRemoved(String matchId) {
            fx.this.a((b<?>) new w<Object>(this.GW, matchId));
        }

        public void r(DataHolder dataHolder) {
            TurnBasedMatchBuffer turnBasedMatchBuffer = new TurnBasedMatchBuffer(dataHolder);
            TurnBasedMatch turnBasedMatch = null;
            try {
                if (turnBasedMatchBuffer.getCount() > 0) {
                    turnBasedMatch = (TurnBasedMatch) ((TurnBasedMatch) turnBasedMatchBuffer.get(0)).freeze();
                }
                if (turnBasedMatch != null) {
                    fx.this.a((b<?>) new y<Object>(this.GW, turnBasedMatch));
                }
            } finally {
                turnBasedMatchBuffer.close();
            }
        }
    }

    final class y extends b<OnTurnBasedMatchUpdateReceivedListener> {
        private final TurnBasedMatch GX;

        y(OnTurnBasedMatchUpdateReceivedListener onTurnBasedMatchUpdateReceivedListener, TurnBasedMatch turnBasedMatch) {
            super(onTurnBasedMatchUpdateReceivedListener);
            this.GX = turnBasedMatch;
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void a(OnTurnBasedMatchUpdateReceivedListener onTurnBasedMatchUpdateReceivedListener) {
            onTurnBasedMatchUpdateReceivedListener.onTurnBasedMatchReceived(this.GX);
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }
    }

    final class z extends b<RealTimeMessageReceivedListener> {
        private final RealTimeMessage GY;

        z(RealTimeMessageReceivedListener realTimeMessageReceivedListener, RealTimeMessage realTimeMessage) {
            super(realTimeMessageReceivedListener);
            this.GY = realTimeMessage;
        }

        public void a(RealTimeMessageReceivedListener realTimeMessageReceivedListener) {
            if (realTimeMessageReceivedListener != null) {
                realTimeMessageReceivedListener.onRealTimeMessageReceived(this.GY);
            }
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }
    }

    public fx(Context context, Looper looper, String str, String str2, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, String[] strArr, int i2, View view, boolean z2, boolean z3, int i3, boolean z4, int i4) {
        super(context, looper, connectionCallbacks, onConnectionFailedListener, strArr);
        this.Gv = str;
        this.vi = (String) er.f(str2);
        this.GD = new Binder();
        this.Gw = new HashMap();
        this.Gz = gd.a(this, i2);
        e(view);
        this.GB = z3;
        this.GC = i3;
        this.GE = (long) hashCode();
        this.GF = z2;
        this.GH = z4;
        this.GG = i4;
        registerConnectionCallbacks((ConnectionCallbacks) this);
        registerConnectionFailedListener((OnConnectionFailedListener) this);
    }

    /* access modifiers changed from: private */
    public Room G(DataHolder dataHolder) {
        com.google.android.gms.games.multiplayer.realtime.a aVar = new com.google.android.gms.games.multiplayer.realtime.a(dataHolder);
        Room room = null;
        try {
            if (aVar.getCount() > 0) {
                room = (Room) ((Room) aVar.get(0)).freeze();
            }
            return room;
        } finally {
            aVar.close();
        }
    }

    private RealTimeSocket aw(String str) {
        try {
            ParcelFileDescriptor aD = ((gb) eb()).aD(str);
            if (aD != null) {
                fz.f("GamesClientImpl", "Created native libjingle socket.");
                gc gcVar = new gc(aD);
                this.Gw.put(str, gcVar);
                return gcVar;
            }
            fz.f("GamesClientImpl", "Unable to create native libjingle socket, resorting to old socket.");
            String ay2 = ((gb) eb()).ay(str);
            if (ay2 == null) {
                return null;
            }
            LocalSocket localSocket = new LocalSocket();
            try {
                localSocket.connect(new LocalSocketAddress(ay2));
                ge geVar = new ge(localSocket, str);
                this.Gw.put(str, geVar);
                return geVar;
            } catch (IOException e2) {
                fz.h("GamesClientImpl", "connect() call failed on socket: " + e2.getMessage());
                return null;
            }
        } catch (RemoteException e3) {
            fz.h("GamesClientImpl", "Unable to create socket. Service died.");
            return null;
        }
    }

    private void fG() {
        for (RealTimeSocket close : this.Gw.values()) {
            try {
                close.close();
            } catch (IOException e2) {
                fz.a("GamesClientImpl", "IOException:", e2);
            }
        }
        this.Gw.clear();
    }

    private void fm() {
        this.Gx = null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: H */
    public gb p(IBinder iBinder) {
        return com.google.android.gms.internal.gb.a.J(iBinder);
    }

    public int a(ReliableMessageSentCallback reliableMessageSentCallback, byte[] bArr, String str, String str2) {
        try {
            return ((gb) eb()).a((ga) new an(reliableMessageSentCallback), bArr, str, str2);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return -1;
        }
    }

    public int a(byte[] bArr, String str, String[] strArr) {
        er.b(strArr, (Object) "Participant IDs must not be null");
        try {
            return ((gb) eb()).b(bArr, str, strArr);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return -1;
        }
    }

    public Intent a(int i2, int i3, boolean z2) {
        try {
            return ((gb) eb()).a(i2, i3, z2);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return null;
        }
    }

    public Intent a(int i2, byte[] bArr, int i3, Bitmap bitmap, String str) {
        try {
            Intent a2 = ((gb) eb()).a(i2, bArr, i3, str);
            er.b(bitmap, (Object) "Must provide a non null icon");
            a2.putExtra("com.google.android.gms.games.REQUEST_ITEM_ICON", bitmap);
            return a2;
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return null;
        }
    }

    public Intent a(Room room, int i2) {
        try {
            return ((gb) eb()).a((RoomEntity) room.freeze(), i2);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2, IBinder iBinder, Bundle bundle) {
        if (i2 == 0 && bundle != null) {
            this.GA = bundle.getBoolean("show_welcome_popup");
        }
        super.a(i2, iBinder, bundle);
    }

    public void a(IBinder iBinder, Bundle bundle) {
        if (isConnected()) {
            try {
                ((gb) eb()).a(iBinder, bundle);
            } catch (RemoteException e2) {
                fz.g("GamesClientImpl", "service died");
            }
        }
    }

    public void a(com.google.android.gms.common.api.a.c<LoadRequestsResult> cVar, int i2, int i3, int i4) {
        try {
            ((gb) eb()).a((ga) new ar(cVar), i2, i3, i4);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<LoadPlayersResult> cVar, int i2, boolean z2, boolean z3) {
        try {
            ((gb) eb()).a((ga) new ak(cVar), i2, z2, z3);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<LoadMatchesResult> cVar, int i2, int[] iArr) {
        try {
            ((gb) eb()).a((ga) new bq(cVar), i2, iArr);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<LoadScoresResult> cVar, LeaderboardScoreBuffer leaderboardScoreBuffer, int i2, int i3) {
        try {
            ((gb) eb()).a((ga) new r(cVar), leaderboardScoreBuffer.fX().fY(), i2, i3);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<InitiateMatchResult> cVar, TurnBasedMatchConfig turnBasedMatchConfig) {
        try {
            ((gb) eb()).a((ga) new bi(cVar), turnBasedMatchConfig.getVariant(), turnBasedMatchConfig.getMinPlayers(), turnBasedMatchConfig.getInvitedPlayerIds(), turnBasedMatchConfig.getAutoMatchCriteria());
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<LoadPlayersResult> cVar, String str) {
        try {
            ((gb) eb()).a((ga) new ak(cVar), str);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<UpdateAchievementResult> cVar, String str, int i2) {
        try {
            ((gb) eb()).a((ga) cVar == null ? null : new d(cVar), str, i2, this.Gz.fP(), this.Gz.fO());
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<LoadScoresResult> cVar, String str, int i2, int i3, int i4, boolean z2) {
        try {
            ((gb) eb()).a((ga) new r(cVar), str, i2, i3, i4, z2);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<LoadPlayersResult> cVar, String str, int i2, boolean z2, boolean z3) {
        if (!str.equals("playedWith")) {
            throw new IllegalArgumentException("Invalid player collection: " + str);
        }
        try {
            ((gb) eb()).d(new ak(cVar), str, i2, z2, z3);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<SubmitScoreResult> cVar, String str, long j2, String str2) {
        try {
            ((gb) eb()).a((ga) cVar == null ? null : new bd(cVar), str, j2, str2);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<LeaveMatchResult> cVar, String str, String str2) {
        try {
            ((gb) eb()).c((ga) new bk(cVar), str, str2);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<LoadPlayerScoreResult> cVar, String str, String str2, int i2, int i3) {
        try {
            ((gb) eb()).a((ga) new ai(cVar), str, str2, i2, i3);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<LeaderboardMetadataResult> cVar, String str, boolean z2) {
        try {
            ((gb) eb()).c((ga) new t(cVar), str, z2);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<UpdateMatchResult> cVar, String str, byte[] bArr, String str2, ParticipantResult[] participantResultArr) {
        try {
            ((gb) eb()).a((ga) new bo(cVar), str, bArr, str2, participantResultArr);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<UpdateMatchResult> cVar, String str, byte[] bArr, ParticipantResult[] participantResultArr) {
        try {
            ((gb) eb()).a((ga) new bo(cVar), str, bArr, participantResultArr);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<LoadPlayersResult> cVar, boolean z2) {
        try {
            ((gb) eb()).c((ga) new ak(cVar), z2);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<UpdateRequestsResult> cVar, String[] strArr) {
        try {
            ((gb) eb()).a((ga) new at(cVar), strArr);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(OnInvitationReceivedListener onInvitationReceivedListener) {
        try {
            ((gb) eb()).a((ga) new l(onInvitationReceivedListener), this.GE);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(RoomConfig roomConfig) {
        try {
            ((gb) eb()).a((ga) new ax(roomConfig.getRoomUpdateListener(), roomConfig.getRoomStatusUpdateListener(), roomConfig.getMessageReceivedListener()), (IBinder) this.GD, roomConfig.getVariant(), roomConfig.getInvitedPlayerIds(), roomConfig.getAutoMatchCriteria(), roomConfig.isSocketEnabled(), this.GE);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(RoomUpdateListener roomUpdateListener, String str) {
        try {
            ((gb) eb()).c((ga) new ax(roomUpdateListener), str);
            fG();
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(OnTurnBasedMatchUpdateReceivedListener onTurnBasedMatchUpdateReceivedListener) {
        try {
            ((gb) eb()).b((ga) new x(onTurnBasedMatchUpdateReceivedListener), this.GE);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void a(OnRequestReceivedListener onRequestReceivedListener) {
        try {
            ((gb) eb()).c((ga) new ao(onRequestReceivedListener), this.GE);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    /* access modifiers changed from: protected */
    public void a(en enVar, com.google.android.gms.internal.eh.e eVar) throws RemoteException {
        String locale = getContext().getResources().getConfiguration().locale.toString();
        Bundle bundle = new Bundle();
        bundle.putBoolean("com.google.android.gms.games.key.isHeadless", this.GF);
        bundle.putBoolean("com.google.android.gms.games.key.showConnectingPopup", this.GB);
        bundle.putInt("com.google.android.gms.games.key.connectingPopupGravity", this.GC);
        bundle.putBoolean("com.google.android.gms.games.key.retryingSignIn", this.GH);
        bundle.putInt("com.google.android.gms.games.key.sdkVariant", this.GG);
        enVar.a(eVar, GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, getContext().getPackageName(), this.vi, ea(), this.Gv, this.Gz.fP(), locale, bundle);
    }

    /* access modifiers changed from: protected */
    public String aF() {
        return "com.google.android.gms.games.service.START";
    }

    /* access modifiers changed from: protected */
    public String aG() {
        return "com.google.android.gms.games.internal.IGamesService";
    }

    public void aT(int i2) {
        this.Gz.setGravity(i2);
    }

    public void aU(int i2) {
        try {
            ((gb) eb()).aU(i2);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public Intent au(String str) {
        try {
            return ((gb) eb()).au(str);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return null;
        }
    }

    public void av(String str) {
        try {
            ((gb) eb()).aC(str);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public Intent b(int i2, int i3, boolean z2) {
        try {
            return ((gb) eb()).b(i2, i3, z2);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return null;
        }
    }

    public void b(com.google.android.gms.common.api.a.c<Status> cVar) {
        try {
            ((gb) eb()).a(new bb(cVar));
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void b(com.google.android.gms.common.api.a.c<UpdateAchievementResult> cVar, String str) {
        try {
            ((gb) eb()).a((ga) cVar == null ? null : new d(cVar), str, this.Gz.fP(), this.Gz.fO());
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void b(com.google.android.gms.common.api.a.c<UpdateAchievementResult> cVar, String str, int i2) {
        try {
            ((gb) eb()).b((ga) cVar == null ? null : new d(cVar), str, i2, this.Gz.fP(), this.Gz.fO());
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void b(com.google.android.gms.common.api.a.c<LoadScoresResult> cVar, String str, int i2, int i3, int i4, boolean z2) {
        try {
            ((gb) eb()).b(new r(cVar), str, i2, i3, i4, z2);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void b(com.google.android.gms.common.api.a.c<LeaderboardMetadataResult> cVar, boolean z2) {
        try {
            ((gb) eb()).b((ga) new t(cVar), z2);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void b(com.google.android.gms.common.api.a.c<UpdateRequestsResult> cVar, String[] strArr) {
        try {
            ((gb) eb()).b((ga) new at(cVar), strArr);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void b(RoomConfig roomConfig) {
        try {
            ((gb) eb()).a((ga) new ax(roomConfig.getRoomUpdateListener(), roomConfig.getRoomStatusUpdateListener(), roomConfig.getMessageReceivedListener()), (IBinder) this.GD, roomConfig.getInvitationId(), roomConfig.isSocketEnabled(), this.GE);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    /* access modifiers changed from: protected */
    public void b(String... strArr) {
        boolean z2 = false;
        boolean z3 = false;
        for (String str : strArr) {
            if (str.equals(Scopes.GAMES)) {
                z3 = true;
            } else if (str.equals("https://www.googleapis.com/auth/games.firstparty")) {
                z2 = true;
            }
        }
        if (z2) {
            er.a(!z3, String.format("Cannot have both %s and %s!", new Object[]{Scopes.GAMES, "https://www.googleapis.com/auth/games.firstparty"}));
            return;
        }
        er.a(z3, String.format("Games APIs requires %s to function.", new Object[]{Scopes.GAMES}));
    }

    public void c(com.google.android.gms.common.api.a.c<LoadInvitationsResult> cVar, int i2) {
        try {
            ((gb) eb()).a((ga) new o(cVar), i2);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void c(com.google.android.gms.common.api.a.c<UpdateAchievementResult> cVar, String str) {
        try {
            ((gb) eb()).b((ga) cVar == null ? null : new d(cVar), str, this.Gz.fP(), this.Gz.fO());
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void c(com.google.android.gms.common.api.a.c<LoadAchievementsResult> cVar, boolean z2) {
        try {
            ((gb) eb()).a((ga) new f(cVar), z2);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public Bundle cY() {
        try {
            Bundle cY = ((gb) eb()).cY();
            if (cY == null) {
                return cY;
            }
            cY.setClassLoader(fx.class.getClassLoader());
            return cY;
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return null;
        }
    }

    public void connect() {
        fm();
        super.connect();
    }

    public int d(byte[] bArr, String str) {
        try {
            return ((gb) eb()).b(bArr, str, (String[]) null);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return -1;
        }
    }

    public void d(com.google.android.gms.common.api.a.c<InitiateMatchResult> cVar, String str) {
        try {
            ((gb) eb()).l((ga) new bi(cVar), str);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void disconnect() {
        this.GA = false;
        if (isConnected()) {
            try {
                gb gbVar = (gb) eb();
                gbVar.fH();
                gbVar.n(this.GE);
            } catch (RemoteException e2) {
                fz.g("GamesClientImpl", "Failed to notify client disconnect.");
            }
        }
        fG();
        super.disconnect();
    }

    public void e(View view) {
        this.Gz.f(view);
    }

    public void e(com.google.android.gms.common.api.a.c<InitiateMatchResult> cVar, String str) {
        try {
            ((gb) eb()).m((ga) new bi(cVar), str);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void f(com.google.android.gms.common.api.a.c<LeaveMatchResult> cVar, String str) {
        try {
            ((gb) eb()).o((ga) new bk(cVar), str);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public int fA() {
        try {
            return ((gb) eb()).fA();
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return 4368;
        }
    }

    public String fB() {
        try {
            return ((gb) eb()).fB();
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return null;
        }
    }

    public int fC() {
        try {
            return ((gb) eb()).fC();
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return 2;
        }
    }

    public Intent fD() {
        try {
            return ((gb) eb()).fD();
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return null;
        }
    }

    public int fE() {
        try {
            return ((gb) eb()).fE();
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return 2;
        }
    }

    public int fF() {
        try {
            return ((gb) eb()).fF();
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return 2;
        }
    }

    public void fH() {
        if (isConnected()) {
            try {
                ((gb) eb()).fH();
            } catch (RemoteException e2) {
                fz.g("GamesClientImpl", "service died");
            }
        }
    }

    public String fn() {
        try {
            return ((gb) eb()).fn();
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return null;
        }
    }

    public String fo() {
        try {
            return ((gb) eb()).fo();
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return null;
        }
    }

    public Player fp() {
        PlayerBuffer playerBuffer;
        bm();
        synchronized (this) {
            if (this.Gx == null) {
                try {
                    playerBuffer = new PlayerBuffer(((gb) eb()).fI());
                    if (playerBuffer.getCount() > 0) {
                        this.Gx = (PlayerEntity) playerBuffer.get(0).freeze();
                    }
                    playerBuffer.close();
                } catch (RemoteException e2) {
                    fz.g("GamesClientImpl", "service died");
                } catch (Throwable th) {
                    playerBuffer.close();
                    throw th;
                }
            }
        }
        return this.Gx;
    }

    public Game fq() {
        GameBuffer gameBuffer;
        bm();
        synchronized (this) {
            if (this.Gy == null) {
                try {
                    gameBuffer = new GameBuffer(((gb) eb()).fK());
                    if (gameBuffer.getCount() > 0) {
                        this.Gy = (GameEntity) gameBuffer.get(0).freeze();
                    }
                    gameBuffer.close();
                } catch (RemoteException e2) {
                    fz.g("GamesClientImpl", "service died");
                } catch (Throwable th) {
                    gameBuffer.close();
                    throw th;
                }
            }
        }
        return this.Gy;
    }

    public Intent fr() {
        try {
            return ((gb) eb()).fr();
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return null;
        }
    }

    public Intent fs() {
        try {
            return ((gb) eb()).fs();
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return null;
        }
    }

    public Intent ft() {
        try {
            return ((gb) eb()).ft();
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return null;
        }
    }

    public Intent fu() {
        try {
            return ((gb) eb()).fu();
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return null;
        }
    }

    public void fv() {
        try {
            ((gb) eb()).o(this.GE);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void fw() {
        try {
            ((gb) eb()).p(this.GE);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void fx() {
        try {
            ((gb) eb()).q(this.GE);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public Intent fy() {
        try {
            return ((gb) eb()).fy();
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return null;
        }
    }

    public Intent fz() {
        try {
            return ((gb) eb()).fz();
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
            return null;
        }
    }

    public void g(com.google.android.gms.common.api.a.c<LoadGamesResult> cVar) {
        try {
            ((gb) eb()).d(new j(cVar));
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void g(com.google.android.gms.common.api.a.c<CancelMatchResult> cVar, String str) {
        try {
            ((gb) eb()).n((ga) new bg(cVar), str);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void h(com.google.android.gms.common.api.a.c<LoadMatchResult> cVar, String str) {
        try {
            ((gb) eb()).p(new bm(cVar), str);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public RealTimeSocket i(String str, String str2) {
        if (str2 == null || !ParticipantUtils.aE(str2)) {
            throw new IllegalArgumentException("Bad participant ID");
        }
        RealTimeSocket realTimeSocket = (RealTimeSocket) this.Gw.get(str2);
        return (realTimeSocket == null || realTimeSocket.isClosed()) ? aw(str2) : realTimeSocket;
    }

    public void l(String str, int i2) {
        try {
            ((gb) eb()).l(str, i2);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void m(String str, int i2) {
        try {
            ((gb) eb()).m(str, i2);
        } catch (RemoteException e2) {
            fz.g("GamesClientImpl", "service died");
        }
    }

    public void onConnected(Bundle connectionHint) {
        if (this.GA) {
            this.Gz.fN();
            this.GA = false;
        }
    }

    public void onConnectionFailed(ConnectionResult result) {
        this.GA = false;
    }

    public void onConnectionSuspended(int cause) {
    }
}
