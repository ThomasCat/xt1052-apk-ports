package com.motorola.zap;

public class ZapConstants {
    public static final String ACTION_AUTO_POLL = "com.motorola.zap.ACTION_AUTO_POLL";
    public static final String ACTION_BLAST_SETTING_LAUNCH = "com.motorola.motoblast.intent.action.LAUNCH_BLAST_SETTINGS";
    public static final String ACTION_CANCEL_DOWNLOAD = "com.motorola.gs.ACTION_CANCEL_DOWNLOAD";
    public static final String ACTION_CANCEL_UPLOAD = "com.motorola.gs.ACTION_CANCEL_UPLOAD";
    public static final String ACTION_FINISH_DIALOG_ACTIVITY = "com.motorola.gs.ACTION_FINISH_DIALOG_ACTIVITY";
    public static final String ACTION_FINISH_USER_ACTION_DIALOG_ACTIVITY = "com.motorola.gs.ACTION_FINISH_USER_ACTION_DIALOG_ACTIVITY";
    public static final String ACTION_GS_GALLERY_PHOTO_SHARED = "com.motorola.gs.ACTION_GS_GALLERY_PHOTO_SHARED";
    public static final String ACTION_GS_GALLERY_SHARE_PHOTO = "com.motorola.gs.ACTION_GS_GALLERY_SHARE_PHOTO";
    public static final String ACTION_GS_MODALITY_CHANGE = "com.motorola.internal.intent.action.GS_MODALITY_CHANGE";
    public static final String ACTION_GS_REGISTER_LOC_MODALITY = "com.motorola.internal.intent.action.GS_REGISTER_LOC_MODALITY";
    public static final String ACTION_GS_REGISTER_SH_MODALITY = "com.motorola.internal.intent.action.GS_REGISTER_SH_MODALITY";
    public static final String ACTION_GS_SWIPE_DOWN = "com.motorola.internal.intent.action.GS_SWIPE_DOWN";
    public static final String ACTION_GS_SWIPE_UP = "com.motorola.internal.intent.action.GS_SWIPE_UP";
    public static final String ACTION_GS_UNREGISTER_LOC_MODALITY = "com.motorola.internal.intent.action.GS_UNREGISTER_LOC_MODALITY";
    public static final String ACTION_GS_UNREGISTER_SH_MODALITY = "com.motorola.internal.intent.action.GS_UNREGISTER_SH_MODALITY";
    public static final String ACTION_LAUNCH_MOTOCARE = "com.motorola.gs.ACTION_LAUNCH_MOTOCARE";
    public static final String ACTION_LEAVE_ZZ = "com.motorola.zap.ACTION_LEAVE_ZZ";
    public static final String ACTION_MOTO_PRIVACY = "com.motorola.intent.action.MOTOROLA_FULL_PRIVACY_POLICY";
    public static final String ACTION_PIN_INPUT = "com.motorola.gs.ACTION_PIN_INPUT";
    public static final String ACTION_REPORT_ABUSE = "com.motorola.gs.ACTION_REPORT_ABUSE";
    public static final String ACTION_SWYPE_RECEIVE_FAILED = "com.motorola.gs.ACTION_SWYPE_RECEIVE_FAILED";
    public static final String ACTION_ZAP_UPGRADE_FEATURE_NOTIFICATION = "com.motorola.zap.ACTION_UPGRADE_NOTIFICATION";
    public static final String ACTION_ZZ_CANCEL_INVITE = "com.motorola.zap.ACTION_ZZ_CANCEL_INVITE";
    public static final String ACTION_ZZ_DO_POLL = "com.motorola.zap.ACTION_ZZ_DO_POLL";
    public static final String ACTION_ZZ_IGNORE_INVITE = "com.motorola.zap.ACTION_ZZ_IGNORE_INVITE";
    public static final String ACTION_ZZ_NOTIFICATION_UPDATE = "com.motorola.zap.ACTION_ZZ_NOTIFICATION_UPDATE";
    public static final String ACTION_ZZ_POLL_HINT = "com.motorola.zap.ACTION_ZZ_POLL_HINT";
    public static final String ALL_MOTOROLA_PACKAGES = "*.motorola.*";
    public static final Boolean APPLY_WHITELIST_FOR_INITIAL_SETUP = Boolean.valueOf(true);
    public static final Boolean APPLY_WHITELIST_FOR_ZAP_GESTURE = Boolean.valueOf(true);
    public static final String CONTENT_TITLE = "content_item";
    public static final String DOCUMENTS_UI_PACKAGE = "com.android.documentsui";
    public static final String EXTRA_ABUSE_TYPE = "com.motorola.gs.EXTRA_ABUSE_TYPE";
    public static final String EXTRA_AUTO_SHARE = "com.motorola.gs.EXTRA_AUTO_SHARE";
    public static final String EXTRA_CANCEL_USER_VISIBLE = "com.motorola.gs.EXTRA_CANCEL_USER_VISIBLE";
    public static final String EXTRA_DIALOG_MESSAGE = "com.motorola.gs.EXTRA_DIALOG_MESSAGE";
    public static final String EXTRA_DIALOG_TITLE = "com.motorola.gs.EXTRA_DIALOG_TITLE";
    public static final String EXTRA_DIALOG_USER_ACTION_TYPE = "com.motorola.gs.EXTRA_DIALOG_USER_ACTION_TYPE";
    public static final String EXTRA_ESTIMATE_TRANSF_DURATION = "com.motorola.gs.EXTRA_ESTIMATE_TRANSF_DURATION";
    public static final String EXTRA_EVENT_ORIGIN_ZAP_APP = "com.motorola.gs.EVENT_ORIGIN_ZAP_APP";
    public static final String EXTRA_FROM_ID = "com.motorola.gs.EXTRA_FROM_ID";
    public static final String EXTRA_FROM_SHARE_INTENT = "com.motorola.zap.EXTRA_FROM_SHARE_INTENT";
    public static final String EXTRA_GS_GALLERY_PHOTO_SHARED = "com.motorola.gs.EXTRA_GS_GALLERY_PHOTO_SHARED";
    public static final String EXTRA_GS_MODALITY_LOC_MIN_MOVING_SPEED = "com.motorola.internal.intent.extra.GS_LOC_MODALITY_LOC_MIN_MOVING_SPEED";
    public static final String EXTRA_GS_MODALITY_LOC_MIN_VEHICLE_SPEED = "com.motorola.internal.intent.extra.GS_MODALITY_LOC_MIN_VEHICLE_SPEED";
    public static final String EXTRA_GS_MODALITY_LOC_REG_EXPIRE = "com.motorola.internal.intent.extra.GS_MODALITY_LOC_REG_EXPIRE";
    public static final String EXTRA_GS_MODALITY_LOC_UPDATES_INTERVAL = "com.motorola.internal.intent.extra.GS_MODALITY_LOC_UPDATES_INTERVAL";
    public static final String EXTRA_GS_MODALITY_SH_END_DURATION = "com.motorola.internal.intent.extra.GS_MODALITY_SH_END_DURATION";
    public static final String EXTRA_GS_MODALITY_SH_REGISTER_ALL = "com.motorola.internal.intent.extra.GS_MODALITY_SH_REGISTER_ALL";
    public static final String EXTRA_GS_MODALITY_SH_RUNNING_EXPIRE = "com.motorola.internal.intent.extra.GS_MODALITY_SH_RUNNING_EXPIRE";
    public static final String EXTRA_GS_MODALITY_SH_START_DURATION = "com.motorola.internal.intent.extra.GS_MODALITY_SH_START_DURATION";
    public static final String EXTRA_GS_MODALITY_SH_VEHICLE_EXPIRE = "com.motorola.internal.intent.extra.GS_MODALITY_SH_VEHICLE_EXPIRE";
    public static final String EXTRA_GS_MODALITY_SH_WALKING_EXPIRE = "com.motorola.internal.intent.extra.GS_MODALITY_SH_WALKING_EXPIRE";
    public static final String EXTRA_GS_NEW_MODALITY = "com.motorola.internal.intent.action.GS_NEW_MODALITY";
    public static final String EXTRA_MEDIA_MIME_TYPE = "com.motorola.gs.EXTRA_MEDIA_MIME_TYPE";
    public static final String EXTRA_ORIGINAL_INTENT = "com.motorola.gs.EXTRA_ORIGINAL_INTENT";
    public static final String EXTRA_PIN = "com.motorola.gs.EXTRA_PIN";
    public static final String EXTRA_RECOVER_USER_ACTION_INTENT = "com.motorola.gs.EXTRA_RECOVER_USER_ACTION_INTENT";
    public static final String EXTRA_REPORT_FILE_PATH = "com.motorola.gs.EXTRA_REPORT_FILE_PATH";
    public static final String EXTRA_REPORT_MIMETYPE = "com.motorola.gs.EXTRA_REPORT_MIMETYPE";
    public static final String EXTRA_REPORT_TIMESTAMP = "com.motorola.gs.EXTRA_REPORT_TIMESTAMP";
    public static final String EXTRA_SCREENSHOT_REQ_ORIGIN = "com.motorola.screenshot.req.origin";
    public static final String EXTRA_SERVER_SHARE_ID = "com.motorola.gs.EXTRA_SERVER_SHARE_ID";
    public static final String EXTRA_SHOW_UPGRADE_FEATURE_TYPE = "com.motorola.zap.SHOW_UPGRADE_FEATURE_TYPE";
    public static final String EXTRA_TOPIC_ID = "topic_id";
    public static final String EXTRA_TRANSACTION_UID = "com.motorola.gs.EXTRA_TRANSACTION_UID";
    public static final String EXTRA_ZZ_INVITE_UID = "com.motorola.zap.EXTRA_ZZ_INVITE_UID";
    public static final String EXTRA_ZZ_POLL_NOW = "com.motorola.zap.EXTRA_ZZ_POLL_NOW";
    public static final String GOOGLE_ACCOUNT_NONE = "";
    public static final String GS_GALLERY_CANNOT_SHARE_DRM = "++NO_SHARE_DRM++";
    public static final String GS_GALLERY_CANNOT_SHARE_NULL = "++NO_SHARE_NULL++";
    public static final String ID_MOTO = "zap";
    public static final String KEY_AUTO_PROCESSING = "autoProcessing";
    public static final float KNOWN_LOCATION_REQUIRED_ACCURACY = 500.0f;
    public static final String LEGACY_DROIDS_DEVICES_LIST = "XT875,XT894,MASERATI_VZW,XT912,SPYDER_VZW,XT905,XT906,XT907,XT926,MZ609,MZ617,6435LVW,ADR6410";
    public static final long LIMIT_UPLOAD_TIME_WINDOW = 180000;
    public static final long LOCATION_MAX_AGE_MODALITY_NONE = 1200000;
    public static final long LOCATION_MAX_AGE_NO_MODALITY_SUPPORT = 300000;
    public static final long LOCATION_MAX_AGE_RUNNING = 30000;
    public static final long LOCATION_MAX_AGE_VEHICLE = 10000;
    public static final long LOCATION_MAX_AGE_WALKING = 60000;
    public static final long LOCATION_RETRY_MAX_TIME_SETTINGS_DISABLED = 15000;
    public static final long LOCATION_RETRY_MAX_TIME_SETTINGS_ENABLED = 30000;
    public static final long LOCATION_UPDATE_DURATION = 30000;
    public static final long LOCATION_USE_GPS_FOR_MODALITY_TIME_WINODW = 3600000;
    public static final int MAX_UPLOADS_ALLOWED_IN_WINDOW = 50;
    public static final long MAX_UPLOAD_VIDEO_SIZE = 230686720;
    public static final float MODALITY_LOC_MIN_SPEED_MOVING = 2.0f;
    public static final float MODALITY_LOC_MIN_SPEED_VEHICLE = 6.0f;
    public static final long MODALITY_LOC_REG_EXPIRE = 10800000;
    public static final long MODALITY_LOC_SHORT_UPDATES_INTERVAL = 20000;
    public static final long MODALITY_LOC_UPDATES_INTERVAL = 120000;
    public static final String PACKAGES_TO_ALLOW_GESTURE = "*.motorola.*|com.android.launcher3";
    public static final String PACKAGES_TO_IGNORE_GESTURE = "com.motorola.messaging";
    public static final int PIN_SIZE = 4;
    public static final String PREFERENCES_FILE_NAME = "blast-pref";
    public static final long RECEIVE_ACTION_THRESHOLD = 1000;
    public static final long REG_MODALITY_SH_END_DURATION = 60000;
    public static final long REG_MODALITY_SH_RUNNING_EXPIRE = 7200000;
    public static final long REG_MODALITY_SH_START_DURATION = 60000;
    public static final long REG_MODALITY_SH_VEHICLE_EXPIRE = 10800000;
    public static final long REG_MODALITY_SH_WALKING_EXPIRE = 7200000;
    public static final long REUSE_PIN_TIME = 10000;
    public static final String SCREENSHOT_KEYPRESS_ORIGIN = "com.motorola.screenshot.keypress.req";
    public static final String SCREENSHOT_VOICE_ORIGIN = "com.motorola.screenshot.voice.req";
    public static final long SEND_ACTION_THRESHOLD = 2000;
    public static final int SHARE_DISTANCE = 300;
    public static final long SHARE_TIMEOUT = 120000;
    public static final String SUPPORTS_SENDING_ALL_DEVICES = "ALL_MODELS";
    public static final int UNIVERSAL_NOTIFICATION_ID = 0;
    public static final int UPGRADE_NOTIF_TYPE_PROFILE = 0;
    public static final int UPGRADE_NOTIF_TYPE_VOICE = 1;
    public static final UseGpsOnModalityChange USE_GPS_ON_MODALITY_CHANGE = UseGpsOnModalityChange.ONLY_IF_NO_WIFI;
    public static final boolean USE_GPS_ON_SCREEN_ON = false;
    public static final int VEHICLE_SHARE_DISTANCE = 3200;
    public static final String VZW_VVM_PACKAGE = "com.motorola.vmm";
    public static final String ZAP_DIRECTORY = "Droid Zap";
    public static final int ZAP_SHOW_NOTIF_THRESHOLD = 3;
    public static final long ZZ_SHARED_INVITE_TIMEOUT = 3600000;

    public enum BlastSettingsLaunchPoint {
        CIRCLE,
        SETTINGS
    }

    public enum UseGpsOnModalityChange {
        YES,
        NO,
        ONLY_IF_NO_WIFI
    }
}
