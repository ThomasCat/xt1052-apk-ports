package com.google.sample.castcompanionlibrary.cast.exceptions;

public interface OnFailedListener {
    void onFailed(int i, int i2);
}
