package com.motorola.zap.app;

import com.motorola.sharing.GroupManager.JoinParams;
import java.io.Serializable;

public class GroupInvite implements Serializable {
    public final String id;
    public final int inviteUid;
    public final boolean locked;
    public final String name;
    public final String owner;
    public final String thumbnail;

    public GroupInvite(String owner2, String id2, String name2, String thumbnail2, boolean locked2, int inviteUid2) {
        this.owner = owner2;
        this.id = id2;
        this.name = name2;
        this.thumbnail = thumbnail2;
        this.locked = locked2;
        this.inviteUid = inviteUid2;
    }

    public JoinParams getJoinParams(String pin, boolean rejoin) {
        JoinParams p = new JoinParams();
        p.owner = this.owner;
        p.id = this.id;
        p.name = this.name;
        p.thumbnail = this.thumbnail;
        p.pin = pin;
        p.rejoin = rejoin;
        return p;
    }
}
