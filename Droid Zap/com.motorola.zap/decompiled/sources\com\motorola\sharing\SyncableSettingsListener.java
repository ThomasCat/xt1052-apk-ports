package com.motorola.sharing;

import java.util.Map;

public interface SyncableSettingsListener {
    void onSyncableSettings(Map<String, String> map);
}
