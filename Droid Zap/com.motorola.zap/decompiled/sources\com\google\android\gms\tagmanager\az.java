package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.b;
import com.google.android.gms.plus.PlusShare;
import com.motorola.zap.ZapConstants;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

class az extends aj {
    private static final String ID = com.google.android.gms.internal.a.JOINER.toString();
    private static final String US = b.ARG0.toString();
    private static final String Vo = b.ITEM_SEPARATOR.toString();
    private static final String Vp = b.KEY_VALUE_SEPARATOR.toString();
    private static final String Vq = b.ESCAPE.toString();

    private enum a {
        NONE,
        URL,
        BACKSLASH
    }

    public az() {
        super(ID, US);
    }

    private String a(String str, a aVar, Set<Character> set) {
        switch (aVar) {
            case URL:
                try {
                    return dl.bO(str);
                } catch (UnsupportedEncodingException e) {
                    bh.c("Joiner: unsupported encoding", e);
                    return str;
                }
            case BACKSLASH:
                String replace = str.replace("\\", "\\\\");
                Iterator it = set.iterator();
                while (true) {
                    String str2 = replace;
                    if (!it.hasNext()) {
                        return str2;
                    }
                    String ch = ((Character) it.next()).toString();
                    replace = str2.replace(ch, "\\" + ch);
                }
            default:
                return str;
        }
    }

    private void a(StringBuilder sb, String str, a aVar, Set<Character> set) {
        sb.append(a(str, aVar, set));
    }

    private void a(Set<Character> set, String str) {
        for (int i = 0; i < str.length(); i++) {
            set.add(Character.valueOf(str.charAt(i)));
        }
    }

    public boolean iy() {
        return true;
    }

    public com.google.android.gms.internal.d.a u(Map<String, com.google.android.gms.internal.d.a> map) {
        HashSet hashSet;
        a aVar;
        com.google.android.gms.internal.d.a aVar2 = (com.google.android.gms.internal.d.a) map.get(US);
        if (aVar2 == null) {
            return di.ku();
        }
        com.google.android.gms.internal.d.a aVar3 = (com.google.android.gms.internal.d.a) map.get(Vo);
        String str = aVar3 != null ? di.j(aVar3) : ZapConstants.GOOGLE_ACCOUNT_NONE;
        com.google.android.gms.internal.d.a aVar4 = (com.google.android.gms.internal.d.a) map.get(Vp);
        String str2 = aVar4 != null ? di.j(aVar4) : "=";
        a aVar5 = a.NONE;
        com.google.android.gms.internal.d.a aVar6 = (com.google.android.gms.internal.d.a) map.get(Vq);
        if (aVar6 != null) {
            String j = di.j(aVar6);
            if (PlusShare.KEY_CALL_TO_ACTION_URL.equals(j)) {
                aVar = a.URL;
                hashSet = null;
            } else if ("backslash".equals(j)) {
                aVar = a.BACKSLASH;
                hashSet = new HashSet();
                a(hashSet, str);
                a(hashSet, str2);
                hashSet.remove(Character.valueOf('\\'));
            } else {
                bh.t("Joiner: unsupported escape type: " + j);
                return di.ku();
            }
        } else {
            hashSet = null;
            aVar = aVar5;
        }
        StringBuilder sb = new StringBuilder();
        switch (aVar2.type) {
            case 2:
                boolean z = true;
                com.google.android.gms.internal.d.a[] aVarArr = aVar2.fZ;
                int length = aVarArr.length;
                int i = 0;
                while (i < length) {
                    com.google.android.gms.internal.d.a aVar7 = aVarArr[i];
                    if (!z) {
                        sb.append(str);
                    }
                    a(sb, di.j(aVar7), aVar, hashSet);
                    i++;
                    z = false;
                }
                break;
            case 3:
                for (int i2 = 0; i2 < aVar2.ga.length; i2++) {
                    if (i2 > 0) {
                        sb.append(str);
                    }
                    String j2 = di.j(aVar2.ga[i2]);
                    String j3 = di.j(aVar2.gb[i2]);
                    a(sb, j2, aVar, hashSet);
                    sb.append(str2);
                    a(sb, j3, aVar, hashSet);
                }
                break;
            default:
                a(sb, di.j(aVar2), aVar, hashSet);
                break;
        }
        return di.r(sb.toString());
    }
}
