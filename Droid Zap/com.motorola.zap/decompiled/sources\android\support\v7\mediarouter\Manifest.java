package android.support.v7.mediarouter;

public final class Manifest {

    public static final class permission {
        public static final String SCREENSHOT_TAKEN = "com.motorola.screenshot.permission.SCREENSHOT_TAKEN";
        public static final String TRIGGER_BLAST_ACTION = "com.motorola.blast.permission.TRIGGER_BLAST_ACTION";
        public static final String TRIGGER_BLAST_TUTORIAL = "com.motorola.blast.permission.TRIGGER_BLAST_TUTORIAL";
    }
}
