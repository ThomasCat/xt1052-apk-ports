package com.motorola.zap.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import com.motorola.sharing.util.NetworkUtils;
import com.motorola.zap.R;

public class PrivacyPolicyActivity extends Activity {
    /* access modifiers changed from: private */
    public static boolean pageLoaded = false;
    /* access modifiers changed from: private */
    public AlertDialog alertDialog;
    private boolean isShowingErrorMsg = false;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (PrivacyPolicyActivity.this.mUrl != null) {
                PrivacyPolicyActivity.this.webview.loadUrl(PrivacyPolicyActivity.this.mUrl);
            }
        }
    };
    private TextView mProgressDialogTitleText = null;
    /* access modifiers changed from: private */
    public String mUrl = "https://help.motorola.com/hc/apps/settings/index.php?type=privacy&topic=zap";
    /* access modifiers changed from: private */
    public ProgressDialog progressBar;
    WebView webview;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!NetworkUtils.hasDataConnection(this)) {
            this.isShowingErrorMsg = true;
            Dialog noDataConnectionDialog = new Builder(this).setTitle(getPrivacyTitle()).setMessage(getErrorMessage()).setPositiveButton(getString(R.string.dialog_ok_button), new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    PrivacyPolicyActivity.this.finishActivitiyWithoutAnimation();
                }
            }).setOnCancelListener(new OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    PrivacyPolicyActivity.this.finishActivitiyWithoutAnimation();
                }
            }).show();
            noDataConnectionDialog.setCanceledOnTouchOutside(false);
            noDataConnectionDialog.show();
            return;
        }
        overridePendingTransition(0, 0);
        setTheme(16974124);
        setContentView(R.layout.privacy_policy_webview);
        this.mProgressDialogTitleText = (TextView) findViewById(R.id.full_privacy_policy_title);
        this.webview = (WebView) findViewById(R.id.webview);
        this.webview.getSettings().setJavaScriptEnabled(true);
        this.webview.getSettings().setSupportZoom(true);
        this.webview.getSettings().setBuiltInZoomControls(true);
        this.webview.getSettings().setEnableSmoothTransition(true);
        this.webview.getSettings().setLoadWithOverviewMode(true);
        this.webview.getSettings().setAppCacheEnabled(true);
        this.alertDialog = new Builder(this).create();
        pageLoaded = false;
        showProgressBar(pageLoaded);
        this.webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                PrivacyPolicyActivity.pageLoaded = true;
                if (PrivacyPolicyActivity.this.progressBar != null && PrivacyPolicyActivity.this.progressBar.isShowing()) {
                    PrivacyPolicyActivity.this.progressBar.dismiss();
                    PrivacyPolicyActivity.this.progressBar = null;
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                PrivacyPolicyActivity.this.alertDialog.setTitle(PrivacyPolicyActivity.this.getString(R.string.settings_privacy_error_title));
                PrivacyPolicyActivity.this.alertDialog.setMessage(description);
                PrivacyPolicyActivity.this.alertDialog.setButton(PrivacyPolicyActivity.this.getString(R.string.dialog_ok_button), new OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                PrivacyPolicyActivity.this.alertDialog.show();
            }
        });
        this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(101), 1000);
    }

    private String getPrivacyTitle() {
        return getString(R.string.settings_privacy_more_info_link);
    }

    private String getErrorMessage() {
        return getString(R.string.settings_privacy_policy_no_data_connection);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.progressBar != null && this.progressBar.isShowing()) {
            this.progressBar.dismiss();
            this.progressBar = null;
        }
    }

    /* access modifiers changed from: private */
    public void finishActivitiyWithoutAnimation() {
        finish();
        overridePendingTransition(0, 0);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.isShowingErrorMsg) {
            showProgressBar(pageLoaded);
        }
    }

    public void onDoneButtonClick(View v) {
        finishActivitiyWithoutAnimation();
    }

    private void showProgressBar(boolean pageLoaded2) {
        this.mProgressDialogTitleText.setText(getPrivacyTitle());
        if (this.progressBar == null && !pageLoaded2) {
            this.progressBar = ProgressDialog.show(this, getPrivacyTitle(), getString(R.string.settings_privacy_please_wait));
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.alertDialog != null) {
            if (this.alertDialog.isShowing()) {
                this.alertDialog.dismiss();
            }
            this.alertDialog = null;
        }
        this.mHandler.removeMessages(101);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && this.webview != null && this.webview.canGoBack()) {
            this.webview.goBack();
            return true;
        } else if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        } else {
            finishActivitiyWithoutAnimation();
            return true;
        }
    }
}
