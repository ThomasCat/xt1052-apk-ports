package com.motorola.zap.identity;

import android.app.Fragment;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.motorola.sharing.Profile;
import com.motorola.sharing.ProfileManager;
import com.motorola.sharing.ProfileManagerListener;
import com.motorola.sharing.ProfileManagerListener.ProfileEvent;
import com.motorola.sharing.ProfileManagerListener.ProfileEvent.Status;
import com.motorola.zap.R;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.ui.DroidBlastLauncher;
import com.motorola.zap.ui.ZapUi;

public class ProfileWelcomeFragment extends Fragment {
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.cloneInContext(new ContextThemeWrapper(getActivity(), 16973934)).inflate(R.layout.profile_welcome_card, container, false);
        Button activate = (Button) rootView.findViewById(R.id.profile_activate);
        Button notNow = (Button) rootView.findViewById(R.id.profile_notnow);
        TextView privacyPolicy = (TextView) rootView.findViewById(R.id.settings_privacy_policy);
        privacyPolicy.setPaintFlags(8);
        privacyPolicy.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                ZapUtils.showGlobalPrivacyPolicy(ProfileWelcomeFragment.this.getActivity());
            }
        });
        activate.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                final Profile profile = ZapProfileSharedPerfs.getProfile();
                if (profile != null) {
                    final ZapUi ui = ZapUi.getInstance();
                    ui.showToastMessage(ProfileWelcomeFragment.this.getActivity().getString(R.string.profile_activating_toast), ZapUi.getZapToastIconId(), true, true);
                    profile.anonymous = false;
                    ProfileManager.getInstance().updateMyProfile(profile, new ProfileManagerListener() {
                        public void onProfileEvent(ProfileEvent event) {
                            if (event.status == Status.SUCCESS) {
                                ui.showToastMessage(ProfileWelcomeFragment.this.getActivity().getString(R.string.profile_activated_toast), ZapUi.getZapToastIconId(), true, true);
                                ZapProfileSharedPerfs.ZAP_PROFILE_ACTIVATE.write(Boolean.valueOf(true));
                                ((DroidBlastLauncher) ProfileWelcomeFragment.this.getActivity()).selectItem(0);
                                ZapProfileSharedPerfs.backupProfile(profile);
                            }
                        }
                    });
                }
            }
        });
        notNow.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                ZapProfileSharedPerfs.ZAP_PROFILE_ACTIVATE.write(Boolean.valueOf(false));
                ProfileWelcomeFragment.this.getActivity().onBackPressed();
            }
        });
        return rootView;
    }
}
