package com.google.android.gms.common.data;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.google.android.gms.internal.ep;
import com.google.android.gms.internal.er;

public abstract class b {
    protected final DataHolder zU;
    protected final int zW;
    private final int zX;

    public b(DataHolder dataHolder, int i) {
        this.zU = (DataHolder) er.f(dataHolder);
        er.v(i >= 0 && i < dataHolder.getCount());
        this.zW = i;
        this.zX = dataHolder.I(this.zW);
    }

    /* access modifiers changed from: protected */
    public void a(String str, CharArrayBuffer charArrayBuffer) {
        this.zU.copyToBuffer(str, this.zW, this.zX, charArrayBuffer);
    }

    /* access modifiers changed from: protected */
    public Uri aa(String str) {
        return this.zU.parseUri(str, this.zW, this.zX);
    }

    /* access modifiers changed from: protected */
    public boolean ab(String str) {
        return this.zU.hasNull(str, this.zW, this.zX);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        return ep.equal(Integer.valueOf(bVar.zW), Integer.valueOf(this.zW)) && ep.equal(Integer.valueOf(bVar.zX), Integer.valueOf(this.zX)) && bVar.zU == this.zU;
    }

    /* access modifiers changed from: protected */
    public boolean getBoolean(String column) {
        return this.zU.getBoolean(column, this.zW, this.zX);
    }

    /* access modifiers changed from: protected */
    public byte[] getByteArray(String column) {
        return this.zU.getByteArray(column, this.zW, this.zX);
    }

    /* access modifiers changed from: protected */
    public int getInteger(String column) {
        return this.zU.getInteger(column, this.zW, this.zX);
    }

    /* access modifiers changed from: protected */
    public long getLong(String column) {
        return this.zU.getLong(column, this.zW, this.zX);
    }

    /* access modifiers changed from: protected */
    public String getString(String column) {
        return this.zU.getString(column, this.zW, this.zX);
    }

    public boolean hasColumn(String column) {
        return this.zU.hasColumn(column);
    }

    public int hashCode() {
        return ep.hashCode(Integer.valueOf(this.zW), Integer.valueOf(this.zX), this.zU);
    }

    public boolean isDataValid() {
        return !this.zU.isClosed();
    }
}
