package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.appstate.AppStateBuffer;
import com.google.android.gms.appstate.AppStateManager.StateConflictResult;
import com.google.android.gms.appstate.AppStateManager.StateDeletedResult;
import com.google.android.gms.appstate.AppStateManager.StateListResult;
import com.google.android.gms.appstate.AppStateManager.StateLoadedResult;
import com.google.android.gms.appstate.AppStateManager.StateResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;

public final class dl extends eh<dn> {
    private final String vi;

    final class a extends dk {
        private final com.google.android.gms.common.api.a.c<StateDeletedResult> vj;

        public a(com.google.android.gms.common.api.a.c<StateDeletedResult> cVar) {
            this.vj = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Result holder must not be null");
        }

        public void b(int i, int i2) {
            dl.this.a((b<?>) new b<Object>(this.vj, new Status(i), i2));
        }
    }

    final class b extends b<com.google.android.gms.common.api.a.c<StateDeletedResult>> implements StateDeletedResult {
        private final Status vl;
        private final int vm;

        public b(com.google.android.gms.common.api.a.c<StateDeletedResult> cVar, Status status, int i) {
            super(cVar);
            this.vl = status;
            this.vm = i;
        }

        /* renamed from: c */
        public void a(com.google.android.gms.common.api.a.c<StateDeletedResult> cVar) {
            cVar.b(this);
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }

        public int getStateKey() {
            return this.vm;
        }

        public Status getStatus() {
            return this.vl;
        }
    }

    final class c extends dk {
        private final com.google.android.gms.common.api.a.c<StateListResult> vj;

        public c(com.google.android.gms.common.api.a.c<StateListResult> cVar) {
            this.vj = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Result holder must not be null");
        }

        public void a(DataHolder dataHolder) {
            dl.this.a((b<?>) new d<Object>(this.vj, new Status(dataHolder.getStatusCode()), dataHolder));
        }
    }

    final class d extends com.google.android.gms.internal.eh.d<com.google.android.gms.common.api.a.c<StateListResult>> implements StateListResult {
        private final Status vl;
        private final AppStateBuffer vn;

        public d(com.google.android.gms.common.api.a.c<StateListResult> cVar, Status status, DataHolder dataHolder) {
            super(cVar, dataHolder);
            this.vl = status;
            this.vn = new AppStateBuffer(dataHolder);
        }

        public void a(com.google.android.gms.common.api.a.c<StateListResult> cVar, DataHolder dataHolder) {
            cVar.b(this);
        }

        public AppStateBuffer getStateBuffer() {
            return this.vn;
        }

        public Status getStatus() {
            return this.vl;
        }
    }

    final class e extends dk {
        private final com.google.android.gms.common.api.a.c<StateResult> vj;

        public e(com.google.android.gms.common.api.a.c<StateResult> cVar) {
            this.vj = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Result holder must not be null");
        }

        public void a(int i, DataHolder dataHolder) {
            dl.this.a((b<?>) new f<Object>(this.vj, i, dataHolder));
        }
    }

    final class f extends com.google.android.gms.internal.eh.d<com.google.android.gms.common.api.a.c<StateResult>> implements StateConflictResult, StateLoadedResult, StateResult {
        private final Status vl;
        private final int vm;
        private final AppStateBuffer vn;

        public f(com.google.android.gms.common.api.a.c<StateResult> cVar, int i, DataHolder dataHolder) {
            super(cVar, dataHolder);
            this.vm = i;
            this.vl = new Status(dataHolder.getStatusCode());
            this.vn = new AppStateBuffer(dataHolder);
        }

        private boolean cQ() {
            return this.vl.getStatusCode() == 2000;
        }

        public void a(com.google.android.gms.common.api.a.c<StateResult> cVar, DataHolder dataHolder) {
            cVar.b(this);
        }

        public StateConflictResult getConflictResult() {
            if (cQ()) {
                return this;
            }
            return null;
        }

        public StateLoadedResult getLoadedResult() {
            if (cQ()) {
                return null;
            }
            return this;
        }

        public byte[] getLocalData() {
            if (this.vn.getCount() == 0) {
                return null;
            }
            return this.vn.get(0).getLocalData();
        }

        public String getResolvedVersion() {
            if (this.vn.getCount() == 0) {
                return null;
            }
            return this.vn.get(0).getConflictVersion();
        }

        public byte[] getServerData() {
            if (this.vn.getCount() == 0) {
                return null;
            }
            return this.vn.get(0).getConflictData();
        }

        public int getStateKey() {
            return this.vm;
        }

        public Status getStatus() {
            return this.vl;
        }

        public void release() {
            this.vn.close();
        }
    }

    final class g extends dk {
        com.google.android.gms.common.api.a.c<Status> vj;

        public g(com.google.android.gms.common.api.a.c<Status> cVar) {
            this.vj = (com.google.android.gms.common.api.a.c) er.b(cVar, (Object) "Holder must not be null");
        }

        public void cM() {
            dl.this.a((b<?>) new h<Object>(this.vj, new Status(0)));
        }
    }

    final class h extends b<com.google.android.gms.common.api.a.c<Status>> {
        private final Status vl;

        public h(com.google.android.gms.common.api.a.c<Status> cVar, Status status) {
            super(cVar);
            this.vl = status;
        }

        /* renamed from: c */
        public void a(com.google.android.gms.common.api.a.c<Status> cVar) {
            cVar.b(this.vl);
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }
    }

    public dl(Context context, Looper looper, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, String str, String[] strArr) {
        super(context, looper, connectionCallbacks, onConnectionFailedListener, strArr);
        this.vi = (String) er.f(str);
    }

    public void a(com.google.android.gms.common.api.a.c<StateListResult> cVar) {
        try {
            ((dn) eb()).a(new c(cVar));
        } catch (RemoteException e2) {
            Log.w("AppStateClient", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<StateDeletedResult> cVar, int i) {
        try {
            ((dn) eb()).b(new a(cVar), i);
        } catch (RemoteException e2) {
            Log.w("AppStateClient", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<StateResult> cVar, int i, String str, byte[] bArr) {
        try {
            ((dn) eb()).a(new e(cVar), i, str, bArr);
        } catch (RemoteException e2) {
            Log.w("AppStateClient", "service died");
        }
    }

    public void a(com.google.android.gms.common.api.a.c<StateResult> cVar, int i, byte[] bArr) {
        try {
            ((dn) eb()).a(cVar == null ? null : new e(cVar), i, bArr);
        } catch (RemoteException e2) {
            Log.w("AppStateClient", "service died");
        }
    }

    /* access modifiers changed from: protected */
    public void a(en enVar, com.google.android.gms.internal.eh.e eVar) throws RemoteException {
        enVar.a((em) eVar, (int) GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, getContext().getPackageName(), this.vi, ea());
    }

    /* access modifiers changed from: protected */
    public String aF() {
        return "com.google.android.gms.appstate.service.START";
    }

    /* access modifiers changed from: protected */
    public String aG() {
        return "com.google.android.gms.appstate.internal.IAppStateService";
    }

    public void b(com.google.android.gms.common.api.a.c<Status> cVar) {
        try {
            ((dn) eb()).b(new g(cVar));
        } catch (RemoteException e2) {
            Log.w("AppStateClient", "service died");
        }
    }

    public void b(com.google.android.gms.common.api.a.c<StateResult> cVar, int i) {
        try {
            ((dn) eb()).a(new e(cVar), i);
        } catch (RemoteException e2) {
            Log.w("AppStateClient", "service died");
        }
    }

    /* access modifiers changed from: protected */
    public void b(String... strArr) {
        boolean z = false;
        for (String equals : strArr) {
            if (equals.equals(Scopes.APP_STATE)) {
                z = true;
            }
        }
        er.a(z, String.format("App State APIs requires %s to function.", new Object[]{Scopes.APP_STATE}));
    }

    public int cN() {
        try {
            return ((dn) eb()).cN();
        } catch (RemoteException e2) {
            Log.w("AppStateClient", "service died");
            return 2;
        }
    }

    public int cO() {
        try {
            return ((dn) eb()).cO();
        } catch (RemoteException e2) {
            Log.w("AppStateClient", "service died");
            return 2;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: s */
    public dn p(IBinder iBinder) {
        return com.google.android.gms.internal.dn.a.u(iBinder);
    }
}
