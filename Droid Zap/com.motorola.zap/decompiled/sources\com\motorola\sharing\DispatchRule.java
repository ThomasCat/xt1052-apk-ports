package com.motorola.sharing;

import java.util.HashMap;

public interface DispatchRule extends ContentApi {
    public static final String KEY_CLASS_NAME = "className";

    public static class Params extends HashMap<String, Object> {
    }

    Params getParams();

    Endpoint[] getStaticEndpoints();
}
