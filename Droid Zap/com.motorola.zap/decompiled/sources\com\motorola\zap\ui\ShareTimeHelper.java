package com.motorola.zap.ui;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.motorola.zap.R;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ShareTimeHelper {
    public int CHECK_STATUS = 1;
    private final long DELAY = 1000;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler(Looper.getMainLooper()) {
        public synchronized void handleMessage(Message msg) {
            if (msg.what == ShareTimeHelper.this.CHECK_STATUS) {
                Iterator<TextView> it = ShareTimeHelper.this.mSentTextViewList.iterator();
                while (it.hasNext()) {
                    TextView tv = (TextView) it.next();
                    Context context = tv.getContext();
                    long timeDiff = ((Long) tv.getTag(R.id.KEY_EXPIRY)).longValue() - System.currentTimeMillis();
                    ViewGroup layout = (ViewGroup) tv.getTag(R.id.KEY_LAYOUT);
                    ImageView contextMenu = (ImageView) layout.findViewById(R.id.context_menu);
                    if (timeDiff > 0) {
                        contextMenu.setVisibility(0);
                        int secLeft = (int) (timeDiff / 1000);
                        int seconds = secLeft % 60;
                        tv.setText(context.getString(R.string.sharing_expires_in) + (secLeft / 60) + ":" + (seconds <= 9 ? "0" + String.valueOf(seconds) : String.valueOf(seconds)) + context.getString(R.string.second));
                    } else {
                        TextView dateView = (TextView) layout.findViewById(R.id.activity_googlecards_card_date);
                        dateView.setText((String) tv.getTag(R.id.KEY_DATE));
                        dateView.setCompoundDrawables(null, null, null, null);
                        ((TextView) layout.findViewById(R.id.activity_googlecards_card_textview)).setText((String) tv.getTag(R.id.KEY_PLACE));
                        tv.setVisibility(8);
                        dateView.setVisibility(0);
                        contextMenu.setImageResource(R.drawable.ic_zap_overflow);
                        it.remove();
                    }
                }
                if (!ShareTimeHelper.this.mSentTextViewList.isEmpty()) {
                    ShareTimeHelper.this.mHandler.sendEmptyMessageDelayed(ShareTimeHelper.this.CHECK_STATUS, 1000);
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public final Set<TextView> mSentTextViewList = new HashSet();

    public synchronized void put(TextView tv) {
        this.mSentTextViewList.add(tv);
        this.mHandler.removeMessages(this.CHECK_STATUS);
        this.mHandler.sendEmptyMessage(this.CHECK_STATUS);
    }

    public synchronized void remove(TextView tv) {
        this.mSentTextViewList.remove(tv);
    }

    public synchronized void clear() {
        this.mSentTextViewList.clear();
    }
}
