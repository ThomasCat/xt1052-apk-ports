package com.google.android.gms.internal;

import android.content.Context;
import java.util.Iterator;

public final class aw {
    private final bf kH;
    private final Context mContext;
    private final cd mf;
    private final Object mg = new Object();
    private final ay mh;
    private boolean mi = false;
    private bb mj;

    public aw(Context context, cd cdVar, bf bfVar, ay ayVar) {
        this.mContext = context;
        this.mf = cdVar;
        this.kH = bfVar;
        this.mh = ayVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0080, code lost:
        r4 = r16.mj.b(r17, r19);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x008e, code lost:
        if (r4.mL != 0) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0090, code lost:
        com.google.android.gms.internal.da.s("Adapter succeeded.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x009b, code lost:
        if (r4.mN == null) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x009d, code lost:
        com.google.android.gms.internal.cz.pT.post(new com.google.android.gms.internal.aw.AnonymousClass1(r16));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        return r4;
     */
    public bc a(long j, long j2) {
        da.s("Starting mediation.");
        for (ax axVar : this.mh.mr) {
            da.u("Trying mediation network: " + axVar.mm);
            Iterator it = axVar.mn.iterator();
            while (true) {
                if (it.hasNext()) {
                    String str = (String) it.next();
                    synchronized (this.mg) {
                        if (this.mi) {
                            bc bcVar = new bc(-1);
                            return bcVar;
                        }
                        this.mj = new bb(this.mContext, str, this.kH, this.mh, axVar, this.mf.oc, this.mf.kQ, this.mf.kN);
                    }
                }
            }
            while (true) {
            }
        }
        return new bc(1);
    }

    public void cancel() {
        synchronized (this.mg) {
            this.mi = true;
            if (this.mj != null) {
                this.mj.cancel();
            }
        }
    }
}
