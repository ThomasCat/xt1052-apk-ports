package com.motorola.zap;

import java.security.SecureRandom;

public class PinGenerator {
    private String mPin;
    private long mPinTime;
    private final SecureRandom rand = new SecureRandom();

    public synchronized String getPin() {
        String str;
        long now = System.currentTimeMillis();
        if (now - this.mPinTime < ((Long) Z.REUSE_PIN_TIME.read()).longValue()) {
            this.mPinTime = now;
            str = this.mPin;
        } else {
            StringBuilder b = new StringBuilder();
            for (int i = 0; i < ((Integer) Z.PIN_SIZE.read()).intValue(); i++) {
                b.append(this.rand.nextInt(10));
            }
            this.mPin = b.toString();
            this.mPinTime = now;
            str = this.mPin;
        }
        return str;
    }
}
