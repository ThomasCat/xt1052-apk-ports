package com.motorola.sharing.util;

import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener;
import com.motorola.sharing.GroupListener.GroupEvent;
import com.motorola.sharing.GroupManager.GroupInfo;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

public class ContainerExpiryTracker implements GroupListener {
    private static final String TAG = Logger.getGcspTag("ExpiryTracker");
    private String mActiveGroupId;
    private ExpiryHandler mExpiryHandler = new ExpiryHandler(this);
    private ExpiryListener mExpiryListener = null;
    private Set<ContentContainer> mGroupTrackedContainers = new HashSet();
    private HistoryManager mHistoryManager = HistoryManager.getInstance();
    private List<ContentContainer> mTimeTrackedContainers = new LinkedList();

    private static class ExpiryHandler extends Handler {
        private WeakReference<ContainerExpiryTracker> mTracker = null;

        public ExpiryHandler(ContainerExpiryTracker tracker) {
            this.mTracker = new WeakReference<>(tracker);
        }

        public void handleMessage(Message msg) {
            if (msg.what == 4000) {
                ContainerExpiryTracker tracker = (ContainerExpiryTracker) this.mTracker.get();
                if (tracker != null) {
                    tracker.onTimerExpired();
                }
            }
        }
    }

    public interface ExpiryListener {
        void onContainerExpired(ContentContainer contentContainer, ExpiryTrigger expiryTrigger);
    }

    public enum ExpiryTrigger {
        BY_TIME,
        BY_GROUP_TERMINATION
    }

    public static class ExpiryTriggers extends HashSet<ExpiryTrigger> {
        public ExpiryTriggers(ExpiryTrigger... triggers) {
            for (ExpiryTrigger trigger : triggers) {
                add(trigger);
            }
        }
    }

    public ContainerExpiryTracker(ExpiryListener listener) {
        String str = null;
        this.mExpiryListener = listener;
        GroupInfo groupInfo = GCSP.getGroupManager().getGroupInfo(null);
        if (groupInfo.isActive) {
            str = groupInfo.id;
        }
        this.mActiveGroupId = str;
        GCSP.getGroupManager().registerGroupListener(this);
    }

    public boolean isExpired(ContentContainer container) {
        if (container.expiryTriggers == null || container.expiryTriggers.isEmpty()) {
            Logger.w(TAG, "expiry trigger not set");
            return true;
        } else if (container.expiryTriggers.contains(ExpiryTrigger.BY_TIME) && container.getExpiryElapsedTime() <= SystemClock.elapsedRealtime()) {
            return true;
        } else {
            if (!container.expiryTriggers.contains(ExpiryTrigger.BY_GROUP_TERMINATION) || matchActiveGroup(container)) {
                return false;
            }
            return true;
        }
    }

    public synchronized void add(ContentContainer container) {
        if (container.expiryTriggers == null || container.expiryTriggers.isEmpty()) {
            throw new IllegalArgumentException("expiry trigger not set");
        } else if (TextUtils.isEmpty(container.shareId)) {
            throw new IllegalArgumentException("share id is null");
        } else {
            stop();
            boolean added = false;
            try {
                if (container.expiryTriggers.contains(ExpiryTrigger.BY_TIME)) {
                    long remainingTime = container.getExpiryElapsedTime() - SystemClock.elapsedRealtime();
                    if (!this.mTimeTrackedContainers.contains(container)) {
                        if (remainingTime > 0) {
                            Logger.d(TAG, "added tracking by time (" + (remainingTime / 1000) + " sec left): " + container.getPrintableSignature());
                            ListIterator<ContentContainer> iter = this.mTimeTrackedContainers.listIterator();
                            while (true) {
                                if (iter.hasNext()) {
                                    if (((ContentContainer) iter.next()).getExpiryElapsedTime() > container.getExpiryElapsedTime()) {
                                        iter.previous();
                                        iter.add(container);
                                        break;
                                    }
                                } else {
                                    iter.add(container);
                                    break;
                                }
                            }
                            added = true;
                        } else {
                            onExpired(container, ExpiryTrigger.BY_TIME);
                            start();
                        }
                    }
                }
                if (container.expiryTriggers.contains(ExpiryTrigger.BY_GROUP_TERMINATION) && !this.mGroupTrackedContainers.contains(container)) {
                    if (matchActiveGroup(container)) {
                        Logger.d(TAG, "added tracking by group (gid=" + this.mActiveGroupId + "): " + container.getPrintableSignature());
                        this.mGroupTrackedContainers.add(container);
                        added = true;
                    } else {
                        onExpired(container, ExpiryTrigger.BY_GROUP_TERMINATION);
                        start();
                    }
                }
                if (added) {
                    saveToDb(container);
                }
            } finally {
                start();
            }
        }
    }

    public synchronized void remove(ContentContainer container) {
        stop();
        boolean removed = false;
        if (this.mTimeTrackedContainers.remove(container)) {
            Logger.d(TAG, "removed tracking by time: " + container.getPrintableSignature());
            removed = true;
        }
        if (this.mGroupTrackedContainers.remove(container)) {
            Logger.d(TAG, "removed tracking by group: " + container.getPrintableSignature());
            removed = true;
        }
        if (removed) {
            removeFromDb(container.shareId);
        }
        start();
    }

    public synchronized void remove(String shareId) {
        stop();
        boolean removed = false;
        Iterator<ContentContainer> iter = this.mTimeTrackedContainers.iterator();
        while (true) {
            if (!iter.hasNext()) {
                break;
            }
            ContentContainer container = (ContentContainer) iter.next();
            if (container.shareId != null && container.shareId.equals(shareId)) {
                Logger.d(TAG, "removed tracking by time: " + container.getPrintableSignature());
                iter.remove();
                removed = true;
                break;
            }
        }
        Iterator<ContentContainer> iter2 = this.mGroupTrackedContainers.iterator();
        while (true) {
            if (!iter2.hasNext()) {
                break;
            }
            ContentContainer container2 = (ContentContainer) iter2.next();
            if (container2.shareId != null && container2.shareId.equals(shareId)) {
                Logger.d(TAG, "removed tracking by group: " + container2.getPrintableSignature());
                iter2.remove();
                removed = true;
                break;
            }
        }
        if (removed) {
            removeFromDb(shareId);
        }
        start();
    }

    /* access modifiers changed from: private */
    public synchronized void onTimerExpired() {
        start();
    }

    private synchronized void onGroupStatusChanged(GroupInfo groupInfo) {
        String newGroupId = groupInfo.isActive ? groupInfo.id : null;
        if (!TextUtils.equals(newGroupId, this.mActiveGroupId)) {
            this.mActiveGroupId = newGroupId;
            stop();
            Iterator<ContentContainer> iter = this.mGroupTrackedContainers.iterator();
            while (iter.hasNext()) {
                ContentContainer container = (ContentContainer) iter.next();
                if (!matchActiveGroup(container)) {
                    iter.remove();
                    onExpired(container, ExpiryTrigger.BY_GROUP_TERMINATION);
                }
            }
            start();
        }
    }

    private void onExpired(ContentContainer container, ExpiryTrigger trigger) {
        Logger.d(TAG, "expired: " + container.getPrintableSignature());
        removeFromDb(container.shareId);
        this.mTimeTrackedContainers.remove(container);
        this.mGroupTrackedContainers.remove(container);
        if (this.mExpiryListener != null) {
            this.mExpiryListener.onContainerExpired(container, trigger);
        }
    }

    private void start() {
        long now = SystemClock.elapsedRealtime();
        Iterator<ContentContainer> iter = this.mTimeTrackedContainers.iterator();
        while (iter.hasNext()) {
            ContentContainer container = (ContentContainer) iter.next();
            if (container.getExpiryElapsedTime() <= now) {
                iter.remove();
                onExpired(container, ExpiryTrigger.BY_TIME);
            }
        }
        if (this.mTimeTrackedContainers.size() > 0) {
            startTimer(((ContentContainer) this.mTimeTrackedContainers.get(0)).getExpiryElapsedTime() - now);
        }
        Logger.d(TAG, "total tracked by time: " + this.mTimeTrackedContainers.size() + "; by group: " + this.mGroupTrackedContainers.size());
    }

    private void stop() {
        stopTimer();
    }

    private void startTimer(long delay) {
        this.mExpiryHandler.removeMessages(4000);
        this.mExpiryHandler.sendEmptyMessageDelayed(4000, delay);
        Logger.d(TAG, "timer started (" + (delay / 1000) + " sec)");
    }

    private void stopTimer() {
        this.mExpiryHandler.removeMessages(4000);
    }

    private void saveToDb(ContentContainer container) {
        if (!this.mHistoryManager.shareExists(container)) {
            this.mHistoryManager.addShare(container);
        }
    }

    private void removeFromDb(String shareId) {
        this.mHistoryManager.removeShare(shareId);
    }

    public void onGroupEvent(GroupEvent event) {
        switch (event.type) {
            case GROUP_ACTIVE:
            case GROUP_INACTIVE:
                onGroupStatusChanged(event.groupInfo);
                return;
            default:
                return;
        }
    }

    private boolean isGroupActive() {
        return !TextUtils.isEmpty(this.mActiveGroupId);
    }

    private boolean matchActiveGroup(ContentContainer container) {
        if (!isGroupActive()) {
            return false;
        }
        GroupInfo groupInfo = container.getGroupInfo();
        if (groupInfo != null) {
            return TextUtils.equals(this.mActiveGroupId, groupInfo.id);
        }
        return false;
    }
}
