package com.motorola.sharing.provider.cloud;

import com.motorola.sharing.ContentContainer;

public class CloudData {
    public boolean isOperationCanceled = false;
    public boolean isPendingStopShare = false;
    public ContentContainer originalContainer = null;

    public static boolean isOperationCanceled(ContentContainer container) {
        return getExtras(container).isOperationCanceled;
    }

    public static CloudData getExtras(ContentContainer contentContainer) {
        if (contentContainer == null) {
            throw new IllegalArgumentException("container is null");
        }
        CloudData cloudData = (CloudData) contentContainer.getProviderData(CloudData.class);
        if (cloudData != null) {
            return cloudData;
        }
        CloudData cloudData2 = new CloudData();
        contentContainer.putProviderData(cloudData2);
        return cloudData2;
    }
}
