package com.google.android.gms.internal;

public final class gt {
    public static boolean isValid(int outcome) {
        switch (outcome) {
            case 0:
            case 1:
            case 2:
            case 3:
                return true;
            default:
                return false;
        }
    }
}
