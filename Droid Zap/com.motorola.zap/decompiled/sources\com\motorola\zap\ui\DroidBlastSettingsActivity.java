package com.motorola.zap.ui;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Build.VERSION;
import android.os.Bundle;
import com.motorola.sharing.GCSP;
import com.motorola.zap.ZapConstants.BlastSettingsLaunchPoint;

public class DroidBlastSettingsActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent() != null) {
            GCSP.getCheckinManager().logSettingsLaunchPoint(getIntent().getBooleanExtra("circle", false) ? BlastSettingsLaunchPoint.CIRCLE.toString() : BlastSettingsLaunchPoint.SETTINGS.toString());
        }
        if (VERSION.SDK_INT < 16) {
            getActionBar().setDisplayHomeAsUpEnabled(false);
        } else {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(16908290, new DroidBlastPreferenceFragment());
            fragmentTransaction.commit();
        }
    }

    public boolean onNavigateUp() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            finish();
        }
        return true;
    }
}
