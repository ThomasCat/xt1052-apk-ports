package com.motorola.gcsp.json;

import com.motorola.sharing.util.Logger;

public enum StatusCode {
    OK,
    ERROR,
    DISABLED,
    BLOCKED,
    CANCELLED,
    RETRY,
    LOCKED,
    NO_MATCH,
    NOT_FOUND,
    NO,
    INACTIVE,
    DUPLICATE,
    ERR_001,
    ERR_009,
    ERR_010,
    ERR_011,
    ERR_012,
    ERR_013,
    ERR_014,
    ERR_015,
    ERR_016,
    ERR_026,
    ERR_028,
    ERR_040,
    ERR_041,
    ERR_044,
    UNKNOWN;

    public static StatusCode codeOf(String status) {
        if (status == null) {
            return null;
        }
        try {
            return valueOf(status.toUpperCase());
        } catch (Exception e) {
            Logger.d(Logger.getCloudTag("StatusCode"), "Unknown server status: " + status);
            return UNKNOWN;
        }
    }

    public boolean equalsIgnoreCase(String value) {
        if (value != null) {
            return value.equalsIgnoreCase(toString());
        }
        return false;
    }

    public boolean equals(String value) {
        if (value != null) {
            return value.equals(toString());
        }
        return false;
    }
}
