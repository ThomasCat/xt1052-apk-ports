package com.google.android.gms.cast;

import android.content.Context;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ApiOptions;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a.C0003a;
import com.google.android.gms.internal.dq;
import com.google.android.gms.internal.ee;
import com.google.android.gms.internal.er;
import com.motorola.zap.ZapConstants;
import java.io.IOException;

public final class Cast {
    public static final Api API = new Api(va, new Scope[0]);
    public static final CastApi CastApi = new a();
    public static final String EXTRA_APP_NO_LONGER_RUNNING = "com.google.android.gms.cast.EXTRA_APP_NO_LONGER_RUNNING";
    public static final int MAX_MESSAGE_LENGTH = 65536;
    public static final int MAX_NAMESPACE_LENGTH = 128;
    static final com.google.android.gms.common.api.Api.b<dq> va = new com.google.android.gms.common.api.Api.b<dq>() {
        /* renamed from: c */
        public dq b(Context context, Looper looper, ee eeVar, ApiOptions apiOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            er.b(apiOptions, (Object) "Setting the API options is required.");
            er.b(apiOptions instanceof CastOptions, (Object) "Must provide valid CastOptions!");
            CastOptions castOptions = (CastOptions) apiOptions;
            return new dq(context, looper, castOptions.wv, (long) castOptions.wx, castOptions.ww, connectionCallbacks, onConnectionFailedListener);
        }

        public int getPriority() {
            return Integer.MAX_VALUE;
        }
    };

    public interface ApplicationConnectionResult extends Result {
        ApplicationMetadata getApplicationMetadata();

        String getApplicationStatus();

        String getSessionId();

        boolean getWasLaunched();
    }

    public interface CastApi {

        public static final class a implements CastApi {
            public ApplicationMetadata getApplicationMetadata(GoogleApiClient client) throws IllegalStateException {
                return ((dq) client.a(Cast.va)).getApplicationMetadata();
            }

            public String getApplicationStatus(GoogleApiClient client) throws IllegalStateException {
                return ((dq) client.a(Cast.va)).getApplicationStatus();
            }

            public double getVolume(GoogleApiClient client) throws IllegalStateException {
                return ((dq) client.a(Cast.va)).da();
            }

            public boolean isMute(GoogleApiClient client) throws IllegalStateException {
                return ((dq) client.a(Cast.va)).isMute();
            }

            public PendingResult<ApplicationConnectionResult> joinApplication(GoogleApiClient client) {
                return client.b(new c() {
                    /* access modifiers changed from: protected */
                    public void a(dq dqVar) throws RemoteException {
                        try {
                            dqVar.b(null, null, this);
                        } catch (IllegalStateException e) {
                            x(2001);
                        }
                    }
                });
            }

            public PendingResult<ApplicationConnectionResult> joinApplication(GoogleApiClient client, final String applicationId) {
                return client.b(new c() {
                    /* access modifiers changed from: protected */
                    public void a(dq dqVar) throws RemoteException {
                        try {
                            dqVar.b(applicationId, null, this);
                        } catch (IllegalStateException e) {
                            x(2001);
                        }
                    }
                });
            }

            public PendingResult<ApplicationConnectionResult> joinApplication(GoogleApiClient client, final String applicationId, final String sessionId) {
                return client.b(new c() {
                    /* access modifiers changed from: protected */
                    public void a(dq dqVar) throws RemoteException {
                        try {
                            dqVar.b(applicationId, sessionId, this);
                        } catch (IllegalStateException e) {
                            x(2001);
                        }
                    }
                });
            }

            public PendingResult<ApplicationConnectionResult> launchApplication(GoogleApiClient client, final String applicationId) {
                return client.b(new c() {
                    /* access modifiers changed from: protected */
                    public void a(dq dqVar) throws RemoteException {
                        try {
                            dqVar.a(applicationId, false, (com.google.android.gms.common.api.a.c<ApplicationConnectionResult>) this);
                        } catch (IllegalStateException e) {
                            x(2001);
                        }
                    }
                });
            }

            public PendingResult<ApplicationConnectionResult> launchApplication(GoogleApiClient client, final String applicationId, final boolean relaunchIfRunning) {
                return client.b(new c() {
                    /* access modifiers changed from: protected */
                    public void a(dq dqVar) throws RemoteException {
                        try {
                            dqVar.a(applicationId, relaunchIfRunning, (com.google.android.gms.common.api.a.c<ApplicationConnectionResult>) this);
                        } catch (IllegalStateException e) {
                            x(2001);
                        }
                    }
                });
            }

            public PendingResult<Status> leaveApplication(GoogleApiClient client) {
                return client.b(new b() {
                    /* access modifiers changed from: protected */
                    public void a(dq dqVar) throws RemoteException {
                        try {
                            dqVar.e((com.google.android.gms.common.api.a.c<Status>) this);
                        } catch (IllegalStateException e) {
                            x(2001);
                        }
                    }
                });
            }

            public void removeMessageReceivedCallbacks(GoogleApiClient client, String namespace) throws IOException, IllegalArgumentException {
                try {
                    ((dq) client.a(Cast.va)).Q(namespace);
                } catch (RemoteException e) {
                    throw new IOException("service error");
                }
            }

            public void requestStatus(GoogleApiClient client) throws IOException, IllegalStateException {
                try {
                    ((dq) client.a(Cast.va)).cZ();
                } catch (RemoteException e) {
                    throw new IOException("service error");
                }
            }

            public PendingResult<Status> sendMessage(GoogleApiClient client, final String namespace, final String message) {
                return client.b(new b() {
                    /* access modifiers changed from: protected */
                    public void a(dq dqVar) throws RemoteException {
                        try {
                            dqVar.a(namespace, message, (com.google.android.gms.common.api.a.c<Status>) this);
                        } catch (IllegalArgumentException e) {
                            x(2001);
                        } catch (IllegalStateException e2) {
                            x(2001);
                        }
                    }
                });
            }

            public void setMessageReceivedCallbacks(GoogleApiClient client, String namespace, MessageReceivedCallback callbacks) throws IOException, IllegalStateException {
                try {
                    ((dq) client.a(Cast.va)).a(namespace, callbacks);
                } catch (RemoteException e) {
                    throw new IOException("service error");
                }
            }

            public void setMute(GoogleApiClient client, boolean mute) throws IOException, IllegalStateException {
                try {
                    ((dq) client.a(Cast.va)).t(mute);
                } catch (RemoteException e) {
                    throw new IOException("service error");
                }
            }

            public void setVolume(GoogleApiClient client, double volume) throws IOException, IllegalArgumentException, IllegalStateException {
                try {
                    ((dq) client.a(Cast.va)).a(volume);
                } catch (RemoteException e) {
                    throw new IOException("service error");
                }
            }

            public PendingResult<Status> stopApplication(GoogleApiClient client) {
                return client.b(new b() {
                    /* access modifiers changed from: protected */
                    public void a(dq dqVar) throws RemoteException {
                        try {
                            dqVar.a(ZapConstants.GOOGLE_ACCOUNT_NONE, (com.google.android.gms.common.api.a.c<Status>) this);
                        } catch (IllegalStateException e) {
                            x(2001);
                        }
                    }
                });
            }

            public PendingResult<Status> stopApplication(GoogleApiClient client, final String sessionId) {
                return client.b(new b() {
                    /* access modifiers changed from: protected */
                    public void a(dq dqVar) throws RemoteException {
                        if (TextUtils.isEmpty(sessionId)) {
                            c(2001, "IllegalArgument: sessionId cannot be null or empty");
                            return;
                        }
                        try {
                            dqVar.a(sessionId, (com.google.android.gms.common.api.a.c<Status>) this);
                        } catch (IllegalStateException e) {
                            x(2001);
                        }
                    }
                });
            }
        }

        ApplicationMetadata getApplicationMetadata(GoogleApiClient googleApiClient) throws IllegalStateException;

        String getApplicationStatus(GoogleApiClient googleApiClient) throws IllegalStateException;

        double getVolume(GoogleApiClient googleApiClient) throws IllegalStateException;

        boolean isMute(GoogleApiClient googleApiClient) throws IllegalStateException;

        PendingResult<ApplicationConnectionResult> joinApplication(GoogleApiClient googleApiClient);

        PendingResult<ApplicationConnectionResult> joinApplication(GoogleApiClient googleApiClient, String str);

        PendingResult<ApplicationConnectionResult> joinApplication(GoogleApiClient googleApiClient, String str, String str2);

        PendingResult<ApplicationConnectionResult> launchApplication(GoogleApiClient googleApiClient, String str);

        PendingResult<ApplicationConnectionResult> launchApplication(GoogleApiClient googleApiClient, String str, boolean z);

        PendingResult<Status> leaveApplication(GoogleApiClient googleApiClient);

        void removeMessageReceivedCallbacks(GoogleApiClient googleApiClient, String str) throws IOException, IllegalArgumentException;

        void requestStatus(GoogleApiClient googleApiClient) throws IOException, IllegalStateException;

        PendingResult<Status> sendMessage(GoogleApiClient googleApiClient, String str, String str2);

        void setMessageReceivedCallbacks(GoogleApiClient googleApiClient, String str, MessageReceivedCallback messageReceivedCallback) throws IOException, IllegalStateException;

        void setMute(GoogleApiClient googleApiClient, boolean z) throws IOException, IllegalStateException;

        void setVolume(GoogleApiClient googleApiClient, double d) throws IOException, IllegalArgumentException, IllegalStateException;

        PendingResult<Status> stopApplication(GoogleApiClient googleApiClient);

        PendingResult<Status> stopApplication(GoogleApiClient googleApiClient, String str);
    }

    public static final class CastOptions implements ApiOptions {
        final CastDevice wv;
        final Listener ww;
        /* access modifiers changed from: private */
        public final int wx;

        public static final class Builder {
            /* access modifiers changed from: private */
            public int wA;
            CastDevice wy;
            Listener wz;

            private Builder(CastDevice castDevice, Listener castListener) {
                er.b(castDevice, (Object) "CastDevice parameter cannot be null");
                er.b(castListener, (Object) "CastListener parameter cannot be null");
                this.wy = castDevice;
                this.wz = castListener;
                this.wA = 0;
            }

            public CastOptions build() {
                return new CastOptions(this);
            }

            public Builder setVerboseLoggingEnabled(boolean enabled) {
                if (enabled) {
                    this.wA |= 1;
                } else {
                    this.wA &= -2;
                }
                return this;
            }
        }

        private CastOptions(Builder builder) {
            this.wv = builder.wy;
            this.ww = builder.wz;
            this.wx = builder.wA;
        }

        public static Builder builder(CastDevice castDevice, Listener castListener) {
            return new Builder(castDevice, castListener);
        }
    }

    public static abstract class Listener {
        public void onApplicationDisconnected(int statusCode) {
        }

        public void onApplicationStatusChanged() {
        }

        public void onVolumeChanged() {
        }
    }

    public interface MessageReceivedCallback {
        void onMessageReceived(CastDevice castDevice, String str, String str2);
    }

    protected static abstract class a<R extends Result> extends C0003a<R, dq> implements PendingResult<R> {
        public a() {
            super(Cast.va);
        }

        public void c(int i, String str) {
            b(d(new Status(i, str, null)));
        }

        public void x(int i) {
            b(d(new Status(i)));
        }
    }

    private static abstract class b extends a<Status> {
        private b() {
        }

        /* renamed from: f */
        public Status d(Status status) {
            return status;
        }
    }

    private static abstract class c extends a<ApplicationConnectionResult> {
        private c() {
        }

        /* renamed from: h */
        public ApplicationConnectionResult d(final Status status) {
            return new ApplicationConnectionResult() {
                public ApplicationMetadata getApplicationMetadata() {
                    return null;
                }

                public String getApplicationStatus() {
                    return null;
                }

                public String getSessionId() {
                    return null;
                }

                public Status getStatus() {
                    return status;
                }

                public boolean getWasLaunched() {
                    return false;
                }
            };
        }
    }

    private Cast() {
    }
}
