package com.google.tagmanager;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.google.analytics.containertag.proto.Serving.SupplementedResource;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.tagmanager.LoadCallback.Failure;
import com.motorola.zap.ZapConstants;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

class ResourceLoader implements Runnable {
    private LoadCallback<SupplementedResource> mCallback;
    private final NetworkClientFactory mClientFactory;
    private final String mContainerId;
    private final Context mContext;
    private volatile CtfeHost mCtfeHost;
    private volatile String mCtfeUrlPathAndQuery;
    private final String mDefaultCtfeUrlPathAndQuery;
    private volatile String mPreviousVersion;

    public ResourceLoader(Context context, String containerId, CtfeHost ctfeHost) {
        this(context, containerId, new NetworkClientFactory(), ctfeHost);
    }

    @VisibleForTesting
    ResourceLoader(Context context, String containerId, NetworkClientFactory factory, CtfeHost ctfeHost) {
        this.mContext = context;
        this.mClientFactory = factory;
        this.mContainerId = containerId;
        this.mCtfeHost = ctfeHost;
        this.mDefaultCtfeUrlPathAndQuery = "/r?id=" + containerId;
        this.mCtfeUrlPathAndQuery = this.mDefaultCtfeUrlPathAndQuery;
        this.mPreviousVersion = null;
    }

    public void run() {
        if (this.mCallback == null) {
            throw new IllegalStateException("callback must be set before execute");
        }
        this.mCallback.startLoad();
        loadResource();
    }

    private boolean okToLoad() {
        NetworkInfo network = ((ConnectivityManager) this.mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (network != null && network.isConnected()) {
            return true;
        }
        Log.v("...no network connectivity");
        return false;
    }

    /* access modifiers changed from: 0000 */
    public void setLoadCallback(LoadCallback<SupplementedResource> callback) {
        this.mCallback = callback;
    }

    private void loadResource() {
        if (!okToLoad()) {
            this.mCallback.onFailure(Failure.NOT_AVAILABLE);
            return;
        }
        Log.v("Start loading resource from network ...");
        String url = getCtfeUrl();
        NetworkClient networkClient = this.mClientFactory.createNetworkClient();
        try {
            InputStream inputStream = networkClient.getInputStream(url);
            try {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                ResourceUtil.copyStream(inputStream, outputStream);
                SupplementedResource resource = SupplementedResource.parseFrom(outputStream.toByteArray());
                Log.v("Successfully loaded supplemented resource: " + resource);
                if (resource.resource == null) {
                    Log.v("No change for container: " + this.mContainerId);
                }
                this.mCallback.onSuccess(resource);
                networkClient.close();
                Log.v("Load resource from network finished.");
            } catch (IOException e) {
                Log.w("Error when parsing downloaded resources from url: " + url + " " + e.getMessage(), e);
                this.mCallback.onFailure(Failure.SERVER_ERROR);
                networkClient.close();
            }
        } catch (FileNotFoundException e2) {
            Log.w("No data is retrieved from the given url: " + url + ". Make sure container_id: " + this.mContainerId + " is correct.");
            this.mCallback.onFailure(Failure.SERVER_ERROR);
            networkClient.close();
        } catch (IOException e3) {
            Log.w("Error when loading resources from url: " + url + " " + e3.getMessage(), e3);
            this.mCallback.onFailure(Failure.IO_ERROR);
            networkClient.close();
        } catch (Throwable th) {
            networkClient.close();
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public String getCtfeUrl() {
        String str = this.mCtfeHost.getCtfeServerAddress() + this.mCtfeUrlPathAndQuery + "&v=a62676326";
        if (this.mPreviousVersion != null && !this.mPreviousVersion.trim().equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
            str = str + "&pv=" + this.mPreviousVersion;
        }
        if (PreviewManager.getInstance().getPreviewMode().equals(PreviewMode.CONTAINER_DEBUG)) {
            return str + "&gtm_debug=x";
        }
        return str;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void setCtfeURLPathAndQuery(String str) {
        if (str == null) {
            this.mCtfeUrlPathAndQuery = this.mDefaultCtfeUrlPathAndQuery;
            return;
        }
        Log.d("Setting CTFE URL path: " + str);
        this.mCtfeUrlPathAndQuery = str;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void setPreviousVersion(String str) {
        Log.d("Setting previous container version: " + str);
        this.mPreviousVersion = str;
    }
}
