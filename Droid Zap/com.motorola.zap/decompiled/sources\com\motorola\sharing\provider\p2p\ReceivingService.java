package com.motorola.sharing.provider.p2p;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.text.TextUtils;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentContainer.TransferStatus;
import com.motorola.sharing.ContentDownloadListener.ContentDownloadEvent.Substatus;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.util.FileUtils;
import com.motorola.sharing.util.Logger;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ReceivingService extends Service {
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getP2pTag("ReceivingSvc");
    private static Queue<ContentContainer> gPendingContainers = new ConcurrentLinkedQueue();
    /* access modifiers changed from: private */
    public ConnectionStateReceiver mConnectionStateListener = new ConnectionStateReceiver();
    ConnectionTimeoutHandler mConnectionTimeoutHandler = new ConnectionTimeoutHandler(this);
    /* access modifiers changed from: private */
    public ContentDownloader mContentDownloader = null;
    /* access modifiers changed from: private */
    public ContentContainer mCurrentContainer = null;
    /* access modifiers changed from: private */
    public int mPreviousNetworkId = -1;
    /* access modifiers changed from: private */
    public ReConnectionStateReceiver mReConnectionStateListener = new ReConnectionStateReceiver();
    /* access modifiers changed from: private */
    public boolean mReceivingInProgress = false;
    private int mSharingNetworkId = -1;
    /* access modifiers changed from: private */
    public WifiManager mWifiManager = null;
    /* access modifiers changed from: private */
    public WifiStateReceiver mWifiStateReceiver = new WifiStateReceiver();

    private class ConnectionStateReceiver extends BroadcastReceiver {
        private ConnectionStateReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
                WifiInfo info = ReceivingService.this.mWifiManager.getConnectionInfo();
                if (info != null && info.getSSID() != null && info.getSSID().equals("\"" + ReceivingService.this.mCurrentContainer.p2pParams.name + "\"") && P2pUtils.isWifiConnected(ReceivingService.this)) {
                    Logger.i(ReceivingService.TAG, "Connected to sharing device, downloading file...");
                    ReceivingService.this.mConnectionTimeoutHandler.removeMessages(2000);
                    ReceivingService.this.unregisterReceiver(ReceivingService.this.mConnectionStateListener);
                    ReceivingService.this.downloadFile();
                }
            }
        }
    }

    private static class ConnectionTimeoutHandler extends Handler {
        private WeakReference<ReceivingService> mService;

        public ConnectionTimeoutHandler(ReceivingService service) {
            this.mService = new WeakReference<>(service);
        }

        public void handleMessage(Message msg) {
            if (msg.what == 2000) {
                Logger.i(ReceivingService.TAG, "Connection attempt timed out");
                ReceivingService service = (ReceivingService) this.mService.get();
                if (service != null) {
                    service.notifyContainersOnError();
                    service.unregisterReceiver(service.mConnectionStateListener);
                    service.reconnectPreviousNetwork();
                }
            }
        }
    }

    private class ContentDownloader extends AsyncTask<Void, Integer, Boolean> {
        private ContentDownloader() {
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x012d A[SYNTHETIC, Splitter:B:44:0x012d] */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x0132  */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x0168 A[SYNTHETIC, Splitter:B:54:0x0168] */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x016d  */
        /* JADX WARNING: Removed duplicated region for block: B:62:0x0180 A[SYNTHETIC, Splitter:B:62:0x0180] */
        /* JADX WARNING: Removed duplicated region for block: B:65:0x0185  */
        /* JADX WARNING: Removed duplicated region for block: B:89:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:91:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Unknown top exception splitter block from list: {B:51:0x0143=Splitter:B:51:0x0143, B:41:0x00f6=Splitter:B:41:0x00f6} */
        public Boolean doInBackground(Void... params) {
            HttpURLConnection conn = null;
            FileOutputStream fos = null;
            int downloadedSize = 0;
            int previouslyReported = 0;
            try {
                File targetFile = new File(ContentContainer.generateDownloadPath(ReceivingService.this.mCurrentContainer));
                try {
                    FileOutputStream fos2 = new FileOutputStream(targetFile);
                    try {
                        conn = (HttpURLConnection) new URL(ReceivingService.this.mCurrentContainer.p2pParams.url).openConnection();
                        InputStream is = conn.getInputStream();
                        byte[] buffer = new byte[102400];
                        while (true) {
                            int size = is.read(buffer);
                            if (size != -1 && !isCancelled()) {
                                if (size > 0) {
                                    fos2.write(buffer, 0, size);
                                    downloadedSize += size;
                                    if (downloadedSize - previouslyReported > 1048576) {
                                        int progress = downloadedSize / AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START;
                                        publishProgress(new Integer[]{Integer.valueOf(progress)});
                                        Logger.i(ReceivingService.TAG, "Downloaded from peer: " + progress + " MB");
                                        previouslyReported = downloadedSize;
                                    }
                                }
                            }
                        }
                        if (isCancelled()) {
                            Boolean valueOf = Boolean.valueOf(false);
                            if (fos2 != null) {
                                try {
                                    fos2.close();
                                } catch (IOException e) {
                                    Logger.e(ReceivingService.TAG, "Failed to close the downloaded file");
                                }
                            }
                            if (conn != null) {
                                conn.disconnect();
                            }
                            File file = targetFile;
                            FileOutputStream fileOutputStream = fos2;
                            return valueOf;
                        }
                        ReceivingService.this.mCurrentContainer.path = targetFile.getAbsolutePath();
                        if (fos2 != null) {
                            try {
                                fos2.close();
                            } catch (IOException e2) {
                                Logger.e(ReceivingService.TAG, "Failed to close the downloaded file");
                            }
                        }
                        if (conn != null) {
                            conn.disconnect();
                        }
                        if (FileUtils.isMedia(ReceivingService.this.mCurrentContainer.mimeType)) {
                            FileUtils.refreshMediaFile(targetFile.getAbsolutePath(), ReceivingService.this);
                        }
                        File file2 = targetFile;
                        FileOutputStream fileOutputStream2 = fos2;
                        return Boolean.valueOf(true);
                    } catch (MalformedURLException e3) {
                        File file3 = targetFile;
                        fos = fos2;
                        try {
                            Logger.e(ReceivingService.TAG, "Bad URL: " + ReceivingService.this.mCurrentContainer.p2pParams.url);
                            Boolean valueOf2 = Boolean.valueOf(false);
                            if (fos != null) {
                                try {
                                    fos.close();
                                } catch (IOException e4) {
                                    Logger.e(ReceivingService.TAG, "Failed to close the downloaded file");
                                }
                            }
                            if (conn == null) {
                                return valueOf2;
                            }
                            conn.disconnect();
                            return valueOf2;
                        } catch (Throwable th) {
                            th = th;
                        }
                    } catch (IOException e5) {
                        e = e5;
                        File file4 = targetFile;
                        fos = fos2;
                        Logger.e(ReceivingService.TAG, "Connection or disk failure: " + e.getMessage());
                        Boolean valueOf3 = Boolean.valueOf(false);
                        if (fos != null) {
                            try {
                                fos.close();
                            } catch (IOException e6) {
                                Logger.e(ReceivingService.TAG, "Failed to close the downloaded file");
                            }
                        }
                        if (conn == null) {
                            return valueOf3;
                        }
                        conn.disconnect();
                        return valueOf3;
                    } catch (Throwable th2) {
                        th = th2;
                        File file5 = targetFile;
                        fos = fos2;
                        if (fos != null) {
                            try {
                                fos.close();
                            } catch (IOException e7) {
                                Logger.e(ReceivingService.TAG, "Failed to close the downloaded file");
                            }
                        }
                        if (conn != null) {
                            conn.disconnect();
                        }
                        throw th;
                    }
                } catch (MalformedURLException e8) {
                    File file6 = targetFile;
                    Logger.e(ReceivingService.TAG, "Bad URL: " + ReceivingService.this.mCurrentContainer.p2pParams.url);
                    Boolean valueOf22 = Boolean.valueOf(false);
                    if (fos != null) {
                    }
                    if (conn == null) {
                    }
                } catch (IOException e9) {
                    e = e9;
                    File file7 = targetFile;
                    Logger.e(ReceivingService.TAG, "Connection or disk failure: " + e.getMessage());
                    Boolean valueOf32 = Boolean.valueOf(false);
                    if (fos != null) {
                    }
                    if (conn == null) {
                    }
                } catch (Throwable th3) {
                    th = th3;
                    File file8 = targetFile;
                    if (fos != null) {
                    }
                    if (conn != null) {
                    }
                    throw th;
                }
            } catch (MalformedURLException e10) {
                Logger.e(ReceivingService.TAG, "Bad URL: " + ReceivingService.this.mCurrentContainer.p2pParams.url);
                Boolean valueOf222 = Boolean.valueOf(false);
                if (fos != null) {
                }
                if (conn == null) {
                }
            } catch (IOException e11) {
                e = e11;
                Logger.e(ReceivingService.TAG, "Connection or disk failure: " + e.getMessage());
                Boolean valueOf322 = Boolean.valueOf(false);
                if (fos != null) {
                }
                if (conn == null) {
                }
            }
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... values) {
            int progress = values[0].intValue();
            if (ReceivingService.this.mCurrentContainer.transferStatus == null) {
                ReceivingService.this.mCurrentContainer.transferStatus = new TransferStatus(ReceivingService.this.mCurrentContainer.size / AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START);
            }
            ReceivingService.this.mCurrentContainer.transferStatus.transferredBytes = progress;
            ReceivingService.this.mCurrentContainer.onContentDownloadEvent(Status.INFO, Substatus.PROGRESS, P2pProvider.getInstance().getEndpoint());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result.booleanValue()) {
                Logger.i(ReceivingService.TAG, "Downloaded file");
                ReceivingService.this.mCurrentContainer.onContentDownloadEvent(Status.SUCCESS, Substatus.DOWNLOADED, P2pProvider.getInstance().getEndpoint());
            } else {
                Logger.e(ReceivingService.TAG, "Failed to download file");
                ReceivingService.this.mCurrentContainer.onContentDownloadEvent(Status.ERROR, Substatus.NETWORK, P2pProvider.getInstance().getEndpoint());
            }
            ReceivingService.this.mContentDownloader = null;
            ReceivingService.this.reconnectPreviousNetwork();
        }
    }

    private class ReConnectionStateReceiver extends BroadcastReceiver {
        private ReConnectionStateReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
                WifiInfo info = ReceivingService.this.mWifiManager.getConnectionInfo();
                if (info != null && info.getNetworkId() == ReceivingService.this.mPreviousNetworkId && P2pUtils.isWifiConnected(ReceivingService.this)) {
                    Logger.i(ReceivingService.TAG, "Reconnected to previous network");
                    ReceivingService.this.unregisterReceiver(ReceivingService.this.mReConnectionStateListener);
                    ReceivingService.this.mCurrentContainer = null;
                    ReceivingService.this.mReceivingInProgress = false;
                    ReceivingService.this.processPendingContainers();
                }
            }
        }
    }

    private class WifiStateReceiver extends BroadcastReceiver {
        private WifiStateReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("android.net.wifi.WIFI_STATE_CHANGED".equals(intent.getAction()) && intent.getIntExtra("wifi_state", -1) == 3) {
                Logger.i(ReceivingService.TAG, "WiFi enabled");
                ReceivingService.this.unregisterReceiver(ReceivingService.this.mWifiStateReceiver);
                ReceivingService.this.connectToSharingDevice();
            }
        }
    }

    public static void startReceiving(Context context, ContentContainer contentContainer) {
        gPendingContainers.add(contentContainer);
        Intent intent = new Intent(context, ReceivingService.class);
        intent.putExtra("command", "start_receiving");
        context.startService(intent);
    }

    public static void stopReceiving(Context context, ContentContainer contentContainer) {
        Intent intent = new Intent(context, ReceivingService.class);
        intent.putExtra("command", "stop_receiving");
        intent.putExtra("share_id", contentContainer.shareId);
        context.startService(intent);
    }

    public void onCreate() {
        super.onCreate();
        this.mWifiManager = (WifiManager) getSystemService("wifi");
        this.mPreviousNetworkId = -1;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        String command = intent.getStringExtra("command");
        if ("start_receiving".equals(command)) {
            if (!this.mReceivingInProgress) {
                processPendingContainers();
            }
        } else if ("stop_receiving".equals(command)) {
            String shareId = intent.getStringExtra("share_id");
            if (!TextUtils.isEmpty(shareId)) {
                if (this.mCurrentContainer == null || !this.mCurrentContainer.shareId.equals(shareId)) {
                    Iterator i$ = gPendingContainers.iterator();
                    while (true) {
                        if (!i$.hasNext()) {
                            break;
                        }
                        ContentContainer container = (ContentContainer) i$.next();
                        if (container.shareId.equals(shareId)) {
                            gPendingContainers.remove(container);
                            container.onContentDownloadEvent(Status.SUCCESS, Substatus.CANCELED, P2pProvider.getInstance().getEndpoint());
                            break;
                        }
                    }
                } else {
                    if (this.mConnectionTimeoutHandler.hasMessages(2000)) {
                        this.mConnectionTimeoutHandler.removeMessages(2000);
                        unregisterReceiver(this.mConnectionStateListener);
                    }
                    if (this.mContentDownloader != null) {
                        this.mContentDownloader.cancel(true);
                        this.mContentDownloader = null;
                    }
                    reconnectPreviousNetwork();
                    this.mCurrentContainer.onContentDownloadEvent(Status.SUCCESS, Substatus.CANCELED, P2pProvider.getInstance().getEndpoint());
                }
            }
        }
        return 2;
    }

    /* access modifiers changed from: private */
    public void processPendingContainers() {
        if (gPendingContainers.size() > 0) {
            this.mCurrentContainer = (ContentContainer) gPendingContainers.poll();
            if (TextUtils.isEmpty(this.mCurrentContainer.mimeType)) {
                String fileName = Uri.parse(this.mCurrentContainer.p2pParams.url).getLastPathSegment();
                this.mCurrentContainer.mimeType = FileUtils.getMimeTypeFromFileName(fileName);
            }
            this.mReceivingInProgress = true;
            connectToSharingDevice();
        }
    }

    /* access modifiers changed from: private */
    public void connectToSharingDevice() {
        if (!this.mWifiManager.isWifiEnabled()) {
            Logger.i(TAG, "Enabling WiFi");
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
            registerReceiver(this.mWifiStateReceiver, filter);
            this.mWifiManager.setWifiEnabled(true);
            return;
        }
        WifiInfo info = this.mWifiManager.getConnectionInfo();
        if (info == null || !info.getSSID().equals("\"" + this.mCurrentContainer.p2pParams.name + "\"") || !P2pUtils.isWifiConnected(this)) {
            this.mSharingNetworkId = -1;
            this.mPreviousNetworkId = -1;
            for (WifiConfiguration conf : this.mWifiManager.getConfiguredNetworks()) {
                if (conf.SSID != null && conf.SSID.equals("\"" + this.mCurrentContainer.p2pParams.name + "\"")) {
                    Logger.i(TAG, "Found existing network configuration");
                    this.mSharingNetworkId = conf.networkId;
                } else if (conf.status == 0 && conf.SSID != null) {
                    Logger.i(TAG, "Found active Wifi connection: " + conf.SSID);
                    this.mPreviousNetworkId = conf.networkId;
                }
            }
            if (this.mSharingNetworkId == -1) {
                WifiConfiguration conf2 = new WifiConfiguration();
                conf2.SSID = "\"" + this.mCurrentContainer.p2pParams.name + "\"";
                conf2.preSharedKey = "\"" + this.mCurrentContainer.p2pParams.password + "\"";
                this.mSharingNetworkId = this.mWifiManager.addNetwork(conf2);
                if (this.mSharingNetworkId == -1) {
                    Logger.e(TAG, "Failed to add WiFi network");
                    return;
                }
                Logger.i(TAG, "Created new network configuration");
            }
            if (!this.mWifiManager.disconnect()) {
                Logger.e(TAG, "Failed to disconnect current network");
                return;
            }
            IntentFilter filter2 = new IntentFilter();
            filter2.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            registerReceiver(this.mConnectionStateListener, filter2);
            boolean error = false;
            Logger.i(TAG, "Enabling P2p network...");
            if (!this.mWifiManager.enableNetwork(this.mSharingNetworkId, true)) {
                Logger.e(TAG, "Failed to enable new network");
                error = true;
            }
            if (!error) {
                Logger.i(TAG, "Connecting to P2p network...");
                this.mConnectionTimeoutHandler.sendEmptyMessageDelayed(2000, 60000);
                if (!this.mWifiManager.reconnect()) {
                    Logger.e(TAG, "Failed to reconnect WiFi network");
                    error = true;
                }
            }
            if (error) {
                unregisterReceiver(this.mConnectionStateListener);
                this.mConnectionTimeoutHandler.removeMessages(2000);
                reconnectPreviousNetwork();
                return;
            }
            return;
        }
        Logger.i(TAG, "Already connected to source device");
        downloadFile();
    }

    /* access modifiers changed from: private */
    public void reconnectPreviousNetwork() {
        this.mWifiManager.disconnect();
        this.mWifiManager.disableNetwork(this.mSharingNetworkId);
        this.mWifiManager.removeNetwork(this.mSharingNetworkId);
        if (this.mPreviousNetworkId != -1) {
            Logger.i(TAG, "Reconnecting previous network...");
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            registerReceiver(this.mReConnectionStateListener, filter);
            boolean error = false;
            if (!this.mWifiManager.enableNetwork(this.mPreviousNetworkId, true)) {
                Logger.w(TAG, "Failed to enable previous network");
                error = true;
            } else if (!this.mWifiManager.reconnect()) {
                Logger.w(TAG, "Failed to reconnect to previous network");
                error = true;
            }
            if (error) {
                unregisterReceiver(this.mReConnectionStateListener);
            }
        }
    }

    /* access modifiers changed from: private */
    public void downloadFile() {
        Logger.i(TAG, "Downloading file...");
        this.mContentDownloader = new ContentDownloader();
        this.mContentDownloader.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void notifyContainersOnError() {
        if (this.mCurrentContainer != null) {
            this.mCurrentContainer.onContentDownloadEvent(Status.ERROR, Substatus.NETWORK, P2pProvider.getInstance().getEndpoint());
        }
        while (true) {
            ContentContainer container = (ContentContainer) gPendingContainers.poll();
            if (container != null) {
                container.onContentDownloadEvent(Status.ERROR, Substatus.NETWORK, P2pProvider.getInstance().getEndpoint());
            } else {
                return;
            }
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
