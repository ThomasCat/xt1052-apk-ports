package com.google.android.gms.drive;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.b;
import com.google.android.gms.common.api.GoogleApiClient.ApiOptions;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.drive.internal.l;
import com.google.android.gms.drive.internal.n;
import com.google.android.gms.drive.internal.p;
import com.google.android.gms.internal.ee;
import java.util.List;

public final class Drive {
    public static final Api API = new Api(va, new Scope[0]);
    public static final Scope Da = new Scope(Scopes.DRIVE_FULL);
    public static final c Db = new p();
    public static final DriveApi DriveApi = new l();
    public static final Scope SCOPE_APPFOLDER = new Scope(Scopes.DRIVE_APPFOLDER);
    public static final Scope SCOPE_FILE = new Scope(Scopes.DRIVE_FILE);
    public static final b<n> va = new b<n>() {
        /* renamed from: d */
        public n b(Context context, Looper looper, ee eeVar, ApiOptions apiOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            List dT = eeVar.dT();
            return new n(context, looper, eeVar, connectionCallbacks, onConnectionFailedListener, (String[]) dT.toArray(new String[dT.size()]));
        }

        public int getPriority() {
            return Integer.MAX_VALUE;
        }
    };

    private Drive() {
    }
}
