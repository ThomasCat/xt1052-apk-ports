package com.motorola.zap.ui;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView.RecyclerListener;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.google.android.gms.drive.DriveFile;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentContainer.TransferStatus;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupManager.GroupInfo;
import com.motorola.sharing.Profile;
import com.motorola.sharing.util.FileUtils;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.app.ActionReceiver;
import com.motorola.zap.app.App;
import com.motorola.zap.app.GroupInvite;
import com.motorola.zap.app.ZapTracker;
import com.motorola.zap.app.ZzInviteStore;
import com.motorola.zap.app.ZzUtil;
import com.motorola.zap.cacheutils.ZapImageCache;
import com.motorola.zap.identity.ZapProfileSharedPerfs;
import com.motorola.zap.provider.HistoryHandler;
import com.motorola.zap.provider.TCH;
import com.motorola.zap.ui.BitmapCache.BitmapWorkerTask;
import com.motorola.zap.ui.ZapGestureTouchListener.ZapGestureListener;
import com.motorola.zap.zz.ui.ZzLocationQueue;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class HistoryCursorAdapter extends CursorAdapter implements RecyclerListener {
    /* access modifiers changed from: private */
    public static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    ImageLoader imageLoader = ZapImageCache.getInstance().getImageLoader();
    /* access modifiers changed from: private */
    public final Context mContext;
    OnClickListener mContextMenuListener = new OnClickListener() {
        public void onClick(View v) {
            if (v != null) {
                int position = ((Integer) v.getTag()).intValue();
                Cursor cursor = HistoryCursorAdapter.this.getCursor();
                if (cursor.moveToPosition(position)) {
                    long uid = TCH.getEffectiveUid(cursor);
                    if (TCH.isTransferInProgress(cursor)) {
                        ContentContainer trans = ZapTracker.getInstance().get(uid);
                        if (trans == null) {
                            Log.e("HistoryAdapter", "BlastTrasaction is null for uid -" + uid);
                        } else if (TCH.isUploadInProgress(cursor)) {
                            HistoryCursorAdapter.this.cancelUpload(HistoryCursorAdapter.this.mContext, trans.getUid());
                        } else {
                            HistoryCursorAdapter.cancelDownload(HistoryCursorAdapter.this.mContext, trans);
                        }
                    } else {
                        long expiryTime = TCH.getMediaExpirationTimestamp(cursor);
                        int tranferStatus = TCH.getTransferCompleteStatus(cursor);
                        if (GCSP.getGroupManager().isAnyGroupActive() || tranferStatus != 0 || expiryTime - System.currentTimeMillis() <= 0) {
                            HistoryCursorAdapter.this.initiatePopupWindow(v, cursor);
                        } else {
                            HistoryCursorAdapter.this.cancelUpload(HistoryCursorAdapter.this.mContext, uid);
                        }
                    }
                }
            }
        }
    };
    OnClickListener mDeleteListener = new OnClickListener() {
        public void onClick(View v) {
            if (v != null) {
                int position = ((Integer) v.getTag()).intValue();
                Cursor cursor = HistoryCursorAdapter.this.getCursor();
                if (TCH.isZzSession(cursor) && !TCH.isActiveZz(cursor)) {
                    HistoryCursorAdapter.this.confirmDeleteZapZone(HistoryCursorAdapter.this.mContext, TCH.getZzId(cursor), TCH.getZzOwner(cursor), TCH.getZzName(cursor));
                    HistoryCursorAdapter.this.dismissPopupWindow();
                } else if (cursor.moveToPosition(position)) {
                    HistoryCursorAdapter.this.mHistoryHandler.deleteMedia(TCH.getFilePath(cursor));
                    HistoryCursorAdapter.this.dismissPopupWindow();
                }
            }
        }
    };
    DragZapAnimator mDragZapAnimator = null;
    private final Executor mExecutor = Executors.newFixedThreadPool(2);
    ZapGestureListener mGestureDetector = null;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final HistoryHandler mHistoryHandler = new HistoryHandler();
    private final LayoutInflater mInflater;
    ListView mListView = null;
    OnEditorActionListener mOnTextEditor = new OnEditorActionListener() {
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId != 6) {
                return false;
            }
            String pin = v.getText().toString();
            int uid = ((Integer) v.getTag()).intValue();
            View layout_t = (View) v.getTag(R.id.KEY_LAYOUT);
            v.setText(ZapConstants.GOOGLE_ACCOUNT_NONE);
            ((InputMethodManager) HistoryCursorAdapter.this.mContext.getSystemService("input_method")).hideSoftInputFromWindow(v.getWindowToken(), 0);
            HistoryCursorAdapter.this.joinZone(layout_t, uid, pin);
            return true;
        }
    };
    private final ProgressBarHelper mProgressBarHelper;
    OnClickListener mReportAbuseListener = new OnClickListener() {
        public void onClick(View v) {
            if (v != null) {
                int position = ((Integer) v.getTag()).intValue();
                Cursor cursor = HistoryCursorAdapter.this.getCursor();
                if (cursor.moveToPosition(position)) {
                    String filePath = TCH.getFilePath(cursor);
                    ZapUtils.reportImage(HistoryCursorAdapter.this.mContext, TCH.getFrom(cursor), filePath, TCH.getShareId(cursor));
                    HistoryCursorAdapter.this.dismissPopupWindow();
                }
            }
        }
    };
    OnClickListener mShareListener = new OnClickListener() {
        public void onClick(View view) {
            String filePath = null;
            int mediaType = 0;
            if (view != null) {
                int position = ((Integer) view.getTag()).intValue();
                Cursor cursor = HistoryCursorAdapter.this.getCursor();
                if (cursor.moveToPosition(position)) {
                    filePath = TCH.getFilePath(cursor);
                    mediaType = TCH.getMediaType(cursor);
                }
            }
            if (filePath != null) {
                HistoryCursorAdapter.this.launchShareIntents(filePath, mediaType);
            }
            HistoryCursorAdapter.this.dismissPopupWindow();
        }
    };
    private final ShareTimeHelper mShareTimeHelper;
    OnClickListener mViewPagerClickListener = new OnClickListener() {
        public void onClick(View view) {
            ViewPager viewPager = (ViewPager) view;
            int currentItem = viewPager.getCurrentItem();
            String str = (String) viewPager.getTag(R.id.KEY_ZZ_OWNER);
            String str2 = (String) viewPager.getTag(R.id.KEY_ZZ_ID);
            Uri imageContentUri = FileUtils.getImageContentUri(HistoryCursorAdapter.this.mContext, ((ZzViewPagerAdapter) viewPager.getAdapter()).getItem(currentItem));
            Intent intent = new Intent(App.getAppContext(), ImageViewActivity.class);
            intent.setData(imageContentUri);
            intent.putExtra(ImageViewActivity.ZZ_OWNER_NAME, str);
            intent.putExtra(ImageViewActivity.ZZ_ID, str2);
            intent.putExtra(ImageViewActivity.ZZ_ITEM_POSITION, currentItem);
            intent.putExtra(ImageViewActivity.IS_ZZ_FLODER_VIEW, true);
            intent.setFlags(DriveFile.MODE_READ_ONLY);
            HistoryCursorAdapter.this.mContext.startActivity(intent);
        }
    };
    /* access modifiers changed from: private */
    public int mWidth = 0;
    PopupWindow mWindow = null;
    OnClickListener mZapitListener = new OnClickListener() {
        public void onClick(View view) {
            int position = ((Integer) view.getTag()).intValue();
            Cursor cursor = HistoryCursorAdapter.this.getCursor();
            if (cursor.moveToPosition(position)) {
                int cardType = ZapHistoryInfo.getCardType(cursor);
                if (cardType != 2 && cardType != 3) {
                    String filePath = TCH.getFilePath(cursor);
                    if (filePath == null || !FileUtils.isFileExists(filePath)) {
                        ZapTracker.getInstance().onZapEvent(HistoryCursorAdapter.this.mContext.getString(R.string.toast_msg_media_deleted));
                        return;
                    }
                    Intent i = new Intent(ZapConstants.ACTION_GS_GALLERY_PHOTO_SHARED);
                    i.putExtra(ZapConstants.EXTRA_GS_GALLERY_PHOTO_SHARED, "file://" + filePath);
                    i.putExtra(ZapConstants.EXTRA_EVENT_ORIGIN_ZAP_APP, true);
                    App.getAppContext().sendBroadcast(i);
                }
            }
        }
    };
    private final Executor mZzFolderExecutor = Executors.newFixedThreadPool(2);
    OnClickListener mZzInviteClickListener = new OnClickListener() {
        public void onClick(View v) {
            int position = ((Integer) v.getTag()).intValue();
            Cursor cursor = HistoryCursorAdapter.this.getCursor();
            if (cursor.moveToPosition(position)) {
                int zzInviteUid = TCH.getUid(cursor);
                ViewGroup layout_t = (ViewGroup) v.getTag(R.id.KEY_LAYOUT);
                if (v.getId() == R.id.zz_accept) {
                    if (TCH.isPinLocked(cursor)) {
                        layout_t.findViewById(R.id.lockedview).setVisibility(0);
                        layout_t.findViewById(R.id.zz_join_consent).setVisibility(8);
                        layout_t.findViewById(R.id.zz_pin_entry).setVisibility(0);
                        layout_t.findViewById(R.id.zz_name_header).setVisibility(8);
                        EditText et = (EditText) layout_t.findViewById(R.id.zz_progress_code);
                        et.setTag(Integer.valueOf(zzInviteUid));
                        et.setTag(R.id.KEY_LAYOUT, layout_t);
                        et.setOnEditorActionListener(HistoryCursorAdapter.this.mOnTextEditor);
                        et.requestFocus();
                        ((InputMethodManager) HistoryCursorAdapter.this.mContext.getSystemService("input_method")).showSoftInput(et, 1);
                        return;
                    }
                    v.setSelected(true);
                    HistoryCursorAdapter.this.joinZone(layout_t, zzInviteUid);
                } else if (v.getId() == R.id.zz_reject || v.getId() == R.id.zz_img_reject) {
                    v.setSelected(true);
                    HistoryCursorAdapter.this.leaveZone(layout_t, zzInviteUid);
                }
            }
        }
    };
    OnClickListener mZzRejoinListener = new OnClickListener() {
        public void onClick(View v) {
            if (v != null) {
                int position = ((Integer) v.getTag()).intValue();
                Cursor cursor = HistoryCursorAdapter.this.getCursor();
                if (cursor.moveToPosition(position)) {
                    String zzOwner = TCH.getZzOwner(cursor);
                    String zzId = TCH.getZzId(cursor);
                    String zzName = TCH.getZzName(cursor);
                    String zzThumbnail = TCH.getThumbnail(cursor);
                    String zzPin = TCH.getPin(cursor);
                    ZzLocationQueue.getInstance().zzJoin(new GroupInvite(zzOwner, zzId, zzName, zzThumbnail, !TextUtils.isEmpty(zzPin), -1).getJoinParams(zzPin, true));
                    HistoryCursorAdapter.this.dismissPopupWindow();
                }
            }
        }
    };

    class ZzFolderLoaderTask implements Runnable, Comparable<ZzFolderLoaderTask> {
        private final Long mTs = Long.valueOf(System.currentTimeMillis());
        private final ViewPager mViewPager;
        private final String mZzId;
        private final String mZzOwner;

        public ZzFolderLoaderTask(ViewPager pager, String zz_id, String owner) {
            this.mViewPager = pager;
            this.mZzId = zz_id;
            this.mZzOwner = owner;
        }

        private boolean isViewUpToDate() {
            return this.mZzId.equals(this.mViewPager.getTag(R.id.KEY_ZZ_ID));
        }

        public void run() {
            if (isViewUpToDate()) {
                final ZzViewPagerAdapter imageAdapter = new ZzViewPagerAdapter(HistoryCursorAdapter.this.mContext, this.mZzId, this.mZzOwner, HistoryCursorAdapter.this.mWidth);
                HistoryCursorAdapter.this.mHandler.post(new Runnable() {
                    public void run() {
                        ZzFolderLoaderTask.this.onPostExecute(imageAdapter);
                    }
                });
            }
        }

        /* access modifiers changed from: private */
        public void onPostExecute(ZzViewPagerAdapter adapter) {
            if (isViewUpToDate()) {
                TextView countText = (TextView) this.mViewPager.getTag(R.id.KEY_COUNT_TEXT);
                if (adapter.getCount() == 0) {
                    HistoryCursorAdapter.this.mHistoryHandler.setZzEmpty(this.mZzOwner, this.mZzId);
                }
                countText.setText(HistoryCursorAdapter.this.mContext.getResources().getString(R.string.zap_count, new Object[]{Integer.valueOf(adapter.getCount())}));
                this.mViewPager.setAdapter(adapter);
            }
        }

        public int compareTo(ZzFolderLoaderTask another) {
            return another.mTs.compareTo(this.mTs);
        }
    }

    public HistoryCursorAdapter(Context context, Cursor cursor, int type, ProgressBarHelper progressBarHelper, ShareTimeHelper shareTimeHelper) {
        super(context, cursor, 0);
        this.mInflater = LayoutInflater.from(context);
        this.mContext = context;
        this.mProgressBarHelper = progressBarHelper;
        this.mShareTimeHelper = shareTimeHelper;
    }

    public Context getContext() {
        return this.mContext;
    }

    private String getPlaceString(Cursor cursor, int cardType) {
        getContext().getResources();
        switch (cardType) {
            case 0:
                String city = TCH.getCity(cursor);
                if (city == null) {
                    return this.mContext.getString(R.string.location_uncertain);
                }
                return city;
            default:
                String place = TCH.getZzName(cursor);
                if (place == null || place.isEmpty()) {
                    return TCH.getCity(cursor) == null ? this.mContext.getString(R.string.location_uncertain) : TCH.getCity(cursor);
                }
                return place;
        }
    }

    private String getDateString(Cursor cursor) {
        Resources res = getContext().getResources();
        long timestamp = TCH.getTimestamp(cursor);
        Date date = new Date(timestamp);
        Calendar cal = Calendar.getInstance();
        boolean isZZFolder = TCH.isZzSession(cursor) && !TCH.isActiveZz(cursor);
        String sendReceiveOn = TCH.isSendCard(cursor) ? res.getString(R.string.sent_text_on) : res.getString(R.string.rec_text_on);
        if (isZZFolder) {
            if (TCH.isUserZzOwner(cursor)) {
                sendReceiveOn = res.getString(R.string.zap_zone_create_text_on);
            } else {
                sendReceiveOn = res.getString(R.string.zap_zone_join_text_on);
            }
        }
        String sendReceiveAt = TCH.isSendCard(cursor) ? res.getString(R.string.sent_text_at) : res.getString(R.string.rec_text_at);
        if (isZZFolder) {
            if (TCH.isUserZzOwner(cursor)) {
                sendReceiveAt = res.getString(R.string.zap_zone_create_text_at);
            } else {
                sendReceiveAt = res.getString(R.string.zap_zone_join_text_at);
            }
        }
        int nowYear = cal.get(1);
        int nowDayOfYear = cal.get(6);
        int nowMonth = cal.get(2);
        cal.setTime(date);
        int thenYear = cal.get(1);
        int thenDayOfYear = cal.get(6);
        int thenMonth = cal.get(2);
        if (nowYear != thenYear) {
            return String.format(sendReceiveOn, new Object[]{formatDate(date, res.getString(R.string.history_year_date_format))});
        } else if (nowMonth != thenMonth) {
            return String.format(sendReceiveOn, new Object[]{formatDate(date, res.getString(R.string.history_month_date_format))});
        } else if (nowDayOfYear != thenDayOfYear) {
            return String.format(sendReceiveOn, new Object[]{DateUtils.formatDateTime(getContext(), date.getTime(), 65544)});
        } else {
            return String.format(sendReceiveAt, new Object[]{DateUtils.formatDateTime(this.mContext, timestamp, 1)});
        }
    }

    private String formatDate(Date date, String formatStr) {
        Locale locale = getContext().getResources().getConfiguration().locale;
        return new SimpleDateFormat(formatStr, locale).format(date).toUpperCase(locale);
    }

    public Cursor getItem(int position) {
        Cursor cursor = getCursor();
        if (cursor == null || cursor.isClosed() || !cursor.moveToPosition(position)) {
            return null;
        }
        return cursor;
    }

    /* access modifiers changed from: 0000 */
    public ViewGroup getZapInProgressLayout(ViewGroup parent, ViewGroup current, Cursor cursor, int cardType) {
        ViewGroup layout;
        if (current == null) {
            layout = (ViewGroup) this.mInflater.inflate(R.layout.zap_progress_card, null);
        } else {
            layout = current;
        }
        layout.setEnabled(false);
        TextView header = (TextView) layout.findViewById(R.id.zz_active_header);
        TextView lockDesc = (TextView) layout.findViewById(R.id.content_desc);
        if (TCH.isPinLocked(cursor)) {
            lockDesc.setVisibility(0);
            lockDesc.setText(TCH.getPin(cursor));
            lockDesc.setCompoundDrawablesWithIntrinsicBounds(this.mContext.getResources().getDrawable(R.drawable.ic_zap_notification_locked), null, null, null);
        } else {
            lockDesc.setVisibility(8);
        }
        header.setText(this.mContext.getString(R.string.notif_msg_catching));
        ImageView iv = (ImageView) layout.findViewById(R.id.zz_thumbnail);
        if (TCH.getThumbnail(cursor) != null) {
            byte[] thumbBytes = Base64.decode(TCH.getThumbnail(cursor).getBytes(), 0);
            iv.setImageBitmap(BitmapFactory.decodeByteArray(thumbBytes, 0, thumbBytes.length));
        } else {
            Log.e("HistoryAdapter", "Thumbnail received as null for id =" + TCH.getId(cursor));
        }
        ImageView contextMenu = (ImageView) layout.findViewById(R.id.context_menu);
        contextMenu.setTag(Integer.valueOf(cursor.getPosition()));
        contextMenu.setOnClickListener(this.mContextMenuListener);
        ProgressBar progressBar = (ProgressBar) layout.findViewById(R.id.progress);
        if (progressBar != null) {
            progressBar.setVisibility(8);
            this.mProgressBarHelper.remove(progressBar);
            setProgressBar(cursor, progressBar);
        }
        return layout;
    }

    /* access modifiers changed from: 0000 */
    public ViewGroup getCardLayout(ViewGroup parent, ViewGroup current, Cursor cursor, int cardType) {
        switch (cardType) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 6:
                return getMediaCardLayout(parent, current, cursor, cardType);
            case 4:
            case 5:
                return getZZInviteCardLayout(parent, current, cursor, cardType);
            case 7:
                return getZapInProgressLayout(parent, current, cursor, cardType);
            case 8:
                return getZzInviteRequestCardLayout(parent, current, cursor, cardType);
            default:
                return null;
        }
    }

    private ViewGroup getZzInviteRequestCardLayout(ViewGroup parent, ViewGroup current, Cursor cursor, int cardType) {
        ViewGroup layout;
        if (current == null) {
            layout = (ViewGroup) this.mInflater.inflate(R.layout.zz_invite_in_progress_card, null);
        } else {
            layout = current;
        }
        layout.setEnabled(false);
        ImageView thumb = (ImageView) layout.findViewById(R.id.zz_thumbnail);
        ((TextView) layout.findViewById(R.id.zz_name)).setText(TCH.getZzName(cursor));
        if (!TCH.getProfileVisiblity(cursor).booleanValue()) {
            String profilePicUrl = TCH.getProfilePic(cursor);
            ImageLoader imageLoader2 = ZapImageCache.getInstance().getImageLoader();
            if (!TextUtils.isEmpty(profilePicUrl)) {
                imageLoader2.get(profilePicUrl, ImageLoader.getImageListener(thumb, R.drawable.ic_zap_inviteaddphoto, R.drawable.ic_zap_inviteaddphoto));
            }
        } else {
            thumb.setImageResource(R.drawable.ic_zap_lockedzapzone);
        }
        int position = cursor.getPosition();
        Button acceptView = (Button) layout.findViewById(R.id.zz_accept);
        acceptView.setSelected(false);
        acceptView.setTag(Integer.valueOf(position));
        acceptView.setTag(R.id.KEY_LAYOUT, layout);
        acceptView.setOnClickListener(this.mZzInviteClickListener);
        EditText et = (EditText) layout.findViewById(R.id.zz_progress_code);
        if (!TCH.isPinLocked(cursor) || et.getVisibility() != 0) {
            layout.findViewById(R.id.lockedview).setVisibility(8);
            layout.findViewById(R.id.zz_pin_entry).setVisibility(8);
        } else {
            et.requestFocus();
        }
        Button rejectView = (Button) layout.findViewById(R.id.zz_reject);
        rejectView.setSelected(false);
        rejectView.setTag(Integer.valueOf(cursor.getPosition()));
        rejectView.setTag(R.id.KEY_LAYOUT, layout);
        rejectView.setOnClickListener(this.mZzInviteClickListener);
        View rejImgview = layout.findViewById(R.id.zz_img_reject);
        rejImgview.setSelected(false);
        rejImgview.setTag(Integer.valueOf(cursor.getPosition()));
        rejImgview.setTag(R.id.KEY_LAYOUT, layout);
        rejImgview.setOnClickListener(this.mZzInviteClickListener);
        return layout;
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        dismissPopupWindow();
    }

    /* access modifiers changed from: 0000 */
    public ViewGroup getZZInviteCardLayout(ViewGroup parent, ViewGroup current, Cursor cursor, int cardType) {
        ViewGroup layout;
        if (current == null) {
            layout = (ViewGroup) this.mInflater.inflate(R.layout.zz_invite_card, null);
        } else {
            layout = current;
        }
        TextView header = (TextView) layout.findViewById(R.id.zz_active_header);
        TextView title = (TextView) layout.findViewById(R.id.zz_title);
        TextView ts = (TextView) layout.findViewById(R.id.zz_timestamp);
        String zzName = TCH.getZzName(cursor);
        ImageView thumb = (ImageView) layout.findViewById(R.id.zz_thumbnail);
        GroupInfo zzInfo = GCSP.getGroupManager().getGroupInfo(null);
        if (!ZzUtil.isUserOwner(zzInfo)) {
            title.setText(zzName);
        } else if (!zzInfo.isActive || TextUtils.isEmpty(zzInfo.pin)) {
            title.setText(zzName);
        } else {
            title.setText(zzName + " Code# " + zzInfo.pin);
        }
        String _thumbnail = TCH.getThumbnail(cursor);
        ts.setText(DateUtils.formatDateTime(this.mContext, TCH.getTimestamp(cursor), 1));
        if (_thumbnail != null) {
            byte[] thumbBytes = Base64.decode(_thumbnail.getBytes(), 0);
            thumb.setImageBitmap(BitmapFactory.decodeByteArray(thumbBytes, 0, thumbBytes.length));
        }
        switch (cardType) {
            case 4:
                header.setText(R.string.zz_invite_recv);
                layout.setEnabled(false);
                break;
            default:
                header.setText(R.string.zz_invite_sent);
                layout.setEnabled(true);
                break;
        }
        return layout;
    }

    public void setAnimators(ListView listView, DragZapAnimator dragZapAnimator, ZapGestureListener zapgestureListener) {
        this.mDragZapAnimator = dragZapAnimator;
        this.mListView = listView;
        this.mGestureDetector = zapgestureListener;
    }

    private void setProfileData(ViewGroup layout, Profile aProfile, boolean isSample) {
        TextView profileNameView = (TextView) layout.findViewById(R.id.profile_name);
        profileNameView.setVisibility(0);
        NetworkImageView profilePicView = (NetworkImageView) layout.findViewById(R.id.profile_pic);
        profilePicView.setVisibility(0);
        profilePicView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
            }
        });
        if (isSample) {
            profileNameView.setVisibility(4);
            profilePicView.setVisibility(4);
        } else if (aProfile != null && aProfile.anonymous) {
            profilePicView.setImageResource(R.drawable.ic_default_profile);
            profileNameView.setText(R.string.prof_anonymous);
        } else if (aProfile == null || aProfile.pictureUrl == null) {
            profilePicView.setImageResource(R.drawable.ic_default_profile);
            profileNameView.setText(R.string.prof_you);
        } else {
            profilePicView.setImageUrl(aProfile.pictureUrl, this.imageLoader);
            profilePicView.setDefaultImageResId(R.drawable.ic_default_profile);
            profilePicView.setErrorImageResId(R.drawable.ic_default_profile);
            profileNameView.setText(aProfile.nickname);
        }
    }

    private void setZapZoneCardProfileData(ViewGroup layout, Cursor cursor) {
        TextView profileNameView = (TextView) layout.findViewById(R.id.profile_name);
        NetworkImageView profilePicView = (NetworkImageView) layout.findViewById(R.id.profile_pic);
        String profileName = TCH.getProfileName(cursor);
        boolean anonymous = TCH.getProfileVisiblity(cursor).booleanValue();
        String profileUrl = TCH.getProfilePic(cursor);
        if (anonymous || profileName == null || profileUrl == null) {
            profilePicView.setImageResource(R.drawable.ic_default_profile);
            profileNameView.setText(R.string.prof_anonymous);
        } else {
            profileNameView.setText(profileName);
            profilePicView.setImageUrl(profileUrl, this.imageLoader);
            profilePicView.setDefaultImageResId(R.drawable.ic_default_profile);
            profilePicView.setErrorImageResId(R.drawable.ic_default_profile);
        }
        ((ImageView) layout.findViewById(R.id.zap_kind)).setImageResource(R.drawable.ic_drawer_zone);
    }

    private void setZapCardProfileData(ViewGroup layout, Cursor cursor) {
        String profileUrl = TCH.getProfilePic(cursor);
        String nickname = TCH.getProfileName(cursor);
        String userid = TCH.getProfileUserId(cursor);
        boolean anonymous = TCH.getProfileVisiblity(cursor).booleanValue();
        boolean hasProfiledata = !TextUtils.isEmpty(profileUrl) || !TextUtils.isEmpty(nickname);
        Profile currentUserProfile = ZapProfileSharedPerfs.getProfile();
        String currentUserId = currentUserProfile != null ? currentUserProfile.userId : null;
        String city = TCH.getCity(cursor);
        if (city != null && city.equals(App.getAppContext().getString(R.string.sample_zap))) {
            Profile profile = new Profile();
            profile.pictureUrl = profileUrl;
            profile.nickname = nickname;
            setProfileData(layout, profile, true);
        } else if ((userid != null && !userid.equals(currentUserId) && anonymous) || !hasProfiledata) {
            Profile lProfile = new Profile();
            if (!hasProfiledata) {
                lProfile.anonymous = true;
            } else {
                lProfile.anonymous = anonymous;
            }
            setProfileData(layout, lProfile, false);
        } else if (profileUrl == null || profileUrl.length() <= 0 || nickname == null || nickname.length() <= 0) {
            Profile lProfile2 = new Profile();
            lProfile2.nickname = getContext().getString(R.string.prof_anonymous);
            setProfileData(layout, lProfile2, false);
        } else {
            Profile lProfile3 = new Profile();
            lProfile3.pictureUrl = profileUrl;
            if (TextUtils.equals(userid, currentUserId)) {
                lProfile3.nickname = getContext().getString(R.string.prof_you);
            } else {
                lProfile3.nickname = nickname;
            }
            setProfileData(layout, lProfile3, false);
        }
        ImageView zapType = (ImageView) layout.findViewById(R.id.zap_kind);
        int mediaType = TCH.getMediaType(cursor);
        if (mediaType == 1) {
            zapType.setImageResource(R.drawable.ic_drawer_photos);
        } else if (mediaType == 2) {
            zapType.setImageResource(R.drawable.ic_drawer_videos);
        }
    }

    /* access modifiers changed from: 0000 */
    public ViewGroup getMediaCardLayout(ViewGroup viewGroup, ViewGroup viewGroup2, Cursor cursor, int i) {
        boolean z;
        String string;
        if (viewGroup2 == null) {
            if (i == 2 || i == 3) {
                viewGroup2 = (ViewGroup) this.mInflater.inflate(R.layout.zap_cardstack, null);
            } else {
                viewGroup2 = (ViewGroup) this.mInflater.inflate(R.layout.zap_card, null);
            }
        }
        ImageView imageView = (ImageView) viewGroup2.findViewById(R.id.context_menu);
        TextView textView = (TextView) viewGroup2.findViewById(R.id.activity_googlecards_card_textview);
        TextView textView2 = (TextView) viewGroup2.findViewById(R.id.activity_googlecards_card_date);
        TextView textView3 = (TextView) viewGroup2.findViewById(R.id.timer);
        View findViewById = viewGroup2.findViewById(R.id.swipe_up_tutorial);
        if (findViewById != null) {
            findViewById.setVisibility(4);
        }
        switch (i) {
            case 0:
            case 1:
            case 6:
                viewGroup2.setBackgroundResource(R.drawable.ic_zap_card);
                viewGroup2.setEnabled(true);
                setZapCardProfileData(viewGroup2, cursor);
                break;
            case 2:
            case 3:
                viewGroup2.setBackgroundResource(R.drawable.ic_zap_card);
                viewGroup2.setEnabled(false);
                TextView textView4 = (TextView) viewGroup2.findViewById(R.id.zap_count);
                CustomViewPager customViewPager = (CustomViewPager) viewGroup2.findViewById(R.id.viewpager);
                String zzId = TCH.getZzId(cursor);
                String zzOwner = TCH.getZzOwner(cursor);
                if (customViewPager.getTag(R.id.KEY_ZZ_ID) == null || !((String) customViewPager.getTag(R.id.KEY_ZZ_ID)).equals(zzId)) {
                    z = true;
                } else {
                    z = false;
                }
                if (z) {
                    customViewPager.setTag(R.id.KEY_ZZ_OWNER, zzOwner);
                    customViewPager.setTag(R.id.KEY_ZZ_ID, zzId);
                    customViewPager.setTag(R.id.KEY_COUNT_TEXT, textView4);
                    customViewPager.setOnClickListener(this.mViewPagerClickListener);
                    customViewPager.setAnimators(this.mListView, this.mDragZapAnimator, this.mGestureDetector);
                    this.mZzFolderExecutor.execute(new ZzFolderLoaderTask(customViewPager, zzId, zzOwner));
                }
                setZapZoneCardProfileData(viewGroup2, cursor);
                break;
        }
        if (imageView != null) {
            imageView.setEnabled(true);
            imageView.setTag(Integer.valueOf(cursor.getPosition()));
            imageView.setOnClickListener(this.mContextMenuListener);
        }
        resetDataViewTags(textView2);
        fillLockStatusInfo(cursor, viewGroup2);
        if (textView3 != null) {
            this.mShareTimeHelper.remove(textView3);
            textView3.setVisibility(4);
        }
        String filePath = TCH.getFilePath(cursor);
        if (TCH.isUploadInProgress(cursor)) {
            viewGroup2.setEnabled(false);
            if (imageView != null) {
                imageView.setImageResource(R.drawable.ic_zap_cancel);
            }
            textView.setText(this.mContext.getString(R.string.notif_msg_sending));
            String mimeTypeFromFileName = FileUtils.getMimeTypeFromFileName(filePath);
            if (filePath != null) {
                if (FileUtils.isVideo(mimeTypeFromFileName)) {
                    string = this.mContext.getString(R.string.sending_video);
                } else {
                    string = this.mContext.getString(R.string.sending_photo);
                }
                textView.setText(string);
            }
        } else {
            if (imageView != null) {
                imageView.setImageResource(R.drawable.ic_zap_overflow);
            }
            long mediaExpirationTimestamp = TCH.getMediaExpirationTimestamp(cursor);
            int transferCompleteStatus = TCH.getTransferCompleteStatus(cursor);
            if (GCSP.getGroupManager().isAnyGroupActive() || transferCompleteStatus != 0 || mediaExpirationTimestamp - System.currentTimeMillis() <= 0) {
                textView2.setVisibility(0);
                if (TCH.isDemoCard(cursor)) {
                    textView.setText(R.string.sample_zap);
                } else {
                    textView.setText(getPlaceString(cursor, i));
                }
                textView2.setText(getDateString(cursor));
            } else {
                textView3.setVisibility(0);
                textView3.setTag(R.id.KEY_EXPIRY, Long.valueOf(mediaExpirationTimestamp));
                textView3.setTag(R.id.KEY_LAYOUT, viewGroup2);
                if (TCH.isDemoCard(cursor)) {
                    textView3.setTag(R.id.KEY_PLACE, this.mContext.getString(R.string.sample_zap));
                } else {
                    textView3.setTag(R.id.KEY_PLACE, getPlaceString(cursor, i));
                }
                textView3.setTag(R.id.KEY_DATE, getDateString(cursor));
                this.mShareTimeHelper.put(textView3);
                textView3.setCompoundDrawablesWithIntrinsicBounds(this.mContext.getResources().getDrawable(R.drawable.ic_zap_timer), null, null, null);
                imageView.setImageResource(R.drawable.ic_zap_cancel);
                textView.setText(this.mContext.getText(R.string.sent));
                String mimeTypeFromFileName2 = FileUtils.getMimeTypeFromFileName(filePath);
                if (filePath != null) {
                    textView.setText(FileUtils.isVideo(mimeTypeFromFileName2) ? this.mContext.getString(R.string.sent_video) : this.mContext.getString(R.string.sent_photo));
                }
                fillLockInformation(cursor, viewGroup2);
            }
        }
        ImageView imageView2 = (ImageView) viewGroup2.findViewById(R.id.activity_googlecards_card_imageview);
        ProgressBar progressBar = (ProgressBar) viewGroup2.findViewById(R.id.progress);
        if (progressBar != null) {
            progressBar.setVisibility(8);
            this.mProgressBarHelper.remove(progressBar);
        }
        if (filePath == null || !isSdCardMounted() || FileUtils.isFileExists(filePath)) {
            if (progressBar != null) {
                setProgressBar(cursor, progressBar);
            }
            if (imageView2 != null) {
                setImage(imageView2, TCH.getFilePath(cursor), this.mWidth, cursor);
            }
        } else {
            if (imageView2 != null) {
                imageView2.setImageResource(R.drawable.ic_missingimage);
            }
            if (!(i == 2 || i == 3)) {
                viewGroup2.setEnabled(false);
                this.mHistoryHandler.setDeleted((long) TCH.getId(cursor));
            }
        }
        populatePlayIcon(viewGroup2, filePath);
        return viewGroup2;
    }

    /* access modifiers changed from: 0000 */
    public void populatePlayIcon(ViewGroup viewGroup, String str) {
        String mimeTypeFromFileName = FileUtils.getMimeTypeFromFileName(str);
        ImageView imageView = (ImageView) viewGroup.findViewById(R.id.activity_googlecards_card_playicon);
        if (imageView == null) {
            return;
        }
        if (FileUtils.isVideo(mimeTypeFromFileName)) {
            imageView.setVisibility(0);
        } else {
            imageView.setVisibility(4);
        }
    }

    private void resetDataViewTags(TextView textView) {
        if (textView != null) {
            textView.setTag(R.id.KEY_EXPIRY, null);
            textView.setTag(R.id.KEY_PIN, null);
            textView.setTag(R.id.KEY_LAYOUT, null);
            textView.setTag(R.id.KEY_PLACE, null);
            textView.setTag(R.id.KEY_DATE, null);
            textView.setCompoundDrawables(null, null, null, null);
        }
    }

    private void fillLockStatusInfo(Cursor cursor, ViewGroup layout) {
        if (TCH.isUploadInProgress(cursor)) {
            fillLockInformation(cursor, layout);
        }
    }

    private void fillLockInformation(Cursor cursor, ViewGroup viewGroup) {
        TextView textView = (TextView) viewGroup.findViewById(R.id.activity_googlecards_card_date);
        if (textView != null) {
            if (TCH.isPinLocked(cursor)) {
                textView.setVisibility(0);
                textView.setText(TCH.getPin(cursor));
                textView.setCompoundDrawablesWithIntrinsicBounds(this.mContext.getResources().getDrawable(R.drawable.ic_zap_notification_locked), null, null, null);
                return;
            }
            textView.setVisibility(GCSP.getGroupManager().isAnyGroupActive() ? 4 : 8);
            textView.setText(this.mContext.getString(R.string.notif_msg_unlocked));
            textView.setCompoundDrawablesWithIntrinsicBounds(this.mContext.getResources().getDrawable(R.drawable.ic_zap_notification_unlocked), null, null, null);
        }
    }

    private void setProgressBar(Cursor cursor, ProgressBar progressBar) {
        int progressPct;
        if (TCH.isTransferInProgress(cursor) && !TCH.isZzSession(cursor)) {
            long uid = TCH.getEffectiveUid(cursor);
            ContentContainer trans = ZapTracker.getInstance().get(uid);
            if (trans != null) {
                this.mProgressBarHelper.put(uid, progressBar);
                progressBar.setVisibility(0);
                TransferStatus transferStatus = trans.getTransferStatus();
                if (transferStatus != null) {
                    progressPct = transferStatus.progress();
                } else {
                    progressPct = 0;
                }
                if (progressPct > 0) {
                    progressBar.setIndeterminate(false);
                    progressBar.setMax(100);
                    progressBar.setProgress(progressPct);
                    return;
                }
                progressBar.setIndeterminate(true);
            }
        }
    }

    public boolean isSdCardMounted() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    private void setImage(ImageView view, String filePath, int width, Cursor cursor) {
        view.setImageDrawable(null);
        view.setTag(filePath);
        if (TCH.isDownloadInProgress(cursor)) {
            byte[] thumbBytes = Base64.decode(TCH.getThumbnail(cursor).getBytes(), 0);
            view.setImageBitmap(BitmapFactory.decodeByteArray(thumbBytes, 0, thumbBytes.length));
        } else if (filePath != null) {
            Bitmap bitmap = BitmapCache.getInstance().get(filePath);
            if (bitmap == null) {
                this.mExecutor.execute(new BitmapWorkerTask(this.mHandler, view, width, ZapUtils.dipToPix(this.mContext, 200), filePath));
                return;
            }
            view.setImageBitmap(bitmap);
        }
    }

    public void bindView(View view, Context context, Cursor cursor) {
        if (DBG) {
            Log.d("HistoryAdapter", "newView called at " + cursor.getPosition());
        }
        getCardLayout(null, (ViewGroup) view, cursor, ZapHistoryInfo.getCardType(cursor));
    }

    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        if (DBG) {
            Log.d("HistoryAdapter", "newView called at " + cursor.getPosition());
        }
        if (viewGroup != null) {
            this.mWidth = viewGroup.getWidth();
        }
        return getCardLayout(viewGroup, null, cursor, ZapHistoryInfo.getCardType(cursor));
    }

    public int getViewTypeCount() {
        return 5;
    }

    public int getItemViewType(int position) {
        Cursor cursor = getCursor();
        cursor.moveToPosition(position);
        switch (ZapHistoryInfo.getCardType(cursor)) {
            case 0:
            case 1:
            case 6:
                return 0;
            case 2:
            case 3:
                return 2;
            case 4:
            case 5:
                return 1;
            case 7:
                return 3;
            case 8:
                return 4;
            default:
                return -1;
        }
    }

    public void onMovedToScrapHeap(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            TextView tv = (TextView) viewGroup.findViewById(R.id.timer);
            if (tv != null) {
                this.mShareTimeHelper.remove(tv);
            }
            ProgressBar progressBar = (ProgressBar) viewGroup.findViewById(R.id.progress);
            if (progressBar != null) {
                this.mProgressBarHelper.remove(progressBar);
            }
            int childcount = viewGroup.getChildCount();
            for (int i = 0; i < childcount; i++) {
                View v = viewGroup.getChildAt(i);
                if (v instanceof ImageView) {
                    ((ImageView) v).setImageBitmap(null);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void initiatePopupWindow(View view, Cursor cursor) {
        View inflate;
        boolean z = false;
        try {
            boolean z2 = TCH.isZzSession(cursor) && !TCH.isActiveZz(cursor);
            if (z2) {
                inflate = this.mInflater.inflate(R.layout.zap_folder_overflow_menu, null);
                this.mWindow = new PopupWindow(inflate, ZapUtils.dipToPix(this.mContext, 140), -2, true);
            } else {
                inflate = this.mInflater.inflate(R.layout.zap_card_overflow_menu, null);
                this.mWindow = new PopupWindow(inflate, ZapUtils.dipToPix(this.mContext, 140), -2, true);
            }
            this.mWindow.setTouchable(true);
            this.mWindow.setFocusable(true);
            this.mWindow.setOutsideTouchable(true);
            this.mWindow.setBackgroundDrawable(new BitmapDrawable());
            String filePath = TCH.getFilePath(cursor);
            boolean isSendCard = TCH.isSendCard(cursor);
            boolean isDemoCard = TCH.isDemoCard(cursor);
            if (z2) {
                View findViewById = inflate.findViewById(R.id.pop_up_menu_delete_all);
                View findViewById2 = inflate.findViewById(R.id.zz_rejoin);
                if (TCH.isUserZzOwner(cursor) || !TCH.isLeftZz(cursor) || GCSP.getGroupManager().isAnyGroupActive()) {
                    findViewById2.setVisibility(8);
                } else {
                    findViewById2.setTag(Integer.valueOf(cursor.getPosition()));
                    findViewById2.setOnClickListener(this.mZzRejoinListener);
                }
                findViewById.setTag(Integer.valueOf(cursor.getPosition()));
                findViewById.setOnClickListener(this.mDeleteListener);
            } else {
                View findViewById3 = inflate.findViewById(R.id.pop_up_menu_report);
                boolean abuseReported = TCH.getAbuseReported(cursor);
                View findViewById4 = inflate.findViewById(R.id.pop_up_menu_delete);
                findViewById4.setTag(Integer.valueOf(cursor.getPosition()));
                findViewById4.setOnClickListener(this.mDeleteListener);
                View findViewById5 = inflate.findViewById(R.id.pop_up_menu_share);
                findViewById5.setTag(Integer.valueOf(cursor.getPosition()));
                findViewById5.setOnClickListener(this.mShareListener);
                View findViewById6 = inflate.findViewById(R.id.pop_up_menu_zapit);
                findViewById6.setTag(Integer.valueOf(cursor.getPosition()));
                findViewById6.setOnClickListener(this.mZapitListener);
                if (isDemoCard) {
                    findViewById5.setVisibility(8);
                }
                if (abuseReported || !FileUtils.isFileExists(filePath) || isSendCard || isDemoCard || z2) {
                    findViewById3.setVisibility(8);
                } else {
                    findViewById3.setTag(Integer.valueOf(cursor.getPosition()));
                    findViewById3.setOnClickListener(this.mReportAbuseListener);
                    z = true;
                }
            }
            int[] iArr = new int[2];
            view.getLocationOnScreen(iArr);
            int i = (z2 || isDemoCard) ? 35 : z ? 110 : 70;
            this.mWindow.showAtLocation(view, 0, iArr[0] - ZapUtils.dipToPix(this.mContext, 120), iArr[1] - ZapUtils.dipToPix(this.mContext, i));
        } catch (Exception e) {
            Log.e("HistoryAdapter", "Exception while showing popup window");
        }
    }

    /* access modifiers changed from: private */
    public void launchShareIntents(String str, int i) {
        Uri videoContentUri;
        ArrayList arrayList = new ArrayList();
        PackageManager packageManager = getContext().getPackageManager();
        Intent intent = new Intent("android.intent.action.SEND");
        if (i == 1) {
            intent.setType("image/*");
        } else {
            intent.setType("video/*");
        }
        if (i == 1) {
            videoContentUri = FileUtils.getImageContentUri(this.mContext, str);
        } else {
            videoContentUri = FileUtils.getVideoContentUri(this.mContext, str);
        }
        for (ResolveInfo resolveInfo : packageManager.queryIntentActivities(intent, 0)) {
            String str2 = resolveInfo.activityInfo.packageName;
            if (str2.contains("twitter") || str2.contains("facebook") || str2.contains("whatsapp") || str2.contains("google")) {
                Intent intent2 = new Intent();
                intent2.setComponent(new ComponentName(str2, resolveInfo.activityInfo.name));
                intent2.setAction("android.intent.action.SEND");
                intent2.putExtra("android.intent.extra.STREAM", videoContentUri);
                if (i == 1) {
                    intent2.setType("image/*");
                } else {
                    intent2.setType("video/*");
                }
                arrayList.add(new LabeledIntent(intent2, str2, resolveInfo.loadLabel(packageManager), resolveInfo.icon));
            }
        }
        if (arrayList != null || arrayList.size() != 0) {
            Intent createChooser = Intent.createChooser((Intent) arrayList.remove(0), this.mContext.getString(R.string.share_this_zap));
            createChooser.putExtra("android.intent.extra.INITIAL_INTENTS", (LabeledIntent[]) arrayList.toArray(new LabeledIntent[arrayList.size()]));
            getContext().startActivity(createChooser);
        }
    }

    public void dismissPopupWindow() {
        if (this.mWindow != null && this.mWindow.isShowing()) {
            this.mWindow.dismiss();
        }
    }

    /* access modifiers changed from: private */
    public static void cancelDownload(Context context, ContentContainer thisBlast) {
        GCSP.getContentDispatcher().cancelDownload(thisBlast);
    }

    /* access modifiers changed from: private */
    public void cancelUpload(Context context, long j) {
        String shareId;
        ContentContainer contentContainer = ZapTracker.getInstance().get(j);
        if (contentContainer != null) {
            shareId = contentContainer.getShareId();
        } else {
            shareId = this.mHistoryHandler.getShareId(j);
        }
        if (shareId == null) {
            Log.e("HistoryAdapter", String.format("Could not get shareId for uid %d", new Object[]{Long.valueOf(j)}));
            return;
        }
        Intent intent = new Intent(ZapConstants.ACTION_CANCEL_UPLOAD);
        intent.setClass(context.getApplicationContext(), ActionReceiver.class);
        intent.putExtra(ZapConstants.EXTRA_TRANSACTION_UID, j);
        intent.putExtra(ZapConstants.EXTRA_SERVER_SHARE_ID, shareId);
        intent.putExtra(ZapConstants.EXTRA_CANCEL_USER_VISIBLE, true);
        context.sendBroadcast(intent);
    }

    /* access modifiers changed from: private */
    public void joinZone(View view, int i) {
        view.findViewById(R.id.lockedview).setVisibility(8);
        view.findViewById(R.id.zz_join_consent).setVisibility(0);
        view.findViewById(R.id.zz_pin_entry).setVisibility(8);
        view.findViewById(R.id.zz_name_header).setVisibility(0);
        ZzLocationQueue.getInstance().zzJoin(ZzInviteStore.get(i).getJoinParams(null, false));
    }

    /* access modifiers changed from: private */
    public void joinZone(View view, int i, String str) {
        view.findViewById(R.id.lockedview).setVisibility(8);
        view.findViewById(R.id.zz_join_consent).setVisibility(0);
        view.findViewById(R.id.zz_pin_entry).setVisibility(8);
        view.findViewById(R.id.zz_name_header).setVisibility(0);
        ZzLocationQueue.getInstance().zzJoin(ZzInviteStore.get(i).getJoinParams(str, false));
    }

    /* access modifiers changed from: private */
    public void leaveZone(View view, final int i) {
        view.findViewById(R.id.lockedview).setVisibility(8);
        view.findViewById(R.id.zz_join_consent).setVisibility(0);
        view.findViewById(R.id.zz_pin_entry).setVisibility(8);
        view.findViewById(R.id.zz_name_header).setVisibility(0);
        new Thread() {
            public void run() {
                ZzUtil.ignoreZoneInvite(ZapUi.getInstance(), i);
            }
        }.start();
    }

    public int getCount() {
        if (FileUtils.isSdcardMounted()) {
            return super.getCount();
        }
        Log.e("HistoryAdapter", "SDCARD NOT MOUNTED");
        return 0;
    }

    /* access modifiers changed from: 0000 */
    public void confirmDeleteZapZone(Context context, final String str, final String str2, String str3) {
        Builder builder = new Builder(context);
        builder.setMessage(String.format(context.getString(R.string.delete_zap_zone), new Object[]{str3}));
        builder.setPositiveButton(context.getString(R.string.dialog_yes_button), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                new HistoryHandler().deleteZzSession(str2, str);
            }
        });
        builder.setNegativeButton(context.getString(R.string.dialog_no_button), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setOnCancelListener(new OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                if (HistoryCursorAdapter.DBG) {
                    Log.d("HistoryAdapter", "Dialog - onCancel");
                }
            }
        });
        AlertDialog create = builder.create();
        create.setCanceledOnTouchOutside(true);
        create.show();
    }
}
