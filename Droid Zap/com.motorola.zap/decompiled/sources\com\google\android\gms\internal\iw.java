package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.internal.ir.b.C0044b;
import java.util.HashSet;
import java.util.Set;

public class iw implements Creator<C0044b> {
    static void a(C0044b bVar, Parcel parcel, int i) {
        int p = b.p(parcel);
        Set hB = bVar.hB();
        if (hB.contains(Integer.valueOf(1))) {
            b.c(parcel, 1, bVar.getVersionCode());
        }
        if (hB.contains(Integer.valueOf(2))) {
            b.c(parcel, 2, bVar.getHeight());
        }
        if (hB.contains(Integer.valueOf(3))) {
            b.a(parcel, 3, bVar.getUrl(), true);
        }
        if (hB.contains(Integer.valueOf(4))) {
            b.c(parcel, 4, bVar.getWidth());
        }
        b.D(parcel, p);
    }

    /* renamed from: aM */
    public C0044b createFromParcel(Parcel parcel) {
        int i = 0;
        int o = a.o(parcel);
        HashSet hashSet = new HashSet();
        String str = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    i3 = a.g(parcel, n);
                    hashSet.add(Integer.valueOf(1));
                    break;
                case 2:
                    i2 = a.g(parcel, n);
                    hashSet.add(Integer.valueOf(2));
                    break;
                case 3:
                    str = a.m(parcel, n);
                    hashSet.add(Integer.valueOf(3));
                    break;
                case 4:
                    i = a.g(parcel, n);
                    hashSet.add(Integer.valueOf(4));
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new C0044b(hashSet, i3, i2, str, i);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }

    /* renamed from: bJ */
    public C0044b[] newArray(int i) {
        return new C0044b[i];
    }
}
