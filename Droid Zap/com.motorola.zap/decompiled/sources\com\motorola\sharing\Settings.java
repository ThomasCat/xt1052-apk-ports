package com.motorola.sharing;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.motorola.sharing.util.Logger.LogLevel;
import com.motorola.sharing.util.SharedPreference;
import com.motorola.sharing.util.SharedPreference.Verificator;

public class Settings {
    public static final SharedPreference<String> CHROMECAST_DOWNLOADS_DIRECTORY = new SharedPreference<>(Config.CHROMECAST_DOWNLOADS_DIRECTORY, String.class, Defaults.CHROMECAST_DOWNLOADS_DIRECTORY, SharedPreference.stringHandler, SharedPreference.emptyTextVerificator);
    public static final SharedPreference<String> DOWNLOADS_DIRECTORY = new SharedPreference<>(Config.DOWNLOADS_DIRECTORY, String.class, Defaults.DOWNLOADS_DIRECTORY, SharedPreference.stringHandler, SharedPreference.emptyTextVerificator);
    public static final SharedPreference<Boolean> GOOGLE_ANALYTICS_ENABLED = new SharedPreference<>(Config.GOOGLE_ANALYTICS_ENABLED, Boolean.class, Boolean.valueOf(false), SharedPreference.booleanHandler, SharedPreference.nullVerificator);
    public static final SharedPreference<String> GOOGLE_ANALYTICS_ID = new SharedPreference<>(Config.GOOGLE_ANALYTICS_ID, String.class, null, SharedPreference.stringHandler, SharedPreference.emptyTextVerificator);
    public static final SharedPreference<Long> GROUP_INVITE_TTL = new SharedPreference<>(Config.GROUP_INVITE_TTL, Long.class, Long.valueOf(120000), SharedPreference.longHandler, SharedPreference.positiveValueVerificator);
    public static final SharedPreference<Integer> JPEG_REDUCTION_MAX_SIDE_SIZE = new SharedPreference<>(Config.JPEG_REDUCTION_MAX_SIDE_SIZE, Integer.class, Integer.valueOf(1400), SharedPreference.integerHandler, SharedPreference.positiveValueVerificator);
    public static final SharedPreference<Float> JPEG_REDUCTION_SIZE_FACTOR = new SharedPreference<>(Config.JPEG_REDUCTION_SIZE_FACTOR, Float.class, Float.valueOf(0.75f), SharedPreference.floatHandler, new Verificator() {
        public boolean isValid(Object value) {
            if (value == null) {
                return false;
            }
            float floatValue = ((Float) value).floatValue();
            if (floatValue <= BitmapDescriptorFactory.HUE_RED || floatValue >= 1.0f) {
                return false;
            }
            return true;
        }
    });
    public static final SharedPreference<LogLevel> LOG_LEVEL = new SharedPreference<>(Config.LOG_LEVEL, LogLevel.class, Defaults.LOG_LEVEL, SharedPreference.logLevelHandler, SharedPreference.nullVerificator);
    public static final SharedPreference<Boolean> MOTO_CHECKIN_ENABLED = new SharedPreference<>(Config.MOTO_CHECKIN_ENABLED, Boolean.class, Boolean.valueOf(false), SharedPreference.booleanHandler, SharedPreference.nullVerificator);
    public static final SharedPreference<String> MOTO_CHECKIN_ID = new SharedPreference<>(Config.MOTO_CHECKIN_ID, String.class, null, SharedPreference.stringHandler, SharedPreference.emptyTextVerificator);
}
