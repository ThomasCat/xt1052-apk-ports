package com.motorola.sharing.provider.cloud;

public class CloudDefaults {
    public static final boolean DISCOVER_SELF_SHARED_CONTENT = false;
    public static final String SERVER_HOST = "gcsp4prod.appspot.com";
    public static final int SETTINGS_VERSION = 1;
    public static final boolean USE_HTTPS_FOR_REQUESTS = true;
    public static final boolean WIFI_ONLY_CONTENT_TRANSFER = false;
}
