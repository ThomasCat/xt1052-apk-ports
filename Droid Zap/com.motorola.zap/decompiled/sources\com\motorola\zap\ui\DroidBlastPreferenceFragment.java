package com.motorola.zap.ui;

import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.os.Handler;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.text.TextUtils;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import com.google.android.gms.drive.DriveFile;
import com.motorola.sharing.Config;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupManager;
import com.motorola.sharing.ProfileManager;
import com.motorola.sharing.ProfileManagerListener;
import com.motorola.sharing.ProfileManagerListener.ProfileEvent;
import com.motorola.sharing.ProfileManagerListener.ProfileEvent.Status;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConfig;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.app.App;
import com.motorola.zap.app.ZapTracker;
import com.motorola.zap.dialogs.DialogMessageActivity;
import com.motorola.zap.identity.ZapProfileSharedPerfs;
import com.motorola.zap.location.ModalityHelper;
import com.motorola.zap.ui.DroidBlastDeleteProfileDialogFragment.DeleteProfileDialogListener;
import com.motorola.zap.ui.DroidBlastPrivacyDialogFragment.PrivacyDialogListener;

public class DroidBlastPreferenceFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener, OnPreferenceChangeListener, OnCheckedChangeListener, DeleteProfileDialogListener, PrivacyDialogListener {
    private CheckBoxPreference mAgreeTermCondition;
    private Switch mFeatureSwitch;
    private ListPreference mReceiveSelfShare;
    private EditTextPreference mServerHost;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName(ZapConstants.PREFERENCES_FILE_NAME);
        addPreferencesFromResource(R.xml.droid_blast_settings);
        this.mFeatureSwitch = new Switch(getActivity());
        this.mFeatureSwitch.setChecked(((Boolean) Z.FEATURE_ENABLED.read()).booleanValue());
        this.mFeatureSwitch.setOnCheckedChangeListener(this);
        ActionBar actionBar = getActivity().getActionBar();
        actionBar.setDisplayOptions(16, 16);
        actionBar.setCustomView(this.mFeatureSwitch, new LayoutParams(-2, -2, 8388629));
        PreferenceScreen screen = getPreferenceScreen();
        if (screen != null) {
            if (!ZapConfig.isGoogleAnalyticsSupported()) {
                screen.removePreference((CheckBoxPreference) findPreference(Z.SEND_GOOGLE_ANALYTICS_REPORTS.name));
            }
            Preference prototypeSettings = findPreference("prototype");
            if (LogUtils.IS_DEBUGGABLE || ZapConfig.hasMotoAccount()) {
                this.mReceiveSelfShare = (ListPreference) findPreference("receiveSelfShare");
                this.mReceiveSelfShare.setOnPreferenceChangeListener(this);
                this.mServerHost = (EditTextPreference) findPreference("serverHost");
                this.mServerHost.setOnPreferenceChangeListener(this);
            } else {
                screen.removePreference(prototypeSettings);
            }
            Preference zapScreen = findPreference("zapScreenshot");
            if (!ZapConfig.supportsZaptoscreen()) {
                screen.removePreference(zapScreen);
            }
            this.mAgreeTermCondition = (CheckBoxPreference) findPreference("privacy_agreement");
            if (this.mAgreeTermCondition != null) {
                this.mAgreeTermCondition.setOnPreferenceChangeListener(this);
            }
        }
    }

    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        getActivity().getActionBar().setTitle(R.string.app_name);
        this.mFeatureSwitch.setVisibility(0);
        this.mAgreeTermCondition.setChecked(((Boolean) Z.PRIVACY_AGREED.read()).booleanValue());
        Preference sharePrivacy = findPreference("sharePrivacy");
        if (sharePrivacy != null) {
            if (GCSP.getGroupManager().isAnyGroupActive()) {
                getPreferenceScreen().removePreference(sharePrivacy);
            } else {
                getPreferenceScreen().addPreference(sharePrivacy);
            }
        }
        refresh();
    }

    public void onPause() {
        super.onPause();
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        this.mFeatureSwitch.setVisibility(4);
    }

    private void refresh() {
        boolean enableAccount;
        int i = 1;
        boolean enabled = ((Boolean) Z.FEATURE_ENABLED.read()).booleanValue();
        this.mFeatureSwitch.setChecked(enabled);
        setPreference(Z.CONTROL_ACCESS.name, enabled);
        setPreference(Z.SCREENSHOT_ACCESS.name, enabled);
        setPreference("sharePrivacy", enabled);
        setPreference("zapScreenshot", enabled);
        setPreference(Z.SEND_GOOGLE_ANALYTICS_REPORTS.name, enabled);
        setPreference(Z.USE_CONNECTION_TYPE.name, enabled);
        setPreference("receiveSelfShare", enabled);
        setPreference("serverHost", enabled);
        setPreference(Z.MODALITY_TYPE.name, enabled);
        if (ZapUtils.hasUserSignedOut() || !enabled) {
            enableAccount = false;
        } else {
            enableAccount = true;
        }
        setPreference("signoutzap", enableAccount);
        setPreference("deleteprofile", enableAccount);
        ListPreference connectionType = (ListPreference) findPreference(Z.USE_CONNECTION_TYPE.name);
        connectionType.setSummary(connectionType.getEntry());
        Config config = GCSP.getConfig();
        if (this.mReceiveSelfShare != null) {
            ListPreference listPreference = this.mReceiveSelfShare;
            if (!config.getBoolean(Config.DISCOVER_SELF_SHARED_CONTENT).booleanValue()) {
                i = 0;
            }
            listPreference.setValueIndex(i);
            this.mReceiveSelfShare.setSummary(this.mReceiveSelfShare.getEntry());
        }
        if (this.mServerHost != null) {
            String host = config.getString(Config.SERVER_HOST);
            this.mServerHost.setText(host);
            this.mServerHost.setSummary(host);
        }
        ListPreference modalityType = (ListPreference) findPreference(Z.MODALITY_TYPE.name);
        if (modalityType != null) {
            modalityType.setSummary(modalityType.getEntry());
        }
    }

    private void setPreference(String key, boolean enabled) {
        Preference pref = findPreference(key);
        if (pref != null) {
            pref.setEnabled(enabled);
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (Z.USE_CONNECTION_TYPE.name.equals(key)) {
            ListPreference connectionType = (ListPreference) findPreference(key);
            connectionType.setSummary(connectionType.getEntry());
            boolean isWifiOnly = ZapUtils.isWifiOnlyEnabled();
            GCSP.getConfig().setBoolean(Config.WIFI_ONLY_CONTENT_TRANSFER, Boolean.valueOf(isWifiOnly));
            GCSP.getCheckinManager().logWifiEnabledEvent(isWifiOnly);
        } else if (Z.MODALITY_TYPE.name.equals(key)) {
            Z.LAST_ACTION_TIME.write(Long.valueOf(0));
            ModalityHelper.getInstance().requestUnregisterModality();
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (((Boolean) Z.FEATURE_ENABLED.read()).booleanValue()) {
                        ModalityHelper.getInstance().requestRegisterModality(true);
                    }
                }
            }, 3000);
        } else if (Z.SEND_GOOGLE_ANALYTICS_REPORTS.name.equals(key)) {
            GCSP.getConfig().setBoolean(Config.GOOGLE_ANALYTICS_ENABLED, Boolean.valueOf(ZapConfig.isGoogleAnalyticsEnabled()));
        }
        refresh();
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if ("privacy-policy".equals(preference.getKey())) {
            ZapUtils.showGlobalPrivacyPolicy(App.getAppContext());
            return true;
        } else if ("about-app".equals(preference.getKey())) {
            Intent intent = new Intent(App.getAppContext(), DialogMessageActivity.class);
            intent.putExtra(ZapConstants.EXTRA_DIALOG_TITLE, getString(R.string.about_title));
            intent.putExtra(ZapConstants.EXTRA_DIALOG_MESSAGE, getString(R.string.about_body));
            intent.setFlags(DriveFile.MODE_READ_ONLY);
            App.getAppContext().startActivity(intent);
            return true;
        } else {
            if ("signoutzap".equals(preference.getKey())) {
                ZapUi.getInstance().showProgressDialog(getString(R.string.settings_privacy_please_wait));
                handleSignout();
            }
            if ("deleteprofile".equals(preference.getKey())) {
                DroidBlastDeleteProfileDialogFragment.newInstance(this).show(getFragmentManager(), "delete_preference");
            }
            return super.onPreferenceTreeClick(preferenceScreen, preference);
        }
    }

    public void handlePrivacyAgreementDisabled() {
        Z.FEATURE_ENABLED.write(Boolean.valueOf(false));
        this.mFeatureSwitch.setChecked(false);
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Z.FEATURE_ENABLED.write(Boolean.valueOf(isChecked));
        this.mAgreeTermCondition.setChecked(isChecked);
        ZapConfig.savePrivacyAgreed(isChecked);
        Z.LAST_ACTION_TIME.write(Long.valueOf(0));
        if (isChecked) {
            ModalityHelper.getInstance().requestRegisterModality(true);
        } else {
            cancelAllOngoingZaps();
            ModalityHelper.getInstance().requestUnregisterModality();
        }
        GCSP.getCheckinManager().logZapEnabledEvent(isChecked);
        refresh();
    }

    /* access modifiers changed from: private */
    public void handleSignout() {
        cancelAllOngoingZaps();
        new Handler().postDelayed(new Runnable() {
            public void run() {
                ZapUi.getInstance().dismissProgressDialog();
                Z.USER_SIGNEDOUT.write(Boolean.valueOf(true));
                ZapProfileSharedPerfs.resetProfile();
                Activity activity = DroidBlastPreferenceFragment.this.getActivity();
                if (activity != null) {
                    activity.finish();
                }
            }
        }, 5000);
    }

    private void cancelAllOngoingZaps() {
        ((NotificationManager) App.getAppContext().getSystemService("notification")).cancelAll();
        ZapTracker.getInstance().cancelAll();
        GroupManager groupManager = GCSP.getGroupManager();
        if (groupManager.isAnyGroupActive()) {
            groupManager.leave(null);
        }
    }

    public boolean onPreferenceChange(Preference preference, Object newValue) {
        boolean z;
        Config config = GCSP.getConfig();
        if (preference == this.mReceiveSelfShare) {
            int index = Integer.parseInt((String) newValue);
            String str = Config.DISCOVER_SELF_SHARED_CONTENT;
            if (index == 1) {
                z = true;
            } else {
                z = false;
            }
            config.setBoolean(str, Boolean.valueOf(z));
            this.mReceiveSelfShare.setSummary(this.mReceiveSelfShare.getEntries()[index]);
            return true;
        } else if (preference == this.mServerHost) {
            String host = (String) newValue;
            if (host != null) {
                host = host.trim();
            }
            if (TextUtils.isEmpty(host)) {
                host = null;
            }
            try {
                config.setString(Config.SERVER_HOST, host);
            } catch (Exception e) {
            }
            String host2 = config.getString(Config.SERVER_HOST);
            this.mServerHost.setText(host2);
            this.mServerHost.setSummary(host2);
            return false;
        } else if (preference != this.mAgreeTermCondition) {
            return false;
        } else {
            if (((Boolean) Z.PRIVACY_AGREED.read()).booleanValue()) {
                DroidBlastPrivacyDialogFragment.newInstance(this).show(getFragmentManager(), "privacy_preference");
                return false;
            }
            ZapConfig.savePrivacyAgreed(true);
            Z.FEATURE_ENABLED.write(Boolean.valueOf(true));
            this.mAgreeTermCondition.setChecked(((Boolean) Z.PRIVACY_AGREED.read()).booleanValue());
            return false;
        }
    }

    public void onDialogPositiveClick() {
        ZapConfig.savePrivacyAgreed(false);
        this.mAgreeTermCondition.setChecked(false);
        handlePrivacyAgreementDisabled();
    }

    public void onDialogNegativeClick() {
        this.mAgreeTermCondition.setChecked(((Boolean) Z.PRIVACY_AGREED.read()).booleanValue());
    }

    public void onDeletePositiveClick() {
        ZapUi.getInstance().showProgressDialog(getActivity().getString(R.string.profile_delete_toast));
        ProfileManager.getInstance().deleteMyProfile(new ProfileManagerListener() {
            public void onProfileEvent(ProfileEvent event) {
                if (event.status == Status.SUCCESS) {
                    DroidBlastPreferenceFragment.this.handleSignout();
                }
                if (event.status == Status.ERROR) {
                    ZapUi.getInstance().dismissProgressDialog();
                    ZapUi.getInstance().showDialogMessage(DroidBlastPreferenceFragment.this.getActivity().getString(R.string.settings_account_delete_profile_title), DroidBlastPreferenceFragment.this.getActivity().getString(R.string.profile_err_delete_toast), true);
                }
            }
        });
    }

    public void onDeleteNegativeClick() {
    }
}
