package com.motorola.sharing;

import java.io.Serializable;

public class Profile implements Serializable {
    public static final long serialVersionUID = 1;
    public boolean anonymous;
    public String nickname;
    public String pictureUrl;
    public String uploadPictureUrl;
    public String userId;
    public String version;

    public Profile() {
    }

    Profile(Profile profile) {
        this.version = profile.version;
        this.userId = profile.userId;
        this.anonymous = profile.anonymous;
        this.nickname = profile.nickname;
        this.pictureUrl = profile.pictureUrl;
        this.uploadPictureUrl = profile.uploadPictureUrl;
    }
}
