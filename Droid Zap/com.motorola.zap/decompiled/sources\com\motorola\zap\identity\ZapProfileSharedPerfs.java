package com.motorola.zap.identity;

import com.motorola.sharing.Profile;
import com.motorola.zap.Z;
import com.motorola.zap.ZapSharedPreference;

public class ZapProfileSharedPerfs {
    public static final ZapSharedPreference<Boolean> ZAP_PROFILE_ACTIVATE = new ZapSharedPreference<>("zapprofileactivate", Boolean.valueOf(false), ZapSharedPreference.booleanHandler);
    public static final ZapSharedPreference<Boolean> ZAP_PROFILE_ANONYMOUS = new ZapSharedPreference<>("zapprofileanonymous", Boolean.valueOf(false), ZapSharedPreference.booleanHandler);
    public static final ZapSharedPreference<Boolean> ZAP_PROFILE_AVAILABLE = new ZapSharedPreference<>("zapprofileavailable", Boolean.valueOf(false), ZapSharedPreference.booleanHandler);
    public static final ZapSharedPreference<String> ZAP_PROFILE_EMAIL = new ZapSharedPreference<>("zapprofileemail", null, ZapSharedPreference.stringHandler);
    public static final ZapSharedPreference<String> ZAP_PROFILE_NAME = new ZapSharedPreference<>("zapprofilename", null, ZapSharedPreference.stringHandler);
    public static final ZapSharedPreference<String> ZAP_PROFILE_PIC_URL = new ZapSharedPreference<>("zapprofilepicurl", null, ZapSharedPreference.stringHandler);
    public static final ZapSharedPreference<String> ZAP_PROFILE_UUID = new ZapSharedPreference<>("zapprofileuuid", null, ZapSharedPreference.stringHandler);

    public static void backupProfile(Profile aProfile) {
        ZAP_PROFILE_NAME.write(aProfile.nickname);
        ZAP_PROFILE_PIC_URL.write(aProfile.pictureUrl);
        ZAP_PROFILE_UUID.write(aProfile.userId);
        ZAP_PROFILE_ANONYMOUS.write(Boolean.valueOf(aProfile.anonymous));
        ZAP_PROFILE_AVAILABLE.write(Boolean.valueOf(true));
    }

    public static void resetProfile() {
        ZAP_PROFILE_NAME.write(null);
        ZAP_PROFILE_PIC_URL.write(null);
        ZAP_PROFILE_UUID.write(null);
        ZAP_PROFILE_ANONYMOUS.write(Boolean.valueOf(false));
        ZAP_PROFILE_AVAILABLE.write(Boolean.valueOf(false));
        ZAP_PROFILE_ACTIVATE.write(Boolean.valueOf(false));
        Z.SHOW_ZZ_MENU_TUTORIAL.write(Boolean.valueOf(true));
    }

    public static Profile getProfile() {
        if (!((Boolean) ZAP_PROFILE_AVAILABLE.read()).booleanValue()) {
            return null;
        }
        Profile lProfile = new Profile();
        lProfile.nickname = (String) ZAP_PROFILE_NAME.read();
        lProfile.pictureUrl = (String) ZAP_PROFILE_PIC_URL.read();
        lProfile.userId = (String) ZAP_PROFILE_UUID.read();
        lProfile.anonymous = ((Boolean) ZAP_PROFILE_ANONYMOUS.read()).booleanValue();
        return lProfile;
    }
}
