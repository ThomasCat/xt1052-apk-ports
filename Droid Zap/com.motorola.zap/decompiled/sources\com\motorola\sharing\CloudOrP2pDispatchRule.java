package com.motorola.sharing;

import android.text.TextUtils;
import com.motorola.sharing.ContentContainer.DiscoveryParams;
import com.motorola.sharing.ContentContainer.Type;
import com.motorola.sharing.ContentContainer.UploadRule;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.ContentShareListener.ContentCancelShareEvent;
import com.motorola.sharing.ContentShareListener.ContentShareEvent;
import com.motorola.sharing.ContentShareListener.ContentShareEvent.Substatus;
import com.motorola.sharing.ContentShareListener.ContentStopShareEvent;
import com.motorola.sharing.DispatchRule.Params;
import com.motorola.sharing.provider.cloud.CloudProvider;
import com.motorola.sharing.provider.p2p.P2pProvider;
import com.motorola.sharing.provider.p2p.P2pUtils;
import com.motorola.sharing.util.ContainerExpiryTracker.ExpiryTrigger;
import com.motorola.sharing.util.ContainerExpiryTracker.ExpiryTriggers;
import com.motorola.sharing.util.Logger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class CloudOrP2pDispatchRule implements DispatchRule {
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getGcspTag("CloudOrP2pDispatch");
    private static Set<CloudOrP2pDispatchRule> sRuleInstances = new HashSet();
    /* access modifiers changed from: private */
    public CloudProvider mCloudProvider = CloudProvider.getInstance();
    private final String mMimeTypePrefix;
    private P2pProvider mP2pProvider = P2pProvider.getInstance();
    private final int mSizeThreshold;

    private class ContentShareInterceptListener implements ContentShareListener {
        private ContentShareListener mOriginalListener = null;

        ContentShareInterceptListener(ContentShareListener listener) {
            this.mOriginalListener = listener;
        }

        public void onContentShareEvent(ContentShareEvent event) {
            ExpiryTriggers expiryTriggers;
            switch (event.status) {
                case INFO:
                    if (event.substatus == Substatus.META_SHARED) {
                        if (!CloudOrP2pDispatchRule.this.useP2pForUploadAndDownload(event.container)) {
                            CloudOrP2pDispatchRule.this.mCloudProvider.uploadContent(event.container, null);
                            break;
                        } else {
                            event.container.onContentShareEvent(Status.SUCCESS, Substatus.SHARED, CloudProvider.getInstance().getEndpoint());
                            break;
                        }
                    }
                    break;
                case ERROR:
                    if (!TextUtils.isEmpty(event.container.shareId)) {
                        GCSP.getContentDispatcher().stopSharing(event.container.shareId, null);
                        break;
                    }
                    break;
                case SUCCESS:
                    if (event.substatus == Substatus.SHARED) {
                        ContentContainer contentContainer = event.container;
                        if (event.container.isGroupRelated()) {
                            expiryTriggers = new ExpiryTriggers(ExpiryTrigger.BY_GROUP_TERMINATION);
                        } else {
                            expiryTriggers = new ExpiryTriggers(ExpiryTrigger.BY_TIME);
                        }
                        contentContainer.expiryTriggers = expiryTriggers;
                        break;
                    }
                    break;
            }
            if (this.mOriginalListener != null) {
                this.mOriginalListener.onContentShareEvent(event);
            }
        }

        public void onContentStopShareEvent(ContentStopShareEvent event) {
            if (this.mOriginalListener != null) {
                if (event.container.type == Type.CANCEL_SHARE) {
                    event.container = null;
                }
                this.mOriginalListener.onContentStopShareEvent(event);
            }
        }

        public void onContentCancelShareEvent(ContentCancelShareEvent event) {
            if (this.mOriginalListener != null) {
                this.mOriginalListener.onContentCancelShareEvent(event);
            }
        }
    }

    private class P2pContentShareInterceptListener implements ContentShareListener {
        private Endpoint[] mEndpoints = null;
        private ContentShareListener mOriginalListener = null;

        P2pContentShareInterceptListener(ContentShareListener listener, Endpoint[] endpoints) {
            this.mOriginalListener = listener;
            this.mEndpoints = endpoints;
        }

        public void onContentShareEvent(ContentShareEvent event) {
            switch (event.status) {
                case INFO:
                    if (event.substatus == Substatus.META_SHARED) {
                        Logger.i(CloudOrP2pDispatchRule.TAG, "Shared on P2p, now let's send metadata to the cloud");
                        event.container.useJpegSizeReduction = false;
                        event.container.uploadRule = UploadRule.NEVER;
                        CloudOrP2pDispatchRule.this.setContentShareInterceptListener(event.container, this.mOriginalListener);
                        CloudOrP2pDispatchRule.this.mCloudProvider.shareContent(event.container, null, this.mEndpoints);
                        return;
                    }
                    return;
                case ERROR:
                    if (this.mOriginalListener != null) {
                        this.mOriginalListener.onContentShareEvent(event);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }

        public void onContentStopShareEvent(ContentStopShareEvent event) {
            switch (event.status) {
                case ERROR:
                    if (this.mOriginalListener != null) {
                        if (event.container.type == Type.CANCEL_SHARE) {
                            event.container = null;
                        }
                        this.mOriginalListener.onContentStopShareEvent(event);
                        return;
                    }
                    return;
                case SUCCESS:
                    Logger.i(CloudOrP2pDispatchRule.TAG, "Stopped sharing on P2p, let's stop on the cloud now");
                    CloudOrP2pDispatchRule.this.setContentShareInterceptListener(event.container, this.mOriginalListener);
                    CloudOrP2pDispatchRule.this.mCloudProvider.stopSharing(event.container);
                    return;
                default:
                    return;
            }
        }

        public void onContentCancelShareEvent(ContentCancelShareEvent event) {
            if (this.mOriginalListener != null) {
                this.mOriginalListener.onContentCancelShareEvent(event);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r3 = new com.motorola.sharing.CloudOrP2pDispatchRule(r2, r5);
        sRuleInstances.add(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0072, code lost:
        r4 = r3;
     */
    static synchronized DispatchRule getInstance(Params params) {
        CloudOrP2pDispatchRule rule;
        synchronized (CloudOrP2pDispatchRule.class) {
            try {
                String mimeTypePrefix = (String) params.get("mimeTypePrefix");
                int sizeThreshold = ((Integer) params.get("sizeThreshold")).intValue();
                if (sizeThreshold >= 0) {
                    Iterator i$ = sRuleInstances.iterator();
                    while (true) {
                        if (!i$.hasNext()) {
                            break;
                        }
                        rule = (CloudOrP2pDispatchRule) i$.next();
                        if ((rule.getMimeTypePrefix() == null || rule.getMimeTypePrefix().equals(mimeTypePrefix)) && rule.getSizeThreshold() == sizeThreshold) {
                            break;
                        }
                    }
                } else {
                    throw new IllegalArgumentException("size=" + sizeThreshold);
                }
            } catch (Exception e) {
                throw new IllegalArgumentException("missing mandatory params");
            }
        }
        return rule;
    }

    private CloudOrP2pDispatchRule(String mimeTypePrefix, int sizeThreshold) {
        this.mMimeTypePrefix = mimeTypePrefix;
        this.mSizeThreshold = sizeThreshold;
    }

    public String getMimeTypePrefix() {
        return this.mMimeTypePrefix;
    }

    public int getSizeThreshold() {
        return this.mSizeThreshold;
    }

    public Params getParams() {
        Params params = new Params();
        params.put(DispatchRule.KEY_CLASS_NAME, getClass().getName());
        params.put("mimeTypePrefix", this.mMimeTypePrefix);
        params.put("sizeThreshold", Integer.valueOf(this.mSizeThreshold));
        return params;
    }

    public void discoverContent(DiscoveryParams params, ContentDiscoveryListener listener, Endpoint[] endpoints) {
        Logger.d(TAG, "discoverContent(): " + params.getPrintableSignature());
        params.setContentDiscoveryListener(listener);
        this.mCloudProvider.discoverContent(params, null, endpoints);
    }

    public void stopDiscovery(DiscoveryParams params) {
        Logger.d(TAG, "stopDiscovery(): " + params.getPrintableSignature());
        this.mCloudProvider.stopDiscovery(params);
    }

    public void downloadContent(ContentContainer container, ContentDownloadListener listener) {
        Logger.d(TAG, "downloadContent(): " + container.getPrintableSignature());
        container.setContentDownloadListener(listener);
        if (container.getP2pParams() != null) {
            this.mP2pProvider.downloadContent(container, null);
        } else {
            this.mCloudProvider.downloadContent(container, null);
        }
    }

    public void cancelDownload(ContentContainer container) {
        Logger.d(TAG, "cancelDownload(): " + container.getPrintableSignature());
        if (container.getP2pParams() != null) {
            this.mP2pProvider.cancelDownload(container);
        } else {
            this.mCloudProvider.cancelDownload(container);
        }
    }

    public void reportAbuse(ContentContainer container, ReportAbuseListener listener) {
        Logger.d(TAG, "reportAbuse(): " + container.getPrintableSignature());
        container.setReportAbuseListener(listener);
        this.mCloudProvider.reportAbuse(container, null);
    }

    public void shareContent(ContentContainer container, ContentShareListener listener, Endpoint[] endpoints) {
        Logger.d(TAG, "shareContent(): " + container.getPrintableSignature());
        if (useP2pForUploadAndDownload(container)) {
            setP2pContentShareInterceptListener(container, listener, endpoints);
            this.mP2pProvider.shareContent(container, null, endpoints);
            return;
        }
        container.uploadRule = UploadRule.ALWAYS;
        setContentShareInterceptListener(container, listener);
        this.mCloudProvider.shareContent(container, null, endpoints);
    }

    public void stopSharing(ContentContainer container) {
        Logger.d(TAG, "stopSharing(): " + container.getPrintableSignature());
        if (useP2pForUploadAndDownload(container)) {
            setP2pContentShareInterceptListener(container, null, null);
            this.mP2pProvider.stopSharing(container);
            return;
        }
        this.mCloudProvider.stopSharing(container);
    }

    public void stopSharing(String shareId, ContentShareListener listener) {
        Logger.d(TAG, "stopSharing(): sid=" + shareId);
        ContentContainer container = ContentContainer.generateCancelShareContainer(shareId);
        setP2pContentShareInterceptListener(container, listener, null);
        this.mP2pProvider.stopSharing(container);
    }

    public void cancelSharing(ContentContainer container) {
        Logger.d(TAG, "cancelSharing(): " + container.getPrintableSignature());
        this.mP2pProvider.cancelSharing(container);
        this.mCloudProvider.cancelSharing(container);
    }

    public void resumeSharing(ContentContainer container, ContentShareListener listener) {
        Logger.d(TAG, "resumeSharing(): " + container.getPrintableSignature());
        container.setContentShareListener(listener);
        if (useP2pForUploadAndDownload(container)) {
            this.mP2pProvider.resumeSharing(container, null);
        } else {
            this.mCloudProvider.resumeSharing(container, null);
        }
    }

    private synchronized void setP2pContentShareInterceptListener(ContentContainer container, ContentShareListener clientListener, Endpoint[] endpoints) {
        if (clientListener instanceof P2pContentShareInterceptListener) {
            container.setContentShareListener(clientListener);
        } else {
            Logger.v(TAG, "intercepting share events for: " + container.getPrintableSignature());
            container.setContentShareListener(new P2pContentShareInterceptListener(clientListener, endpoints));
        }
    }

    /* access modifiers changed from: private */
    public synchronized void setContentShareInterceptListener(ContentContainer container, ContentShareListener clientListener) {
        if (clientListener instanceof ContentShareInterceptListener) {
            container.setContentShareListener(clientListener);
        } else {
            Logger.v(TAG, "intercepting share events for: " + container.getPrintableSignature());
            container.setContentShareListener(new ContentShareInterceptListener(clientListener));
        }
    }

    /* access modifiers changed from: private */
    public boolean useP2pForUploadAndDownload(ContentContainer container) {
        if (TextUtils.isEmpty(this.mMimeTypePrefix) || TextUtils.isEmpty(container.mimeType)) {
            return P2pUtils.contentSize(container) >= ((long) this.mSizeThreshold);
        }
        return container.mimeType.startsWith(this.mMimeTypePrefix);
    }

    public Endpoint[] getStaticEndpoints() {
        Endpoint[] endpoints1 = CloudProvider.getInstance().getStaticEndpoints();
        Endpoint[] endpoints2 = P2pProvider.getInstance().getStaticEndpoints();
        Endpoint[] result = (Endpoint[]) Arrays.copyOf(endpoints1, endpoints1.length + endpoints2.length);
        System.arraycopy(endpoints2, 0, result, endpoints1.length, endpoints2.length);
        return result;
    }
}
