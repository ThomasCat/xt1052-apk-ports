package com.motorola.zap.app;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import com.motorola.sharing.ContentContainer.DiscoveryParams;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.Locality;
import com.motorola.sharing.instrumentation.ActionResult.Error;
import com.motorola.sharing.instrumentation.CheckinInterface;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.location.BlastLocationProvider;
import com.motorola.zap.location.BlastLocationProvider.GeoLocationInfo;
import com.motorola.zap.location.BlastLocationProvider.LocationResult;
import com.motorola.zap.location.ModalityHelper;
import com.motorola.zap.ui.ZapUi;
import com.motorola.zap.ui.ZapUi.LocationEvent;
import com.motorola.zap.zz.ui.ZzPoller;
import java.util.concurrent.TimeUnit;

public class DownloadHandler {
    /* access modifiers changed from: private */
    public static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    public static long sLastReceiveActionTime = 0;
    public static long sLastReceiveGesture = 0;

    public void handleDownloadGesture(Intent intent) {
        long now = SystemClock.elapsedRealtime();
        if (now - sLastReceiveGesture < 1000) {
            Log.i("Zap-DownloadHandler", "ACTION_GS_SWIPE_DOWN dropped");
            return;
        }
        sLastReceiveGesture = now;
        if (Log.isLoggable("Zap-DownloadHandler", 4)) {
            Log.i("Zap-DownloadHandler", "ACTION_GS_SWIPE_DOWN");
        }
        if (!ZapUtils.isPhoneLocked()) {
            if (ZapUtils.hasUserSignedOut()) {
                ZapTracker.getInstance().onRefreshComplete();
                ZapUtils.startSetupActivity(intent, false);
                return;
            }
            handleActionReceive(intent);
        } else if (Log.isLoggable("Zap-DownloadHandler", 4)) {
            Log.i("Zap-DownloadHandler", "Phone is locked or action is unexpected - ignoring action ");
            ZapTracker.getInstance().onRefreshComplete();
        }
    }

    public void handleActionReceive(Intent intent) {
        DiscoveryParams discoveryParams = new DiscoveryParams();
        String pin = intent.getStringExtra(ZapConstants.EXTRA_PIN);
        if (TextUtils.isEmpty(pin)) {
            pin = null;
        }
        discoveryParams.setPin(pin);
        boolean refreshComplete = false;
        if (!ZapUtils.checkGeneralTransferPreConditions(intent, discoveryParams)) {
            refreshComplete = true;
        } else if (!handleReceiving(intent, discoveryParams, intent.getBooleanExtra(ZapConstants.EXTRA_RECOVER_USER_ACTION_INTENT, false))) {
            refreshComplete = true;
        }
        if (refreshComplete) {
            ZapTracker.getInstance().onRefreshComplete();
        }
    }

    private boolean handleReceiving(Intent intent, DiscoveryParams discoveryParams, boolean isRecoverUserAction) {
        ZapTracker.getInstance().onRefresh();
        if (Log.isLoggable("Zap-DownloadHandler", 4)) {
            Log.i("Zap-DownloadHandler", "*** RECEIVING ***");
        }
        if (!((Boolean) Z.FEATURE_ENABLED.read()).booleanValue()) {
            Log.d("Zap-DownloadHandler", "feature is disabled, drop handling");
            return false;
        }
        Context context = App.getAppContext();
        long now = System.currentTimeMillis();
        if (now - State.sUserActionDialogLaunchedTime > 1000 && now - sLastReceiveActionTime < 1000) {
            if (DBG) {
                Log.d("Zap-DownloadHandler", "Receive action already started in the last 1 seconds");
            }
            context.sendBroadcast(new Intent(ZapConstants.ACTION_SWYPE_RECEIVE_FAILED), "com.motorola.blast.permission.TRIGGER_BLAST_ACTION");
            return false;
        } else if (discoveryParams.getUserAccount() == null) {
            if (Log.isLoggable("Zap-DownloadHandler", 6)) {
                Log.e("Zap-DownloadHandler", "ERROR - User account is null");
            }
            return false;
        } else {
            if (ZapUtils.isAutoProcessing(discoveryParams)) {
                sLastReceiveActionTime = now;
            } else if (!new GestureRules().canHandleGesture(intent, isRecoverUserAction, false, false)) {
                if (DBG) {
                    Log.d("Zap-DownloadHandler", "Gesture is not allowed from this package");
                }
                return false;
            } else {
                sLastReceiveActionTime = now;
                ZapUi zapUi = ZapUi.getInstance();
                if (LocationHandler.getInstance().isWaitingForLocation(discoveryParams)) {
                    if (DBG) {
                        Log.d("Zap-DownloadHandler", "Still busy with location obtaing in another task");
                    }
                    zapUi.onLocationEvent(LocationEvent.STILL_VERIFYING_LOCATION);
                    return false;
                }
                ZapUi.vibrate(300);
                zapUi.showToastMessage(discoveryParams.getPin() != null ? context.getString(R.string.toast_msg_verifying_code) : context.getString(R.string.toast_msg_looking_around), ZapUi.getReceiveToastIconId(), true, false);
            }
            asyncReceive(discoveryParams);
            return true;
        }
    }

    private static void asyncReceive(final DiscoveryParams discoveryParams) {
        final Context context = App.getAppContext();
        final ZapUi zapUi = ZapUi.getInstance();
        new Thread(new Runnable() {
            public void run() {
                Locality locality;
                String string;
                ModalityHelper.getInstance().requestRegisterModality(false);
                Z.LAST_ACTION_TIME.write(Long.valueOf(System.currentTimeMillis()));
                if (!discoveryParams.isGroupRelated()) {
                    long currentTimeMillis = System.currentTimeMillis();
                    LocationResult locationForCurrentModality = LocationHandler.getInstance().getLocationForCurrentModality(false);
                    if (locationForCurrentModality == null) {
                        if (DownloadHandler.DBG) {
                            Log.d("Zap-DownloadHandler", "Still busy with location obtaining in another task, receiving discarded");
                        }
                        zapUi.onLocationEvent(LocationEvent.STILL_VERIFYING_LOCATION);
                        return;
                    }
                    discoveryParams.putCheckinData(CheckinInterface.KEY_MODALITY, ModalityHelper.getInstance().getCurrentModality());
                    if (locationForCurrentModality.location != null) {
                        locality = new Locality();
                        locality.latitude = locationForCurrentModality.location.getLatitude();
                        locality.longitude = locationForCurrentModality.location.getLongitude();
                        locality.provider = locationForCurrentModality.location.getProvider();
                        locality.accuracy = (double) locationForCurrentModality.location.getAccuracy();
                        locality.timestamp = locationForCurrentModality.getTime();
                        locality.distance = ModalityHelper.getInstance().getShareDistance();
                    } else {
                        locality = null;
                    }
                    if (!locationForCurrentModality.hasSatisfiedLocation()) {
                        if (Log.isLoggable("Zap-DownloadHandler", 5)) {
                            Log.w("Zap-DownloadHandler", "WARN - Could not acquire accurate enough location");
                        }
                        discoveryParams.setLocality(locality);
                        discoveryParams.actionResult.setError(Error.LOCATION_NOT_ACCURATE);
                        GCSP.getCheckinManager().logZapEvent(discoveryParams);
                        if (!BlastLocationProvider.getInstance().areAllLocationSettingsEnabled()) {
                            Intent intent = new Intent(ZapConstants.ACTION_GS_SWIPE_DOWN);
                            intent.setClass(context, ActionReceiver.class);
                            intent.putExtra(ZapConstants.EXTRA_EVENT_ORIGIN_ZAP_APP, true);
                            zapUi.showFixLocationSettingsDialog(intent);
                            State.sUserActionDialogLaunchedTime = System.currentTimeMillis();
                            if (DownloadHandler.DBG) {
                                Log.d("Zap-DownloadHandler", "Showing dialog for user to enable setting");
                                return;
                            }
                            return;
                        }
                        if (Log.isLoggable("Zap-DownloadHandler", 6)) {
                            Log.e("Zap-DownloadHandler", "ERROR - Could not determine accurate location - stopping receive action");
                        }
                        zapUi.showDialogMessage(context.getString(R.string.dialog_title_location_not_determined), context.getString(R.string.dialog_msg_no_accurate_location), true);
                        return;
                    }
                    if (System.currentTimeMillis() - currentTimeMillis > TimeUnit.SECONDS.toMillis(6)) {
                        ZapUi zapUi = zapUi;
                        if (TextUtils.isEmpty(discoveryParams.getPin())) {
                            string = context.getString(R.string.toast_msg_looking_around);
                        } else {
                            string = context.getString(R.string.toast_msg_verifying_code);
                        }
                        zapUi.showToastMessage(string, ZapUi.getReceiveToastIconId(), true, false);
                    }
                    GeoLocationInfo locationInfo = BlastLocationProvider.getLocationInfo(locality.latitude, locality.longitude, context);
                    if (locationInfo != null) {
                        locality.city = locationInfo.city;
                        locality.state = locationInfo.state;
                        locality.country = locationInfo.country;
                        if (DownloadHandler.DBG) {
                            Log.d("Zap-DownloadHandler", locationInfo.toString());
                        }
                    } else if (DownloadHandler.DBG) {
                        Log.d("Zap-DownloadHandler", "Could not acquire GeoLocation info for this download");
                    }
                    discoveryParams.setLocality(locality);
                }
                GCSP.getCheckinManager().logZapContext(discoveryParams);
                GCSP.getContentDispatcher().discoverContent(discoveryParams, zapUi.getContentDiscoveryListener(), null);
            }
        }).start();
    }

    public void handleZzPolling(Intent intent) {
        if (!GCSP.getGroupManager().isAnyGroupActive()) {
            ZzCameraWatcher.getInstance().setCameraAutoShareSettingEnabled(false);
            ZzPoller.getInstance().setPollingSettingEnabled(false);
            return;
        }
        boolean refreshComplete = false;
        DiscoveryParams discoveryParams = new DiscoveryParams();
        ZapUtils.setAutoProcessing(discoveryParams, true);
        if (!ZapUtils.checkGeneralTransferPreConditions(null, discoveryParams)) {
            refreshComplete = true;
        } else if (!handleReceiving(intent, discoveryParams, false)) {
            refreshComplete = true;
        }
        if (refreshComplete) {
            ZapTracker.getInstance().onRefreshComplete();
        }
    }

    public void handleAutoPolling(Intent intent) {
        boolean refreshComplete = false;
        DiscoveryParams discoveryParams = new DiscoveryParams();
        ZapUtils.setAutoProcessing(discoveryParams, true);
        if (!ZapUtils.checkGeneralTransferPreConditions(null, discoveryParams)) {
            refreshComplete = true;
        } else if (!handleReceiving(intent, discoveryParams, false)) {
            refreshComplete = true;
        }
        if (refreshComplete) {
            ZapTracker.getInstance().onRefreshComplete();
        }
    }
}
