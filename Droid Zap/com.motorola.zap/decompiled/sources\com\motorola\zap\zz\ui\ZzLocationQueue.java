package com.motorola.zap.zz.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.wifi.WifiManager;
import android.os.SystemClock;
import android.util.Log;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupManager;
import com.motorola.sharing.GroupManager.CreateParams;
import com.motorola.sharing.GroupManager.JoinParams;
import com.motorola.sharing.GroupManager.ShareInviteParams;
import com.motorola.sharing.Locality;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.app.App;
import com.motorola.zap.location.BlastLocationProvider;
import com.motorola.zap.location.BlastLocationProvider.LocationResult;
import com.motorola.zap.location.ModalityHelper;
import com.motorola.zap.ui.ZapUi;
import com.motorola.zap.ui.ZapUi.LocationEvent;
import java.io.Serializable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ZzLocationQueue {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private static ZzLocationQueue sInstance;
    private final Executor mExecutor = Executors.newSingleThreadExecutor();

    public static class Receiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            ZzLocationQueue.getInstance().addRunnable((ZzRequestParams) intent.getSerializableExtra("params"));
        }
    }

    public static final class ZzRequestParams implements Serializable {
        public Object paramObject;
        public Type type;

        public enum Type {
            ZZ_CREATE,
            ZZ_JOIN,
            ZZ_SHARE_INVITE,
            ZZ_LEAVE
        }
    }

    public static synchronized ZzLocationQueue getInstance() {
        ZzLocationQueue zzLocationQueue;
        synchronized (ZzLocationQueue.class) {
            if (sInstance == null) {
                sInstance = new ZzLocationQueue();
            }
            zzLocationQueue = sInstance;
        }
        return zzLocationQueue;
    }

    /* access modifiers changed from: private */
    public void addRunnable(final ZzRequestParams params) {
        this.mExecutor.execute(new Runnable() {
            public void run() {
                ZzLocationQueue.this.executeRequest(params);
            }
        });
    }

    public void add(ZzRequestParams params) {
        addRunnable(params);
    }

    private Intent getRetryIntent(ZzRequestParams params) {
        Intent i = new Intent("com.motorola.zap.retry");
        i.putExtra("params", params);
        return i;
    }

    public void zzCreate(CreateParams params) {
        ZzRequestParams requestParams = new ZzRequestParams();
        requestParams.type = Type.ZZ_CREATE;
        requestParams.paramObject = params;
        add(requestParams);
    }

    public void zzJoin(JoinParams params) {
        ZzRequestParams requestParams = new ZzRequestParams();
        requestParams.type = Type.ZZ_JOIN;
        requestParams.paramObject = params;
        add(requestParams);
    }

    public void zzShareInvite(ShareInviteParams params) {
        ZzRequestParams requestParams = new ZzRequestParams();
        requestParams.type = Type.ZZ_SHARE_INVITE;
        requestParams.paramObject = params;
        add(requestParams);
    }

    public void zzLeave() {
        ZzRequestParams requestParams = new ZzRequestParams();
        requestParams.type = Type.ZZ_LEAVE;
        requestParams.paramObject = null;
        add(requestParams);
    }

    /* access modifiers changed from: private */
    public void executeRequest(ZzRequestParams params) {
        GroupManager manager = GCSP.getGroupManager();
        switch (params.type) {
            case ZZ_CREATE:
                CreateParams createParams = (CreateParams) params.paramObject;
                if (createParams.locality == null) {
                    Location location = getCurrentLocation();
                    if (location == null) {
                        handleLocationError(getRetryIntent(params));
                        return;
                    }
                    createParams.locality = new Locality();
                    createParams.locality.latitude = location.getLatitude();
                    createParams.locality.longitude = location.getLongitude();
                    createParams.locality.distance = ModalityHelper.getInstance().getShareDistance();
                }
                manager.create(createParams);
                return;
            case ZZ_JOIN:
                JoinParams joinParams = (JoinParams) params.paramObject;
                if (joinParams.locality == null) {
                    Location location2 = getCurrentLocation();
                    if (location2 == null) {
                        handleLocationError(getRetryIntent(params));
                        return;
                    }
                    joinParams.locality = new Locality();
                    joinParams.locality.latitude = location2.getLatitude();
                    joinParams.locality.longitude = location2.getLongitude();
                    joinParams.locality.distance = ModalityHelper.getInstance().getShareDistance();
                }
                manager.join(joinParams);
                return;
            case ZZ_SHARE_INVITE:
                ShareInviteParams shareInviteParams = (ShareInviteParams) params.paramObject;
                if (shareInviteParams.locality == null) {
                    Location location3 = getCurrentLocation();
                    if (location3 == null) {
                        handleLocationError(getRetryIntent(params));
                        return;
                    }
                    shareInviteParams.locality = new Locality();
                    shareInviteParams.locality.latitude = location3.getLatitude();
                    shareInviteParams.locality.longitude = location3.getLongitude();
                    shareInviteParams.locality.distance = ModalityHelper.getInstance().getShareDistance();
                }
                manager.shareInvite(shareInviteParams);
                return;
            case ZZ_LEAVE:
                manager.leave(null);
                return;
            default:
                return;
        }
    }

    private Location getCurrentLocation() {
        Long l;
        Context context = App.getAppContext();
        LocationResult result = BlastLocationProvider.getBestLastKnownLocation(context, ZapConstants.LOCATION_MAX_AGE_NO_MODALITY_SUPPORT);
        if (result.hasSatisfiedLocation()) {
            if (DBG) {
                Log.d("Zap-ZzRequestHandler", "Known location satisfies required conditions");
            }
            return result.location;
        }
        if (DBG) {
            Log.d("Zap-ZzRequestHandler", "Known location does not satisfy required conditions - starting updates");
        }
        BlastLocationProvider.getInstance().requestUpdates(true, true);
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        if (wifiManager != null && wifiManager.isWifiEnabled()) {
            if (DBG) {
                Log.d("Zap-ZzRequestHandler", "Triggering WiFi scan to help on obtaining accurate location");
            }
            wifiManager.startScan();
        }
        long startTime = System.currentTimeMillis();
        if (BlastLocationProvider.getInstance().areAllLocationSettingsEnabled()) {
            l = (Long) Z.LOCATION_RETRY_MAX_TIME_SETTINGS_ENABLED.read();
        } else {
            l = (Long) Z.LOCATION_RETRY_MAX_TIME_SETTINGS_DISABLED.read();
        }
        long retryUntil = startTime + l.longValue();
        boolean toastShown = false;
        while (!result.hasSatisfiedLocation() && System.currentTimeMillis() < retryUntil) {
            if (System.currentTimeMillis() - startTime > 3000 && !toastShown) {
                toastShown = true;
                ZapUi.getInstance().onLocationEvent(LocationEvent.VERIFYING_LOCATION);
            }
            SystemClock.sleep(2000);
            if (DBG) {
                Log.d("Zap-ZzRequestHandler", "Retrying location");
            }
            result = BlastLocationProvider.getBestLastKnownLocation(context, ZapConstants.LOCATION_MAX_AGE_NO_MODALITY_SUPPORT);
        }
        if (result.hasSatisfiedLocation()) {
            return result.location;
        }
        return null;
    }

    private boolean handleLocationError(Intent retryIntent) {
        Context context = App.getAppContext();
        ZapUi ui = ZapUi.getInstance();
        BlastLocationProvider provider = BlastLocationProvider.getInstance();
        if (retryIntent == null || provider.areAllLocationSettingsEnabled()) {
            if (DBG) {
                Log.d("Zap-ZzRequestHandler", "No accurate location");
            }
            ui.showDialogMessage(context.getString(R.string.dialog_title_location_not_determined), context.getString(R.string.dialog_msg_no_accurate_location), true);
        } else {
            if (DBG) {
                Log.d("Zap-ZzRequestHandler", "No accurate location, try to fix this");
            }
            ui.showFixLocationSettingsDialog(retryIntent);
        }
        return true;
    }
}
