package com.motorola.zap.ui;

import android.os.Handler;
import android.os.Looper;
import android.widget.ProgressBar;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentContainer.TransferStatus;
import com.motorola.zap.app.ZapTracker;
import com.motorola.zap.app.ZapTracker.ProgressListener;
import java.util.HashMap;
import java.util.Map;

public class ProgressBarHelper implements ProgressListener {
    private final Handler mHandler = new Handler(Looper.getMainLooper());
    private final Map<ProgressBar, Long> mProgressBarUidMap = new HashMap();
    /* access modifiers changed from: private */
    public final Map<Long, ProgressBar> mUidProgressBarMap = new HashMap();

    public void put(long uid, ProgressBar progressBar) {
        this.mUidProgressBarMap.put(Long.valueOf(uid), progressBar);
        this.mProgressBarUidMap.put(progressBar, Long.valueOf(uid));
    }

    public void remove(ProgressBar progressBar) {
        this.mUidProgressBarMap.remove((Long) this.mProgressBarUidMap.remove(progressBar));
    }

    public void clear() {
        this.mUidProgressBarMap.clear();
        this.mProgressBarUidMap.clear();
    }

    public void registerListener() {
        ZapTracker.getInstance().addListener((ProgressListener) this);
    }

    public void unregisterListener() {
        ZapTracker.getInstance().removeListener((ProgressListener) this);
    }

    public void onProgress(ContentContainer container) {
        this.mHandler.post(getUpdateRunnable(container));
    }

    private Runnable getUpdateRunnable(final ContentContainer trans) {
        return new Runnable() {
            public void run() {
                int progressPct;
                ProgressBar p = (ProgressBar) ProgressBarHelper.this.mUidProgressBarMap.get(Long.valueOf(trans.getUid()));
                if (p != null) {
                    TransferStatus transferStatus = trans.getTransferStatus();
                    if (transferStatus != null) {
                        progressPct = transferStatus.progress();
                    } else {
                        progressPct = 0;
                    }
                    p.setIndeterminate(false);
                    p.setProgress(progressPct);
                    p.setMax(100);
                }
            }
        };
    }
}
