package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;
import java.util.ArrayList;

public class ez implements Creator<ey> {
    static void a(ey eyVar, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.c(parcel, 1, eyVar.getVersionCode());
        b.b(parcel, 2, eyVar.ek(), false);
        b.D(parcel, p);
    }

    /* renamed from: U */
    public ey[] newArray(int i) {
        return new ey[i];
    }

    /* renamed from: r */
    public ey createFromParcel(Parcel parcel) {
        int o = a.o(parcel);
        int i = 0;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    i = a.g(parcel, n);
                    break;
                case 2:
                    arrayList = a.c(parcel, n, ey.a.CREATOR);
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new ey(i, arrayList);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }
}
