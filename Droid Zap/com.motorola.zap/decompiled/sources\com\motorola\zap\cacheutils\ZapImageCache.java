package com.motorola.zap.cacheutils;

import android.text.TextUtils;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.motorola.zap.app.App;
import com.motorola.zap.app.LruBitmapCache;

public class ZapImageCache {
    public static final String TAG = ZapImageCache.class.getSimpleName();
    private static ZapImageCache mInstance;
    private LruBitmapCache mCache;
    private ImageLoader mImageLoader;
    private RequestQueue mRequestQueue;

    public static synchronized ZapImageCache getInstance() {
        ZapImageCache zapImageCache;
        synchronized (ZapImageCache.class) {
            if (mInstance == null) {
                mInstance = new ZapImageCache();
            }
            zapImageCache = mInstance;
        }
        return zapImageCache;
    }

    public RequestQueue getRequestQueue() {
        if (this.mRequestQueue == null) {
            this.mRequestQueue = Volley.newRequestQueue(App.getAppContext());
        }
        return this.mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (this.mImageLoader == null) {
            this.mCache = new LruBitmapCache();
            this.mImageLoader = new ImageLoader(this.mRequestQueue, this.mCache);
        }
        return this.mImageLoader;
    }

    public void clearCache() {
        if (this.mCache != null) {
            this.mCache.evictAll();
        }
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        if (TextUtils.isEmpty(tag)) {
            tag = TAG;
        }
        req.setTag(tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (this.mRequestQueue != null) {
            this.mRequestQueue.cancelAll(tag);
        }
    }
}
