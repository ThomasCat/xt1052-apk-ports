package com.motorola.sharing.instrumentation;

import android.content.Context;
import android.text.TextUtils;
import com.motorola.blur.util.CheckinEventWrapper;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentContainer.Type;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.Settings;
import com.motorola.sharing.instrumentation.ActionResult.Status;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker.GroupState;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker.GroupStats;
import com.motorola.sharing.util.DeviceUtils;
import com.motorola.sharing.util.Logger;
import com.motorola.sharing.util.PackageUtils;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.location.ModalityHelper;

class MotoCheckin implements CheckinInterface {
    private static final String TAG = Logger.getGcspTag("MotoCheckin");
    private static MotoCheckin sInstance = null;
    private Context mContext = null;
    private boolean mEnabled = false;
    private String mTag = null;

    public static boolean isSupported() {
        if (CheckinEventWrapper.isInitialized()) {
            Context c = GCSP.getContext();
            if (c != null && c.checkCallingOrSelfPermission("com.motorola.permission.ACCESS_CHECKIN") == 0) {
                return true;
            }
        }
        return false;
    }

    public static synchronized MotoCheckin getInstance() {
        MotoCheckin motoCheckin;
        synchronized (MotoCheckin.class) {
            if (sInstance == null) {
                sInstance = new MotoCheckin(GCSP.getContext());
            }
            motoCheckin = sInstance;
        }
        return motoCheckin;
    }

    private MotoCheckin(Context context) {
        this.mContext = context;
        init();
    }

    private void init() {
        this.mEnabled = ((Boolean) Settings.MOTO_CHECKIN_ENABLED.get()).booleanValue();
        this.mTag = (String) Settings.MOTO_CHECKIN_ID.get();
        if (!this.mEnabled || TextUtils.isEmpty(this.mTag)) {
            Logger.i(TAG, "reporting disabled");
        } else {
            Logger.i(TAG, "reporting enabled");
        }
    }

    private boolean needUpdate() {
        return this.mEnabled != ((Boolean) Settings.MOTO_CHECKIN_ENABLED.get()).booleanValue() || !TextUtils.equals(this.mTag, (CharSequence) Settings.MOTO_CHECKIN_ID.get());
    }

    public void logException(Thread thread, Throwable exception, boolean fatal) {
    }

    public void logZapContext(ContentContainer container) {
        if (!container.isGroupRelated()) {
            CheckinEventWrapper event = createEvent("ZapContext", "1.1");
            if (event != null) {
                if (container.locality != null) {
                    event.setValue("loc_lat", container.locality.latitude);
                    event.setValue("loc_long", container.locality.longitude);
                    event.setValue("loc_prov", notNull(container.locality.provider));
                    event.setValue("loc_accy", container.locality.accuracy);
                    event.setValue("loc_age", container.locality.getAge());
                    event.setValue(CheckinInterface.KEY_MODALITY, getModality(container));
                }
                logEvent(event);
            }
        }
    }

    public void logZapEvent(ContentContainer container) {
        Status status = container.actionResult.getStatus();
        if (status != Status.CANCELED && !container.isGroupRelated()) {
            CheckinEventWrapper event = createEvent(container.type == Type.SHARE ? "ZapSent" : "ZapReceived", "1.1");
            if (event != null) {
                event.setValue("app_ver", PackageUtils.getAppVersion(this.mContext));
                event.setValue("priv", container.pin != null ? "t" : "f");
                event.setValue("wifi", "wifi".equalsIgnoreCase(getNetworkType(container)) ? "t" : "f");
                event.setValue("trig_ts", container.creationTime);
                event.setValue("xfer_status", status.toString());
                event.setValue(CheckinInterface.KEY_MODALITY, getModality(container));
                event.setValue(CheckinInterface.KEY_SHARE_TYPE, CheckinUtil.getShareType(container));
                event.setValue(CheckinInterface.KEY_DEST, CheckinUtil.getDest(container));
                if (container.locality != null && isNotEmpty(container.locality.provider)) {
                    event.setValue("loc_prov", container.locality.provider);
                }
                if (container.locality != null && container.locality.accuracy > 0.0d) {
                    event.setValue("loc_accy", container.locality.accuracy);
                }
                if (container.locality != null && container.locality.getAge() > 0) {
                    event.setValue("loc_age", container.locality.getAge());
                }
                if (isNotEmpty(container.mimeType)) {
                    event.setValue("media_mime", container.mimeType);
                    event.setValue("media_size", container.size);
                }
                if (isNotEmpty(container.shareId)) {
                    event.setValue("share_id", container.shareId);
                }
                if (container.transferStatus != null) {
                    if (container.transferStatus.startTime > 0) {
                        event.setValue("xfer_start_ts", container.transferStatus.startTime);
                    }
                    if (container.transferStatus.stopTime > 0) {
                        event.setValue("xfer_stop_ts", container.transferStatus.stopTime);
                    }
                }
                if (status == Status.FAIL) {
                    event.setValue("err_type", container.actionResult.getErrorType().toString());
                    event.setValue("err_text", container.actionResult.getErrorText());
                    if (container.actionResult.getHttpStatusCode() > 0) {
                        event.setValue("http_code", container.actionResult.getHttpStatusCode());
                    }
                }
                logEvent(event);
            }
        }
    }

    public void logGroupEvent(GroupState info) {
        GroupStats[] arr$;
        CheckinEventWrapper event = createEvent("ZapZone", "1.0");
        if (event != null) {
            event.setValue("app_ver", PackageUtils.getAppVersion(this.mContext));
            event.setValue("dev_info", DeviceUtils.getDeviceModelInfo());
            event.setValue("priv", isNotEmpty(info.getPin()) ? "t" : "f");
            event.setValue("owner", info.isUserOwner() ? "t" : "f");
            event.setValue("man_stop", info.isStoppedByUser() ? "t" : "f");
            event.setValue("start_ts", info.getStartedTime());
            event.setValue("stop_ts", info.getStoppedTime());
            for (GroupStats stats : GroupStats.values()) {
                int count = info.getStatsCount(stats);
                if (count > 0) {
                    event.setValue(stats.attribute(), count);
                }
            }
            logEvent(event);
        }
    }

    public void logZapCancelShare(ContentContainer container) {
        Status status = container.actionResult.getStatus();
        CheckinEventWrapper event = createEvent("ZapSent", "1.1");
        if (event != null) {
            event.setValue("app_ver", PackageUtils.getAppVersion(this.mContext));
            event.setValue("priv", container.pin != null ? "t" : "f");
            event.setValue("wifi", "wifi".equalsIgnoreCase(getNetworkType(container)) ? "t" : "f");
            event.setValue("share_id", notNull(container.shareId));
            event.setValue("trig_ts", container.creationTime);
            event.setValue(CheckinInterface.KEY_MODALITY, getModality(container));
            if (status == Status.SUCCESS) {
                event.setValue("xfer_status", Status.CANCELED.toString());
            } else {
                event.setValue("xfer_status", status.toString());
            }
            if (status == Status.FAIL) {
                event.setValue("err_type", container.actionResult.getErrorType().toString());
                event.setValue("err_text", container.actionResult.getErrorText());
                if (container.actionResult.getHttpStatusCode() > 0) {
                    event.setValue("http_code", container.actionResult.getHttpStatusCode());
                }
            }
            logEvent(event);
        }
    }

    public void logZapAbuse(ContentContainer container) {
        Status status = container.actionResult.getStatus();
        CheckinEventWrapper event = createEvent("ZapAbuse", "1.1");
        if (event != null) {
            event.setValue("share_id", notNull(container.shareId));
            event.setValue("trig_ts", container.creationTime);
            event.setValue("xfer_status", status.toString());
            event.setValue("abuse_type", container.abuseType.toString());
            if (status == Status.FAIL) {
                event.setValue("err_type", container.actionResult.getErrorType().toString());
                event.setValue("err_text", container.actionResult.getErrorText());
            }
            logEvent(event);
        }
    }

    public void logSettingsSync(Operation operation) {
        Status status = operation.actionResult.getStatus();
        CheckinEventWrapper event = createEvent("ZapSettingsSync", "1.1");
        if (event != null) {
            event.setValue("app_ver", PackageUtils.getAppVersion(this.mContext));
            event.setValue("wifi", "wifi".equalsIgnoreCase(operation.networkType) ? "t" : "f");
            event.setValue("trig_ts", operation.creationTime);
            event.setValue("xfer_status", status.toString());
            if (status == Status.FAIL) {
                event.setValue("err_type", operation.actionResult.getErrorType().toString());
                event.setValue("err_text", operation.actionResult.getErrorText());
                if (operation.actionResult.getHttpStatusCode() > 0) {
                    event.setValue("http_code", operation.actionResult.getHttpStatusCode());
                }
            }
            logEvent(event);
        }
    }

    public void logZapFeatureOptIn(boolean optedIn) {
        CheckinEventWrapper event = createEvent("ZapSettings", "1.1");
        if (event != null) {
            event.setValue("opted_in", optedIn ? "t" : "f");
            logEvent(event);
        }
    }

    public void logZapEnabledEvent(boolean enabled) {
        CheckinEventWrapper event = createEvent("ZapSettings", "1.1");
        if (event != null) {
            event.setValue("enabled", enabled ? "t" : "f");
            logEvent(event);
        }
    }

    public void logWifiEnabledEvent(boolean enabled) {
        CheckinEventWrapper event = createEvent("ZapSettings", "1.1");
        if (event != null) {
            event.setValue("wifi_enabled", enabled ? "t" : "f");
            logEvent(event);
        }
    }

    public void logSettingsLaunchPoint(String launchPoint) {
        CheckinEventWrapper event = createEvent("ZapSettings", "1.1");
        if (event != null) {
            event.setValue("settings_lp", launchPoint);
            logEvent(event);
        }
    }

    public void logShareAppEvent(boolean sendLinkPressed) {
        CheckinEventWrapper event = createEvent("ZapShareApp", "1.0");
        if (event != null) {
            event.setValue("send_pressed", sendLinkPressed ? "t" : "f");
            logEvent(event);
        }
    }

    private CheckinEventWrapper createEvent(String eventName, String version) {
        if (needUpdate()) {
            init();
        }
        if (!this.mEnabled || TextUtils.isEmpty(this.mTag) || !CheckinEventWrapper.isInitialized()) {
            return null;
        }
        return new CheckinEventWrapper(this.mTag, eventName, version, System.currentTimeMillis());
    }

    private void logEvent(CheckinEventWrapper event) {
        if (event != null) {
            event.publish(this.mContext.getContentResolver());
        }
    }

    private static String getModality(ContentContainer container) {
        String modality = container.getCheckinData(CheckinInterface.KEY_MODALITY);
        return TextUtils.isEmpty(modality) ? ModalityHelper.MODALITY_NONE : modality;
    }

    private static String getNetworkType(ContentContainer container) {
        return container.getCheckinData(CheckinInterface.KEY_NETWORK_TYPE);
    }

    private static boolean isNotEmpty(String str) {
        return !TextUtils.isEmpty(str);
    }

    private static String notNull(String str) {
        return str != null ? str : ZapConstants.GOOGLE_ACCOUNT_NONE;
    }
}
