package com.motorola.zap.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.ui.ZapGestureTouchListener.AnimationCallback;
import com.motorola.zap.ui.ZapGestureTouchListener.OnAnimationEndListener;

public class ZapAnimator implements AnimationCallback {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private static final String TAG = ZapAnimator.class.getSimpleName();
    private final int mAnimationTime;
    private PointF mDownPoint1;
    private PointF mDownPoint2;
    /* access modifiers changed from: private */
    public final ImageView mShadowView;

    public ZapAnimator(Context context, ImageView shadowView) {
        this.mAnimationTime = context.getResources().getInteger(17694722);
        this.mShadowView = shadowView;
    }

    public void animateOnZapGestureInit(View view, View listItem, PointF p1) {
    }

    public void animateOnZapGestureStart(View view, View listItem, PointF p1, PointF p2) {
        this.mDownPoint1 = p1;
        this.mDownPoint2 = p2;
        if (listItem != null && listItem.isEnabled()) {
            this.mShadowView.setLayoutParams(new LayoutParams(listItem.getWidth(), listItem.getHeight()));
            View ghostView = listItem.findViewById(R.id.ghost_layout);
            if (ghostView != null) {
                ghostView.setVisibility(0);
                ghostView.setBackgroundColor(listItem.getContext().getResources().getColor(R.color.overlay_blue_50p_alpha));
            }
            int[] location = new int[2];
            listItem.getLocationOnScreen(location);
            this.mShadowView.setTranslationY((float) (location[1] - (listItem.getHeight() / 2)));
            this.mShadowView.setScaleX(1.0f);
            this.mShadowView.setScaleY(1.0f);
            this.mShadowView.setAlpha(1.0f);
            listItem.setDrawingCacheEnabled(true);
            View tv = view.findViewById(R.id.swipe_up_tutorial);
            if (tv != null) {
                tv.setVisibility(4);
            }
            this.mShadowView.setImageBitmap(Bitmap.createBitmap(listItem.getDrawingCache()));
            listItem.setDrawingCacheEnabled(false);
            this.mShadowView.setVisibility(4);
            this.mShadowView.animate().scaleX(0.95f).scaleY(0.95f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animation) {
                    ZapAnimator.this.mShadowView.setScaleX(0.95f);
                    ZapAnimator.this.mShadowView.setScaleY(0.95f);
                }
            });
        }
    }

    public void animateOnZapGestureMove(View view, View listItem, boolean isUp, PointF p1, PointF p2) {
        if (listItem != null && listItem.isEnabled()) {
            float deltaY = ((this.mDownPoint1.y + this.mDownPoint2.y) / 2.0f) - ((p1.y + p2.y) / 2.0f);
            if (DBG) {
                Log.d(TAG, "animateOnZapGestureMove - (deltaY)=( " + deltaY + " )");
            }
            View ghostView = listItem.findViewById(R.id.ghost_layout);
            if (isUp) {
                this.mShadowView.setVisibility(0);
                int[] location = new int[2];
                listItem.getLocationOnScreen(location);
                this.mShadowView.setTranslationY((((float) location[1]) - deltaY) - ((float) (listItem.getHeight() / 2)));
                if (ghostView != null) {
                    ghostView.setVisibility(0);
                    ghostView.setBackgroundColor(listItem.getContext().getResources().getColor(R.color.overlay_blue_30p_alpha));
                }
            } else if (ghostView != null) {
                ghostView.setVisibility(4);
            }
        }
    }

    public void animateOnZapGestureComplete(View view, final View listItem, boolean isUp, float velocity, final OnAnimationEndListener animateEndCallback) {
        if (listItem != null && listItem.isEnabled()) {
            if (isUp) {
                Z.LAST_SWIPE_UP_SUCCESS.write(Boolean.valueOf(true));
                Z.LAST_SWIPE_DOWN_EVENT_FAILED.write(Boolean.valueOf(true));
                this.mShadowView.animate().translationY((float) (-this.mShadowView.getHeight())).setDuration((long) this.mAnimationTime).setListener(new AnimatorListenerAdapter() {
                    private boolean ended = false;

                    public void onAnimationEnd(Animator animation) {
                        if (animateEndCallback != null && !this.ended) {
                            this.ended = true;
                            animateEndCallback.onAnimationEnd();
                            ZapAnimator.this.reset(listItem);
                        }
                    }
                });
                return;
            }
            reset(listItem);
        }
    }

    /* access modifiers changed from: private */
    public void reset(View view) {
        this.mShadowView.setImageBitmap(null);
        this.mShadowView.setVisibility(8);
        if (view != null) {
            View ghostView = view.findViewById(R.id.ghost_layout);
            if (ghostView != null) {
                ghostView.setVisibility(4);
            }
        }
    }

    public void animateOnZapGestureCancel(View view, final View listItem) {
        if (listItem != null && listItem.isEnabled()) {
            if (this.mShadowView.getVisibility() == 0) {
                Z.LAST_SWIPE_UP_SUCCESS.write(Boolean.valueOf(false));
                Z.LAST_SWIPE_DOWN_EVENT_FAILED.write(Boolean.valueOf(false));
            } else {
                Z.LAST_SWIPE_DOWN_SUCCESS.write(Boolean.valueOf(false));
                Z.LAST_SWIPE_DOWN_EVENT_FAILED.write(Boolean.valueOf(true));
            }
            int[] location = new int[2];
            listItem.getLocationInWindow(location);
            this.mShadowView.animate().translationX(listItem.getX()).translationY((float) location[1]).setDuration((long) this.mAnimationTime).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animation) {
                    ZapAnimator.this.mShadowView.setImageBitmap(null);
                    ZapAnimator.this.mShadowView.setVisibility(8);
                    View ghostView = listItem.findViewById(R.id.ghost_layout);
                    if (ghostView != null) {
                        ghostView.setVisibility(4);
                    }
                }
            });
        }
    }
}
