package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.d;
import java.util.Map;

class c extends aj {
    private static final String ID = a.ADVERTISING_TRACKING_ENABLED.toString();
    private final a TC;

    public c(Context context) {
        this(a.E(context));
    }

    c(a aVar) {
        super(ID, new String[0]);
        this.TC = aVar;
    }

    public boolean iy() {
        return false;
    }

    public d.a u(Map<String, d.a> map) {
        return di.r(Boolean.valueOf(this.TC.isLimitAdTrackingEnabled()));
    }
}
