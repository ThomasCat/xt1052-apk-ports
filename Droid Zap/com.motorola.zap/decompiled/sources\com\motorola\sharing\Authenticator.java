package com.motorola.sharing;

import android.text.TextUtils;

public interface Authenticator {

    public interface Callback {
        void onAuthComplete(Status status, TokenInfo tokenInfo, Object obj);
    }

    public enum Status {
        SUCCESS,
        ERROR_NETWORK,
        ERROR_NOT_AUTHORIZED,
        ERROR_GENERAL
    }

    public static class TokenInfo {
        private String mAccount = null;
        private String mToken = null;
        private String mUserId = null;
        private long mValidUntil = 0;

        public String getAccount() {
            return this.mAccount;
        }

        public void setAccount(String account) {
            this.mAccount = account;
        }

        public String getToken() {
            return this.mToken;
        }

        public void setToken(String token) {
            this.mToken = token;
        }

        public String getUserId() {
            return this.mUserId;
        }

        public void setUserId(String userId) {
            this.mUserId = userId;
        }

        public long getValidUntil() {
            return this.mValidUntil;
        }

        public void setValidUntil(long validUntil) {
            this.mValidUntil = validUntil;
        }

        public boolean isEmpty() {
            return TextUtils.isEmpty(this.mAccount) || TextUtils.isEmpty(this.mToken) || TextUtils.isEmpty(this.mUserId);
        }

        public boolean isValid() {
            String account = GCSP.getAuthenticator().getAccount();
            if (TextUtils.isEmpty(account) || !TextUtils.equals(account, this.mAccount) || TextUtils.isEmpty(this.mToken) || TextUtils.isEmpty(this.mUserId) || System.currentTimeMillis() >= this.mValidUntil) {
                return false;
            }
            return true;
        }
    }

    void authenticate(Callback callback, Object obj);

    void authenticate(Callback callback, Object obj, boolean z);

    String getAccount();

    TokenInfo getTokenInfo();
}
