package com.google.android.gms.common.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class c<T extends SafeParcelable> extends DataBuffer<T> {
    private static final String[] zY = {"data"};
    private final Creator<T> zZ;

    public c(DataHolder dataHolder, Creator<T> creator) {
        super(dataHolder);
        this.zZ = creator;
    }

    /* renamed from: H */
    public T get(int i) {
        byte[] byteArray = this.zU.getByteArray("data", i, 0);
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(byteArray, 0, byteArray.length);
        obtain.setDataPosition(0);
        T t = (SafeParcelable) this.zZ.createFromParcel(obtain);
        obtain.recycle();
        return t;
    }
}
