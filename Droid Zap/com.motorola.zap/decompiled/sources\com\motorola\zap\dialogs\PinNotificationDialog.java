package com.motorola.zap.dialogs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.google.android.gms.drive.DriveFile;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;

public class PinNotificationDialog extends Activity {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private static long TIMEOUT = 30000;
    private final Runnable mFinishRunnable = new Runnable() {
        public void run() {
            PinNotificationDialog.this.finish();
        }
    };
    private final Handler mHandler = new Handler();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (DBG) {
            Log.d("Zap-PinNotifDialog", "onCreate");
        }
        setContentView(R.layout.pin_notification_dialog);
        findViewById(R.id.notification_dialog_top_view).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                PinNotificationDialog.this.finish();
            }
        });
        ((TextView) findViewById(R.id.pin_notification_action_text)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(PinNotificationDialog.this, PinInputActivity.class);
                intent.setFlags(DriveFile.MODE_READ_ONLY);
                PinNotificationDialog.this.startActivity(intent);
                PinNotificationDialog.this.finish();
            }
        });
        this.mHandler.postDelayed(this.mFinishRunnable, TIMEOUT);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mHandler.removeCallbacks(this.mFinishRunnable);
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        if (DBG) {
            Log.d("Zap-PinNotifDialog", "onNewIntent");
        }
        this.mHandler.removeCallbacks(this.mFinishRunnable);
        this.mHandler.postDelayed(this.mFinishRunnable, TIMEOUT);
    }
}
