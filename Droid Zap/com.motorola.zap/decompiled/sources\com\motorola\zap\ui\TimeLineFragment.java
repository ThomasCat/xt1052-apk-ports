package com.motorola.zap.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.LoaderManager.LoaderCallbacks;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Video.Media;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.plus.PlusShare;
import com.motorola.sharing.CastConnectionListener;
import com.motorola.sharing.CastManager;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupManager.GroupInfo;
import com.motorola.sharing.GroupManager.ShareInviteParams;
import com.motorola.sharing.Profile;
import com.motorola.sharing.util.FileUtils;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConfig;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.app.ActionReceiver;
import com.motorola.zap.app.App;
import com.motorola.zap.app.InAppNotifCenter;
import com.motorola.zap.app.InAppNotifCenter.ICallbacks;
import com.motorola.zap.app.ZapTracker;
import com.motorola.zap.app.ZapTracker.LockedContentListener;
import com.motorola.zap.app.ZapTracker.ZapRefreshListener;
import com.motorola.zap.app.ZapTracker.ZzListener;
import com.motorola.zap.app.ZzCameraWatcher;
import com.motorola.zap.cacheutils.ZapImageCache;
import com.motorola.zap.dialogs.DialogMessageActivity;
import com.motorola.zap.provider.HistoryHandler;
import com.motorola.zap.provider.TCH;
import com.motorola.zap.provider.ZapContract.History;
import com.motorola.zap.provider.ZapContract.History.Columns;
import com.motorola.zap.provider.ZapContract.History.Timeline;
import com.motorola.zap.provider.ZapContract.History.ZapZoneDetail;
import com.motorola.zap.provider.ZapContract.History.ZapZones;
import com.motorola.zap.ui.DroidBlastLauncher.UserActivityListener;
import com.motorola.zap.ui.ZapGestureTouchListener.ZapGestureListener;
import com.motorola.zap.zz.ui.ZzLocationQueue;
import com.motorola.zap.zz.ui.ZzPoller.PollingReceiver;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TimeLineFragment extends Fragment implements ICallbacks, LoaderCallbacks<Cursor>, LockedContentListener, UserActivityListener, CastConnectionListener, ZapRefreshListener {
    /* access modifiers changed from: private */
    public static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    public static final int DETAILED_VIEW = 3;
    public static String ID = "ID";
    public static String OWNER = "OWNER";
    public static final int PHOTOS_VIEW = 4;
    /* access modifiers changed from: private */
    public static final String TAG = TimeLineFragment.class.getSimpleName();
    public static final int TIME_LINE_VIEW = 1;
    public static String TITLE = PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE;
    public static final String TYPE = "type";
    public static final int VIDEOS_VIEW = 5;
    public static final int ZAP_ZONE_VIEW = 2;
    OnClickListener cancelChromecastToolTip = new OnClickListener() {
        public void onClick(View view) {
            Z.SHOW_CHROMECAST_TOOLTIP.write(Boolean.valueOf(false));
            TimeLineFragment.this.mCcAvailableToolTip.setVisibility(8);
        }
    };
    /* access modifiers changed from: private */
    public boolean isSwipeDownTutorialShown = true;
    OnClickListener mActiveInAppClickListener = new OnClickListener() {
        public void onClick(View v) {
            boolean z = true;
            if (v.getId() == R.id.zz_end) {
                ZzLocationQueue.getInstance().zzLeave();
                TimeLineFragment.this.dismissPopupWindow();
            } else if (v.getId() == R.id.zz_context_menu) {
                TimeLineFragment.this.initiatePopupWindow(v);
            } else if (v.getId() == R.id.zap_lock_reject) {
                ZapTracker.getInstance().setLockedContentAvailable(false);
            } else if (v.getId() == R.id.zz_reshare) {
                ZzLocationQueue.getInstance().zzShareInvite(new ShareInviteParams());
                TimeLineFragment.this.dismissPopupWindow();
            } else if (v.getId() == R.id.zz_auto_share) {
                CheckedTextView cb = (CheckedTextView) v;
                if (cb.isChecked()) {
                    z = false;
                }
                cb.setChecked(z);
                TimeLineFragment.this.updateAutoShare(cb.isChecked());
                TimeLineFragment.this.dismissPopupWindow();
            } else if (v.getId() == R.id.zz_reduce_image) {
                CheckedTextView cb2 = (CheckedTextView) v;
                if (cb2.isChecked()) {
                    z = false;
                }
                cb2.setChecked(z);
                TimeLineFragment.this.updateReducedImageOption(cb2.isChecked());
                TimeLineFragment.this.dismissPopupWindow();
            }
        }
    };
    /* access modifiers changed from: private */
    public View mActiveZzone = null;
    /* access modifiers changed from: private */
    public HistoryCursorAdapter mAdapter;
    private ImageView mCancelCcToolTip = null;
    OnClickListener mCancelFirstZapMenuTutorialToolTip = new OnClickListener() {
        public void onClick(View view) {
            TimeLineFragment.this.mFirstZapTip.setVisibility(8);
        }
    };
    OnClickListener mCancelZZMenuTutorialToolTip = new OnClickListener() {
        public void onClick(View view) {
            TimeLineFragment.this.mZZMenuTutorialTip.setVisibility(8);
            Z.SHOW_ZZ_TIP_FOR_SESSION.write(Boolean.valueOf(false));
        }
    };
    /* access modifiers changed from: private */
    public View mCcAvailableToolTip = null;
    ConcentricCircles mConcentricCircles;
    private String mCurrentZapCameraFile;
    private Dialog mDialog = null;
    DragZapAnimator mDragZapAnimator;
    /* access modifiers changed from: private */
    public View mFirstZapTip = null;
    private ImageView mImageView;
    /* access modifiers changed from: private */
    public ListView mListView;
    /* access modifiers changed from: private */
    public View mLockedContent = null;
    OnEditorActionListener mOnTextEditor = new OnEditorActionListener() {
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (event != null || actionId != 6) {
                return false;
            }
            String pin = v.getText().toString();
            if (pin.length() < 4) {
                ZapTracker.getInstance().onZapEvent(TimeLineFragment.this.getString(R.string.incorrect_input_pin));
                return true;
            }
            TimeLineFragment.this.handleInput(pin);
            ((InputMethodManager) App.getAppContext().getSystemService("input_method")).hideSoftInputFromWindow(v.getWindowToken(), 0);
            ZapTracker.getInstance().setLockedContentAvailable(false);
            return true;
        }
    };
    private ProgressBarHelper mProgressBarHelper;
    private ProgressBar mRefreshBar = null;
    /* access modifiers changed from: private */
    public ViewGroup mRootView = null;
    private ShareTimeHelper mShareTimeHelper;
    OnClickListener mShareZapAppListener = new OnClickListener() {
        public void onClick(View view) {
            DroidBlastLauncher launcher = (DroidBlastLauncher) TimeLineFragment.this.getActivity();
            if (launcher != null) {
                launcher.invokeShareAppDialogFragment();
            }
        }
    };
    private final BroadcastReceiver mStorageStateReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Uri uri = intent.getData();
            String path = uri != null ? uri.getPath() : null;
            if (path != null && TextUtils.equals(path, FileUtils.getSdcardPath())) {
                TimeLineFragment.this.getLoaderManager().restartLoader(TimeLineFragment.this.mType, TimeLineFragment.this.getArguments(), TimeLineFragment.this);
            }
        }
    };
    View mSwipeDownTutorialLayout = null;
    /* access modifiers changed from: private */
    public View mSwipeUpPlusHint = null;
    Handler mTutorialHanlder = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    if (!TimeLineFragment.this.isSwipeDownTutorialShown) {
                        TimeLineFragment.this.launchSwipeDownTutorials();
                        return;
                    }
                    return;
                case 2:
                    if (TimeLineFragment.this.isResumed()) {
                        LayoutInflater inflater = LayoutInflater.from(TimeLineFragment.this.getActivity());
                        if (TimeLineFragment.this.mSwipeUpPlusHint != null) {
                            TimeLineFragment.this.mRootView.removeView(TimeLineFragment.this.mSwipeUpPlusHint);
                        }
                        TimeLineFragment.this.mSwipeUpPlusHint = inflater.inflate(R.layout.swipe_up_tutorial_plus, TimeLineFragment.this.mRootView, false);
                        TimeLineFragment.this.mSwipeUpPlusHint.setY(BitmapDescriptorFactory.HUE_RED);
                        TimeLineFragment.this.mRootView.addView(TimeLineFragment.this.mSwipeUpPlusHint);
                        TimeLineFragment.this.mTutorialHanlder.sendEmptyMessageDelayed(3, 10000);
                        return;
                    }
                    return;
                case 3:
                    if (TimeLineFragment.this.isResumed()) {
                        if (TimeLineFragment.this.mSwipeUpPlusHint != null) {
                            TimeLineFragment.this.mRootView.removeView(TimeLineFragment.this.mSwipeUpPlusHint);
                        }
                        Z.SWIPE_UP_HINT_TO_BE_SHOWN.write(Boolean.valueOf(false));
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    int mType = 1;
    View mWalkthroughEmptyState = null;
    PopupWindow mWindow = null;
    /* access modifiers changed from: private */
    public View mZZMenuTutorialTip = null;
    ZapGestureTouchListener mZapGestureTouchListener = null;
    private final ZapGestureListener mZapListener = new ZapGestureListener() {
        public void onVerticalZap(boolean isUp, View view, int position) {
            if (!isUp) {
                if (TimeLineFragment.this.mLockedContent.findViewById(R.id.error_msg).getVisibility() == 0) {
                    TimeLineFragment.this.mLockedContent.setVisibility(8);
                    TimeLineFragment.this.reCalculateLayoutMargins();
                }
                if (!TimeLineFragment.this.isSwipeDownTutorialShown) {
                    TimeLineFragment.this.onSwipeDownTutorialComplete();
                } else {
                    TimeLineFragment.this.initiateRefresh();
                }
            } else if (position >= 0 && position < TimeLineFragment.this.mAdapter.getCount()) {
                Cursor cursor = (Cursor) TimeLineFragment.this.mListView.getAdapter().getItem(position);
                if (cursor == null) {
                    return;
                }
                if (ZapHistoryInfo.getCardType(cursor) == 5) {
                    if (TimeLineFragment.DBG) {
                        Log.i(TimeLineFragment.TAG, "Resharing Active invite");
                    }
                    ZzLocationQueue.getInstance().zzShareInvite(new ShareInviteParams());
                    return;
                }
                if (TCH.isDemoCard(cursor) && ((Boolean) Z.SWIPE_UP_HINT_TO_BE_SHOWN.read()).booleanValue()) {
                    TimeLineFragment.this.mTutorialHanlder.sendEmptyMessageDelayed(2, 2000);
                }
                TimeLineFragment.this.sendCard(position);
            }
        }

        public void onHorizontalZap(boolean isLefttoRight) {
        }
    };
    private EditText mZzCode = null;
    private final ZzListener mZzListener = new ZzListener() {
        public void onZzParticipantCountChange(final int count) {
            Activity activity = TimeLineFragment.this.getActivity();
            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        if (TimeLineFragment.DBG) {
                            Log.i(TimeLineFragment.TAG, "onZzParticipantCountChange =" + count);
                        }
                        TextView textZzInfo = (TextView) TimeLineFragment.this.mActiveZzone.findViewById(R.id.zz_guest_count);
                        if (textZzInfo != null && TimeLineFragment.this.isResumed()) {
                            textZzInfo.setText(String.valueOf(count));
                        }
                    }
                });
            }
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (DBG) {
            Log.i(TAG, "Inside onCreateView");
        }
        setHasOptionsMenu(true);
        this.mRootView = (ViewGroup) inflater.inflate(R.layout.zapcards, container, false);
        this.mImageView = (ImageView) this.mRootView.findViewById(R.id.empty);
        this.mListView = (ListView) this.mRootView.findViewById(R.id.activity_googlecards_listview);
        this.mLockedContent = this.mRootView.findViewById(R.id.zap_locked);
        this.mActiveZzone = this.mRootView.findViewById(R.id.zap_active);
        this.mType = getArguments().getInt(TYPE, 1);
        ActionBar actionbar = getActivity().getActionBar();
        if (this.mType == 3) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeButtonEnabled(true);
            actionbar.setDisplayShowTitleEnabled(true);
            ((DroidBlastLauncher) getActivity()).getDrawerToggle().setDrawerIndicatorEnabled(false);
            getActivity().findViewById(R.id.left_drawer).setVisibility(8);
            actionbar.setTitle(getResources().getStringArray(R.array.default_navigation_drawer_list)[0]);
        } else {
            ((DroidBlastLauncher) getActivity()).getDrawerToggle().setDrawerIndicatorEnabled(true);
        }
        if (savedInstanceState != null) {
            this.mCurrentZapCameraFile = savedInstanceState.getString("zap_file_name");
        }
        this.mRefreshBar = (ProgressBar) this.mRootView.findViewById(R.id.refresh_bar);
        this.mProgressBarHelper = new ProgressBarHelper();
        this.mShareTimeHelper = new ShareTimeHelper();
        this.mProgressBarHelper.registerListener();
        this.mAdapter = new HistoryCursorAdapter(getActivity(), null, this.mType, this.mProgressBarHelper, this.mShareTimeHelper);
        this.mListView.setAdapter(this.mAdapter);
        this.mCcAvailableToolTip = this.mRootView.findViewById(R.id.chromecast_btn_tip);
        this.mCancelCcToolTip = (ImageView) this.mCcAvailableToolTip.findViewById(R.id.imageView_clearToast);
        this.mCancelCcToolTip.setOnClickListener(this.cancelChromecastToolTip);
        this.mZZMenuTutorialTip = this.mRootView.findViewById(R.id.tooltip_zz_active);
        ((ImageView) this.mZZMenuTutorialTip.findViewById(R.id.zz_clear_tooltip)).setOnClickListener(this.mCancelZZMenuTutorialToolTip);
        this.mZZMenuTutorialTip.findViewById(R.id.share_app).setOnClickListener(this.mShareZapAppListener);
        this.mFirstZapTip = this.mRootView.findViewById(R.id.tooltip_first_zap);
        ((ImageView) this.mFirstZapTip.findViewById(R.id.clear_tooltip)).setOnClickListener(this.mCancelFirstZapMenuTutorialToolTip);
        this.mFirstZapTip.findViewById(R.id.share_app).setOnClickListener(this.mShareZapAppListener);
        if (ZapConfig.supportsChromecast()) {
            if (CastManager.getInstance().isChromecastAvailable().booleanValue() && ((Boolean) Z.SHOW_CHROMECAST_TOOLTIP.read()).booleanValue()) {
                Z.SHOW_CHROMECAST_TOOLTIP.write(Boolean.valueOf(false));
                this.mCcAvailableToolTip.setVisibility(0);
            }
            CastManager.getInstance().registerCastConnectionListener(this);
        }
        getLoaderManager().initLoader(this.mType, getArguments(), this);
        this.mListView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (TimeLineFragment.DBG) {
                    Log.d(TimeLineFragment.TAG, "onItemClick position = " + position);
                }
                if (TimeLineFragment.this.mZapGestureTouchListener == null || TimeLineFragment.this.mZapGestureTouchListener.getPointerCount() <= 1) {
                    Cursor cursor = (Cursor) TimeLineFragment.this.mListView.getAdapter().getItem(position);
                    if (cursor != null && !TCH.isDemoCard(cursor)) {
                        switch (ZapHistoryInfo.getCardType(cursor)) {
                            case 0:
                            case 1:
                                ZapUtils.viewImage(TimeLineFragment.this.getActivity(), TCH.getFilePath(cursor));
                                return;
                            case 2:
                            case 3:
                                TimeLineFragment.this.launchZapZoneDetailView(position);
                                return;
                            default:
                                return;
                        }
                    }
                }
            }
        });
        this.mZapGestureTouchListener = new ZapGestureTouchListener(getActivity());
        this.mZapGestureTouchListener.setListView(this.mListView);
        this.mZapGestureTouchListener.registerAnimationCallback(new ZapAnimator(getActivity(), (ImageView) this.mRootView.findViewById(R.id.animation_shadow_view)));
        this.mZapGestureTouchListener.registerZapGestureListener(this.mZapListener);
        this.mListView.setOnTouchListener(this.mZapGestureTouchListener);
        addStaticDragIcons(this.mRootView, this.mZapGestureTouchListener);
        ZapGestureTouchListener lActiveZoneTouchListener = new ZapGestureTouchListener(getActivity());
        lActiveZoneTouchListener.registerAnimationCallback(new ZapAnimator(getActivity(), (ImageView) this.mRootView.findViewById(R.id.animation_shadow_view)));
        this.mActiveZzone.setOnTouchListener(lActiveZoneTouchListener);
        addStaticDragIcons(this.mRootView, lActiveZoneTouchListener);
        this.mZzCode = (EditText) this.mLockedContent.findViewById(R.id.zz_code);
        this.mZzCode.setOnEditorActionListener(this.mOnTextEditor);
        return this.mRootView;
    }

    /* access modifiers changed from: private */
    public void launchZapZoneDetailView(int position) {
        Cursor cursor = (Cursor) this.mListView.getAdapter().getItem(position);
        if (cursor != null) {
            Fragment fragment = new TimeLineFragment();
            Bundle args = new Bundle();
            args.putString(ZapConstants.CONTENT_TITLE, TCH.getZzName(cursor));
            args.putInt(TYPE, 3);
            args.putString(ID, TCH.getZzId(cursor));
            args.putString(OWNER, TCH.getZzOwner(cursor));
            fragment.setArguments(args);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.addToBackStack(null);
            ft.commit();
            getActivity().getActionBar().setTitle(TCH.getZzName(cursor));
        }
    }

    /* access modifiers changed from: private */
    public void sendCard(int position) {
        Cursor cursor = (Cursor) this.mListView.getAdapter().getItem(position);
        if (cursor != null) {
            int cardType = ZapHistoryInfo.getCardType(cursor);
            if (cardType != 2 && cardType != 3) {
                String filePath = TCH.getFilePath(cursor);
                if (filePath == null || !FileUtils.isFileExists(filePath)) {
                    ZapTracker.getInstance().onZapEvent(getString(R.string.toast_msg_media_deleted));
                    return;
                }
                Intent i = new Intent(ZapConstants.ACTION_GS_GALLERY_PHOTO_SHARED);
                i.putExtra(ZapConstants.EXTRA_GS_GALLERY_PHOTO_SHARED, "file://" + filePath);
                i.putExtra(ZapConstants.EXTRA_EVENT_ORIGIN_ZAP_APP, true);
                App.getAppContext().sendBroadcast(i);
            }
        }
    }

    public void onResume() {
        super.onResume();
        if (this.mDialog != null && this.mDialog.isShowing()) {
            this.mDialog.dismiss();
        }
        this.mDialog = null;
        if (!((Boolean) Z.FEATURE_ENABLED.read()).booleanValue()) {
            if (DBG) {
                Log.d(TAG, "Feature disabled");
            }
            showZapDisabledDialog(getActivity());
            return;
        }
        InAppNotifCenter.getInAppNotifCenter().registerForEvents(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
        getActivity().registerReceiver(this.mStorageStateReceiver, intentFilter);
        if (getActivity() instanceof DroidBlastLauncher) {
            ((DroidBlastLauncher) getActivity()).addListener(this);
        }
        ZapTracker.getInstance().addListener((LockedContentListener) this);
        ZapTracker.getInstance().addRefreshListener(this);
        ZapTracker.getInstance().addListener(this.mZzListener);
        checkLockedContent();
        getNotificationManager().cancel("zap", 0);
        resetInActivityTimer();
        checkTutorialLayout();
        refreshVisibleItems();
        Intent intent = new Intent(ZapConstants.ACTION_ZZ_POLL_HINT);
        intent.setClass(App.getAppContext(), PollingReceiver.class);
        intent.putExtra(ZapConstants.EXTRA_ZZ_POLL_NOW, true);
        getActivity().sendBroadcast(intent, "com.motorola.blast.permission.TRIGGER_BLAST_ACTION");
        this.mRefreshBar.setVisibility(8);
    }

    private void refreshVisibleItems() {
        new Thread() {
            public void run() {
                int firstVisiblePostion = TimeLineFragment.this.mListView.getFirstVisiblePosition();
                int lastVisiblePosition = TimeLineFragment.this.mListView.getLastVisiblePosition();
                int i = firstVisiblePostion;
                while (i <= lastVisiblePosition && TimeLineFragment.this.isResumed()) {
                    Cursor cursor = (Cursor) TimeLineFragment.this.mListView.getAdapter().getItem(i);
                    if (cursor != null) {
                        String fileName = TCH.getFilePath(cursor);
                        if (fileName != null && !FileUtils.isFileExists(fileName)) {
                            new HistoryHandler().setDeleted((long) TCH.getId(cursor));
                        }
                        i++;
                    } else {
                        return;
                    }
                }
            }
        }.start();
    }

    private void checkTutorialLayout() {
        this.isSwipeDownTutorialShown = ((Boolean) Z.SWIPE_DOWN_TUTORIAL_SHOWN.read()).booleanValue();
        if (!this.isSwipeDownTutorialShown) {
            this.mSwipeDownTutorialLayout.setVisibility(0);
            this.mSwipeDownTutorialLayout.findViewById(R.id.tutorial_text).setVisibility(0);
            reCalculateLayoutMargins();
        } else {
            this.mSwipeDownTutorialLayout.setVisibility(4);
            this.mSwipeDownTutorialLayout.findViewById(R.id.tutorial_text).setVisibility(4);
        }
        this.mSwipeDownTutorialLayout.bringToFront();
    }

    private NotificationManager getNotificationManager() {
        return (NotificationManager) App.getAppContext().getSystemService("notification");
    }

    /* access modifiers changed from: private */
    public void checkLockedContent() {
        boolean isAvailable = ZapTracker.getInstance().getLockedContenAvailable();
        resetInActivityTimer();
        if (isAvailable) {
            if (this.mAdapter != null) {
                this.mAdapter.dismissPopupWindow();
            }
            this.mLockedContent.setVisibility(0);
            this.mZzCode.setText(ZapConstants.GOOGLE_ACCOUNT_NONE);
            this.mLockedContent.findViewById(R.id.error_msg).setVisibility(8);
            this.mLockedContent.findViewById(R.id.locked_zap_editor).setVisibility(0);
            this.mLockedContent.findViewById(R.id.zap_lock_reject).setVisibility(0);
            this.mLockedContent.findViewById(R.id.zap_lock_reject).setOnClickListener(this.mActiveInAppClickListener);
        } else {
            this.mLockedContent.setVisibility(8);
        }
        reCalculateLayoutMargins();
        handleEmpty();
    }

    /* access modifiers changed from: private */
    public void checkActiveZapZone() {
        if (GCSP.getGroupManager().isAnyGroupActive()) {
            this.mActiveZzone.setVisibility(0);
            boolean equals = TextUtils.equals(GCSP.getGroupManager().getGroupInfo(null).owner, GCSP.getAuthenticator().getAccount());
            if (((Boolean) Z.SHOW_ZZ_TIP_FOR_SESSION.read()).booleanValue() && equals) {
                this.mZZMenuTutorialTip.setVisibility(0);
            }
            TextView textView = (TextView) this.mActiveZzone.findViewById(R.id.zz_name);
            GroupInfo groupInfo = GCSP.getGroupManager().getGroupInfo(null);
            if (groupInfo != null) {
                ZapTracker.getInstance().setLockedContentAvailable(false);
                ImageView imageView = (ImageView) this.mActiveZzone.findViewById(R.id.zz_context_menu);
                Profile profileForActiveZone = new HistoryHandler().getProfileForActiveZone(groupInfo.owner, groupInfo.id);
                textView.setText(groupInfo.name);
                ImageView imageView2 = (ImageView) this.mActiveZzone.findViewById(R.id.zz_thumbnail);
                TextView textView2 = (TextView) this.mActiveZzone.findViewById(R.id.zz_guest_count);
                TextView textView3 = (TextView) this.mActiveZzone.findViewById(R.id.zz_active_code);
                if (profileForActiveZone == null || TextUtils.isEmpty(profileForActiveZone.pictureUrl) || (profileForActiveZone.anonymous && !equals)) {
                    imageView2.setImageResource(R.drawable.ic_zap_lockedzapzone);
                } else {
                    ZapImageCache.getInstance().getImageLoader().get(profileForActiveZone.pictureUrl, ImageLoader.getImageListener(imageView2, R.drawable.ic_zap_lockedzapzone, R.drawable.ic_zap_lockedzapzone));
                }
                String str = groupInfo.pin;
                textView2.setText(String.valueOf(groupInfo.memberCount));
                if (str == null || str.isEmpty()) {
                    textView3.setVisibility(8);
                } else {
                    textView3.setVisibility(0);
                    textView3.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_zap_locked), null, null, null);
                    textView3.setText(str);
                }
                imageView.setSelected(false);
                imageView.setClickable(true);
                imageView.setOnClickListener(this.mActiveInAppClickListener);
                if (!this.mActiveZzone.equals(null)) {
                    this.mActiveZzone.setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            Fragment fragment = new ZapZoneParticipantFragment();
                            if (fragment != null) {
                                FragmentTransaction transaction = TimeLineFragment.this.getFragmentManager().beginTransaction();
                                transaction.replace(R.id.content_frame, fragment);
                                transaction.addToBackStack(null);
                                transaction.commit();
                            }
                        }
                    });
                }
            } else {
                return;
            }
        } else {
            this.mActiveZzone.setVisibility(8);
            this.mZZMenuTutorialTip.setVisibility(8);
            Z.SHOW_ZZ_TIP_FOR_SESSION.write(Boolean.valueOf(true));
        }
        reCalculateLayoutMargins();
    }

    public void onPause() {
        super.onPause();
        if (this.mDialog != null && this.mDialog.isShowing()) {
            this.mDialog.dismiss();
        }
        if (((Boolean) Z.FEATURE_ENABLED.read()).booleanValue()) {
            InAppNotifCenter.getInAppNotifCenter().deregisterForEvents(this);
            getActivity().unregisterReceiver(this.mStorageStateReceiver);
            ZapTracker.getInstance().removeListener((LockedContentListener) this);
            ZapTracker.getInstance().removeRefreshListener(this);
            ZapTracker.getInstance().removeListener(this.mZzListener);
            if (getActivity() instanceof DroidBlastLauncher) {
                ((DroidBlastLauncher) getActivity()).removeListener(this);
            }
            cleanUpAllMessages();
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("zap_file_name", this.mCurrentZapCameraFile);
    }

    /* access modifiers changed from: private */
    public void initiateRefresh() {
        if (this.mRefreshBar.getVisibility() != 0) {
            Intent intent = new Intent(ZapConstants.ACTION_GS_SWIPE_DOWN);
            intent.setClass(App.getAppContext(), ActionReceiver.class);
            intent.putExtra(ZapConstants.EXTRA_EVENT_ORIGIN_ZAP_APP, true);
            App.getAppContext().sendBroadcast(intent);
            ZapTracker.getInstance().onRefresh();
        }
    }

    /* access modifiers changed from: private */
    public void initiatePopupWindow(View view) {
        try {
            Activity activity = getActivity();
            View inflate = LayoutInflater.from(activity).inflate(R.layout.zz_active_overflow_menu, null);
            this.mWindow = new PopupWindow(inflate, ZapUtils.dipToPix(activity, 160), -2, true);
            this.mWindow.setTouchable(true);
            this.mWindow.setFocusable(true);
            this.mWindow.setOutsideTouchable(true);
            this.mWindow.setBackgroundDrawable(new BitmapDrawable());
            inflate.findViewById(R.id.zz_end).setOnClickListener(this.mActiveInAppClickListener);
            GCSP.getGroupManager().getGroupInfo(null);
            inflate.findViewById(R.id.zz_help).setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    TimeLineFragment.this.dismissPopupWindow();
                    FragmentTransaction ft = TimeLineFragment.this.getFragmentManager().beginTransaction();
                    Fragment prev = TimeLineFragment.this.getFragmentManager().findFragmentByTag("help");
                    if (prev != null) {
                        ft.remove(prev);
                    }
                    ft.addToBackStack(null);
                    ft.replace(16908290, new ZzHelpFragment(), "help").commit();
                }
            });
            CheckedTextView checkedTextView = (CheckedTextView) inflate.findViewById(R.id.zz_auto_share);
            checkedTextView.setChecked(ZzCameraWatcher.getInstance().isCameraAutoShareSettingEnabled());
            checkedTextView.setOnClickListener(this.mActiveInAppClickListener);
            CheckedTextView checkedTextView2 = (CheckedTextView) inflate.findViewById(R.id.zz_reduce_image);
            checkedTextView2.setChecked(((Boolean) Z.ZZ_REDUCED_IMAGE.read()).booleanValue());
            checkedTextView2.setOnClickListener(this.mActiveInAppClickListener);
            int[] iArr = new int[2];
            view.getLocationOnScreen(iArr);
            this.mWindow.showAtLocation(view, 0, iArr[0] - ZapUtils.dipToPix(activity, 140), iArr[1] + ZapUtils.dipToPix(activity, 35));
        } catch (Exception e) {
            Log.e(TAG, "Exception while showing popup window");
        }
    }

    public void dismissPopupWindow() {
        if (this.mWindow != null && this.mWindow.isShowing()) {
            this.mWindow.dismiss();
        }
    }

    private void launchGallery() {
        Intent systemGalleryPickerIntent;
        if (!GCSP.getGroupManager().isAnyGroupActive() || ZapConfig.supportVideoInZapZone()) {
            systemGalleryPickerIntent = ZapUtils.getSystemGalleryPickerIntent(App.getAppContext(), "image/* video/*");
        } else {
            systemGalleryPickerIntent = ZapUtils.getSystemGalleryPickerIntent(App.getAppContext(), "image/*");
        }
        if (systemGalleryPickerIntent != null) {
            startActivityForResult(systemGalleryPickerIntent, 1012);
        } else {
            Toast.makeText(App.getAppContext(), "Gallery application not found", 0).show();
        }
    }

    public void launchCamera(Intent intent, int i) {
        Uri zapCameraFileUri = getZapCameraFileUri(intent);
        if (zapCameraFileUri != null) {
            intent.putExtra("output", zapCameraFileUri);
            String defaultCameraPackage = ZapUtils.getDefaultCameraPackage(App.getAppContext());
            if (defaultCameraPackage != null) {
                try {
                    intent.setPackage(defaultCameraPackage);
                } catch (ActivityNotFoundException e) {
                    Intent intent2 = new Intent(getActivity(), DialogMessageActivity.class);
                    intent2.putExtra(ZapConstants.EXTRA_DIALOG_TITLE, getActivity().getString(R.string.app_name));
                    intent2.putExtra(ZapConstants.EXTRA_DIALOG_MESSAGE, getActivity().getString(R.string.toast_msg_camera_err));
                    intent2.setFlags(DriveFile.MODE_READ_ONLY);
                    startActivity(intent2);
                    return;
                }
            }
            startActivityForResult(intent, i);
        } else if (Log.isLoggable(TAG, 6)) {
            Log.e(TAG, "Could not get new file uri");
        }
    }

    private Uri getZapCameraFileUri(Intent intent) {
        String str;
        String format = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ROOT).format(new Date());
        if ("android.media.action.IMAGE_CAPTURE".equals(intent.getAction())) {
            str = "/" + ("ZAP_IMG_" + format) + ".jpg";
        } else if (!"android.media.action.VIDEO_CAPTURE".equals(intent.getAction())) {
            return null;
        } else {
            str = "/" + ("ZAP_VID_" + format) + ".mp4";
        }
        String zapCameraFilePath = ZapUtils.getZapCameraFilePath(str);
        if (zapCameraFilePath == null) {
            return null;
        }
        this.mCurrentZapCameraFile = zapCameraFilePath;
        return Uri.fromFile(new File(zapCameraFilePath));
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        Uri uri;
        Uri fromFile;
        if (DBG) {
            Log.d(TAG, "onActivityResult - requestCode: " + i + " - resultCode: " + i2);
        }
        switch (i) {
            case 1010:
            case 1011:
                if (i2 == -1) {
                    fromFile = Uri.fromFile(new File(this.mCurrentZapCameraFile));
                } else if (this.mCurrentZapCameraFile == null || !new File(this.mCurrentZapCameraFile).exists()) {
                    fromFile = null;
                } else {
                    FileUtils.deleteFile(this.mCurrentZapCameraFile, App.getAppContext());
                    fromFile = null;
                }
                if (fromFile != null) {
                    addToMediaStore(this.mCurrentZapCameraFile, i == 1011);
                    Intent intent2 = new Intent(ZapConstants.ACTION_GS_GALLERY_PHOTO_SHARED);
                    intent2.setClass(App.getAppContext(), ActionReceiver.class);
                    intent2.putExtra(ZapConstants.EXTRA_GS_GALLERY_PHOTO_SHARED, fromFile.toString());
                    intent2.putExtra(ZapConstants.EXTRA_EVENT_ORIGIN_ZAP_APP, true);
                    App.getAppContext().sendBroadcast(intent2);
                }
                this.mCurrentZapCameraFile = null;
                uri = fromFile;
                break;
            case 1012:
                if (i2 != -1 || intent == null) {
                    uri = null;
                } else {
                    uri = intent.getData();
                }
                if (uri != null) {
                    Intent intent3 = new Intent(App.getAppContext(), ShareSupportActivity.class);
                    intent3.setData(uri);
                    startActivity(intent3);
                    break;
                }
                break;
            default:
                uri = null;
                break;
        }
        Log.i(TAG, "Selected Uri: " + uri);
    }

    private void addToMediaStore(final String filepath, final boolean isVideo) {
        new AsyncTask<Void, Void, Void>() {
            /* access modifiers changed from: protected */
            public Void doInBackground(Void... voids) {
                File zapCameraDir = ZapUtils.getZapCameraDir();
                String dir = zapCameraDir.getAbsolutePath();
                ContentValues values = new ContentValues();
                values.put("_data", filepath);
                values.put("bucket_id", Integer.valueOf(dir.hashCode()));
                values.put("bucket_display_name", zapCameraDir.getName());
                values.put("datetaken", Long.valueOf(System.currentTimeMillis()));
                values.put("mime_type", FileUtils.getMimeTypeFromFileName(filepath));
                if (isVideo) {
                    App.getAppContext().getContentResolver().insert(Media.EXTERNAL_CONTENT_URI, values);
                } else {
                    App.getAppContext().getContentResolver().insert(Images.Media.EXTERNAL_CONTENT_URI, values);
                }
                return null;
            }
        }.execute(new Void[0]);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        if (((DroidBlastLauncher) getActivity()).showMenu()) {
            menuInflater.inflate(R.menu.add_zap_action_bar, menu);
            super.onCreateOptionsMenu(menu, menuInflater);
        }
    }

    public void onPrepareOptionsMenu(Menu menu) {
        if (((DroidBlastLauncher) getActivity()).showMenu()) {
            if (GCSP.getGroupManager().isAnyGroupActive()) {
                menu.findItem(R.id.menu_zap_zone_new).setVisible(false);
                menu.findItem(R.id.menu_zap_camera).setVisible(true);
                if (ZapConfig.supportVideoInZapZone()) {
                    menu.findItem(R.id.menu_zap_camcorder).setVisible(true);
                } else {
                    menu.findItem(R.id.menu_zap_camcorder).setVisible(false);
                }
            } else {
                menu.findItem(R.id.menu_zap_zone_new).setVisible(true);
            }
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                getFragmentManager().popBackStack();
                break;
            case R.id.refresh_menu_item /*2131296564*/:
                initiateRefresh();
                break;
            case R.id.menu_zap_camera /*2131296566*/:
                launchCamera(new Intent("android.media.action.IMAGE_CAPTURE"), 1010);
                break;
            case R.id.menu_zap_camcorder /*2131296567*/:
                if (!GCSP.getGroupManager().isAnyGroupActive() || ZapConfig.supportVideoInZapZone()) {
                    launchCamera(new Intent("android.media.action.VIDEO_CAPTURE"), 1011);
                    break;
                }
            case R.id.menu_zap_gallery /*2131296568*/:
                launchGallery();
                break;
            case R.id.menu_zap_zone_new /*2131296569*/:
                if (!ZapConfig.supportsZapZoneCreation()) {
                    Intent intent = new Intent(App.getAppContext(), DialogMessageActivity.class);
                    intent.putExtra(ZapConstants.EXTRA_DIALOG_TITLE, getString(R.string.app_name));
                    intent.putExtra(ZapConstants.EXTRA_DIALOG_MESSAGE, getString(R.string.toast_error_cannot_create_zone_from_this_device));
                    intent.setFlags(DriveFile.MODE_READ_ONLY);
                    App.getAppContext().startActivity(intent);
                    break;
                } else {
                    startActivity(new Intent(App.getAppContext(), ZzSendInviteActivity.class));
                    break;
                }
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: private */
    public void updateReducedImageOption(boolean isChecked) {
        Z.ZZ_REDUCED_IMAGE.write(Boolean.valueOf(isChecked));
    }

    public void updateAutoShare(boolean z) {
        ZzCameraWatcher.getInstance().setCameraAutoShareSettingEnabled(z);
        if (!z) {
            ZapUi.getInstance().showToastMessage(getString(R.string.zz_auto_share_alert), ZapUi.getZzToastIconId(), true, true, true);
        }
    }

    /* access modifiers changed from: 0000 */
    public void showZapDisabledDialog(Context context) {
        Builder builder = new Builder(context);
        builder.setTitle(R.string.app_name);
        builder.setMessage(R.string.enable_zap);
        builder.setPositiveButton(getString(R.string.dialog_yes_button), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                TimeLineFragment.this.startActivity(new Intent(App.getAppContext(), DroidBlastSettingsActivity.class));
            }
        });
        builder.setNegativeButton(getString(R.string.dialog_no_button), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (TimeLineFragment.this.isResumed()) {
                    TimeLineFragment.this.getActivity().finish();
                }
            }
        });
        builder.setOnCancelListener(new OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                if (TimeLineFragment.DBG) {
                    Log.d(TimeLineFragment.TAG, "Dialog - onCancel");
                }
                if (TimeLineFragment.this.isResumed()) {
                    TimeLineFragment.this.getActivity().finish();
                }
            }
        });
        this.mDialog = builder.create();
        this.mDialog.setCanceledOnTouchOutside(false);
        this.mDialog.show();
    }

    private void addStaticDragIcons(ViewGroup viewGroup, ZapGestureTouchListener zapGestureTouchListener) {
        this.mConcentricCircles = new ConcentricCircles((Context) getActivity(), false);
        this.mConcentricCircles.setX(BitmapDescriptorFactory.HUE_RED);
        this.mConcentricCircles.setY(8.0f * getResources().getDisplayMetrics().density);
        this.mConcentricCircles.setVisibleHeight(this.mConcentricCircles.getCircleWidth());
        this.mConcentricCircles.setDirection(false);
        viewGroup.addView(this.mConcentricCircles);
        this.mSwipeDownTutorialLayout = viewGroup.findViewById(R.id.tutorial_swipe_down);
        this.mWalkthroughEmptyState = viewGroup.findViewById(R.id.zap_walkthrough);
        checkTutorialLayout();
        this.mDragZapAnimator = new DragZapAnimator(this.mConcentricCircles, null, this.mSwipeDownTutorialLayout, this.mListView, getResources().getDisplayMetrics().density);
        zapGestureTouchListener.registerAnimationCallback(this.mDragZapAnimator);
        this.mAdapter.setAnimators(this.mListView, this.mDragZapAnimator, this.mZapListener);
        ViewGroupZapGestureTouchListener viewGroupZapGestureTouchListener = new ViewGroupZapGestureTouchListener(getActivity());
        viewGroup.setOnTouchListener(viewGroupZapGestureTouchListener);
        viewGroupZapGestureTouchListener.registerZapGestureListener(this.mZapListener);
        viewGroupZapGestureTouchListener.registerAnimationCallback(this.mDragZapAnimator);
    }

    public void eventChanged() {
        if (isResumed()) {
            getActivity().invalidateOptionsMenu();
            Activity activity = getActivity();
            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        TimeLineFragment.this.checkActiveZapZone();
                    }
                });
            }
        }
    }

    public void onDestroyView() {
        this.mProgressBarHelper.clear();
        this.mProgressBarHelper.unregisterListener();
        this.mShareTimeHelper.clear();
        if (ZapConfig.supportsChromecast()) {
            CastManager.getInstance().unRegisterCastConnectionListener(this);
        }
        super.onDestroyView();
    }

    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String str;
        String[] strArr = null;
        if (DBG) {
            Log.d(TAG, "onCreateLoader, id =" + i);
        }
        if (i == 1) {
            str = Timeline.FULL_SELECTION;
        } else if (i == 2) {
            str = ZapZones.SELECTION;
        } else if (i == 3) {
            str = ZapZoneDetail.SELECTION;
            strArr = new String[]{bundle.getString(OWNER, ZapConstants.GOOGLE_ACCOUNT_NONE), bundle.getString(ID, "- 1")};
        } else {
            str = i == 4 ? Timeline.SELECTION_ONLY_IMAGES : i == 5 ? Timeline.SELECTION_ONLY_VIDEOS : null;
        }
        return new CursorLoader(getActivity(), History.CONTENT_URI, Columns.DEFAULT_PROJECTION, str, strArr, History.INVERSE_CHRONOLOGICAL_ORDER);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0035, code lost:
        if (java.lang.System.currentTimeMillis() < com.motorola.zap.provider.TCH.getMediaExpirationTimestamp(r8)) goto L_0x0037;
     */
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        boolean z = true;
        if (DBG) {
            Log.d(TAG, "onLoadFinished");
        }
        this.mAdapter.swapCursor(cursor);
        if (isResumed()) {
        }
        if (!GCSP.getGroupManager().isAnyGroupActive() && cursor.getCount() == 1) {
            cursor.moveToFirst();
        }
        z = false;
        if (z) {
            this.mFirstZapTip.setVisibility(0);
        } else {
            this.mFirstZapTip.setVisibility(8);
        }
        handleEmpty();
    }

    public void onLoaderReset(Loader<Cursor> loader) {
        if (DBG) {
            Log.d(TAG, "onLoaderReset");
        }
        this.mAdapter.swapCursor(null);
    }

    private void handleEmpty() {
        boolean lockedContenAvailable = ZapTracker.getInstance().getLockedContenAvailable();
        if (this.mAdapter.getCount() != 0 || GCSP.getGroupManager().isAnyGroupActive() || lockedContenAvailable) {
            this.mWalkthroughEmptyState.setVisibility(4);
            this.mImageView.setVisibility(4);
            this.mListView.setVisibility(0);
            return;
        }
        this.mImageView.setVisibility(0);
        this.mListView.setVisibility(4);
        if (this.mType == 5) {
            this.mImageView.setImageResource(R.drawable.ic_empty_video);
        } else if (this.mType == 2) {
            this.mImageView.setImageResource(R.drawable.ic_empty_zz);
        } else if (this.mType == 4) {
            this.mImageView.setImageResource(R.drawable.ic_content_picture_lg);
        } else {
            this.mWalkthroughEmptyState.setVisibility(0);
            this.mImageView.setVisibility(4);
        }
    }

    public void onLockedContent(boolean z) {
        if (DBG) {
            Log.i(TAG, "onLockedContent =" + z);
        }
        if (isResumed()) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    TimeLineFragment.this.checkLockedContent();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void handleInput(String str) {
        Intent intent = new Intent(ZapConstants.ACTION_PIN_INPUT);
        intent.setClass(App.getAppContext(), ActionReceiver.class);
        intent.putExtra(ZapConstants.EXTRA_PIN, str);
        intent.putExtra(ZapConstants.EXTRA_RECOVER_USER_ACTION_INTENT, true);
        App.getAppContext().sendBroadcast(intent, "com.motorola.blast.permission.TRIGGER_BLAST_ACTION");
    }

    public void onWrongPin() {
        if (isResumed()) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    if (TimeLineFragment.this.mAdapter != null) {
                        TimeLineFragment.this.mAdapter.dismissPopupWindow();
                    }
                    TimeLineFragment.this.mLockedContent.setVisibility(0);
                    TimeLineFragment.this.mLockedContent.findViewById(R.id.zap_lock_reject).setVisibility(4);
                    TimeLineFragment.this.mLockedContent.findViewById(R.id.locked_zap_editor).setVisibility(8);
                    TimeLineFragment.this.mLockedContent.findViewById(R.id.error_msg).setVisibility(0);
                    TimeLineFragment.this.reCalculateLayoutMargins();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void reCalculateLayoutMargins() {
        int i;
        if (isResumed()) {
            float f = getResources().getDisplayMetrics().density;
            if (!this.isSwipeDownTutorialShown) {
                i = (int) (210.0f * f);
            } else {
                i = 0;
            }
            if (this.mActiveZzone.getVisibility() == 0) {
                ((MarginLayoutParams) this.mActiveZzone.getLayoutParams()).setMargins(0, i, 0, 0);
                i = (int) (((float) i) + (80.0f * f));
                this.mActiveZzone.requestLayout();
            }
            if (this.mLockedContent.getVisibility() == 0) {
                ((MarginLayoutParams) this.mLockedContent.getLayoutParams()).setMargins(0, i, 0, 0);
                i = (int) (((float) i) + (120.0f * f));
                this.mLockedContent.requestLayout();
            }
            ((MarginLayoutParams) this.mListView.getLayoutParams()).setMargins(0, i, 0, 0);
            this.mListView.requestLayout();
            this.mRootView.invalidate();
        }
    }

    private void cleanUpAllMessages() {
        this.mTutorialHanlder.removeCallbacksAndMessages(null);
        if (this.mDragZapAnimator != null) {
            this.mDragZapAnimator.cleanUpAllHandlerMessages();
        }
    }

    private void cancelTutorialTimer() {
        this.mTutorialHanlder.removeMessages(1);
    }

    private void resetInActivityTimer() {
    }

    /* access modifiers changed from: private */
    public void launchSwipeDownTutorials() {
        if (isResumed()) {
            Animation customAnimation = this.mConcentricCircles.getCustomAnimation(this.mConcentricCircles.getVisibleHeight(), (int) (getResources().getDisplayMetrics().density * 350.0f));
            customAnimation.start();
            customAnimation.setAnimationListener(new AnimationListener() {
                public void onAnimationStart(Animation animation) {
                    TimeLineFragment.this.mConcentricCircles.setVisibility(0);
                    TimeLineFragment.this.mSwipeDownTutorialLayout.setVisibility(4);
                    TimeLineFragment.this.reCalculateLayoutMargins();
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                    TimeLineFragment.this.mConcentricCircles.setVisibility(4);
                    if (TimeLineFragment.this.isResumed()) {
                        TimeLineFragment.this.onSwipeDownTutorialComplete();
                    }
                }
            });
            this.mRootView.invalidate();
        }
    }

    /* access modifiers changed from: private */
    public void onSwipeDownTutorialComplete() {
        if (!this.isSwipeDownTutorialShown && isResumed()) {
            cancelTutorialTimer();
            addSampleCard();
            Z.SWIPE_DOWN_TUTORIAL_SHOWN.write(Boolean.valueOf(true));
            this.isSwipeDownTutorialShown = true;
            this.mConcentricCircles.setVisibility(4);
            this.mSwipeDownTutorialLayout.setVisibility(4);
            this.mSwipeDownTutorialLayout.findViewById(R.id.tutorial_text).setVisibility(4);
            resetInActivityTimer();
            reCalculateLayoutMargins();
        }
    }

    private void addSampleCard() {
        new Thread() {
            public void run() {
                File f = ZapUtils.getDemoCardFile();
                if (f == null) {
                    Log.e(TimeLineFragment.TAG, "Error retriving the sample card");
                } else {
                    new HistoryHandler().addDemoCard(f);
                }
            }
        }.start();
    }

    public void onUserActivity() {
        resetInActivityTimer();
    }

    public void onCastAvailabilityChanged(boolean castPresent) {
        if (this.mCcAvailableToolTip.getVisibility() != 0) {
            if (!castPresent || !((Boolean) Z.SHOW_CHROMECAST_TOOLTIP.read()).booleanValue()) {
                this.mCcAvailableToolTip.setVisibility(8);
            } else {
                this.mCcAvailableToolTip.setVisibility(0);
            }
        }
    }

    public void onApplicationConnected(ApplicationMetadata appMetadata, String applicationStatus, String sessionId, boolean wasLaunched) {
        Z.SHOW_CHROMECAST_TOOLTIP.write(Boolean.valueOf(false));
    }

    public void onApplicationDisconnected(int errorCode) {
    }

    public void onApplicationStopFailed(int errorCode) {
    }

    public boolean onApplicationConnectionFailed(int errorCode) {
        return false;
    }

    public void onApplicationStatusChanged(String appStatus) {
    }

    public void onVolumeChanged(double value, boolean isMute) {
    }

    public void onMessageReceived(CastDevice castDevice, String namespace, String message) {
    }

    public void onMessageSendFailed(Status status) {
    }

    public void onRemoved(CastDevice castDevice, String namespace) {
    }

    public void onConnected() {
    }

    public void onDisconnected() {
    }

    public void onRefresh() {
        if (this.mRefreshBar.getVisibility() != 0) {
            this.mRefreshBar.setVisibility(0);
            cleanUpAllMessages();
        }
    }

    public void onRefreshComplete() {
        resetInActivityTimer();
        this.mRefreshBar.setVisibility(8);
    }
}
