package com.google.tagmanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public final class InstallReferrerReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String referrer = intent.getStringExtra("referrer");
        if ("com.android.vending.INSTALL_REFERRER".equals(intent.getAction()) && referrer != null) {
            InstallReferrerUtil.cacheInstallReferrer(referrer);
            Intent serviceIntent = new Intent(context, InstallReferrerService.class);
            serviceIntent.putExtra("referrer", referrer);
            context.startService(serviceIntent);
        }
    }
}
