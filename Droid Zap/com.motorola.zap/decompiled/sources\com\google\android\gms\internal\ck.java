package com.google.android.gms.internal;

import com.google.android.gms.plus.PlusShare;
import com.motorola.zap.ui.TimeLineFragment;
import java.util.Map;

public final class ck {
    /* access modifiers changed from: private */
    public final Object mg = new Object();
    /* access modifiers changed from: private */
    public int nX = -2;
    private dd ng;
    public final ar oA = new ar() {
        public void a(dd ddVar, Map<String, String> map) {
            synchronized (ck.this.mg) {
                String str = (String) map.get(PlusShare.KEY_CALL_TO_ACTION_URL);
                if (str == null) {
                    da.w("URL missing in loadAdUrl GMSG.");
                    return;
                }
                if (str.contains("%40mediation_adapters%40")) {
                    str = str.replaceAll("%40mediation_adapters%40", cs.b(ddVar.getContext(), (String) map.get("check_adapters"), ck.this.ox));
                    da.v("Ad request URL modified to " + str);
                }
                ck.this.oy = str;
                ck.this.mg.notify();
            }
        }
    };
    /* access modifiers changed from: private */
    public String ox;
    /* access modifiers changed from: private */
    public String oy;
    public final ar oz = new ar() {
        public void a(dd ddVar, Map<String, String> map) {
            synchronized (ck.this.mg) {
                String str = (String) map.get("errors");
                da.w("Invalid " + ((String) map.get(TimeLineFragment.TYPE)) + " request error: " + str);
                ck.this.nX = 1;
                ck.this.mg.notify();
            }
        }
    };

    public ck(String str) {
        this.ox = str;
    }

    public String aI() {
        String str;
        synchronized (this.mg) {
            while (this.oy == null && this.nX == -2) {
                try {
                    this.mg.wait();
                } catch (InterruptedException e) {
                    da.w("Ad request service was interrupted.");
                    str = null;
                }
            }
            str = this.oy;
        }
        return str;
    }

    public void b(dd ddVar) {
        synchronized (this.mg) {
            this.ng = ddVar;
        }
    }

    public int getErrorCode() {
        int i;
        synchronized (this.mg) {
            i = this.nX;
        }
        return i;
    }
}
