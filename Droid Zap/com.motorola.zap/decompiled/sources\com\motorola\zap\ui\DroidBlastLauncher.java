package com.motorola.zap.ui;

import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.motorola.sharing.CastManager;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.Profile;
import com.motorola.sharing.ProfileManager;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConfig;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.app.App;
import com.motorola.zap.app.ZapTracker;
import com.motorola.zap.app.ZapTracker.ZapListener;
import com.motorola.zap.identity.EditProfileFragment;
import com.motorola.zap.identity.ProfileWelcomeFragment;
import com.motorola.zap.identity.ZapProfileSharedPerfs;
import com.motorola.zap.identity.ZapUserProfile;
import com.motorola.zap.location.BlastLocationProvider;
import com.motorola.zap.screenshot.VoiceIntroFragment;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

public class DroidBlastLauncher extends ActionBarActivity implements ZapListener {
    public static final int DRAWER_ITEM_PHOTOS = 1;
    public static final int DRAWER_ITEM_VIDEOS = 2;
    public static final int DRAWER_ITEM_ZAPACTIVITY = 0;
    public static final int DRAWER_ITEM_ZAPZONE = 3;
    private static WeakReference<DroidBlastLauncher> mBlastActivity = null;
    OnClickListener editProfileListener = new OnClickListener() {
        public void onClick(View v) {
            DroidBlastLauncher.this.showEditProfileFragment();
        }
    };
    private boolean isAccountSetupFailed = false;
    private boolean isAccountSetupLaunched = false;
    private boolean isAccountSetupReturn = false;
    private boolean isProfileNotificationSent = false;
    private boolean isRunning = false;
    private boolean isVoiceNotificationSent;
    private final Set<UserActivityListener> mActivityListeners = new HashSet();
    View mCustomHeaderView = null;
    View mCustomView = null;
    private DrawerAdapter mDrawerAdapter;
    /* access modifiers changed from: private */
    public RelativeLayout mDrawerContainer;
    /* access modifiers changed from: private */
    public DrawerLayout mDrawerLayout;
    /* access modifiers changed from: private */
    public ListView mDrawerList;
    boolean mDrawerOpen = false;
    /* access modifiers changed from: private */
    public ActionBarDrawerToggle mDrawerToggle;
    /* access modifiers changed from: private */
    public boolean mFoundSomeThing = false;
    Handler mHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            DroidBlastLauncher.this.updateUserActivity();
            ActionBar actionBar = DroidBlastLauncher.this.getActionBar();
            if (msg.what == 1) {
                if (DroidBlastLauncher.this.mMenu != null) {
                    DroidBlastLauncher.this.mMenu.close();
                }
                DroidBlastLauncher.this.mShowMenu = true;
                DroidBlastLauncher.this.invalidateOptionsMenu();
                DroidBlastLauncher.this.mDrawerToggle.setDrawerIndicatorEnabled(true);
                actionBar.setDisplayShowHomeEnabled(true);
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setHomeButtonEnabled(true);
                if (!DroidBlastLauncher.this.mDrawerOpen) {
                    actionBar.setDisplayShowCustomEnabled(false);
                    actionBar.setDisplayShowTitleEnabled(true);
                }
                actionBar.setCustomView(DroidBlastLauncher.this.mCustomView, new LayoutParams(-1, -2, 19));
                if (DroidBlastLauncher.this.mFoundSomeThing) {
                    if (DroidBlastLauncher.this.mPosition != 0) {
                        DroidBlastLauncher.this.launchZapActivity();
                    }
                    DroidBlastLauncher.this.mFoundSomeThing = false;
                }
            } else if (msg.what == 2) {
                if (DroidBlastLauncher.this.mMenu != null) {
                    DroidBlastLauncher.this.mMenu.close();
                }
                DroidBlastLauncher.this.mShowMenu = false;
                DroidBlastLauncher.this.invalidateOptionsMenu();
                ((TextView) DroidBlastLauncher.this.mCustomHeaderView.findViewById(R.id.text)).setText((String) msg.obj);
                DroidBlastLauncher.this.mDrawerToggle.setDrawerIndicatorEnabled(false);
                actionBar.setCustomView(DroidBlastLauncher.this.mCustomHeaderView, new LayoutParams(-1, -1, 17));
                actionBar.setDisplayShowCustomEnabled(true);
                actionBar.setDisplayShowTitleEnabled(false);
                actionBar.setDisplayShowHomeEnabled(false);
            }
        }
    };
    private long mLastLocationRequest = 0;
    /* access modifiers changed from: private */
    public Menu mMenu = null;
    int mPosition = 1;
    private ProfileManager mProfileManager = null;
    boolean mShowMenu = true;
    private ZapUserProfile mZapProfile = null;

    public class DrawerAdapter extends ArrayAdapter<DrawerActionItem> {
        int mResourceId = R.layout.drawer_list_item;

        public DrawerAdapter(Context context) {
            super(context, 0);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = DroidBlastLauncher.this.getLayoutInflater().inflate(this.mResourceId, parent, false);
            TextView title = (TextView) view.findViewById(R.id.text);
            ImageView icon = (ImageView) view.findViewById(R.id.icon);
            if (position == DroidBlastLauncher.this.mPosition) {
                title.setTypeface(Typeface.create("sans-serif", 1));
                title.setTextSize(18.0f);
            } else {
                title.setTypeface(Typeface.create("sans-serif-light", 0));
                title.setTextSize(18.0f);
            }
            DrawerActionItem item = (DrawerActionItem) getItem(position);
            title.setText(item.getStringRes());
            icon.setImageResource(item.getIconRes());
            return view;
        }
    }

    private class DrawerItemClickListener implements OnItemClickListener {
        private DrawerItemClickListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            DroidBlastLauncher.this.mDrawerList.invalidate();
            DroidBlastLauncher.this.selectItem(position);
        }
    }

    public interface UserActivityListener {
        void onUserActivity();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBlastActivity = new WeakReference<>(this);
        int playServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (playServicesAvailable != 0) {
            GooglePlayServicesUtil.getErrorDialog(playServicesAvailable, this, 0).show();
        }
        getSystemService("layout_inflater");
        setContentView((int) R.layout.activity_main);
        ActionBar actionbar = getActionBar();
        this.mCustomView = getLayoutInflater().inflate(R.layout.custom_title, null);
        this.mCustomHeaderView = getLayoutInflater().inflate(R.layout.customised_header, null);
        getActionBar().setCustomView(this.mCustomView, new LayoutParams(-2, -2, 19));
        this.mProfileManager = ProfileManager.getInstance();
        this.mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        this.mDrawerList = (ListView) findViewById(R.id.left_drawer_list);
        this.mDrawerContainer = (RelativeLayout) findViewById(R.id.left_drawer);
        populateDrawerItems();
        this.mDrawerLayout.setDrawerShadow((int) R.drawable.drawer_shadow, (int) GravityCompat.START);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeButtonEnabled(true);
        getActionBar().setDisplayShowCustomEnabled(false);
        getActionBar().setDisplayShowTitleEnabled(true);
        setTitle(((DrawerActionItem) this.mDrawerAdapter.getItem(this.mPosition)).getStringRes());
        this.mDrawerToggle = new ActionBarDrawerToggle(this, this.mDrawerLayout, R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                DroidBlastLauncher.this.mDrawerOpen = false;
                DroidBlastLauncher.this.getActionBar().setDisplayShowCustomEnabled(false);
                DroidBlastLauncher.this.getActionBar().setDisplayShowTitleEnabled(true);
                DroidBlastLauncher.this.setTitle(DroidBlastLauncher.this.getActionBar().getTitle());
                DroidBlastLauncher.this.invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                DroidBlastLauncher.this.mDrawerOpen = true;
                DroidBlastLauncher.this.getActionBar().setDisplayShowCustomEnabled(true);
                DroidBlastLauncher.this.getActionBar().setDisplayShowTitleEnabled(false);
                DroidBlastLauncher.this.invalidateOptionsMenu();
            }
        };
        this.mDrawerLayout.setDrawerListener(this.mDrawerToggle);
        if (savedInstanceState == null) {
            selectItem(0);
        }
        try {
            Field mDragger = this.mDrawerLayout.getClass().getDeclaredField("mLeftDragger");
            mDragger.setAccessible(true);
            ViewDragHelper draggerObj = (ViewDragHelper) mDragger.get(this.mDrawerLayout);
            Field mEdgeSize = draggerObj.getClass().getDeclaredField("mEdgeSize");
            mEdgeSize.setAccessible(true);
            mEdgeSize.setInt(draggerObj, mEdgeSize.getInt(draggerObj) * 2);
            if (ZapConfig.supportsZaptoscreen()) {
                int usageCount = ((Integer) Z.ZAP_APP_USAGE_COUNT.read()).intValue();
                if (usageCount <= 3) {
                    Z.ZAP_APP_USAGE_COUNT.write(Integer.valueOf(usageCount + 1));
                }
            }
        } catch (Exception e) {
        }
    }

    private void populateDrawerItems() {
        this.mDrawerAdapter = new DrawerAdapter(this);
        this.mDrawerAdapter.add(DrawerActionItem.create(0, R.drawable.ic_drawer_zapactivity_selector, R.string.drawer_item_zapactivity));
        this.mDrawerAdapter.add(DrawerActionItem.create(1, R.drawable.ic_drawer_photos_selector, R.string.drawer_item_photos));
        this.mDrawerAdapter.add(DrawerActionItem.create(2, R.drawable.ic_drawer_videos_selector, R.string.drawer_item_videos));
        this.mDrawerAdapter.add(DrawerActionItem.create(3, R.drawable.ic_drawer_zapzone_selector, R.string.drawer_item_zapzones));
        this.mDrawerList.setAdapter(this.mDrawerAdapter);
        this.mDrawerList.setChoiceMode(1);
        this.mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        ViewGroup container = (ViewGroup) findViewById(R.id.list_profile);
        loadZapUserProfile(container);
        container.setOnClickListener(this.editProfileListener);
        ((ImageView) findViewById(R.id.profile_image)).setOnClickListener(this.editProfileListener);
        ViewGroup appSettings = (ViewGroup) findViewById(R.id.app_settings);
        ((ImageView) appSettings.findViewById(R.id.icon)).setImageDrawable(getResources().getDrawable(R.drawable.ic_zap_settings));
        TextView titleSettings = (TextView) appSettings.findViewById(R.id.text);
        titleSettings.setText(R.string.drawer_item_settings);
        titleSettings.setTypeface(Typeface.create("sans-serif-light", 0));
        titleSettings.setTextSize(12.0f);
        appSettings.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                DroidBlastLauncher.this.invokeSettings();
                DroidBlastLauncher.this.mDrawerList.setItemChecked(DroidBlastLauncher.this.mPosition, true);
                DroidBlastLauncher.this.mDrawerLayout.closeDrawer((View) DroidBlastLauncher.this.mDrawerContainer);
            }
        });
        ViewGroup shareApp = (ViewGroup) findViewById(R.id.share_app);
        ((ImageView) shareApp.findViewById(R.id.icon)).setImageDrawable(getResources().getDrawable(R.drawable.ic_zap_share));
        TextView titleShareApp = (TextView) shareApp.findViewById(R.id.text);
        titleShareApp.setText(R.string.drawer_item_shareapp);
        titleShareApp.setTypeface(Typeface.create("sans-serif-light", 0));
        titleShareApp.setTextSize(12.0f);
        shareApp.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                DroidBlastLauncher.this.invokeShareAppDialogFragment();
                DroidBlastLauncher.this.mDrawerList.setItemChecked(DroidBlastLauncher.this.mPosition, true);
                DroidBlastLauncher.this.mDrawerLayout.closeDrawer((View) DroidBlastLauncher.this.mDrawerContainer);
            }
        });
    }

    public void updateDrawer() {
        loadZapUserProfile((ViewGroup) findViewById(R.id.list_profile));
        this.mDrawerList.setItemChecked(this.mPosition, true);
        setTitle(((DrawerActionItem) this.mDrawerAdapter.getItem(this.mPosition)).getStringRes());
    }

    private void loadZapUserProfile(ViewGroup v) {
        this.mZapProfile = new ZapUserProfile(v);
        if (GCSP.getAuthenticator().getAccount() == null) {
            this.mZapProfile.resetZapProfile();
        } else if (!((Boolean) Z.PRIVACY_AGREED.read()).booleanValue()) {
            this.mZapProfile.resetZapProfile();
        } else {
            Profile lProfile = ZapProfileSharedPerfs.getProfile();
            if (lProfile == null) {
                this.mZapProfile.resetZapProfile();
            }
            boolean hasEmailIdChanged = TextUtils.equals(GCSP.getAuthenticator().getAccount(), (CharSequence) ZapProfileSharedPerfs.ZAP_PROFILE_EMAIL.read());
            if (lProfile == null || !hasEmailIdChanged) {
                this.mProfileManager.getMyProfile(this.mZapProfile);
            } else {
                this.mZapProfile.loadZapProfile(lProfile);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        this.isAccountSetupFailed = false;
        int showIntroType = intent.getIntExtra(ZapConstants.EXTRA_SHOW_UPGRADE_FEATURE_TYPE, -1);
        if (this.isProfileNotificationSent || this.isVoiceNotificationSent) {
            switch (showIntroType) {
                case 0:
                    showEditProfileFragment();
                    return;
                case 1:
                    showVoiceIntroFragment();
                    return;
                default:
                    return;
            }
        }
    }

    public boolean showMenu() {
        return this.mShowMenu;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.mMenu = menu;
        if (this.mShowMenu) {
            getMenuInflater().inflate(R.menu.droid_blast_launcher, menu);
            if (ZapConfig.supportsChromecast()) {
                CastManager.getInstance().addCastButton(menu, R.id.media_route_menu_item);
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    public void showEditProfileFragment() {
        if (!((Boolean) ZapProfileSharedPerfs.ZAP_PROFILE_ACTIVATE.read()).booleanValue()) {
            showProfileWelcomeFragment();
        } else {
            Fragment fragment = new EditProfileFragment();
            if (!((mBlastActivity.get() == null && ((DroidBlastLauncher) mBlastActivity.get()).isFinishing()) || fragment == null)) {
                FragmentTransaction transaction = ((DroidBlastLauncher) mBlastActivity.get()).getFragmentManager().beginTransaction();
                transaction.replace(R.id.content_frame, fragment);
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        }
        setTitle(R.string.drawer_item_profile);
        this.mDrawerLayout.closeDrawer((View) this.mDrawerContainer);
    }

    public void showProfileWelcomeFragment() {
        Fragment fragment = new ProfileWelcomeFragment();
        if ((mBlastActivity.get() != null || !((DroidBlastLauncher) mBlastActivity.get()).isFinishing()) && fragment != null) {
            FragmentTransaction transaction = ((DroidBlastLauncher) mBlastActivity.get()).getFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, fragment);
            transaction.addToBackStack(null);
            transaction.commitAllowingStateLoss();
        }
    }

    public void showVoiceIntroFragment() {
        Fragment fragment = new VoiceIntroFragment();
        if ((mBlastActivity.get() != null || !((DroidBlastLauncher) mBlastActivity.get()).isFinishing()) && fragment != null) {
            FragmentTransaction transaction = ((DroidBlastLauncher) mBlastActivity.get()).getFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, fragment);
            transaction.addToBackStack(null);
            transaction.commitAllowingStateLoss();
        }
    }

    public void invokeSettings() {
        startActivity(new Intent(getApplicationContext(), DroidBlastSettingsActivity.class));
    }

    public void invokeShareAppDialogFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("share");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        new DroidBlastShareAppDialogFragment().show(ft, "share");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.isRunning = false;
        super.onPause();
        ZapTracker.getInstance().removeListener((ZapListener) this);
        AutoRefresher.getInstance().stop();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.isAccountSetupFailed) {
            finish();
        } else if (this.isAccountSetupLaunched || !ZapUtils.hasUserSignedOut()) {
            this.isRunning = true;
            if (this.isAccountSetupReturn && GCSP.getAuthenticator().getAccount() != null) {
                this.isAccountSetupReturn = false;
                selectItem(0);
            }
            if (!this.isAccountSetupLaunched && !((Boolean) ZapProfileSharedPerfs.ZAP_PROFILE_ACTIVATE.read()).booleanValue() && !this.isProfileNotificationSent) {
                this.isProfileNotificationSent = true;
                ZapUi.getInstance().showUpgradeNotification(0);
            }
            if (ZapConfig.supportsZaptoscreen() && !this.isAccountSetupLaunched && ((Integer) Z.ZAP_APP_USAGE_COUNT.read()).intValue() == 3 && !this.isVoiceNotificationSent) {
                this.isVoiceNotificationSent = true;
                Z.ZAP_APP_USAGE_COUNT.write(Integer.valueOf(4));
                ZapUi.getInstance().showUpgradeNotification(1);
            }
            ZapTracker.getInstance().addListener((ZapListener) this);
            ((NotificationManager) getSystemService("notification")).cancel(0);
            AutoRefresher.getInstance().start();
        } else {
            ZapProfileSharedPerfs.resetProfile();
            Intent intent = new Intent(this, SetupActivity.class);
            intent.setFlags(67108864);
            synchronized (this) {
                startActivityForResult(intent, 0);
                this.isAccountSetupLaunched = true;
            }
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(1);
        this.mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (ZapUtils.hasUserSignedOut()) {
                this.isAccountSetupFailed = true;
            }
            this.isAccountSetupLaunched = false;
            if (resultCode == 4) {
                finish();
                return;
            }
            this.isAccountSetupReturn = true;
            updateDrawer();
        } else if (requestCode == 1) {
            selectItem(0);
        }
    }

    public void onStart() {
        super.onStart();
        long now = System.currentTimeMillis();
        if (now - this.mLastLocationRequest > ZapConstants.LOCATION_MAX_AGE_NO_MODALITY_SUPPORT) {
            BlastLocationProvider.getInstance().requestUpdates(true, true);
            this.mLastLocationRequest = now;
        }
    }

    public void onStop() {
        BlastLocationProvider.getInstance().stopUpdates();
        super.onStop();
    }

    public boolean isActive() {
        return this.isRunning;
    }

    public void launchZapZone() {
        selectItem(3);
    }

    public void launchZapActivity() {
        selectItem(0);
    }

    public void selectItem(int i) {
        TimeLineFragment timeLineFragment;
        if (mBlastActivity.get() != null && !((DroidBlastLauncher) mBlastActivity.get()).isFinishing()) {
            FragmentManager fragmentManager = ((DroidBlastLauncher) mBlastActivity.get()).getFragmentManager();
            if (fragmentManager.getBackStackEntryCount() != 0) {
                fragmentManager.popBackStack();
            }
            Bundle bundle = new Bundle();
            switch (i) {
                case 0:
                    bundle.putInt(TimeLineFragment.TYPE, 1);
                    timeLineFragment = new TimeLineFragment();
                    break;
                case 1:
                    bundle.putInt(TimeLineFragment.TYPE, 4);
                    timeLineFragment = new TimeLineFragment();
                    break;
                case 2:
                    bundle.putInt(TimeLineFragment.TYPE, 5);
                    timeLineFragment = new TimeLineFragment();
                    break;
                case 3:
                    if (!ZapConfig.supportsZapZoneCreation() || !((Boolean) Z.SHOW_ZZ_MENU_TUTORIAL.read()).booleanValue()) {
                        bundle.putInt(TimeLineFragment.TYPE, 2);
                        timeLineFragment = new TimeLineFragment();
                        break;
                    } else {
                        startActivity(new Intent(App.getAppContext(), ZzSendInviteActivity.class));
                        this.mDrawerList.invalidate();
                        this.mDrawerList.setItemChecked(this.mPosition, true);
                        this.mDrawerLayout.closeDrawer((View) this.mDrawerContainer);
                        return;
                    }
                    break;
                default:
                    timeLineFragment = null;
                    break;
            }
            if (timeLineFragment != null) {
                bundle.putString(ZapConstants.CONTENT_TITLE, getString(((DrawerActionItem) this.mDrawerAdapter.getItem(this.mPosition)).getStringRes()));
                timeLineFragment.setArguments(bundle);
                fragmentManager.beginTransaction().replace(R.id.content_frame, timeLineFragment).commitAllowingStateLoss();
            }
            this.mPosition = i;
            this.mDrawerList.setItemChecked(i, true);
            setTitle(((DrawerActionItem) this.mDrawerAdapter.getItem(i)).getStringRes());
            this.mDrawerLayout.closeDrawer((View) this.mDrawerContainer);
        }
    }

    public void setTitle(CharSequence title) {
        getActionBar().setTitle(Html.fromHtml(title.toString()).toString());
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        this.mDrawerToggle.syncState();
    }

    public void onBackPressed() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager.getBackStackEntryCount() != 0) {
            fragmentManager.popBackStack();
            this.mDrawerList.setItemChecked(this.mPosition, true);
            setTitle(((DrawerActionItem) this.mDrawerAdapter.getItem(this.mPosition)).getStringRes());
            return;
        }
        super.onBackPressed();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (this.mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    public ActionBarDrawerToggle getDrawerToggle() {
        return this.mDrawerToggle;
    }

    public void onZapEvent(String str) {
        this.mHandler.removeMessages(1);
        this.mHandler.removeMessages(2);
        this.mHandler.sendEmptyMessageDelayed(1, 3500);
        Message obtain = Message.obtain();
        obtain.what = 2;
        obtain.obj = str;
        this.mHandler.sendMessage(obtain);
        if (str != null && str.equals(getString(R.string.toast_msg_found_something))) {
            this.mFoundSomeThing = true;
        }
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getPointerCount() >= 2) {
            this.mDrawerLayout.requestDisallowInterceptTouchEvent(false);
            MotionEvent down2Event = MotionEvent.obtain(ev);
            down2Event.setAction((ev.getActionIndex() << 8) | 3);
            this.mDrawerLayout.onTouchEvent(down2Event);
        }
        super.dispatchTouchEvent(ev);
        updateUserActivity();
        return false;
    }

    public void updateUserActivity() {
        for (UserActivityListener list : this.mActivityListeners) {
            list.onUserActivity();
        }
    }

    public synchronized void addListener(UserActivityListener listener) {
        this.mActivityListeners.add(listener);
    }

    public synchronized boolean removeListener(UserActivityListener listener) {
        return this.mActivityListeners.remove(listener);
    }
}
