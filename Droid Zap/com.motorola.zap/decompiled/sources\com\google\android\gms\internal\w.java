package com.google.android.gms.internal;

import java.lang.ref.WeakReference;

public final class w {
    private final Runnable kW;
    /* access modifiers changed from: private */
    public z kX;
    /* access modifiers changed from: private */
    public boolean kY = false;

    public w(final v vVar) {
        this.kW = new Runnable() {
            private final WeakReference<v> kZ = new WeakReference<>(vVar);

            public void run() {
                w.this.kY = false;
                v vVar = (v) this.kZ.get();
                if (vVar != null) {
                    vVar.b(w.this.kX);
                }
            }
        };
    }

    public void a(z zVar, long j) {
        if (this.kY) {
            da.w("An ad refresh is already scheduled.");
            return;
        }
        da.u("Scheduling ad refresh " + j + " milliseconds from now.");
        this.kX = zVar;
        this.kY = true;
        cz.pT.postDelayed(this.kW, j);
    }

    public void cancel() {
        cz.pT.removeCallbacks(this.kW);
    }

    public void d(z zVar) {
        a(zVar, 60000);
    }
}
