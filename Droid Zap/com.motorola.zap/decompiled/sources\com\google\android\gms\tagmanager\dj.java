package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.HitTypes;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.d;
import com.motorola.zap.ZapConstants;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class dj extends dg {
    private static final String ID = a.UNIVERSAL_ANALYTICS.toString();
    private static final String XR = b.ACCOUNT.toString();
    private static final String XS = b.ANALYTICS_PASS_THROUGH.toString();
    private static final String XT = b.ANALYTICS_FIELDS.toString();
    private static final String XU = b.TRACK_TRANSACTION.toString();
    private static final String XV = b.TRANSACTION_DATALAYER_MAP.toString();
    private static final String XW = b.TRANSACTION_ITEM_DATALAYER_MAP.toString();
    private static Map<String, String> XX;
    private static Map<String, String> XY;
    private final DataLayer TN;
    private final Set<String> XZ;
    private final df Ya;

    public dj(Context context, DataLayer dataLayer) {
        this(context, dataLayer, new df(context));
    }

    dj(Context context, DataLayer dataLayer, df dfVar) {
        super(ID, new String[0]);
        this.TN = dataLayer;
        this.Ya = dfVar;
        this.XZ = new HashSet();
        this.XZ.add(ZapConstants.GOOGLE_ACCOUNT_NONE);
        this.XZ.add("0");
        this.XZ.add("false");
    }

    private Map<String, String> E(Map<String, d.a> map) {
        d.a aVar = (d.a) map.get(XV);
        if (aVar != null) {
            return c(aVar);
        }
        if (XX == null) {
            HashMap hashMap = new HashMap();
            hashMap.put("transactionId", Fields.TRANSACTION_ID);
            hashMap.put("transactionAffiliation", Fields.TRANSACTION_AFFILIATION);
            hashMap.put("transactionTax", Fields.TRANSACTION_TAX);
            hashMap.put("transactionShipping", Fields.TRANSACTION_SHIPPING);
            hashMap.put("transactionTotal", Fields.TRANSACTION_REVENUE);
            hashMap.put("transactionCurrency", Fields.CURRENCY_CODE);
            XX = hashMap;
        }
        return XX;
    }

    private Map<String, String> F(Map<String, d.a> map) {
        d.a aVar = (d.a) map.get(XW);
        if (aVar != null) {
            return c(aVar);
        }
        if (XY == null) {
            HashMap hashMap = new HashMap();
            hashMap.put("name", Fields.ITEM_NAME);
            hashMap.put("sku", Fields.ITEM_SKU);
            hashMap.put("category", Fields.ITEM_CATEGORY);
            hashMap.put("price", Fields.ITEM_PRICE);
            hashMap.put("quantity", Fields.ITEM_QUANTITY);
            hashMap.put("currency", Fields.CURRENCY_CODE);
            XY = hashMap;
        }
        return XY;
    }

    private void a(Tracker tracker, Map<String, d.a> map) {
        String bN = bN("transactionId");
        if (bN == null) {
            bh.t("Cannot find transactionId in data layer.");
            return;
        }
        LinkedList<Map> linkedList = new LinkedList<>();
        try {
            Map p = p((d.a) map.get(XT));
            p.put(Fields.HIT_TYPE, HitTypes.TRANSACTION);
            for (Entry entry : E(map).entrySet()) {
                b(p, (String) entry.getValue(), bN((String) entry.getKey()));
            }
            linkedList.add(p);
            List<Map> kv = kv();
            if (kv != null) {
                for (Map map2 : kv) {
                    if (map2.get("name") == null) {
                        bh.t("Unable to send transaction item hit due to missing 'name' field.");
                        return;
                    }
                    Map p2 = p((d.a) map.get(XT));
                    p2.put(Fields.HIT_TYPE, HitTypes.ITEM);
                    p2.put(Fields.TRANSACTION_ID, bN);
                    for (Entry entry2 : F(map).entrySet()) {
                        b(p2, (String) entry2.getValue(), (String) map2.get(entry2.getKey()));
                    }
                    linkedList.add(p2);
                }
            }
            for (Map send : linkedList) {
                tracker.send(send);
            }
        } catch (IllegalArgumentException e) {
            bh.c("Unable to send transaction", e);
        }
    }

    private void b(Map<String, String> map, String str, String str2) {
        if (str2 != null) {
            map.put(str, str2);
        }
    }

    private String bN(String str) {
        Object obj = this.TN.get(str);
        if (obj == null) {
            return null;
        }
        return obj.toString();
    }

    private Map<String, String> c(d.a aVar) {
        Object o = di.o(aVar);
        if (!(o instanceof Map)) {
            return null;
        }
        Map map = (Map) o;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Entry entry : map.entrySet()) {
            linkedHashMap.put(entry.getKey().toString(), entry.getValue().toString());
        }
        return linkedHashMap;
    }

    private boolean d(Map<String, d.a> map, String str) {
        d.a aVar = (d.a) map.get(str);
        if (aVar == null) {
            return false;
        }
        return di.n(aVar).booleanValue();
    }

    private List<Map<String, String>> kv() {
        Object obj = this.TN.get("transactionProducts");
        if (obj == null) {
            return null;
        }
        if (!(obj instanceof List)) {
            throw new IllegalArgumentException("transactionProducts should be of type List.");
        }
        for (Object obj2 : (List) obj) {
            if (!(obj2 instanceof Map)) {
                throw new IllegalArgumentException("Each element of transactionProducts should be of type Map.");
            }
        }
        return (List) obj;
    }

    private Map<String, String> p(d.a aVar) {
        if (aVar == null) {
            return new HashMap();
        }
        Map c = c(aVar);
        if (c == null) {
            return new HashMap();
        }
        String str = (String) c.get(Fields.ANONYMIZE_IP);
        if (str != null && this.XZ.contains(str.toLowerCase())) {
            c.remove(Fields.ANONYMIZE_IP);
        }
        return c;
    }

    public void w(Map<String, d.a> map) {
        Tracker bF = this.Ya.bF("_GTM_DEFAULT_TRACKER_");
        if (d(map, XS)) {
            bF.send(p((d.a) map.get(XT)));
        } else if (d(map, XU)) {
            a(bF, map);
        } else {
            bh.w("Ignoring unknown tag.");
        }
    }
}
