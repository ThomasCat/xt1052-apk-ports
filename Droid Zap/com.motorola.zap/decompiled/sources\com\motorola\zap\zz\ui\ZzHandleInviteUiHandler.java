package com.motorola.zap.zz.ui;

import com.motorola.sharing.GroupListener.GroupEvent;
import com.motorola.sharing.GroupManager.GroupInfo;
import com.motorola.sharing.Profile;
import com.motorola.sharing.util.Uid;
import com.motorola.zap.app.GroupInvite;
import com.motorola.zap.app.ZzInviteStore;

public class ZzHandleInviteUiHandler extends BaseZzUiHandler {
    /* access modifiers changed from: protected */
    public void handleOk(GroupEvent ge) {
        if (!ZzInviteStore.has(ge.groupInfo.owner, ge.groupInfo.id)) {
            GroupInfo gi = ge.groupInfo;
            GroupInvite zzInvite = new GroupInvite(gi.owner, gi.id, gi.name, gi.thumbnail, gi.locked, Uid.getNext());
            ZzInviteStore.put(zzInvite, (Profile) ge.bundle.getSerializable(GroupEvent.KEY_USER_PROFILE));
            scheduleZzInviteExpiration(zzInvite.inviteUid);
        }
    }
}
