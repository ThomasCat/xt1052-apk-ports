package com.motorola.zap.ui;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import com.google.android.gms.games.GamesStatusCodes;
import com.motorola.zap.R;

public class DroidBlastDeleteProfileDialogFragment extends DialogFragment {

    public interface DeleteProfileDialogListener {
        void onDeleteNegativeClick();

        void onDeletePositiveClick();
    }

    public static DroidBlastDeleteProfileDialogFragment newInstance(DeleteProfileDialogListener listener) {
        DroidBlastDeleteProfileDialogFragment f = new DroidBlastDeleteProfileDialogFragment();
        f.setTargetFragment((Fragment) listener, GamesStatusCodes.STATUS_ACHIEVEMENT_UNKNOWN);
        return f;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Builder builder = new Builder(getActivity());
        builder.setMessage(R.string.dialog_msg_delete_account).setTitle(R.string.dialog_title_delete_account).setPositiveButton(R.string.notif_button_delete, new OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ((DeleteProfileDialogListener) DroidBlastDeleteProfileDialogFragment.this.getTargetFragment()).onDeletePositiveClick();
            }
        }).setNegativeButton(17039360, new OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ((DeleteProfileDialogListener) DroidBlastDeleteProfileDialogFragment.this.getTargetFragment()).onDeleteNegativeClick();
            }
        });
        return builder.create();
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        ((DeleteProfileDialogListener) getTargetFragment()).onDeleteNegativeClick();
    }
}
