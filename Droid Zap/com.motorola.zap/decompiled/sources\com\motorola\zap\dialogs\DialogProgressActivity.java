package com.motorola.zap.dialogs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.motorola.zap.LogUtils;
import com.motorola.zap.ZapConstants;

public class DialogProgressActivity extends Activity {
    /* access modifiers changed from: private */
    public static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private BroadcastReceiver mDismissDialogReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ZapConstants.ACTION_FINISH_DIALOG_ACTIVITY)) {
                if (DialogProgressActivity.DBG) {
                    Log.d("Zap-DialogProgActvt", "Dismiss dialog intent");
                }
                DialogProgressActivity.this.finish();
            }
        }
    };
    private String mMessage = null;
    private ProgressDialog mProgressDialog = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (DBG) {
            Log.d("Zap-DialogProgActvt", "Activity - onCreate");
        }
        this.mMessage = getIntent().getStringExtra(ZapConstants.EXTRA_DIALOG_MESSAGE);
        LocalBroadcastManager.getInstance(this).registerReceiver(this.mDismissDialogReceiver, new IntentFilter(ZapConstants.ACTION_FINISH_DIALOG_ACTIVITY));
    }

    public void onResume() {
        super.onResume();
        if (DBG) {
            Log.d("Zap-DialogProgActvt", "Activity - onResume");
        }
        this.mProgressDialog = ProgressDialog.show(this, null, this.mMessage);
    }

    public void onPause() {
        if (DBG) {
            Log.d("Zap-DialogProgActvt", "Activity - onPause");
        }
        if (this.mProgressDialog != null) {
            this.mProgressDialog.dismiss();
        }
        new Handler().post(new Runnable() {
            public void run() {
                DialogProgressActivity.this.finish();
            }
        });
        super.onPause();
    }

    public void onDestroy() {
        if (DBG) {
            Log.d("Zap-DialogProgActvt", "Activity - onDestroy");
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(this.mDismissDialogReceiver);
        super.onDestroy();
    }
}
