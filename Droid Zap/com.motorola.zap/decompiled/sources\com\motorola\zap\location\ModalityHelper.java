package com.motorola.zap.location;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import com.motorola.sharing.util.NetworkUtils;
import com.motorola.zap.LogUtils;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.ZapConstants.UseGpsOnModalityChange;
import com.motorola.zap.app.App;
import java.io.Serializable;

public class ModalityHelper {
    /* access modifiers changed from: private */
    public static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    public static final String MODALITY_NONE = "MODALITY_NONE";
    public static final String MODALITY_RUNNING = "MODALITY_RUNNING";
    public static int MODALITY_TYPE_NETWORK_LOCATION = 1;
    public static int MODALITY_TYPE_SENSOR_HUB = 0;
    public static final String MODALITY_UNREGISTERED = "MODALITY_UNREGISTERED";
    public static final String MODALITY_VEHICLE = "MODALITY_VEHICLE";
    public static final String MODALITY_WALKING = "MODALITY_WALKING";
    private static ModalityHelper sInstance = null;
    private Context mContext = null;
    private long mLastTimeRegisterSent = 0;
    private Runnable mUpdateRegAfterInitialPeriodTask = new Runnable() {
        public void run() {
            SystemClock.sleep(((Long) Z.MODALITY_LOC_UPDATES_INTERVAL.read()).longValue());
            if (Log.isLoggable("Zap-ModalityHelper", 4)) {
                Log.i("Zap-ModalityHelper", "Updating registration after initial period..");
            }
            if (ModalityHelper.DBG) {
                Log.d("Zap-ModalityHelper", "Unregistering current registration");
            }
            ModalityHelper.this.mUpdateRegInitPeriodTime = System.currentTimeMillis();
            ModalityHelper.this.requestUnregisterModality();
            SystemClock.sleep(300);
            if (ModalityHelper.DBG) {
                Log.d("Zap-ModalityHelper", "Registering new registration");
            }
            ModalityHelper.this.requestRegisterModality(true);
        }
    };
    /* access modifiers changed from: private */
    public long mUpdateRegInitPeriodTime = 0;

    public static ModalityHelper getInstance() {
        if (sInstance == null) {
            sInstance = new ModalityHelper(App.getAppContext());
        }
        return sInstance;
    }

    private ModalityHelper(Context context) {
        this.mContext = context;
    }

    public void requestRegisterModality(boolean force) {
        Intent intent;
        long now = System.currentTimeMillis();
        if (force || now - this.mLastTimeRegisterSent >= ZapConstants.LOCATION_MAX_AGE_NO_MODALITY_SUPPORT) {
            this.mLastTimeRegisterSent = now;
            int modalityType = Integer.parseInt((String) Z.MODALITY_TYPE.read());
            if (DBG) {
                Log.d("Zap-ModalityHelper", "Current modality type: " + modalityType);
            }
            if (modalityType == MODALITY_TYPE_SENSOR_HUB) {
                if (DBG) {
                    Log.d("Zap-ModalityHelper", "Registering for MODALITY_TYPE_SENSOR_HUB");
                }
                intent = new Intent(ZapConstants.ACTION_GS_REGISTER_SH_MODALITY);
                intent.putExtra(ZapConstants.EXTRA_GS_MODALITY_SH_WALKING_EXPIRE, (Serializable) Z.REG_MODALITY_SH_WALKING_EXPIRE.read());
                intent.putExtra(ZapConstants.EXTRA_GS_MODALITY_SH_RUNNING_EXPIRE, (Serializable) Z.REG_MODALITY_SH_RUNNING_EXPIRE.read());
                intent.putExtra(ZapConstants.EXTRA_GS_MODALITY_SH_VEHICLE_EXPIRE, (Serializable) Z.REG_MODALITY_SH_VEHICLE_EXPIRE.read());
                intent.putExtra(ZapConstants.EXTRA_GS_MODALITY_SH_START_DURATION, (Serializable) Z.REG_MODALITY_SH_START_DURATION.read());
                intent.putExtra(ZapConstants.EXTRA_GS_MODALITY_SH_END_DURATION, (Serializable) Z.REG_MODALITY_SH_END_DURATION.read());
                intent.putExtra(ZapConstants.EXTRA_GS_MODALITY_SH_REGISTER_ALL, false);
                this.mContext.sendBroadcast(new Intent(ZapConstants.ACTION_GS_UNREGISTER_LOC_MODALITY), "com.motorola.blast.permission.TRIGGER_BLAST_ACTION");
            } else if (modalityType == MODALITY_TYPE_NETWORK_LOCATION) {
                if (DBG) {
                    Log.d("Zap-ModalityHelper", "Registering for MODALITY_TYPE_NETWORK_LOCATION");
                }
                if (DBG) {
                    Log.d("Zap-ModalityHelper", "Modality - last blast action: " + ((now - ((Long) Z.LAST_ACTION_TIME.read()).longValue()) / 1000) + " secs ago");
                }
                long updatesInterval = ((Long) Z.MODALITY_LOC_UPDATES_INTERVAL.read()).longValue();
                if (now - ((Long) Z.LAST_ACTION_TIME.read()).longValue() > ((Long) Z.MODALITY_LOC_REG_EXPIRE.read()).longValue()) {
                    if (Log.isLoggable("Zap-ModalityHelper", 4)) {
                        Log.i("Zap-ModalityHelper", "Modality - Registering first time - short update interval");
                    }
                    updatesInterval = ((Long) Z.MODALITY_LOC_SHORT_UPDATES_INTERVAL.read()).longValue();
                    requestUnregisterModality();
                    SystemClock.sleep(300);
                    new Thread(this.mUpdateRegAfterInitialPeriodTask).start();
                }
                intent = new Intent(ZapConstants.ACTION_GS_REGISTER_LOC_MODALITY);
                intent.putExtra(ZapConstants.EXTRA_GS_MODALITY_LOC_UPDATES_INTERVAL, updatesInterval);
                intent.putExtra(ZapConstants.EXTRA_GS_MODALITY_LOC_MIN_VEHICLE_SPEED, (Serializable) Z.MODALITY_LOC_MIN_SPEED_VEHICLE.read());
                intent.putExtra(ZapConstants.EXTRA_GS_MODALITY_LOC_MIN_MOVING_SPEED, (Serializable) Z.MODALITY_LOC_MIN_SPEED_MOVING.read());
                intent.putExtra(ZapConstants.EXTRA_GS_MODALITY_LOC_REG_EXPIRE, (Serializable) Z.MODALITY_LOC_REG_EXPIRE.read());
                this.mContext.sendBroadcast(new Intent(ZapConstants.ACTION_GS_UNREGISTER_SH_MODALITY), "com.motorola.blast.permission.TRIGGER_BLAST_ACTION");
            } else if (Log.isLoggable("Zap-ModalityHelper", 6)) {
                Log.e("Zap-ModalityHelper", "Invalid Modality type");
                return;
            } else {
                return;
            }
            Z.LAST_ACTION_TIME.write(Long.valueOf(now));
            this.mContext.sendBroadcast(intent);
        } else if (DBG) {
            Log.d("Zap-ModalityHelper", "Register Modality request already sent in last 5 min");
        }
    }

    public void requestUnregisterModality() {
        this.mContext.sendBroadcast(new Intent(ZapConstants.ACTION_GS_UNREGISTER_SH_MODALITY), "com.motorola.blast.permission.TRIGGER_BLAST_ACTION");
        this.mContext.sendBroadcast(new Intent(ZapConstants.ACTION_GS_UNREGISTER_LOC_MODALITY), "com.motorola.blast.permission.TRIGGER_BLAST_ACTION");
        Z.MODALITY_STATE.write(MODALITY_NONE);
    }

    public void handleModalityUpdate(Intent intent) {
        long now = System.currentTimeMillis();
        if (now - this.mUpdateRegInitPeriodTime >= 10000) {
            String newModality = intent.getStringExtra(ZapConstants.EXTRA_GS_NEW_MODALITY);
            if (Log.isLoggable("Zap-ModalityHelper", 4)) {
                Log.i("Zap-ModalityHelper", "New modality state: " + newModality);
            }
            if (newModality != null) {
                if (newModality.equals(MODALITY_UNREGISTERED)) {
                    if (DBG) {
                        Log.d("Zap-ModalityHelper", "Updating local modality to MODALITY_NONE");
                    }
                    Z.MODALITY_STATE.write(MODALITY_NONE);
                    return;
                }
                Z.MODALITY_STATE.write(newModality);
            }
            long lastBlastTimeDiff = now - ((Long) Z.LAST_ACTION_TIME.read()).longValue();
            if (DBG) {
                Log.d("Zap-ModalityHelper", "Last Blast action was " + (lastBlastTimeDiff / 1000) + " secs ago");
            }
            boolean isWifiConnected = NetworkUtils.isWifiConnected(this.mContext);
            boolean useGps = false;
            if (lastBlastTimeDiff < ((Long) Z.USE_GPS_FOR_MODALITY_TIME_WINODW.read()).longValue()) {
                if (((Integer) Z.USE_GPS_ON_MODALITY_CHANGE.read()).intValue() == UseGpsOnModalityChange.YES.ordinal()) {
                    useGps = true;
                } else if (((Integer) Z.USE_GPS_ON_MODALITY_CHANGE.read()).intValue() == UseGpsOnModalityChange.ONLY_IF_NO_WIFI.ordinal()) {
                    useGps = !isWifiConnected;
                }
            }
            if (DBG) {
                Log.d("Zap-ModalityHelper", "Requesting GPS: " + useGps);
            }
            BlastLocationProvider.getInstance().requestUpdates(useGps, true);
        } else if (Log.isLoggable("Zap-ModalityHelper", 4)) {
            Log.i("Zap-ModalityHelper", "Ignoring state change after update reg for initial period");
        }
    }

    public String getCurrentModality() {
        return (String) Z.MODALITY_STATE.read();
    }

    public int getShareDistance() {
        return (TextUtils.equals(getCurrentModality(), MODALITY_VEHICLE) ? (Integer) Z.VEHICLE_SHARE_DISTANCE.read() : (Integer) Z.SHARE_DISTANCE.read()).intValue();
    }
}
