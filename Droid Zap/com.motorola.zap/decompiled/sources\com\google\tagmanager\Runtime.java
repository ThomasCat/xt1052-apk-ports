package com.google.tagmanager;

import android.content.Context;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.google.analytics.containertag.common.Key;
import com.google.analytics.containertag.proto.Serving.Supplemental;
import com.google.analytics.midtier.proto.containertag.TypeSystem.Value;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.tagmanager.CacheFactory.CacheSizeManager;
import com.google.tagmanager.CustomFunctionCall.CustomEvaluator;
import com.google.tagmanager.ResourceUtil.ExpandedFunctionCall;
import com.google.tagmanager.ResourceUtil.ExpandedResource;
import com.google.tagmanager.ResourceUtil.ExpandedRule;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class Runtime {
    private static final ObjectAndStatic<Value> DEFAULT_VALUE_AND_STATIC = new ObjectAndStatic<>(Types.getDefaultValue(), true);
    private final EventInfoDistributor eventInfoDistributor;
    private volatile String mCurrentEventName;
    private final DataLayer mDataLayer;
    private final Cache<ExpandedFunctionCall, ObjectAndStatic<Value>> mFunctionCallCache;
    private final Cache<String, CachedMacro> mMacroEvaluationCache;
    private final Map<String, MacroInfo> mMacroLookup;
    private final Map<String, FunctionCallImplementation> mMacroMap;
    private final Map<String, FunctionCallImplementation> mPredicateMap;
    private final ExpandedResource mResource;
    private final Set<ExpandedRule> mRules;
    private final Map<String, FunctionCallImplementation> mTrackingTagMap;

    interface AddRemoveSetPopulator {
        void rulePassed(ExpandedRule expandedRule, Set<ExpandedFunctionCall> set, Set<ExpandedFunctionCall> set2, ResolvedRuleBuilder resolvedRuleBuilder);
    }

    private static class CachedMacro {
        private ObjectAndStatic<Value> mObjectAndStatic;
        private Value mPushAfterEvaluate;

        public CachedMacro(ObjectAndStatic<Value> objectAndStatic, Value pushAfterEvaluate) {
            this.mObjectAndStatic = objectAndStatic;
            this.mPushAfterEvaluate = pushAfterEvaluate;
        }

        public ObjectAndStatic<Value> getObjectAndStatic() {
            return this.mObjectAndStatic;
        }

        public Value getPushAfterEvaluate() {
            return this.mPushAfterEvaluate;
        }

        public int getSize() {
            return (this.mPushAfterEvaluate == null ? 0 : this.mPushAfterEvaluate.getCachedSize()) + ((Value) this.mObjectAndStatic.getObject()).getCachedSize();
        }
    }

    private static class MacroInfo {
        private final Map<ExpandedRule, List<String>> mAddMacroNames = new HashMap();
        private final Map<ExpandedRule, List<ExpandedFunctionCall>> mAddMacros = new HashMap();
        private ExpandedFunctionCall mDefault;
        private final Map<ExpandedRule, List<String>> mRemoveMacroNames = new HashMap();
        private final Map<ExpandedRule, List<ExpandedFunctionCall>> mRemoveMacros = new HashMap();
        private final Set<ExpandedRule> mRules = new HashSet();

        public Set<ExpandedRule> getRules() {
            return this.mRules;
        }

        public void addRule(ExpandedRule rule) {
            this.mRules.add(rule);
        }

        public Map<ExpandedRule, List<ExpandedFunctionCall>> getAddMacros() {
            return this.mAddMacros;
        }

        public Map<ExpandedRule, List<String>> getAddMacroRuleNames() {
            return this.mAddMacroNames;
        }

        public Map<ExpandedRule, List<String>> getRemoveMacroRuleNames() {
            return this.mRemoveMacroNames;
        }

        public void addAddMacroForRule(ExpandedRule rule, ExpandedFunctionCall function) {
            List<ExpandedFunctionCall> result = (List) this.mAddMacros.get(rule);
            if (result == null) {
                result = new ArrayList<>();
                this.mAddMacros.put(rule, result);
            }
            result.add(function);
        }

        public void addAddMacroRuleNameForRule(ExpandedRule rule, String ruleName) {
            List<String> result = (List) this.mAddMacroNames.get(rule);
            if (result == null) {
                result = new ArrayList<>();
                this.mAddMacroNames.put(rule, result);
            }
            result.add(ruleName);
        }

        public Map<ExpandedRule, List<ExpandedFunctionCall>> getRemoveMacros() {
            return this.mRemoveMacros;
        }

        public void addRemoveMacroForRule(ExpandedRule rule, ExpandedFunctionCall function) {
            List<ExpandedFunctionCall> result = (List) this.mRemoveMacros.get(rule);
            if (result == null) {
                result = new ArrayList<>();
                this.mRemoveMacros.put(rule, result);
            }
            result.add(function);
        }

        public void addRemoveMacroRuleNameForRule(ExpandedRule rule, String ruleName) {
            List<String> result = (List) this.mRemoveMacroNames.get(rule);
            if (result == null) {
                result = new ArrayList<>();
                this.mRemoveMacroNames.put(rule, result);
            }
            result.add(ruleName);
        }

        public ExpandedFunctionCall getDefault() {
            return this.mDefault;
        }

        public void setDefault(ExpandedFunctionCall def) {
            this.mDefault = def;
        }
    }

    public Runtime(Context context, ExpandedResource expandedResource, DataLayer dataLayer, CustomEvaluator customEvaluator, CustomEvaluator customEvaluator2, EventInfoDistributor eventInfoDistributor2) {
        if (expandedResource == null) {
            throw new NullPointerException("resource cannot be null");
        }
        this.mResource = expandedResource;
        this.mRules = new HashSet(expandedResource.getRules());
        this.mDataLayer = dataLayer;
        this.eventInfoDistributor = eventInfoDistributor2;
        this.mFunctionCallCache = new CacheFactory().createCache(AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, new CacheSizeManager<ExpandedFunctionCall, ObjectAndStatic<Value>>() {
            public int sizeOf(ExpandedFunctionCall key, ObjectAndStatic<Value> value) {
                return ((Value) value.getObject()).getCachedSize();
            }
        });
        this.mMacroEvaluationCache = new CacheFactory().createCache(AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, new CacheSizeManager<String, CachedMacro>() {
            public int sizeOf(String key, CachedMacro value) {
                return key.length() + value.getSize();
            }
        });
        this.mTrackingTagMap = new HashMap();
        addTrackingTag(new ArbitraryPixelTag(context));
        addTrackingTag(new CustomFunctionCall(customEvaluator2));
        addTrackingTag(new DataLayerWriteTag(dataLayer));
        addTrackingTag(new UniversalAnalyticsTag(context, dataLayer));
        this.mPredicateMap = new HashMap();
        addPredicate(new ContainsPredicate());
        addPredicate(new EndsWithPredicate());
        addPredicate(new EqualsPredicate());
        addPredicate(new GreaterEqualsPredicate());
        addPredicate(new GreaterThanPredicate());
        addPredicate(new LessEqualsPredicate());
        addPredicate(new LessThanPredicate());
        addPredicate(new RegexPredicate());
        addPredicate(new StartsWithPredicate());
        this.mMacroMap = new HashMap();
        addMacro(new AdvertiserIdMacro(context));
        addMacro(new AdvertisingTrackingEnabledMacro());
        addMacro(new AdwordsClickReferrerMacro(context));
        addMacro(new AppIdMacro(context));
        addMacro(new AppNameMacro(context));
        addMacro(new AppVersionMacro(context));
        addMacro(new ConstantMacro());
        addMacro(new ContainerVersionMacro(this));
        addMacro(new CustomFunctionCall(customEvaluator));
        addMacro(new DataLayerMacro(dataLayer));
        addMacro(new DeviceIdMacro(context));
        addMacro(new DeviceNameMacro());
        addMacro(new EncodeMacro());
        addMacro(new EventMacro(this));
        addMacro(new GtmVersionMacro());
        addMacro(new HashMacro());
        addMacro(new InstallReferrerMacro(context));
        addMacro(new JoinerMacro());
        addMacro(new LanguageMacro());
        addMacro(new MobileAdwordsUniqueIdMacro(context));
        addMacro(new OsVersionMacro());
        addMacro(new PlatformMacro());
        addMacro(new RandomMacro());
        addMacro(new RegexGroupMacro());
        addMacro(new ResolutionMacro(context));
        addMacro(new RuntimeVersionMacro());
        addMacro(new SdkVersionMacro());
        addMacro(new TimeMacro());
        this.mMacroLookup = new HashMap();
        for (ExpandedRule expandedRule : this.mRules) {
            if (eventInfoDistributor2.debugMode()) {
                verifyFunctionAndNameListSizes(expandedRule.getAddMacros(), expandedRule.getAddMacroRuleNames(), "add macro");
                verifyFunctionAndNameListSizes(expandedRule.getRemoveMacros(), expandedRule.getRemoveMacroRuleNames(), "remove macro");
                verifyFunctionAndNameListSizes(expandedRule.getAddTags(), expandedRule.getAddTagRuleNames(), "add tag");
                verifyFunctionAndNameListSizes(expandedRule.getRemoveTags(), expandedRule.getRemoveTagRuleNames(), "remove tag");
            }
            for (int i = 0; i < expandedRule.getAddMacros().size(); i++) {
                ExpandedFunctionCall expandedFunctionCall = (ExpandedFunctionCall) expandedRule.getAddMacros().get(i);
                String str = "Unknown";
                if (eventInfoDistributor2.debugMode() && i < expandedRule.getAddMacroRuleNames().size()) {
                    str = (String) expandedRule.getAddMacroRuleNames().get(i);
                }
                MacroInfo orAddMacroInfo = getOrAddMacroInfo(this.mMacroLookup, getFunctionName(expandedFunctionCall));
                orAddMacroInfo.addRule(expandedRule);
                orAddMacroInfo.addAddMacroForRule(expandedRule, expandedFunctionCall);
                orAddMacroInfo.addAddMacroRuleNameForRule(expandedRule, str);
            }
            for (int i2 = 0; i2 < expandedRule.getRemoveMacros().size(); i2++) {
                ExpandedFunctionCall expandedFunctionCall2 = (ExpandedFunctionCall) expandedRule.getRemoveMacros().get(i2);
                String str2 = "Unknown";
                if (eventInfoDistributor2.debugMode() && i2 < expandedRule.getRemoveMacroRuleNames().size()) {
                    str2 = (String) expandedRule.getRemoveMacroRuleNames().get(i2);
                }
                MacroInfo orAddMacroInfo2 = getOrAddMacroInfo(this.mMacroLookup, getFunctionName(expandedFunctionCall2));
                orAddMacroInfo2.addRule(expandedRule);
                orAddMacroInfo2.addRemoveMacroForRule(expandedRule, expandedFunctionCall2);
                orAddMacroInfo2.addRemoveMacroRuleNameForRule(expandedRule, str2);
            }
        }
        for (Entry entry : this.mResource.getAllMacros().entrySet()) {
            for (ExpandedFunctionCall expandedFunctionCall3 : (List) entry.getValue()) {
                if (!Types.valueToBoolean((Value) expandedFunctionCall3.getProperties().get(Key.NOT_DEFAULT_MACRO.toString())).booleanValue()) {
                    getOrAddMacroInfo(this.mMacroLookup, (String) entry.getKey()).setDefault(expandedFunctionCall3);
                }
            }
        }
    }

    public ExpandedResource getResource() {
        return this.mResource;
    }

    public synchronized void setSupplementals(List<Supplemental> list) {
        for (Supplemental supplemental : list) {
            if (supplemental.name == null || !supplemental.name.startsWith("gaExperiment:")) {
                Log.v("Ignored supplemental: " + supplemental);
            } else {
                ExperimentMacroHelper.handleExperimentSupplemental(this.mDataLayer, supplemental);
            }
        }
    }

    public synchronized void evaluateTags(String currentEventName) {
        setCurrentEventName(currentEventName);
        EventInfoBuilder eventInfoBuilder = this.eventInfoDistributor.createDataLayerEventEvaluationEventInfo(currentEventName);
        DataLayerEventEvaluationInfoBuilder debugDataLayerBuilder = eventInfoBuilder.createDataLayerEventEvaluationInfoBuilder();
        for (ExpandedFunctionCall tag : (Set) calculateTagsToRun(this.mRules, debugDataLayerBuilder.createRulesEvaluation()).getObject()) {
            executeFunction(this.mTrackingTagMap, tag, new HashSet(), debugDataLayerBuilder.createAndAddResult());
        }
        eventInfoBuilder.processEventInfo();
        setCurrentEventName(null);
    }

    public ObjectAndStatic<Value> evaluateMacroReference(String macroName) {
        EventInfoBuilder eventInfoBuilder = this.eventInfoDistributor.createMacroEvalutionEventInfo(macroName);
        ObjectAndStatic<Value> result = evaluateMacroReferenceCycleDetection(macroName, new HashSet(), eventInfoBuilder.createMacroEvaluationInfoBuilder());
        eventInfoBuilder.processEventInfo();
        return result;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public synchronized void setCurrentEventName(String currentEventName) {
        this.mCurrentEventName = currentEventName;
    }

    /* access modifiers changed from: 0000 */
    public synchronized String getCurrentEventName() {
        return this.mCurrentEventName;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public ObjectAndStatic<Set<ExpandedFunctionCall>> calculateMacrosToRun(String macroName, Set<ExpandedRule> rules, Map<ExpandedRule, List<ExpandedFunctionCall>> addMacros, Map<ExpandedRule, List<String>> addMacroRuleNames, Map<ExpandedRule, List<ExpandedFunctionCall>> removeMacros, Map<ExpandedRule, List<String>> removeMacroRuleNames, Set<String> pendingMacroExpansions, RuleEvaluationStepInfoBuilder debugRulesEvaluation) {
        final Map<ExpandedRule, List<ExpandedFunctionCall>> map = addMacros;
        final Map<ExpandedRule, List<String>> map2 = addMacroRuleNames;
        final Map<ExpandedRule, List<ExpandedFunctionCall>> map3 = removeMacros;
        final Map<ExpandedRule, List<String>> map4 = removeMacroRuleNames;
        return calculateGenericToRun(rules, pendingMacroExpansions, new AddRemoveSetPopulator() {
            public void rulePassed(ExpandedRule rule, Set<ExpandedFunctionCall> add, Set<ExpandedFunctionCall> remove, ResolvedRuleBuilder debugRuleBuilder) {
                List<ExpandedFunctionCall> thisAddMacro = (List) map.get(rule);
                List<String> thisMacroEnablingRuleNames = (List) map2.get(rule);
                if (thisAddMacro != null) {
                    add.addAll(thisAddMacro);
                    debugRuleBuilder.getAddedMacroFunctions().translateAndAddAll(thisAddMacro, thisMacroEnablingRuleNames);
                }
                List<ExpandedFunctionCall> thisRemoveMacro = (List) map3.get(rule);
                List<String> thisRemoveMacroRuleNames = (List) map4.get(rule);
                if (thisRemoveMacro != null) {
                    remove.addAll(thisRemoveMacro);
                    debugRuleBuilder.getRemovedMacroFunctions().translateAndAddAll(thisRemoveMacro, thisRemoveMacroRuleNames);
                }
            }
        }, debugRulesEvaluation);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public ObjectAndStatic<Set<ExpandedFunctionCall>> calculateTagsToRun(Set<ExpandedRule> rules, RuleEvaluationStepInfoBuilder debugRulesEvaluation) {
        return calculateGenericToRun(rules, new HashSet(), new AddRemoveSetPopulator() {
            public void rulePassed(ExpandedRule rule, Set<ExpandedFunctionCall> add, Set<ExpandedFunctionCall> remove, ResolvedRuleBuilder debugRuleBuilder) {
                add.addAll(rule.getAddTags());
                remove.addAll(rule.getRemoveTags());
                debugRuleBuilder.getAddedTagFunctions().translateAndAddAll(rule.getAddTags(), rule.getAddTagRuleNames());
                debugRuleBuilder.getRemovedTagFunctions().translateAndAddAll(rule.getRemoveTags(), rule.getRemoveTagRuleNames());
            }
        }, debugRulesEvaluation);
    }

    private static MacroInfo getOrAddMacroInfo(Map<String, MacroInfo> macroLookup, String key) {
        MacroInfo result = (MacroInfo) macroLookup.get(key);
        if (result != null) {
            return result;
        }
        MacroInfo result2 = new MacroInfo();
        macroLookup.put(key, result2);
        return result2;
    }

    private ObjectAndStatic<Set<ExpandedFunctionCall>> calculateGenericToRun(Set<ExpandedRule> rules, Set<String> pendingMacroExpansions, AddRemoveSetPopulator setPopulator, RuleEvaluationStepInfoBuilder debugRulesEvaluation) {
        Set<ExpandedFunctionCall> add = new HashSet<>();
        Set<ExpandedFunctionCall> remove = new HashSet<>();
        boolean allStatic = true;
        for (ExpandedRule rule : rules) {
            ResolvedRuleBuilder debugRuleBuilder = debugRulesEvaluation.createResolvedRuleBuilder();
            ObjectAndStatic<Boolean> result = evaluatePredicatesInRule(rule, pendingMacroExpansions, debugRuleBuilder);
            if (((Boolean) result.getObject()).booleanValue()) {
                setPopulator.rulePassed(rule, add, remove, debugRuleBuilder);
            }
            allStatic = allStatic && result.isStatic();
        }
        add.removeAll(remove);
        debugRulesEvaluation.setEnabledFunctions(add);
        return new ObjectAndStatic<>(add, allStatic);
    }

    private static String getFunctionName(ExpandedFunctionCall functionCall) {
        return Types.valueToString((Value) functionCall.getProperties().get(Key.INSTANCE_NAME.toString()));
    }

    private static void addFunctionImplToMap(Map<String, FunctionCallImplementation> map, FunctionCallImplementation functionCallImplementation) {
        if (map.containsKey(functionCallImplementation.getInstanceFunctionId())) {
            throw new IllegalArgumentException("Duplicate function type name: " + functionCallImplementation.getInstanceFunctionId());
        }
        map.put(functionCallImplementation.getInstanceFunctionId(), functionCallImplementation);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void addMacro(FunctionCallImplementation macro) {
        addFunctionImplToMap(this.mMacroMap, macro);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void addTrackingTag(FunctionCallImplementation tag) {
        addFunctionImplToMap(this.mTrackingTagMap, tag);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void addPredicate(FunctionCallImplementation predicate) {
        addFunctionImplToMap(this.mPredicateMap, predicate);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public ObjectAndStatic<Boolean> evaluatePredicate(ExpandedFunctionCall predicate, Set<String> pendingMacroExpansions, ResolvedFunctionCallBuilder debugFunctionCall) {
        ObjectAndStatic<Value> result = executeFunction(this.mPredicateMap, predicate, pendingMacroExpansions, debugFunctionCall);
        Boolean predicateResult = Types.valueToBoolean((Value) result.getObject());
        debugFunctionCall.setFunctionResult(Types.objectToValue(predicateResult));
        return new ObjectAndStatic<>(predicateResult, result.isStatic());
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public ObjectAndStatic<Boolean> evaluatePredicatesInRule(ExpandedRule rule, Set<String> pendingMacroExpansions, ResolvedRuleBuilder debugRuleBuilder) {
        boolean allStatic;
        boolean allStatic2 = true;
        for (ExpandedFunctionCall f : rule.getNegativePredicates()) {
            ObjectAndStatic<Boolean> result = evaluatePredicate(f, pendingMacroExpansions, debugRuleBuilder.createNegativePredicate());
            if (((Boolean) result.getObject()).booleanValue()) {
                debugRuleBuilder.setValue(Types.objectToValue(Boolean.valueOf(false)));
                return new ObjectAndStatic<>(Boolean.valueOf(false), result.isStatic());
            } else if (!allStatic2 || !result.isStatic()) {
                allStatic2 = false;
            } else {
                allStatic2 = true;
            }
        }
        for (ExpandedFunctionCall f2 : rule.getPositivePredicates()) {
            ObjectAndStatic<Boolean> result2 = evaluatePredicate(f2, pendingMacroExpansions, debugRuleBuilder.createPositivePredicate());
            if (!((Boolean) result2.getObject()).booleanValue()) {
                debugRuleBuilder.setValue(Types.objectToValue(Boolean.valueOf(false)));
                return new ObjectAndStatic<>(Boolean.valueOf(false), result2.isStatic());
            } else if (!allStatic2 || !result2.isStatic()) {
                allStatic = false;
            } else {
                allStatic = true;
            }
        }
        debugRuleBuilder.setValue(Types.objectToValue(Boolean.valueOf(true)));
        return new ObjectAndStatic<>(Boolean.valueOf(true), allStatic2);
    }

    private ObjectAndStatic<Value> evaluateMacroReferenceCycleDetection(String str, Set<String> set, MacroEvaluationInfoBuilder macroEvaluationInfoBuilder) {
        ExpandedFunctionCall expandedFunctionCall;
        CachedMacro cachedMacro = (CachedMacro) this.mMacroEvaluationCache.get(str);
        if (cachedMacro == null || this.eventInfoDistributor.debugMode()) {
            MacroInfo macroInfo = (MacroInfo) this.mMacroLookup.get(str);
            if (macroInfo == null) {
                Log.e("Invalid macro: " + str);
                return DEFAULT_VALUE_AND_STATIC;
            }
            ObjectAndStatic calculateMacrosToRun = calculateMacrosToRun(str, macroInfo.getRules(), macroInfo.getAddMacros(), macroInfo.getAddMacroRuleNames(), macroInfo.getRemoveMacros(), macroInfo.getRemoveMacroRuleNames(), set, macroEvaluationInfoBuilder.createRulesEvaluation());
            if (((Set) calculateMacrosToRun.getObject()).isEmpty()) {
                expandedFunctionCall = macroInfo.getDefault();
            } else {
                if (((Set) calculateMacrosToRun.getObject()).size() > 1) {
                    Log.w("Multiple macros active for macroName " + str);
                }
                expandedFunctionCall = (ExpandedFunctionCall) ((Set) calculateMacrosToRun.getObject()).iterator().next();
            }
            if (expandedFunctionCall == null) {
                return DEFAULT_VALUE_AND_STATIC;
            }
            ObjectAndStatic<Value> executeFunction = executeFunction(this.mMacroMap, expandedFunctionCall, set, macroEvaluationInfoBuilder.createResult());
            ObjectAndStatic objectAndStatic = executeFunction == DEFAULT_VALUE_AND_STATIC ? DEFAULT_VALUE_AND_STATIC : new ObjectAndStatic(executeFunction.getObject(), calculateMacrosToRun.isStatic() && executeFunction.isStatic());
            Value pushAfterEvaluate = expandedFunctionCall.getPushAfterEvaluate();
            if (objectAndStatic.isStatic()) {
                this.mMacroEvaluationCache.put(str, new CachedMacro(objectAndStatic, pushAfterEvaluate));
            }
            pushUnevaluatedValueToDataLayer(pushAfterEvaluate, set);
            return objectAndStatic;
        }
        pushUnevaluatedValueToDataLayer(cachedMacro.getPushAfterEvaluate(), set);
        return cachedMacro.getObjectAndStatic();
    }

    private void pushUnevaluatedValueToDataLayer(Value value, Set<String> set) {
        if (value != null) {
            ObjectAndStatic<Value> macroExpandValue = macroExpandValue(value, set, new NoopValueBuilder());
            if (macroExpandValue != DEFAULT_VALUE_AND_STATIC) {
                Object valueToObject = Types.valueToObject((Value) macroExpandValue.getObject());
                if (valueToObject instanceof Map) {
                    this.mDataLayer.push((Map) valueToObject);
                } else if (valueToObject instanceof List) {
                    for (Object next : (List) valueToObject) {
                        if (next instanceof Map) {
                            this.mDataLayer.push((Map) next);
                        } else {
                            Log.w("pushAfterEvaluate: value not a Map");
                        }
                    }
                } else {
                    Log.w("pushAfterEvaluate: value not a Map or List");
                }
            }
        }
    }

    private ObjectAndStatic<Value> macroExpandValue(Value value, Set<String> set, ValueBuilder valueBuilder) {
        if (!value.containsReferences) {
            return new ObjectAndStatic<>(value, true);
        }
        switch (value.type) {
            case 2:
                Value newValueBasedOnValue = ResourceUtil.newValueBasedOnValue(value);
                newValueBasedOnValue.listItem = new Value[value.listItem.length];
                for (int i = 0; i < value.listItem.length; i++) {
                    ObjectAndStatic<Value> macroExpandValue = macroExpandValue(value.listItem[i], set, valueBuilder.getListItem(i));
                    if (macroExpandValue == DEFAULT_VALUE_AND_STATIC) {
                        return DEFAULT_VALUE_AND_STATIC;
                    }
                    newValueBasedOnValue.listItem[i] = (Value) macroExpandValue.getObject();
                }
                return new ObjectAndStatic<>(newValueBasedOnValue, false);
            case 3:
                Value newValueBasedOnValue2 = ResourceUtil.newValueBasedOnValue(value);
                if (value.mapKey.length != value.mapValue.length) {
                    Log.e("Invalid serving value: " + value.toString());
                    return DEFAULT_VALUE_AND_STATIC;
                }
                newValueBasedOnValue2.mapKey = new Value[value.mapKey.length];
                newValueBasedOnValue2.mapValue = new Value[value.mapKey.length];
                for (int i2 = 0; i2 < value.mapKey.length; i2++) {
                    ObjectAndStatic<Value> macroExpandValue2 = macroExpandValue(value.mapKey[i2], set, valueBuilder.getMapKey(i2));
                    ObjectAndStatic<Value> macroExpandValue3 = macroExpandValue(value.mapValue[i2], set, valueBuilder.getMapValue(i2));
                    if (macroExpandValue2 == DEFAULT_VALUE_AND_STATIC || macroExpandValue3 == DEFAULT_VALUE_AND_STATIC) {
                        return DEFAULT_VALUE_AND_STATIC;
                    }
                    newValueBasedOnValue2.mapKey[i2] = (Value) macroExpandValue2.getObject();
                    newValueBasedOnValue2.mapValue[i2] = (Value) macroExpandValue3.getObject();
                }
                return new ObjectAndStatic<>(newValueBasedOnValue2, false);
            case 4:
                if (set.contains(value.macroReference)) {
                    Log.e("Macro cycle detected.  Current macro reference: " + value.macroReference + "." + "  Previous macro references: " + set.toString() + ".");
                    return DEFAULT_VALUE_AND_STATIC;
                }
                set.add(value.macroReference);
                ObjectAndStatic<Value> applyEscapings = ValueEscapeUtil.applyEscapings(evaluateMacroReferenceCycleDetection(value.macroReference, set, valueBuilder.createValueMacroEvaluationInfoExtension()), value.escaping);
                set.remove(value.macroReference);
                return applyEscapings;
            case 7:
                Value newValueBasedOnValue3 = ResourceUtil.newValueBasedOnValue(value);
                newValueBasedOnValue3.templateToken = new Value[value.templateToken.length];
                for (int i3 = 0; i3 < value.templateToken.length; i3++) {
                    ObjectAndStatic<Value> macroExpandValue4 = macroExpandValue(value.templateToken[i3], set, valueBuilder.getTemplateToken(i3));
                    if (macroExpandValue4 == DEFAULT_VALUE_AND_STATIC) {
                        return DEFAULT_VALUE_AND_STATIC;
                    }
                    newValueBasedOnValue3.templateToken[i3] = (Value) macroExpandValue4.getObject();
                }
                return new ObjectAndStatic<>(newValueBasedOnValue3, false);
            default:
                Log.e("Unknown type: " + value.type);
                return DEFAULT_VALUE_AND_STATIC;
        }
    }

    private ObjectAndStatic<Value> executeFunction(Map<String, FunctionCallImplementation> map, ExpandedFunctionCall expandedFunctionCall, Set<String> set, ResolvedFunctionCallBuilder resolvedFunctionCallBuilder) {
        boolean z;
        boolean z2 = true;
        Value value = (Value) expandedFunctionCall.getProperties().get(Key.FUNCTION.toString());
        if (value == null) {
            Log.e("No function id in properties");
            return DEFAULT_VALUE_AND_STATIC;
        }
        String str = value.functionId;
        FunctionCallImplementation functionCallImplementation = (FunctionCallImplementation) map.get(str);
        if (functionCallImplementation == null) {
            Log.e(str + " has no backing implementation.");
            return DEFAULT_VALUE_AND_STATIC;
        }
        ObjectAndStatic<Value> objectAndStatic = (ObjectAndStatic) this.mFunctionCallCache.get(expandedFunctionCall);
        if (objectAndStatic != null && !this.eventInfoDistributor.debugMode()) {
            return objectAndStatic;
        }
        HashMap hashMap = new HashMap();
        boolean z3 = true;
        for (Entry entry : expandedFunctionCall.getProperties().entrySet()) {
            ObjectAndStatic<Value> macroExpandValue = macroExpandValue((Value) entry.getValue(), set, resolvedFunctionCallBuilder.createResolvedPropertyBuilder((String) entry.getKey()).createPropertyValueBuilder((Value) entry.getValue()));
            if (macroExpandValue == DEFAULT_VALUE_AND_STATIC) {
                return DEFAULT_VALUE_AND_STATIC;
            }
            if (macroExpandValue.isStatic()) {
                expandedFunctionCall.updateCacheableProperty((String) entry.getKey(), (Value) macroExpandValue.getObject());
                z = z3;
            } else {
                z = false;
            }
            hashMap.put(entry.getKey(), macroExpandValue.getObject());
            z3 = z;
        }
        if (!functionCallImplementation.hasRequiredKeys(hashMap.keySet())) {
            Log.e("Incorrect keys for function " + str + " required " + functionCallImplementation.getRequiredKeys() + " had " + hashMap.keySet());
            return DEFAULT_VALUE_AND_STATIC;
        }
        if (!z3 || !functionCallImplementation.isCacheable()) {
            z2 = false;
        }
        ObjectAndStatic<Value> objectAndStatic2 = new ObjectAndStatic<>(functionCallImplementation.evaluate(hashMap), z2);
        if (z2) {
            this.mFunctionCallCache.put(expandedFunctionCall, objectAndStatic2);
        }
        resolvedFunctionCallBuilder.setFunctionResult((Value) objectAndStatic2.getObject());
        return objectAndStatic2;
    }

    private static void verifyFunctionAndNameListSizes(List<ExpandedFunctionCall> list, List<String> list2, String str) {
        if (list.size() != list2.size()) {
            Log.i("Invalid resource: imbalance of rule names of functions for " + str + " operation. Using default rule name instead");
        }
    }
}
