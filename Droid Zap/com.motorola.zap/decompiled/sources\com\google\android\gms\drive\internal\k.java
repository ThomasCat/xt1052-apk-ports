package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;

public class k implements Creator<DisconnectRequest> {
    static void a(DisconnectRequest disconnectRequest, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.c(parcel, 1, disconnectRequest.wj);
        b.D(parcel, p);
    }

    /* renamed from: K */
    public DisconnectRequest createFromParcel(Parcel parcel) {
        int o = a.o(parcel);
        int i = 0;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    i = a.g(parcel, n);
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new DisconnectRequest(i);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }

    /* renamed from: ap */
    public DisconnectRequest[] newArray(int i) {
        return new DisconnectRequest[i];
    }
}
