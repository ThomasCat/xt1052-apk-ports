package com.google.android.gms.internal;

import android.content.Intent;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a.c;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerBuffer;
import com.google.android.gms.games.Players;
import com.google.android.gms.games.Players.LoadPlayersResult;

public final class gm implements Players {

    private static abstract class a extends com.google.android.gms.games.Games.a<LoadPlayersResult> {
        private a() {
        }

        /* renamed from: A */
        public LoadPlayersResult d(final Status status) {
            return new LoadPlayersResult() {
                public PlayerBuffer getPlayers() {
                    return new PlayerBuffer(DataHolder.empty(14));
                }

                public Status getStatus() {
                    return status;
                }

                public void release() {
                }
            };
        }
    }

    public Player getCurrentPlayer(GoogleApiClient apiClient) {
        return Games.c(apiClient).fp();
    }

    public String getCurrentPlayerId(GoogleApiClient apiClient) {
        return Games.c(apiClient).fo();
    }

    public Intent getPlayerSearchIntent(GoogleApiClient apiClient) {
        return Games.c(apiClient).fy();
    }

    public PendingResult<LoadPlayersResult> loadConnectedPlayers(GoogleApiClient apiClient, final boolean forceReload) {
        return apiClient.a(new a() {
            /* access modifiers changed from: protected */
            public void a(fx fxVar) {
                fxVar.a((c<LoadPlayersResult>) this, forceReload);
            }
        });
    }

    public PendingResult<LoadPlayersResult> loadInvitablePlayers(GoogleApiClient apiClient, final int pageSize, final boolean forceReload) {
        return apiClient.a(new a() {
            /* access modifiers changed from: protected */
            public void a(fx fxVar) {
                fxVar.a((c<LoadPlayersResult>) this, pageSize, false, forceReload);
            }
        });
    }

    public PendingResult<LoadPlayersResult> loadMoreInvitablePlayers(GoogleApiClient apiClient, final int pageSize) {
        return apiClient.a(new a() {
            /* access modifiers changed from: protected */
            public void a(fx fxVar) {
                fxVar.a((c<LoadPlayersResult>) this, pageSize, true, false);
            }
        });
    }

    public PendingResult<LoadPlayersResult> loadMoreRecentlyPlayedWithPlayers(GoogleApiClient apiClient, final int pageSize) {
        return apiClient.a(new a() {
            /* access modifiers changed from: protected */
            public void a(fx fxVar) {
                fxVar.a((c<LoadPlayersResult>) this, "playedWith", pageSize, true, false);
            }
        });
    }

    public PendingResult<LoadPlayersResult> loadPlayer(GoogleApiClient apiClient, final String playerId) {
        return apiClient.a(new a() {
            /* access modifiers changed from: protected */
            public void a(fx fxVar) {
                fxVar.a((c<LoadPlayersResult>) this, playerId);
            }
        });
    }

    public PendingResult<LoadPlayersResult> loadRecentlyPlayedWithPlayers(GoogleApiClient apiClient, final int pageSize, final boolean forceReload) {
        return apiClient.a(new a() {
            /* access modifiers changed from: protected */
            public void a(fx fxVar) {
                fxVar.a((c<LoadPlayersResult>) this, "playedWith", pageSize, false, forceReload);
            }
        });
    }
}
