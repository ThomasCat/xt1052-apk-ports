package com.motorola.sharing.util.auth;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.drive.DriveFile;
import com.motorola.sharing.Authenticator;
import com.motorola.sharing.Authenticator.Callback;
import com.motorola.sharing.Authenticator.Status;
import com.motorola.sharing.Authenticator.TokenInfo;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.provider.cloud.ConnectionException;
import com.motorola.sharing.provider.cloud.HttpHelper;
import com.motorola.sharing.util.Logger;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class GoogleAuthenticator implements Authenticator {
    private static long AUTH_REQUEST_RUNNING = 4000;
    private static long AUTH_REQUEST_TIMEOUT = 60000;
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getGcspTag("GoogleAuth");
    /* access modifiers changed from: private */
    public static int WHAT_AUTH_RUNNING = 2;
    /* access modifiers changed from: private */
    public static int WHAT_AUTH_TIMEOUT = 1;
    private BroadcastReceiver mActivityResultReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Logger.d(GoogleAuthenticator.TAG, "Received " + intent.getAction());
            if (intent.getAction().equals(UserRecoveryActivity.ACTION_RECOVERY_RESULT)) {
                GoogleAuthenticator.this.onAuthUserRecoveryResult(intent.getBooleanExtra(UserRecoveryActivity.EXTRA_AUTHORIZED, false));
            }
        }
    };
    /* access modifiers changed from: private */
    public State mAuthState = State.IDLE;
    /* access modifiers changed from: private */
    public Context mContext = null;
    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            if (msg.what == GoogleAuthenticator.WHAT_AUTH_RUNNING) {
                synchronized (this) {
                    if (GoogleAuthenticator.this.mRequestors.size() > 0 && GoogleAuthenticator.this.mAuthState != State.USER_RECOVERY) {
                        Logger.d(GoogleAuthenticator.TAG, "Authentication is taking to long...");
                        GoogleAuthenticator.this.onAuthTakingTooLong();
                    }
                }
            } else if (msg.what == GoogleAuthenticator.WHAT_AUTH_TIMEOUT) {
                GoogleAuthenticator.this.onAuthTimeout();
            }
        }
    };
    /* access modifiers changed from: private */
    public final List<Requestor> mRequestors = new ArrayList();
    private TokenInfo mTokenInfo = null;
    private boolean mTokenRefreshRequested = false;

    private static class Requestor {
        Callback callback;
        Object data;

        private Requestor(Callback callback2, Object data2) {
            this.callback = callback2;
            this.data = data2;
        }
    }

    private enum State {
        IDLE,
        BUSY,
        USER_RECOVERY
    }

    public abstract void onAuthError(Status status);

    public abstract void onAuthTakingTooLong();

    public GoogleAuthenticator(Context context) {
        this.mContext = context;
    }

    public synchronized TokenInfo getTokenInfo() {
        return (this.mTokenInfo == null || !this.mTokenInfo.isValid()) ? null : this.mTokenInfo;
    }

    public synchronized void authenticate(Callback callback, Object data) {
        authenticate(callback, data, false);
    }

    public synchronized void authenticate(Callback callback, Object data, boolean refreshToken) {
        Logger.d(TAG, "Got request to authenticate");
        if (refreshToken) {
            Logger.d(TAG, "Token refresh requested");
            this.mTokenRefreshRequested = true;
        }
        if (callback != null) {
            Logger.d(TAG, "New requestor added");
            this.mRequestors.add(new Requestor(callback, data));
        }
        startWaitingForAuthResult();
        if (this.mAuthState == State.IDLE) {
            this.mAuthState = State.BUSY;
            this.mTokenInfo = null;
            authenticate(true);
        }
    }

    private synchronized void startWaitingForAuthResult() {
        stopWaitingForAuthResult();
        if (this.mRequestors.size() > 0 && this.mAuthState != State.USER_RECOVERY) {
            Logger.d(TAG, "Number of requestors in waiting: " + this.mRequestors.size());
            this.mHandler.sendEmptyMessageDelayed(WHAT_AUTH_RUNNING, AUTH_REQUEST_RUNNING);
            this.mHandler.sendEmptyMessageDelayed(WHAT_AUTH_TIMEOUT, AUTH_REQUEST_TIMEOUT);
        }
    }

    private synchronized void stopWaitingForAuthResult() {
        this.mHandler.removeMessages(WHAT_AUTH_RUNNING);
        this.mHandler.removeMessages(WHAT_AUTH_TIMEOUT);
    }

    /* access modifiers changed from: private */
    public synchronized void startUserRecoveryActivity(Intent intent) {
        this.mAuthState = State.USER_RECOVERY;
        stopWaitingForAuthResult();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UserRecoveryActivity.ACTION_RECOVERY_RESULT);
        LocalBroadcastManager.getInstance(this.mContext).registerReceiver(this.mActivityResultReceiver, intentFilter);
        Intent intent2 = new Intent(this.mContext, UserRecoveryActivity.class);
        intent2.putExtra(UserRecoveryActivity.EXTRA_RECOVERY_INTENT, intent);
        intent2.setFlags(DriveFile.MODE_READ_ONLY);
        this.mContext.startActivity(intent2);
    }

    /* access modifiers changed from: private */
    public synchronized void onAuthUserRecoveryResult(boolean authorized) {
        Logger.d(TAG, "Authentication user recovery got authorized: " + authorized);
        LocalBroadcastManager.getInstance(this.mContext).unregisterReceiver(this.mActivityResultReceiver);
        if (authorized) {
            startWaitingForAuthResult();
            this.mAuthState = State.BUSY;
            authenticate(false);
        } else {
            onAuthComplete(Status.ERROR_NOT_AUTHORIZED, null);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void onAuthComplete(Status status, TokenInfo tokenInfo) {
        Logger.d(TAG, "Authentication completed with status: " + status);
        this.mAuthState = State.IDLE;
        this.mTokenInfo = tokenInfo;
        Logger.d(TAG, "New token available: " + (tokenInfo != null));
        notifyOnResult(status, tokenInfo);
    }

    /* access modifiers changed from: private */
    public synchronized void onAuthTimeout() {
        Logger.d(TAG, "Authentication timeout triggered");
        notifyOnResult(Status.ERROR_NOT_AUTHORIZED, null);
    }

    private synchronized void notifyOnResult(Status status, TokenInfo tokenInfo) {
        stopWaitingForAuthResult();
        if (this.mRequestors.size() > 0) {
            if (status != Status.SUCCESS) {
                onAuthError(status);
            }
            Logger.d(TAG, "Number of requestors to respond: " + this.mRequestors.size());
            for (Requestor requestor : this.mRequestors) {
                requestor.callback.onAuthComplete(status, tokenInfo, requestor.data);
            }
            this.mRequestors.clear();
        }
    }

    /* access modifiers changed from: private */
    public void authenticate(final boolean allowUserRecovery) {
        new Thread(new Runnable() {
            public void run() {
                Status status;
                String account = GCSP.getAuthenticator().getAccount();
                Logger.d(GoogleAuthenticator.TAG, "Authentication started for " + Logger.obfuscate(account));
                if (account == null) {
                    if (Log.isLoggable(GoogleAuthenticator.TAG, 6)) {
                        Log.e(GoogleAuthenticator.TAG, "ERROR - User account is null");
                    }
                    GoogleAuthenticator.this.onAuthComplete(Status.ERROR_NOT_AUTHORIZED, null);
                    return;
                }
                TokenInfo tokenInfo = new TokenInfo();
                Status status2 = Status.ERROR_GENERAL;
                boolean isRetry = false;
                while (true) {
                    try {
                        Logger.d(GoogleAuthenticator.TAG, "Requesting a token...");
                        String token = GoogleAuthUtil.getToken(GoogleAuthenticator.this.mContext, account, "oauth2:profile email");
                        Logger.d(GoogleAuthenticator.TAG, "Token obtained: " + Logger.obfuscate(token));
                        if (!GoogleAuthenticator.this.shouldInvalidateToken()) {
                            status = GoogleAuthenticator.this.validateToken(account, token, tokenInfo);
                            if (status == Status.SUCCESS) {
                                break;
                            }
                        }
                        GoogleAuthenticator.this.invalidateToken(token);
                        Logger.d(GoogleAuthenticator.TAG, "Requesting a new token...");
                        String token2 = GoogleAuthUtil.getToken(GoogleAuthenticator.this.mContext, account, "oauth2:profile email");
                        Logger.d(GoogleAuthenticator.TAG, "New token obtained: " + Logger.obfuscate(token2));
                        status = GoogleAuthenticator.this.validateToken(account, token2, tokenInfo);
                        if (status == Status.SUCCESS) {
                            break;
                        }
                        if (!isRetry) {
                            GoogleAuthenticator.this.invalidateToken(token2);
                        }
                        if (!isRetry) {
                            isRetry = true;
                            if (status == Status.ERROR_NETWORK) {
                                Logger.d(GoogleAuthenticator.TAG, "Network or server error, wait before retry");
                                SystemClock.sleep(3000);
                            }
                        } else if (Log.isLoggable(GoogleAuthenticator.TAG, 6)) {
                            Log.e(GoogleAuthenticator.TAG, "All attempts to authenticate failed");
                        }
                    } catch (IOException e) {
                        if (Log.isLoggable(GoogleAuthenticator.TAG, 6)) {
                            Log.e(GoogleAuthenticator.TAG, "ERROR - Network or server problem. " + e);
                        }
                        status = Status.ERROR_NETWORK;
                    } catch (UserRecoverableAuthException e2) {
                        if (Log.isLoggable(GoogleAuthenticator.TAG, 5)) {
                            Log.w(GoogleAuthenticator.TAG, "User interaction required. " + e2);
                        }
                        if (allowUserRecovery) {
                            GoogleAuthenticator.this.startUserRecoveryActivity(e2.getIntent());
                            return;
                        }
                        status = Status.ERROR_NOT_AUTHORIZED;
                    } catch (GoogleAuthException e3) {
                        if (Log.isLoggable(GoogleAuthenticator.TAG, 6)) {
                            Log.e(GoogleAuthenticator.TAG, "ERROR - Could not authenticate. " + e3.toString());
                        }
                        status = Status.ERROR_NOT_AUTHORIZED;
                    } catch (Exception e4) {
                        if (Log.isLoggable(GoogleAuthenticator.TAG, 6)) {
                            Log.e(GoogleAuthenticator.TAG, "ERROR - Could not authenticate. " + e4.toString());
                        }
                        status = Status.ERROR_NOT_AUTHORIZED;
                    }
                }
                if (status != Status.SUCCESS || !GoogleAuthenticator.this.shouldInvalidateToken()) {
                    if (status == Status.SUCCESS && tokenInfo.isEmpty()) {
                        status = Status.ERROR_GENERAL;
                    }
                    GoogleAuthenticator googleAuthenticator = GoogleAuthenticator.this;
                    if (status != Status.SUCCESS) {
                        tokenInfo = null;
                    }
                    googleAuthenticator.onAuthComplete(status, tokenInfo);
                    return;
                }
                Logger.d(GoogleAuthenticator.TAG, "Token refresh was requested but missed that, restarting...");
                GoogleAuthenticator.this.authenticate(true);
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void invalidateToken(String token) {
        Logger.d(TAG, "Invalidating token: " + Logger.obfuscate(token) + " ...");
        GoogleAuthUtil.invalidateToken(this.mContext, token);
        synchronized (this) {
            this.mTokenRefreshRequested = false;
        }
    }

    /* access modifiers changed from: private */
    public synchronized boolean shouldInvalidateToken() {
        return this.mTokenRefreshRequested;
    }

    /* access modifiers changed from: private */
    public Status validateToken(String account, String token, TokenInfo outTokenInfo) {
        Logger.d(TAG, "Validating token: " + Logger.obfuscate(token) + " ...");
        if (TextUtils.isEmpty(account) || TextUtils.isEmpty(token)) {
            return Status.ERROR_GENERAL;
        }
        try {
            String response = HttpHelper.sendGetRequest("https://www.googleapis.com/oauth2/v2/tokeninfo?access_token=" + token, null, null, false);
            if (response == null) {
                if (Log.isLoggable(TAG, 6)) {
                    Log.e(TAG, "ERROR - Got invalid response");
                }
                return Status.ERROR_GENERAL;
            }
            try {
                JSONObject responseJson = new JSONObject(response);
                String userId = null;
                if (responseJson.has("user_id")) {
                    userId = responseJson.getString("user_id");
                    Logger.d(TAG, "Token userId: " + Logger.obfuscate(userId));
                }
                long expiresIn = 0;
                if (responseJson.has("expires_in")) {
                    expiresIn = responseJson.getLong("expires_in");
                    Logger.d(TAG, "Token expires in: " + expiresIn + " sec");
                }
                if (TextUtils.isEmpty(userId) || expiresIn <= 600) {
                    return Status.ERROR_NOT_AUTHORIZED;
                }
                long validUntil = System.currentTimeMillis() + ((expiresIn - 600) * 1000);
                if (outTokenInfo != null) {
                    outTokenInfo.setAccount(account);
                    outTokenInfo.setToken(token);
                    outTokenInfo.setUserId(userId);
                    outTokenInfo.setValidUntil(validUntil);
                }
                return Status.SUCCESS;
            } catch (JSONException e) {
                if (Log.isLoggable(TAG, 6)) {
                    Log.e(TAG, "ERROR - Got unexpected response. " + e);
                }
                return Status.ERROR_GENERAL;
            }
        } catch (UnknownHostException e2) {
            if (Log.isLoggable(TAG, 6)) {
                Log.e(TAG, "ERROR - Network or server problem. " + e2);
            }
            return Status.ERROR_NETWORK;
        } catch (ConnectionException e3) {
            if (Log.isLoggable(TAG, 6)) {
                Log.e(TAG, "ERROR - Network or server problem. " + e3 + " - Status code: " + e3.getStatusCode());
            }
            return Status.ERROR_NETWORK;
        }
    }
}
