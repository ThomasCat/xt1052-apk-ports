package com.google.android.gms.tagmanager;

import android.content.Context;
import android.os.Process;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.internal.fl;
import com.google.android.gms.internal.fn;
import java.io.IOException;

class a {
    private static a TA;
    private static Object qI = new Object();
    private volatile long Tv;
    private volatile long Tw;
    private volatile long Tx;
    private final fl Ty;
    private C0085a Tz;
    private volatile boolean mClosed;
    /* access modifiers changed from: private */
    public final Context mContext;
    private final Thread pI;
    private volatile Info qK;

    /* renamed from: com.google.android.gms.tagmanager.a$a reason: collision with other inner class name */
    public interface C0085a {
        Info ix();
    }

    private a(Context context) {
        this(context, null, fn.eI());
    }

    a(Context context, C0085a aVar, fl flVar) {
        this.Tv = 900000;
        this.Tw = 30000;
        this.mClosed = false;
        this.Tz = new C0085a() {
            public Info ix() {
                boolean z = false;
                try {
                    return AdvertisingIdClient.getAdvertisingIdInfo(a.this.mContext);
                } catch (IllegalStateException e) {
                    bh.w("IllegalStateException getting Advertising Id Info");
                    return z;
                } catch (GooglePlayServicesRepairableException e2) {
                    bh.w("GooglePlayServicesRepairableException getting Advertising Id Info");
                    return z;
                } catch (IOException e3) {
                    bh.w("IOException getting Ad Id Info");
                    return z;
                } catch (GooglePlayServicesNotAvailableException e4) {
                    bh.w("GooglePlayServicesNotAvailableException getting Advertising Id Info");
                    return z;
                } catch (Exception e5) {
                    bh.w("Unknown exception. Could not get the Advertising Id Info.");
                    return z;
                }
            }
        };
        this.Ty = flVar;
        if (context != null) {
            this.mContext = context.getApplicationContext();
        } else {
            this.mContext = context;
        }
        if (aVar != null) {
            this.Tz = aVar;
        }
        this.pI = new Thread(new Runnable() {
            public void run() {
                a.this.iv();
            }
        });
    }

    static a E(Context context) {
        if (TA == null) {
            synchronized (qI) {
                if (TA == null) {
                    TA = new a(context);
                    TA.start();
                }
            }
        }
        return TA;
    }

    /* access modifiers changed from: private */
    public void iv() {
        Process.setThreadPriority(10);
        while (!this.mClosed) {
            try {
                this.qK = this.Tz.ix();
                Thread.sleep(this.Tv);
            } catch (InterruptedException e) {
                bh.u("sleep interrupted in AdvertiserDataPoller thread; continuing");
            }
        }
    }

    private void iw() {
        if (this.Ty.currentTimeMillis() - this.Tx >= this.Tw) {
            interrupt();
            this.Tx = this.Ty.currentTimeMillis();
        }
    }

    /* access modifiers changed from: 0000 */
    public void interrupt() {
        this.pI.interrupt();
    }

    public boolean isLimitAdTrackingEnabled() {
        iw();
        if (this.qK == null) {
            return true;
        }
        return this.qK.isLimitAdTrackingEnabled();
    }

    public String iu() {
        iw();
        if (this.qK == null) {
            return null;
        }
        return this.qK.getId();
    }

    /* access modifiers changed from: 0000 */
    public void start() {
        this.pI.start();
    }
}
