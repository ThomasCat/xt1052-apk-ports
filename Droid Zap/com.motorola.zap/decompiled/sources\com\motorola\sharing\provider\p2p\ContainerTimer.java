package com.motorola.sharing.provider.p2p;

import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.util.Logger;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class ContainerTimer {
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getP2pTag("ContainerTimer");
    private List<ContainerInfo> mContainerList = new LinkedList();
    private ExpiryHandler mExpiryHandler = new ExpiryHandler(this);
    private ExpiryListener mExpiryListener = null;

    private class ContainerInfo {
        public ContentContainer container;
        public long expiryTime;

        public ContainerInfo(ContentContainer container2, long expiryTime2) {
            this.container = container2;
            this.expiryTime = expiryTime2;
        }
    }

    private static class ExpiryHandler extends Handler {
        private WeakReference<ContainerTimer> mTimer = null;

        public ExpiryHandler(ContainerTimer timer) {
            this.mTimer = new WeakReference<>(timer);
        }

        public void handleMessage(Message msg) {
            if (msg.what == 4000) {
                Logger.i(ContainerTimer.TAG, "Container expired");
                ContainerTimer timer = (ContainerTimer) this.mTimer.get();
                if (timer != null) {
                    timer.expired();
                }
            }
        }
    }

    public interface ExpiryListener {
        void onContainerExpired(ContentContainer contentContainer);
    }

    public ContainerTimer(ExpiryListener listener) {
        this.mExpiryListener = listener;
    }

    public void add(ContentContainer container) {
        stopTimer();
        long newExpiry = SystemClock.elapsedRealtime() + (container.getTtl() > 0 ? container.getTtl() : 120000);
        ContainerInfo newInfo = new ContainerInfo(container, newExpiry);
        ListIterator<ContainerInfo> iter = this.mContainerList.listIterator();
        while (true) {
            if (iter.hasNext()) {
                if (((ContainerInfo) iter.next()).expiryTime > newExpiry) {
                    iter.previous();
                    iter.add(newInfo);
                    break;
                }
            } else {
                iter.add(newInfo);
                break;
            }
        }
        rescan();
    }

    public void remove(ContentContainer containerToRemove) {
        stopTimer();
        Iterator<ContainerInfo> iter = this.mContainerList.iterator();
        while (true) {
            if (iter.hasNext()) {
                if (((ContainerInfo) iter.next()).container.equals(containerToRemove)) {
                    iter.remove();
                    break;
                }
            } else {
                break;
            }
        }
        rescan();
    }

    public void removeByPath(String path) {
        stopTimer();
        Iterator<ContainerInfo> iter = this.mContainerList.iterator();
        while (true) {
            if (!iter.hasNext()) {
                break;
            }
            ContainerInfo info = (ContainerInfo) iter.next();
            if (info.container.path != null && info.container.path.equals(path)) {
                iter.remove();
                break;
            }
        }
        rescan();
    }

    public void removeByShareId(String shareId) {
        stopTimer();
        Iterator<ContainerInfo> iter = this.mContainerList.iterator();
        while (true) {
            if (!iter.hasNext()) {
                break;
            }
            ContainerInfo info = (ContainerInfo) iter.next();
            if (info.container.shareId != null && info.container.shareId.equals(shareId)) {
                iter.remove();
                break;
            }
        }
        rescan();
    }

    public void removeAll() {
        stopTimer();
        this.mContainerList.clear();
    }

    private void rescan() {
        long now = SystemClock.elapsedRealtime();
        Iterator<ContainerInfo> iter = this.mContainerList.iterator();
        while (iter.hasNext()) {
            ContainerInfo info = (ContainerInfo) iter.next();
            if (info.expiryTime <= now) {
                iter.remove();
                if (this.mExpiryListener != null) {
                    this.mExpiryListener.onContainerExpired(info.container);
                }
            }
        }
        if (this.mContainerList.size() > 0) {
            startTimer(((ContainerInfo) this.mContainerList.get(0)).expiryTime - now);
        }
    }

    private void startTimer(long delay) {
        this.mExpiryHandler.removeMessages(4000);
        this.mExpiryHandler.sendEmptyMessageDelayed(4000, delay);
        Logger.i(TAG, "Timer started");
    }

    private void stopTimer() {
        this.mExpiryHandler.removeMessages(4000);
    }

    /* access modifiers changed from: private */
    public void expired() {
        rescan();
    }
}
