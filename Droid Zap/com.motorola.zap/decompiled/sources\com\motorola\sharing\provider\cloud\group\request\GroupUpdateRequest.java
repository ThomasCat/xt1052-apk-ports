package com.motorola.sharing.provider.cloud.group.request;

import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener.GroupEvent.Type;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker.GroupState;
import com.motorola.sharing.util.Logger;

public class GroupUpdateRequest extends GroupRequest {
    private static final String TAG = Logger.getCloudTag("GroupUpdateReq");

    public GroupUpdateRequest() {
        super(GCSP.getContext(), false);
    }

    public Type getWhat() {
        return Type.GROUP_UPDATE;
    }

    public void run() {
        Logger.d(TAG, String.format("Running request, retry: %b", new Object[]{Boolean.valueOf(isRetry())}));
        GroupState groupState = this.mGroupStateTracker.getState();
        onGroupUpdate();
        Logger.d(TAG, "Group is " + (groupState.isActive() ? "active - gid: " + groupState.getId() : "not active"));
        sendOk();
    }
}
