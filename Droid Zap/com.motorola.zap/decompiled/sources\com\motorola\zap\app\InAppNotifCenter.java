package com.motorola.zap.app;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.Iterator;
import java.util.Vector;

public class InAppNotifCenter {
    public static final int EVENT_ZAP_CANCEL_ALL = 9;
    public static final int EVENT_ZAP_PROGRESS = 8;
    public static final int EVENT_ZAP_RECEIVE_CANCEL = 7;
    public static final int EVENT_ZAP_RECEIVE_CANCELED = 5;
    public static final int EVENT_ZAP_RECEIVE_END = 3;
    public static final int EVENT_ZAP_RECEIVE_START = 1;
    public static final int EVENT_ZAP_SEND_CANCEL = 6;
    public static final int EVENT_ZAP_SEND_CANCELED = 4;
    public static final int EVENT_ZAP_SEND_END = 2;
    public static final int EVENT_ZAP_SEND_START = 0;
    public static final int EVENT_ZZ_BEGIN = 10;
    public static final int EVENT_ZZ_END = 11;
    private static InAppNotifCenter sInAppNotificationCenter = null;
    private final Vector<ICallbacks> mCallbackList = new Vector<>();
    private final Handler mEventDispatcher = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            InAppNotifCenter.this.distributeEvent(msg);
        }
    };

    public interface ICallbacks {
        void eventChanged();
    }

    public static synchronized InAppNotifCenter getInAppNotifCenter() {
        InAppNotifCenter inAppNotifCenter;
        synchronized (InAppNotifCenter.class) {
            if (sInAppNotificationCenter == null) {
                sInAppNotificationCenter = new InAppNotifCenter();
            }
            inAppNotifCenter = sInAppNotificationCenter;
        }
        return inAppNotifCenter;
    }

    public void registerForEvents(ICallbacks callback) {
        int i = 0;
        while (i < this.mCallbackList.size()) {
            try {
                if (!((ICallbacks) this.mCallbackList.elementAt(i)).equals(callback)) {
                    i++;
                } else {
                    return;
                }
            } catch (Exception e) {
                return;
            }
        }
        this.mCallbackList.add(callback);
        callback.eventChanged();
    }

    public void deregisterForEvents(ICallbacks callback) {
        int i = 0;
        while (i < this.mCallbackList.size()) {
            try {
                if (((ICallbacks) this.mCallbackList.elementAt(i)).equals(callback)) {
                    this.mCallbackList.removeElementAt(i);
                }
                i++;
            } catch (Exception e) {
                return;
            }
        }
    }

    public void postEvent(int event) {
        Message message = new Message();
        message.what = event;
        message.obj = null;
        this.mEventDispatcher.sendMessage(message);
    }

    public void postEvent(int event, Bundle bundle) {
        Message message = new Message();
        message.what = event;
        message.obj = bundle;
        this.mEventDispatcher.sendMessage(message);
    }

    /* access modifiers changed from: private */
    public void distributeEvent(Message msg) {
        eventReceived(msg.what, (Bundle) msg.obj);
    }

    private synchronized void eventReceived(int event, Bundle bundle) {
        Iterator i$ = this.mCallbackList.iterator();
        while (i$.hasNext()) {
            ((ICallbacks) i$.next()).eventChanged();
        }
    }
}
