package com.motorola.sharing.instrumentation;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentContainer.Type;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.instrumentation.ActionResult.Status;
import com.motorola.sharing.provider.cloud.group.GroupHandler;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker.GroupState;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker.GroupStats;
import com.motorola.sharing.util.Logger;
import com.motorola.sharing.util.NetworkUtils;
import java.util.ArrayList;
import java.util.List;

public class CheckinManager implements CheckinInterface {
    private static final String TAG = Logger.getGcspTag("CheckinManager");
    private static CheckinManager sInstance = null;
    private Context mContext = null;
    private List<CheckinInterface> mSinks = null;

    private class Warning extends Throwable {
        private Warning() {
        }
    }

    public static synchronized CheckinManager getInstance() {
        CheckinManager checkinManager;
        synchronized (CheckinManager.class) {
            if (sInstance == null) {
                sInstance = new CheckinManager(GCSP.getContext());
            }
            checkinManager = sInstance;
        }
        return checkinManager;
    }

    private CheckinManager(Context context) {
        this.mContext = context;
        this.mSinks = new ArrayList();
        if (MotoCheckin.isSupported()) {
            this.mSinks.add(MotoCheckin.getInstance());
        }
        if (GoogleCheckin.isSupported()) {
            this.mSinks.add(GoogleCheckin.getInstance());
        }
    }

    public void logException(Thread thread, Throwable exception, boolean fatal) {
        if (this.mSinks.size() > 0) {
            if (thread == null) {
                thread = Thread.currentThread();
            }
            for (CheckinInterface sink : this.mSinks) {
                sink.logException(thread, exception, fatal);
            }
        }
    }

    public void logZapContext(ContentContainer container) {
        if (this.mSinks.size() > 0) {
            if (container.type == Type.SHARE || container.type == Type.RECEIVE) {
                for (CheckinInterface sink : this.mSinks) {
                    sink.logZapContext(container);
                }
                return;
            }
            printFixMe();
        }
    }

    public void logZapEvent(ContentContainer container) {
        if (this.mSinks.size() > 0) {
            if ((container.type == Type.SHARE || container.type == Type.RECEIVE) && container.actionResult.getStatus() != Status.UNKNOWN) {
                if (container.isGroupRelated()) {
                    incrementGroupStatsCount(container);
                }
                addNetworkType(container);
                for (CheckinInterface sink : this.mSinks) {
                    sink.logZapEvent(container);
                }
                return;
            }
            printFixMe();
        }
    }

    public void logGroupEvent(GroupState info) {
        if (this.mSinks.size() > 0 && info.getStartedTime() > 0 && info.getStoppedTime() > 0) {
            for (CheckinInterface sink : this.mSinks) {
                sink.logGroupEvent(info);
            }
        }
    }

    public void logZapCancelShare(ContentContainer container) {
        if (this.mSinks.size() > 0) {
            if (container.type != Type.CANCEL_SHARE || container.actionResult.getStatus() == Status.UNKNOWN) {
                printFixMe();
                return;
            }
            addNetworkType(container);
            for (CheckinInterface sink : this.mSinks) {
                sink.logZapCancelShare(container);
            }
        }
    }

    public void logZapAbuse(ContentContainer container) {
        if (this.mSinks.size() > 0) {
            if (container.type != Type.REPORT_ABUSE || container.actionResult.getStatus() == Status.UNKNOWN) {
                printFixMe();
                return;
            }
            for (CheckinInterface sink : this.mSinks) {
                sink.logZapAbuse(container);
            }
        }
    }

    public void logSettingsSync(Operation operation) {
        if (this.mSinks.size() > 0) {
            if (operation.type != Operation.Type.SETTINGS_SYNC || operation.actionResult.getStatus() == Status.UNKNOWN) {
                printFixMe();
                return;
            }
            for (CheckinInterface sink : this.mSinks) {
                sink.logSettingsSync(operation);
            }
        }
    }

    public void logZapFeatureOptIn(boolean optedIn) {
        for (CheckinInterface sink : this.mSinks) {
            sink.logZapFeatureOptIn(optedIn);
        }
    }

    public void logZapEnabledEvent(boolean enabled) {
        for (CheckinInterface sink : this.mSinks) {
            sink.logZapEnabledEvent(enabled);
        }
    }

    public void logWifiEnabledEvent(boolean enabled) {
        for (CheckinInterface sink : this.mSinks) {
            sink.logWifiEnabledEvent(enabled);
        }
    }

    public void logSettingsLaunchPoint(String launchPoint) {
        for (CheckinInterface sink : this.mSinks) {
            sink.logSettingsLaunchPoint(launchPoint);
        }
    }

    public void logShareAppEvent(boolean sendLinkPressed) {
        for (CheckinInterface sink : this.mSinks) {
            sink.logShareAppEvent(sendLinkPressed);
        }
    }

    private void incrementGroupStatsCount(ContentContainer container) {
        GroupStats stats = null;
        switch (container.actionResult.getStatus()) {
            case SUCCESS:
                if (container.type != Type.SHARE) {
                    if (!isAutoProcessing(container)) {
                        stats = GroupStats.MANUALLY_RECEIVED;
                        break;
                    } else {
                        stats = GroupStats.AUTO_RECEIVED;
                        break;
                    }
                } else if (!isAutoProcessing(container)) {
                    stats = GroupStats.MANUALLY_SENT;
                    break;
                } else {
                    stats = GroupStats.AUTO_SENT;
                    break;
                }
            case FAIL:
                if (container.type != Type.SHARE) {
                    stats = GroupStats.RECEIVING_ERRORS;
                    break;
                } else {
                    stats = GroupStats.SENDING_ERRORS;
                    break;
                }
        }
        if (stats != null) {
            GroupHandler.getInstance().incrementStatsCount(stats, 1, container.groupInfo.owner, container.groupInfo.id);
        }
    }

    private void addNetworkType(ContentContainer container) {
        String networkType = NetworkUtils.getActiveNetworkType(this.mContext);
        if (!TextUtils.isEmpty(networkType)) {
            container.putCheckinData(CheckinInterface.KEY_NETWORK_TYPE, networkType);
        }
    }

    private void printFixMe() {
        if (Logger.isDebuggable()) {
            Logger.d(TAG, "please fix instrumentation: " + Log.getStackTraceString(new Warning()));
        }
    }

    protected static boolean isAutoProcessing(ContentContainer container) {
        return Boolean.parseBoolean(container.getCheckinData(CheckinInterface.KEY_AUTO_PROCESSING));
    }
}
