package com.google.sample.castcompanionlibrary.cast;

import android.content.Context;
import android.support.v7.media.MediaRouter;
import android.support.v7.media.MediaRouter.Callback;
import android.support.v7.media.MediaRouter.RouteInfo;
import com.google.android.gms.cast.CastDevice;
import com.google.sample.castcompanionlibrary.cast.BaseCastManager.ReconnectionStatus;
import com.google.sample.castcompanionlibrary.utils.LogUtils;
import com.google.sample.castcompanionlibrary.utils.Utils;

public class CastMediaRouterCallback extends Callback {
    private static final String TAG = LogUtils.makeLogTag(CastMediaRouterCallback.class);
    private final Context mContext;
    private int mRouteCount = 0;
    private final DeviceSelectionListener selectDeviceInterface;

    public CastMediaRouterCallback(DeviceSelectionListener callback, Context context) {
        this.selectDeviceInterface = callback;
        this.mContext = context;
    }

    public void onRouteSelected(MediaRouter router, RouteInfo info) {
        LogUtils.LOGD(TAG, "onRouteSelected: info=" + info);
        if (BaseCastManager.getCastManager().getReconnectionStatus() == ReconnectionStatus.FINALIZE) {
            BaseCastManager.getCastManager().setReconnectionStatus(ReconnectionStatus.INACTIVE);
            BaseCastManager.getCastManager().cancelReconnectionTask();
            return;
        }
        Utils.saveStringToPreference(this.mContext, BaseCastManager.PREFS_KEY_ROUTE_ID, info.getId());
        CastDevice device = CastDevice.getFromBundle(info.getExtras());
        this.selectDeviceInterface.onDeviceSelected(device);
        LogUtils.LOGD(TAG, "onResult: mSelectedDevice=" + device.getFriendlyName());
    }

    public void onRouteUnselected(MediaRouter router, RouteInfo route) {
        LogUtils.LOGD(TAG, "onRouteUnselected: route=" + route);
        this.selectDeviceInterface.onDeviceSelected(null);
    }

    public void onRouteAdded(MediaRouter router, RouteInfo route) {
        super.onRouteAdded(router, route);
        if (!router.getDefaultRoute().equals(route)) {
            int i = this.mRouteCount + 1;
            this.mRouteCount = i;
            if (i == 1) {
                BaseCastManager.getCastManager().onCastAvailabilityChanged(true);
            }
            this.selectDeviceInterface.onCastDeviceDetected(route);
        }
        if (BaseCastManager.getCastManager().getReconnectionStatus() == ReconnectionStatus.STARTED) {
            if (route.getId().equals(Utils.getStringFromPreference(this.mContext, BaseCastManager.PREFS_KEY_ROUTE_ID))) {
                LogUtils.LOGD(TAG, "onRouteAdded: Attempting to recover a session with info=" + route);
                BaseCastManager.getCastManager().setReconnectionStatus(ReconnectionStatus.IN_PROGRESS);
                CastDevice device = CastDevice.getFromBundle(route.getExtras());
                LogUtils.LOGD(TAG, "onRouteAdded: Attempting to recover a session with device: " + device.getFriendlyName());
                this.selectDeviceInterface.onDeviceSelected(device);
            }
        }
    }

    public void onRouteRemoved(MediaRouter router, RouteInfo route) {
        super.onRouteRemoved(router, route);
        int i = this.mRouteCount - 1;
        this.mRouteCount = i;
        if (i == 0) {
            BaseCastManager.getCastManager().onCastAvailabilityChanged(false);
        }
    }
}
