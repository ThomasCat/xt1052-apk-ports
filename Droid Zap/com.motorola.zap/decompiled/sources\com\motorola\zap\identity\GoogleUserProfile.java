package com.motorola.zap.identity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.plus.PlusShare;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.Profile;
import com.motorola.sharing.ProfileManager;
import com.motorola.sharing.ProfileManagerListener;
import com.motorola.sharing.ProfileManagerListener.ProfileEvent;
import com.motorola.sharing.util.GooglePlusProfileListener;
import com.motorola.sharing.util.GooglePlusProfileListener.Status;
import com.motorola.sharing.util.GooglePlusProfileListener.Substatus;
import com.motorola.zap.R;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.cacheutils.ZapImageCache;
import org.json.JSONException;
import org.json.JSONObject;

public class GoogleUserProfile implements GooglePlusProfileListener {
    private View mProfileView;

    public GoogleUserProfile(View v) {
        this.mProfileView = v;
    }

    public void loadGoogleProfile(Profile aProfile) {
        ZapImageCache.getInstance().getImageLoader().get(aProfile.pictureUrl, ImageLoader.getImageListener((ImageView) this.mProfileView.findViewById(R.id.profile_image), R.drawable.ic_default_profile, R.drawable.ic_default_profile));
        ((TextView) this.mProfileView.findViewById(R.id.profile_username)).setText(aProfile.nickname);
        ZapProfileSharedPerfs.ZAP_PROFILE_EMAIL.write(GCSP.getAuthenticator().getAccount());
    }

    public void onProfile(JSONObject profile, Status status, Substatus substatus) {
        if (status == Status.SUCCESS || substatus == Substatus.NO_ERROR) {
            try {
                final Profile lProfile = new Profile();
                String fullName = profile.getString("displayName");
                String[] elements = fullName.split(" ");
                if (elements.length > 0) {
                    String initial = ZapConstants.GOOGLE_ACCOUNT_NONE;
                    if (elements[1].length() > 0) {
                        initial = String.valueOf(elements[1].charAt(0));
                    }
                    lProfile.nickname = elements[0] + " " + initial;
                } else {
                    lProfile.nickname = fullName;
                }
                lProfile.pictureUrl = profile.getJSONObject("image").getString(PlusShare.KEY_CALL_TO_ACTION_URL) + "&sz=360";
                lProfile.anonymous = false;
                loadGoogleProfile(lProfile);
                if (!((Boolean) ZapProfileSharedPerfs.ZAP_PROFILE_ACTIVATE.read()).booleanValue()) {
                    lProfile.anonymous = true;
                }
                ProfileManager.getInstance().updateMyProfile(lProfile, new ProfileManagerListener() {
                    public void onProfileEvent(ProfileEvent event) {
                        if (event.status == ProfileEvent.Status.SUCCESS) {
                            lProfile.userId = event.profile.userId;
                            ZapProfileSharedPerfs.backupProfile(lProfile);
                        }
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
