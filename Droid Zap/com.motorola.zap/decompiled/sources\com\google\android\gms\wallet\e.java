package com.google.android.gms.wallet;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;

public class e implements Creator<d> {
    static void a(d dVar, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.c(parcel, 1, dVar.getVersionCode());
        b.a(parcel, 2, (Parcelable) dVar.Yj, i, false);
        b.D(parcel, p);
    }

    /* renamed from: aV */
    public d createFromParcel(Parcel parcel) {
        int o = a.o(parcel);
        int i = 0;
        LoyaltyWalletObject loyaltyWalletObject = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    i = a.g(parcel, n);
                    break;
                case 2:
                    loyaltyWalletObject = (LoyaltyWalletObject) a.a(parcel, n, LoyaltyWalletObject.CREATOR);
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new d(i, loyaltyWalletObject);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }

    /* renamed from: cb */
    public d[] newArray(int i) {
        return new d[i];
    }
}
