package com.google.android.gms.drive.internal;

import com.google.android.gms.internal.jy;
import com.google.android.gms.internal.jz;
import com.google.android.gms.internal.kd;
import com.google.android.gms.internal.ke;
import com.google.android.gms.internal.kh;
import com.motorola.zap.ZapConstants;
import java.io.IOException;

public final class y extends ke {
    public static final y[] DU = new y[0];
    public String DV = ZapConstants.GOOGLE_ACCOUNT_NONE;
    public long DW = -1;
    public long DX = -1;
    private int DY = -1;
    public int versionCode = 1;

    public static y g(byte[] bArr) throws kd {
        return (y) ke.a(new y(), bArr);
    }

    public void a(jz jzVar) throws IOException {
        jzVar.f(1, this.versionCode);
        jzVar.b(2, this.DV);
        jzVar.c(3, this.DW);
        jzVar.c(4, this.DX);
    }

    public int c() {
        int g = 0 + jz.g(1, this.versionCode) + jz.g(2, this.DV) + jz.e(3, this.DW) + jz.e(4, this.DX);
        this.DY = g;
        return g;
    }

    public int eW() {
        if (this.DY < 0) {
            c();
        }
        return this.DY;
    }

    /* renamed from: m */
    public y b(jy jyVar) throws IOException {
        while (true) {
            int ky = jyVar.ky();
            switch (ky) {
                case 0:
                    break;
                case 8:
                    this.versionCode = jyVar.kB();
                    continue;
                case 18:
                    this.DV = jyVar.readString();
                    continue;
                case 24:
                    this.DW = jyVar.kD();
                    continue;
                case 32:
                    this.DX = jyVar.kD();
                    continue;
                default:
                    if (!kh.b(jyVar, ky)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }
}
