package com.motorola.zap.ui;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.provider.p2p.NanoHTTPD;
import com.motorola.zap.R;

public class DroidBlastShareAppDialogFragment extends DialogFragment {
    String mShareURL;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String dialogMessage = getString(R.string.settings_share_app_dialog_message, new Object[]{getString(R.string.settings_share_app_url)});
        this.mShareURL = getString(R.string.settings_share_app_url);
        Builder builder = new Builder(getActivity());
        builder.setMessage(Html.fromHtml(dialogMessage)).setTitle(R.string.settings_share_app_dialog_title).setPositiveButton(R.string.settings_share_app_dialog_ok, new OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DroidBlastShareAppDialogFragment.this.sendShareAppIntent();
                GCSP.getCheckinManager().logShareAppEvent(true);
            }
        }).setNegativeButton(17039360, new OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                GCSP.getCheckinManager().logShareAppEvent(false);
            }
        });
        return builder.create();
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        GCSP.getCheckinManager().logShareAppEvent(false);
    }

    /* access modifiers changed from: private */
    public void sendShareAppIntent() {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.TEXT", this.mShareURL);
        intent.setType(NanoHTTPD.MIME_PLAINTEXT);
        startActivity(Intent.createChooser(intent, getString(R.string.menu_share_app)));
    }
}
