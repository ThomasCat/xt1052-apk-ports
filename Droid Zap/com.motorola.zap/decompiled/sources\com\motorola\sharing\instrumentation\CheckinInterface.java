package com.motorola.sharing.instrumentation;

import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker.GroupState;

public interface CheckinInterface {
    public static final String KEY_AUTO_PROCESSING = "auto";
    public static final String KEY_DEST = "dest";
    public static final String KEY_MODALITY = "loc_mod";
    public static final String KEY_NETWORK_TYPE = "net_type";
    public static final String KEY_SHARE_TYPE = "share_type";
    public static final String VAL_DEST_CHROMECAST = "Chromecast";
    public static final String VAL_DEST_CLOUD = "Cloud";
    public static final String VAL_SHARE_TYPE_GESTURE = "Gesture";
    public static final String VAL_SHARE_TYPE_KEYPRESS = "Keypress";
    public static final String VAL_SHARE_TYPE_VOICE = "Voice";

    void logException(Thread thread, Throwable th, boolean z);

    void logGroupEvent(GroupState groupState);

    void logSettingsLaunchPoint(String str);

    void logSettingsSync(Operation operation);

    void logShareAppEvent(boolean z);

    void logWifiEnabledEvent(boolean z);

    void logZapAbuse(ContentContainer contentContainer);

    void logZapCancelShare(ContentContainer contentContainer);

    void logZapContext(ContentContainer contentContainer);

    void logZapEnabledEvent(boolean z);

    void logZapEvent(ContentContainer contentContainer);

    void logZapFeatureOptIn(boolean z);
}
