package com.google.android.gms.analytics;

import android.content.Context;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import com.google.android.gms.analytics.u.a;

class r extends af {
    /* access modifiers changed from: private */
    public static final Object ri = new Object();
    private static r ru;
    private Context mContext;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private d rj;
    private volatile f rk;
    /* access modifiers changed from: private */
    public int rl = 1800;
    private boolean rm = true;
    private boolean rn;
    private String ro;
    /* access modifiers changed from: private */
    public boolean rp = true;
    private boolean rq = true;
    private e rr = new e() {
        public void p(boolean z) {
            r.this.a(z, r.this.rp);
        }
    };
    private q rs;
    /* access modifiers changed from: private */
    public boolean rt = false;

    private r() {
    }

    public static r bB() {
        if (ru == null) {
            ru = new r();
        }
        return ru;
    }

    private void bC() {
        this.rs = new q(this);
        this.rs.o(this.mContext);
    }

    private void bD() {
        this.mHandler = new Handler(this.mContext.getMainLooper(), new Callback() {
            public boolean handleMessage(Message msg) {
                if (1 == msg.what && r.ri.equals(msg.obj)) {
                    u.bR().r(true);
                    r.this.dispatchLocalHits();
                    u.bR().r(false);
                    if (r.this.rl > 0 && !r.this.rt) {
                        r.this.mHandler.sendMessageDelayed(r.this.mHandler.obtainMessage(1, r.ri), (long) (r.this.rl * 1000));
                    }
                }
                return true;
            }
        });
        if (this.rl > 0) {
            this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(1, ri), (long) (this.rl * 1000));
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(Context context, f fVar) {
        if (this.mContext == null) {
            this.mContext = context.getApplicationContext();
            if (this.rk == null) {
                this.rk = fVar;
                if (this.rm) {
                    dispatchLocalHits();
                    this.rm = false;
                }
                if (this.rn) {
                    br();
                    this.rn = false;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(boolean z, boolean z2) {
        if (!(this.rt == z && this.rp == z2)) {
            if (z || !z2) {
                if (this.rl > 0) {
                    this.mHandler.removeMessages(1, ri);
                }
            }
            if (!z && z2 && this.rl > 0) {
                this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(1, ri), (long) (this.rl * 1000));
            }
            aa.v("PowerSaveMode " + ((z || !z2) ? "initiated." : "terminated."));
            this.rt = z;
            this.rp = z2;
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized d bE() {
        if (this.rj == null) {
            if (this.mContext == null) {
                throw new IllegalStateException("Cant get a store unless we have a context");
            }
            this.rj = new ac(this.rr, this.mContext);
            if (this.ro != null) {
                this.rj.bq().A(this.ro);
                this.ro = null;
            }
        }
        if (this.mHandler == null) {
            bD();
        }
        if (this.rs == null && this.rq) {
            bC();
        }
        return this.rj;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void bF() {
        if (!this.rt && this.rp && this.rl > 0) {
            this.mHandler.removeMessages(1, ri);
            this.mHandler.sendMessage(this.mHandler.obtainMessage(1, ri));
        }
    }

    /* access modifiers changed from: 0000 */
    public void br() {
        if (this.rk == null) {
            aa.v("setForceLocalDispatch() queued. It will be called once initialization is complete.");
            this.rn = true;
            return;
        }
        u.bR().a(a.SET_FORCE_LOCAL_DISPATCH);
        this.rk.br();
    }

    /* access modifiers changed from: 0000 */
    public synchronized void dispatchLocalHits() {
        if (this.rk == null) {
            aa.v("Dispatch call queued. Dispatch will run once initialization is complete.");
            this.rm = true;
        } else {
            u.bR().a(a.DISPATCH);
            this.rk.bp();
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void q(boolean z) {
        a(this.rt, z);
    }

    /* access modifiers changed from: 0000 */
    public synchronized void setLocalDispatchPeriod(int dispatchPeriodInSeconds) {
        if (this.mHandler == null) {
            aa.v("Dispatch period set with null handler. Dispatch will run once initialization is complete.");
            this.rl = dispatchPeriodInSeconds;
        } else {
            u.bR().a(a.SET_DISPATCH_PERIOD);
            if (!this.rt && this.rp && this.rl > 0) {
                this.mHandler.removeMessages(1, ri);
            }
            this.rl = dispatchPeriodInSeconds;
            if (dispatchPeriodInSeconds > 0 && !this.rt && this.rp) {
                this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(1, ri), (long) (dispatchPeriodInSeconds * 1000));
            }
        }
    }
}
