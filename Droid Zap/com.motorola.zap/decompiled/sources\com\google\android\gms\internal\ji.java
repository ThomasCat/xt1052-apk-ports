package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;

public class ji implements Creator<jh> {
    static void a(jh jhVar, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.c(parcel, 1, jhVar.getVersionCode());
        b.a(parcel, 2, jhVar.label, false);
        b.a(parcel, 3, jhVar.value, false);
        b.D(parcel, p);
    }

    /* renamed from: bg */
    public jh createFromParcel(Parcel parcel) {
        String str = null;
        int o = a.o(parcel);
        int i = 0;
        String str2 = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    i = a.g(parcel, n);
                    break;
                case 2:
                    str2 = a.m(parcel, n);
                    break;
                case 3:
                    str = a.m(parcel, n);
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new jh(i, str2, str);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }

    /* renamed from: cm */
    public jh[] newArray(int i) {
        return new jh[i];
    }
}
