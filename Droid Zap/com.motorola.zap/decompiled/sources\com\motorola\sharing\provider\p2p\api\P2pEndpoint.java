package com.motorola.sharing.provider.p2p.api;

import com.motorola.sharing.Endpoint;
import com.motorola.sharing.Endpoint.AttrValue;
import com.motorola.sharing.Endpoint.Attributes;

public class P2pEndpoint extends Endpoint {
    public P2pEndpoint() {
        super(new Attributes(AttrValue.STATIC, AttrValue.DIRECT, AttrValue.SHARE_AND_RECEIVE));
    }

    public String getName() {
        return P2pEndpoint.class.getCanonicalName();
    }
}
