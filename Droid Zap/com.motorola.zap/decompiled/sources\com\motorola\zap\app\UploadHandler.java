package com.motorola.zap.app;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.drive.DriveFile;
import com.motorola.sharing.CastManager;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentContainer.Builder;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.Locality;
import com.motorola.sharing.instrumentation.ActionResult.Error;
import com.motorola.sharing.instrumentation.CheckinInterface;
import com.motorola.sharing.util.FileUtils;
import com.motorola.zap.LogUtils;
import com.motorola.zap.PinGenerator;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConfig;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.dialogs.DialogMessageActivity;
import com.motorola.zap.location.BlastLocationProvider;
import com.motorola.zap.location.BlastLocationProvider.GeoLocationInfo;
import com.motorola.zap.location.BlastLocationProvider.LocationResult;
import com.motorola.zap.location.ModalityHelper;
import com.motorola.zap.provider.HistoryHandler;
import com.motorola.zap.ui.ZapUi;
import com.motorola.zap.ui.ZapUi.LocationEvent;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

public class UploadHandler {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private static Map<Long, Long> sLastItemsShared = new Hashtable();
    private static String sLastPathShared = ZapConstants.GOOGLE_ACCOUNT_NONE;
    private static long sLastPathSharedTime = 0;
    private static final PinGenerator sPinGenerator = new PinGenerator();
    Context mContext = App.getAppContext();
    ZapUi mUi = ZapUi.getInstance();

    public void handleUploadGesture() {
        if (Log.isLoggable("Zap-UploadHandler", 4)) {
            Log.i("Zap-UploadHandler", "ACTION_GS_SWIPE_UP");
        }
        if (!ZapUtils.isPhoneLocked()) {
            String foreground = ZapUtils.getForegroundApp();
            if (DBG) {
                Log.d("Zap-UploadHandler", "Foreground app: " + foreground);
            }
            if (isGalleryPkg(foreground)) {
                this.mContext.sendBroadcast(new Intent(ZapConstants.ACTION_GS_GALLERY_SHARE_PHOTO), "com.motorola.blast.permission.TRIGGER_BLAST_ACTION");
            }
        } else if (Log.isLoggable("Zap-UploadHandler", 4)) {
            Log.i("Zap-UploadHandler", "Phone is locked - ignoring action");
        }
    }

    private boolean isGalleryPkg(String pkg) {
        return pkg != null && pkg.toLowerCase(Locale.ROOT).contains("gallery") && pkg.toLowerCase(Locale.ROOT).contains("motorola");
    }

    public void handleUpload(Intent intent) {
        if (DBG) {
            Log.d("Zap-UploadHandler", "ACTION_GS_GALLERY_PHOTO_SHARED");
        }
        if (!ZapUtils.hasUserSignedOut()) {
            handleActionShare(intent);
        } else if (intent.getBooleanExtra(ZapConstants.EXTRA_FROM_SHARE_INTENT, false)) {
            ZapUtils.startSetupActivity(intent, true);
        } else {
            ZapUtils.startSetupActivity(intent, false);
        }
    }

    private void handleActionShare(Intent intent) {
        if (intent != null) {
            try {
                ContentContainer container = new Builder().setPath(getSharedMediaFromIntent(intent)).setTtl(120000).build();
                container.putCheckinData(CheckinInterface.KEY_SHARE_TYPE, getShareType(intent));
                if (intent.getBooleanExtra(ZapConstants.EXTRA_AUTO_SHARE, false)) {
                    ZapUtils.setAutoProcessing(container, true);
                }
                container.setPin(container.isGroupRelated() ? null : getPin());
                if (container.isGroupRelated() && FileUtils.isVideo(container.getMimeType()) && !ZapConfig.supportVideoInZapZone()) {
                    this.mUi.showToastMessage(this.mContext.getString(R.string.zz_video_sent_alert), ZapUi.getZzToastIconId(), true, true);
                } else if (ZapUtils.checkGeneralTransferPreConditions(intent, container)) {
                    handleSharing(container);
                }
            } catch (IllegalStateException e) {
                Log.e("Zap-UploadHandler", "got exception:" + e);
                this.mUi.showDialogMessage(this.mContext.getString(R.string.dialog_title_generic), this.mContext.getString(R.string.dialog_msg_media_cannot_be_shared), true);
            }
        }
    }

    private String getShareType(Intent intent) {
        String req = intent.getStringExtra(ZapConstants.EXTRA_SCREENSHOT_REQ_ORIGIN);
        if (TextUtils.equals(req, ZapConstants.SCREENSHOT_VOICE_ORIGIN)) {
            return CheckinInterface.VAL_SHARE_TYPE_VOICE;
        }
        if (TextUtils.equals(req, ZapConstants.SCREENSHOT_KEYPRESS_ORIGIN)) {
            return CheckinInterface.VAL_SHARE_TYPE_KEYPRESS;
        }
        return CheckinInterface.VAL_SHARE_TYPE_GESTURE;
    }

    private String getPin() {
        if (!((Boolean) Z.CONTROL_ACCESS.read()).booleanValue()) {
            return null;
        }
        return sPinGenerator.getPin();
    }

    private String getSharedMediaFromIntent(Intent intent) {
        String mediaUri = intent.getStringExtra(ZapConstants.EXTRA_GS_GALLERY_PHOTO_SHARED);
        if (DBG) {
            Log.d("Zap-UploadHandler", "Share Intent Received URI: " + mediaUri);
        }
        if (mediaUri == null) {
            return null;
        }
        if (mediaUri.equals(ZapConstants.GS_GALLERY_CANNOT_SHARE_DRM)) {
            this.mUi.showDialogMessage(this.mContext.getString(R.string.dialog_title_generic), this.mContext.getString(R.string.dialog_msg_media_drm_protected), true);
            return null;
        } else if (!mediaUri.equals(ZapConstants.GS_GALLERY_CANNOT_SHARE_NULL)) {
            return FileUtils.getMediaPathFromUri(this.mContext, Uri.parse(mediaUri));
        } else {
            this.mUi.showDialogMessage(this.mContext.getString(R.string.dialog_title_generic), this.mContext.getString(R.string.dialog_msg_media_cannot_be_shared), true);
            return null;
        }
    }

    private boolean handleSharing(ContentContainer container) {
        String string;
        if (Log.isLoggable("Zap-UploadHandler", 4)) {
            Log.i("Zap-UploadHandler", "*** SHARING ***");
        }
        if (ZapConfig.hasMotoAccount() || ZapConfig.supportsFullFeature() || ZapConfig.isLegacyDroid() || !FileUtils.isVideo(container.getMimeType())) {
            long now = System.currentTimeMillis();
            if (now - State.sUserActionDialogLaunchedTime > 2000 && sLastPathShared.equals(container.getPath()) && now - sLastPathSharedTime < 2000) {
                if (DBG) {
                    Log.d("Zap-UploadHandler", "Ignoring duplicate share of same image");
                }
                return false;
            } else if (!checkSharePreconditions(container)) {
                if (Log.isLoggable("Zap-UploadHandler", 4)) {
                    Log.i("Zap-UploadHandler", "Share pre-conditions not satisfied");
                }
                return false;
            } else if (container.getUserAccount() == null) {
                if (Log.isLoggable("Zap-UploadHandler", 6)) {
                    Log.e("Zap-UploadHandler", "ERROR - Cannot Share - User account is null");
                }
                return false;
            } else if (LocationHandler.getInstance().isWaitingForLocation(container)) {
                if (DBG) {
                    Log.d("Zap-UploadHandler", "Still busy with location obtaining in another task");
                }
                this.mUi.onLocationEvent(LocationEvent.STILL_VERIFYING_LOCATION);
                return false;
            } else {
                if (container.isGroupRelated() && ((Boolean) Z.ZZ_REDUCED_IMAGE.read()).booleanValue() && container.getMimeType().contains("image/jpeg")) {
                    container.useJpegSizeReduction = true;
                }
                sLastPathShared = container.getPath();
                sLastPathSharedTime = now;
                int numUploadsInTimeWindow = getNumUploadsSince(((Long) Z.LIMIT_UPLOAD_TIME_WINDOW.read()).longValue());
                if (DBG) {
                    Log.d("Zap-UploadHandler", "Num of items shared in last " + (((Long) Z.LIMIT_UPLOAD_TIME_WINDOW.read()).longValue() / 60000) + " minutes: " + numUploadsInTimeWindow);
                }
                if (numUploadsInTimeWindow >= ((Integer) Z.MAX_UPLOADS_ALLOWED_IN_WINDOW.read()).intValue()) {
                    this.mUi.showDialogMessage(this.mContext.getString(R.string.dialog_title_generic), this.mContext.getString(R.string.dialog_msg_num_uploads_exceeded), true);
                    return false;
                }
                sLastItemsShared.put(Long.valueOf(container.getUid()), Long.valueOf(now));
                ZapUi zapUi = this.mUi;
                ZapUi.vibrate(300);
                if (!container.isGroupRelated() && !ZapUtils.isZapAppOnTop()) {
                    ZapUi zapUi2 = this.mUi;
                    if (container.getPin() != null) {
                        string = String.format(this.mContext.getString(R.string.toast_msg_getting_ready_locked), new Object[]{container.getPin()});
                    } else {
                        string = this.mContext.getString(R.string.toast_msg_getting_ready_unlocked);
                    }
                    zapUi2.showToastMessage(string, ZapUi.getSendToastIconId(), true, false);
                }
                asyncShare(container);
                return true;
            }
        } else {
            showDeviceLimitationMessage();
            return false;
        }
    }

    private void showDeviceLimitationMessage() {
        if (Log.isLoggable("Zap-UploadHandler", 4)) {
            Log.i("Zap-UploadHandler", "This version of the app does not support sending capability");
        }
        Intent intent = new Intent(this.mContext, DialogMessageActivity.class);
        intent.putExtra(ZapConstants.EXTRA_DIALOG_TITLE, this.mContext.getString(R.string.app_name));
        intent.putExtra(ZapConstants.EXTRA_DIALOG_MESSAGE, this.mContext.getString(R.string.toast_error_cannot_send_from_this_device));
        intent.setFlags(DriveFile.MODE_READ_ONLY);
        this.mContext.startActivity(intent);
    }

    private boolean checkSharePreconditions(ContentContainer container) {
        if (DBG) {
            Log.d("Zap-UploadHandler", "Media to share: " + container.getPath() + " (" + container.getMimeType() + ")");
        }
        if (!FileUtils.isVideo(container.getMimeType()) || ((long) container.getSize()) <= ((Long) Z.MAX_UPLOAD_VIDEO_SIZE.read()).longValue()) {
            return true;
        }
        if (Log.isLoggable("Zap-UploadHandler", 6)) {
            Log.e("Zap-UploadHandler", "ERROR - File is too big");
        }
        container.actionResult.setError(Error.FILE_TOO_LARGE);
        GCSP.getCheckinManager().logZapEvent(container);
        this.mUi.showDialogMessage(this.mContext.getString(R.string.dialog_title_generic), this.mContext.getString(R.string.dialog_msg_too_big_to_share), true, false);
        return false;
    }

    private synchronized int getNumUploadsSince(long backInTime) {
        int count;
        long since = System.currentTimeMillis() - backInTime;
        count = 0;
        for (Entry<Long, Long> e : sLastItemsShared.entrySet()) {
            if (((Long) e.getValue()).longValue() > since) {
                count++;
            }
        }
        return count;
    }

    private void asyncShare(final ContentContainer container) {
        new Thread() {
            public void run() {
                if (UploadHandler.this.sharePrerequisite(container)) {
                    GCSP.getContentDispatcher().shareContent(container, UploadHandler.this.mUi.getContentShareListener(), null);
                    ZapTracker.getInstance().put(container);
                    new HistoryHandler().addUpload(container);
                    if (ZapConfig.supportsChromecast() && CastManager.getInstance().isChromecastConnected().booleanValue()) {
                        try {
                            CastManager.getInstance().castContent(container, null);
                        } catch (IllegalStateException ex) {
                            Log.e("Zap-UploadHandler", "Exception while sharing to Chrome Cast" + ex);
                        }
                    }
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public boolean sharePrerequisite(ContentContainer container) {
        if (!((Boolean) Z.FEATURE_ENABLED.read()).booleanValue()) {
            if (DBG) {
                Log.d("Zap-UploadHandler", "Feature is disabled, ignore sharing");
            }
            return false;
        }
        ModalityHelper.getInstance().requestRegisterModality(false);
        Z.LAST_ACTION_TIME.write(Long.valueOf(System.currentTimeMillis()));
        try {
            if (!container.isGroupRelated()) {
                long currentTimeMillis = System.currentTimeMillis();
                LocationResult result = LocationHandler.getInstance().getLocationForCurrentModality(false);
                if (result == null) {
                    if (DBG) {
                        Log.d("Zap-UploadHandler", "Still busy with location obtaining in another task, sending discarded");
                    }
                    this.mUi.onLocationEvent(LocationEvent.STILL_VERIFYING_LOCATION);
                    return false;
                }
                container.putCheckinData(CheckinInterface.KEY_MODALITY, ModalityHelper.getInstance().getCurrentModality());
                Locality locality = null;
                if (result.location != null) {
                    locality = new Locality();
                    locality.latitude = result.location.getLatitude();
                    locality.longitude = result.location.getLongitude();
                    locality.provider = result.location.getProvider();
                    locality.accuracy = (double) result.location.getAccuracy();
                    locality.timestamp = result.getAge();
                    locality.distance = ModalityHelper.getInstance().getShareDistance();
                }
                if (!result.hasSatisfiedLocation()) {
                    if (Log.isLoggable("Zap-UploadHandler", 5)) {
                        Log.w("Zap-UploadHandler", "WARN - Could not acquire accurate enough location");
                    }
                    container.setLocality(locality);
                    container.actionResult.setError(Error.LOCATION_NOT_ACCURATE);
                    GCSP.getCheckinManager().logZapEvent(container);
                    if (!BlastLocationProvider.getInstance().areAllLocationSettingsEnabled()) {
                        Intent i = new Intent(ZapConstants.ACTION_GS_GALLERY_PHOTO_SHARED);
                        i.putExtra(ZapConstants.EXTRA_GS_GALLERY_PHOTO_SHARED, "file://" + container.getPath());
                        this.mUi.showFixLocationSettingsDialog(i);
                        State.sUserActionDialogLaunchedTime = System.currentTimeMillis();
                        if (DBG) {
                            Log.d("Zap-UploadHandler", "Showing dialog for user to enable setting");
                        }
                    } else {
                        if (Log.isLoggable("Zap-UploadHandler", 6)) {
                            Log.e("Zap-UploadHandler", "ERROR - Could not determine accurate location - stopping send action");
                        }
                        this.mUi.showDialogMessage(this.mContext.getString(R.string.dialog_title_location_not_determined), this.mContext.getString(R.string.dialog_msg_no_accurate_location), true);
                    }
                    return false;
                }
                GeoLocationInfo geoLocation = BlastLocationProvider.getLocationInfo(locality.latitude, locality.longitude, this.mContext);
                if (geoLocation != null) {
                    locality.city = geoLocation.city;
                    locality.state = geoLocation.state;
                    locality.country = geoLocation.country;
                    if (DBG) {
                        Log.d("Zap-UploadHandler", geoLocation.toString());
                    }
                } else if (DBG) {
                    Log.d("Zap-UploadHandler", "Could not acquire GeoLocation info for this upload");
                }
                container.setLocality(locality);
            }
            return true;
        } catch (Exception e) {
            if (Log.isLoggable("Zap-UploadHandler", 6)) {
                Log.e("Zap-UploadHandler", "ERROR - Failed to upload metadata: " + e);
            }
            this.mUi.showErrorSendingMediaNotification(container);
            this.mUi.showErrorSendingMediaToast(container.getMimeType());
            container.actionResult.setError(Error.NO_METADATA);
            GCSP.getCheckinManager().logZapEvent(container);
            return false;
        }
    }
}
