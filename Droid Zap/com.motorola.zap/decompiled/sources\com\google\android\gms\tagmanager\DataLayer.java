package com.google.android.gms.tagmanager;

import com.google.android.gms.location.LocationRequest;
import com.motorola.zap.ZapConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataLayer {
    public static final String EVENT_KEY = "event";
    public static final Object OBJECT_NOT_PRESENT = new Object();
    static final String[] Ur = "gtm.lifetime".toString().split("\\.");
    private static final Pattern Us = Pattern.compile("(\\d+)\\s*([smhd]?)");
    private final ConcurrentHashMap<b, Integer> Ut;
    private final Map<String, Object> Uu;
    private final ReentrantLock Uv;
    private final LinkedList<Map<String, Object>> Uw;
    private final c Ux;
    /* access modifiers changed from: private */
    public final CountDownLatch Uy;

    static final class a {
        public final String UA;
        public final Object UB;

        a(String str, Object obj) {
            this.UA = str;
            this.UB = obj;
        }

        public boolean equals(Object o) {
            if (!(o instanceof a)) {
                return false;
            }
            a aVar = (a) o;
            return this.UA.equals(aVar.UA) && this.UB.equals(aVar.UB);
        }

        public int hashCode() {
            return Arrays.hashCode(new Integer[]{Integer.valueOf(this.UA.hashCode()), Integer.valueOf(this.UB.hashCode())});
        }

        public String toString() {
            return "Key: " + this.UA + " value: " + this.UB.toString();
        }
    }

    interface b {
        void v(Map<String, Object> map);
    }

    interface c {

        public interface a {
            void b(List<a> list);
        }

        void a(a aVar);

        void a(List<a> list, long j);

        void bi(String str);
    }

    DataLayer() {
        this(new c() {
            public void a(a aVar) {
                aVar.b(new ArrayList());
            }

            public void a(List<a> list, long j) {
            }

            public void bi(String str) {
            }
        });
    }

    DataLayer(c persistentStore) {
        this.Ux = persistentStore;
        this.Ut = new ConcurrentHashMap<>();
        this.Uu = new HashMap();
        this.Uv = new ReentrantLock();
        this.Uw = new LinkedList<>();
        this.Uy = new CountDownLatch(1);
        iP();
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.Map<java.lang.String, java.lang.Object>, code=java.lang.Object, for r7v0, types: [java.util.Map<java.lang.String, java.lang.Object>, java.lang.Object] */
    private Object A(Object obj) {
        String[] strArr = Ur;
        int length = strArr.length;
        int i = 0;
        Object obj2 = obj;
        while (i < length) {
            String str = strArr[i];
            if (!(obj2 instanceof Map)) {
                return null;
            }
            i++;
            obj2 = ((Map) obj2).get(str);
        }
        return obj2;
    }

    private List<a> B(Map<String, Object> map) {
        ArrayList arrayList = new ArrayList();
        a(map, ZapConstants.GOOGLE_ACCOUNT_NONE, arrayList);
        return arrayList;
    }

    private void C(Map<String, Object> map) {
        synchronized (this.Uu) {
            for (String str : map.keySet()) {
                a(b(str, map.get(str)), this.Uu);
            }
        }
        D(map);
    }

    private void D(Map<String, Object> map) {
        for (b v : this.Ut.keySet()) {
            v.v(map);
        }
    }

    private void a(Map<String, Object> map, String str, Collection<a> collection) {
        for (Entry entry : map.entrySet()) {
            String str2 = str + (str.length() == 0 ? ZapConstants.GOOGLE_ACCOUNT_NONE : ".") + ((String) entry.getKey());
            if (entry.getValue() instanceof Map) {
                a((Map) entry.getValue(), str2, collection);
            } else if (!str2.equals("gtm.lifetime")) {
                collection.add(new a(str2, entry.getValue()));
            }
        }
    }

    static Long bh(String str) {
        long j;
        Matcher matcher = Us.matcher(str);
        if (!matcher.matches()) {
            bh.u("unknown _lifetime: " + str);
            return null;
        }
        try {
            j = Long.parseLong(matcher.group(1));
        } catch (NumberFormatException e) {
            bh.w("illegal number in _lifetime value: " + str);
            j = 0;
        }
        if (j <= 0) {
            bh.u("non-positive _lifetime: " + str);
            return null;
        }
        String group = matcher.group(2);
        if (group.length() == 0) {
            return Long.valueOf(j);
        }
        switch (group.charAt(0)) {
            case 'd':
                return Long.valueOf(j * 1000 * 60 * 60 * 24);
            case LocationRequest.PRIORITY_LOW_POWER /*104*/:
                return Long.valueOf(j * 1000 * 60 * 60);
            case 'm':
                return Long.valueOf(j * 1000 * 60);
            case 's':
                return Long.valueOf(j * 1000);
            default:
                bh.w("unknown units in _lifetime: " + str);
                return null;
        }
    }

    private void iP() {
        this.Ux.a(new a() {
            public void b(List<a> list) {
                for (a aVar : list) {
                    DataLayer.this.x(DataLayer.this.b(aVar.UA, aVar.UB));
                }
                DataLayer.this.Uy.countDown();
            }
        });
    }

    private void iQ() {
        int i = 0;
        while (true) {
            int i2 = i;
            Map map = (Map) this.Uw.poll();
            if (map != null) {
                C(map);
                i = i2 + 1;
                if (i > 500) {
                    this.Uw.clear();
                    throw new RuntimeException("Seems like an infinite loop of pushing to the data layer");
                }
            } else {
                return;
            }
        }
    }

    public static List<Object> listOf(Object... objects) {
        ArrayList arrayList = new ArrayList();
        for (Object add : objects) {
            arrayList.add(add);
        }
        return arrayList;
    }

    public static Map<String, Object> mapOf(Object... objects) {
        if (objects.length % 2 != 0) {
            throw new IllegalArgumentException("expected even number of key-value pairs");
        }
        HashMap hashMap = new HashMap();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= objects.length) {
                return hashMap;
            }
            if (!(objects[i2] instanceof String)) {
                throw new IllegalArgumentException("key is not a string: " + objects[i2]);
            }
            hashMap.put(objects[i2], objects[i2 + 1]);
            i = i2 + 2;
        }
    }

    /* access modifiers changed from: private */
    public void x(Map<String, Object> map) {
        this.Uv.lock();
        try {
            this.Uw.offer(map);
            if (this.Uv.getHoldCount() == 1) {
                iQ();
            }
            y(map);
        } finally {
            this.Uv.unlock();
        }
    }

    private void y(Map<String, Object> map) {
        Long z = z(map);
        if (z != null) {
            List B = B(map);
            B.remove("gtm.lifetime");
            this.Ux.a(B, z.longValue());
        }
    }

    private Long z(Map<String, Object> map) {
        Object A = A(map);
        if (A == null) {
            return null;
        }
        return bh(A.toString());
    }

    /* access modifiers changed from: 0000 */
    public void a(b bVar) {
        this.Ut.put(bVar, Integer.valueOf(0));
    }

    /* access modifiers changed from: 0000 */
    public void a(List<Object> list, List<Object> list2) {
        while (list2.size() < list.size()) {
            list2.add(null);
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < list.size()) {
                Object obj = list.get(i2);
                if (obj instanceof List) {
                    if (!(list2.get(i2) instanceof List)) {
                        list2.set(i2, new ArrayList());
                    }
                    a((List) obj, (List) list2.get(i2));
                } else if (obj instanceof Map) {
                    if (!(list2.get(i2) instanceof Map)) {
                        list2.set(i2, new HashMap());
                    }
                    a((Map) obj, (Map) list2.get(i2));
                } else if (obj != OBJECT_NOT_PRESENT) {
                    list2.set(i2, obj);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(Map<String, Object> map, Map<String, Object> map2) {
        for (String str : map.keySet()) {
            Object obj = map.get(str);
            if (obj instanceof List) {
                if (!(map2.get(str) instanceof List)) {
                    map2.put(str, new ArrayList());
                }
                a((List) obj, (List) map2.get(str));
            } else if (obj instanceof Map) {
                if (!(map2.get(str) instanceof Map)) {
                    map2.put(str, new HashMap());
                }
                a((Map) obj, (Map) map2.get(str));
            } else {
                map2.put(str, obj);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public Map<String, Object> b(String str, Object obj) {
        HashMap hashMap = new HashMap();
        String[] split = str.toString().split("\\.");
        int i = 0;
        HashMap hashMap2 = hashMap;
        while (i < split.length - 1) {
            HashMap hashMap3 = new HashMap();
            hashMap2.put(split[i], hashMap3);
            i++;
            hashMap2 = hashMap3;
        }
        hashMap2.put(split[split.length - 1], obj);
        return hashMap;
    }

    /* access modifiers changed from: 0000 */
    public void bg(String str) {
        push(str, null);
        this.Ux.bi(str);
    }

    public Object get(String key) {
        synchronized (this.Uu) {
            Object obj = this.Uu;
            String[] split = key.split("\\.");
            int length = split.length;
            Object obj2 = obj;
            int i = 0;
            while (i < length) {
                String str = split[i];
                if (!(obj2 instanceof Map)) {
                    return null;
                }
                Object obj3 = ((Map) obj2).get(str);
                if (obj3 == null) {
                    return null;
                }
                i++;
                obj2 = obj3;
            }
            return obj2;
        }
    }

    public void push(String key, Object value) {
        push(b(key, value));
    }

    public void push(Map<String, Object> update) {
        try {
            this.Uy.await();
        } catch (InterruptedException e) {
            bh.w("DataLayer.push: unexpected InterruptedException");
        }
        x(update);
    }

    public void pushEvent(String eventName, Map<String, Object> update) {
        HashMap hashMap = new HashMap(update);
        hashMap.put("event", eventName);
        push(hashMap);
    }
}
