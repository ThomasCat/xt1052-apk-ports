package com.motorola.gcsp.json;

public class TargetMeta implements InputSet {
    public Long expirationTime;
    public Long inactiveExpirationTime;
    public Integer memberCount;
    public String pin;
    public Integer shareCount;
    public Long targetInactiveTTL;
    public Double targetLat;
    public Double targetLng;
    public String targetName;
    public String targetOwner;
    public String targetStatus;
    public Long targetTTL;
    public String targetThumbnail;
}
