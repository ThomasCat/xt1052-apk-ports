package com.motorola.zap.zz.ui;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.util.Log;
import com.google.android.gms.drive.DriveFile;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.util.NetworkUtils;
import com.motorola.sharing.util.PackageUtils;
import com.motorola.zap.LogUtils;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.app.App;

public class ZzPoller {
    /* access modifiers changed from: private */
    public static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private static final long[] POLLING_TIMEOUT = {2000, ZapConstants.LOCATION_RETRY_MAX_TIME_SETTINGS_DISABLED, ZapConstants.LOCATION_RETRY_MAX_TIME_SETTINGS_DISABLED, ZapConstants.LOCATION_RETRY_MAX_TIME_SETTINGS_DISABLED, ZapConstants.LOCATION_RETRY_MAX_TIME_SETTINGS_DISABLED, 30000, 30000, 30000, 30000, 60000, 60000, 60000, 60000, 120000, 120000, 120000, 120000, ZapConstants.LOCATION_MAX_AGE_NO_MODALITY_SUPPORT, ZapConstants.LOCATION_MAX_AGE_NO_MODALITY_SUPPORT, ZapConstants.LOCATION_MAX_AGE_NO_MODALITY_SUPPORT, ZapConstants.LOCATION_MAX_AGE_NO_MODALITY_SUPPORT, 600000, 600000, 600000, 600000, 900000, 900000, 900000, 900000};
    private static ZzPoller sInstance = null;
    private AlarmManager mAlarmManager = null;
    private Context mContext = null;
    private PowerManager mPowerManager = null;
    private ScreenReceiver mScreenReceiver = null;
    private WakeLock mWakeLock = null;

    public static class PollingReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            boolean canceled = false;
            String action = intent.getAction();
            if (ZzPoller.DBG) {
                Log.d("Zap-ZzPoller", "Got " + action);
            }
            ZzPoller poller = ZzPoller.getInstance();
            if (!poller.isActive()) {
                if (ZzPoller.DBG) {
                    Log.d("Zap-ZzPoller", "Polling is not active");
                }
                poller.stop();
            } else if ("com.motorola.zap.ACTION_ZZ_POLLING_ALARM".equals(action)) {
                poller.handleAlarmTriggered();
            } else if (ZapConstants.ACTION_ZZ_DO_POLL.equals(action)) {
                if (getResultCode() == 0) {
                    canceled = true;
                }
                poller.handlePollingRequestReceived(canceled);
            } else if (ZapConstants.ACTION_ZZ_POLL_HINT.equals(action)) {
                poller.handleRescheduleRequested(intent.getBooleanExtra(ZapConstants.EXTRA_ZZ_POLL_NOW, false));
            } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
                poller.handleNetworkChanged();
            }
        }

        /* access modifiers changed from: private */
        public static void setEnabled(Context context, boolean z) {
            if (PackageUtils.setComponentEnabled(context, new ComponentName(context, PollingReceiver.class), z) && ZzPoller.DBG) {
                Log.d("Zap-ZzPoller", "Polling receiver " + (z ? "enabled" : "disabled"));
            }
        }

        /* access modifiers changed from: private */
        public static boolean isEnabled(Context context) {
            return PackageUtils.isComponentEnabled(context, new ComponentName(context, PollingReceiver.class));
        }
    }

    private enum SchedulerCommand {
        START,
        RESET,
        RESET_SKIP_STEP,
        NEXT,
        RESUME
    }

    private class ScreenReceiver extends BroadcastReceiver {
        private ScreenReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ZzPoller.DBG) {
                Log.d("Zap-ZzPoller", "Got " + action);
            }
            ZzPoller poller = ZzPoller.getInstance();
            if (!poller.isActive()) {
                if (ZzPoller.DBG) {
                    Log.d("Zap-ZzPoller", "Polling is not active");
                }
                poller.stop();
            } else if ("android.intent.action.SCREEN_ON".equals(action)) {
                poller.handleRescheduleRequested(true);
            }
        }
    }

    public static synchronized ZzPoller getInstance() {
        ZzPoller zzPoller;
        synchronized (ZzPoller.class) {
            if (sInstance == null) {
                sInstance = new ZzPoller(App.getAppContext());
            }
            zzPoller = sInstance;
        }
        return zzPoller;
    }

    private ZzPoller(Context context) {
        this.mContext = context;
    }

    public synchronized boolean isPollingSettingEnabled() {
        return ((Boolean) Z.ZZ_POLLING_ENABLED.read()).booleanValue();
    }

    public synchronized void setPollingSettingEnabled(boolean enabled) {
        Z.ZZ_POLLING_ENABLED.write(Boolean.valueOf(enabled));
        updateState();
    }

    private void updateState() {
        if (!GCSP.getGroupManager().isAnyGroupActive()) {
            stop();
        } else if (!isPollingSettingEnabled()) {
            stop();
        } else if (!isActive()) {
            start();
        }
    }

    private synchronized void start() {
        reset();
        enableReceivers();
        scheduleAlarm(SchedulerCommand.START);
    }

    /* access modifiers changed from: private */
    public synchronized void stop() {
        reset();
        disableReceivers();
        cancelAlarm();
        releaseWakeLock();
    }

    /* access modifiers changed from: private */
    public synchronized boolean isActive() {
        return pendingIntentExists();
    }

    private void reset() {
        setWaitingNetworkConnected(false);
        setCurrentTimeoutIndex(0);
    }

    private boolean pendingIntentExists() {
        return getPendingIntent(true) != null;
    }

    private PendingIntent getPendingIntent(boolean z) {
        Intent intent = new Intent("com.motorola.zap.ACTION_ZZ_POLLING_ALARM");
        intent.setClass(this.mContext, PollingReceiver.class);
        return PendingIntent.getBroadcast(this.mContext, 0, intent, z ? DriveFile.MODE_WRITE_ONLY : 134217728);
    }

    private AlarmManager getAlarmManager() {
        if (this.mAlarmManager == null) {
            this.mAlarmManager = (AlarmManager) this.mContext.getSystemService("alarm");
        }
        return this.mAlarmManager;
    }

    private void scheduleAlarm(SchedulerCommand command) {
        int index;
        if (!PollingReceiver.isEnabled(this.mContext)) {
            cancelAlarm();
            if (DBG) {
                Log.d("Zap-ZzPoller", "Polling receiver has been disabled, don't schedule polling alarm");
                return;
            }
            return;
        }
        switch (command) {
            case START:
            case RESET:
                index = setCurrentTimeoutIndex(0);
                break;
            case RESET_SKIP_STEP:
                index = setCurrentTimeoutIndex(1);
                break;
            case NEXT:
                index = incrementCurrentTimeoutIndex();
                break;
            case RESUME:
                index = 0;
                break;
            default:
                if (Log.isLoggable("Zap-ZzPoller", 6)) {
                    Log.e("Zap-ZzPoller", "Unknown scheduler command ignored");
                    return;
                }
                return;
        }
        long now = SystemClock.elapsedRealtime();
        long timeout = getTimeoutByIndex(index);
        getAlarmManager().set(2, now + timeout, getPendingIntent(false));
        if (DBG) {
            Log.d("Zap-ZzPoller", "Next polling alarm scheduled in " + (timeout / 1000) + " secs");
        }
    }

    private void cancelAlarm() {
        PendingIntent pendingIntent = getPendingIntent(true);
        if (pendingIntent != null) {
            getAlarmManager().cancel(pendingIntent);
            pendingIntent.cancel();
            if (DBG) {
                Log.d("Zap-ZzPoller", "Polling alarm canceled");
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void handleAlarmTriggered() {
        if (isNetworkConnected()) {
            sendPollingRequest();
            if (isWaitingNetworkConnected()) {
                setWaitingNetworkConnected(false);
            }
        } else {
            setWaitingNetworkConnected(true);
            if (DBG) {
                Log.d("Zap-ZzPoller", "No network connection, postpone");
            }
        }
        if (this.mScreenReceiver == null) {
            registerScreenReceiver();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void handleNetworkChanged() {
        if (isWaitingNetworkConnected() && isNetworkConnected()) {
            scheduleAlarm(SchedulerCommand.RESUME);
            setWaitingNetworkConnected(false);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void handlePollingRequestReceived(boolean canceled) {
        releaseWakeLock();
        if (canceled) {
            stop();
        } else {
            scheduleAlarm(SchedulerCommand.NEXT);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void handleRescheduleRequested(boolean urgent) {
        if (canResetPollingNow()) {
            scheduleAlarm(urgent ? SchedulerCommand.RESET : SchedulerCommand.RESET_SKIP_STEP);
        }
    }

    private boolean canResetPollingNow() {
        return getCurrentTimeoutIndex() >= 2;
    }

    private void sendPollingRequest() {
        acquireWakeLock();
        this.mContext.sendOrderedBroadcast(new Intent(ZapConstants.ACTION_ZZ_DO_POLL), "com.motorola.blast.permission.TRIGGER_BLAST_ACTION", new PollingReceiver(), null, -1, null, null);
    }

    private PowerManager getPowerManager() {
        if (this.mPowerManager == null) {
            this.mPowerManager = (PowerManager) this.mContext.getSystemService("power");
        }
        return this.mPowerManager;
    }

    private void acquireWakeLock() {
        if (this.mWakeLock == null) {
            this.mWakeLock = getPowerManager().newWakeLock(1, "Zap-ZzPoller");
            this.mWakeLock.setReferenceCounted(false);
        }
        this.mWakeLock.acquire(1000);
    }

    private void releaseWakeLock() {
        if (this.mWakeLock != null && this.mWakeLock.isHeld()) {
            this.mWakeLock.release();
        }
    }

    private boolean isNetworkConnected() {
        if (!NetworkUtils.hasDataConnection(this.mContext)) {
            return false;
        }
        if (ZapUtils.isWifiOnlyEnabled()) {
            return NetworkUtils.isWifiConnected(this.mContext);
        }
        return true;
    }

    private void setWaitingNetworkConnected(boolean waiting) {
        Z.POLLING_WAITING_NETWORK_CONNECTED.write(Boolean.valueOf(waiting));
    }

    private boolean isWaitingNetworkConnected() {
        return ((Boolean) Z.POLLING_WAITING_NETWORK_CONNECTED.read()).booleanValue();
    }

    private int capIndex(int index) {
        if (index < 0) {
            return 0;
        }
        if (index >= POLLING_TIMEOUT.length) {
            return POLLING_TIMEOUT.length - 1;
        }
        return index;
    }

    private int setCurrentTimeoutIndex(int index) {
        int index2 = capIndex(index);
        Z.POLLING_CURRENT_TIMEOUT_INDEX.write(Integer.valueOf(index2));
        return index2;
    }

    private int getCurrentTimeoutIndex() {
        return ((Integer) Z.POLLING_CURRENT_TIMEOUT_INDEX.read()).intValue();
    }

    private int incrementCurrentTimeoutIndex() {
        int index;
        int index2 = getCurrentTimeoutIndex();
        if (!ZapUtils.isZapAppOnTop() || !getPowerManager().isScreenOn()) {
            index = index2 + 4;
        } else {
            index = index2 + 1;
        }
        return setCurrentTimeoutIndex(index);
    }

    private long getTimeoutByIndex(int index) {
        return POLLING_TIMEOUT[capIndex(index)];
    }

    private void enableReceivers() {
        PollingReceiver.setEnabled(this.mContext, true);
        registerScreenReceiver();
    }

    private void disableReceivers() {
        PollingReceiver.setEnabled(this.mContext, false);
        unregisterScreenReceiver();
    }

    private void registerScreenReceiver() {
        if (this.mScreenReceiver == null) {
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.intent.action.SCREEN_ON");
            filter.addAction("android.intent.action.SCREEN_OFF");
            this.mScreenReceiver = new ScreenReceiver();
            this.mContext.registerReceiver(this.mScreenReceiver, filter, "com.motorola.blast.permission.TRIGGER_BLAST_ACTION", null);
            if (DBG) {
                Log.d("Zap-ZzPoller", "Screen receiver registered");
            }
        }
    }

    private void unregisterScreenReceiver() {
        if (this.mScreenReceiver != null) {
            this.mContext.unregisterReceiver(this.mScreenReceiver);
            this.mScreenReceiver = null;
            if (DBG) {
                Log.d("Zap-ZzPoller", "Screen receiver unregistered");
            }
        }
    }
}
