package com.motorola.zap.screenshot;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.motorola.zap.LogUtils;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConfig;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.app.ActionReceiver;

public class ScreenshotReceiver extends BroadcastReceiver {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;

    private static boolean isAllowedToReceive(Context context) {
        return ZapUtils.isValidPermission(context, "com.motorola.screenshot.permission.SCREENSHOT_TAKEN") && ZapConfig.supportsZaptoscreen();
    }

    public void onReceive(Context context, Intent intent) {
        boolean z;
        if (isAllowedToReceive(context)) {
            String stringExtra = intent.getStringExtra("com.motorola.screenshot.imagepath");
            Bundle bundleExtra = intent.getBundleExtra("com.motorola.screenshot.reqbundle");
            if (!TextUtils.isEmpty(stringExtra)) {
                String string = bundleExtra.getString(ZapConstants.EXTRA_SCREENSHOT_REQ_ORIGIN);
                if (TextUtils.equals(string, ZapConstants.SCREENSHOT_VOICE_ORIGIN)) {
                    Z.ZAP_APP_USAGE_COUNT.write(Integer.valueOf(4));
                    z = true;
                } else if (!TextUtils.equals(string, ZapConstants.SCREENSHOT_KEYPRESS_ORIGIN) || !((Boolean) Z.SCREENSHOT_ACCESS.read()).booleanValue()) {
                    z = false;
                } else {
                    z = true;
                }
                if (z) {
                    Intent intent2 = new Intent(ZapConstants.ACTION_GS_GALLERY_PHOTO_SHARED);
                    intent2.setClass(context.getApplicationContext(), ActionReceiver.class);
                    intent2.putExtra(ZapConstants.EXTRA_GS_GALLERY_PHOTO_SHARED, "file://" + stringExtra);
                    intent2.putExtra(ZapConstants.EXTRA_EVENT_ORIGIN_ZAP_APP, false);
                    intent2.putExtra(ZapConstants.EXTRA_FROM_SHARE_INTENT, true);
                    intent2.putExtra(ZapConstants.EXTRA_SCREENSHOT_REQ_ORIGIN, string);
                    context.sendBroadcast(intent2);
                }
            }
        }
    }
}
