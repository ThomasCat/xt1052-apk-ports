package com.motorola.zap.ui;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.Notification.BigPictureStyle;
import android.app.Notification.BigTextStyle;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.drive.DriveFile;
import com.motorola.sharing.Abuse.AbuseAction;
import com.motorola.sharing.CastManager;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentContainer.TransferStatus;
import com.motorola.sharing.ContentDiscoveryListener;
import com.motorola.sharing.ContentDiscoveryListener.ContentDiscoveryEvent;
import com.motorola.sharing.ContentDownloadListener;
import com.motorola.sharing.ContentDownloadListener.ContentDownloadEvent;
import com.motorola.sharing.ContentShareListener;
import com.motorola.sharing.ContentShareListener.ContentCancelShareEvent;
import com.motorola.sharing.ContentShareListener.ContentShareEvent;
import com.motorola.sharing.ContentShareListener.ContentStopShareEvent;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener;
import com.motorola.sharing.GroupListener.GroupEvent;
import com.motorola.sharing.GroupManager.GroupInfo;
import com.motorola.sharing.ReportAbuseListener;
import com.motorola.sharing.ReportAbuseListener.ReportAbuseEvent;
import com.motorola.sharing.instrumentation.ActionResult.Error;
import com.motorola.sharing.util.FileUtils;
import com.motorola.sharing.util.Uid;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConfig;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.app.ActionReceiver;
import com.motorola.zap.app.App;
import com.motorola.zap.app.InAppNotifCenter;
import com.motorola.zap.app.TransferState;
import com.motorola.zap.app.ZapTracker;
import com.motorola.zap.app.ZzCameraWatcher;
import com.motorola.zap.app.ZzInviteStore;
import com.motorola.zap.dialogs.DialogMessageActivity;
import com.motorola.zap.dialogs.DialogProgressActivity;
import com.motorola.zap.dialogs.DialogUserActionRequired;
import com.motorola.zap.dialogs.DialogUserActionRequired.ActionType;
import com.motorola.zap.provider.HistoryHandler;
import com.motorola.zap.zz.ui.ZzCreateUiHandler;
import com.motorola.zap.zz.ui.ZzExpiredUiHandler;
import com.motorola.zap.zz.ui.ZzHandleInviteUiHandler;
import com.motorola.zap.zz.ui.ZzJoinUiHandler;
import com.motorola.zap.zz.ui.ZzLeaveUiHandler;
import com.motorola.zap.zz.ui.ZzPoller;
import com.motorola.zap.zz.ui.ZzShareInviteUiHandler;
import com.motorola.zap.zz.ui.ZzSyncStateUiHandler;
import com.motorola.zap.zz.ui.ZzUpdateUiHandler;
import java.util.Hashtable;
import java.util.Map;

public class ZapUi implements GroupListener {
    public static final int ZAP_CONTENT_NOTIFICATION_ID = 0;
    public static final String ZAP_CONTENT_TAG = "zap";
    private static ZapUi sInstance;
    /* access modifiers changed from: private */
    public final Context mContext;
    private final Handler mHandler;
    /* access modifiers changed from: private */
    public final HistoryHandler mHistoryHandler = new HistoryHandler();
    private Map<String, Integer> mNotifIdMap = null;
    private Runnable mShowProgressDialogTask = null;
    private boolean mShowingProgressDialog = false;
    private Toast mToast = null;

    public enum AuthenticationEvent {
        AUTH_TAKING_TOO_LONG
    }

    private class DiscoveryTracker implements ContentDiscoveryListener {
        private boolean mIsFirstContent;
        private boolean mIsFirstEvent;

        private DiscoveryTracker() {
            this.mIsFirstEvent = true;
            this.mIsFirstContent = true;
        }

        public void onContentDiscoveryEvent(ContentDiscoveryEvent event) {
            if (this.mIsFirstEvent) {
                this.mIsFirstEvent = false;
                if (!ZapUtils.isAutoProcessing(event.container)) {
                    ZapUi.playReceivingSound();
                }
            }
            ZapTracker.getInstance().onRefreshComplete();
            switch (event.status) {
                case INFO:
                    handleInfo(event);
                    return;
                case SUCCESS:
                    handleSuccess(event);
                    return;
                case ERROR:
                    handleError(event);
                    return;
                default:
                    return;
            }
        }

        private void handleInfo(ContentDiscoveryEvent event) {
            switch (event.substatus) {
                case CONTENT_FOUND:
                    ContentContainer container = event.container;
                    if (!ZapTracker.getInstance().isRecentlyDiscovered(container)) {
                        if (this.mIsFirstContent) {
                            handleFirstContent();
                        }
                        GCSP.getContentDispatcher().downloadContent(container, ZapUi.this.getContentDownloadListener());
                        ZapUi.this.mHistoryHandler.addDownload(container, container.getThumbnailBase64());
                        ZapTracker.getInstance().put(container);
                        Bundle bundle = new Bundle();
                        bundle.putString(ZapConstants.EXTRA_MEDIA_MIME_TYPE, container.getMimeType());
                        bundle.putLong(ZapConstants.EXTRA_REPORT_TIMESTAMP, System.currentTimeMillis());
                        InAppNotifCenter.getInAppNotifCenter().postEvent(1, bundle);
                        return;
                    }
                    return;
                case LOCKED_CONTENT_FOUND:
                    if (!ZapTracker.getInstance().getLockedContenAvailable()) {
                        if (this.mIsFirstContent) {
                            handleFirstContent();
                        }
                        ZapTracker.getInstance().setLockedContentAvailable(true);
                        ZapUi.this.showUniversalNotification();
                        return;
                    }
                    return;
                case GROUP_FOUND:
                    boolean inviteExists = ZzInviteStore.has(event.container.groupInfo.owner, event.container.groupInfo.id);
                    if (this.mIsFirstContent && !inviteExists) {
                        handleFirstContent();
                        return;
                    }
                    return;
                case ABUSE:
                    ZapUi.this.onAbuseEvent(event.abuseAction);
                    return;
                default:
                    return;
            }
        }

        private void handleFirstContent() {
            this.mIsFirstContent = false;
            if (ZapConfig.supportsFullFeature() && !ZapUtils.isZapAppOnTop()) {
                ZapUi.getInstance().startMainActivity();
            }
            ZapUi.this.showToastMessage(ZapUi.this.mContext.getString(R.string.toast_msg_found_something), ZapUi.getReceiveToastIconId(), true, true);
        }

        private void handleSuccess(ContentDiscoveryEvent event) {
            switch (event.substatus) {
                case ENDED:
                    if (this.mIsFirstContent) {
                        this.mIsFirstContent = false;
                        if (ZapUtils.isAutoProcessing(event.container)) {
                            return;
                        }
                        if (event.container.getPin() == null) {
                            ZapUi.this.showToastMessage(ZapUi.this.mContext.getString(R.string.toast_msg_no_content_found), ZapUi.getReceiveToastIconId(), true, true);
                            return;
                        }
                        ZapTracker.getInstance().onWrongPin();
                        ZapUi.this.showToastMessage(ZapUi.this.mContext.getString(R.string.toast_msg_wrong_code), ZapUi.getErrorToastIconId(), true, true);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }

        private void handleError(ContentDiscoveryEvent event) {
            switch (event.substatus) {
                case AUTH:
                    ZapUi.this.showDialogMessage(ZapUi.this.mContext.getString(R.string.dialog_title_generic), ZapUi.this.mContext.getString(R.string.dialog_msg_google_auth_error_server), true);
                    return;
                case GENERAL:
                    if (!ZapUtils.isAutoProcessing(event.container)) {
                        ZapUi.this.showServerErrorDialog();
                        return;
                    }
                    return;
                case NETWORK:
                    if (!ZapUtils.isAutoProcessing(event.container)) {
                        ZapUi.this.showNetworkConnectionErrorDialog();
                        return;
                    }
                    return;
                case INVALID_METADATA:
                    if (!ZapUtils.isAutoProcessing(event.container)) {
                        ZapUi.this.showMetadataErrorToast(event.container.getMimeType());
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public class DownloadTracker implements ContentDownloadListener {
        public DownloadTracker() {
        }

        public void onContentDownloadEvent(ContentDownloadEvent event) {
            switch (event.status) {
                case INFO:
                    handleInfo(event);
                    return;
                case SUCCESS:
                    handleSuccess(event);
                    return;
                case ERROR:
                    handleError(event);
                    return;
                default:
                    return;
            }
        }

        private void handleInfo(ContentDownloadEvent event) {
            switch (event.substatus) {
                case PROGRESS:
                    if (event.container.getTransferStatus().progress() > 0) {
                        ZapTracker.getInstance().onProgressUpdate(event.container);
                        Bundle bundle = new Bundle();
                        bundle.putLong(ZapConstants.EXTRA_TRANSACTION_UID, event.container.getUid());
                        InAppNotifCenter.getInAppNotifCenter().postEvent(8, bundle);
                        return;
                    }
                    return;
                case NOT_READY_FOR_TOO_LONG:
                    ZapUi.this.showReceivingNotReadyActionDialog(ZapUi.this.getCancelDownloadIntent(event.container));
                    return;
                case ESTIMATED_TRANSFER_TIME_TOO_LONG:
                    TransferStatus transferStatus = event.container.getTransferStatus();
                    if (transferStatus.estimatedRemainingTime > 0 && transferStatus.maxAllowedTime > 0) {
                        ZapUi.this.showReceiveEstimateTooLongActionDialog(ZapUi.this.getCancelDownloadIntent(event.container), (int) (transferStatus.estimatedRemainingTime / 60000), (int) (transferStatus.maxAllowedTime / 60000));
                        return;
                    }
                    return;
                case TRANSFER_TAKING_TOO_LONG:
                    TransferStatus transferStatus2 = event.container.getTransferStatus();
                    ZapUi.this.showReceiveTakingTooLongActionDialog(ZapUi.this.getCancelDownloadIntent(event.container), (int) ((System.currentTimeMillis() - transferStatus2.startTime) / 60000));
                    return;
                default:
                    return;
            }
        }

        private void handleSuccess(ContentDownloadEvent event) {
            switch (event.substatus) {
                case DOWNLOADED:
                    handleFinal(event, 0, 3);
                    ZapUi.this.dismissUserActionDialogs(ActionType.RECEIVE_TAKING_TOO_LONG);
                    ZapUi.this.dismissUserActionDialogs(ActionType.LONG_ESTIMATE_RECEIVE);
                    ZapUi.this.dismissUserActionDialogs(ActionType.NOT_READY_TAKING_LONG);
                    ZapUi.this.showMediaReceivedNotification(event.container);
                    if (ZapConfig.supportsChromecast() && CastManager.getInstance().isChromecastConnected().booleanValue()) {
                        try {
                            CastManager.getInstance().castContent(event.container, null);
                            return;
                        } catch (IllegalStateException ex) {
                            Log.e("Zap-ZapUi", "Exception while sharing to Chrome Cast" + ex);
                            return;
                        }
                    } else {
                        return;
                    }
                case CANCELED:
                    handleFinal(event, 2, 5);
                    ZapUi.this.showToastMessage(ZapUi.this.mContext.getString(R.string.toast_msg_receiving_canceled), ZapUi.getReceiveToastIconId(), true, true);
                    return;
                default:
                    return;
            }
        }

        private void handleError(ContentDownloadEvent event) {
            handleFinal(event, 1, 3);
            ZapUi.this.showErrorReceivingMediaNotification(event.container);
            switch (event.substatus) {
                case AVAILABILITY_TIMEOUT:
                    ZapUi.this.showCancelDownloadTakingTooLongNotReadyDialog();
                    return;
                case TRANSFER_TIMEOUT:
                    ZapUi.this.showCancelDownloadTakingTooLongNetworkDialog();
                    return;
                case STORAGE_FULL:
                    ZapUi.this.showDialogMessage(ZapUi.this.mContext.getString(R.string.dialog_title_generic), ZapUi.this.mContext.getString(R.string.dialog_low_memory_error), true);
                    return;
                case SENDER_CANCELED:
                    if (FileUtils.isVideo(event.container.getMimeType())) {
                        ZapUi.this.showDialogMessage(ZapUi.this.mContext.getString(R.string.dialog_title_sending_cancelled), ZapUi.this.mContext.getString(R.string.dialog_msg_share_cancelled_video), true);
                        return;
                    } else {
                        ZapUi.this.showDialogMessage(ZapUi.this.mContext.getString(R.string.dialog_title_sending_cancelled), ZapUi.this.mContext.getString(R.string.dialog_msg_share_cancelled_photo), true);
                        return;
                    }
                default:
                    return;
            }
        }

        private void handleFinal(ContentDownloadEvent event, int transferStatus, int notifEvent) {
            ContentContainer container = event.container;
            ZapUi.this.mHistoryHandler.setDownloadComplete(container, transferStatus);
            ZapTracker.getInstance().remove(container);
            Bundle bundle = new Bundle();
            bundle.putString(ZapConstants.EXTRA_MEDIA_MIME_TYPE, container.getMimeType());
            InAppNotifCenter.getInAppNotifCenter().postEvent(notifEvent, bundle);
        }
    }

    public enum LocationEvent {
        VERIFYING_LOCATION,
        STILL_VERIFYING_LOCATION
    }

    private static class NotifParams {
        public boolean autoCancel;
        public Bitmap bigImage;
        public CharSequence bigText;
        public String contentInfo;
        public int iconId;
        public Bitmap largeIcon;
        public Intent leftButtonAction;
        public int leftButtonIcon;
        public String leftButtonLabel;
        public boolean leftButtonLaunchActivity;
        public Intent notifAction;
        public boolean notifActionBroadcast;
        public int notifId;
        public boolean persistent;
        public int progressPct;
        public Intent rightButtonAction;
        public int rightButtonIcon;
        public String rightButtonLabel;
        public boolean rightButtonLaunchActivity;
        public boolean showProgress;
        public String text;
        public String ticker;
        public long timestamp;
        public String title;
        public boolean vibrate;

        private NotifParams() {
        }
    }

    private class ReportAbuseTracker implements ReportAbuseListener {
        private ReportAbuseTracker() {
        }

        public void onReportAbuseEvent(ReportAbuseEvent event) {
            switch (event.status) {
                case SUCCESS:
                    handleSuccess(event);
                    return;
                case ERROR:
                    handleError(event);
                    return;
                default:
                    return;
            }
        }

        private void handleSuccess(ReportAbuseEvent event) {
            switch (event.substatus) {
                case SENT:
                    ZapUi.this.mHistoryHandler.setAbuseReported(App.getAppContext(), event.container.getPath(), true);
                    ZapUi.this.showToastMessage(ZapUi.this.mContext.getString(R.string.toast_msg_report_sent), ZapUi.getZapToastIconId(), true, true);
                    return;
                default:
                    return;
            }
        }

        private void handleError(ReportAbuseEvent event) {
            switch (event.substatus) {
                case GENERAL:
                    ZapUi.this.showServerErrorDialog();
                    return;
                case AUTH:
                    ZapUi.this.showDialogMessage(ZapUi.this.mContext.getString(R.string.dialog_title_generic), ZapUi.this.mContext.getString(R.string.dialog_msg_google_auth_error_server), true);
                    return;
                case NETWORK:
                    ZapUi.this.showNetworkConnectionErrorDialog();
                    return;
                default:
                    return;
            }
        }
    }

    private class ShareTracker implements ContentShareListener {
        private ShareTracker() {
        }

        public void onContentShareEvent(ContentShareEvent event) {
            switch (event.status) {
                case INFO:
                    handleShareInfo(event);
                    return;
                case SUCCESS:
                    handleShareSuccess(event);
                    return;
                case ERROR:
                    handleShareError(event);
                    return;
                default:
                    return;
            }
        }

        private void handleShareInfo(ContentShareEvent event) {
            String format;
            String format2;
            switch (event.substatus) {
                case META_SHARED:
                    ZapUi.this.mHistoryHandler.setFromShareId(event.container.getUid(), event.container.getOrigin(), event.container.getShareId());
                    Bundle bundle = new Bundle();
                    bundle.putString(ZapConstants.EXTRA_MEDIA_MIME_TYPE, event.container.getMimeType());
                    bundle.putLong(ZapConstants.EXTRA_REPORT_TIMESTAMP, System.currentTimeMillis());
                    InAppNotifCenter.getInAppNotifCenter().postEvent(0, bundle);
                    ZapUi.playSendingSound();
                    if (!event.container.isGroupRelated()) {
                        if (ZapUtils.isZapAppOnTop()) {
                            ZapUi zapUi = ZapUi.this;
                            if (event.container.getPin() == null) {
                                format2 = ZapUi.this.mContext.getString(R.string.actionbar_msg_media_available_to_catch);
                            } else {
                                format2 = String.format(ZapUi.this.mContext.getString(R.string.actionbar_msg_media_available_to_catch_code), new Object[]{event.container.getPin()});
                            }
                            zapUi.showToastMessage(format2, ZapUi.getSendToastIconId(), true, false);
                        } else {
                            ZapUi zapUi2 = ZapUi.this;
                            if (event.container.getPin() == null) {
                                format = ZapUi.this.mContext.getString(R.string.toast_msg_media_available_to_catch);
                            } else {
                                format = String.format(ZapUi.this.mContext.getString(R.string.toast_msg_media_available_to_catch_code), new Object[]{event.container.getPin()});
                            }
                            zapUi2.showToastMessage(format, ZapUi.getSendToastIconId(), true, false);
                        }
                    }
                    ZapUi.this.showUniversalNotification();
                    return;
                case ABUSE:
                    ZapUi.this.onAbuseEvent(event.abuseAction);
                    return;
                case TRANSFER_TAKING_TOO_LONG:
                    TransferStatus transferStatus = event.container.getTransferStatus();
                    ZapUi.this.showSendTakingTooLongActionDialog(ZapUi.this.getCancelUploadIntent(event.container, true), (int) ((System.currentTimeMillis() - transferStatus.startTime) / 60000));
                    return;
                case ESTIMATED_TRANSFER_TIME_TOO_LONG:
                    TransferStatus transferStatus2 = event.container.getTransferStatus();
                    if (transferStatus2.estimatedRemainingTime > 0 && transferStatus2.maxAllowedTime > 0) {
                        ZapUi.this.showSendEstimateTooLongActionDialog(ZapUi.this.getCancelUploadIntent(event.container, true), (int) (transferStatus2.estimatedRemainingTime / 60000), (int) (transferStatus2.maxAllowedTime / 60000));
                        return;
                    }
                    return;
                case PROGRESS:
                    ZapUi.this.showUniversalNotification();
                    if (event.container.getTransferStatus().progress() > 0) {
                        ZapTracker.getInstance().onProgressUpdate(event.container);
                        Bundle bundle2 = new Bundle();
                        bundle2.putLong(ZapConstants.EXTRA_TRANSACTION_UID, event.container.getUid());
                        InAppNotifCenter.getInAppNotifCenter().postEvent(8, bundle2);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }

        private void handleShareSuccess(ContentShareEvent event) {
            switch (event.substatus) {
                case SHARED:
                    ZapUi.this.dismissUserActionDialogs(ActionType.SEND_TAKING_TOO_LONG);
                    ZapUi.this.dismissUserActionDialogs(ActionType.LONG_ESTIMATE_SEND);
                    ZapTracker.getInstance().remove(event.container);
                    ZapUi.this.mHistoryHandler.setUploadComplete(event.container, 0);
                    InAppNotifCenter.getInAppNotifCenter().postEvent(2, new Bundle());
                    ZapUi.this.showMediaSentNotification(event.container);
                    return;
                default:
                    return;
            }
        }

        private void handleShareError(ContentShareEvent event) {
            ZapTracker.getInstance().remove(event.container);
            ZapUi.this.mHistoryHandler.setUploadComplete(event.container, 1);
            switch (event.substatus) {
                case AUTH:
                    Bundle bundle = new Bundle();
                    bundle.putString(ZapConstants.EXTRA_MEDIA_MIME_TYPE, event.container.getMimeType());
                    InAppNotifCenter.getInAppNotifCenter().postEvent(4, bundle);
                    ZapUi.this.showDialogMessage(ZapUi.this.mContext.getString(R.string.dialog_title_generic), ZapUi.this.mContext.getString(R.string.dialog_msg_google_auth_error_server), true);
                    ZapUi.this.showErrorSendingMediaNotification(event.container);
                    return;
                case GENERAL:
                    ZapUi.this.showServerErrorDialog();
                    return;
                case NETWORK:
                    ZapUi.this.showNetworkConnectionErrorDialog();
                    return;
                case INVALID_METADATA:
                    ZapUi.this.showErrorSendingMediaNotification(event.container);
                    ZapUi.this.showErrorSendingMediaToast(event.container.getMimeType());
                    return;
                case TRANSFER_TIMEOUT:
                    ZapUi.this.showCancelUploadTakingTooLongDialog();
                    return;
                case TRANSFER_FAILED:
                    ZapUi.this.showErrorSendingMediaNotification(event.container);
                    ZapUi.this.showErrorSendingMediaToast(event.container.getMimeType());
                    return;
                default:
                    return;
            }
        }

        public void onContentStopShareEvent(ContentStopShareEvent event) {
            switch (event.status) {
                case SUCCESS:
                    handleStopShareSuccess(event);
                    return;
                case ERROR:
                    handleStopShareError(event);
                    return;
                default:
                    return;
            }
        }

        public void onContentCancelShareEvent(ContentCancelShareEvent event) {
            handleShareCanceled(event);
        }

        private void handleStopShareSuccess(ContentStopShareEvent event) {
            switch (event.substatus) {
                case NOT_SHARED:
                    if (ZapConfig.supportsFullFeature()) {
                        TransferState.getInstance().removeSent(ZapUi.this.mContext);
                    }
                    ZapUi.this.showToastMessage(ZapUi.this.mContext.getString(R.string.toast_msg_sharing_canceled), ZapUi.getSendToastIconId(), true, true);
                    return;
                default:
                    return;
            }
        }

        private void handleStopShareError(ContentStopShareEvent event) {
            switch (event.substatus) {
                case AUTH:
                    ZapUi.this.showDialogMessage(ZapUi.this.mContext.getString(R.string.dialog_title_generic), ZapUi.this.mContext.getString(R.string.dialog_msg_google_auth_error_server), true);
                    return;
                case GENERAL:
                    ZapUi.this.showServerErrorDialog();
                    return;
                case NETWORK:
                    ZapUi.this.showNetworkConnectionErrorDialog();
                    return;
                default:
                    return;
            }
        }

        private void handleShareCanceled(ContentCancelShareEvent event) {
            switch (event.substatus) {
            }
        }
    }

    public static synchronized ZapUi getInstance() {
        ZapUi zapUi;
        synchronized (ZapUi.class) {
            if (sInstance == null) {
                sInstance = new ZapUi(App.getAppContext());
            }
            zapUi = sInstance;
        }
        return zapUi;
    }

    private ZapUi(Context context) {
        this.mContext = context;
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mNotifIdMap = new Hashtable();
    }

    private NotificationManager getNotificationManager() {
        return (NotificationManager) this.mContext.getSystemService("notification");
    }

    public static int getZapToastIconId() {
        return R.drawable.ic_zap_toast_receive;
    }

    public static int getSendToastIconId() {
        return R.drawable.ic_zap_toast_send;
    }

    public static int getReceiveToastIconId() {
        return R.drawable.ic_zap_toast_receive;
    }

    public static int getZzToastIconId() {
        return R.drawable.ic_zapzone;
    }

    public static int getErrorToastIconId() {
        return 17301624;
    }

    public void showToastMessage(String message, int iconId, boolean durationLong, boolean vibrate) {
        showToastMessage(message, iconId, durationLong, vibrate, false);
    }

    public void showToastMessage(String message, int iconId, boolean durationLong, boolean vibrate, boolean showOnlyAsToast) {
        if (!showOnlyAsToast) {
            ZapTracker.getInstance().onZapEvent(message);
        }
        if (getProgressDialogFlag()) {
            dismissProgressDialog();
        }
        final boolean z = showOnlyAsToast;
        final String str = message;
        final int i = iconId;
        final boolean z2 = durationLong;
        final boolean z3 = vibrate;
        this.mHandler.post(new Runnable() {
            public void run() {
                if (z || ZapUi.this.isShowAndroidToast()) {
                    View toastView = ((LayoutInflater) ZapUi.this.mContext.getSystemService("layout_inflater")).inflate(R.layout.toast_layout, null);
                    ((TextView) toastView.findViewById(R.id.toast_text)).setText(str);
                    if (i != 0) {
                        ((ImageView) toastView.findViewById(R.id.toast_image)).setImageResource(i);
                    }
                    ZapUi.this.showToast(toastView, z2);
                }
                if (z3) {
                    ((Vibrator) ZapUi.this.mContext.getSystemService("vibrator")).vibrate(200);
                }
                if (str.equals(ZapUi.this.mContext.getString(R.string.toast_error_receiving_media))) {
                    ZapUi.this.mContext.sendBroadcast(new Intent(ZapConstants.ACTION_SWYPE_RECEIVE_FAILED), "com.motorola.blast.permission.TRIGGER_BLAST_ACTION");
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean isShowAndroidToast() {
        return !ZapUtils.isZapAppOnTop();
    }

    public void showDialogMessage(String title, String message, boolean vibrate) {
        showDialogMessage(title, message, vibrate, true);
    }

    public void showDialogMessage(final String title, final String message, boolean vibrate, boolean delay) {
        if (vibrate) {
            ((Vibrator) this.mContext.getSystemService("vibrator")).vibrate(400);
        }
        this.mHandler.postDelayed(new Runnable() {
            public void run() {
                ZapUi.this.cancelToast();
                if (ZapUi.this.getProgressDialogFlag()) {
                    ZapUi.this.dismissProgressDialog();
                }
                Intent intent = new Intent(ZapUi.this.mContext, DialogMessageActivity.class);
                intent.putExtra(ZapConstants.EXTRA_DIALOG_TITLE, title);
                intent.putExtra(ZapConstants.EXTRA_DIALOG_MESSAGE, message);
                intent.setFlags(DriveFile.MODE_READ_ONLY);
                ZapUi.this.mContext.startActivity(intent);
            }
        }, (long) (delay ? 300 : 0));
    }

    public void showProgressDialog(final String message) {
        finishCurrentDialogActivity();
        this.mShowProgressDialogTask = new Runnable() {
            public void run() {
                ZapUi.this.cancelToast();
                Intent intent = new Intent(ZapUi.this.mContext, DialogProgressActivity.class);
                intent.putExtra(ZapConstants.EXTRA_DIALOG_MESSAGE, message);
                intent.setFlags(DriveFile.MODE_READ_ONLY);
                ZapUi.this.mContext.startActivity(intent);
                ZapUi.this.setProgressDialogFlag(true);
            }
        };
        this.mHandler.postDelayed(this.mShowProgressDialogTask, 300);
    }

    public void dismissProgressDialog() {
        if (this.mShowProgressDialogTask != null) {
            this.mHandler.removeCallbacks(this.mShowProgressDialogTask);
            this.mShowProgressDialogTask = null;
        }
        finishCurrentDialogActivity();
        setProgressDialogFlag(false);
    }

    /* access modifiers changed from: private */
    public void showUniversalNotification() {
        if (!ZapUtils.isZapAppOnTop()) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    Builder contentText = new Builder(ZapUi.this.mContext).setLargeIcon(BitmapFactory.decodeResource(ZapUi.this.mContext.getResources(), R.drawable.ic_zap_lockedzap)).setSmallIcon(R.drawable.ic_zap_toast_receive).setContentTitle(ZapUi.this.mContext.getString(R.string.app_name)).setContentText(ZapUi.this.mContext.getString(R.string.universal_notification_progress));
                    Intent intent = new Intent(ZapUi.this.mContext, DroidBlastLauncher.class);
                    intent.setFlags(DriveFile.MODE_READ_ONLY);
                    contentText.setContentIntent(PendingIntent.getActivity(ZapUi.this.mContext.getApplicationContext(), 0, intent, 0));
                    ((NotificationManager) ZapUi.this.mContext.getSystemService("notification")).notify(0, contentText.build());
                }
            });
        }
    }

    public void showMediaSentNotification(ContentContainer trans) {
        removeZapUniversalNotification();
    }

    public void showMediaReceivedNotification(ContentContainer trans) {
        if (!ZapUtils.isAutoProcessing(trans)) {
            showToastMessage(this.mContext.getString(R.string.toast_msg_saved_to_gallery), getReceiveToastIconId(), true, false);
        }
        removeZapUniversalNotification();
        showUniversalNotification();
    }

    public void showErrorReceivingMediaNotification(ContentContainer trans) {
        removeZapUniversalNotification();
        showErrorNotification(this.mContext.getString(R.string.notif_error_media_transfer_ticker), this.mContext.getString(R.string.notif_error_media_transfer_title), getErrorText(trans.actionResult.getError()), 17301624);
    }

    public void showErrorSendingMediaNotification(ContentContainer trans) {
        removeZapUniversalNotification();
        showErrorNotification(this.mContext.getString(R.string.notif_error_media_transfer_ticker), this.mContext.getString(R.string.notif_error_media_transfer_title), getErrorText(trans.actionResult.getError()), 17301624);
    }

    private String getErrorText(Error error) {
        if (error == null) {
            error = Error.UNKNOWN;
        }
        switch (error) {
            case WIFI_ONLY_ALLOWED:
                return this.mContext.getString(R.string.notif_error_media_transfer_wifi_only_text);
            default:
                return this.mContext.getString(R.string.notif_error_media_transfer_general_text);
        }
    }

    private synchronized void showAbuseActionNotification(String title, String text) {
        final NotifParams p = new NotifParams();
        p.notifId = getNotifId(GCSP.getAuthenticator().getAccount());
        p.timestamp = System.currentTimeMillis();
        p.ticker = title;
        p.title = title;
        p.text = text;
        p.contentInfo = null;
        p.iconId = getZapToastIconId();
        p.largeIcon = null;
        p.bigImage = null;
        p.bigText = text;
        setSupportIntent(p);
        p.persistent = false;
        p.showProgress = false;
        p.progressPct = 0;
        p.leftButtonIcon = 0;
        p.leftButtonLabel = null;
        p.leftButtonAction = null;
        p.leftButtonLaunchActivity = false;
        p.rightButtonIcon = 0;
        p.rightButtonLabel = null;
        p.rightButtonAction = null;
        p.rightButtonLaunchActivity = false;
        p.vibrate = false;
        this.mHandler.post(new Runnable() {
            public void run() {
                ZapUi.this.showDetailedNotification(p);
            }
        });
    }

    private void setSupportIntent(NotifParams p) {
        if (ZapConfig.supportsFullFeature()) {
            p.notifAction = new Intent(ZapConstants.ACTION_LAUNCH_MOTOCARE);
            p.notifActionBroadcast = true;
            return;
        }
        String url = this.mContext.getResources().getString(R.string.motocare_url);
        Intent i = new Intent("android.intent.action.VIEW");
        i.setData(Uri.parse(url));
        p.notifAction = i;
        p.notifActionBroadcast = false;
    }

    /* access modifiers changed from: private */
    public void showMetadataErrorToast(String mimeType) {
        if (mimeType == null) {
            showToastMessage(this.mContext.getString(R.string.toast_error_receiving_media), 17301624, true, true);
        } else if (FileUtils.isVideo(mimeType)) {
            showToastMessage(this.mContext.getString(R.string.toast_error_receiving_video), 17301624, true, true);
        } else {
            showToastMessage(this.mContext.getString(R.string.toast_error_receiving_photo), 17301624, true, true);
        }
    }

    public void showErrorSendingMediaToast(String mimeType) {
        if (mimeType == null) {
            showToastMessage(this.mContext.getString(R.string.toast_error_sending_media), 17301624, true, true);
        } else if (FileUtils.isVideo(mimeType)) {
            showToastMessage(this.mContext.getString(R.string.toast_error_sending_video), 17301624, true, true);
        } else {
            showToastMessage(this.mContext.getString(R.string.toast_error_sending_photo), 17301624, true, true);
        }
    }

    public void showEnableLocationDialog(Intent originalIntent) {
        if (originalIntent != null) {
            originalIntent.putExtra(ZapConstants.EXTRA_RECOVER_USER_ACTION_INTENT, true);
        }
        showUserActionRequiredDialog(ActionType.ENABLE_LOCATION, originalIntent);
    }

    public void showConnectNetworkDialog(Intent originalIntent) {
        if (originalIntent != null) {
            originalIntent.putExtra(ZapConstants.EXTRA_RECOVER_USER_ACTION_INTENT, true);
        }
        showUserActionRequiredDialog(ActionType.CONNECT_NETWORK, originalIntent);
    }

    public void showWifiOnlyNotConnectedDialog(Intent originalIntent) {
        if (originalIntent != null) {
            originalIntent.putExtra(ZapConstants.EXTRA_RECOVER_USER_ACTION_INTENT, true);
        }
        showUserActionRequiredDialog(ActionType.CHANGE_WIFI_ONLY, originalIntent);
    }

    public void showFixLocationSettingsDialog(Intent originalIntent) {
        if (originalIntent != null) {
            originalIntent.putExtra(ZapConstants.EXTRA_RECOVER_USER_ACTION_INTENT, true);
        }
        showUserActionRequiredDialog(ActionType.FIX_LOCATION_SETTINGS, originalIntent);
    }

    public void showNetworkConnectionErrorDialog() {
        showDialogMessage(this.mContext.getString(R.string.dialog_title_no_connection), this.mContext.getString(R.string.dialog_msg_network_error), true);
    }

    public void showServerErrorDialog() {
        showDialogMessage(this.mContext.getString(R.string.dialog_title_server_error), this.mContext.getString(R.string.dialog_msg_server_error), true);
    }

    public void showSendEstimateTooLongActionDialog(Intent cancelShareIntent, int estimateTimeMin, int warningMaxTimeMin) {
        if (estimateTimeMin > warningMaxTimeMin) {
            estimateTimeMin = warningMaxTimeMin;
        }
        showCancelTransferActionDialog(ActionType.LONG_ESTIMATE_SEND, cancelShareIntent, estimateTimeMin);
    }

    public void showReceiveEstimateTooLongActionDialog(Intent cancelReceiveIntent, int estimateTimeMin, int warningMaxTimeMin) {
        if (estimateTimeMin > warningMaxTimeMin) {
            estimateTimeMin = warningMaxTimeMin;
        }
        showCancelTransferActionDialog(ActionType.LONG_ESTIMATE_RECEIVE, cancelReceiveIntent, estimateTimeMin);
    }

    public void showReceivingNotReadyActionDialog(Intent cancelReceiveIntent) {
        showCancelTransferActionDialog(ActionType.NOT_READY_TAKING_LONG, cancelReceiveIntent, 0);
    }

    public void showSendTakingTooLongActionDialog(Intent cancelSendIntent, int elapsedTimeMin) {
        showCancelTransferActionDialog(ActionType.SEND_TAKING_TOO_LONG, cancelSendIntent, elapsedTimeMin);
    }

    public void showReceiveTakingTooLongActionDialog(Intent cancelReceiveIntent, int elapsedTimeMin) {
        showCancelTransferActionDialog(ActionType.RECEIVE_TAKING_TOO_LONG, cancelReceiveIntent, elapsedTimeMin);
    }

    public void showCancelUploadTakingTooLongDialog() {
        showDialogMessage(this.mContext.getString(R.string.dialog_title_slow_connection), this.mContext.getString(R.string.dialog_msg_slow_connection_cancel_send), true);
    }

    public void showCancelDownloadTakingTooLongNetworkDialog() {
        showDialogMessage(this.mContext.getString(R.string.dialog_title_slow_connection), this.mContext.getString(R.string.dialog_msg_slow_connection_cancel_receive), true);
    }

    public void showCancelDownloadTakingTooLongNotReadyDialog() {
        showDialogMessage(this.mContext.getString(R.string.dialog_title_sender_not_ready), this.mContext.getString(R.string.dialog_msg_not_ready_cancel_receive), true);
    }

    public void showDataWarningDialog() {
        Intent intent = new Intent(this.mContext, DialogMessageActivity.class);
        intent.putExtra(ZapConstants.EXTRA_DIALOG_MESSAGE, this.mContext.getString(R.string.zz_data_warning));
        intent.setFlags(DriveFile.MODE_READ_ONLY);
        this.mContext.startActivity(intent);
    }

    public static void playSendingSound() {
        playRingtone(R.raw.send, 500);
    }

    public static void playReceivingSound() {
        playRingtone(R.raw.receive, 200);
    }

    public static void vibrate(final long duration) {
        new Thread(new Runnable() {
            public void run() {
                ((Vibrator) App.getAppContext().getSystemService("vibrator")).vibrate(duration);
            }
        }).start();
    }

    private synchronized int getNotifId(String key) {
        int newNotifId;
        if (this.mNotifIdMap.containsKey(key)) {
            newNotifId = ((Integer) this.mNotifIdMap.get(key)).intValue();
        } else {
            newNotifId = getNextNotificationId();
            this.mNotifIdMap.put(key, Integer.valueOf(newNotifId));
        }
        return newNotifId;
    }

    public synchronized int getNextNotificationId() {
        return Uid.getNext();
    }

    private void showErrorNotification(String ticker, String title, String text, int iconId) {
        final NotifParams p = new NotifParams();
        p.notifId = getNextNotificationId();
        p.timestamp = System.currentTimeMillis();
        p.autoCancel = true;
        p.ticker = ticker;
        p.title = title;
        p.text = text;
        p.iconId = iconId;
        p.persistent = false;
        p.showProgress = false;
        p.notifActionBroadcast = true;
        this.mHandler.post(new Runnable() {
            public void run() {
                ZapUi.this.showDetailedNotification(p);
            }
        });
    }

    public void showUpgradeNotification(int i) {
        final NotifParams notifParams = new NotifParams();
        notifParams.notifId = getNextNotificationId();
        notifParams.timestamp = System.currentTimeMillis();
        Intent intent = new Intent(this.mContext, ActionReceiver.class);
        intent.setAction(ZapConstants.ACTION_ZAP_UPGRADE_FEATURE_NOTIFICATION);
        switch (i) {
            case 0:
                notifParams.autoCancel = true;
                notifParams.ticker = this.mContext.getString(R.string.notif_info_upgrade_profile_ticker);
                notifParams.title = this.mContext.getString(R.string.notif_info_upgrade_profile_title);
                notifParams.text = this.mContext.getString(R.string.notif_info_upgrade_profile_msg);
                notifParams.iconId = getZapToastIconId();
                notifParams.persistent = false;
                notifParams.showProgress = false;
                notifParams.notifActionBroadcast = true;
                break;
            case 1:
                notifParams.autoCancel = true;
                notifParams.ticker = this.mContext.getString(R.string.notif_info_upgrade_voice_ticker);
                notifParams.title = this.mContext.getString(R.string.notif_info_upgrade_voice_title);
                notifParams.text = this.mContext.getString(R.string.notif_info_upgrade_voice_msg);
                notifParams.iconId = getZapToastIconId();
                notifParams.persistent = false;
                notifParams.showProgress = false;
                notifParams.notifActionBroadcast = true;
                break;
        }
        intent.putExtra(ZapConstants.EXTRA_SHOW_UPGRADE_FEATURE_TYPE, i);
        notifParams.notifAction = intent;
        this.mHandler.post(new Runnable() {
            public void run() {
                ZapUi.this.showDetailedNotification(notifParams);
            }
        });
    }

    /* access modifiers changed from: private */
    public void showDetailedNotification(NotifParams p) {
        PendingIntent notifPendingIntent;
        if (p.notifActionBroadcast) {
            notifPendingIntent = PendingIntent.getBroadcast(this.mContext, p.notifId, p.notifAction == null ? new Intent() : p.notifAction, 134217728);
        } else {
            notifPendingIntent = PendingIntent.getActivity(this.mContext, p.notifId, p.notifAction == null ? new Intent() : p.notifAction, 134217728);
        }
        PendingIntent leftButtonPendingIntent = null;
        PendingIntent rightButtonPendingIntent = null;
        if (!(p.leftButtonLabel == null || p.leftButtonAction == null)) {
            leftButtonPendingIntent = p.leftButtonLaunchActivity ? PendingIntent.getActivity(this.mContext, p.notifId, p.leftButtonAction, 134217728) : PendingIntent.getBroadcast(this.mContext, p.notifId, p.leftButtonAction, 134217728);
        }
        if (!(p.rightButtonLabel == null || p.rightButtonAction == null)) {
            rightButtonPendingIntent = p.rightButtonLaunchActivity ? PendingIntent.getActivity(this.mContext, p.notifId, p.rightButtonAction, 134217728) : PendingIntent.getBroadcast(this.mContext, p.notifId, p.rightButtonAction, 134217728);
        }
        if (VERSION.SDK_INT >= 16) {
            showDetailedNotificationJB(p, notifPendingIntent, leftButtonPendingIntent, rightButtonPendingIntent);
        } else {
            showDetailedNotificationICS(p, notifPendingIntent);
        }
    }

    @SuppressLint({"NewApi"})
    private void showDetailedNotificationJB(NotifParams p, PendingIntent notifPendingIntent, PendingIntent leftButtonPendingIntent, PendingIntent rightButtonPendingIntent) {
        Notification notif;
        Builder notifBuilder = new Builder(this.mContext).setSmallIcon(p.iconId).setTicker(p.ticker).setContentTitle(p.title).setContentText(p.text).setContentIntent(notifPendingIntent).setOngoing(p.persistent).setAutoCancel(p.autoCancel);
        if (leftButtonPendingIntent != null) {
            notifBuilder.addAction(p.leftButtonIcon, p.leftButtonLabel, leftButtonPendingIntent);
        }
        if (rightButtonPendingIntent != null) {
            notifBuilder.addAction(p.rightButtonIcon, p.rightButtonLabel, rightButtonPendingIntent);
        }
        if (p.largeIcon != null) {
            notifBuilder.setLargeIcon(p.largeIcon);
        }
        if (p.showProgress) {
            if (p.progressPct < 0) {
                notifBuilder.setProgress(0, 0, true);
            } else {
                notifBuilder.setProgress(100, p.progressPct, false);
            }
        }
        if (p.contentInfo != null) {
            notifBuilder.setContentInfo(p.contentInfo);
        }
        NotificationManager notifManager = getNotificationManager();
        if (p.bigImage != null) {
            notif = new BigPictureStyle(notifBuilder).bigPicture(p.bigImage).build();
        } else if (p.bigText != null) {
            notif = new BigTextStyle(notifBuilder).bigText(p.bigText).build();
        } else {
            notif = notifBuilder.build();
        }
        if (p.timestamp > 0) {
            notif.when = p.timestamp;
        }
        notify(notifManager, p, notif);
        if (p.vibrate) {
            ((Vibrator) this.mContext.getSystemService("vibrator")).vibrate(300);
        }
    }

    private void notify(NotificationManager notifManager, NotifParams p, Notification notif) {
        notifManager.notify(p.notifId, notif);
    }

    private void showDetailedNotificationICS(NotifParams p, PendingIntent notifPendingIntent) {
        NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(this.mContext).setSmallIcon(p.iconId).setTicker(p.ticker).setContentTitle(p.title).setContentText(p.text).setContentIntent(notifPendingIntent).setOngoing(p.persistent).setAutoCancel(p.autoCancel);
        if (p.largeIcon != null) {
            notifBuilder.setLargeIcon(p.largeIcon);
        }
        if (p.showProgress) {
            if (p.progressPct < 0) {
                notifBuilder.setProgress(0, 0, true);
            } else {
                notifBuilder.setProgress(100, p.progressPct, false);
            }
        }
        if (p.contentInfo != null) {
            notifBuilder.setContentInfo(p.contentInfo);
        }
        NotificationManager notifManager = getNotificationManager();
        Notification notif = notifBuilder.build();
        if (p.timestamp > 0) {
            notif.when = p.timestamp;
        }
        notify(notifManager, p, notif);
        if (p.vibrate) {
            ((Vibrator) this.mContext.getSystemService("vibrator")).vibrate(300);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void showToast(View toastView, boolean durationLong) {
        int i = 0;
        synchronized (this) {
            cancelToast();
            this.mToast = new Toast(this.mContext);
            Toast toast = this.mToast;
            if (durationLong) {
                i = 1;
            }
            toast.setDuration(i);
            this.mToast.setGravity(17, 0, 0);
            this.mToast.setView(toastView);
            this.mToast.show();
        }
    }

    private void showUserActionRequiredDialog(final ActionType actionType, final Intent originalIntent) {
        ((Vibrator) this.mContext.getSystemService("vibrator")).vibrate(400);
        this.mHandler.postDelayed(new Runnable() {
            public void run() {
                ZapUi.this.cancelToast();
                if (ZapUi.this.getProgressDialogFlag()) {
                    ZapUi.this.dismissProgressDialog();
                }
                Intent intent = new Intent(ZapUi.this.mContext, DialogUserActionRequired.class);
                intent.putExtra(ZapConstants.EXTRA_DIALOG_USER_ACTION_TYPE, actionType);
                intent.putExtra(ZapConstants.EXTRA_ORIGINAL_INTENT, originalIntent);
                intent.setFlags(402653184);
                ZapUi.this.mContext.startActivity(intent);
            }
        }, 300);
    }

    private void showCancelTransferActionDialog(final ActionType actionType, final Intent originalIntent, final int estimateTimeMin) {
        if (actionType == ActionType.LONG_ESTIMATE_SEND || actionType == ActionType.LONG_ESTIMATE_RECEIVE || actionType == ActionType.NOT_READY_TAKING_LONG || actionType == ActionType.SEND_TAKING_TOO_LONG || actionType == ActionType.RECEIVE_TAKING_TOO_LONG) {
            ((Vibrator) this.mContext.getSystemService("vibrator")).vibrate(400);
            this.mHandler.postDelayed(new Runnable() {
                public void run() {
                    ZapUi.this.cancelToast();
                    if (ZapUi.this.getProgressDialogFlag()) {
                        ZapUi.this.dismissProgressDialog();
                    }
                    Intent intent = new Intent(ZapUi.this.mContext, DialogUserActionRequired.class);
                    intent.putExtra(ZapConstants.EXTRA_DIALOG_USER_ACTION_TYPE, actionType);
                    intent.putExtra(ZapConstants.EXTRA_ORIGINAL_INTENT, originalIntent);
                    intent.putExtra(ZapConstants.EXTRA_ESTIMATE_TRANSF_DURATION, estimateTimeMin);
                    intent.setFlags(402653184);
                    ZapUi.this.mContext.startActivity(intent);
                }
            }, 300);
            return;
        }
        throw new IllegalArgumentException("showCancelTransferActionDialog - Invalid action type.");
    }

    /* access modifiers changed from: private */
    public synchronized void cancelToast() {
        if (this.mToast != null) {
            this.mToast.cancel();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void setProgressDialogFlag(boolean value) {
        this.mShowingProgressDialog = value;
    }

    /* access modifiers changed from: private */
    public synchronized boolean getProgressDialogFlag() {
        return this.mShowingProgressDialog;
    }

    private void finishCurrentDialogActivity() {
        LocalBroadcastManager.getInstance(this.mContext).sendBroadcast(new Intent(ZapConstants.ACTION_FINISH_DIALOG_ACTIVITY));
    }

    public void dismissUserActionDialogs(ActionType actionType) {
        Intent intent = new Intent(ZapConstants.ACTION_FINISH_USER_ACTION_DIALOG_ACTIVITY);
        intent.putExtra(ZapConstants.EXTRA_DIALOG_USER_ACTION_TYPE, actionType);
        LocalBroadcastManager.getInstance(this.mContext).sendBroadcast(intent);
    }

    private static void playRingtone(int resId, long waitAfter) {
    }

    private void removeZapUniversalNotification() {
        if (!this.mHistoryHandler.hasTransfersInProgress()) {
            getNotificationManager().cancel(0);
        }
    }

    public void startMainActivity() {
        ZapUtils.startZapApp();
    }

    public void startSetupActivity(Intent intent) {
        vibrate(200);
        Intent intent2 = new Intent(App.getAppContext(), SetupActivity.class);
        intent2.setFlags(335544320);
        intent2.putExtra(ZapConstants.EXTRA_ORIGINAL_INTENT, intent);
        App.getAppContext().startActivity(intent2);
    }

    public Intent getStartMainActivityIntent() {
        return new Intent(App.getAppContext(), DroidBlastLauncher.class);
    }

    public void onLocationEvent(LocationEvent locationEvent) {
        switch (locationEvent) {
            case VERIFYING_LOCATION:
                showToastMessage(this.mContext.getString(R.string.toast_msg_verifying_location), 0, true, false);
                return;
            case STILL_VERIFYING_LOCATION:
                showToastMessage(this.mContext.getString(R.string.toast_msg_still_verifying_location), 0, true, false);
                return;
            default:
                return;
        }
    }

    public void onAuthenticationEvent(AuthenticationEvent authenticationEvent) {
        switch (authenticationEvent) {
            case AUTH_TAKING_TOO_LONG:
                showToastMessage(this.mContext.getString(R.string.toast_msg_auth_in_progress), 0, true, false);
                return;
            default:
                return;
        }
    }

    public void onAbuseEvent(AbuseAction abuseAction) {
        switch (abuseAction) {
            case WARNING:
                showAbuseActionNotification(this.mContext.getString(R.string.notify_abuse_warning_title), this.mContext.getString(R.string.notify_abuse_warning_summary));
                return;
            case SENDING_BLOCKED:
                showAbuseActionNotification(this.mContext.getString(R.string.notify_abuse_blocked_title), this.mContext.getString(R.string.notify_abuse_blocked_summary));
                return;
            case BLOCKED:
                showAbuseActionNotification(this.mContext.getString(R.string.notify_abuse_disabled_title), this.mContext.getString(R.string.notify_abuse_disabled_summary));
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public Intent getCancelUploadIntent(ContentContainer contentContainer, boolean z) {
        Intent intent = new Intent(ZapConstants.ACTION_CANCEL_UPLOAD);
        intent.putExtra(ZapConstants.EXTRA_TRANSACTION_UID, contentContainer.getUid());
        intent.putExtra(ZapConstants.EXTRA_SERVER_SHARE_ID, contentContainer.getShareId());
        intent.putExtra(ZapConstants.EXTRA_CANCEL_USER_VISIBLE, z);
        return intent;
    }

    /* access modifiers changed from: private */
    public Intent getCancelDownloadIntent(ContentContainer contentContainer) {
        Intent intent = new Intent(ZapConstants.ACTION_CANCEL_DOWNLOAD);
        intent.putExtra(ZapConstants.EXTRA_TRANSACTION_UID, contentContainer.getUid());
        intent.putExtra(ZapConstants.EXTRA_FROM_ID, contentContainer.getOrigin());
        intent.putExtra(ZapConstants.EXTRA_SERVER_SHARE_ID, contentContainer.getShareId());
        return intent;
    }

    public void onCancelOnGoingUpload(ContentContainer trans) {
        this.mHistoryHandler.setUploadComplete(trans, 2);
        InAppNotifCenter.getInAppNotifCenter().postEvent(4, new Bundle());
        ZapTracker.getInstance().remove(trans);
    }

    public void onGroupEvent(GroupEvent groupEvent) {
        switch (groupEvent.type) {
            case GROUP_ACTIVE:
                if (ZapConfig.supportsFullFeature()) {
                    TransferState.getInstance().setZz(this.mContext, true);
                }
                ZzPoller.getInstance().setPollingSettingEnabled(true);
                ZzCameraWatcher.getInstance().setCameraAutoShareSettingEnabled(true);
                Z.ZZ_REDUCED_IMAGE.write(Boolean.valueOf(true));
                GroupInfo groupInfo = GCSP.getGroupManager().getGroupInfo(null);
                this.mHistoryHandler.setLeftZz(groupInfo.owner, groupInfo.id, false);
                this.mHistoryHandler.insertActiveZzSession(groupInfo.owner, groupInfo.id, groupInfo.name, groupInfo.thumbnail, groupInfo.pin);
                InAppNotifCenter.getInAppNotifCenter().postEvent(10);
                return;
            case GROUP_MODIFIED:
                ZapTracker.getInstance().onZzParticipantCountChange(groupEvent.groupInfo.memberCount);
                return;
            case GROUP_INACTIVE:
                if (ZapConfig.supportsFullFeature()) {
                    TransferState.getInstance().setZz(this.mContext, false);
                }
                ZzPoller.getInstance().setPollingSettingEnabled(false);
                ZzCameraWatcher.getInstance().setCameraAutoShareSettingEnabled(false);
                AutoRefresher.getInstance().start();
                this.mHistoryHandler.setZzInactive();
                InAppNotifCenter.getInAppNotifCenter().postEvent(11);
                return;
            case GROUP_CREATE:
                new ZzCreateUiHandler().handleMessage(groupEvent);
                return;
            case GROUP_INVITE_SHARE:
                new ZzShareInviteUiHandler().handleMessage(groupEvent);
                return;
            case GROUP_JOIN:
                new ZzJoinUiHandler().handleMessage(groupEvent);
                return;
            case GROUP_LEAVE:
                new ZzLeaveUiHandler().handleMessage(groupEvent);
                return;
            case GROUP_INVITE_RECEIVE:
                new ZzHandleInviteUiHandler().handleMessage(groupEvent);
                return;
            case GROUP_EXPIRE:
                new ZzExpiredUiHandler().handleMessage(groupEvent);
                return;
            case GROUP_SYNC:
                new ZzSyncStateUiHandler().handleMessage(groupEvent);
                return;
            case GROUP_UPDATE:
                new ZzUpdateUiHandler().handleMessage(groupEvent);
                return;
            default:
                return;
        }
    }

    public ContentDiscoveryListener getContentDiscoveryListener() {
        return new DiscoveryTracker();
    }

    public ContentDownloadListener getContentDownloadListener() {
        return new DownloadTracker();
    }

    public ContentShareListener getContentShareListener() {
        return new ShareTracker();
    }

    public ReportAbuseTracker getReportAbuseListener() {
        return new ReportAbuseTracker();
    }
}
