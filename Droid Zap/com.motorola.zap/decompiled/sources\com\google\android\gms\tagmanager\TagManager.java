package com.google.android.gms.tagmanager;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.api.PendingResult;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class TagManager {
    private static TagManager XB;
    private final DataLayer TN;
    private final r Wj;
    private final ConcurrentMap<n, Boolean> XA;
    private final a Xz;
    private final Context mContext;

    interface a {
        o a(Context context, TagManager tagManager, Looper looper, String str, int i, r rVar);
    }

    TagManager(Context context, a containerHolderLoaderProvider, DataLayer dataLayer) {
        if (context == null) {
            throw new NullPointerException("context cannot be null");
        }
        this.mContext = context.getApplicationContext();
        this.Xz = containerHolderLoaderProvider;
        this.XA = new ConcurrentHashMap();
        this.TN = dataLayer;
        this.TN.a((b) new b() {
            public void v(Map<String, Object> map) {
                Object obj = map.get("event");
                if (obj != null) {
                    TagManager.this.bE(obj.toString());
                }
            }
        });
        this.TN.a((b) new d(this.mContext));
        this.Wj = new r();
    }

    /* access modifiers changed from: private */
    public void bE(String str) {
        for (n ba : this.XA.keySet()) {
            ba.ba(str);
        }
    }

    public static TagManager getInstance(Context context) {
        TagManager tagManager;
        synchronized (TagManager.class) {
            if (XB == null) {
                if (context == null) {
                    bh.t("TagManager.getInstance requires non-null context.");
                    throw new NullPointerException();
                }
                XB = new TagManager(context, new a() {
                    public o a(Context context, TagManager tagManager, Looper looper, String str, int i, r rVar) {
                        return new o(context, tagManager, looper, str, i, rVar);
                    }
                }, new DataLayer(new v(context)));
            }
            tagManager = XB;
        }
        return tagManager;
    }

    /* access modifiers changed from: 0000 */
    public void a(n nVar) {
        this.XA.put(nVar, Boolean.valueOf(true));
    }

    /* access modifiers changed from: 0000 */
    public boolean b(n nVar) {
        return this.XA.remove(nVar) != null;
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean f(Uri uri) {
        boolean z;
        ce ju = ce.ju();
        if (ju.f(uri)) {
            String containerId = ju.getContainerId();
            switch (ju.jv()) {
                case NONE:
                    for (n nVar : this.XA.keySet()) {
                        if (nVar.getContainerId().equals(containerId)) {
                            nVar.bc(null);
                            nVar.refresh();
                        }
                    }
                    break;
                case CONTAINER:
                case CONTAINER_DEBUG:
                    for (n nVar2 : this.XA.keySet()) {
                        if (nVar2.getContainerId().equals(containerId)) {
                            nVar2.bc(ju.jw());
                            nVar2.refresh();
                        } else if (nVar2.iF() != null) {
                            nVar2.bc(null);
                            nVar2.refresh();
                        }
                    }
                    break;
            }
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    public DataLayer getDataLayer() {
        return this.TN;
    }

    public PendingResult<ContainerHolder> loadContainerDefaultOnly(String containerId, int defaultContainerResourceId) {
        o a2 = this.Xz.a(this.mContext, this, null, containerId, defaultContainerResourceId, this.Wj);
        a2.iI();
        return a2;
    }

    public PendingResult<ContainerHolder> loadContainerDefaultOnly(String containerId, int defaultContainerResourceId, Handler handler) {
        o a2 = this.Xz.a(this.mContext, this, handler.getLooper(), containerId, defaultContainerResourceId, this.Wj);
        a2.iI();
        return a2;
    }

    public PendingResult<ContainerHolder> loadContainerPreferFresh(String containerId, int defaultContainerResourceId) {
        o a2 = this.Xz.a(this.mContext, this, null, containerId, defaultContainerResourceId, this.Wj);
        a2.iK();
        return a2;
    }

    public PendingResult<ContainerHolder> loadContainerPreferFresh(String containerId, int defaultContainerResourceId, Handler handler) {
        o a2 = this.Xz.a(this.mContext, this, handler.getLooper(), containerId, defaultContainerResourceId, this.Wj);
        a2.iK();
        return a2;
    }

    public PendingResult<ContainerHolder> loadContainerPreferNonDefault(String containerId, int defaultContainerResourceId) {
        o a2 = this.Xz.a(this.mContext, this, null, containerId, defaultContainerResourceId, this.Wj);
        a2.iJ();
        return a2;
    }

    public PendingResult<ContainerHolder> loadContainerPreferNonDefault(String containerId, int defaultContainerResourceId, Handler handler) {
        o a2 = this.Xz.a(this.mContext, this, handler.getLooper(), containerId, defaultContainerResourceId, this.Wj);
        a2.iJ();
        return a2;
    }

    public void setVerboseLoggingEnabled(boolean enableVerboseLogging) {
        bh.setLogLevel(enableVerboseLogging ? 2 : 5);
    }
}
