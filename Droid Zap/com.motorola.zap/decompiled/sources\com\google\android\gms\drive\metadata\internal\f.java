package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;

public class f implements Creator<MetadataBundle> {
    static void a(MetadataBundle metadataBundle, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.c(parcel, 1, metadataBundle.wj);
        b.a(parcel, 2, metadataBundle.Ek, false);
        b.D(parcel, p);
    }

    /* renamed from: aF */
    public MetadataBundle[] newArray(int i) {
        return new MetadataBundle[i];
    }

    /* renamed from: aa */
    public MetadataBundle createFromParcel(Parcel parcel) {
        int o = a.o(parcel);
        int i = 0;
        Bundle bundle = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    i = a.g(parcel, n);
                    break;
                case 2:
                    bundle = a.o(parcel, n);
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new MetadataBundle(i, bundle);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }
}
