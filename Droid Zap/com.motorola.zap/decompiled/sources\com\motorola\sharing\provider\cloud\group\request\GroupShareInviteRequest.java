package com.motorola.sharing.provider.cloud.group.request;

import android.content.Intent;
import com.motorola.gcsp.json.StatusCode;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener.GroupEvent.Substatus;
import com.motorola.sharing.GroupListener.GroupEvent.Type;
import com.motorola.sharing.GroupManager.ShareInviteParams;
import com.motorola.sharing.Settings;
import com.motorola.sharing.provider.cloud.GroupReceiver;
import com.motorola.sharing.provider.cloud.HttpHelper;
import com.motorola.sharing.provider.cloud.UrlHelper;
import com.motorola.sharing.provider.cloud.group.GroupHandler;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker.GroupState;
import com.motorola.sharing.util.JsonHelper;
import com.motorola.sharing.util.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class GroupShareInviteRequest extends GroupRequest {
    private static final String TAG = Logger.getCloudTag("GroupShareInvReq");
    private GroupState mGroupState = null;
    private final ShareInviteParams mParams;

    public GroupShareInviteRequest(ShareInviteParams params) {
        super(GCSP.getContext(), false);
        this.mParams = params;
        this.mGroupState = this.mGroupStateTracker.getState();
    }

    public Type getWhat() {
        return Type.GROUP_INVITE_SHARE;
    }

    public Substatus checkPreconditions() {
        if (!this.mGroupState.isUserOwner()) {
            return Substatus.NOT_ALLOWED;
        }
        if (!isNetworkConnected()) {
            return Substatus.NETWORK_ERROR;
        }
        return super.checkPreconditions();
    }

    public Intent getRetryIntent() {
        Intent intent = new Intent(GroupReceiver.ACTION_GROUP_SHARE_INVITE);
        intent.putExtra(GroupReceiver.EXTRA_GROUP_PARAMS, this.mParams);
        if (isRetry()) {
            return null;
        }
        return intent;
    }

    public void run() {
        Logger.d(TAG, String.format("Running request, retry: %b", new Object[]{Boolean.valueOf(isRetry())}));
        if (!authenticate()) {
            Logger.e(TAG, "Authentication error");
            sendError(Substatus.AUTHENTICATION_ERROR);
            return;
        }
        String requestData = createRequestData();
        if (requestData == null) {
            Logger.e(TAG, "Failed to construct request");
            sendError(Substatus.CLIENT_ERROR);
            return;
        }
        String response = null;
        try {
            Logger.d(TAG, "Sending request to server...");
            response = HttpHelper.sendPostRequest(UrlHelper.updateTarget(), requestData, this.mUserToken, this.mUserId, true);
        } catch (Exception e) {
            Logger.e(TAG, "Exception sending request to server: " + e.toString());
        }
        if (response == null) {
            Logger.e(TAG, "Request to server failed.");
            sendError(Substatus.NETWORK_ERROR);
            return;
        }
        handleServerResponse(response);
    }

    private String createRequestData() {
        JSONObject jsonObj = new JSONObject();
        try {
            JsonHelper.addCommonRequestFields(jsonObj);
            jsonObj.put("targetType", "group");
            jsonObj.put("targetId", this.mGroupState.getId());
            JSONObject targetMeta = new JSONObject();
            jsonObj.put("targetMeta", targetMeta);
            targetMeta.put("targetTTL", Settings.GROUP_INVITE_TTL.get());
            if (this.mParams.locality != null) {
                JSONObject shareFilter = new JSONObject();
                jsonObj.put("shareFilter", shareFilter);
                JSONObject location = new JSONObject();
                shareFilter.put("location", location);
                location.put("lat", this.mParams.locality.latitude);
                location.put("lng", this.mParams.locality.longitude);
                location.put("distance", this.mParams.locality.distance);
            }
            Logger.d(TAG, "Server Request:");
            Logger.dump(TAG, jsonObj);
            return jsonObj.toString();
        } catch (JSONException e) {
            return null;
        }
    }

    private void handleServerResponse(String response) {
        try {
            JSONObject responseJson = new JSONObject(response);
            Logger.d(TAG, "Server Response:");
            Logger.dump(TAG, responseJson);
            StatusCode status = StatusCode.codeOf(responseJson.getString("status"));
            switch (status) {
                case OK:
                    onGroupInviteSent();
                    Logger.d(TAG, "Group invite shared - gid: " + this.mGroupState.getId());
                    sendOk();
                    return;
                case INACTIVE:
                    Logger.w(TAG, "Group expired, quitting it");
                    GroupHandler.getInstance().handleGroupExpired();
                    return;
                case ERR_010:
                    sendError(Substatus.NOT_ALLOWED);
                    return;
                default:
                    Logger.e(TAG, "Request failed with status: " + status);
                    sendError(statusToError(status));
                    return;
            }
        } catch (JSONException e) {
            Logger.e(TAG, "Exception parsing JSON response: " + e);
            sendError(Substatus.SERVER_ERROR);
        }
    }
}
