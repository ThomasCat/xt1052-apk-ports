package com.motorola.sharing;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import com.motorola.sharing.GroupManager.GroupContract;
import com.motorola.sharing.GroupManager.GroupContract.Columns;
import com.motorola.zap.provider.HistoryHandler;

public class GroupContentProvider extends ContentProvider {
    private static OpenHelper sOpenHelper;
    private static UriMatcher sUriMatcher;

    private static class OpenHelper extends SQLiteOpenHelper {
        private SQLiteDatabase mDb;

        private OpenHelper(Context context) {
            super(context, "groups.db", null, 2);
        }

        public synchronized SQLiteDatabase getDb() {
            if (this.mDb == null) {
                this.mDb = getWritableDatabase();
            }
            return this.mDb;
        }

        public void onCreate(SQLiteDatabase db) {
            StringBuffer b = new StringBuffer();
            b.append(String.format("CREATE TABLE %s (", new Object[]{"groupMembers"}));
            b.append(String.format("%s INTEGER PRIMARY KEY AUTOINCREMENT,", new Object[]{HistoryHandler.HIST_ID}));
            b.append(String.format("%s TEXT NOT NULL,", new Object[]{"groupId"}));
            b.append(String.format("%s TEXT NOT NULL,", new Object[]{Columns.USER_ID}));
            b.append(String.format("%s TEXT,", new Object[]{Columns.PROFILE_VERSION}));
            b.append(String.format("%s TEXT,", new Object[]{Columns.NICKNAME}));
            b.append(String.format("%s TEXT,", new Object[]{Columns.PICTURE_URL}));
            b.append(String.format("%s INTEGER)", new Object[]{Columns.BLOCKED}));
            db.execSQL(b.toString());
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS groupMembers");
            onCreate(db);
        }
    }

    private static synchronized OpenHelper getOpenHelper(Context context) {
        OpenHelper openHelper;
        synchronized (GroupContentProvider.class) {
            if (sOpenHelper == null) {
                sOpenHelper = new OpenHelper(context);
            }
            openHelper = sOpenHelper;
        }
        return openHelper;
    }

    private SQLiteDatabase getDb() {
        return getOpenHelper(getContext()).getDb();
    }

    private static synchronized UriMatcher getUriMatcher() {
        UriMatcher uriMatcher;
        synchronized (GroupContentProvider.class) {
            if (sUriMatcher == null) {
                sUriMatcher = new UriMatcher(-1);
                sUriMatcher.addURI(GroupContract.getAuthority(), GroupContract.MEMBERS_URI_PATH, 0);
            }
            uriMatcher = sUriMatcher;
        }
        return uriMatcher;
    }

    public boolean onCreate() {
        return true;
    }

    public String getType(Uri uri) {
        switch (getUriMatcher().match(uri)) {
            case 0:
                return GroupContract.MEMBERS_MIME_TYPE;
            default:
                return null;
        }
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        switch (getUriMatcher().match(uri)) {
            case 0:
                builder.setTables("groupMembers");
                Cursor cursor = builder.query(getDb(), projection, selection, selectionArgs, null, null, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), GroupContract.getMembersContentUri());
                return cursor;
            default:
                return null;
        }
    }

    /* JADX INFO: finally extract failed */
    public Uri insert(Uri uri, ContentValues values) {
        switch (getUriMatcher().match(uri)) {
            case 0:
                getDb().beginTransaction();
                try {
                    if (DatabaseUtils.queryNumEntries(getDb(), "groupMembers", GroupContract.SELECTION_MEMBER, new String[]{values.getAsString("groupId"), values.getAsString(Columns.USER_ID)}) != 0) {
                        getDb().endTransaction();
                        return null;
                    }
                    getDb().insert("groupMembers", null, values);
                    getContext().getContentResolver().notifyChange(GroupContract.getMembersContentUri(), null);
                    getDb().setTransactionSuccessful();
                    getDb().endTransaction();
                    return uri;
                } catch (Throwable th) {
                    getDb().endTransaction();
                    throw th;
                }
            default:
                return null;
        }
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        switch (getUriMatcher().match(uri)) {
            case 0:
                int updated = getDb().update("groupMembers", values, selection, selectionArgs);
                if (updated == 0) {
                    return updated;
                }
                getContext().getContentResolver().notifyChange(GroupContract.getMembersContentUri(), null);
                return updated;
            default:
                return 0;
        }
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        switch (getUriMatcher().match(uri)) {
            case 0:
                int deleted = getDb().delete("groupMembers", selection, selectionArgs);
                if (deleted == 0) {
                    return deleted;
                }
                getContext().getContentResolver().notifyChange(GroupContract.getMembersContentUri(), null);
                return deleted;
            default:
                return 0;
        }
    }
}
