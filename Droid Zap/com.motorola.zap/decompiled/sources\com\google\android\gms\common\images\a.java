package com.google.android.gms.common.images;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.common.images.ImageManager.OnImageLoadedListener;
import com.google.android.gms.internal.ea;
import com.google.android.gms.internal.eb;
import com.google.android.gms.internal.ec;
import com.google.android.gms.internal.ed;
import com.google.android.gms.internal.ep;
import com.google.android.gms.internal.fr;
import java.lang.ref.WeakReference;

public final class a {
    final C0005a AF;
    private int AG;
    private int AH;
    int AI;
    private int AJ;
    private WeakReference<OnImageLoadedListener> AK;
    private WeakReference<ImageView> AL;
    private WeakReference<TextView> AM;
    private int AN;
    private boolean AO;
    private boolean AP;
    private int AQ;

    /* renamed from: com.google.android.gms.common.images.a$a reason: collision with other inner class name */
    public static final class C0005a {
        public final Uri uri;

        public C0005a(Uri uri2) {
            this.uri = uri2;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C0005a)) {
                return false;
            }
            return this == obj || ((C0005a) obj).hashCode() == hashCode();
        }

        public int hashCode() {
            return ep.hashCode(this.uri);
        }
    }

    public a(int i) {
        this.AG = 0;
        this.AH = 0;
        this.AN = -1;
        this.AO = true;
        this.AP = false;
        this.AF = new C0005a(null);
        this.AH = i;
    }

    public a(Uri uri) {
        this.AG = 0;
        this.AH = 0;
        this.AN = -1;
        this.AO = true;
        this.AP = false;
        this.AF = new C0005a(uri);
        this.AH = 0;
    }

    private ea a(Drawable drawable, Drawable drawable2) {
        if (drawable == null) {
            drawable = null;
        } else if (drawable instanceof ea) {
            drawable = ((ea) drawable).dO();
        }
        return new ea(drawable, drawable2);
    }

    private void a(Drawable drawable, boolean z, boolean z2, boolean z3) {
        switch (this.AI) {
            case 1:
                if (!z2) {
                    OnImageLoadedListener onImageLoadedListener = (OnImageLoadedListener) this.AK.get();
                    if (onImageLoadedListener != null) {
                        onImageLoadedListener.onImageLoaded(this.AF.uri, drawable, z3);
                        return;
                    }
                    return;
                }
                return;
            case 2:
                ImageView imageView = (ImageView) this.AL.get();
                if (imageView != null) {
                    a(imageView, drawable, z, z2, z3);
                    return;
                }
                return;
            case 3:
                TextView textView = (TextView) this.AM.get();
                if (textView != null) {
                    a(textView, this.AN, drawable, z, z2);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void a(ImageView imageView, Drawable drawable, boolean z, boolean z2, boolean z3) {
        boolean z4 = !z2 && !z3;
        if (z4 && (imageView instanceof ec)) {
            int dQ = ((ec) imageView).dQ();
            if (this.AH != 0 && dQ == this.AH) {
                return;
            }
        }
        boolean b = b(z, z2);
        Drawable drawable2 = b ? a(imageView.getDrawable(), drawable) : drawable;
        imageView.setImageDrawable(drawable2);
        if (imageView instanceof ec) {
            ec ecVar = (ec) imageView;
            ecVar.d(z3 ? this.AF.uri : null);
            ecVar.N(z4 ? this.AH : 0);
        }
        if (b) {
            ((ea) drawable2).startTransition(250);
        }
    }

    private void a(TextView textView, int i, Drawable drawable, boolean z, boolean z2) {
        boolean b = b(z, z2);
        Drawable[] compoundDrawables = fr.eO() ? textView.getCompoundDrawablesRelative() : textView.getCompoundDrawables();
        Drawable drawable2 = b ? a(compoundDrawables[i], drawable) : drawable;
        Drawable drawable3 = i == 0 ? drawable2 : compoundDrawables[0];
        Drawable drawable4 = i == 1 ? drawable2 : compoundDrawables[1];
        Drawable drawable5 = i == 2 ? drawable2 : compoundDrawables[2];
        Drawable drawable6 = i == 3 ? drawable2 : compoundDrawables[3];
        if (fr.eO()) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable3, drawable4, drawable5, drawable6);
        } else {
            textView.setCompoundDrawablesWithIntrinsicBounds(drawable3, drawable4, drawable5, drawable6);
        }
        if (b) {
            ((ea) drawable2).startTransition(250);
        }
    }

    private boolean b(boolean z, boolean z2) {
        return this.AO && !z2 && (!z || this.AP);
    }

    public void L(int i) {
        this.AH = i;
    }

    /* access modifiers changed from: 0000 */
    public void a(Context context, Bitmap bitmap, boolean z) {
        ed.d(bitmap);
        if ((this.AQ & 1) != 0) {
            bitmap = eb.a(bitmap);
        }
        a(new BitmapDrawable(context.getResources(), bitmap), z, false, true);
    }

    public void a(ImageView imageView) {
        ed.d(imageView);
        this.AK = null;
        this.AL = new WeakReference<>(imageView);
        this.AM = null;
        this.AN = -1;
        this.AI = 2;
        this.AJ = imageView.hashCode();
    }

    public void a(OnImageLoadedListener onImageLoadedListener) {
        ed.d(onImageLoadedListener);
        this.AK = new WeakReference<>(onImageLoadedListener);
        this.AL = null;
        this.AM = null;
        this.AN = -1;
        this.AI = 1;
        this.AJ = ep.hashCode(onImageLoadedListener, this.AF);
    }

    /* access modifiers changed from: 0000 */
    public void b(Context context, boolean z) {
        Drawable drawable = null;
        if (this.AH != 0) {
            Resources resources = context.getResources();
            drawable = resources.getDrawable(this.AH);
            if ((this.AQ & 1) != 0) {
                drawable = eb.a(resources, drawable);
            }
        }
        a(drawable, z, false, false);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof a)) {
            return false;
        }
        return this == obj || ((a) obj).hashCode() == hashCode();
    }

    public int hashCode() {
        return this.AJ;
    }

    /* access modifiers changed from: 0000 */
    public void x(Context context) {
        Drawable drawable = null;
        if (this.AG != 0) {
            Resources resources = context.getResources();
            drawable = resources.getDrawable(this.AG);
            if ((this.AQ & 1) != 0) {
                drawable = eb.a(resources, drawable);
            }
        }
        a(drawable, false, true, false);
    }
}
