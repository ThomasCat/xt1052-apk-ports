package com.google.android.gms.tagmanager;

import android.content.Context;
import android.provider.Settings.Secure;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.d;
import java.util.Map;

class bk extends aj {
    private static final String ID = a.MOBILE_ADWORDS_UNIQUE_ID.toString();
    private final Context mContext;

    public bk(Context context) {
        super(ID, new String[0]);
        this.mContext = context;
    }

    /* access modifiers changed from: protected */
    public String G(Context context) {
        return Secure.getString(context.getContentResolver(), "android_id");
    }

    public boolean iy() {
        return true;
    }

    public d.a u(Map<String, d.a> map) {
        String G = G(this.mContext);
        return G == null ? di.ku() : di.r(G);
    }
}
