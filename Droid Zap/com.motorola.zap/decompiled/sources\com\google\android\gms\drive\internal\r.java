package com.google.android.gms.drive.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi.MetadataBufferResult;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.DriveResource.MetadataResult;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.events.ChangeEvent;
import com.google.android.gms.drive.events.DriveEvent.Listener;

public class r implements DriveResource {
    protected final DriveId CS;

    private abstract class a extends m<MetadataResult> {
        private a() {
        }

        /* renamed from: r */
        public MetadataResult d(Status status) {
            return new e(status, null);
        }
    }

    private static class b extends c {
        private final com.google.android.gms.common.api.a.c<MetadataBufferResult> vj;

        public b(com.google.android.gms.common.api.a.c<MetadataBufferResult> cVar) {
            this.vj = cVar;
        }

        public void a(OnListParentsResponse onListParentsResponse) throws RemoteException {
            this.vj.b(new e(Status.zQ, new MetadataBuffer(onListParentsResponse.fd(), null)));
        }

        public void l(Status status) throws RemoteException {
            this.vj.b(new e(status, null));
        }
    }

    private abstract class c extends m<MetadataBufferResult> {
        private c() {
        }

        /* renamed from: o */
        public MetadataBufferResult d(Status status) {
            return new e(status, null);
        }
    }

    private static class d extends c {
        private final com.google.android.gms.common.api.a.c<MetadataResult> vj;

        public d(com.google.android.gms.common.api.a.c<MetadataResult> cVar) {
            this.vj = cVar;
        }

        public void a(OnMetadataResponse onMetadataResponse) throws RemoteException {
            this.vj.b(new e(Status.zQ, new j(onMetadataResponse.fe())));
        }

        public void l(Status status) throws RemoteException {
            this.vj.b(new e(status, null));
        }
    }

    private static class e implements MetadataResult {
        private final Metadata DQ;
        private final Status vl;

        public e(Status status, Metadata metadata) {
            this.vl = status;
            this.DQ = metadata;
        }

        public Metadata getMetadata() {
            return this.DQ;
        }

        public Status getStatus() {
            return this.vl;
        }
    }

    private abstract class f extends m<MetadataResult> {
        private f() {
        }

        /* renamed from: r */
        public MetadataResult d(Status status) {
            return new e(status, null);
        }
    }

    protected r(DriveId driveId) {
        this.CS = driveId;
    }

    public PendingResult<Status> addChangeListener(GoogleApiClient apiClient, Listener<ChangeEvent> listener) {
        return ((n) apiClient.a(Drive.va)).a(apiClient, this.CS, 1, listener);
    }

    public DriveId getDriveId() {
        return this.CS;
    }

    public PendingResult<MetadataResult> getMetadata(GoogleApiClient apiClient) {
        return apiClient.a(new a() {
            /* access modifiers changed from: protected */
            public void a(n nVar) throws RemoteException {
                nVar.eT().a(new GetMetadataRequest(r.this.CS), (v) new d(this));
            }
        });
    }

    public PendingResult<MetadataBufferResult> listParents(GoogleApiClient apiClient) {
        return apiClient.a(new c() {
            /* access modifiers changed from: protected */
            public void a(n nVar) throws RemoteException {
                nVar.eT().a(new ListParentsRequest(r.this.CS), (v) new b(this));
            }
        });
    }

    public PendingResult<Status> removeChangeListener(GoogleApiClient apiClient, Listener<ChangeEvent> listener) {
        return ((n) apiClient.a(Drive.va)).b(apiClient, this.CS, 1, listener);
    }

    public PendingResult<MetadataResult> updateMetadata(GoogleApiClient apiClient, final MetadataChangeSet changeSet) {
        if (changeSet != null) {
            return apiClient.b(new f() {
                /* access modifiers changed from: protected */
                public void a(n nVar) throws RemoteException {
                    nVar.eT().a(new UpdateMetadataRequest(r.this.CS, changeSet.eS()), (v) new d(this));
                }
            });
        }
        throw new IllegalArgumentException("ChangeSet must be provided.");
    }
}
