package com.motorola.sharing.chromecast;

import android.content.Context;
import android.os.Environment;
import android.webkit.MimeTypeMap;
import com.motorola.sharing.provider.p2p.NanoHTTPD;
import com.motorola.sharing.provider.p2p.NanoHTTPD.Method;
import com.motorola.sharing.provider.p2p.NanoHTTPD.Response;
import com.motorola.sharing.provider.p2p.NanoHTTPD.Response.Status;
import com.motorola.sharing.provider.tv.TVProvider;
import com.motorola.sharing.util.Logger;
import com.motorola.zap.ZapConstants;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class NanoHttpDServer extends NanoHTTPD {
    public static final String TAG = Logger.getTvTag("NanoHTTPD");
    private TVProvider mTVProvider = TVProvider.getInstance();

    public NanoHttpDServer(String host, int port) throws IOException {
        super(host, port);
    }

    private String mimeTypeFromUri(String uri) {
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(uri));
    }

    public Response serve(String aKey, Method method, Map<String, String> header, Map<String, String> map, Map<String, String> map2) {
        if (!this.mTVProvider.hasMediaUri(aKey).booleanValue()) {
            return null;
        }
        String aUri = this.mTVProvider.getMediaUri(aKey);
        String mimeType = mimeTypeFromUri(aUri);
        if (!aUri.startsWith("/") || aUri.length() <= 1) {
            return createNotFoundResponse();
        }
        File mediaFile = new File(null, aUri.substring(1));
        if (mediaFile.exists()) {
            return serveFile(aUri, header, mediaFile, mimeType);
        }
        return createNotFoundResponse();
    }

    private Response createNotFoundResponse() {
        return new Response(Status.NOT_FOUND, NanoHTTPD.MIME_PLAINTEXT, "Error 404, file not found.");
    }

    public static File getImagesRootDir(Context context) {
        File wwwRoot = new File(context.getCacheDir(), "www");
        if (!wwwRoot.exists()) {
            wwwRoot.mkdir();
        }
        return wwwRoot;
    }

    public static File getVideoRootDir(Context context) {
        return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
    }

    private Response createResponse(Status status, String mimeType, InputStream message) {
        Response res = new Response(status, mimeType, message);
        res.addHeader("Accept-Ranges", "bytes");
        return res;
    }

    private Response createResponse(Status status, String mimeType, String message) {
        Response res = new Response(status, mimeType, message);
        res.addHeader("Accept-Ranges", "bytes");
        return res;
    }

    /* access modifiers changed from: 0000 */
    public Response serveFile(String uri, Map<String, String> header, File file, String mime) {
        Logger.d(TAG, "serveFile: " + file);
        try {
            String etag = Integer.toHexString((file.getAbsolutePath() + file.lastModified() + ZapConstants.GOOGLE_ACCOUNT_NONE + file.length()).hashCode());
            long startFrom = 0;
            long endAt = -1;
            String range = (String) header.get("range");
            if (range != null && range.startsWith("bytes=")) {
                range = range.substring("bytes=".length());
                int minus = range.indexOf(45);
                if (minus > 0) {
                    try {
                        startFrom = Long.parseLong(range.substring(0, minus));
                        endAt = Long.parseLong(range.substring(minus + 1));
                    } catch (NumberFormatException e) {
                    }
                }
            }
            long fileLen = file.length();
            if (range == null || startFrom < 0) {
                if (etag.equals(header.get("if-none-match"))) {
                    Logger.d(TAG, "NOT_MODIFIED");
                    return createResponse(Status.NOT_MODIFIED, mime, ZapConstants.GOOGLE_ACCOUNT_NONE);
                }
                Logger.d(TAG, "OK");
                Status status = Status.OK;
                FileInputStream fileInputStream = new FileInputStream(file);
                Response res = createResponse(status, mime, (InputStream) fileInputStream);
                res.addHeader("Content-Length", ZapConstants.GOOGLE_ACCOUNT_NONE + fileLen);
                res.addHeader("ETag", etag);
                return res;
            } else if (startFrom >= fileLen) {
                Response res2 = createResponse(Status.RANGE_NOT_SATISFIABLE, NanoHTTPD.MIME_PLAINTEXT, ZapConstants.GOOGLE_ACCOUNT_NONE);
                res2.addHeader("Content-Range", "bytes 0-0/" + fileLen);
                res2.addHeader("ETag", etag);
                return res2;
            } else {
                if (endAt < 0) {
                    endAt = fileLen - 1;
                }
                long newLen = (endAt - startFrom) + 1;
                if (newLen < 0) {
                    newLen = 0;
                }
                final long dataLen = newLen;
                FileInputStream fis = new FileInputStream(file) {
                    public int available() throws IOException {
                        return (int) dataLen;
                    }
                };
                fis.skip(startFrom);
                Logger.d(TAG, "PARTIAL_CONTENT");
                Response res3 = createResponse(Status.PARTIAL_CONTENT, mime, (InputStream) fis);
                res3.addHeader("Content-Length", ZapConstants.GOOGLE_ACCOUNT_NONE + dataLen);
                res3.addHeader("Content-Range", "bytes " + startFrom + "-" + endAt + "/" + fileLen);
                res3.addHeader("ETag", etag);
                return res3;
            }
        } catch (IOException e2) {
            return createResponse(Status.FORBIDDEN, NanoHTTPD.MIME_PLAINTEXT, "FORBIDDEN: Reading file failed.");
        }
    }
}
