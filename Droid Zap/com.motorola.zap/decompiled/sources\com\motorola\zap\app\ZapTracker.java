package com.motorola.zap.app;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.GCSP;
import com.motorola.zap.ZapConfig;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

public class ZapTracker {
    private static ZapTracker sInstance;
    private final Map<String, Long> mDiscoveredMap = new HashMap();
    private Runnable mExpireRunnable = new Runnable() {
        public void run() {
            ZapTracker.this.setLockedContentAvailable(false);
        }
    };
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private boolean mLockedContentAvailable;
    private final Set<LockedContentListener> mLockedContentListeners = new HashSet();
    private final Set<ProgressListener> mProgressListeners = new HashSet();
    private final Map<Long, ContentContainer> mTransactionMap = new HashMap();
    private final Set<ZapListener> mZapListeners = new HashSet();
    private final Set<ZapRefreshListener> mZapRefreshListeners = new HashSet();
    private final Set<ZzListener> mZzListeners = new HashSet();

    public interface LockedContentListener {
        void onLockedContent(boolean z);

        void onWrongPin();
    }

    public interface ProgressListener {
        void onProgress(ContentContainer contentContainer);
    }

    public interface ZapListener {
        void onZapEvent(String str);
    }

    public interface ZapRefreshListener {
        void onRefresh();

        void onRefreshComplete();
    }

    public interface ZzListener {
        void onZzParticipantCountChange(int i);
    }

    public static synchronized ZapTracker getInstance() {
        ZapTracker zapTracker;
        synchronized (ZapTracker.class) {
            if (sInstance == null) {
                sInstance = new ZapTracker();
            }
            zapTracker = sInstance;
        }
        return zapTracker;
    }

    private ZapTracker() {
    }

    public synchronized void put(ContentContainer container) {
        Context context = App.getAppContext();
        this.mTransactionMap.put(Long.valueOf(container.getUid()), container);
        if (container.isReceiving()) {
            String id = getFullShareId(container);
            if (id != null) {
                this.mDiscoveredMap.put(id, Long.valueOf(container.getUid()));
            }
        }
        if (ZapConfig.supportsFullFeature()) {
            if (container.isReceiving()) {
                TransferState.getInstance().addReceiving(context);
            } else if (container.isSharing()) {
                TransferState.getInstance().addSending(context);
            }
        }
    }

    public synchronized ContentContainer get(long uid) {
        return (ContentContainer) this.mTransactionMap.get(Long.valueOf(uid));
    }

    public synchronized ContentContainer remove(ContentContainer container) {
        Context context = App.getAppContext();
        if (ZapConfig.supportsFullFeature()) {
            if (container.isReceiving()) {
                TransferState.getInstance().removeReceiving(context);
            } else if (container.isSharing()) {
                long timeout = container.getExpiryTime() - System.currentTimeMillis();
                TransferState instance = TransferState.getInstance();
                if (timeout <= 0) {
                    timeout = 0;
                }
                instance.removeSending(context, timeout);
            }
        }
        if (container.isReceiving()) {
            String id = getFullShareId(container);
            if (id != null) {
                this.mDiscoveredMap.remove(id);
            }
        }
        return (ContentContainer) this.mTransactionMap.remove(Long.valueOf(container.getUid()));
    }

    public synchronized boolean isRecentlyDiscovered(ContentContainer container) {
        return container.isReceiving() ? this.mDiscoveredMap.containsKey(getFullShareId(container)) : false;
    }

    private static String getFullShareId(ContentContainer container) {
        if (container == null) {
            return null;
        }
        String origin = container.getOrigin();
        String shareId = container.getShareId();
        if (origin == null || shareId == null) {
            return null;
        }
        return origin + shareId;
    }

    public synchronized void addListener(ProgressListener listener) {
        this.mProgressListeners.add(listener);
    }

    public synchronized boolean removeListener(ProgressListener listener) {
        return this.mProgressListeners.remove(listener);
    }

    public synchronized void addListener(LockedContentListener listener) {
        this.mLockedContentListeners.add(listener);
    }

    public synchronized boolean removeListener(LockedContentListener listener) {
        return this.mLockedContentListeners.remove(listener);
    }

    public synchronized void addRefreshListener(ZapRefreshListener listener) {
        this.mZapRefreshListeners.add(listener);
    }

    public synchronized boolean removeRefreshListener(ZapRefreshListener listener) {
        return this.mZapRefreshListeners.remove(listener);
    }

    public synchronized void addListener(ZapListener listener) {
        this.mZapListeners.add(listener);
    }

    public synchronized boolean removeListener(ZapListener listener) {
        return this.mZapListeners.remove(listener);
    }

    public synchronized void addListener(ZzListener listener) {
        this.mZzListeners.add(listener);
    }

    public synchronized boolean removeListener(ZzListener listener) {
        return this.mZzListeners.remove(listener);
    }

    public synchronized void cancelAll() {
        for (Object obj : new TreeSet<>(this.mTransactionMap.keySet())) {
            ContentContainer trans = (ContentContainer) this.mTransactionMap.get(obj);
            if (trans.isReceiving()) {
                GCSP.getContentDispatcher().cancelDownload(trans);
            } else if (trans.isSharing()) {
                new CancelShareHandler().cancelShare(trans.getUid(), trans.getShareId(), false);
            }
        }
    }

    public synchronized void onProgressUpdate(ContentContainer container) {
        for (ProgressListener p : this.mProgressListeners) {
            p.onProgress(container);
        }
    }

    public synchronized void setLockedContentAvailable(boolean available) {
        this.mLockedContentAvailable = available;
        for (LockedContentListener l : this.mLockedContentListeners) {
            l.onLockedContent(available);
        }
        if (this.mLockedContentAvailable) {
            this.mHandler.removeCallbacks(this.mExpireRunnable);
            this.mHandler.postDelayed(this.mExpireRunnable, TimeUnit.MINUTES.toMillis(2));
        } else {
            this.mHandler.removeCallbacks(this.mExpireRunnable);
        }
    }

    public synchronized void onWrongPin() {
        for (LockedContentListener l : this.mLockedContentListeners) {
            l.onWrongPin();
        }
    }

    public synchronized void onRefresh() {
        for (ZapRefreshListener l : this.mZapRefreshListeners) {
            l.onRefresh();
        }
    }

    public synchronized void onRefreshComplete() {
        for (ZapRefreshListener l : this.mZapRefreshListeners) {
            l.onRefreshComplete();
        }
    }

    public synchronized boolean getLockedContenAvailable() {
        return this.mLockedContentAvailable;
    }

    public synchronized void onZapEvent(String message) {
        for (ZapListener l : this.mZapListeners) {
            l.onZapEvent(message);
        }
    }

    public synchronized void onZzParticipantCountChange(int count) {
        for (ZzListener l : this.mZzListeners) {
            l.onZzParticipantCountChange(count);
        }
    }
}
