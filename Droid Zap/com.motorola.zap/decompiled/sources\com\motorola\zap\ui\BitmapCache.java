package com.motorola.zap.ui;

import android.graphics.Bitmap;
import android.os.Handler;
import android.util.LruCache;
import android.widget.ImageView;
import com.motorola.sharing.util.FileUtils;

public class BitmapCache {
    private static BitmapCache sInstance;
    private LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(((int) (Runtime.getRuntime().maxMemory() / 1024)) / 4) {
        /* access modifiers changed from: protected */
        public int sizeOf(String key, Bitmap bitmap) {
            return bitmap.getByteCount() / 1024;
        }
    };

    public static class BitmapWorkerTask implements Runnable, Comparable<BitmapWorkerTask> {
        private final String mFilePath;
        private final Handler mHandler;
        private final int mHeight;
        private final ImageView mImageView;
        private final Long mTs = Long.valueOf(System.currentTimeMillis());
        private final int mWidth;

        public BitmapWorkerTask(Handler handler, ImageView imageView, int width, int height, String filePath) {
            this.mImageView = imageView;
            this.mWidth = width;
            this.mHeight = height;
            this.mHandler = handler;
            this.mFilePath = filePath;
        }

        private boolean isViewUpToDate() {
            return this.mFilePath.equals(this.mImageView.getTag());
        }

        public void run() {
            if (isViewUpToDate()) {
                Bitmap bitmap = BitmapCache.getInstance().get(this.mFilePath);
                if (bitmap == null) {
                    try {
                        bitmap = FileUtils.getThumbnail(this.mFilePath, this.mWidth, this.mHeight);
                        if (bitmap != null) {
                            BitmapCache.getInstance().put(this.mFilePath, bitmap);
                        }
                    } catch (OutOfMemoryError e) {
                        BitmapCache.getInstance().evictAll();
                    }
                }
                final Bitmap finalBitmap = bitmap;
                if (finalBitmap != null) {
                    this.mHandler.post(new Runnable() {
                        public void run() {
                            BitmapWorkerTask.this.onPostExecute(finalBitmap);
                        }
                    });
                }
            }
        }

        /* access modifiers changed from: private */
        public void onPostExecute(Bitmap bitmap) {
            if (isViewUpToDate()) {
                this.mImageView.setImageBitmap(bitmap);
            }
        }

        public int compareTo(BitmapWorkerTask another) {
            return another.mTs.compareTo(this.mTs);
        }
    }

    public static synchronized BitmapCache getInstance() {
        BitmapCache bitmapCache;
        synchronized (BitmapCache.class) {
            if (sInstance == null) {
                sInstance = new BitmapCache();
            }
            bitmapCache = sInstance;
        }
        return bitmapCache;
    }

    private BitmapCache() {
    }

    public synchronized void put(String key, Bitmap bitmap) {
        if (this.mCache.get(key) == null) {
            this.mCache.put(key, bitmap);
        }
    }

    public synchronized Bitmap get(String key) {
        return (Bitmap) this.mCache.get(key);
    }

    public synchronized void evictAll() {
        this.mCache.evictAll();
    }
}
