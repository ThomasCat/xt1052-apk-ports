package com.motorola.zap.ui;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;

public class ViewGroupZapGestureTouchListener extends ZapGestureTouchListener {
    ViewGroupZapGestureTouchListener(Context context) {
        super(context);
    }

    public boolean onTouch(View view, MotionEvent event) {
        super.onTouch(view, event);
        return true;
    }
}
