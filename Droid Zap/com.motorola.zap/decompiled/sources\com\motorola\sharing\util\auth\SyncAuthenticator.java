package com.motorola.sharing.util.auth;

import com.motorola.sharing.Authenticator.Callback;
import com.motorola.sharing.Authenticator.Status;
import com.motorola.sharing.Authenticator.TokenInfo;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.util.Logger;
import java.util.concurrent.Semaphore;

public class SyncAuthenticator {
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getGcspTag("SyncAuth");

    public static TokenInfo auth() {
        final Semaphore semaphore = new Semaphore(0);
        final TokenInfo[] tokenInfoA = new TokenInfo[1];
        Callback authCallback = new Callback() {
            public void onAuthComplete(Status status, TokenInfo tokenInfo, Object data) {
                Logger.d(SyncAuthenticator.TAG, "onAuthComplete status: " + status);
                if (tokenInfo == null || tokenInfo.isEmpty()) {
                    tokenInfoA[0] = null;
                } else {
                    tokenInfoA[0] = tokenInfo;
                }
                semaphore.release();
            }
        };
        TokenInfo tokenInfo = GCSP.getAuthenticator().getTokenInfo();
        if (tokenInfo != null) {
            return tokenInfo;
        }
        Logger.d(TAG, "Doesn't have a valid token, so starting authentication process");
        GCSP.getAuthenticator().authenticate(authCallback, null);
        Logger.d(TAG, "Blocking on semaphore to wait for authentication to complete");
        try {
            semaphore.acquire();
            return tokenInfoA[0];
        } catch (InterruptedException e) {
            Logger.e(TAG, "Authentication interrupted", e);
            return null;
        }
    }
}
