package com.google.sample.castcompanionlibrary.cast.player;

import com.google.android.gms.cast.MediaInfo;
import org.json.JSONObject;

public interface IMediaAuthListener {
    void onFailure(String str);

    void onResult(MediaAuthStatus mediaAuthStatus, MediaInfo mediaInfo, String str, int i, JSONObject jSONObject);
}
