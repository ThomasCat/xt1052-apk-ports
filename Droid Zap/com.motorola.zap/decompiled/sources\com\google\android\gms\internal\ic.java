package com.google.android.gms.internal;

import com.google.android.gms.internal.hx.a;
import com.google.android.gms.internal.hx.a.C0041a;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ic {
    private static int OB = 10000;
    private static int OC = 1000;
    private final hl<hg> Lk;
    private final String OD;
    private final BlockingQueue<a> OE;
    private int OF;
    private final int Ou;
    private final Object mg = new Object();

    public ic(hl<hg> hlVar, String str, int i) {
        this.Lk = hlVar;
        this.OD = str;
        this.Ou = i;
        this.OE = new LinkedBlockingQueue(OB);
        this.OF = OC;
    }

    public void a(C0041a aVar) {
        aVar.aK(this.OD);
        aVar.bv(this.Ou);
        this.OE.offer(aVar.gJ());
    }
}
