package com.google.android.gms.panorama;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ApiOptions;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a.C0003a;
import com.google.android.gms.internal.ee;
import com.google.android.gms.internal.ih;

public final class Panorama {
    public static final Api API = new Api(va, new Scope[0]);
    static final com.google.android.gms.common.api.Api.b<ih> va = new com.google.android.gms.common.api.Api.b<ih>() {
        /* renamed from: g */
        public ih b(Context context, Looper looper, ee eeVar, ApiOptions apiOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return new ih(context, looper, connectionCallbacks, onConnectionFailedListener);
        }

        public int getPriority() {
            return Integer.MAX_VALUE;
        }
    };

    public interface PanoramaResult extends Result {
        Intent getViewerIntent();
    }

    public interface a extends PanoramaResult {
    }

    private static abstract class b extends C0003a<PanoramaResult, ih> {
        public b() {
            super(Panorama.va);
        }

        /* renamed from: J */
        public PanoramaResult d(final Status status) {
            return new PanoramaResult() {
                public Status getStatus() {
                    return status;
                }

                public Intent getViewerIntent() {
                    return null;
                }
            };
        }
    }

    private Panorama() {
    }

    public static PendingResult<PanoramaResult> loadPanoramaInfo(GoogleApiClient client, final Uri uri) {
        return client.a(new b() {
            /* access modifiers changed from: protected */
            public void a(ih ihVar) {
                ihVar.a(this, uri, false);
            }
        });
    }

    public static PendingResult<PanoramaResult> loadPanoramaInfoAndGrantAccess(GoogleApiClient client, final Uri uri) {
        return client.a(new b() {
            /* access modifiers changed from: protected */
            public void a(ih ihVar) {
                ihVar.a(this, uri, true);
            }
        });
    }
}
