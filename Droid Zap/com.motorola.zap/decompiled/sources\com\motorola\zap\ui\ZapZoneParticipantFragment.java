package com.motorola.zap.ui;

import android.app.Fragment;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener;
import com.motorola.sharing.GroupListener.GroupEvent;
import com.motorola.sharing.GroupListener.GroupEvent.Status;
import com.motorola.sharing.GroupListener.GroupEvent.Type;
import com.motorola.sharing.GroupManager;
import com.motorola.sharing.GroupManager.BanParams;
import com.motorola.sharing.GroupManager.GroupContract;
import com.motorola.sharing.GroupManager.GroupContract.Columns;
import com.motorola.sharing.GroupManager.GroupInfo;
import com.motorola.sharing.Profile;
import com.motorola.zap.R;
import com.motorola.zap.cacheutils.ZapImageCache;
import com.motorola.zap.identity.ZapProfileSharedPerfs;
import java.util.HashMap;
import java.util.Map;

public class ZapZoneParticipantFragment extends Fragment implements LoaderCallbacks<Cursor>, GroupListener {
    Context mContext;
    GroupManager mGroupManager = GCSP.getGroupManager();
    /* access modifiers changed from: private */
    public boolean mIsZapZoneOwner = false;
    CustomCursorAdapter mParticipantAdapter;
    /* access modifiers changed from: private */
    public final Map<String, BanItemViewModel> mUidBanMap = new HashMap();
    /* access modifiers changed from: private */
    public String mZapZoneId = null;

    class BanGroupUserObserver extends ContentObserver {
        public BanGroupUserObserver(Handler handler) {
            super(handler);
        }

        /* access modifiers changed from: 0000 */
        public void observe() {
            ZapZoneParticipantFragment.this.mContext.getContentResolver().registerContentObserver(GroupContract.getMembersContentUri(), true, this);
        }

        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            ZapZoneParticipantFragment.this.mParticipantAdapter.notifyDataSetChanged();
        }

        public void onChange(boolean selfChange, Uri uri) {
            super.onChange(selfChange, uri);
            ZapZoneParticipantFragment.this.mParticipantAdapter.notifyDataSetChanged();
        }
    }

    class BanItemViewModel {
        public Boolean isBanRequested = Boolean.valueOf(false);
        /* access modifiers changed from: private */
        public FrameLayout mFrameLayout;

        public BanItemViewModel(FrameLayout aFrameLayout) {
            this.mFrameLayout = aFrameLayout;
        }
    }

    public class CustomCursorAdapter extends CursorAdapter {
        private LayoutInflater mInflater;

        public CustomCursorAdapter(Context context, Cursor c, int flags) {
            super(context, c, flags);
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        }

        private ViewGroup getBanItemLayout(ViewGroup parent, ViewGroup current, Cursor cursor) {
            ViewGroup layout;
            if (current == null) {
                layout = (ViewGroup) this.mInflater.inflate(R.layout.zapzone_participant_list_item, parent, false);
            } else {
                layout = current;
            }
            TextView participantNameView = (TextView) layout.findViewById(R.id.participant_name);
            if (!TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(Columns.NICKNAME)))) {
                participantNameView.setText(cursor.getString(cursor.getColumnIndex(Columns.NICKNAME)));
            } else {
                participantNameView.setText(R.string.prof_anonymous);
            }
            String profilePicUrl = cursor.getString(cursor.getColumnIndex(Columns.PICTURE_URL));
            NetworkImageView profilePic = (NetworkImageView) layout.findViewById(R.id.participantPic);
            if (profilePicUrl != null) {
                profilePic.setImageUrl(profilePicUrl, ZapImageCache.getInstance().getImageLoader());
                profilePic.setDefaultImageResId(R.drawable.ic_default_profile);
                profilePic.setErrorImageResId(R.drawable.ic_default_profile);
            } else {
                profilePic.setImageResource(R.drawable.ic_default_profile);
            }
            Cursor groupCursor = cursor;
            String userId = groupCursor.getString(groupCursor.getColumnIndex(Columns.USER_ID));
            if (ZapZoneParticipantFragment.this.mIsZapZoneOwner) {
                ImageButton removeParticipant = (ImageButton) layout.findViewById(R.id.removeParticipant);
                FrameLayout removeContainer = (FrameLayout) layout.findViewById(R.id.removeprogressframe);
                ProgressBar removeProgress = (ProgressBar) layout.findViewById(R.id.progressBarRemoveParticipant);
                BanItemViewModel bmModel = (BanItemViewModel) ZapZoneParticipantFragment.this.mUidBanMap.get(userId);
                if (bmModel == null || !bmModel.isBanRequested.booleanValue()) {
                    ZapZoneParticipantFragment.this.mUidBanMap.put(userId, new BanItemViewModel(removeContainer));
                    removeProgress.setVisibility(4);
                    removeParticipant.setVisibility(0);
                } else {
                    removeProgress.setVisibility(0);
                    removeParticipant.setVisibility(4);
                }
                removeParticipant.setTag(userId);
                removeParticipant.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        String uuid = view.getTag().toString();
                        BanItemViewModel bmModel = (BanItemViewModel) ZapZoneParticipantFragment.this.mUidBanMap.get(uuid);
                        bmModel.isBanRequested = Boolean.valueOf(true);
                        ZapZoneParticipantFragment.this.mUidBanMap.put(uuid, bmModel);
                        ZapZoneParticipantFragment.this.refreshView(uuid);
                        BanParams banParams = new BanParams();
                        banParams.groupId = ZapZoneParticipantFragment.this.mZapZoneId;
                        banParams.userId = uuid;
                        ZapZoneParticipantFragment.this.mGroupManager.banUser(banParams);
                    }
                });
            }
            return layout;
        }

        public void bindView(View view, Context context, Cursor cursor) {
            getBanItemLayout(null, (ViewGroup) view, cursor);
        }

        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return getBanItemLayout(parent, null, cursor);
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        GroupInfo zzInfo = GCSP.getGroupManager().getGroupInfo(null);
        View rootView = inflater.inflate(R.layout.zapzone_participants, container, false);
        ImageView profilePic = (ImageView) rootView.findViewById(R.id.myprofilepic);
        Profile lProfile = ZapProfileSharedPerfs.getProfile();
        if (lProfile != null && !TextUtils.isEmpty(lProfile.pictureUrl)) {
            ZapImageCache.getInstance().getImageLoader().get(lProfile.pictureUrl, ImageLoader.getImageListener(profilePic, R.drawable.ic_default_profile, R.drawable.ic_default_profile));
        }
        if (lProfile != null && !TextUtils.isEmpty(lProfile.nickname)) {
            ((TextView) rootView.findViewById(R.id.mynickname)).setText(lProfile.nickname);
        }
        ListView participantList = (ListView) rootView.findViewById(R.id.participant_list);
        this.mParticipantAdapter = new CustomCursorAdapter(getActivity(), null, 0);
        participantList.setAdapter(this.mParticipantAdapter);
        if (zzInfo != null && !TextUtils.isEmpty(zzInfo.id) && !TextUtils.isEmpty(zzInfo.owner)) {
            this.mZapZoneId = zzInfo.id;
            this.mIsZapZoneOwner = zzInfo.owner.equals(GCSP.getAuthenticator().getAccount());
            getLoaderManager().initLoader(1, null, this);
        }
        this.mContext = getActivity();
        this.mGroupManager.registerGroupListener(this);
        new BanGroupUserObserver(new Handler()).observe();
        return rootView;
    }

    public void onResume() {
        super.onResume();
    }

    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(getActivity(), GroupContract.getMembersContentUri(), Columns.ALL_COLUMNS, GroupContract.SELECTION_ALL_MEMBERS, new String[]{this.mZapZoneId}, null);
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (!cursor.equals(null)) {
            this.mParticipantAdapter.swapCursor(cursor);
        }
    }

    public void onLoaderReset(Loader<Cursor> loader) {
        this.mParticipantAdapter.swapCursor(null);
    }

    public void onGroupEvent(GroupEvent event) {
        if (event.type != Type.GROUP_BAN_USER) {
            return;
        }
        if (event.status == Status.OK) {
            this.mUidBanMap.remove(event.bundle.get(GroupEvent.KEY_BANNED_USER_ID).toString());
        } else if (event.status == Status.ERROR) {
            this.mUidBanMap.remove(event.bundle.get(GroupEvent.KEY_BANNED_USER_ID).toString());
            this.mParticipantAdapter.notifyDataSetChanged();
        } else {
            this.mParticipantAdapter.notifyDataSetChanged();
        }
    }

    /* access modifiers changed from: private */
    public void refreshView(String uuid) {
        BanItemViewModel bmModel = (BanItemViewModel) this.mUidBanMap.get(uuid);
        if (bmModel != null) {
            ImageButton removeParticipant = (ImageButton) bmModel.mFrameLayout.findViewById(R.id.removeParticipant);
            ProgressBar removeProgress = (ProgressBar) bmModel.mFrameLayout.findViewById(R.id.progressBarRemoveParticipant);
            if (bmModel == null || !bmModel.isBanRequested.booleanValue()) {
                removeProgress.setVisibility(4);
                removeParticipant.setVisibility(0);
                return;
            }
            removeProgress.setVisibility(0);
            removeParticipant.setVisibility(4);
        }
    }
}
