package com.google.android.gms.internal;

import android.net.Uri;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Moments;
import com.google.android.gms.plus.Moments.LoadMomentsResult;
import com.google.android.gms.plus.internal.e;
import com.google.android.gms.plus.model.moments.Moment;
import com.google.android.gms.plus.model.moments.MomentBuffer;

public final class ik implements Moments {
    private final com.google.android.gms.common.api.Api.b<e> Rw;

    private static abstract class a extends com.google.android.gms.plus.Plus.a<LoadMomentsResult> {
        a(com.google.android.gms.common.api.Api.b<e> bVar) {
            super(bVar);
        }

        /* renamed from: M */
        public LoadMomentsResult d(final Status status) {
            return new LoadMomentsResult() {
                public MomentBuffer getMomentBuffer() {
                    return null;
                }

                public String getNextPageToken() {
                    return null;
                }

                public Status getStatus() {
                    return status;
                }

                public String getUpdated() {
                    return null;
                }

                public void release() {
                }
            };
        }
    }

    private static abstract class b extends com.google.android.gms.plus.Plus.a<Status> {
        b(com.google.android.gms.common.api.Api.b<e> bVar) {
            super(bVar);
        }

        /* renamed from: f */
        public Status d(Status status) {
            return status;
        }
    }

    private static abstract class c extends com.google.android.gms.plus.Plus.a<Status> {
        c(com.google.android.gms.common.api.Api.b<e> bVar) {
            super(bVar);
        }

        /* renamed from: f */
        public Status d(Status status) {
            return status;
        }
    }

    public ik(com.google.android.gms.common.api.Api.b<e> bVar) {
        this.Rw = bVar;
    }

    public PendingResult<LoadMomentsResult> load(GoogleApiClient googleApiClient) {
        return googleApiClient.a(new a(this.Rw) {
            /* access modifiers changed from: protected */
            public void a(e eVar) {
                eVar.i(this);
            }
        });
    }

    public PendingResult<LoadMomentsResult> load(GoogleApiClient googleApiClient, int maxResults, String pageToken, Uri targetUrl, String type, String userId) {
        final int i = maxResults;
        final String str = pageToken;
        final Uri uri = targetUrl;
        final String str2 = type;
        final String str3 = userId;
        return googleApiClient.a(new a(this.Rw) {
            /* access modifiers changed from: protected */
            public void a(e eVar) {
                eVar.a(this, i, str, uri, str2, str3);
            }
        });
    }

    public PendingResult<Status> remove(GoogleApiClient googleApiClient, final String momentId) {
        return googleApiClient.b(new b(this.Rw) {
            /* access modifiers changed from: protected */
            public void a(e eVar) {
                eVar.removeMoment(momentId);
                b(Status.zQ);
            }
        });
    }

    public PendingResult<Status> write(GoogleApiClient googleApiClient, final Moment moment) {
        return googleApiClient.b(new c(this.Rw) {
            /* access modifiers changed from: protected */
            public void a(e eVar) {
                eVar.a((com.google.android.gms.common.api.a.c<Status>) this, moment);
            }
        });
    }
}
