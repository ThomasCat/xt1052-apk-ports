package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.multiplayer.InvitationBuffer;
import com.google.android.gms.internal.gv;

public final class LoadMatchesResponse {
    private final InvitationBuffer JV;
    private final TurnBasedMatchBuffer JW;
    private final TurnBasedMatchBuffer JX;
    private final TurnBasedMatchBuffer JY;

    public LoadMatchesResponse(Bundle matchData) {
        DataHolder a = a(matchData, 0);
        if (a != null) {
            this.JV = new InvitationBuffer(a);
        } else {
            this.JV = null;
        }
        DataHolder a2 = a(matchData, 1);
        if (a2 != null) {
            this.JW = new TurnBasedMatchBuffer(a2);
        } else {
            this.JW = null;
        }
        DataHolder a3 = a(matchData, 2);
        if (a3 != null) {
            this.JX = new TurnBasedMatchBuffer(a3);
        } else {
            this.JX = null;
        }
        DataHolder a4 = a(matchData, 3);
        if (a4 != null) {
            this.JY = new TurnBasedMatchBuffer(a4);
        } else {
            this.JY = null;
        }
    }

    private static DataHolder a(Bundle bundle, int i) {
        String aW = gv.aW(i);
        if (!bundle.containsKey(aW)) {
            return null;
        }
        return (DataHolder) bundle.getParcelable(aW);
    }

    public void close() {
        if (this.JV != null) {
            this.JV.close();
        }
        if (this.JW != null) {
            this.JW.close();
        }
        if (this.JX != null) {
            this.JX.close();
        }
        if (this.JY != null) {
            this.JY.close();
        }
    }

    public TurnBasedMatchBuffer getCompletedMatches() {
        return this.JY;
    }

    public InvitationBuffer getInvitations() {
        return this.JV;
    }

    public TurnBasedMatchBuffer getMyTurnMatches() {
        return this.JW;
    }

    public TurnBasedMatchBuffer getTheirTurnMatches() {
        return this.JX;
    }

    public boolean hasData() {
        if (this.JV != null && this.JV.getCount() > 0) {
            return true;
        }
        if (this.JW != null && this.JW.getCount() > 0) {
            return true;
        }
        if (this.JX == null || this.JX.getCount() <= 0) {
            return this.JY != null && this.JY.getCount() > 0;
        }
        return true;
    }
}
