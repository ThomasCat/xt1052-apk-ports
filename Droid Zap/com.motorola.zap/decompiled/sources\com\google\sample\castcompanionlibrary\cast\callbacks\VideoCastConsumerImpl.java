package com.google.sample.castcompanionlibrary.cast.callbacks;

import com.google.android.gms.cast.ApplicationMetadata;

public class VideoCastConsumerImpl extends BaseCastConsumerImpl implements IVideoCastConsumer {
    public void onApplicationConnected(ApplicationMetadata appMetadata, String sessionId, boolean wasLaunched) {
    }

    public boolean onApplicationConnectionFailed(int errorCode) {
        return true;
    }

    public void onApplicationStatusChanged(String appStatus) {
    }

    public void onApplicationDisconnected(int errorCode) {
    }

    public void onRemoteMediaPlayerMetadataUpdated() {
    }

    public void onRemoteMediaPlayerStatusUpdated() {
    }

    public void onVolumeChanged(double value, boolean isMute) {
    }

    public void onApplicationStopFailed(int errorCode) {
    }

    public void onRemovedNamespace() {
    }

    public void onDataMessageSendFailed(int errorCode) {
    }

    public void onDataMessageReceived(String message) {
    }
}
