package com.motorola.sharing.provider.cloud;

import android.content.Context;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.ContentShareListener.ContentShareEvent.Substatus;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.instrumentation.ActionResult;
import com.motorola.sharing.instrumentation.ActionResult.Error;
import com.motorola.sharing.instrumentation.CheckinManager;
import com.motorola.sharing.provider.cloud.HttpHelper.UploadResult;
import com.motorola.sharing.util.FileUtils;
import com.motorola.sharing.util.Logger;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

class ContentUpload implements ContentTransferProgressListener {
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getCloudTag("ContentUpload");
    private static final Executor sBigFileExecutor = Executors.newSingleThreadExecutor();
    private static final Executor sRegularFileExecutor = Executors.newFixedThreadPool(2);
    private Callback mCallerCallback;
    /* access modifiers changed from: private */
    public ContentContainer mContainer;
    private Context mContext;
    private boolean mIsStarted;
    private long mShowTransferTakingTooLongDialogTime;
    private boolean mTransferCanceledByTimeout;
    private boolean mTransferEstimateTooLongWarningNotified;
    private boolean mTransferTakingTooLongWarningNotified;

    public interface Callback {
        void onComplete(ContentContainer contentContainer, Status status, Substatus substatus);

        void onProgress(ContentContainer contentContainer, Substatus substatus);
    }

    private class ContentUploadTask implements Runnable {
        private ContentUploadTask() {
        }

        public void run() {
            int i;
            int i2 = 0;
            if (Logger.isDebuggable()) {
                Logger.d(ContentUpload.TAG, "---------------------");
                Logger.d(ContentUpload.TAG, "ContentUploadTask:");
                Logger.d(ContentUpload.TAG, "  - uid: " + ContentUpload.this.mContainer.uid);
                Logger.d(ContentUpload.TAG, "  - userAccount: " + Logger.obfuscate(ContentUpload.this.mContainer.userCred.account));
                Logger.d(ContentUpload.TAG, "  - userToken length: " + (ContentUpload.this.mContainer.userCred.token != null ? ContentUpload.this.mContainer.userCred.token.length() : 0));
                String access$100 = ContentUpload.TAG;
                StringBuilder append = new StringBuilder().append("  - userId length: ");
                if (ContentUpload.this.mContainer.userCred.id != null) {
                    i = ContentUpload.this.mContainer.userCred.id.length();
                } else {
                    i = 0;
                }
                Logger.d(access$100, append.append(i).toString());
                Logger.d(ContentUpload.TAG, "  - mediaPath: " + ContentUpload.this.mContainer.path);
                if (ContentUpload.this.mContainer.tmpPath != null) {
                    Logger.d(ContentUpload.TAG, "  - mediaTmpPath: " + ContentUpload.this.mContainer.tmpPath);
                }
                Logger.d(ContentUpload.TAG, "  - mimeType: " + ContentUpload.this.mContainer.mimeType);
                Logger.d(ContentUpload.TAG, "  - shareId: " + ContentUpload.this.mContainer.shareId);
                Logger.d(ContentUpload.TAG, "  - uploadUrl: " + ContentUpload.this.mContainer.url);
                String access$1002 = ContentUpload.TAG;
                StringBuilder append2 = new StringBuilder().append("  - pin length: ");
                if (ContentUpload.this.mContainer.pin != null) {
                    i2 = ContentUpload.this.mContainer.pin.length();
                }
                Logger.d(access$1002, append2.append(i2).toString());
                if (ContentUpload.this.mContainer.groupInfo != null) {
                    Logger.d(ContentUpload.TAG, "  - groupOwner: " + Logger.obfuscate(ContentUpload.this.mContainer.groupInfo.owner));
                    Logger.d(ContentUpload.TAG, "  - groupId: " + ContentUpload.this.mContainer.groupInfo.id);
                }
                Logger.d(ContentUpload.TAG, "---------------------");
            }
            if (CloudData.isOperationCanceled(ContentUpload.this.mContainer)) {
                Logger.i(ContentUpload.TAG, "do not upload, share was canceled - shareId: " + ContentUpload.this.mContainer.shareId);
                ContentUpload.this.onComplete(Status.SUCCESS, Substatus.INTERRUPTED);
                return;
            }
            ContentUpload.this.uploadContent();
        }
    }

    ContentUpload(ContentContainer container) {
        this.mIsStarted = false;
        this.mContext = null;
        this.mContainer = null;
        this.mCallerCallback = null;
        this.mTransferEstimateTooLongWarningNotified = false;
        this.mShowTransferTakingTooLongDialogTime = 0;
        this.mTransferTakingTooLongWarningNotified = false;
        this.mTransferCanceledByTimeout = false;
        this.mContext = GCSP.getContext();
        this.mContainer = container;
    }

    public void start(Callback callback) {
        synchronized (this) {
            if (this.mIsStarted) {
                throw new IllegalStateException("task already started");
            }
            this.mIsStarted = true;
        }
        this.mCallerCallback = callback;
        if (FileUtils.isBigFile((long) this.mContainer.size)) {
            sBigFileExecutor.execute(new ContentUploadTask());
        } else {
            sRegularFileExecutor.execute(new ContentUploadTask());
        }
    }

    private void onProgress(Substatus substatus) {
        if (this.mCallerCallback != null) {
            this.mCallerCallback.onProgress(this.mContainer, substatus);
        }
    }

    /* access modifiers changed from: private */
    public void onComplete(Status status, Substatus substatus) {
        if (CloudData.isOperationCanceled(this.mContainer)) {
            this.mContainer.actionResult.setSuccess(ActionResult.Status.CANCELED);
        }
        if (this.mCallerCallback != null) {
            this.mCallerCallback.onComplete(this.mContainer, status, substatus);
        }
        CheckinManager.getInstance().logZapEvent(this.mContainer);
    }

    /* access modifiers changed from: private */
    public void uploadContent() {
        UploadResult uploadResult = HttpHelper.uploadContent(this.mContainer, this.mContext, 500, this, false, true);
        if (CloudData.isOperationCanceled(this.mContainer)) {
            Logger.i(TAG, "share was canceled by user");
            onComplete(Status.SUCCESS, Substatus.INTERRUPTED);
        } else if (!uploadResult.success) {
            Logger.e(TAG, "failed to upload");
            if (this.mContainer.actionResult.getStatus() != ActionResult.Status.FAIL) {
                this.mContainer.actionResult.setError(Error.UPLOAD_FAILED);
            }
            onComplete(Status.ERROR, this.mTransferCanceledByTimeout ? Substatus.TRANSFER_TIMEOUT : Substatus.TRANSFER_FAILED);
        } else {
            this.mContainer.actionResult.setSuccess();
            onComplete(Status.SUCCESS, Substatus.SHARED);
        }
    }

    public void onProgressUpdate(ContentContainer container) {
        assertContainer(container);
        if (!CloudData.isOperationCanceled(this.mContainer)) {
            onProgress(Substatus.PROGRESS);
            long now = System.currentTimeMillis();
            if (this.mTransferEstimateTooLongWarningNotified && !this.mTransferTakingTooLongWarningNotified && now > this.mShowTransferTakingTooLongDialogTime && this.mContainer.transferStatus.progress() < 90) {
                onProgress(Substatus.TRANSFER_TAKING_TOO_LONG);
                this.mTransferTakingTooLongWarningNotified = true;
            }
        }
    }

    public void onEstimateLongerThanLimit(ContentContainer container) {
        assertContainer(container);
        if (!CloudData.isOperationCanceled(this.mContainer) && !this.mTransferEstimateTooLongWarningNotified) {
            onProgress(Substatus.ESTIMATED_TRANSFER_TIME_TOO_LONG);
            Logger.d(TAG, "estimated upload is too long for shareId: " + this.mContainer.shareId);
            this.mShowTransferTakingTooLongDialogTime = System.currentTimeMillis() + this.mContainer.transferStatus.estimatedRemainingTime;
            this.mTransferEstimateTooLongWarningNotified = true;
        }
    }

    public void onCancelTransferTakingTooLongNetwork(ContentContainer container) {
        this.mTransferCanceledByTimeout = true;
    }

    public void onCancelTransferTakingTooLongNotReady(ContentContainer container) {
    }

    public void onTakingTooLongNotReady(ContentContainer container) {
    }

    public void onSenderCancelled(ContentContainer container) {
    }

    public void onLowStorage(ContentContainer container) {
    }

    private void assertContainer(ContentContainer container) {
        if (container != this.mContainer) {
            throw new IllegalStateException("wrong container");
        }
    }
}
