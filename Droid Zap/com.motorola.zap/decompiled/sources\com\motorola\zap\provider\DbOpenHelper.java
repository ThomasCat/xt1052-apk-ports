package com.motorola.zap.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.motorola.zap.app.App;

public class DbOpenHelper extends SQLiteOpenHelper {
    private static DbOpenHelper sInstance;
    private SQLiteDatabase mDb;
    private final HistoryHandler mHistoryHandler = new HistoryHandler();

    public static synchronized DbOpenHelper getInstance() {
        DbOpenHelper dbOpenHelper;
        synchronized (DbOpenHelper.class) {
            if (sInstance == null) {
                sInstance = new DbOpenHelper(App.getAppContext());
            }
            dbOpenHelper = sInstance;
        }
        return dbOpenHelper;
    }

    private DbOpenHelper(Context context) {
        super(context, "motozap.db", null, 17);
    }

    public synchronized SQLiteDatabase getDb() {
        if (this.mDb == null) {
            this.mDb = getWritableDatabase();
            init();
        }
        return this.mDb;
    }

    private void init() {
        this.mHistoryHandler.init();
    }

    public void onCreate(SQLiteDatabase db) {
        this.mHistoryHandler.onCreate(db, 17);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        this.mHistoryHandler.onUpgrade(db, oldVersion, newVersion);
    }
}
