package com.motorola.blur.util;

import android.content.ContentResolver;
import android.util.Log;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class CheckinEventWrapper {
    private static final String TAG = CheckinEventWrapper.class.getSimpleName();
    private static Constructor<?> sConstructorCheckinEvent;
    private static Constructor<?> sConstructorCheckinEventTimestamp;
    private static boolean sInitialized;
    private static Method sMethodAddSegment;
    private static Method sMethodGetEventName;
    private static Method sMethodGetTagName;
    private static Method sMethodGetTimestamp;
    private static Method sMethodGetVersion;
    private static Method sMethodPublish;
    private static Method sMethodSerializeEvent;
    private static Method sMethodSetNameValueBoolean;
    private static Method sMethodSetNameValueDouble;
    private static Method sMethodSetNameValueInt;
    private static Method sMethodSetNameValueLong;
    private static Method sMethodSetNameValueString;
    private final Object mReflectedCheckinEvent;

    static {
        sInitialized = false;
        try {
            Class cls = Class.forName("com.motorola.data.event.api.Event");
            Class cls2 = Class.forName("com.motorola.android.provider.CheckinEvent");
            Class cls3 = Class.forName("com.motorola.data.event.api.Segment");
            sConstructorCheckinEvent = cls2.getDeclaredConstructor(new Class[]{String.class, String.class, String.class});
            sConstructorCheckinEventTimestamp = cls2.getDeclaredConstructor(new Class[]{String.class, String.class, String.class, Long.TYPE});
            sMethodAddSegment = cls.getDeclaredMethod("addSegment", new Class[]{cls3});
            sMethodGetEventName = cls.getDeclaredMethod("getEventName", new Class[0]);
            sMethodGetTagName = cls.getDeclaredMethod("getTagName", new Class[0]);
            sMethodGetTimestamp = cls.getDeclaredMethod("getTimestamp", new Class[0]);
            sMethodGetVersion = cls.getDeclaredMethod("getVersion", new Class[0]);
            sMethodSerializeEvent = cls.getDeclaredMethod("serializeEvent", new Class[0]);
            sMethodSetNameValueBoolean = cls.getDeclaredMethod("setValue", new Class[]{String.class, Boolean.TYPE});
            sMethodSetNameValueDouble = cls.getDeclaredMethod("setValue", new Class[]{String.class, Double.TYPE});
            sMethodSetNameValueInt = cls.getDeclaredMethod("setValue", new Class[]{String.class, Integer.TYPE});
            sMethodSetNameValueLong = cls.getDeclaredMethod("setValue", new Class[]{String.class, Long.TYPE});
            sMethodSetNameValueString = cls.getDeclaredMethod("setValue", new Class[]{String.class, String.class});
            sMethodPublish = cls2.getDeclaredMethod("publish", new Class[]{Object.class});
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "Unable to get checkin class.");
            sInitialized = false;
        }
    }

    public CheckinEventWrapper(String tag, String eventName, String version) {
        Object checkinEvent = null;
        if (sInitialized && tag != null && !tag.isEmpty() && eventName != null && !eventName.isEmpty() && version != null && !version.isEmpty()) {
            try {
                checkinEvent = sConstructorCheckinEvent.newInstance(new Object[]{tag, eventName, version});
            } catch (Throwable th) {
                Log.w(TAG, "Unable to instantiate CheckinEvent.");
            }
        }
        this.mReflectedCheckinEvent = checkinEvent;
    }

    public CheckinEventWrapper(String tag, String eventName, String version, long timestamp) {
        Object checkinEvent = null;
        if (sInitialized && tag != null && !tag.isEmpty() && eventName != null && !eventName.isEmpty() && version != null && !version.isEmpty()) {
            try {
                checkinEvent = sConstructorCheckinEventTimestamp.newInstance(new Object[]{tag, eventName, version, Long.valueOf(timestamp)});
            } catch (Throwable th) {
                Log.w(TAG, "Unable to instantiate CheckinEvent.");
            }
        }
        this.mReflectedCheckinEvent = checkinEvent;
    }

    public static boolean isInitialized() {
        return sInitialized;
    }

    public void addSegment(CheckinSegmentWrapper segment) {
        if (sInitialized && this.mReflectedCheckinEvent != null && segment != null) {
            try {
                sMethodAddSegment.invoke(this.mReflectedCheckinEvent, new Object[]{segment.getSegment()});
            } catch (Throwable th) {
                Log.w(TAG, "Unable to add Segment.");
            }
        }
    }

    public String getEventName() {
        String eventName = null;
        if (!sInitialized || this.mReflectedCheckinEvent == null) {
            return eventName;
        }
        try {
            return String.valueOf(sMethodGetEventName.invoke(this.mReflectedCheckinEvent, new Object[0]));
        } catch (Throwable th) {
            Log.w(TAG, "Unable to get event name.");
            return eventName;
        }
    }

    public String getTagName() {
        String tagName = null;
        if (!sInitialized || this.mReflectedCheckinEvent == null) {
            return tagName;
        }
        try {
            return String.valueOf(sMethodGetTagName.invoke(this.mReflectedCheckinEvent, new Object[0]));
        } catch (Throwable th) {
            Log.w(TAG, "Unable to get tag name.");
            return tagName;
        }
    }

    public long getTimestamp() {
        if (!sInitialized || this.mReflectedCheckinEvent == null) {
            return 0;
        }
        try {
            Long val = (Long) sMethodGetTimestamp.invoke(this.mReflectedCheckinEvent, new Object[0]);
            if (val != null) {
                return val.longValue();
            }
            return 0;
        } catch (Throwable th) {
            Log.w(TAG, "Unable to get timestamp.");
            return 0;
        }
    }

    public String getVersion() {
        String version = null;
        if (!sInitialized || this.mReflectedCheckinEvent == null) {
            return version;
        }
        try {
            return String.valueOf(sMethodGetVersion.invoke(this.mReflectedCheckinEvent, new Object[0]));
        } catch (Throwable th) {
            Log.w(TAG, "Unable to get version.");
            return version;
        }
    }

    public StringBuilder serializeEvent() {
        StringBuilder sb = null;
        if (!sInitialized || this.mReflectedCheckinEvent == null) {
            return sb;
        }
        try {
            return (StringBuilder) sMethodSerializeEvent.invoke(this.mReflectedCheckinEvent, new Object[0]);
        } catch (Throwable th) {
            Log.w(TAG, "Unable to serialize event.");
            return sb;
        }
    }

    public void setValue(String name, boolean value) {
        if (sInitialized && this.mReflectedCheckinEvent != null) {
            try {
                sMethodSetNameValueBoolean.invoke(this.mReflectedCheckinEvent, new Object[]{name, Boolean.valueOf(value)});
            } catch (Throwable th) {
                Log.w(TAG, "Unable to set value.");
            }
        }
    }

    public void setValue(String name, double value) {
        if (sInitialized && this.mReflectedCheckinEvent != null) {
            try {
                sMethodSetNameValueDouble.invoke(this.mReflectedCheckinEvent, new Object[]{name, Double.valueOf(value)});
            } catch (Throwable th) {
                Log.w(TAG, "Unable to set value.");
            }
        }
    }

    public void setValue(String name, int value) {
        if (sInitialized && this.mReflectedCheckinEvent != null) {
            try {
                sMethodSetNameValueInt.invoke(this.mReflectedCheckinEvent, new Object[]{name, Integer.valueOf(value)});
            } catch (Throwable th) {
                Log.w(TAG, "Unable to set value.");
            }
        }
    }

    public void setValue(String name, long value) {
        if (sInitialized && this.mReflectedCheckinEvent != null) {
            try {
                sMethodSetNameValueLong.invoke(this.mReflectedCheckinEvent, new Object[]{name, Long.valueOf(value)});
            } catch (Throwable th) {
                Log.w(TAG, "Unable to set value.");
            }
        }
    }

    public void setValue(String name, String value) {
        if (sInitialized && this.mReflectedCheckinEvent != null) {
            if (value == null) {
                value = "null";
            }
            try {
                sMethodSetNameValueString.invoke(this.mReflectedCheckinEvent, new Object[]{name, value});
            } catch (Throwable th) {
                Log.w(TAG, "Unable to set value.");
            }
        }
    }

    public void publish(ContentResolver cr) {
        if (sInitialized && this.mReflectedCheckinEvent != null && cr != null) {
            try {
                sMethodPublish.invoke(this.mReflectedCheckinEvent, new Object[]{cr});
            } catch (Throwable th) {
                Log.w(TAG, "Unable to publish.");
            }
        }
    }

    public static void log(ContentResolver cr, String tag, String eventName, String version, String key1, String value1, String key2, String value2) {
        if (isInitialized() && tag != null && !tag.isEmpty() && eventName != null && !eventName.isEmpty() && version != null && !version.isEmpty()) {
            CheckinEventWrapper eventWrapper = new CheckinEventWrapper(tag, eventName, version, System.currentTimeMillis());
            if (!(key1 == null || value1 == null)) {
                eventWrapper.setValue(key1, value1);
            }
            if (!(key2 == null || value2 == null)) {
                eventWrapper.setValue(key2, value2);
            }
            eventWrapper.publish(cr);
        }
    }
}
