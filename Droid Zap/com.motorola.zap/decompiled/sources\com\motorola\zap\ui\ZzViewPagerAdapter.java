package com.motorola.zap.ui;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.motorola.sharing.util.FileUtils;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.provider.HistoryHandler;
import com.motorola.zap.provider.TCH;
import com.motorola.zap.provider.ZapContract.History;
import com.motorola.zap.provider.ZapContract.History.ZapZoneDetail;
import com.motorola.zap.ui.BitmapCache.BitmapWorkerTask;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ZzViewPagerAdapter extends PagerAdapter {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    public static final String[] DEFAULT_PROJECTION = {HistoryHandler.HIST_ID, HistoryHandler.HIST_FILEPATH};
    private static final String TAG = ZzViewPagerAdapter.class.getSimpleName();
    private final Context mContext;
    private final Executor mExecutor = Executors.newFixedThreadPool(2);
    private String[] mFiles = null;
    private final Handler mHandler = new Handler(Looper.getMainLooper());
    private int mWidth = 0;
    private final String mZzId;
    private final String mZzowner;

    public ZzViewPagerAdapter(Context c, String zz_id, String owner, int width) {
        this.mContext = c;
        this.mZzId = zz_id;
        this.mZzowner = owner;
        if (this.mFiles == null) {
            this.mFiles = loadFilePaths(this.mContext, this.mZzowner, this.mZzId);
        }
        this.mWidth = width;
    }

    public int getCount() {
        return this.mFiles.length;
    }

    public String getItem(int position) {
        return this.mFiles[position];
    }

    public float getPageWidth(int position) {
        if (getCount() == 1) {
            return 1.0f;
        }
        return 0.95f;
    }

    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    /* access modifiers changed from: 0000 */
    public int dipToPix(int dp) {
        return (int) TypedValue.applyDimension(1, (float) dp, this.mContext.getResources().getDisplayMetrics());
    }

    public Object instantiateItem(ViewGroup container, int position) {
        if (DBG) {
            Log.i(TAG, "Inside instantiateItem, position =" + position);
        }
        LinearLayout ll = (LinearLayout) LayoutInflater.from(this.mContext).inflate(R.layout.view_pager_item, null);
        ImageView imageView = (ImageView) ll.findViewById(R.id.imageview_p);
        if (getCount() == 1) {
            imageView.getLayoutParams().width = this.mWidth - dipToPix(2);
        }
        String filePath = this.mFiles[position];
        Bitmap bitmap = BitmapCache.getInstance().get(filePath);
        imageView.setTag(filePath);
        if (bitmap == null) {
            this.mExecutor.execute(new BitmapWorkerTask(this.mHandler, imageView, this.mWidth, dipToPix(200), filePath));
        } else {
            imageView.setImageBitmap(bitmap);
        }
        View dividerview = ll.findViewById(R.id.view_divider_vp);
        dividerview.bringToFront();
        if (position == this.mFiles.length - 1) {
            dividerview.setVisibility(8);
        } else {
            dividerview.setVisibility(0);
        }
        container.addView(ll, 0);
        return ll;
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        if (object instanceof LinearLayout) {
            ((ImageView) ((LinearLayout) object).findViewById(R.id.imageview_p)).setImageBitmap(null);
        }
        ((ViewPager) container).removeView((View) object);
    }

    /* JADX INFO: finally extract failed */
    public static String[] loadFilePaths(Context context, String owner, String zzId) {
        String[] strArr;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(History.CONTENT_URI, DEFAULT_PROJECTION, ZapZoneDetail.SELECTION, new String[]{owner, zzId}, History.INVERSE_CHRONOLOGICAL_ORDER);
            if (cursor == null || cursor.getCount() == 0) {
                strArr = new String[0];
                if (cursor != null) {
                    cursor.close();
                }
            } else {
                int count = cursor.getCount();
                List<String> filePaths = new ArrayList<>(count);
                for (int i = 0; i < count; i++) {
                    cursor.moveToPosition(i);
                    String path = TCH.getFilePath(cursor);
                    if (path != null) {
                        if (!FileUtils.isFileExists(path)) {
                            new HistoryHandler().setDeleted((long) TCH.getId(cursor));
                        } else {
                            filePaths.add(path);
                        }
                    }
                }
                strArr = (String[]) filePaths.toArray(new String[filePaths.size()]);
                if (cursor != null) {
                    cursor.close();
                }
            }
            return strArr;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }
}
