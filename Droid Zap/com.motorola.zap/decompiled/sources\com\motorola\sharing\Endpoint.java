package com.motorola.sharing;

import java.util.HashSet;

public abstract class Endpoint {
    private Attributes mAttributes;

    public enum AttrValue {
        STATIC,
        DYNAMIC,
        CLOUD,
        DIRECT,
        SHARE_ONLY,
        DISCOVER_AND_RECEIVE_ONLY,
        SHARE_AND_RECEIVE,
        WIFI_ONLY,
        MOBILE_ONLY,
        WIFI_AND_MOBILE
    }

    public static class Attributes extends HashSet<AttrValue> {
        public Attributes(AttrValue... attrs) {
            for (AttrValue attr : attrs) {
                add(attr);
            }
        }

        public boolean matches(Attributes attrs) {
            return containsAll(attrs);
        }
    }

    public abstract String getName();

    public Endpoint(Attributes attrs) {
        this.mAttributes = attrs;
    }

    public Attributes getAttributes() {
        return this.mAttributes;
    }
}
