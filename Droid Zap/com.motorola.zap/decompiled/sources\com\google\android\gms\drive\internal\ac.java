package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.drive.events.ChangeEvent;
import com.google.android.gms.drive.events.ConflictEvent;

public class ac implements Creator<OnEventResponse> {
    static void a(OnEventResponse onEventResponse, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.c(parcel, 1, onEventResponse.wj);
        b.c(parcel, 2, onEventResponse.Dm);
        b.a(parcel, 3, (Parcelable) onEventResponse.Eb, i, false);
        b.a(parcel, 4, (Parcelable) onEventResponse.Ec, i, false);
        b.D(parcel, p);
    }

    /* renamed from: Q */
    public OnEventResponse createFromParcel(Parcel parcel) {
        ConflictEvent conflictEvent;
        ChangeEvent changeEvent;
        int i;
        int i2;
        ConflictEvent conflictEvent2 = null;
        int i3 = 0;
        int o = a.o(parcel);
        ChangeEvent changeEvent2 = null;
        int i4 = 0;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    ConflictEvent conflictEvent3 = conflictEvent2;
                    changeEvent = changeEvent2;
                    i = i3;
                    i2 = a.g(parcel, n);
                    conflictEvent = conflictEvent3;
                    break;
                case 2:
                    i2 = i4;
                    ChangeEvent changeEvent3 = changeEvent2;
                    i = a.g(parcel, n);
                    conflictEvent = conflictEvent2;
                    changeEvent = changeEvent3;
                    break;
                case 3:
                    i = i3;
                    i2 = i4;
                    ConflictEvent conflictEvent4 = conflictEvent2;
                    changeEvent = (ChangeEvent) a.a(parcel, n, ChangeEvent.CREATOR);
                    conflictEvent = conflictEvent4;
                    break;
                case 4:
                    conflictEvent = (ConflictEvent) a.a(parcel, n, ConflictEvent.CREATOR);
                    changeEvent = changeEvent2;
                    i = i3;
                    i2 = i4;
                    break;
                default:
                    a.b(parcel, n);
                    conflictEvent = conflictEvent2;
                    changeEvent = changeEvent2;
                    i = i3;
                    i2 = i4;
                    break;
            }
            i4 = i2;
            i3 = i;
            changeEvent2 = changeEvent;
            conflictEvent2 = conflictEvent;
        }
        if (parcel.dataPosition() == o) {
            return new OnEventResponse(i4, i3, changeEvent2, conflictEvent2);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }

    /* renamed from: av */
    public OnEventResponse[] newArray(int i) {
        return new OnEventResponse[i];
    }
}
