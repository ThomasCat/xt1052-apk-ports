package com.motorola.sharing.util;

import android.os.Build;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.provider.cloud.CloudSettings;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonHelper {
    public static JSONObject addCommonRequestFields(JSONObject req) throws JSONException {
        req.put("appId", "123456");
        req.put("fromEmail", GCSP.getAuthenticator().getAccount());
        req.put("fromDeviceId", DeviceUtils.getImei(GCSP.getContext()));
        JSONObject deviceInfo = new JSONObject();
        req.put("deviceInfo", deviceInfo);
        deviceInfo.put("imei", DeviceUtils.getImei(GCSP.getContext()));
        deviceInfo.put("model", DeviceUtils.getDeviceModelInfo());
        deviceInfo.put("serial", Build.SERIAL);
        req.put("appVersion", PackageUtils.getAppVersionCode(GCSP.getContext()));
        req.put("settingsVersion", CloudSettings.SETTINGS_VERSION.get());
        return req;
    }
}
