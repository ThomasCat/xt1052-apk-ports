package com.google.sample.castcompanionlibrary.cast.player;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import com.google.sample.castcompanionlibrary.cast.BaseCastManager;
import com.google.sample.castcompanionlibrary.cast.VideoCastManager;
import com.google.sample.castcompanionlibrary.cast.exceptions.CastException;
import com.google.sample.castcompanionlibrary.cast.exceptions.NoConnectionException;
import com.google.sample.castcompanionlibrary.cast.exceptions.TransientNetworkDisconnectionException;
import com.google.sample.castcompanionlibrary.utils.LogUtils;
import com.google.sample.castcompanionlibrary.utils.Utils;
import com.motorola.zap.R;

public class VideoCastControllerActivity extends ActionBarActivity implements IVideoCastController {
    public static final float DEFAULT_VOLUME_INCREMENT = 0.05f;
    /* access modifiers changed from: private */
    public static final String TAG = LogUtils.makeLogTag(VideoCastControllerActivity.class);
    private VideoCastManager mCastManager;
    private View mControllers;
    private TextView mEnd;
    private TextView mLine1;
    private TextView mLine2;
    /* access modifiers changed from: private */
    public OnVideoCastControllerListener mListener;
    private TextView mLiveText;
    private ProgressBar mLoading;
    private View mPageView;
    private Drawable mPauseDrawable;
    private Drawable mPlayDrawable;
    private ImageView mPlayPause;
    private SeekBar mSeekbar;
    /* access modifiers changed from: private */
    public TextView mStart;
    private Drawable mStopDrawable;
    private int mStreamType;
    private float mVolumeIncrement;
    private VideoCastControllerFragment mediaAuthFragment;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.cast_activity);
        loadAndSetupViews();
        this.mVolumeIncrement = Utils.getFloatFromPreference(this, BaseCastManager.PREFS_KEY_VOLUME_INCREMENT);
        if (this.mVolumeIncrement == Float.MIN_VALUE) {
            this.mVolumeIncrement = 0.05f;
        }
        try {
            this.mCastManager = VideoCastManager.getInstance(this);
        } catch (CastException e) {
        }
        setupActionBar();
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            finish();
            return;
        }
        FragmentManager fm = getSupportFragmentManager();
        this.mediaAuthFragment = (VideoCastControllerFragment) fm.findFragmentByTag("task");
        if (this.mediaAuthFragment == null) {
            this.mediaAuthFragment = VideoCastControllerFragment.newInstance(extras);
            fm.beginTransaction().add((Fragment) this.mediaAuthFragment, "task").commit();
            this.mListener = this.mediaAuthFragment;
            setOnVideoCastControllerChangedListener(this.mListener);
            return;
        }
        this.mListener = this.mediaAuthFragment;
        this.mListener.onConfigurationChanged();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.cast_player_menu, menu);
        this.mCastManager.addMediaRouterButton(menu, R.id.media_route_menu_item);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            finish();
        }
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!this.mCastManager.isConnected()) {
            return super.onKeyDown(keyCode, event);
        }
        if (keyCode == 24) {
            onVolumeChange((double) this.mVolumeIncrement);
        } else if (keyCode != 25) {
            return super.onKeyDown(keyCode, event);
        } else {
            onVolumeChange(-((double) this.mVolumeIncrement));
        }
        if (this.mCastManager.getPlaybackStatus() == 2) {
            return super.onKeyDown(keyCode, event);
        }
        return true;
    }

    private void onVolumeChange(double volumeIncrement) {
        if (this.mCastManager != null) {
            try {
                this.mCastManager.incrementVolume(volumeIncrement);
            } catch (Exception e) {
                LogUtils.LOGE(TAG, "onVolumeChange() Failed to change volume", e);
                Utils.showErrorDialog((Context) this, (int) R.string.failed_setting_volume);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        LogUtils.LOGD(TAG, "onResume() was called");
        try {
            this.mCastManager = VideoCastManager.getInstance(this);
        } catch (CastException e) {
        }
        super.onResume();
    }

    private void loadAndSetupViews() {
        this.mPauseDrawable = getResources().getDrawable(R.drawable.ic_av_pause_dark);
        this.mPlayDrawable = getResources().getDrawable(R.drawable.ic_av_play_dark);
        this.mStopDrawable = getResources().getDrawable(R.drawable.ic_av_stop_dark);
        this.mPageView = findViewById(R.id.pageView);
        this.mPlayPause = (ImageView) findViewById(R.id.imageView1);
        this.mLiveText = (TextView) findViewById(R.id.liveText);
        this.mStart = (TextView) findViewById(R.id.startText);
        this.mEnd = (TextView) findViewById(R.id.endText);
        this.mSeekbar = (SeekBar) findViewById(R.id.seekBar1);
        this.mLine1 = (TextView) findViewById(R.id.textView1);
        this.mLine2 = (TextView) findViewById(R.id.textView2);
        this.mLoading = (ProgressBar) findViewById(R.id.progressBar1);
        this.mControllers = findViewById(R.id.controllers);
        this.mPlayPause.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                try {
                    VideoCastControllerActivity.this.mListener.onPlayPauseClicked(v);
                } catch (TransientNetworkDisconnectionException e) {
                    LogUtils.LOGE(VideoCastControllerActivity.TAG, "Failed to toggle playback due to temporary network issue", e);
                    Utils.showErrorDialog((Context) VideoCastControllerActivity.this, (int) R.string.failed_no_connection_trans);
                } catch (NoConnectionException e2) {
                    LogUtils.LOGE(VideoCastControllerActivity.TAG, "Failed to toggle playback due to network issues", e2);
                    Utils.showErrorDialog((Context) VideoCastControllerActivity.this, (int) R.string.failed_no_connection);
                } catch (Exception e3) {
                    LogUtils.LOGE(VideoCastControllerActivity.TAG, "Failed to toggle playback due to other issues", e3);
                    Utils.showErrorDialog((Context) VideoCastControllerActivity.this, (int) R.string.failed_perform_action);
                }
            }
        });
        this.mSeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    if (VideoCastControllerActivity.this.mListener != null) {
                        VideoCastControllerActivity.this.mListener.onStopTrackingTouch(seekBar);
                    }
                } catch (Exception e) {
                    LogUtils.LOGE(VideoCastControllerActivity.TAG, "Failed to complete seek", e);
                    VideoCastControllerActivity.this.finish();
                }
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                try {
                    if (VideoCastControllerActivity.this.mListener != null) {
                        VideoCastControllerActivity.this.mListener.onStartTrackingTouch(seekBar);
                    }
                } catch (Exception e) {
                    LogUtils.LOGE(VideoCastControllerActivity.TAG, "Failed to start seek", e);
                    VideoCastControllerActivity.this.finish();
                }
            }

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                VideoCastControllerActivity.this.mStart.setText(Utils.formatMillis(progress));
                try {
                    if (VideoCastControllerActivity.this.mListener != null) {
                        VideoCastControllerActivity.this.mListener.onProgressChanged(seekBar, progress, fromUser);
                    }
                } catch (Exception e) {
                    LogUtils.LOGE(VideoCastControllerActivity.TAG, "Failed to set teh progress result", e);
                }
            }
        });
    }

    private void setupActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle((CharSequence) " ");
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_bg_gradient_light));
    }

    public void showLoading(boolean visible) {
        this.mLoading.setVisibility(visible ? 0 : 4);
    }

    public void adjustControllersForLiveStream(boolean isLive) {
        int visibility;
        int i = 0;
        if (isLive) {
            visibility = 4;
        } else {
            visibility = 0;
        }
        TextView textView = this.mLiveText;
        if (!isLive) {
            i = 4;
        }
        textView.setVisibility(i);
        this.mStart.setVisibility(visibility);
        this.mEnd.setVisibility(visibility);
        this.mSeekbar.setVisibility(visibility);
    }

    public void setPlaybackStatus(int state) {
        LogUtils.LOGD(TAG, "setPlaybackStatus(): state = " + state);
        switch (state) {
            case 1:
                this.mLoading.setVisibility(4);
                this.mPlayPause.setImageDrawable(this.mPlayDrawable);
                this.mPlayPause.setVisibility(0);
                this.mLine2.setText(getString(R.string.casting_to_device, new Object[]{this.mCastManager.getDeviceName()}));
                return;
            case 2:
                this.mLoading.setVisibility(4);
                this.mPlayPause.setVisibility(0);
                if (this.mStreamType == 2) {
                    this.mPlayPause.setImageDrawable(this.mStopDrawable);
                } else {
                    this.mPlayPause.setImageDrawable(this.mPauseDrawable);
                }
                this.mLine2.setText(getString(R.string.casting_to_device, new Object[]{this.mCastManager.getDeviceName()}));
                this.mControllers.setVisibility(0);
                return;
            case 3:
                this.mControllers.setVisibility(0);
                this.mLoading.setVisibility(4);
                this.mPlayPause.setVisibility(0);
                this.mPlayPause.setImageDrawable(this.mPlayDrawable);
                this.mLine2.setText(getString(R.string.casting_to_device, new Object[]{this.mCastManager.getDeviceName()}));
                return;
            case 4:
                this.mPlayPause.setVisibility(4);
                this.mLoading.setVisibility(0);
                this.mLine2.setText(getString(R.string.loading));
                return;
            default:
                return;
        }
    }

    public void updateSeekbar(int position, int duration) {
        this.mSeekbar.setProgress(position);
        this.mSeekbar.setMax(duration);
        this.mStart.setText(Utils.formatMillis(position));
        this.mEnd.setText(Utils.formatMillis(duration));
    }

    public void setImage(Bitmap bitmap) {
        if (bitmap == null) {
            return;
        }
        if (this.mPageView instanceof ImageView) {
            ((ImageView) this.mPageView).setImageBitmap(bitmap);
        } else {
            this.mPageView.setBackgroundDrawable(new BitmapDrawable(getResources(), bitmap));
        }
    }

    public void setLine1(String text) {
        this.mLine1.setText(text);
    }

    public void setLine2(String text) {
        this.mLine2.setText(text);
    }

    public void setOnVideoCastControllerChangedListener(OnVideoCastControllerListener listener) {
        if (listener != null) {
            this.mListener = listener;
        }
    }

    public void setStreamType(int streamType) {
        this.mStreamType = streamType;
    }

    public void updateControllersStatus(boolean enabled) {
        boolean z = false;
        this.mControllers.setVisibility(enabled ? 0 : 4);
        if (enabled) {
            if (this.mStreamType == 2) {
                z = true;
            }
            adjustControllersForLiveStream(z);
        }
    }

    public void closeActivity() {
        finish();
    }
}
