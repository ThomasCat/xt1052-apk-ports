package com.google.sample.castcompanionlibrary.cast.dialog.video;

import android.support.v7.app.MediaRouteDialogFactory;

public class VideoMediaRouteDialogFactory extends MediaRouteDialogFactory {
    public VideoMediaRouteControllerDialogFragment onCreateControllerDialogFragment() {
        return new VideoMediaRouteControllerDialogFragment();
    }
}
