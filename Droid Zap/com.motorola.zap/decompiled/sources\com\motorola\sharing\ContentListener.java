package com.motorola.sharing;

public interface ContentListener {

    public static abstract class Event {
        public Status status;

        public enum Status {
            INFO,
            SUCCESS,
            ERROR
        }
    }
}
