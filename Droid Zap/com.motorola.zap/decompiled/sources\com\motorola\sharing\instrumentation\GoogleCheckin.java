package com.motorola.sharing.instrumentation;

import android.content.Context;
import android.text.TextUtils;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GAServiceManager;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Logger.LogLevel;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.StandardExceptionParser;
import com.google.analytics.tracking.android.Tracker;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.Settings;
import com.motorola.sharing.instrumentation.ActionResult.Status;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker.GroupState;
import com.motorola.sharing.util.Logger;
import com.motorola.zap.ZapConstants;
import java.util.Map;

class GoogleCheckin implements CheckinInterface {
    private static final LogLevel GA_LOG_VERBOSITY = LogLevel.INFO;
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getGcspTag("GoogleCheckin");
    private static GoogleCheckin sInstance = null;
    private Context mContext = null;
    /* access modifiers changed from: private */
    public GoogleAnalytics mGa = null;
    private GcspTracker mGcspTracker = null;

    private class GcspTracker {
        private boolean mEnabled;
        private Tracker mGaTracker;
        private String mId;

        private GcspTracker() {
            this.mEnabled = false;
            this.mId = null;
            this.mGaTracker = null;
            init();
        }

        private void init() {
            this.mEnabled = ((Boolean) Settings.GOOGLE_ANALYTICS_ENABLED.get()).booleanValue();
            this.mId = (String) Settings.GOOGLE_ANALYTICS_ID.get();
            if (!this.mEnabled || TextUtils.isEmpty(this.mId)) {
                Logger.i(GoogleCheckin.TAG, "reporting disabled");
                this.mGaTracker = null;
                GoogleCheckin.this.mGa.setAppOptOut(true);
                return;
            }
            Logger.i(GoogleCheckin.TAG, "reporting enabled");
            this.mGaTracker = GoogleCheckin.this.mGa.getTracker(this.mId);
            this.mGaTracker.set(Fields.ANONYMIZE_IP, Boolean.toString(true));
            GoogleCheckin.this.mGa.setAppOptOut(false);
        }

        private boolean needUpdate() {
            return this.mEnabled != ((Boolean) Settings.GOOGLE_ANALYTICS_ENABLED.get()).booleanValue() || !TextUtils.equals(this.mId, (CharSequence) Settings.GOOGLE_ANALYTICS_ID.get());
        }

        /* access modifiers changed from: private */
        public void send(Map<String, String> params) {
            if (needUpdate()) {
                init();
            }
            if (this.mGaTracker != null) {
                this.mGaTracker.send(params);
            }
        }
    }

    public static boolean isSupported() {
        return true;
    }

    public static synchronized GoogleCheckin getInstance() {
        GoogleCheckin googleCheckin;
        synchronized (GoogleCheckin.class) {
            if (sInstance == null) {
                sInstance = new GoogleCheckin(GCSP.getContext());
            }
            googleCheckin = sInstance;
        }
        return googleCheckin;
    }

    private GoogleCheckin(Context context) {
        this.mContext = context;
        this.mGa = GoogleAnalytics.getInstance(this.mContext);
        this.mGa.getLogger().setLogLevel(GA_LOG_VERBOSITY);
        this.mGa.setDryRun(false);
        GAServiceManager.getInstance().setLocalDispatchPeriod(60);
        this.mGcspTracker = new GcspTracker();
    }

    public void logException(Thread thread, Throwable exception, boolean fatal) {
        this.mGcspTracker.send(MapBuilder.createException(new StandardExceptionParser(this.mContext, null).getDescription(thread.getName(), exception), Boolean.valueOf(fatal)).build());
    }

    public void logZapContext(ContentContainer container) {
    }

    public void logZapEvent(ContentContainer container) {
        String privacy;
        Status status = container.actionResult.getStatus();
        if (status == Status.SUCCESS || status == Status.FAIL || status == Status.CANCELED) {
            MapBuilder builder = MapBuilder.createEvent("Zap", container.type.humanReadable(), status.humanReadable(), status == Status.SUCCESS ? Long.valueOf((long) container.size) : null);
            if (container.isGroupRelated()) {
                privacy = "Zone";
            } else if (container.pin != null) {
                privacy = "Private";
            } else {
                privacy = "Public";
            }
            builder.set(Fields.customDimension(3), privacy);
            if (container.isGroupRelated()) {
                builder.set(Fields.customDimension(4), CheckinManager.isAutoProcessing(container) ? "Auto" : "Manual");
            }
            String networkType = getNetworkType(container);
            if (isNotEmpty(networkType)) {
                builder.set(Fields.customDimension(5), networkType);
            }
            builder.set(Fields.customDimension(13), CheckinUtil.getShareType(container));
            builder.set(Fields.customDimension(14), CheckinUtil.getDest(container));
            if (container.locality != null && isNotEmpty(container.locality.provider)) {
                builder.set(Fields.customDimension(6), capitalize(container.locality.provider));
            }
            if (container.locality != null && container.locality.accuracy > 0.0d) {
                builder.set(Fields.customDimension(11), String.format("%.1f", new Object[]{Double.valueOf(container.locality.accuracy)}));
            }
            if (container.locality != null && container.locality.getAge() > 0) {
                builder.set(Fields.customDimension(12), Long.toString(container.locality.getAge() / 1000));
            }
            String modality = getModality(container);
            if (isNotEmpty(modality)) {
                builder.set(Fields.customDimension(7), modality);
            }
            if (isNotEmpty(container.mimeType)) {
                builder.set(Fields.customDimension(8), container.mimeType);
            }
            if (isNotEmpty(container.shareId)) {
                builder.set(Fields.customDimension(10), container.shareId);
            }
            if (status == Status.FAIL) {
                builder.set(Fields.customDimension(1), container.actionResult.getErrorType().humanReadable());
                builder.set(Fields.customDimension(2), container.actionResult.getErrorText());
            }
            this.mGcspTracker.send(builder.build());
        }
    }

    public void logGroupEvent(GroupState info) {
        MapBuilder builder = MapBuilder.createEvent("Zap Zone", info.isStoppedByUser() ? "Leave" : "Expire", info.isUserOwner() ? "Owner" : "Guest", Long.valueOf(info.isUserOwner() ? 1 : 0));
        builder.set(Fields.customDimension(3), TextUtils.isEmpty(info.getPin()) ? "Public" : "Private");
        this.mGcspTracker.send(builder.build());
    }

    public void logZapCancelShare(ContentContainer container) {
        Status status = container.actionResult.getStatus();
        MapBuilder builder = MapBuilder.createEvent("Zap", container.type.humanReadable(), status.humanReadable(), null);
        builder.set(Fields.customDimension(10), container.shareId);
        String networkType = getNetworkType(container);
        if (isNotEmpty(networkType)) {
            builder.set(Fields.customDimension(5), networkType);
        }
        if (status == Status.FAIL) {
            builder.set(Fields.customDimension(1), container.actionResult.getErrorType().humanReadable());
            builder.set(Fields.customDimension(2), container.actionResult.getErrorText());
        }
        this.mGcspTracker.send(builder.build());
    }

    public void logZapAbuse(ContentContainer container) {
        Status status = container.actionResult.getStatus();
        MapBuilder builder = MapBuilder.createEvent("Zap", container.type.humanReadable(), status.humanReadable(), null);
        builder.set(Fields.customDimension(9), container.abuseType.humanReadable());
        if (isNotEmpty(container.shareId)) {
            builder.set(Fields.customDimension(10), container.shareId);
        }
        if (status == Status.FAIL) {
            builder.set(Fields.customDimension(1), container.actionResult.getErrorType().humanReadable());
            builder.set(Fields.customDimension(2), container.actionResult.getErrorText());
        }
        this.mGcspTracker.send(builder.build());
    }

    public void logSettingsSync(Operation operation) {
        Status status = operation.actionResult.getStatus();
        MapBuilder builder = MapBuilder.createEvent("Settings", operation.type.humanReadable(), status.humanReadable(), null);
        if (status == Status.FAIL) {
            builder.set(Fields.customDimension(1), operation.actionResult.getErrorType().humanReadable());
            builder.set(Fields.customDimension(2), operation.actionResult.getErrorText());
        }
        this.mGcspTracker.send(builder.build());
    }

    public void logZapFeatureOptIn(boolean optedIn) {
        this.mGcspTracker.send(MapBuilder.createEvent("Setup", optedIn ? "Opt-in" : "Opt-out", null, null).build());
    }

    public void logZapEnabledEvent(boolean enabled) {
        this.mGcspTracker.send(MapBuilder.createEvent("Settings", "Feature Toggle", enabled ? "On" : "Off", null).build());
    }

    public void logWifiEnabledEvent(boolean enabled) {
        this.mGcspTracker.send(MapBuilder.createEvent("Settings", "WiFi Only Mode", enabled ? "On" : "Off", null).build());
    }

    public void logSettingsLaunchPoint(String launchPoint) {
    }

    public void logShareAppEvent(boolean sendLinkPressed) {
        this.mGcspTracker.send(MapBuilder.createEvent("Settings", "Share App", sendLinkPressed ? "Send Link" : "View", null).build());
    }

    private static String getModality(ContentContainer container) {
        String modality = container.getCheckinData(CheckinInterface.KEY_MODALITY);
        if (isNotEmpty(modality)) {
            return capitalize(modality.replace("MODALITY_", ZapConstants.GOOGLE_ACCOUNT_NONE));
        }
        return modality;
    }

    private static String getNetworkType(ContentContainer container) {
        return capitalize(container.getCheckinData(CheckinInterface.KEY_NETWORK_TYPE));
    }

    private static boolean isNotEmpty(String str) {
        return !TextUtils.isEmpty(str);
    }

    private static String capitalize(String str) {
        if (str != null) {
            return str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
        }
        return null;
    }
}
