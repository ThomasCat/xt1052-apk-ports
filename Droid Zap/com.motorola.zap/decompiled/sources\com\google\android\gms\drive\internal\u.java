package com.google.android.gms.drive.internal;

import android.content.IntentSender;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface u extends IInterface {

    public static abstract class a extends Binder implements u {

        /* renamed from: com.google.android.gms.drive.internal.u$a$a reason: collision with other inner class name */
        private static class C0008a implements u {
            private IBinder ky;

            C0008a(IBinder iBinder) {
                this.ky = iBinder;
            }

            public IntentSender a(CreateFileIntentSenderRequest createFileIntentSenderRequest) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (createFileIntentSenderRequest != null) {
                        obtain.writeInt(1);
                        createFileIntentSenderRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.ky.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (IntentSender) IntentSender.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IntentSender a(OpenFileIntentSenderRequest openFileIntentSenderRequest) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (openFileIntentSenderRequest != null) {
                        obtain.writeInt(1);
                        openFileIntentSenderRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.ky.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (IntentSender) IntentSender.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(AddEventListenerRequest addEventListenerRequest, w wVar, String str, v vVar) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (addEventListenerRequest != null) {
                        obtain.writeInt(1);
                        addEventListenerRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(wVar != null ? wVar.asBinder() : null);
                    obtain.writeString(str);
                    if (vVar != null) {
                        iBinder = vVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.ky.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(AuthorizeAccessRequest authorizeAccessRequest, v vVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (authorizeAccessRequest != null) {
                        obtain.writeInt(1);
                        authorizeAccessRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(vVar != null ? vVar.asBinder() : null);
                    this.ky.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(CloseContentsAndUpdateMetadataRequest closeContentsAndUpdateMetadataRequest, v vVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (closeContentsAndUpdateMetadataRequest != null) {
                        obtain.writeInt(1);
                        closeContentsAndUpdateMetadataRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(vVar != null ? vVar.asBinder() : null);
                    this.ky.transact(18, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(CloseContentsRequest closeContentsRequest, v vVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (closeContentsRequest != null) {
                        obtain.writeInt(1);
                        closeContentsRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(vVar != null ? vVar.asBinder() : null);
                    this.ky.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(CreateContentsRequest createContentsRequest, v vVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (createContentsRequest != null) {
                        obtain.writeInt(1);
                        createContentsRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(vVar != null ? vVar.asBinder() : null);
                    this.ky.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(CreateFileRequest createFileRequest, v vVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (createFileRequest != null) {
                        obtain.writeInt(1);
                        createFileRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(vVar != null ? vVar.asBinder() : null);
                    this.ky.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(CreateFolderRequest createFolderRequest, v vVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (createFolderRequest != null) {
                        obtain.writeInt(1);
                        createFolderRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(vVar != null ? vVar.asBinder() : null);
                    this.ky.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(DisconnectRequest disconnectRequest) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (disconnectRequest != null) {
                        obtain.writeInt(1);
                        disconnectRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.ky.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(GetMetadataRequest getMetadataRequest, v vVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (getMetadataRequest != null) {
                        obtain.writeInt(1);
                        getMetadataRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(vVar != null ? vVar.asBinder() : null);
                    this.ky.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ListParentsRequest listParentsRequest, v vVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (listParentsRequest != null) {
                        obtain.writeInt(1);
                        listParentsRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(vVar != null ? vVar.asBinder() : null);
                    this.ky.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(OpenContentsRequest openContentsRequest, v vVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (openContentsRequest != null) {
                        obtain.writeInt(1);
                        openContentsRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(vVar != null ? vVar.asBinder() : null);
                    this.ky.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(QueryRequest queryRequest, v vVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (queryRequest != null) {
                        obtain.writeInt(1);
                        queryRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(vVar != null ? vVar.asBinder() : null);
                    this.ky.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(RemoveEventListenerRequest removeEventListenerRequest, w wVar, String str, v vVar) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (removeEventListenerRequest != null) {
                        obtain.writeInt(1);
                        removeEventListenerRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(wVar != null ? wVar.asBinder() : null);
                    obtain.writeString(str);
                    if (vVar != null) {
                        iBinder = vVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.ky.transact(15, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(TrashResourceRequest trashResourceRequest, v vVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (trashResourceRequest != null) {
                        obtain.writeInt(1);
                        trashResourceRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(vVar != null ? vVar.asBinder() : null);
                    this.ky.transact(17, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(UpdateMetadataRequest updateMetadataRequest, v vVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (updateMetadataRequest != null) {
                        obtain.writeInt(1);
                        updateMetadataRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(vVar != null ? vVar.asBinder() : null);
                    this.ky.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(v vVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    obtain.writeStrongBinder(vVar != null ? vVar.asBinder() : null);
                    this.ky.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.ky;
            }
        }

        public static u D(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.drive.internal.IDriveService");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof u)) ? new C0008a(iBinder) : (u) queryLocalInterface;
        }

        /* JADX WARNING: type inference failed for: r0v0 */
        /* JADX WARNING: type inference failed for: r0v1, types: [com.google.android.gms.drive.internal.CloseContentsAndUpdateMetadataRequest] */
        /* JADX WARNING: type inference failed for: r0v5, types: [com.google.android.gms.drive.internal.CloseContentsAndUpdateMetadataRequest] */
        /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.drive.internal.TrashResourceRequest] */
        /* JADX WARNING: type inference failed for: r0v10, types: [com.google.android.gms.drive.internal.TrashResourceRequest] */
        /* JADX WARNING: type inference failed for: r0v11, types: [com.google.android.gms.drive.internal.DisconnectRequest] */
        /* JADX WARNING: type inference failed for: r0v15, types: [com.google.android.gms.drive.internal.DisconnectRequest] */
        /* JADX WARNING: type inference failed for: r0v16, types: [com.google.android.gms.drive.internal.RemoveEventListenerRequest] */
        /* JADX WARNING: type inference failed for: r0v20, types: [com.google.android.gms.drive.internal.RemoveEventListenerRequest] */
        /* JADX WARNING: type inference failed for: r0v21, types: [com.google.android.gms.drive.internal.AddEventListenerRequest] */
        /* JADX WARNING: type inference failed for: r0v25, types: [com.google.android.gms.drive.internal.AddEventListenerRequest] */
        /* JADX WARNING: type inference failed for: r0v26, types: [com.google.android.gms.drive.internal.ListParentsRequest] */
        /* JADX WARNING: type inference failed for: r0v30, types: [com.google.android.gms.drive.internal.ListParentsRequest] */
        /* JADX WARNING: type inference failed for: r0v31, types: [com.google.android.gms.drive.internal.AuthorizeAccessRequest] */
        /* JADX WARNING: type inference failed for: r0v35, types: [com.google.android.gms.drive.internal.AuthorizeAccessRequest] */
        /* JADX WARNING: type inference failed for: r0v36, types: [com.google.android.gms.drive.internal.CreateFileIntentSenderRequest] */
        /* JADX WARNING: type inference failed for: r0v41, types: [com.google.android.gms.drive.internal.CreateFileIntentSenderRequest] */
        /* JADX WARNING: type inference failed for: r0v42, types: [com.google.android.gms.drive.internal.OpenFileIntentSenderRequest] */
        /* JADX WARNING: type inference failed for: r0v47, types: [com.google.android.gms.drive.internal.OpenFileIntentSenderRequest] */
        /* JADX WARNING: type inference failed for: r0v52, types: [com.google.android.gms.drive.internal.CloseContentsRequest] */
        /* JADX WARNING: type inference failed for: r0v56, types: [com.google.android.gms.drive.internal.CloseContentsRequest] */
        /* JADX WARNING: type inference failed for: r0v57, types: [com.google.android.gms.drive.internal.OpenContentsRequest] */
        /* JADX WARNING: type inference failed for: r0v61, types: [com.google.android.gms.drive.internal.OpenContentsRequest] */
        /* JADX WARNING: type inference failed for: r0v62, types: [com.google.android.gms.drive.internal.CreateFolderRequest] */
        /* JADX WARNING: type inference failed for: r0v66, types: [com.google.android.gms.drive.internal.CreateFolderRequest] */
        /* JADX WARNING: type inference failed for: r0v67, types: [com.google.android.gms.drive.internal.CreateFileRequest] */
        /* JADX WARNING: type inference failed for: r0v71, types: [com.google.android.gms.drive.internal.CreateFileRequest] */
        /* JADX WARNING: type inference failed for: r0v72, types: [com.google.android.gms.drive.internal.CreateContentsRequest] */
        /* JADX WARNING: type inference failed for: r0v76, types: [com.google.android.gms.drive.internal.CreateContentsRequest] */
        /* JADX WARNING: type inference failed for: r0v77, types: [com.google.android.gms.drive.internal.UpdateMetadataRequest] */
        /* JADX WARNING: type inference failed for: r0v81, types: [com.google.android.gms.drive.internal.UpdateMetadataRequest] */
        /* JADX WARNING: type inference failed for: r0v82, types: [com.google.android.gms.drive.internal.QueryRequest] */
        /* JADX WARNING: type inference failed for: r0v86, types: [com.google.android.gms.drive.internal.QueryRequest] */
        /* JADX WARNING: type inference failed for: r0v87, types: [com.google.android.gms.drive.internal.GetMetadataRequest] */
        /* JADX WARNING: type inference failed for: r0v91, types: [com.google.android.gms.drive.internal.GetMetadataRequest] */
        /* JADX WARNING: type inference failed for: r0v95 */
        /* JADX WARNING: type inference failed for: r0v96 */
        /* JADX WARNING: type inference failed for: r0v97 */
        /* JADX WARNING: type inference failed for: r0v98 */
        /* JADX WARNING: type inference failed for: r0v99 */
        /* JADX WARNING: type inference failed for: r0v100 */
        /* JADX WARNING: type inference failed for: r0v101 */
        /* JADX WARNING: type inference failed for: r0v102 */
        /* JADX WARNING: type inference failed for: r0v103 */
        /* JADX WARNING: type inference failed for: r0v104 */
        /* JADX WARNING: type inference failed for: r0v105 */
        /* JADX WARNING: type inference failed for: r0v106 */
        /* JADX WARNING: type inference failed for: r0v107 */
        /* JADX WARNING: type inference failed for: r0v108 */
        /* JADX WARNING: type inference failed for: r0v109 */
        /* JADX WARNING: type inference failed for: r0v110 */
        /* JADX WARNING: type inference failed for: r0v111 */
        /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.drive.internal.TrashResourceRequest, com.google.android.gms.drive.internal.CloseContentsAndUpdateMetadataRequest, com.google.android.gms.drive.internal.DisconnectRequest, com.google.android.gms.drive.internal.RemoveEventListenerRequest, com.google.android.gms.drive.internal.AddEventListenerRequest, com.google.android.gms.drive.internal.ListParentsRequest, com.google.android.gms.drive.internal.AuthorizeAccessRequest, com.google.android.gms.drive.internal.CreateFileIntentSenderRequest, com.google.android.gms.drive.internal.OpenFileIntentSenderRequest, com.google.android.gms.drive.internal.CloseContentsRequest, com.google.android.gms.drive.internal.OpenContentsRequest, com.google.android.gms.drive.internal.CreateFolderRequest, com.google.android.gms.drive.internal.CreateFileRequest, com.google.android.gms.drive.internal.CreateContentsRequest, com.google.android.gms.drive.internal.UpdateMetadataRequest, com.google.android.gms.drive.internal.QueryRequest, com.google.android.gms.drive.internal.GetMetadataRequest]
  uses: [com.google.android.gms.drive.internal.CloseContentsAndUpdateMetadataRequest, com.google.android.gms.drive.internal.TrashResourceRequest, com.google.android.gms.drive.internal.DisconnectRequest, com.google.android.gms.drive.internal.RemoveEventListenerRequest, com.google.android.gms.drive.internal.AddEventListenerRequest, com.google.android.gms.drive.internal.ListParentsRequest, com.google.android.gms.drive.internal.AuthorizeAccessRequest, com.google.android.gms.drive.internal.CreateFileIntentSenderRequest, com.google.android.gms.drive.internal.OpenFileIntentSenderRequest, com.google.android.gms.drive.internal.CloseContentsRequest, com.google.android.gms.drive.internal.OpenContentsRequest, com.google.android.gms.drive.internal.CreateFolderRequest, com.google.android.gms.drive.internal.CreateFileRequest, com.google.android.gms.drive.internal.CreateContentsRequest, com.google.android.gms.drive.internal.UpdateMetadataRequest, com.google.android.gms.drive.internal.QueryRequest, com.google.android.gms.drive.internal.GetMetadataRequest]
  mth insns count: 243
        	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
        	at jadx.core.ProcessClass.process(ProcessClass.java:30)
        	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
        	at jadx.core.ProcessClass.process(ProcessClass.java:35)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
         */
        /* JADX WARNING: Unknown variable types count: 18 */
        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            ? r0 = 0;
            switch (code) {
                case 1:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (GetMetadataRequest) GetMetadataRequest.CREATOR.createFromParcel(data);
                    }
                    a((GetMetadataRequest) r0, com.google.android.gms.drive.internal.v.a.E(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (QueryRequest) QueryRequest.CREATOR.createFromParcel(data);
                    }
                    a((QueryRequest) r0, com.google.android.gms.drive.internal.v.a.E(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (UpdateMetadataRequest) UpdateMetadataRequest.CREATOR.createFromParcel(data);
                    }
                    a((UpdateMetadataRequest) r0, com.google.android.gms.drive.internal.v.a.E(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 4:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (CreateContentsRequest) CreateContentsRequest.CREATOR.createFromParcel(data);
                    }
                    a((CreateContentsRequest) r0, com.google.android.gms.drive.internal.v.a.E(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 5:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (CreateFileRequest) CreateFileRequest.CREATOR.createFromParcel(data);
                    }
                    a((CreateFileRequest) r0, com.google.android.gms.drive.internal.v.a.E(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 6:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (CreateFolderRequest) CreateFolderRequest.CREATOR.createFromParcel(data);
                    }
                    a((CreateFolderRequest) r0, com.google.android.gms.drive.internal.v.a.E(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 7:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (OpenContentsRequest) OpenContentsRequest.CREATOR.createFromParcel(data);
                    }
                    a((OpenContentsRequest) r0, com.google.android.gms.drive.internal.v.a.E(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 8:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (CloseContentsRequest) CloseContentsRequest.CREATOR.createFromParcel(data);
                    }
                    a((CloseContentsRequest) r0, com.google.android.gms.drive.internal.v.a.E(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 9:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    a(com.google.android.gms.drive.internal.v.a.E(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 10:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (OpenFileIntentSenderRequest) OpenFileIntentSenderRequest.CREATOR.createFromParcel(data);
                    }
                    IntentSender a = a((OpenFileIntentSenderRequest) r0);
                    reply.writeNoException();
                    if (a != null) {
                        reply.writeInt(1);
                        a.writeToParcel(reply, 1);
                    } else {
                        reply.writeInt(0);
                    }
                    return true;
                case 11:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (CreateFileIntentSenderRequest) CreateFileIntentSenderRequest.CREATOR.createFromParcel(data);
                    }
                    IntentSender a2 = a((CreateFileIntentSenderRequest) r0);
                    reply.writeNoException();
                    if (a2 != null) {
                        reply.writeInt(1);
                        a2.writeToParcel(reply, 1);
                    } else {
                        reply.writeInt(0);
                    }
                    return true;
                case 12:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (AuthorizeAccessRequest) AuthorizeAccessRequest.CREATOR.createFromParcel(data);
                    }
                    a((AuthorizeAccessRequest) r0, com.google.android.gms.drive.internal.v.a.E(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 13:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (ListParentsRequest) ListParentsRequest.CREATOR.createFromParcel(data);
                    }
                    a((ListParentsRequest) r0, com.google.android.gms.drive.internal.v.a.E(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 14:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (AddEventListenerRequest) AddEventListenerRequest.CREATOR.createFromParcel(data);
                    }
                    a((AddEventListenerRequest) r0, com.google.android.gms.drive.internal.w.a.F(data.readStrongBinder()), data.readString(), com.google.android.gms.drive.internal.v.a.E(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 15:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (RemoveEventListenerRequest) RemoveEventListenerRequest.CREATOR.createFromParcel(data);
                    }
                    a((RemoveEventListenerRequest) r0, com.google.android.gms.drive.internal.w.a.F(data.readStrongBinder()), data.readString(), com.google.android.gms.drive.internal.v.a.E(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 16:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (DisconnectRequest) DisconnectRequest.CREATOR.createFromParcel(data);
                    }
                    a((DisconnectRequest) r0);
                    reply.writeNoException();
                    return true;
                case 17:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (TrashResourceRequest) TrashResourceRequest.CREATOR.createFromParcel(data);
                    }
                    a((TrashResourceRequest) r0, com.google.android.gms.drive.internal.v.a.E(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 18:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (CloseContentsAndUpdateMetadataRequest) CloseContentsAndUpdateMetadataRequest.CREATOR.createFromParcel(data);
                    }
                    a((CloseContentsAndUpdateMetadataRequest) r0, com.google.android.gms.drive.internal.v.a.E(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.google.android.gms.drive.internal.IDriveService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    IntentSender a(CreateFileIntentSenderRequest createFileIntentSenderRequest) throws RemoteException;

    IntentSender a(OpenFileIntentSenderRequest openFileIntentSenderRequest) throws RemoteException;

    void a(AddEventListenerRequest addEventListenerRequest, w wVar, String str, v vVar) throws RemoteException;

    void a(AuthorizeAccessRequest authorizeAccessRequest, v vVar) throws RemoteException;

    void a(CloseContentsAndUpdateMetadataRequest closeContentsAndUpdateMetadataRequest, v vVar) throws RemoteException;

    void a(CloseContentsRequest closeContentsRequest, v vVar) throws RemoteException;

    void a(CreateContentsRequest createContentsRequest, v vVar) throws RemoteException;

    void a(CreateFileRequest createFileRequest, v vVar) throws RemoteException;

    void a(CreateFolderRequest createFolderRequest, v vVar) throws RemoteException;

    void a(DisconnectRequest disconnectRequest) throws RemoteException;

    void a(GetMetadataRequest getMetadataRequest, v vVar) throws RemoteException;

    void a(ListParentsRequest listParentsRequest, v vVar) throws RemoteException;

    void a(OpenContentsRequest openContentsRequest, v vVar) throws RemoteException;

    void a(QueryRequest queryRequest, v vVar) throws RemoteException;

    void a(RemoveEventListenerRequest removeEventListenerRequest, w wVar, String str, v vVar) throws RemoteException;

    void a(TrashResourceRequest trashResourceRequest, v vVar) throws RemoteException;

    void a(UpdateMetadataRequest updateMetadataRequest, v vVar) throws RemoteException;

    void a(v vVar) throws RemoteException;
}
