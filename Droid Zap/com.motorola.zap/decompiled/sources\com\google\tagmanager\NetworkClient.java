package com.google.tagmanager;

import java.io.IOException;
import java.io.InputStream;

interface NetworkClient {
    void close();

    InputStream getInputStream(String str) throws IOException;
}
