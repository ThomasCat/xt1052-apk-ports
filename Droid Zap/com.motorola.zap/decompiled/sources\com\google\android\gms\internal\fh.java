package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.internal.fb.a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class fh extends fb implements SafeParcelable {
    public static final fi CREATOR = new fi();
    private final fe CC;
    private final Parcel CJ;
    private final int CK;
    private int CL;
    private int CM;
    private final String mClassName;
    private final int wj;

    fh(int i, Parcel parcel, fe feVar) {
        this.wj = i;
        this.CJ = (Parcel) er.f(parcel);
        this.CK = 2;
        this.CC = feVar;
        if (this.CC == null) {
            this.mClassName = null;
        } else {
            this.mClassName = this.CC.eD();
        }
        this.CL = 2;
    }

    private fh(SafeParcelable safeParcelable, fe feVar, String str) {
        this.wj = 1;
        this.CJ = Parcel.obtain();
        safeParcelable.writeToParcel(this.CJ, 0);
        this.CK = 1;
        this.CC = (fe) er.f(feVar);
        this.mClassName = (String) er.f(str);
        this.CL = 2;
    }

    public static <T extends fb & SafeParcelable> fh a(T t) {
        String canonicalName = t.getClass().getCanonicalName();
        return new fh((SafeParcelable) t, b(t), canonicalName);
    }

    private static void a(fe feVar, fb fbVar) {
        Class cls = fbVar.getClass();
        if (!feVar.b(cls)) {
            HashMap en = fbVar.en();
            feVar.a(cls, fbVar.en());
            for (String str : en.keySet()) {
                a aVar = (a) en.get(str);
                Class ev = aVar.ev();
                if (ev != null) {
                    try {
                        a(feVar, (fb) ev.newInstance());
                    } catch (InstantiationException e) {
                        throw new IllegalStateException("Could not instantiate an object of type " + aVar.ev().getCanonicalName(), e);
                    } catch (IllegalAccessException e2) {
                        throw new IllegalStateException("Could not access object of type " + aVar.ev().getCanonicalName(), e2);
                    }
                }
            }
        }
    }

    private void a(StringBuilder sb, int i, Object obj) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                sb.append(obj);
                return;
            case 7:
                sb.append("\"").append(fp.ap(obj.toString())).append("\"");
                return;
            case 8:
                sb.append("\"").append(fk.d((byte[]) obj)).append("\"");
                return;
            case 9:
                sb.append("\"").append(fk.e((byte[]) obj));
                sb.append("\"");
                return;
            case 10:
                fq.a(sb, (HashMap) obj);
                return;
            case 11:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                throw new IllegalArgumentException("Unknown type = " + i);
        }
    }

    private void a(StringBuilder sb, a<?, ?> aVar, Parcel parcel, int i) {
        switch (aVar.em()) {
            case 0:
                b(sb, aVar, a(aVar, Integer.valueOf(com.google.android.gms.common.internal.safeparcel.a.g(parcel, i))));
                return;
            case 1:
                b(sb, aVar, a(aVar, com.google.android.gms.common.internal.safeparcel.a.i(parcel, i)));
                return;
            case 2:
                b(sb, aVar, a(aVar, Long.valueOf(com.google.android.gms.common.internal.safeparcel.a.h(parcel, i))));
                return;
            case 3:
                b(sb, aVar, a(aVar, Float.valueOf(com.google.android.gms.common.internal.safeparcel.a.j(parcel, i))));
                return;
            case 4:
                b(sb, aVar, a(aVar, Double.valueOf(com.google.android.gms.common.internal.safeparcel.a.k(parcel, i))));
                return;
            case 5:
                b(sb, aVar, a(aVar, com.google.android.gms.common.internal.safeparcel.a.l(parcel, i)));
                return;
            case 6:
                b(sb, aVar, a(aVar, Boolean.valueOf(com.google.android.gms.common.internal.safeparcel.a.c(parcel, i))));
                return;
            case 7:
                b(sb, aVar, a(aVar, com.google.android.gms.common.internal.safeparcel.a.m(parcel, i)));
                return;
            case 8:
            case 9:
                b(sb, aVar, a(aVar, com.google.android.gms.common.internal.safeparcel.a.p(parcel, i)));
                return;
            case 10:
                b(sb, aVar, a(aVar, c(com.google.android.gms.common.internal.safeparcel.a.o(parcel, i))));
                return;
            case 11:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                throw new IllegalArgumentException("Unknown field out type = " + aVar.em());
        }
    }

    private void a(StringBuilder sb, String str, a<?, ?> aVar, Parcel parcel, int i) {
        sb.append("\"").append(str).append("\":");
        if (aVar.ex()) {
            a(sb, aVar, parcel, i);
        } else {
            b(sb, aVar, parcel, i);
        }
    }

    private void a(StringBuilder sb, HashMap<String, a<?, ?>> hashMap, Parcel parcel) {
        HashMap c = c(hashMap);
        sb.append('{');
        int o = com.google.android.gms.common.internal.safeparcel.a.o(parcel);
        boolean z = false;
        while (parcel.dataPosition() < o) {
            int n = com.google.android.gms.common.internal.safeparcel.a.n(parcel);
            Entry entry = (Entry) c.get(Integer.valueOf(com.google.android.gms.common.internal.safeparcel.a.S(n)));
            if (entry != null) {
                if (z) {
                    sb.append(",");
                }
                a(sb, (String) entry.getKey(), (a) entry.getValue(), parcel, n);
                z = true;
            }
        }
        if (parcel.dataPosition() != o) {
            throw new C0006a("Overread allowed size end=" + o, parcel);
        }
        sb.append('}');
    }

    private static fe b(fb fbVar) {
        fe feVar = new fe(fbVar.getClass());
        a(feVar, fbVar);
        feVar.eB();
        feVar.eA();
        return feVar;
    }

    private void b(StringBuilder sb, a<?, ?> aVar, Parcel parcel, int i) {
        if (aVar.es()) {
            sb.append("[");
            switch (aVar.em()) {
                case 0:
                    fj.a(sb, com.google.android.gms.common.internal.safeparcel.a.r(parcel, i));
                    break;
                case 1:
                    fj.a(sb, (T[]) com.google.android.gms.common.internal.safeparcel.a.t(parcel, i));
                    break;
                case 2:
                    fj.a(sb, com.google.android.gms.common.internal.safeparcel.a.s(parcel, i));
                    break;
                case 3:
                    fj.a(sb, com.google.android.gms.common.internal.safeparcel.a.u(parcel, i));
                    break;
                case 4:
                    fj.a(sb, com.google.android.gms.common.internal.safeparcel.a.v(parcel, i));
                    break;
                case 5:
                    fj.a(sb, (T[]) com.google.android.gms.common.internal.safeparcel.a.w(parcel, i));
                    break;
                case 6:
                    fj.a(sb, com.google.android.gms.common.internal.safeparcel.a.q(parcel, i));
                    break;
                case 7:
                    fj.a(sb, com.google.android.gms.common.internal.safeparcel.a.x(parcel, i));
                    break;
                case 8:
                case 9:
                case 10:
                    throw new UnsupportedOperationException("List of type BASE64, BASE64_URL_SAFE, or STRING_MAP is not supported");
                case 11:
                    Parcel[] A = com.google.android.gms.common.internal.safeparcel.a.A(parcel, i);
                    int length = A.length;
                    for (int i2 = 0; i2 < length; i2++) {
                        if (i2 > 0) {
                            sb.append(",");
                        }
                        A[i2].setDataPosition(0);
                        a(sb, aVar.ez(), A[i2]);
                    }
                    break;
                default:
                    throw new IllegalStateException("Unknown field type out.");
            }
            sb.append("]");
            return;
        }
        switch (aVar.em()) {
            case 0:
                sb.append(com.google.android.gms.common.internal.safeparcel.a.g(parcel, i));
                return;
            case 1:
                sb.append(com.google.android.gms.common.internal.safeparcel.a.i(parcel, i));
                return;
            case 2:
                sb.append(com.google.android.gms.common.internal.safeparcel.a.h(parcel, i));
                return;
            case 3:
                sb.append(com.google.android.gms.common.internal.safeparcel.a.j(parcel, i));
                return;
            case 4:
                sb.append(com.google.android.gms.common.internal.safeparcel.a.k(parcel, i));
                return;
            case 5:
                sb.append(com.google.android.gms.common.internal.safeparcel.a.l(parcel, i));
                return;
            case 6:
                sb.append(com.google.android.gms.common.internal.safeparcel.a.c(parcel, i));
                return;
            case 7:
                sb.append("\"").append(fp.ap(com.google.android.gms.common.internal.safeparcel.a.m(parcel, i))).append("\"");
                return;
            case 8:
                sb.append("\"").append(fk.d(com.google.android.gms.common.internal.safeparcel.a.p(parcel, i))).append("\"");
                return;
            case 9:
                sb.append("\"").append(fk.e(com.google.android.gms.common.internal.safeparcel.a.p(parcel, i)));
                sb.append("\"");
                return;
            case 10:
                Bundle o = com.google.android.gms.common.internal.safeparcel.a.o(parcel, i);
                Set<String> keySet = o.keySet();
                keySet.size();
                sb.append("{");
                boolean z = true;
                for (String str : keySet) {
                    if (!z) {
                        sb.append(",");
                    }
                    sb.append("\"").append(str).append("\"");
                    sb.append(":");
                    sb.append("\"").append(fp.ap(o.getString(str))).append("\"");
                    z = false;
                }
                sb.append("}");
                return;
            case 11:
                Parcel z2 = com.google.android.gms.common.internal.safeparcel.a.z(parcel, i);
                z2.setDataPosition(0);
                a(sb, aVar.ez(), z2);
                return;
            default:
                throw new IllegalStateException("Unknown field type out");
        }
    }

    private void b(StringBuilder sb, a<?, ?> aVar, Object obj) {
        if (aVar.er()) {
            b(sb, aVar, (ArrayList) obj);
        } else {
            a(sb, aVar.el(), obj);
        }
    }

    private void b(StringBuilder sb, a<?, ?> aVar, ArrayList<?> arrayList) {
        sb.append("[");
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                sb.append(",");
            }
            a(sb, aVar.el(), arrayList.get(i));
        }
        sb.append("]");
    }

    public static HashMap<String, String> c(Bundle bundle) {
        HashMap<String, String> hashMap = new HashMap<>();
        for (String str : bundle.keySet()) {
            hashMap.put(str, bundle.getString(str));
        }
        return hashMap;
    }

    private static HashMap<Integer, Entry<String, a<?, ?>>> c(HashMap<String, a<?, ?>> hashMap) {
        HashMap<Integer, Entry<String, a<?, ?>>> hashMap2 = new HashMap<>();
        for (Entry entry : hashMap.entrySet()) {
            hashMap2.put(Integer.valueOf(((a) entry.getValue()).eu()), entry);
        }
        return hashMap2;
    }

    /* access modifiers changed from: protected */
    public Object ak(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    /* access modifiers changed from: protected */
    public boolean al(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    public int describeContents() {
        fi fiVar = CREATOR;
        return 0;
    }

    public Parcel eF() {
        switch (this.CL) {
            case 0:
                this.CM = b.p(this.CJ);
                b.D(this.CJ, this.CM);
                this.CL = 2;
                break;
            case 1:
                b.D(this.CJ, this.CM);
                this.CL = 2;
                break;
        }
        return this.CJ;
    }

    /* access modifiers changed from: 0000 */
    public fe eG() {
        switch (this.CK) {
            case 0:
                return null;
            case 1:
                return this.CC;
            case 2:
                return this.CC;
            default:
                throw new IllegalStateException("Invalid creation type: " + this.CK);
        }
    }

    public HashMap<String, a<?, ?>> en() {
        if (this.CC == null) {
            return null;
        }
        return this.CC.ao(this.mClassName);
    }

    public int getVersionCode() {
        return this.wj;
    }

    public String toString() {
        er.b(this.CC, (Object) "Cannot convert to JSON on client side.");
        Parcel eF = eF();
        eF.setDataPosition(0);
        StringBuilder sb = new StringBuilder(100);
        a(sb, this.CC.ao(this.mClassName), eF);
        return sb.toString();
    }

    public void writeToParcel(Parcel out, int flags) {
        fi fiVar = CREATOR;
        fi.a(this, out, flags);
    }
}
