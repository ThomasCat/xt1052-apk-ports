package com.motorola.sharing;

import android.os.Bundle;
import com.motorola.sharing.GroupManager.GroupInfo;

public interface GroupListener {

    public static class GroupEvent {
        public static final String KEY_BANNED_USER_ID = "com.motorola.sharing.KEY_BANNED_USER_ID";
        public static final String KEY_RETRY_INTENT = "com.motorola.sharing.KEY_RETRY_INTENT";
        public static final String KEY_USER_PROFILE = "com.motorola.sharing.KEY_USER_PROFILE";
        public Bundle bundle;
        public GroupInfo groupInfo;
        public Status status;
        public Substatus subStatus;
        public Type type;

        public enum Status {
            INFO,
            OK,
            ERROR
        }

        public enum Substatus {
            NO_ERROR,
            CANCELED,
            NETWORK_ERROR,
            AUTHENTICATION_ERROR,
            CLIENT_ERROR,
            SERVER_ERROR,
            GROUP_INACTIVE,
            NOT_ALLOWED,
            WRONG_PIN,
            USER_DISABLED,
            USER_BLOCKED
        }

        public enum Type {
            GROUP_CREATE,
            GROUP_INVITE_SHARE,
            GROUP_JOIN,
            GROUP_LEAVE,
            GROUP_INVITE_RECEIVE,
            GROUP_EXPIRE,
            GROUP_SYNC,
            GROUP_UPDATE,
            GROUP_ACTIVE,
            GROUP_MODIFIED,
            GROUP_INACTIVE,
            GROUP_BAN_USER
        }
    }

    void onGroupEvent(GroupEvent groupEvent);
}
