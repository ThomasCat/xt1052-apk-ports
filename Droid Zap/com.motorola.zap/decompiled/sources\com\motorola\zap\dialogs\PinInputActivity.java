package com.motorola.zap.dialogs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.app.ActionReceiver;
import com.motorola.zap.app.ZzInviteStore;
import com.motorola.zap.zz.ui.ZzLocationQueue;

public class PinInputActivity extends Activity {
    private int mZzInviteUid;

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        int uid = intent.getIntExtra(ZapConstants.EXTRA_ZZ_INVITE_UID, -1);
        if (uid != this.mZzInviteUid) {
            this.mZzInviteUid = uid;
            initContentView();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mZzInviteUid = getIntent().getIntExtra(ZapConstants.EXTRA_ZZ_INVITE_UID, -1);
        initContentView();
    }

    private void initContentView() {
        if (this.mZzInviteUid == -1) {
            setContentView(R.layout.number_pin_input_dialog);
        } else {
            setContentView(R.layout.number_pin_input_dialog);
        }
        getWindow().setSoftInputMode(4);
        ((Button) findViewById(R.id.cancel)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                PinInputActivity.this.finish();
            }
        });
        final TextView pinText = (TextView) findViewById(R.id.pin_input_text);
        final Button ok = (Button) findViewById(R.id.ok);
        ok.setEnabled(false);
        ok.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                PinInputActivity.this.handleInput(pinText);
            }
        });
        pinText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (s.length() < PinInputActivity.this.getMinPinSize()) {
                    ok.setEnabled(false);
                } else {
                    ok.setEnabled(true);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        pinText.setOnEditorActionListener(new OnEditorActionListener() {
            public boolean onEditorAction(TextView t, int actionId, KeyEvent event) {
                if (actionId != 6 || t.getText().length() < PinInputActivity.this.getMinPinSize()) {
                    return false;
                }
                PinInputActivity.this.handleInput(t);
                return true;
            }
        });
    }

    /* access modifiers changed from: private */
    public int getMinPinSize() {
        if (this.mZzInviteUid == -1) {
            return ((Integer) Z.PIN_SIZE.read()).intValue();
        }
        return 1;
    }

    /* access modifiers changed from: private */
    public void handleInput(TextView textView) {
        String obj = textView.getText().toString();
        if (this.mZzInviteUid != -1) {
            ZzLocationQueue.getInstance().zzJoin(ZzInviteStore.get(this.mZzInviteUid).getJoinParams(obj, false));
        } else {
            Intent intent = new Intent(ZapConstants.ACTION_PIN_INPUT);
            intent.setClass(getApplicationContext(), ActionReceiver.class);
            intent.putExtra(ZapConstants.EXTRA_PIN, obj);
            intent.putExtra(ZapConstants.EXTRA_RECOVER_USER_ACTION_INTENT, true);
            sendBroadcast(intent, "com.motorola.blast.permission.TRIGGER_BLAST_ACTION");
        }
        finish();
    }
}
