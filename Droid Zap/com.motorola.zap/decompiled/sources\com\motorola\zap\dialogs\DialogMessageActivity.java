package com.motorola.zap.dialogs;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.ZapConstants;

public class DialogMessageActivity extends Activity {
    /* access modifiers changed from: private */
    public static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private Dialog mDialog = null;
    private DismissReceiver mDismissReceiver = null;
    private String mMessage = null;
    private String mTitle = null;

    private class DismissReceiver extends BroadcastReceiver {
        private DismissReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (ZapConstants.ACTION_FINISH_DIALOG_ACTIVITY.equals(intent.getAction())) {
                if (DialogMessageActivity.DBG) {
                    Log.d("Zap-DialogMsgActvt", "Dismiss dialog intent");
                }
                DialogMessageActivity.this.finish();
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (DBG) {
            Log.d("Zap-DialogMsgActvt", "onCreate");
        }
        registerDismissReceiver();
        handleIntent(getIntent());
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (DBG) {
            Log.d("Zap-DialogMsgActvt", "onNewIntent");
        }
        handleIntent(getIntent());
    }

    public void onDestroy() {
        if (DBG) {
            Log.d("Zap-DialogMsgActvt", "onDestroy");
        }
        dismissDialog();
        unregisterDismissReceiver();
        super.onDestroy();
    }

    private void handleIntent(Intent intent) {
        if (intent == null) {
            finish();
            return;
        }
        this.mTitle = intent.getStringExtra(ZapConstants.EXTRA_DIALOG_TITLE);
        this.mMessage = intent.getStringExtra(ZapConstants.EXTRA_DIALOG_MESSAGE);
        if (TextUtils.isEmpty(this.mMessage)) {
            if (DBG) {
                Log.d("Zap-DialogMsgActvt", "Empty message is not allowed");
            }
            finish();
            return;
        }
        if (this.mMessage.equals(getString(R.string.dialog_msg_no_accurate_location)) || this.mMessage.equals(getString(R.string.dialog_msg_google_auth_error)) || this.mMessage.equals(getString(R.string.dialog_msg_google_auth_error_server)) || this.mMessage.equals(getString(R.string.dialog_msg_server_error)) || this.mMessage.equals(getString(R.string.dialog_msg_network_error))) {
            sendBroadcast(new Intent(ZapConstants.ACTION_SWYPE_RECEIVE_FAILED), "com.motorola.blast.permission.TRIGGER_BLAST_ACTION");
        }
        if (this.mMessage.equals(getString(R.string.toast_error_cannot_send_from_this_device))) {
            showDeviceLimitationDialog(R.string.toast_error_cannot_send_from_this_device);
        } else if (this.mMessage.equals(getString(R.string.toast_error_cannot_create_zone_from_this_device))) {
            showDeviceLimitationDialog(R.string.toast_error_cannot_create_zone_from_this_device);
        } else {
            showDialog();
        }
    }

    private void registerDismissReceiver() {
        IntentFilter filter = new IntentFilter(ZapConstants.ACTION_FINISH_DIALOG_ACTIVITY);
        this.mDismissReceiver = new DismissReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(this.mDismissReceiver, filter);
    }

    private void unregisterDismissReceiver() {
        if (this.mDismissReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(this.mDismissReceiver);
            this.mDismissReceiver = null;
        }
    }

    private void showDialog() {
        dismissDialog();
        this.mDialog = new Builder(this).setTitle(this.mTitle).setMessage(this.mMessage).setPositiveButton(getString(R.string.dialog_ok_button), new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (DialogMessageActivity.DBG) {
                    Log.d("Zap-DialogMsgActvt", "onClick");
                }
                DialogMessageActivity.this.finish();
            }
        }).setOnCancelListener(new OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                if (DialogMessageActivity.DBG) {
                    Log.d("Zap-DialogMsgActvt", "onCancel");
                }
                DialogMessageActivity.this.finish();
            }
        }).show();
        this.mDialog.setCanceledOnTouchOutside(false);
    }

    private void showDeviceLimitationDialog(int res) {
        dismissDialog();
        View msgView = getLayoutInflater().inflate(R.layout.limitation_message, null);
        ((TextView) msgView.findViewById(R.id.message)).setText(getResources().getString(res) + getResources().getString(R.string.droid_link));
        this.mDialog = new Builder(this).setTitle(this.mTitle).setView(msgView).setPositiveButton(getString(R.string.dialog_ok_button), new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (DialogMessageActivity.DBG) {
                    Log.d("Zap-DialogMsgActvt", "onClick");
                }
                DialogMessageActivity.this.finish();
            }
        }).setOnCancelListener(new OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                if (DialogMessageActivity.DBG) {
                    Log.d("Zap-DialogMsgActvt", "onCancel");
                }
                DialogMessageActivity.this.finish();
            }
        }).show();
        this.mDialog.setCanceledOnTouchOutside(false);
    }

    private void dismissDialog() {
        if (this.mDialog != null) {
            this.mDialog.dismiss();
            this.mDialog = null;
        }
    }
}
