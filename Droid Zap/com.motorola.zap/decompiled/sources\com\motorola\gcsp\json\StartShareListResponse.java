package com.motorola.gcsp.json;

public class StartShareListResponse {
    public int abuseAction;
    public GetTargetInfoResponseList[] activeTargets;
    public boolean cidRequired;
    public boolean settingsUpdateRequired;
    public String shareId;
    public long shareMetaTTL;
    public String status;
    public long time;
    public int uploadLimit;
    public String uploadUrl;
}
