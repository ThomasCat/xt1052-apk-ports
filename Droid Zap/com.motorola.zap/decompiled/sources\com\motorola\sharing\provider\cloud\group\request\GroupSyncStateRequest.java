package com.motorola.sharing.provider.cloud.group.request;

import android.content.Intent;
import android.text.TextUtils;
import com.motorola.gcsp.json.StatusCode;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener.GroupEvent.Substatus;
import com.motorola.sharing.GroupListener.GroupEvent.Type;
import com.motorola.sharing.provider.cloud.GroupReceiver;
import com.motorola.sharing.provider.cloud.HttpHelper;
import com.motorola.sharing.provider.cloud.UrlHelper;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker.GroupState;
import com.motorola.sharing.util.HistoryManager.Shares.C;
import com.motorola.sharing.util.JsonHelper;
import com.motorola.sharing.util.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GroupSyncStateRequest extends GroupRequest {
    private static final String TAG = Logger.getCloudTag("GroupSyncStateReq");

    public GroupSyncStateRequest() {
        super(GCSP.getContext(), false);
    }

    public Type getWhat() {
        return Type.GROUP_SYNC;
    }

    public Substatus checkPreconditions() {
        if (!isNetworkConnected()) {
            return Substatus.NETWORK_ERROR;
        }
        return super.checkPreconditions();
    }

    public Intent getRetryIntent() {
        Intent intent = new Intent(GroupReceiver.ACTION_GROUP_SYNC_STATE);
        if (isRetry()) {
            return null;
        }
        return intent;
    }

    public void run() {
        String response;
        Logger.d(TAG, String.format("Running request, retry: %b", new Object[]{Boolean.valueOf(isRetry())}));
        if (!authenticate()) {
            Logger.e(TAG, "Request failed - Authentication error");
            sendError(Substatus.AUTHENTICATION_ERROR);
            return;
        }
        String requestData = createRequestData();
        if (requestData == null) {
            Logger.e(TAG, "Failed to construct request");
            sendError(Substatus.CLIENT_ERROR);
            return;
        }
        try {
            Logger.d(TAG, "Sending request to server...");
            response = HttpHelper.sendPostRequest(UrlHelper.getTargetInfo(), requestData, this.mUserToken, this.mUserId, true);
        } catch (Exception e) {
            response = null;
            Logger.e(TAG, "Exception sending request to server: " + e.toString());
        }
        if (response == null) {
            Logger.e(TAG, "Request to server failed");
            sendError(Substatus.NETWORK_ERROR);
            return;
        }
        handleServerResponse(response);
    }

    private String createRequestData() {
        JSONObject jsonObj = new JSONObject();
        try {
            JsonHelper.addCommonRequestFields(jsonObj);
            jsonObj.put("targetType", "group");
            jsonObj.put("membersRequired", true);
            jsonObj.put("thumbnailRequired", true);
            jsonObj.put("targetStatus", "active");
            jsonObj.put("targetMemberStatus", "active");
            Logger.d(TAG, "Server Request:");
            Logger.dump(TAG, jsonObj);
            return jsonObj.toString();
        } catch (JSONException e) {
            Logger.e(TAG, "ERROR generating group JSON request");
            return null;
        }
    }

    private void handleServerResponse(String response) {
        StatusCode status = StatusCode.OK;
        String owner = null;
        String id = null;
        String name = null;
        String pin = null;
        String thumbnail = null;
        try {
            JSONArray jsonArray = new JSONArray(response);
            Logger.d(TAG, "Server Response:");
            Logger.dump(TAG, jsonArray);
            JSONObject responseJson = jsonArray.getJSONObject(0);
            if (responseJson.has("status")) {
                status = StatusCode.codeOf(responseJson.getString("status"));
            }
            if (responseJson.has("targetId")) {
                id = responseJson.getString("targetId");
            }
            if (responseJson.has("targetMeta")) {
                JSONObject targetMeta = responseJson.getJSONObject("targetMeta");
                owner = targetMeta.getString("targetOwner");
                name = targetMeta.getString("targetName");
                if (targetMeta.has(C.PIN)) {
                    pin = targetMeta.getString(C.PIN);
                }
                if (targetMeta.has("targetThumbnail")) {
                    thumbnail = targetMeta.getString("targetThumbnail");
                }
            }
            switch (status) {
                case OK:
                case ERR_012:
                    GroupState groupState = this.mGroupStateTracker.getState();
                    if (TextUtils.equals(groupState.getOwner(), owner) && TextUtils.equals(groupState.getId(), id)) {
                        Logger.d(TAG, "Group state hasn't changed: " + (TextUtils.isEmpty(id) ? "not active" : "active - gid: " + id));
                        return;
                    } else if (TextUtils.isEmpty(id)) {
                        onGroupStopped();
                        Logger.d(TAG, "Group left - gid: " + id);
                        sendOk();
                        return;
                    } else if (!GroupStateTracker.isValidGroup(owner, id)) {
                        Logger.e(TAG, "Missing mandatory param: owner: " + Logger.obfuscate(owner) + "; gid: " + id);
                        sendError(Substatus.SERVER_ERROR);
                        return;
                    } else {
                        onGroupStarted(owner, id, name, pin, thumbnail, null);
                        Logger.d(TAG, "Group restored by server info - gid: " + id);
                        sendOk();
                        return;
                    }
                default:
                    Logger.e(TAG, "Request failed with status: " + status);
                    sendError(statusToError(status));
                    return;
            }
        } catch (JSONException e) {
            Logger.e(TAG, "Exception parsing response JSON.", e);
            sendError(Substatus.SERVER_ERROR);
        }
    }
}
