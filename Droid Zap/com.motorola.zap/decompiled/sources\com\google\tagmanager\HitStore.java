package com.google.tagmanager;

interface HitStore {
    void dispatch();

    void putHit(long j, String str);
}
