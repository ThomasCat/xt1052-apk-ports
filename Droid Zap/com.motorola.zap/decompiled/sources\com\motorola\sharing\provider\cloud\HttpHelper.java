package com.motorola.sharing.provider.cloud;

import android.content.Context;
import android.os.SystemClock;
import android.text.TextUtils;
import com.motorola.gcsp.json.StatusCode;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentContainer.TransferStatus;
import com.motorola.sharing.instrumentation.ActionResult.Error;
import com.motorola.sharing.util.FileUtils;
import com.motorola.sharing.util.Logger;
import com.motorola.sharing.util.NetworkUtils;
import com.motorola.zap.ZapConstants;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.util.Date;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONException;
import org.json.JSONObject;

public class HttpHelper {
    private static final String TAG = Logger.getCloudTag("HttpHelper");
    private static int sNumRegularFileUploadsInProgress = 0;
    private static SecureRandom sRandom = null;

    protected interface ContentTransferProgressListener {
        void onCancelTransferTakingTooLongNetwork(ContentContainer contentContainer);

        void onCancelTransferTakingTooLongNotReady(ContentContainer contentContainer);

        void onEstimateLongerThanLimit(ContentContainer contentContainer);

        void onLowStorage(ContentContainer contentContainer);

        void onProgressUpdate(ContentContainer contentContainer);

        void onSenderCancelled(ContentContainer contentContainer);

        void onTakingTooLongNotReady(ContentContainer contentContainer);
    }

    private static class DownloadInfo {
        String filePath;
        DownloadStatus status;

        public DownloadInfo(DownloadStatus status2, String filePath2) {
            this.status = status2;
            this.filePath = filePath2;
        }
    }

    private enum DownloadStatus {
        SUCCESS,
        CANCELED,
        RETRY_NOT_READY,
        RETRY_ERROR,
        RETRY_NO_WIFI,
        FAILED
    }

    public static class UploadResult {
        public String response;
        public boolean success;

        private UploadResult(boolean success2, String response2) {
            this.success = success2;
            this.response = response2;
        }
    }

    private HttpHelper() {
    }

    public static String sendGetRequest(String url, String authToken, String userId, boolean retryIfError) throws UnknownHostException, ConnectionException {
        int numAttempts;
        if (retryIfError) {
            numAttempts = 1;
        } else {
            numAttempts = 10;
        }
        while (true) {
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
            HttpConnectionParams.setSoTimeout(client.getParams(), 30000);
            HttpGet request = new HttpGet(url);
            if (authToken != null) {
                request.addHeader("Authorization", "OAuth " + authToken + "||" + userId);
            }
            request.addHeader("x-api-version", "1.4");
            try {
                if (Logger.isDebuggable()) {
                    URI uri = request.getURI();
                    Logger.d(TAG, "sending GET request to server... (" + (uri.getScheme() + "://" + uri.getHost() + uri.getPath()) + ")");
                }
                HttpResponse response = client.execute(request);
                Logger.d(TAG, "response received");
                int httpStatus = response.getStatusLine().getStatusCode();
                if (httpStatus == 200) {
                    String responseString = getResponseString(response);
                    if (client != null) {
                        client.getConnectionManager().shutdown();
                    }
                    return responseString;
                }
                Logger.d(TAG, "status: " + httpStatus + "; body: " + getResponseString(response));
                numAttempts += 4;
                Logger.e(TAG, "request failed: " + httpStatus);
                if (numAttempts >= 10) {
                    Logger.e(TAG, "all retries failed: could not execute request");
                    throw new ConnectionException("all retries failed", httpStatus);
                }
                if (client != null) {
                    client.getConnectionManager().shutdown();
                }
                numAttempts++;
                Logger.i(TAG, "retrying in 3 seconds...");
                SystemClock.sleep(3000);
            } catch (UnknownHostException e) {
                Logger.e(TAG, "network connection error", e);
                if (numAttempts >= 10) {
                    Logger.e(TAG, "all retries failed", e);
                    throw e;
                } else if (client != null) {
                    client.getConnectionManager().shutdown();
                }
            } catch (Exception e2) {
                if (e2 instanceof ConnectionException) {
                    throw ((ConnectionException) e2);
                }
                Logger.e(TAG, "request failed", e2);
                if (numAttempts >= 10) {
                    Logger.e(TAG, "all retries failed: could not execute request");
                    throw new ConnectionException(e2.toString(), -1);
                } else if (client != null) {
                    client.getConnectionManager().shutdown();
                }
            } catch (Throwable th) {
                if (client != null) {
                    client.getConnectionManager().shutdown();
                }
                throw th;
            }
        }
    }

    public static String sendPostRequest(String url, String body, String authToken, String userId, boolean retryIfError) throws UnknownHostException, ConnectionException {
        int numAttempts = retryIfError ? 1 : 10;
        while (true) {
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
            HttpConnectionParams.setSoTimeout(client.getParams(), 30000);
            HttpPost request = new HttpPost(url);
            if (authToken != null) {
                request.addHeader("Authorization", "OAuth " + authToken + "||" + userId);
            }
            request.addHeader("x-api-version", "1.4");
            try {
                request.setEntity(new StringEntity(body, "UTF-8"));
                try {
                    if (Logger.isDebuggable()) {
                        URI uri = request.getURI();
                        Logger.d(TAG, "sending POST request to server... (" + (uri.getScheme() + "://" + uri.getHost() + uri.getPath()) + ")");
                    }
                    HttpResponse response = client.execute(request);
                    Logger.d(TAG, "response received");
                    int httpStatus = response.getStatusLine().getStatusCode();
                    if (httpStatus == 200) {
                        String responseString = getResponseString(response);
                        if (client != null) {
                            client.getConnectionManager().shutdown();
                        }
                        return responseString;
                    }
                    numAttempts += 4;
                    Logger.e(TAG, "request failed: " + httpStatus);
                    if (numAttempts >= 10) {
                        Logger.e(TAG, "all retries failed: could not execute request");
                        throw new ConnectionException("all retries failed", httpStatus);
                    }
                    if (client != null) {
                        client.getConnectionManager().shutdown();
                    }
                    numAttempts++;
                    Logger.i(TAG, "retrying in 3 seconds...");
                    SystemClock.sleep(3000);
                } catch (UnknownHostException e) {
                    Logger.e(TAG, "network connection error", e);
                    if (numAttempts >= 10) {
                        Logger.e(TAG, "all retries failed", e);
                        throw e;
                    } else if (client != null) {
                        client.getConnectionManager().shutdown();
                    }
                } catch (Exception e2) {
                    if (e2 instanceof ConnectionException) {
                        throw ((ConnectionException) e2);
                    }
                    Logger.e(TAG, "request failed", e2);
                    if (numAttempts >= 10) {
                        Logger.e(TAG, "all retries failed: could not execute request");
                        throw new ConnectionException(e2.toString(), -1);
                    } else if (client != null) {
                        client.getConnectionManager().shutdown();
                    }
                } catch (Throwable th) {
                    if (client != null) {
                        client.getConnectionManager().shutdown();
                    }
                    throw th;
                }
            } catch (UnsupportedEncodingException encodingEx) {
                Logger.e(TAG, "unsupported encoding on payload: " + encodingEx.toString());
                throw new ConnectionException(encodingEx.toString(), -1);
            }
        }
    }

    public static UploadResult uploadContent(ContentContainer container, Context context, long updateProgressInterval, ContentTransferProgressListener progressListener, boolean isReportAbuse, boolean retryIfError) {
        String path;
        if (container.transferStatus == null || container.transferStatus.isCompleted()) {
            Logger.i(TAG, "uploading: " + container.mimeType + " ...");
            Logger.d(TAG, "upload " + container.path + " => " + container.url);
            boolean useTemp = !TextUtils.isEmpty(container.tmpPath);
            if (useTemp) {
                Logger.d(TAG, "upload temp file: " + container.tmpPath);
                path = container.tmpPath;
            } else {
                path = container.path;
            }
            int size = FileUtils.getFileSize(path);
            if (size <= 0) {
                Logger.e(TAG, "file doesn't exist: " + path);
                return new UploadResult(false, null);
            }
            boolean isBigFile = FileUtils.isBigFile((long) size);
            if (!isBigFile) {
                sNumRegularFileUploadsInProgress++;
            }
            container.transferStatus = new TransferStatus(size);
            Logger.i(TAG, "upload file size: " + container.transferStatus.totalBytes);
            Logger.d(TAG, "upload started: " + new Date(container.transferStatus.startTime));
            UploadResult uploadFile = uploadFile(container, context, useTemp ? container.tmpPath : container.path, updateProgressInterval, progressListener, isReportAbuse, retryIfError);
            container.transferStatus.stopTime = now();
            if (!isBigFile) {
                sNumRegularFileUploadsInProgress--;
            }
            if (!useTemp) {
                return uploadFile;
            }
            Logger.d(TAG, "deleting temp file: " + container.tmpPath);
            FileUtils.deleteFile(container.tmpPath, context);
            return uploadFile;
        }
        Logger.e(TAG, "upload is still in progress");
        return new UploadResult(false, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:124:?, code lost:
        com.motorola.sharing.util.Logger.d(TAG, "file bytes sent");
        r0.writeBytes("\r\n");
        r0.writeBytes("--" + r16 + "--\r\n");
        r0.flush();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x0568, code lost:
        if (com.motorola.sharing.provider.cloud.CloudData.isOperationCanceled(r49) == false) goto L_0x05a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x056a, code lost:
        com.motorola.sharing.util.Logger.i(TAG, "share was canceled: " + r49.shareId);
        r9 = new com.motorola.sharing.provider.cloud.HttpHelper.UploadResult(false, null, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x058e, code lost:
        if (r48 == null) goto L_0x0593;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x0590, code lost:
        r48.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x0593, code lost:
        if (r0 == null) goto L_0x0598;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:?, code lost:
        r48.setReadTimeout(60000);
        r27 = r48.getResponseCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x05b7, code lost:
        if (r27 != 200) goto L_0x0625;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x05b9, code lost:
        com.motorola.sharing.util.Logger.i(TAG, "upload DONE: " + r49.shareId);
        com.motorola.sharing.util.Logger.d(TAG, "total upload time: " + ((now() - r5) / 1000) + " secs");
        r28 = r48.getInputStream();
        r9 = new com.motorola.sharing.provider.cloud.HttpHelper.UploadResult(true, getStringFromInputStream(r28), null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x060c, code lost:
        if (r48 == null) goto L_0x0611;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x060e, code lost:
        r48.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x0611, code lost:
        if (r0 == null) goto L_0x0616;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:?, code lost:
        com.motorola.sharing.util.Logger.e(TAG, "could not upload data, status: " + r27);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x063f, code lost:
        if (r48 == null) goto L_0x0644;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x0641, code lost:
        r48.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x0644, code lost:
        if (r0 == null) goto L_0x0649;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0519  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x051e A[SYNTHETIC, Splitter:B:113:0x051e] */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0523 A[SYNTHETIC, Splitter:B:116:0x0523] */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x0528 A[SYNTHETIC, Splitter:B:119:0x0528] */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x059f A[SYNTHETIC, Splitter:B:136:0x059f] */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x061d A[SYNTHETIC, Splitter:B:153:0x061d] */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x0650 A[SYNTHETIC, Splitter:B:167:0x0650] */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x065b  */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x0660 A[SYNTHETIC, Splitter:B:174:0x0660] */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x0665 A[SYNTHETIC, Splitter:B:177:0x0665] */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x066a A[SYNTHETIC, Splitter:B:180:0x066a] */
    /* JADX WARNING: Removed duplicated region for block: B:216:0x00be A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:217:0x00be A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:229:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:230:? A[RETURN, SYNTHETIC] */
    private static UploadResult uploadFile(ContentContainer container, Context context, String filePath, long updateProgressInterval, ContentTransferProgressListener progressListener, boolean isReportAbuse, boolean retryIfError) {
        InputStream inputStream;
        FileInputStream fileInputStream;
        UploadResult uploadResult;
        InputStream inputStream2;
        UploadResult uploadResult2;
        String boundary = "---------------------------" + randomString() + randomString() + randomString();
        String mimeType = container.mimeType;
        boolean isBigFile = FileUtils.isBigFile((long) container.transferStatus.totalBytes);
        int chunkSize = isBigFile ? 20480 : 10240;
        Logger.d(TAG, "upload chunk size: " + (chunkSize / 1024) + "K");
        byte[] buffer = new byte[chunkSize];
        long lastProgressUpdateTime = 0;
        long transferCancelTime = isBigFile ? ZapConstants.LOCATION_MAX_AGE_MODALITY_NONE : 600000;
        long transfEstimateWarnTime = isBigFile ? 420000 : ZapConstants.LOCATION_MAX_AGE_NO_MODALITY_SUPPORT;
        long transfEstimateWindowSize = isBigFile ? 120000 : 60000;
        container.transferStatus.maxAllowedTime = transferCancelTime;
        Error error = null;
        int httpStatusCode = 0;
        int maxAttempts = retryIfError ? 10 : 1;
        int numAttempts = 0;
        while (numAttempts < maxAttempts) {
            if (numAttempts > 0) {
                error = null;
                httpStatusCode = 0;
                Logger.i(TAG, "retrying in 3 seconds...");
                SystemClock.sleep(3000);
            }
            if (!((Boolean) CloudSettings.WIFI_ONLY_CONTENT_TRANSFER.get()).booleanValue() || NetworkUtils.isWifiConnected(context)) {
                String uploadType = isReportAbuse ? "1" : "0";
                Logger.d(TAG, "upload type: " + uploadType);
                HttpURLConnection urlConnection = null;
                DataOutputStream dataOutStream = null;
                FileInputStream fileInputStream2 = null;
                inputStream = null;
                try {
                    long uploadStartTime = now();
                    long lastStatusVerificationTime = uploadStartTime;
                    fileInputStream = new FileInputStream(filePath);
                    try {
                        URL url = new URL(container.url);
                        urlConnection = (HttpURLConnection) url.openConnection();
                        urlConnection.setDoOutput(true);
                        urlConnection.setDoInput(true);
                        urlConnection.setRequestProperty("x-api-version", "1.4");
                        urlConnection.setRequestProperty("Connection", "close");
                        if (container.userCred.token != null) {
                            urlConnection.addRequestProperty("Authorization", "OAuth " + container.userCred.token + "||" + container.userCred.id);
                        }
                        urlConnection.setConnectTimeout(10000);
                        urlConnection.setReadTimeout(30000);
                        urlConnection.setRequestMethod("POST");
                        urlConnection.setChunkedStreamingMode(chunkSize);
                        urlConnection.addRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
                        DataOutputStream dataOutputStream = new DataOutputStream(urlConnection.getOutputStream());
                        try {
                            dataOutputStream.writeBytes("--" + boundary + "\r\n");
                            dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + container.userCred.account + "/" + container.shareId + "/" + uploadType + "\"; filename=\"" + mimeType.substring(0, mimeType.indexOf("/")) + "." + FileUtils.getFileExtensionFromFileName(filePath) + "\"\r\n");
                            dataOutputStream.writeBytes("Content-Type: " + mimeType + "\r\n\r\n");
                            int totalBytesUploaded = 0;
                            int bytesUploadedLastVerification = 0;
                            while (true) {
                                int bytesRead = fileInputStream.read(buffer, 0, chunkSize);
                                if (bytesRead == -1) {
                                    break;
                                } else if (CloudData.isOperationCanceled(container)) {
                                    Logger.i(TAG, "stop uploading - share was canceled: " + container.shareId);
                                    UploadResult uploadResult3 = new UploadResult(false, null);
                                    if (urlConnection != null) {
                                        urlConnection.disconnect();
                                    }
                                    if (dataOutputStream != null) {
                                        try {
                                            dataOutputStream.close();
                                        } catch (IOException e) {
                                        }
                                    }
                                    if (fileInputStream != null) {
                                        try {
                                            fileInputStream.close();
                                        } catch (IOException e2) {
                                        }
                                    }
                                    if (inputStream == null) {
                                        return uploadResult3;
                                    }
                                    try {
                                        inputStream.close();
                                        return uploadResult3;
                                    } catch (IOException e3) {
                                        return uploadResult3;
                                    }
                                } else {
                                    if (isBigFile) {
                                        if (sNumRegularFileUploadsInProgress > 0) {
                                            Logger.d(TAG, "regular file being uploaded, yield big file upload");
                                            SystemClock.sleep(2000);
                                        }
                                    }
                                    dataOutputStream.write(buffer, 0, bytesRead);
                                    totalBytesUploaded += bytesRead;
                                    int currentProgress = calculateProgress(totalBytesUploaded, container.transferStatus);
                                    long now = now();
                                    if (now - container.transferStatus.startTime <= transferCancelTime || currentProgress >= 90) {
                                        if (now - lastStatusVerificationTime > transfEstimateWindowSize) {
                                            updateProgressStatus(totalBytesUploaded, bytesUploadedLastVerification, uploadStartTime, lastStatusVerificationTime, container.transferStatus);
                                            Logger.d(TAG, "----- Upload Status ------");
                                            Logger.d(TAG, "total size: " + container.transferStatus.totalBytes);
                                            Logger.d(TAG, "started at: " + new Date(uploadStartTime));
                                            Logger.d(TAG, "bytes uploaded so far: " + totalBytesUploaded);
                                            Logger.d(TAG, "percent: " + container.transferStatus.progress());
                                            Logger.d(TAG, "remaining bytes: " + container.transferStatus.remainingBytes());
                                            Logger.d(TAG, "bytes uploaded in last window: " + (totalBytesUploaded - bytesUploadedLastVerification));
                                            Logger.d(TAG, "last window duration time: " + ((now - lastStatusVerificationTime) / 1000) + " secs");
                                            Logger.i(TAG, "estimated remaining time: " + (container.transferStatus.estimatedRemainingTime / 1000) + " secs");
                                            Logger.d(TAG, "----------------------------------");
                                            bytesUploadedLastVerification = totalBytesUploaded;
                                            lastStatusVerificationTime = now;
                                            long totalEstimatedTime = container.transferStatus.estimatedRemainingTime + container.transferStatus.elapsedTime;
                                            Logger.i(TAG, "estimated total upload time: " + (totalEstimatedTime / 1000) + " secs");
                                            if (totalEstimatedTime > transfEstimateWarnTime && currentProgress < 90 && container.transferStatus.estimatedRemainingTime > 120000) {
                                                Logger.i(TAG, "estimated upload time exceeds limit");
                                                if (progressListener != null) {
                                                    progressListener.onEstimateLongerThanLimit(container);
                                                }
                                            }
                                        }
                                        if (now - lastProgressUpdateTime > updateProgressInterval || currentProgress == 100) {
                                            lastProgressUpdateTime = now;
                                            updateProgressStatus(totalBytesUploaded, 0, uploadStartTime, uploadStartTime, container.transferStatus);
                                            Logger.d(TAG, "total bytes uploaded (" + (isBigFile ? "big" : "reg") + ": " + container.shareId + "): " + container.transferStatus.transferredBytes + " (" + container.transferStatus.progress() + "%)");
                                            if (progressListener != null) {
                                                progressListener.onProgressUpdate(container);
                                            }
                                        }
                                    } else {
                                        Logger.w(TAG, "this upload is taking too long - canceling it");
                                        if (progressListener != null) {
                                            progressListener.onCancelTransferTakingTooLongNetwork(container);
                                        }
                                        container.actionResult.setError(Error.TRANSFER_TIMEOUT);
                                        UploadResult uploadResult4 = new UploadResult(false, null);
                                        if (urlConnection != null) {
                                            urlConnection.disconnect();
                                        }
                                        if (dataOutputStream != null) {
                                            try {
                                                dataOutputStream.close();
                                            } catch (IOException e4) {
                                            }
                                        }
                                        if (fileInputStream != null) {
                                            try {
                                                fileInputStream.close();
                                            } catch (IOException e5) {
                                            }
                                        }
                                        if (inputStream == null) {
                                            return uploadResult4;
                                        }
                                        try {
                                            inputStream.close();
                                            return uploadResult4;
                                        } catch (IOException e6) {
                                            return uploadResult4;
                                        }
                                    }
                                }
                            }
                        } catch (Exception e7) {
                            e = e7;
                            fileInputStream2 = fileInputStream;
                            dataOutStream = dataOutputStream;
                            try {
                                Logger.e(TAG, "could not upload data", e);
                                if (urlConnection != null) {
                                }
                                if (dataOutStream != null) {
                                }
                                if (fileInputStream2 != null) {
                                }
                                if (inputStream != null) {
                                }
                                numAttempts++;
                            } catch (Throwable th) {
                                th = th;
                                if (urlConnection != null) {
                                }
                                if (dataOutStream != null) {
                                }
                                if (fileInputStream2 != null) {
                                }
                                if (inputStream != null) {
                                }
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            fileInputStream2 = fileInputStream;
                            dataOutStream = dataOutputStream;
                            if (urlConnection != null) {
                            }
                            if (dataOutStream != null) {
                            }
                            if (fileInputStream2 != null) {
                            }
                            if (inputStream != null) {
                            }
                            throw th;
                        }
                    } catch (Exception e8) {
                        e = e8;
                        fileInputStream2 = fileInputStream;
                        Logger.e(TAG, "could not upload data", e);
                        if (urlConnection != null) {
                            urlConnection.disconnect();
                        }
                        if (dataOutStream != null) {
                            try {
                                dataOutStream.close();
                            } catch (IOException e9) {
                            }
                        }
                        if (fileInputStream2 != null) {
                            try {
                                fileInputStream2.close();
                            } catch (IOException e10) {
                            }
                        }
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e11) {
                            }
                        }
                        numAttempts++;
                    } catch (Throwable th3) {
                        th = th3;
                        fileInputStream2 = fileInputStream;
                        if (urlConnection != null) {
                            urlConnection.disconnect();
                        }
                        if (dataOutStream != null) {
                            try {
                                dataOutStream.close();
                            } catch (IOException e12) {
                            }
                        }
                        if (fileInputStream2 != null) {
                            try {
                                fileInputStream2.close();
                            } catch (IOException e13) {
                            }
                        }
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e14) {
                            }
                        }
                        throw th;
                    }
                } catch (Exception e15) {
                    e = e15;
                    Logger.e(TAG, "could not upload data", e);
                    if (urlConnection != null) {
                    }
                    if (dataOutStream != null) {
                    }
                    if (fileInputStream2 != null) {
                    }
                    if (inputStream != null) {
                    }
                    numAttempts++;
                }
            } else {
                Logger.w(TAG, "WiFi Only mode selected, but WiFi is not available");
                error = Error.WIFI_ONLY_ALLOWED;
                numAttempts++;
            }
            numAttempts++;
        }
        Logger.e(TAG, "all retries failed: could not upload content to server");
        if (error == null) {
            error = Error.EXCEEDED_MAX_RETRIES;
        }
        container.actionResult.setError(error, null, httpStatusCode);
        return new UploadResult(false, null);
        if (fileInputStream != null) {
            try {
                fileInputStream.close();
            } catch (IOException e16) {
            }
        }
        if (inputStream == null) {
            try {
                inputStream.close();
            } catch (IOException e17) {
            }
        }
        numAttempts++;
        if (inputStream == null) {
        }
        numAttempts++;
        if (fileInputStream != null) {
            try {
                fileInputStream.close();
            } catch (IOException e18) {
            }
        }
        if (inputStream2 != null) {
            return uploadResult2;
        }
        try {
            inputStream2.close();
            return uploadResult2;
        } catch (IOException e19) {
            return uploadResult2;
        }
        if (fileInputStream != null) {
            try {
                fileInputStream.close();
            } catch (IOException e20) {
            }
        }
        if (inputStream != null) {
            return uploadResult;
        }
        try {
            inputStream.close();
            return uploadResult;
        } catch (IOException e21) {
            return uploadResult;
        }
        if (inputStream2 != null) {
        }
        if (inputStream != null) {
        }
    }

    protected static String downloadContent(ContentContainer contentContainer, Context context, long j, ContentTransferProgressListener contentTransferProgressListener, boolean z) {
        if (contentContainer.transferStatus == null || contentContainer.transferStatus.isCompleted()) {
            Logger.d(TAG, "download ...  <= " + contentContainer.url);
            int i = contentContainer.size;
            if (i <= 0) {
                Logger.e(TAG, "file size unknown");
                return null;
            }
            contentContainer.transferStatus = new TransferStatus(i);
            Logger.i(TAG, "download file size: " + contentContainer.transferStatus.totalBytes);
            Logger.d(TAG, "download started: " + new Date(contentContainer.transferStatus.startTime));
            boolean isBigFile = FileUtils.isBigFile((long) i);
            long j2 = isBigFile ? 240000 : 120000;
            long j3 = isBigFile ? ZapConstants.LOCATION_MAX_AGE_MODALITY_NONE : 600000;
            long j4 = 5000;
            long j5 = contentContainer.transferStatus.startTime;
            int i2 = z ? 1 : 10;
            while (true) {
                try {
                    DownloadInfo downloadAndSaveFile = downloadAndSaveFile(contentContainer, context, j, contentTransferProgressListener);
                    if (downloadAndSaveFile == null) {
                        contentContainer.transferStatus.stopTime = now();
                        return null;
                    }
                    switch (downloadAndSaveFile.status) {
                        case CANCELED:
                        case FAILED:
                            contentContainer.transferStatus.stopTime = now();
                            return null;
                        case SUCCESS:
                            Logger.i(TAG, "download DONE: " + contentContainer.shareId);
                            contentContainer.path = downloadAndSaveFile.filePath;
                            return contentContainer.path;
                        case RETRY_NO_WIFI:
                            i2++;
                            break;
                        case RETRY_ERROR:
                            break;
                        case RETRY_NOT_READY:
                            long now = now();
                            if (now - contentContainer.transferStatus.startTime > j3) {
                                Logger.w(TAG, "content took too long to be ready - cancelling download");
                                if (contentTransferProgressListener != null) {
                                    contentTransferProgressListener.onCancelTransferTakingTooLongNotReady(contentContainer);
                                }
                                contentContainer.actionResult.setError(Error.CONTENT_AVAILABILITY_TIMEOUT);
                                contentContainer.transferStatus.stopTime = now();
                                return null;
                            }
                            if (now - contentContainer.transferStatus.startTime > j2 && contentTransferProgressListener != null) {
                                contentTransferProgressListener.onTakingTooLongNotReady(contentContainer);
                            }
                            long j6 = now - j5;
                            if (j6 <= 30000) {
                                j4 = 5000;
                            } else if (j6 - 30000 < 240000) {
                                j4 = (long) (((float) j4) * 1.55f);
                            }
                            Logger.i(TAG, "content NOT READY (" + contentContainer.shareId + ") - retrying in " + (j4 / 1000) + " seconds...");
                            SystemClock.sleep(j4);
                            continue;
                        default:
                            throw new IllegalStateException("unsupported download status");
                    }
                    if (i2 >= 10) {
                        Logger.e(TAG, "all retries failed: could not download content");
                        if (downloadAndSaveFile.status == DownloadStatus.RETRY_NO_WIFI) {
                            contentContainer.actionResult.setError(Error.WIFI_ONLY_ALLOWED);
                        } else {
                            contentContainer.actionResult.setError(Error.EXCEEDED_MAX_RETRIES);
                        }
                        contentContainer.transferStatus.stopTime = now();
                        return null;
                    }
                    i2++;
                    Logger.i(TAG, "retrying in 3 seconds...");
                    SystemClock.sleep(3000);
                } finally {
                    contentContainer.transferStatus.stopTime = now();
                }
            }
        } else {
            Logger.e(TAG, "download is still in progress");
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:233:0x0810, code lost:
        r0.flush();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:234:0x0813, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:236:?, code lost:
        com.motorola.sharing.util.Logger.i(TAG, "download DONE: " + r30.shareId);
        com.motorola.sharing.util.Logger.d(TAG, "total download time: " + ((now() - r4) / 1000) + " secs");
        com.motorola.sharing.util.Logger.d(TAG, "content saved to file: " + r0.getAbsolutePath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:237:0x087b, code lost:
        if (com.motorola.sharing.util.FileUtils.isMedia(r30.mimeType) == false) goto L_0x0886;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:238:0x087d, code lost:
        com.motorola.sharing.util.FileUtils.refreshMediaFile(r0.getAbsolutePath(), r31);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:239:0x0886, code lost:
        r2 = new com.motorola.sharing.provider.cloud.HttpHelper.DownloadInfo(com.motorola.sharing.provider.cloud.HttpHelper.DownloadStatus.SUCCESS, r0.getAbsolutePath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:240:0x0891, code lost:
        if (r9 == null) goto L_0x0896;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:241:0x0893, code lost:
        r9.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:242:0x0896, code lost:
        if (r19 == null) goto L_0x089b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:244:?, code lost:
        r19.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:254:0x08cc, code lost:
        r2 = new com.motorola.sharing.provider.cloud.HttpHelper.DownloadInfo(com.motorola.sharing.provider.cloud.HttpHelper.DownloadStatus.RETRY_ERROR, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:255:0x08d4, code lost:
        if (r9 != null) goto L_0x08d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:256:0x08d6, code lost:
        r9.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:257:0x08d9, code lost:
        if (r6 != null) goto L_0x08db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:259:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:260:0x08de, code lost:
        if (r5 != null) goto L_0x08e0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:262:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:263:0x08e3, code lost:
        if (r4 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:265:0x08e7, code lost:
        r4.delete();
        com.motorola.sharing.util.Logger.d(TAG, "file deleted: " + r4.getAbsolutePath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:267:?, code lost:
        com.motorola.sharing.util.Logger.w(TAG, "low storage detected");
        r34.onLowStorage(r30);
        r2 = new com.motorola.sharing.provider.cloud.HttpHelper.DownloadInfo(com.motorola.sharing.provider.cloud.HttpHelper.DownloadStatus.FAILED, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:268:0x091e, code lost:
        if (r9 != null) goto L_0x0920;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:269:0x0920, code lost:
        r9.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:270:0x0923, code lost:
        if (r6 != null) goto L_0x0925;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:272:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:273:0x0928, code lost:
        if (r5 != null) goto L_0x092a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:275:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:276:0x092d, code lost:
        if (r4 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:278:0x0931, code lost:
        r4.delete();
        com.motorola.sharing.util.Logger.d(TAG, "file deleted: " + r4.getAbsolutePath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:282:0x0959, code lost:
        r9.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:285:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:288:?, code lost:
        r18.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:328:0x09ef, code lost:
        r2 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:329:0x09f0, code lost:
        r16 = true;
        r5 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:338:0x0a1e, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:339:0x0a1f, code lost:
        r4 = r0;
        r5 = r0;
        r6 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:374:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:375:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:377:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:378:?, code lost:
        return r2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:254:0x08cc A[Catch:{ all -> 0x09f6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:266:0x0908 A[SYNTHETIC, Splitter:B:266:0x0908] */
    /* JADX WARNING: Removed duplicated region for block: B:282:0x0959  */
    /* JADX WARNING: Removed duplicated region for block: B:284:0x095e A[SYNTHETIC, Splitter:B:284:0x095e] */
    /* JADX WARNING: Removed duplicated region for block: B:287:0x0963 A[SYNTHETIC, Splitter:B:287:0x0963] */
    private static DownloadInfo downloadAndSaveFile(ContentContainer contentContainer, Context context, long j, ContentTransferProgressListener contentTransferProgressListener) {
        FileOutputStream fileOutputStream;
        File file;
        InputStream inputStream;
        FileOutputStream fileOutputStream2;
        File file2;
        boolean z;
        String str;
        DownloadInfo downloadInfo;
        int i;
        long j2;
        long j3;
        Logger.d(TAG, "attempt to download url: " + contentContainer.url);
        if (CloudData.isOperationCanceled(contentContainer)) {
            Logger.i(TAG, "download canceled: " + contentContainer.shareId);
            return new DownloadInfo(DownloadStatus.CANCELED, null);
        }
        String str2 = contentContainer.mimeType;
        boolean isBigFile = FileUtils.isBigFile((long) contentContainer.transferStatus.totalBytes);
        long j4 = isBigFile ? 1200000 : 600000;
        long j5 = isBigFile ? 420000 : 300000;
        long j6 = isBigFile ? 120000 : 60000;
        contentContainer.transferStatus.maxAllowedTime = j4;
        if (!((Boolean) CloudSettings.WIFI_ONLY_CONTENT_TRANSFER.get()).booleanValue() || NetworkUtils.isWifiConnected(context)) {
            HttpURLConnection httpURLConnection = null;
            InputStream inputStream2 = null;
            FileOutputStream fileOutputStream3 = null;
            File file3 = null;
            boolean z2 = false;
            try {
                httpURLConnection = (HttpURLConnection) new URL(contentContainer.url).openConnection();
                if (contentContainer.userCred.token != null) {
                    httpURLConnection.addRequestProperty("Authorization", "OAuth " + contentContainer.userCred.token + "||" + contentContainer.userCred.id);
                }
                httpURLConnection.setConnectTimeout(10000);
                httpURLConnection.setReadTimeout(30000);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setRequestProperty("x-api-version", "1.4");
                httpURLConnection.setRequestProperty("Connection", "close");
                httpURLConnection.connect();
                String headerField = httpURLConnection.getHeaderField("Content-Type");
                Logger.d(TAG, "content type: " + headerField);
                if (headerField == null) {
                    Logger.w(TAG, "content NOT READY on server yet: " + contentContainer.shareId);
                    DownloadInfo downloadInfo2 = new DownloadInfo(DownloadStatus.RETRY_NOT_READY, null);
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    if (inputStream2 != null) {
                        try {
                            inputStream2.close();
                        } catch (IOException e) {
                        }
                    }
                    if (fileOutputStream3 != null) {
                        try {
                            fileOutputStream3.close();
                        } catch (IOException e2) {
                        }
                    }
                    if (file3 == null) {
                        return downloadInfo2;
                    }
                    file3.delete();
                    Logger.d(TAG, "file deleted: " + file3.getAbsolutePath());
                    return downloadInfo2;
                } else if (headerField.contains("text/")) {
                    String stringFromInputStream = getStringFromInputStream(httpURLConnection.getInputStream());
                    Logger.i(TAG, "server response: " + stringFromInputStream);
                    try {
                        StatusCode codeOf = StatusCode.codeOf(new JSONObject(stringFromInputStream).getString("status"));
                        if (codeOf == StatusCode.RETRY) {
                            Logger.w(TAG, "content NOT READY on server yet: " + contentContainer.shareId);
                            DownloadInfo downloadInfo3 = new DownloadInfo(DownloadStatus.RETRY_NOT_READY, null);
                            if (httpURLConnection != null) {
                                httpURLConnection.disconnect();
                            }
                            if (inputStream2 != null) {
                                try {
                                    inputStream2.close();
                                } catch (IOException e3) {
                                }
                            }
                            if (fileOutputStream3 != null) {
                                try {
                                    fileOutputStream3.close();
                                } catch (IOException e4) {
                                }
                            }
                            if (file3 == null) {
                                return downloadInfo3;
                            }
                            file3.delete();
                            Logger.d(TAG, "file deleted: " + file3.getAbsolutePath());
                            return downloadInfo3;
                        } else if (codeOf == StatusCode.CANCELLED) {
                            Logger.w(TAG, "upload was canceled by sender: " + contentContainer.shareId);
                            if (contentTransferProgressListener != null) {
                                contentTransferProgressListener.onSenderCancelled(contentContainer);
                            }
                            contentContainer.actionResult.setError(Error.SHARE_CANCELED);
                            DownloadInfo downloadInfo4 = new DownloadInfo(DownloadStatus.CANCELED, null);
                            if (httpURLConnection != null) {
                                httpURLConnection.disconnect();
                            }
                            if (inputStream2 != null) {
                                try {
                                    inputStream2.close();
                                } catch (IOException e5) {
                                }
                            }
                            if (fileOutputStream3 != null) {
                                try {
                                    fileOutputStream3.close();
                                } catch (IOException e6) {
                                }
                            }
                            if (file3 == null) {
                                return downloadInfo4;
                            }
                            file3.delete();
                            Logger.d(TAG, "file deleted: " + file3.getAbsolutePath());
                            return downloadInfo4;
                        } else {
                            Logger.e(TAG, "invalid server status: " + codeOf);
                            contentContainer.actionResult.setError(Error.INVALID_STATUS, codeOf.name(), 0);
                            DownloadInfo downloadInfo5 = new DownloadInfo(DownloadStatus.CANCELED, null);
                            if (httpURLConnection != null) {
                                httpURLConnection.disconnect();
                            }
                            if (inputStream2 != null) {
                                try {
                                    inputStream2.close();
                                } catch (IOException e7) {
                                }
                            }
                            if (fileOutputStream3 != null) {
                                try {
                                    fileOutputStream3.close();
                                } catch (IOException e8) {
                                }
                            }
                            if (file3 == null) {
                                return downloadInfo5;
                            }
                            file3.delete();
                            Logger.d(TAG, "file deleted: " + file3.getAbsolutePath());
                            return downloadInfo5;
                        }
                    } catch (JSONException e9) {
                        Logger.e(TAG, "response not parsable", e9);
                        contentContainer.actionResult.setError(Error.RESPONSE_NOT_PARSABLE);
                        DownloadInfo downloadInfo6 = new DownloadInfo(DownloadStatus.FAILED, null);
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        if (inputStream2 != null) {
                            try {
                                inputStream2.close();
                            } catch (IOException e10) {
                            }
                        }
                        if (fileOutputStream3 != null) {
                            try {
                                fileOutputStream3.close();
                            } catch (IOException e11) {
                            }
                        }
                        if (file3 == null) {
                            return downloadInfo6;
                        }
                        file3.delete();
                        Logger.d(TAG, "file deleted: " + file3.getAbsolutePath());
                        return downloadInfo6;
                    }
                } else if (now() - contentContainer.transferStatus.startTime > j4) {
                    Logger.w(TAG, "this download is taking too long - canceling it");
                    if (contentTransferProgressListener != null) {
                        contentTransferProgressListener.onCancelTransferTakingTooLongNetwork(contentContainer);
                    }
                    contentContainer.actionResult.setError(Error.TRANSFER_TIMEOUT);
                    DownloadInfo downloadInfo7 = new DownloadInfo(DownloadStatus.FAILED, null);
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    if (inputStream2 != null) {
                        try {
                            inputStream2.close();
                        } catch (IOException e12) {
                        }
                    }
                    if (fileOutputStream3 != null) {
                        try {
                            fileOutputStream3.close();
                        } catch (IOException e13) {
                        }
                    }
                    if (file3 == null) {
                        return downloadInfo7;
                    }
                    file3.delete();
                    Logger.d(TAG, "file deleted: " + file3.getAbsolutePath());
                    return downloadInfo7;
                } else {
                    if (str2 == null) {
                        str = headerField;
                    } else {
                        str = str2;
                    }
                    if (!headerField.equals(str)) {
                        Logger.e(TAG, "content type does not match mime type (" + str + "): cannot download");
                        contentContainer.actionResult.setError(Error.INVALID_RESPONSE, "mime-type mismatch", 0);
                        DownloadInfo downloadInfo8 = new DownloadInfo(DownloadStatus.FAILED, null);
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        if (inputStream2 != null) {
                            try {
                                inputStream2.close();
                            } catch (IOException e14) {
                            }
                        }
                        if (fileOutputStream3 != null) {
                            try {
                                fileOutputStream3.close();
                            } catch (IOException e15) {
                            }
                        }
                        if (file3 == null) {
                            return downloadInfo8;
                        }
                        file3.delete();
                        Logger.d(TAG, "file deleted: " + file3.getAbsolutePath());
                        return downloadInfo8;
                    }
                    String headerField2 = httpURLConnection.getHeaderField("Content-Length");
                    if (headerField2 != null) {
                        int parseInt = Integer.parseInt(headerField2);
                        if (parseInt != contentContainer.transferStatus.totalBytes) {
                            Logger.e(TAG, "content size does not match (" + parseInt + "): cannot download");
                            contentContainer.actionResult.setError(Error.INVALID_RESPONSE, "size mismatch", 0);
                            DownloadInfo downloadInfo9 = new DownloadInfo(DownloadStatus.FAILED, null);
                            if (httpURLConnection != null) {
                                httpURLConnection.disconnect();
                            }
                            if (inputStream2 != null) {
                                try {
                                    inputStream2.close();
                                } catch (IOException e16) {
                                }
                            }
                            if (fileOutputStream3 != null) {
                                try {
                                    fileOutputStream3.close();
                                } catch (IOException e17) {
                                }
                            }
                            if (file3 == null) {
                                return downloadInfo9;
                            }
                            file3.delete();
                            Logger.d(TAG, "file deleted: " + file3.getAbsolutePath());
                            return downloadInfo9;
                        }
                    }
                    try {
                        file = new File(ContentContainer.generateDownloadPath(contentContainer));
                    } catch (IOException e18) {
                        Logger.e(TAG, "got exception: " + e18);
                        contentContainer.actionResult.setError(Error.FILE_IO_ERROR);
                        DownloadInfo downloadInfo10 = new DownloadInfo(DownloadStatus.FAILED, null);
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        if (inputStream2 != null) {
                            try {
                                inputStream2.close();
                            } catch (IOException e19) {
                            }
                        }
                        if (fileOutputStream3 != null) {
                            try {
                                fileOutputStream3.close();
                            } catch (IOException e20) {
                            }
                        }
                        if (file3 == null) {
                            return downloadInfo10;
                        }
                        file3.delete();
                        Logger.d(TAG, "file deleted: " + file3.getAbsolutePath());
                        return downloadInfo10;
                    }
                    try {
                        Logger.i(TAG, "downloading file (" + contentContainer.shareId + ") ...");
                        fileOutputStream = new FileOutputStream(file);
                        try {
                            InputStream inputStream3 = httpURLConnection.getInputStream();
                            try {
                                byte[] bArr = new byte[10240];
                                long now = now();
                                long j7 = 0;
                                int i2 = 0;
                                int i3 = 0;
                                long j8 = now;
                                while (true) {
                                    int read = inputStream3.read(bArr);
                                    if (read == -1) {
                                        break;
                                    }
                                    fileOutputStream.write(bArr, 0, read);
                                    i2 += read;
                                    if (CloudData.isOperationCanceled(contentContainer)) {
                                        Logger.i(TAG, "download canceled: " + contentContainer.shareId);
                                        DownloadInfo downloadInfo11 = new DownloadInfo(DownloadStatus.CANCELED, null);
                                        if (httpURLConnection != null) {
                                            httpURLConnection.disconnect();
                                        }
                                        if (inputStream3 != null) {
                                            try {
                                                inputStream3.close();
                                            } catch (IOException e21) {
                                            }
                                        }
                                        if (fileOutputStream != null) {
                                            try {
                                                fileOutputStream.close();
                                            } catch (IOException e22) {
                                            }
                                        }
                                        if (file == null) {
                                            return downloadInfo11;
                                        }
                                        file.delete();
                                        Logger.d(TAG, "file deleted: " + file.getAbsolutePath());
                                        return downloadInfo11;
                                    }
                                    int calculateProgress = calculateProgress(i2, contentContainer.transferStatus);
                                    long now2 = now();
                                    if (now2 - contentContainer.transferStatus.startTime <= j4 || calculateProgress >= 90) {
                                        if (now2 - j8 > j6) {
                                            updateProgressStatus(i2, i3, now, j8, contentContainer.transferStatus);
                                            Logger.d(TAG, "----- Download Status ------");
                                            Logger.d(TAG, "total size: " + contentContainer.transferStatus.totalBytes);
                                            String str3 = TAG;
                                            StringBuilder append = new StringBuilder().append("started at: ");
                                            Date date = new Date(now);
                                            Logger.d(str3, append.append(date).toString());
                                            Logger.d(TAG, "bytes downloaded so far: " + i2);
                                            Logger.d(TAG, "percent: " + contentContainer.transferStatus.progress());
                                            Logger.d(TAG, "remaining bytes: " + contentContainer.transferStatus.remainingBytes());
                                            Logger.d(TAG, "bytes downloaded in last window: " + (i2 - i3));
                                            Logger.d(TAG, "last window duration time: " + ((now2 - j8) / 1000) + " secs");
                                            Logger.i(TAG, "estimated remaining time: " + (contentContainer.transferStatus.estimatedRemainingTime / 1000) + " secs");
                                            Logger.d(TAG, "----------------------------------");
                                            long j9 = contentContainer.transferStatus.estimatedRemainingTime + contentContainer.transferStatus.elapsedTime;
                                            Logger.i(TAG, "estimated total download time: " + (j9 / 1000) + " secs");
                                            if (j9 > j5 && calculateProgress < 90 && contentContainer.transferStatus.estimatedRemainingTime > 120000) {
                                                Logger.w(TAG, "estimated transfer time exceeds limit");
                                                if (contentTransferProgressListener != null) {
                                                    contentTransferProgressListener.onEstimateLongerThanLimit(contentContainer);
                                                }
                                            }
                                            i = i2;
                                            j2 = now2;
                                        } else {
                                            i = i3;
                                            j2 = j8;
                                        }
                                        if (now2 - j7 > j || calculateProgress == 100) {
                                            updateProgressStatus(i2, 0, now, now, contentContainer.transferStatus);
                                            Logger.d(TAG, "total bytes downloaded (" + (isBigFile ? "big" : "reg") + ": " + contentContainer.shareId + "): " + contentContainer.transferStatus.transferredBytes + " (" + contentContainer.transferStatus.progress() + "%)");
                                            if (contentTransferProgressListener != null) {
                                                contentTransferProgressListener.onProgressUpdate(contentContainer);
                                            }
                                            j3 = now2;
                                        } else {
                                            j3 = j7;
                                        }
                                        i3 = i;
                                        j7 = j3;
                                        j8 = j2;
                                    } else {
                                        Logger.w(TAG, "this download is taking too long - canceling it");
                                        if (contentTransferProgressListener != null) {
                                            contentTransferProgressListener.onCancelTransferTakingTooLongNetwork(contentContainer);
                                        }
                                        contentContainer.actionResult.setError(Error.TRANSFER_TIMEOUT);
                                        DownloadInfo downloadInfo12 = new DownloadInfo(DownloadStatus.FAILED, null);
                                        if (httpURLConnection != null) {
                                            httpURLConnection.disconnect();
                                        }
                                        if (inputStream3 != null) {
                                            try {
                                                inputStream3.close();
                                            } catch (IOException e23) {
                                            }
                                        }
                                        if (fileOutputStream != null) {
                                            try {
                                                fileOutputStream.close();
                                            } catch (IOException e24) {
                                            }
                                        }
                                        if (file == null) {
                                            return downloadInfo12;
                                        }
                                        file.delete();
                                        Logger.d(TAG, "file deleted: " + file.getAbsolutePath());
                                        return downloadInfo12;
                                    }
                                }
                            } catch (Exception e25) {
                                e = e25;
                                z = false;
                                file2 = file;
                                fileOutputStream2 = fileOutputStream;
                                inputStream = inputStream3;
                                try {
                                    Logger.e(TAG, "got exception downloading content: " + contentContainer.shareId, e);
                                    if (!FileUtils.isLowStorage()) {
                                    }
                                } catch (Throwable th) {
                                    th = th;
                                    z2 = z;
                                    file = file2;
                                    fileOutputStream = fileOutputStream2;
                                    inputStream2 = inputStream;
                                    if (httpURLConnection != null) {
                                    }
                                    if (inputStream2 != null) {
                                    }
                                    if (fileOutputStream != null) {
                                    }
                                    file.delete();
                                    Logger.d(TAG, "file deleted: " + file.getAbsolutePath());
                                    throw th;
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                inputStream2 = inputStream3;
                                if (httpURLConnection != null) {
                                }
                                if (inputStream2 != null) {
                                }
                                if (fileOutputStream != null) {
                                }
                                file.delete();
                                Logger.d(TAG, "file deleted: " + file.getAbsolutePath());
                                throw th;
                            }
                        } catch (Exception e26) {
                            e = e26;
                            z = false;
                            file2 = file;
                            inputStream = inputStream2;
                            fileOutputStream2 = fileOutputStream;
                            Logger.e(TAG, "got exception downloading content: " + contentContainer.shareId, e);
                            if (!FileUtils.isLowStorage()) {
                            }
                        } catch (Throwable th3) {
                            th = th3;
                            if (httpURLConnection != null) {
                            }
                            if (inputStream2 != null) {
                            }
                            if (fileOutputStream != null) {
                            }
                            file.delete();
                            Logger.d(TAG, "file deleted: " + file.getAbsolutePath());
                            throw th;
                        }
                    } catch (Exception e27) {
                        e = e27;
                        z = false;
                        inputStream = inputStream2;
                        fileOutputStream2 = fileOutputStream3;
                        file2 = file;
                        Logger.e(TAG, "got exception downloading content: " + contentContainer.shareId, e);
                        if (!FileUtils.isLowStorage()) {
                        }
                    } catch (Throwable th4) {
                        th = th4;
                        fileOutputStream = fileOutputStream3;
                        if (httpURLConnection != null) {
                        }
                        if (inputStream2 != null) {
                        }
                        if (fileOutputStream != null) {
                        }
                        file.delete();
                        Logger.d(TAG, "file deleted: " + file.getAbsolutePath());
                        throw th;
                    }
                }
            } catch (Exception e28) {
                e = e28;
                inputStream = inputStream2;
                fileOutputStream2 = fileOutputStream3;
                file2 = file3;
                z = false;
                Logger.e(TAG, "got exception downloading content: " + contentContainer.shareId, e);
                if (!FileUtils.isLowStorage()) {
                }
            } catch (Throwable th5) {
                th = th5;
                file = file3;
                fileOutputStream = fileOutputStream3;
                if (httpURLConnection != null) {
                }
                if (inputStream2 != null) {
                }
                if (fileOutputStream != null) {
                }
                file.delete();
                Logger.d(TAG, "file deleted: " + file.getAbsolutePath());
                throw th;
            }
        } else {
            Logger.w(TAG, "WiFi Only mode selected, but WiFi is not available");
            return new DownloadInfo(DownloadStatus.RETRY_NO_WIFI, null);
        }
        if (fileOutputStream != null) {
            try {
                fileOutputStream.close();
            } catch (IOException e29) {
            }
        }
        if (file == null) {
        }
        return downloadInfo;
        if (file == null) {
        }
        return downloadInfo;
    }

    private static void updateProgressStatus(int i, int i2, long j, long j2, TransferStatus transferStatus) {
        long now = now();
        long j3 = now - j2;
        int i3 = i - i2;
        int i4 = transferStatus.totalBytes - i;
        transferStatus.transferredBytes = i;
        transferStatus.elapsedTime = now - j;
        transferStatus.estimatedRemainingTime = (((long) i4) * j3) / ((long) i3);
        if (!transferStatus.isValid()) {
            throw new IllegalStateException("transferStatus is incorrect");
        }
    }

    private static int calculateProgress(int i, TransferStatus transferStatus) {
        if (i <= transferStatus.totalBytes) {
            return (int) ((((float) i) / ((float) transferStatus.totalBytes)) * 100.0f);
        }
        throw new IllegalStateException("bytesTransferred=" + i + " totalBytes=" + transferStatus.totalBytes);
    }

    private static long now() {
        return System.currentTimeMillis();
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r2v1 */
    /* JADX WARNING: type inference failed for: r2v2, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r2v3 */
    /* JADX WARNING: type inference failed for: r2v5 */
    /* JADX WARNING: type inference failed for: r2v6 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0029 A[SYNTHETIC, Splitter:B:20:0x0029] */
    /* JADX WARNING: Unknown variable types count: 2 */
    private static String getResponseString(HttpResponse httpResponse) {
        ? r2;
        Throwable th;
        ? r22;
        String str = 0;
        try {
            InputStream content = httpResponse.getEntity().getContent();
            try {
                str = getStringFromInputStream(content);
                if (content != 0) {
                    try {
                        content.close();
                    } catch (IOException e) {
                    }
                }
            } catch (Exception e2) {
                e = e2;
                r22 = content;
            }
        } catch (Exception e3) {
            e = e3;
            r22 = str;
        } catch (Throwable th2) {
            r2 = str;
            th = th2;
            if (r2 != 0) {
            }
            throw th;
        }
        return str;
        try {
            Logger.e(TAG, "could not extract server response", e);
            if (r22 != 0) {
                try {
                    r22.close();
                } catch (IOException e4) {
                }
            }
            return str;
        } catch (Throwable th3) {
            th = th3;
            r2 = r22;
            if (r2 != 0) {
                try {
                    r2.close();
                } catch (IOException e5) {
                }
            }
            throw th;
        }
    }

    /* JADX WARNING: type inference failed for: r2v0 */
    /* JADX WARNING: type inference failed for: r2v1, types: [java.io.BufferedReader] */
    /* JADX WARNING: type inference failed for: r2v2 */
    /* JADX WARNING: type inference failed for: r2v3, types: [java.io.BufferedReader] */
    /* JADX WARNING: type inference failed for: r2v5 */
    /* JADX WARNING: type inference failed for: r2v6 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004e A[SYNTHETIC, Splitter:B:24:0x004e] */
    /* JADX WARNING: Unknown variable types count: 2 */
    private static String getStringFromInputStream(InputStream inputStream) {
        ? r2;
        ? r22;
        Throwable th;
        String str = 0;
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            try {
                StringBuilder sb = new StringBuilder();
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    sb.append(readLine + "\n");
                }
                str = sb.toString();
                if (bufferedReader != 0) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                    }
                }
            } catch (Exception e2) {
                e = e2;
                r2 = bufferedReader;
            }
        } catch (Exception e3) {
            e = e3;
            r2 = str;
            try {
                Logger.e(TAG, "could not extract string from input stream", e);
                if (r2 != 0) {
                    try {
                        r2.close();
                    } catch (IOException e4) {
                    }
                }
                return str;
            } catch (Throwable th2) {
                th = th2;
                r22 = r2;
                if (r22 != 0) {
                    try {
                        r22.close();
                    } catch (IOException e5) {
                    }
                }
                throw th;
            }
        } catch (Throwable th3) {
            r22 = str;
            th = th3;
            if (r22 != 0) {
            }
            throw th;
        }
        return str;
    }

    private static synchronized String randomString() {
        String l;
        synchronized (HttpHelper.class) {
            if (sRandom == null) {
                sRandom = new SecureRandom();
            }
            l = Long.toString(sRandom.nextLong(), 36);
        }
        return l;
    }
}
