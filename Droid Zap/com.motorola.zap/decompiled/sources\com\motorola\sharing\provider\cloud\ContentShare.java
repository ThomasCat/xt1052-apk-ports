package com.motorola.sharing.provider.cloud;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.motorola.gcsp.json.ContentMetaData;
import com.motorola.gcsp.json.DeviceInfo;
import com.motorola.gcsp.json.P2PCred;
import com.motorola.gcsp.json.ShareFilter;
import com.motorola.gcsp.json.ShareFilterLocation;
import com.motorola.gcsp.json.ShareFilterPin;
import com.motorola.gcsp.json.ShareFilterTarget;
import com.motorola.gcsp.json.ShareMeta;
import com.motorola.gcsp.json.StartShareList;
import com.motorola.gcsp.json.StartShareListResponse;
import com.motorola.gcsp.json.StatusCode;
import com.motorola.sharing.Abuse.AbuseAction;
import com.motorola.sharing.Authenticator;
import com.motorola.sharing.Authenticator.TokenInfo;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentContainer.P2pParams;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.ContentShareListener.ContentShareEvent.Substatus;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.Settings;
import com.motorola.sharing.instrumentation.ActionResult;
import com.motorola.sharing.instrumentation.ActionResult.Error;
import com.motorola.sharing.instrumentation.CheckinManager;
import com.motorola.sharing.provider.cloud.group.GroupHandler;
import com.motorola.sharing.util.DeviceUtils;
import com.motorola.sharing.util.FileUtils;
import com.motorola.sharing.util.Logger;
import com.motorola.sharing.util.PackageUtils;
import java.io.File;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

class ContentShare {
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getCloudTag("ContentShare");
    private static final Set<String> sActivePinShares = new HashSet();
    private com.motorola.sharing.Authenticator.Callback mAuthCallback;
    private Callback mCallerCallback;
    /* access modifiers changed from: private */
    public ContentContainer mContainer;
    private Context mContext;
    private boolean mIsRetrying;
    private boolean mIsStarted;

    public interface Callback {
        void onComplete(ContentContainer contentContainer, Status status, Substatus substatus);
    }

    ContentShare(ContentContainer container) {
        this.mIsStarted = false;
        this.mIsRetrying = false;
        this.mContext = null;
        this.mContainer = null;
        this.mCallerCallback = null;
        this.mAuthCallback = new com.motorola.sharing.Authenticator.Callback() {
            public void onAuthComplete(Authenticator.Status status, TokenInfo tokenInfo, Object data) {
                ContentShare.this.mContainer.userCred.set(tokenInfo);
                if (!ContentShare.this.mContainer.userCred.isEmpty()) {
                    ContentShare.this.startTask();
                    return;
                }
                Logger.e(ContentShare.TAG, "authentication failed");
                ContentShare.this.mContainer.actionResult.setError(status == Authenticator.Status.ERROR_NETWORK ? Error.AUTH_NETWORK_ERROR : Error.NOT_AUTHORIZED);
                ContentShare.this.onComplete(Status.ERROR, Substatus.AUTH);
            }
        };
        this.mContext = GCSP.getContext();
        this.mContainer = container;
    }

    public void start(Callback callback) {
        synchronized (this) {
            if (this.mIsStarted) {
                throw new IllegalStateException("task already started");
            }
            this.mIsStarted = true;
        }
        this.mCallerCallback = callback;
        TokenInfo tokenInfo = GCSP.getAuthenticator().getTokenInfo();
        if (tokenInfo == null || !tokenInfo.isValid()) {
            Logger.d(TAG, "no valid local token, requesting a new one");
            GCSP.getAuthenticator().authenticate(this.mAuthCallback, null);
            return;
        }
        this.mContainer.userCred.set(tokenInfo);
        if (this.mContainer.userCred.isEmpty()) {
            this.mContainer.actionResult.setError(Error.NOT_AUTHORIZED);
            onComplete(Status.ERROR, Substatus.AUTH);
            return;
        }
        startTask();
    }

    /* access modifiers changed from: private */
    public void onComplete(Status status, Substatus substatus) {
        boolean isCanceled = CloudData.isOperationCanceled(this.mContainer);
        if (isCanceled) {
            this.mContainer.actionResult.setSuccess(ActionResult.Status.CANCELED);
        }
        if (this.mCallerCallback != null) {
            this.mCallerCallback.onComplete(this.mContainer, status, substatus);
        }
        if (isCanceled || status == Status.ERROR) {
            CheckinManager.getInstance().logZapEvent(this.mContainer);
        }
    }

    /* access modifiers changed from: private */
    public void startTask() {
        new Thread() {
            public void run() {
                try {
                    ContentShare.this.shareMetadata();
                } catch (JsonSyntaxException e) {
                    Logger.e(ContentShare.TAG, "response not parsable: " + e);
                    ContentShare.this.mContainer.actionResult.setError(Error.RESPONSE_NOT_PARSABLE);
                    ContentShare.this.onComplete(Status.ERROR, Substatus.GENERAL);
                } catch (ConnectionException e2) {
                    ContentShare.this.mContainer.actionResult.setError(Error.CONNECTION_EXCEPTION, null, e2.getStatusCode());
                    ContentShare.this.onComplete(Status.ERROR, Substatus.GENERAL);
                } catch (UnknownHostException e3) {
                    ContentShare.this.mContainer.actionResult.setError(Error.UNKNOWN_HOST_EXCEPTION);
                    ContentShare.this.onComplete(Status.ERROR, Substatus.NETWORK);
                } catch (Exception e4) {
                    Logger.e(ContentShare.TAG, "failed to share metadata: " + e4);
                    ContentShare.this.mContainer.actionResult.setError(Error.UNKNOWN);
                    ContentShare.this.onComplete(Status.ERROR, Substatus.INVALID_METADATA);
                }
            }
        }.start();
    }

    public void shareMetadata() throws ConnectionException, UnknownHostException {
        Logger.i(TAG, "sharing metadata...");
        StartShareListResponse metadata = sendRequest();
        if (metadata == null) {
            Logger.e(TAG, "got empty server response");
            this.mContainer.actionResult.setError(Error.EMPTY_RESPONSE);
            onComplete(Status.ERROR, Substatus.GENERAL);
            return;
        }
        StatusCode serverStatus = StatusCode.codeOf(metadata.status);
        Logger.i(TAG, "server response status: " + serverStatus);
        if (serverStatus != StatusCode.OK) {
            this.mContainer.actionResult.setError(Error.INVALID_STATUS, serverStatus.name(), 0);
            switch (serverStatus) {
                case ERR_001:
                    Logger.w(TAG, "authentication error on server");
                    if (this.mIsRetrying) {
                        Logger.e(TAG, "it's already retrying - authentication failed on server");
                        onComplete(Status.ERROR, Substatus.AUTH);
                        return;
                    }
                    Logger.i(TAG, "starting client authentication process to refresh token");
                    this.mIsRetrying = true;
                    GCSP.getAuthenticator().authenticate(this.mAuthCallback, null, true);
                    return;
                case BLOCKED:
                    Logger.w(TAG, "report abuse - status BLOCKED - cannot send");
                    AbuseActionHandler.getInstance().handleBlocking(this.mContainer, AbuseAction.SENDING_BLOCKED, true);
                    onComplete(Status.ERROR, Substatus.GENERAL);
                    return;
                case DISABLED:
                    Logger.w(TAG, "report abuse - status DISABLED - cannot send");
                    AbuseActionHandler.getInstance().handleBlocking(this.mContainer, AbuseAction.BLOCKED, true);
                    onComplete(Status.ERROR, Substatus.GENERAL);
                    return;
                case ERR_009:
                case ERR_040:
                    String owner = metadata.activeTargets[0].targetMeta.targetOwner;
                    String id = metadata.activeTargets[0].targetId;
                    if (GroupHandler.getInstance().isLeavingGroup()) {
                        GroupHandler.getInstance().leaveGroup(owner, id);
                    } else {
                        GroupHandler.getInstance().syncGroupState();
                    }
                    onComplete(Status.ERROR, Substatus.INVALID_METADATA);
                    return;
                case ERR_010:
                case ERR_028:
                case ERR_044:
                case INACTIVE:
                    GroupHandler.getInstance().handleGroupExpired();
                    onComplete(Status.ERROR, Substatus.INVALID_GROUP);
                    return;
                default:
                    Logger.e(TAG, "invalid server response status: " + serverStatus);
                    onComplete(Status.ERROR, Substatus.INVALID_METADATA);
                    return;
            }
        } else {
            AbuseAction abuseAction = AbuseAction.values()[metadata.abuseAction];
            if (AbuseActionHandler.getInstance().handleBlocking(this.mContainer, abuseAction, true)) {
                this.mContainer.actionResult.setError(Error.INVALID_STATUS, abuseAction.name(), 0);
                onComplete(Status.ERROR, Substatus.GENERAL);
                return;
            }
            if (this.mContainer.pin != null) {
                addActivePinShare(metadata.shareId);
            }
            this.mContainer.shareId = metadata.shareId;
            this.mContainer.url = metadata.uploadUrl;
            if (this.mContainer.url != null) {
                this.mContainer.url = this.mContainer.url.replaceAll("http://", "https://");
            }
            if (metadata.settingsUpdateRequired) {
                new SyncableSettingsDownloader(this.mContext).start(this.mContainer.userCred.account, this.mContainer.userCred.token, this.mContainer.userCred.id);
            }
            Logger.i(TAG, "upload sid: " + this.mContainer.shareId);
            CheckinManager.getInstance().logZapContext(this.mContainer);
            onComplete(Status.INFO, Substatus.META_SHARED);
        }
    }

    public static synchronized Set getsActivePinSharesCopy() {
        HashSet hashSet;
        synchronized (ContentShare.class) {
            hashSet = new HashSet(sActivePinShares);
        }
        return hashSet;
    }

    public static synchronized void addActivePinShare(String shareId) {
        synchronized (ContentShare.class) {
            if (shareId != null) {
                sActivePinShares.add(shareId);
            }
        }
    }

    public static synchronized void removeActivePinShare(String shareId) {
        synchronized (ContentShare.class) {
            sActivePinShares.remove(shareId);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00f8  */
    private StartShareListResponse sendRequest() throws JsonSyntaxException, ConnectionException, UnknownHostException {
        String str;
        String sendPostRequest;
        Logger.d(TAG, "creating Base64 thumbnail");
        String base64Thumbnail = FileUtils.getBase64Thumbnail(this.mContainer.path, 128, 128);
        if (base64Thumbnail != null) {
            Logger.d(TAG, "Base64 thumbnail created (" + base64Thumbnail.length() + " chars)");
        }
        String str2 = this.mContainer.path;
        if (this.mContainer.useJpegSizeReduction && this.mContainer.mimeType.contains("image/jpeg")) {
            this.mContainer.tmpPath = FileUtils.reduceJpegSize(this.mContainer.path, (String) Settings.DOWNLOADS_DIRECTORY.get(), ((Integer) Settings.JPEG_REDUCTION_MAX_SIDE_SIZE.get()).intValue(), ((Float) Settings.JPEG_REDUCTION_SIZE_FACTOR.get()).floatValue());
            if (this.mContainer.tmpPath != null) {
                Logger.d(TAG, "temp reduced file to upload created: " + this.mContainer.tmpPath);
                str = this.mContainer.tmpPath;
                int length = (int) new File(str).length();
                Logger.d(TAG, "upload file size: " + length);
                Logger.d(TAG, "sending request to server...");
                sendPostRequest = HttpHelper.sendPostRequest(UrlHelper.startShare(), getPayload(base64Thumbnail, length), this.mContainer.userCred.token, this.mContainer.userCred.id, true);
                Logger.d(TAG, "Share Metadata Response:");
                Logger.dumpJson(TAG, sendPostRequest);
                if (sendPostRequest != null) {
                    return null;
                }
                return (StartShareListResponse) new Gson().fromJson(sendPostRequest, StartShareListResponse.class);
            }
            Logger.d(TAG, "uploading original file");
        }
        str = str2;
        int length2 = (int) new File(str).length();
        Logger.d(TAG, "upload file size: " + length2);
        Logger.d(TAG, "sending request to server...");
        sendPostRequest = HttpHelper.sendPostRequest(UrlHelper.startShare(), getPayload(base64Thumbnail, length2), this.mContainer.userCred.token, this.mContainer.userCred.id, true);
        Logger.d(TAG, "Share Metadata Response:");
        Logger.dumpJson(TAG, sendPostRequest);
        if (sendPostRequest != null) {
        }
    }

    private String getPayload(String base64Thumbnail, int uploadSize) {
        String imei = DeviceUtils.getImei(this.mContext);
        StartShareList payload = new StartShareList();
        payload.appId = "1234";
        payload.fromDeviceId = imei;
        payload.fromEmail = this.mContainer.userCred.account;
        if (this.mContainer.isGroupRelated()) {
            payload.targetType = "group";
        } else {
            payload.targetType = "public";
        }
        if (this.mContainer.locality != null) {
            payload.shareFilter = new ShareFilter();
            payload.shareFilter.location = new ShareFilterLocation();
            payload.shareFilter.location.lat = Double.valueOf(this.mContainer.locality.latitude);
            payload.shareFilter.location.lng = Double.valueOf(this.mContainer.locality.longitude);
            payload.shareFilter.location.distance = this.mContainer.locality.distance;
        }
        if (!TextUtils.isEmpty(this.mContainer.pin)) {
            if (payload.shareFilter == null) {
                payload.shareFilter = new ShareFilter();
            }
            payload.shareFilter.pin = new ShareFilterPin();
            payload.shareFilter.pin.pin = this.mContainer.pin;
        }
        if (this.mContainer.groupInfo != null) {
            if (payload.shareFilter == null) {
                payload.shareFilter = new ShareFilter();
            }
            payload.shareFilter.target = new ShareFilterTarget();
            payload.shareFilter.target.targetId = this.mContainer.groupInfo.id;
        } else {
            if (payload.shareMeta == null) {
                payload.shareMeta = new ShareMeta();
            }
            payload.shareMeta.shareMetaTTL = this.mContainer.getTtl();
        }
        P2pParams p2pParams = this.mContainer.getP2pParams();
        if (p2pParams != null) {
            payload.p2pCred = new P2PCred();
            payload.p2pCred.p2pName = p2pParams.name;
            payload.p2pCred.p2pPassword = p2pParams.password;
            payload.p2pCred.p2pUrl = p2pParams.url;
        }
        ContentMetaData metaData = new ContentMetaData();
        metaData.name = this.mContainer.name;
        metaData.size = uploadSize;
        metaData.mimeType = this.mContainer.mimeType;
        metaData.thumbnail = base64Thumbnail;
        payload.contentMetaData = new ArrayList();
        payload.contentMetaData.add(metaData);
        switch (this.mContainer.uploadRule) {
            case ALWAYS:
                payload.contentUploadRule = "always";
                break;
            case NEVER:
                payload.contentUploadRule = "never";
                break;
            case ON_DEMAND:
                payload.contentUploadRule = "on-demand";
                break;
        }
        payload.appVersion = Integer.valueOf(PackageUtils.getAppVersionCode(this.mContext));
        payload.settingsVersion = (Integer) CloudSettings.SETTINGS_VERSION.get();
        payload.deviceInfo = new DeviceInfo();
        payload.deviceInfo.imei = imei;
        payload.deviceInfo.model = DeviceUtils.getDeviceModelInfo();
        payload.deviceInfo.serial = Build.SERIAL;
        String jsonPayload = new Gson().toJson((Object) payload);
        Logger.d(TAG, "Start Share Request:");
        Logger.dumpJson(TAG, jsonPayload);
        return jsonPayload;
    }
}
