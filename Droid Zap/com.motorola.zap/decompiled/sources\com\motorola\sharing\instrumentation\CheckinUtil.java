package com.motorola.sharing.instrumentation;

import com.motorola.sharing.ContentContainer;

public class CheckinUtil {
    public static String getShareType(ContentContainer container) {
        String shareType = container.getCheckinData(CheckinInterface.KEY_SHARE_TYPE);
        if (shareType == null) {
            return CheckinInterface.VAL_SHARE_TYPE_GESTURE;
        }
        return shareType;
    }

    public static String getDest(ContentContainer container) {
        String dest = container.getCheckinData(CheckinInterface.KEY_DEST);
        if (dest == null) {
            return CheckinInterface.VAL_DEST_CLOUD;
        }
        return dest;
    }
}
