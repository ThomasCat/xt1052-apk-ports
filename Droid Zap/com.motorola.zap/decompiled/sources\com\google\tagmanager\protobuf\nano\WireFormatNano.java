package com.google.tagmanager.protobuf.nano;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class WireFormatNano {
    public static final boolean[] EMPTY_BOOLEAN_ARRAY = new boolean[0];
    public static final Boolean[] EMPTY_BOOLEAN_REF_ARRAY = new Boolean[0];
    public static final byte[] EMPTY_BYTES = new byte[0];
    public static final byte[][] EMPTY_BYTES_ARRAY = new byte[0][];
    public static final double[] EMPTY_DOUBLE_ARRAY = new double[0];
    public static final Double[] EMPTY_DOUBLE_REF_ARRAY = new Double[0];
    public static final float[] EMPTY_FLOAT_ARRAY = new float[0];
    public static final Float[] EMPTY_FLOAT_REF_ARRAY = new Float[0];
    public static final int[] EMPTY_INT_ARRAY = new int[0];
    public static final Integer[] EMPTY_INT_REF_ARRAY = new Integer[0];
    public static final long[] EMPTY_LONG_ARRAY = new long[0];
    public static final Long[] EMPTY_LONG_REF_ARRAY = new Long[0];
    public static final String[] EMPTY_STRING_ARRAY = new String[0];
    static final int MESSAGE_SET_ITEM_END_TAG = makeTag(1, 4);
    static final int MESSAGE_SET_ITEM_TAG = makeTag(1, 3);
    static final int MESSAGE_SET_MESSAGE_TAG = makeTag(3, 2);
    static final int MESSAGE_SET_TYPE_ID_TAG = makeTag(2, 0);

    private WireFormatNano() {
    }

    static int getTagWireType(int tag) {
        return tag & 7;
    }

    public static int getTagFieldNumber(int tag) {
        return tag >>> 3;
    }

    static int makeTag(int fieldNumber, int wireType) {
        return (fieldNumber << 3) | wireType;
    }

    public static boolean parseUnknownField(CodedInputByteBufferNano input, int tag) throws IOException {
        return input.skipField(tag);
    }

    public static boolean storeUnknownField(List<UnknownFieldData> data, CodedInputByteBufferNano input, int tag) throws IOException {
        int startPos = input.getPosition();
        boolean skip = input.skipField(tag);
        data.add(new UnknownFieldData(tag, input.getData(startPos, input.getPosition() - startPos)));
        return skip;
    }

    public static final int getRepeatedFieldArrayLength(CodedInputByteBufferNano input, int tag) throws IOException {
        int arrayLength = 1;
        int startPos = input.getPosition();
        input.skipField(tag);
        while (input.getBytesUntilLimit() > 0 && input.readTag() == tag) {
            input.skipField(tag);
            arrayLength++;
        }
        input.rewindToPosition(startPos);
        return arrayLength;
    }

    public static <T> T getExtension(Extension<T> extension, List<UnknownFieldData> unknownFields) {
        if (unknownFields == null) {
            return null;
        }
        List<UnknownFieldData> dataForField = new ArrayList<>();
        for (UnknownFieldData data : unknownFields) {
            if (getTagFieldNumber(data.tag) == extension.fieldNumber) {
                dataForField.add(data);
            }
        }
        if (dataForField.isEmpty()) {
            return null;
        }
        if (extension.isRepeatedField) {
            List<Object> result = new ArrayList<>(dataForField.size());
            for (UnknownFieldData data2 : dataForField) {
                result.add(readData(extension.fieldType, data2.bytes));
            }
            return extension.listType.cast(result);
        }
        return readData(extension.fieldType, ((UnknownFieldData) dataForField.get(dataForField.size() - 1)).bytes);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x008f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a8, code lost:
        throw new java.lang.IllegalArgumentException("Error creating instance of class " + r4, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a9, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00b1, code lost:
        throw new java.lang.IllegalArgumentException("Error reading extension field", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b2, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00cb, code lost:
        throw new java.lang.IllegalArgumentException("Error creating instance of class " + r4, r0);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:5:0x000d, B:27:0x0080] */
    private static <T> T readData(Class<T> cls, byte[] bArr) {
        if (bArr.length == 0) {
            return null;
        }
        CodedInputByteBufferNano newInstance = CodedInputByteBufferNano.newInstance(bArr);
        if (cls == String.class) {
            return cls.cast(newInstance.readString());
        }
        if (cls == Integer.class) {
            return cls.cast(Integer.valueOf(newInstance.readInt32()));
        }
        if (cls == Long.class) {
            return cls.cast(Long.valueOf(newInstance.readInt64()));
        }
        if (cls == Boolean.class) {
            return cls.cast(Boolean.valueOf(newInstance.readBool()));
        }
        if (cls == Float.class) {
            return cls.cast(Float.valueOf(newInstance.readFloat()));
        }
        if (cls == Double.class) {
            return cls.cast(Double.valueOf(newInstance.readDouble()));
        }
        if (cls == byte[].class) {
            return cls.cast(newInstance.readBytes());
        }
        if (MessageNano.class.isAssignableFrom(cls)) {
            MessageNano messageNano = (MessageNano) cls.newInstance();
            newInstance.readMessage(messageNano);
            return cls.cast(messageNano);
        }
        throw new IllegalArgumentException("Unhandled extension field type: " + cls);
    }

    public static <T> void setExtension(Extension<T> extension, T value, List<UnknownFieldData> unknownFields) {
        Iterator<UnknownFieldData> i = unknownFields.iterator();
        while (i.hasNext()) {
            if (extension.fieldNumber == getTagFieldNumber(((UnknownFieldData) i.next()).tag)) {
                i.remove();
            }
        }
        if (value != null) {
            if (value instanceof List) {
                for (Object item : (List) value) {
                    unknownFields.add(write(extension.fieldNumber, item));
                }
                return;
            }
            unknownFields.add(write(extension.fieldNumber, value));
        }
    }

    private static UnknownFieldData write(int i, Object obj) {
        byte[] bArr;
        int makeTag;
        Class<byte[]> cls = obj.getClass();
        if (cls == String.class) {
            try {
                String str = (String) obj;
                bArr = new byte[CodedOutputByteBufferNano.computeStringSizeNoTag(str)];
                CodedOutputByteBufferNano.newInstance(bArr).writeStringNoTag(str);
                makeTag = makeTag(i, 2);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        } else if (cls == Integer.class) {
            Integer num = (Integer) obj;
            bArr = new byte[CodedOutputByteBufferNano.computeInt32SizeNoTag(num.intValue())];
            CodedOutputByteBufferNano.newInstance(bArr).writeInt32NoTag(num.intValue());
            makeTag = makeTag(i, 0);
        } else if (cls == Long.class) {
            Long l = (Long) obj;
            bArr = new byte[CodedOutputByteBufferNano.computeInt64SizeNoTag(l.longValue())];
            CodedOutputByteBufferNano.newInstance(bArr).writeInt64NoTag(l.longValue());
            makeTag = makeTag(i, 0);
        } else if (cls == Boolean.class) {
            Boolean bool = (Boolean) obj;
            bArr = new byte[CodedOutputByteBufferNano.computeBoolSizeNoTag(bool.booleanValue())];
            CodedOutputByteBufferNano.newInstance(bArr).writeBoolNoTag(bool.booleanValue());
            makeTag = makeTag(i, 0);
        } else if (cls == Float.class) {
            Float f = (Float) obj;
            bArr = new byte[CodedOutputByteBufferNano.computeFloatSizeNoTag(f.floatValue())];
            CodedOutputByteBufferNano.newInstance(bArr).writeFloatNoTag(f.floatValue());
            makeTag = makeTag(i, 5);
        } else if (cls == Double.class) {
            Double d = (Double) obj;
            bArr = new byte[CodedOutputByteBufferNano.computeDoubleSizeNoTag(d.doubleValue())];
            CodedOutputByteBufferNano.newInstance(bArr).writeDoubleNoTag(d.doubleValue());
            makeTag = makeTag(i, 1);
        } else if (cls == byte[].class) {
            byte[] bArr2 = (byte[]) obj;
            bArr = new byte[CodedOutputByteBufferNano.computeByteArraySizeNoTag(bArr2)];
            CodedOutputByteBufferNano.newInstance(bArr).writeByteArrayNoTag(bArr2);
            makeTag = makeTag(i, 2);
        } else if (MessageNano.class.isAssignableFrom(cls)) {
            MessageNano messageNano = (MessageNano) obj;
            int serializedSize = messageNano.getSerializedSize();
            bArr = new byte[(CodedOutputByteBufferNano.computeRawVarint32Size(serializedSize) + serializedSize)];
            CodedOutputByteBufferNano newInstance = CodedOutputByteBufferNano.newInstance(bArr);
            newInstance.writeRawVarint32(serializedSize);
            newInstance.writeRawBytes(MessageNano.toByteArray(messageNano));
            makeTag = makeTag(i, 2);
        } else {
            throw new IllegalArgumentException("Unhandled extension field type: " + cls);
        }
        return new UnknownFieldData(makeTag, bArr);
    }

    public static int computeWireSize(List<UnknownFieldData> unknownFields) {
        if (unknownFields == null) {
            return 0;
        }
        int size = 0;
        for (UnknownFieldData unknownField : unknownFields) {
            size = size + CodedOutputByteBufferNano.computeRawVarint32Size(unknownField.tag) + unknownField.bytes.length;
        }
        return size;
    }

    public static void writeUnknownFields(List<UnknownFieldData> unknownFields, CodedOutputByteBufferNano outBuffer) throws IOException {
        if (unknownFields != null) {
            for (UnknownFieldData data : unknownFields) {
                outBuffer.writeTag(getTagFieldNumber(data.tag), getTagWireType(data.tag));
                outBuffer.writeRawBytes(data.bytes);
            }
        }
    }
}
