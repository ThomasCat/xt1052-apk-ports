package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class fb {

    public static class a<I, O> implements SafeParcelable {
        public static final fc CREATOR = new fc();
        protected final Class<? extends fb> CA;
        protected final String CB;
        private fe CC;
        /* access modifiers changed from: private */
        public b<I, O> CD;
        protected final int Cu;
        protected final boolean Cv;
        protected final int Cw;
        protected final boolean Cx;
        protected final String Cy;
        protected final int Cz;
        private final int wj;

        a(int i, int i2, boolean z, int i3, boolean z2, String str, int i4, String str2, ew ewVar) {
            this.wj = i;
            this.Cu = i2;
            this.Cv = z;
            this.Cw = i3;
            this.Cx = z2;
            this.Cy = str;
            this.Cz = i4;
            if (str2 == null) {
                this.CA = null;
                this.CB = null;
            } else {
                this.CA = fh.class;
                this.CB = str2;
            }
            if (ewVar == null) {
                this.CD = null;
            } else {
                this.CD = ewVar.ej();
            }
        }

        protected a(int i, boolean z, int i2, boolean z2, String str, int i3, Class<? extends fb> cls, b<I, O> bVar) {
            this.wj = 1;
            this.Cu = i;
            this.Cv = z;
            this.Cw = i2;
            this.Cx = z2;
            this.Cy = str;
            this.Cz = i3;
            this.CA = cls;
            if (cls == null) {
                this.CB = null;
            } else {
                this.CB = cls.getCanonicalName();
            }
            this.CD = bVar;
        }

        public static a a(String str, int i, b<?, ?> bVar, boolean z) {
            return new a(bVar.el(), z, bVar.em(), false, str, i, null, bVar);
        }

        public static <T extends fb> a<T, T> a(String str, int i, Class<T> cls) {
            return new a<>(11, false, 11, false, str, i, cls, null);
        }

        public static <T extends fb> a<ArrayList<T>, ArrayList<T>> b(String str, int i, Class<T> cls) {
            return new a<>(11, true, 11, true, str, i, cls, null);
        }

        public static a<Integer, Integer> g(String str, int i) {
            return new a<>(0, false, 0, false, str, i, null, null);
        }

        public static a<Double, Double> h(String str, int i) {
            return new a<>(4, false, 4, false, str, i, null, null);
        }

        public static a<Boolean, Boolean> i(String str, int i) {
            return new a<>(6, false, 6, false, str, i, null, null);
        }

        public static a<String, String> j(String str, int i) {
            return new a<>(7, false, 7, false, str, i, null, null);
        }

        public static a<ArrayList<String>, ArrayList<String>> k(String str, int i) {
            return new a<>(7, true, 7, true, str, i, null, null);
        }

        public void a(fe feVar) {
            this.CC = feVar;
        }

        public int describeContents() {
            fc fcVar = CREATOR;
            return 0;
        }

        public int el() {
            return this.Cu;
        }

        public int em() {
            return this.Cw;
        }

        public a<I, O> eq() {
            return new a<>(this.wj, this.Cu, this.Cv, this.Cw, this.Cx, this.Cy, this.Cz, this.CB, ey());
        }

        public boolean er() {
            return this.Cv;
        }

        public boolean es() {
            return this.Cx;
        }

        public String et() {
            return this.Cy;
        }

        public int eu() {
            return this.Cz;
        }

        public Class<? extends fb> ev() {
            return this.CA;
        }

        /* access modifiers changed from: 0000 */
        public String ew() {
            if (this.CB == null) {
                return null;
            }
            return this.CB;
        }

        public boolean ex() {
            return this.CD != null;
        }

        /* access modifiers changed from: 0000 */
        public ew ey() {
            if (this.CD == null) {
                return null;
            }
            return ew.a(this.CD);
        }

        public HashMap<String, a<?, ?>> ez() {
            er.f(this.CB);
            er.f(this.CC);
            return this.CC.ao(this.CB);
        }

        public I g(O o) {
            return this.CD.g(o);
        }

        public int getVersionCode() {
            return this.wj;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("Field\n");
            sb.append("            versionCode=").append(this.wj).append(10);
            sb.append("                 typeIn=").append(this.Cu).append(10);
            sb.append("            typeInArray=").append(this.Cv).append(10);
            sb.append("                typeOut=").append(this.Cw).append(10);
            sb.append("           typeOutArray=").append(this.Cx).append(10);
            sb.append("        outputFieldName=").append(this.Cy).append(10);
            sb.append("      safeParcelFieldId=").append(this.Cz).append(10);
            sb.append("       concreteTypeName=").append(ew()).append(10);
            if (ev() != null) {
                sb.append("     concreteType.class=").append(ev().getCanonicalName()).append(10);
            }
            sb.append("          converterName=").append(this.CD == null ? "null" : this.CD.getClass().getCanonicalName()).append(10);
            return sb.toString();
        }

        public void writeToParcel(Parcel out, int flags) {
            fc fcVar = CREATOR;
            fc.a(this, out, flags);
        }
    }

    public interface b<I, O> {
        int el();

        int em();

        I g(O o);
    }

    private void a(StringBuilder sb, a aVar, Object obj) {
        if (aVar.el() == 11) {
            sb.append(((fb) aVar.ev().cast(obj)).toString());
        } else if (aVar.el() == 7) {
            sb.append("\"");
            sb.append(fp.ap((String) obj));
            sb.append("\"");
        } else {
            sb.append(obj);
        }
    }

    private void a(StringBuilder sb, a aVar, ArrayList<Object> arrayList) {
        sb.append("[");
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append(",");
            }
            Object obj = arrayList.get(i);
            if (obj != null) {
                a(sb, aVar, obj);
            }
        }
        sb.append("]");
    }

    /* access modifiers changed from: protected */
    public <O, I> I a(a<I, O> aVar, Object obj) {
        return aVar.CD != null ? aVar.g(obj) : obj;
    }

    /* access modifiers changed from: protected */
    public boolean a(a aVar) {
        return aVar.em() == 11 ? aVar.es() ? an(aVar.et()) : am(aVar.et()) : al(aVar.et());
    }

    /* access modifiers changed from: protected */
    public abstract Object ak(String str);

    /* access modifiers changed from: protected */
    public abstract boolean al(String str);

    /* access modifiers changed from: protected */
    public boolean am(String str) {
        throw new UnsupportedOperationException("Concrete types not supported");
    }

    /* access modifiers changed from: protected */
    public boolean an(String str) {
        throw new UnsupportedOperationException("Concrete type arrays not supported");
    }

    /* access modifiers changed from: protected */
    public Object b(a aVar) {
        boolean z = true;
        String et = aVar.et();
        if (aVar.ev() == null) {
            return ak(aVar.et());
        }
        if (ak(aVar.et()) != null) {
            z = false;
        }
        er.a(z, "Concrete field shouldn't be value object: " + aVar.et());
        HashMap eo = aVar.es() ? ep() : eo();
        if (eo != null) {
            return eo.get(et);
        }
        try {
            return getClass().getMethod("get" + Character.toUpperCase(et.charAt(0)) + et.substring(1), new Class[0]).invoke(this, new Object[0]);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public abstract HashMap<String, a<?, ?>> en();

    public HashMap<String, Object> eo() {
        return null;
    }

    public HashMap<String, Object> ep() {
        return null;
    }

    public String toString() {
        HashMap en = en();
        StringBuilder sb = new StringBuilder(100);
        for (String str : en.keySet()) {
            a aVar = (a) en.get(str);
            if (a(aVar)) {
                Object a2 = a(aVar, b(aVar));
                if (sb.length() == 0) {
                    sb.append("{");
                } else {
                    sb.append(",");
                }
                sb.append("\"").append(str).append("\":");
                if (a2 != null) {
                    switch (aVar.em()) {
                        case 8:
                            sb.append("\"").append(fk.d((byte[]) a2)).append("\"");
                            break;
                        case 9:
                            sb.append("\"").append(fk.e((byte[]) a2)).append("\"");
                            break;
                        case 10:
                            fq.a(sb, (HashMap) a2);
                            break;
                        default:
                            if (!aVar.er()) {
                                a(sb, aVar, a2);
                                break;
                            } else {
                                a(sb, aVar, (ArrayList) a2);
                                break;
                            }
                    }
                } else {
                    sb.append("null");
                }
            }
        }
        if (sb.length() > 0) {
            sb.append("}");
        } else {
            sb.append("{}");
        }
        return sb.toString();
    }
}
