package com.motorola.zap.app;

import com.motorola.sharing.SyncableSettingsListener;
import com.motorola.zap.ZapSharedPreference;
import java.util.Map;
import java.util.Map.Entry;

public class ZapSyncableSettingsListener implements SyncableSettingsListener {
    public void onSyncableSettings(Map<String, String> settings) {
        for (Entry<String, String> setting : settings.entrySet()) {
            ZapSharedPreference pref = ZapSharedPreference.forName((String) setting.getKey());
            if (pref != null) {
                pref.write(pref.valueOf((String) setting.getValue()));
            }
        }
    }
}
