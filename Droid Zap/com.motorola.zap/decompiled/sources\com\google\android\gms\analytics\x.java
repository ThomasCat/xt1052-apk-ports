package com.google.android.gms.analytics;

import android.text.TextUtils;

class x {
    private String tK;
    private final long tL;
    private final long tM;
    private String tN = "https:";

    x(String str, long j, long j2) {
        this.tK = str;
        this.tL = j;
        this.tM = j2;
    }

    /* access modifiers changed from: 0000 */
    public void E(String str) {
        this.tK = str;
    }

    /* access modifiers changed from: 0000 */
    public void F(String str) {
        if (str != null && !TextUtils.isEmpty(str.trim()) && str.toLowerCase().startsWith("http:")) {
            this.tN = "http:";
        }
    }

    /* access modifiers changed from: 0000 */
    public String ch() {
        return this.tK;
    }

    /* access modifiers changed from: 0000 */
    public long ci() {
        return this.tL;
    }

    /* access modifiers changed from: 0000 */
    public long cj() {
        return this.tM;
    }

    /* access modifiers changed from: 0000 */
    public String ck() {
        return this.tN;
    }
}
