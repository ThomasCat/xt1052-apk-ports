package com.motorola.sharing.provider.cloud;

import com.motorola.sharing.Config;
import com.motorola.sharing.util.SharedPreference;

public class CloudSettings {
    public static final SharedPreference<Boolean> DISCOVER_SELF_SHARED_CONTENT = new SharedPreference<>(Config.DISCOVER_SELF_SHARED_CONTENT, Boolean.class, Boolean.valueOf(false), SharedPreference.booleanHandler, SharedPreference.nullVerificator);
    public static final SharedPreference<String> SERVER_HOST = new SharedPreference<>(Config.SERVER_HOST, String.class, CloudDefaults.SERVER_HOST, SharedPreference.stringHandler, SharedPreference.hostNullableVerificator);
    public static final SharedPreference<Integer> SETTINGS_VERSION = new SharedPreference<>("cloud_settingsVersion", Integer.class, Integer.valueOf(1), SharedPreference.integerHandler);
    public static final SharedPreference<Boolean> WIFI_ONLY_CONTENT_TRANSFER = new SharedPreference<>(Config.WIFI_ONLY_CONTENT_TRANSFER, Boolean.class, Boolean.valueOf(false), SharedPreference.booleanHandler, SharedPreference.nullVerificator);
}
