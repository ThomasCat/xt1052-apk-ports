package com.google.android.gms.internal;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public abstract class j extends i {
    private static Method kc;
    private static Method kd;
    private static Method ke;
    private static Method kf;
    private static Method kg;
    private static Method kh;
    private static String ki;
    private static p kj;
    static boolean kk = false;
    private static long startTime = 0;

    static class a extends Exception {
        public a() {
        }

        public a(Throwable th) {
            super(th);
        }
    }

    protected j(Context context, n nVar, o oVar) {
        super(context, nVar, oVar);
    }

    static String a(Context context, n nVar) throws a {
        if (ke == null) {
            throw new a();
        }
        try {
            ByteBuffer byteBuffer = (ByteBuffer) ke.invoke(null, new Object[]{context});
            if (byteBuffer != null) {
                return nVar.a(byteBuffer.array(), true);
            }
            throw new a();
        } catch (IllegalAccessException e) {
            throw new a(e);
        } catch (InvocationTargetException e2) {
            throw new a(e2);
        }
    }

    static ArrayList<Long> a(MotionEvent motionEvent, DisplayMetrics displayMetrics) throws a {
        if (kf == null || motionEvent == null) {
            throw new a();
        }
        try {
            return (ArrayList) kf.invoke(null, new Object[]{motionEvent, displayMetrics});
        } catch (IllegalAccessException e) {
            throw new a(e);
        } catch (InvocationTargetException e2) {
            throw new a(e2);
        }
    }

    protected static synchronized void a(String str, Context context, n nVar) {
        synchronized (j.class) {
            if (!kk) {
                try {
                    kj = new p(nVar, null);
                    ki = str;
                    e(context);
                    startTime = w().longValue();
                    kk = true;
                } catch (a | UnsupportedOperationException e) {
                }
            }
        }
    }

    static String b(Context context, n nVar) throws a {
        if (kh == null) {
            throw new a();
        }
        try {
            ByteBuffer byteBuffer = (ByteBuffer) kh.invoke(null, new Object[]{context});
            if (byteBuffer != null) {
                return nVar.a(byteBuffer.array(), true);
            }
            throw new a();
        } catch (IllegalAccessException e) {
            throw new a(e);
        } catch (InvocationTargetException e2) {
            throw new a(e2);
        }
    }

    private static String b(byte[] bArr, String str) throws a {
        try {
            return new String(kj.c(bArr, str), "UTF-8");
        } catch (com.google.android.gms.internal.p.a e) {
            throw new a(e);
        } catch (UnsupportedEncodingException e2) {
            throw new a(e2);
        }
    }

    static String d(Context context) throws a {
        if (kg == null) {
            throw new a();
        }
        try {
            String str = (String) kg.invoke(null, new Object[]{context});
            if (str != null) {
                return str;
            }
            throw new a();
        } catch (IllegalAccessException e) {
            throw new a(e);
        } catch (InvocationTargetException e2) {
            throw new a(e2);
        }
    }

    private static void e(Context context) throws a {
        try {
            byte[] d = kj.d(r.getKey());
            byte[] c = kj.c(d, r.A());
            File cacheDir = context.getCacheDir();
            if (cacheDir == null) {
                cacheDir = context.getDir("dex", 0);
                if (cacheDir == null) {
                    throw new a();
                }
            }
            File createTempFile = File.createTempFile("ads", ".jar", cacheDir);
            FileOutputStream fileOutputStream = new FileOutputStream(createTempFile);
            fileOutputStream.write(c, 0, c.length);
            fileOutputStream.close();
            DexClassLoader dexClassLoader = new DexClassLoader(createTempFile.getAbsolutePath(), cacheDir.getAbsolutePath(), null, context.getClassLoader());
            Class loadClass = dexClassLoader.loadClass(b(d, r.B()));
            Class loadClass2 = dexClassLoader.loadClass(b(d, r.H()));
            Class loadClass3 = dexClassLoader.loadClass(b(d, r.F()));
            Class loadClass4 = dexClassLoader.loadClass(b(d, r.L()));
            Class loadClass5 = dexClassLoader.loadClass(b(d, r.D()));
            Class loadClass6 = dexClassLoader.loadClass(b(d, r.J()));
            kc = loadClass.getMethod(b(d, r.C()), new Class[0]);
            kd = loadClass2.getMethod(b(d, r.I()), new Class[0]);
            ke = loadClass3.getMethod(b(d, r.G()), new Class[]{Context.class});
            kf = loadClass4.getMethod(b(d, r.M()), new Class[]{MotionEvent.class, DisplayMetrics.class});
            kg = loadClass5.getMethod(b(d, r.E()), new Class[]{Context.class});
            kh = loadClass6.getMethod(b(d, r.K()), new Class[]{Context.class});
            String name = createTempFile.getName();
            createTempFile.delete();
            new File(cacheDir, name.replace(".jar", ".dex")).delete();
        } catch (FileNotFoundException e) {
            throw new a(e);
        } catch (IOException e2) {
            throw new a(e2);
        } catch (ClassNotFoundException e3) {
            throw new a(e3);
        } catch (com.google.android.gms.internal.p.a e4) {
            throw new a(e4);
        } catch (NoSuchMethodException e5) {
            throw new a(e5);
        } catch (NullPointerException e6) {
            throw new a(e6);
        }
    }

    static String v() throws a {
        if (ki != null) {
            return ki;
        }
        throw new a();
    }

    static Long w() throws a {
        if (kc == null) {
            throw new a();
        }
        try {
            return (Long) kc.invoke(null, new Object[0]);
        } catch (IllegalAccessException e) {
            throw new a(e);
        } catch (InvocationTargetException e2) {
            throw new a(e2);
        }
    }

    static String x() throws a {
        if (kd == null) {
            throw new a();
        }
        try {
            return (String) kd.invoke(null, new Object[0]);
        } catch (IllegalAccessException e) {
            throw new a(e);
        } catch (InvocationTargetException e2) {
            throw new a(e2);
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0027 A[ExcHandler: IOException (e java.io.IOException), Splitter:B:1:0x0001] */
    public void b(Context context) {
        try {
            a(1, x());
            a(2, v());
        } catch (a e) {
        } catch (IOException e2) {
        }
        a(25, w().longValue());
        a(24, d(context));
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008a A[ExcHandler: IOException (e java.io.IOException), Splitter:B:6:0x0010] */
    public void c(Context context) {
        try {
            a(2, v());
        } catch (a e) {
        } catch (IOException e2) {
        }
        try {
            a(1, x());
        } catch (a e3) {
        } catch (IOException e22) {
        }
        try {
            long longValue = w().longValue();
            a(25, longValue);
            if (startTime != 0) {
                a(17, longValue - startTime);
                a(23, startTime);
            }
            ArrayList a2 = a(this.jY, this.jZ);
            a(14, ((Long) a2.get(0)).longValue());
            a(15, ((Long) a2.get(1)).longValue());
            if (a2.size() >= 3) {
                a(16, ((Long) a2.get(2)).longValue());
            }
        } catch (a e4) {
        } catch (IOException e222) {
        }
        a(27, a(context, this.ka));
        a(29, b(context, this.ka));
    }
}
