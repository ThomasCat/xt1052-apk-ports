package com.motorola.sharing.provider.cloud;

import android.content.Context;
import android.os.SystemClock;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentDownloadListener.ContentDownloadEvent.Substatus;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.instrumentation.ActionResult;
import com.motorola.sharing.instrumentation.ActionResult.Error;
import com.motorola.sharing.instrumentation.CheckinManager;
import com.motorola.sharing.util.HistoryManager;
import com.motorola.sharing.util.HistoryManager.Downloads.State;
import com.motorola.sharing.util.Logger;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

class ContentDownloadManager {
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getCloudTag("DownloadMgr");
    private static final Executor sExecutor = Executors.newFixedThreadPool(3);
    private static ContentDownloadManager sInstance;
    /* access modifiers changed from: private */
    public Context mContext;
    private final DownloadItemTaskListener mDownloadListener;
    private int mNumFilesCurrentlyBeingDownloaded;

    private class DownloadItemTask implements ContentTransferProgressListener, Runnable {
        private ContentContainer mContainer = null;
        private boolean mContentNotReadyWarningDisplayed = false;
        private boolean mErrorEventSent = false;
        private DownloadItemTaskListener mListener = null;
        private long mShowTransferTakingTooLongDialogTime = 0;
        private boolean mTransferEstimateTooLongWarningDisplayed = false;
        private boolean mTransferTakingTooLongWarningDisplayed = false;

        public DownloadItemTask(ContentContainer container, DownloadItemTaskListener listener) {
            this.mContainer = container;
            this.mListener = listener;
        }

        public void run() {
            while (ContentDownloadManager.this.getNumFilesBeingDownloaded() >= 4) {
                SystemClock.sleep(500);
            }
            ContentDownloadManager.this.incrNumFilesBeingDownloaded();
            if (Logger.isDebuggable()) {
                Logger.d(ContentDownloadManager.TAG, "---------------------");
                Logger.d(ContentDownloadManager.TAG, "DownloadItemTask:");
                Logger.d(ContentDownloadManager.TAG, "  - uid: " + this.mContainer.uid);
                Logger.d(ContentDownloadManager.TAG, "  - userAccount: " + Logger.obfuscate(this.mContainer.userCred.account));
                Logger.d(ContentDownloadManager.TAG, "  - mimeType: " + this.mContainer.mimeType);
                Logger.d(ContentDownloadManager.TAG, "  - origin: " + Logger.obfuscate(this.mContainer.origin));
                Logger.d(ContentDownloadManager.TAG, "  - shareId: " + this.mContainer.shareId);
                Logger.d(ContentDownloadManager.TAG, "  - downloadUrl: " + this.mContainer.url);
                if (this.mContainer.groupInfo != null) {
                    Logger.d(ContentDownloadManager.TAG, "  - groupOwner: " + Logger.obfuscate(this.mContainer.groupInfo.owner));
                    Logger.d(ContentDownloadManager.TAG, "  - groupId: " + this.mContainer.groupInfo.id);
                }
                Logger.d(ContentDownloadManager.TAG, "---------------------");
            }
            HttpHelper.downloadContent(this.mContainer, ContentDownloadManager.this.mContext, 500, this, true);
            ContentDownloadManager.this.decrNumFilesBeingDownloaded();
            if (this.mListener != null) {
                this.mListener.onDownloadCompleted(this.mContainer, this.mErrorEventSent);
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
            return;
         */
        public void onProgressUpdate(ContentContainer container) {
            assertContainer(container);
            synchronized (ContentDownloadManager.this) {
                if (!CloudData.isOperationCanceled(this.mContainer)) {
                    this.mContainer.onContentDownloadEvent(Status.INFO, Substatus.PROGRESS, CloudProvider.getInstance().getEndpoint());
                    long now = System.currentTimeMillis();
                    if (!this.mTransferTakingTooLongWarningDisplayed && this.mTransferEstimateTooLongWarningDisplayed && now > this.mShowTransferTakingTooLongDialogTime && this.mContainer.transferStatus.progress() < 90) {
                        this.mContainer.onContentDownloadEvent(Status.INFO, Substatus.TRANSFER_TAKING_TOO_LONG, CloudProvider.getInstance().getEndpoint());
                        this.mTransferTakingTooLongWarningDisplayed = true;
                    }
                }
            }
        }

        public void onEstimateLongerThanLimit(ContentContainer container) {
            assertContainer(container);
            synchronized (ContentDownloadManager.this) {
                if (!CloudData.isOperationCanceled(this.mContainer)) {
                    if (!this.mTransferEstimateTooLongWarningDisplayed) {
                        Logger.i(ContentDownloadManager.TAG, "Estimated download is too long for shareId: " + this.mContainer.shareId);
                        this.mContainer.onContentDownloadEvent(Status.INFO, Substatus.ESTIMATED_TRANSFER_TIME_TOO_LONG, CloudProvider.getInstance().getEndpoint());
                        this.mShowTransferTakingTooLongDialogTime = System.currentTimeMillis() + this.mContainer.transferStatus.estimatedRemainingTime;
                        this.mTransferEstimateTooLongWarningDisplayed = true;
                    }
                }
            }
        }

        public void onTakingTooLongNotReady(ContentContainer container) {
            assertContainer(container);
            synchronized (ContentDownloadManager.this) {
                if (!CloudData.isOperationCanceled(this.mContainer)) {
                    if (!this.mContentNotReadyWarningDisplayed) {
                        Logger.i(ContentDownloadManager.TAG, "Content is Not Ready for too long for shareId: " + this.mContainer.shareId);
                        this.mContainer.onContentDownloadEvent(Status.INFO, Substatus.NOT_READY_FOR_TOO_LONG, CloudProvider.getInstance().getEndpoint());
                        this.mContentNotReadyWarningDisplayed = true;
                    }
                }
            }
        }

        public void onCancelTransferTakingTooLongNetwork(ContentContainer container) {
            Logger.i(ContentDownloadManager.TAG, "Cancel download due to timeout for shareId: " + container.shareId);
            onError(container, Substatus.TRANSFER_TIMEOUT);
        }

        public void onCancelTransferTakingTooLongNotReady(ContentContainer container) {
            Logger.i(ContentDownloadManager.TAG, "Cancel download due to not ready timeout for shareId: " + container.shareId);
            onError(container, Substatus.AVAILABILITY_TIMEOUT);
        }

        public void onSenderCancelled(ContentContainer container) {
            Logger.i(ContentDownloadManager.TAG, "Cancel download since sharing canceled for shareId: " + container.shareId);
            onError(container, Substatus.SENDER_CANCELED);
        }

        public void onLowStorage(ContentContainer container) {
            onError(container, Substatus.STORAGE_FULL);
        }

        private void onError(ContentContainer container, Substatus substatus) {
            assertContainer(container);
            synchronized (ContentDownloadManager.this) {
                if (!CloudData.isOperationCanceled(this.mContainer)) {
                    this.mContainer.onContentDownloadEvent(Status.ERROR, substatus, CloudProvider.getInstance().getEndpoint());
                    this.mErrorEventSent = true;
                }
            }
        }

        private void assertContainer(ContentContainer container) {
            if (container != this.mContainer) {
                throw new IllegalStateException("wrong container");
            }
        }
    }

    private interface DownloadItemTaskListener {
        void onDownloadCompleted(ContentContainer contentContainer, boolean z);
    }

    static synchronized ContentDownloadManager getInstance() {
        ContentDownloadManager contentDownloadManager;
        synchronized (ContentDownloadManager.class) {
            if (sInstance == null) {
                sInstance = new ContentDownloadManager();
            }
            contentDownloadManager = sInstance;
        }
        return contentDownloadManager;
    }

    private ContentDownloadManager() {
        this.mDownloadListener = new DownloadItemTaskListener() {
            public void onDownloadCompleted(ContentContainer container, boolean errorSent) {
                ContentDownloadManager.this.handleDownloadCompleted(container, errorSent);
            }
        };
        this.mContext = null;
        this.mNumFilesCurrentlyBeingDownloaded = 0;
        this.mContext = GCSP.getContext();
    }

    public synchronized void download(ContentContainer container) {
        if (!CloudData.isOperationCanceled(container)) {
            HistoryManager.getInstance().addDownload(container);
            sExecutor.execute(new DownloadItemTask(container, this.mDownloadListener));
        }
    }

    public synchronized void cancelDownload(ContentContainer container) {
        CloudData.getExtras(container).isOperationCanceled = true;
        HistoryManager.getInstance().setDownloadState(container, State.CANCELED);
        container.onContentDownloadEvent(Status.SUCCESS, Substatus.CANCELED, CloudProvider.getInstance().getEndpoint());
    }

    /* access modifiers changed from: private */
    public synchronized void handleDownloadCompleted(ContentContainer container, boolean errorSent) {
        if (CloudData.isOperationCanceled(container)) {
            container.actionResult.setSuccess(ActionResult.Status.CANCELED);
        } else if (container.path != null) {
            container.actionResult.setSuccess();
            container.onContentDownloadEvent(Status.SUCCESS, Substatus.DOWNLOADED, CloudProvider.getInstance().getEndpoint());
            HistoryManager.getInstance().setDownloadState(container, State.COMPLETED);
        } else {
            if (container.actionResult.getStatus() != ActionResult.Status.FAIL) {
                container.actionResult.setError(Error.DOWNLOAD_FAILED);
            }
            if (!errorSent) {
                container.onContentDownloadEvent(Status.ERROR, container.actionResult.getError() == Error.WIFI_ONLY_ALLOWED ? Substatus.NETWORK : Substatus.GENERAL, CloudProvider.getInstance().getEndpoint());
            }
            HistoryManager.getInstance().setDownloadState(container, State.FAILED);
        }
        CheckinManager.getInstance().logZapEvent(container);
    }

    /* access modifiers changed from: private */
    public synchronized void incrNumFilesBeingDownloaded() {
        this.mNumFilesCurrentlyBeingDownloaded++;
        Logger.i(TAG, "Number files currently being downloaded: " + this.mNumFilesCurrentlyBeingDownloaded);
    }

    /* access modifiers changed from: private */
    public synchronized void decrNumFilesBeingDownloaded() {
        this.mNumFilesCurrentlyBeingDownloaded--;
        Logger.i(TAG, "Number files currently being downloaded: " + this.mNumFilesCurrentlyBeingDownloaded);
    }

    /* access modifiers changed from: private */
    public synchronized int getNumFilesBeingDownloaded() {
        return this.mNumFilesCurrentlyBeingDownloaded;
    }
}
