package com.motorola.zap.app;

import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import com.motorola.sharing.Config;
import com.motorola.sharing.DispatchRuleFactory;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.util.Logger.LogLevel;
import com.motorola.zap.LogUtils;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConfig;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.auth.ZapAuthenticator;
import com.motorola.zap.ui.CircleNotifierReceiver;
import com.motorola.zap.ui.ZapUi;
import java.lang.Thread.UncaughtExceptionHandler;

public class App extends Application {
    private static Context sAppContext = null;
    /* access modifiers changed from: private */
    public UncaughtExceptionHandler mDefaultHandler;

    public static Context getAppContext() {
        return sAppContext;
    }

    public void onCreate() {
        super.onCreate();
        sAppContext = getApplicationContext();
        GCSP.init(sAppContext, new ZapAuthenticator(sAppContext));
        GCSP.getGroupManager().registerGroupListener(ZapUi.getInstance());
        GCSP.setSyncableSettingsListener(new ZapSyncableSettingsListener());
        GCSP.getContentDispatcher().setDefaultRule(DispatchRuleFactory.getCloudOnly());
        GCSP.getConfig().setString(Config.DOWNLOADS_DIRECTORY, ZapUtils.getZapDirectoryPath()).setBoolean(Config.WIFI_ONLY_CONTENT_TRANSFER, Boolean.valueOf(ZapUtils.isWifiOnlyEnabled())).setLogLevel(LogUtils.IS_DEBUGGABLE ? LogLevel.DEBUG : LogLevel.ERROR).setString(Config.MOTO_CHECKIN_ID, ZapConfig.getMotoCheckinGroupId()).setBoolean(Config.MOTO_CHECKIN_ENABLED, Boolean.valueOf(ZapConfig.isMotoCheckinEnabled())).setString(Config.GOOGLE_ANALYTICS_ID, ZapConfig.getGoogleAnalyticsPropertyId()).setBoolean(Config.GOOGLE_ANALYTICS_ENABLED, Boolean.valueOf(ZapConfig.isGoogleAnalyticsEnabled())).setLong(Config.GROUP_INVITE_TTL, (Long) Z.ZZ_SHARED_INVITE_TIMEOUT.read());
        this.mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
            public void uncaughtException(Thread t, Throwable e) {
                ((NotificationManager) App.this.getSystemService("notification")).cancelAll();
                CircleNotifierReceiver.resetCircleWidget(App.this);
                GCSP.getCheckinManager().logException(t, e, true);
                App.this.mDefaultHandler.uncaughtException(t, e);
            }
        });
    }
}
