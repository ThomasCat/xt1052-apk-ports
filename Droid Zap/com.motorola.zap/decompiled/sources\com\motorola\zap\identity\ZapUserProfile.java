package com.motorola.zap.identity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.Profile;
import com.motorola.sharing.ProfileManagerListener;
import com.motorola.sharing.ProfileManagerListener.ProfileEvent;
import com.motorola.sharing.ProfileManagerListener.ProfileEvent.Status;
import com.motorola.sharing.util.GooglePlusProfileUtil;
import com.motorola.zap.R;
import com.motorola.zap.cacheutils.ZapImageCache;

public class ZapUserProfile implements ProfileManagerListener {
    private View mProfileView;

    public ZapUserProfile(View v) {
        this.mProfileView = v;
    }

    public void loadZapProfile(Profile aProfile) {
        if (aProfile.pictureUrl != null) {
            ZapImageCache.getInstance().getImageLoader().get(aProfile.pictureUrl, ImageLoader.getImageListener((ImageView) this.mProfileView.findViewById(R.id.profile_image), R.drawable.ic_default_profile, R.drawable.ic_default_profile));
        }
        ((TextView) this.mProfileView.findViewById(R.id.profile_username)).setText(aProfile.nickname);
        ZapProfileSharedPerfs.ZAP_PROFILE_EMAIL.write(GCSP.getAuthenticator().getAccount());
    }

    public void resetZapProfile() {
        ((ImageView) this.mProfileView.findViewById(R.id.profile_image)).setImageResource(R.drawable.ic_default_profile);
        ((TextView) this.mProfileView.findViewById(R.id.profile_username)).setText(R.string.prof_you);
    }

    public void onProfileEvent(ProfileEvent event) {
        if (event.status == Status.SUCCESS) {
            Profile lProfile = event.profile;
            if (lProfile == null || lProfile.pictureUrl == null || lProfile.nickname == null) {
                GooglePlusProfileUtil.getsInstance().getMyProfile(new GoogleUserProfile(this.mProfileView));
                return;
            }
            if (!((Boolean) ZapProfileSharedPerfs.ZAP_PROFILE_ACTIVATE.read()).booleanValue()) {
                lProfile.anonymous = true;
            }
            loadZapProfile(lProfile);
            ZapProfileSharedPerfs.backupProfile(lProfile);
        }
    }
}
