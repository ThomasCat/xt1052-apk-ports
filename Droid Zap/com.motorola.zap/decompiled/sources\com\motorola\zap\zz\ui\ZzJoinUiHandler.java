package com.motorola.zap.zz.ui;

import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener.GroupEvent;
import com.motorola.sharing.GroupManager.GroupInfo;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.app.GroupInvite;
import com.motorola.zap.app.ZzInviteStore;
import com.motorola.zap.ui.ZapUi;

public class ZzJoinUiHandler extends BaseZzUiHandler {
    /* access modifiers changed from: protected */
    public void handleInfo(GroupEvent ge) {
        ZapUi.vibrate(300);
        ZapUi.playReceivingSound();
    }

    /* access modifiers changed from: protected */
    public void handleOk(GroupEvent ge) {
        int zzInviteUid = getZzInviteUid(ge.groupInfo);
        if (zzInviteUid != -1) {
            cancelZzInviteNotification(zzInviteUid);
            ZzInviteStore.remove(zzInviteUid);
            if (!((Boolean) Z.FEATURE_ENABLED.read()).booleanValue()) {
                ZapUi.getInstance().showToastMessage(getString(R.string.toast_error_app_disabled), ZapUi.getErrorToastIconId(), true, true);
                GCSP.getGroupManager().leave(null);
                return;
            }
            ZapUi.getInstance().showDataWarningDialog();
        }
        updateZzActiveNotification();
    }

    /* access modifiers changed from: protected */
    public void handleError(GroupEvent ge) {
        int zzInviteUid = getZzInviteUid(ge.groupInfo);
        if (!handleCommonError(ge)) {
            ZapUi ui = ZapUi.getInstance();
            switch (ge.subStatus) {
                case WRONG_PIN:
                    ui.showToastMessage(getString(R.string.toast_error_zz_joining_wrong_code), ZapUi.getErrorToastIconId(), true, true);
                    return;
                case GROUP_INACTIVE:
                    if (zzInviteUid == -1) {
                        ui.showToastMessage(getString(R.string.toast_msg_zz_ended), ZapUi.getErrorToastIconId(), true, true);
                        return;
                    }
                    cancelZzInviteNotification(zzInviteUid);
                    ui.showToastMessage(getString(R.string.toast_error_zz_joining_expired), ZapUi.getErrorToastIconId(), true, true);
                    return;
                default:
                    ui.showToastMessage(getString(R.string.toast_error_zz_joining), ZapUi.getErrorToastIconId(), true, true);
                    return;
            }
        }
    }

    private int getZzInviteUid(GroupInfo g) {
        GroupInvite i = ZzInviteStore.get(g.owner, g.id);
        if (i == null) {
            return -1;
        }
        return i.inviteUid;
    }
}
