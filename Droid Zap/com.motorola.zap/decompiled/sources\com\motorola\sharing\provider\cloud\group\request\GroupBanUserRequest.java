package com.motorola.sharing.provider.cloud.group.request;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import com.motorola.gcsp.json.StatusCode;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener.GroupEvent;
import com.motorola.sharing.GroupListener.GroupEvent.Substatus;
import com.motorola.sharing.GroupListener.GroupEvent.Type;
import com.motorola.sharing.GroupManager.GroupContract;
import com.motorola.sharing.GroupManager.GroupContract.Columns;
import com.motorola.sharing.GroupManager.GroupInfo;
import com.motorola.sharing.provider.cloud.HttpHelper;
import com.motorola.sharing.provider.cloud.UrlHelper;
import com.motorola.sharing.provider.cloud.group.GroupHandler;
import com.motorola.sharing.provider.cloud.group.LeaveGroupTracker;
import com.motorola.sharing.util.JsonHelper;
import com.motorola.sharing.util.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class GroupBanUserRequest extends GroupRequest {
    private static final String TAG = Logger.getCloudTag("GroupBanReq");
    private final String mBannedUserId;
    private final String mId;
    private final GroupInfo mOriginalGroupInfo = GroupHandler.getInstance().getGroupInfo();

    public GroupBanUserRequest(String id, String bannedUserId) {
        super(GCSP.getContext(), false);
        this.mId = id;
        this.mBannedUserId = bannedUserId;
    }

    public Type getWhat() {
        return Type.GROUP_BAN_USER;
    }

    public Bundle getBundle() {
        Bundle bundle = new Bundle();
        bundle.putString(GroupEvent.KEY_BANNED_USER_ID, this.mBannedUserId);
        return bundle;
    }

    public void run() {
        Logger.d(TAG, String.format("Running request, retry: %b", new Object[]{Boolean.valueOf(isRetry())}));
        if (!authenticate()) {
            Logger.e(TAG, "Authentication error");
            handleError(Substatus.AUTHENTICATION_ERROR);
            return;
        }
        String requestData = createRequestData();
        if (requestData == null) {
            Logger.e(TAG, "Failed to construct request");
            handleError(Substatus.CLIENT_ERROR);
            return;
        }
        String response = null;
        try {
            Logger.d(TAG, "Sending request to server...");
            response = HttpHelper.sendPostRequest(UrlHelper.updateTargetMember(), requestData, this.mUserToken, this.mUserId, true);
        } catch (Exception e) {
            Logger.e(TAG, "Exception sending request to server: " + e.toString());
        }
        if (response == null) {
            Logger.e(TAG, "Request to server failed");
            handleError(Substatus.NETWORK_ERROR);
            return;
        }
        handleServerResponse(response);
    }

    private String createRequestData() {
        JSONObject jsonObj = new JSONObject();
        try {
            JsonHelper.addCommonRequestFields(jsonObj);
            jsonObj.put("targetType", "group");
            jsonObj.put("targetId", this.mId);
            jsonObj.put("blockUUID", this.mBannedUserId);
            jsonObj.put("status", Columns.BLOCKED);
            Logger.d(TAG, "Server Request:");
            Logger.dump(TAG, jsonObj);
            return jsonObj.toString();
        } catch (JSONException e) {
            Logger.e(TAG, "ERROR generating group JSON request");
            return null;
        }
    }

    private void handleServerResponse(String response) {
        try {
            JSONObject responseJson = new JSONObject(response);
            Logger.d(TAG, "Server Response:");
            Logger.dump(TAG, responseJson);
            StatusCode status = StatusCode.codeOf(responseJson.getString("status"));
            switch (status) {
                case OK:
                    Uri uri = GroupContract.getMembersContentUri();
                    Context context = GCSP.getContext();
                    ContentValues val = new ContentValues();
                    val.put(Columns.BLOCKED, Boolean.valueOf(true));
                    context.getContentResolver().update(uri, val, GroupContract.SELECTION_MEMBER, new String[]{this.mId, this.mBannedUserId});
                    sendOk();
                    return;
                default:
                    Logger.e(TAG, "Request failed with status: " + status);
                    handleError(statusToError(status));
                    return;
            }
        } catch (JSONException e) {
            Logger.e(TAG, "Exception parsing JSON response: " + e);
            handleError(Substatus.SERVER_ERROR);
        }
    }

    private void handleError(Substatus error) {
        sendError(error);
        LeaveGroupTracker.setEnabled(true);
    }

    public GroupInfo getGroupInfo() {
        return this.mOriginalGroupInfo;
    }
}
