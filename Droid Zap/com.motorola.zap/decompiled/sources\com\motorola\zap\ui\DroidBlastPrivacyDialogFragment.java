package com.motorola.zap.ui;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import com.motorola.zap.R;

public class DroidBlastPrivacyDialogFragment extends DialogFragment {

    public interface PrivacyDialogListener {
        void onDialogNegativeClick();

        void onDialogPositiveClick();
    }

    public static DroidBlastPrivacyDialogFragment newInstance(PrivacyDialogListener listener) {
        DroidBlastPrivacyDialogFragment f = new DroidBlastPrivacyDialogFragment();
        f.setTargetFragment((Fragment) listener, 2001);
        return f;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Builder builder = new Builder(getActivity());
        builder.setMessage(R.string.settings_privacy_dialog_message).setTitle(R.string.settings_privacy_dialog_title).setPositiveButton(17039370, new OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ((PrivacyDialogListener) DroidBlastPrivacyDialogFragment.this.getTargetFragment()).onDialogPositiveClick();
            }
        }).setNegativeButton(17039360, new OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ((PrivacyDialogListener) DroidBlastPrivacyDialogFragment.this.getTargetFragment()).onDialogNegativeClick();
            }
        });
        return builder.create();
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        ((PrivacyDialogListener) getTargetFragment()).onDialogNegativeClick();
    }
}
