package com.google.tagmanager;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Build.VERSION;
import com.google.android.gms.common.util.VisibleForTesting;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;

class SimpleNetworkDispatcher implements Dispatcher {
    private final Context ctx;
    private DispatchListener dispatchListener;
    private final HttpClient httpClient;
    private final String userAgent = createUserAgentString("GoogleTagManager", "3.02", VERSION.RELEASE, getUserAgentLanguage(Locale.getDefault()), Build.MODEL, Build.ID);

    public interface DispatchListener {
        void onHitDispatched(Hit hit);

        void onHitPermanentDispatchFailure(Hit hit);

        void onHitTransientDispatchFailure(Hit hit);
    }

    @VisibleForTesting
    SimpleNetworkDispatcher(HttpClient httpClient2, Context ctx2, DispatchListener dispatchListener2) {
        this.ctx = ctx2.getApplicationContext();
        this.httpClient = httpClient2;
        this.dispatchListener = dispatchListener2;
    }

    public boolean okToDispatch() {
        NetworkInfo network = ((ConnectivityManager) this.ctx.getSystemService("connectivity")).getActiveNetworkInfo();
        if (network != null && network.isConnected()) {
            return true;
        }
        Log.v("...no network connectivity");
        return false;
    }

    public void dispatchHits(List<Hit> hits) {
        int maxHits = Math.min(hits.size(), 40);
        boolean firstSend = true;
        for (int i = 0; i < maxHits; i++) {
            Hit hit = (Hit) hits.get(i);
            URL url = getUrl(hit);
            if (url == null) {
                Log.w("No destination: discarding hit.");
                this.dispatchListener.onHitPermanentDispatchFailure(hit);
            } else {
                HttpEntityEnclosingRequest request = constructGtmRequest(url);
                if (request == null) {
                    this.dispatchListener.onHitPermanentDispatchFailure(hit);
                } else {
                    HttpHost targetHost = new HttpHost(url.getHost(), url.getPort(), url.getProtocol());
                    request.addHeader("Host", targetHost.toHostString());
                    logDebugInformation(request);
                    if (firstSend) {
                        try {
                            NetworkReceiver.sendRadioPoweredBroadcast(this.ctx);
                            firstSend = false;
                        } catch (ClientProtocolException e) {
                            Log.w("ClientProtocolException sending hit; discarding hit...");
                            this.dispatchListener.onHitPermanentDispatchFailure(hit);
                        } catch (IOException e2) {
                            Log.w("Exception sending hit: " + e2.getClass().getSimpleName());
                            Log.w(e2.getMessage());
                            this.dispatchListener.onHitTransientDispatchFailure(hit);
                        }
                    }
                    HttpResponse response = this.httpClient.execute(targetHost, request);
                    int statusCode = response.getStatusLine().getStatusCode();
                    HttpEntity entity = response.getEntity();
                    if (entity != null) {
                        entity.consumeContent();
                    }
                    if (statusCode != 200) {
                        Log.w("Bad response: " + response.getStatusLine().getStatusCode());
                        this.dispatchListener.onHitTransientDispatchFailure(hit);
                    } else {
                        this.dispatchListener.onHitDispatched(hit);
                    }
                }
            }
        }
    }

    private HttpEntityEnclosingRequest constructGtmRequest(URL url) {
        URISyntaxException e;
        HttpEntityEnclosingRequest httpEntityEnclosingRequest;
        try {
            httpEntityEnclosingRequest = new BasicHttpEntityEnclosingRequest("GET", url.toURI().toString());
            try {
                httpEntityEnclosingRequest.addHeader("User-Agent", this.userAgent);
            } catch (URISyntaxException e2) {
                e = e2;
                Log.w("Exception sending hit: " + e.getClass().getSimpleName());
                Log.w(e.getMessage());
                return httpEntityEnclosingRequest;
            }
        } catch (URISyntaxException e3) {
            URISyntaxException uRISyntaxException = e3;
            httpEntityEnclosingRequest = null;
            e = uRISyntaxException;
            Log.w("Exception sending hit: " + e.getClass().getSimpleName());
            Log.w(e.getMessage());
            return httpEntityEnclosingRequest;
        }
        return httpEntityEnclosingRequest;
    }

    private void logDebugInformation(HttpEntityEnclosingRequest httpEntityEnclosingRequest) {
        StringBuffer stringBuffer = new StringBuffer();
        for (Header obj : httpEntityEnclosingRequest.getAllHeaders()) {
            stringBuffer.append(obj.toString()).append("\n");
        }
        stringBuffer.append(httpEntityEnclosingRequest.getRequestLine().toString()).append("\n");
        if (httpEntityEnclosingRequest.getEntity() != null) {
            try {
                InputStream content = httpEntityEnclosingRequest.getEntity().getContent();
                if (content != null) {
                    int available = content.available();
                    if (available > 0) {
                        byte[] bArr = new byte[available];
                        content.read(bArr);
                        stringBuffer.append("POST:\n");
                        stringBuffer.append(new String(bArr)).append("\n");
                    }
                }
            } catch (IOException e) {
                Log.v("Error Writing hit to log...");
            }
        }
        Log.v(stringBuffer.toString());
    }

    /* access modifiers changed from: 0000 */
    public String createUserAgentString(String product, String version, String release, String language, String model, String id) {
        return String.format("%s/%s (Linux; U; Android %s; %s; %s Build/%s)", new Object[]{product, version, release, language, model, id});
    }

    static String getUserAgentLanguage(Locale locale) {
        if (locale == null || locale.getLanguage() == null || locale.getLanguage().length() == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(locale.getLanguage().toLowerCase());
        if (!(locale.getCountry() == null || locale.getCountry().length() == 0)) {
            sb.append("-").append(locale.getCountry().toLowerCase());
        }
        return sb.toString();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public URL getUrl(Hit hit) {
        try {
            return new URL(hit.getHitUrl());
        } catch (MalformedURLException e) {
            Log.e("Error trying to parse the GTM url.");
            return null;
        }
    }
}
