package com.motorola.sharing;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.Menu;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.sample.castcompanionlibrary.cast.DataCastManager;
import com.google.sample.castcompanionlibrary.cast.callbacks.DataCastConsumerImpl;
import com.google.sample.castcompanionlibrary.cast.exceptions.NoConnectionException;
import com.google.sample.castcompanionlibrary.cast.exceptions.TransientNetworkDisconnectionException;
import com.motorola.sharing.ContentContainer.Builder;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.ContentShareListener.ContentShareEvent.Substatus;
import com.motorola.sharing.instrumentation.CheckinInterface;
import com.motorola.sharing.instrumentation.CheckinUtil;
import com.motorola.sharing.provider.tv.TVProvider;
import com.motorola.sharing.util.FileUtils;
import com.motorola.sharing.util.Logger;
import com.motorola.zap.R;
import com.motorola.zap.ui.TimeLineFragment;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.Semaphore;
import org.json.JSONException;
import org.json.JSONObject;

public class CastManager {
    private static final String TAG = Logger.getTvTag("CastManager");
    /* access modifiers changed from: private */
    public static CastManager sCastManager = null;
    private Context mAppContext;
    private DataCastConsumerImpl mCastConsumer;
    /* access modifiers changed from: private */
    public ContentContainer mContainer;
    private DataCastManager mDataCastManager;
    /* access modifiers changed from: private */
    public boolean mIsChromecastAvailable;
    /* access modifiers changed from: private */
    public boolean mIsChromecastConnected;
    /* access modifiers changed from: private */
    public ArrayList<CastConnectionListener> mlisterners;

    public static synchronized CastManager getInstance() {
        CastManager castManager;
        synchronized (CastManager.class) {
            if (sCastManager == null) {
                initCastManager();
            }
            castManager = sCastManager;
        }
        return castManager;
    }

    private static void initCastManager() {
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            sCastManager = new CastManager();
            return;
        }
        final Semaphore semaphore = new Semaphore(0);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                try {
                    CastManager.sCastManager = new CastManager();
                } finally {
                    semaphore.release();
                }
            }
        });
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
        }
    }

    private CastManager() {
        this.mDataCastManager = null;
        this.mCastConsumer = null;
        this.mIsChromecastConnected = false;
        this.mIsChromecastAvailable = false;
        this.mlisterners = new ArrayList<>();
        this.mAppContext = GCSP.getContext();
        setupDataCastListener();
    }

    private synchronized DataCastManager getDataCastManager() {
        DataCastManager dataCastManager;
        if (this.mDataCastManager != null) {
            dataCastManager = this.mDataCastManager;
        } else if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(GCSP.getContext()) != 0) {
            dataCastManager = null;
        } else {
            this.mDataCastManager = DataCastManager.initialize(this.mAppContext, "E99745D8", "urn:x-cast:com.google.cast.droidblast.loadimg");
            this.mDataCastManager.addDataCastConsumer(this.mCastConsumer);
            dataCastManager = this.mDataCastManager;
        }
        return dataCastManager;
    }

    /* access modifiers changed from: private */
    public void notifyEvent(Substatus responseStatus) {
        switch (responseStatus) {
            case PROGRESS:
                this.mContainer.onContentShareEvent(Status.INFO, Substatus.PROGRESS, TVProvider.getInstance().getEndpoint());
                return;
            case SHARED:
                this.mContainer.onContentShareEvent(Status.SUCCESS, Substatus.SHARED, TVProvider.getInstance().getEndpoint());
                if (!TextUtils.isEmpty(this.mContainer.tmpPath)) {
                    FileUtils.deleteFile(this.mContainer.tmpPath, this.mAppContext);
                    return;
                }
                return;
            case TRANSFER_FAILED:
                this.mContainer.onContentShareEvent(Status.ERROR, Substatus.TRANSFER_FAILED, TVProvider.getInstance().getEndpoint());
                if (!TextUtils.isEmpty(this.mContainer.tmpPath)) {
                    FileUtils.deleteFile(this.mContainer.tmpPath, this.mAppContext);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void setupDataCastListener() {
        if (this.mCastConsumer == null) {
            this.mCastConsumer = new DataCastConsumerImpl() {
                public void onConnected() {
                    CastManager.this.mIsChromecastConnected = true;
                    Iterator i$ = CastManager.this.mlisterners.iterator();
                    while (i$.hasNext()) {
                        ((CastConnectionListener) i$.next()).onConnected();
                    }
                    CastManager.clearChromecastTempDirectory();
                }

                public void onCastAvailabilityChanged(boolean castPresent) {
                    CastManager.this.mIsChromecastAvailable = castPresent;
                    Iterator i$ = CastManager.this.mlisterners.iterator();
                    while (i$.hasNext()) {
                        ((CastConnectionListener) i$.next()).onCastAvailabilityChanged(castPresent);
                    }
                }

                public void onApplicationConnected(ApplicationMetadata appMetadata, String applicationStatus, String sessionId, boolean wasLaunched) {
                    CastManager.this.mIsChromecastConnected = true;
                    JSONObject init = getInitJson();
                    if (init != null) {
                        CastManager.this.sendDataMessage(init);
                    }
                    Iterator i$ = CastManager.this.mlisterners.iterator();
                    while (i$.hasNext()) {
                        ((CastConnectionListener) i$.next()).onApplicationConnected(appMetadata, applicationStatus, sessionId, wasLaunched);
                    }
                    CastManager.clearChromecastTempDirectory();
                }

                private JSONObject getInitJson() {
                    try {
                        JSONObject obj = new JSONObject();
                        obj.put(TimeLineFragment.TYPE, "init");
                        JSONObject strings = new JSONObject();
                        obj.put("strings", strings);
                        strings.put("share2tv_tutorial_title", GCSP.getContext().getString(R.string.share2tv_tutorial_title));
                        return obj;
                    } catch (JSONException e) {
                        return null;
                    }
                }

                public void onApplicationDisconnected(int errorCode) {
                    CastManager.this.mIsChromecastConnected = false;
                    Iterator i$ = CastManager.this.mlisterners.iterator();
                    while (i$.hasNext()) {
                        ((CastConnectionListener) i$.next()).onApplicationDisconnected(errorCode);
                    }
                    TVProvider.getInstance().stopHttpServer();
                    CastManager.clearChromecastTempDirectory();
                }

                public void onDisconnected() {
                    CastManager.this.mIsChromecastConnected = false;
                    Iterator i$ = CastManager.this.mlisterners.iterator();
                    while (i$.hasNext()) {
                        ((CastConnectionListener) i$.next()).onDisconnected();
                    }
                    TVProvider.getInstance().stopHttpServer();
                    CastManager.clearChromecastTempDirectory();
                }

                public void onApplicationStopFailed(int errorCode) {
                    Iterator i$ = CastManager.this.mlisterners.iterator();
                    while (i$.hasNext()) {
                        ((CastConnectionListener) i$.next()).onApplicationStopFailed(errorCode);
                    }
                }

                public boolean onApplicationConnectionFailed(int errorCode) {
                    Iterator i$ = CastManager.this.mlisterners.iterator();
                    while (i$.hasNext()) {
                        ((CastConnectionListener) i$.next()).onApplicationConnectionFailed(errorCode);
                    }
                    return true;
                }

                public void onApplicationStatusChanged(String appStatus) {
                    Iterator i$ = CastManager.this.mlisterners.iterator();
                    while (i$.hasNext()) {
                        ((CastConnectionListener) i$.next()).onApplicationStatusChanged(appStatus);
                    }
                }

                public void onMessageReceived(CastDevice castDevice, String namespace, String message) {
                    Iterator i$ = CastManager.this.mlisterners.iterator();
                    while (i$.hasNext()) {
                        ((CastConnectionListener) i$.next()).onMessageReceived(castDevice, namespace, message);
                    }
                    if (CastManager.this.mContainer != null) {
                        String[] responseParts = message.split(":");
                        if (responseParts != null && responseParts.length >= 2) {
                            String sharedId = responseParts[0];
                            String status = responseParts[1];
                            if (CastManager.this.mContainer.shareId.equals(sharedId)) {
                                CastManager.this.notifyEvent(Substatus.valueOf(status));
                            }
                        }
                    }
                }

                public void onMessageSendFailed(com.google.android.gms.common.api.Status status) {
                    Iterator i$ = CastManager.this.mlisterners.iterator();
                    while (i$.hasNext()) {
                        ((CastConnectionListener) i$.next()).onMessageSendFailed(status);
                    }
                }

                public void onVolumeChanged(double value, boolean isMute) {
                    Iterator i$ = CastManager.this.mlisterners.iterator();
                    while (i$.hasNext()) {
                        ((CastConnectionListener) i$.next()).onVolumeChanged(value, isMute);
                    }
                }

                public void onRemoved(CastDevice castDevice, String namespace) {
                    Iterator i$ = CastManager.this.mlisterners.iterator();
                    while (i$.hasNext()) {
                        ((CastConnectionListener) i$.next()).onRemoved(castDevice, namespace);
                    }
                }
            };
        }
    }

    /* access modifiers changed from: private */
    public static boolean clearChromecastTempDirectory() {
        File dir = new File((String) Settings.CHROMECAST_DOWNLOADS_DIRECTORY.get());
        if (dir.exists()) {
            File[] files = dir.listFiles();
            if (files != null) {
                for (File file : files) {
                    file.delete();
                }
            }
        }
        dir.delete();
        return true;
    }

    public void sendDataMessage(JSONObject msgObj) {
        DataCastManager castManager = getDataCastManager();
        if (castManager == null) {
            notifyEvent(Substatus.TRANSFER_FAILED);
            return;
        }
        try {
            msgObj.put("version", 1);
        } catch (JSONException e) {
            Logger.e(TAG, "could not add version");
            notifyEvent(Substatus.TRANSFER_FAILED);
        }
        try {
            castManager.sendDataMessage(msgObj.toString(), "urn:x-cast:com.google.cast.droidblast.loadimg");
        } catch (IllegalArgumentException e2) {
            notifyEvent(Substatus.TRANSFER_FAILED);
        } catch (IllegalStateException e3) {
            notifyEvent(Substatus.TRANSFER_FAILED);
        } catch (IOException e4) {
            notifyEvent(Substatus.TRANSFER_FAILED);
        } catch (TransientNetworkDisconnectionException e5) {
            notifyEvent(Substatus.TRANSFER_FAILED);
        } catch (NoConnectionException e6) {
            notifyEvent(Substatus.TRANSFER_FAILED);
        }
    }

    public void addCastButton(Menu menu, int menuResourceId) {
        DataCastManager castManager = getDataCastManager();
        if (castManager != null) {
            castManager.addMediaRouterButton(menu, menuResourceId);
        }
    }

    public void registerCastConnectionListener(CastConnectionListener listener) {
        this.mlisterners.add(listener);
    }

    public void unRegisterCastConnectionListener(CastConnectionListener listener) {
        this.mlisterners.remove(listener);
    }

    public Boolean isChromecastConnected() {
        return Boolean.valueOf(this.mIsChromecastConnected);
    }

    public Boolean isChromecastAvailable() {
        return Boolean.valueOf(this.mIsChromecastAvailable);
    }

    public ContentContainer castContent(ContentContainer container, ContentShareListener listener) throws IllegalStateException {
        if (!this.mIsChromecastConnected) {
            throw new IllegalStateException("Chrome cast not connected");
        }
        this.mContainer = new Builder().setPath(container.path).setMimeType(container.mimeType).setLocality(container.locality).build();
        this.mContainer.putCheckinData(CheckinInterface.KEY_SHARE_TYPE, CheckinUtil.getShareType(container));
        GCSP.getContentDispatcher().shareContent(this.mContainer, listener, null, DispatchRuleFactory.getTVOnly());
        return this.mContainer;
    }
}
