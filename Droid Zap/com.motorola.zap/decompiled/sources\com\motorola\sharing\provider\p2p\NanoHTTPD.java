package com.motorola.sharing.provider.p2p;

import com.google.android.gms.wallet.WalletConstants;
import com.motorola.sharing.Defaults;
import com.motorola.sharing.util.Logger;
import com.motorola.zap.ZapConstants;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.SequenceInputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class NanoHTTPD {
    public static final String MIME_PLAINTEXT = "text/plain";
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getP2pTag("NanoHTTPD");
    /* access modifiers changed from: private */
    public AsyncRunner mAsyncRunner;
    /* access modifiers changed from: private */
    public AtomicInteger mClientCounter;
    private final String mHostname;
    private final int mPort;
    /* access modifiers changed from: private */
    public ServerSocket mServerSocket;
    private Thread mThread;

    public static class AsyncRunner {
        private long requestCount;

        public void exec(Runnable code) {
            this.requestCount++;
            Thread thread = new Thread(code);
            thread.setDaemon(true);
            thread.setName("NanoHttpd Request Processor (#" + this.requestCount + ")");
            thread.start();
        }
    }

    public static class Cookie {
        private String mExpires;
        private String mName;
        private String mValue;

        public Cookie(String name, String value, String expires) {
            this.mName = name;
            this.mValue = value;
            this.mExpires = expires;
        }

        public String getHTTPHeader() {
            return String.format("%s=%s; expires=%s", new Object[]{this.mName, this.mValue, this.mExpires});
        }

        public static String getHTTPTime(int days) {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            calendar.add(5, days);
            return dateFormat.format(calendar.getTime());
        }
    }

    public class CookieHandler implements Iterable<String> {
        private HashMap<String, String> mCookies = new HashMap<>();
        private ArrayList<Cookie> mQueue = new ArrayList<>();

        public CookieHandler(Map<String, String> httpHeaders) {
            String raw = (String) httpHeaders.get("cookie");
            if (raw != null) {
                for (String token : raw.split(";")) {
                    String[] data = token.trim().split("=");
                    if (data.length == 2) {
                        this.mCookies.put(data[0], data[1]);
                    }
                }
            }
        }

        public Iterator<String> iterator() {
            return this.mCookies.keySet().iterator();
        }

        public String read(String name) {
            return (String) this.mCookies.get(name);
        }

        public void set(String name, String value, int expires) {
            this.mQueue.add(new Cookie(name, value, Cookie.getHTTPTime(expires)));
        }

        public void set(Cookie cookie) {
            this.mQueue.add(cookie);
        }

        public void unloadQueue(Response response) {
            Iterator i$ = this.mQueue.iterator();
            while (i$.hasNext()) {
                response.addHeader("Set-Cookie", ((Cookie) i$.next()).getHTTPHeader());
            }
        }
    }

    protected class HTTPSession implements IHTTPSession {
        public static final int BUF_SIZE = 8192;
        private CookieHandler mCookies;
        private Map<String, String> mHeaders;
        private InputStream mInputStream;
        private Method mMethod;
        private final OutputStream mOutputStream;
        private Map<String, String> mParms;
        private int mRLen;
        private int mSplitbyte;
        private final TempFileManager mTempFileManager;
        private String mUri;

        public HTTPSession(TempFileManager tempFileManager, InputStream inputStream, OutputStream outputStream) {
            this.mTempFileManager = tempFileManager;
            this.mInputStream = inputStream;
            this.mOutputStream = outputStream;
        }

        public void execute() throws IOException {
            try {
                byte[] buf = new byte[8192];
                this.mSplitbyte = 0;
                this.mRLen = 0;
                int read = this.mInputStream.read(buf, 0, 8192);
                if (read == -1) {
                    throw new SocketException("NanoHttpd Shutdown");
                }
                while (read > 0) {
                    this.mRLen += read;
                    this.mSplitbyte = findHeaderEnd(buf, this.mRLen);
                    if (this.mSplitbyte > 0) {
                        break;
                    }
                    read = this.mInputStream.read(buf, this.mRLen, 8192 - this.mRLen);
                }
                if (this.mSplitbyte < this.mRLen) {
                    this.mInputStream = new SequenceInputStream(new ByteArrayInputStream(buf, this.mSplitbyte, this.mRLen - this.mSplitbyte), this.mInputStream);
                }
                this.mParms = new HashMap();
                this.mHeaders = new HashMap();
                BufferedReader hin = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(buf, 0, this.mRLen)));
                Map<String, String> pre = new HashMap<>();
                decodeHeader(hin, pre, this.mParms, this.mHeaders);
                this.mMethod = Method.lookup((String) pre.get("method"));
                if (this.mMethod == null) {
                    throw new ResponseException(Status.BAD_REQUEST, "BAD REQUEST: Syntax error.");
                }
                this.mUri = (String) pre.get("uri");
                this.mCookies = new CookieHandler(this.mHeaders);
                Response r = NanoHTTPD.this.serve(this);
                if (r == null) {
                    throw new ResponseException(Status.INTERNAL_ERROR, "SERVER INTERNAL ERROR: Serve() returned a null response.");
                }
                this.mCookies.unloadQueue(r);
                r.setRequestMethod(this.mMethod);
                r.send(this.mOutputStream);
                this.mTempFileManager.clear();
            } catch (SocketException e) {
                throw e;
            } catch (IOException ioe) {
                new Response(Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "SERVER INTERNAL ERROR: IOException: " + ioe.getMessage()).send(this.mOutputStream);
                NanoHTTPD.safeClose((Closeable) this.mOutputStream);
                this.mTempFileManager.clear();
            } catch (ResponseException re) {
                new Response(re.getStatus(), NanoHTTPD.MIME_PLAINTEXT, re.getMessage()).send(this.mOutputStream);
                NanoHTTPD.safeClose((Closeable) this.mOutputStream);
                this.mTempFileManager.clear();
            } catch (Throwable th) {
                this.mTempFileManager.clear();
                throw th;
            }
        }

        public void parseBody(Map<String, String> files) throws IOException, ResponseException {
            BufferedReader in;
            long size;
            RandomAccessFile randomAccessFile = null;
            try {
                randomAccessFile = getTmpBucket();
                if (this.mHeaders.containsKey("content-length")) {
                    size = (long) Integer.parseInt((String) this.mHeaders.get("content-length"));
                } else if (this.mSplitbyte < this.mRLen) {
                    size = (long) (this.mRLen - this.mSplitbyte);
                } else {
                    size = 0;
                }
                byte[] buf = new byte[512];
                while (this.mRLen >= 0 && size > 0) {
                    this.mRLen = this.mInputStream.read(buf, 0, 512);
                    size -= (long) this.mRLen;
                    if (this.mRLen > 0) {
                        randomAccessFile.write(buf, 0, this.mRLen);
                    }
                }
                ByteBuffer fbuf = randomAccessFile.getChannel().map(MapMode.READ_ONLY, 0, randomAccessFile.length());
                randomAccessFile.seek(0);
                in = new BufferedReader(new InputStreamReader(new FileInputStream(randomAccessFile.getFD())));
                try {
                    if (Method.POST.equals(this.mMethod)) {
                        String contentType = ZapConstants.GOOGLE_ACCOUNT_NONE;
                        String contentTypeHeader = (String) this.mHeaders.get("content-type");
                        StringTokenizer st = null;
                        if (contentTypeHeader != null) {
                            st = new StringTokenizer(contentTypeHeader, ",; ");
                            if (st.hasMoreTokens()) {
                                contentType = st.nextToken();
                            }
                        }
                        if (!"multipart/form-data".equalsIgnoreCase(contentType)) {
                            String postLine = ZapConstants.GOOGLE_ACCOUNT_NONE;
                            char[] pbuf = new char[512];
                            for (int read = in.read(pbuf); read >= 0; read = in.read(pbuf)) {
                                if (postLine.endsWith("\r\n")) {
                                    break;
                                }
                                postLine = postLine + String.valueOf(pbuf, 0, read);
                            }
                            decodeParms(postLine.trim(), this.mParms);
                        } else if (st == null || !st.hasMoreTokens()) {
                            throw new ResponseException(Status.BAD_REQUEST, "BAD REQUEST: Content type is multipart/form-data but boundary missing. Usage: GET /example/file.html");
                        } else {
                            String boundaryStartString = "boundary=";
                            String boundary = contentTypeHeader.substring(contentTypeHeader.indexOf(boundaryStartString) + boundaryStartString.length(), contentTypeHeader.length());
                            if (boundary.startsWith("\"") && boundary.endsWith("\"")) {
                                boundary = boundary.substring(1, boundary.length() - 1);
                            }
                            decodeMultipartData(boundary, fbuf, in, this.mParms, files);
                        }
                    } else if (Method.PUT.equals(this.mMethod)) {
                        files.put(Defaults.BASE_FILE_NAME, saveTmpFile(fbuf, 0, fbuf.limit()));
                    }
                    NanoHTTPD.safeClose((Closeable) randomAccessFile);
                    NanoHTTPD.safeClose((Closeable) in);
                } catch (Throwable th) {
                    th = th;
                    NanoHTTPD.safeClose((Closeable) randomAccessFile);
                    NanoHTTPD.safeClose((Closeable) in);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                in = null;
                NanoHTTPD.safeClose((Closeable) randomAccessFile);
                NanoHTTPD.safeClose((Closeable) in);
                throw th;
            }
        }

        private void decodeHeader(BufferedReader in, Map<String, String> pre, Map<String, String> parms, Map<String, String> headers) throws ResponseException {
            String uri;
            try {
                String inLine = in.readLine();
                if (inLine != null) {
                    StringTokenizer st = new StringTokenizer(inLine);
                    if (!st.hasMoreTokens()) {
                        throw new ResponseException(Status.BAD_REQUEST, "BAD REQUEST: Syntax error. Usage: GET /example/file.html");
                    }
                    pre.put("method", st.nextToken());
                    if (!st.hasMoreTokens()) {
                        throw new ResponseException(Status.BAD_REQUEST, "BAD REQUEST: Missing URI. Usage: GET /example/file.html");
                    }
                    String uri2 = st.nextToken();
                    int qmi = uri2.indexOf(63);
                    if (qmi >= 0) {
                        decodeParms(uri2.substring(qmi + 1), parms);
                        uri = NanoHTTPD.this.decodePercent(uri2.substring(0, qmi));
                    } else {
                        uri = NanoHTTPD.this.decodePercent(uri2);
                    }
                    if (st.hasMoreTokens()) {
                        String line = in.readLine();
                        while (line != null && line.trim().length() > 0) {
                            int p = line.indexOf(58);
                            if (p >= 0) {
                                headers.put(line.substring(0, p).trim().toLowerCase(Locale.US), line.substring(p + 1).trim());
                            }
                            line = in.readLine();
                        }
                    }
                    pre.put("uri", uri);
                }
            } catch (IOException ioe) {
                throw new ResponseException(Status.INTERNAL_ERROR, "SERVER INTERNAL ERROR: IOException: " + ioe.getMessage(), ioe);
            }
        }

        private void decodeMultipartData(String boundary, ByteBuffer fbuf, BufferedReader in, Map<String, String> parms, Map<String, String> files) throws ResponseException {
            try {
                int[] bpositions = getBoundaryPositions(fbuf, boundary.getBytes());
                int boundarycount = 1;
                String mpline = in.readLine();
                while (mpline != null) {
                    if (!mpline.contains(boundary)) {
                        throw new ResponseException(Status.BAD_REQUEST, "BAD REQUEST: Content type is multipart/form-data but next chunk does not start with boundary. Usage: GET /example/file.html");
                    }
                    boundarycount++;
                    Map<String, String> item = new HashMap<>();
                    mpline = in.readLine();
                    while (mpline != null && mpline.trim().length() > 0) {
                        int p = mpline.indexOf(58);
                        if (p != -1) {
                            item.put(mpline.substring(0, p).trim().toLowerCase(Locale.US), mpline.substring(p + 1).trim());
                        }
                        mpline = in.readLine();
                    }
                    if (mpline != null) {
                        String contentDisposition = (String) item.get("content-disposition");
                        if (contentDisposition == null) {
                            throw new ResponseException(Status.BAD_REQUEST, "BAD REQUEST: Content type is multipart/form-data but no content-disposition info found. Usage: GET /example/file.html");
                        }
                        StringTokenizer st = new StringTokenizer(contentDisposition, "; ");
                        Map<String, String> disposition = new HashMap<>();
                        while (st.hasMoreTokens()) {
                            String token = st.nextToken();
                            int p2 = token.indexOf(61);
                            if (p2 != -1) {
                                disposition.put(token.substring(0, p2).trim().toLowerCase(Locale.US), token.substring(p2 + 1).trim());
                            }
                        }
                        String pname = (String) disposition.get("name");
                        String pname2 = pname.substring(1, pname.length() - 1);
                        String value = ZapConstants.GOOGLE_ACCOUNT_NONE;
                        if (item.get("content-type") != null) {
                            if (boundarycount <= bpositions.length) {
                                int offset = stripMultipartHeaders(fbuf, bpositions[boundarycount - 2]);
                                files.put(pname2, saveTmpFile(fbuf, offset, (bpositions[boundarycount - 1] - offset) - 4));
                                String value2 = (String) disposition.get("filename");
                                value = value2.substring(1, value2.length() - 1);
                                do {
                                    mpline = in.readLine();
                                    if (mpline == null) {
                                        break;
                                    }
                                } while (!mpline.contains(boundary));
                            } else {
                                throw new ResponseException(Status.INTERNAL_ERROR, "Error processing request");
                            }
                        } else {
                            while (mpline != null && !mpline.contains(boundary)) {
                                mpline = in.readLine();
                                if (mpline != null) {
                                    int d = mpline.indexOf(boundary);
                                    if (d == -1) {
                                        value = value + mpline;
                                    } else {
                                        value = value + mpline.substring(0, d - 2);
                                    }
                                }
                            }
                        }
                        parms.put(pname2, value);
                    }
                }
            } catch (IOException ioe) {
                ResponseException responseException = new ResponseException(Status.INTERNAL_ERROR, "SERVER INTERNAL ERROR: IOException: " + ioe.getMessage(), ioe);
                throw responseException;
            }
        }

        private int findHeaderEnd(byte[] buf, int rlen) {
            for (int splitbyte = 0; splitbyte + 3 < rlen; splitbyte++) {
                if (buf[splitbyte] == 13 && buf[splitbyte + 1] == 10 && buf[splitbyte + 2] == 13 && buf[splitbyte + 3] == 10) {
                    return splitbyte + 4;
                }
            }
            return 0;
        }

        private int[] getBoundaryPositions(ByteBuffer b, byte[] boundary) {
            int matchcount = 0;
            int matchbyte = -1;
            List<Integer> matchbytes = new ArrayList<>();
            int i = 0;
            while (i < b.limit()) {
                if (b.get(i) == boundary[matchcount]) {
                    if (matchcount == 0) {
                        matchbyte = i;
                    }
                    matchcount++;
                    if (matchcount == boundary.length) {
                        matchbytes.add(Integer.valueOf(matchbyte));
                        matchcount = 0;
                        matchbyte = -1;
                    }
                } else {
                    i -= matchcount;
                    matchcount = 0;
                    matchbyte = -1;
                }
                i++;
            }
            int[] ret = new int[matchbytes.size()];
            for (int i2 = 0; i2 < ret.length; i2++) {
                ret[i2] = ((Integer) matchbytes.get(i2)).intValue();
            }
            return ret;
        }

        private String saveTmpFile(ByteBuffer b, int offset, int len) {
            String path = ZapConstants.GOOGLE_ACCOUNT_NONE;
            if (len > 0) {
                FileOutputStream fileOutputStream = null;
                try {
                    TempFile tempFile = this.mTempFileManager.createTempFile();
                    ByteBuffer src = b.duplicate();
                    FileOutputStream fileOutputStream2 = new FileOutputStream(tempFile.getName());
                    try {
                        FileChannel dest = fileOutputStream2.getChannel();
                        src.position(offset).limit(offset + len);
                        dest.write(src.slice());
                        path = tempFile.getName();
                        NanoHTTPD.safeClose((Closeable) fileOutputStream2);
                    } catch (Exception e) {
                        e = e;
                        fileOutputStream = fileOutputStream2;
                        try {
                            System.err.println("Error: " + e.getMessage());
                            NanoHTTPD.safeClose((Closeable) fileOutputStream);
                            return path;
                        } catch (Throwable th) {
                            th = th;
                            NanoHTTPD.safeClose((Closeable) fileOutputStream);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        fileOutputStream = fileOutputStream2;
                        NanoHTTPD.safeClose((Closeable) fileOutputStream);
                        throw th;
                    }
                } catch (Exception e2) {
                    e = e2;
                    System.err.println("Error: " + e.getMessage());
                    NanoHTTPD.safeClose((Closeable) fileOutputStream);
                    return path;
                }
            }
            return path;
        }

        private RandomAccessFile getTmpBucket() {
            try {
                return new RandomAccessFile(this.mTempFileManager.createTempFile().getName(), "rw");
            } catch (Exception e) {
                System.err.println("Error: " + e.getMessage());
                return null;
            }
        }

        private int stripMultipartHeaders(ByteBuffer b, int offset) {
            int i = offset;
            while (i < b.limit()) {
                if (b.get(i) == 13) {
                    i++;
                    if (b.get(i) == 10) {
                        i++;
                        if (b.get(i) == 13) {
                            i++;
                            if (b.get(i) == 10) {
                                break;
                            }
                        } else {
                            continue;
                        }
                    } else {
                        continue;
                    }
                }
                i++;
            }
            return i + 1;
        }

        private void decodeParms(String parms, Map<String, String> p) {
            if (parms == null) {
                p.put("NanoHttpd.QUERY_STRING", ZapConstants.GOOGLE_ACCOUNT_NONE);
                return;
            }
            p.put("NanoHttpd.QUERY_STRING", parms);
            StringTokenizer st = new StringTokenizer(parms, "&");
            while (st.hasMoreTokens()) {
                String e = st.nextToken();
                int sep = e.indexOf(61);
                if (sep >= 0) {
                    p.put(NanoHTTPD.this.decodePercent(e.substring(0, sep)).trim(), NanoHTTPD.this.decodePercent(e.substring(sep + 1)));
                } else {
                    p.put(NanoHTTPD.this.decodePercent(e).trim(), ZapConstants.GOOGLE_ACCOUNT_NONE);
                }
            }
        }

        public final Map<String, String> getParms() {
            return this.mParms;
        }

        public final Map<String, String> getHeaders() {
            return this.mHeaders;
        }

        public final String getUri() {
            return this.mUri;
        }

        public final Method getMethod() {
            return this.mMethod;
        }

        public final InputStream getInputStream() {
            return this.mInputStream;
        }

        public CookieHandler getCookies() {
            return this.mCookies;
        }
    }

    public interface IHTTPSession {
        void execute() throws IOException;

        CookieHandler getCookies();

        Map<String, String> getHeaders();

        InputStream getInputStream();

        Method getMethod();

        Map<String, String> getParms();

        String getUri();

        void parseBody(Map<String, String> map) throws IOException, ResponseException;
    }

    public enum Method {
        GET,
        PUT,
        POST,
        DELETE,
        HEAD;

        static Method lookup(String method) {
            Method[] arr$;
            for (Method m : values()) {
                if (m.toString().equalsIgnoreCase(method)) {
                    return m;
                }
            }
            return null;
        }
    }

    public static class Response {
        private static final byte[] CRLF = "\r\n".getBytes();
        private boolean mChunkedTransfer;
        private InputStream mData;
        private Map<String, String> mHeader = new HashMap();
        private String mMimeType;
        private Method mRequestMethod;
        private Status mStatus;

        public enum Status {
            OK(200, "OK"),
            CREATED(201, "Created"),
            ACCEPTED(202, "Accepted"),
            NO_CONTENT(204, "No Content"),
            PARTIAL_CONTENT(206, "Partial Content"),
            REDIRECT(301, "Moved Permanently"),
            NOT_MODIFIED(304, "Not Modified"),
            BAD_REQUEST(400, "Bad Request"),
            UNAUTHORIZED(401, "Unauthorized"),
            FORBIDDEN(403, "Forbidden"),
            NOT_FOUND(WalletConstants.ERROR_CODE_INVALID_PARAMETERS, "Not Found"),
            RANGE_NOT_SATISFIABLE(416, "Requested Range Not Satisfiable"),
            INTERNAL_ERROR(500, "Internal Server Error");
            
            private final String description;
            private final int requestStatus;

            private Status(int requestStatus2, String description2) {
                this.requestStatus = requestStatus2;
                this.description = description2;
            }

            public String getDescription() {
                return ZapConstants.GOOGLE_ACCOUNT_NONE + this.requestStatus + " " + this.description;
            }
        }

        public Response(Status status, String mimeType, InputStream data) {
            this.mStatus = status;
            this.mMimeType = mimeType;
            this.mData = data;
        }

        public Response(Status status, String mimeType, String txt) {
            ByteArrayInputStream byteArrayInputStream;
            this.mStatus = status;
            this.mMimeType = mimeType;
            if (txt != null) {
                try {
                    byteArrayInputStream = new ByteArrayInputStream(txt.getBytes("UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    Logger.e(NanoHTTPD.TAG, "Unsupported encoding: " + e.getMessage());
                    return;
                }
            } else {
                byteArrayInputStream = null;
            }
            this.mData = byteArrayInputStream;
        }

        public void addHeader(String name, String value) {
            this.mHeader.put(name, value);
        }

        /* access modifiers changed from: private */
        public void send(OutputStream outputStream) {
            Logger.d(NanoHTTPD.TAG, "HTTP serving...");
            String mime = this.mMimeType;
            SimpleDateFormat gmtFrmt = new SimpleDateFormat("E, d MMM yyyy HH:mm:ss 'GMT'", Locale.US);
            gmtFrmt.setTimeZone(TimeZone.getTimeZone("GMT"));
            try {
                if (this.mStatus == null) {
                    throw new Error("sendResponse(): Status can't be null.");
                }
                PrintWriter pw = new PrintWriter(outputStream);
                pw.print("HTTP/1.1 " + this.mStatus.getDescription() + " \r\n");
                if (mime != null) {
                    pw.print("Content-Type: " + mime + "\r\n");
                }
                if (this.mHeader == null || this.mHeader.get("Date") == null) {
                    pw.print("Date: " + gmtFrmt.format(new Date()) + "\r\n");
                }
                if (this.mHeader != null) {
                    for (String key : this.mHeader.keySet()) {
                        pw.print(key + ": " + ((String) this.mHeader.get(key)) + "\r\n");
                    }
                }
                pw.print("Connection: keep-alive\r\n");
                if (this.mRequestMethod == Method.HEAD || !this.mChunkedTransfer) {
                    sendAsFixedLength(outputStream, pw);
                } else {
                    sendAsChunked(outputStream, pw);
                }
                outputStream.flush();
                NanoHTTPD.safeClose((Closeable) this.mData);
                Logger.d(NanoHTTPD.TAG, "HTTP serve done");
            } catch (IOException e) {
                Logger.e(NanoHTTPD.TAG, "Failed to send response to socket: " + e.getMessage());
            }
        }

        private void sendAsChunked(OutputStream outputStream, PrintWriter pw) throws IOException {
            pw.print("Transfer-Encoding: chunked\r\n");
            pw.print("\r\n");
            pw.flush();
            byte[] buff = new byte[65536];
            while (true) {
                int read = this.mData.read(buff);
                if (read > 0) {
                    outputStream.write(String.format("%x\r\n", new Object[]{Integer.valueOf(read)}).getBytes());
                    outputStream.write(buff, 0, read);
                    outputStream.write(CRLF);
                } else {
                    outputStream.write(String.format("0\r\n\r\n", new Object[0]).getBytes());
                    return;
                }
            }
        }

        private void sendAsFixedLength(OutputStream outputStream, PrintWriter pw) throws IOException {
            int pending;
            int i;
            if (this.mData != null) {
                pending = this.mData.available();
            } else {
                pending = 0;
            }
            pw.print("Content-Length: " + pending + "\r\n");
            pw.print("\r\n");
            pw.flush();
            if (this.mRequestMethod != Method.HEAD && this.mData != null) {
                byte[] buff = new byte[65536];
                while (pending > 0) {
                    InputStream inputStream = this.mData;
                    if (pending > 65536) {
                        i = 65536;
                    } else {
                        i = pending;
                    }
                    int read = inputStream.read(buff, 0, i);
                    if (read > 0) {
                        outputStream.write(buff, 0, read);
                        pending -= read;
                    } else {
                        return;
                    }
                }
            }
        }

        public Status getStatus() {
            return this.mStatus;
        }

        public void setStatus(Status status) {
            this.mStatus = status;
        }

        public String getMimeType() {
            return this.mMimeType;
        }

        public void setMimeType(String mimeType) {
            this.mMimeType = mimeType;
        }

        public InputStream getData() {
            return this.mData;
        }

        public void setData(InputStream data) {
            this.mData = data;
        }

        public Method getRequestMethod() {
            return this.mRequestMethod;
        }

        public void setRequestMethod(Method requestMethod) {
            this.mRequestMethod = requestMethod;
        }

        public void setChunkedTransfer(boolean chunkedTransfer) {
            this.mChunkedTransfer = chunkedTransfer;
        }
    }

    public static final class ResponseException extends Exception {
        private final Status status;

        public ResponseException(Status status2, String message) {
            super(message);
            this.status = status2;
        }

        public ResponseException(Status status2, String message, Exception e) {
            super(message, e);
            this.status = status2;
        }

        public Status getStatus() {
            return this.status;
        }
    }

    public static class TempFile {
        private OutputStream mFStream = new FileOutputStream(this.mFile);
        private File mFile;

        public TempFile(String tempdir) throws IOException {
            this.mFile = File.createTempFile("NanoHTTPD-", ZapConstants.GOOGLE_ACCOUNT_NONE, new File(tempdir));
        }

        public void delete() throws Exception {
            NanoHTTPD.safeClose((Closeable) this.mFStream);
            if (!this.mFile.delete()) {
                Logger.e(NanoHTTPD.TAG, "Failed to delete a temp file");
            }
        }

        public String getName() {
            return this.mFile.getAbsolutePath();
        }
    }

    public static class TempFileManager {
        private final String mTempDir = System.getProperty("java.io.tmpdir");
        private final List<TempFile> mTempFiles = new ArrayList();

        public TempFile createTempFile() throws Exception {
            TempFile tempFile = new TempFile(this.mTempDir);
            this.mTempFiles.add(tempFile);
            return tempFile;
        }

        public void clear() {
            for (TempFile file : this.mTempFiles) {
                try {
                    file.delete();
                } catch (Exception e) {
                }
            }
            this.mTempFiles.clear();
        }
    }

    public NanoHTTPD(int port) {
        this(null, port);
    }

    public NanoHTTPD(String hostname, int port) {
        this.mClientCounter = new AtomicInteger(0);
        this.mHostname = hostname;
        this.mPort = port;
        this.mAsyncRunner = new AsyncRunner();
    }

    public int getClientCount() {
        return this.mClientCounter.get();
    }

    private static void safeClose(ServerSocket serverSocket) {
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                Logger.e(TAG, "Failed to close server socket");
            }
        }
    }

    /* access modifiers changed from: private */
    public static void safeClose(Socket socket) {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                Logger.e(TAG, "Failed to close socket");
            }
        }
    }

    /* access modifiers changed from: private */
    public static void safeClose(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                Logger.e(TAG, "Failed to close socket");
            }
        }
    }

    public void start() throws IOException {
        this.mServerSocket = new ServerSocket();
        if (this.mPort == -1) {
            this.mServerSocket.bind(null);
        } else {
            this.mServerSocket.bind(this.mHostname != null ? new InetSocketAddress(this.mHostname, this.mPort) : new InetSocketAddress(this.mPort));
        }
        this.mThread = new Thread(new Runnable() {
            public void run() {
                do {
                    try {
                        final Socket clientSocket = NanoHTTPD.this.mServerSocket.accept();
                        clientSocket.setSoTimeout(10000);
                        final InputStream inputStream = clientSocket.getInputStream();
                        if (inputStream == null) {
                            NanoHTTPD.safeClose(clientSocket);
                        } else {
                            Logger.i(NanoHTTPD.TAG, "Increased client count = " + NanoHTTPD.this.mClientCounter.incrementAndGet());
                            NanoHTTPD.this.mAsyncRunner.exec(new Runnable() {
                                public void run() {
                                    OutputStream outputStream = null;
                                    try {
                                        outputStream = clientSocket.getOutputStream();
                                        HTTPSession session = new HTTPSession(new TempFileManager(), inputStream, outputStream);
                                        while (!clientSocket.isClosed()) {
                                            session.execute();
                                        }
                                    } catch (Exception e) {
                                        if (!(e instanceof SocketException) || !"NanoHttpd Shutdown".equals(e.getMessage())) {
                                            Logger.i(NanoHTTPD.TAG, "SocketException: " + e.getMessage());
                                        }
                                    } finally {
                                        Logger.i(NanoHTTPD.TAG, "Decreased client count = " + NanoHTTPD.this.mClientCounter.decrementAndGet());
                                        NanoHTTPD.safeClose((Closeable) outputStream);
                                        NanoHTTPD.safeClose((Closeable) inputStream);
                                        NanoHTTPD.safeClose(clientSocket);
                                    }
                                }
                            });
                        }
                    } catch (IOException e) {
                        Logger.w(NanoHTTPD.TAG, "IO exception while serving content");
                    }
                } while (!NanoHTTPD.this.mServerSocket.isClosed());
                NanoHTTPD.this.mServerSocket = null;
            }
        });
        this.mThread.setDaemon(true);
        this.mThread.setName("NanoHttpd Main Listener");
        this.mThread.start();
    }

    public void stop() {
        try {
            safeClose(this.mServerSocket);
            this.mThread = null;
        } catch (Exception e) {
            Logger.e(TAG, "Failed to stop HTTP service: " + e.getMessage());
        }
    }

    public final int getListeningPort() {
        if (this.mServerSocket == null) {
            return -1;
        }
        return this.mServerSocket.getLocalPort();
    }

    public final boolean wasStarted() {
        return (this.mServerSocket == null || this.mThread == null) ? false : true;
    }

    public final boolean isAlive() {
        return wasStarted() && !this.mServerSocket.isClosed() && this.mThread.isAlive();
    }

    public Response serve(String uri, Method method, Map<String, String> map, Map<String, String> map2, Map<String, String> map3) {
        return new Response(Status.NOT_FOUND, MIME_PLAINTEXT, "Not Found");
    }

    public Response serve(IHTTPSession session) {
        Map<String, String> files = new HashMap<>();
        Method method = session.getMethod();
        if (Method.PUT.equals(method) || Method.POST.equals(method)) {
            try {
                session.parseBody(files);
            } catch (IOException ioe) {
                return new Response(Status.INTERNAL_ERROR, MIME_PLAINTEXT, "SERVER INTERNAL ERROR: IOException: " + ioe.getMessage());
            } catch (ResponseException re) {
                return new Response(re.getStatus(), MIME_PLAINTEXT, re.getMessage());
            }
        }
        return serve(session.getUri(), method, session.getHeaders(), session.getParms(), files);
    }

    /* access modifiers changed from: protected */
    public String decodePercent(String str) {
        String decoded = null;
        try {
            return URLDecoder.decode(str, "UTF8");
        } catch (UnsupportedEncodingException e) {
            return decoded;
        }
    }
}
