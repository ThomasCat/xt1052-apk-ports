package com.motorola.zap.dialogs;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.Settings.Secure;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.motorola.sharing.util.NetworkUtils;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.ZapConfig;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.location.BlastLocationProvider;
import com.motorola.zap.ui.ZapUi;

public class DialogUserActionRequired extends Activity {
    /* access modifiers changed from: private */
    public static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    /* access modifiers changed from: private */
    public ActionType mActionType = null;
    private boolean mActive = false;
    /* access modifiers changed from: private */
    public long mCancelTransferDialogStartTime = 0;
    private Dialog mDialog = null;
    private BroadcastReceiver mDismissDialogReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ZapConstants.ACTION_FINISH_DIALOG_ACTIVITY) || intent.getAction().equals(ZapConstants.ACTION_FINISH_USER_ACTION_DIALOG_ACTIVITY)) {
                if (DialogUserActionRequired.DBG) {
                    Log.d("Zap-DialogUsrAction", "Dismiss dialog intent");
                }
                ActionType action = (ActionType) DialogUserActionRequired.this.getIntent().getExtras().get(ZapConstants.EXTRA_DIALOG_USER_ACTION_TYPE);
                if (action == null || action == DialogUserActionRequired.this.mActionType) {
                    DialogUserActionRequired.this.finishActivity();
                }
            }
        }
    };
    private int mDurationEstimate = 0;
    private Intent mOriginalIntent = null;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog = null;
    private boolean mTryingToEnableWifi = false;

    public enum ActionType {
        INVALID,
        ENABLE_LOCATION,
        CONNECT_NETWORK,
        CHANGE_WIFI_ONLY,
        FIX_LOCATION_SETTINGS,
        LONG_ESTIMATE_SEND,
        LONG_ESTIMATE_RECEIVE,
        NOT_READY_TAKING_LONG,
        SEND_TAKING_TOO_LONG,
        RECEIVE_TAKING_TOO_LONG
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (DBG) {
            Log.d("Zap-DialogUsrAction", "Activity - onCreate --");
        }
        this.mActionType = (ActionType) getIntent().getExtras().get(ZapConstants.EXTRA_DIALOG_USER_ACTION_TYPE);
        if (this.mActionType == null) {
            throw new IllegalArgumentException("Action Type must be provided");
        }
        if (this.mActionType == ActionType.ENABLE_LOCATION || this.mActionType == ActionType.CONNECT_NETWORK || this.mActionType == ActionType.CHANGE_WIFI_ONLY || this.mActionType == ActionType.FIX_LOCATION_SETTINGS || isCancelTransferDialog()) {
            this.mOriginalIntent = (Intent) getIntent().getExtras().get(ZapConstants.EXTRA_ORIGINAL_INTENT);
            if (this.mOriginalIntent == null) {
            }
        }
        if (isCancelTransferDialog() && this.mActionType != ActionType.NOT_READY_TAKING_LONG) {
            this.mDurationEstimate = getIntent().getIntExtra(ZapConstants.EXTRA_ESTIMATE_TRANSF_DURATION, 0);
            if (this.mDurationEstimate == 0) {
                throw new IllegalArgumentException("Estimate duration must be provided to the dialog");
            }
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(ZapConstants.ACTION_FINISH_DIALOG_ACTIVITY);
        filter.addAction(ZapConstants.ACTION_FINISH_USER_ACTION_DIALOG_ACTIVITY);
        LocalBroadcastManager.getInstance(this).registerReceiver(this.mDismissDialogReceiver, filter);
        createDialog();
    }

    public void onResume() {
        super.onResume();
        if (DBG) {
            Log.d("Zap-DialogUsrAction", "Activity - onResume");
        }
        if (isProblemResolved()) {
            sendOriginalIntent();
            finishActivity();
            return;
        }
        if (DBG) {
            Log.d("Zap-DialogUsrAction", "Problem not resolved yet");
        }
        if (this.mDialog != null) {
            this.mDialog.show();
        }
        setActive(true);
        if (isCancelTransferDialog()) {
            this.mCancelTransferDialogStartTime = System.currentTimeMillis();
        }
        new Thread(new Runnable() {
            public void run() {
                while (DialogUserActionRequired.this.isActive()) {
                    if (DialogUserActionRequired.this.isProblemResolved()) {
                        DialogUserActionRequired.this.sendOriginalIntent();
                        DialogUserActionRequired.this.finishActivity();
                        return;
                    }
                    if (DialogUserActionRequired.this.isCancelTransferDialog() && System.currentTimeMillis() - DialogUserActionRequired.this.mCancelTransferDialogStartTime > 60000) {
                        if (DialogUserActionRequired.DBG) {
                            Log.d("Zap-DialogUsrAction", "Dialog showing for more than a minute - dismissing it");
                        }
                        DialogUserActionRequired.this.finishActivity();
                    }
                    SystemClock.sleep(1000);
                }
            }
        }).start();
    }

    public void onPause() {
        if (DBG) {
            Log.d("Zap-DialogUsrAction", "Activity - onPause");
        }
        setActive(false);
        dismissActionDialog(false);
        super.onPause();
    }

    public void onDestroy() {
        if (DBG) {
            Log.d("Zap-DialogUsrAction", "Activity - onDestroy");
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(this.mDismissDialogReceiver);
        dismissActionDialog(true);
        dismissProgressDialog(true);
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public synchronized boolean isProblemResolved() {
        boolean z = true;
        synchronized (this) {
            if (this.mActionType != ActionType.ENABLE_LOCATION || !BlastLocationProvider.getInstance().isLocationEnabled()) {
                if (this.mActionType != ActionType.CONNECT_NETWORK || !NetworkUtils.hasDataConnection(this)) {
                    if (this.mActionType != ActionType.CHANGE_WIFI_ONLY || (ZapUtils.isWifiOnlyEnabled() && !NetworkUtils.isWifiConnected(this))) {
                        if (this.mActionType == ActionType.FIX_LOCATION_SETTINGS) {
                            if (isTryingToEnableWifi()) {
                                z = false;
                            } else {
                                WifiManager wifiManager = (WifiManager) getSystemService("wifi");
                                BlastLocationProvider loc = BlastLocationProvider.getInstance();
                                if (wifiManager.isWifiEnabled() && loc.isGpsLocationEnabled() && loc.isNetworkLocationEnabled()) {
                                    if (DBG) {
                                        Log.d("Zap-DialogUsrAction", "Wifi and location settings are all enabled - continuing with the action");
                                    }
                                }
                            }
                        }
                        z = false;
                    } else if (DBG) {
                        Log.d("Zap-DialogUsrAction", "Wi-Fi connected or settings changed - continuing with the action");
                    }
                } else if (DBG) {
                    Log.d("Zap-DialogUsrAction", "Network is connected - continuing with the action");
                }
            } else if (DBG) {
                Log.d("Zap-DialogUsrAction", "Location is enabled - continuing with the action");
            }
        }
        return z;
    }

    /* access modifiers changed from: private */
    public void sendOriginalIntent() {
        SystemClock.sleep(300);
        if (this.mOriginalIntent != null) {
            Intent i = this.mOriginalIntent.cloneFilter();
            i.putExtras(this.mOriginalIntent);
            sendBroadcast(i);
        }
    }

    private void createDialog() {
        String title;
        String message;
        String positiveButtonText;
        String negativeButtonText;
        final String positiveButtonAction;
        if (DBG) {
            Log.d("Zap-DialogUsrAction", "createDialog");
        }
        final ZapUi ui = ZapUi.getInstance();
        if (this.mActionType == ActionType.ENABLE_LOCATION) {
            title = getString(R.string.dialog_title_no_location);
            message = getString(R.string.dialog_msg_location_disabled);
            positiveButtonText = getString(R.string.dialog_launch_location_button);
            negativeButtonText = getString(R.string.dialog_cancel_button);
            positiveButtonAction = "android.settings.LOCATION_SOURCE_SETTINGS";
        } else if (this.mActionType == ActionType.CONNECT_NETWORK) {
            title = getString(R.string.dialog_title_no_connection);
            message = getString(R.string.dialog_msg_network_error);
            positiveButtonText = getString(R.string.dialog_launch_device_settings_button);
            negativeButtonText = getString(R.string.dialog_cancel_button);
            positiveButtonAction = "android.settings.SETTINGS";
        } else if (this.mActionType == ActionType.CHANGE_WIFI_ONLY) {
            title = getString(R.string.dialog_title_no_wifi);
            message = getString(R.string.dialog_msg_wifi_only_not_connected);
            positiveButtonText = getString(R.string.dialog_launch_wifi_button);
            negativeButtonText = getString(R.string.dialog_cancel_button);
            positiveButtonAction = "android.settings.WIFI_SETTINGS";
        } else if (this.mActionType == ActionType.FIX_LOCATION_SETTINGS) {
            title = getString(R.string.dialog_title_location_not_determined);
            WifiManager wifiManager = (WifiManager) getSystemService("wifi");
            boolean gpsEnabled = BlastLocationProvider.getInstance().isGpsLocationEnabled();
            boolean networkEnabled = BlastLocationProvider.getInstance().isNetworkLocationEnabled();
            String msg = null;
            if (wifiManager.isWifiEnabled()) {
                if (!gpsEnabled) {
                    msg = getString(R.string.dialog_msg_enable_gps);
                } else if (!networkEnabled) {
                    msg = getString(R.string.dialog_msg_enable_network_location);
                }
            } else if (!gpsEnabled) {
                msg = getString(R.string.dialog_msg_enable_wifi_and_gps);
            } else if (!networkEnabled) {
                msg = getString(R.string.dialog_msg_enable_wifi_and_network);
            } else {
                msg = getString(R.string.dialog_msg_enable_wifi_indoor);
            }
            if (msg == null) {
                msg = getString(R.string.dialog_msg_no_accurate_location);
            }
            message = msg;
            positiveButtonText = getString(R.string.dialog_yes_button);
            negativeButtonText = getString(R.string.dialog_no_button);
            positiveButtonAction = null;
        } else if (this.mActionType == ActionType.LONG_ESTIMATE_SEND) {
            title = getString(R.string.dialog_title_slow_connection);
            message = String.format(getResources().getString(R.string.dialog_msg_slow_connection_confirm_send), new Object[]{Integer.toString(this.mDurationEstimate)});
            positiveButtonText = getString(R.string.dialog_continue_button);
            negativeButtonText = getString(R.string.dialog_stop_button);
            positiveButtonAction = null;
        } else if (this.mActionType == ActionType.LONG_ESTIMATE_RECEIVE) {
            title = getString(R.string.dialog_title_slow_connection);
            message = String.format(getResources().getString(R.string.dialog_msg_slow_connection_confirm_receive), new Object[]{Integer.toString(this.mDurationEstimate)});
            positiveButtonText = getString(R.string.dialog_continue_button);
            negativeButtonText = getString(R.string.dialog_stop_button);
            positiveButtonAction = null;
        } else if (this.mActionType == ActionType.NOT_READY_TAKING_LONG) {
            title = getString(R.string.dialog_title_sender_not_ready);
            message = getResources().getString(R.string.dialog_msg_not_ready_confirm);
            positiveButtonText = getString(R.string.dialog_continue_button);
            negativeButtonText = getString(R.string.dialog_stop_button);
            positiveButtonAction = null;
        } else if (this.mActionType == ActionType.SEND_TAKING_TOO_LONG) {
            title = getString(R.string.dialog_title_slow_connection);
            message = String.format(getResources().getString(R.string.dialog_msg_slow_connection_send_not_done), new Object[]{Integer.toString(this.mDurationEstimate)});
            positiveButtonText = getString(R.string.dialog_continue_button);
            negativeButtonText = getString(R.string.dialog_stop_button);
            positiveButtonAction = null;
        } else if (this.mActionType == ActionType.RECEIVE_TAKING_TOO_LONG) {
            title = getString(R.string.dialog_title_slow_connection);
            message = String.format(getResources().getString(R.string.dialog_msg_slow_connection_receive_not_done), new Object[]{Integer.toString(this.mDurationEstimate)});
            positiveButtonText = getString(R.string.dialog_continue_button);
            negativeButtonText = getString(R.string.dialog_stop_button);
            positiveButtonAction = null;
        } else {
            throw new IllegalArgumentException("Invalid action type");
        }
        Builder dialogBuilder = new Builder(this);
        dialogBuilder.setTitle(title);
        dialogBuilder.setMessage(message);
        dialogBuilder.setPositiveButton(positiveButtonText, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (DialogUserActionRequired.DBG) {
                    Log.d("Zap-DialogUsrAction", "Dialog - Positive Button clicked");
                }
                if (DialogUserActionRequired.this.mActionType == ActionType.FIX_LOCATION_SETTINGS) {
                    DialogUserActionRequired.this.fixLocationSettings();
                } else if (DialogUserActionRequired.this.isCancelTransferDialog()) {
                    if (DialogUserActionRequired.DBG) {
                        Log.d("Zap-DialogUsrAction", "User decided to continue - dismissing dialog");
                    }
                    DialogUserActionRequired.this.finishActivity();
                } else {
                    DialogUserActionRequired.this.startActivity(new Intent(positiveButtonAction));
                }
            }
        });
        if (this.mActionType == ActionType.CHANGE_WIFI_ONLY) {
            String neutralButtonText = getString(R.string.dialog_launch_zap_settings_button);
            String str = ZapConstants.ACTION_BLAST_SETTING_LAUNCH;
            dialogBuilder.setNeutralButton(neutralButtonText, new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (DialogUserActionRequired.DBG) {
                        Log.d("Zap-DialogUsrAction", "Dialog - Neutral Button clicked");
                    }
                    DialogUserActionRequired.this.startActivity(new Intent(ZapConstants.ACTION_BLAST_SETTING_LAUNCH));
                }
            });
        }
        dialogBuilder.setNegativeButton(negativeButtonText, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (DialogUserActionRequired.DBG) {
                    Log.d("Zap-DialogUsrAction", "Dialog - Cancel Button clicked");
                }
                if (!DialogUserActionRequired.this.isCancelTransferDialog() && DialogUserActionRequired.this.mActionType != ActionType.FIX_LOCATION_SETTINGS) {
                    DialogUserActionRequired.this.sendBroadcast(new Intent(ZapConstants.ACTION_SWYPE_RECEIVE_FAILED), "com.motorola.blast.permission.TRIGGER_BLAST_ACTION");
                }
                if (DialogUserActionRequired.this.mActionType == ActionType.FIX_LOCATION_SETTINGS) {
                    ui.showDialogMessage(DialogUserActionRequired.this.getString(R.string.dialog_title_location_not_determined), DialogUserActionRequired.this.getString(R.string.dialog_msg_no_accurate_location), true);
                } else if (DialogUserActionRequired.this.isCancelTransferDialog()) {
                    if (DialogUserActionRequired.DBG) {
                        Log.d("Zap-DialogUsrAction", "User decided to stop");
                    }
                    DialogUserActionRequired.this.sendOriginalIntent();
                }
                DialogUserActionRequired.this.finishActivity();
            }
        });
        dialogBuilder.setOnCancelListener(new OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                if (DialogUserActionRequired.DBG) {
                    Log.d("Zap-DialogUsrAction", "Dialog - onCancel");
                }
                if (!DialogUserActionRequired.this.isCancelTransferDialog() && DialogUserActionRequired.this.mActionType != ActionType.FIX_LOCATION_SETTINGS) {
                    DialogUserActionRequired.this.sendBroadcast(new Intent(ZapConstants.ACTION_SWYPE_RECEIVE_FAILED), "com.motorola.blast.permission.TRIGGER_BLAST_ACTION");
                }
                if (DialogUserActionRequired.this.mActionType == ActionType.FIX_LOCATION_SETTINGS) {
                    ui.showDialogMessage(DialogUserActionRequired.this.getString(R.string.dialog_title_location_not_determined), DialogUserActionRequired.this.getString(R.string.dialog_msg_no_accurate_location), true);
                }
                DialogUserActionRequired.this.finishActivity();
            }
        });
        this.mDialog = dialogBuilder.create();
        this.mDialog.setCanceledOnTouchOutside(false);
    }

    /* access modifiers changed from: private */
    public void fixLocationSettings() {
        final ZapUi ui = ZapUi.getInstance();
        final BlastLocationProvider loc = BlastLocationProvider.getInstance();
        final WifiManager wifiManager = (WifiManager) getSystemService("wifi");
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                DialogUserActionRequired.this.mProgressDialog = ProgressDialog.show(DialogUserActionRequired.this, null, DialogUserActionRequired.this.getString((!loc.isGpsLocationEnabled() || !loc.isNetworkLocationEnabled()) ? R.string.dialog_msg_enabling_loc_settings_progress : R.string.dialog_msg_enabling_wifi_progress));
            }
        });
        new Thread(new Runnable() {
            public void run() {
                if (!wifiManager.isWifiEnabled()) {
                    DialogUserActionRequired.this.setTryingToEnableWifi(true);
                    wifiManager.setWifiEnabled(true);
                    long tryUntil = System.currentTimeMillis() + 10000;
                    while (!wifiManager.isWifiEnabled() && System.currentTimeMillis() < tryUntil) {
                        if (DialogUserActionRequired.DBG) {
                            Log.d("Zap-DialogUsrAction", "Trying to enable WiFi");
                        }
                        SystemClock.sleep(1000);
                    }
                    DialogUserActionRequired.this.setTryingToEnableWifi(false);
                }
                boolean enablingNetworkLocation = false;
                if (wifiManager.isWifiEnabled()) {
                    if (ZapConfig.supportsFullFeature()) {
                        if (DialogUserActionRequired.DBG) {
                            Log.d("Zap-DialogUsrAction", "Full feature app type");
                        }
                        if (!loc.isGpsLocationEnabled()) {
                            if (DialogUserActionRequired.DBG) {
                                Log.d("Zap-DialogUsrAction", "Trying to enable GPS");
                            }
                            Secure.setLocationProviderEnabled(DialogUserActionRequired.this.getContentResolver(), "gps", true);
                        }
                        if (!loc.isNetworkLocationEnabled()) {
                            if (DialogUserActionRequired.DBG) {
                                Log.d("Zap-DialogUsrAction", "Trying to enable Network Location");
                            }
                            Secure.setLocationProviderEnabled(DialogUserActionRequired.this.getContentResolver(), "network", true);
                            enablingNetworkLocation = true;
                        }
                    } else {
                        if (DialogUserActionRequired.DBG) {
                            Log.d("Zap-DialogUsrAction", "Receive-only app type");
                        }
                        if (!loc.isGpsLocationEnabled() || !loc.isNetworkLocationEnabled()) {
                            if (DialogUserActionRequired.DBG) {
                                Log.d("Zap-DialogUsrAction", "Launching Location Settings");
                            }
                            DialogUserActionRequired.this.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
                        }
                    }
                }
                boolean canFinish = false;
                if (!enablingNetworkLocation) {
                    if (wifiManager.isWifiEnabled() && loc.isGpsLocationEnabled() && loc.isNetworkLocationEnabled()) {
                        if (DialogUserActionRequired.DBG) {
                            Log.d("Zap-DialogUsrAction", "WiFi and location settings are now enabled");
                        }
                        DialogUserActionRequired.this.sendOriginalIntent();
                        canFinish = true;
                    } else if (!wifiManager.isWifiEnabled() || ZapConfig.supportsFullFeature()) {
                        if (!wifiManager.isWifiEnabled()) {
                            if (DialogUserActionRequired.DBG) {
                                Log.d("Zap-DialogUsrAction", "Could not enable Wi-Fi");
                            }
                            ui.showDialogMessage(DialogUserActionRequired.this.getString(R.string.dialog_title_location_not_determined), DialogUserActionRequired.this.getString(R.string.dialog_msg_could_not_enable_wifi), true);
                            canFinish = true;
                        } else if (!enablingNetworkLocation) {
                            if (DialogUserActionRequired.DBG) {
                                Log.d("Zap-DialogUsrAction", "Launching Location Settings");
                            }
                            DialogUserActionRequired.this.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
                        }
                    } else if (DialogUserActionRequired.DBG) {
                        Log.d("Zap-DialogUsrAction", "Wifi is enabled - Receive-only app - showing location settings dialog for user to enable");
                    }
                }
                handler.post(new Runnable() {
                    public void run() {
                        DialogUserActionRequired.this.dismissProgressDialog(false);
                    }
                });
                if (canFinish) {
                    DialogUserActionRequired.this.finishActivity();
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public boolean isCancelTransferDialog() {
        return this.mActionType == ActionType.LONG_ESTIMATE_SEND || this.mActionType == ActionType.LONG_ESTIMATE_RECEIVE || this.mActionType == ActionType.NOT_READY_TAKING_LONG || this.mActionType == ActionType.SEND_TAKING_TOO_LONG || this.mActionType == ActionType.RECEIVE_TAKING_TOO_LONG;
    }

    private synchronized void setActive(boolean active) {
        this.mActive = active;
    }

    /* access modifiers changed from: private */
    public synchronized boolean isActive() {
        return this.mActive;
    }

    /* access modifiers changed from: private */
    public synchronized void setTryingToEnableWifi(boolean value) {
        this.mTryingToEnableWifi = value;
    }

    private synchronized boolean isTryingToEnableWifi() {
        return this.mTryingToEnableWifi;
    }

    private synchronized void dismissActionDialog(boolean setNull) {
        if (this.mDialog != null && this.mDialog.isShowing()) {
            this.mDialog.dismiss();
        }
        if (setNull) {
            this.mDialog = null;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void dismissProgressDialog(boolean setNull) {
        if (this.mProgressDialog != null && this.mProgressDialog.isShowing()) {
            this.mProgressDialog.dismiss();
        }
        if (setNull) {
            this.mProgressDialog = null;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void finishActivity() {
        this.mActive = false;
        finish();
        overridePendingTransition(0, 0);
    }
}
