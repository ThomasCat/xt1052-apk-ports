package com.google.android.gms.internal;

import com.google.android.gms.common.api.Api.b;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a.c;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.People.LoadPeopleResult;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.internal.e;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import java.util.Collection;

public final class il implements People {
    private final b<e> Rw;

    private static abstract class a extends com.google.android.gms.plus.Plus.a<LoadPeopleResult> {
        a(b<e> bVar) {
            super(bVar);
        }

        /* renamed from: N */
        public LoadPeopleResult d(final Status status) {
            return new LoadPeopleResult() {
                public String getNextPageToken() {
                    return null;
                }

                public PersonBuffer getPersonBuffer() {
                    return null;
                }

                public Status getStatus() {
                    return status;
                }

                public void release() {
                }
            };
        }
    }

    public il(b<e> bVar) {
        this.Rw = bVar;
    }

    public Person getCurrentPerson(GoogleApiClient googleApiClient) {
        return Plus.a(googleApiClient, this.Rw).getCurrentPerson();
    }

    public PendingResult<LoadPeopleResult> load(GoogleApiClient googleApiClient, final Collection<String> personIds) {
        return googleApiClient.a(new a(this.Rw) {
            /* access modifiers changed from: protected */
            public void a(e eVar) {
                eVar.a((c<LoadPeopleResult>) this, personIds);
            }
        });
    }

    public PendingResult<LoadPeopleResult> load(GoogleApiClient googleApiClient, final String... personIds) {
        return googleApiClient.a(new a(this.Rw) {
            /* access modifiers changed from: protected */
            public void a(e eVar) {
                eVar.c(this, personIds);
            }
        });
    }

    public PendingResult<LoadPeopleResult> loadConnected(GoogleApiClient googleApiClient) {
        return googleApiClient.a(new a(this.Rw) {
            /* access modifiers changed from: protected */
            public void a(e eVar) {
                eVar.j(this);
            }
        });
    }

    public PendingResult<LoadPeopleResult> loadVisible(GoogleApiClient googleApiClient, final int orderBy, final String pageToken) {
        return googleApiClient.a(new a(this.Rw) {
            /* access modifiers changed from: protected */
            public void a(e eVar) {
                eVar.a((c<LoadPeopleResult>) this, orderBy, pageToken);
            }
        });
    }

    public PendingResult<LoadPeopleResult> loadVisible(GoogleApiClient googleApiClient, final String pageToken) {
        return googleApiClient.a(new a(this.Rw) {
            /* access modifiers changed from: protected */
            public void a(e eVar) {
                eVar.i(this, pageToken);
            }
        });
    }
}
