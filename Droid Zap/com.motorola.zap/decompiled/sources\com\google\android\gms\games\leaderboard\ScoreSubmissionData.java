package com.google.android.gms.games.leaderboard;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.internal.ep;
import com.google.android.gms.internal.ep.a;
import com.google.android.gms.internal.er;
import com.google.android.gms.internal.gu;
import java.util.HashMap;

public final class ScoreSubmissionData {
    private static final String[] IH = {"leaderboardId", "playerId", "timeSpan", "hasResult", "rawScore", "formattedScore", "newBest", "scoreTag"};
    private String Gh;
    private String IJ;
    private HashMap<Integer, Result> Jp = new HashMap<>();
    private int yJ;

    public static final class Result {
        public final String formattedScore;
        public final boolean newBest;
        public final long rawScore;
        public final String scoreTag;

        public Result(long rawScore2, String formattedScore2, String scoreTag2, boolean newBest2) {
            this.rawScore = rawScore2;
            this.formattedScore = formattedScore2;
            this.scoreTag = scoreTag2;
            this.newBest = newBest2;
        }

        public String toString() {
            return ep.e(this).a("RawScore", Long.valueOf(this.rawScore)).a("FormattedScore", this.formattedScore).a("ScoreTag", this.scoreTag).a("NewBest", Boolean.valueOf(this.newBest)).toString();
        }
    }

    public ScoreSubmissionData(DataHolder dataHolder) {
        this.yJ = dataHolder.getStatusCode();
        int count = dataHolder.getCount();
        er.x(count == 3);
        for (int i = 0; i < count; i++) {
            int I = dataHolder.I(i);
            if (i == 0) {
                this.IJ = dataHolder.getString("leaderboardId", i, I);
                this.Gh = dataHolder.getString("playerId", i, I);
            }
            if (dataHolder.getBoolean("hasResult", i, I)) {
                a(new Result(dataHolder.getLong("rawScore", i, I), dataHolder.getString("formattedScore", i, I), dataHolder.getString("scoreTag", i, I), dataHolder.getBoolean("newBest", i, I)), dataHolder.getInteger("timeSpan", i, I));
            }
        }
    }

    private void a(Result result, int i) {
        this.Jp.put(Integer.valueOf(i), result);
    }

    public String getLeaderboardId() {
        return this.IJ;
    }

    public String getPlayerId() {
        return this.Gh;
    }

    public Result getScoreResult(int timeSpan) {
        return (Result) this.Jp.get(Integer.valueOf(timeSpan));
    }

    public String toString() {
        a a = ep.e(this).a("PlayerId", this.Gh).a("StatusCode", Integer.valueOf(this.yJ));
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= 3) {
                return a.toString();
            }
            Result result = (Result) this.Jp.get(Integer.valueOf(i2));
            a.a("TimesSpan", gu.aW(i2));
            a.a("Result", result == null ? "null" : result.toString());
            i = i2 + 1;
        }
    }
}
