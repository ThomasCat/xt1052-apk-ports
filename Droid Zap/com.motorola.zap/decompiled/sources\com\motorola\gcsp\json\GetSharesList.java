package com.motorola.gcsp.json;

import java.util.List;

public class GetSharesList implements InputSet {
    public String appId;
    public String appType;
    public Integer appVersion;
    public DeviceInfo deviceInfo;
    public List<ExcludeItem> excludeList;
    public String fromDeviceId;
    public String fromEmail;
    public String fromUserId;
    public Boolean lockedStatus;
    public P2PCred p2pCred;
    public String pin;
    public Integer settingsVersion;
    public ShareFilter shareFilter;
    public String shareStatus;
}
