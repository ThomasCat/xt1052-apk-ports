package com.motorola.zap.zz.ui;

import com.motorola.sharing.GroupListener.GroupEvent;
import com.motorola.zap.R;
import com.motorola.zap.ui.ZapUi;

public class ZzShareInviteUiHandler extends BaseZzUiHandler {
    /* access modifiers changed from: protected */
    public void handleInfo(GroupEvent ge) {
        ZapUi.vibrate(300);
        ZapUi.playSendingSound();
    }

    /* access modifiers changed from: protected */
    public void handleOk(GroupEvent ge) {
        updateZzActiveNotification();
    }

    /* access modifiers changed from: protected */
    public void handleError(GroupEvent ge) {
        if (!handleCommonError(ge)) {
            ZapUi ui = ZapUi.getInstance();
            switch (ge.subStatus) {
                case NOT_ALLOWED:
                    ui.showToastMessage(getString(R.string.toast_error_zz_invite_sharing_not_allowed), ZapUi.getErrorToastIconId(), true, true);
                    return;
                default:
                    ui.showToastMessage(getString(R.string.toast_error_zz_invite_sharing), ZapUi.getErrorToastIconId(), true, true);
                    return;
            }
        }
    }
}
