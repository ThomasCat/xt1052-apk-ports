package com.motorola.zap.app;

import android.util.Log;
import com.motorola.sharing.Profile;
import com.motorola.zap.LogUtils;
import com.motorola.zap.provider.HistoryHandler;

public class ZzInviteStore {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private static final HistoryHandler sHistoryHandler = new HistoryHandler();

    public static GroupInvite put(GroupInvite invite, Profile profile) {
        sHistoryHandler.insertZzInvite(invite, profile);
        if (DBG) {
            Log.d("Zap-ZzInviteStore", "Invite added - uid: " + invite.inviteUid);
        }
        return invite;
    }

    public static GroupInvite get(int zzInviteUid) {
        return sHistoryHandler.getZzInvite(zzInviteUid);
    }

    public static void remove(int zzInviteUid) {
        sHistoryHandler.deleteZzInvite(zzInviteUid);
    }

    public static boolean has(String zzOwner, String zzId) {
        return sHistoryHandler.hasZzInvite(zzOwner, zzId);
    }

    public static GroupInvite get(String zzOwner, String zzId) {
        return sHistoryHandler.getZzInvite(zzOwner, zzId);
    }
}
