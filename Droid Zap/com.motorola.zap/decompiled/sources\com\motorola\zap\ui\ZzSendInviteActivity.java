package com.motorola.zap.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.motorola.sharing.GroupManager.CreateParams;
import com.motorola.sharing.Profile;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.cacheutils.ZapImageCache;
import com.motorola.zap.identity.ZapProfileSharedPerfs;
import com.motorola.zap.ui.ZapGestureTouchListener.AnimationCallback;
import com.motorola.zap.ui.ZapGestureTouchListener.OnAnimationEndListener;
import com.motorola.zap.ui.ZapGestureTouchListener.ZapGestureListener;
import com.motorola.zap.zz.ui.ZzLocationQueue;

public class ZzSendInviteActivity extends Activity {

    public static class ZzSendInvite extends Fragment implements AnimationCallback, ZapGestureListener {
        /* access modifiers changed from: private */
        public final String TAG = ZzSendInvite.class.getSimpleName();
        ConcentricCircles mConcentricCircle = null;
        private String mCurrentZapCameraFile;
        Dialog mDialog = null;
        ImageView mImagePlaceHolder = null;
        private Button mSendInvite = null;
        float mStartY = -1.0f;
        Handler mUiHandler = new Handler();
        ViewGroup mViewGroup;
        EditText mZzCode = null;
        private TextView mZzHelp = null;
        EditText mZzName = null;

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
        }

        public void onResume() {
            super.onResume();
            if (this.mDialog != null && this.mDialog.isShowing()) {
                this.mDialog.dismiss();
            }
            this.mDialog = null;
            if (!((Boolean) Z.FEATURE_ENABLED.read()).booleanValue()) {
                Log.d(this.TAG, "Feature disabled");
                showZapDisabledDialog(getActivity());
            }
        }

        public void onPause() {
            super.onPause();
            if (this.mDialog != null && this.mDialog.isShowing()) {
                this.mDialog.dismiss();
            }
            if (!((Boolean) Z.FEATURE_ENABLED.read()).booleanValue()) {
            }
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_zap_zone_invite, container, false);
            this.mViewGroup = (ViewGroup) rootView;
            initialize((ViewGroup) rootView);
            if (savedInstanceState != null) {
                this.mCurrentZapCameraFile = savedInstanceState.getString("zap_file_name");
            } else {
                populateZZFileds();
            }
            ActionBar actionbar = getActivity().getActionBar();
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setDisplayShowTitleEnabled(true);
            actionbar.setTitle(R.string.edit_invitation);
            return rootView;
        }

        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
            outState.putString("zap_file_name", this.mCurrentZapCameraFile);
        }

        private void populateZZFileds() {
            String name = ZapConstants.GOOGLE_ACCOUNT_NONE;
            Profile currentUserProfile = ZapProfileSharedPerfs.getProfile();
            ImageLoader imageLoader = ZapImageCache.getInstance().getImageLoader();
            if (currentUserProfile != null) {
                if (currentUserProfile.pictureUrl != null) {
                    imageLoader.get(currentUserProfile.pictureUrl, ImageLoader.getImageListener(this.mImagePlaceHolder, R.drawable.ic_zap_inviteaddphoto, R.drawable.ic_zap_inviteaddphoto));
                }
                if (!TextUtils.isEmpty(currentUserProfile.nickname)) {
                    name = currentUserProfile.nickname;
                }
            } else {
                name = getActivity().getString(R.string.my_zap_zone);
            }
            this.mZzName.setText(name);
            this.mZzCode.setText("1234");
        }

        private void initialize(ViewGroup rootView) {
            this.mImagePlaceHolder = (ImageView) rootView.findViewById(R.id.image_placeholder);
            this.mImagePlaceHolder.setEnabled(true);
            this.mZzName = (EditText) rootView.findViewById(R.id.zz_name);
            this.mZzCode = (EditText) rootView.findViewById(R.id.zz_code);
            this.mConcentricCircle = new ConcentricCircles((Context) getActivity(), false);
            this.mConcentricCircle.setDirection(true);
            this.mConcentricCircle.setX(BitmapDescriptorFactory.HUE_RED);
            rootView.addView(this.mConcentricCircle);
            this.mSendInvite = (Button) rootView.findViewById(R.id.zzinvite_send);
            this.mSendInvite.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    ZzSendInvite.this.createZapZone();
                }
            });
            this.mZzHelp = (TextView) rootView.findViewById(R.id.zz_help);
            this.mZzHelp.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    FragmentTransaction ft = ZzSendInvite.this.getFragmentManager().beginTransaction();
                    Fragment prev = ZzSendInvite.this.getFragmentManager().findFragmentByTag("help");
                    if (prev != null) {
                        ft.remove(prev);
                    }
                    ft.addToBackStack(null);
                    ft.replace(16908290, new ZzHelpFragment(), "help").commit();
                }
            });
            ZapGestureTouchListener l = new ViewGroupZapGestureTouchListener(getActivity());
            rootView.setOnTouchListener(l);
            l.registerAnimationCallback(this);
            l.registerZapGestureListener(this);
        }

        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case 16908332:
                    getActivity().finish();
                    break;
            }
            return super.onOptionsItemSelected(item);
        }

        private void setDragUpYPoint(float y, int height) {
            this.mConcentricCircle.setVisibleHeight(height);
            this.mConcentricCircle.invalidate();
        }

        public void animateOnZapGestureInit(View view, View listItem, PointF p1) {
        }

        public void animateOnZapGestureStart(View view, View listItem, PointF p1, PointF p2) {
            this.mStartY = (p1.y < p2.y ? p1.y : p2.y) + (Math.abs(p1.y - p2.y) / 2.0f);
            this.mConcentricCircle.setY(this.mStartY - ((float) this.mConcentricCircle.getHeight()));
            this.mConcentricCircle.setVisibility(0);
        }

        public void animateOnZapGestureMove(View view, View listItem, boolean isUp, PointF p1, PointF p2) {
            float y = (p1.y < p2.y ? p1.y : p2.y) + (Math.abs(p1.y - p2.y) / 2.0f);
            if (this.mStartY - y <= BitmapDescriptorFactory.HUE_RED || !isUp) {
                setDragUpYPoint(this.mStartY, 0);
            } else {
                setDragUpYPoint(this.mStartY, (int) Math.abs(this.mStartY - y));
            }
        }

        public void animateOnZapGestureComplete(View view, View listItem, boolean isUp, float velocity, OnAnimationEndListener animateEndCallback) {
            Log.i(this.TAG, "ZzSendIviteFragment::animateOnZapGestureComplete");
            if (animateEndCallback != null && isUp) {
                animateEndCallback.onAnimationEnd();
            }
            this.mConcentricCircle.setVisibility(4);
        }

        public void animateOnZapGestureCancel(View view, View listItem) {
            this.mConcentricCircle.setVisibility(4);
        }

        public void onVerticalZap(boolean isUp, View view, int position) {
            if (isUp) {
                Log.i(this.TAG, "onVerticalZap, creating a zone");
                createZapZone();
            }
        }

        public void onHorizontalZap(boolean isLeft) {
        }

        /* access modifiers changed from: private */
        public void createZapZone() {
            String code;
            if (TextUtils.isEmpty(this.mZzCode.getText())) {
                code = null;
            } else {
                code = this.mZzCode.getText().toString();
            }
            if (code != null && code.length() > 0 && code.length() < 4) {
                Builder builder = new Builder(getActivity());
                builder.setMessage(getString(R.string.incorrect_input_pin));
                builder.setCancelable(true);
                builder.setNeutralButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
            }
            Profile currentUserProfile = ZapProfileSharedPerfs.getProfile();
            CreateParams params = new CreateParams();
            String zzName = this.mZzName.getText().toString();
            if (currentUserProfile == null || currentUserProfile.anonymous || TextUtils.isEmpty(zzName)) {
                params.name = getActivity().getString(R.string.my_zap_zone);
            } else {
                params.name = this.mZzName.getText().toString();
            }
            params.pin = code;
            params.imagePath = null;
            ZzLocationQueue.getInstance().zzCreate(params);
            getActivity().finish();
        }

        /* access modifiers changed from: 0000 */
        public void showZapDisabledDialog(Context context) {
            Builder dialogBuilder = new Builder(context);
            dialogBuilder.setTitle(R.string.app_name);
            dialogBuilder.setMessage(R.string.enable_zap);
            dialogBuilder.setPositiveButton(getString(R.string.dialog_yes_button), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    ZzSendInvite.this.startActivity(new Intent(ZzSendInvite.this.getActivity(), DroidBlastSettingsActivity.class));
                }
            });
            dialogBuilder.setNegativeButton(getString(R.string.dialog_no_button), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    ZzSendInvite.this.getActivity().finish();
                }
            });
            dialogBuilder.setOnCancelListener(new OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    Log.d(ZzSendInvite.this.TAG, "Dialog - onCancel");
                    ZzSendInvite.this.getActivity().finish();
                }
            });
            this.mDialog = dialogBuilder.create();
            this.mDialog.setCanceledOnTouchOutside(false);
            this.mDialog.show();
        }
    }

    public static class ZzSendInviteTutorial extends Fragment {
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View lZapZoneInfoView = inflater.inflate(R.layout.zap_zone_menu_tutorial, container, false);
            ((Button) lZapZoneInfoView.findViewById(R.id.getStarted)).setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    FragmentManager fm = ZzSendInviteTutorial.this.getFragmentManager();
                    Z.SHOW_ZZ_MENU_TUTORIAL.write(Boolean.valueOf(false));
                    fm.beginTransaction().add(16908290, new ZzSendInvite()).commit();
                }
            });
            return lZapZoneInfoView;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fm = getFragmentManager();
        if (fm.findFragmentById(16908290) == null && ((Boolean) Z.SHOW_ZZ_MENU_TUTORIAL.read()).booleanValue()) {
            fm.beginTransaction().add(16908290, new ZzSendInviteTutorial()).commit();
        } else if (fm.findFragmentById(16908290) == null && fm.findFragmentById(16908290) == null) {
            fm.beginTransaction().add(16908290, new ZzSendInvite()).commit();
        }
    }
}
