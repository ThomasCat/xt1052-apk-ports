package com.google.tagmanager;

interface HitSender {
    boolean sendHit(String str);
}
