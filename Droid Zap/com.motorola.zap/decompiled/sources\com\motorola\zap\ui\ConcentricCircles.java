package com.motorola.zap.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.NinePatchDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import com.motorola.zap.R;

public class ConcentricCircles extends View {
    public static final int VIEW_MAX_HEIGHT = 290;
    private Paint mCirPaint = null;
    int mCircle1XPosition = 0;
    int mCircle2XPosition = 0;
    Bitmap mCircleBm = null;
    private int mCircleRadius;
    private int mCircleWidth = 0;
    /* access modifiers changed from: private */
    public int mHeight = (this.mCircleRadius * 2);
    private boolean mIsFromGallery = false;
    private boolean mIsHeadTop = false;
    private boolean mIsStaticSize = true;
    private Paint mRectPaint = null;
    NinePatchDrawable mSliderBitmap = null;
    Rect mSliderRect = null;

    public class DraggerViewAnimation extends Animation {
        int mFromHeight = 0;
        int mToHeight = 0;

        DraggerViewAnimation(int fromHeight, int toHeight) {
            this.mFromHeight = fromHeight;
            this.mToHeight = toHeight;
            ConcentricCircles.this.setAnimation(this);
            setDuration(2000);
            setRepeatMode(1);
            setFillAfter(false);
            setFillBefore(true);
            setFillEnabled(false);
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            ConcentricCircles.this.mHeight = (int) (((float) this.mFromHeight) + (((float) this.mToHeight) * interpolatedTime));
            ConcentricCircles.this.requestLayout();
            if (interpolatedTime == 1.0f) {
                ConcentricCircles.this.mHeight = this.mFromHeight;
            }
        }

        public boolean willChangeBounds() {
            return true;
        }
    }

    public ConcentricCircles(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ConcentricCircles(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ConcentricCircles, 0, 0);
        try {
            this.mIsFromGallery = a.getBoolean(0, false);
        } catch (Exception e) {
        } finally {
            a.recycle();
        }
        init();
    }

    public ConcentricCircles(Context context) {
        super(context);
        init();
    }

    public ConcentricCircles(Context context, boolean isStaticSize) {
        super(context);
        this.mIsStaticSize = isStaticSize;
        init();
    }

    public boolean isFromGallery() {
        return this.mIsFromGallery;
    }

    public void setFromGallery(boolean isFromGallery) {
        this.mIsFromGallery = isFromGallery;
        invalidate();
        requestLayout();
    }

    public void makeCirclesToStrech(boolean canStreach) {
        this.mIsStaticSize = !canStreach;
    }

    private void init() {
        this.mRectPaint = new Paint();
        this.mRectPaint.setColor(-1996488705);
        this.mCirPaint = new Paint();
        this.mCirPaint.setXfermode(new PorterDuffXfermode(Mode.SRC_OVER));
        if (isFromGallery()) {
            this.mCircleBm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_zap_circle_gallery);
        } else {
            this.mCircleBm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_zap_circles);
        }
        this.mSliderBitmap = (NinePatchDrawable) getResources().getDrawable(R.drawable.ic_zap_slider);
        this.mSliderBitmap.setAlpha(136);
        this.mCircleRadius = this.mCircleBm.getWidth() / 2;
        this.mSliderRect = new Rect();
        this.mCircleWidth = this.mCircleBm.getWidth();
        this.mHeight = this.mCircleRadius * 2;
    }

    public int getCircleWidth() {
        return this.mCircleWidth;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int height = getResources().getDisplayMetrics().heightPixels;
        int width = getResources().getDisplayMetrics().widthPixels;
        float scale = getResources().getDisplayMetrics().density;
        this.mCircle1XPosition = ((int) ((((float) width) - (100.0f * scale)) - ((float) this.mCircleWidth))) / 2;
        this.mCircle2XPosition = (int) (((float) this.mCircle1XPosition) + (100.0f * scale));
        if (this.mIsStaticSize) {
            setMeasuredDimension(width, this.mCircleWidth);
        } else {
            setMeasuredDimension(width, height);
        }
    }

    public int getVisibleHeight() {
        return this.mHeight;
    }

    public void setVisibleHeight(int height) {
        if (height < getCircleWidth()) {
            this.mHeight = getCircleWidth();
        } else {
            this.mHeight = height;
        }
    }

    /* access modifiers changed from: protected */
    public int getSuggestedMinimumWidth() {
        return this.mCircleRadius * 2;
    }

    /* access modifiers changed from: protected */
    public int getSuggestedMinimumHeight() {
        return this.mCircleRadius * 2;
    }

    public void onDraw(Canvas canvas) {
        float ht = (float) getMeasuredHeight();
        float centerY = (float) (this.mHeight - this.mCircleRadius);
        if (this.mIsHeadTop) {
            centerY = (ht - ((float) this.mHeight)) + ((float) this.mCircleRadius);
            this.mSliderRect.set(this.mCircle1XPosition, ((int) ht) - this.mHeight, this.mCircle1XPosition + getCircleWidth(), (int) ht);
            this.mSliderBitmap.setBounds(this.mSliderRect);
            this.mSliderBitmap.draw(canvas);
            this.mSliderRect.set(this.mCircle2XPosition, ((int) ht) - this.mHeight, this.mCircle2XPosition + getCircleWidth(), (int) ht);
            this.mSliderBitmap.setBounds(this.mSliderRect);
            this.mSliderBitmap.draw(canvas);
        } else {
            this.mSliderRect.set(this.mCircle1XPosition, 0, this.mCircle1XPosition + getCircleWidth(), this.mHeight);
            this.mSliderBitmap.setBounds(this.mSliderRect);
            this.mSliderBitmap.draw(canvas);
            this.mSliderRect.set(this.mCircle2XPosition, 0, this.mCircle2XPosition + getCircleWidth(), this.mHeight);
            this.mSliderBitmap.setBounds(this.mSliderRect);
            this.mSliderBitmap.draw(canvas);
        }
        canvas.drawBitmap(this.mCircleBm, (float) this.mCircle1XPosition, centerY - ((float) this.mCircleRadius), this.mCirPaint);
        canvas.drawBitmap(this.mCircleBm, (float) this.mCircle2XPosition, centerY - ((float) this.mCircleRadius), this.mCirPaint);
    }

    public void setDirection(boolean isUp) {
        this.mIsHeadTop = isUp;
    }

    public Animation getCustomAnimation(int fromHeight, int toHeight) {
        return new DraggerViewAnimation(fromHeight, toHeight);
    }
}
