package com.motorola.sharing.util.auth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.motorola.sharing.util.Logger;

public class UserRecoveryActivity extends Activity {
    public static final String ACTION_RECOVERY_RESULT = "com.motorola.sharing.util.auth.ACTION_RECOVERY_RESULT";
    private static final boolean DBG = Logger.isDebuggable();
    public static final String EXTRA_AUTHORIZED = "authorized";
    public static final String EXTRA_RECOVERY_INTENT = "recoveryIntent";
    private static final String TAG = Logger.getGcspTag("AuthUserRecovery");
    private boolean mIsWaiting = false;
    private Intent mRecoveryIntent = null;
    private boolean mResultSent = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (DBG) {
            Log.d(TAG, "onCreate");
        }
        this.mRecoveryIntent = (Intent) getIntent().getParcelableExtra(EXTRA_RECOVERY_INTENT);
        if (this.mRecoveryIntent == null) {
            if (Log.isLoggable(TAG, 6)) {
                Log.e(TAG, "User recovery intent is missing");
            }
            sendResult(false);
            finish();
            return;
        }
        if (savedInstanceState != null) {
            this.mIsWaiting = savedInstanceState.getBoolean("isWaiting");
            if (DBG) {
                Log.d(TAG, "isWaiting: " + this.mIsWaiting);
            }
        }
        if (!this.mIsWaiting) {
            startActivityForResult(this.mRecoveryIntent, 1);
            this.mIsWaiting = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        if (DBG) {
            Log.d(TAG, "onSaveInstanceState - isWaiting: " + this.mIsWaiting);
        }
        outState.putBoolean("isWaiting", this.mIsWaiting);
        super.onSaveInstanceState(outState);
    }

    public void onDestroy() {
        if (DBG) {
            Log.d(TAG, "onDestroy");
        }
        if (!this.mResultSent && !this.mIsWaiting) {
            sendResult(false);
        }
        super.onDestroy();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        boolean z = true;
        if (DBG) {
            Log.d(TAG, "onActivityResult - requestCode: 1 - resultCode: " + resultCode);
        }
        if (requestCode == 1) {
            this.mIsWaiting = false;
            if (resultCode != -1) {
                z = false;
            }
            sendResult(z);
            finish();
        }
    }

    private void sendResult(boolean authorized) {
        if (DBG) {
            Log.d(TAG, "sendResult - authorized: " + authorized);
        }
        this.mResultSent = true;
        Intent intent = new Intent(ACTION_RECOVERY_RESULT);
        intent.putExtra(EXTRA_AUTHORIZED, authorized);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }
}
