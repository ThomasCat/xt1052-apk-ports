package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.d;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

class ch extends aj {
    private static final String ID = a.REGEX_GROUP.toString();
    private static final String Wc = b.ARG0.toString();
    private static final String Wd = b.ARG1.toString();
    private static final String We = b.IGNORE_CASE.toString();
    private static final String Wf = b.GROUP.toString();

    public ch() {
        super(ID, Wc, Wd);
    }

    public boolean iy() {
        return true;
    }

    public d.a u(Map<String, d.a> map) {
        int i;
        d.a aVar = (d.a) map.get(Wc);
        d.a aVar2 = (d.a) map.get(Wd);
        if (aVar == null || aVar == di.ku() || aVar2 == null || aVar2 == di.ku()) {
            return di.ku();
        }
        int i2 = 64;
        if (di.n((d.a) map.get(We)).booleanValue()) {
            i2 = 66;
        }
        d.a aVar3 = (d.a) map.get(Wf);
        if (aVar3 != null) {
            Long l = di.l(aVar3);
            if (l == di.kp()) {
                return di.ku();
            }
            i = l.intValue();
            if (i < 0) {
                return di.ku();
            }
        } else {
            i = 1;
        }
        try {
            String j = di.j(aVar);
            String str = null;
            Matcher matcher = Pattern.compile(di.j(aVar2), i2).matcher(j);
            if (matcher.find() && matcher.groupCount() >= i) {
                str = matcher.group(i);
            }
            return str == null ? di.ku() : di.r(str);
        } catch (PatternSyntaxException e) {
            return di.ku();
        }
    }
}
