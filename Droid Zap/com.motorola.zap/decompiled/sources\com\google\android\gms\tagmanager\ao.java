package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.d;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

class ao extends aj {
    private static final String ID = a.HASH.toString();
    private static final String US = b.ARG0.toString();
    private static final String UU = b.INPUT_FORMAT.toString();
    private static final String UY = b.ALGORITHM.toString();

    public ao() {
        super(ID, US);
    }

    private byte[] c(String str, byte[] bArr) throws NoSuchAlgorithmException {
        MessageDigest instance = MessageDigest.getInstance(str);
        instance.update(bArr);
        return instance.digest();
    }

    public boolean iy() {
        return true;
    }

    public d.a u(Map<String, d.a> map) {
        byte[] aX;
        d.a aVar = (d.a) map.get(US);
        if (aVar == null || aVar == di.ku()) {
            return di.ku();
        }
        String j = di.j(aVar);
        d.a aVar2 = (d.a) map.get(UY);
        String j2 = aVar2 == null ? "MD5" : di.j(aVar2);
        d.a aVar3 = (d.a) map.get(UU);
        String j3 = aVar3 == null ? "text" : di.j(aVar3);
        if ("text".equals(j3)) {
            aX = j.getBytes();
        } else if ("base16".equals(j3)) {
            aX = j.aX(j);
        } else {
            bh.t("Hash: unknown input format: " + j3);
            return di.ku();
        }
        try {
            return di.r(j.d(c(j2, aX)));
        } catch (NoSuchAlgorithmException e) {
            bh.t("Hash: unknown algorithm: " + j2);
            return di.ku();
        }
    }
}
