package com.motorola.sharing;

import com.motorola.sharing.DispatchRule.Params;

public class DispatchRuleFactory {
    public static DispatchRule getCloudOnly() {
        return CloudOnlyDispatchRule.getInstance(null);
    }

    public static DispatchRule getTVOnly() {
        return TVOnlyDispatchRule.getInstance();
    }

    public static DispatchRule getCloudOrP2p(String mimeTypePrefix, int sizeThreshold) {
        Params params = new Params();
        params.put("mimeTypePrefix", mimeTypePrefix);
        params.put("sizeThreshold", Integer.valueOf(sizeThreshold));
        return CloudOrP2pDispatchRule.getInstance(params);
    }
}
