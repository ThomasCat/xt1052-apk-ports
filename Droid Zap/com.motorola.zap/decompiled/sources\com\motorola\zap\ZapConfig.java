package com.motorola.zap;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Build;
import android.util.Log;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.util.HistoryManager.Shares.C;
import com.motorola.zap.app.App;
import com.motorola.zap.app.MotorolaSettings;
import com.motorola.zap.provider.ZapContract;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class ZapConfig {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private static String MODEL_MANUFACTURER = "MOTOROLA";
    private static String QUANTUM_MODEL_NUM = "XT1254";
    private static Boolean sIsProductionBuild = null;

    public static boolean supportsFullFeature() {
        return App.getAppContext().getPackageManager().hasSystemFeature(ZapContract.AUTHORITY);
    }

    public static boolean supportsQuantumFeature() {
        return (Build.MANUFACTURER != null ? Build.MANUFACTURER.toUpperCase() : ZapConstants.GOOGLE_ACCOUNT_NONE).equals(MODEL_MANUFACTURER) && (Build.MODEL != null ? Build.MODEL.toUpperCase() : ZapConstants.GOOGLE_ACCOUNT_NONE).equals(QUANTUM_MODEL_NUM);
    }

    public static boolean supportsChromecast() {
        return supportsQuantumFeature() || hasMotoAccount();
    }

    public static boolean supportsZaptoscreen() {
        return App.getAppContext().getPackageManager().hasSystemFeature("com.motorola.zapvoice");
    }

    public static boolean supportsZapZoneCreation() {
        return supportsFullFeature() || isLegacyDroid() || hasMotoAccount();
    }

    public static boolean supportVideoInZapZone() {
        return false;
    }

    public static boolean hasMotoAccount() {
        Context context = App.getAppContext();
        if (context == null) {
            return false;
        }
        AccountManager manager = (AccountManager) context.getSystemService(C.ACCOUNT);
        if (manager == null) {
            return false;
        }
        Account[] list = manager.getAccountsByType(GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
        if (list == null) {
            return false;
        }
        for (Account acc : list) {
            if (acc.name.endsWith("@motorola.com")) {
                return true;
            }
        }
        return false;
    }

    public static boolean isLegacyDroid() {
        String[] arr$;
        String model = Build.MODEL != null ? Build.MODEL.toUpperCase() : ZapConstants.GOOGLE_ACCOUNT_NONE;
        String product = Build.PRODUCT != null ? Build.PRODUCT.toUpperCase() : ZapConstants.GOOGLE_ACCOUNT_NONE;
        String modelList = (String) Z.LEGACY_DROIDS_DEVICES_LIST.read();
        if (modelList == null || modelList.isEmpty()) {
            return false;
        }
        String modelList2 = modelList.toUpperCase();
        if (modelList2.contains(ZapConstants.SUPPORTS_SENDING_ALL_DEVICES)) {
            return true;
        }
        for (String supportedModel : modelList2.split(",")) {
            if (model.contains(supportedModel) || product.contains(supportedModel)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isMotoCheckinEnabled() {
        return true;
    }

    public static String getMotoCheckinGroupId() {
        return "MOT_DROID_ZAP";
    }

    public static boolean isGoogleAnalyticsSupported() {
        return true;
    }

    public static boolean isGoogleAnalyticsEnabled() {
        return isGoogleAnalyticsSupported() && ((Boolean) Z.SEND_GOOGLE_ANALYTICS_REPORTS.read()).booleanValue();
    }

    public static String getGoogleAnalyticsPropertyId() {
        return isProductionBuild() ? "UA-45895552-1" : "UA-45632229-1";
    }

    public static boolean isMotoDebugBuild() {
        return Build.MANUFACTURER.toUpperCase(Locale.ROOT).contains("MOTOROLA") && (Build.TYPE.equals("eng") || Build.TYPE.equals("userdebug"));
    }

    public static boolean isProductionBuild() {
        if (sIsProductionBuild == null) {
            List<String> fingerprints = getCertificatesSHA1();
            if (fingerprints == null) {
                throw new IllegalStateException("Unable to read certificate");
            }
            sIsProductionBuild = Boolean.valueOf(false);
            Iterator i$ = fingerprints.iterator();
            while (true) {
                if (!i$.hasNext()) {
                    break;
                }
                if ("D670D099A3A6E657BA839DCE0746FE1A31F4547E".equalsIgnoreCase((String) i$.next())) {
                    sIsProductionBuild = Boolean.valueOf(true);
                    break;
                }
            }
            if (DBG) {
                Log.d("Zap-Config", "Production build: " + sIsProductionBuild);
            }
        }
        return sIsProductionBuild.booleanValue();
    }

    public static boolean isMotoDevice() {
        return Build.MANUFACTURER.toUpperCase(Locale.ROOT).contains("MOTOROLA");
    }

    private static List<String> getCertificatesSHA1() {
        Context context = GCSP.getContext();
        try {
            PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 64);
            try {
                MessageDigest md = MessageDigest.getInstance("SHA1");
                List<String> fingerprints = new ArrayList<>();
                for (Signature signature : pi.signatures) {
                    byte[] sha1 = md.digest(signature.toByteArray());
                    StringBuffer sb = new StringBuffer();
                    for (int i = 0; i < sha1.length; i++) {
                        sb.append(String.format("%02X", new Object[]{Integer.valueOf(sha1[i] & 255)}));
                    }
                    if (DBG) {
                        Log.d("Zap-Config", "Certificate SHA1: " + sb.toString());
                    }
                    fingerprints.add(sb.toString());
                }
                if (fingerprints.size() <= 0) {
                    return null;
                }
                return fingerprints;
            } catch (NoSuchAlgorithmException e) {
                if (Log.isLoggable("Zap-Config", 6)) {
                    Log.e("Zap-Config", "Got exception: " + e);
                }
                return null;
            }
        } catch (NameNotFoundException e2) {
            return null;
        }
    }

    protected static boolean sendGoogleAnalyticsReportsByDefault() {
        Context context = App.getAppContext();
        if (context != null) {
            return context.getResources().getBoolean(R.bool.send_ga_reports_default);
        }
        return false;
    }

    public static synchronized void savePrivacyAgreed(boolean agreed) {
        synchronized (ZapConfig.class) {
            Z.PRIVACY_AGREED.write(Boolean.valueOf(agreed));
            if (supportsFullFeature()) {
                MotorolaSettings.getInstance().putInt("privacy_droid_blast", agreed ? 1 : 0);
            }
        }
    }
}
