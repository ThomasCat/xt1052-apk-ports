package com.google.android.gms.analytics;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.HitTypes;
import com.google.android.gms.internal.er;
import com.motorola.zap.ZapConstants;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class Tracker {
    private final TrackerHandler up;
    private final Map<String, String> uq;
    private ad ur;
    private final h us;
    private final ae ut;
    private final g uu;
    private boolean uv;
    /* access modifiers changed from: private */
    public a uw;
    /* access modifiers changed from: private */
    public aj ux;

    private class a implements a {
        private i rJ;
        /* access modifiers changed from: private */
        public boolean uA = false;
        private boolean uB = false;
        private int uC = 0;
        private long uD = -1;
        private boolean uE = false;
        private long uF;
        private Timer uy;
        private TimerTask uz;

        /* renamed from: com.google.android.gms.analytics.Tracker$a$a reason: collision with other inner class name */
        private class C0000a extends TimerTask {
            private C0000a() {
            }

            public void run() {
                a.this.uA = false;
            }
        }

        public a() {
            this.rJ = new i(Tracker.this) {
                public long currentTimeMillis() {
                    return System.currentTimeMillis();
                }
            };
        }

        private void cx() {
            GoogleAnalytics cf = GoogleAnalytics.cf();
            if (cf == null) {
                aa.t("GoogleAnalytics isn't initialized for the Tracker!");
            } else if (this.uD >= 0 || this.uB) {
                cf.a((a) Tracker.this.uw);
            } else {
                cf.b(Tracker.this.uw);
            }
        }

        private synchronized void cy() {
            if (this.uy != null) {
                this.uy.cancel();
                this.uy = null;
            }
        }

        public long cu() {
            return this.uD;
        }

        public boolean cv() {
            return this.uB;
        }

        public boolean cw() {
            boolean z = this.uE;
            this.uE = false;
            return z;
        }

        /* access modifiers changed from: 0000 */
        public boolean cz() {
            return this.uD == 0 || (this.uD > 0 && this.rJ.currentTimeMillis() > this.uF + this.uD);
        }

        public void enableAutoActivityTracking(boolean enabled) {
            this.uB = enabled;
            cx();
        }

        public void f(Activity activity) {
            u.bR().a(com.google.android.gms.analytics.u.a.EASY_TRACKER_ACTIVITY_START);
            cy();
            if (!this.uA && this.uC == 0 && cz()) {
                this.uE = true;
            }
            this.uA = true;
            this.uC++;
            if (this.uB) {
                HashMap hashMap = new HashMap();
                hashMap.put(Fields.HIT_TYPE, HitTypes.APP_VIEW);
                u.bR().r(true);
                Tracker.this.set("&cd", Tracker.this.ux != null ? Tracker.this.ux.h(activity) : activity.getClass().getCanonicalName());
                Tracker.this.send(hashMap);
                u.bR().r(false);
            }
        }

        public void g(Activity activity) {
            u.bR().a(com.google.android.gms.analytics.u.a.EASY_TRACKER_ACTIVITY_STOP);
            this.uC--;
            this.uC = Math.max(0, this.uC);
            this.uF = this.rJ.currentTimeMillis();
            if (this.uC == 0) {
                cy();
                this.uz = new C0000a();
                this.uy = new Timer("waitForActivityStart");
                this.uy.schedule(this.uz, 1000);
            }
        }

        public void setSessionTimeout(long sessionTimeout) {
            this.uD = sessionTimeout;
            cx();
        }
    }

    Tracker(String trackingId, TrackerHandler handler) {
        this(trackingId, handler, h.bu(), ae.cs(), g.bt(), new z("tracking"));
    }

    Tracker(String trackingId, TrackerHandler handler, h clientIdDefaultProvider, ae screenResolutionDefaultProvider, g appFieldsDefaultProvider, ad rateLimiter) {
        this.uq = new HashMap();
        this.up = handler;
        if (trackingId != null) {
            this.uq.put(Fields.TRACKING_ID, trackingId);
        }
        this.uq.put(Fields.USE_SECURE, "1");
        this.us = clientIdDefaultProvider;
        this.ut = screenResolutionDefaultProvider;
        this.uu = appFieldsDefaultProvider;
        this.ur = rateLimiter;
        this.uw = new a();
    }

    /* access modifiers changed from: 0000 */
    public void a(Context context, aj ajVar) {
        aa.v("Loading Tracker config values.");
        this.ux = ajVar;
        if (this.ux.cB()) {
            String cC = this.ux.cC();
            set(Fields.TRACKING_ID, cC);
            aa.v("[Tracker] trackingId loaded: " + cC);
        }
        if (this.ux.cD()) {
            String d = Double.toString(this.ux.cE());
            set(Fields.SAMPLE_RATE, d);
            aa.v("[Tracker] sample frequency loaded: " + d);
        }
        if (this.ux.cF()) {
            setSessionTimeout((long) this.ux.getSessionTimeout());
            aa.v("[Tracker] session timeout loaded: " + cu());
        }
        if (this.ux.cG()) {
            enableAutoActivityTracking(this.ux.cH());
            aa.v("[Tracker] auto activity tracking loaded: " + cv());
        }
        if (this.ux.cI()) {
            if (this.ux.cJ()) {
                set(Fields.ANONYMIZE_IP, "1");
                aa.v("[Tracker] anonymize ip loaded: true");
            }
            aa.v("[Tracker] anonymize ip loaded: false");
        }
        this.uv = this.ux.cK();
        if (this.ux.cK()) {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionReporter(this, Thread.getDefaultUncaughtExceptionHandler(), context));
            aa.v("[Tracker] report uncaught exceptions loaded: " + this.uv);
        }
    }

    /* access modifiers changed from: 0000 */
    public long cu() {
        return this.uw.cu();
    }

    /* access modifiers changed from: 0000 */
    public boolean cv() {
        return this.uw.cv();
    }

    public void enableAdvertisingIdCollection(boolean enabled) {
        if (!enabled) {
            this.uq.put("&ate", null);
            this.uq.put("&adid", null);
            return;
        }
        if (this.uq.containsKey("&ate")) {
            this.uq.remove("&ate");
        }
        if (this.uq.containsKey("&adid")) {
            this.uq.remove("&adid");
        }
    }

    public void enableAutoActivityTracking(boolean enabled) {
        this.uw.enableAutoActivityTracking(enabled);
    }

    public String get(String key) {
        u.bR().a(com.google.android.gms.analytics.u.a.GET);
        if (TextUtils.isEmpty(key)) {
            return null;
        }
        if (this.uq.containsKey(key)) {
            return (String) this.uq.get(key);
        }
        if (key.equals(Fields.LANGUAGE)) {
            return ak.a(Locale.getDefault());
        }
        if (this.us != null && this.us.x(key)) {
            return this.us.getValue(key);
        }
        if (this.ut != null && this.ut.x(key)) {
            return this.ut.getValue(key);
        }
        if (this.uu == null || !this.uu.x(key)) {
            return null;
        }
        return this.uu.getValue(key);
    }

    public void send(Map<String, String> params) {
        u.bR().a(com.google.android.gms.analytics.u.a.SEND);
        HashMap hashMap = new HashMap();
        hashMap.putAll(this.uq);
        if (params != null) {
            hashMap.putAll(params);
        }
        if (TextUtils.isEmpty((CharSequence) hashMap.get(Fields.TRACKING_ID))) {
            aa.w(String.format("Missing tracking id (%s) parameter.", new Object[]{Fields.TRACKING_ID}));
        }
        String str = (String) hashMap.get(Fields.HIT_TYPE);
        if (TextUtils.isEmpty(str)) {
            aa.w(String.format("Missing hit type (%s) parameter.", new Object[]{Fields.HIT_TYPE}));
            str = ZapConstants.GOOGLE_ACCOUNT_NONE;
        }
        if (this.uw.cw()) {
            hashMap.put(Fields.SESSION_CONTROL, "start");
        }
        if (str.equals(HitTypes.TRANSACTION) || str.equals(HitTypes.ITEM) || this.ur.cl()) {
            this.up.n(hashMap);
        } else {
            aa.w("Too many hits sent too quickly, rate limiting invoked.");
        }
    }

    public void set(String key, String value) {
        er.b(key, (Object) "Key should be non-null");
        u.bR().a(com.google.android.gms.analytics.u.a.SET);
        this.uq.put(key, value);
    }

    public void setAnonymizeIp(boolean anonymize) {
        set(Fields.ANONYMIZE_IP, ak.s(anonymize));
    }

    public void setAppId(String appId) {
        set(Fields.APP_ID, appId);
    }

    public void setAppInstallerId(String appInstallerId) {
        set(Fields.APP_INSTALLER_ID, appInstallerId);
    }

    public void setAppName(String appName) {
        set(Fields.APP_NAME, appName);
    }

    public void setAppVersion(String appVersion) {
        set(Fields.APP_VERSION, appVersion);
    }

    public void setClientId(String clientId) {
        set(Fields.CLIENT_ID, clientId);
    }

    public void setEncoding(String encoding) {
        set(Fields.ENCODING, encoding);
    }

    public void setHostname(String hostname) {
        set(Fields.HOSTNAME, hostname);
    }

    public void setLanguage(String language) {
        set(Fields.LANGUAGE, language);
    }

    public void setLocation(String location) {
        set(Fields.LOCATION, location);
    }

    public void setPage(String page) {
        set(Fields.PAGE, page);
    }

    public void setReferrer(String referrer) {
        set(Fields.REFERRER, referrer);
    }

    public void setSampleRate(double sampleRate) {
        set(Fields.SAMPLE_RATE, Double.toHexString(sampleRate));
    }

    public void setScreenColors(String screenColors) {
        set(Fields.SCREEN_COLORS, screenColors);
    }

    public void setScreenName(String screenName) {
        set("&cd", screenName);
    }

    public void setScreenResolution(int width, int height) {
        if (width >= 0 || height >= 0) {
            set(Fields.SCREEN_RESOLUTION, width + "x" + height);
        } else {
            aa.w("Invalid width or height. The values should be non-negative.");
        }
    }

    public void setSessionTimeout(long sessionTimeout) {
        this.uw.setSessionTimeout(1000 * sessionTimeout);
    }

    public void setTitle(String title) {
        set(Fields.TITLE, title);
    }

    public void setUseSecure(boolean useSecure) {
        set(Fields.USE_SECURE, ak.s(useSecure));
    }

    public void setViewportSize(String viewportSize) {
        set(Fields.VIEWPORT_SIZE, viewportSize);
    }
}
