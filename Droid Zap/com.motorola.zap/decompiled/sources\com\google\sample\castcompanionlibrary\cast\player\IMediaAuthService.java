package com.google.sample.castcompanionlibrary.cast.player;

import com.google.android.gms.cast.MediaInfo;

public interface IMediaAuthService {
    void abort(MediaAuthStatus mediaAuthStatus);

    MediaInfo getMediaInfo();

    String getPendingMessage();

    MediaAuthStatus getStatus();

    long getTimeout();

    void setOnResult(IMediaAuthListener iMediaAuthListener);

    void start();
}
