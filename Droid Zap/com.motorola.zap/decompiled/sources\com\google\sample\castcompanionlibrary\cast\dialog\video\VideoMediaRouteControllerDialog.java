package com.google.sample.castcompanionlibrary.cast.dialog.video;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.MediaRouteControllerDialog;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.common.images.WebImage;
import com.google.sample.castcompanionlibrary.cast.VideoCastManager;
import com.google.sample.castcompanionlibrary.cast.callbacks.VideoCastConsumerImpl;
import com.google.sample.castcompanionlibrary.cast.exceptions.CastException;
import com.google.sample.castcompanionlibrary.cast.exceptions.NoConnectionException;
import com.google.sample.castcompanionlibrary.cast.exceptions.TransientNetworkDisconnectionException;
import com.google.sample.castcompanionlibrary.utils.LogUtils;
import com.motorola.zap.R;
import java.net.URL;

public class VideoMediaRouteControllerDialog extends MediaRouteControllerDialog {
    /* access modifiers changed from: private */
    public static final String TAG = LogUtils.makeLogTag(VideoMediaRouteControllerDialog.class);
    private VideoCastConsumerImpl castConsumerImpl;
    /* access modifiers changed from: private */
    public VideoCastManager mCastManager;
    /* access modifiers changed from: private */
    public boolean mClosed;
    /* access modifiers changed from: private */
    public Context mContext;
    private TextView mEmptyText;
    /* access modifiers changed from: private */
    public ImageView mIcon;
    private View mIconContainer;
    /* access modifiers changed from: private */
    public Uri mIconUri;
    private ProgressBar mLoading;
    private Drawable mPauseDrawable;
    private ImageView mPausePlay;
    private Drawable mPlayDrawable;
    protected int mState;
    private Drawable mStopDrawable;
    private int mStreamType;
    private TextView mSubTitle;
    private TextView mTitle;

    public VideoMediaRouteControllerDialog(Context context, int theme) {
        super(context, theme);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.mCastManager != null) {
            this.mCastManager.removeVideoCastConsumer(this.castConsumerImpl);
        }
        super.onStop();
    }

    public VideoMediaRouteControllerDialog(Context context) {
        super(context, R.style.CastDialog);
        try {
            this.mContext = context;
            this.mCastManager = VideoCastManager.getInstance();
            this.mState = this.mCastManager.getPlaybackStatus();
            this.castConsumerImpl = new VideoCastConsumerImpl() {
                public void onRemoteMediaPlayerStatusUpdated() {
                    VideoMediaRouteControllerDialog.this.mState = VideoMediaRouteControllerDialog.this.mCastManager.getPlaybackStatus();
                    VideoMediaRouteControllerDialog.this.updatePlayPauseState(VideoMediaRouteControllerDialog.this.mState);
                }

                public void onRemoteMediaPlayerMetadataUpdated() {
                    VideoMediaRouteControllerDialog.this.updateMetadata();
                }
            };
            this.mCastManager.addVideoCastConsumer(this.castConsumerImpl);
            this.mPauseDrawable = context.getResources().getDrawable(R.drawable.ic_av_pause_sm_dark);
            this.mPlayDrawable = context.getResources().getDrawable(R.drawable.ic_av_play_sm_dark);
            this.mStopDrawable = context.getResources().getDrawable(R.drawable.ic_av_stop_sm_dark);
        } catch (CastException e) {
            LogUtils.LOGE(TAG, "Failed to update the content of dialog", e);
        } catch (IllegalStateException e2) {
            LogUtils.LOGE(TAG, "Failed to update the content of dialog", e2);
        }
    }

    private void hideControls(boolean hide, int resId) {
        int visibility;
        int i = 0;
        if (hide) {
            visibility = 8;
        } else {
            visibility = 0;
        }
        this.mIcon.setVisibility(visibility);
        this.mIconContainer.setVisibility(visibility);
        this.mTitle.setVisibility(visibility);
        this.mSubTitle.setVisibility(visibility);
        TextView textView = this.mEmptyText;
        if (resId == 0) {
            resId = R.string.no_media_info;
        }
        textView.setText(resId);
        TextView textView2 = this.mEmptyText;
        if (!hide) {
            i = 8;
        }
        textView2.setVisibility(i);
        if (hide) {
            this.mPausePlay.setVisibility(visibility);
        }
    }

    /* access modifiers changed from: private */
    public void updateMetadata() {
        MediaInfo info = null;
        try {
            info = this.mCastManager.getRemoteMediaInformation();
        } catch (TransientNetworkDisconnectionException e) {
            hideControls(true, R.string.failed_no_connection_short);
            return;
        } catch (Exception e2) {
            LogUtils.LOGE(TAG, "Failed to get media information", e2);
        }
        if (info == null) {
            hideControls(true, R.string.no_media_info);
            return;
        }
        this.mStreamType = info.getStreamType();
        hideControls(false, 0);
        MediaMetadata mm = info.getMetadata();
        this.mTitle.setText(mm.getString(MediaMetadata.KEY_TITLE));
        this.mSubTitle.setText(mm.getString(MediaMetadata.KEY_SUBTITLE));
        setIcon(mm.hasImages() ? ((WebImage) mm.getImages().get(0)).getUrl() : null);
    }

    public void setIcon(Uri uri) {
        if (this.mIconUri == null || !this.mIconUri.equals(uri)) {
            this.mIconUri = uri;
            if (uri == null) {
                this.mIcon.setImageBitmap(BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.video_placeholder_200x200));
                return;
            }
            new Thread(new Runnable() {
                Bitmap bm = null;

                public void run() {
                    try {
                        this.bm = BitmapFactory.decodeStream(new URL(VideoMediaRouteControllerDialog.this.mIconUri.toString()).openStream());
                    } catch (Exception e) {
                        LogUtils.LOGE(VideoMediaRouteControllerDialog.TAG, "setIcon(): Failed to load the image with url: " + VideoMediaRouteControllerDialog.this.mIconUri + ", using the default one", e);
                        this.bm = BitmapFactory.decodeResource(VideoMediaRouteControllerDialog.this.mContext.getResources(), R.drawable.video_placeholder_200x200);
                    }
                    if (!VideoMediaRouteControllerDialog.this.mClosed) {
                        VideoMediaRouteControllerDialog.this.mIcon.post(new Runnable() {
                            public void run() {
                                VideoMediaRouteControllerDialog.this.mIcon.setImageBitmap(AnonymousClass2.this.bm);
                            }
                        });
                    }
                }
            }).start();
        }
    }

    /* access modifiers changed from: private */
    public void updatePlayPauseState(int state) {
        if (this.mPausePlay != null) {
            switch (state) {
                case 1:
                    this.mPausePlay.setVisibility(4);
                    setLoadingVisibility(false);
                    if (this.mState == 1 && this.mCastManager.getIdleReason() == 1) {
                        hideControls(true, R.string.no_media_info);
                        return;
                    }
                    switch (this.mStreamType) {
                        case 1:
                            this.mPausePlay.setVisibility(4);
                            setLoadingVisibility(false);
                            return;
                        case 2:
                            if (this.mCastManager.getIdleReason() == 2) {
                                this.mPausePlay.setImageDrawable(this.mPlayDrawable);
                                adjustControlsVisibility(true);
                                return;
                            }
                            this.mPausePlay.setVisibility(4);
                            setLoadingVisibility(false);
                            return;
                        default:
                            return;
                    }
                case 2:
                    this.mPausePlay.setImageDrawable(getPauseStopButton());
                    adjustControlsVisibility(true);
                    return;
                case 3:
                    this.mPausePlay.setImageDrawable(this.mPlayDrawable);
                    adjustControlsVisibility(true);
                    return;
                case 4:
                    adjustControlsVisibility(false);
                    return;
                default:
                    this.mPausePlay.setVisibility(4);
                    setLoadingVisibility(false);
                    return;
            }
        }
    }

    private Drawable getPauseStopButton() {
        switch (this.mStreamType) {
            case 1:
                return this.mPauseDrawable;
            case 2:
                return this.mStopDrawable;
            default:
                return this.mPauseDrawable;
        }
    }

    private void setLoadingVisibility(boolean show) {
        this.mLoading.setVisibility(show ? 0 : 8);
    }

    /* access modifiers changed from: private */
    public void adjustControlsVisibility(boolean showPlayPlause) {
        boolean z = false;
        this.mPausePlay.setVisibility(showPlayPlause ? 0 : 4);
        if (!showPlayPlause) {
            z = true;
        }
        setLoadingVisibility(z);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.castConsumerImpl != null) {
            this.mCastManager.removeVideoCastConsumer(this.castConsumerImpl);
        }
        this.mClosed = true;
    }

    public View onCreateMediaControlView(Bundle savedInstanceState) {
        View controls = getLayoutInflater().inflate(R.layout.custom_media_route_controller_controls_dialog, null);
        loadViews(controls);
        this.mState = this.mCastManager.getPlaybackStatus();
        updateMetadata();
        updatePlayPauseState(this.mState);
        setupCallbacks();
        return controls;
    }

    private void setupCallbacks() {
        this.mPausePlay.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (VideoMediaRouteControllerDialog.this.mCastManager != null) {
                    try {
                        VideoMediaRouteControllerDialog.this.adjustControlsVisibility(false);
                        VideoMediaRouteControllerDialog.this.mCastManager.togglePlayback();
                    } catch (CastException e) {
                        VideoMediaRouteControllerDialog.this.adjustControlsVisibility(true);
                        LogUtils.LOGE(VideoMediaRouteControllerDialog.TAG, "Failed to toggle playback", e);
                    } catch (TransientNetworkDisconnectionException e2) {
                        VideoMediaRouteControllerDialog.this.adjustControlsVisibility(true);
                        LogUtils.LOGE(VideoMediaRouteControllerDialog.TAG, "Failed to toggle playback due to network issues", e2);
                    } catch (NoConnectionException e3) {
                        VideoMediaRouteControllerDialog.this.adjustControlsVisibility(true);
                        LogUtils.LOGE(VideoMediaRouteControllerDialog.TAG, "Failed to toggle playback due to network issues", e3);
                    }
                }
            }
        });
        this.mIcon.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (VideoMediaRouteControllerDialog.this.mCastManager != null && VideoMediaRouteControllerDialog.this.mCastManager.getTargetActivity() != null) {
                    try {
                        VideoMediaRouteControllerDialog.this.mCastManager.onTargetActivityInvoked(VideoMediaRouteControllerDialog.this.mContext);
                    } catch (TransientNetworkDisconnectionException e) {
                        LogUtils.LOGE(VideoMediaRouteControllerDialog.TAG, "Failed to start the target activity due to network issues", e);
                    } catch (NoConnectionException e2) {
                        LogUtils.LOGE(VideoMediaRouteControllerDialog.TAG, "Failed to start the target activity due to network issues", e2);
                    }
                    VideoMediaRouteControllerDialog.this.cancel();
                }
            }
        });
    }

    private void loadViews(View controls) {
        this.mIcon = (ImageView) controls.findViewById(R.id.iconView);
        this.mIconContainer = controls.findViewById(R.id.iconContainer);
        this.mPausePlay = (ImageView) controls.findViewById(R.id.playPauseView);
        this.mTitle = (TextView) controls.findViewById(R.id.titleView);
        this.mSubTitle = (TextView) controls.findViewById(R.id.subTitleView);
        this.mLoading = (ProgressBar) controls.findViewById(R.id.loadingView);
        this.mEmptyText = (TextView) controls.findViewById(R.id.emptyView);
    }
}
