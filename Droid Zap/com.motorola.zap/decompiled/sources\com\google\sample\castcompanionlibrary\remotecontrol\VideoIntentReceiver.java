package com.google.sample.castcompanionlibrary.remotecontrol;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import com.google.sample.castcompanionlibrary.cast.VideoCastManager;
import com.google.sample.castcompanionlibrary.cast.exceptions.CastException;
import com.google.sample.castcompanionlibrary.notification.VideoCastNotificationService;
import com.google.sample.castcompanionlibrary.utils.LogUtils;

public class VideoIntentReceiver extends BroadcastReceiver {
    private static final String TAG = LogUtils.makeLogTag(VideoIntentReceiver.class);

    public void onReceive(Context context, Intent intent) {
        VideoCastManager castMgr = null;
        try {
            castMgr = VideoCastManager.getInstance();
        } catch (CastException e) {
            LogUtils.LOGE(TAG, "onReceive(): No CastManager instance exists");
        }
        String action = intent.getAction();
        if (action != null) {
            if (action.equals(VideoCastNotificationService.ACTION_TOGGLE_PLAYBACK)) {
                if (castMgr != null) {
                    try {
                        LogUtils.LOGD(TAG, "Toggling playback via CastManager");
                        castMgr.togglePlayback();
                    } catch (Exception e2) {
                        LogUtils.LOGE(TAG, "onReceive(): Failed to toggle playback", e2);
                        startService(context, VideoCastNotificationService.ACTION_TOGGLE_PLAYBACK);
                    }
                } else {
                    LogUtils.LOGD(TAG, "Toggling playback via NotificationService");
                    startService(context, VideoCastNotificationService.ACTION_TOGGLE_PLAYBACK);
                }
            } else if (action.equals(VideoCastNotificationService.ACTION_STOP)) {
                if (castMgr != null) {
                    try {
                        LogUtils.LOGD(TAG, "Calling stopApplication from intent");
                        castMgr.disconnect();
                    } catch (Exception e3) {
                        LogUtils.LOGE(TAG, "onReceive(): Failed to stop application", e3);
                        startService(context, VideoCastNotificationService.ACTION_STOP);
                    }
                } else {
                    startService(context, VideoCastNotificationService.ACTION_STOP);
                }
            } else if (action.equals("android.intent.action.MEDIA_BUTTON")) {
                KeyEvent keyEvent = (KeyEvent) intent.getExtras().get("android.intent.extra.KEY_EVENT");
                if (keyEvent.getAction() == 0) {
                    switch (keyEvent.getKeyCode()) {
                        case 85:
                            try {
                                castMgr.togglePlayback();
                                return;
                            } catch (Exception e4) {
                                return;
                            }
                        default:
                            return;
                    }
                }
            }
        }
    }

    private void startService(Context context, String action) {
        Intent serviceIntent = new Intent(action);
        serviceIntent.setPackage(context.getPackageName());
        context.startService(serviceIntent);
    }
}
