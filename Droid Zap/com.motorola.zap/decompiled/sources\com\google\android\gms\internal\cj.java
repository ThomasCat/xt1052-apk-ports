package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.text.TextUtils;
import com.google.android.gms.plus.PlusShare;
import com.motorola.sharing.instrumentation.CheckinInterface;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class cj {
    private static final SimpleDateFormat ow = new SimpleDateFormat("yyyyMMdd");

    public static cf a(Context context, cd cdVar, String str) {
        cf cfVar;
        List<String> list;
        List<String> list2;
        List<String> list3;
        try {
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("ad_base_url", null);
            String optString2 = jSONObject.optString("ad_url", null);
            String optString3 = jSONObject.optString("ad_size", null);
            String optString4 = jSONObject.optString("ad_html", null);
            long j = -1;
            long j2 = jSONObject.has("interstitial_timeout") ? (long) (jSONObject.getDouble("interstitial_timeout") * 1000.0d) : -1;
            String optString5 = jSONObject.optString("orientation", null);
            int i = -1;
            if ("portrait".equals(optString5)) {
                i = cv.aU();
            } else if ("landscape".equals(optString5)) {
                i = cv.aT();
            }
            if (!TextUtils.isEmpty(optString4)) {
                if (TextUtils.isEmpty(optString)) {
                    da.w("Could not parse the mediation config: Missing required ad_base_url field");
                    return new cf(0);
                }
                cfVar = null;
            } else if (!TextUtils.isEmpty(optString2)) {
                cf a = ci.a(context, cdVar.kN.pU, optString2);
                optString = a.nw;
                optString4 = a.oi;
                j = a.oo;
                cfVar = a;
            } else {
                da.w("Could not parse the mediation config: Missing required ad_html or ad_url field.");
                return new cf(0);
            }
            JSONArray optJSONArray = jSONObject.optJSONArray("click_urls");
            List<String> list4 = cfVar == null ? null : cfVar.mt;
            if (optJSONArray != null) {
                if (list4 == null) {
                    list4 = new LinkedList<>();
                }
                for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                    list4.add(optJSONArray.getString(i2));
                }
                list = list4;
            } else {
                list = list4;
            }
            JSONArray optJSONArray2 = jSONObject.optJSONArray("impression_urls");
            List<String> list5 = cfVar == null ? null : cfVar.mu;
            if (optJSONArray2 != null) {
                if (list5 == null) {
                    list5 = new LinkedList<>();
                }
                for (int i3 = 0; i3 < optJSONArray2.length(); i3++) {
                    list5.add(optJSONArray2.getString(i3));
                }
                list2 = list5;
            } else {
                list2 = list5;
            }
            JSONArray optJSONArray3 = jSONObject.optJSONArray("manual_impression_urls");
            List<String> list6 = cfVar == null ? null : cfVar.om;
            if (optJSONArray3 != null) {
                if (list6 == null) {
                    list6 = new LinkedList<>();
                }
                for (int i4 = 0; i4 < optJSONArray3.length(); i4++) {
                    list6.add(optJSONArray3.getString(i4));
                }
                list3 = list6;
            } else {
                list3 = list6;
            }
            if (cfVar != null) {
                if (cfVar.orientation != -1) {
                    i = cfVar.orientation;
                }
                if (cfVar.oj > 0) {
                    j2 = cfVar.oj;
                }
            }
            return new cf(optString, optString4, list, list2, j2, false, -1, list3, -1, i, optString3, j);
        } catch (JSONException e) {
            da.w("Could not parse the mediation config: " + e.getMessage());
            return new cf(0);
        }
    }

    public static String a(cd cdVar, cm cmVar, Location location) {
        try {
            HashMap hashMap = new HashMap();
            if (cdVar.ob != null) {
                hashMap.put("ad_pos", cdVar.ob);
            }
            a(hashMap, cdVar.oc);
            hashMap.put("format", cdVar.kQ.ln);
            if (cdVar.kQ.width == -1) {
                hashMap.put("smart_w", "full");
            }
            if (cdVar.kQ.height == -2) {
                hashMap.put("smart_h", CheckinInterface.KEY_AUTO_PROCESSING);
            }
            if (cdVar.kQ.lp != null) {
                StringBuilder sb = new StringBuilder();
                ab[] abVarArr = cdVar.kQ.lp;
                int length = abVarArr.length;
                for (int i = 0; i < length; i++) {
                    ab abVar = abVarArr[i];
                    if (sb.length() != 0) {
                        sb.append("|");
                    }
                    sb.append(abVar.width == -1 ? (int) (((float) abVar.widthPixels) / cmVar.pc) : abVar.width);
                    sb.append("x");
                    sb.append(abVar.height == -2 ? (int) (((float) abVar.heightPixels) / cmVar.pc) : abVar.height);
                }
                hashMap.put("sz", sb);
            }
            hashMap.put("slotname", cdVar.adUnitId);
            hashMap.put("pn", cdVar.applicationInfo.packageName);
            if (cdVar.od != null) {
                hashMap.put("vc", Integer.valueOf(cdVar.od.versionCode));
            }
            hashMap.put("ms", cdVar.oe);
            hashMap.put("seq_num", cdVar.of);
            hashMap.put("session_id", cdVar.og);
            hashMap.put("js", cdVar.kN.pU);
            a(hashMap, cmVar);
            if (cdVar.oc.versionCode >= 2 && cdVar.oc.ll != null) {
                a(hashMap, cdVar.oc.ll);
            }
            if (cdVar.versionCode >= 2) {
                hashMap.put("quality_signals", cdVar.oh);
            }
            if (da.n(2)) {
                da.v("Ad Request JSON: " + cv.m((Map<String, ?>) hashMap).toString(2));
            }
            return cv.m((Map<String, ?>) hashMap).toString();
        } catch (JSONException e) {
            da.w("Problem serializing ad request to JSON: " + e.getMessage());
            return null;
        }
    }

    private static void a(HashMap<String, Object> hashMap, Location location) {
        HashMap hashMap2 = new HashMap();
        Float valueOf = Float.valueOf(location.getAccuracy() * 1000.0f);
        Long valueOf2 = Long.valueOf(location.getTime() * 1000);
        Long valueOf3 = Long.valueOf((long) (location.getLatitude() * 1.0E7d));
        Long valueOf4 = Long.valueOf((long) (location.getLongitude() * 1.0E7d));
        hashMap2.put("radius", valueOf);
        hashMap2.put("lat", valueOf3);
        hashMap2.put("long", valueOf4);
        hashMap2.put("time", valueOf2);
        hashMap.put("uule", hashMap2);
    }

    private static void a(HashMap<String, Object> hashMap, am amVar) {
        String str;
        String str2 = null;
        if (Color.alpha(amVar.lI) != 0) {
            hashMap.put("acolor", m(amVar.lI));
        }
        if (Color.alpha(amVar.backgroundColor) != 0) {
            hashMap.put("bgcolor", m(amVar.backgroundColor));
        }
        if (!(Color.alpha(amVar.lJ) == 0 || Color.alpha(amVar.lK) == 0)) {
            hashMap.put("gradientto", m(amVar.lJ));
            hashMap.put("gradientfrom", m(amVar.lK));
        }
        if (Color.alpha(amVar.lL) != 0) {
            hashMap.put("bcolor", m(amVar.lL));
        }
        hashMap.put("bthick", Integer.toString(amVar.lM));
        switch (amVar.lN) {
            case 0:
                str = "none";
                break;
            case 1:
                str = "dashed";
                break;
            case 2:
                str = "dotted";
                break;
            case 3:
                str = "solid";
                break;
            default:
                str = null;
                break;
        }
        if (str != null) {
            hashMap.put("btype", str);
        }
        switch (amVar.lO) {
            case 0:
                str2 = "light";
                break;
            case 1:
                str2 = "medium";
                break;
            case 2:
                str2 = "dark";
                break;
        }
        if (str2 != null) {
            hashMap.put("callbuttoncolor", str2);
        }
        if (amVar.lP != null) {
            hashMap.put("channel", amVar.lP);
        }
        if (Color.alpha(amVar.lQ) != 0) {
            hashMap.put("dcolor", m(amVar.lQ));
        }
        if (amVar.lR != null) {
            hashMap.put("font", amVar.lR);
        }
        if (Color.alpha(amVar.lS) != 0) {
            hashMap.put("hcolor", m(amVar.lS));
        }
        hashMap.put("headersize", Integer.toString(amVar.lT));
        if (amVar.lU != null) {
            hashMap.put("q", amVar.lU);
        }
    }

    private static void a(HashMap<String, Object> hashMap, cm cmVar) {
        hashMap.put("am", Integer.valueOf(cmVar.oM));
        hashMap.put("cog", j(cmVar.oN));
        hashMap.put("coh", j(cmVar.oO));
        if (!TextUtils.isEmpty(cmVar.oP)) {
            hashMap.put("carrier", cmVar.oP);
        }
        hashMap.put("gl", cmVar.oQ);
        if (cmVar.oR) {
            hashMap.put("simulator", Integer.valueOf(1));
        }
        hashMap.put("ma", j(cmVar.oS));
        hashMap.put("sp", j(cmVar.oT));
        hashMap.put("hl", cmVar.oU);
        if (!TextUtils.isEmpty(cmVar.oV)) {
            hashMap.put("mv", cmVar.oV);
        }
        hashMap.put("muv", Integer.valueOf(cmVar.oW));
        if (cmVar.oX != -2) {
            hashMap.put("cnt", Integer.valueOf(cmVar.oX));
        }
        hashMap.put("gnt", Integer.valueOf(cmVar.oY));
        hashMap.put("pt", Integer.valueOf(cmVar.oZ));
        hashMap.put("rm", Integer.valueOf(cmVar.pa));
        hashMap.put("riv", Integer.valueOf(cmVar.pb));
        hashMap.put("u_sd", Float.valueOf(cmVar.pc));
        hashMap.put("sh", Integer.valueOf(cmVar.pe));
        hashMap.put("sw", Integer.valueOf(cmVar.pd));
    }

    private static void a(HashMap<String, Object> hashMap, z zVar) {
        String aR = cs.aR();
        if (aR != null) {
            hashMap.put("abf", aR);
        }
        if (zVar.le != -1) {
            hashMap.put("cust_age", ow.format(new Date(zVar.le)));
        }
        if (zVar.extras != null) {
            hashMap.put("extras", zVar.extras);
        }
        if (zVar.lf != -1) {
            hashMap.put("cust_gender", Integer.valueOf(zVar.lf));
        }
        if (zVar.lg != null) {
            hashMap.put("kw", zVar.lg);
        }
        if (zVar.tagForChildDirectedTreatment != -1) {
            hashMap.put("tag_for_child_directed_treatment", Integer.valueOf(zVar.tagForChildDirectedTreatment));
        }
        if (zVar.lh) {
            hashMap.put("adtest", "on");
        }
        if (zVar.versionCode >= 2) {
            if (zVar.li) {
                hashMap.put("d_imp_hdr", Integer.valueOf(1));
            }
            if (!TextUtils.isEmpty(zVar.lj)) {
                hashMap.put("ppid", zVar.lj);
            }
            if (zVar.lk != null) {
                a(hashMap, zVar.lk);
            }
        }
        if (zVar.versionCode >= 3 && zVar.lm != null) {
            hashMap.put(PlusShare.KEY_CALL_TO_ACTION_URL, zVar.lm);
        }
    }

    private static Integer j(boolean z) {
        return Integer.valueOf(z ? 1 : 0);
    }

    private static String m(int i) {
        return String.format(Locale.US, "#%06x", new Object[]{Integer.valueOf(16777215 & i)});
    }
}
