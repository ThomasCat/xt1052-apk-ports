package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.drive.Contents;

public class e implements Creator<CloseContentsRequest> {
    static void a(CloseContentsRequest closeContentsRequest, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.c(parcel, 1, closeContentsRequest.wj);
        b.a(parcel, 2, (Parcelable) closeContentsRequest.Dq, i, false);
        b.a(parcel, 3, closeContentsRequest.Dr, false);
        b.D(parcel, p);
    }

    /* renamed from: F */
    public CloseContentsRequest createFromParcel(Parcel parcel) {
        Boolean d;
        Contents contents;
        int i;
        Boolean bool = null;
        int o = a.o(parcel);
        int i2 = 0;
        Contents contents2 = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    Boolean bool2 = bool;
                    contents = contents2;
                    i = a.g(parcel, n);
                    d = bool2;
                    break;
                case 2:
                    i = i2;
                    Contents contents3 = (Contents) a.a(parcel, n, Contents.CREATOR);
                    d = bool;
                    contents = contents3;
                    break;
                case 3:
                    d = a.d(parcel, n);
                    contents = contents2;
                    i = i2;
                    break;
                default:
                    a.b(parcel, n);
                    d = bool;
                    contents = contents2;
                    i = i2;
                    break;
            }
            i2 = i;
            contents2 = contents;
            bool = d;
        }
        if (parcel.dataPosition() == o) {
            return new CloseContentsRequest(i2, contents2, bool);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }

    /* renamed from: ak */
    public CloseContentsRequest[] newArray(int i) {
        return new CloseContentsRequest[i];
    }
}
