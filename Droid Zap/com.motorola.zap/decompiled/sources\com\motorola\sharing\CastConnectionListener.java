package com.motorola.sharing;

import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.common.api.Status;

public interface CastConnectionListener {
    void onApplicationConnected(ApplicationMetadata applicationMetadata, String str, String str2, boolean z);

    boolean onApplicationConnectionFailed(int i);

    void onApplicationDisconnected(int i);

    void onApplicationStatusChanged(String str);

    void onApplicationStopFailed(int i);

    void onCastAvailabilityChanged(boolean z);

    void onConnected();

    void onDisconnected();

    void onMessageReceived(CastDevice castDevice, String str, String str2);

    void onMessageSendFailed(Status status);

    void onRemoved(CastDevice castDevice, String str);

    void onVolumeChanged(double d, boolean z);
}
