package com.motorola.zap.screenshot;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.motorola.zap.R;
import com.motorola.zap.ui.DroidBlastLauncher;

public class VoiceIntroFragment extends Fragment {
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.voice_intro_card, container, false);
        TextView settings = (TextView) rootView.findViewById(R.id.intro_body);
        settings.setText(Html.fromHtml(getResources().getString(R.string.zap_voice_intro_msg)));
        settings.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                ((DroidBlastLauncher) VoiceIntroFragment.this.getActivity()).invokeSettings();
            }
        });
        ((Button) rootView.findViewById(R.id.intro_close)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                VoiceIntroFragment.this.getActivity().onBackPressed();
            }
        });
        return rootView;
    }
}
