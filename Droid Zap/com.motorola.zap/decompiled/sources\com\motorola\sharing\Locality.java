package com.motorola.sharing;

import java.io.Serializable;

public class Locality implements Serializable {
    public double accuracy;
    public String city;
    public String country;
    public int distance = 300;
    public double latitude = -1000.0d;
    public double longitude = -1000.0d;
    public String provider;
    public String state;
    public long timestamp;

    public long getAge() {
        long age = System.currentTimeMillis() - this.timestamp;
        if (age < 0) {
            return 0;
        }
        return age;
    }

    public boolean hasValidCoordinates() {
        if (this.latitude < -90.0d || this.latitude > 90.0d || this.longitude < -180.0d || this.longitude > 180.0d) {
            return false;
        }
        return true;
    }
}
