package com.google.tagmanager;

import java.util.List;

interface Dispatcher {
    void dispatchHits(List<Hit> list);

    boolean okToDispatch();
}
