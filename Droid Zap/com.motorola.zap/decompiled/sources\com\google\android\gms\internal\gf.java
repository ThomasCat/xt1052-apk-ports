package com.google.android.gms.internal;

import android.content.Intent;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a.c;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.achievement.Achievements.LoadAchievementsResult;
import com.google.android.gms.games.achievement.Achievements.UpdateAchievementResult;

public final class gf implements Achievements {

    private static abstract class a extends com.google.android.gms.games.Games.a<LoadAchievementsResult> {
        private a() {
        }

        /* renamed from: s */
        public LoadAchievementsResult d(final Status status) {
            return new LoadAchievementsResult() {
                public AchievementBuffer getAchievements() {
                    return new AchievementBuffer(DataHolder.empty(14));
                }

                public Status getStatus() {
                    return status;
                }

                public void release() {
                }
            };
        }
    }

    private static abstract class b extends com.google.android.gms.games.Games.a<UpdateAchievementResult> {
        /* access modifiers changed from: private */
        public final String uS;

        public b(String str) {
            this.uS = str;
        }

        /* renamed from: t */
        public UpdateAchievementResult d(final Status status) {
            return new UpdateAchievementResult() {
                public String getAchievementId() {
                    return b.this.uS;
                }

                public Status getStatus() {
                    return status;
                }
            };
        }
    }

    public Intent getAchievementsIntent(GoogleApiClient apiClient) {
        return Games.c(apiClient).fs();
    }

    public void increment(GoogleApiClient apiClient, final String id, final int numSteps) {
        apiClient.b(new b(id) {
            public void a(fx fxVar) {
                fxVar.a(null, id, numSteps);
            }
        });
    }

    public PendingResult<UpdateAchievementResult> incrementImmediate(GoogleApiClient apiClient, final String id, final int numSteps) {
        return apiClient.b(new b(id) {
            public void a(fx fxVar) {
                fxVar.a((c<UpdateAchievementResult>) this, id, numSteps);
            }
        });
    }

    public PendingResult<LoadAchievementsResult> load(GoogleApiClient apiClient, final boolean forceReload) {
        return apiClient.a(new a() {
            public void a(fx fxVar) {
                fxVar.c((c<LoadAchievementsResult>) this, forceReload);
            }
        });
    }

    public void reveal(GoogleApiClient apiClient, final String id) {
        apiClient.b(new b(id) {
            public void a(fx fxVar) {
                fxVar.b(null, id);
            }
        });
    }

    public PendingResult<UpdateAchievementResult> revealImmediate(GoogleApiClient apiClient, final String id) {
        return apiClient.b(new b(id) {
            public void a(fx fxVar) {
                fxVar.b((c<UpdateAchievementResult>) this, id);
            }
        });
    }

    public void setSteps(GoogleApiClient apiClient, final String id, final int numSteps) {
        apiClient.b(new b(id) {
            public void a(fx fxVar) {
                fxVar.b(null, id, numSteps);
            }
        });
    }

    public PendingResult<UpdateAchievementResult> setStepsImmediate(GoogleApiClient apiClient, final String id, final int numSteps) {
        return apiClient.b(new b(id) {
            public void a(fx fxVar) {
                fxVar.b((c<UpdateAchievementResult>) this, id, numSteps);
            }
        });
    }

    public void unlock(GoogleApiClient apiClient, final String id) {
        apiClient.b(new b(id) {
            public void a(fx fxVar) {
                fxVar.c(null, id);
            }
        });
    }

    public PendingResult<UpdateAchievementResult> unlockImmediate(GoogleApiClient apiClient, final String id) {
        return apiClient.b(new b(id) {
            public void a(fx fxVar) {
                fxVar.c((c<UpdateAchievementResult>) this, id);
            }
        });
    }
}
