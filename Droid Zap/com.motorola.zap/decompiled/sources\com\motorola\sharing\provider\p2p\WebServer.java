package com.motorola.sharing.provider.p2p;

import android.webkit.MimeTypeMap;
import com.motorola.sharing.provider.p2p.NanoHTTPD.Method;
import com.motorola.sharing.provider.p2p.NanoHTTPD.Response;
import com.motorola.sharing.provider.p2p.NanoHTTPD.Response.Status;
import com.motorola.sharing.util.Logger;
import com.motorola.zap.ZapConstants;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Map;

public class WebServer extends NanoHTTPD {
    private static final String TAG = Logger.getP2pTag("WebServer");

    public WebServer(String localHostName) {
        super(localHostName, -1);
    }

    private Response createNotFoundResponse() {
        return new Response(Status.NOT_FOUND, NanoHTTPD.MIME_PLAINTEXT, "Error 404, file not found.");
    }

    private Response createResponse(Status status, String mimeType, InputStream message) {
        Response res = new Response(status, mimeType, message);
        res.addHeader("Accept-Ranges", "bytes");
        return res;
    }

    private Response createResponse(Status status, String mimeType, String message) {
        Response res = new Response(status, mimeType, message);
        res.addHeader("Accept-Ranges", "bytes");
        return res;
    }

    private String mimeTypeFromUri(String uri) {
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(uri));
    }

    public Response serve(String uri, Method method, Map<String, String> header, Map<String, String> map, Map<String, String> map2) {
        if (uri.length() > 1 && uri.startsWith("/")) {
            String filePath = SharingService.getPathById(uri.substring(1));
            if (filePath != null) {
                String mimeType = mimeTypeFromUri(uri);
                File mediaFile = new File(filePath);
                if (mediaFile.exists()) {
                    Logger.i(TAG, "Serving: " + uri);
                    return serveFile(header, mediaFile, mimeType);
                }
            }
        }
        return createNotFoundResponse();
    }

    /* access modifiers changed from: 0000 */
    public Response serveFile(Map<String, String> header, File file, String mime) {
        Response res;
        try {
            String etag = Integer.toHexString((file.getAbsolutePath() + file.lastModified() + ZapConstants.GOOGLE_ACCOUNT_NONE + file.length()).hashCode());
            long startFrom = 0;
            long endAt = -1;
            String range = (String) header.get("range");
            if (range != null && range.startsWith("bytes=")) {
                range = range.substring("bytes=".length());
                int minus = range.indexOf(45);
                if (minus > 0) {
                    try {
                        startFrom = Long.parseLong(range.substring(0, minus));
                        endAt = Long.parseLong(range.substring(minus + 1));
                    } catch (NumberFormatException e) {
                    }
                }
            }
            long fileLen = file.length();
            if (range == null || startFrom < 0) {
                if (etag.equals(header.get("if-none-match"))) {
                    res = createResponse(Status.NOT_MODIFIED, mime, ZapConstants.GOOGLE_ACCOUNT_NONE);
                } else {
                    Status status = Status.OK;
                    FileInputStream fileInputStream = new FileInputStream(file);
                    res = createResponse(status, mime, (InputStream) fileInputStream);
                    res.addHeader("Content-Length", ZapConstants.GOOGLE_ACCOUNT_NONE + fileLen);
                    res.addHeader("ETag", etag);
                }
                return res;
            }
            if (startFrom >= fileLen) {
                res = createResponse(Status.RANGE_NOT_SATISFIABLE, NanoHTTPD.MIME_PLAINTEXT, ZapConstants.GOOGLE_ACCOUNT_NONE);
                res.addHeader("Content-Range", "bytes 0-0/" + fileLen);
                res.addHeader("ETag", etag);
            } else {
                if (endAt < 0) {
                    endAt = fileLen - 1;
                }
                long newLen = (endAt - startFrom) + 1;
                if (newLen < 0) {
                    newLen = 0;
                }
                final long dataLen = newLen;
                FileInputStream fis = new FileInputStream(file) {
                    public int available() throws IOException {
                        return (int) dataLen;
                    }
                };
                if (fis.skip(startFrom) != startFrom) {
                    Logger.e(TAG, "failed to skip to specified range");
                    return createResponse(Status.FORBIDDEN, NanoHTTPD.MIME_PLAINTEXT, "FORBIDDEN: Seek failed");
                }
                res = createResponse(Status.PARTIAL_CONTENT, mime, (InputStream) fis);
                res.addHeader("Content-Length", ZapConstants.GOOGLE_ACCOUNT_NONE + dataLen);
                res.addHeader("Content-Range", "bytes " + startFrom + "-" + endAt + "/" + fileLen);
                res.addHeader("ETag", etag);
            }
            return res;
        } catch (IOException e2) {
            res = createResponse(Status.FORBIDDEN, NanoHTTPD.MIME_PLAINTEXT, "FORBIDDEN: Reading file failed");
        }
    }

    private static String getIPAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                NetworkInterface intf = (NetworkInterface) en.nextElement();
                if (intf.getName().startsWith("p2p")) {
                    Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses();
                    while (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddr = (InetAddress) enumIpAddr.nextElement();
                        if (!inetAddr.isLoopbackAddress() && !(inetAddr instanceof Inet6Address)) {
                            return inetAddr.getHostAddress();
                        }
                    }
                    continue;
                }
            }
        } catch (SocketException e) {
            Logger.e(TAG, "Could not get local IP address: " + e.getMessage());
        }
        return null;
    }

    public String getBaseUri() {
        String ipAddress = getIPAddress();
        if (ipAddress == null) {
            return null;
        }
        return "http://" + ipAddress + ":" + getListeningPort() + "/";
    }
}
