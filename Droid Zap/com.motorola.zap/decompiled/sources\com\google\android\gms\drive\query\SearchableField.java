package com.google.android.gms.drive.query;

import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.CollectionMetadataField;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.OrderedMetadataField;
import com.google.android.gms.internal.fs;
import com.google.android.gms.internal.ft;
import java.util.Date;

public class SearchableField {
    public static final OrderedMetadataField<Date> EH = ft.EH;
    public static final MetadataField<Boolean> IS_PINNED = fs.IS_PINNED;
    public static final OrderedMetadataField<Date> LAST_VIEWED_BY_ME = ft.LAST_VIEWED_BY_ME;
    public static final MetadataField<String> MIME_TYPE = fs.MIME_TYPE;
    public static final OrderedMetadataField<Date> MODIFIED_DATE = ft.EE;
    public static final CollectionMetadataField<DriveId> PARENTS = fs.PARENTS;
    public static final MetadataField<Boolean> STARRED = fs.STARRED;
    public static final MetadataField<String> TITLE = fs.TITLE;
    public static final MetadataField<Boolean> TRASHED = fs.TRASHED;
}
