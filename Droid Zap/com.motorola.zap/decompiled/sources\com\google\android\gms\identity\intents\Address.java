package com.google.android.gms.identity.intents;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.b;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ApiOptions;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a.C0003a;
import com.google.android.gms.internal.ee;
import com.google.android.gms.internal.er;
import com.google.android.gms.internal.hc;

public final class Address {
    public static final Api API = new Api(va, new Scope[0]);
    static final b<hc> va = new b<hc>() {
        /* renamed from: f */
        public hc b(Context context, Looper looper, ee eeVar, ApiOptions apiOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            AddressOptions addressOptions;
            er.b(context instanceof Activity, (Object) "An Activity must be used for Address APIs");
            AddressOptions addressOptions2 = new AddressOptions();
            if (apiOptions != null) {
                er.b(apiOptions instanceof AddressOptions, (Object) "Must use AddressOptions with Address API");
                addressOptions = (AddressOptions) apiOptions;
            } else {
                addressOptions = addressOptions2;
            }
            return new hc((Activity) context, looper, connectionCallbacks, onConnectionFailedListener, eeVar.getAccountName(), addressOptions.theme);
        }

        public int getPriority() {
            return Integer.MAX_VALUE;
        }
    };

    public static final class AddressOptions implements ApiOptions {
        public final int theme;

        public AddressOptions() {
            this.theme = 0;
        }

        public AddressOptions(int theme2) {
            this.theme = theme2;
        }
    }

    private static abstract class a extends C0003a<Status, hc> {
        public a() {
            super(Address.va);
        }

        /* renamed from: f */
        public Status d(Status status) {
            return status;
        }
    }

    public static void requestUserAddress(GoogleApiClient googleApiClient, final UserAddressRequest request, final int requestCode) {
        googleApiClient.a(new a() {
            /* access modifiers changed from: protected */
            public void a(hc hcVar) throws RemoteException {
                hcVar.a(request, requestCode);
                b(Status.zQ);
            }
        });
    }
}
