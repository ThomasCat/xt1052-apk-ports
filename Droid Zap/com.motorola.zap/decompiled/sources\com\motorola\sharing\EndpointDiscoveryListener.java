package com.motorola.sharing;

import android.os.Handler;
import android.os.Looper;

public abstract class EndpointDiscoveryListener {
    private Handler mHandler = null;

    public static class EndpointDiscoveryEvent implements Cloneable {
        public Endpoint endpoint;
        public ProviderApi provider;
        public Status status;
        public Substatus substatus;

        public enum Status {
            INFO,
            SUCCESS,
            ERROR
        }

        public enum Substatus {
            DISCOVERED,
            GENERAL,
            NETWORK,
            STOPPED,
            FINISHED
        }

        public EndpointDiscoveryEvent(Status status2, Substatus substatus2) {
            this.status = status2;
            this.substatus = substatus2;
            this.provider = null;
            this.endpoint = null;
        }

        public EndpointDiscoveryEvent(Status status2, Substatus substatus2, ProviderApi provider2) {
            this.status = status2;
            this.substatus = substatus2;
            this.provider = provider2;
            this.endpoint = null;
        }

        public EndpointDiscoveryEvent(Status status2, Substatus substatus2, ProviderApi provider2, Endpoint endpoint2) {
            this.status = status2;
            this.substatus = substatus2;
            this.provider = provider2;
            this.endpoint = endpoint2;
        }

        public EndpointDiscoveryEvent clone() throws CloneNotSupportedException {
            return (EndpointDiscoveryEvent) super.clone();
        }
    }

    public abstract void onEndpointDiscoveryEvent(EndpointDiscoveryEvent endpointDiscoveryEvent);

    public EndpointDiscoveryListener() {
        Looper looper = Looper.myLooper();
        if (looper == null) {
            looper = Looper.getMainLooper();
        }
        this.mHandler = new Handler(looper);
    }

    public final void sendEvent(final EndpointDiscoveryEvent event) {
        this.mHandler.post(new Runnable() {
            public void run() {
                try {
                    EndpointDiscoveryListener.this.onEndpointDiscoveryEvent(event.clone());
                } catch (CloneNotSupportedException e) {
                }
            }
        });
    }
}
