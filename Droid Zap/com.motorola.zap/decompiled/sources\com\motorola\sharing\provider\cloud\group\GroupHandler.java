package com.motorola.sharing.provider.cloud.group;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener.GroupEvent;
import com.motorola.sharing.GroupListener.GroupEvent.Status;
import com.motorola.sharing.GroupListener.GroupEvent.Substatus;
import com.motorola.sharing.GroupListener.GroupEvent.Type;
import com.motorola.sharing.GroupManager.BanParams;
import com.motorola.sharing.GroupManager.CreateParams;
import com.motorola.sharing.GroupManager.GroupInfo;
import com.motorola.sharing.GroupManager.JoinParams;
import com.motorola.sharing.GroupManager.ShareInviteParams;
import com.motorola.sharing.Profile;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker.GroupState;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker.GroupStats;
import com.motorola.sharing.provider.cloud.group.request.GroupBanUserRequest;
import com.motorola.sharing.provider.cloud.group.request.GroupCreateRequest;
import com.motorola.sharing.provider.cloud.group.request.GroupExpiredRequest;
import com.motorola.sharing.provider.cloud.group.request.GroupJoinRequest;
import com.motorola.sharing.provider.cloud.group.request.GroupLeaveRequest;
import com.motorola.sharing.provider.cloud.group.request.GroupRequest;
import com.motorola.sharing.provider.cloud.group.request.GroupShareInviteRequest;
import com.motorola.sharing.provider.cloud.group.request.GroupSyncStateRequest;
import com.motorola.sharing.provider.cloud.group.request.GroupUpdateRequest;
import com.motorola.sharing.util.Logger;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class GroupHandler {
    private static final String TAG = Logger.getCloudTag("GroupHandler");
    private static GroupHandler sInstance = null;
    private final Executor mExecutor;
    protected GroupStateTracker mGroupStateTracker;

    public static synchronized GroupHandler getInstance() {
        GroupHandler groupHandler;
        synchronized (GroupHandler.class) {
            if (sInstance == null) {
                sInstance = new GroupHandler();
            }
            groupHandler = sInstance;
        }
        return groupHandler;
    }

    private GroupHandler() {
        this.mGroupStateTracker = null;
        this.mExecutor = Executors.newSingleThreadExecutor();
        this.mGroupStateTracker = GroupStateTracker.getInstance();
    }

    public void createGroup(CreateParams params) {
        Logger.d(TAG, "createGroup invoked");
        enqueue(new GroupCreateRequest(params));
    }

    public void leaveGroup(String owner, String id) {
        Logger.d(TAG, "leaveGroup invoked");
        enqueue(new GroupLeaveRequest(owner, id));
    }

    public void leaveGroup(boolean onlyIfPending) {
        Logger.d(TAG, "leaveGroup (current) invoked");
        enqueue(new GroupLeaveRequest(onlyIfPending));
    }

    public void shareGroupInvite(ShareInviteParams params) {
        Logger.d(TAG, "shareGroupInvite invoked");
        enqueue(new GroupShareInviteRequest(params));
    }

    public void banUser(BanParams params) {
        Logger.d(TAG, "banUser invoked");
        enqueue(new GroupBanUserRequest(params.groupId, params.userId));
    }

    public void retry(GroupRequest request) {
        if (request == null || !request.isRetry()) {
            throw new IllegalArgumentException("Request is null or not allowed to retry");
        }
        enqueue(request);
    }

    private void enqueue(GroupRequest request) {
        Substatus error = request.checkPreconditions();
        if (error != Substatus.NO_ERROR) {
            Logger.d(TAG, "Request aborted, preconditions error: " + error);
            Bundle data = new Bundle();
            Intent intent = request.getRetryIntent();
            if (intent != null) {
                Logger.d(TAG, "Retry intent available for recovery: " + intent.getAction());
                data.putParcelable(GroupEvent.KEY_RETRY_INTENT, intent);
            }
            Bundle bundle = request.getBundle();
            if (bundle != null) {
                data.putAll(bundle);
            }
            GCSP.getGroupManager().sendEvent(request.getWhat(), Status.ERROR, error, data, request.getGroupInfo());
            return;
        }
        request.onEnqueue();
        this.mExecutor.execute(request);
    }

    public void handleGroupInvite(GroupInfo groupInfo, Profile profile) {
        Logger.d(TAG, "handleGroupInvite invoked");
        Bundle bundle = new Bundle();
        bundle.putSerializable(GroupEvent.KEY_USER_PROFILE, profile);
        GCSP.getGroupManager().sendEvent(Type.GROUP_INVITE_RECEIVE, Status.OK, Substatus.NO_ERROR, bundle, groupInfo);
    }

    public void handleGroupExpired() {
        Logger.d(TAG, "handleGroupExpired invoked");
        enqueue(new GroupExpiredRequest());
    }

    public void joinGroup(JoinParams params) {
        Logger.d(TAG, "joinGroup invoked");
        enqueue(new GroupJoinRequest(params, false));
    }

    public void syncGroupState() {
        Logger.d(TAG, "syncGroupState invoked");
        enqueue(new GroupSyncStateRequest());
    }

    public void updateGroupState() {
        Logger.d(TAG, "updateGroupState invoked");
        enqueue(new GroupUpdateRequest());
    }

    public GroupInfo getGroupInfo() {
        GroupState groupState = getInstance().getGroupState();
        if (groupState == null) {
            return null;
        }
        return new GroupInfo(groupState.isActive(), groupState.getOwner(), groupState.getId(), groupState.getName(), groupState.getThumbnail(), TextUtils.isEmpty(groupState.getPin()), groupState.getPin(), groupState.getStatsCount(GroupStats.CURRENT_MEMBERS), groupState.getLastInviteSentTime());
    }

    public boolean isGroupActive() {
        return this.mGroupStateTracker.isActive();
    }

    public boolean isLeavingGroup() {
        return this.mGroupStateTracker.isLeaving();
    }

    public GroupState getGroupState() {
        return this.mGroupStateTracker.getState();
    }

    public void incrementStatsCount(GroupStats stats, int delta, String owner, String id) {
        this.mGroupStateTracker.incrementStatsCount(stats, delta, owner, id);
    }
}
