package com.motorola.sharing.provider.p2p;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.WifiP2pManager.ChannelListener;
import android.net.wifi.p2p.WifiP2pManager.GroupInfoListener;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.view.MotionEventCompat;
import android.text.TextUtils;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentContainer.P2pParams;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.ContentShareListener.ContentShareEvent.Substatus;
import com.motorola.sharing.ContentShareListener.ContentStopShareEvent;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.provider.p2p.ContainerTimer.ExpiryListener;
import com.motorola.sharing.util.Logger;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class SharingService extends Service implements ChannelListener, ExpiryListener {
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getP2pTag("SharingSvc");
    private static Queue<ContentContainer> gPendingContainers = new ConcurrentLinkedQueue();
    private static Map<String, ContentContainer> gSharedContainers = new ConcurrentHashMap();
    private ContainerTimer mContainerTimer = new ContainerTimer(this);
    GroupInfoHandler mGroupInfoHandler = new GroupInfoHandler(this);
    /* access modifiers changed from: private */
    public int mGroupInfoRetries = 0;
    /* access modifiers changed from: private */
    public String mNetworkName = null;
    private Channel mP2pChannel = null;
    /* access modifiers changed from: private */
    public String mPassphrase = null;
    private boolean mRetryChannel = false;
    /* access modifiers changed from: private */
    public boolean mServerActivating = false;
    private boolean mServerActive = false;
    private WebServer mWebServer = null;
    /* access modifiers changed from: private */
    public boolean mWiFiWasEnabled = false;
    private WifiManager mWifiManager = null;
    private WifiP2pManager mWifiP2pManager = null;
    /* access modifiers changed from: private */
    public WiFiP2pBroadcastReceiver mWifiP2pReceiver = new WiFiP2pBroadcastReceiver();
    /* access modifiers changed from: private */
    public WifiStateReceiver mWifiStateReceiver = new WifiStateReceiver();

    private static class GroupInfoHandler extends Handler {
        private WeakReference<SharingService> mService;

        public GroupInfoHandler(SharingService service) {
            this.mService = new WeakReference<>(service);
        }

        public void handleMessage(Message msg) {
            if (msg.what == 1000) {
                SharingService service = (SharingService) this.mService.get();
                if (service != null) {
                    service.requestGroupInfo();
                }
            }
        }
    }

    public class WiFiP2pBroadcastReceiver extends BroadcastReceiver {
        public WiFiP2pBroadcastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("android.net.wifi.p2p.STATE_CHANGED".equals(intent.getAction())) {
                Logger.i(SharingService.TAG, "WIFI_P2P_STATE_CHANGED_ACTION received");
                SharingService.this.unregisterReceiver(SharingService.this.mWifiP2pReceiver);
                if (intent.getIntExtra("wifi_p2p_state", -1) == 2) {
                    Logger.i(SharingService.TAG, "WiFi P2P is enabled");
                    SharingService.this.checkForExistingGroup();
                    return;
                }
                Logger.i(SharingService.TAG, "Wifi P2P failed to initialize");
            }
        }
    }

    private class WifiStateReceiver extends BroadcastReceiver {
        private WifiStateReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("android.net.wifi.WIFI_STATE_CHANGED".equals(intent.getAction()) && intent.getIntExtra("wifi_state", -1) == 3) {
                SharingService.this.unregisterReceiver(SharingService.this.mWifiStateReceiver);
                Logger.i(SharingService.TAG, "WiFi enabled");
                SharingService.this.mWiFiWasEnabled = true;
                SharingService.this.initWifiP2p();
            }
        }
    }

    public static void startSharing(Context context, ContentContainer contentContainer) {
        if (!gSharedContainers.containsKey(getPathId(contentContainer.path))) {
            gPendingContainers.add(contentContainer);
            Intent intent = new Intent(context, SharingService.class);
            intent.putExtra("command", "start_sharing");
            context.startService(intent);
            return;
        }
        Logger.w(TAG, "Already shared: " + contentContainer.path);
        contentContainer.onContentShareEvent(Status.INFO, Substatus.META_SHARED, P2pProvider.getInstance().getEndpoint());
    }

    public static void stopSharing(Context context, ContentContainer containerToStop) {
        cancelSharing(context, containerToStop);
        containerToStop.onContentStopShareEvent(Status.SUCCESS, ContentStopShareEvent.Substatus.NOT_SHARED, P2pProvider.getInstance().getEndpoint());
    }

    public static void cancelSharing(Context context, ContentContainer contentContainer) {
        boolean z;
        boolean z2 = false;
        if (!TextUtils.isEmpty(contentContainer.path)) {
            for (ContentContainer contentContainer2 : gPendingContainers) {
                if (contentContainer2.path.equals(contentContainer.path)) {
                    gPendingContainers.remove(contentContainer2);
                }
            }
            String pathId = getPathId(contentContainer.path);
            if (gSharedContainers.containsKey(pathId)) {
                gSharedContainers.remove(pathId);
                z = true;
            } else {
                z = false;
            }
            Intent intent = new Intent(context, SharingService.class);
            intent.putExtra("command", "stop_by_path");
            intent.putExtra("param", contentContainer.path);
            context.startService(intent);
            z2 = z;
        } else if (!TextUtils.isEmpty(contentContainer.shareId)) {
            for (ContentContainer contentContainer3 : gSharedContainers.values()) {
                if (!TextUtils.isEmpty(contentContainer3.shareId) && contentContainer3.shareId.equals(contentContainer.shareId)) {
                    gSharedContainers.remove(getPathId(contentContainer3.path));
                    z2 = true;
                }
            }
            Intent intent2 = new Intent(context, SharingService.class);
            intent2.putExtra("command", "stop_by_shareid");
            intent2.putExtra("param", contentContainer.shareId);
            context.startService(intent2);
        }
        if (z2 && gSharedContainers.size() == 0 && gPendingContainers.size() == 0) {
            Intent intent3 = new Intent(context, SharingService.class);
            intent3.putExtra("command", "stop_all_sharing");
            context.startService(intent3);
        }
    }

    public void onCreate() {
        Logger.i(TAG, "onCreate");
        super.onCreate();
        this.mWifiManager = (WifiManager) getSystemService("wifi");
        this.mWifiP2pManager = (WifiP2pManager) getSystemService("wifip2p");
        Context context = GCSP.getContext();
        GCSP.getContext();
        int ipAddress = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getIpAddress();
        this.mWebServer = new WebServer(String.format("%d.%d.%d.%d", new Object[]{Integer.valueOf(ipAddress & MotionEventCompat.ACTION_MASK), Integer.valueOf((ipAddress >> 8) & MotionEventCompat.ACTION_MASK), Integer.valueOf((ipAddress >> 16) & MotionEventCompat.ACTION_MASK), Integer.valueOf((ipAddress >> 24) & MotionEventCompat.ACTION_MASK)}));
    }

    private void processPendingContainers() {
        while (true) {
            ContentContainer container = (ContentContainer) gPendingContainers.poll();
            if (container != null) {
                String contentUri = makeContentUri(container.path);
                Logger.i(TAG, "P2P media URI: " + contentUri);
                container.setP2pParams(new P2pParams(contentUri, this.mNetworkName, this.mPassphrase));
                gSharedContainers.put(getPathId(container.path), container);
                container.onContentShareEvent(Status.INFO, Substatus.META_SHARED, P2pProvider.getInstance().getEndpoint());
            } else {
                return;
            }
        }
    }

    public void onContainerExpired(ContentContainer container) {
        stopSharing(this, container);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Logger.i(TAG, "onStartCommand");
        String command = intent.getStringExtra("command");
        if ("start_sharing".equals(command)) {
            if (this.mServerActive) {
                processPendingContainers();
            } else if (!this.mServerActivating) {
                this.mServerActivating = true;
                checkWifi();
            }
        } else if ("stop_all_sharing".equals(command)) {
            Logger.i(TAG, "Shutting down web service and P2P group");
            if (this.mWebServer.isAlive()) {
                this.mWebServer.stop();
            }
            removeExistingGroup();
            if (this.mWiFiWasEnabled && this.mWifiManager.isWifiEnabled()) {
                this.mWifiManager.setWifiEnabled(false);
            }
            shutdown();
            stopSelf();
        } else if ("stop_by_path".equals(command)) {
            this.mContainerTimer.removeByPath(intent.getStringExtra("param"));
        } else if ("stop_by_shareid".equals(command)) {
            this.mContainerTimer.removeByShareId(intent.getStringExtra("param"));
        }
        return 2;
    }

    private void checkWifi() {
        if (!this.mWifiManager.isWifiEnabled()) {
            Logger.i(TAG, "Enabling WiFi");
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
            registerReceiver(this.mWifiStateReceiver, filter);
            this.mWifiManager.setWifiEnabled(true);
            return;
        }
        initWifiP2p();
    }

    /* access modifiers changed from: private */
    public void initWifiP2p() {
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("android.net.wifi.p2p.STATE_CHANGED");
        registerReceiver(this.mWifiP2pReceiver, mIntentFilter);
        this.mP2pChannel = this.mWifiP2pManager.initialize(this, getMainLooper(), this);
    }

    /* access modifiers changed from: private */
    public void checkForExistingGroup() {
        this.mWifiP2pManager.requestGroupInfo(this.mP2pChannel, new GroupInfoListener() {
            public void onGroupInfoAvailable(WifiP2pGroup group) {
                if (group != null) {
                    SharingService.this.mNetworkName = group.getNetworkName();
                    SharingService.this.mPassphrase = group.getPassphrase();
                    Logger.i(SharingService.TAG, "Found existing group: Network = " + SharingService.this.mNetworkName + "; Passphrase = " + SharingService.this.mPassphrase);
                    SharingService.this.enableSharing();
                    return;
                }
                Logger.i(SharingService.TAG, "No existing group found, creating new one");
                SharingService.this.createNewGroup();
            }
        });
    }

    /* access modifiers changed from: private */
    public void createNewGroup() {
        this.mWifiP2pManager.createGroup(this.mP2pChannel, new ActionListener() {
            public void onFailure(int reason) {
                Logger.e(SharingService.TAG, "Failed to create group (error = " + reason + ")");
                SharingService.this.shutdown();
                SharingService.this.stopSelf();
                SharingService.this.mServerActivating = false;
            }

            public void onSuccess() {
                Logger.i(SharingService.TAG, "Successfully created group, requesting group info");
                SharingService.this.mGroupInfoRetries = 0;
                SharingService.this.mGroupInfoHandler.sendEmptyMessageDelayed(1000, 300);
            }
        });
    }

    /* access modifiers changed from: private */
    public void requestGroupInfo() {
        this.mGroupInfoRetries++;
        this.mWifiP2pManager.requestGroupInfo(this.mP2pChannel, new GroupInfoListener() {
            public void onGroupInfoAvailable(WifiP2pGroup group) {
                if (group != null) {
                    SharingService.this.mGroupInfoHandler.removeMessages(1000);
                    SharingService.this.mNetworkName = group.getNetworkName();
                    SharingService.this.mPassphrase = group.getPassphrase();
                    Logger.i(SharingService.TAG, "Network = " + SharingService.this.mNetworkName + "; Passphrase = " + SharingService.this.mPassphrase);
                    SharingService.this.enableSharing();
                } else if (SharingService.this.mGroupInfoRetries < 10) {
                    Logger.i(SharingService.TAG, "Group is null, requesting group info again...");
                    SharingService.this.mGroupInfoHandler.sendEmptyMessageDelayed(1000, 300);
                } else {
                    Logger.e(SharingService.TAG, "Failed to retrieve group info");
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void enableSharing() {
        try {
            if (!this.mWebServer.isAlive()) {
                this.mWebServer.start();
            }
            this.mServerActivating = false;
            this.mServerActive = true;
            processPendingContainers();
        } catch (IOException e) {
            Logger.e(TAG, "Failed to start web service");
            this.mServerActivating = false;
        }
    }

    private void removeExistingGroup() {
        this.mWifiP2pManager.removeGroup(this.mP2pChannel, new ActionListener() {
            public void onFailure(int reason) {
                Logger.e(SharingService.TAG, "Failed to remove group (error = " + reason + ")");
                SharingService.this.stopSelf();
            }

            public void onSuccess() {
                Logger.i(SharingService.TAG, "Successfully removed group");
                SharingService.this.stopSelf();
            }
        });
    }

    public void onDestroy() {
        shutdown();
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    /* access modifiers changed from: private */
    public void shutdown() {
        this.mContainerTimer.removeAll();
        this.mGroupInfoHandler.removeMessages(1000);
        this.mRetryChannel = false;
        this.mNetworkName = null;
        this.mPassphrase = null;
        this.mGroupInfoRetries = 0;
        this.mServerActivating = false;
        this.mServerActive = false;
    }

    public void onChannelDisconnected() {
        if (this.mWifiP2pManager == null || this.mRetryChannel) {
            Logger.e(TAG, "Lost channel to P2P service");
            return;
        }
        Logger.w(TAG, "Channel lost, reconnecting");
        this.mRetryChannel = true;
        this.mWifiP2pManager.initialize(this, getMainLooper(), this);
    }

    static String getPathById(String id) {
        ContentContainer container = (ContentContainer) gSharedContainers.get(id);
        if (container != null) {
            return container.path;
        }
        return null;
    }

    private static String getPathId(String path) {
        String extension = null;
        int index = path.lastIndexOf(46);
        if (index > 0) {
            extension = path.substring(index + 1);
        }
        try {
            byte[] digest = MessageDigest.getInstance("SHA-1").digest(path.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(String.format("%02x", new Object[]{Integer.valueOf(b & 255)}));
            }
            if (extension != null) {
                sb.append(".");
                sb.append(extension);
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            Logger.e(TAG, "No such algo exception" + e.getMessage());
            return null;
        } catch (UnsupportedEncodingException e2) {
            Logger.e(TAG, "Unsupported encoding" + e2.getMessage());
            return null;
        }
    }

    private String makeContentUri(String filePath) {
        return this.mWebServer.getBaseUri() + getPathId(filePath);
    }
}
