package com.motorola.zap.app;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.util.PackageUtils;
import com.motorola.zap.LogUtils;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConfig;
import com.motorola.zap.ZapConstants;
import java.util.List;
import java.util.Locale;

public class ZzCameraWatcher {
    /* access modifiers changed from: private */
    public static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private static ZzCameraWatcher sInstance = null;
    private Context mContext = null;

    public static class CameraReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ZzCameraWatcher.DBG) {
                Log.d("Zap-ZzCameraWatcher", "Got " + action);
            }
            if (!GCSP.getGroupManager().isAnyGroupActive()) {
                if (ZzCameraWatcher.DBG) {
                    Log.d("Zap-ZzCameraWatcher", "Zz is not active");
                }
                ZzCameraWatcher.getInstance().stop();
            } else if (isZapAppCameraInTopStack(context)) {
                if (ZzCameraWatcher.DBG) {
                    Log.d("Zap-ZzCameraWatcher", "You are in Camera's service mode. Do not update");
                }
            } else if (!"android.hardware.action.NEW_PICTURE".equals(action) && !"android.hardware.action.NEW_VIDEO".equals(action)) {
            } else {
                if (!"android.hardware.action.NEW_VIDEO".equals(action) || ZapConfig.supportVideoInZapZone()) {
                    Intent intent2 = new Intent(ZapConstants.ACTION_GS_GALLERY_PHOTO_SHARED);
                    intent2.setClass(context.getApplicationContext(), ActionReceiver.class);
                    intent2.putExtra(ZapConstants.EXTRA_GS_GALLERY_PHOTO_SHARED, intent.getData().toString());
                    intent2.putExtra(ZapConstants.EXTRA_EVENT_ORIGIN_ZAP_APP, true);
                    intent2.putExtra(ZapConstants.EXTRA_AUTO_SHARE, true);
                    context.sendBroadcast(intent2);
                }
            }
        }

        /* access modifiers changed from: private */
        public static void setEnabled(Context context, boolean z) {
            if (PackageUtils.setComponentEnabled(context, new ComponentName(context, CameraReceiver.class), z) && ZzCameraWatcher.DBG) {
                Log.d("Zap-ZzCameraWatcher", "Camera capture receiver " + (z ? "enabled" : "disabled"));
            }
        }

        /* access modifiers changed from: private */
        public static boolean isEnabled(Context context) {
            return PackageUtils.isComponentEnabled(context, new ComponentName(context, CameraReceiver.class));
        }

        private boolean isZapAppCameraInTopStack(Context context) {
            List<RunningTaskInfo> runningTasks = ((ActivityManager) context.getSystemService("activity")).getRunningTasks(1);
            if (runningTasks != null && runningTasks.size() > 0) {
                RunningTaskInfo rti = (RunningTaskInfo) runningTasks.get(0);
                if (ZzCameraWatcher.DBG) {
                    Log.d("Zap-ZzCameraWatcher", "Base Package =" + rti.baseActivity.getPackageName() + " , topActivity packageName =" + rti.topActivity.getPackageName());
                }
                if (rti.baseActivity.getPackageName().equals(context.getPackageName()) && rti.topActivity.getPackageName().toLowerCase(Locale.ROOT).contains("camera")) {
                    return true;
                }
            }
            return false;
        }
    }

    public static synchronized ZzCameraWatcher getInstance() {
        ZzCameraWatcher zzCameraWatcher;
        synchronized (ZzCameraWatcher.class) {
            if (sInstance == null) {
                sInstance = new ZzCameraWatcher(App.getAppContext());
            }
            zzCameraWatcher = sInstance;
        }
        return zzCameraWatcher;
    }

    private ZzCameraWatcher(Context context) {
        this.mContext = context;
    }

    public synchronized boolean isCameraAutoShareSettingEnabled() {
        return ((Boolean) Z.ZZ_CAMERA_AUTO_SHARE_ENABLED.read()).booleanValue();
    }

    public synchronized void setCameraAutoShareSettingEnabled(boolean enabled) {
        Z.ZZ_CAMERA_AUTO_SHARE_ENABLED.write(Boolean.valueOf(enabled));
        updateState();
    }

    private synchronized void updateState() {
        if (GCSP.getGroupManager().isAnyGroupActive()) {
            if (isCameraAutoShareSettingEnabled()) {
                if (!isActive()) {
                    start();
                }
            } else if (isActive()) {
                stop();
            }
        }
    }

    private synchronized void start() {
        CameraReceiver.setEnabled(this.mContext, true);
    }

    /* access modifiers changed from: private */
    public synchronized void stop() {
        CameraReceiver.setEnabled(this.mContext, false);
    }

    private synchronized boolean isActive() {
        return CameraReceiver.isEnabled(this.mContext);
    }
}
