package com.motorola.sharing;

import com.motorola.sharing.Abuse.AbuseAction;
import com.motorola.sharing.ContentListener.Event;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.util.Logger;

public interface ContentShareListener extends ContentListener {

    public static class ContentCancelShareEvent extends Event {
        protected static final String TAG = Logger.getGcspTag("CancelShareEvent");
        public ContentContainer container;
        public Endpoint endpoint;
        public Substatus substatus;

        public enum Substatus {
            EXPIRED,
            GROUP_ENDED
        }

        ContentCancelShareEvent(Status status, Substatus substatus2, ContentContainer container2, Endpoint endpoint2) {
            this.status = status;
            this.substatus = substatus2;
            this.container = container2;
            this.endpoint = endpoint2;
        }
    }

    public static class ContentShareEvent extends Event {
        protected static final String TAG = Logger.getGcspTag("ContShareEvent");
        public AbuseAction abuseAction = AbuseAction.NONE;
        public ContentContainer container;
        public Endpoint endpoint;
        public Substatus substatus;

        public enum Substatus {
            META_SHARED,
            PROGRESS,
            ESTIMATED_TRANSFER_TIME_TOO_LONG,
            TRANSFER_TAKING_TOO_LONG,
            ABUSE,
            TRANSFER_TIMEOUT,
            TRANSFER_FAILED,
            AUTH,
            GENERAL,
            NETWORK,
            INVALID_METADATA,
            INVALID_GROUP,
            INTERRUPTED,
            SHARED,
            RESUMED
        }

        public ContentShareEvent(Status status, Substatus substatus2, ContentContainer container2, Endpoint endpoint2) {
            this.status = status;
            this.substatus = substatus2;
            this.container = container2;
            this.endpoint = endpoint2;
        }
    }

    public static class ContentStopShareEvent extends Event {
        protected static final String TAG = Logger.getGcspTag("StopShareEvent");
        public ContentContainer container;
        public Endpoint endpoint;
        public String shareId;
        public Substatus substatus;

        public enum Substatus {
            AUTH,
            GENERAL,
            NETWORK,
            NOT_SHARED
        }

        ContentStopShareEvent(Status status, Substatus substatus2, ContentContainer container2, Endpoint endpoint2) {
            this.status = status;
            this.substatus = substatus2;
            this.container = container2;
            if (container2 != null) {
                this.shareId = container2.shareId;
            }
            this.endpoint = endpoint2;
        }
    }

    void onContentCancelShareEvent(ContentCancelShareEvent contentCancelShareEvent);

    void onContentShareEvent(ContentShareEvent contentShareEvent);

    void onContentStopShareEvent(ContentStopShareEvent contentStopShareEvent);
}
