package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;

public class ex implements Creator<ew> {
    static void a(ew ewVar, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.c(parcel, 1, ewVar.getVersionCode());
        b.a(parcel, 2, (Parcelable) ewVar.ei(), i, false);
        b.D(parcel, p);
    }

    /* renamed from: T */
    public ew[] newArray(int i) {
        return new ew[i];
    }

    /* renamed from: q */
    public ew createFromParcel(Parcel parcel) {
        int o = a.o(parcel);
        int i = 0;
        ey eyVar = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    i = a.g(parcel, n);
                    break;
                case 2:
                    eyVar = (ey) a.a(parcel, n, (Creator<T>) ey.CREATOR);
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new ew(i, eyVar);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }
}
