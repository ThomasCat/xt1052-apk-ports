package com.motorola.sharing.util;

import org.json.JSONObject;

public interface GooglePlusProfileListener {

    public enum Status {
        SUCCESS,
        ERROR
    }

    public enum Substatus {
        NO_ERROR,
        NETWORK_ERROR,
        AUTHENTICATION_ERROR,
        CLIENT_ERROR,
        SERVER_ERROR
    }

    void onProfile(JSONObject jSONObject, Status status, Substatus substatus);
}
