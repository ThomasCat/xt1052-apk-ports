package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.drive.query.Query;

public class ai implements Creator<QueryRequest> {
    static void a(QueryRequest queryRequest, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.c(parcel, 1, queryRequest.wj);
        b.a(parcel, 2, (Parcelable) queryRequest.Ef, i, false);
        b.D(parcel, p);
    }

    /* renamed from: W */
    public QueryRequest createFromParcel(Parcel parcel) {
        int o = a.o(parcel);
        int i = 0;
        Query query = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    i = a.g(parcel, n);
                    break;
                case 2:
                    query = (Query) a.a(parcel, n, Query.CREATOR);
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new QueryRequest(i, query);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }

    /* renamed from: aB */
    public QueryRequest[] newArray(int i) {
        return new QueryRequest[i];
    }
}
