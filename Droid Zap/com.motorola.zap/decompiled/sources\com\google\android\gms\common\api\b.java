package com.google.android.gms.common.api;

import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ApiOptions;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.a.C0003a;
import com.google.android.gms.internal.ee;
import com.google.android.gms.internal.ei;
import com.google.android.gms.internal.er;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

final class b implements GoogleApiClient {
    /* access modifiers changed from: private */
    public int zA;
    /* access modifiers changed from: private */
    public int zB = 4;
    /* access modifiers changed from: private */
    public int zC = 0;
    private boolean zD = false;
    private int zE;
    /* access modifiers changed from: private */
    public long zF = 5000;
    final Handler zG;
    /* access modifiers changed from: private */
    public final Bundle zH = new Bundle();
    private final Map<com.google.android.gms.common.api.Api.b<?>, com.google.android.gms.common.api.Api.a> zI = new HashMap();
    /* access modifiers changed from: private */
    public boolean zJ;
    final Set<c> zK = new HashSet();
    final ConnectionCallbacks zL = new ConnectionCallbacks() {
        public void onConnected(Bundle connectionHint) {
            b.this.zv.lock();
            try {
                if (b.this.zB == 1) {
                    if (connectionHint != null) {
                        b.this.zH.putAll(connectionHint);
                    }
                    b.this.dy();
                }
            } finally {
                b.this.zv.unlock();
            }
        }

        public void onConnectionSuspended(int cause) {
            b.this.zv.lock();
            try {
                b.this.G(cause);
                switch (cause) {
                    case 1:
                        if (!b.this.dA()) {
                            b.this.zC = 2;
                            b.this.zG.sendMessageDelayed(b.this.zG.obtainMessage(1), b.this.zF);
                            break;
                        } else {
                            b.this.zv.unlock();
                            return;
                        }
                    case 2:
                        b.this.connect();
                        break;
                }
            } finally {
                b.this.zv.unlock();
            }
        }
    };
    private final com.google.android.gms.internal.ei.b zM = new com.google.android.gms.internal.ei.b() {
        public Bundle cY() {
            return null;
        }

        public boolean dC() {
            return b.this.zJ;
        }

        public boolean isConnected() {
            return b.this.isConnected();
        }
    };
    private final a zm = new a() {
        public void b(c cVar) {
            b.this.zv.lock();
            try {
                b.this.zK.remove(cVar);
            } finally {
                b.this.zv.unlock();
            }
        }
    };
    /* access modifiers changed from: private */
    public final Lock zv = new ReentrantLock();
    private final Condition zw = this.zv.newCondition();
    private final ei zx;
    final Queue<c<?>> zy = new LinkedList();
    /* access modifiers changed from: private */
    public ConnectionResult zz;

    interface a {
        void b(c cVar);
    }

    /* renamed from: com.google.android.gms.common.api.b$b reason: collision with other inner class name */
    class C0004b extends Handler {
        C0004b(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                b.this.zv.lock();
                try {
                    if (!b.this.isConnected() && !b.this.isConnecting()) {
                        b.this.connect();
                    }
                } finally {
                    b.this.zv.unlock();
                }
            } else {
                Log.wtf("GoogleApiClientImpl", "Don't know how to handle this message.");
            }
        }
    }

    interface c<A extends com.google.android.gms.common.api.Api.a> {
        void a(a aVar);

        void b(A a) throws DeadObjectException;

        com.google.android.gms.common.api.Api.b<A> dp();

        int dr();

        void du();
    }

    public b(Context context, Looper looper, ee eeVar, Map<Api, ApiOptions> map, Set<ConnectionCallbacks> set, Set<OnConnectionFailedListener> set2) {
        this.zx = new ei(context, looper, this.zM);
        this.zG = new C0004b(looper);
        for (ConnectionCallbacks registerConnectionCallbacks : set) {
            this.zx.registerConnectionCallbacks(registerConnectionCallbacks);
        }
        for (OnConnectionFailedListener registerConnectionFailedListener : set2) {
            this.zx.registerConnectionFailedListener(registerConnectionFailedListener);
        }
        for (Api api : map.keySet()) {
            final com.google.android.gms.common.api.Api.b dp = api.dp();
            ApiOptions apiOptions = (ApiOptions) map.get(api);
            this.zI.put(dp, dp.b(context, looper, eeVar, apiOptions, this.zL, new OnConnectionFailedListener() {
                public void onConnectionFailed(ConnectionResult result) {
                    b.this.zv.lock();
                    try {
                        if (b.this.zz == null || dp.getPriority() < b.this.zA) {
                            b.this.zz = result;
                            b.this.zA = dp.getPriority();
                        }
                        b.this.dy();
                    } finally {
                        b.this.zv.unlock();
                    }
                }
            }));
        }
    }

    /* access modifiers changed from: private */
    public void G(int i) {
        this.zv.lock();
        try {
            if (this.zB != 3) {
                if (i == -1) {
                    if (isConnecting()) {
                        Iterator it = this.zy.iterator();
                        while (it.hasNext()) {
                            if (((c) it.next()).dr() != 1) {
                                it.remove();
                            }
                        }
                    } else {
                        this.zy.clear();
                    }
                    if (this.zz == null && !this.zy.isEmpty()) {
                        this.zD = true;
                        return;
                    }
                }
                boolean isConnecting = isConnecting();
                boolean isConnected = isConnected();
                this.zB = 3;
                if (isConnecting) {
                    if (i == -1) {
                        this.zz = null;
                    }
                    this.zw.signalAll();
                }
                for (c du : this.zK) {
                    du.du();
                }
                this.zK.clear();
                this.zJ = false;
                for (com.google.android.gms.common.api.Api.a aVar : this.zI.values()) {
                    if (aVar.isConnected()) {
                        aVar.disconnect();
                    }
                }
                this.zJ = true;
                this.zB = 4;
                if (isConnected) {
                    if (i != -1) {
                        this.zx.P(i);
                    }
                    this.zJ = false;
                }
            }
            this.zv.unlock();
        } finally {
            this.zv.unlock();
        }
    }

    private <A extends com.google.android.gms.common.api.Api.a> void a(c<A> cVar) throws DeadObjectException {
        this.zv.lock();
        try {
            er.a(isConnected(), "GoogleApiClient is not connected yet.");
            er.a(cVar.dp() != null, "This task can not be executed or enqueued (it's probably a Batch or malformed)");
            if (cVar instanceof Releasable) {
                this.zK.add(cVar);
                cVar.a(this.zm);
            }
            cVar.b(a(cVar.dp()));
        } finally {
            this.zv.unlock();
        }
    }

    /* access modifiers changed from: private */
    public boolean dA() {
        this.zv.lock();
        try {
            return this.zC != 0;
        } finally {
            this.zv.unlock();
        }
    }

    private void dB() {
        this.zv.lock();
        try {
            this.zC = 0;
            this.zG.removeMessages(1);
        } finally {
            this.zv.unlock();
        }
    }

    /* access modifiers changed from: private */
    public void dy() {
        this.zv.lock();
        try {
            this.zE--;
            if (this.zE == 0) {
                if (this.zz != null) {
                    this.zD = false;
                    G(3);
                    if (dA()) {
                        this.zC--;
                    }
                    if (dA()) {
                        this.zG.sendMessageDelayed(this.zG.obtainMessage(1), this.zF);
                    } else {
                        this.zx.a(this.zz);
                    }
                    this.zJ = false;
                } else {
                    this.zB = 2;
                    dB();
                    this.zw.signalAll();
                    dz();
                    if (this.zD) {
                        this.zD = false;
                        G(-1);
                    } else {
                        this.zx.b(this.zH.isEmpty() ? null : this.zH);
                    }
                }
            }
        } finally {
            this.zv.unlock();
        }
    }

    private void dz() {
        er.a(isConnected(), "GoogleApiClient is not connected yet.");
        this.zv.lock();
        while (!this.zy.isEmpty()) {
            try {
                a((c) this.zy.remove());
            } catch (DeadObjectException e) {
                Log.w("GoogleApiClientImpl", "Service died while flushing queue", e);
            } catch (Throwable th) {
                this.zv.unlock();
                throw th;
            }
        }
        this.zv.unlock();
    }

    public <C extends com.google.android.gms.common.api.Api.a> C a(com.google.android.gms.common.api.Api.b<C> bVar) {
        C c2 = (com.google.android.gms.common.api.Api.a) this.zI.get(bVar);
        er.b(c2, (Object) "Appropriate Api was not requested.");
        return c2;
    }

    public <A extends com.google.android.gms.common.api.Api.a, T extends C0003a<? extends Result, A>> T a(T t) {
        this.zv.lock();
        try {
            if (isConnected()) {
                b(t);
            } else {
                this.zy.add(t);
            }
            return t;
        } finally {
            this.zv.unlock();
        }
    }

    public <A extends com.google.android.gms.common.api.Api.a, T extends C0003a<? extends Result, A>> T b(T t) {
        er.a(isConnected(), "GoogleApiClient is not connected yet.");
        dz();
        try {
            a((c<A>) t);
        } catch (DeadObjectException e) {
            G(1);
        }
        return t;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0058, code lost:
        if (isConnected() == false) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005a, code lost:
        r0 = com.google.android.gms.common.ConnectionResult.yI;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0064, code lost:
        if (r4.zz == null) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0066, code lost:
        r0 = r4.zz;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0068, code lost:
        r4.zv.unlock();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r0 = new com.google.android.gms.common.ConnectionResult(13, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0076, code lost:
        r4.zv.unlock();
     */
    public ConnectionResult blockingConnect(long timeout, TimeUnit unit) {
        ConnectionResult connectionResult;
        er.a(Looper.myLooper() != Looper.getMainLooper(), "blockingConnect must not be called on the UI thread");
        this.zv.lock();
        try {
            connect();
            long nanos = unit.toNanos(timeout);
            while (true) {
                if (!isConnecting()) {
                    break;
                }
                nanos = this.zw.awaitNanos(nanos);
                if (nanos <= 0) {
                    connectionResult = new ConnectionResult(14, null);
                    break;
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            connectionResult = new ConnectionResult(15, null);
        } finally {
            this.zv.unlock();
        }
        return connectionResult;
    }

    public void connect() {
        this.zv.lock();
        try {
            this.zD = false;
            if (!isConnected() && !isConnecting()) {
                this.zJ = true;
                this.zz = null;
                this.zB = 1;
                this.zH.clear();
                this.zE = this.zI.size();
                for (com.google.android.gms.common.api.Api.a connect : this.zI.values()) {
                    connect.connect();
                }
                this.zv.unlock();
            }
        } finally {
            this.zv.unlock();
        }
    }

    public void disconnect() {
        dB();
        G(-1);
    }

    public boolean isConnected() {
        this.zv.lock();
        try {
            return this.zB == 2;
        } finally {
            this.zv.unlock();
        }
    }

    public boolean isConnecting() {
        boolean z = true;
        this.zv.lock();
        try {
            if (this.zB != 1) {
                z = false;
            }
            return z;
        } finally {
            this.zv.unlock();
        }
    }

    public boolean isConnectionCallbacksRegistered(ConnectionCallbacks listener) {
        return this.zx.isConnectionCallbacksRegistered(listener);
    }

    public boolean isConnectionFailedListenerRegistered(OnConnectionFailedListener listener) {
        return this.zx.isConnectionFailedListenerRegistered(listener);
    }

    public void reconnect() {
        disconnect();
        connect();
    }

    public void registerConnectionCallbacks(ConnectionCallbacks listener) {
        this.zx.registerConnectionCallbacks(listener);
    }

    public void registerConnectionFailedListener(OnConnectionFailedListener listener) {
        this.zx.registerConnectionFailedListener(listener);
    }

    public void unregisterConnectionCallbacks(ConnectionCallbacks listener) {
        this.zx.unregisterConnectionCallbacks(listener);
    }

    public void unregisterConnectionFailedListener(OnConnectionFailedListener listener) {
        this.zx.unregisterConnectionFailedListener(listener);
    }
}
