package com.motorola.zap.ui;

import android.database.Cursor;
import com.motorola.zap.provider.TCH;

public class ZapHistoryInfo {
    public static final int TYPE_PUBLIC_CONTENT_SHARE = 0;
    public static final int TYPE_ZAP_DOWNLOAD_IN_PROGRESS = 7;
    public static final int TYPE_ZAP_UPLOAD_IN_PROGRESS = 6;
    public static final int TYPE_ZZ_ACTIVE_INVITE_IN = 4;
    public static final int TYPE_ZZ_ACTIVE_INVITE_OUT = 5;
    public static final int TYPE_ZZ_CONTENT_SHARE = 1;
    public static final int TYPE_ZZ_INVITE_IN = 2;
    public static final int TYPE_ZZ_INVITE_OUT = 3;
    public static final int TYPE_ZZ_INVITE_REQUEST = 8;

    public static int getCardType(Cursor c) {
        if (TCH.isZzInvite(c)) {
            return 8;
        }
        if (TCH.isZzSession(c)) {
            if (TCH.isActiveZz(c)) {
                if (TCH.isUserZzOwner(c)) {
                    return 5;
                }
                return 4;
            } else if (TCH.isUserZzOwner(c)) {
                return 3;
            } else {
                return 2;
            }
        } else if (TCH.isTransferInProgress(c)) {
            if (TCH.isUploadInProgress(c)) {
                return 6;
            }
            return 7;
        } else if (TCH.isSharedInZz(c)) {
            return 1;
        } else {
            return 0;
        }
    }
}
