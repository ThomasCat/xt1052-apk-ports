package com.motorola.zap.location;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.motorola.zap.LogUtils;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.app.App;
import java.util.List;
import java.util.Locale;

public class BlastLocationProvider {
    /* access modifiers changed from: private */
    public static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private static BlastLocationProvider sInstance = null;
    private static Location sLastLocationInfoUsed = null;
    /* access modifiers changed from: private */
    public Context mContext = null;
    /* access modifiers changed from: private */
    public GpsLocationListener mGpsLocListener = null;
    /* access modifiers changed from: private */
    public boolean mGpsUpdateInProgress = false;
    /* access modifiers changed from: private */
    public long mLastGpsUpdateTime = 0;
    /* access modifiers changed from: private */
    public NetworkLocationListener mNetworkLocListener = null;
    /* access modifiers changed from: private */
    public boolean mNetworkUpdateInProgress = false;
    private RegisterLocationListeners mRegisterLocListeners = null;
    /* access modifiers changed from: private */
    public Handler mScheduler = null;
    /* access modifiers changed from: private */
    public UnregisterLocationListeners mUnregisterLocListeners = null;
    /* access modifiers changed from: private */
    public boolean mUseGpsLocation = false;
    /* access modifiers changed from: private */
    public boolean mUseNetworkLocation = false;

    private abstract class BaseLocationListener implements LocationListener {
        public abstract String getProviderName();

        private BaseLocationListener() {
        }

        public void onLocationChanged(Location location) {
            if (location != null && Log.isLoggable("Zap-LocationProvider", 4)) {
                Log.i("Zap-LocationProvider", ">> Location Update - Provider: " + location.getProvider() + " - Accuracy " + location.getAccuracy());
            }
        }

        public void onProviderDisabled(String provider) {
            if (BlastLocationProvider.DBG) {
                Log.d("Zap-LocationProvider", "onProviderDisabled() - Provider: " + provider);
            }
        }

        public void onProviderEnabled(String provider) {
            if (BlastLocationProvider.DBG) {
                Log.d("Zap-LocationProvider", "onProviderEnabled() - Provider: " + provider);
            }
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (BlastLocationProvider.DBG) {
                Log.d("Zap-LocationProvider", "onStatusChanged invoked - Provider: " + provider + " - status: " + status);
            }
        }

        public void registerListener() {
            if (Log.isLoggable("Zap-LocationProvider", 4)) {
                Log.i("Zap-LocationProvider", "registerListener() - Provider: " + getProviderName());
            }
            LocationManager locationManager = (LocationManager) BlastLocationProvider.this.mContext.getSystemService("location");
            if (locationManager != null) {
                try {
                    locationManager.requestLocationUpdates(getProviderName(), 0, BitmapDescriptorFactory.HUE_RED, this);
                } catch (IllegalArgumentException e) {
                    if (Log.isLoggable("Zap-LocationProvider", 6)) {
                        Log.e("Zap-LocationProvider", "Unable to register location listener", e);
                    }
                }
            }
        }

        public void unregisterListener() {
            if (Log.isLoggable("Zap-LocationProvider", 4)) {
                Log.i("Zap-LocationProvider", "unregisterListener() - Provider: " + getProviderName());
            }
            LocationManager locationManager = (LocationManager) BlastLocationProvider.this.mContext.getSystemService("location");
            if (locationManager != null) {
                locationManager.removeUpdates(this);
            }
        }

        public void requestSingleUpdate() {
            if (Log.isLoggable("Zap-LocationProvider", 4)) {
                Log.i("Zap-LocationProvider", "requestSingleUpdate() - Provider: " + getProviderName());
            }
            LocationManager locationManager = (LocationManager) BlastLocationProvider.this.mContext.getSystemService("location");
            if (locationManager != null) {
                locationManager.requestSingleUpdate(getProviderName(), this, null);
            }
        }

        public boolean isProviderEnabled() {
            return ((LocationManager) BlastLocationProvider.this.mContext.getSystemService("location")).isProviderEnabled(getProviderName());
        }
    }

    public static class GeoLocationInfo {
        public String city;
        public String country;
        public String state;

        public GeoLocationInfo(String city2, String state2, String country2) {
            this.city = city2;
            this.state = state2;
            this.country = country2;
        }

        public String toString() {
            return "GeoLocationInfo - City: " + this.city + " - State: " + this.state + " - Country: " + this.country;
        }
    }

    private class GpsLocationListener extends BaseLocationListener {
        private GpsLocationListener() {
            super();
        }

        public String getProviderName() {
            return "gps";
        }

        public void onLocationChanged(Location location) {
            if (location != null) {
                super.onLocationChanged(location);
                if (getProviderName().equals("gps") && location.getAccuracy() < 100.0f) {
                    long now = System.currentTimeMillis();
                    if (now - BlastLocationProvider.this.mLastGpsUpdateTime < 3000) {
                        if (BlastLocationProvider.DBG) {
                            Log.d("Zap-LocationProvider", "Received GPS fix");
                        }
                        BlastLocationProvider.this.stopUpdates();
                    }
                    BlastLocationProvider.this.mLastGpsUpdateTime = now;
                }
            }
        }
    }

    public static class LocationResult {
        public Location location;
        /* access modifiers changed from: private */
        public boolean satisfied;

        private LocationResult(Location location2) {
            this.location = null;
            this.satisfied = false;
            this.location = location2;
        }

        public long getAge() {
            if (this.location != null) {
                return BlastLocationProvider.getLocationAgeMillis(this.location);
            }
            return 0;
        }

        public long getTime() {
            if (this.location == null) {
                return -1;
            }
            return this.location.getTime();
        }

        public boolean hasSatisfiedLocation() {
            return this.location != null && this.satisfied;
        }
    }

    private class NetworkLocationListener extends BaseLocationListener {
        private NetworkLocationListener() {
            super();
        }

        public String getProviderName() {
            return "network";
        }
    }

    private class RegisterLocationListeners implements Runnable {
        private RegisterLocationListeners() {
        }

        public void run() {
            boolean newRegistration = false;
            if (BlastLocationProvider.this.mUseGpsLocation && !BlastLocationProvider.this.mGpsUpdateInProgress) {
                newRegistration = true;
                BlastLocationProvider.this.mGpsUpdateInProgress = true;
                BlastLocationProvider.this.mGpsLocListener.registerListener();
            }
            if (BlastLocationProvider.this.mUseNetworkLocation && !BlastLocationProvider.this.mNetworkUpdateInProgress) {
                newRegistration = true;
                BlastLocationProvider.this.mNetworkUpdateInProgress = true;
                BlastLocationProvider.this.mNetworkLocListener.registerListener();
            }
            if (newRegistration) {
                BlastLocationProvider.this.mScheduler.removeCallbacks(BlastLocationProvider.this.mUnregisterLocListeners);
                BlastLocationProvider.this.mScheduler.postDelayed(BlastLocationProvider.this.mUnregisterLocListeners, ((Long) Z.LOCATION_UPDATE_DURATION.read()).longValue());
            }
        }
    }

    private class UnregisterLocationListeners implements Runnable {
        private UnregisterLocationListeners() {
        }

        public void run() {
            BlastLocationProvider.this.mGpsLocListener.unregisterListener();
            BlastLocationProvider.this.mGpsUpdateInProgress = false;
            BlastLocationProvider.this.mNetworkLocListener.unregisterListener();
            BlastLocationProvider.this.mNetworkUpdateInProgress = false;
        }
    }

    public static synchronized BlastLocationProvider getInstance() {
        BlastLocationProvider blastLocationProvider;
        synchronized (BlastLocationProvider.class) {
            if (sInstance == null) {
                sInstance = new BlastLocationProvider(App.getAppContext());
            }
            blastLocationProvider = sInstance;
        }
        return blastLocationProvider;
    }

    private BlastLocationProvider(Context context) {
        this.mContext = context;
        this.mGpsLocListener = new GpsLocationListener();
        this.mNetworkLocListener = new NetworkLocationListener();
        this.mScheduler = new Handler(Looper.getMainLooper());
        this.mRegisterLocListeners = new RegisterLocationListeners();
        this.mUnregisterLocListeners = new UnregisterLocationListeners();
    }

    public synchronized void requestUpdates(boolean gpsProvider, boolean networkProvider) {
        if (Log.isLoggable("Zap-LocationProvider", 4)) {
            Log.i("Zap-LocationProvider", "Requesting Location Updates - Provider - GPS: " + gpsProvider + " - Network: " + networkProvider);
        }
        this.mUseGpsLocation = gpsProvider;
        this.mUseNetworkLocation = networkProvider;
        this.mScheduler.post(this.mRegisterLocListeners);
    }

    public synchronized void stopUpdates() {
        if (Log.isLoggable("Zap-LocationProvider", 4)) {
            Log.i("Zap-LocationProvider", "Stopping Location Provider");
        }
        this.mGpsLocListener.unregisterListener();
        this.mGpsUpdateInProgress = false;
        this.mNetworkLocListener.unregisterListener();
        this.mNetworkUpdateInProgress = false;
        this.mScheduler.removeCallbacks(this.mRegisterLocListeners);
        this.mScheduler.removeCallbacks(this.mUnregisterLocListeners);
    }

    public void requestSingleUpdate(boolean gpsProvider) {
        if (gpsProvider) {
            this.mGpsLocListener.requestSingleUpdate();
        } else {
            this.mNetworkLocListener.requestSingleUpdate();
        }
    }

    public boolean isLocationEnabled() {
        boolean gpsEnabled = isGpsLocationEnabled();
        boolean networkEnabled = isNetworkLocationEnabled();
        if (DBG) {
            Log.d("Zap-LocationProvider", "Location - GPS location enabled: " + gpsEnabled + " - Network location enabled: " + networkEnabled);
        }
        return gpsEnabled || networkEnabled;
    }

    public boolean isGpsLocationEnabled() {
        return this.mGpsLocListener.isProviderEnabled();
    }

    public boolean isNetworkLocationEnabled() {
        return this.mNetworkLocListener.isProviderEnabled();
    }

    public boolean areAllLocationSettingsEnabled() {
        WifiManager wifiManager = (WifiManager) this.mContext.getSystemService("wifi");
        if (Log.isLoggable("Zap-LocationProvider", 4)) {
            Log.i("Zap-LocationProvider", "Wi-Fi enabled: " + (wifiManager != null ? wifiManager.isWifiEnabled() : false));
            Log.i("Zap-LocationProvider", "GPS Enabled: " + isGpsLocationEnabled());
            Log.i("Zap-LocationProvider", "Network Location Enabled: " + isNetworkLocationEnabled());
        }
        if (wifiManager != null && wifiManager.isWifiEnabled() && isGpsLocationEnabled() && isNetworkLocationEnabled()) {
            return true;
        }
        return false;
    }

    public static Location getLastLocationInfoUsed() {
        return sLastLocationInfoUsed;
    }

    public static LocationResult getBestLastKnownLocation(Context context, long maxAge) {
        String str;
        if (DBG) {
            Log.d("Zap-LocationProvider", "getBestLastKnownLocation - maxAge: " + maxAge);
        }
        LocationManager locationManager = (LocationManager) context.getSystemService("location");
        LocationResult result = new LocationResult(null);
        List<String> providers = locationManager.getAllProviders();
        if (providers == null) {
            if (Log.isLoggable("Zap-LocationProvider", 5)) {
                Log.w("Zap-LocationProvider", "Could not get any location providers");
            }
            return result;
        }
        if (DBG) {
            Log.d("Zap-LocationProvider", "Providers: " + providers.size());
        }
        for (String provider : providers) {
            LocationResult temp = new LocationResult(locationManager.getLastKnownLocation(provider));
            if (temp.location != null) {
                long age = temp.getAge();
                if (DBG) {
                    Log.d("Zap-LocationProvider", "Known Location (" + temp.location.getProvider() + ") - lat, lng: (" + temp.location.getLatitude() + ", " + temp.location.getLongitude() + ") - Accuracy: " + temp.location.getAccuracy() + " - Age: " + (age / 1000) + " secs");
                }
                if (age > maxAge) {
                    if (DBG) {
                        Log.d("Zap-LocationProvider", "Too old - should be ignored");
                    }
                    temp.satisfied = false;
                } else if (temp.location.getAccuracy() > ((Float) Z.KNOWN_LOCATION_REQUIRED_ACCURACY.read()).floatValue()) {
                    if (DBG) {
                        Log.d("Zap-LocationProvider", "Bad accuracy - should be ignored");
                    }
                    temp.satisfied = false;
                } else {
                    temp.satisfied = true;
                }
                if (result.location == null) {
                    result = temp;
                } else if (compareLocations(temp.location, result.location) > 0) {
                    if (DBG) {
                        Log.d("Zap-LocationProvider", "-- Best location so far --");
                    }
                    result = temp;
                }
            }
        }
        if (result.location != null && result.satisfied) {
            sLastLocationInfoUsed = result.location;
            if (Log.isLoggable("Zap-LocationProvider", 4)) {
                Log.i("Zap-LocationProvider", "** Returning Location (" + result.location.getProvider() + ") - Accuracy: " + result.location.getAccuracy() + " - Age: " + (result.getAge() / 1000) + " secs");
            }
        } else if (Log.isLoggable("Zap-LocationProvider", 4)) {
            String str2 = "Zap-LocationProvider";
            StringBuilder append = new StringBuilder().append("** No location available");
            if (result.location != null) {
                str = " satisfying requirenments";
            } else {
                str = ZapConstants.GOOGLE_ACCOUNT_NONE;
            }
            Log.i(str2, append.append(str).toString());
        }
        return result;
    }

    private static int compareLocations(Location loc1, Location loc2) {
        long diffTime = loc1.getTime() - loc2.getTime();
        float diffAccuracy = loc2.getAccuracy() - loc1.getAccuracy();
        if (diffTime == 0 && diffAccuracy == BitmapDescriptorFactory.HUE_RED) {
            return 0;
        }
        if (diffTime > ZapConstants.LOCATION_MAX_AGE_NO_MODALITY_SUPPORT || (Math.abs(diffTime) < ZapConstants.LOCATION_MAX_AGE_NO_MODALITY_SUPPORT && (diffAccuracy > 50.0f || (Math.abs(diffAccuracy) < 50.0f && ((diffAccuracy > BitmapDescriptorFactory.HUE_RED && diffTime > 0) || diffTime > 30000))))) {
            return 1;
        }
        return -1;
    }

    @SuppressLint({"NewApi"})
    public static long getLocationAgeMillis(Location location) {
        if (VERSION.SDK_INT >= 17) {
            return SystemClock.elapsedRealtime() - (location.getElapsedRealtimeNanos() / 1000000);
        }
        long age = System.currentTimeMillis() - location.getTime();
        if (age < 0) {
            return 0;
        }
        return age;
    }

    public static GeoLocationInfo getLocationInfo(double latitude, double longitude, Context context) {
        try {
            List<Address> addresses = new Geocoder(context, Locale.getDefault()).getFromLocation(latitude, longitude, 1);
            if (addresses == null || addresses.size() == 0) {
                if (DBG) {
                    Log.d("Zap-LocationProvider", "No address returned.");
                }
                return null;
            }
            Address addr = (Address) addresses.get(0);
            return new GeoLocationInfo(addr.getLocality(), addr.getAdminArea(), addr.getCountryName());
        } catch (Exception e) {
            if (Log.isLoggable("Zap-LocationProvider", 6)) {
                Log.e("Zap-LocationProvider", "Could not get geo location from address. " + e.toString());
            }
            return null;
        }
    }
}
