package com.google.android.gms.internal;

import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public abstract class eg implements SafeParcelable {
    private static final Object Br = new Object();
    private static ClassLoader Bs = null;
    private static Integer Bt = null;
    private boolean Bu = false;

    private static boolean a(Class<?> cls) {
        boolean z = false;
        try {
            return SafeParcelable.NULL.equals(cls.getField("NULL").get(null));
        } catch (IllegalAccessException | NoSuchFieldException e) {
            return z;
        }
    }

    /* access modifiers changed from: protected */
    public static boolean ae(String str) {
        ClassLoader dX = dX();
        if (dX == null) {
            return true;
        }
        try {
            return a(dX.loadClass(str));
        } catch (Exception e) {
            return false;
        }
    }

    protected static ClassLoader dX() {
        ClassLoader classLoader;
        synchronized (Br) {
            classLoader = Bs;
        }
        return classLoader;
    }

    /* access modifiers changed from: protected */
    public static Integer dY() {
        Integer num;
        synchronized (Br) {
            num = Bt;
        }
        return num;
    }

    /* access modifiers changed from: protected */
    public boolean dZ() {
        return this.Bu;
    }
}
