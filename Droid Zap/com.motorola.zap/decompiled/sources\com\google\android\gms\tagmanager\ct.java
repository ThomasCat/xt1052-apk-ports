package com.google.android.gms.tagmanager;

import android.content.Context;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.google.android.gms.internal.c.i;
import com.google.android.gms.tagmanager.cr.e;
import com.motorola.zap.ZapConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class ct {
    private static final by<com.google.android.gms.internal.d.a> WH = new by<>(di.ku(), true);
    private final DataLayer TN;
    private final com.google.android.gms.tagmanager.cr.c WI;
    private final ag WJ;
    private final Map<String, aj> WK;
    private final Map<String, aj> WL;
    private final Map<String, aj> WM;
    private final k<com.google.android.gms.tagmanager.cr.a, by<com.google.android.gms.internal.d.a>> WN;
    private final k<String, b> WO;
    private final Set<e> WP;
    private final Map<String, c> WQ;
    private volatile String WR;
    private int WS;

    interface a {
        void a(e eVar, Set<com.google.android.gms.tagmanager.cr.a> set, Set<com.google.android.gms.tagmanager.cr.a> set2, cn cnVar);
    }

    private static class b {
        private by<com.google.android.gms.internal.d.a> WY;
        private com.google.android.gms.internal.d.a Wt;

        public b(by<com.google.android.gms.internal.d.a> byVar, com.google.android.gms.internal.d.a aVar) {
            this.WY = byVar;
            this.Wt = aVar;
        }

        public int getSize() {
            return (this.Wt == null ? 0 : this.Wt.eW()) + ((com.google.android.gms.internal.d.a) this.WY.getObject()).eW();
        }

        public com.google.android.gms.internal.d.a jG() {
            return this.Wt;
        }

        public by<com.google.android.gms.internal.d.a> ka() {
            return this.WY;
        }
    }

    private static class c {
        private final Set<e> WP = new HashSet();
        private final Map<e, List<com.google.android.gms.tagmanager.cr.a>> WZ = new HashMap();
        private final Map<e, List<com.google.android.gms.tagmanager.cr.a>> Xa = new HashMap();
        private final Map<e, List<String>> Xb = new HashMap();
        private final Map<e, List<String>> Xc = new HashMap();
        private com.google.android.gms.tagmanager.cr.a Xd;

        public void a(e eVar, com.google.android.gms.tagmanager.cr.a aVar) {
            List list = (List) this.WZ.get(eVar);
            if (list == null) {
                list = new ArrayList();
                this.WZ.put(eVar, list);
            }
            list.add(aVar);
        }

        public void a(e eVar, String str) {
            List list = (List) this.Xb.get(eVar);
            if (list == null) {
                list = new ArrayList();
                this.Xb.put(eVar, list);
            }
            list.add(str);
        }

        public void b(e eVar) {
            this.WP.add(eVar);
        }

        public void b(e eVar, com.google.android.gms.tagmanager.cr.a aVar) {
            List list = (List) this.Xa.get(eVar);
            if (list == null) {
                list = new ArrayList();
                this.Xa.put(eVar, list);
            }
            list.add(aVar);
        }

        public void b(e eVar, String str) {
            List list = (List) this.Xc.get(eVar);
            if (list == null) {
                list = new ArrayList();
                this.Xc.put(eVar, list);
            }
            list.add(str);
        }

        public void i(com.google.android.gms.tagmanager.cr.a aVar) {
            this.Xd = aVar;
        }

        public Set<e> kb() {
            return this.WP;
        }

        public Map<e, List<com.google.android.gms.tagmanager.cr.a>> kc() {
            return this.WZ;
        }

        public Map<e, List<String>> kd() {
            return this.Xb;
        }

        public Map<e, List<String>> ke() {
            return this.Xc;
        }

        public Map<e, List<com.google.android.gms.tagmanager.cr.a>> kf() {
            return this.Xa;
        }

        public com.google.android.gms.tagmanager.cr.a kg() {
            return this.Xd;
        }
    }

    public ct(Context context, com.google.android.gms.tagmanager.cr.c cVar, DataLayer dataLayer, com.google.android.gms.tagmanager.s.a aVar, com.google.android.gms.tagmanager.s.a aVar2, ag agVar) {
        if (cVar == null) {
            throw new NullPointerException("resource cannot be null");
        }
        this.WI = cVar;
        this.WP = new HashSet(cVar.jJ());
        this.TN = dataLayer;
        this.WJ = agVar;
        this.WN = new l().a(AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, new com.google.android.gms.tagmanager.l.a<com.google.android.gms.tagmanager.cr.a, by<com.google.android.gms.internal.d.a>>() {
            /* renamed from: a */
            public int sizeOf(com.google.android.gms.tagmanager.cr.a aVar, by<com.google.android.gms.internal.d.a> byVar) {
                return ((com.google.android.gms.internal.d.a) byVar.getObject()).eW();
            }
        });
        this.WO = new l().a(AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, new com.google.android.gms.tagmanager.l.a<String, b>() {
            /* renamed from: a */
            public int sizeOf(String str, b bVar) {
                return str.length() + bVar.getSize();
            }
        });
        this.WK = new HashMap();
        b(new i(context));
        b(new s(aVar2));
        b(new w(dataLayer));
        b(new dj(context, dataLayer));
        this.WL = new HashMap();
        c(new q());
        c(new ad());
        c(new ae());
        c(new al());
        c(new am());
        c(new bd());
        c(new be());
        c(new ci());
        c(new dc());
        this.WM = new HashMap();
        a(new b(context));
        a(new c(context));
        a(new e(context));
        a(new f(context));
        a(new g(context));
        a(new h(context));
        a(new m());
        a(new p(this.WI.getVersion()));
        a(new s(aVar));
        a(new u(dataLayer));
        a(new z(context));
        a(new aa());
        a(new ac());
        a(new ah(this));
        a(new an());
        a(new ao());
        a(new ax(context));
        a(new az());
        a(new bc());
        a(new bk(context));
        a(new bz());
        a(new cc());
        a(new cf());
        a(new ch());
        a(new cj(context));
        a(new cu());
        a(new cv());
        a(new de());
        this.WQ = new HashMap();
        for (e eVar : this.WP) {
            if (agVar.jb()) {
                a(eVar.jR(), eVar.jS(), "add macro");
                a(eVar.jW(), eVar.jT(), "remove macro");
                a(eVar.jP(), eVar.jU(), "add tag");
                a(eVar.jQ(), eVar.jV(), "remove tag");
            }
            for (int i = 0; i < eVar.jR().size(); i++) {
                com.google.android.gms.tagmanager.cr.a aVar3 = (com.google.android.gms.tagmanager.cr.a) eVar.jR().get(i);
                String str = "Unknown";
                if (agVar.jb() && i < eVar.jS().size()) {
                    str = (String) eVar.jS().get(i);
                }
                c c2 = c(this.WQ, h(aVar3));
                c2.b(eVar);
                c2.a(eVar, aVar3);
                c2.a(eVar, str);
            }
            for (int i2 = 0; i2 < eVar.jW().size(); i2++) {
                com.google.android.gms.tagmanager.cr.a aVar4 = (com.google.android.gms.tagmanager.cr.a) eVar.jW().get(i2);
                String str2 = "Unknown";
                if (agVar.jb() && i2 < eVar.jT().size()) {
                    str2 = (String) eVar.jT().get(i2);
                }
                c c3 = c(this.WQ, h(aVar4));
                c3.b(eVar);
                c3.b(eVar, aVar4);
                c3.b(eVar, str2);
            }
        }
        for (Entry entry : this.WI.jK().entrySet()) {
            for (com.google.android.gms.tagmanager.cr.a aVar5 : (List) entry.getValue()) {
                if (!di.n((com.google.android.gms.internal.d.a) aVar5.jF().get(com.google.android.gms.internal.b.NOT_DEFAULT_MACRO.toString())).booleanValue()) {
                    c(this.WQ, (String) entry.getKey()).i(aVar5);
                }
            }
        }
    }

    private by<com.google.android.gms.internal.d.a> a(com.google.android.gms.internal.d.a aVar, Set<String> set, dk dkVar) {
        if (!aVar.gi) {
            return new by<>(aVar, true);
        }
        switch (aVar.type) {
            case 2:
                com.google.android.gms.internal.d.a g = cr.g(aVar);
                g.fZ = new com.google.android.gms.internal.d.a[aVar.fZ.length];
                for (int i = 0; i < aVar.fZ.length; i++) {
                    by<com.google.android.gms.internal.d.a> a2 = a(aVar.fZ[i], set, dkVar.bS(i));
                    if (a2 == WH) {
                        return WH;
                    }
                    g.fZ[i] = (com.google.android.gms.internal.d.a) a2.getObject();
                }
                return new by<>(g, false);
            case 3:
                com.google.android.gms.internal.d.a g2 = cr.g(aVar);
                if (aVar.ga.length != aVar.gb.length) {
                    bh.t("Invalid serving value: " + aVar.toString());
                    return WH;
                }
                g2.ga = new com.google.android.gms.internal.d.a[aVar.ga.length];
                g2.gb = new com.google.android.gms.internal.d.a[aVar.ga.length];
                for (int i2 = 0; i2 < aVar.ga.length; i2++) {
                    by<com.google.android.gms.internal.d.a> a3 = a(aVar.ga[i2], set, dkVar.bT(i2));
                    by<com.google.android.gms.internal.d.a> a4 = a(aVar.gb[i2], set, dkVar.bU(i2));
                    if (a3 == WH || a4 == WH) {
                        return WH;
                    }
                    g2.ga[i2] = (com.google.android.gms.internal.d.a) a3.getObject();
                    g2.gb[i2] = (com.google.android.gms.internal.d.a) a4.getObject();
                }
                return new by<>(g2, false);
            case 4:
                if (set.contains(aVar.gc)) {
                    bh.t("Macro cycle detected.  Current macro reference: " + aVar.gc + "." + "  Previous macro references: " + set.toString() + ".");
                    return WH;
                }
                set.add(aVar.gc);
                by<com.google.android.gms.internal.d.a> a5 = dl.a(a(aVar.gc, set, dkVar.jq()), aVar.gh);
                set.remove(aVar.gc);
                return a5;
            case 7:
                com.google.android.gms.internal.d.a g3 = cr.g(aVar);
                g3.gg = new com.google.android.gms.internal.d.a[aVar.gg.length];
                for (int i3 = 0; i3 < aVar.gg.length; i3++) {
                    by<com.google.android.gms.internal.d.a> a6 = a(aVar.gg[i3], set, dkVar.bV(i3));
                    if (a6 == WH) {
                        return WH;
                    }
                    g3.gg[i3] = (com.google.android.gms.internal.d.a) a6.getObject();
                }
                return new by<>(g3, false);
            default:
                bh.t("Unknown type: " + aVar.type);
                return WH;
        }
    }

    private by<com.google.android.gms.internal.d.a> a(String str, Set<String> set, bj bjVar) {
        com.google.android.gms.tagmanager.cr.a aVar;
        this.WS++;
        b bVar = (b) this.WO.get(str);
        if (bVar == null || this.WJ.jb()) {
            c cVar = (c) this.WQ.get(str);
            if (cVar == null) {
                bh.t(jZ() + "Invalid macro: " + str);
                this.WS--;
                return WH;
            }
            by a2 = a(str, cVar.kb(), cVar.kc(), cVar.kd(), cVar.kf(), cVar.ke(), set, bjVar.iS());
            if (((Set) a2.getObject()).isEmpty()) {
                aVar = cVar.kg();
            } else {
                if (((Set) a2.getObject()).size() > 1) {
                    bh.w(jZ() + "Multiple macros active for macroName " + str);
                }
                aVar = (com.google.android.gms.tagmanager.cr.a) ((Set) a2.getObject()).iterator().next();
            }
            if (aVar == null) {
                this.WS--;
                return WH;
            }
            by<com.google.android.gms.internal.d.a> a3 = a(this.WM, aVar, set, bjVar.jh());
            by byVar = a3 == WH ? WH : new by(a3.getObject(), a2.jr() && a3.jr());
            com.google.android.gms.internal.d.a jG = aVar.jG();
            if (byVar.jr()) {
                this.WO.e(str, new b(byVar, jG));
            }
            a(jG, set);
            this.WS--;
            return byVar;
        }
        a(bVar.jG(), set);
        this.WS--;
        return bVar.ka();
    }

    private by<com.google.android.gms.internal.d.a> a(Map<String, aj> map, com.google.android.gms.tagmanager.cr.a aVar, Set<String> set, ck ckVar) {
        boolean z;
        boolean z2 = true;
        com.google.android.gms.internal.d.a aVar2 = (com.google.android.gms.internal.d.a) aVar.jF().get(com.google.android.gms.internal.b.FUNCTION.toString());
        if (aVar2 == null) {
            bh.t("No function id in properties");
            return WH;
        }
        String str = aVar2.gd;
        aj ajVar = (aj) map.get(str);
        if (ajVar == null) {
            bh.t(str + " has no backing implementation.");
            return WH;
        }
        by<com.google.android.gms.internal.d.a> byVar = (by) this.WN.get(aVar);
        if (byVar != null && !this.WJ.jb()) {
            return byVar;
        }
        HashMap hashMap = new HashMap();
        boolean z3 = true;
        for (Entry entry : aVar.jF().entrySet()) {
            by<com.google.android.gms.internal.d.a> a2 = a((com.google.android.gms.internal.d.a) entry.getValue(), set, ckVar.bs((String) entry.getKey()).e((com.google.android.gms.internal.d.a) entry.getValue()));
            if (a2 == WH) {
                return WH;
            }
            if (a2.jr()) {
                aVar.a((String) entry.getKey(), (com.google.android.gms.internal.d.a) a2.getObject());
                z = z3;
            } else {
                z = false;
            }
            hashMap.put(entry.getKey(), a2.getObject());
            z3 = z;
        }
        if (!ajVar.a(hashMap.keySet())) {
            bh.t("Incorrect keys for function " + str + " required " + ajVar.jd() + " had " + hashMap.keySet());
            return WH;
        }
        if (!z3 || !ajVar.iy()) {
            z2 = false;
        }
        by<com.google.android.gms.internal.d.a> byVar2 = new by<>(ajVar.u(hashMap), z2);
        if (z2) {
            this.WN.e(aVar, byVar2);
        }
        ckVar.d((com.google.android.gms.internal.d.a) byVar2.getObject());
        return byVar2;
    }

    private by<Set<com.google.android.gms.tagmanager.cr.a>> a(Set<e> set, Set<String> set2, a aVar, cs csVar) {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        boolean z = true;
        for (e eVar : set) {
            cn jp = csVar.jp();
            by a2 = a(eVar, set2, jp);
            if (((Boolean) a2.getObject()).booleanValue()) {
                aVar.a(eVar, hashSet, hashSet2, jp);
            }
            z = z && a2.jr();
        }
        hashSet.removeAll(hashSet2);
        csVar.b(hashSet);
        return new by<>(hashSet, z);
    }

    private void a(com.google.android.gms.internal.d.a aVar, Set<String> set) {
        if (aVar != null) {
            by<com.google.android.gms.internal.d.a> a2 = a(aVar, set, (dk) new bw());
            if (a2 != WH) {
                Object o = di.o((com.google.android.gms.internal.d.a) a2.getObject());
                if (o instanceof Map) {
                    this.TN.push((Map) o);
                } else if (o instanceof List) {
                    for (Object next : (List) o) {
                        if (next instanceof Map) {
                            this.TN.push((Map) next);
                        } else {
                            bh.w("pushAfterEvaluate: value not a Map");
                        }
                    }
                } else {
                    bh.w("pushAfterEvaluate: value not a Map or List");
                }
            }
        }
    }

    private static void a(List<com.google.android.gms.tagmanager.cr.a> list, List<String> list2, String str) {
        if (list.size() != list2.size()) {
            bh.u("Invalid resource: imbalance of rule names of functions for " + str + " operation. Using default rule name instead");
        }
    }

    private static void a(Map<String, aj> map, aj ajVar) {
        if (map.containsKey(ajVar.jc())) {
            throw new IllegalArgumentException("Duplicate function type name: " + ajVar.jc());
        }
        map.put(ajVar.jc(), ajVar);
    }

    private static c c(Map<String, c> map, String str) {
        c cVar = (c) map.get(str);
        if (cVar != null) {
            return cVar;
        }
        c cVar2 = new c();
        map.put(str, cVar2);
        return cVar2;
    }

    private static String h(com.google.android.gms.tagmanager.cr.a aVar) {
        return di.j((com.google.android.gms.internal.d.a) aVar.jF().get(com.google.android.gms.internal.b.INSTANCE_NAME.toString()));
    }

    private String jZ() {
        if (this.WS <= 1) {
            return ZapConstants.GOOGLE_ACCOUNT_NONE;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(Integer.toString(this.WS));
        for (int i = 2; i < this.WS; i++) {
            sb.append(' ');
        }
        sb.append(": ");
        return sb.toString();
    }

    /* access modifiers changed from: 0000 */
    public by<Boolean> a(com.google.android.gms.tagmanager.cr.a aVar, Set<String> set, ck ckVar) {
        by a2 = a(this.WL, aVar, set, ckVar);
        Boolean n = di.n((com.google.android.gms.internal.d.a) a2.getObject());
        ckVar.d(di.r(n));
        return new by<>(n, a2.jr());
    }

    /* access modifiers changed from: 0000 */
    public by<Boolean> a(e eVar, Set<String> set, cn cnVar) {
        boolean z;
        boolean z2 = true;
        for (com.google.android.gms.tagmanager.cr.a a2 : eVar.jO()) {
            by a3 = a(a2, set, cnVar.jj());
            if (((Boolean) a3.getObject()).booleanValue()) {
                cnVar.f(di.r(Boolean.valueOf(false)));
                return new by<>(Boolean.valueOf(false), a3.jr());
            }
            z2 = z && a3.jr();
        }
        for (com.google.android.gms.tagmanager.cr.a a4 : eVar.jN()) {
            by a5 = a(a4, set, cnVar.jk());
            if (!((Boolean) a5.getObject()).booleanValue()) {
                cnVar.f(di.r(Boolean.valueOf(false)));
                return new by<>(Boolean.valueOf(false), a5.jr());
            }
            z = z && a5.jr();
        }
        cnVar.f(di.r(Boolean.valueOf(true)));
        return new by<>(Boolean.valueOf(true), z);
    }

    /* access modifiers changed from: 0000 */
    public by<Set<com.google.android.gms.tagmanager.cr.a>> a(String str, Set<e> set, Map<e, List<com.google.android.gms.tagmanager.cr.a>> map, Map<e, List<String>> map2, Map<e, List<com.google.android.gms.tagmanager.cr.a>> map3, Map<e, List<String>> map4, Set<String> set2, cs csVar) {
        final Map<e, List<com.google.android.gms.tagmanager.cr.a>> map5 = map;
        final Map<e, List<String>> map6 = map2;
        final Map<e, List<com.google.android.gms.tagmanager.cr.a>> map7 = map3;
        final Map<e, List<String>> map8 = map4;
        return a(set, set2, (a) new a() {
            public void a(e eVar, Set<com.google.android.gms.tagmanager.cr.a> set, Set<com.google.android.gms.tagmanager.cr.a> set2, cn cnVar) {
                List list = (List) map5.get(eVar);
                List list2 = (List) map6.get(eVar);
                if (list != null) {
                    set.addAll(list);
                    cnVar.jl().b(list, list2);
                }
                List list3 = (List) map7.get(eVar);
                List list4 = (List) map8.get(eVar);
                if (list3 != null) {
                    set2.addAll(list3);
                    cnVar.jm().b(list3, list4);
                }
            }
        }, csVar);
    }

    /* access modifiers changed from: 0000 */
    public by<Set<com.google.android.gms.tagmanager.cr.a>> a(Set<e> set, cs csVar) {
        return a(set, (Set<String>) new HashSet<String>(), (a) new a() {
            public void a(e eVar, Set<com.google.android.gms.tagmanager.cr.a> set, Set<com.google.android.gms.tagmanager.cr.a> set2, cn cnVar) {
                set.addAll(eVar.jP());
                set2.addAll(eVar.jQ());
                cnVar.jn().b(eVar.jP(), eVar.jU());
                cnVar.jo().b(eVar.jQ(), eVar.jV());
            }
        }, csVar);
    }

    /* access modifiers changed from: 0000 */
    public void a(aj ajVar) {
        a(this.WM, ajVar);
    }

    /* access modifiers changed from: 0000 */
    public void b(aj ajVar) {
        a(this.WK, ajVar);
    }

    public by<com.google.android.gms.internal.d.a> bC(String str) {
        this.WS = 0;
        af bl = this.WJ.bl(str);
        by<com.google.android.gms.internal.d.a> a2 = a(str, (Set<String>) new HashSet<String>(), bl.iY());
        bl.ja();
        return a2;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void bD(String str) {
        this.WR = str;
    }

    public synchronized void ba(String str) {
        bD(str);
        af bm = this.WJ.bm(str);
        t iZ = bm.iZ();
        for (com.google.android.gms.tagmanager.cr.a a2 : (Set) a(this.WP, iZ.iS()).getObject()) {
            a(this.WK, a2, (Set<String>) new HashSet<String>(), iZ.iR());
        }
        bm.ja();
        bD(null);
    }

    /* access modifiers changed from: 0000 */
    public void c(aj ajVar) {
        a(this.WL, ajVar);
    }

    public synchronized void f(List<i> list) {
        for (i iVar : list) {
            if (iVar.name == null || !iVar.name.startsWith("gaExperiment:")) {
                bh.v("Ignored supplemental: " + iVar);
            } else {
                ai.a(this.TN, iVar);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized String jY() {
        return this.WR;
    }
}
