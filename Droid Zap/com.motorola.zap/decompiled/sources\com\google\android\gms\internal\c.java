package com.google.android.gms.internal;

import android.support.v4.media.TransportMediator;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.motorola.zap.ZapConstants;
import java.io.IOException;

public interface c {

    public static final class a extends ka<a> {
        public int eP;
        public int eQ;
        public int level;

        public a() {
            b();
        }

        /* renamed from: a */
        public a b(jy jyVar) throws IOException {
            while (true) {
                int ky = jyVar.ky();
                switch (ky) {
                    case 0:
                        break;
                    case 8:
                        int kB = jyVar.kB();
                        switch (kB) {
                            case 1:
                            case 2:
                            case 3:
                                this.level = kB;
                                break;
                            default:
                                continue;
                        }
                    case 16:
                        this.eP = jyVar.kB();
                        continue;
                    case 24:
                        this.eQ = jyVar.kB();
                        continue;
                    default:
                        if (!a(jyVar, ky)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        public void a(jz jzVar) throws IOException {
            if (this.level != 1) {
                jzVar.f(1, this.level);
            }
            if (this.eP != 0) {
                jzVar.f(2, this.eP);
            }
            if (this.eQ != 0) {
                jzVar.f(3, this.eQ);
            }
            super.a(jzVar);
        }

        public a b() {
            this.level = 1;
            this.eP = 0;
            this.eQ = 0;
            this.aae = null;
            this.DY = -1;
            return this;
        }

        public int c() {
            int c = super.c();
            if (this.level != 1) {
                c += jz.g(1, this.level);
            }
            if (this.eP != 0) {
                c += jz.g(2, this.eP);
            }
            if (this.eQ != 0) {
                c += jz.g(3, this.eQ);
            }
            this.DY = c;
            return c;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof a)) {
                return false;
            }
            a aVar = (a) o;
            if (this.level != aVar.level || this.eP != aVar.eP || this.eQ != aVar.eQ) {
                return false;
            }
            if (this.aae == null || this.aae.isEmpty()) {
                return aVar.aae == null || aVar.aae.isEmpty();
            }
            return this.aae.equals(aVar.aae);
        }

        public int hashCode() {
            return ((this.aae == null || this.aae.isEmpty()) ? 0 : this.aae.hashCode()) + ((((((this.level + 527) * 31) + this.eP) * 31) + this.eQ) * 31);
        }
    }

    public static final class b extends ka<b> {
        private static volatile b[] eR;
        public int[] eS;
        public int eT;
        public boolean eU;
        public boolean eV;
        public int name;

        public b() {
            e();
        }

        public static b[] d() {
            if (eR == null) {
                synchronized (kc.aah) {
                    if (eR == null) {
                        eR = new b[0];
                    }
                }
            }
            return eR;
        }

        public void a(jz jzVar) throws IOException {
            if (this.eV) {
                jzVar.a(1, this.eV);
            }
            jzVar.f(2, this.eT);
            if (this.eS != null && this.eS.length > 0) {
                for (int f : this.eS) {
                    jzVar.f(3, f);
                }
            }
            if (this.name != 0) {
                jzVar.f(4, this.name);
            }
            if (this.eU) {
                jzVar.a(6, this.eU);
            }
            super.a(jzVar);
        }

        public int c() {
            int i;
            int i2 = 0;
            int c = super.c();
            if (this.eV) {
                c += jz.b(1, this.eV);
            }
            int g = jz.g(2, this.eT) + c;
            if (this.eS == null || this.eS.length <= 0) {
                i = g;
            } else {
                for (int cC : this.eS) {
                    i2 += jz.cC(cC);
                }
                i = g + i2 + (this.eS.length * 1);
            }
            if (this.name != 0) {
                i += jz.g(4, this.name);
            }
            if (this.eU) {
                i += jz.b(6, this.eU);
            }
            this.DY = i;
            return i;
        }

        /* renamed from: c */
        public b b(jy jyVar) throws IOException {
            while (true) {
                int ky = jyVar.ky();
                switch (ky) {
                    case 0:
                        break;
                    case 8:
                        this.eV = jyVar.kC();
                        continue;
                    case 16:
                        this.eT = jyVar.kB();
                        continue;
                    case 24:
                        int c = kh.c(jyVar, 24);
                        int length = this.eS == null ? 0 : this.eS.length;
                        int[] iArr = new int[(c + length)];
                        if (length != 0) {
                            System.arraycopy(this.eS, 0, iArr, 0, length);
                        }
                        while (length < iArr.length - 1) {
                            iArr[length] = jyVar.kB();
                            jyVar.ky();
                            length++;
                        }
                        iArr[length] = jyVar.kB();
                        this.eS = iArr;
                        continue;
                    case 26:
                        int cw = jyVar.cw(jyVar.kE());
                        int position = jyVar.getPosition();
                        int i = 0;
                        while (jyVar.kJ() > 0) {
                            jyVar.kB();
                            i++;
                        }
                        jyVar.cy(position);
                        int length2 = this.eS == null ? 0 : this.eS.length;
                        int[] iArr2 = new int[(i + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.eS, 0, iArr2, 0, length2);
                        }
                        while (length2 < iArr2.length) {
                            iArr2[length2] = jyVar.kB();
                            length2++;
                        }
                        this.eS = iArr2;
                        jyVar.cx(cw);
                        continue;
                    case 32:
                        this.name = jyVar.kB();
                        continue;
                    case 48:
                        this.eU = jyVar.kC();
                        continue;
                    default:
                        if (!a(jyVar, ky)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        public b e() {
            this.eS = kh.aaj;
            this.eT = 0;
            this.name = 0;
            this.eU = false;
            this.eV = false;
            this.aae = null;
            this.DY = -1;
            return this;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof b)) {
                return false;
            }
            b bVar = (b) o;
            if (!kc.equals(this.eS, bVar.eS) || this.eT != bVar.eT || this.name != bVar.name || this.eU != bVar.eU || this.eV != bVar.eV) {
                return false;
            }
            if (this.aae == null || this.aae.isEmpty()) {
                return bVar.aae == null || bVar.aae.isEmpty();
            }
            return this.aae.equals(bVar.aae);
        }

        public int hashCode() {
            int i = 1231;
            int hashCode = ((this.eU ? 1231 : 1237) + ((((((kc.hashCode(this.eS) + 527) * 31) + this.eT) * 31) + this.name) * 31)) * 31;
            if (!this.eV) {
                i = 1237;
            }
            return ((this.aae == null || this.aae.isEmpty()) ? 0 : this.aae.hashCode()) + ((hashCode + i) * 31);
        }
    }

    /* renamed from: com.google.android.gms.internal.c$c reason: collision with other inner class name */
    public static final class C0022c extends ka<C0022c> {
        private static volatile C0022c[] eW;
        public String eX;
        public long eY;
        public long eZ;
        public boolean fa;
        public long fb;

        public C0022c() {
            g();
        }

        public static C0022c[] f() {
            if (eW == null) {
                synchronized (kc.aah) {
                    if (eW == null) {
                        eW = new C0022c[0];
                    }
                }
            }
            return eW;
        }

        public void a(jz jzVar) throws IOException {
            if (!this.eX.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                jzVar.b(1, this.eX);
            }
            if (this.eY != 0) {
                jzVar.b(2, this.eY);
            }
            if (this.eZ != 2147483647L) {
                jzVar.b(3, this.eZ);
            }
            if (this.fa) {
                jzVar.a(4, this.fa);
            }
            if (this.fb != 0) {
                jzVar.b(5, this.fb);
            }
            super.a(jzVar);
        }

        public int c() {
            int c = super.c();
            if (!this.eX.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                c += jz.g(1, this.eX);
            }
            if (this.eY != 0) {
                c += jz.d(2, this.eY);
            }
            if (this.eZ != 2147483647L) {
                c += jz.d(3, this.eZ);
            }
            if (this.fa) {
                c += jz.b(4, this.fa);
            }
            if (this.fb != 0) {
                c += jz.d(5, this.fb);
            }
            this.DY = c;
            return c;
        }

        /* renamed from: d */
        public C0022c b(jy jyVar) throws IOException {
            while (true) {
                int ky = jyVar.ky();
                switch (ky) {
                    case 0:
                        break;
                    case 10:
                        this.eX = jyVar.readString();
                        continue;
                    case 16:
                        this.eY = jyVar.kA();
                        continue;
                    case 24:
                        this.eZ = jyVar.kA();
                        continue;
                    case 32:
                        this.fa = jyVar.kC();
                        continue;
                    case 40:
                        this.fb = jyVar.kA();
                        continue;
                    default:
                        if (!a(jyVar, ky)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof C0022c)) {
                return false;
            }
            C0022c cVar = (C0022c) o;
            if (this.eX == null) {
                if (cVar.eX != null) {
                    return false;
                }
            } else if (!this.eX.equals(cVar.eX)) {
                return false;
            }
            if (this.eY != cVar.eY || this.eZ != cVar.eZ || this.fa != cVar.fa || this.fb != cVar.fb) {
                return false;
            }
            if (this.aae == null || this.aae.isEmpty()) {
                return cVar.aae == null || cVar.aae.isEmpty();
            }
            return this.aae.equals(cVar.aae);
        }

        public C0022c g() {
            this.eX = ZapConstants.GOOGLE_ACCOUNT_NONE;
            this.eY = 0;
            this.eZ = 2147483647L;
            this.fa = false;
            this.fb = 0;
            this.aae = null;
            this.DY = -1;
            return this;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((((this.fa ? 1231 : 1237) + (((((((this.eX == null ? 0 : this.eX.hashCode()) + 527) * 31) + ((int) (this.eY ^ (this.eY >>> 32)))) * 31) + ((int) (this.eZ ^ (this.eZ >>> 32)))) * 31)) * 31) + ((int) (this.fb ^ (this.fb >>> 32)))) * 31;
            if (this.aae != null && !this.aae.isEmpty()) {
                i = this.aae.hashCode();
            }
            return hashCode + i;
        }
    }

    public static final class d extends ka<d> {
        public com.google.android.gms.internal.d.a[] fc;
        public com.google.android.gms.internal.d.a[] fd;
        public C0022c[] fe;

        public d() {
            h();
        }

        public void a(jz jzVar) throws IOException {
            if (this.fc != null && this.fc.length > 0) {
                for (com.google.android.gms.internal.d.a aVar : this.fc) {
                    if (aVar != null) {
                        jzVar.a(1, (ke) aVar);
                    }
                }
            }
            if (this.fd != null && this.fd.length > 0) {
                for (com.google.android.gms.internal.d.a aVar2 : this.fd) {
                    if (aVar2 != null) {
                        jzVar.a(2, (ke) aVar2);
                    }
                }
            }
            if (this.fe != null && this.fe.length > 0) {
                for (C0022c cVar : this.fe) {
                    if (cVar != null) {
                        jzVar.a(3, (ke) cVar);
                    }
                }
            }
            super.a(jzVar);
        }

        public int c() {
            int c = super.c();
            if (this.fc != null && this.fc.length > 0) {
                int i = c;
                for (com.google.android.gms.internal.d.a aVar : this.fc) {
                    if (aVar != null) {
                        i += jz.b(1, (ke) aVar);
                    }
                }
                c = i;
            }
            if (this.fd != null && this.fd.length > 0) {
                int i2 = c;
                for (com.google.android.gms.internal.d.a aVar2 : this.fd) {
                    if (aVar2 != null) {
                        i2 += jz.b(2, (ke) aVar2);
                    }
                }
                c = i2;
            }
            if (this.fe != null && this.fe.length > 0) {
                for (C0022c cVar : this.fe) {
                    if (cVar != null) {
                        c += jz.b(3, (ke) cVar);
                    }
                }
            }
            this.DY = c;
            return c;
        }

        /* renamed from: e */
        public d b(jy jyVar) throws IOException {
            while (true) {
                int ky = jyVar.ky();
                switch (ky) {
                    case 0:
                        break;
                    case 10:
                        int c = kh.c(jyVar, 10);
                        int length = this.fc == null ? 0 : this.fc.length;
                        com.google.android.gms.internal.d.a[] aVarArr = new com.google.android.gms.internal.d.a[(c + length)];
                        if (length != 0) {
                            System.arraycopy(this.fc, 0, aVarArr, 0, length);
                        }
                        while (length < aVarArr.length - 1) {
                            aVarArr[length] = new com.google.android.gms.internal.d.a();
                            jyVar.a(aVarArr[length]);
                            jyVar.ky();
                            length++;
                        }
                        aVarArr[length] = new com.google.android.gms.internal.d.a();
                        jyVar.a(aVarArr[length]);
                        this.fc = aVarArr;
                        continue;
                    case 18:
                        int c2 = kh.c(jyVar, 18);
                        int length2 = this.fd == null ? 0 : this.fd.length;
                        com.google.android.gms.internal.d.a[] aVarArr2 = new com.google.android.gms.internal.d.a[(c2 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.fd, 0, aVarArr2, 0, length2);
                        }
                        while (length2 < aVarArr2.length - 1) {
                            aVarArr2[length2] = new com.google.android.gms.internal.d.a();
                            jyVar.a(aVarArr2[length2]);
                            jyVar.ky();
                            length2++;
                        }
                        aVarArr2[length2] = new com.google.android.gms.internal.d.a();
                        jyVar.a(aVarArr2[length2]);
                        this.fd = aVarArr2;
                        continue;
                    case 26:
                        int c3 = kh.c(jyVar, 26);
                        int length3 = this.fe == null ? 0 : this.fe.length;
                        C0022c[] cVarArr = new C0022c[(c3 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.fe, 0, cVarArr, 0, length3);
                        }
                        while (length3 < cVarArr.length - 1) {
                            cVarArr[length3] = new C0022c();
                            jyVar.a(cVarArr[length3]);
                            jyVar.ky();
                            length3++;
                        }
                        cVarArr[length3] = new C0022c();
                        jyVar.a(cVarArr[length3]);
                        this.fe = cVarArr;
                        continue;
                    default:
                        if (!a(jyVar, ky)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof d)) {
                return false;
            }
            d dVar = (d) o;
            if (!kc.equals((Object[]) this.fc, (Object[]) dVar.fc) || !kc.equals((Object[]) this.fd, (Object[]) dVar.fd) || !kc.equals((Object[]) this.fe, (Object[]) dVar.fe)) {
                return false;
            }
            if (this.aae == null || this.aae.isEmpty()) {
                return dVar.aae == null || dVar.aae.isEmpty();
            }
            return this.aae.equals(dVar.aae);
        }

        public d h() {
            this.fc = com.google.android.gms.internal.d.a.r();
            this.fd = com.google.android.gms.internal.d.a.r();
            this.fe = C0022c.f();
            this.aae = null;
            this.DY = -1;
            return this;
        }

        public int hashCode() {
            return ((this.aae == null || this.aae.isEmpty()) ? 0 : this.aae.hashCode()) + ((((((kc.hashCode((Object[]) this.fc) + 527) * 31) + kc.hashCode((Object[]) this.fd)) * 31) + kc.hashCode((Object[]) this.fe)) * 31);
        }
    }

    public static final class e extends ka<e> {
        private static volatile e[] ff;
        public int key;
        public int value;

        public e() {
            j();
        }

        public static e[] i() {
            if (ff == null) {
                synchronized (kc.aah) {
                    if (ff == null) {
                        ff = new e[0];
                    }
                }
            }
            return ff;
        }

        public void a(jz jzVar) throws IOException {
            jzVar.f(1, this.key);
            jzVar.f(2, this.value);
            super.a(jzVar);
        }

        public int c() {
            int c = super.c() + jz.g(1, this.key) + jz.g(2, this.value);
            this.DY = c;
            return c;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof e)) {
                return false;
            }
            e eVar = (e) o;
            if (this.key != eVar.key || this.value != eVar.value) {
                return false;
            }
            if (this.aae == null || this.aae.isEmpty()) {
                return eVar.aae == null || eVar.aae.isEmpty();
            }
            return this.aae.equals(eVar.aae);
        }

        /* renamed from: f */
        public e b(jy jyVar) throws IOException {
            while (true) {
                int ky = jyVar.ky();
                switch (ky) {
                    case 0:
                        break;
                    case 8:
                        this.key = jyVar.kB();
                        continue;
                    case 16:
                        this.value = jyVar.kB();
                        continue;
                    default:
                        if (!a(jyVar, ky)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        public int hashCode() {
            return ((this.aae == null || this.aae.isEmpty()) ? 0 : this.aae.hashCode()) + ((((this.key + 527) * 31) + this.value) * 31);
        }

        public e j() {
            this.key = 0;
            this.value = 0;
            this.aae = null;
            this.DY = -1;
            return this;
        }
    }

    public static final class f extends ka<f> {
        public String[] fg;
        public String[] fh;
        public com.google.android.gms.internal.d.a[] fi;
        public e[] fj;
        public b[] fk;
        public b[] fl;
        public b[] fm;
        public g[] fn;
        public String fo;
        public String fp;
        public String fq;
        public String fr;
        public a fs;
        public float ft;
        public boolean fu;
        public String[] fv;
        public int fw;

        public f() {
            k();
        }

        public static f a(byte[] bArr) throws kd {
            return (f) ke.a(new f(), bArr);
        }

        public void a(jz jzVar) throws IOException {
            if (this.fh != null && this.fh.length > 0) {
                for (String str : this.fh) {
                    if (str != null) {
                        jzVar.b(1, str);
                    }
                }
            }
            if (this.fi != null && this.fi.length > 0) {
                for (com.google.android.gms.internal.d.a aVar : this.fi) {
                    if (aVar != null) {
                        jzVar.a(2, (ke) aVar);
                    }
                }
            }
            if (this.fj != null && this.fj.length > 0) {
                for (e eVar : this.fj) {
                    if (eVar != null) {
                        jzVar.a(3, (ke) eVar);
                    }
                }
            }
            if (this.fk != null && this.fk.length > 0) {
                for (b bVar : this.fk) {
                    if (bVar != null) {
                        jzVar.a(4, (ke) bVar);
                    }
                }
            }
            if (this.fl != null && this.fl.length > 0) {
                for (b bVar2 : this.fl) {
                    if (bVar2 != null) {
                        jzVar.a(5, (ke) bVar2);
                    }
                }
            }
            if (this.fm != null && this.fm.length > 0) {
                for (b bVar3 : this.fm) {
                    if (bVar3 != null) {
                        jzVar.a(6, (ke) bVar3);
                    }
                }
            }
            if (this.fn != null && this.fn.length > 0) {
                for (g gVar : this.fn) {
                    if (gVar != null) {
                        jzVar.a(7, (ke) gVar);
                    }
                }
            }
            if (!this.fo.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                jzVar.b(9, this.fo);
            }
            if (!this.fp.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                jzVar.b(10, this.fp);
            }
            if (!this.fq.equals("0")) {
                jzVar.b(12, this.fq);
            }
            if (!this.fr.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                jzVar.b(13, this.fr);
            }
            if (this.fs != null) {
                jzVar.a(14, (ke) this.fs);
            }
            if (Float.floatToIntBits(this.ft) != Float.floatToIntBits(BitmapDescriptorFactory.HUE_RED)) {
                jzVar.a(15, this.ft);
            }
            if (this.fv != null && this.fv.length > 0) {
                for (String str2 : this.fv) {
                    if (str2 != null) {
                        jzVar.b(16, str2);
                    }
                }
            }
            if (this.fw != 0) {
                jzVar.f(17, this.fw);
            }
            if (this.fu) {
                jzVar.a(18, this.fu);
            }
            if (this.fg != null && this.fg.length > 0) {
                for (String str3 : this.fg) {
                    if (str3 != null) {
                        jzVar.b(19, str3);
                    }
                }
            }
            super.a(jzVar);
        }

        public int c() {
            int i;
            int c = super.c();
            if (this.fh == null || this.fh.length <= 0) {
                i = c;
            } else {
                int i2 = 0;
                int i3 = 0;
                for (String str : this.fh) {
                    if (str != null) {
                        i3++;
                        i2 += jz.bQ(str);
                    }
                }
                i = c + i2 + (i3 * 1);
            }
            if (this.fi != null && this.fi.length > 0) {
                int i4 = i;
                for (com.google.android.gms.internal.d.a aVar : this.fi) {
                    if (aVar != null) {
                        i4 += jz.b(2, (ke) aVar);
                    }
                }
                i = i4;
            }
            if (this.fj != null && this.fj.length > 0) {
                int i5 = i;
                for (e eVar : this.fj) {
                    if (eVar != null) {
                        i5 += jz.b(3, (ke) eVar);
                    }
                }
                i = i5;
            }
            if (this.fk != null && this.fk.length > 0) {
                int i6 = i;
                for (b bVar : this.fk) {
                    if (bVar != null) {
                        i6 += jz.b(4, (ke) bVar);
                    }
                }
                i = i6;
            }
            if (this.fl != null && this.fl.length > 0) {
                int i7 = i;
                for (b bVar2 : this.fl) {
                    if (bVar2 != null) {
                        i7 += jz.b(5, (ke) bVar2);
                    }
                }
                i = i7;
            }
            if (this.fm != null && this.fm.length > 0) {
                int i8 = i;
                for (b bVar3 : this.fm) {
                    if (bVar3 != null) {
                        i8 += jz.b(6, (ke) bVar3);
                    }
                }
                i = i8;
            }
            if (this.fn != null && this.fn.length > 0) {
                int i9 = i;
                for (g gVar : this.fn) {
                    if (gVar != null) {
                        i9 += jz.b(7, (ke) gVar);
                    }
                }
                i = i9;
            }
            if (!this.fo.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                i += jz.g(9, this.fo);
            }
            if (!this.fp.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                i += jz.g(10, this.fp);
            }
            if (!this.fq.equals("0")) {
                i += jz.g(12, this.fq);
            }
            if (!this.fr.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                i += jz.g(13, this.fr);
            }
            if (this.fs != null) {
                i += jz.b(14, (ke) this.fs);
            }
            if (Float.floatToIntBits(this.ft) != Float.floatToIntBits(BitmapDescriptorFactory.HUE_RED)) {
                i += jz.b(15, this.ft);
            }
            if (this.fv != null && this.fv.length > 0) {
                int i10 = 0;
                int i11 = 0;
                for (String str2 : this.fv) {
                    if (str2 != null) {
                        i11++;
                        i10 += jz.bQ(str2);
                    }
                }
                i = i + i10 + (i11 * 2);
            }
            if (this.fw != 0) {
                i += jz.g(17, this.fw);
            }
            if (this.fu) {
                i += jz.b(18, this.fu);
            }
            if (this.fg != null && this.fg.length > 0) {
                int i12 = 0;
                int i13 = 0;
                for (String str3 : this.fg) {
                    if (str3 != null) {
                        i13++;
                        i12 += jz.bQ(str3);
                    }
                }
                i = i + i12 + (i13 * 2);
            }
            this.DY = i;
            return i;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof f)) {
                return false;
            }
            f fVar = (f) o;
            if (!kc.equals((Object[]) this.fg, (Object[]) fVar.fg) || !kc.equals((Object[]) this.fh, (Object[]) fVar.fh) || !kc.equals((Object[]) this.fi, (Object[]) fVar.fi) || !kc.equals((Object[]) this.fj, (Object[]) fVar.fj) || !kc.equals((Object[]) this.fk, (Object[]) fVar.fk) || !kc.equals((Object[]) this.fl, (Object[]) fVar.fl) || !kc.equals((Object[]) this.fm, (Object[]) fVar.fm) || !kc.equals((Object[]) this.fn, (Object[]) fVar.fn)) {
                return false;
            }
            if (this.fo == null) {
                if (fVar.fo != null) {
                    return false;
                }
            } else if (!this.fo.equals(fVar.fo)) {
                return false;
            }
            if (this.fp == null) {
                if (fVar.fp != null) {
                    return false;
                }
            } else if (!this.fp.equals(fVar.fp)) {
                return false;
            }
            if (this.fq == null) {
                if (fVar.fq != null) {
                    return false;
                }
            } else if (!this.fq.equals(fVar.fq)) {
                return false;
            }
            if (this.fr == null) {
                if (fVar.fr != null) {
                    return false;
                }
            } else if (!this.fr.equals(fVar.fr)) {
                return false;
            }
            if (this.fs == null) {
                if (fVar.fs != null) {
                    return false;
                }
            } else if (!this.fs.equals(fVar.fs)) {
                return false;
            }
            if (Float.floatToIntBits(this.ft) != Float.floatToIntBits(fVar.ft) || this.fu != fVar.fu || !kc.equals((Object[]) this.fv, (Object[]) fVar.fv) || this.fw != fVar.fw) {
                return false;
            }
            if (this.aae == null || this.aae.isEmpty()) {
                return fVar.aae == null || fVar.aae.isEmpty();
            }
            return this.aae.equals(fVar.aae);
        }

        /* renamed from: g */
        public f b(jy jyVar) throws IOException {
            while (true) {
                int ky = jyVar.ky();
                switch (ky) {
                    case 0:
                        break;
                    case 10:
                        int c = kh.c(jyVar, 10);
                        int length = this.fh == null ? 0 : this.fh.length;
                        String[] strArr = new String[(c + length)];
                        if (length != 0) {
                            System.arraycopy(this.fh, 0, strArr, 0, length);
                        }
                        while (length < strArr.length - 1) {
                            strArr[length] = jyVar.readString();
                            jyVar.ky();
                            length++;
                        }
                        strArr[length] = jyVar.readString();
                        this.fh = strArr;
                        continue;
                    case 18:
                        int c2 = kh.c(jyVar, 18);
                        int length2 = this.fi == null ? 0 : this.fi.length;
                        com.google.android.gms.internal.d.a[] aVarArr = new com.google.android.gms.internal.d.a[(c2 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.fi, 0, aVarArr, 0, length2);
                        }
                        while (length2 < aVarArr.length - 1) {
                            aVarArr[length2] = new com.google.android.gms.internal.d.a();
                            jyVar.a(aVarArr[length2]);
                            jyVar.ky();
                            length2++;
                        }
                        aVarArr[length2] = new com.google.android.gms.internal.d.a();
                        jyVar.a(aVarArr[length2]);
                        this.fi = aVarArr;
                        continue;
                    case 26:
                        int c3 = kh.c(jyVar, 26);
                        int length3 = this.fj == null ? 0 : this.fj.length;
                        e[] eVarArr = new e[(c3 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.fj, 0, eVarArr, 0, length3);
                        }
                        while (length3 < eVarArr.length - 1) {
                            eVarArr[length3] = new e();
                            jyVar.a(eVarArr[length3]);
                            jyVar.ky();
                            length3++;
                        }
                        eVarArr[length3] = new e();
                        jyVar.a(eVarArr[length3]);
                        this.fj = eVarArr;
                        continue;
                    case 34:
                        int c4 = kh.c(jyVar, 34);
                        int length4 = this.fk == null ? 0 : this.fk.length;
                        b[] bVarArr = new b[(c4 + length4)];
                        if (length4 != 0) {
                            System.arraycopy(this.fk, 0, bVarArr, 0, length4);
                        }
                        while (length4 < bVarArr.length - 1) {
                            bVarArr[length4] = new b();
                            jyVar.a(bVarArr[length4]);
                            jyVar.ky();
                            length4++;
                        }
                        bVarArr[length4] = new b();
                        jyVar.a(bVarArr[length4]);
                        this.fk = bVarArr;
                        continue;
                    case 42:
                        int c5 = kh.c(jyVar, 42);
                        int length5 = this.fl == null ? 0 : this.fl.length;
                        b[] bVarArr2 = new b[(c5 + length5)];
                        if (length5 != 0) {
                            System.arraycopy(this.fl, 0, bVarArr2, 0, length5);
                        }
                        while (length5 < bVarArr2.length - 1) {
                            bVarArr2[length5] = new b();
                            jyVar.a(bVarArr2[length5]);
                            jyVar.ky();
                            length5++;
                        }
                        bVarArr2[length5] = new b();
                        jyVar.a(bVarArr2[length5]);
                        this.fl = bVarArr2;
                        continue;
                    case 50:
                        int c6 = kh.c(jyVar, 50);
                        int length6 = this.fm == null ? 0 : this.fm.length;
                        b[] bVarArr3 = new b[(c6 + length6)];
                        if (length6 != 0) {
                            System.arraycopy(this.fm, 0, bVarArr3, 0, length6);
                        }
                        while (length6 < bVarArr3.length - 1) {
                            bVarArr3[length6] = new b();
                            jyVar.a(bVarArr3[length6]);
                            jyVar.ky();
                            length6++;
                        }
                        bVarArr3[length6] = new b();
                        jyVar.a(bVarArr3[length6]);
                        this.fm = bVarArr3;
                        continue;
                    case 58:
                        int c7 = kh.c(jyVar, 58);
                        int length7 = this.fn == null ? 0 : this.fn.length;
                        g[] gVarArr = new g[(c7 + length7)];
                        if (length7 != 0) {
                            System.arraycopy(this.fn, 0, gVarArr, 0, length7);
                        }
                        while (length7 < gVarArr.length - 1) {
                            gVarArr[length7] = new g();
                            jyVar.a(gVarArr[length7]);
                            jyVar.ky();
                            length7++;
                        }
                        gVarArr[length7] = new g();
                        jyVar.a(gVarArr[length7]);
                        this.fn = gVarArr;
                        continue;
                    case 74:
                        this.fo = jyVar.readString();
                        continue;
                    case 82:
                        this.fp = jyVar.readString();
                        continue;
                    case 98:
                        this.fq = jyVar.readString();
                        continue;
                    case 106:
                        this.fr = jyVar.readString();
                        continue;
                    case 114:
                        if (this.fs == null) {
                            this.fs = new a();
                        }
                        jyVar.a(this.fs);
                        continue;
                    case 125:
                        this.ft = jyVar.readFloat();
                        continue;
                    case TransportMediator.KEYCODE_MEDIA_RECORD /*130*/:
                        int c8 = kh.c(jyVar, TransportMediator.KEYCODE_MEDIA_RECORD);
                        int length8 = this.fv == null ? 0 : this.fv.length;
                        String[] strArr2 = new String[(c8 + length8)];
                        if (length8 != 0) {
                            System.arraycopy(this.fv, 0, strArr2, 0, length8);
                        }
                        while (length8 < strArr2.length - 1) {
                            strArr2[length8] = jyVar.readString();
                            jyVar.ky();
                            length8++;
                        }
                        strArr2[length8] = jyVar.readString();
                        this.fv = strArr2;
                        continue;
                    case 136:
                        this.fw = jyVar.kB();
                        continue;
                    case 144:
                        this.fu = jyVar.kC();
                        continue;
                    case 154:
                        int c9 = kh.c(jyVar, 154);
                        int length9 = this.fg == null ? 0 : this.fg.length;
                        String[] strArr3 = new String[(c9 + length9)];
                        if (length9 != 0) {
                            System.arraycopy(this.fg, 0, strArr3, 0, length9);
                        }
                        while (length9 < strArr3.length - 1) {
                            strArr3[length9] = jyVar.readString();
                            jyVar.ky();
                            length9++;
                        }
                        strArr3[length9] = jyVar.readString();
                        this.fg = strArr3;
                        continue;
                    default:
                        if (!a(jyVar, ky)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((((((this.fu ? 1231 : 1237) + (((((this.fs == null ? 0 : this.fs.hashCode()) + (((this.fr == null ? 0 : this.fr.hashCode()) + (((this.fq == null ? 0 : this.fq.hashCode()) + (((this.fp == null ? 0 : this.fp.hashCode()) + (((this.fo == null ? 0 : this.fo.hashCode()) + ((((((((((((((((kc.hashCode((Object[]) this.fg) + 527) * 31) + kc.hashCode((Object[]) this.fh)) * 31) + kc.hashCode((Object[]) this.fi)) * 31) + kc.hashCode((Object[]) this.fj)) * 31) + kc.hashCode((Object[]) this.fk)) * 31) + kc.hashCode((Object[]) this.fl)) * 31) + kc.hashCode((Object[]) this.fm)) * 31) + kc.hashCode((Object[]) this.fn)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31) + Float.floatToIntBits(this.ft)) * 31)) * 31) + kc.hashCode((Object[]) this.fv)) * 31) + this.fw) * 31;
            if (this.aae != null && !this.aae.isEmpty()) {
                i = this.aae.hashCode();
            }
            return hashCode + i;
        }

        public f k() {
            this.fg = kh.aao;
            this.fh = kh.aao;
            this.fi = com.google.android.gms.internal.d.a.r();
            this.fj = e.i();
            this.fk = b.d();
            this.fl = b.d();
            this.fm = b.d();
            this.fn = g.l();
            this.fo = ZapConstants.GOOGLE_ACCOUNT_NONE;
            this.fp = ZapConstants.GOOGLE_ACCOUNT_NONE;
            this.fq = "0";
            this.fr = ZapConstants.GOOGLE_ACCOUNT_NONE;
            this.fs = null;
            this.ft = BitmapDescriptorFactory.HUE_RED;
            this.fu = false;
            this.fv = kh.aao;
            this.fw = 0;
            this.aae = null;
            this.DY = -1;
            return this;
        }
    }

    public static final class g extends ka<g> {
        private static volatile g[] fx;
        public int[] fA;
        public int[] fB;
        public int[] fC;
        public int[] fD;
        public int[] fE;
        public int[] fF;
        public int[] fG;
        public int[] fH;
        public int[] fy;
        public int[] fz;

        public g() {
            m();
        }

        public static g[] l() {
            if (fx == null) {
                synchronized (kc.aah) {
                    if (fx == null) {
                        fx = new g[0];
                    }
                }
            }
            return fx;
        }

        public void a(jz jzVar) throws IOException {
            if (this.fy != null && this.fy.length > 0) {
                for (int f : this.fy) {
                    jzVar.f(1, f);
                }
            }
            if (this.fz != null && this.fz.length > 0) {
                for (int f2 : this.fz) {
                    jzVar.f(2, f2);
                }
            }
            if (this.fA != null && this.fA.length > 0) {
                for (int f3 : this.fA) {
                    jzVar.f(3, f3);
                }
            }
            if (this.fB != null && this.fB.length > 0) {
                for (int f4 : this.fB) {
                    jzVar.f(4, f4);
                }
            }
            if (this.fC != null && this.fC.length > 0) {
                for (int f5 : this.fC) {
                    jzVar.f(5, f5);
                }
            }
            if (this.fD != null && this.fD.length > 0) {
                for (int f6 : this.fD) {
                    jzVar.f(6, f6);
                }
            }
            if (this.fE != null && this.fE.length > 0) {
                for (int f7 : this.fE) {
                    jzVar.f(7, f7);
                }
            }
            if (this.fF != null && this.fF.length > 0) {
                for (int f8 : this.fF) {
                    jzVar.f(8, f8);
                }
            }
            if (this.fG != null && this.fG.length > 0) {
                for (int f9 : this.fG) {
                    jzVar.f(9, f9);
                }
            }
            if (this.fH != null && this.fH.length > 0) {
                for (int f10 : this.fH) {
                    jzVar.f(10, f10);
                }
            }
            super.a(jzVar);
        }

        public int c() {
            int i;
            int c = super.c();
            if (this.fy == null || this.fy.length <= 0) {
                i = c;
            } else {
                int i2 = 0;
                for (int cC : this.fy) {
                    i2 += jz.cC(cC);
                }
                i = c + i2 + (this.fy.length * 1);
            }
            if (this.fz != null && this.fz.length > 0) {
                int i3 = 0;
                for (int cC2 : this.fz) {
                    i3 += jz.cC(cC2);
                }
                i = i + i3 + (this.fz.length * 1);
            }
            if (this.fA != null && this.fA.length > 0) {
                int i4 = 0;
                for (int cC3 : this.fA) {
                    i4 += jz.cC(cC3);
                }
                i = i + i4 + (this.fA.length * 1);
            }
            if (this.fB != null && this.fB.length > 0) {
                int i5 = 0;
                for (int cC4 : this.fB) {
                    i5 += jz.cC(cC4);
                }
                i = i + i5 + (this.fB.length * 1);
            }
            if (this.fC != null && this.fC.length > 0) {
                int i6 = 0;
                for (int cC5 : this.fC) {
                    i6 += jz.cC(cC5);
                }
                i = i + i6 + (this.fC.length * 1);
            }
            if (this.fD != null && this.fD.length > 0) {
                int i7 = 0;
                for (int cC6 : this.fD) {
                    i7 += jz.cC(cC6);
                }
                i = i + i7 + (this.fD.length * 1);
            }
            if (this.fE != null && this.fE.length > 0) {
                int i8 = 0;
                for (int cC7 : this.fE) {
                    i8 += jz.cC(cC7);
                }
                i = i + i8 + (this.fE.length * 1);
            }
            if (this.fF != null && this.fF.length > 0) {
                int i9 = 0;
                for (int cC8 : this.fF) {
                    i9 += jz.cC(cC8);
                }
                i = i + i9 + (this.fF.length * 1);
            }
            if (this.fG != null && this.fG.length > 0) {
                int i10 = 0;
                for (int cC9 : this.fG) {
                    i10 += jz.cC(cC9);
                }
                i = i + i10 + (this.fG.length * 1);
            }
            if (this.fH != null && this.fH.length > 0) {
                int i11 = 0;
                for (int cC10 : this.fH) {
                    i11 += jz.cC(cC10);
                }
                i = i + i11 + (this.fH.length * 1);
            }
            this.DY = i;
            return i;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof g)) {
                return false;
            }
            g gVar = (g) o;
            if (!kc.equals(this.fy, gVar.fy) || !kc.equals(this.fz, gVar.fz) || !kc.equals(this.fA, gVar.fA) || !kc.equals(this.fB, gVar.fB) || !kc.equals(this.fC, gVar.fC) || !kc.equals(this.fD, gVar.fD) || !kc.equals(this.fE, gVar.fE) || !kc.equals(this.fF, gVar.fF) || !kc.equals(this.fG, gVar.fG) || !kc.equals(this.fH, gVar.fH)) {
                return false;
            }
            if (this.aae == null || this.aae.isEmpty()) {
                return gVar.aae == null || gVar.aae.isEmpty();
            }
            return this.aae.equals(gVar.aae);
        }

        /* renamed from: h */
        public g b(jy jyVar) throws IOException {
            while (true) {
                int ky = jyVar.ky();
                switch (ky) {
                    case 0:
                        break;
                    case 8:
                        int c = kh.c(jyVar, 8);
                        int length = this.fy == null ? 0 : this.fy.length;
                        int[] iArr = new int[(c + length)];
                        if (length != 0) {
                            System.arraycopy(this.fy, 0, iArr, 0, length);
                        }
                        while (length < iArr.length - 1) {
                            iArr[length] = jyVar.kB();
                            jyVar.ky();
                            length++;
                        }
                        iArr[length] = jyVar.kB();
                        this.fy = iArr;
                        continue;
                    case 10:
                        int cw = jyVar.cw(jyVar.kE());
                        int position = jyVar.getPosition();
                        int i = 0;
                        while (jyVar.kJ() > 0) {
                            jyVar.kB();
                            i++;
                        }
                        jyVar.cy(position);
                        int length2 = this.fy == null ? 0 : this.fy.length;
                        int[] iArr2 = new int[(i + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.fy, 0, iArr2, 0, length2);
                        }
                        while (length2 < iArr2.length) {
                            iArr2[length2] = jyVar.kB();
                            length2++;
                        }
                        this.fy = iArr2;
                        jyVar.cx(cw);
                        continue;
                    case 16:
                        int c2 = kh.c(jyVar, 16);
                        int length3 = this.fz == null ? 0 : this.fz.length;
                        int[] iArr3 = new int[(c2 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.fz, 0, iArr3, 0, length3);
                        }
                        while (length3 < iArr3.length - 1) {
                            iArr3[length3] = jyVar.kB();
                            jyVar.ky();
                            length3++;
                        }
                        iArr3[length3] = jyVar.kB();
                        this.fz = iArr3;
                        continue;
                    case 18:
                        int cw2 = jyVar.cw(jyVar.kE());
                        int position2 = jyVar.getPosition();
                        int i2 = 0;
                        while (jyVar.kJ() > 0) {
                            jyVar.kB();
                            i2++;
                        }
                        jyVar.cy(position2);
                        int length4 = this.fz == null ? 0 : this.fz.length;
                        int[] iArr4 = new int[(i2 + length4)];
                        if (length4 != 0) {
                            System.arraycopy(this.fz, 0, iArr4, 0, length4);
                        }
                        while (length4 < iArr4.length) {
                            iArr4[length4] = jyVar.kB();
                            length4++;
                        }
                        this.fz = iArr4;
                        jyVar.cx(cw2);
                        continue;
                    case 24:
                        int c3 = kh.c(jyVar, 24);
                        int length5 = this.fA == null ? 0 : this.fA.length;
                        int[] iArr5 = new int[(c3 + length5)];
                        if (length5 != 0) {
                            System.arraycopy(this.fA, 0, iArr5, 0, length5);
                        }
                        while (length5 < iArr5.length - 1) {
                            iArr5[length5] = jyVar.kB();
                            jyVar.ky();
                            length5++;
                        }
                        iArr5[length5] = jyVar.kB();
                        this.fA = iArr5;
                        continue;
                    case 26:
                        int cw3 = jyVar.cw(jyVar.kE());
                        int position3 = jyVar.getPosition();
                        int i3 = 0;
                        while (jyVar.kJ() > 0) {
                            jyVar.kB();
                            i3++;
                        }
                        jyVar.cy(position3);
                        int length6 = this.fA == null ? 0 : this.fA.length;
                        int[] iArr6 = new int[(i3 + length6)];
                        if (length6 != 0) {
                            System.arraycopy(this.fA, 0, iArr6, 0, length6);
                        }
                        while (length6 < iArr6.length) {
                            iArr6[length6] = jyVar.kB();
                            length6++;
                        }
                        this.fA = iArr6;
                        jyVar.cx(cw3);
                        continue;
                    case 32:
                        int c4 = kh.c(jyVar, 32);
                        int length7 = this.fB == null ? 0 : this.fB.length;
                        int[] iArr7 = new int[(c4 + length7)];
                        if (length7 != 0) {
                            System.arraycopy(this.fB, 0, iArr7, 0, length7);
                        }
                        while (length7 < iArr7.length - 1) {
                            iArr7[length7] = jyVar.kB();
                            jyVar.ky();
                            length7++;
                        }
                        iArr7[length7] = jyVar.kB();
                        this.fB = iArr7;
                        continue;
                    case 34:
                        int cw4 = jyVar.cw(jyVar.kE());
                        int position4 = jyVar.getPosition();
                        int i4 = 0;
                        while (jyVar.kJ() > 0) {
                            jyVar.kB();
                            i4++;
                        }
                        jyVar.cy(position4);
                        int length8 = this.fB == null ? 0 : this.fB.length;
                        int[] iArr8 = new int[(i4 + length8)];
                        if (length8 != 0) {
                            System.arraycopy(this.fB, 0, iArr8, 0, length8);
                        }
                        while (length8 < iArr8.length) {
                            iArr8[length8] = jyVar.kB();
                            length8++;
                        }
                        this.fB = iArr8;
                        jyVar.cx(cw4);
                        continue;
                    case 40:
                        int c5 = kh.c(jyVar, 40);
                        int length9 = this.fC == null ? 0 : this.fC.length;
                        int[] iArr9 = new int[(c5 + length9)];
                        if (length9 != 0) {
                            System.arraycopy(this.fC, 0, iArr9, 0, length9);
                        }
                        while (length9 < iArr9.length - 1) {
                            iArr9[length9] = jyVar.kB();
                            jyVar.ky();
                            length9++;
                        }
                        iArr9[length9] = jyVar.kB();
                        this.fC = iArr9;
                        continue;
                    case 42:
                        int cw5 = jyVar.cw(jyVar.kE());
                        int position5 = jyVar.getPosition();
                        int i5 = 0;
                        while (jyVar.kJ() > 0) {
                            jyVar.kB();
                            i5++;
                        }
                        jyVar.cy(position5);
                        int length10 = this.fC == null ? 0 : this.fC.length;
                        int[] iArr10 = new int[(i5 + length10)];
                        if (length10 != 0) {
                            System.arraycopy(this.fC, 0, iArr10, 0, length10);
                        }
                        while (length10 < iArr10.length) {
                            iArr10[length10] = jyVar.kB();
                            length10++;
                        }
                        this.fC = iArr10;
                        jyVar.cx(cw5);
                        continue;
                    case 48:
                        int c6 = kh.c(jyVar, 48);
                        int length11 = this.fD == null ? 0 : this.fD.length;
                        int[] iArr11 = new int[(c6 + length11)];
                        if (length11 != 0) {
                            System.arraycopy(this.fD, 0, iArr11, 0, length11);
                        }
                        while (length11 < iArr11.length - 1) {
                            iArr11[length11] = jyVar.kB();
                            jyVar.ky();
                            length11++;
                        }
                        iArr11[length11] = jyVar.kB();
                        this.fD = iArr11;
                        continue;
                    case 50:
                        int cw6 = jyVar.cw(jyVar.kE());
                        int position6 = jyVar.getPosition();
                        int i6 = 0;
                        while (jyVar.kJ() > 0) {
                            jyVar.kB();
                            i6++;
                        }
                        jyVar.cy(position6);
                        int length12 = this.fD == null ? 0 : this.fD.length;
                        int[] iArr12 = new int[(i6 + length12)];
                        if (length12 != 0) {
                            System.arraycopy(this.fD, 0, iArr12, 0, length12);
                        }
                        while (length12 < iArr12.length) {
                            iArr12[length12] = jyVar.kB();
                            length12++;
                        }
                        this.fD = iArr12;
                        jyVar.cx(cw6);
                        continue;
                    case 56:
                        int c7 = kh.c(jyVar, 56);
                        int length13 = this.fE == null ? 0 : this.fE.length;
                        int[] iArr13 = new int[(c7 + length13)];
                        if (length13 != 0) {
                            System.arraycopy(this.fE, 0, iArr13, 0, length13);
                        }
                        while (length13 < iArr13.length - 1) {
                            iArr13[length13] = jyVar.kB();
                            jyVar.ky();
                            length13++;
                        }
                        iArr13[length13] = jyVar.kB();
                        this.fE = iArr13;
                        continue;
                    case 58:
                        int cw7 = jyVar.cw(jyVar.kE());
                        int position7 = jyVar.getPosition();
                        int i7 = 0;
                        while (jyVar.kJ() > 0) {
                            jyVar.kB();
                            i7++;
                        }
                        jyVar.cy(position7);
                        int length14 = this.fE == null ? 0 : this.fE.length;
                        int[] iArr14 = new int[(i7 + length14)];
                        if (length14 != 0) {
                            System.arraycopy(this.fE, 0, iArr14, 0, length14);
                        }
                        while (length14 < iArr14.length) {
                            iArr14[length14] = jyVar.kB();
                            length14++;
                        }
                        this.fE = iArr14;
                        jyVar.cx(cw7);
                        continue;
                    case 64:
                        int c8 = kh.c(jyVar, 64);
                        int length15 = this.fF == null ? 0 : this.fF.length;
                        int[] iArr15 = new int[(c8 + length15)];
                        if (length15 != 0) {
                            System.arraycopy(this.fF, 0, iArr15, 0, length15);
                        }
                        while (length15 < iArr15.length - 1) {
                            iArr15[length15] = jyVar.kB();
                            jyVar.ky();
                            length15++;
                        }
                        iArr15[length15] = jyVar.kB();
                        this.fF = iArr15;
                        continue;
                    case 66:
                        int cw8 = jyVar.cw(jyVar.kE());
                        int position8 = jyVar.getPosition();
                        int i8 = 0;
                        while (jyVar.kJ() > 0) {
                            jyVar.kB();
                            i8++;
                        }
                        jyVar.cy(position8);
                        int length16 = this.fF == null ? 0 : this.fF.length;
                        int[] iArr16 = new int[(i8 + length16)];
                        if (length16 != 0) {
                            System.arraycopy(this.fF, 0, iArr16, 0, length16);
                        }
                        while (length16 < iArr16.length) {
                            iArr16[length16] = jyVar.kB();
                            length16++;
                        }
                        this.fF = iArr16;
                        jyVar.cx(cw8);
                        continue;
                    case 72:
                        int c9 = kh.c(jyVar, 72);
                        int length17 = this.fG == null ? 0 : this.fG.length;
                        int[] iArr17 = new int[(c9 + length17)];
                        if (length17 != 0) {
                            System.arraycopy(this.fG, 0, iArr17, 0, length17);
                        }
                        while (length17 < iArr17.length - 1) {
                            iArr17[length17] = jyVar.kB();
                            jyVar.ky();
                            length17++;
                        }
                        iArr17[length17] = jyVar.kB();
                        this.fG = iArr17;
                        continue;
                    case 74:
                        int cw9 = jyVar.cw(jyVar.kE());
                        int position9 = jyVar.getPosition();
                        int i9 = 0;
                        while (jyVar.kJ() > 0) {
                            jyVar.kB();
                            i9++;
                        }
                        jyVar.cy(position9);
                        int length18 = this.fG == null ? 0 : this.fG.length;
                        int[] iArr18 = new int[(i9 + length18)];
                        if (length18 != 0) {
                            System.arraycopy(this.fG, 0, iArr18, 0, length18);
                        }
                        while (length18 < iArr18.length) {
                            iArr18[length18] = jyVar.kB();
                            length18++;
                        }
                        this.fG = iArr18;
                        jyVar.cx(cw9);
                        continue;
                    case 80:
                        int c10 = kh.c(jyVar, 80);
                        int length19 = this.fH == null ? 0 : this.fH.length;
                        int[] iArr19 = new int[(c10 + length19)];
                        if (length19 != 0) {
                            System.arraycopy(this.fH, 0, iArr19, 0, length19);
                        }
                        while (length19 < iArr19.length - 1) {
                            iArr19[length19] = jyVar.kB();
                            jyVar.ky();
                            length19++;
                        }
                        iArr19[length19] = jyVar.kB();
                        this.fH = iArr19;
                        continue;
                    case 82:
                        int cw10 = jyVar.cw(jyVar.kE());
                        int position10 = jyVar.getPosition();
                        int i10 = 0;
                        while (jyVar.kJ() > 0) {
                            jyVar.kB();
                            i10++;
                        }
                        jyVar.cy(position10);
                        int length20 = this.fH == null ? 0 : this.fH.length;
                        int[] iArr20 = new int[(i10 + length20)];
                        if (length20 != 0) {
                            System.arraycopy(this.fH, 0, iArr20, 0, length20);
                        }
                        while (length20 < iArr20.length) {
                            iArr20[length20] = jyVar.kB();
                            length20++;
                        }
                        this.fH = iArr20;
                        jyVar.cx(cw10);
                        continue;
                    default:
                        if (!a(jyVar, ky)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        public int hashCode() {
            return ((this.aae == null || this.aae.isEmpty()) ? 0 : this.aae.hashCode()) + ((((((((((((((((((((kc.hashCode(this.fy) + 527) * 31) + kc.hashCode(this.fz)) * 31) + kc.hashCode(this.fA)) * 31) + kc.hashCode(this.fB)) * 31) + kc.hashCode(this.fC)) * 31) + kc.hashCode(this.fD)) * 31) + kc.hashCode(this.fE)) * 31) + kc.hashCode(this.fF)) * 31) + kc.hashCode(this.fG)) * 31) + kc.hashCode(this.fH)) * 31);
        }

        public g m() {
            this.fy = kh.aaj;
            this.fz = kh.aaj;
            this.fA = kh.aaj;
            this.fB = kh.aaj;
            this.fC = kh.aaj;
            this.fD = kh.aaj;
            this.fE = kh.aaj;
            this.fF = kh.aaj;
            this.fG = kh.aaj;
            this.fH = kh.aaj;
            this.aae = null;
            this.DY = -1;
            return this;
        }
    }

    public static final class h extends ka<h> {
        public static final kb<com.google.android.gms.internal.d.a, h> fI = kb.a(11, h.class, 810);
        private static final h[] fJ = new h[0];
        public int[] fK;
        public int[] fL;
        public int[] fM;
        public int fN;
        public int[] fO;
        public int fP;
        public int fQ;

        public h() {
            n();
        }

        public void a(jz jzVar) throws IOException {
            if (this.fK != null && this.fK.length > 0) {
                for (int f : this.fK) {
                    jzVar.f(1, f);
                }
            }
            if (this.fL != null && this.fL.length > 0) {
                for (int f2 : this.fL) {
                    jzVar.f(2, f2);
                }
            }
            if (this.fM != null && this.fM.length > 0) {
                for (int f3 : this.fM) {
                    jzVar.f(3, f3);
                }
            }
            if (this.fN != 0) {
                jzVar.f(4, this.fN);
            }
            if (this.fO != null && this.fO.length > 0) {
                for (int f4 : this.fO) {
                    jzVar.f(5, f4);
                }
            }
            if (this.fP != 0) {
                jzVar.f(6, this.fP);
            }
            if (this.fQ != 0) {
                jzVar.f(7, this.fQ);
            }
            super.a(jzVar);
        }

        public int c() {
            int i;
            int c = super.c();
            if (this.fK == null || this.fK.length <= 0) {
                i = c;
            } else {
                int i2 = 0;
                for (int cC : this.fK) {
                    i2 += jz.cC(cC);
                }
                i = c + i2 + (this.fK.length * 1);
            }
            if (this.fL != null && this.fL.length > 0) {
                int i3 = 0;
                for (int cC2 : this.fL) {
                    i3 += jz.cC(cC2);
                }
                i = i + i3 + (this.fL.length * 1);
            }
            if (this.fM != null && this.fM.length > 0) {
                int i4 = 0;
                for (int cC3 : this.fM) {
                    i4 += jz.cC(cC3);
                }
                i = i + i4 + (this.fM.length * 1);
            }
            if (this.fN != 0) {
                i += jz.g(4, this.fN);
            }
            if (this.fO != null && this.fO.length > 0) {
                int i5 = 0;
                for (int cC4 : this.fO) {
                    i5 += jz.cC(cC4);
                }
                i = i + i5 + (this.fO.length * 1);
            }
            if (this.fP != 0) {
                i += jz.g(6, this.fP);
            }
            if (this.fQ != 0) {
                i += jz.g(7, this.fQ);
            }
            this.DY = i;
            return i;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof h)) {
                return false;
            }
            h hVar = (h) o;
            if (!kc.equals(this.fK, hVar.fK) || !kc.equals(this.fL, hVar.fL) || !kc.equals(this.fM, hVar.fM) || this.fN != hVar.fN || !kc.equals(this.fO, hVar.fO) || this.fP != hVar.fP || this.fQ != hVar.fQ) {
                return false;
            }
            if (this.aae == null || this.aae.isEmpty()) {
                return hVar.aae == null || hVar.aae.isEmpty();
            }
            return this.aae.equals(hVar.aae);
        }

        public int hashCode() {
            return ((this.aae == null || this.aae.isEmpty()) ? 0 : this.aae.hashCode()) + ((((((((((((((kc.hashCode(this.fK) + 527) * 31) + kc.hashCode(this.fL)) * 31) + kc.hashCode(this.fM)) * 31) + this.fN) * 31) + kc.hashCode(this.fO)) * 31) + this.fP) * 31) + this.fQ) * 31);
        }

        /* renamed from: i */
        public h b(jy jyVar) throws IOException {
            while (true) {
                int ky = jyVar.ky();
                switch (ky) {
                    case 0:
                        break;
                    case 8:
                        int c = kh.c(jyVar, 8);
                        int length = this.fK == null ? 0 : this.fK.length;
                        int[] iArr = new int[(c + length)];
                        if (length != 0) {
                            System.arraycopy(this.fK, 0, iArr, 0, length);
                        }
                        while (length < iArr.length - 1) {
                            iArr[length] = jyVar.kB();
                            jyVar.ky();
                            length++;
                        }
                        iArr[length] = jyVar.kB();
                        this.fK = iArr;
                        continue;
                    case 10:
                        int cw = jyVar.cw(jyVar.kE());
                        int position = jyVar.getPosition();
                        int i = 0;
                        while (jyVar.kJ() > 0) {
                            jyVar.kB();
                            i++;
                        }
                        jyVar.cy(position);
                        int length2 = this.fK == null ? 0 : this.fK.length;
                        int[] iArr2 = new int[(i + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.fK, 0, iArr2, 0, length2);
                        }
                        while (length2 < iArr2.length) {
                            iArr2[length2] = jyVar.kB();
                            length2++;
                        }
                        this.fK = iArr2;
                        jyVar.cx(cw);
                        continue;
                    case 16:
                        int c2 = kh.c(jyVar, 16);
                        int length3 = this.fL == null ? 0 : this.fL.length;
                        int[] iArr3 = new int[(c2 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.fL, 0, iArr3, 0, length3);
                        }
                        while (length3 < iArr3.length - 1) {
                            iArr3[length3] = jyVar.kB();
                            jyVar.ky();
                            length3++;
                        }
                        iArr3[length3] = jyVar.kB();
                        this.fL = iArr3;
                        continue;
                    case 18:
                        int cw2 = jyVar.cw(jyVar.kE());
                        int position2 = jyVar.getPosition();
                        int i2 = 0;
                        while (jyVar.kJ() > 0) {
                            jyVar.kB();
                            i2++;
                        }
                        jyVar.cy(position2);
                        int length4 = this.fL == null ? 0 : this.fL.length;
                        int[] iArr4 = new int[(i2 + length4)];
                        if (length4 != 0) {
                            System.arraycopy(this.fL, 0, iArr4, 0, length4);
                        }
                        while (length4 < iArr4.length) {
                            iArr4[length4] = jyVar.kB();
                            length4++;
                        }
                        this.fL = iArr4;
                        jyVar.cx(cw2);
                        continue;
                    case 24:
                        int c3 = kh.c(jyVar, 24);
                        int length5 = this.fM == null ? 0 : this.fM.length;
                        int[] iArr5 = new int[(c3 + length5)];
                        if (length5 != 0) {
                            System.arraycopy(this.fM, 0, iArr5, 0, length5);
                        }
                        while (length5 < iArr5.length - 1) {
                            iArr5[length5] = jyVar.kB();
                            jyVar.ky();
                            length5++;
                        }
                        iArr5[length5] = jyVar.kB();
                        this.fM = iArr5;
                        continue;
                    case 26:
                        int cw3 = jyVar.cw(jyVar.kE());
                        int position3 = jyVar.getPosition();
                        int i3 = 0;
                        while (jyVar.kJ() > 0) {
                            jyVar.kB();
                            i3++;
                        }
                        jyVar.cy(position3);
                        int length6 = this.fM == null ? 0 : this.fM.length;
                        int[] iArr6 = new int[(i3 + length6)];
                        if (length6 != 0) {
                            System.arraycopy(this.fM, 0, iArr6, 0, length6);
                        }
                        while (length6 < iArr6.length) {
                            iArr6[length6] = jyVar.kB();
                            length6++;
                        }
                        this.fM = iArr6;
                        jyVar.cx(cw3);
                        continue;
                    case 32:
                        this.fN = jyVar.kB();
                        continue;
                    case 40:
                        int c4 = kh.c(jyVar, 40);
                        int length7 = this.fO == null ? 0 : this.fO.length;
                        int[] iArr7 = new int[(c4 + length7)];
                        if (length7 != 0) {
                            System.arraycopy(this.fO, 0, iArr7, 0, length7);
                        }
                        while (length7 < iArr7.length - 1) {
                            iArr7[length7] = jyVar.kB();
                            jyVar.ky();
                            length7++;
                        }
                        iArr7[length7] = jyVar.kB();
                        this.fO = iArr7;
                        continue;
                    case 42:
                        int cw4 = jyVar.cw(jyVar.kE());
                        int position4 = jyVar.getPosition();
                        int i4 = 0;
                        while (jyVar.kJ() > 0) {
                            jyVar.kB();
                            i4++;
                        }
                        jyVar.cy(position4);
                        int length8 = this.fO == null ? 0 : this.fO.length;
                        int[] iArr8 = new int[(i4 + length8)];
                        if (length8 != 0) {
                            System.arraycopy(this.fO, 0, iArr8, 0, length8);
                        }
                        while (length8 < iArr8.length) {
                            iArr8[length8] = jyVar.kB();
                            length8++;
                        }
                        this.fO = iArr8;
                        jyVar.cx(cw4);
                        continue;
                    case 48:
                        this.fP = jyVar.kB();
                        continue;
                    case 56:
                        this.fQ = jyVar.kB();
                        continue;
                    default:
                        if (!a(jyVar, ky)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        public h n() {
            this.fK = kh.aaj;
            this.fL = kh.aaj;
            this.fM = kh.aaj;
            this.fN = 0;
            this.fO = kh.aaj;
            this.fP = 0;
            this.fQ = 0;
            this.aae = null;
            this.DY = -1;
            return this;
        }
    }

    public static final class i extends ka<i> {
        private static volatile i[] fR;
        public com.google.android.gms.internal.d.a fS;
        public d fT;
        public String name;

        public i() {
            p();
        }

        public static i[] o() {
            if (fR == null) {
                synchronized (kc.aah) {
                    if (fR == null) {
                        fR = new i[0];
                    }
                }
            }
            return fR;
        }

        public void a(jz jzVar) throws IOException {
            if (!this.name.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                jzVar.b(1, this.name);
            }
            if (this.fS != null) {
                jzVar.a(2, (ke) this.fS);
            }
            if (this.fT != null) {
                jzVar.a(3, (ke) this.fT);
            }
            super.a(jzVar);
        }

        public int c() {
            int c = super.c();
            if (!this.name.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                c += jz.g(1, this.name);
            }
            if (this.fS != null) {
                c += jz.b(2, (ke) this.fS);
            }
            if (this.fT != null) {
                c += jz.b(3, (ke) this.fT);
            }
            this.DY = c;
            return c;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof i)) {
                return false;
            }
            i iVar = (i) o;
            if (this.name == null) {
                if (iVar.name != null) {
                    return false;
                }
            } else if (!this.name.equals(iVar.name)) {
                return false;
            }
            if (this.fS == null) {
                if (iVar.fS != null) {
                    return false;
                }
            } else if (!this.fS.equals(iVar.fS)) {
                return false;
            }
            if (this.fT == null) {
                if (iVar.fT != null) {
                    return false;
                }
            } else if (!this.fT.equals(iVar.fT)) {
                return false;
            }
            if (this.aae == null || this.aae.isEmpty()) {
                return iVar.aae == null || iVar.aae.isEmpty();
            }
            return this.aae.equals(iVar.aae);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.fT == null ? 0 : this.fT.hashCode()) + (((this.fS == null ? 0 : this.fS.hashCode()) + (((this.name == null ? 0 : this.name.hashCode()) + 527) * 31)) * 31)) * 31;
            if (this.aae != null && !this.aae.isEmpty()) {
                i = this.aae.hashCode();
            }
            return hashCode + i;
        }

        /* renamed from: j */
        public i b(jy jyVar) throws IOException {
            while (true) {
                int ky = jyVar.ky();
                switch (ky) {
                    case 0:
                        break;
                    case 10:
                        this.name = jyVar.readString();
                        continue;
                    case 18:
                        if (this.fS == null) {
                            this.fS = new com.google.android.gms.internal.d.a();
                        }
                        jyVar.a(this.fS);
                        continue;
                    case 26:
                        if (this.fT == null) {
                            this.fT = new d();
                        }
                        jyVar.a(this.fT);
                        continue;
                    default:
                        if (!a(jyVar, ky)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        public i p() {
            this.name = ZapConstants.GOOGLE_ACCOUNT_NONE;
            this.fS = null;
            this.fT = null;
            this.aae = null;
            this.DY = -1;
            return this;
        }
    }

    public static final class j extends ka<j> {
        public i[] fU;
        public f fV;
        public String fW;

        public j() {
            q();
        }

        public static j b(byte[] bArr) throws kd {
            return (j) ke.a(new j(), bArr);
        }

        public void a(jz jzVar) throws IOException {
            if (this.fU != null && this.fU.length > 0) {
                for (i iVar : this.fU) {
                    if (iVar != null) {
                        jzVar.a(1, (ke) iVar);
                    }
                }
            }
            if (this.fV != null) {
                jzVar.a(2, (ke) this.fV);
            }
            if (!this.fW.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                jzVar.b(3, this.fW);
            }
            super.a(jzVar);
        }

        public int c() {
            int c = super.c();
            if (this.fU != null && this.fU.length > 0) {
                for (i iVar : this.fU) {
                    if (iVar != null) {
                        c += jz.b(1, (ke) iVar);
                    }
                }
            }
            if (this.fV != null) {
                c += jz.b(2, (ke) this.fV);
            }
            if (!this.fW.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                c += jz.g(3, this.fW);
            }
            this.DY = c;
            return c;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof j)) {
                return false;
            }
            j jVar = (j) o;
            if (!kc.equals((Object[]) this.fU, (Object[]) jVar.fU)) {
                return false;
            }
            if (this.fV == null) {
                if (jVar.fV != null) {
                    return false;
                }
            } else if (!this.fV.equals(jVar.fV)) {
                return false;
            }
            if (this.fW == null) {
                if (jVar.fW != null) {
                    return false;
                }
            } else if (!this.fW.equals(jVar.fW)) {
                return false;
            }
            if (this.aae == null || this.aae.isEmpty()) {
                return jVar.aae == null || jVar.aae.isEmpty();
            }
            return this.aae.equals(jVar.aae);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.fW == null ? 0 : this.fW.hashCode()) + (((this.fV == null ? 0 : this.fV.hashCode()) + ((kc.hashCode((Object[]) this.fU) + 527) * 31)) * 31)) * 31;
            if (this.aae != null && !this.aae.isEmpty()) {
                i = this.aae.hashCode();
            }
            return hashCode + i;
        }

        /* renamed from: k */
        public j b(jy jyVar) throws IOException {
            while (true) {
                int ky = jyVar.ky();
                switch (ky) {
                    case 0:
                        break;
                    case 10:
                        int c = kh.c(jyVar, 10);
                        int length = this.fU == null ? 0 : this.fU.length;
                        i[] iVarArr = new i[(c + length)];
                        if (length != 0) {
                            System.arraycopy(this.fU, 0, iVarArr, 0, length);
                        }
                        while (length < iVarArr.length - 1) {
                            iVarArr[length] = new i();
                            jyVar.a(iVarArr[length]);
                            jyVar.ky();
                            length++;
                        }
                        iVarArr[length] = new i();
                        jyVar.a(iVarArr[length]);
                        this.fU = iVarArr;
                        continue;
                    case 18:
                        if (this.fV == null) {
                            this.fV = new f();
                        }
                        jyVar.a(this.fV);
                        continue;
                    case 26:
                        this.fW = jyVar.readString();
                        continue;
                    default:
                        if (!a(jyVar, ky)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        public j q() {
            this.fU = i.o();
            this.fV = null;
            this.fW = ZapConstants.GOOGLE_ACCOUNT_NONE;
            this.aae = null;
            this.DY = -1;
            return this;
        }
    }
}
