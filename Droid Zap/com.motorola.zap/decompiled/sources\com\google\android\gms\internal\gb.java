package com.google.android.gms.internal;

import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.GamesActivityResultCodes;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.multiplayer.ParticipantEntity;
import com.google.android.gms.games.multiplayer.ParticipantResult;
import com.google.android.gms.games.multiplayer.realtime.RoomEntity;

public interface gb extends IInterface {

    public static abstract class a extends Binder implements gb {

        /* renamed from: com.google.android.gms.internal.gb$a$a reason: collision with other inner class name */
        private static class C0035a implements gb {
            private IBinder ky;

            C0035a(IBinder iBinder) {
                this.ky = iBinder;
            }

            public int a(ga gaVar, byte[] bArr, String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeByteArray(bArr);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.ky.transact(5033, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Intent a(int i, int i2, boolean z) throws RemoteException {
                int i3 = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    if (z) {
                        i3 = 1;
                    }
                    obtain.writeInt(i3);
                    this.ky.transact(9008, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Intent) Intent.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Intent a(int i, byte[] bArr, int i2, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeInt(i);
                    obtain.writeByteArray(bArr);
                    obtain.writeInt(i2);
                    obtain.writeString(str);
                    this.ky.transact(10012, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Intent) Intent.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Intent a(RoomEntity roomEntity, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    if (roomEntity != null) {
                        obtain.writeInt(1);
                        roomEntity.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeInt(i);
                    this.ky.transact(9011, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Intent) Intent.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Intent a(gy gyVar, String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    if (gyVar != null) {
                        obtain.writeInt(1);
                        gyVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.ky.transact(10021, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Intent) Intent.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Intent a(gz gzVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    if (gzVar != null) {
                        obtain.writeInt(1);
                        gzVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.ky.transact(10022, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Intent) Intent.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Intent a(ParticipantEntity[] participantEntityArr, String str, String str2, Uri uri, Uri uri2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeTypedArray(participantEntityArr, 0);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    if (uri != null) {
                        obtain.writeInt(1);
                        uri.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (uri2 != null) {
                        obtain.writeInt(1);
                        uri2.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.ky.transact(9031, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Intent) Intent.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(long j, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeLong(j);
                    obtain.writeString(str);
                    this.ky.transact(8019, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(IBinder iBinder, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(iBinder);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.ky.transact(5005, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    this.ky.transact(5002, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeInt(i);
                    this.ky.transact(10016, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, int i, int i2, int i3) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    this.ky.transact(10009, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, int i, int i2, boolean z, boolean z2) throws RemoteException {
                int i3 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(z ? 1 : 0);
                    if (!z2) {
                        i3 = 0;
                    }
                    obtain.writeInt(i3);
                    this.ky.transact(5044, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, int i, int i2, String[] strArr, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeStringArray(strArr);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.ky.transact(8004, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, int i, boolean z, boolean z2) throws RemoteException {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeInt(z ? 1 : 0);
                    if (!z2) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    this.ky.transact(5015, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, int i, int[] iArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeIntArray(iArr);
                    this.ky.transact(10018, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, long j) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeLong(j);
                    this.ky.transact(5058, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, long j, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeLong(j);
                    obtain.writeString(str);
                    this.ky.transact(8018, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, Bundle bundle, int i, int i2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.ky.transact(5021, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, IBinder iBinder, int i, String[] strArr, Bundle bundle, boolean z, long j) throws RemoteException {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeInt(i);
                    obtain.writeStringArray(strArr);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (!z) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    obtain.writeLong(j);
                    this.ky.transact(5030, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, IBinder iBinder, String str, boolean z, long j) throws RemoteException {
                int i = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    if (z) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    obtain.writeLong(j);
                    this.ky.transact(5031, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    this.ky.transact(5014, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.ky.transact(10011, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, int i, int i2, int i3, boolean z) throws RemoteException {
                int i4 = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    if (z) {
                        i4 = 1;
                    }
                    obtain.writeInt(i4);
                    this.ky.transact(5019, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, int i, IBinder iBinder, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(iBinder);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.ky.transact(5025, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, int i, boolean z) throws RemoteException {
                int i2 = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    if (z) {
                        i2 = 1;
                    }
                    obtain.writeInt(i2);
                    this.ky.transact(8023, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, int i, boolean z, boolean z2) throws RemoteException {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(z ? 1 : 0);
                    if (!z2) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    this.ky.transact(5045, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, int i, boolean z, boolean z2, boolean z3, boolean z4) throws RemoteException {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(z ? 1 : 0);
                    obtain.writeInt(z2 ? 1 : 0);
                    obtain.writeInt(z3 ? 1 : 0);
                    if (!z4) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    this.ky.transact(GamesStatusCodes.STATUS_MATCH_ERROR_INACTIVE_MATCH, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, int i, int[] iArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeIntArray(iArr);
                    this.ky.transact(10019, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, long j) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeLong(j);
                    this.ky.transact(5016, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, long j, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeLong(j);
                    obtain.writeString(str2);
                    this.ky.transact(GamesStatusCodes.STATUS_INVALID_REAL_TIME_ROOM_ID, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, IBinder iBinder, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeStrongBinder(iBinder);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.ky.transact(5023, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.ky.transact(5038, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, String str2, int i, int i2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.ky.transact(8001, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, String str2, int i, int i2, int i3) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    this.ky.transact(10010, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, String str2, int i, int i2, int i3, boolean z) throws RemoteException {
                int i4 = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    if (z) {
                        i4 = 1;
                    }
                    obtain.writeInt(i4);
                    this.ky.transact(5039, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, String str2, int i, boolean z, boolean z2) throws RemoteException {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeInt(i);
                    obtain.writeInt(z ? 1 : 0);
                    if (!z2) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    this.ky.transact(9028, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, String str2, boolean z) throws RemoteException {
                int i = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    if (z) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    this.ky.transact(GamesStatusCodes.STATUS_MULTIPLAYER_ERROR_INVALID_MULTIPLAYER_TYPE, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, String str2, String[] strArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeStringArray(strArr);
                    this.ky.transact(10008, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, boolean z) throws RemoteException {
                int i = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    if (z) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    this.ky.transact(5054, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, byte[] bArr, String str2, ParticipantResult[] participantResultArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeByteArray(bArr);
                    obtain.writeString(str2);
                    obtain.writeTypedArray(participantResultArr, 0);
                    this.ky.transact(8007, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, byte[] bArr, ParticipantResult[] participantResultArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeByteArray(bArr);
                    obtain.writeTypedArray(participantResultArr, 0);
                    this.ky.transact(8008, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, int[] iArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeIntArray(iArr);
                    this.ky.transact(8017, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String str, String[] strArr, int i, byte[] bArr, int i2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeStringArray(strArr);
                    obtain.writeInt(i);
                    obtain.writeByteArray(bArr);
                    obtain.writeInt(i2);
                    this.ky.transact(GamesActivityResultCodes.RESULT_LEFT_ROOM, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, boolean z) throws RemoteException {
                int i = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    if (z) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    this.ky.transact(GamesStatusCodes.STATUS_MULTIPLAYER_ERROR_NOT_TRUSTED_TESTER, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, boolean z, Bundle bundle) throws RemoteException {
                int i = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    if (!z) {
                        i = 0;
                    }
                    obtain.writeInt(i);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.ky.transact(5063, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, int[] iArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeIntArray(iArr);
                    this.ky.transact(8003, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ga gaVar, String[] strArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeStringArray(strArr);
                    this.ky.transact(GamesActivityResultCodes.RESULT_NETWORK_FAILURE, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int aA(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeString(str);
                    this.ky.transact(5060, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Uri aB(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeString(str);
                    this.ky.transact(5066, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Uri) Uri.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void aC(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeString(str);
                    this.ky.transact(8002, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public ParcelFileDescriptor aD(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeString(str);
                    this.ky.transact(9030, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (ParcelFileDescriptor) ParcelFileDescriptor.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void aU(int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeInt(i);
                    this.ky.transact(5036, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.ky;
            }

            public Intent au(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeString(str);
                    this.ky.transact(9004, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Intent) Intent.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String ax(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeString(str);
                    this.ky.transact(5064, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String ay(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeString(str);
                    this.ky.transact(5035, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void az(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeString(str);
                    this.ky.transact(5050, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int b(byte[] bArr, String str, String[] strArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeByteArray(bArr);
                    obtain.writeString(str);
                    obtain.writeStringArray(strArr);
                    this.ky.transact(5034, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Intent b(int i, int i2, boolean z) throws RemoteException {
                int i3 = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    if (z) {
                        i3 = 1;
                    }
                    obtain.writeInt(i3);
                    this.ky.transact(9009, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Intent) Intent.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(long j, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeLong(j);
                    obtain.writeString(str);
                    this.ky.transact(8021, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ga gaVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    this.ky.transact(5017, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ga gaVar, int i, boolean z, boolean z2) throws RemoteException {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeInt(z ? 1 : 0);
                    if (!z2) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    this.ky.transact(5046, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ga gaVar, long j) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeLong(j);
                    this.ky.transact(8012, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ga gaVar, long j, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeLong(j);
                    obtain.writeString(str);
                    this.ky.transact(8020, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ga gaVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    this.ky.transact(5018, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ga gaVar, String str, int i, int i2, int i3, boolean z) throws RemoteException {
                int i4 = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    if (z) {
                        i4 = 1;
                    }
                    obtain.writeInt(i4);
                    this.ky.transact(5020, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ga gaVar, String str, int i, IBinder iBinder, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(iBinder);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.ky.transact(GamesStatusCodes.STATUS_PARTICIPANT_NOT_CONNECTED, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ga gaVar, String str, int i, boolean z) throws RemoteException {
                int i2 = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    if (z) {
                        i2 = 1;
                    }
                    obtain.writeInt(i2);
                    this.ky.transact(10017, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ga gaVar, String str, int i, boolean z, boolean z2) throws RemoteException {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(z ? 1 : 0);
                    if (!z2) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    this.ky.transact(5501, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ga gaVar, String str, IBinder iBinder, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeStrongBinder(iBinder);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.ky.transact(5024, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ga gaVar, String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.ky.transact(5041, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ga gaVar, String str, String str2, int i, int i2, int i3, boolean z) throws RemoteException {
                int i4 = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    if (z) {
                        i4 = 1;
                    }
                    obtain.writeInt(i4);
                    this.ky.transact(5040, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ga gaVar, String str, String str2, boolean z) throws RemoteException {
                int i = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    if (z) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    this.ky.transact(GamesStatusCodes.STATUS_MATCH_NOT_FOUND, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ga gaVar, String str, boolean z) throws RemoteException {
                int i = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    if (z) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    this.ky.transact(GamesStatusCodes.STATUS_MATCH_ERROR_INVALID_MATCH_STATE, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ga gaVar, boolean z) throws RemoteException {
                int i = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    if (z) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    this.ky.transact(GamesStatusCodes.STATUS_MATCH_ERROR_OUT_OF_DATE_VERSION, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ga gaVar, String[] strArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeStringArray(strArr);
                    this.ky.transact(GamesActivityResultCodes.RESULT_SEND_REQUEST_FAILED, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(String str, String str2, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeInt(i);
                    this.ky.transact(5051, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(long j, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeLong(j);
                    obtain.writeString(str);
                    this.ky.transact(GamesActivityResultCodes.RESULT_APP_MISCONFIGURED, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(ga gaVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    this.ky.transact(5022, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(ga gaVar, int i, boolean z, boolean z2) throws RemoteException {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeInt(z ? 1 : 0);
                    if (!z2) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    this.ky.transact(5048, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(ga gaVar, long j) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeLong(j);
                    this.ky.transact(GamesActivityResultCodes.RESULT_RECONNECT_REQUIRED, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(ga gaVar, long j, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeLong(j);
                    obtain.writeString(str);
                    this.ky.transact(GamesActivityResultCodes.RESULT_LICENSE_FAILED, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(ga gaVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    this.ky.transact(5032, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(ga gaVar, String str, int i, boolean z, boolean z2) throws RemoteException {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(z ? 1 : 0);
                    if (!z2) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    this.ky.transact(9001, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(ga gaVar, String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.ky.transact(8011, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(ga gaVar, String str, boolean z) throws RemoteException {
                int i = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    if (z) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    this.ky.transact(GamesStatusCodes.STATUS_MATCH_ERROR_INVALID_MATCH_RESULTS, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(ga gaVar, boolean z) throws RemoteException {
                int i = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    if (z) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    this.ky.transact(8027, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(ga gaVar, String[] strArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeStringArray(strArr);
                    this.ky.transact(10020, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(String str, String str2, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeInt(i);
                    this.ky.transact(8026, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Bundle cY() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(5004, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void d(ga gaVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    this.ky.transact(5026, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void d(ga gaVar, int i, boolean z, boolean z2) throws RemoteException {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeInt(z ? 1 : 0);
                    if (!z2) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    this.ky.transact(GamesStatusCodes.STATUS_MULTIPLAYER_DISABLED, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void d(ga gaVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    this.ky.transact(5037, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void d(ga gaVar, String str, int i, boolean z, boolean z2) throws RemoteException {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(z ? 1 : 0);
                    if (!z2) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    this.ky.transact(9020, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void d(ga gaVar, String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.ky.transact(8015, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void d(ga gaVar, String str, boolean z) throws RemoteException {
                int i = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    if (z) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    this.ky.transact(GamesStatusCodes.STATUS_MATCH_ERROR_ALREADY_REMATCHED, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public ParcelFileDescriptor e(Uri uri) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    if (uri != null) {
                        obtain.writeInt(1);
                        uri.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.ky.transact(GamesStatusCodes.STATUS_MATCH_ERROR_LOCALLY_MODIFIED, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (ParcelFileDescriptor) ParcelFileDescriptor.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void e(ga gaVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    this.ky.transact(5027, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void e(ga gaVar, int i, boolean z, boolean z2) throws RemoteException {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeInt(z ? 1 : 0);
                    if (!z2) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    this.ky.transact(GamesStatusCodes.STATUS_MULTIPLAYER_ERROR_INVALID_OPERATION, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void e(ga gaVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    this.ky.transact(5042, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void e(ga gaVar, String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.ky.transact(8016, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void f(ga gaVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    this.ky.transact(5047, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void f(ga gaVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    this.ky.transact(5043, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int fA() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(9019, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String fB() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(5003, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int fC() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(8024, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Intent fD() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(10015, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Intent) Intent.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int fE() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(10013, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int fF() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(10023, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void fH() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(5006, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public DataHolder fI() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(5013, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? DataHolder.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean fJ() throws RemoteException {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(5067, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public DataHolder fK() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(5502, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? DataHolder.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void fL() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(8022, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Intent fM() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(9013, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Intent) Intent.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String fn() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(5007, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String fo() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(5012, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Intent fr() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(9003, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Intent) Intent.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Intent fs() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(9005, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Intent) Intent.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Intent ft() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(9006, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Intent) Intent.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Intent fu() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(9007, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Intent) Intent.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Intent fy() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(9010, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Intent) Intent.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Intent fz() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    this.ky.transact(9012, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Intent) Intent.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void g(ga gaVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    this.ky.transact(5049, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void g(ga gaVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    this.ky.transact(5052, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public DataHolder h(ga gaVar, String str) throws RemoteException {
                DataHolder dataHolder = null;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    this.ky.transact(5053, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        dataHolder = DataHolder.CREATOR.createFromParcel(obtain2);
                    }
                    return dataHolder;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void h(ga gaVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    this.ky.transact(5056, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void i(ga gaVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    this.ky.transact(5062, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void i(ga gaVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    this.ky.transact(5061, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void j(ga gaVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    this.ky.transact(5057, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void j(String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.ky.transact(5065, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void k(ga gaVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    this.ky.transact(GamesStatusCodes.STATUS_REAL_TIME_MESSAGE_SEND_FAILED, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void k(String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.ky.transact(8025, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void l(ga gaVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    this.ky.transact(8005, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void l(String str, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.ky.transact(5029, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m(ga gaVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    this.ky.transact(8006, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m(String str, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.ky.transact(5028, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void n(long j) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeLong(j);
                    this.ky.transact(5001, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void n(ga gaVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    this.ky.transact(8009, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void n(String str, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.ky.transact(5055, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void o(long j) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeLong(j);
                    this.ky.transact(5059, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void o(ga gaVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    this.ky.transact(8010, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void o(String str, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.ky.transact(10014, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void p(long j) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeLong(j);
                    this.ky.transact(8013, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void p(ga gaVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    this.ky.transact(8014, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void q(long j) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeLong(j);
                    this.ky.transact(GamesActivityResultCodes.RESULT_SIGN_IN_FAILED, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void q(ga gaVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    obtain.writeStrongBinder(gaVar != null ? gaVar.asBinder() : null);
                    obtain.writeString(str);
                    this.ky.transact(9002, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void y(boolean z) throws RemoteException {
                int i = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.games.internal.IGamesService");
                    if (z) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    this.ky.transact(5068, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public static gb J(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.games.internal.IGamesService");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof gb)) ? new C0035a(iBinder) : (gb) queryLocalInterface;
        }

        /* JADX WARNING: type inference failed for: r12v0, types: [android.os.Parcel] */
        /* JADX WARNING: type inference failed for: r5v0 */
        /* JADX WARNING: type inference failed for: r5v1, types: [com.google.android.gms.internal.gy] */
        /* JADX WARNING: type inference failed for: r5v2, types: [com.google.android.gms.internal.gy] */
        /* JADX WARNING: type inference failed for: r5v3, types: [com.google.android.gms.internal.gz] */
        /* JADX WARNING: type inference failed for: r5v4, types: [com.google.android.gms.internal.gz] */
        /* JADX WARNING: type inference failed for: r5v13, types: [android.net.Uri] */
        /* JADX WARNING: type inference failed for: r0v90, types: [android.net.Uri] */
        /* JADX WARNING: type inference failed for: r5v14 */
        /* JADX WARNING: type inference failed for: r5v20, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v209, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r5v21 */
        /* JADX WARNING: type inference failed for: r5v23, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v224, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r5v24 */
        /* JADX WARNING: type inference failed for: r5v41, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v410, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r5v42 */
        /* JADX WARNING: type inference failed for: r5v43, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v432, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r5v44 */
        /* JADX WARNING: type inference failed for: r5v47 */
        /* JADX WARNING: type inference failed for: r5v48 */
        /* JADX WARNING: Incorrect type for immutable var: ssa=android.os.Parcel, code=null, for r12v0, types: [android.os.Parcel] */
        /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r5v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.internal.gz, com.google.android.gms.internal.gy, ?[OBJECT, ARRAY]]
  uses: [com.google.android.gms.internal.gy, com.google.android.gms.internal.gz, android.net.Uri, android.os.Bundle]
  mth insns count: 1559
        	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
        	at jadx.core.ProcessClass.process(ProcessClass.java:30)
        	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
        	at jadx.core.ProcessClass.process(ProcessClass.java:35)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
         */
        /* JADX WARNING: Unknown variable types count: 9 */
        public boolean onTransact(int code, Parcel r12, Parcel reply, int flags) throws RemoteException {
            ? r5 = 0;
            boolean z = false;
            switch (code) {
                case 5001:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    n(r12.readLong());
                    reply.writeNoException();
                    return true;
                case 5002:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 5003:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    String fB = fB();
                    reply.writeNoException();
                    reply.writeString(fB);
                    return true;
                case 5004:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    Bundle cY = cY();
                    reply.writeNoException();
                    if (cY != null) {
                        reply.writeInt(1);
                        cY.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 5005:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(r12.readStrongBinder(), r12.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(r12) : null);
                    reply.writeNoException();
                    return true;
                case 5006:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    fH();
                    reply.writeNoException();
                    return true;
                case 5007:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    String fn = fn();
                    reply.writeNoException();
                    reply.writeString(fn);
                    return true;
                case 5012:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    String fo = fo();
                    reply.writeNoException();
                    reply.writeString(fo);
                    return true;
                case 5013:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    DataHolder fI = fI();
                    reply.writeNoException();
                    if (fI != null) {
                        reply.writeInt(1);
                        fI.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 5014:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString());
                    reply.writeNoException();
                    return true;
                case 5015:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    int readInt = r12.readInt();
                    boolean z2 = r12.readInt() != 0;
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    a(I, readInt, z2, z);
                    reply.writeNoException();
                    return true;
                case 5016:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readLong());
                    reply.writeNoException();
                    return true;
                case 5017:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    b(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 5018:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    b(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString());
                    reply.writeNoException();
                    return true;
                case 5019:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readInt(), r12.readInt(), r12.readInt(), r12.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 5020:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    b(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readInt(), r12.readInt(), r12.readInt(), r12.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 5021:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(r12) : null, r12.readInt(), r12.readInt());
                    reply.writeNoException();
                    return true;
                case 5022:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    c(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 5023:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readStrongBinder(), r12.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(r12) : null);
                    reply.writeNoException();
                    return true;
                case 5024:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    b(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readStrongBinder(), r12.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(r12) : null);
                    reply.writeNoException();
                    return true;
                case 5025:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I2 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    String readString = r12.readString();
                    int readInt2 = r12.readInt();
                    IBinder readStrongBinder = r12.readStrongBinder();
                    if (r12.readInt() != 0) {
                        r5 = (Bundle) Bundle.CREATOR.createFromParcel(r12);
                    }
                    a(I2, readString, readInt2, readStrongBinder, (Bundle) r5);
                    reply.writeNoException();
                    return true;
                case 5026:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    d(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 5027:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    e(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 5028:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    m(r12.readString(), r12.readInt());
                    reply.writeNoException();
                    return true;
                case 5029:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    l(r12.readString(), r12.readInt());
                    reply.writeNoException();
                    return true;
                case 5030:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I3 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    IBinder readStrongBinder2 = r12.readStrongBinder();
                    int readInt3 = r12.readInt();
                    String[] createStringArray = r12.createStringArray();
                    if (r12.readInt() != 0) {
                        r5 = (Bundle) Bundle.CREATOR.createFromParcel(r12);
                    }
                    a(I3, readStrongBinder2, readInt3, createStringArray, (Bundle) r5, r12.readInt() != 0, r12.readLong());
                    reply.writeNoException();
                    return true;
                case 5031:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readStrongBinder(), r12.readString(), r12.readInt() != 0, r12.readLong());
                    reply.writeNoException();
                    return true;
                case 5032:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    c(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString());
                    reply.writeNoException();
                    return true;
                case 5033:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    int a = a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.createByteArray(), r12.readString(), r12.readString());
                    reply.writeNoException();
                    reply.writeInt(a);
                    return true;
                case 5034:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    int b = b(r12.createByteArray(), r12.readString(), r12.createStringArray());
                    reply.writeNoException();
                    reply.writeInt(b);
                    return true;
                case 5035:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    String ay = ay(r12.readString());
                    reply.writeNoException();
                    reply.writeString(ay);
                    return true;
                case 5036:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    aU(r12.readInt());
                    reply.writeNoException();
                    return true;
                case 5037:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    d(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString());
                    reply.writeNoException();
                    return true;
                case 5038:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readString());
                    reply.writeNoException();
                    return true;
                case 5039:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I4 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    String readString2 = r12.readString();
                    String readString3 = r12.readString();
                    int readInt4 = r12.readInt();
                    int readInt5 = r12.readInt();
                    int readInt6 = r12.readInt();
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    a(I4, readString2, readString3, readInt4, readInt5, readInt6, z);
                    reply.writeNoException();
                    return true;
                case 5040:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I5 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    String readString4 = r12.readString();
                    String readString5 = r12.readString();
                    int readInt7 = r12.readInt();
                    int readInt8 = r12.readInt();
                    int readInt9 = r12.readInt();
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    b(I5, readString4, readString5, readInt7, readInt8, readInt9, z);
                    reply.writeNoException();
                    return true;
                case 5041:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    b(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readString());
                    reply.writeNoException();
                    return true;
                case 5042:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    e(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString());
                    reply.writeNoException();
                    return true;
                case 5043:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    f(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString());
                    reply.writeNoException();
                    return true;
                case 5044:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readInt(), r12.readInt(), r12.readInt() != 0, r12.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 5045:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readInt(), r12.readInt() != 0, r12.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 5046:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I6 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    int readInt10 = r12.readInt();
                    boolean z3 = r12.readInt() != 0;
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    b(I6, readInt10, z3, z);
                    reply.writeNoException();
                    return true;
                case 5047:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    f(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 5048:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I7 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    int readInt11 = r12.readInt();
                    boolean z4 = r12.readInt() != 0;
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    c(I7, readInt11, z4, z);
                    reply.writeNoException();
                    return true;
                case 5049:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    g(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 5050:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    az(r12.readString());
                    reply.writeNoException();
                    return true;
                case 5051:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    b(r12.readString(), r12.readString(), r12.readInt());
                    reply.writeNoException();
                    return true;
                case 5052:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    g(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString());
                    reply.writeNoException();
                    return true;
                case 5053:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    DataHolder h = h(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString());
                    reply.writeNoException();
                    if (h != null) {
                        reply.writeInt(1);
                        h.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 5054:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I8 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    String readString6 = r12.readString();
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    a(I8, readString6, z);
                    reply.writeNoException();
                    return true;
                case 5055:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    n(r12.readString(), r12.readInt());
                    reply.writeNoException();
                    return true;
                case 5056:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    h(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 5057:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    j(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString());
                    reply.writeNoException();
                    return true;
                case 5058:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readLong());
                    reply.writeNoException();
                    return true;
                case 5059:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    o(r12.readLong());
                    reply.writeNoException();
                    return true;
                case 5060:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    int aA = aA(r12.readString());
                    reply.writeNoException();
                    reply.writeInt(aA);
                    return true;
                case 5061:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    i(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString());
                    reply.writeNoException();
                    return true;
                case 5062:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    i(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 5063:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I9 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    a(I9, z, r12.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(r12) : null);
                    reply.writeNoException();
                    return true;
                case 5064:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    String ax = ax(r12.readString());
                    reply.writeNoException();
                    reply.writeString(ax);
                    return true;
                case 5065:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    j(r12.readString(), r12.readString());
                    reply.writeNoException();
                    return true;
                case 5066:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    Uri aB = aB(r12.readString());
                    reply.writeNoException();
                    if (aB != null) {
                        reply.writeInt(1);
                        aB.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 5067:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    boolean fJ = fJ();
                    reply.writeNoException();
                    if (fJ) {
                        z = true;
                    }
                    reply.writeInt(z);
                    return true;
                case 5068:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    y(z);
                    reply.writeNoException();
                    return true;
                case 5501:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    b(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readInt(), r12.readInt() != 0, r12.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 5502:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    DataHolder fK = fK();
                    reply.writeNoException();
                    if (fK != null) {
                        reply.writeInt(1);
                        fK.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case GamesStatusCodes.STATUS_MULTIPLAYER_ERROR_NOT_TRUSTED_TESTER /*6001*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I10 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    a(I10, z);
                    reply.writeNoException();
                    return true;
                case GamesStatusCodes.STATUS_MULTIPLAYER_ERROR_INVALID_MULTIPLAYER_TYPE /*6002*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I11 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    String readString7 = r12.readString();
                    String readString8 = r12.readString();
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    a(I11, readString7, readString8, z);
                    reply.writeNoException();
                    return true;
                case GamesStatusCodes.STATUS_MULTIPLAYER_DISABLED /*6003*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I12 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    int readInt12 = r12.readInt();
                    boolean z5 = r12.readInt() != 0;
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    d(I12, readInt12, z5, z);
                    reply.writeNoException();
                    return true;
                case GamesStatusCodes.STATUS_MULTIPLAYER_ERROR_INVALID_OPERATION /*6004*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I13 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    int readInt13 = r12.readInt();
                    boolean z6 = r12.readInt() != 0;
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    e(I13, readInt13, z6, z);
                    reply.writeNoException();
                    return true;
                case GamesStatusCodes.STATUS_MATCH_ERROR_INACTIVE_MATCH /*6501*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I14 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    String readString9 = r12.readString();
                    int readInt14 = r12.readInt();
                    boolean z7 = r12.readInt() != 0;
                    boolean z8 = r12.readInt() != 0;
                    boolean z9 = r12.readInt() != 0;
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    a(I14, readString9, readInt14, z7, z8, z9, z);
                    reply.writeNoException();
                    return true;
                case GamesStatusCodes.STATUS_MATCH_ERROR_INVALID_MATCH_STATE /*6502*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I15 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    String readString10 = r12.readString();
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    b(I15, readString10, z);
                    reply.writeNoException();
                    return true;
                case GamesStatusCodes.STATUS_MATCH_ERROR_OUT_OF_DATE_VERSION /*6503*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I16 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    b(I16, z);
                    reply.writeNoException();
                    return true;
                case GamesStatusCodes.STATUS_MATCH_ERROR_INVALID_MATCH_RESULTS /*6504*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I17 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    String readString11 = r12.readString();
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    c(I17, readString11, z);
                    reply.writeNoException();
                    return true;
                case GamesStatusCodes.STATUS_MATCH_ERROR_ALREADY_REMATCHED /*6505*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I18 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    String readString12 = r12.readString();
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    d(I18, readString12, z);
                    reply.writeNoException();
                    return true;
                case GamesStatusCodes.STATUS_MATCH_NOT_FOUND /*6506*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I19 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    String readString13 = r12.readString();
                    String readString14 = r12.readString();
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    b(I19, readString13, readString14, z);
                    reply.writeNoException();
                    return true;
                case GamesStatusCodes.STATUS_MATCH_ERROR_LOCALLY_MODIFIED /*6507*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ParcelFileDescriptor e = e(r12.readInt() != 0 ? (Uri) Uri.CREATOR.createFromParcel(r12) : null);
                    reply.writeNoException();
                    if (e != null) {
                        reply.writeInt(1);
                        e.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case GamesStatusCodes.STATUS_REAL_TIME_MESSAGE_SEND_FAILED /*7001*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    k(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString());
                    reply.writeNoException();
                    return true;
                case GamesStatusCodes.STATUS_INVALID_REAL_TIME_ROOM_ID /*7002*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readLong(), r12.readString());
                    reply.writeNoException();
                    return true;
                case GamesStatusCodes.STATUS_PARTICIPANT_NOT_CONNECTED /*7003*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I20 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    String readString15 = r12.readString();
                    int readInt15 = r12.readInt();
                    IBinder readStrongBinder3 = r12.readStrongBinder();
                    if (r12.readInt() != 0) {
                        r5 = (Bundle) Bundle.CREATOR.createFromParcel(r12);
                    }
                    b(I20, readString15, readInt15, readStrongBinder3, (Bundle) r5);
                    reply.writeNoException();
                    return true;
                case 8001:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readString(), r12.readInt(), r12.readInt());
                    reply.writeNoException();
                    return true;
                case 8002:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    aC(r12.readString());
                    reply.writeNoException();
                    return true;
                case 8003:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.createIntArray());
                    reply.writeNoException();
                    return true;
                case 8004:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I21 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    int readInt16 = r12.readInt();
                    int readInt17 = r12.readInt();
                    String[] createStringArray2 = r12.createStringArray();
                    if (r12.readInt() != 0) {
                        r5 = (Bundle) Bundle.CREATOR.createFromParcel(r12);
                    }
                    a(I21, readInt16, readInt17, createStringArray2, (Bundle) r5);
                    reply.writeNoException();
                    return true;
                case 8005:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    l(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString());
                    reply.writeNoException();
                    return true;
                case 8006:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    m(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString());
                    reply.writeNoException();
                    return true;
                case 8007:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.createByteArray(), r12.readString(), (ParticipantResult[]) r12.createTypedArray(ParticipantResult.CREATOR));
                    reply.writeNoException();
                    return true;
                case 8008:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.createByteArray(), (ParticipantResult[]) r12.createTypedArray(ParticipantResult.CREATOR));
                    reply.writeNoException();
                    return true;
                case 8009:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    n(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString());
                    reply.writeNoException();
                    return true;
                case 8010:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    o(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString());
                    reply.writeNoException();
                    return true;
                case 8011:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    c(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readString());
                    reply.writeNoException();
                    return true;
                case 8012:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    b(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readLong());
                    reply.writeNoException();
                    return true;
                case 8013:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    p(r12.readLong());
                    reply.writeNoException();
                    return true;
                case 8014:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    p(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString());
                    reply.writeNoException();
                    return true;
                case 8015:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    d(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readString());
                    reply.writeNoException();
                    return true;
                case 8016:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    e(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readString());
                    reply.writeNoException();
                    return true;
                case 8017:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.createIntArray());
                    reply.writeNoException();
                    return true;
                case 8018:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readLong(), r12.readString());
                    reply.writeNoException();
                    return true;
                case 8019:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(r12.readLong(), r12.readString());
                    reply.writeNoException();
                    return true;
                case 8020:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    b(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readLong(), r12.readString());
                    reply.writeNoException();
                    return true;
                case 8021:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    b(r12.readLong(), r12.readString());
                    reply.writeNoException();
                    return true;
                case 8022:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    fL();
                    reply.writeNoException();
                    return true;
                case 8023:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I22 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    String readString16 = r12.readString();
                    int readInt18 = r12.readInt();
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    a(I22, readString16, readInt18, z);
                    reply.writeNoException();
                    return true;
                case 8024:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    int fC = fC();
                    reply.writeNoException();
                    reply.writeInt(fC);
                    return true;
                case 8025:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    k(r12.readString(), r12.readString());
                    reply.writeNoException();
                    return true;
                case 8026:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    c(r12.readString(), r12.readString(), r12.readInt());
                    reply.writeNoException();
                    return true;
                case 8027:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I23 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    c(I23, z);
                    reply.writeNoException();
                    return true;
                case 9001:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    c(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readInt(), r12.readInt() != 0, r12.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 9002:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    q(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString());
                    reply.writeNoException();
                    return true;
                case 9003:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    Intent fr = fr();
                    reply.writeNoException();
                    if (fr != null) {
                        reply.writeInt(1);
                        fr.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 9004:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    Intent au = au(r12.readString());
                    reply.writeNoException();
                    if (au != null) {
                        reply.writeInt(1);
                        au.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 9005:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    Intent fs = fs();
                    reply.writeNoException();
                    if (fs != null) {
                        reply.writeInt(1);
                        fs.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 9006:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    Intent ft = ft();
                    reply.writeNoException();
                    if (ft != null) {
                        reply.writeInt(1);
                        ft.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 9007:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    Intent fu = fu();
                    reply.writeNoException();
                    if (fu != null) {
                        reply.writeInt(1);
                        fu.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 9008:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    Intent a2 = a(r12.readInt(), r12.readInt(), r12.readInt() != 0);
                    reply.writeNoException();
                    if (a2 != null) {
                        reply.writeInt(1);
                        a2.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 9009:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    Intent b2 = b(r12.readInt(), r12.readInt(), r12.readInt() != 0);
                    reply.writeNoException();
                    if (b2 != null) {
                        reply.writeInt(1);
                        b2.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 9010:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    Intent fy = fy();
                    reply.writeNoException();
                    if (fy != null) {
                        reply.writeInt(1);
                        fy.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 9011:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    Intent a3 = a(r12.readInt() != 0 ? (RoomEntity) RoomEntity.CREATOR.createFromParcel(r12) : null, r12.readInt());
                    reply.writeNoException();
                    if (a3 != null) {
                        reply.writeInt(1);
                        a3.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 9012:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    Intent fz = fz();
                    reply.writeNoException();
                    if (fz != null) {
                        reply.writeInt(1);
                        fz.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 9013:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    Intent fM = fM();
                    reply.writeNoException();
                    if (fM != null) {
                        reply.writeInt(1);
                        fM.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 9019:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    int fA = fA();
                    reply.writeNoException();
                    reply.writeInt(fA);
                    return true;
                case 9020:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    d(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readInt(), r12.readInt() != 0, r12.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 9028:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readString(), r12.readInt(), r12.readInt() != 0, r12.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 9030:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ParcelFileDescriptor aD = aD(r12.readString());
                    reply.writeNoException();
                    if (aD != null) {
                        reply.writeInt(1);
                        aD.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 9031:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ParticipantEntity[] participantEntityArr = (ParticipantEntity[]) r12.createTypedArray(ParticipantEntity.CREATOR);
                    String readString17 = r12.readString();
                    String readString18 = r12.readString();
                    Uri uri = r12.readInt() != 0 ? (Uri) Uri.CREATOR.createFromParcel(r12) : null;
                    if (r12.readInt() != 0) {
                        r5 = (Uri) Uri.CREATOR.createFromParcel(r12);
                    }
                    Intent a4 = a(participantEntityArr, readString17, readString18, uri, (Uri) r5);
                    reply.writeNoException();
                    if (a4 != null) {
                        reply.writeInt(1);
                        a4.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case GamesActivityResultCodes.RESULT_RECONNECT_REQUIRED /*10001*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    c(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readLong());
                    reply.writeNoException();
                    return true;
                case GamesActivityResultCodes.RESULT_SIGN_IN_FAILED /*10002*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    q(r12.readLong());
                    reply.writeNoException();
                    return true;
                case GamesActivityResultCodes.RESULT_LICENSE_FAILED /*10003*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    c(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readLong(), r12.readString());
                    reply.writeNoException();
                    return true;
                case GamesActivityResultCodes.RESULT_APP_MISCONFIGURED /*10004*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    c(r12.readLong(), r12.readString());
                    reply.writeNoException();
                    return true;
                case GamesActivityResultCodes.RESULT_LEFT_ROOM /*10005*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.createStringArray(), r12.readInt(), r12.createByteArray(), r12.readInt());
                    reply.writeNoException();
                    return true;
                case GamesActivityResultCodes.RESULT_NETWORK_FAILURE /*10006*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.createStringArray());
                    reply.writeNoException();
                    return true;
                case GamesActivityResultCodes.RESULT_SEND_REQUEST_FAILED /*10007*/:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    b(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.createStringArray());
                    reply.writeNoException();
                    return true;
                case 10008:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readString(), r12.createStringArray());
                    reply.writeNoException();
                    return true;
                case 10009:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readInt(), r12.readInt(), r12.readInt());
                    reply.writeNoException();
                    return true;
                case 10010:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readString(), r12.readInt(), r12.readInt(), r12.readInt());
                    reply.writeNoException();
                    return true;
                case 10011:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readInt());
                    reply.writeNoException();
                    return true;
                case 10012:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    Intent a5 = a(r12.readInt(), r12.createByteArray(), r12.readInt(), r12.readString());
                    reply.writeNoException();
                    if (a5 != null) {
                        reply.writeInt(1);
                        a5.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 10013:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    int fE = fE();
                    reply.writeNoException();
                    reply.writeInt(fE);
                    return true;
                case 10014:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    o(r12.readString(), r12.readInt());
                    reply.writeNoException();
                    return true;
                case 10015:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    Intent fD = fD();
                    reply.writeNoException();
                    if (fD != null) {
                        reply.writeInt(1);
                        fD.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 10016:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readInt());
                    reply.writeNoException();
                    return true;
                case 10017:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    ga I24 = com.google.android.gms.internal.ga.a.I(r12.readStrongBinder());
                    String readString19 = r12.readString();
                    int readInt19 = r12.readInt();
                    if (r12.readInt() != 0) {
                        z = true;
                    }
                    b(I24, readString19, readInt19, z);
                    reply.writeNoException();
                    return true;
                case 10018:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readInt(), r12.createIntArray());
                    reply.writeNoException();
                    return true;
                case 10019:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    a(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.readString(), r12.readInt(), r12.createIntArray());
                    reply.writeNoException();
                    return true;
                case 10020:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    c(com.google.android.gms.internal.ga.a.I(r12.readStrongBinder()), r12.createStringArray());
                    reply.writeNoException();
                    return true;
                case 10021:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    if (r12.readInt() != 0) {
                        r5 = gy.CREATOR.createFromParcel(r12);
                    }
                    Intent a6 = a((gy) r5, r12.readString(), r12.readString());
                    reply.writeNoException();
                    if (a6 != null) {
                        reply.writeInt(1);
                        a6.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 10022:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    if (r12.readInt() != 0) {
                        r5 = gz.CREATOR.createFromParcel(r12);
                    }
                    Intent a7 = a((gz) r5, r12.readString());
                    reply.writeNoException();
                    if (a7 != null) {
                        reply.writeInt(1);
                        a7.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 10023:
                    r12.enforceInterface("com.google.android.gms.games.internal.IGamesService");
                    int fF = fF();
                    reply.writeNoException();
                    reply.writeInt(fF);
                    return true;
                case 1598968902:
                    reply.writeString("com.google.android.gms.games.internal.IGamesService");
                    return true;
                default:
                    return super.onTransact(code, r12, reply, flags);
            }
        }
    }

    int a(ga gaVar, byte[] bArr, String str, String str2) throws RemoteException;

    Intent a(int i, int i2, boolean z) throws RemoteException;

    Intent a(int i, byte[] bArr, int i2, String str) throws RemoteException;

    Intent a(RoomEntity roomEntity, int i) throws RemoteException;

    Intent a(gy gyVar, String str, String str2) throws RemoteException;

    Intent a(gz gzVar, String str) throws RemoteException;

    Intent a(ParticipantEntity[] participantEntityArr, String str, String str2, Uri uri, Uri uri2) throws RemoteException;

    void a(long j, String str) throws RemoteException;

    void a(IBinder iBinder, Bundle bundle) throws RemoteException;

    void a(ga gaVar) throws RemoteException;

    void a(ga gaVar, int i) throws RemoteException;

    void a(ga gaVar, int i, int i2, int i3) throws RemoteException;

    void a(ga gaVar, int i, int i2, boolean z, boolean z2) throws RemoteException;

    void a(ga gaVar, int i, int i2, String[] strArr, Bundle bundle) throws RemoteException;

    void a(ga gaVar, int i, boolean z, boolean z2) throws RemoteException;

    void a(ga gaVar, int i, int[] iArr) throws RemoteException;

    void a(ga gaVar, long j) throws RemoteException;

    void a(ga gaVar, long j, String str) throws RemoteException;

    void a(ga gaVar, Bundle bundle, int i, int i2) throws RemoteException;

    void a(ga gaVar, IBinder iBinder, int i, String[] strArr, Bundle bundle, boolean z, long j) throws RemoteException;

    void a(ga gaVar, IBinder iBinder, String str, boolean z, long j) throws RemoteException;

    void a(ga gaVar, String str) throws RemoteException;

    void a(ga gaVar, String str, int i) throws RemoteException;

    void a(ga gaVar, String str, int i, int i2, int i3, boolean z) throws RemoteException;

    void a(ga gaVar, String str, int i, IBinder iBinder, Bundle bundle) throws RemoteException;

    void a(ga gaVar, String str, int i, boolean z) throws RemoteException;

    void a(ga gaVar, String str, int i, boolean z, boolean z2) throws RemoteException;

    void a(ga gaVar, String str, int i, boolean z, boolean z2, boolean z3, boolean z4) throws RemoteException;

    void a(ga gaVar, String str, int i, int[] iArr) throws RemoteException;

    void a(ga gaVar, String str, long j) throws RemoteException;

    void a(ga gaVar, String str, long j, String str2) throws RemoteException;

    void a(ga gaVar, String str, IBinder iBinder, Bundle bundle) throws RemoteException;

    void a(ga gaVar, String str, String str2) throws RemoteException;

    void a(ga gaVar, String str, String str2, int i, int i2) throws RemoteException;

    void a(ga gaVar, String str, String str2, int i, int i2, int i3) throws RemoteException;

    void a(ga gaVar, String str, String str2, int i, int i2, int i3, boolean z) throws RemoteException;

    void a(ga gaVar, String str, String str2, int i, boolean z, boolean z2) throws RemoteException;

    void a(ga gaVar, String str, String str2, boolean z) throws RemoteException;

    void a(ga gaVar, String str, String str2, String[] strArr) throws RemoteException;

    void a(ga gaVar, String str, boolean z) throws RemoteException;

    void a(ga gaVar, String str, byte[] bArr, String str2, ParticipantResult[] participantResultArr) throws RemoteException;

    void a(ga gaVar, String str, byte[] bArr, ParticipantResult[] participantResultArr) throws RemoteException;

    void a(ga gaVar, String str, int[] iArr) throws RemoteException;

    void a(ga gaVar, String str, String[] strArr, int i, byte[] bArr, int i2) throws RemoteException;

    void a(ga gaVar, boolean z) throws RemoteException;

    void a(ga gaVar, boolean z, Bundle bundle) throws RemoteException;

    void a(ga gaVar, int[] iArr) throws RemoteException;

    void a(ga gaVar, String[] strArr) throws RemoteException;

    int aA(String str) throws RemoteException;

    Uri aB(String str) throws RemoteException;

    void aC(String str) throws RemoteException;

    ParcelFileDescriptor aD(String str) throws RemoteException;

    void aU(int i) throws RemoteException;

    Intent au(String str) throws RemoteException;

    String ax(String str) throws RemoteException;

    String ay(String str) throws RemoteException;

    void az(String str) throws RemoteException;

    int b(byte[] bArr, String str, String[] strArr) throws RemoteException;

    Intent b(int i, int i2, boolean z) throws RemoteException;

    void b(long j, String str) throws RemoteException;

    void b(ga gaVar) throws RemoteException;

    void b(ga gaVar, int i, boolean z, boolean z2) throws RemoteException;

    void b(ga gaVar, long j) throws RemoteException;

    void b(ga gaVar, long j, String str) throws RemoteException;

    void b(ga gaVar, String str) throws RemoteException;

    void b(ga gaVar, String str, int i, int i2, int i3, boolean z) throws RemoteException;

    void b(ga gaVar, String str, int i, IBinder iBinder, Bundle bundle) throws RemoteException;

    void b(ga gaVar, String str, int i, boolean z) throws RemoteException;

    void b(ga gaVar, String str, int i, boolean z, boolean z2) throws RemoteException;

    void b(ga gaVar, String str, IBinder iBinder, Bundle bundle) throws RemoteException;

    void b(ga gaVar, String str, String str2) throws RemoteException;

    void b(ga gaVar, String str, String str2, int i, int i2, int i3, boolean z) throws RemoteException;

    void b(ga gaVar, String str, String str2, boolean z) throws RemoteException;

    void b(ga gaVar, String str, boolean z) throws RemoteException;

    void b(ga gaVar, boolean z) throws RemoteException;

    void b(ga gaVar, String[] strArr) throws RemoteException;

    void b(String str, String str2, int i) throws RemoteException;

    void c(long j, String str) throws RemoteException;

    void c(ga gaVar) throws RemoteException;

    void c(ga gaVar, int i, boolean z, boolean z2) throws RemoteException;

    void c(ga gaVar, long j) throws RemoteException;

    void c(ga gaVar, long j, String str) throws RemoteException;

    void c(ga gaVar, String str) throws RemoteException;

    void c(ga gaVar, String str, int i, boolean z, boolean z2) throws RemoteException;

    void c(ga gaVar, String str, String str2) throws RemoteException;

    void c(ga gaVar, String str, boolean z) throws RemoteException;

    void c(ga gaVar, boolean z) throws RemoteException;

    void c(ga gaVar, String[] strArr) throws RemoteException;

    void c(String str, String str2, int i) throws RemoteException;

    Bundle cY() throws RemoteException;

    void d(ga gaVar) throws RemoteException;

    void d(ga gaVar, int i, boolean z, boolean z2) throws RemoteException;

    void d(ga gaVar, String str) throws RemoteException;

    void d(ga gaVar, String str, int i, boolean z, boolean z2) throws RemoteException;

    void d(ga gaVar, String str, String str2) throws RemoteException;

    void d(ga gaVar, String str, boolean z) throws RemoteException;

    ParcelFileDescriptor e(Uri uri) throws RemoteException;

    void e(ga gaVar) throws RemoteException;

    void e(ga gaVar, int i, boolean z, boolean z2) throws RemoteException;

    void e(ga gaVar, String str) throws RemoteException;

    void e(ga gaVar, String str, String str2) throws RemoteException;

    void f(ga gaVar) throws RemoteException;

    void f(ga gaVar, String str) throws RemoteException;

    int fA() throws RemoteException;

    String fB() throws RemoteException;

    int fC() throws RemoteException;

    Intent fD() throws RemoteException;

    int fE() throws RemoteException;

    int fF() throws RemoteException;

    void fH() throws RemoteException;

    DataHolder fI() throws RemoteException;

    boolean fJ() throws RemoteException;

    DataHolder fK() throws RemoteException;

    void fL() throws RemoteException;

    Intent fM() throws RemoteException;

    String fn() throws RemoteException;

    String fo() throws RemoteException;

    Intent fr() throws RemoteException;

    Intent fs() throws RemoteException;

    Intent ft() throws RemoteException;

    Intent fu() throws RemoteException;

    Intent fy() throws RemoteException;

    Intent fz() throws RemoteException;

    void g(ga gaVar) throws RemoteException;

    void g(ga gaVar, String str) throws RemoteException;

    DataHolder h(ga gaVar, String str) throws RemoteException;

    void h(ga gaVar) throws RemoteException;

    void i(ga gaVar) throws RemoteException;

    void i(ga gaVar, String str) throws RemoteException;

    void j(ga gaVar, String str) throws RemoteException;

    void j(String str, String str2) throws RemoteException;

    void k(ga gaVar, String str) throws RemoteException;

    void k(String str, String str2) throws RemoteException;

    void l(ga gaVar, String str) throws RemoteException;

    void l(String str, int i) throws RemoteException;

    void m(ga gaVar, String str) throws RemoteException;

    void m(String str, int i) throws RemoteException;

    void n(long j) throws RemoteException;

    void n(ga gaVar, String str) throws RemoteException;

    void n(String str, int i) throws RemoteException;

    void o(long j) throws RemoteException;

    void o(ga gaVar, String str) throws RemoteException;

    void o(String str, int i) throws RemoteException;

    void p(long j) throws RemoteException;

    void p(ga gaVar, String str) throws RemoteException;

    void q(long j) throws RemoteException;

    void q(ga gaVar, String str) throws RemoteException;

    void y(boolean z) throws RemoteException;
}
