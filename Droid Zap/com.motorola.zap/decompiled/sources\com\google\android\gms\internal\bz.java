package com.google.android.gms.internal;

import android.content.Context;
import android.os.SystemClock;
import android.text.TextUtils;
import android.webkit.WebView;
import org.json.JSONException;

public final class bz extends ct implements com.google.android.gms.internal.ca.a, com.google.android.gms.internal.de.a {
    private final bf kH;
    private final Context mContext;
    /* access modifiers changed from: private */
    public final Object mg = new Object();
    private ay mh;
    /* access modifiers changed from: private */
    public final com.google.android.gms.internal.by.a nM;
    private final Object nN = new Object();
    private final com.google.android.gms.internal.cd.a nO;
    private final l nP;
    private ct nQ;
    /* access modifiers changed from: private */
    public cf nR;
    private boolean nS = false;
    private aw nT;
    private bc nU;
    /* access modifiers changed from: private */
    public final dd ng;

    private static final class a extends Exception {
        private final int nX;

        public a(String str, int i) {
            super(str);
            this.nX = i;
        }

        public int getErrorCode() {
            return this.nX;
        }
    }

    public bz(Context context, com.google.android.gms.internal.cd.a aVar, l lVar, dd ddVar, bf bfVar, com.google.android.gms.internal.by.a aVar2) {
        this.kH = bfVar;
        this.nM = aVar2;
        this.ng = ddVar;
        this.mContext = context;
        this.nO = aVar;
        this.nP = lVar;
    }

    private ab a(cd cdVar) throws a {
        if (this.nR.on == null) {
            throw new a("The ad response must specify one of the supported ad sizes.", 0);
        }
        String[] split = this.nR.on.split("x");
        if (split.length != 2) {
            throw new a("Could not parse the ad size from the ad response: " + this.nR.on, 0);
        }
        try {
            int parseInt = Integer.parseInt(split[0]);
            int parseInt2 = Integer.parseInt(split[1]);
            ab[] abVarArr = cdVar.kQ.lp;
            int length = abVarArr.length;
            for (int i = 0; i < length; i++) {
                ab abVar = abVarArr[i];
                float f = this.mContext.getResources().getDisplayMetrics().density;
                int i2 = abVar.width == -1 ? (int) (((float) abVar.widthPixels) / f) : abVar.width;
                int i3 = abVar.height == -2 ? (int) (((float) abVar.heightPixels) / f) : abVar.height;
                if (parseInt == i2 && parseInt2 == i3) {
                    return new ab(abVar, cdVar.kQ.lp);
                }
            }
            throw new a("The ad size from the ad response was not one of the requested sizes: " + this.nR.on, 0);
        } catch (NumberFormatException e) {
            throw new a("Could not parse the ad size from the ad response: " + this.nR.on, 0);
        }
    }

    private void a(cd cdVar, long j) throws a {
        synchronized (this.nN) {
            this.nT = new aw(this.mContext, cdVar, this.kH, this.mh);
        }
        this.nU = this.nT.a(j, 60000);
        switch (this.nU.mL) {
            case 0:
                return;
            case 1:
                throw new a("No fill from any mediation ad networks.", 3);
            default:
                throw new a("Unexpected mediation result: " + this.nU.mL, 0);
        }
    }

    private void aC() throws a {
        if (this.nR.errorCode != -3) {
            if (TextUtils.isEmpty(this.nR.oi)) {
                throw new a("No fill from ad server.", 3);
            } else if (this.nR.ok) {
                try {
                    this.mh = new ay(this.nR.oi);
                } catch (JSONException e) {
                    throw new a("Could not parse mediation config: " + this.nR.oi, 0);
                }
            }
        }
    }

    private void b(long j) throws a {
        cz.pT.post(new Runnable() {
            /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
                return;
             */
            public void run() {
                synchronized (bz.this.mg) {
                    if (bz.this.nR.errorCode == -2) {
                        bz.this.ng.bb().a((com.google.android.gms.internal.de.a) bz.this);
                        if (bz.this.nR.errorCode == -3) {
                            da.v("Loading URL in WebView: " + bz.this.nR.nw);
                            bz.this.ng.loadUrl(bz.this.nR.nw);
                        } else {
                            da.v("Loading HTML in WebView.");
                            bz.this.ng.loadDataWithBaseURL(cv.p(bz.this.nR.nw), bz.this.nR.oi, "text/html", "UTF-8", null);
                        }
                    }
                }
            }
        });
        d(j);
    }

    private void c(long j) throws a {
        while (e(j)) {
            if (this.nR != null) {
                synchronized (this.nN) {
                    this.nQ = null;
                }
                if (this.nR.errorCode != -2 && this.nR.errorCode != -3) {
                    throw new a("There was a problem getting an ad response. ErrorCode: " + this.nR.errorCode, this.nR.errorCode);
                }
                return;
            }
        }
        throw new a("Timed out waiting for ad response.", 2);
    }

    private void d(long j) throws a {
        while (e(j)) {
            if (this.nS) {
                return;
            }
        }
        throw new a("Timed out waiting for WebView to finish loading.", 2);
    }

    private boolean e(long j) throws a {
        long elapsedRealtime = 60000 - (SystemClock.elapsedRealtime() - j);
        if (elapsedRealtime <= 0) {
            return false;
        }
        try {
            this.mg.wait(elapsedRealtime);
            return true;
        } catch (InterruptedException e) {
            throw new a("Ad request cancelled.", -1);
        }
    }

    public void a(cf cfVar) {
        synchronized (this.mg) {
            da.s("Received ad response.");
            this.nR = cfVar;
            this.mg.notify();
        }
    }

    public void a(dd ddVar) {
        synchronized (this.mg) {
            da.s("WebView finished loading.");
            this.nS = true;
            this.mg.notify();
        }
    }

    public void aB() {
        long j;
        ab abVar;
        synchronized (this.mg) {
            da.s("AdLoaderBackgroundTask started.");
            cd cdVar = new cd(this.nO, this.nP.y().a(this.mContext));
            ab abVar2 = null;
            int i = -2;
            try {
                long elapsedRealtime = SystemClock.elapsedRealtime();
                ct a2 = ca.a(this.mContext, cdVar, this);
                synchronized (this.nN) {
                    this.nQ = a2;
                    if (this.nQ == null) {
                        throw new a("Could not start the ad request service.", 0);
                    }
                }
                c(elapsedRealtime);
                long elapsedRealtime2 = SystemClock.elapsedRealtime();
                aC();
                if (cdVar.kQ.lp != null) {
                    abVar2 = a(cdVar);
                }
                if (this.nR.ok) {
                    a(cdVar, elapsedRealtime);
                } else {
                    b(elapsedRealtime);
                }
                j = elapsedRealtime2;
                abVar = abVar2;
            } catch (a e) {
                i = e.getErrorCode();
                if (i == 3 || i == -1) {
                    da.u(e.getMessage());
                } else {
                    da.w(e.getMessage());
                }
                this.nR = new cf(i);
                cz.pT.post(new Runnable() {
                    public void run() {
                        bz.this.onStop();
                    }
                });
                j = -1;
                abVar = null;
            }
            final cn cnVar = new cn(cdVar.oc, this.ng, this.nR.mt, i, this.nR.mu, this.nR.om, this.nR.orientation, this.nR.mx, cdVar.of, this.nR.ok, this.nU != null ? this.nU.mM : null, this.nU != null ? this.nU.mN : null, this.nU != null ? this.nU.mO : null, this.mh, this.nU != null ? this.nU.mP : null, this.nR.ol, abVar, this.nR.oj, j, this.nR.oo);
            cz.pT.post(new Runnable() {
                public void run() {
                    synchronized (bz.this.mg) {
                        bz.this.nM.a(cnVar);
                    }
                }
            });
        }
    }

    public void onStop() {
        synchronized (this.nN) {
            if (this.nQ != null) {
                this.nQ.cancel();
            }
            this.ng.stopLoading();
            cv.a((WebView) this.ng);
            if (this.nT != null) {
                this.nT.cancel();
            }
        }
    }
}
