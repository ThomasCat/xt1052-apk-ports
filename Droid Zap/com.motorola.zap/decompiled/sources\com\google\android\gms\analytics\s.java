package com.google.android.gms.analytics;

import android.content.Context;
import android.content.Intent;
import com.google.android.gms.analytics.c.C0001c;
import com.google.android.gms.internal.di;
import com.motorola.zap.ZapConstants;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

class s implements ag, com.google.android.gms.analytics.c.b, C0001c {
    private final Context mContext;
    private final GoogleAnalytics rA;
    /* access modifiers changed from: private */
    public final Queue<d> rB;
    private volatile int rC;
    private volatile Timer rD;
    private volatile Timer rE;
    /* access modifiers changed from: private */
    public volatile Timer rF;
    private boolean rG;
    private boolean rH;
    private boolean rI;
    /* access modifiers changed from: private */
    public i rJ;
    /* access modifiers changed from: private */
    public long rK;
    private d rj;
    private final f rk;
    private boolean rm;
    /* access modifiers changed from: private */
    public volatile long rw;
    /* access modifiers changed from: private */
    public volatile a rx;
    private volatile b ry;
    private d rz;

    private enum a {
        CONNECTING,
        CONNECTED_SERVICE,
        CONNECTED_LOCAL,
        BLOCKED,
        PENDING_CONNECTION,
        PENDING_DISCONNECT,
        DISCONNECTED
    }

    private class b extends TimerTask {
        private b() {
        }

        public void run() {
            if (s.this.rx != a.CONNECTED_SERVICE || !s.this.rB.isEmpty() || s.this.rw + s.this.rK >= s.this.rJ.currentTimeMillis()) {
                s.this.rF.schedule(new b(), s.this.rK);
                return;
            }
            aa.v("Disconnecting due to inactivity");
            s.this.aD();
        }
    }

    private class c extends TimerTask {
        private c() {
        }

        public void run() {
            if (s.this.rx == a.CONNECTING) {
                s.this.bL();
            }
        }
    }

    private static class d {
        private final Map<String, String> rV;
        private final long rW;
        private final String rX;
        private final List<di> rY;

        public d(Map<String, String> map, long j, String str, List<di> list) {
            this.rV = map;
            this.rW = j;
            this.rX = str;
            this.rY = list;
        }

        public Map<String, String> bO() {
            return this.rV;
        }

        public long bP() {
            return this.rW;
        }

        public List<di> bQ() {
            return this.rY;
        }

        public String getPath() {
            return this.rX;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("PATH: ");
            sb.append(this.rX);
            if (this.rV != null) {
                sb.append("  PARAMS: ");
                for (Entry entry : this.rV.entrySet()) {
                    sb.append((String) entry.getKey());
                    sb.append("=");
                    sb.append((String) entry.getValue());
                    sb.append(",  ");
                }
            }
            return sb.toString();
        }
    }

    private class e extends TimerTask {
        private e() {
        }

        public void run() {
            s.this.bM();
        }
    }

    s(Context context, f fVar) {
        this(context, fVar, null, GoogleAnalytics.getInstance(context));
    }

    s(Context context, f fVar, d dVar, GoogleAnalytics googleAnalytics) {
        this.rB = new ConcurrentLinkedQueue();
        this.rK = ZapConstants.LOCATION_MAX_AGE_NO_MODALITY_SUPPORT;
        this.rz = dVar;
        this.mContext = context;
        this.rk = fVar;
        this.rA = googleAnalytics;
        this.rJ = new i() {
            public long currentTimeMillis() {
                return System.currentTimeMillis();
            }
        };
        this.rC = 0;
        this.rx = a.DISCONNECTED;
    }

    private Timer a(Timer timer) {
        if (timer != null) {
            timer.cancel();
        }
        return null;
    }

    /* access modifiers changed from: private */
    public synchronized void aD() {
        if (this.ry != null && this.rx == a.CONNECTED_SERVICE) {
            this.rx = a.PENDING_DISCONNECT;
            this.ry.disconnect();
        }
    }

    private void bH() {
        this.rD = a(this.rD);
        this.rE = a(this.rE);
        this.rF = a(this.rF);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003c, code lost:
        if (r7.rB.isEmpty() != false) goto L_0x0077;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003e, code lost:
        r6 = (com.google.android.gms.analytics.s.d) r7.rB.poll();
        com.google.android.gms.analytics.aa.v("Sending hit to store  " + r6);
        r7.rj.a(r6.bO(), r6.bP(), r6.getPath(), r6.bQ());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0079, code lost:
        if (r7.rm == false) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x007b, code lost:
        bK();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d0, code lost:
        r7.rw = r7.rJ.currentTimeMillis();
     */
    public synchronized void bJ() {
        if (Thread.currentThread().equals(this.rk.getThread())) {
            if (this.rG) {
                bk();
            }
            switch (this.rx) {
                case CONNECTED_LOCAL:
                    break;
                case CONNECTED_SERVICE:
                    while (!this.rB.isEmpty()) {
                        d dVar = (d) this.rB.peek();
                        aa.v("Sending hit to service   " + dVar);
                        if (!this.rA.isDryRunEnabled()) {
                            this.ry.a(dVar.bO(), dVar.bP(), dVar.getPath(), dVar.bQ());
                        } else {
                            aa.v("Dry run enabled. Hit not actually sent to service.");
                        }
                        this.rB.poll();
                    }
                    break;
                case DISCONNECTED:
                    aa.v("Need to reconnect");
                    if (!this.rB.isEmpty()) {
                        bM();
                        break;
                    }
                    break;
            }
        } else {
            this.rk.bs().add(new Runnable() {
                public void run() {
                    s.this.bJ();
                }
            });
        }
    }

    private void bK() {
        this.rj.bp();
        this.rm = false;
    }

    /* access modifiers changed from: private */
    public synchronized void bL() {
        if (this.rx != a.CONNECTED_LOCAL) {
            bH();
            aa.v("falling back to local store");
            if (this.rz != null) {
                this.rj = this.rz;
            } else {
                r bB = r.bB();
                bB.a(this.mContext, this.rk);
                this.rj = bB.bE();
            }
            this.rx = a.CONNECTED_LOCAL;
            bJ();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void bM() {
        if (this.rI || this.ry == null || this.rx == a.CONNECTED_LOCAL) {
            aa.w("client not initialized.");
            bL();
        } else {
            try {
                this.rC++;
                a(this.rE);
                this.rx = a.CONNECTING;
                this.rE = new Timer("Failed Connect");
                this.rE.schedule(new c(), 3000);
                aa.v("connecting to Analytics service");
                this.ry.connect();
            } catch (SecurityException e2) {
                aa.w("security exception on connectToService");
                bL();
            }
        }
        return;
    }

    private void bN() {
        this.rD = a(this.rD);
        this.rD = new Timer("Service Reconnect");
        this.rD.schedule(new e(), 5000);
    }

    public synchronized void a(int i, Intent intent) {
        this.rx = a.PENDING_CONNECTION;
        if (this.rC < 2) {
            aa.w("Service unavailable (code=" + i + "), will retry.");
            bN();
        } else {
            aa.w("Service unavailable (code=" + i + "), using local store.");
            bL();
        }
    }

    public void b(Map<String, String> map, long j, String str, List<di> list) {
        aa.v("putHit called");
        this.rB.add(new d(map, j, str, list));
        bJ();
    }

    public void bI() {
        if (this.ry == null) {
            this.ry = new c(this.mContext, this, this);
            bM();
        }
    }

    public void bk() {
        aa.v("clearHits called");
        this.rB.clear();
        switch (this.rx) {
            case CONNECTED_LOCAL:
                this.rj.i(0);
                this.rG = false;
                return;
            case CONNECTED_SERVICE:
                this.ry.bk();
                this.rG = false;
                return;
            default:
                this.rG = true;
                return;
        }
    }

    public void bp() {
        switch (this.rx) {
            case CONNECTED_LOCAL:
                bK();
                return;
            case CONNECTED_SERVICE:
                return;
            default:
                this.rm = true;
                return;
        }
    }

    public synchronized void br() {
        if (!this.rI) {
            aa.v("setForceLocalDispatch called.");
            this.rI = true;
            switch (this.rx) {
                case CONNECTED_LOCAL:
                case PENDING_CONNECTION:
                case PENDING_DISCONNECT:
                case DISCONNECTED:
                    break;
                case CONNECTED_SERVICE:
                    aD();
                    break;
                case CONNECTING:
                    this.rH = true;
                    break;
            }
        }
    }

    public synchronized void onConnected() {
        this.rE = a(this.rE);
        this.rC = 0;
        aa.v("Connected to service");
        this.rx = a.CONNECTED_SERVICE;
        if (this.rH) {
            aD();
            this.rH = false;
        } else {
            bJ();
            this.rF = a(this.rF);
            this.rF = new Timer("disconnect check");
            this.rF.schedule(new b(), this.rK);
        }
    }

    public synchronized void onDisconnected() {
        if (this.rx == a.PENDING_DISCONNECT) {
            aa.v("Disconnected from service");
            bH();
            this.rx = a.DISCONNECTED;
        } else {
            aa.v("Unexpected disconnect.");
            this.rx = a.PENDING_CONNECTION;
            if (this.rC < 2) {
                bN();
            } else {
                bL();
            }
        }
    }
}
