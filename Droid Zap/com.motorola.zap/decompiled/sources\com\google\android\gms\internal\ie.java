package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;

public class ie implements Creator<id> {
    static void a(id idVar, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.a(parcel, 1, idVar.OG, false);
        b.c(parcel, 1000, idVar.versionCode);
        b.a(parcel, 2, idVar.OH, false);
        b.D(parcel, p);
    }

    /* renamed from: aD */
    public id createFromParcel(Parcel parcel) {
        String str = null;
        int o = a.o(parcel);
        int i = 0;
        String str2 = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    str2 = a.m(parcel, n);
                    break;
                case 2:
                    str = a.m(parcel, n);
                    break;
                case 1000:
                    i = a.g(parcel, n);
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new id(i, str2, str);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }

    /* renamed from: by */
    public id[] newArray(int i) {
        return new id[i];
    }
}
