package com.motorola.sharing;

import android.database.Cursor;
import android.os.AsyncTask;
import android.text.TextUtils;
import com.motorola.sharing.ContentContainer.DiscoveryParams;
import com.motorola.sharing.ContentContainer.Type;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.ContentShareListener.ContentCancelShareEvent;
import com.motorola.sharing.ContentShareListener.ContentCancelShareEvent.Substatus;
import com.motorola.sharing.ContentShareListener.ContentShareEvent;
import com.motorola.sharing.ContentShareListener.ContentStopShareEvent;
import com.motorola.sharing.util.ContainerExpiryTracker;
import com.motorola.sharing.util.ContainerExpiryTracker.ExpiryListener;
import com.motorola.sharing.util.ContainerExpiryTracker.ExpiryTrigger;
import com.motorola.sharing.util.DbUtils;
import com.motorola.sharing.util.HistoryManager;
import com.motorola.sharing.util.Logger;
import java.util.HashSet;
import java.util.Set;

public class ContentDispatcher {
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getGcspTag("ContentDispatcher");
    private DispatchRule mDefaultRule;
    /* access modifiers changed from: private */
    public ContainerExpiryTracker mExpiryTracker;
    private boolean mSharingResumeInitiated;

    private class ContainerExpiryListener implements ExpiryListener {
        private ContainerExpiryListener() {
        }

        public void onContainerExpired(ContentContainer container, ExpiryTrigger trigger) {
            Endpoint[] endpoints = container.dispatchRule.getStaticEndpoints();
            Endpoint source = null;
            if (endpoints != null && endpoints.length > 0) {
                source = endpoints[0];
            }
            container.dispatchRule.cancelSharing(container);
            switch (trigger) {
                case BY_TIME:
                    container.onContentCancelShareEvent(Status.SUCCESS, Substatus.EXPIRED, source);
                    return;
                case BY_GROUP_TERMINATION:
                    container.onContentCancelShareEvent(Status.SUCCESS, Substatus.GROUP_ENDED, source);
                    return;
                default:
                    throw new IllegalStateException("unsupported expiry trigger: " + trigger);
            }
        }
    }

    private class ContentShareInterceptListener implements ContentShareListener {
        private ContentShareListener mOriginalListener = null;

        ContentShareInterceptListener(ContentShareListener listener) {
            this.mOriginalListener = listener;
        }

        public void onContentShareEvent(ContentShareEvent event) {
            switch (event.status) {
                case SUCCESS:
                    if ((event.substatus == ContentShareEvent.Substatus.SHARED || event.substatus == ContentShareEvent.Substatus.RESUMED) && event.container.expiryTriggers != null) {
                        ContentDispatcher.this.mExpiryTracker.add(event.container);
                        break;
                    }
                case ERROR:
                    if (event.container.expiryTriggers != null) {
                        HistoryManager.getInstance().removeShare(event.container);
                        break;
                    }
                    break;
            }
            if (this.mOriginalListener != null) {
                this.mOriginalListener.onContentShareEvent(event);
            }
        }

        public void onContentStopShareEvent(ContentStopShareEvent event) {
            switch (event.status) {
                case SUCCESS:
                    if (event.substatus == ContentStopShareEvent.Substatus.NOT_SHARED) {
                        if (event.container == null) {
                            ContentDispatcher.this.mExpiryTracker.remove(event.shareId);
                            break;
                        } else {
                            ContentDispatcher.this.mExpiryTracker.remove(event.container);
                            break;
                        }
                    }
                    break;
            }
            if (this.mOriginalListener != null) {
                this.mOriginalListener.onContentStopShareEvent(event);
            }
        }

        public void onContentCancelShareEvent(ContentCancelShareEvent event) {
            if (this.mOriginalListener != null) {
                this.mOriginalListener.onContentCancelShareEvent(event);
            }
        }
    }

    ContentDispatcher() {
        this.mDefaultRule = null;
        this.mExpiryTracker = null;
        this.mSharingResumeInitiated = false;
        this.mExpiryTracker = new ContainerExpiryTracker(new ContainerExpiryListener());
        resumeSharing();
    }

    public void setDefaultRule(DispatchRule defaultRule) {
        this.mDefaultRule = defaultRule;
    }

    public void discoverContent(DiscoveryParams params, ContentDiscoveryListener listener, Endpoint[] endpoints, DispatchRule dispatchRule) {
        if (params == null) {
            throw new IllegalArgumentException("params is null");
        } else if (params.type != Type.RECEIVE) {
            throw new IllegalArgumentException("params has wrong type");
        } else if (!params.canBeReused) {
            throw new IllegalArgumentException("params can not be reused");
        } else {
            params.canBeReused = false;
            ensureDispatchRule(params, dispatchRule);
            params.dispatchRule.discoverContent(params, listener, endpoints);
        }
    }

    public void discoverContent(DiscoveryParams params, ContentDiscoveryListener listener, Endpoint[] endpoints) {
        discoverContent(params, listener, endpoints, null);
    }

    public void discoverContent(DiscoveryParams params, ContentDiscoveryListener listener) {
        discoverContent(params, listener, null, null);
    }

    public void stopDiscovery(DiscoveryParams params) {
        if (params == null) {
            throw new IllegalArgumentException("params is null");
        } else if (params.type != Type.RECEIVE) {
            throw new IllegalArgumentException("params has wrong type: " + params.type);
        } else {
            ensureDispatchRule(params, null);
            params.dispatchRule.stopDiscovery(params);
        }
    }

    public void downloadContent(ContentContainer container, ContentDownloadListener listener) {
        if (container == null) {
            throw new IllegalArgumentException("container is null");
        } else if (container.type != Type.RECEIVE) {
            throw new IllegalArgumentException("container has wrong type: " + container.type);
        } else if (!container.canBeReused) {
            throw new IllegalArgumentException("container can not be reused");
        } else {
            container.canBeReused = false;
            ensureDispatchRule(container, null);
            container.dispatchRule.downloadContent(container, listener);
        }
    }

    public void cancelDownload(ContentContainer container) {
        if (container == null) {
            throw new IllegalArgumentException("container is null");
        } else if (container.type != Type.RECEIVE) {
            throw new IllegalArgumentException("container has wrong type: " + container.type);
        } else {
            ensureDispatchRule(container, null);
            container.dispatchRule.cancelDownload(container);
        }
    }

    public void reportAbuse(ContentContainer container, ReportAbuseListener listener, DispatchRule dispatchRule) {
        if (container == null) {
            throw new IllegalArgumentException("container is null");
        } else if (container.type != Type.REPORT_ABUSE) {
            throw new IllegalArgumentException("container has wrong type: " + container.type);
        } else if (!container.canBeReused) {
            throw new IllegalArgumentException("container can not be reused");
        } else {
            container.canBeReused = false;
            ensureDispatchRule(container, dispatchRule);
            container.dispatchRule.reportAbuse(container, listener);
        }
    }

    public void reportAbuse(ContentContainer container, ReportAbuseListener listener) {
        reportAbuse(container, listener, null);
    }

    public void shareContent(ContentContainer container, ContentShareListener listener, Endpoint[] endpoints, DispatchRule dispatchRule) {
        if (container == null) {
            throw new IllegalArgumentException("container is null");
        } else if (container.type != Type.SHARE) {
            throw new IllegalArgumentException("container has wrong type: " + container.type);
        } else if (!container.canBeReused) {
            throw new IllegalArgumentException("container can not be reused");
        } else {
            container.canBeReused = false;
            ensureDispatchRule(container, dispatchRule);
            container.dispatchRule.shareContent(container, getContentShareInterceptListener(listener), endpoints);
        }
    }

    public void shareContent(ContentContainer container, ContentShareListener listener, Endpoint[] endpoints) {
        shareContent(container, getContentShareInterceptListener(listener), endpoints, null);
    }

    public void stopSharing(ContentContainer container) {
        if (container == null) {
            throw new IllegalArgumentException("container is null");
        } else if (container.type != Type.SHARE) {
            throw new IllegalArgumentException("container has wrong type: " + container.type);
        } else {
            ensureDispatchRule(container, null);
            container.dispatchRule.stopSharing(container);
        }
    }

    public void stopSharing(String shareId, ContentShareListener listener, DispatchRule dispatchRule) {
        if (TextUtils.isEmpty(shareId)) {
            throw new IllegalArgumentException("shareId is null");
        } else if (dispatchRule != null) {
            dispatchRule.stopSharing(shareId, getContentShareInterceptListener(listener));
        } else {
            getDefaultRule().stopSharing(shareId, getContentShareInterceptListener(listener));
        }
    }

    public void stopSharing(String shareId, ContentShareListener listener) {
        stopSharing(shareId, getContentShareInterceptListener(listener), null);
    }

    private synchronized void resumeSharing() {
        if (!this.mSharingResumeInitiated) {
            this.mSharingResumeInitiated = true;
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                public Void doInBackground(Void... params) {
                    Cursor c = null;
                    try {
                        Cursor c2 = HistoryManager.getInstance().getShares();
                        if (c2 != null) {
                            Set<String> badRecords = new HashSet<>();
                            while (c2.moveToNext()) {
                                ContentContainer container = ContentContainer.restoreSharedContainer(c2);
                                if (container == null) {
                                    String shareId = DbUtils.getString(c2, "shareId");
                                    if (!TextUtils.isEmpty(shareId)) {
                                        badRecords.add(shareId);
                                    }
                                    Logger.d(ContentDispatcher.TAG, "unable to resume sharing sid=" + shareId);
                                } else if (ContentDispatcher.this.mExpiryTracker.isExpired(container)) {
                                    badRecords.add(container.shareId);
                                    Logger.d(ContentDispatcher.TAG, "sharing already expired sid=" + container.shareId);
                                } else {
                                    container.dispatchRule.resumeSharing(container, ContentDispatcher.this.getContentShareInterceptListener(null));
                                }
                            }
                            if (badRecords.size() > 0) {
                                String[] shareIds = new String[badRecords.size()];
                                badRecords.toArray(shareIds);
                                HistoryManager.getInstance().removeShares(shareIds);
                            }
                            if (c2 != null) {
                                c2.close();
                            }
                        } else if (c2 != null) {
                            c2.close();
                        }
                    } catch (Exception e) {
                        Logger.e(ContentDispatcher.TAG, "got exception: " + e);
                        if (c != null) {
                            c.close();
                        }
                    } catch (Throwable th) {
                        if (c != null) {
                            c.close();
                        }
                        throw th;
                    }
                    return null;
                }
            }.execute(new Void[0]);
        }
    }

    /* access modifiers changed from: private */
    public ContentShareListener getContentShareInterceptListener(ContentShareListener clientListener) {
        return clientListener instanceof ContentShareInterceptListener ? clientListener : new ContentShareInterceptListener(clientListener);
    }

    private DispatchRule getDefaultRule() {
        if (this.mDefaultRule != null) {
            return this.mDefaultRule;
        }
        throw new IllegalStateException("DispatchRule hasn't been set");
    }

    private void ensureDispatchRule(ContentContainer container, DispatchRule dispatchRule) {
        if (container.dispatchRule == null) {
            if (dispatchRule == null) {
                dispatchRule = getDefaultRule();
            }
            container.dispatchRule = dispatchRule;
        } else if (dispatchRule != null) {
            throw new IllegalArgumentException("container has a rule already");
        }
    }
}
