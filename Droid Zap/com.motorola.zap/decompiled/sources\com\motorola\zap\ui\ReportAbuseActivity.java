package com.motorola.zap.ui;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import com.motorola.sharing.Abuse.AbuseType;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.app.ActionReceiver;
import com.motorola.zap.provider.HistoryHandler;
import java.io.File;

public class ReportAbuseActivity extends Activity {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private final HistoryHandler mHistoryHandler = new HistoryHandler();

    public void onCreate(Bundle savedInstanceState) {
        if (DBG) {
            Log.d("MOTGS-ReportAbuseActvt", "Activity - onCreate");
        }
        super.onCreate(savedInstanceState);
        if (isAlreadyReported()) {
            handleAlreadyReported();
        } else if (isDeleted()) {
            handleDeleted();
        } else {
            setContentView(R.layout.report_abuse);
        }
    }

    private boolean isAlreadyReported() {
        return this.mHistoryHandler.isAbuseReported(getIntent().getStringExtra(ZapConstants.EXTRA_REPORT_FILE_PATH));
    }

    private void handleAlreadyReported() {
        ZapUi.getInstance().showToastMessage(getString(R.string.toast_msg_report_sent), ZapUi.getZapToastIconId(), true, true);
        finish();
    }

    private boolean isDeleted() {
        return new File(getIntent().getStringExtra(ZapConstants.EXTRA_REPORT_FILE_PATH)).length() == 0;
    }

    private void handleDeleted() {
        ZapUi.getInstance().showToastMessage(getString(R.string.toast_msg_media_deleted), ZapUi.getZapToastIconId(), true, true);
        finish();
    }

    public void onResume() {
        if (DBG) {
            Log.d("MOTGS-ReportAbuseActvt", "Activity - onResume");
        }
        super.onResume();
    }

    public void onPause() {
        if (DBG) {
            Log.d("MOTGS-ReportAbuseActvt", "Activity - onPause");
        }
        super.onPause();
    }

    public void onDestroy() {
        if (DBG) {
            Log.d("MOTGS-ReportAbuseActvt", "Activity - onDestroy");
        }
        super.onDestroy();
    }

    public void onCancelButtonClicked(View view) {
        if (DBG) {
            Log.d("MOTGS-ReportAbuseActvt", "Activity - onCancelButtonClicked");
        }
        finish();
    }

    public void onSubmitButtonClicked(View view) {
        if (DBG) {
            Log.d("MOTGS-ReportAbuseActvt", "Activity - onSubmitButtonClicked");
        }
        int checkedRadioButtonId = ((RadioGroup) findViewById(R.id.report_abuse_types_radio_group)).getCheckedRadioButtonId();
        AbuseType abuseType = AbuseType.UNKNOWN;
        switch (checkedRadioButtonId) {
            case R.id.report_abuse_type_spam /*2131296431*/:
                abuseType = AbuseType.SPAM;
                break;
            case R.id.report_abuse_type_pornography /*2131296432*/:
                abuseType = AbuseType.PORNOGRAPHY;
                break;
            case R.id.report_abuse_type_violence /*2131296433*/:
                abuseType = AbuseType.VIOLENCE;
                break;
            case R.id.report_abuse_type_harassment /*2131296434*/:
                abuseType = AbuseType.HARASSMENT;
                break;
            case R.id.report_abuse_type_copyrighted /*2131296435*/:
                abuseType = AbuseType.COPYRIGHTED;
                break;
        }
        if (DBG) {
            Log.d("MOTGS-ReportAbuseActvt", "Activity - onSubmitButtonClicked: id=" + checkedRadioButtonId + " abuseType=" + abuseType);
        }
        if (abuseType != AbuseType.UNKNOWN) {
            Intent intent = new Intent(ZapConstants.ACTION_REPORT_ABUSE);
            intent.setClass(getApplicationContext(), ActionReceiver.class);
            intent.putExtras(getIntent());
            intent.putExtra(ZapConstants.EXTRA_ABUSE_TYPE, abuseType.ordinal());
            sendBroadcast(intent);
        }
        finish();
    }

    public void onLearnMoreClicked(View v) {
        if (v != null && v.getId() == R.id.report_abuse_learn_more) {
            Builder alert = new Builder(this);
            alert.setTitle(R.string.report_abuse_dialog_title);
            alert.setMessage(R.string.report_abuse_dialog_summary);
            alert.setCancelable(true);
            alert.setPositiveButton(17039370, new OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                }
            });
            alert.create();
            alert.show();
        }
    }
}
