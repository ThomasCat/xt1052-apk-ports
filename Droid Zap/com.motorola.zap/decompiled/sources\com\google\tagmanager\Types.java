package com.google.tagmanager;

import com.google.analytics.midtier.proto.containertag.TypeSystem.Value;
import com.motorola.zap.ZapConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class Types {
    private static Boolean DEFAULT_BOOLEAN = new Boolean(false);
    private static Double DEFAULT_DOUBLE = new Double(0.0d);
    private static Long DEFAULT_INT64 = new Long(0);
    private static List<Object> DEFAULT_LIST = new ArrayList(0);
    private static Map<Object, Object> DEFAULT_MAP = new HashMap();
    private static TypedNumber DEFAULT_NUMBER = TypedNumber.numberWithInt64(0);
    private static final Object DEFAULT_OBJECT = null;
    private static String DEFAULT_STRING = new String(ZapConstants.GOOGLE_ACCOUNT_NONE);
    private static Value DEFAULT_VALUE = objectToValue(DEFAULT_STRING);

    private Types() {
    }

    public static Object getDefaultObject() {
        return DEFAULT_OBJECT;
    }

    public static Long getDefaultInt64() {
        return DEFAULT_INT64;
    }

    public static Double getDefaultDouble() {
        return DEFAULT_DOUBLE;
    }

    public static Boolean getDefaultBoolean() {
        return DEFAULT_BOOLEAN;
    }

    public static TypedNumber getDefaultNumber() {
        return DEFAULT_NUMBER;
    }

    public static String getDefaultString() {
        return DEFAULT_STRING;
    }

    public static Value getDefaultValue() {
        return DEFAULT_VALUE;
    }

    public static String objectToString(Object o) {
        return o == null ? DEFAULT_STRING : o.toString();
    }

    public static TypedNumber objectToNumber(Object o) {
        if (o instanceof TypedNumber) {
            return (TypedNumber) o;
        }
        if (isInt64ableNumber(o)) {
            return TypedNumber.numberWithInt64(getInt64(o));
        }
        if (isDoubleableNumber(o)) {
            return TypedNumber.numberWithDouble(Double.valueOf(getDouble(o)));
        }
        return parseNumber(objectToString(o));
    }

    public static Long objectToInt64(Object o) {
        return isInt64ableNumber(o) ? Long.valueOf(getInt64(o)) : parseInt64(objectToString(o));
    }

    public static Double objectToDouble(Object o) {
        return isDoubleableNumber(o) ? Double.valueOf(getDouble(o)) : parseDouble(objectToString(o));
    }

    public static Boolean objectToBoolean(Object o) {
        return o instanceof Boolean ? (Boolean) o : parseBoolean(objectToString(o));
    }

    public static String valueToString(Value v) {
        return objectToString(valueToObject(v));
    }

    public static TypedNumber valueToNumber(Value v) {
        return objectToNumber(valueToObject(v));
    }

    public static Long valueToInt64(Value v) {
        return objectToInt64(valueToObject(v));
    }

    public static Double valueToDouble(Value v) {
        return objectToDouble(valueToObject(v));
    }

    public static Boolean valueToBoolean(Value v) {
        return objectToBoolean(valueToObject(v));
    }

    public static Value objectToValue(Object obj) {
        boolean z;
        boolean z2;
        boolean z3 = false;
        Value value = new Value();
        if (obj instanceof Value) {
            return (Value) obj;
        }
        if (obj instanceof String) {
            value.type = 1;
            value.string = (String) obj;
        } else if (obj instanceof List) {
            value.type = 2;
            List<Object> list = (List) obj;
            ArrayList arrayList = new ArrayList(list.size());
            boolean z4 = false;
            for (Object objectToValue : list) {
                Value objectToValue2 = objectToValue(objectToValue);
                if (objectToValue2 == DEFAULT_VALUE) {
                    return DEFAULT_VALUE;
                }
                if (z4 || objectToValue2.containsReferences) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                arrayList.add(objectToValue2);
                z4 = z2;
            }
            value.listItem = (Value[]) arrayList.toArray(new Value[0]);
            z3 = z4;
        } else if (obj instanceof Map) {
            value.type = 3;
            Set<Entry> entrySet = ((Map) obj).entrySet();
            ArrayList arrayList2 = new ArrayList(entrySet.size());
            ArrayList arrayList3 = new ArrayList(entrySet.size());
            boolean z5 = false;
            for (Entry entry : entrySet) {
                Value objectToValue3 = objectToValue(entry.getKey());
                Value objectToValue4 = objectToValue(entry.getValue());
                if (objectToValue3 == DEFAULT_VALUE || objectToValue4 == DEFAULT_VALUE) {
                    return DEFAULT_VALUE;
                }
                if (z5 || objectToValue3.containsReferences || objectToValue4.containsReferences) {
                    z = true;
                } else {
                    z = false;
                }
                arrayList2.add(objectToValue3);
                arrayList3.add(objectToValue4);
                z5 = z;
            }
            value.mapKey = (Value[]) arrayList2.toArray(new Value[0]);
            value.mapValue = (Value[]) arrayList3.toArray(new Value[0]);
            z3 = z5;
        } else if (isDoubleableNumber(obj)) {
            value.type = 1;
            value.string = obj.toString();
        } else if (isInt64ableNumber(obj)) {
            value.type = 6;
            value.integer = getInt64(obj);
        } else if (obj instanceof Boolean) {
            value.type = 8;
            value.boolean_ = ((Boolean) obj).booleanValue();
        } else {
            Log.e("Converting to Value from unknown object type: " + (obj == null ? "null" : obj.getClass().toString()));
            return DEFAULT_VALUE;
        }
        value.containsReferences = z3;
        return value;
    }

    public static Value functionIdToValue(String id) {
        Value value = new Value();
        value.type = 5;
        value.functionId = id;
        return value;
    }

    private static boolean isDoubleableNumber(Object o) {
        return (o instanceof Double) || (o instanceof Float) || ((o instanceof TypedNumber) && ((TypedNumber) o).isDouble());
    }

    private static double getDouble(Object obj) {
        if (obj instanceof Number) {
            return ((Number) obj).doubleValue();
        }
        Log.e("getDouble received non-Number");
        return 0.0d;
    }

    private static boolean isInt64ableNumber(Object o) {
        return (o instanceof Byte) || (o instanceof Short) || (o instanceof Integer) || (o instanceof Long) || ((o instanceof TypedNumber) && ((TypedNumber) o).isInt64());
    }

    private static long getInt64(Object obj) {
        if (obj instanceof Number) {
            return ((Number) obj).longValue();
        }
        Log.e("getInt64 received non-Number");
        return 0;
    }

    private static TypedNumber parseNumber(String str) {
        try {
            return TypedNumber.numberWithString(str);
        } catch (NumberFormatException e) {
            Log.e("Failed to convert '" + str + "' to a number.");
            return DEFAULT_NUMBER;
        }
    }

    private static Long parseInt64(String s) {
        TypedNumber result = parseNumber(s);
        return result == DEFAULT_NUMBER ? DEFAULT_INT64 : Long.valueOf(result.longValue());
    }

    private static Double parseDouble(String s) {
        TypedNumber result = parseNumber(s);
        return result == DEFAULT_NUMBER ? DEFAULT_DOUBLE : Double.valueOf(result.doubleValue());
    }

    private static Boolean parseBoolean(String str) {
        if ("true".equalsIgnoreCase(str)) {
            return Boolean.TRUE;
        }
        if ("false".equalsIgnoreCase(str)) {
            return Boolean.FALSE;
        }
        return DEFAULT_BOOLEAN;
    }

    public static Object valueToObject(Value value) {
        int i = 0;
        if (value == null) {
            return DEFAULT_OBJECT;
        }
        switch (value.type) {
            case 1:
                return value.string;
            case 2:
                ArrayList arrayList = new ArrayList(value.listItem.length);
                Value[] valueArr = value.listItem;
                int length = valueArr.length;
                while (i < length) {
                    Object valueToObject = valueToObject(valueArr[i]);
                    if (valueToObject == DEFAULT_OBJECT) {
                        return DEFAULT_OBJECT;
                    }
                    arrayList.add(valueToObject);
                    i++;
                }
                return arrayList;
            case 3:
                if (value.mapKey.length != value.mapValue.length) {
                    Log.e("Converting an invalid value to object: " + value.toString());
                    return DEFAULT_OBJECT;
                }
                LinkedHashMap linkedHashMap = new LinkedHashMap(value.mapValue.length);
                while (i < value.mapKey.length) {
                    Object valueToObject2 = valueToObject(value.mapKey[i]);
                    Object valueToObject3 = valueToObject(value.mapValue[i]);
                    if (valueToObject2 == DEFAULT_OBJECT || valueToObject3 == DEFAULT_OBJECT) {
                        return DEFAULT_OBJECT;
                    }
                    linkedHashMap.put(valueToObject2, valueToObject3);
                    i++;
                }
                return linkedHashMap;
            case 4:
                Log.e("Trying to convert a macro reference to object");
                return DEFAULT_OBJECT;
            case 5:
                Log.e("Trying to convert a function id to object");
                return DEFAULT_OBJECT;
            case 6:
                return Long.valueOf(value.integer);
            case 7:
                StringBuffer stringBuffer = new StringBuffer();
                Value[] valueArr2 = value.templateToken;
                int length2 = valueArr2.length;
                while (i < length2) {
                    String valueToString = valueToString(valueArr2[i]);
                    if (valueToString == DEFAULT_STRING) {
                        return DEFAULT_OBJECT;
                    }
                    stringBuffer.append(valueToString);
                    i++;
                }
                return stringBuffer.toString();
            case 8:
                return Boolean.valueOf(value.boolean_);
            default:
                Log.e("Failed to convert a value of type: " + value.type);
                return DEFAULT_OBJECT;
        }
    }
}
