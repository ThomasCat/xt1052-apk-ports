package com.motorola.sharing.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.GCSP;
import com.motorola.zap.provider.HistoryHandler;

public class HistoryManager {
    private static final String TAG = Logger.getGcspTag("HistoryManager");
    private static HistoryManager sInstance;
    private DbHelper mDbHelper = null;

    private class DbHelper extends SQLiteOpenHelper {
        private SQLiteDatabase mDb;

        private DbHelper(Context context) {
            super(context, "gcsp_history.db", null, 1);
        }

        public void onCreate(SQLiteDatabase db) {
            StringBuffer b = new StringBuffer();
            b.append(String.format("CREATE TABLE %s (", new Object[]{Downloads.NAME}));
            b.append(String.format("%s INTEGER PRIMARY KEY AUTOINCREMENT,", new Object[]{HistoryHandler.HIST_ID}));
            b.append(String.format("%s TEXT NOT NULL,", new Object[]{C.STATE}));
            b.append(String.format("%s TEXT NOT NULL UNIQUE,", new Object[]{"shareId"}));
            b.append(String.format("%s INTEGER NOT NULL)", new Object[]{C.TS}));
            db.execSQL(b.toString());
            StringBuffer b2 = new StringBuffer();
            b2.append(String.format("CREATE TABLE %s (", new Object[]{Shares.NAME}));
            b2.append(String.format("%s INTEGER PRIMARY KEY AUTOINCREMENT,", new Object[]{HistoryHandler.HIST_ID}));
            b2.append(String.format("%s INTEGER NOT NULL UNIQUE,", new Object[]{C.UID}));
            b2.append(String.format("%s INTEGER,", new Object[]{C.CREATED_TIME}));
            b2.append(String.format("%s TEXT NOT NULL UNIQUE,", new Object[]{"shareId"}));
            b2.append(String.format("%s TEXT,", new Object[]{"groupId"}));
            b2.append(String.format("%s TEXT,", new Object[]{C.GROUP_OWNER}));
            b2.append(String.format("%s INTEGER,", new Object[]{C.SHARED_TIME}));
            b2.append(String.format("%s INTEGER,", new Object[]{C.TTL}));
            b2.append(String.format("%s TEXT,", new Object[]{C.ACCOUNT}));
            b2.append(String.format("%s TEXT,", new Object[]{C.PATH}));
            b2.append(String.format("%s INTEGER,", new Object[]{C.SIZE}));
            b2.append(String.format("%s TEXT,", new Object[]{C.MIME_TYPE}));
            b2.append(String.format("%s TEXT,", new Object[]{C.PIN}));
            b2.append(String.format("%s TEXT,", new Object[]{C.P2P_URL}));
            b2.append(String.format("%s TEXT,", new Object[]{C.P2P_NAME}));
            b2.append(String.format("%s TEXT,", new Object[]{C.P2P_PASSWORD}));
            b2.append(String.format("%s TEXT,", new Object[]{C.UPLOAD_RULE}));
            b2.append(String.format("%s BLOB,", new Object[]{C.EXPIRY_TRIGGERS}));
            b2.append(String.format("%s BLOB)", new Object[]{C.DISPATCH_RULE}));
            db.execSQL(b2.toString());
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }

        public synchronized SQLiteDatabase getDb() {
            if (this.mDb == null) {
                this.mDb = getWritableDatabase();
                HistoryManager.this.cleanup(this.mDb);
            }
            return this.mDb;
        }
    }

    public static class Downloads {
        public static final String NAME = "downloads";

        public static class C {
            public static final String SHARE_ID = "shareId";
            public static final String STATE = "state";
            public static final String TS = "ts";
        }

        public enum State {
            DOWNLOADING,
            CANCELED,
            FAILED,
            COMPLETED
        }
    }

    public static class Shares {
        public static final String NAME = "shares";

        public static class C {
            public static final String ACCOUNT = "account";
            public static final String CREATED_TIME = "createTime";
            public static final String DISPATCH_RULE = "dispatchRule";
            public static final String EXPIRY_TRIGGERS = "expiryTriggers";
            public static final String GROUP_ID = "groupId";
            public static final String GROUP_OWNER = "groupOwner";
            public static final String MIME_TYPE = "mimeType";
            public static final String P2P_NAME = "p2pName";
            public static final String P2P_PASSWORD = "p2pPassword";
            public static final String P2P_URL = "p2pUrl";
            public static final String PATH = "path";
            public static final String PIN = "pin";
            public static final String SHARED_TIME = "sharedTime";
            public static final String SHARE_ID = "shareId";
            public static final String SIZE = "size";
            public static final String TTL = "ttl";
            public static final String UID = "uid";
            public static final String UPLOAD_RULE = "uploadRule";
        }
    }

    public static synchronized HistoryManager getInstance() {
        HistoryManager historyManager;
        synchronized (HistoryManager.class) {
            if (sInstance == null) {
                sInstance = new HistoryManager(GCSP.getContext());
            }
            historyManager = sInstance;
        }
        return historyManager;
    }

    private HistoryManager(Context context) {
        this.mDbHelper = new DbHelper(context);
    }

    private SQLiteDatabase getDb() {
        return this.mDbHelper.getDb();
    }

    /* access modifiers changed from: private */
    public void cleanup(SQLiteDatabase db) {
        db.delete(Downloads.NAME, String.format("%s = ?", new Object[]{C.STATE}), new String[]{State.DOWNLOADING.toString()});
    }

    public void addDownload(ContentContainer container) {
        ContentValues val = new ContentValues();
        val.put(C.STATE, State.DOWNLOADING.toString());
        val.put("shareId", container.shareId);
        val.put(C.TS, Long.valueOf(System.currentTimeMillis()));
        getDb().insert(Downloads.NAME, null, val);
    }

    public void removeDownload(ContentContainer container) {
        removeDownload(container.shareId);
    }

    public void removeDownload(String shareId) {
        getDb().delete(Downloads.NAME, String.format("%s = ?", new Object[]{"shareId"}), new String[]{shareId});
    }

    public void removeDownloads(String[] shareIds) {
        StringBuffer b = new StringBuffer();
        b.append(String.format("DELETE FROM %s WHERE %s IN (", new Object[]{Downloads.NAME, "shareId"}));
        for (int i = 0; i < shareIds.length; i++) {
            b.append("?");
            if (i < shareIds.length - 1) {
                b.append(",");
            }
        }
        b.append(")");
        getDb().execSQL(b.toString(), shareIds);
    }

    public boolean downloadExists(String shareId) {
        if (DatabaseUtils.queryNumEntries(getDb(), Downloads.NAME, String.format("%s = ?", new Object[]{"shareId"}), new String[]{shareId}) > 0) {
            return true;
        }
        return false;
    }

    public Cursor getDownloads() {
        return getDb().query(Downloads.NAME, new String[]{"shareId"}, null, null, null, null, null, "100");
    }

    public void setDownloadState(ContentContainer container, State state) {
        ContentValues val = new ContentValues();
        val.put(C.STATE, state.toString());
        getDb().update(Downloads.NAME, val, String.format("%s = ?", new Object[]{"shareId"}), new String[]{container.shareId});
    }

    public void addShare(ContentContainer container) {
        ContentValues val = container.getStateIfShared();
        if (val == null) {
            Logger.w(TAG, "unable to save sharing state for " + container.getPrintableSignature());
        } else {
            getDb().insert(Shares.NAME, null, val);
        }
    }

    public void removeShare(ContentContainer container) {
        removeShare(container.shareId);
    }

    public void removeShare(String shareId) {
        getDb().delete(Shares.NAME, String.format("%s = ?", new Object[]{"shareId"}), new String[]{shareId});
    }

    public void removeShares(String[] shareIds) {
        StringBuffer b = new StringBuffer();
        b.append(String.format("DELETE FROM %s WHERE %s IN (", new Object[]{Shares.NAME, "shareId"}));
        for (int i = 0; i < shareIds.length; i++) {
            b.append("?");
            if (i < shareIds.length - 1) {
                b.append(",");
            }
        }
        b.append(")");
        getDb().execSQL(b.toString(), shareIds);
    }

    public boolean shareExists(ContentContainer container) {
        return shareExists(container.shareId);
    }

    public boolean shareExists(String shareId) {
        if (DatabaseUtils.queryNumEntries(getDb(), Shares.NAME, String.format("%s = ?", new Object[]{"shareId"}), new String[]{shareId}) > 0) {
            return true;
        }
        return false;
    }

    public Cursor getShares() {
        return getDb().query(Shares.NAME, null, null, null, null, null, null);
    }

    public Cursor getPinProtectedShares() {
        return getDb().query(Shares.NAME, new String[]{"shareId"}, String.format("%s IS NOT NULL", new Object[]{C.PIN}), null, null, null, null);
    }
}
