package com.motorola.sharing.instrumentation;

import com.motorola.sharing.GCSP;
import com.motorola.sharing.util.NetworkUtils;

public class Operation {
    public ActionResult actionResult = new ActionResult();
    public long creationTime = 0;
    public String networkType = null;
    public Type type;

    public enum Type {
        SETTINGS_SYNC("Sync");
        
        private String humanReadable;

        private Type(String humanReadable2) {
            this.humanReadable = humanReadable2;
        }

        public String humanReadable() {
            return this.humanReadable;
        }
    }

    public Operation(Type type2) {
        if (type2 == null) {
            throw new IllegalArgumentException("type is null");
        }
        this.type = type2;
        if (GCSP.getContext() != null) {
            this.networkType = NetworkUtils.getActiveNetworkType(GCSP.getContext());
        }
    }
}
