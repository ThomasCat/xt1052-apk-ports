package com.motorola.zap.app;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.SystemClock;
import android.util.Log;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.util.NetworkUtils;
import com.motorola.zap.LogUtils;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConfig;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.location.BlastLocationProvider;
import com.motorola.zap.location.BlastLocationProvider.LocationResult;
import com.motorola.zap.location.ModalityHelper;
import com.motorola.zap.ui.ZapUi;
import com.motorola.zap.ui.ZapUi.LocationEvent;
import java.util.concurrent.TimeUnit;

public class LocationHandler {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private static LocationHandler sInstance;
    private boolean mIsWaitingForLocation = false;
    private final Object mLocationLock = new Object();

    public static synchronized LocationHandler getInstance() {
        LocationHandler locationHandler;
        synchronized (LocationHandler.class) {
            if (sInstance == null) {
                sInstance = new LocationHandler();
            }
            locationHandler = sInstance;
        }
        return locationHandler;
    }

    private LocationHandler() {
    }

    public boolean isWaitingForLocation(ContentContainer container) {
        if (container.isGroupRelated()) {
            return false;
        }
        return this.mIsWaitingForLocation;
    }

    public LocationResult getLocationForCurrentModality(boolean waitIfBusy) {
        long locationAge;
        Long l;
        Context context = App.getAppContext();
        if (ZapConfig.supportsFullFeature()) {
            locationAge = ((Long) Z.LOCATION_MAX_AGE_MODALITY_NONE.read()).longValue();
            String modality = ModalityHelper.getInstance().getCurrentModality();
            if (Log.isLoggable("Zap-LocationHandler", 4)) {
                Log.i("Zap-LocationHandler", "Current modality: " + modality);
            }
            if (modality != null) {
                if (modality.equals(ModalityHelper.MODALITY_WALKING)) {
                    locationAge = ((Long) Z.LOCATION_MAX_AGE_WALKING.read()).longValue();
                } else if (modality.equals(ModalityHelper.MODALITY_RUNNING)) {
                    locationAge = ((Long) Z.LOCATION_MAX_AGE_RUNNING.read()).longValue();
                } else if (modality.equals(ModalityHelper.MODALITY_VEHICLE)) {
                    locationAge = ((Long) Z.LOCATION_MAX_AGE_VEHICLE.read()).longValue();
                }
            }
            if (DBG) {
                Log.d("Zap-LocationHandler", "Location age for this modality: " + (locationAge / 1000) + " secs");
            }
        } else {
            locationAge = ((Long) Z.LOCATION_MAX_AGE_NO_MODALITY.read()).longValue();
            if (DBG) {
                Log.d("Zap-LocationHandler", "No modality support, Location age: " + (locationAge / 1000) + " secs");
            }
        }
        LocationResult result = BlastLocationProvider.getBestLastKnownLocation(context, locationAge);
        if (result.hasSatisfiedLocation()) {
            if (DBG) {
                Log.d("Zap-LocationHandler", "Known location satisfies required conditions");
            }
            long knownLocationAge = result.getAge();
            if (DBG) {
                Log.d("Zap-LocationHandler", "Known location age: " + (knownLocationAge / 1000) + " secs");
            }
            if (knownLocationAge > ZapConstants.LOCATION_MAX_AGE_NO_MODALITY_SUPPORT || result.location.getAccuracy() > 100.0f) {
                boolean isOnWifi = NetworkUtils.isWifiConnected(context);
                BlastLocationProvider.getInstance().requestUpdates(!isOnWifi, isOnWifi);
            }
            return result;
        } else if (this.mIsWaitingForLocation && !waitIfBusy) {
            return null;
        } else {
            synchronized (this.mLocationLock) {
                this.mIsWaitingForLocation = true;
                if (Log.isLoggable("Zap-LocationHandler", 4)) {
                    Log.i("Zap-LocationHandler", "Known location does not satisfy required conditions - starting updates");
                }
                BlastLocationProvider.getInstance().requestUpdates(true, true);
                WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
                if (wifiManager != null && wifiManager.isWifiEnabled()) {
                    if (DBG) {
                        Log.d("Zap-LocationHandler", "Triggering WiFi scan to help on obtaining accurate location");
                    }
                    wifiManager.startScan();
                }
                long startTime = System.currentTimeMillis();
                long retryInterval = TimeUnit.SECONDS.toMillis(2);
                if (BlastLocationProvider.getInstance().areAllLocationSettingsEnabled()) {
                    l = (Long) Z.LOCATION_RETRY_MAX_TIME_SETTINGS_ENABLED.read();
                } else {
                    l = (Long) Z.LOCATION_RETRY_MAX_TIME_SETTINGS_DISABLED.read();
                }
                long retryUntil = startTime + l.longValue();
                int retries = 0;
                boolean toastShown = false;
                while (!result.hasSatisfiedLocation() && System.currentTimeMillis() < retryUntil) {
                    retries++;
                    if (retries > 2 && !toastShown) {
                        toastShown = true;
                        ZapUi.getInstance().onLocationEvent(LocationEvent.VERIFYING_LOCATION);
                    }
                    SystemClock.sleep(retryInterval);
                    if (DBG) {
                        Log.d("Zap-LocationHandler", "Retrying location " + retries);
                    }
                    result = BlastLocationProvider.getBestLastKnownLocation(context, locationAge);
                }
                this.mIsWaitingForLocation = false;
            }
            return result;
        }
    }
}
