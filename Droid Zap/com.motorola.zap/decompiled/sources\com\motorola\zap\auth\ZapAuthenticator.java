package com.motorola.zap.auth;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import com.motorola.sharing.Authenticator.Status;
import com.motorola.sharing.util.auth.GoogleAuthenticator;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConfig;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.app.App;
import com.motorola.zap.ui.ZapUi;
import com.motorola.zap.ui.ZapUi.AuthenticationEvent;

public class ZapAuthenticator extends GoogleAuthenticator {
    private final Context mContext;

    public ZapAuthenticator(Context context) {
        super(context);
        this.mContext = context;
    }

    public String getAccount() {
        if (ZapConfig.supportsFullFeature()) {
            return getMotoIdAccount();
        }
        return getGoogleAccount();
    }

    private static String getMotoIdAccount() {
        String str = "com.motorola.blur.service.bsutils.motorola";
        Account[] accounts = AccountManager.get(App.getAppContext()).getAccountsByType("com.motorola.blur.service.bsutils.motorola");
        if (accounts.length == 0) {
            return null;
        }
        String account = accounts[0].name;
        if (!accountExists(account)) {
            return null;
        }
        return account;
    }

    private static String getGoogleAccount() {
        String account = (String) Z.GOOGLE_ACCOUNT.read();
        if (accountExists(account)) {
            return account;
        }
        Z.GOOGLE_ACCOUNT.write(null);
        return null;
    }

    private static boolean accountExists(String account) {
        if (account == null || ZapConstants.GOOGLE_ACCOUNT_NONE.equals(account)) {
            return false;
        }
        for (Account a : AccountManager.get(App.getAppContext()).getAccounts()) {
            if (a.name.equals(account)) {
                return true;
            }
        }
        return false;
    }

    public synchronized void onAuthTakingTooLong() {
        ZapUi.getInstance().onAuthenticationEvent(AuthenticationEvent.AUTH_TAKING_TOO_LONG);
    }

    public void onAuthError(Status status) {
    }
}
