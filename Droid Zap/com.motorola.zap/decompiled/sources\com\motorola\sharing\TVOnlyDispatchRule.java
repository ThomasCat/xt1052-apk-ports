package com.motorola.sharing;

import com.motorola.sharing.ContentContainer.DiscoveryParams;
import com.motorola.sharing.ContentContainer.UploadRule;
import com.motorola.sharing.DispatchRule.Params;
import com.motorola.sharing.provider.tv.TVProvider;
import com.motorola.sharing.util.Logger;

public class TVOnlyDispatchRule implements DispatchRule {
    private static final String TAG = Logger.getTvTag("TVOnlyDispatch");
    private static TVOnlyDispatchRule sRuleInstance = null;
    private TVProvider mTVProvider = TVProvider.getInstance();

    static synchronized DispatchRule getInstance() {
        TVOnlyDispatchRule tVOnlyDispatchRule;
        synchronized (TVOnlyDispatchRule.class) {
            if (sRuleInstance == null) {
                sRuleInstance = new TVOnlyDispatchRule();
            }
            tVOnlyDispatchRule = sRuleInstance;
        }
        return tVOnlyDispatchRule;
    }

    public Params getParams() {
        return null;
    }

    public Endpoint[] getStaticEndpoints() {
        return TVProvider.getInstance().getStaticEndpoints();
    }

    private TVOnlyDispatchRule() {
    }

    public void discoverContent(DiscoveryParams params, ContentDiscoveryListener listener, Endpoint[] endpoints) {
        Logger.d(TAG, "discoverContent(): " + params.getPrintableSignature());
        params.setContentDiscoveryListener(listener);
        this.mTVProvider.discoverContent(params, null, endpoints);
    }

    public void stopDiscovery(DiscoveryParams params) {
        Logger.d(TAG, "stopDiscovery(): " + params.getPrintableSignature());
        this.mTVProvider.stopDiscovery(params);
    }

    public void downloadContent(ContentContainer container, ContentDownloadListener listener) {
        Logger.d(TAG, "downloadContent(): " + container.getPrintableSignature());
        container.setContentDownloadListener(listener);
        this.mTVProvider.downloadContent(container, null);
    }

    public void cancelDownload(ContentContainer container) {
        Logger.d(TAG, "cancelDownload(): " + container.getPrintableSignature());
        this.mTVProvider.cancelDownload(container);
    }

    public void reportAbuse(ContentContainer container, ReportAbuseListener listener) {
        Logger.d(TAG, "reportAbuse(): " + container.getPrintableSignature());
        container.setReportAbuseListener(listener);
        this.mTVProvider.reportAbuse(container, null);
    }

    public void shareContent(ContentContainer container, ContentShareListener listener, Endpoint[] endpoints) {
        Logger.d(TAG, "shareContent(): " + container.getPrintableSignature());
        container.uploadRule = UploadRule.ALWAYS;
        container.setContentShareListener(listener);
        this.mTVProvider.shareContent(container, null, endpoints);
    }

    public void stopSharing(ContentContainer container) {
        Logger.d(TAG, "stopSharing(): " + container.getPrintableSignature());
        this.mTVProvider.stopSharing(container);
    }

    public void stopSharing(String shareId, ContentShareListener listener) {
        Logger.d(TAG, "stopSharing(): sid=" + shareId);
        this.mTVProvider.stopSharing(ContentContainer.generateCancelShareContainer(shareId));
    }

    public void cancelSharing(ContentContainer container) {
        throw new UnsupportedOperationException("cancelSharing is not supported");
    }

    public void resumeSharing(ContentContainer container, ContentShareListener listener) {
        throw new UnsupportedOperationException("resumeSharing is not supported");
    }
}
