package com.motorola.zap.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

public class CustomHorizontalScrollView extends HorizontalScrollView {
    /* access modifiers changed from: private */
    public boolean mIsScrolling = false;
    /* access modifiers changed from: private */
    public boolean mIsTouching = false;
    /* access modifiers changed from: private */
    public int mLastScrollPosition = 0;
    /* access modifiers changed from: private */
    public long mLastScrollTime = 0;
    private Runnable mScrollStopTask = new Runnable() {
        public void run() {
            while (true) {
                if (!CustomHorizontalScrollView.this.mIsTouching && System.currentTimeMillis() - CustomHorizontalScrollView.this.mLastScrollTime >= 200) {
                    break;
                }
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }
            CustomHorizontalScrollView.this.mIsScrolling = false;
            if (CustomHorizontalScrollView.this.mTickerActionListener != null) {
                CustomHorizontalScrollView.this.mTickerActionListener.onScrollStop(CustomHorizontalScrollView.this.mLastScrollPosition);
            }
        }
    };
    /* access modifiers changed from: private */
    public TickerActionListener mTickerActionListener = null;
    private Type mType = Type.MAIN_IMAGE;

    public interface TickerActionListener {
        void onScrollStop(int i);

        void onUserAction();
    }

    public enum Type {
        MAIN_IMAGE,
        TICKER
    }

    public CustomHorizontalScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setScrollStopListener(TickerActionListener listener) {
        this.mTickerActionListener = listener;
    }

    public void setType(Type type) {
        this.mType = type;
    }

    public void onScrollChanged(int x, int y, int prevX, int prevY) {
        super.onScrollChanged(x, y, prevX, prevY);
        this.mLastScrollTime = System.currentTimeMillis();
        this.mLastScrollPosition = x;
        if (!this.mIsScrolling) {
            this.mIsScrolling = true;
            new Thread(this.mScrollStopTask).start();
        }
    }

    public boolean onTouchEvent(MotionEvent ev) {
        super.onTouchEvent(ev);
        if (ev.getAction() == 0 || ev.getAction() == 2) {
            this.mIsTouching = true;
            if (this.mTickerActionListener != null) {
                this.mTickerActionListener.onUserAction();
            }
        } else if (ev.getAction() == 3 || ev.getAction() == 1) {
            this.mIsTouching = false;
        }
        if (this.mType == Type.TICKER) {
            return true;
        }
        return false;
    }
}
