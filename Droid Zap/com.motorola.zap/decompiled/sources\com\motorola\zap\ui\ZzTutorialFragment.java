package com.motorola.zap.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ViewFlipper;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConstants;
import java.util.Arrays;

public class ZzTutorialFragment extends Fragment implements OnClickListener {
    boolean isMenuNeeded = true;
    int mCardShareIndex;
    int mInviteShareIndex;
    int mInviteTextIndex;
    ViewFlipper mRootView;
    int mStartInviteIndex;
    int mStartViewIndex;
    int mZoneEndIndex;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.mRootView = (ViewFlipper) inflater.inflate(R.layout.zz_tutorial, container, false);
        this.mStartViewIndex = this.mRootView.indexOfChild(this.mRootView.findViewById(R.id.start_view));
        this.mStartInviteIndex = this.mRootView.indexOfChild(this.mRootView.findViewById(R.id.start_invite));
        this.mInviteTextIndex = this.mRootView.indexOfChild(this.mRootView.findViewById(R.id.invite_text));
        this.mInviteShareIndex = this.mRootView.indexOfChild(this.mRootView.findViewById(R.id.invite_share));
        this.mCardShareIndex = this.mRootView.indexOfChild(this.mRootView.findViewById(R.id.card_share));
        this.mZoneEndIndex = this.mRootView.indexOfChild(this.mRootView.findViewById(R.id.zone_end));
        initializeStartView();
        initializeStartInvite();
        initializeInviteText();
        initializeInviteShare();
        initializeCardShare();
        initializeZoneEnd();
        this.mRootView.setDisplayedChild(this.mStartViewIndex);
        return this.mRootView;
    }

    private void initializeZoneEnd() {
        ((Button) this.mRootView.findViewById(R.id.zone_end_done)).setOnClickListener(this);
    }

    private void initializeCardShare() {
        ((Button) this.mRootView.findViewById(R.id.card_share_next)).setOnClickListener(this);
    }

    private void initializeInviteShare() {
        ((Button) this.mRootView.findViewById(R.id.invite_share_next)).setOnClickListener(this);
    }

    private void initializeInviteText() {
        ((Button) this.mRootView.findViewById(R.id.invite_text_next)).setOnClickListener(this);
    }

    private void initializeStartInvite() {
        ((Button) this.mRootView.findViewById(R.id.start_invite_next)).setOnClickListener(this);
    }

    private void initializeStartView() {
        ((Button) this.mRootView.findViewById(R.id.skip)).setOnClickListener(this);
        ((Button) this.mRootView.findViewById(R.id.take_tour)).setOnClickListener(this);
    }

    /* access modifiers changed from: 0000 */
    public void launchZapZoneView(View v) {
        Z.ZZ_TUTORIAL_SHOWN.write(Boolean.valueOf(true));
        Bundle args = new Bundle();
        args.putInt(TimeLineFragment.TYPE, 2);
        Fragment fragment = new TimeLineFragment();
        args.putString(ZapConstants.CONTENT_TITLE, getResources().getStringArray(R.array.default_navigation_drawer_list)[3]);
        fragment.setArguments(args);
        getFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.skip /*2131296548*/:
            case R.id.zone_end_done /*2131296563*/:
                launchZapZoneView(arg0);
                return;
            case R.id.take_tour /*2131296549*/:
                this.mRootView.setDisplayedChild(this.mStartInviteIndex);
                return;
            case R.id.start_invite_next /*2131296552*/:
                this.mRootView.setDisplayedChild(this.mInviteTextIndex);
                this.isMenuNeeded = false;
                getActivity().invalidateOptionsMenu();
                getActivity().setTitle(R.string.zap_zone_invite);
                return;
            case R.id.invite_text_next /*2131296557*/:
                this.mRootView.setDisplayedChild(this.mInviteShareIndex);
                return;
            case R.id.invite_share_next /*2131296559*/:
                this.mRootView.setDisplayedChild(this.mCardShareIndex);
                this.isMenuNeeded = true;
                getActivity().invalidateOptionsMenu();
                getActivity().setTitle((CharSequence) Arrays.asList(getResources().getStringArray(R.array.default_navigation_drawer_list)).get(3));
                return;
            case R.id.card_share_next /*2131296560*/:
                this.mRootView.setDisplayedChild(this.mZoneEndIndex);
                return;
            default:
                return;
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (((DroidBlastLauncher) getActivity()).showMenu()) {
            if (this.isMenuNeeded) {
                menu.add(0, 0, 0, null).setIcon(R.drawable.ic_zap_new).setTitle(R.string.menu_zap_new).setShowAsAction(2);
            }
            super.onCreateOptionsMenu(menu, inflater);
        }
    }
}
