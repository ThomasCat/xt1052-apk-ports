package com.motorola.zap;

import android.text.TextUtils;

public class LogUtils {
    public static final boolean IS_DEBUGGABLE = (ZapConfig.hasMotoAccount() || ZapConfig.isMotoDebugBuild());
    public static final String PREFIX = "Zap-";

    public static String obfuscate(String string) {
        int length = TextUtils.isEmpty(string) ? 0 : string.length();
        if (length == 0) {
            return string;
        }
        if (length < 5) {
            return "*****";
        }
        return string.charAt(0) + "*****" + string.charAt(length - 1);
    }
}
