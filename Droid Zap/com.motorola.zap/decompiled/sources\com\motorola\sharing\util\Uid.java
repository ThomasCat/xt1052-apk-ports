package com.motorola.sharing.util;

public class Uid {
    private static final SharedPreference<Integer> NEXT_UID = new SharedPreference<>("g_nextUid", Integer.class, Integer.valueOf(1073741823), SharedPreference.integerHandler, SharedPreference.nullVerificator);

    public static synchronized int getNext() {
        int nextUid;
        synchronized (Uid.class) {
            nextUid = ((Integer) NEXT_UID.get()).intValue();
            if (nextUid >= Integer.MAX_VALUE) {
                NEXT_UID.set(Integer.valueOf(1));
            } else {
                NEXT_UID.set(Integer.valueOf(nextUid + 1));
            }
        }
        return nextUid;
    }
}
