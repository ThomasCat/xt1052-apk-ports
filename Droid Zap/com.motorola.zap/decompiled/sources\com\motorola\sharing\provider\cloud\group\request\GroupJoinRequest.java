package com.motorola.sharing.provider.cloud.group.request;

import android.content.Intent;
import android.text.TextUtils;
import com.motorola.gcsp.json.StatusCode;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener.GroupEvent.Substatus;
import com.motorola.sharing.GroupListener.GroupEvent.Type;
import com.motorola.sharing.GroupManager.GroupInfo;
import com.motorola.sharing.GroupManager.JoinParams;
import com.motorola.sharing.provider.cloud.GroupReceiver;
import com.motorola.sharing.provider.cloud.HttpHelper;
import com.motorola.sharing.provider.cloud.UrlHelper;
import com.motorola.sharing.provider.cloud.group.GroupHandler;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker.GroupState;
import com.motorola.sharing.util.HistoryManager.Shares.C;
import com.motorola.sharing.util.JsonHelper;
import com.motorola.sharing.util.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class GroupJoinRequest extends GroupRequest {
    private static final String TAG = Logger.getCloudTag("GroupJoinReq");
    private final JoinParams mJoinParams;

    public GroupJoinRequest(JoinParams params, boolean isRetry) {
        super(GCSP.getContext(), isRetry);
        this.mJoinParams = params;
    }

    private static GroupJoinRequest getClone(GroupJoinRequest request, boolean isRetry) {
        return new GroupJoinRequest(request.mJoinParams, isRetry);
    }

    public Type getWhat() {
        return Type.GROUP_JOIN;
    }

    public Substatus checkPreconditions() {
        if (!isNetworkConnected()) {
            return Substatus.NETWORK_ERROR;
        }
        return super.checkPreconditions();
    }

    public Intent getRetryIntent() {
        Intent intent = new Intent(GroupReceiver.ACTION_GROUP_JOIN);
        intent.putExtra(GroupReceiver.EXTRA_GROUP_PARAMS, this.mJoinParams);
        if (isRetry()) {
            return null;
        }
        return intent;
    }

    public void run() {
        Logger.d(TAG, String.format("Running request, retry: %b", new Object[]{Boolean.valueOf(isRetry())}));
        GroupState groupState = this.mGroupStateTracker.getState();
        if (groupState.isActive() && TextUtils.equals(groupState.getOwner(), this.mJoinParams.owner) && TextUtils.equals(groupState.getId(), this.mJoinParams.id)) {
            Logger.d(TAG, "Group already joined - gid: " + groupState.getId());
            sendOk();
        } else if (!authenticate()) {
            Logger.e(TAG, "Authentication error");
            sendError(Substatus.AUTHENTICATION_ERROR);
        } else {
            String requestData = createRequestData();
            if (requestData == null) {
                Logger.e(TAG, "Failed to construct request");
                sendError(Substatus.CLIENT_ERROR);
                return;
            }
            String response = null;
            try {
                Logger.d(TAG, "Sending request to server...");
                response = HttpHelper.sendPostRequest(UrlHelper.addTargetMember(), requestData, this.mUserToken, this.mUserId, true);
            } catch (Exception e) {
                Logger.e(TAG, "Exception sending request to server: " + e.toString());
            }
            if (response == null) {
                Logger.e(TAG, "Request to server failed");
                sendError(Substatus.NETWORK_ERROR);
                return;
            }
            handleServerResponse(response);
        }
    }

    private String createRequestData() {
        JSONObject jsonObj = new JSONObject();
        try {
            JsonHelper.addCommonRequestFields(jsonObj);
            jsonObj.put("targetId", this.mJoinParams.id);
            jsonObj.put("targetType", "group");
            if (!this.mJoinParams.rejoin) {
                JSONObject shareFilter = new JSONObject();
                jsonObj.put("shareFilter", shareFilter);
                JSONObject location = new JSONObject();
                shareFilter.put("location", location);
                location.put("lat", this.mJoinParams.locality.latitude);
                location.put("lng", this.mJoinParams.locality.longitude);
                if (!TextUtils.isEmpty(this.mJoinParams.pin)) {
                    JSONObject pin = new JSONObject();
                    shareFilter.put(C.PIN, pin);
                    pin.put(C.PIN, this.mJoinParams.pin);
                }
            }
            Logger.d(TAG, "Server Request:");
            Logger.dump(TAG, jsonObj);
            return jsonObj.toString();
        } catch (JSONException e) {
            return null;
        }
    }

    private void handleServerResponse(String response) {
        try {
            JSONObject responseJson = new JSONObject(response);
            Logger.d(TAG, "Server Response:");
            Logger.dump(TAG, responseJson);
            StatusCode status = StatusCode.codeOf(responseJson.getString("status"));
            switch (status) {
                case OK:
                    handleOk();
                    return;
                case ERR_026:
                    handleOk();
                    return;
                case ERR_009:
                    if (!isRetry()) {
                        handleAlreadyInOtherZone(responseJson);
                        return;
                    }
                    break;
            }
            Logger.e(TAG, "Request failed with status: " + status);
            sendError(statusToError(status));
        } catch (JSONException e) {
            Logger.e(TAG, "Exception parsing JSON response: " + e);
            sendError(Substatus.SERVER_ERROR);
        }
    }

    private void handleOk() throws JSONException {
        onGroupStarted(this.mJoinParams.owner, this.mJoinParams.id, this.mJoinParams.name, this.mJoinParams.pin, this.mJoinParams.thumbnail, this.mJoinParams.locality);
        Logger.d(TAG, "Group joined - gid: " + this.mJoinParams.id);
        sendOk();
    }

    private void handleAlreadyInOtherZone(JSONObject responseJson) throws JSONException {
        Logger.w(TAG, "Still in an active group, quitting it first");
        JSONObject target = responseJson.getJSONArray("activeTargets").getJSONObject(0);
        String id = target.getString("targetId");
        GroupHandler.getInstance().leaveGroup(target.getJSONObject("targetMeta").getString("targetOwner"), id);
        GroupHandler.getInstance().retry(getClone(this, true));
    }

    public GroupInfo getGroupInfo() {
        return new GroupInfo(false, this.mJoinParams.owner, this.mJoinParams.id, this.mJoinParams.name, this.mJoinParams.thumbnail, TextUtils.isEmpty(this.mJoinParams.pin), this.mJoinParams.pin, 0, 0);
    }
}
