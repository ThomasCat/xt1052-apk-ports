package com.motorola.sharing;

public interface ProfileManagerListener {

    public static class ProfileEvent {
        public Profile profile;
        public Status status;
        public Substatus substatus;
        public Type type;

        public enum Status {
            SUCCESS,
            ERROR
        }

        public enum Substatus {
            NO_ERROR,
            NETWORK_ERROR,
            AUTHENTICATION_ERROR,
            PROFILE_NOT_FOUND,
            CLIENT_ERROR,
            SERVER_ERROR
        }

        public enum Type {
            GET_MY_PROFILE,
            UPDATE_MY_PROFILE,
            DELETE_MY_PROFILE,
            UPDATE_MY_PROFILE_PICTURE
        }
    }

    void onProfileEvent(ProfileEvent profileEvent);
}
