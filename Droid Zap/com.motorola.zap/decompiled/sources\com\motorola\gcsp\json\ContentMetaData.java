package com.motorola.gcsp.json;

public class ContentMetaData implements InputSet {
    public String md5Hash;
    public String mimeType;
    public String name;
    public int size;
    public String thumbnail;
}
