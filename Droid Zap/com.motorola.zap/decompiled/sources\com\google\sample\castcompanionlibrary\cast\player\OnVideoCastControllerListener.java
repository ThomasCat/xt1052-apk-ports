package com.google.sample.castcompanionlibrary.cast.player;

import android.view.View;
import android.widget.SeekBar;
import com.google.sample.castcompanionlibrary.cast.exceptions.CastException;
import com.google.sample.castcompanionlibrary.cast.exceptions.NoConnectionException;
import com.google.sample.castcompanionlibrary.cast.exceptions.TransientNetworkDisconnectionException;

public interface OnVideoCastControllerListener {
    void onConfigurationChanged();

    void onPlayPauseClicked(View view) throws CastException, TransientNetworkDisconnectionException, NoConnectionException;

    void onProgressChanged(SeekBar seekBar, int i, boolean z);

    void onStartTrackingTouch(SeekBar seekBar);

    void onStopTrackingTouch(SeekBar seekBar);
}
