package com.motorola.zap.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.google.android.gms.drive.DriveFile;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.GCSP;
import com.motorola.zap.LogUtils;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.location.ModalityHelper;
import com.motorola.zap.ui.DroidBlastLauncher;
import com.motorola.zap.ui.ZapUi;
import com.motorola.zap.zz.ui.BaseZzUiHandler;

public class ActionReceiver extends BroadcastReceiver {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(ZapConstants.ACTION_ZAP_UPGRADE_FEATURE_NOTIFICATION)) {
            if (DBG) {
                Log.d("Zap-ActionReceiver", "ACTION_ZAP_UPGRADE_FEATURE_NOTIFICATION");
            }
            Intent intent2 = new Intent(context, DroidBlastLauncher.class);
            intent2.setFlags(DriveFile.MODE_READ_ONLY);
            intent2.putExtra(ZapConstants.EXTRA_SHOW_UPGRADE_FEATURE_TYPE, intent.getIntExtra(ZapConstants.EXTRA_SHOW_UPGRADE_FEATURE_TYPE, -1));
            context.startActivity(intent2);
        } else if (action.equals(ZapConstants.ACTION_GS_SWIPE_UP)) {
            if (DBG) {
                Log.d("Zap-ActionReceiver", "ACTION_GS_SWIPE_UP");
            }
            if (((Boolean) Z.FEATURE_ENABLED.read()).booleanValue()) {
                new UploadHandler().handleUploadGesture();
            }
        } else if (action.equals(ZapConstants.ACTION_GS_SWIPE_DOWN)) {
            if (DBG) {
                Log.d("Zap-ActionReceiver", "ACTION_GS_SWIPE_DOWN");
            }
            if (!((Boolean) Z.FEATURE_ENABLED.read()).booleanValue()) {
                ZapTracker.getInstance().onRefreshComplete();
            } else {
                new DownloadHandler().handleDownloadGesture(intent);
            }
        } else if (action.equals(ZapConstants.ACTION_GS_GALLERY_PHOTO_SHARED)) {
            if (DBG) {
                Log.d("Zap-ActionReceiver", "ACTION_GS_GALLERY_PHOTO_SHARED");
            }
            if (((Boolean) Z.FEATURE_ENABLED.read()).booleanValue()) {
                new UploadHandler().handleUpload(intent);
                return;
            }
            Intent intent3 = new Intent(context, DroidBlastLauncher.class);
            intent3.setFlags(DriveFile.MODE_READ_ONLY);
            context.startActivity(intent3);
        } else if (action.equals(ZapConstants.ACTION_PIN_INPUT)) {
            if (DBG) {
                Log.d("Zap-ActionReceiver", "ACTION_PIN_INPUT");
            }
            new DownloadHandler().handleActionReceive(intent);
        } else if (action.equals(ZapConstants.ACTION_REPORT_ABUSE)) {
            if (DBG) {
                Log.d("Zap-ActionReceiver", "ACTION_REPORT_ABUSE");
            }
            new ReportAbuseHandler().handleActionReportAbuse(intent);
        } else if (action.equals(ZapConstants.ACTION_ZZ_DO_POLL)) {
            if (DBG) {
                Log.d("Zap-ActionReceiver", "ACTION_ZZ_DO_POLL");
            }
            new DownloadHandler().handleZzPolling(intent);
        } else if (action.equals(ZapConstants.ACTION_AUTO_POLL)) {
            if (DBG) {
                Log.d("Zap-ActionReceiver", "ACTION_AUTO_DO_POLL");
            }
            new DownloadHandler().handleAutoPolling(intent);
        } else if (action.equals(ZapConstants.ACTION_CANCEL_DOWNLOAD)) {
            long longExtra = intent.getLongExtra(ZapConstants.EXTRA_TRANSACTION_UID, -1);
            intent.getStringExtra(ZapConstants.EXTRA_FROM_ID);
            intent.getStringExtra(ZapConstants.EXTRA_SERVER_SHARE_ID);
            ContentContainer contentContainer = ZapTracker.getInstance().get(longExtra);
            if (contentContainer != null) {
                GCSP.getContentDispatcher().cancelDownload(contentContainer);
            }
        } else if (action.equals(ZapConstants.ACTION_CANCEL_UPLOAD)) {
            if (DBG) {
                Log.d("Zap-ActionReceiver", "ACTION_CANCEL_UPLOAD");
            }
            new CancelShareHandler().handleActionCancelShare(intent);
        } else if (action.equals(ZapConstants.ACTION_ZZ_IGNORE_INVITE) || action.equals(ZapConstants.ACTION_ZZ_CANCEL_INVITE)) {
            ZzUtil.ignoreZoneInvite(ZapUi.getInstance(), intent.getIntExtra(ZapConstants.EXTRA_ZZ_INVITE_UID, -1));
        } else if (action.equals(ZapConstants.ACTION_GS_MODALITY_CHANGE)) {
            ModalityHelper.getInstance().handleModalityUpdate(intent);
        } else if (action.equals(ZapConstants.ACTION_LAUNCH_MOTOCARE)) {
            if (DBG) {
                Log.d("Zap-ActionReceiver", "handleActionCallMotocare");
            }
            Intent intent4 = new Intent("com.motorola.genie.GLOBAL_QUESTS_LIST");
            intent4.setFlags(DriveFile.MODE_READ_ONLY);
            App.getAppContext().startActivity(intent4);
        } else if (action.equals(ZapConstants.ACTION_LEAVE_ZZ)) {
            GCSP.getGroupManager().leave(null);
        } else if (action.equals(ZapConstants.ACTION_ZZ_NOTIFICATION_UPDATE)) {
            BaseZzUiHandler.updateZzActiveNotification();
        }
    }
}
