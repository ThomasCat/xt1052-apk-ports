package com.google.android.gms.maps;

import com.google.android.gms.dynamic.b;
import com.google.android.gms.internal.er;

public final class CameraUpdate {
    private final b OI;

    CameraUpdate(b remoteObject) {
        this.OI = (b) er.f(remoteObject);
    }

    /* access modifiers changed from: 0000 */
    public b gK() {
        return this.OI;
    }
}
