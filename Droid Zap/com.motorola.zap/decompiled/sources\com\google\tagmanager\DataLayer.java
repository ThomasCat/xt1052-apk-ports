package com.google.tagmanager;

import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.location.LocationRequest;
import com.motorola.zap.ZapConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataLayer {
    static final String[] LIFETIME_KEY_COMPONENTS = "gtm.lifetime".toString().split("\\.");
    private static final Pattern LIFETIME_PATTERN = Pattern.compile("(\\d+)\\s*([smhd]?)");
    public static final Object OBJECT_NOT_PRESENT = new Object();
    private final ConcurrentHashMap<Listener, Integer> mListeners;
    private final Map<Object, Object> mModel;
    private final PersistentStore mPersistentStore;
    /* access modifiers changed from: private */
    public final CountDownLatch mPersistentStoreLoaded;
    private final ReentrantLock mPushLock;
    private final LinkedList<Map<Object, Object>> mUpdateQueue;

    static final class KeyValue {
        public final String mKey;
        public final Object mValue;

        KeyValue(String key, Object value) {
            this.mKey = key;
            this.mValue = value;
        }

        public String toString() {
            return "Key: " + this.mKey + " value: " + this.mValue.toString();
        }

        public int hashCode() {
            return Arrays.hashCode(new Integer[]{Integer.valueOf(this.mKey.hashCode()), Integer.valueOf(this.mValue.hashCode())});
        }

        public boolean equals(Object o) {
            if (!(o instanceof KeyValue)) {
                return false;
            }
            KeyValue other = (KeyValue) o;
            if (!this.mKey.equals(other.mKey) || !this.mValue.equals(other.mValue)) {
                return false;
            }
            return true;
        }
    }

    interface Listener {
        void changed(Map<Object, Object> map);
    }

    interface PersistentStore {

        public interface Callback {
            void onKeyValuesLoaded(List<KeyValue> list);
        }

        void clearKeysWithPrefix(String str);

        void loadSaved(Callback callback);

        void saveKeyValues(List<KeyValue> list, long j);
    }

    @VisibleForTesting
    DataLayer() {
        this(new PersistentStore() {
            public void saveKeyValues(List<KeyValue> list, long lifetimeInMillis) {
            }

            public void loadSaved(Callback callback) {
                callback.onKeyValuesLoaded(new ArrayList());
            }

            public void clearKeysWithPrefix(String keyPrefix) {
            }
        });
    }

    DataLayer(PersistentStore persistentStore) {
        this.mPersistentStore = persistentStore;
        this.mListeners = new ConcurrentHashMap<>();
        this.mModel = new HashMap();
        this.mPushLock = new ReentrantLock();
        this.mUpdateQueue = new LinkedList<>();
        this.mPersistentStoreLoaded = new CountDownLatch(1);
        loadSavedMaps();
    }

    public void push(Object key, Object value) {
        push(expandKeyValue(key, value));
    }

    public void push(Map<Object, Object> update) {
        try {
            this.mPersistentStoreLoaded.await();
        } catch (InterruptedException e) {
            Log.w("DataLayer.push: unexpected InterruptedException");
        }
        pushWithoutWaitingForSaved(update);
    }

    /* access modifiers changed from: private */
    public void pushWithoutWaitingForSaved(Map<Object, Object> update) {
        this.mPushLock.lock();
        try {
            this.mUpdateQueue.offer(update);
            if (this.mPushLock.getHoldCount() == 1) {
                processQueuedUpdates();
            }
            savePersistentlyIfNeeded(update);
        } finally {
            this.mPushLock.unlock();
        }
    }

    private void loadSavedMaps() {
        this.mPersistentStore.loadSaved(new Callback() {
            public void onKeyValuesLoaded(List<KeyValue> keyValues) {
                for (KeyValue keyValue : keyValues) {
                    DataLayer.this.pushWithoutWaitingForSaved(DataLayer.this.expandKeyValue(keyValue.mKey, keyValue.mValue));
                }
                DataLayer.this.mPersistentStoreLoaded.countDown();
            }
        });
    }

    private void savePersistentlyIfNeeded(Map<Object, Object> update) {
        Long lifetime = getLifetimeValue(update);
        if (lifetime != null) {
            List<KeyValue> flattenedMap = flattenMap(update);
            flattenedMap.remove("gtm.lifetime");
            this.mPersistentStore.saveKeyValues(flattenedMap, lifetime.longValue());
        }
    }

    private Long getLifetimeValue(Map<Object, Object> update) {
        Object lifetimeObject = getLifetimeObject(update);
        if (lifetimeObject == null) {
            return null;
        }
        return parseLifetime(lifetimeObject.toString());
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.Map<java.lang.Object, java.lang.Object>, code=java.lang.Object, for r8v0, types: [java.util.Map<java.lang.Object, java.lang.Object>, java.lang.Object] */
    private Object getLifetimeObject(Object update) {
        String[] arr$;
        Object obj = update;
        for (String component : LIFETIME_KEY_COMPONENTS) {
            if (!(obj instanceof Map)) {
                return null;
            }
            obj = ((Map) obj).get(component);
        }
        return obj;
    }

    /* access modifiers changed from: 0000 */
    public void clearPersistentKeysWithPrefix(String prefix) {
        push(prefix, null);
        this.mPersistentStore.clearKeysWithPrefix(prefix);
    }

    private List<KeyValue> flattenMap(Map<Object, Object> map) {
        List<KeyValue> result = new ArrayList<>();
        flattenMapHelper(map, ZapConstants.GOOGLE_ACCOUNT_NONE, result);
        return result;
    }

    private void flattenMapHelper(Map<Object, Object> map, String str, Collection<KeyValue> collection) {
        for (Entry entry : map.entrySet()) {
            String str2 = str + (str.length() == 0 ? ZapConstants.GOOGLE_ACCOUNT_NONE : ".") + entry.getKey();
            if (entry.getValue() instanceof Map) {
                flattenMapHelper((Map) entry.getValue(), str2, collection);
            } else if (!str2.equals("gtm.lifetime")) {
                collection.add(new KeyValue(str2, entry.getValue()));
            }
        }
    }

    @VisibleForTesting
    static Long parseLifetime(String str) {
        long j;
        Matcher matcher = LIFETIME_PATTERN.matcher(str);
        if (!matcher.matches()) {
            Log.i("unknown _lifetime: " + str);
            return null;
        }
        try {
            j = Long.parseLong(matcher.group(1));
        } catch (NumberFormatException e) {
            Log.w("illegal number in _lifetime value: " + str);
            j = 0;
        }
        if (j <= 0) {
            Log.i("non-positive _lifetime: " + str);
            return null;
        }
        String group = matcher.group(2);
        if (group.length() == 0) {
            return Long.valueOf(j);
        }
        switch (group.charAt(0)) {
            case 'd':
                return Long.valueOf(j * 1000 * 60 * 60 * 24);
            case LocationRequest.PRIORITY_LOW_POWER /*104*/:
                return Long.valueOf(j * 1000 * 60 * 60);
            case 'm':
                return Long.valueOf(j * 1000 * 60);
            case 's':
                return Long.valueOf(j * 1000);
            default:
                Log.w("unknown units in _lifetime: " + str);
                return null;
        }
    }

    private void processQueuedUpdates() {
        int i = 0;
        while (true) {
            int i2 = i;
            Map map = (Map) this.mUpdateQueue.poll();
            if (map != null) {
                processUpdate(map);
                i = i2 + 1;
                if (i > 500) {
                    this.mUpdateQueue.clear();
                    throw new RuntimeException("Seems like an infinite loop of pushing to the data layer");
                }
            } else {
                return;
            }
        }
    }

    private void processUpdate(Map<Object, Object> update) {
        synchronized (this.mModel) {
            for (Object key : update.keySet()) {
                mergeMap(expandKeyValue(key, update.get(key)), this.mModel);
            }
        }
        notifyListeners(update);
    }

    public Object get(String str) {
        synchronized (this.mModel) {
            Object obj = this.mModel;
            String[] split = str.split("\\.");
            int length = split.length;
            Object obj2 = obj;
            int i = 0;
            while (i < length) {
                String str2 = split[i];
                if (!(obj2 instanceof Map)) {
                    return null;
                }
                Object obj3 = ((Map) obj2).get(str2);
                if (obj3 == null) {
                    return null;
                }
                i++;
                obj2 = obj3;
            }
            return obj2;
        }
    }

    public static Map<Object, Object> mapOf(Object... objArr) {
        if (objArr.length % 2 != 0) {
            throw new IllegalArgumentException("expected even number of key-value pairs");
        }
        HashMap hashMap = new HashMap();
        for (int i = 0; i < objArr.length; i += 2) {
            hashMap.put(objArr[i], objArr[i + 1]);
        }
        return hashMap;
    }

    public static List<Object> listOf(Object... objects) {
        List<Object> list = new ArrayList<>();
        for (Object add : objects) {
            list.add(add);
        }
        return list;
    }

    /* access modifiers changed from: 0000 */
    public void registerListener(Listener listener) {
        this.mListeners.put(listener, Integer.valueOf(0));
    }

    private void notifyListeners(Map<Object, Object> update) {
        for (Listener listener : this.mListeners.keySet()) {
            listener.changed(update);
        }
    }

    /* access modifiers changed from: 0000 */
    public Map<Object, Object> expandKeyValue(Object obj, Object obj2) {
        HashMap hashMap = new HashMap();
        String[] split = obj.toString().split("\\.");
        int i = 0;
        HashMap hashMap2 = hashMap;
        while (i < split.length - 1) {
            HashMap hashMap3 = new HashMap();
            hashMap2.put(split[i], hashMap3);
            i++;
            hashMap2 = hashMap3;
        }
        hashMap2.put(split[split.length - 1], obj2);
        return hashMap;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void mergeMap(Map<Object, Object> from, Map<Object, Object> to) {
        for (Object key : from.keySet()) {
            Object fromValue = from.get(key);
            if (fromValue instanceof List) {
                if (!(to.get(key) instanceof List)) {
                    to.put(key, new ArrayList());
                }
                mergeList((List) fromValue, (List) to.get(key));
            } else if (fromValue instanceof Map) {
                if (!(to.get(key) instanceof Map)) {
                    to.put(key, new HashMap());
                }
                mergeMap((Map) fromValue, (Map) to.get(key));
            } else {
                to.put(key, fromValue);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void mergeList(List<Object> from, List<Object> to) {
        while (to.size() < from.size()) {
            to.add(null);
        }
        for (int index = 0; index < from.size(); index++) {
            Object fromValue = from.get(index);
            if (fromValue instanceof List) {
                if (!(to.get(index) instanceof List)) {
                    to.set(index, new ArrayList());
                }
                mergeList((List) fromValue, (List) to.get(index));
            } else if (fromValue instanceof Map) {
                if (!(to.get(index) instanceof Map)) {
                    to.set(index, new HashMap());
                }
                mergeMap((Map) fromValue, (Map) to.get(index));
            } else if (fromValue != OBJECT_NOT_PRESENT) {
                to.set(index, fromValue);
            }
        }
    }
}
