package com.motorola.zap.app;

import android.content.Intent;
import android.util.Log;
import com.motorola.zap.LogUtils;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.ZapUtils;
import java.util.Locale;

public class GestureRules {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;

    public boolean canHandleGesture(Intent intent, boolean isRecoverUserAction, boolean isInitialSetup, boolean fromShareIntent) {
        boolean isInternalIntent = isInternalIntent(intent);
        if (!isRecoverUserAction && !isInternalIntent) {
            String pkg = ZapUtils.getForegroundApp();
            if (DBG) {
                Log.d("Zap-GestureRules", "Foregound app: " + pkg);
            }
            if (ignoreGestureForPackage(pkg)) {
                if (Log.isLoggable("Zap-GestureRules", 4)) {
                    Log.i("Zap-GestureRules", "Gesture ignored because app " + pkg + " is in black list");
                }
                return false;
            } else if (((isInitialSetup && ((Boolean) Z.APPLY_WHITELIST_FOR_INITIAL_SETUP.read()).booleanValue()) || (!isInitialSetup && ((Boolean) Z.APPLY_WHITELIST_FOR_ZAP_GESTURE.read()).booleanValue())) && !allowGestureForPackage(pkg) && !fromShareIntent) {
                if (Log.isLoggable("Zap-GestureRules", 4)) {
                    Log.i("Zap-GestureRules", "Gesture ignored because app " + pkg + " is not in white list");
                }
                return false;
            }
        } else if (DBG) {
            Log.d("Zap-GestureRules", "Package check for gesture ignored because it's a recover user action or an internal app intent");
        }
        return true;
    }

    private boolean isInternalIntent(Intent intent) {
        String action = intent.getAction();
        if (action.equals(ZapConstants.ACTION_GS_SWIPE_DOWN) || action.equals(ZapConstants.ACTION_GS_SWIPE_UP)) {
            return intent.getBooleanExtra(ZapConstants.EXTRA_EVENT_ORIGIN_ZAP_APP, false);
        }
        return true;
    }

    private boolean ignoreGestureForPackage(String pkg) {
        String[] arr$;
        if (pkg == null) {
            return false;
        }
        String pkg2 = pkg.toLowerCase(Locale.ROOT);
        String ignoreList = (String) Z.PACKAGES_TO_IGNORE_GESTURE.read();
        if (pkg2 == null || ignoreList == null || ignoreList.isEmpty()) {
            return false;
        }
        for (String p : ignoreList.trim().split("\\|")) {
            if (DBG) {
                Log.d("Zap-GestureRules", "Pkg to ignore gesture: " + p);
            }
            if (p.toLowerCase(Locale.ROOT).equals(pkg2)) {
                if (DBG) {
                    Log.d("Zap-GestureRules", "Ignoring gesture for package");
                }
                return true;
            }
        }
        return false;
    }

    private boolean allowGestureForPackage(String pkg) {
        String[] arr$;
        if (pkg == null) {
            return false;
        }
        String pkg2 = pkg.toLowerCase(Locale.ROOT);
        if (pkg2.equals(App.getAppContext().getPackageName().toLowerCase(Locale.ROOT))) {
            if (DBG) {
                Log.d("Zap-GestureRules", "Package is Zap app - Allowing gesture for package");
            }
            return true;
        }
        String allowList = (String) Z.PACKAGES_TO_ALLOW_GESTURE.read();
        if (allowList == null || allowList.isEmpty()) {
            return false;
        }
        for (String p : allowList.trim().split("\\|")) {
            if (DBG) {
                Log.d("Zap-GestureRules", "Pkg to allow gesture: " + p);
            }
            if (p.toLowerCase(Locale.ROOT).equals(pkg2) || (p.toLowerCase(Locale.ROOT).equals(ZapConstants.ALL_MOTOROLA_PACKAGES) && pkg2.contains(".motorola."))) {
                if (DBG) {
                    Log.d("Zap-GestureRules", "Allowing gesture for package");
                }
                return true;
            }
        }
        return false;
    }
}
