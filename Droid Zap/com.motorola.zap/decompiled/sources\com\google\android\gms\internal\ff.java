package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;
import java.util.ArrayList;

public class ff implements Creator<fe> {
    static void a(fe feVar, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.c(parcel, 1, feVar.getVersionCode());
        b.b(parcel, 2, feVar.eC(), false);
        b.a(parcel, 3, feVar.eD(), false);
        b.D(parcel, p);
    }

    /* renamed from: Y */
    public fe[] newArray(int i) {
        return new fe[i];
    }

    /* renamed from: v */
    public fe createFromParcel(Parcel parcel) {
        String str = null;
        int o = a.o(parcel);
        int i = 0;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    i = a.g(parcel, n);
                    break;
                case 2:
                    arrayList = a.c(parcel, n, fe.a.CREATOR);
                    break;
                case 3:
                    str = a.m(parcel, n);
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new fe(i, arrayList, str);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }
}
