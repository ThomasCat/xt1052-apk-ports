package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;

public class jw implements Creator<jv> {
    static void a(jv jvVar, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.c(parcel, 1, jvVar.getVersionCode());
        b.a(parcel, 2, jvVar.ZK, false);
        b.a(parcel, 3, jvVar.oi, false);
        b.a(parcel, 4, (Parcelable) jvVar.ZO, i, false);
        b.a(parcel, 5, (Parcelable) jvVar.ZP, i, false);
        b.a(parcel, 6, (Parcelable) jvVar.ZQ, i, false);
        b.D(parcel, p);
    }

    /* renamed from: bn */
    public jv createFromParcel(Parcel parcel) {
        jt jtVar = null;
        int o = a.o(parcel);
        int i = 0;
        jt jtVar2 = null;
        jr jrVar = null;
        String str = null;
        String str2 = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    i = a.g(parcel, n);
                    break;
                case 2:
                    str2 = a.m(parcel, n);
                    break;
                case 3:
                    str = a.m(parcel, n);
                    break;
                case 4:
                    jrVar = (jr) a.a(parcel, n, jr.CREATOR);
                    break;
                case 5:
                    jtVar2 = (jt) a.a(parcel, n, jt.CREATOR);
                    break;
                case 6:
                    jtVar = (jt) a.a(parcel, n, jt.CREATOR);
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new jv(i, str2, str, jrVar, jtVar2, jtVar);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }

    /* renamed from: ct */
    public jv[] newArray(int i) {
        return new jv[i];
    }
}
