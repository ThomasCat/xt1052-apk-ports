package com.motorola.sharing.provider.cloud.group.request;

import android.text.TextUtils;
import com.motorola.gcsp.json.StatusCode;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener.GroupEvent.Substatus;
import com.motorola.sharing.GroupListener.GroupEvent.Type;
import com.motorola.sharing.GroupManager.GroupInfo;
import com.motorola.sharing.provider.cloud.HttpHelper;
import com.motorola.sharing.provider.cloud.UrlHelper;
import com.motorola.sharing.provider.cloud.group.GroupHandler;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker.GroupState;
import com.motorola.sharing.provider.cloud.group.LeaveGroupTracker;
import com.motorola.sharing.util.JsonHelper;
import com.motorola.sharing.util.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class GroupLeaveRequest extends GroupRequest {
    private static final String TAG = Logger.getCloudTag("GroupLeaveReq");
    private final String mId;
    private final boolean mIsOwner;
    private final boolean mLeaveCurrent;
    private final boolean mLeaveOnlyIfPending;
    private final GroupInfo mOriginalGroupInfo;
    private final String mOwner;

    public GroupLeaveRequest(String owner, String id) {
        super(GCSP.getContext(), false);
        this.mOriginalGroupInfo = GroupHandler.getInstance().getGroupInfo();
        this.mLeaveCurrent = false;
        this.mLeaveOnlyIfPending = false;
        this.mOwner = owner;
        this.mId = id;
        this.mIsOwner = TextUtils.equals(this.mOwner, GCSP.getAuthenticator().getAccount());
    }

    public GroupLeaveRequest(boolean onlyIfPending) {
        super(GCSP.getContext(), false);
        this.mOriginalGroupInfo = GroupHandler.getInstance().getGroupInfo();
        this.mLeaveCurrent = true;
        this.mLeaveOnlyIfPending = onlyIfPending;
        GroupState groupState = this.mGroupStateTracker.getState();
        this.mOwner = groupState.getOwner();
        this.mId = groupState.getId();
        this.mIsOwner = TextUtils.equals(this.mOwner, GCSP.getAuthenticator().getAccount());
    }

    public Type getWhat() {
        return Type.GROUP_LEAVE;
    }

    public void run() {
        String url;
        Logger.d(TAG, String.format("Running request, retry: %b", new Object[]{Boolean.valueOf(isRetry())}));
        if (this.mLeaveCurrent) {
            GroupState groupState = this.mGroupStateTracker.getState();
            if (this.mLeaveOnlyIfPending && !groupState.isLeaving()) {
                Logger.d(TAG, "Group is not pending to leave, discard request");
                handleError(Substatus.CANCELED);
                return;
            }
        }
        if (GroupStateTracker.isValidGroup(this.mOwner, this.mId)) {
            onGroupLeaveRequested(this.mOwner, this.mId);
            if (!authenticate()) {
                Logger.e(TAG, "Authentication error");
                handleError(Substatus.AUTHENTICATION_ERROR);
                return;
            }
            String requestData = createRequestData();
            if (requestData == null) {
                Logger.e(TAG, "Failed to construct request");
                handleError(Substatus.CLIENT_ERROR);
                return;
            }
            String response = null;
            try {
                Logger.d(TAG, "Sending request to server...");
                if (this.mIsOwner) {
                    url = UrlHelper.updateTarget();
                } else {
                    url = UrlHelper.updateTargetMember();
                }
                response = HttpHelper.sendPostRequest(url, requestData, this.mUserToken, this.mUserId, true);
            } catch (Exception e) {
                Logger.e(TAG, "Exception sending request to server: " + e.toString());
            }
            if (response == null) {
                Logger.e(TAG, "Request to server failed");
                handleError(Substatus.NETWORK_ERROR);
                return;
            }
            handleServerResponse(response);
            return;
        }
        Logger.d(TAG, "Group has been already left, discard request");
        handleError(Substatus.CANCELED);
    }

    private String createRequestData() {
        JSONObject jsonObj = new JSONObject();
        try {
            JsonHelper.addCommonRequestFields(jsonObj);
            jsonObj.put("targetType", "group");
            jsonObj.put("targetId", this.mId);
            if (this.mIsOwner) {
                JSONObject targetMeta = new JSONObject();
                jsonObj.put("targetMeta", targetMeta);
                targetMeta.put("targetStatus", "disabled");
            } else {
                jsonObj.put("status", "disabled");
            }
            Logger.d(TAG, "Server Request:");
            Logger.dump(TAG, jsonObj);
            return jsonObj.toString();
        } catch (JSONException e) {
            Logger.e(TAG, "ERROR generating group JSON request");
            return null;
        }
    }

    private void handleServerResponse(String response) {
        try {
            JSONObject responseJson = new JSONObject(response);
            Logger.d(TAG, "Server Response:");
            Logger.dump(TAG, responseJson);
            StatusCode status = StatusCode.codeOf(responseJson.getString("status"));
            switch (status) {
                case OK:
                case INACTIVE:
                case ERR_010:
                case ERR_012:
                case DISABLED:
                    onGroupStopped();
                    Logger.d(TAG, "Group left - gid: " + this.mId);
                    LeaveGroupTracker.setEnabled(false);
                    sendOk();
                    return;
                default:
                    Logger.e(TAG, "Request failed with status: " + status);
                    handleError(statusToError(status));
                    return;
            }
        } catch (JSONException e) {
            Logger.e(TAG, "Exception parsing JSON response: " + e);
            handleError(Substatus.SERVER_ERROR);
        }
    }

    private void handleError(Substatus error) {
        sendError(error);
        LeaveGroupTracker.setEnabled(true);
    }

    public GroupInfo getGroupInfo() {
        return this.mOriginalGroupInfo;
    }
}
