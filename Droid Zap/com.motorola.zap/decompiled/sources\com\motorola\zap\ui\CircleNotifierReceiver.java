package com.motorola.zap.ui;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.app.TransferState;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Array;

public class CircleNotifierReceiver {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private static int[][] mStateResourceIds = ((int[][]) Array.newInstance(Integer.TYPE, new int[]{5, 3}));

    public static final class BlastStateReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            new CircleNotifierReceiver().onReceive(context, intent);
        }
    }

    public static final class CircleWidgetReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            new CircleNotifierReceiver().onReceive(context, intent);
        }
    }

    public static final class WirelessDisplayReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            new CircleNotifierReceiver().onReceive(context, intent);
        }
    }

    public CircleNotifierReceiver() {
        mStateResourceIds[0][0] = R.drawable.ic_blast_rest_off;
        mStateResourceIds[0][1] = R.drawable.ic_blast_rest_unconnected;
        mStateResourceIds[0][2] = R.drawable.ic_blast_rest_connected;
        mStateResourceIds[1][0] = R.drawable.ic_blast_send_off;
        mStateResourceIds[1][1] = R.drawable.ic_blast_send_unconnected;
        mStateResourceIds[1][2] = R.drawable.ic_blast_send_connected;
        mStateResourceIds[2][0] = R.drawable.ic_blast_send_off;
        mStateResourceIds[2][1] = R.drawable.ic_blast_send_unconnected;
        mStateResourceIds[2][2] = R.drawable.ic_blast_send_connected;
        mStateResourceIds[3][0] = R.drawable.ic_blast_receive_off;
        mStateResourceIds[3][1] = R.drawable.ic_blast_receive_unconnected;
        mStateResourceIds[3][2] = R.drawable.ic_blast_receive_connected;
        mStateResourceIds[4][0] = R.drawable.ic_blast_zz_off;
        mStateResourceIds[4][1] = R.drawable.ic_blast_zz_unconnected;
        mStateResourceIds[4][2] = R.drawable.ic_blast_zz_connected;
    }

    public void onReceive(Context context, Intent intent) {
        if (isValidPermissions(context)) {
            String str = "com.motorola.widget.circlewidget3d.Action.CirclesReady";
            String action = intent.getAction();
            if (DBG) {
                Log.d("Zap-CircleNotifier", "Intent Action: " + action);
            }
            if (action.equals("com.motorola.widget.circlewidget3d.Action.CirclesReady")) {
                notifyCircle(context, getDroidBlastState(context), getWirelessDisplayState(context));
            } else if (action.equals("com.motorola.wirelessdisplay.service.WIRELESS_DISPLAY_MIRRORING_STATE_CHANGED")) {
                processWirelessDisplayState(context, intent);
            } else if (action.equals(TransferState.BLAST_STATE_CHANGED_ACTION)) {
                processDroidBlastState(context, intent);
            }
        }
    }

    private void processWirelessDisplayState(Context context, Intent intent) {
        notifyCircle(context, getDroidBlastState(context), getWirelessDisplayState(intent));
    }

    private void processDroidBlastState(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            notifyCircle(context, extras.getInt(TransferState.EXTRA_BLAST_STATE), getWirelessDisplayState(context));
        }
    }

    private static int getDroidBlastState(Context context) {
        return TransferState.getInstance().getBlastState();
    }

    private static int getWirelessDisplayState(Context context) {
        if (!isValidPermissions(context)) {
            return 0;
        }
        return getWirelessDisplayState(context.registerReceiver(null, new IntentFilter("com.motorola.wirelessdisplay.service.WIRELESS_DISPLAY_MIRRORING_STATE_CHANGED"), "com.motorola.wirelessdisplay.service.permission.CONFIG_WIRELESS_DISPLAY", null));
    }

    private static int getWirelessDisplayState(Intent intent) {
        if (intent == null) {
            return 0;
        }
        int state = intent.getIntExtra("wireless_display_mirroring_state", 0);
        if (!isValidWirelessDisplayState(state)) {
            return 0;
        }
        return state;
    }

    private static boolean isValidWirelessDisplayState(int state) {
        switch (state) {
            case 0:
            case 1:
            case 2:
                return true;
            default:
                return false;
        }
    }

    public static void resetCircleWidget(Context context) {
        notifyCircle(context, 0, getWirelessDisplayState(context));
    }

    private static void notifyCircle(Context context, int droidBlastState, int wirelessDisplayState) {
        String str = "com.motorola.widget.circlewidget3d.Action.SetClockNotification";
        String str2 = "com.motorola.widget.circlewidget3d.Action.HideClockNotification";
        String str3 = "com.motorola.widget.circlewidget3d.Extra.flipCircle";
        String str4 = "com.motorola.widget.circlewidget3d.Extra.texture";
        String str5 = "com.motorola.widget.circlewidget3d.Extra.upperClick";
        String str6 = "com.motorola.widget.circlewidget3d.Extra.lowerClick";
        if (DBG) {
            Log.d("Zap-CircleNotifier", "notifyCircle: " + getStateString(droidBlastState, wirelessDisplayState));
        }
        Bitmap bMap = getBitmap(context, droidBlastState, wirelessDisplayState);
        if (bMap != null) {
            Intent broadcast = new Intent("com.motorola.widget.circlewidget3d.Action.SetClockNotification");
            broadcast.putExtra("com.motorola.widget.circlewidget3d.Extra.lowerClick", getWirelessDisplayIntent(context));
            broadcast.putExtra("com.motorola.widget.circlewidget3d.Extra.upperClick", getDroidBlastIntent(context));
            broadcast.putExtra("com.motorola.widget.circlewidget3d.Extra.texture", encodeBitmap(bMap));
            boolean isShow = isShowCircle(droidBlastState, wirelessDisplayState);
            broadcast.putExtra("com.motorola.widget.circlewidget3d.Extra.flipCircle", isShow);
            sendBroadcastToCircleWidget(context, broadcast);
            if (isShow && DBG) {
                Log.d("Zap-CircleNotifier", "show circle");
            }
            if (isHideCircle(droidBlastState, wirelessDisplayState)) {
                if (DBG) {
                    Log.d("Zap-CircleNotifier", "hide circle");
                }
                sendBroadcastToCircleWidget(context, new Intent("com.motorola.widget.circlewidget3d.Action.HideClockNotification"));
            }
        } else if (Log.isLoggable("Zap-CircleNotifier", 6)) {
            Log.e("Zap-CircleNotifier", "could not get bitmap, broadcast not sent");
        }
    }

    private static String getStateString(int droidBlastState, int wirelessDisplayState) {
        StringBuilder b = new StringBuilder();
        b.append("blast: ");
        switch (droidBlastState) {
            case 0:
                b.append("idle");
                break;
            case 1:
                b.append("sending");
                break;
            case 2:
                b.append("sent");
                break;
            case 3:
                b.append("receiving");
                break;
            case 4:
                b.append("zz");
                break;
        }
        b.append("invalid");
        b.append(", wd: ");
        switch (wirelessDisplayState) {
            case 0:
                b.append("off");
                break;
            case 1:
                b.append("ready");
                break;
            case 2:
                b.append("on");
                break;
            default:
                b.append("invalid");
                break;
        }
        return b.toString();
    }

    private static boolean isShowCircle(int droidBlastState, int wirelessDisplayState) {
        return droidBlastState == 1 || droidBlastState == 2 || droidBlastState == 3 || droidBlastState == 4;
    }

    private static boolean isHideCircle(int droidBlastState, int wirelessDisplayState) {
        return droidBlastState == 0 && wirelessDisplayState == 0;
    }

    private static Bitmap getBitmap(Context context, int droidBlastState, int wirelessDisplayState) {
        return BitmapFactory.decodeResource(context.getResources(), mStateResourceIds[droidBlastState][wirelessDisplayState]);
    }

    private static byte[] encodeBitmap(Bitmap bitmap) {
        if (bitmap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            if (bitmap.compress(CompressFormat.PNG, 100, stream)) {
                return stream.toByteArray();
            }
            if (!Log.isLoggable("Zap-CircleNotifier", 6)) {
                return null;
            }
            Log.e("Zap-CircleNotifier", "could not compress bitmap");
            return null;
        } else if (!Log.isLoggable("Zap-CircleNotifier", 6)) {
            return null;
        } else {
            Log.e("Zap-CircleNotifier", "null bitmap");
            return null;
        }
    }

    private static PendingIntent getWirelessDisplayIntent(Context context) {
        Intent launchIntent = new Intent();
        launchIntent.setClassName("com.motorola.wirelessdisplay.widget", "com.motorola.wirelessdisplay.widget.WirelessDisplayWidgetProvider");
        launchIntent.addCategory("android.intent.category.ALTERNATIVE");
        launchIntent.setData(Uri.parse("custom:" + 0));
        return PendingIntent.getBroadcast(context, 0, launchIntent, 0);
    }

    private static PendingIntent getDroidBlastIntent(Context context) {
        Intent intent = new Intent(context, DroidBlastLauncher.class);
        intent.setFlags(337641472);
        intent.putExtra("circle", true);
        return PendingIntent.getActivity(context, 0, intent, 0);
    }

    private static void sendBroadcastToCircleWidget(Context context, Intent intent) {
        try {
            intent.setPackage(context.getPackageManager().getPermissionInfo("com.motorola.widget.circlewidget3d.POST_NOTIFICATIONS", 0).packageName);
            context.sendBroadcast(intent);
        } catch (NameNotFoundException e) {
            if (Log.isLoggable("Zap-CircleNotifier", 6)) {
                Log.e("Zap-CircleNotifier", "could not find circle widget package", e);
            }
        }
    }

    private static boolean isValidPermissions(Context context) {
        return ZapUtils.isValidPermission(context, "com.motorola.widget.circlewidget3d.POST_NOTIFICATIONS") && ZapUtils.isValidPermission(context, "com.motorola.wirelessdisplay.service.permission.CONFIG_WIRELESS_DISPLAY");
    }
}
