package com.google.sample.castcompanionlibrary.cast;

import android.content.Context;
import android.support.v7.app.MediaRouteDialogFactory;
import android.support.v7.media.MediaRouter.RouteInfo;
import android.text.TextUtils;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.Cast.CastOptions;
import com.google.android.gms.cast.Cast.CastOptions.Builder;
import com.google.android.gms.cast.Cast.Listener;
import com.google.android.gms.cast.Cast.MessageReceivedCallback;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.sample.castcompanionlibrary.cast.BaseCastManager.ReconnectionStatus;
import com.google.sample.castcompanionlibrary.cast.callbacks.IDataCastConsumer;
import com.google.sample.castcompanionlibrary.cast.exceptions.CastException;
import com.google.sample.castcompanionlibrary.cast.exceptions.NoConnectionException;
import com.google.sample.castcompanionlibrary.cast.exceptions.TransientNetworkDisconnectionException;
import com.google.sample.castcompanionlibrary.utils.LogUtils;
import com.google.sample.castcompanionlibrary.utils.Utils;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class DataCastManager extends BaseCastManager implements MessageReceivedCallback {
    private static final String TAG = LogUtils.makeLogTag(DataCastManager.class);
    private static DataCastManager sInstance;
    private Set<IDataCastConsumer> mDataConsumers = Collections.synchronizedSet(new HashSet());
    private final Set<String> mNamespaceList = new HashSet();

    class CastListener extends Listener {
        CastListener() {
        }

        public void onApplicationDisconnected(int statusCode) {
            DataCastManager.this.onApplicationDisconnected(statusCode);
        }

        public void onApplicationStatusChanged() {
            DataCastManager.this.onApplicationStatusChanged();
        }
    }

    public static DataCastManager initialize(Context context, String applicationId, String... namespaces) {
        if (sInstance == null) {
            LogUtils.LOGD(TAG, "New instance of DataCastManager is created");
            if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(context) != 0) {
                String msg = "Couldn't find the appropriate version of Google Play Services";
                LogUtils.LOGE(TAG, msg);
                throw new RuntimeException(msg);
            }
            sInstance = new DataCastManager(context, applicationId, namespaces);
            mCastManager = sInstance;
        }
        return sInstance;
    }

    protected DataCastManager(Context context, String applicationId, String... namespaces) {
        super(context, applicationId);
        if (namespaces != null) {
            for (String namespace : namespaces) {
                this.mNamespaceList.add(namespace);
            }
        }
    }

    public static DataCastManager getInstance() throws CastException {
        if (sInstance != null) {
            return sInstance;
        }
        LogUtils.LOGE(TAG, "No DataCastManager instance was initialized, you need to call initialize() first");
        throw new CastException();
    }

    public static DataCastManager getInstance(Context ctx) throws CastException {
        if (sInstance == null) {
            LogUtils.LOGE(TAG, "No DataCastManager instance was initialized, you need to call initialize() first");
            throw new CastException();
        }
        LogUtils.LOGD(TAG, "Updated context to: " + ctx.getClass().getName());
        sInstance.mContext = ctx;
        return sInstance;
    }

    public boolean addNamespace(String namespace) throws IllegalStateException, IOException, TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        if (TextUtils.isEmpty(namespace)) {
            throw new IllegalArgumentException("namespace cannot be empty");
        } else if (this.mNamespaceList.contains(namespace)) {
            LogUtils.LOGD(TAG, "Ignoring to add a namespace that is already added.");
            return false;
        } else {
            try {
                Cast.CastApi.setMessageReceivedCallbacks(this.mApiClient, namespace, this);
                this.mNamespaceList.add(namespace);
                return true;
            } catch (IOException e) {
                LogUtils.LOGE(TAG, "Failed to add namespace", e);
                return false;
            } catch (IllegalStateException e2) {
                LogUtils.LOGE(TAG, "Failed to add namespace", e2);
                return false;
            }
        }
    }

    public boolean removeNamespace(String namespace) throws TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        if (TextUtils.isEmpty(namespace)) {
            throw new IllegalArgumentException("namespace cannot be empty");
        } else if (!this.mNamespaceList.contains(namespace)) {
            LogUtils.LOGD(TAG, "Ignoring to remove a namespace that is not registered.");
            return false;
        } else {
            try {
                Cast.CastApi.removeMessageReceivedCallbacks(this.mApiClient, namespace);
                this.mNamespaceList.remove(namespace);
                return true;
            } catch (IOException e) {
                LogUtils.LOGE(TAG, "Failed to remove namespace: " + namespace, e);
                return false;
            } catch (IllegalStateException e2) {
                LogUtils.LOGE(TAG, "Failed to remove namespace: " + namespace, e2);
                return false;
            }
        }
    }

    public void sendDataMessage(String message, String namespace) throws IllegalArgumentException, IllegalStateException, IOException, TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        if (TextUtils.isEmpty(namespace)) {
            throw new IllegalArgumentException("namespace cannot be empty");
        }
        Cast.CastApi.sendMessage(this.mApiClient, namespace, message).setResultCallback(new ResultCallback<Status>() {
            public void onResult(Status result) {
                if (!result.isSuccess()) {
                    DataCastManager.this.onMessageSendFailed(result);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDeviceUnselected() {
        detachDataChannels();
    }

    /* access modifiers changed from: protected */
    public Builder getCastOptionBuilder(CastDevice device) {
        Builder builder = CastOptions.builder(this.mSelectedCastDevice, new CastListener());
        if (isFeatureEnabled(1)) {
            builder.setVerboseLoggingEnabled(true);
        }
        return builder;
    }

    /* access modifiers changed from: protected */
    public MediaRouteDialogFactory getMediaRouteDialogFactory() {
        return null;
    }

    public void onApplicationConnected(ApplicationMetadata appMetadata, String applicationStatus, String sessionId, boolean wasLaunched) {
        LogUtils.LOGD(TAG, "onApplicationConnected() reached with sessionId: " + sessionId);
        Utils.saveStringToPreference(this.mContext, BaseCastManager.PREFS_KEY_SESSION_ID, sessionId);
        if (this.mReconnectionStatus == ReconnectionStatus.IN_PROGRESS) {
            List<RouteInfo> routes = this.mMediaRouter.getRoutes();
            if (routes != null) {
                String routeId = Utils.getStringFromPreference(this.mContext, BaseCastManager.PREFS_KEY_ROUTE_ID);
                boolean found = false;
                Iterator i$ = routes.iterator();
                while (true) {
                    if (!i$.hasNext()) {
                        break;
                    }
                    RouteInfo routeInfo = (RouteInfo) i$.next();
                    if (routeId.equals(routeInfo.getId())) {
                        LogUtils.LOGD(TAG, "Found the correct route during reconnection attempt");
                        found = true;
                        this.mReconnectionStatus = ReconnectionStatus.FINALIZE;
                        this.mMediaRouter.selectRoute(routeInfo);
                        break;
                    }
                }
                if (!found) {
                    onDeviceSelected(null);
                    this.mReconnectionStatus = ReconnectionStatus.INACTIVE;
                    return;
                }
            }
        }
        try {
            attachDataChannels();
            this.mSessionId = sessionId;
            synchronized (this.mDataConsumers) {
                for (IDataCastConsumer consumer : this.mDataConsumers) {
                    try {
                        consumer.onApplicationConnected(appMetadata, applicationStatus, sessionId, wasLaunched);
                    } catch (Exception e) {
                        LogUtils.LOGE(TAG, "onApplicationConnected(): Failed to inform " + consumer, e);
                    }
                }
            }
        } catch (IllegalStateException e2) {
            LogUtils.LOGE(TAG, "Failed to attach namespaces", e2);
        } catch (IOException e3) {
            LogUtils.LOGE(TAG, "Failed to attach namespaces", e3);
        } catch (TransientNetworkDisconnectionException e4) {
            LogUtils.LOGE(TAG, "Failed to attach namespaces", e4);
        } catch (NoConnectionException e5) {
            LogUtils.LOGE(TAG, "Failed to attach namespaces", e5);
        }
    }

    private void attachDataChannels() throws IllegalStateException, IOException, TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        if (!this.mNamespaceList.isEmpty() && Cast.CastApi != null) {
            for (String namespace : this.mNamespaceList) {
                Cast.CastApi.setMessageReceivedCallbacks(this.mApiClient, namespace, this);
            }
        }
    }

    private void detachDataChannels() {
        if (!this.mNamespaceList.isEmpty() && Cast.CastApi != null && this.mApiClient != null) {
            for (String namespace : this.mNamespaceList) {
                try {
                    Cast.CastApi.removeMessageReceivedCallbacks(this.mApiClient, namespace);
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, "Failed to add namespace: " + namespace, e);
                }
            }
        }
    }

    public void onApplicationConnectionFailed(int errorCode) {
        onDeviceSelected(null);
        synchronized (this.mDataConsumers) {
            for (IDataCastConsumer consumer : this.mDataConsumers) {
                try {
                    consumer.onApplicationConnectionFailed(errorCode);
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, "onApplicationConnectionFailed(): Failed to inform " + consumer, e);
                }
            }
        }
    }

    public void onApplicationDisconnected(int errorCode) {
        synchronized (this.mDataConsumers) {
            for (IDataCastConsumer consumer : this.mDataConsumers) {
                try {
                    consumer.onApplicationDisconnected(errorCode);
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, "onApplicationDisconnected(): Failed to inform " + consumer, e);
                }
            }
        }
        if (this.mMediaRouter != null) {
            this.mMediaRouter.selectRoute(this.mMediaRouter.getDefaultRoute());
        }
        onDeviceSelected(null);
    }

    public void onApplicationStatusChanged() {
        if (isConnected()) {
            try {
                String appStatus = Cast.CastApi.getApplicationStatus(this.mApiClient);
                LogUtils.LOGD(TAG, "onApplicationStatusChanged() reached: " + Cast.CastApi.getApplicationStatus(this.mApiClient));
                synchronized (this.mDataConsumers) {
                    for (IDataCastConsumer consumer : this.mDataConsumers) {
                        try {
                            consumer.onApplicationStatusChanged(appStatus);
                        } catch (Exception e) {
                            LogUtils.LOGE(TAG, "onApplicationStatusChanged(): Failed to inform " + consumer, e);
                        }
                    }
                }
            } catch (IllegalStateException e2) {
                LogUtils.LOGE(TAG, "onApplicationStatusChanged(): Failed", e2);
            }
        }
    }

    public void onApplicationStopFailed(int errorCode) {
        synchronized (this.mDataConsumers) {
            for (IDataCastConsumer consumer : this.mDataConsumers) {
                try {
                    consumer.onApplicationStopFailed(errorCode);
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, "onApplicationStopFailed(): Failed to inform " + consumer, e);
                }
            }
        }
    }

    public void onVolumeChanged() {
    }

    public void onMessageReceived(CastDevice castDevice, String namespace, String message) {
        synchronized (this.mDataConsumers) {
            for (IDataCastConsumer consumer : this.mDataConsumers) {
                try {
                    consumer.onMessageReceived(castDevice, namespace, message);
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, "onMessageReceived(): Failed to inform " + consumer, e);
                }
            }
        }
    }

    public void onMessageSendFailed(Status result) {
        synchronized (this.mDataConsumers) {
            for (IDataCastConsumer consumer : this.mDataConsumers) {
                try {
                    consumer.onMessageSendFailed(result);
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, "onMessageSendFailed(): Failed to inform " + consumer, e);
                }
            }
        }
    }

    public void addDataCastConsumer(IDataCastConsumer listener) {
        boolean result;
        if (listener != null) {
            super.addBaseCastConsumer(listener);
            synchronized (this.mDataConsumers) {
                result = this.mDataConsumers.add(listener);
            }
            if (result) {
                LogUtils.LOGD(TAG, "Successfully added the new DataCastConsumer listener " + listener);
            } else {
                LogUtils.LOGD(TAG, "Adding Listener " + listener + " was already registered, " + "skipping this step");
            }
        }
    }

    public void removeDataCastConsumer(IDataCastConsumer listener) {
        if (listener != null) {
            super.removeBaseCastConsumer(listener);
            synchronized (this.mDataConsumers) {
                this.mDataConsumers.remove(listener);
            }
        }
    }
}
