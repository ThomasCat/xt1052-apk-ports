package com.google.android.gms.common.api;

import android.os.DeadObjectException;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.internal.er;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class a {

    /* renamed from: com.google.android.gms.common.api.a$a reason: collision with other inner class name */
    public static abstract class C0003a<R extends Result, A extends com.google.android.gms.common.api.Api.a> implements PendingResult<R>, c<R>, c<A> {
        private final com.google.android.gms.common.api.Api.b<A> zc;
        private final Object zd;
        private b<R> ze;
        private final CountDownLatch zf;
        private final ArrayList<com.google.android.gms.common.api.PendingResult.a> zg;
        private ResultCallback<R> zh;
        private volatile R zi;
        private volatile boolean zj;
        private boolean zk;
        private boolean zl;
        private a zm;

        protected C0003a() {
            this(null);
        }

        protected C0003a(com.google.android.gms.common.api.Api.b<A> bVar) {
            this.zd = new Object();
            this.zf = new CountDownLatch(1);
            this.zg = new ArrayList<>();
            this.zc = bVar;
        }

        private void a(RemoteException remoteException) {
            b((R) d(new Status(8, remoteException.getLocalizedMessage(), null)));
        }

        private R ds() {
            R r;
            synchronized (this.zd) {
                er.a(!this.zj, "Result has already been consumed.");
                er.a(isReady(), "Result is not ready.");
                r = this.zi;
                dt();
            }
            return r;
        }

        private void dv() {
            if (this.zi != null && (this instanceof Releasable)) {
                try {
                    ((Releasable) this).release();
                } catch (Exception e) {
                    Log.w("GoogleApi", "Unable to release " + this, e);
                }
            }
        }

        /* access modifiers changed from: protected */
        public abstract void a(A a) throws RemoteException;

        /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
            return;
         */
        /* renamed from: a */
        public final void b(R r) {
            boolean z = true;
            synchronized (this.zd) {
                if (!this.zl) {
                    er.a(!isReady(), "Results have already been set");
                    if (this.zj) {
                        z = false;
                    }
                    er.a(z, "Result has already been consumed");
                    this.zi = r;
                    if (this.zk) {
                        dv();
                        return;
                    }
                    this.zf.countDown();
                    Status status = this.zi.getStatus();
                    if (this.zh != null) {
                        this.ze.dw();
                        this.ze.a(this.zh, ds());
                    }
                    Iterator it = this.zg.iterator();
                    while (it.hasNext()) {
                        ((com.google.android.gms.common.api.PendingResult.a) it.next()).k(status);
                    }
                    this.zg.clear();
                } else if (r instanceof Releasable) {
                    ((Releasable) r).release();
                }
            }
        }

        public void a(a aVar) {
            this.zm = aVar;
        }

        public final R await() {
            boolean z = false;
            er.a(!this.zj, "Results has already been consumed");
            if (isReady() || Looper.myLooper() != Looper.getMainLooper()) {
                z = true;
            }
            er.a(z, "await must not be called on the UI thread");
            try {
                this.zf.await();
            } catch (InterruptedException e) {
                synchronized (this.zd) {
                    b((R) d(Status.zR));
                    this.zl = true;
                }
            }
            er.a(isReady(), "Result is not ready.");
            return ds();
        }

        public final R await(long time, TimeUnit units) {
            boolean z = false;
            er.a(!this.zj, "Result has already been consumed.");
            if (isReady() || Looper.myLooper() != Looper.getMainLooper()) {
                z = true;
            }
            er.a(z, "await must not be called on the UI thread");
            try {
                if (!this.zf.await(time, units)) {
                    synchronized (this.zd) {
                        b((R) d(Status.zS));
                        this.zl = true;
                    }
                }
            } catch (InterruptedException e) {
                synchronized (this.zd) {
                    b((R) d(Status.zR));
                    this.zl = true;
                }
            }
            er.a(isReady(), "Result is not ready.");
            return ds();
        }

        public final void b(A a) throws DeadObjectException {
            this.ze = new b<>(a.getLooper());
            try {
                a(a);
            } catch (DeadObjectException e) {
                a((RemoteException) e);
                throw e;
            } catch (RemoteException e2) {
                a(e2);
            }
        }

        /* access modifiers changed from: protected */
        public abstract R d(Status status);

        public final com.google.android.gms.common.api.Api.b<A> dp() {
            return this.zc;
        }

        public int dr() {
            return 0;
        }

        /* access modifiers changed from: 0000 */
        public void dt() {
            this.zj = true;
            this.zi = null;
            if (this.zm != null) {
                this.zm.b(this);
            }
        }

        public void du() {
            dv();
            this.zk = true;
        }

        public final boolean isReady() {
            return this.zf.getCount() == 0;
        }

        public final void setResultCallback(ResultCallback<R> callback) {
            er.a(!this.zj, "Result has already been consumed.");
            synchronized (this.zd) {
                if (isReady()) {
                    this.ze.a(callback, ds());
                } else {
                    this.zh = callback;
                }
            }
        }

        public final void setResultCallback(ResultCallback<R> callback, long time, TimeUnit units) {
            er.a(!this.zj, "Result has already been consumed.");
            synchronized (this.zd) {
                if (isReady()) {
                    this.ze.a(callback, ds());
                } else {
                    this.zh = callback;
                    this.ze.a(this, units.toMillis(time));
                }
            }
        }
    }

    public static class b<R extends Result> extends Handler {
        public b() {
            this(Looper.getMainLooper());
        }

        public b(Looper looper) {
            super(looper);
        }

        public void a(ResultCallback<R> resultCallback, R r) {
            sendMessage(obtainMessage(1, new Pair(resultCallback, r)));
        }

        public void a(C0003a<R, ?> aVar, long j) {
            sendMessageDelayed(obtainMessage(2, aVar), j);
        }

        /* access modifiers changed from: protected */
        public void b(ResultCallback<R> resultCallback, R r) {
            resultCallback.onResult(r);
        }

        public void dw() {
            removeMessages(2);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Pair pair = (Pair) msg.obj;
                    b((ResultCallback) pair.first, (Result) pair.second);
                    return;
                case 2:
                    C0003a aVar = (C0003a) msg.obj;
                    aVar.b(aVar.d(Status.zS));
                    return;
                default:
                    Log.wtf("GoogleApi", "Don't know how to handle this message.");
                    return;
            }
        }
    }

    public interface c<R> {
        void b(R r);
    }
}
