package com.google.android.gms.tagmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.google.android.gms.internal.fl;
import com.google.android.gms.internal.fn;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

class v implements c {
    /* access modifiers changed from: private */
    public static final String UD = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' STRING NOT NULL, '%s' BLOB NOT NULL, '%s' INTEGER NOT NULL);", new Object[]{"datalayer", "ID", "key", "value", "expires"});
    private fl Ty;
    private final Executor UE;
    private a UF;
    private int UG;
    /* access modifiers changed from: private */
    public final Context mContext;

    class a extends SQLiteOpenHelper {
        a(Context context, String str) {
            super(context, str, null, 1);
        }

        /* JADX INFO: finally extract failed */
        private void a(SQLiteDatabase sQLiteDatabase) {
            Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT * FROM datalayer WHERE 0", null);
            HashSet hashSet = new HashSet();
            try {
                String[] columnNames = rawQuery.getColumnNames();
                for (String add : columnNames) {
                    hashSet.add(add);
                }
                rawQuery.close();
                if (!hashSet.remove("key") || !hashSet.remove("value") || !hashSet.remove("ID") || !hashSet.remove("expires")) {
                    throw new SQLiteException("Database column missing");
                } else if (!hashSet.isEmpty()) {
                    throw new SQLiteException("Database has extra columns");
                }
            } catch (Throwable th) {
                rawQuery.close();
                throw th;
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0048  */
        private boolean a(String str, SQLiteDatabase sQLiteDatabase) {
            Cursor cursor;
            Cursor cursor2 = null;
            try {
                SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
                Cursor query = sQLiteDatabase2.query("SQLITE_MASTER", new String[]{"name"}, "name=?", new String[]{str}, null, null, null);
                try {
                    boolean moveToFirst = query.moveToFirst();
                    if (query == null) {
                        return moveToFirst;
                    }
                    query.close();
                    return moveToFirst;
                } catch (SQLiteException e) {
                    cursor = query;
                } catch (Throwable th) {
                    th = th;
                    cursor2 = query;
                    if (cursor2 != null) {
                    }
                    throw th;
                }
            } catch (SQLiteException e2) {
                cursor = null;
            } catch (Throwable th2) {
                th = th2;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
            try {
                bh.w("Error querying for table " + str);
                if (cursor != null) {
                    cursor.close();
                }
                return false;
            } catch (Throwable th3) {
                cursor2 = cursor;
                th = th3;
                if (cursor2 != null) {
                }
                throw th;
            }
        }

        public SQLiteDatabase getWritableDatabase() {
            SQLiteDatabase sQLiteDatabase = null;
            try {
                sQLiteDatabase = super.getWritableDatabase();
            } catch (SQLiteException e) {
                v.this.mContext.getDatabasePath("google_tagmanager.db").delete();
            }
            return sQLiteDatabase == null ? super.getWritableDatabase() : sQLiteDatabase;
        }

        public void onCreate(SQLiteDatabase db) {
            ak.B(db.getPath());
        }

        public void onOpen(SQLiteDatabase db) {
            if (VERSION.SDK_INT < 15) {
                Cursor rawQuery = db.rawQuery("PRAGMA journal_mode=memory", null);
                try {
                    rawQuery.moveToFirst();
                } finally {
                    rawQuery.close();
                }
            }
            if (!a("datalayer", db)) {
                db.execSQL(v.UD);
            } else {
                a(db);
            }
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    private static class b {
        final String UA;
        final byte[] UM;

        b(String str, byte[] bArr) {
            this.UA = str;
            this.UM = bArr;
        }

        public String toString() {
            return "KeyAndSerialized: key = " + this.UA + " serialized hash = " + Arrays.hashCode(this.UM);
        }
    }

    public v(Context context) {
        this(context, fn.eI(), "google_tagmanager.db", 2000, Executors.newSingleThreadExecutor());
    }

    v(Context context, fl flVar, String str, int i, Executor executor) {
        this.mContext = context;
        this.Ty = flVar;
        this.UG = i;
        this.UE = executor;
        this.UF = new a(this.mContext, str);
    }

    private SQLiteDatabase G(String str) {
        try {
            return this.UF.getWritableDatabase();
        } catch (SQLiteException e) {
            bh.w(str);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void b(List<b> list, long j) {
        try {
            long currentTimeMillis = this.Ty.currentTimeMillis();
            t(currentTimeMillis);
            bQ(list.size());
            c(list, currentTimeMillis + j);
            iW();
        } catch (Throwable th) {
            iW();
            throw th;
        }
    }

    private void bQ(int i) {
        int iV = (iV() - this.UG) + i;
        if (iV > 0) {
            List bR = bR(iV);
            bh.u("DataLayer store full, deleting " + bR.size() + " entries to make room.");
            g((String[]) bR.toArray(new String[0]));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0082  */
    private List<String> bR(int i) {
        Cursor cursor;
        ArrayList arrayList = new ArrayList();
        if (i <= 0) {
            bh.w("Invalid maxEntries specified. Skipping.");
            return arrayList;
        }
        SQLiteDatabase G = G("Error opening database for peekEntryIds.");
        if (G == null) {
            return arrayList;
        }
        try {
            cursor = G.query("datalayer", new String[]{"ID"}, null, null, null, null, String.format("%s ASC", new Object[]{"ID"}), Integer.toString(i));
            try {
                if (cursor.moveToFirst()) {
                    do {
                        arrayList.add(String.valueOf(cursor.getLong(0)));
                    } while (cursor.moveToNext());
                }
                if (cursor != null) {
                    cursor.close();
                }
            } catch (SQLiteException e) {
                e = e;
                try {
                    bh.w("Error in peekEntries fetching entryIds: " + e.getMessage());
                    if (cursor != null) {
                        cursor.close();
                    }
                    return arrayList;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e2) {
            e = e2;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void bj(String str) {
        SQLiteDatabase G = G("Error opening database for clearKeysWithPrefix.");
        if (G != null) {
            try {
                bh.v("Cleared " + G.delete("datalayer", "key = ? OR key LIKE ?", new String[]{str, str + ".%"}) + " items");
            } catch (SQLiteException e) {
                bh.w("Error deleting entries with key prefix: " + str + " (" + e + ").");
            } finally {
                iW();
            }
        }
    }

    private List<a> c(List<b> list) {
        ArrayList arrayList = new ArrayList();
        for (b bVar : list) {
            arrayList.add(new a(bVar.UA, j(bVar.UM)));
        }
        return arrayList;
    }

    private void c(List<b> list, long j) {
        SQLiteDatabase G = G("Error opening database for writeEntryToDatabase.");
        if (G != null) {
            for (b bVar : list) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("expires", Long.valueOf(j));
                contentValues.put("key", bVar.UA);
                contentValues.put("value", bVar.UM);
                G.insert("datalayer", null, contentValues);
            }
        }
    }

    private List<b> d(List<a> list) {
        ArrayList arrayList = new ArrayList();
        for (a aVar : list) {
            arrayList.add(new b(aVar.UA, j(aVar.UB)));
        }
        return arrayList;
    }

    private void g(String[] strArr) {
        if (strArr != null && strArr.length != 0) {
            SQLiteDatabase G = G("Error opening database for deleteEntries.");
            if (G != null) {
                try {
                    G.delete("datalayer", String.format("%s in (%s)", new Object[]{"ID", TextUtils.join(",", Collections.nCopies(strArr.length, "?"))}), strArr);
                } catch (SQLiteException e) {
                    bh.w("Error deleting entries " + Arrays.toString(strArr));
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public List<a> iT() {
        try {
            t(this.Ty.currentTimeMillis());
            return c(iU());
        } finally {
            iW();
        }
    }

    /* JADX INFO: finally extract failed */
    private List<b> iU() {
        SQLiteDatabase G = G("Error opening database for loadSerialized.");
        ArrayList arrayList = new ArrayList();
        if (G == null) {
            return arrayList;
        }
        Cursor query = G.query("datalayer", new String[]{"key", "value"}, null, null, null, null, "ID", null);
        while (query.moveToNext()) {
            try {
                arrayList.add(new b(query.getString(0), query.getBlob(1)));
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        query.close();
        return arrayList;
    }

    private int iV() {
        Cursor cursor = null;
        int i = 0;
        SQLiteDatabase G = G("Error opening database for getNumStoredEntries.");
        if (G != null) {
            try {
                Cursor rawQuery = G.rawQuery("SELECT COUNT(*) from datalayer", null);
                if (rawQuery.moveToFirst()) {
                    i = (int) rawQuery.getLong(0);
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } catch (SQLiteException e) {
                bh.w("Error getting numStoredEntries");
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
        return i;
    }

    private void iW() {
        try {
            this.UF.close();
        } catch (SQLiteException e) {
        }
    }

    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0029 A[SYNTHETIC, Splitter:B:20:0x0029] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0038 A[SYNTHETIC, Splitter:B:27:0x0038] */
    /* JADX WARNING: Unknown variable types count: 1 */
    private Object j(byte[] bArr) {
        ObjectInputStream objectInputStream;
        Throwable th;
        ObjectInputStream objectInputStream2;
        ObjectInputStream objectInputStream3;
        Object obj;
        ? r0 = 0;
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        try {
            ObjectInputStream objectInputStream4 = new ObjectInputStream(byteArrayInputStream);
            try {
                Object readObject = objectInputStream4.readObject();
                if (objectInputStream4 != 0) {
                    try {
                        objectInputStream4.close();
                    } catch (IOException e) {
                        obj = readObject;
                    }
                }
                byteArrayInputStream.close();
                obj = readObject;
            } catch (IOException e2) {
                objectInputStream3 = objectInputStream4;
            } catch (ClassNotFoundException e3) {
                objectInputStream2 = objectInputStream4;
                if (objectInputStream2 != 0) {
                }
                byteArrayInputStream.close();
                obj = r0;
                return obj;
            } catch (Throwable th2) {
                th = th2;
                objectInputStream = objectInputStream4;
                if (objectInputStream != 0) {
                }
                byteArrayInputStream.close();
                throw th;
            }
        } catch (IOException e4) {
            objectInputStream3 = r0;
            if (objectInputStream3 != 0) {
                try {
                    objectInputStream3.close();
                } catch (IOException e5) {
                    obj = r0;
                }
            }
            byteArrayInputStream.close();
            obj = r0;
            return obj;
        } catch (ClassNotFoundException e6) {
            objectInputStream2 = r0;
            if (objectInputStream2 != 0) {
                try {
                    objectInputStream2.close();
                } catch (IOException e7) {
                    obj = r0;
                }
            }
            byteArrayInputStream.close();
            obj = r0;
            return obj;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            objectInputStream = r0;
            th = th4;
            if (objectInputStream != 0) {
                try {
                    objectInputStream.close();
                } catch (IOException e8) {
                    throw th;
                }
            }
            byteArrayInputStream.close();
            throw th;
        }
        return obj;
    }

    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x002e A[SYNTHETIC, Splitter:B:20:0x002e] */
    /* JADX WARNING: Unknown variable types count: 1 */
    private byte[] j(Object obj) {
        ObjectOutputStream objectOutputStream;
        Throwable th;
        ObjectOutputStream objectOutputStream2;
        byte[] bArr;
        ? r0 = 0;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ObjectOutputStream objectOutputStream3 = new ObjectOutputStream(byteArrayOutputStream);
            try {
                objectOutputStream3.writeObject(obj);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                if (objectOutputStream3 != 0) {
                    try {
                        objectOutputStream3.close();
                    } catch (IOException e) {
                        bArr = byteArray;
                    }
                }
                byteArrayOutputStream.close();
                bArr = byteArray;
            } catch (IOException e2) {
                objectOutputStream2 = objectOutputStream3;
            } catch (Throwable th2) {
                th = th2;
                objectOutputStream = objectOutputStream3;
                if (objectOutputStream != 0) {
                    try {
                        objectOutputStream.close();
                    } catch (IOException e3) {
                        throw th;
                    }
                }
                byteArrayOutputStream.close();
                throw th;
            }
        } catch (IOException e4) {
            objectOutputStream2 = r0;
            if (objectOutputStream2 != 0) {
                try {
                    objectOutputStream2.close();
                } catch (IOException e5) {
                    bArr = r0;
                }
            }
            byteArrayOutputStream.close();
            bArr = r0;
            return bArr;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            objectOutputStream = r0;
            th = th4;
            if (objectOutputStream != 0) {
            }
            byteArrayOutputStream.close();
            throw th;
        }
        return bArr;
    }

    private void t(long j) {
        SQLiteDatabase G = G("Error opening database for deleteOlderThan.");
        if (G != null) {
            try {
                bh.v("Deleted " + G.delete("datalayer", "expires <= ?", new String[]{Long.toString(j)}) + " expired items");
            } catch (SQLiteException e) {
                bh.w("Error deleting old entries.");
            }
        }
    }

    public void a(final com.google.android.gms.tagmanager.DataLayer.c.a aVar) {
        this.UE.execute(new Runnable() {
            public void run() {
                aVar.b(v.this.iT());
            }
        });
    }

    public void a(List<a> list, final long j) {
        final List d = d(list);
        this.UE.execute(new Runnable() {
            public void run() {
                v.this.b(d, j);
            }
        });
    }

    public void bi(final String str) {
        this.UE.execute(new Runnable() {
            public void run() {
                v.this.bj(str);
            }
        });
    }
}
