package com.google.android.gms.analytics;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.google.analytics.tracking.android.Fields;
import com.google.android.gms.analytics.internal.Command;
import com.google.android.gms.internal.di;
import com.motorola.sharing.Constants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.http.impl.client.DefaultHttpClient;

class ac implements d {
    /* access modifiers changed from: private */
    public static final String ua = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL, '%s' TEXT NOT NULL, '%s' INTEGER);", new Object[]{"hits2", "hit_id", "hit_time", "hit_url", "hit_string", "hit_app_id"});
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public i rJ;
    private final e rr;
    private final a ub;
    private volatile n uc;
    /* access modifiers changed from: private */
    public final String ud;
    private ab ue;
    private long uf;
    private final int ug;

    class a extends SQLiteOpenHelper {
        private boolean ui;
        private long uj = 0;

        a(Context context, String str) {
            super(context, str, null, 1);
        }

        /* JADX INFO: finally extract failed */
        private void a(SQLiteDatabase sQLiteDatabase) {
            boolean z = false;
            Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT * FROM hits2 WHERE 0", null);
            HashSet hashSet = new HashSet();
            try {
                String[] columnNames = rawQuery.getColumnNames();
                for (String add : columnNames) {
                    hashSet.add(add);
                }
                rawQuery.close();
                if (!hashSet.remove("hit_id") || !hashSet.remove("hit_url") || !hashSet.remove("hit_string") || !hashSet.remove("hit_time")) {
                    throw new SQLiteException("Database column missing");
                }
                if (!hashSet.remove("hit_app_id")) {
                    z = true;
                }
                if (!hashSet.isEmpty()) {
                    throw new SQLiteException("Database has extra columns");
                } else if (z) {
                    sQLiteDatabase.execSQL("ALTER TABLE hits2 ADD COLUMN hit_app_id");
                }
            } catch (Throwable th) {
                rawQuery.close();
                throw th;
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0048  */
        private boolean a(String str, SQLiteDatabase sQLiteDatabase) {
            Cursor cursor;
            Cursor cursor2 = null;
            try {
                SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
                Cursor query = sQLiteDatabase2.query("SQLITE_MASTER", new String[]{"name"}, "name=?", new String[]{str}, null, null, null);
                try {
                    boolean moveToFirst = query.moveToFirst();
                    if (query == null) {
                        return moveToFirst;
                    }
                    query.close();
                    return moveToFirst;
                } catch (SQLiteException e) {
                    cursor = query;
                } catch (Throwable th) {
                    th = th;
                    cursor2 = query;
                    if (cursor2 != null) {
                    }
                    throw th;
                }
            } catch (SQLiteException e2) {
                cursor = null;
            } catch (Throwable th2) {
                th = th2;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
            try {
                aa.w("Error querying for table " + str);
                if (cursor != null) {
                    cursor.close();
                }
                return false;
            } catch (Throwable th3) {
                cursor2 = cursor;
                th = th3;
                if (cursor2 != null) {
                }
                throw th;
            }
        }

        public SQLiteDatabase getWritableDatabase() {
            if (!this.ui || this.uj + 3600000 <= ac.this.rJ.currentTimeMillis()) {
                SQLiteDatabase sQLiteDatabase = null;
                this.ui = true;
                this.uj = ac.this.rJ.currentTimeMillis();
                try {
                    sQLiteDatabase = super.getWritableDatabase();
                } catch (SQLiteException e) {
                    ac.this.mContext.getDatabasePath(ac.this.ud).delete();
                }
                if (sQLiteDatabase == null) {
                    sQLiteDatabase = super.getWritableDatabase();
                }
                this.ui = false;
                return sQLiteDatabase;
            }
            throw new SQLiteException("Database creation failed");
        }

        public void onCreate(SQLiteDatabase db) {
            p.B(db.getPath());
        }

        public void onOpen(SQLiteDatabase db) {
            if (VERSION.SDK_INT < 15) {
                Cursor rawQuery = db.rawQuery("PRAGMA journal_mode=memory", null);
                try {
                    rawQuery.moveToFirst();
                } finally {
                    rawQuery.close();
                }
            }
            if (!a("hits2", db)) {
                db.execSQL(ac.ua);
            } else {
                a(db);
            }
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    ac(e eVar, Context context) {
        this(eVar, context, "google_analytics_v4.db", 2000);
    }

    ac(e eVar, Context context, String str, int i) {
        this.mContext = context.getApplicationContext();
        this.ud = str;
        this.rr = eVar;
        this.rJ = new i() {
            public long currentTimeMillis() {
                return System.currentTimeMillis();
            }
        };
        this.ub = new a(this.mContext, this.ud);
        this.uc = new ah(new DefaultHttpClient(), this.mContext);
        this.uf = 0;
        this.ug = i;
    }

    private SQLiteDatabase G(String str) {
        try {
            return this.ub.getWritableDatabase();
        } catch (SQLiteException e) {
            aa.w(str);
            return null;
        }
    }

    private void a(Map<String, String> map, long j, String str) {
        long j2;
        SQLiteDatabase G = G("Error opening database for putHit");
        if (G != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("hit_string", t(map));
            contentValues.put("hit_time", Long.valueOf(j));
            if (map.containsKey(Fields.ANDROID_APP_UID)) {
                try {
                    j2 = Long.parseLong((String) map.get(Fields.ANDROID_APP_UID));
                } catch (NumberFormatException e) {
                    j2 = 0;
                }
            } else {
                j2 = 0;
            }
            contentValues.put("hit_app_id", Long.valueOf(j2));
            if (str == null) {
                str = "http://www.google-analytics.com/collect";
            }
            if (str.length() == 0) {
                aa.w("Empty path: not sending hit");
                return;
            }
            contentValues.put("hit_url", str);
            try {
                G.insert("hits2", null, contentValues);
                this.rr.p(false);
            } catch (SQLiteException e2) {
                aa.w("Error storing hit");
            }
        }
    }

    private void a(Map<String, String> map, Collection<di> collection) {
        String substring = "&_v".substring(1);
        if (collection != null) {
            for (di diVar : collection) {
                if (Command.APPEND_VERSION.equals(diVar.getId())) {
                    map.put(substring, diVar.getValue());
                    return;
                }
            }
        }
    }

    private void co() {
        int cq = (cq() - this.ug) + 1;
        if (cq > 0) {
            List s = s(cq);
            aa.v("Store full, deleting " + s.size() + " hits to make room.");
            a((String[]) s.toArray(new String[0]));
        }
    }

    static String t(Map<String, String> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Entry entry : map.entrySet()) {
            arrayList.add(y.encode((String) entry.getKey()) + "=" + y.encode((String) entry.getValue()));
        }
        return TextUtils.join("&", arrayList);
    }

    public void a(Map<String, String> map, long j, String str, Collection<di> collection) {
        cp();
        co();
        a(map, collection);
        a(map, j, str);
    }

    /* access modifiers changed from: 0000 */
    public void a(String[] strArr) {
        boolean z = true;
        if (strArr == null || strArr.length == 0) {
            aa.w("Empty hitIds passed to deleteHits.");
            return;
        }
        SQLiteDatabase G = G("Error opening database for deleteHits.");
        if (G != null) {
            try {
                G.delete("hits2", String.format("HIT_ID in (%s)", new Object[]{TextUtils.join(",", Collections.nCopies(strArr.length, "?"))}), strArr);
                e eVar = this.rr;
                if (cq() != 0) {
                    z = false;
                }
                eVar.p(z);
            } catch (SQLiteException e) {
                aa.w("Error deleting hits " + strArr);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @Deprecated
    public void b(Collection<x> collection) {
        if (collection == null || collection.isEmpty()) {
            aa.w("Empty/Null collection passed to deleteHits.");
            return;
        }
        String[] strArr = new String[collection.size()];
        int i = 0;
        for (x ci : collection) {
            int i2 = i + 1;
            strArr[i] = String.valueOf(ci.ci());
            i = i2;
        }
        a(strArr);
    }

    public void bp() {
        boolean z = true;
        aa.v("Dispatch running...");
        if (this.uc.bA()) {
            List t = t(40);
            if (t.isEmpty()) {
                aa.v("...nothing to dispatch");
                this.rr.p(true);
                return;
            }
            if (this.ue == null) {
                this.ue = new ab("_t=dispatch&_v=ma4.0.0", true);
            }
            if (cq() > t.size()) {
                z = false;
            }
            int a2 = this.uc.a(t, this.ue, z);
            aa.v("sent " + a2 + " of " + t.size() + " hits");
            b((Collection<x>) t.subList(0, Math.min(a2, t.size())));
            if (a2 != t.size() || cq() <= 0) {
                this.ue = null;
            } else {
                GoogleAnalytics.getInstance(this.mContext).dispatchLocalHits();
            }
        }
    }

    public n bq() {
        return this.uc;
    }

    /* access modifiers changed from: 0000 */
    public int cp() {
        boolean z = true;
        long currentTimeMillis = this.rJ.currentTimeMillis();
        if (currentTimeMillis <= this.uf + Constants.DAY) {
            return 0;
        }
        this.uf = currentTimeMillis;
        SQLiteDatabase G = G("Error opening database for deleteStaleHits.");
        if (G == null) {
            return 0;
        }
        int delete = G.delete("hits2", "HIT_TIME < ?", new String[]{Long.toString(this.rJ.currentTimeMillis() - 2592000000L)});
        e eVar = this.rr;
        if (cq() != 0) {
            z = false;
        }
        eVar.p(z);
        return delete;
    }

    /* access modifiers changed from: 0000 */
    public int cq() {
        Cursor cursor = null;
        int i = 0;
        SQLiteDatabase G = G("Error opening database for getNumStoredHits.");
        if (G != null) {
            try {
                Cursor rawQuery = G.rawQuery("SELECT COUNT(*) from hits2", null);
                if (rawQuery.moveToFirst()) {
                    i = (int) rawQuery.getLong(0);
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } catch (SQLiteException e) {
                aa.w("Error getting numStoredHits");
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
        return i;
    }

    public void i(long j) {
        boolean z = true;
        SQLiteDatabase G = G("Error opening database for clearHits");
        if (G != null) {
            if (j == 0) {
                G.delete("hits2", null, null);
            } else {
                G.delete("hits2", "hit_app_id = ?", new String[]{Long.valueOf(j).toString()});
            }
            e eVar = this.rr;
            if (cq() != 0) {
                z = false;
            }
            eVar.p(z);
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0082  */
    public List<String> s(int i) {
        Cursor cursor;
        ArrayList arrayList = new ArrayList();
        if (i <= 0) {
            aa.w("Invalid maxHits specified. Skipping");
            return arrayList;
        }
        SQLiteDatabase G = G("Error opening database for peekHitIds.");
        if (G == null) {
            return arrayList;
        }
        try {
            cursor = G.query("hits2", new String[]{"hit_id"}, null, null, null, null, String.format("%s ASC", new Object[]{"hit_id"}), Integer.toString(i));
            try {
                if (cursor.moveToFirst()) {
                    do {
                        arrayList.add(String.valueOf(cursor.getLong(0)));
                    } while (cursor.moveToNext());
                }
                if (cursor != null) {
                    cursor.close();
                }
            } catch (SQLiteException e) {
                e = e;
                try {
                    aa.w("Error in peekHits fetching hitIds: " + e.getMessage());
                    if (cursor != null) {
                        cursor.close();
                    }
                    return arrayList;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e2) {
            e = e2;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00f2, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00fa, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0173, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0174, code lost:
        r11 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0179, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x017a, code lost:
        r2 = r1;
        r3 = r12;
        r1 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        return r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0173 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:6:0x0039] */
    /* JADX WARNING: Removed duplicated region for block: B:80:? A[RETURN, SYNTHETIC] */
    public List<x> t(int i) {
        Cursor cursor;
        SQLiteException sQLiteException;
        ArrayList arrayList;
        Cursor query;
        ArrayList<x> arrayList2;
        ArrayList arrayList3 = new ArrayList();
        SQLiteDatabase G = G("Error opening database for peekHits");
        if (G == null) {
            return arrayList3;
        }
        Cursor cursor2 = null;
        try {
            query = G.query("hits2", new String[]{"hit_id", "hit_time"}, null, null, null, null, String.format("%s ASC", new Object[]{"hit_id"}), Integer.toString(i));
            try {
                arrayList2 = new ArrayList<>();
                if (query.moveToFirst()) {
                    do {
                        arrayList2.add(new x(null, query.getLong(0), query.getLong(1)));
                    } while (query.moveToNext());
                }
                if (query != null) {
                    query.close();
                }
                try {
                    Cursor query2 = G.query("hits2", new String[]{"hit_id", "hit_string", "hit_url"}, null, null, null, null, String.format("%s ASC", new Object[]{"hit_id"}), Integer.toString(i));
                    try {
                        if (query2.moveToFirst()) {
                            int i2 = 0;
                            while (true) {
                                if (((SQLiteCursor) query2).getWindow().getNumRows() > 0) {
                                    ((x) arrayList2.get(i2)).E(query2.getString(1));
                                    ((x) arrayList2.get(i2)).F(query2.getString(2));
                                } else {
                                    aa.w(String.format("HitString for hitId %d too large.  Hit will be deleted.", new Object[]{Long.valueOf(((x) arrayList2.get(i2)).ci())}));
                                }
                                int i3 = i2 + 1;
                                if (!query2.moveToNext()) {
                                    break;
                                }
                                i2 = i3;
                            }
                        }
                        if (query2 != null) {
                            query2.close();
                        }
                        return arrayList2;
                    } catch (SQLiteException e) {
                        e = e;
                        query = query2;
                    } catch (Throwable th) {
                        th = th;
                        query = query2;
                        if (query != null) {
                            query.close();
                        }
                        throw th;
                    }
                } catch (SQLiteException e2) {
                    e = e2;
                }
            } catch (SQLiteException e3) {
                sQLiteException = e3;
                cursor = query;
                arrayList = arrayList2;
                try {
                    aa.w("Error in peekHits fetching hitIds: " + sQLiteException.getMessage());
                    if (cursor == null) {
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor2 = cursor;
                    if (cursor2 != null) {
                    }
                    throw th;
                }
            } catch (Throwable th3) {
            }
        } catch (SQLiteException e4) {
            sQLiteException = e4;
            cursor = null;
            arrayList = arrayList3;
        } catch (Throwable th4) {
            th = th4;
            if (cursor2 != null) {
            }
            throw th;
        }
        try {
            aa.w("Error in peekHits fetching hitString: " + e.getMessage());
            ArrayList arrayList4 = new ArrayList();
            boolean z = false;
            for (x xVar : arrayList2) {
                if (TextUtils.isEmpty(xVar.ch())) {
                    if (z) {
                        break;
                    }
                    z = true;
                }
                arrayList4.add(xVar);
            }
            if (query != null) {
                query.close();
            }
            return arrayList4;
        } catch (Throwable th5) {
            th = th5;
        }
    }
}
