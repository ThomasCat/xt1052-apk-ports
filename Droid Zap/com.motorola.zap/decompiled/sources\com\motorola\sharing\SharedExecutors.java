package com.motorola.sharing;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SharedExecutors {
    public static final ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(5);
}
