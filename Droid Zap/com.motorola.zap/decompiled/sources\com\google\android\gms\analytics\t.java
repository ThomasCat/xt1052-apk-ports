package com.google.android.gms.analytics;

import android.content.Context;
import android.os.Process;
import android.support.v4.os.EnvironmentCompat;
import android.text.TextUtils;
import com.google.analytics.tracking.android.Fields;
import com.google.android.gms.analytics.internal.Command;
import com.google.android.gms.internal.di;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

class t extends Thread implements f {
    private static t sd;
    private volatile boolean mClosed = false;
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public volatile String qX;
    private final LinkedBlockingQueue<Runnable> rZ = new LinkedBlockingQueue<>();
    private volatile boolean sa = false;
    /* access modifiers changed from: private */
    public volatile List<di> sb;
    /* access modifiers changed from: private */
    public volatile String sc;
    /* access modifiers changed from: private */
    public volatile ag se;

    private t(Context context) {
        super("GAThread");
        if (context != null) {
            this.mContext = context.getApplicationContext();
        } else {
            this.mContext = context;
        }
        start();
    }

    static int C(String str) {
        int i = 1;
        if (!TextUtils.isEmpty(str)) {
            i = 0;
            for (int length = str.length() - 1; length >= 0; length--) {
                char charAt = str.charAt(length);
                i = ((i << 6) & 65535) + charAt + (charAt << 14);
                int i2 = 266338304 & i;
                if (i2 != 0) {
                    i ^= i2 >> 21;
                }
            }
        }
        return i;
    }

    private String a(Throwable th) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(byteArrayOutputStream);
        th.printStackTrace(printStream);
        printStream.flush();
        return new String(byteArrayOutputStream.toByteArray());
    }

    /* access modifiers changed from: private */
    public String o(Map<String, String> map) {
        return map.containsKey(Fields.USE_SECURE) ? ak.d((String) map.get(Fields.USE_SECURE), true) ? "https:" : "http:" : "https:";
    }

    /* access modifiers changed from: private */
    public boolean p(Map<String, String> map) {
        if (map.get(Fields.SAMPLE_RATE) == null) {
            return false;
        }
        double a = ak.a((String) map.get(Fields.SAMPLE_RATE), 100.0d);
        if (a >= 100.0d) {
            return false;
        }
        if (((double) (C((String) map.get(Fields.CLIENT_ID)) % 10000)) < a * 100.0d) {
            return false;
        }
        aa.v(String.format("%s hit sampled out", new Object[]{map.get(Fields.HIT_TYPE) == null ? EnvironmentCompat.MEDIA_UNKNOWN : (String) map.get(Fields.HIT_TYPE)}));
        return true;
    }

    static t q(Context context) {
        if (sd == null) {
            sd = new t(context);
        }
        return sd;
    }

    /* access modifiers changed from: private */
    public void q(Map<String, String> map) {
        m m = a.m(this.mContext);
        ak.a(map, "&adid", m.getValue("&adid"));
        ak.a(map, "&ate", m.getValue("&ate"));
    }

    static String r(Context context) {
        try {
            FileInputStream openFileInput = context.openFileInput("gaInstallData");
            byte[] bArr = new byte[8192];
            int read = openFileInput.read(bArr, 0, 8192);
            if (openFileInput.available() > 0) {
                aa.t("Too much campaign data, ignoring it.");
                openFileInput.close();
                context.deleteFile("gaInstallData");
                return null;
            }
            openFileInput.close();
            context.deleteFile("gaInstallData");
            if (read <= 0) {
                aa.w("Campaign file is empty.");
                return null;
            }
            String str = new String(bArr, 0, read);
            aa.u("Campaign found: " + str);
            return str;
        } catch (FileNotFoundException e) {
            aa.u("No campaign data found.");
            return null;
        } catch (IOException e2) {
            aa.t("Error reading campaign data.");
            context.deleteFile("gaInstallData");
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void r(Map<String, String> map) {
        g bt = g.bt();
        ak.a(map, Fields.APP_NAME, bt.getValue(Fields.APP_NAME));
        ak.a(map, Fields.APP_VERSION, bt.getValue(Fields.APP_VERSION));
        ak.a(map, Fields.APP_ID, bt.getValue(Fields.APP_ID));
        ak.a(map, Fields.APP_INSTALLER_ID, bt.getValue(Fields.APP_INSTALLER_ID));
        map.put("&v", "1");
    }

    /* access modifiers changed from: 0000 */
    public void a(Runnable runnable) {
        this.rZ.add(runnable);
    }

    public void bk() {
        a((Runnable) new Runnable() {
            public void run() {
                t.this.se.bk();
            }
        });
    }

    public void bp() {
        a((Runnable) new Runnable() {
            public void run() {
                t.this.se.bp();
            }
        });
    }

    public void br() {
        a((Runnable) new Runnable() {
            public void run() {
                t.this.se.br();
            }
        });
    }

    public LinkedBlockingQueue<Runnable> bs() {
        return this.rZ;
    }

    public Thread getThread() {
        return this;
    }

    /* access modifiers changed from: protected */
    public void init() {
        this.se.bI();
        this.sb = new ArrayList();
        this.sb.add(new di(Command.APPEND_VERSION, "&_v".substring(1), "ma4.0.0"));
        this.sb.add(new di(Command.APPEND_QUEUE_TIME, "&qt".substring(1), null));
        this.sb.add(new di(Command.APPEND_CACHE_BUSTER, "&z".substring(1), null));
    }

    public void n(Map<String, String> map) {
        final HashMap hashMap = new HashMap(map);
        String str = (String) map.get("&ht");
        if (str != null) {
            try {
                Long.valueOf(str);
            } catch (NumberFormatException e) {
                str = null;
            }
        }
        if (str == null) {
            hashMap.put("&ht", Long.toString(System.currentTimeMillis()));
        }
        a((Runnable) new Runnable() {
            public void run() {
                if (TextUtils.isEmpty((CharSequence) hashMap.get(Fields.CLIENT_ID))) {
                    hashMap.put(Fields.CLIENT_ID, t.this.qX);
                }
                if (!GoogleAnalytics.getInstance(t.this.mContext).getAppOptOut() && !t.this.p(hashMap)) {
                    if (!TextUtils.isEmpty(t.this.sc)) {
                        u.bR().r(true);
                        hashMap.putAll(new HitBuilder().setCampaignParamsFromUrl(t.this.sc).build());
                        u.bR().r(false);
                        t.this.sc = null;
                    }
                    t.this.r(hashMap);
                    t.this.q(hashMap);
                    t.this.se.b(y.s(hashMap), Long.valueOf((String) hashMap.get("&ht")).longValue(), t.this.o(hashMap), t.this.sb);
                }
            }
        });
    }

    public void run() {
        Process.setThreadPriority(10);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            aa.w("sleep interrupted in GAThread initialize");
        }
        try {
            if (this.se == null) {
                this.se = new s(this.mContext, this);
            }
            init();
            this.qX = h.bu().getValue(Fields.CLIENT_ID);
            if (this.qX == null) {
                this.sa = true;
            }
            this.sc = r(this.mContext);
            aa.v("Initialized GA Thread");
        } catch (Throwable th) {
            aa.t("Error initializing the GAThread: " + a(th));
            aa.t("Google Analytics will not start up.");
            this.sa = true;
        }
        while (!this.mClosed) {
            try {
                Runnable runnable = (Runnable) this.rZ.take();
                if (!this.sa) {
                    runnable.run();
                }
            } catch (InterruptedException e2) {
                aa.u(e2.toString());
            } catch (Throwable th2) {
                aa.t("Error on GAThread: " + a(th2));
                aa.t("Google Analytics is shutting down.");
                this.sa = true;
            }
        }
    }
}
