package com.motorola.zap.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConfig;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.ui.DroidBlastPrivacyDialogFragment.PrivacyDialogListener;

public class DroidBlastPrivacyFragment extends Fragment implements PrivacyDialogListener {
    private CheckBox mCheckBox;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.privacy_fragment, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.mCheckBox = (CheckBox) getActivity().findViewById(R.id.privacy_agreement);
        this.mCheckBox.setOnClickListener(new OnClickListener() {
            @SuppressLint({"CommitTransaction"})
            public void onClick(View v) {
                if (((Boolean) Z.PRIVACY_AGREED.read()).booleanValue()) {
                    FragmentTransaction ft = DroidBlastPrivacyFragment.this.getFragmentManager().beginTransaction();
                    Fragment prev = DroidBlastPrivacyFragment.this.getFragmentManager().findFragmentByTag("privacy");
                    if (prev != null) {
                        ft.remove(prev);
                    }
                    ft.addToBackStack(null);
                    DroidBlastPrivacyDialogFragment.newInstance(DroidBlastPrivacyFragment.this).show(ft, "privacy");
                    return;
                }
                ZapConfig.savePrivacyAgreed(true);
                Z.FEATURE_ENABLED.write(Boolean.valueOf(true));
            }
        });
        setupPrivacyClickable(getActivity());
    }

    static void setupPrivacyClickable(Activity activity) {
        TextView textView = (TextView) activity.findViewById(R.id.privacy_more_info);
        String clickableText = activity.getResources().getString(R.string.settings_privacy_more_info_link);
        String text = String.format(activity.getResources().getString(R.string.settings_privacy_more_info), new Object[]{clickableText});
        CustomClickableSpan span = new CustomClickableSpan((Context) activity, getPrivacyIntent(activity.getApplicationContext()));
        int start = text.indexOf(clickableText);
        int end = start + clickableText.length();
        SpannableString s = SpannableString.valueOf(text);
        s.setSpan(span, start, end, 33);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(s);
    }

    private static Intent getPrivacyIntent(Context context) {
        if (ZapConfig.supportsFullFeature()) {
            return new Intent(ZapConstants.ACTION_MOTO_PRIVACY);
        }
        String url = context.getResources().getString(R.string.privacy_url);
        Intent i = new Intent("android.intent.action.VIEW");
        i.setData(Uri.parse(url));
        return i;
    }

    public void onResume() {
        super.onResume();
        this.mCheckBox.setChecked(((Boolean) Z.PRIVACY_AGREED.read()).booleanValue());
        getActivity().getActionBar().setTitle(R.string.settings_privacy_policy);
    }

    public void onDialogPositiveClick() {
        ZapConfig.savePrivacyAgreed(false);
        this.mCheckBox.setChecked(false);
        ((DroidBlastPreferenceFragment) getTargetFragment()).handlePrivacyAgreementDisabled();
    }

    public void onDialogNegativeClick() {
        this.mCheckBox.setChecked(((Boolean) Z.PRIVACY_AGREED.read()).booleanValue());
    }
}
