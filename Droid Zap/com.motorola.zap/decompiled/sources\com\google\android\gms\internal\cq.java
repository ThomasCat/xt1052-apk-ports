package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;

public class cq {
    private final Object mg = new Object();
    private int pA = 0;
    private long pB = -1;
    private long pC = -1;
    private int pD = 0;
    private int pE = -1;
    private final String pz;

    public cq(String str) {
        this.pz = str;
    }

    public synchronized void aJ() {
        synchronized (this.mg) {
            this.pD++;
        }
    }

    public void aK() {
        synchronized (this.mg) {
            this.pA++;
        }
    }

    public Bundle b(String str, Context context) {
        Bundle bundle;
        synchronized (this.mg) {
            bundle = new Bundle();
            bundle.putString("session_id", this.pz);
            bundle.putLong("basets", this.pC);
            bundle.putLong("currts", this.pB);
            bundle.putString("seq_num", str);
            bundle.putInt("preqs", this.pE);
            bundle.putInt("pclick", this.pA);
            bundle.putInt("pimp", this.pD);
            cm cmVar = new cm(context);
            bundle.putInt("gnt", cmVar.oY);
            if (cmVar.oX == 1) {
                bundle.putString("net", "wi");
            } else {
                bundle.putString("net", "ed");
            }
        }
        return bundle;
    }

    public void b(z zVar, long j) {
        synchronized (this.mg) {
            if (this.pC == -1) {
                this.pC = j;
                this.pB = this.pC;
            } else {
                this.pB = j;
            }
            if (zVar.extras == null || zVar.extras.getInt("gw", 2) != 1) {
                this.pE++;
            }
        }
    }
}
