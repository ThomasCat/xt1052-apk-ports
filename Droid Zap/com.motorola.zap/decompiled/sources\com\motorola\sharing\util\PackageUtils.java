package com.motorola.sharing.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import com.motorola.sharing.ProviderApi;
import com.motorola.sharing.provider.cloud.CloudProvider;
import com.motorola.sharing.provider.p2p.P2pProvider;

public class PackageUtils {
    private static final String TAG = Logger.getGcspTag("PackageUtils");
    private static String sAppVersion = null;
    private static Integer sAppVersionCode = null;

    public static boolean setComponentEnabled(Context context, ComponentName component, boolean enabled) {
        try {
            context.getPackageManager().setComponentEnabledSetting(component, enabled ? 1 : 2, 1);
            return true;
        } catch (Exception e) {
            Logger.e(TAG, "got exception " + e);
            return false;
        }
    }

    public static boolean isComponentEnabled(Context context, ComponentName component) {
        try {
            if (context.getPackageManager().getComponentEnabledSetting(component) == 1) {
                return true;
            }
            return false;
        } catch (Exception e) {
            Logger.e(TAG, "got exception " + e);
            return false;
        }
    }

    public static int getAppVersionCode(Context context) {
        if (sAppVersionCode == null) {
            try {
                sAppVersionCode = Integer.valueOf(getPackageInfo(context).versionCode);
            } catch (NameNotFoundException e) {
                Logger.e(TAG, "got exception " + e);
                sAppVersionCode = Integer.valueOf(0);
            } catch (Exception e2) {
                Logger.e(TAG, "got exception " + e2);
                return 0;
            }
        }
        return sAppVersionCode.intValue();
    }

    public static String getAppVersion(Context context) {
        String str = "NA";
        if (sAppVersion == null) {
            try {
                sAppVersion = getPackageInfo(context).versionName;
            } catch (NameNotFoundException e) {
                Logger.e(TAG, "got exception " + e);
                sAppVersion = "NA";
            } catch (Exception e2) {
                Logger.e(TAG, "got exception " + e2);
                return "NA";
            }
        }
        return sAppVersion;
    }

    public static ProviderApi[] getProviders() {
        return new ProviderApi[]{CloudProvider.getInstance(), P2pProvider.getInstance()};
    }

    private static PackageInfo getPackageInfo(Context context) throws NameNotFoundException {
        return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
    }
}
