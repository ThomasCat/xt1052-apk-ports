package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public class e implements Creator<InFilter> {
    static void a(InFilter inFilter, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.c(parcel, 1000, inFilter.wj);
        b.a(parcel, 1, (Parcelable) inFilter.EP, i, false);
        b.D(parcel, p);
    }

    /* renamed from: aK */
    public InFilter[] newArray(int i) {
        return new InFilter[i];
    }

    /* renamed from: af */
    public InFilter createFromParcel(Parcel parcel) {
        int o = a.o(parcel);
        int i = 0;
        MetadataBundle metadataBundle = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    metadataBundle = (MetadataBundle) a.a(parcel, n, MetadataBundle.CREATOR);
                    break;
                case 1000:
                    i = a.g(parcel, n);
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new InFilter(i, metadataBundle);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }
}
