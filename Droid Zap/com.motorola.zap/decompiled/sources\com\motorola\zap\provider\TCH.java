package com.motorola.zap.provider;

import android.database.Cursor;
import android.text.TextUtils;
import com.motorola.sharing.GCSP;

public class TCH {
    public static int getId(Cursor c) {
        return c.getInt(c.getColumnIndex(HistoryHandler.HIST_ID));
    }

    public static String getProfilePic(Cursor c) {
        return getString(c, c.getColumnIndex(HistoryHandler.HIST_PROF_PIC));
    }

    public static String getProfileName(Cursor c) {
        return getString(c, c.getColumnIndex(HistoryHandler.HIST_PROF_NAME));
    }

    public static String getProfileUserId(Cursor c) {
        return getString(c, c.getColumnIndex(HistoryHandler.HIST_PROF_USERID));
    }

    public static Boolean getProfileVisiblity(Cursor c) {
        return Boolean.valueOf(getBoolean(c, c.getColumnIndex(HistoryHandler.HIST_PROF_ANONYMOUS)));
    }

    public static String getFilePath(Cursor c) {
        return getString(c, c.getColumnIndex(HistoryHandler.HIST_FILEPATH));
    }

    public static String getShareId(Cursor c) {
        return getString(c, c.getColumnIndex(HistoryHandler.HIST_SHAREID));
    }

    public static String getFrom(Cursor c) {
        return getString(c, c.getColumnIndex(HistoryHandler.HIST_FROM));
    }

    public static String getAccount(Cursor c) {
        return getString(c, c.getColumnIndex(HistoryHandler.HIST_ACCOUNT_COLUMN));
    }

    public static long getTimestamp(Cursor c) {
        return getLong(c, c.getColumnIndex(HistoryHandler.HIST_TIMESTAMP), 0);
    }

    public static double getLatitue(Cursor c) {
        return getDouble(c, c.getColumnIndex(HistoryHandler.HIST_LATITUDE), 0.0d);
    }

    public static double getLongitude(Cursor c) {
        return getDouble(c, c.getColumnIndex(HistoryHandler.HIST_LONGITUDE), 0.0d);
    }

    public static String getCity(Cursor c) {
        return getString(c, c.getColumnIndex(HistoryHandler.HIST_CITY));
    }

    public static String getState(Cursor c) {
        return getString(c, c.getColumnIndex(HistoryHandler.HIST_STATE));
    }

    public static String getCountry(Cursor c) {
        return getString(c, c.getColumnIndex(HistoryHandler.HIST_COUNTRY));
    }

    public static String getThumbnail(Cursor c) {
        return getString(c, c.getColumnIndex(HistoryHandler.HIST_THUMBNAIL));
    }

    public static boolean getAbuseReported(Cursor c) {
        return getBoolean(c, c.getColumnIndex(HistoryHandler.HIST_IS_ABUSE_REPORTED));
    }

    public static boolean isSharedInZz(Cursor c) {
        return !TextUtils.isEmpty(getZzOwner(c)) && !TextUtils.isEmpty(getZzId(c));
    }

    public static String getZzName(Cursor c) {
        return getString(c, c.getColumnIndex(HistoryHandler.HIST_ZZ_NAME));
    }

    public static String getAction(Cursor c) {
        return getString(c, c.getColumnIndex(HistoryHandler.HIST_ACTION));
    }

    public static String getZzOwner(Cursor c) {
        return getString(c, c.getColumnIndex(HistoryHandler.HIST_ZZ_OWNER));
    }

    public static String getZzId(Cursor c) {
        return getString(c, c.getColumnIndex(HistoryHandler.HIST_ZZ_ID));
    }

    public static boolean isPinLocked(Cursor c) {
        return getBoolean(c, c.getColumnIndex(HistoryHandler.HIST_IS_PIN_LOCKED));
    }

    public static String getPin(Cursor c) {
        return getString(c, c.getColumnIndex(HistoryHandler.HIST_PIN));
    }

    public static int getUid(Cursor c) {
        return getInt(c, c.getColumnIndex(HistoryHandler.HIST_UID), -1);
    }

    public static boolean isSendCard(Cursor c) {
        return HistoryHandler.ACTION_MEDIA_SEND.equals(getAction(c));
    }

    public static boolean isZzInvite(Cursor c) {
        return HistoryHandler.ACTION_ZZ_INVITE.equals(getAction(c));
    }

    public static int getMediaType(Cursor c) {
        return getInt(c, c.getColumnIndex(HistoryHandler.HIST_MEDIA_TYPE), 0);
    }

    public static boolean isZzSession(Cursor c) {
        return HistoryHandler.ACTION_ZZ_SESSION.equals(getAction(c));
    }

    public static boolean isActiveZz(Cursor c) {
        if (!isZzSession(c)) {
            return false;
        }
        return getBoolean(c, c.getColumnIndex(HistoryHandler.HIST_IS_ZZ_ACTIVE));
    }

    public static boolean isDeleted(Cursor c) {
        return getBoolean(c, c.getColumnIndex(HistoryHandler.HIST_IS_DELETED));
    }

    public static boolean isTransferInProgress(Cursor c) {
        return getBoolean(c, c.getColumnIndex(HistoryHandler.HIST_IS_TRANSFER_IN_PROGRESS));
    }

    public static boolean isUserZzOwner(Cursor c) {
        if (!isZzSession(c)) {
            return false;
        }
        String userAccount = GCSP.getAuthenticator().getAccount();
        if (userAccount != null) {
            return userAccount.equals(getZzOwner(c));
        }
        return false;
    }

    public static boolean isUploadInProgress(Cursor c) {
        return !isZzSession(c) && isTransferInProgress(c) && getFilePath(c) != null;
    }

    public static boolean isDownloadInProgress(Cursor c) {
        return !isZzSession(c) && isTransferInProgress(c) && getFilePath(c) == null;
    }

    public static long getReshareUid(Cursor c) {
        return getLong(c, c.getColumnIndex(HistoryHandler.HIST_RESHARE_UID), -1);
    }

    public static long getMediaExpirationTimestamp(Cursor c) {
        return getLong(c, c.getColumnIndex(HistoryHandler.HIST_MEDIA_EXPIRATION_TIMESTAMP), -1);
    }

    public static int getTransferCompleteStatus(Cursor c) {
        return getInt(c, c.getColumnIndex(HistoryHandler.HIST_TRANSFER_COMPLETE_STATUS), -1);
    }

    public static boolean isDemoCard(Cursor c) {
        return HistoryHandler.ACTION_DEMO_CARD.equals(getAction(c));
    }

    public static boolean isLeftZz(Cursor c) {
        return getBoolean(c, c.getColumnIndex(HistoryHandler.HIST_IS_LEFT_ZZ));
    }

    public static long getEffectiveUid(Cursor c) {
        if (getReshareUid(c) != -1) {
            return getReshareUid(c);
        }
        return (long) getUid(c);
    }

    private static String getString(Cursor c, int columnIndex) {
        if (c.isNull(columnIndex)) {
            return null;
        }
        return c.getString(columnIndex);
    }

    private static int getInt(Cursor c, int columnIndex, int defaultValue) {
        return c.isNull(columnIndex) ? defaultValue : c.getInt(columnIndex);
    }

    private static long getLong(Cursor c, int columnIndex, long defaultValue) {
        return c.isNull(columnIndex) ? defaultValue : c.getLong(columnIndex);
    }

    private static double getDouble(Cursor c, int columnIndex, double defaultValue) {
        return c.isNull(columnIndex) ? defaultValue : c.getDouble(columnIndex);
    }

    private static boolean getBoolean(Cursor c, int columnIndex) {
        return c.getInt(columnIndex) != 0;
    }
}
