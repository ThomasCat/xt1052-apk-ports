package com.motorola.sharing.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtils {
    private static Boolean sMobileDataCapable = null;

    public static boolean isWifiConnected(Context context) {
        NetworkInfo wifiInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1);
        if (wifiInfo == null) {
            return false;
        }
        return wifiInfo.isConnected();
    }

    public static boolean isMobileConnected(Context context) {
        NetworkInfo netInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0);
        if (netInfo == null) {
            return false;
        }
        return netInfo.isConnected();
    }

    public static boolean isWifiConnected(Context context, NetworkInfo networkInfo) {
        if (networkInfo == null) {
            networkInfo = getActiveNetworkInfo(context);
        }
        if (networkInfo == null || networkInfo.getType() != 1 || !networkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public static boolean hasDataConnection(Context context) {
        return getActiveNetworkInfo(context) != null;
    }

    public static String getActiveNetworkType(Context context) {
        NetworkInfo networkInfo = getActiveNetworkInfo(context);
        if (networkInfo != null) {
            return networkInfo.getTypeName();
        }
        return null;
    }

    public static NetworkInfo getActiveNetworkInfo(Context context) {
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnectedOrConnecting()) {
            return null;
        }
        return networkInfo;
    }

    public static boolean isMobileDataCapable(Context context) {
        if (sMobileDataCapable == null) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService("connectivity");
            if (cm != null) {
                NetworkInfo[] infoList = cm.getAllNetworkInfo();
                if (infoList != null) {
                    NetworkInfo[] arr$ = infoList;
                    int len$ = arr$.length;
                    int i$ = 0;
                    while (true) {
                        if (i$ >= len$) {
                            break;
                        }
                        NetworkInfo info = arr$[i$];
                        if (info != null && info.getType() == 0) {
                            sMobileDataCapable = Boolean.valueOf(true);
                            break;
                        }
                        i$++;
                    }
                }
            }
            sMobileDataCapable = Boolean.valueOf(false);
        }
        return sMobileDataCapable.booleanValue();
    }
}
