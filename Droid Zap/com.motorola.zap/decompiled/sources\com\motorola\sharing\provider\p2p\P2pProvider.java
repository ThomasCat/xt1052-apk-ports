package com.motorola.sharing.provider.p2p;

import android.text.TextUtils;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentContainer.DiscoveryParams;
import com.motorola.sharing.ContentDiscoveryListener;
import com.motorola.sharing.ContentDownloadListener;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.ContentShareListener;
import com.motorola.sharing.ContentShareListener.ContentShareEvent.Substatus;
import com.motorola.sharing.Endpoint;
import com.motorola.sharing.Endpoint.AttrValue;
import com.motorola.sharing.Endpoint.Attributes;
import com.motorola.sharing.EndpointDiscoveryListener;
import com.motorola.sharing.EndpointDiscoveryListener.EndpointDiscoveryEvent;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.ProviderApi;
import com.motorola.sharing.ReportAbuseListener;
import com.motorola.sharing.provider.p2p.api.P2pEndpoint;
import com.motorola.sharing.util.Logger;

public class P2pProvider implements ProviderApi {
    private static final String TAG = Logger.getP2pTag("Provider");
    private static P2pProvider sProvider = null;
    private P2pEndpoint mEndpoint = null;

    public static synchronized P2pProvider getInstance() {
        P2pProvider p2pProvider;
        synchronized (P2pProvider.class) {
            if (sProvider == null) {
                sProvider = new P2pProvider();
            }
            p2pProvider = sProvider;
        }
        return p2pProvider;
    }

    private P2pProvider() {
    }

    public void discoverContent(DiscoveryParams params, ContentDiscoveryListener listener, Endpoint[] endpoints) {
        Logger.d(TAG, "discoverContent(): " + params.getPrintableSignature());
        throw new UnsupportedOperationException("P2P discovery is not supported");
    }

    public void stopDiscovery(DiscoveryParams params) {
        Logger.d(TAG, "stopDiscovery(): " + params.getPrintableSignature());
        throw new UnsupportedOperationException("P2P discovery is not supported");
    }

    public void downloadContent(ContentContainer container, ContentDownloadListener listener) {
        Logger.d(TAG, "downloadContent(): " + container.getPrintableSignature());
        if (container.p2pParams == null || !container.p2pParams.isValid()) {
            throw new IllegalArgumentException("Container doesn't have required P2P params");
        }
        ReceivingService.startReceiving(GCSP.getContext(), container);
    }

    public void cancelDownload(ContentContainer container) {
        Logger.d(TAG, "cancelDownload(): " + container.getPrintableSignature());
        ReceivingService.stopReceiving(GCSP.getContext(), container);
    }

    public void reportAbuse(ContentContainer container, ReportAbuseListener listener) {
        Logger.d(TAG, "reportAbuse(): " + container.getPrintableSignature());
        throw new UnsupportedOperationException("Reporting abuse is not supported for P2P");
    }

    public void shareContent(ContentContainer container, ContentShareListener listener, Endpoint[] endpoints) {
        Logger.d(TAG, "shareContent(): " + container.getPrintableSignature());
        if (!TextUtils.isEmpty(container.shareId)) {
            throw new IllegalArgumentException("container has share id which is not allowed");
        }
        SharingService.startSharing(GCSP.getContext(), container);
    }

    public void stopSharing(ContentContainer container) {
        Logger.d(TAG, "stopSharing(): " + container.getPrintableSignature());
        SharingService.stopSharing(GCSP.getContext(), container);
    }

    public void stopSharing(String shareId, ContentShareListener listener) {
        Logger.d(TAG, "stopSharing(): sid" + shareId);
        throw new UnsupportedOperationException("stopShare with container must be called");
    }

    public void cancelSharing(ContentContainer container) {
        Logger.d(TAG, "cancelSharing(): " + container.getPrintableSignature());
        SharingService.cancelSharing(GCSP.getContext(), container);
    }

    public void resumeSharing(ContentContainer container, ContentShareListener listener) {
        Logger.d(TAG, "resumeSharing(): " + container.getPrintableSignature());
        container.onContentShareEvent(Status.ERROR, Substatus.GENERAL, getEndpoint());
    }

    public void discoverEndpoints(Attributes attrs, EndpointDiscoveryListener listener) {
        Logger.d(TAG, "discoverEndpoints()");
        if (attrs.contains(AttrValue.DIRECT)) {
            listener.sendEvent(new EndpointDiscoveryEvent(EndpointDiscoveryEvent.Status.INFO, EndpointDiscoveryEvent.Substatus.DISCOVERED, this, getEndpoint()));
        }
        listener.sendEvent(new EndpointDiscoveryEvent(EndpointDiscoveryEvent.Status.SUCCESS, EndpointDiscoveryEvent.Substatus.FINISHED, this));
    }

    public void stopEndpointDiscovery(EndpointDiscoveryListener listener) {
        Logger.d(TAG, "stopEndpointDiscovery()");
        listener.sendEvent(new EndpointDiscoveryEvent(EndpointDiscoveryEvent.Status.SUCCESS, EndpointDiscoveryEvent.Substatus.STOPPED, this));
    }

    public synchronized Endpoint getEndpoint() {
        if (this.mEndpoint == null) {
            this.mEndpoint = new P2pEndpoint();
        }
        return this.mEndpoint;
    }

    public Endpoint[] getStaticEndpoints() {
        return new Endpoint[]{getEndpoint()};
    }
}
