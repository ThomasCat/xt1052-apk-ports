package com.google.sample.castcompanionlibrary.utils;

import android.util.Log;
import com.google.sample.castcompanionlibrary.cast.BaseCastManager;

public class LogUtils {
    private static final int LOG_PREFIX_LENGTH = "ccl_".length();

    private LogUtils() {
    }

    public static String makeLogTag(String str) {
        if (str.length() > 23 - LOG_PREFIX_LENGTH) {
            return "ccl_" + str.substring(0, (23 - LOG_PREFIX_LENGTH) - 1);
        }
        return "ccl_" + str;
    }

    public static String makeLogTag(Class<?> cls) {
        return makeLogTag(cls.getSimpleName());
    }

    public static void LOGD(String tag, String message) {
        if (Log.isLoggable(tag, 3)) {
            Log.d(tag, getVersionPrefix() + message);
        }
    }

    public static void LOGD(String tag, String message, Throwable cause) {
        if (Log.isLoggable(tag, 3)) {
            Log.d(tag, getVersionPrefix() + message, cause);
        }
    }

    public static void LOGV(String tag, String message) {
    }

    public static void LOGV(String tag, String message, Throwable cause) {
    }

    public static void LOGI(String tag, String message) {
        Log.i(tag, getVersionPrefix() + message);
    }

    public static void LOGI(String tag, String message, Throwable cause) {
        Log.i(tag, message, cause);
    }

    public static void LOGW(String tag, String message) {
        Log.w(tag, getVersionPrefix() + message);
    }

    public static void LOGW(String tag, String message, Throwable cause) {
        Log.w(tag, getVersionPrefix() + message, cause);
    }

    public static void LOGE(String tag, String message) {
        Log.e(tag, getVersionPrefix() + message);
    }

    public static void LOGE(String tag, String message, Throwable cause) {
        Log.e(tag, getVersionPrefix() + message, cause);
    }

    public static String getVersionPrefix() {
        return "[v" + BaseCastManager.getCclVersion() + "] ";
    }
}
