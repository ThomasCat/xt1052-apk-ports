package com.motorola.sharing.util;

import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.motorola.sharing.Defaults;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.util.Logger.LogLevel;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class SharedPreference<T> {
    public static final String PREFERENCES_FILE_NAME = "gscp-pref";
    public static final SettingHandler<Boolean> booleanHandler = new SettingHandler<Boolean>() {
        public synchronized Boolean get(SharedPreference<Boolean> s) {
            return Boolean.valueOf(GCSP.getContext().getSharedPreferences(SharedPreference.PREFERENCES_FILE_NAME, 0).getBoolean(s.name, ((Boolean) s.defaultValue).booleanValue()));
        }

        public synchronized void set(SharedPreference<Boolean> s, Boolean value) {
            Editor editor = GCSP.getContext().getSharedPreferences(SharedPreference.PREFERENCES_FILE_NAME, 0).edit();
            if (value != null) {
                editor.putBoolean(s.name, value.booleanValue());
            } else {
                editor.remove(s.name);
            }
            editor.apply();
        }

        public Boolean valueOf(String value) {
            return Boolean.valueOf(value);
        }
    };
    public static final Verificator emptyTextVerificator = new Verificator() {
        public boolean isValid(Object value) {
            return !TextUtils.isEmpty((String) value);
        }
    };
    public static final SettingHandler<Float> floatHandler = new SettingHandler<Float>() {
        public synchronized Float get(SharedPreference<Float> s) {
            return Float.valueOf(GCSP.getContext().getSharedPreferences(SharedPreference.PREFERENCES_FILE_NAME, 0).getFloat(s.name, ((Float) s.defaultValue).floatValue()));
        }

        public synchronized void set(SharedPreference<Float> s, Float value) {
            Editor editor = GCSP.getContext().getSharedPreferences(SharedPreference.PREFERENCES_FILE_NAME, 0).edit();
            if (value != null) {
                editor.putFloat(s.name, value.floatValue());
            } else {
                editor.remove(s.name);
            }
            editor.apply();
        }

        public Float valueOf(String value) {
            return Float.valueOf(value);
        }
    };
    public static final Verificator hostNullableVerificator = new Verificator() {
        private final Pattern hostPattern = Pattern.compile("^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\\-]*[A-Za-z0-9])$", 2);

        public boolean isValid(Object value) {
            String host = (String) value;
            if (host == null) {
                return true;
            }
            try {
                return this.hostPattern.matcher(host).matches();
            } catch (Exception e) {
                return false;
            }
        }
    };
    public static final SettingHandler<Integer> integerHandler = new SettingHandler<Integer>() {
        public synchronized Integer get(SharedPreference<Integer> s) {
            return Integer.valueOf(GCSP.getContext().getSharedPreferences(SharedPreference.PREFERENCES_FILE_NAME, 0).getInt(s.name, ((Integer) s.defaultValue).intValue()));
        }

        public synchronized void set(SharedPreference<Integer> s, Integer value) {
            Editor editor = GCSP.getContext().getSharedPreferences(SharedPreference.PREFERENCES_FILE_NAME, 0).edit();
            if (value != null) {
                editor.putInt(s.name, value.intValue());
            } else {
                editor.remove(s.name);
            }
            editor.apply();
        }

        public Integer valueOf(String value) {
            return Integer.valueOf(value);
        }
    };
    public static final SettingHandler<LogLevel> logLevelHandler = new SettingHandler<LogLevel>() {
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0033, code lost:
            r3 = (com.motorola.sharing.util.Logger.LogLevel) com.motorola.sharing.util.SharedPreference.access$000(r10);
         */
        public synchronized LogLevel get(SharedPreference<LogLevel> s) {
            LogLevel level;
            int value = GCSP.getContext().getSharedPreferences(SharedPreference.PREFERENCES_FILE_NAME, 0).getInt(s.name, ((LogLevel) s.defaultValue).value);
            LogLevel[] arr$ = LogLevel.values();
            int len$ = arr$.length;
            int i$ = 0;
            while (true) {
                if (i$ >= len$) {
                    break;
                }
                level = arr$[i$];
                if (level.value == value) {
                    break;
                }
                i$++;
            }
            return level;
        }

        public synchronized void set(SharedPreference<LogLevel> s, LogLevel value) {
            Editor editor = GCSP.getContext().getSharedPreferences(SharedPreference.PREFERENCES_FILE_NAME, 0).edit();
            if (value != null) {
                editor.putInt(s.name, value.value);
            } else {
                editor.remove(s.name);
            }
            editor.apply();
        }

        public LogLevel valueOf(String value) {
            LogLevel[] arr$;
            int intValue = Integer.valueOf(value).intValue();
            for (LogLevel level : LogLevel.values()) {
                if (level.value == intValue) {
                    return level;
                }
            }
            return Defaults.LOG_LEVEL;
        }
    };
    public static final SettingHandler<Long> longHandler = new SettingHandler<Long>() {
        public synchronized Long get(SharedPreference<Long> s) {
            return Long.valueOf(GCSP.getContext().getSharedPreferences(SharedPreference.PREFERENCES_FILE_NAME, 0).getLong(s.name, ((Long) s.defaultValue).longValue()));
        }

        public synchronized void set(SharedPreference<Long> s, Long value) {
            Editor editor = GCSP.getContext().getSharedPreferences(SharedPreference.PREFERENCES_FILE_NAME, 0).edit();
            if (value != null) {
                editor.putLong(s.name, value.longValue());
            } else {
                editor.remove(s.name);
            }
            editor.apply();
        }

        public Long valueOf(String value) {
            return Long.valueOf(value);
        }
    };
    public static final Verificator nullVerificator = new Verificator() {
        public boolean isValid(Object value) {
            return value != null;
        }
    };
    public static final Verificator positiveValueVerificator = new Verificator() {
        public boolean isValid(Object value) {
            if (value == null) {
                return false;
            }
            if (value instanceof Integer) {
                if (((Integer) value).intValue() <= 0) {
                    return false;
                }
                return true;
            } else if (value instanceof Long) {
                if (((Long) value).longValue() <= 0) {
                    return false;
                }
                return true;
            } else if (value instanceof Float) {
                if (((Float) value).floatValue() <= BitmapDescriptorFactory.HUE_RED) {
                    return false;
                }
                return true;
            } else if (!(value instanceof Double)) {
                return false;
            } else {
                if (((Double) value).doubleValue() <= 0.0d) {
                    return false;
                }
                return true;
            }
        }
    };
    private static Map<String, SharedPreference<? extends Object>> sNameMap = new HashMap();
    public static final SettingHandler<String> stringHandler = new SettingHandler<String>() {
        public synchronized String get(SharedPreference<String> s) {
            return GCSP.getContext().getSharedPreferences(SharedPreference.PREFERENCES_FILE_NAME, 0).getString(s.name, (String) s.defaultValue);
        }

        public synchronized void set(SharedPreference<String> s, String value) {
            Editor editor = GCSP.getContext().getSharedPreferences(SharedPreference.PREFERENCES_FILE_NAME, 0).edit();
            if (value != null) {
                editor.putString(s.name, value);
            } else {
                editor.remove(s.name);
            }
            editor.apply();
        }

        public String valueOf(String value) {
            return value;
        }
    };
    private T cachedValue;
    /* access modifiers changed from: private */
    public T defaultValue;
    private SettingHandler<T> handler;
    public String name;
    private Class<T> type;
    private Verificator verificator;

    private interface SettingHandler<T> {
        T get(SharedPreference<T> sharedPreference);

        void set(SharedPreference<T> sharedPreference, T t);

        T valueOf(String str);
    }

    public interface Verificator {
        boolean isValid(Object obj);
    }

    public SharedPreference(String name2, Class<T> type2, T defaultValue2, SettingHandler<T> handler2) {
        this(name2, type2, defaultValue2, handler2, null);
    }

    public SharedPreference(String name2, Class<T> type2, T defaultValue2, SettingHandler<T> handler2, Verificator verificator2) {
        this.name = null;
        this.type = null;
        this.defaultValue = null;
        this.cachedValue = null;
        this.handler = null;
        this.verificator = null;
        if (name2 == null || type2 == null || handler2 == null) {
            throw new IllegalStateException("missing name, type or handler");
        }
        this.name = name2;
        this.type = type2;
        this.defaultValue = defaultValue2;
        this.handler = handler2;
        this.verificator = verificator2;
        sNameMap.put(name2, this);
    }

    public Class<T> getType() {
        return this.type;
    }

    public T get() {
        if (this.cachedValue == null) {
            this.cachedValue = this.handler.get(this);
        }
        return this.cachedValue;
    }

    public void set(T value) {
        if (this.verificator == null || this.verificator.isValid(value)) {
            this.cachedValue = null;
            this.handler.set(this, value);
            return;
        }
        throw new IllegalArgumentException("setting '" + value + "' for '" + this.name + "' is not allowed");
    }

    public T valueOf(String value) {
        return this.handler.valueOf(value);
    }

    public static SharedPreference<? extends Object> forName(String name2) {
        if (name2 == null) {
            return null;
        }
        return (SharedPreference) sNameMap.get(name2);
    }
}
