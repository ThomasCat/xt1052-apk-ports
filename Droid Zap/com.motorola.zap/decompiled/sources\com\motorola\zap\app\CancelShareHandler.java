package com.motorola.zap.app;

import android.content.Intent;
import android.util.Log;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.GCSP;
import com.motorola.zap.LogUtils;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.provider.HistoryHandler;
import com.motorola.zap.ui.ZapUi;

public class CancelShareHandler {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;

    public void handleActionCancelShare(Intent intent) {
        cancelShare(intent.getLongExtra(ZapConstants.EXTRA_TRANSACTION_UID, -1), intent.getStringExtra(ZapConstants.EXTRA_SERVER_SHARE_ID), intent.getBooleanExtra(ZapConstants.EXTRA_CANCEL_USER_VISIBLE, true));
    }

    public void cancelShare(long uid, String shareId, boolean userVisible) {
        if (DBG) {
            Log.d("Zap-CancelShareHandler", "Canceling media share (uid: " + uid + ", shareId: " + shareId + ")");
        }
        ZapUi ui = ZapUi.getInstance();
        ContentContainer ongoingContainer = ZapTracker.getInstance().get(uid);
        if (ongoingContainer != null) {
            ui.onCancelOnGoingUpload(ongoingContainer);
            GCSP.getContentDispatcher().stopSharing(ongoingContainer);
            return;
        }
        new HistoryHandler().cancelShare(GCSP.getAuthenticator().getAccount(), shareId);
        GCSP.getContentDispatcher().stopSharing(shareId, ui.getContentShareListener());
    }
}
