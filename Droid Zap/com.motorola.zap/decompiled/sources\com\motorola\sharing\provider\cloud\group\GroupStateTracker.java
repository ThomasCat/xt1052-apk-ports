package com.motorola.sharing.provider.cloud.group;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.instrumentation.CheckinManager;
import com.motorola.sharing.util.DbUtils;
import com.motorola.sharing.util.HistoryManager.Downloads.C;
import com.motorola.sharing.util.HistoryManager.Shares;
import com.motorola.sharing.util.Logger;

public class GroupStateTracker {
    /* access modifiers changed from: private */
    public static int STATE_ACTIVE = 1;
    /* access modifiers changed from: private */
    public static int STATE_INACTIVE = 0;
    /* access modifiers changed from: private */
    public static int STATE_LEAVING = -1;
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getCloudTag("GroupStateTracker");
    private static GroupStateTracker sInstance = null;
    private ContentValues mCachedStateValues;
    private DbHelper mDbHelper;

    private static class DbHelper extends SQLiteOpenHelper {
        private static final String CREATE_TABLE = ("CREATE TABLE state (state INTEGER,owner TEXT NOT NULL,id TEXT NOT NULL,name TEXT,pin TEXT,thumbnail BLOB,latitude INTEGER,longitude INTEGER,time_started INTEGER,time_stopped INTEGER,time_invited INTEGER,manually_stopped INTEGER,member_list_verion TEXT," + generateStatsColumns() + "marker" + " TEXT NOT NULL UNIQUE" + ")");
        private SQLiteDatabase mDatabase;

        private static String generateStatsColumns() {
            StringBuffer sb = new StringBuffer();
            for (GroupStats s : GroupStats.values()) {
                sb.append(s.column() + " INTEGER DEFAULT 0,");
            }
            return sb.toString();
        }

        private DbHelper(Context context) {
            super(context, "gcsp_group.db", null, 4);
            this.mDatabase = null;
        }

        public void onCreate(SQLiteDatabase database) {
            database.execSQL(CREATE_TABLE);
            database.execSQL("CREATE UNIQUE INDEX stateIndex ON state(owner, id)");
        }

        public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
            database.execSQL("DROP INDEX IF EXISTS stateIndex");
            database.execSQL("DROP TABLE IF EXISTS state");
            onCreate(database);
        }

        private synchronized SQLiteDatabase getDatabase() {
            if (this.mDatabase == null) {
                this.mDatabase = getWritableDatabase();
            }
            return this.mDatabase;
        }

        private static ContentValues getDefaultState() {
            ContentValues stateValues = new ContentValues();
            stateValues.put(C.STATE, Integer.valueOf(GroupStateTracker.STATE_INACTIVE));
            return stateValues;
        }

        public synchronized ContentValues readState() {
            ContentValues stateValues;
            GroupStats[] arr$;
            Cursor c = null;
            try {
                c = getDatabase().query(C.STATE, null, null, null, null, null, null, null);
                if (c.getCount() > 1) {
                    clearState(null);
                    throw new IllegalStateException("More then one record found in db");
                }
                if (c.moveToFirst()) {
                    stateValues = new ContentValues();
                    stateValues.put(C.STATE, Integer.valueOf(DbUtils.getInt(c, C.STATE, GroupStateTracker.STATE_INACTIVE)));
                    stateValues.put("owner", DbUtils.getString(c, "owner"));
                    stateValues.put("id", DbUtils.getString(c, "id"));
                    stateValues.put("name", DbUtils.getString(c, "name"));
                    stateValues.put(Shares.C.PIN, DbUtils.getString(c, Shares.C.PIN));
                    stateValues.put("thumbnail", DbUtils.getString(c, "thumbnail"));
                    stateValues.put("latitude", Double.valueOf(DbUtils.getDouble(c, "latitude", 0.0d)));
                    stateValues.put("longitude", Double.valueOf(DbUtils.getDouble(c, "longitude", 0.0d)));
                    stateValues.put("time_started", Long.valueOf(DbUtils.getLong(c, "time_started", 0)));
                    stateValues.put("time_stopped", Long.valueOf(DbUtils.getLong(c, "time_stopped", 0)));
                    stateValues.put("time_invited", Long.valueOf(DbUtils.getLong(c, "time_invited", 0)));
                    stateValues.put("manually_stopped", Integer.valueOf(DbUtils.getInt(c, "manually_stopped", 0)));
                    stateValues.put("member_list_verion", DbUtils.getString(c, "member_list_verion"));
                    for (GroupStats s : GroupStats.values()) {
                        stateValues.put(s.column(), Integer.valueOf(DbUtils.getInt(c, s.column(), 0)));
                    }
                } else {
                    stateValues = getDefaultState();
                }
                if (Logger.isDebuggable()) {
                    if (c.getCount() > 0) {
                        Logger.d(GroupStateTracker.TAG, "Group record found in db - gid: " + GroupStateTracker.getString(stateValues, "id"));
                    } else {
                        Logger.d(GroupStateTracker.TAG, "Group record not found in db");
                    }
                }
                if (c != null) {
                    c.close();
                }
            } catch (Throwable th) {
                if (c != null) {
                    c.close();
                }
                throw th;
            }
            return stateValues;
        }

        /* access modifiers changed from: private */
        public synchronized void updateState(ContentValues stateValues) {
            String owner = GroupStateTracker.getString(stateValues, "owner");
            String id = GroupStateTracker.getString(stateValues, "id");
            if (!GroupStateTracker.isValidGroup(owner, id)) {
                throw new IllegalArgumentException("Missing mandatory parameters");
            }
            SQLiteDatabase db = getDatabase();
            stateValues.put("marker", "group");
            int count = db.update(C.STATE, stateValues, "owner=? AND id=?", new String[]{owner, id});
            if (count == 1) {
                Logger.d(GroupStateTracker.TAG, "Group record updated in db - gid: " + id);
            } else if (count > 1) {
                clearState(stateValues);
                throw new IllegalStateException("More then one group record found in db");
            } else if (db.insert(C.STATE, null, stateValues) == -1) {
                clearState(stateValues);
                throw new IllegalStateException("Couldn't insert group record to db");
            } else {
                Logger.d(GroupStateTracker.TAG, "New group record inserted to db - gid: " + id);
            }
        }

        /* access modifiers changed from: private */
        public synchronized void clearState(ContentValues stateValues) {
            long count = (long) getDatabase().delete(C.STATE, null, null);
            if (Logger.isDebuggable() && count > 0) {
                Logger.d(GroupStateTracker.TAG, "Group record deleted from db - gid: " + GroupStateTracker.getString(stateValues, "id"));
            }
            if (stateValues != null) {
                stateValues.clear();
                stateValues.put(C.STATE, Integer.valueOf(GroupStateTracker.STATE_INACTIVE));
            }
        }
    }

    public static class GroupState {
        private ContentValues mStateValues;

        private GroupState(ContentValues stateValues) {
            this.mStateValues = null;
            this.mStateValues = stateValues;
        }

        public boolean isActive() {
            return GroupStateTracker.getInt(this.mStateValues, C.STATE, GroupStateTracker.STATE_INACTIVE) == GroupStateTracker.STATE_ACTIVE;
        }

        public boolean isLeaving() {
            return GroupStateTracker.getInt(this.mStateValues, C.STATE, GroupStateTracker.STATE_INACTIVE) == GroupStateTracker.STATE_LEAVING;
        }

        public boolean isStoppedByUser() {
            return GroupStateTracker.getInt(this.mStateValues, "manually_stopped", 0) > 0;
        }

        public boolean isUserOwner() {
            String owner = getOwner();
            if (owner == null) {
                return false;
            }
            return owner.equals(GCSP.getAuthenticator().getAccount());
        }

        public boolean isSameGroup(String owner, String id) {
            return !TextUtils.isEmpty(owner) && TextUtils.equals(getOwner(), owner) && !TextUtils.isEmpty(id) && TextUtils.equals(getId(), id);
        }

        public String getOwner() {
            return GroupStateTracker.getString(this.mStateValues, "owner");
        }

        public String getId() {
            return GroupStateTracker.getString(this.mStateValues, "id");
        }

        public String getName() {
            return GroupStateTracker.getString(this.mStateValues, "name");
        }

        public String getPin() {
            return GroupStateTracker.getString(this.mStateValues, Shares.C.PIN);
        }

        public String getThumbnail() {
            return GroupStateTracker.getInstance().getThumbnail();
        }

        public double getLatitude() {
            return GroupStateTracker.getDouble(this.mStateValues, "latitude", 0.0d);
        }

        public double getLongitude() {
            return GroupStateTracker.getDouble(this.mStateValues, "longitude", 0.0d);
        }

        public long getStartedTime() {
            return GroupStateTracker.getLong(this.mStateValues, "time_started", 0);
        }

        public long getStoppedTime() {
            return GroupStateTracker.getLong(this.mStateValues, "time_stopped", 0);
        }

        public long getLastInviteSentTime() {
            return GroupStateTracker.getLong(this.mStateValues, "time_invited", 0);
        }

        public int getStatsCount(GroupStats stats) {
            return GroupStateTracker.getInt(this.mStateValues, stats.column(), 0);
        }

        public String getMemberListVersion() {
            return GroupStateTracker.getString(this.mStateValues, "member_list_verion");
        }
    }

    public enum GroupStats {
        INVITES_SENT("invites_sent", "inv_out"),
        CURRENT_MEMBERS("current_members", "cur_mbr"),
        MAX_MEMBERS("max_members", "max_mbr"),
        AUTO_RECEIVED("auto_received", "auto_in"),
        MANUALLY_RECEIVED("manually_received", "man_in"),
        AUTO_SENT("auto_sent", "auto_out"),
        MANUALLY_SENT("manually_sent", "man_out"),
        RECEIVING_ERRORS("receiving_errors", "err_in"),
        SENDING_ERRORS("sending_errors", "err_out");
        
        private final String attribute;
        private final String column;

        private GroupStats(String column2, String attribute2) {
            this.column = column2;
            this.attribute = attribute2;
        }

        /* access modifiers changed from: private */
        public String column() {
            return this.column;
        }

        public String attribute() {
            return this.attribute;
        }
    }

    public static boolean isValidGroup(String owner, String id) {
        return !TextUtils.isEmpty(owner) && !TextUtils.isEmpty(id);
    }

    public static synchronized GroupStateTracker getInstance() {
        GroupStateTracker groupStateTracker;
        synchronized (GroupStateTracker.class) {
            if (sInstance == null) {
                sInstance = new GroupStateTracker();
            }
            groupStateTracker = sInstance;
        }
        return groupStateTracker;
    }

    private GroupStateTracker() {
        this.mDbHelper = null;
        this.mCachedStateValues = null;
        this.mDbHelper = new DbHelper(GCSP.getContext());
    }

    public synchronized void setInactive() {
        checkStateCached(true);
        if (!isInactive()) {
            this.mCachedStateValues.put(C.STATE, Integer.valueOf(STATE_INACTIVE));
            this.mCachedStateValues.put("time_stopped", Long.valueOf(System.currentTimeMillis()));
            CheckinManager.getInstance().logGroupEvent(getState());
        }
        this.mDbHelper.clearState(this.mCachedStateValues);
    }

    public synchronized void setActive(String owner, String id, String name, String pin, String thumbnail, double latitude, double longitude) {
        if (!isValidGroup(owner, id)) {
            throw new IllegalArgumentException("Missing mandatory parameters");
        }
        checkStateCached(true);
        if (!isInactive()) {
            setInactive();
        }
        this.mCachedStateValues = new ContentValues();
        this.mCachedStateValues.put(C.STATE, Integer.valueOf(STATE_ACTIVE));
        this.mCachedStateValues.put("owner", owner);
        this.mCachedStateValues.put("id", id);
        this.mCachedStateValues.put("name", name);
        this.mCachedStateValues.put(Shares.C.PIN, pin);
        this.mCachedStateValues.put("thumbnail", thumbnail);
        this.mCachedStateValues.put("latitude", Double.valueOf(latitude));
        this.mCachedStateValues.put("longitude", Double.valueOf(longitude));
        this.mCachedStateValues.put("time_started", Long.valueOf(System.currentTimeMillis()));
        this.mDbHelper.updateState(this.mCachedStateValues);
    }

    public synchronized void setLeaving(String owner, String id) {
        if (!isValidGroup(owner, id)) {
            throw new IllegalArgumentException("Missing mandatory parameters");
        }
        checkStateCached(true);
        if (!TextUtils.equals(getOwner(), owner) || !TextUtils.equals(getId(), id)) {
            if (!isInactive()) {
                setInactive();
            }
            this.mCachedStateValues = new ContentValues();
            this.mCachedStateValues.put("owner", owner);
            this.mCachedStateValues.put("id", id);
        }
        this.mCachedStateValues.put(C.STATE, Integer.valueOf(STATE_LEAVING));
        this.mCachedStateValues.put("manually_stopped", Integer.valueOf(1));
        this.mDbHelper.updateState(this.mCachedStateValues);
    }

    public synchronized void setLastInviteSentTime(long time) {
        GroupState state = new GroupState(this.mCachedStateValues);
        if (state.isActive() && time > 0) {
            if (state.getLastInviteSentTime() != time) {
                this.mCachedStateValues.put(GroupStats.INVITES_SENT.column(), Integer.valueOf(state.getStatsCount(GroupStats.INVITES_SENT) + 1));
                this.mCachedStateValues.put("time_invited", Long.valueOf(time));
                this.mDbHelper.updateState(this.mCachedStateValues);
            }
        }
    }

    public synchronized void setCurrentMembersCount(int count, String owner, String id) {
        GroupState state = new GroupState(this.mCachedStateValues);
        if (state.isActive() && state.isSameGroup(owner, id) && count > 0) {
            if (state.getStatsCount(GroupStats.CURRENT_MEMBERS) != count) {
                this.mCachedStateValues.put(GroupStats.CURRENT_MEMBERS.column(), Integer.valueOf(count));
                if (count > state.getStatsCount(GroupStats.MAX_MEMBERS)) {
                    this.mCachedStateValues.put(GroupStats.MAX_MEMBERS.column(), Integer.valueOf(count));
                }
                this.mDbHelper.updateState(this.mCachedStateValues);
            }
        }
    }

    public synchronized void setMemberListVersion(String version) {
        checkStateCached(true);
        this.mCachedStateValues.put("member_list_verion", version);
        this.mDbHelper.updateState(this.mCachedStateValues);
    }

    public synchronized void incrementStatsCount(GroupStats stats, int delta, String owner, String id) {
        GroupState state = new GroupState(this.mCachedStateValues);
        if (state.isActive() && state.isSameGroup(owner, id) && delta > 0) {
            this.mCachedStateValues.put(stats.column(), Integer.valueOf(state.getStatsCount(stats) + delta));
            this.mDbHelper.updateState(this.mCachedStateValues);
        }
    }

    public synchronized GroupState getState() {
        return new GroupState(getCachedStateSnapshot());
    }

    public synchronized boolean isActive() {
        return getInt(getCachedState(), C.STATE, STATE_INACTIVE) == STATE_ACTIVE;
    }

    public synchronized boolean isLeaving() {
        return getInt(getCachedState(), C.STATE, STATE_INACTIVE) == STATE_LEAVING;
    }

    private synchronized boolean isInactive() {
        return getInt(getCachedState(), C.STATE, STATE_INACTIVE) == STATE_INACTIVE;
    }

    private synchronized String getOwner() {
        return getString(getCachedState(), "owner");
    }

    private synchronized String getId() {
        return getString(getCachedState(), "id");
    }

    /* access modifiers changed from: private */
    public synchronized String getThumbnail() {
        return getString(getCachedState(), "thumbnail");
    }

    private synchronized ContentValues getCachedState() {
        checkStateCached(true);
        return this.mCachedStateValues;
    }

    private synchronized ContentValues getCachedStateSnapshot() {
        ContentValues copiedStateValues;
        ContentValues cachedStateValues = getCachedState();
        if (cachedStateValues.containsKey("thumbnail")) {
            String thumbnail = getString(cachedStateValues, "thumbnail");
            cachedStateValues.remove("thumbnail");
            copiedStateValues = new ContentValues(cachedStateValues);
            cachedStateValues.put("thumbnail", thumbnail);
        } else {
            copiedStateValues = new ContentValues(cachedStateValues);
        }
        return copiedStateValues;
    }

    private synchronized boolean checkStateCached(boolean throwException) {
        if (this.mCachedStateValues == null) {
            this.mCachedStateValues = this.mDbHelper.readState();
        }
        if (throwException && this.mCachedStateValues == null) {
            throw new IllegalStateException("Couldn't get group state");
        }
        return this.mCachedStateValues != null;
    }

    /* access modifiers changed from: private */
    public static int getInt(ContentValues values, String key, int defaultValue) {
        Integer value = values != null ? values.getAsInteger(key) : null;
        return value == null ? defaultValue : value.intValue();
    }

    /* access modifiers changed from: private */
    public static long getLong(ContentValues values, String key, long defaultValue) {
        Long value = values != null ? values.getAsLong(key) : null;
        return value == null ? defaultValue : value.longValue();
    }

    /* access modifiers changed from: private */
    public static double getDouble(ContentValues values, String key, double defaultValue) {
        Double value = values != null ? values.getAsDouble(key) : null;
        return value == null ? defaultValue : value.doubleValue();
    }

    /* access modifiers changed from: private */
    public static String getString(ContentValues values, String key) {
        if (values != null) {
            return values.getAsString(key);
        }
        return null;
    }
}
