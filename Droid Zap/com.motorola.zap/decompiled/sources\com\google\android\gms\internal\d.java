package com.google.android.gms.internal;

import com.google.ads.AdSize;
import com.motorola.zap.ZapConstants;
import java.io.IOException;

public interface d {

    public static final class a extends ka<a> {
        private static volatile a[] fX;
        public String fY;
        public a[] fZ;
        public a[] ga;
        public a[] gb;
        public String gc;
        public String gd;
        public long ge;
        public boolean gf;
        public a[] gg;
        public int[] gh;
        public boolean gi;
        public int type;

        public a() {
            s();
        }

        public static a[] r() {
            if (fX == null) {
                synchronized (kc.aah) {
                    if (fX == null) {
                        fX = new a[0];
                    }
                }
            }
            return fX;
        }

        public void a(jz jzVar) throws IOException {
            jzVar.f(1, this.type);
            if (!this.fY.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                jzVar.b(2, this.fY);
            }
            if (this.fZ != null && this.fZ.length > 0) {
                for (a aVar : this.fZ) {
                    if (aVar != null) {
                        jzVar.a(3, (ke) aVar);
                    }
                }
            }
            if (this.ga != null && this.ga.length > 0) {
                for (a aVar2 : this.ga) {
                    if (aVar2 != null) {
                        jzVar.a(4, (ke) aVar2);
                    }
                }
            }
            if (this.gb != null && this.gb.length > 0) {
                for (a aVar3 : this.gb) {
                    if (aVar3 != null) {
                        jzVar.a(5, (ke) aVar3);
                    }
                }
            }
            if (!this.gc.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                jzVar.b(6, this.gc);
            }
            if (!this.gd.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                jzVar.b(7, this.gd);
            }
            if (this.ge != 0) {
                jzVar.b(8, this.ge);
            }
            if (this.gi) {
                jzVar.a(9, this.gi);
            }
            if (this.gh != null && this.gh.length > 0) {
                for (int f : this.gh) {
                    jzVar.f(10, f);
                }
            }
            if (this.gg != null && this.gg.length > 0) {
                for (a aVar4 : this.gg) {
                    if (aVar4 != null) {
                        jzVar.a(11, (ke) aVar4);
                    }
                }
            }
            if (this.gf) {
                jzVar.a(12, this.gf);
            }
            super.a(jzVar);
        }

        public int c() {
            int c = super.c() + jz.g(1, this.type);
            if (!this.fY.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                c += jz.g(2, this.fY);
            }
            if (this.fZ != null && this.fZ.length > 0) {
                int i = c;
                for (a aVar : this.fZ) {
                    if (aVar != null) {
                        i += jz.b(3, (ke) aVar);
                    }
                }
                c = i;
            }
            if (this.ga != null && this.ga.length > 0) {
                int i2 = c;
                for (a aVar2 : this.ga) {
                    if (aVar2 != null) {
                        i2 += jz.b(4, (ke) aVar2);
                    }
                }
                c = i2;
            }
            if (this.gb != null && this.gb.length > 0) {
                int i3 = c;
                for (a aVar3 : this.gb) {
                    if (aVar3 != null) {
                        i3 += jz.b(5, (ke) aVar3);
                    }
                }
                c = i3;
            }
            if (!this.gc.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                c += jz.g(6, this.gc);
            }
            if (!this.gd.equals(ZapConstants.GOOGLE_ACCOUNT_NONE)) {
                c += jz.g(7, this.gd);
            }
            if (this.ge != 0) {
                c += jz.d(8, this.ge);
            }
            if (this.gi) {
                c += jz.b(9, this.gi);
            }
            if (this.gh != null && this.gh.length > 0) {
                int i4 = 0;
                for (int cC : this.gh) {
                    i4 += jz.cC(cC);
                }
                c = c + i4 + (this.gh.length * 1);
            }
            if (this.gg != null && this.gg.length > 0) {
                for (a aVar4 : this.gg) {
                    if (aVar4 != null) {
                        c += jz.b(11, (ke) aVar4);
                    }
                }
            }
            if (this.gf) {
                c += jz.b(12, this.gf);
            }
            this.DY = c;
            return c;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof a)) {
                return false;
            }
            a aVar = (a) o;
            if (this.type != aVar.type) {
                return false;
            }
            if (this.fY == null) {
                if (aVar.fY != null) {
                    return false;
                }
            } else if (!this.fY.equals(aVar.fY)) {
                return false;
            }
            if (!kc.equals((Object[]) this.fZ, (Object[]) aVar.fZ) || !kc.equals((Object[]) this.ga, (Object[]) aVar.ga) || !kc.equals((Object[]) this.gb, (Object[]) aVar.gb)) {
                return false;
            }
            if (this.gc == null) {
                if (aVar.gc != null) {
                    return false;
                }
            } else if (!this.gc.equals(aVar.gc)) {
                return false;
            }
            if (this.gd == null) {
                if (aVar.gd != null) {
                    return false;
                }
            } else if (!this.gd.equals(aVar.gd)) {
                return false;
            }
            if (this.ge != aVar.ge || this.gf != aVar.gf || !kc.equals((Object[]) this.gg, (Object[]) aVar.gg) || !kc.equals(this.gh, aVar.gh) || this.gi != aVar.gi) {
                return false;
            }
            if (this.aae == null || this.aae.isEmpty()) {
                return aVar.aae == null || aVar.aae.isEmpty();
            }
            return this.aae.equals(aVar.aae);
        }

        public int hashCode() {
            int i = 1231;
            int i2 = 0;
            int hashCode = ((((((this.gf ? 1231 : 1237) + (((((this.gd == null ? 0 : this.gd.hashCode()) + (((this.gc == null ? 0 : this.gc.hashCode()) + (((((((((this.fY == null ? 0 : this.fY.hashCode()) + ((this.type + 527) * 31)) * 31) + kc.hashCode((Object[]) this.fZ)) * 31) + kc.hashCode((Object[]) this.ga)) * 31) + kc.hashCode((Object[]) this.gb)) * 31)) * 31)) * 31) + ((int) (this.ge ^ (this.ge >>> 32)))) * 31)) * 31) + kc.hashCode((Object[]) this.gg)) * 31) + kc.hashCode(this.gh)) * 31;
            if (!this.gi) {
                i = 1237;
            }
            int i3 = (hashCode + i) * 31;
            if (this.aae != null && !this.aae.isEmpty()) {
                i2 = this.aae.hashCode();
            }
            return i3 + i2;
        }

        /* renamed from: l */
        public a b(jy jyVar) throws IOException {
            int i;
            while (true) {
                int ky = jyVar.ky();
                switch (ky) {
                    case 0:
                        break;
                    case 8:
                        int kB = jyVar.kB();
                        switch (kB) {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                                this.type = kB;
                                break;
                            default:
                                continue;
                        }
                    case 18:
                        this.fY = jyVar.readString();
                        continue;
                    case 26:
                        int c = kh.c(jyVar, 26);
                        int length = this.fZ == null ? 0 : this.fZ.length;
                        a[] aVarArr = new a[(c + length)];
                        if (length != 0) {
                            System.arraycopy(this.fZ, 0, aVarArr, 0, length);
                        }
                        while (length < aVarArr.length - 1) {
                            aVarArr[length] = new a();
                            jyVar.a(aVarArr[length]);
                            jyVar.ky();
                            length++;
                        }
                        aVarArr[length] = new a();
                        jyVar.a(aVarArr[length]);
                        this.fZ = aVarArr;
                        continue;
                    case 34:
                        int c2 = kh.c(jyVar, 34);
                        int length2 = this.ga == null ? 0 : this.ga.length;
                        a[] aVarArr2 = new a[(c2 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.ga, 0, aVarArr2, 0, length2);
                        }
                        while (length2 < aVarArr2.length - 1) {
                            aVarArr2[length2] = new a();
                            jyVar.a(aVarArr2[length2]);
                            jyVar.ky();
                            length2++;
                        }
                        aVarArr2[length2] = new a();
                        jyVar.a(aVarArr2[length2]);
                        this.ga = aVarArr2;
                        continue;
                    case 42:
                        int c3 = kh.c(jyVar, 42);
                        int length3 = this.gb == null ? 0 : this.gb.length;
                        a[] aVarArr3 = new a[(c3 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.gb, 0, aVarArr3, 0, length3);
                        }
                        while (length3 < aVarArr3.length - 1) {
                            aVarArr3[length3] = new a();
                            jyVar.a(aVarArr3[length3]);
                            jyVar.ky();
                            length3++;
                        }
                        aVarArr3[length3] = new a();
                        jyVar.a(aVarArr3[length3]);
                        this.gb = aVarArr3;
                        continue;
                    case 50:
                        this.gc = jyVar.readString();
                        continue;
                    case 58:
                        this.gd = jyVar.readString();
                        continue;
                    case 64:
                        this.ge = jyVar.kA();
                        continue;
                    case 72:
                        this.gi = jyVar.kC();
                        continue;
                    case 80:
                        int c4 = kh.c(jyVar, 80);
                        int[] iArr = new int[c4];
                        int i2 = 0;
                        int i3 = 0;
                        while (i2 < c4) {
                            if (i2 != 0) {
                                jyVar.ky();
                            }
                            int kB2 = jyVar.kB();
                            switch (kB2) {
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case 17:
                                    i = i3 + 1;
                                    iArr[i3] = kB2;
                                    break;
                                default:
                                    i = i3;
                                    break;
                            }
                            i2++;
                            i3 = i;
                        }
                        if (i3 != 0) {
                            int length4 = this.gh == null ? 0 : this.gh.length;
                            if (length4 != 0 || i3 != iArr.length) {
                                int[] iArr2 = new int[(length4 + i3)];
                                if (length4 != 0) {
                                    System.arraycopy(this.gh, 0, iArr2, 0, length4);
                                }
                                System.arraycopy(iArr, 0, iArr2, length4, i3);
                                this.gh = iArr2;
                                break;
                            } else {
                                this.gh = iArr;
                                break;
                            }
                        } else {
                            continue;
                        }
                    case 82:
                        int cw = jyVar.cw(jyVar.kE());
                        int position = jyVar.getPosition();
                        int i4 = 0;
                        while (jyVar.kJ() > 0) {
                            switch (jyVar.kB()) {
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case 17:
                                    i4++;
                                    break;
                            }
                        }
                        if (i4 != 0) {
                            jyVar.cy(position);
                            int length5 = this.gh == null ? 0 : this.gh.length;
                            int[] iArr3 = new int[(i4 + length5)];
                            if (length5 != 0) {
                                System.arraycopy(this.gh, 0, iArr3, 0, length5);
                            }
                            while (jyVar.kJ() > 0) {
                                int kB3 = jyVar.kB();
                                switch (kB3) {
                                    case 1:
                                    case 2:
                                    case 3:
                                    case 4:
                                    case 5:
                                    case 6:
                                    case 7:
                                    case 8:
                                    case 9:
                                    case 10:
                                    case 11:
                                    case 12:
                                    case 13:
                                    case 14:
                                    case 15:
                                    case 16:
                                    case 17:
                                        int i5 = length5 + 1;
                                        iArr3[length5] = kB3;
                                        length5 = i5;
                                        break;
                                }
                            }
                            this.gh = iArr3;
                        }
                        jyVar.cx(cw);
                        continue;
                    case AdSize.LARGE_AD_HEIGHT /*90*/:
                        int c5 = kh.c(jyVar, 90);
                        int length6 = this.gg == null ? 0 : this.gg.length;
                        a[] aVarArr4 = new a[(c5 + length6)];
                        if (length6 != 0) {
                            System.arraycopy(this.gg, 0, aVarArr4, 0, length6);
                        }
                        while (length6 < aVarArr4.length - 1) {
                            aVarArr4[length6] = new a();
                            jyVar.a(aVarArr4[length6]);
                            jyVar.ky();
                            length6++;
                        }
                        aVarArr4[length6] = new a();
                        jyVar.a(aVarArr4[length6]);
                        this.gg = aVarArr4;
                        continue;
                    case 96:
                        this.gf = jyVar.kC();
                        continue;
                    default:
                        if (!a(jyVar, ky)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        public a s() {
            this.type = 1;
            this.fY = ZapConstants.GOOGLE_ACCOUNT_NONE;
            this.fZ = r();
            this.ga = r();
            this.gb = r();
            this.gc = ZapConstants.GOOGLE_ACCOUNT_NONE;
            this.gd = ZapConstants.GOOGLE_ACCOUNT_NONE;
            this.ge = 0;
            this.gf = false;
            this.gg = r();
            this.gh = kh.aaj;
            this.gi = false;
            this.aae = null;
            this.DY = -1;
            return this;
        }
    }
}
