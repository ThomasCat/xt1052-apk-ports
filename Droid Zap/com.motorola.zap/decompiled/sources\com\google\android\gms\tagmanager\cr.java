package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.c.h;
import com.motorola.zap.ZapConstants;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

class cr {

    public static class a {
        private final Map<String, com.google.android.gms.internal.d.a> Ws;
        private final com.google.android.gms.internal.d.a Wt;

        private a(Map<String, com.google.android.gms.internal.d.a> map, com.google.android.gms.internal.d.a aVar) {
            this.Ws = map;
            this.Wt = aVar;
        }

        public static b jE() {
            return new b();
        }

        public void a(String str, com.google.android.gms.internal.d.a aVar) {
            this.Ws.put(str, aVar);
        }

        public Map<String, com.google.android.gms.internal.d.a> jF() {
            return Collections.unmodifiableMap(this.Ws);
        }

        public com.google.android.gms.internal.d.a jG() {
            return this.Wt;
        }

        public String toString() {
            return "Properties: " + jF() + " pushAfterEvaluate: " + this.Wt;
        }
    }

    public static class b {
        private final Map<String, com.google.android.gms.internal.d.a> Ws;
        private com.google.android.gms.internal.d.a Wt;

        private b() {
            this.Ws = new HashMap();
        }

        public b b(String str, com.google.android.gms.internal.d.a aVar) {
            this.Ws.put(str, aVar);
            return this;
        }

        public b i(com.google.android.gms.internal.d.a aVar) {
            this.Wt = aVar;
            return this;
        }

        public a jH() {
            return new a(this.Ws, this.Wt);
        }
    }

    public static class c {
        private final String Un;
        private final List<e> Wu;
        private final Map<String, List<a>> Wv;
        private final int Ww;

        private c(List<e> list, Map<String, List<a>> map, String str, int i) {
            this.Wu = Collections.unmodifiableList(list);
            this.Wv = Collections.unmodifiableMap(map);
            this.Un = str;
            this.Ww = i;
        }

        public static d jI() {
            return new d();
        }

        public String getVersion() {
            return this.Un;
        }

        public List<e> jJ() {
            return this.Wu;
        }

        public Map<String, List<a>> jK() {
            return this.Wv;
        }

        public String toString() {
            return "Rules: " + jJ() + "  Macros: " + this.Wv;
        }
    }

    public static class d {
        private String Un;
        private final List<e> Wu;
        private final Map<String, List<a>> Wv;
        private int Ww;

        private d() {
            this.Wu = new ArrayList();
            this.Wv = new HashMap();
            this.Un = ZapConstants.GOOGLE_ACCOUNT_NONE;
            this.Ww = 0;
        }

        public d a(a aVar) {
            String j = di.j((com.google.android.gms.internal.d.a) aVar.jF().get(com.google.android.gms.internal.b.INSTANCE_NAME.toString()));
            List list = (List) this.Wv.get(j);
            if (list == null) {
                list = new ArrayList();
                this.Wv.put(j, list);
            }
            list.add(aVar);
            return this;
        }

        public d a(e eVar) {
            this.Wu.add(eVar);
            return this;
        }

        public d bW(int i) {
            this.Ww = i;
            return this;
        }

        public d bx(String str) {
            this.Un = str;
            return this;
        }

        public c jL() {
            return new c(this.Wu, this.Wv, this.Un, this.Ww);
        }
    }

    public static class e {
        private final List<a> WA;
        private final List<a> WB;
        private final List<a> WC;
        private final List<String> WD;
        private final List<String> WE;
        private final List<String> WF;
        private final List<String> WG;
        private final List<a> Wx;
        private final List<a> Wy;
        private final List<a> Wz;

        private e(List<a> list, List<a> list2, List<a> list3, List<a> list4, List<a> list5, List<a> list6, List<String> list7, List<String> list8, List<String> list9, List<String> list10) {
            this.Wx = Collections.unmodifiableList(list);
            this.Wy = Collections.unmodifiableList(list2);
            this.Wz = Collections.unmodifiableList(list3);
            this.WA = Collections.unmodifiableList(list4);
            this.WB = Collections.unmodifiableList(list5);
            this.WC = Collections.unmodifiableList(list6);
            this.WD = Collections.unmodifiableList(list7);
            this.WE = Collections.unmodifiableList(list8);
            this.WF = Collections.unmodifiableList(list9);
            this.WG = Collections.unmodifiableList(list10);
        }

        public static f jM() {
            return new f();
        }

        public List<a> jN() {
            return this.Wx;
        }

        public List<a> jO() {
            return this.Wy;
        }

        public List<a> jP() {
            return this.Wz;
        }

        public List<a> jQ() {
            return this.WA;
        }

        public List<a> jR() {
            return this.WB;
        }

        public List<String> jS() {
            return this.WD;
        }

        public List<String> jT() {
            return this.WE;
        }

        public List<String> jU() {
            return this.WF;
        }

        public List<String> jV() {
            return this.WG;
        }

        public List<a> jW() {
            return this.WC;
        }

        public String toString() {
            return "Positive predicates: " + jN() + "  Negative predicates: " + jO() + "  Add tags: " + jP() + "  Remove tags: " + jQ() + "  Add macros: " + jR() + "  Remove macros: " + jW();
        }
    }

    public static class f {
        private final List<a> WA;
        private final List<a> WB;
        private final List<a> WC;
        private final List<String> WD;
        private final List<String> WE;
        private final List<String> WF;
        private final List<String> WG;
        private final List<a> Wx;
        private final List<a> Wy;
        private final List<a> Wz;

        private f() {
            this.Wx = new ArrayList();
            this.Wy = new ArrayList();
            this.Wz = new ArrayList();
            this.WA = new ArrayList();
            this.WB = new ArrayList();
            this.WC = new ArrayList();
            this.WD = new ArrayList();
            this.WE = new ArrayList();
            this.WF = new ArrayList();
            this.WG = new ArrayList();
        }

        public f b(a aVar) {
            this.Wx.add(aVar);
            return this;
        }

        public f bA(String str) {
            this.WD.add(str);
            return this;
        }

        public f bB(String str) {
            this.WE.add(str);
            return this;
        }

        public f by(String str) {
            this.WF.add(str);
            return this;
        }

        public f bz(String str) {
            this.WG.add(str);
            return this;
        }

        public f c(a aVar) {
            this.Wy.add(aVar);
            return this;
        }

        public f d(a aVar) {
            this.Wz.add(aVar);
            return this;
        }

        public f e(a aVar) {
            this.WA.add(aVar);
            return this;
        }

        public f f(a aVar) {
            this.WB.add(aVar);
            return this;
        }

        public f g(a aVar) {
            this.WC.add(aVar);
            return this;
        }

        public e jX() {
            return new e(this.Wx, this.Wy, this.Wz, this.WA, this.WB, this.WC, this.WD, this.WE, this.WF, this.WG);
        }
    }

    public static class g extends Exception {
        public g(String str) {
            super(str);
        }
    }

    private static com.google.android.gms.internal.d.a a(int i, com.google.android.gms.internal.c.f fVar, com.google.android.gms.internal.d.a[] aVarArr, Set<Integer> set) throws g {
        int i2 = 0;
        if (set.contains(Integer.valueOf(i))) {
            bw("Value cycle detected.  Current value reference: " + i + "." + "  Previous value references: " + set + ".");
        }
        com.google.android.gms.internal.d.a aVar = (com.google.android.gms.internal.d.a) a(fVar.fi, i, "values");
        if (aVarArr[i] != null) {
            return aVarArr[i];
        }
        com.google.android.gms.internal.d.a aVar2 = null;
        set.add(Integer.valueOf(i));
        switch (aVar.type) {
            case 1:
            case 5:
            case 6:
            case 8:
                aVar2 = aVar;
                break;
            case 2:
                h h = h(aVar);
                aVar2 = g(aVar);
                aVar2.fZ = new com.google.android.gms.internal.d.a[h.fK.length];
                int[] iArr = h.fK;
                int length = iArr.length;
                int i3 = 0;
                while (i2 < length) {
                    int i4 = i3 + 1;
                    aVar2.fZ[i3] = a(iArr[i2], fVar, aVarArr, set);
                    i2++;
                    i3 = i4;
                }
                break;
            case 3:
                aVar2 = g(aVar);
                h h2 = h(aVar);
                if (h2.fL.length != h2.fM.length) {
                    bw("Uneven map keys (" + h2.fL.length + ") and map values (" + h2.fM.length + ")");
                }
                aVar2.ga = new com.google.android.gms.internal.d.a[h2.fL.length];
                aVar2.gb = new com.google.android.gms.internal.d.a[h2.fL.length];
                int[] iArr2 = h2.fL;
                int length2 = iArr2.length;
                int i5 = 0;
                int i6 = 0;
                while (i5 < length2) {
                    int i7 = i6 + 1;
                    aVar2.ga[i6] = a(iArr2[i5], fVar, aVarArr, set);
                    i5++;
                    i6 = i7;
                }
                int[] iArr3 = h2.fM;
                int length3 = iArr3.length;
                int i8 = 0;
                while (i2 < length3) {
                    int i9 = i8 + 1;
                    aVar2.gb[i8] = a(iArr3[i2], fVar, aVarArr, set);
                    i2++;
                    i8 = i9;
                }
                break;
            case 4:
                aVar2 = g(aVar);
                aVar2.gc = di.j(a(h(aVar).fP, fVar, aVarArr, set));
                break;
            case 7:
                aVar2 = g(aVar);
                h h3 = h(aVar);
                aVar2.gg = new com.google.android.gms.internal.d.a[h3.fO.length];
                int[] iArr4 = h3.fO;
                int length4 = iArr4.length;
                int i10 = 0;
                while (i2 < length4) {
                    int i11 = i10 + 1;
                    aVar2.gg[i10] = a(iArr4[i2], fVar, aVarArr, set);
                    i2++;
                    i10 = i11;
                }
                break;
        }
        if (aVar2 == null) {
            bw("Invalid value: " + aVar);
        }
        aVarArr[i] = aVar2;
        set.remove(Integer.valueOf(i));
        return aVar2;
    }

    private static a a(com.google.android.gms.internal.c.b bVar, com.google.android.gms.internal.c.f fVar, com.google.android.gms.internal.d.a[] aVarArr, int i) throws g {
        b jE = a.jE();
        for (int valueOf : bVar.eS) {
            com.google.android.gms.internal.c.e eVar = (com.google.android.gms.internal.c.e) a(fVar.fj, Integer.valueOf(valueOf).intValue(), "properties");
            String str = (String) a(fVar.fh, eVar.key, "keys");
            com.google.android.gms.internal.d.a aVar = (com.google.android.gms.internal.d.a) a(aVarArr, eVar.value, "values");
            if (com.google.android.gms.internal.b.PUSH_AFTER_EVALUATE.toString().equals(str)) {
                jE.i(aVar);
            } else {
                jE.b(str, aVar);
            }
        }
        return jE.jH();
    }

    private static e a(com.google.android.gms.internal.c.g gVar, List<a> list, List<a> list2, List<a> list3, com.google.android.gms.internal.c.f fVar) {
        f jM = e.jM();
        for (int valueOf : gVar.fy) {
            jM.b((a) list3.get(Integer.valueOf(valueOf).intValue()));
        }
        for (int valueOf2 : gVar.fz) {
            jM.c((a) list3.get(Integer.valueOf(valueOf2).intValue()));
        }
        for (int valueOf3 : gVar.fA) {
            jM.d((a) list.get(Integer.valueOf(valueOf3).intValue()));
        }
        for (int valueOf4 : gVar.fC) {
            jM.by(fVar.fi[Integer.valueOf(valueOf4).intValue()].fY);
        }
        for (int valueOf5 : gVar.fB) {
            jM.e((a) list.get(Integer.valueOf(valueOf5).intValue()));
        }
        for (int valueOf6 : gVar.fD) {
            jM.bz(fVar.fi[Integer.valueOf(valueOf6).intValue()].fY);
        }
        for (int valueOf7 : gVar.fE) {
            jM.f((a) list2.get(Integer.valueOf(valueOf7).intValue()));
        }
        for (int valueOf8 : gVar.fG) {
            jM.bA(fVar.fi[Integer.valueOf(valueOf8).intValue()].fY);
        }
        for (int valueOf9 : gVar.fF) {
            jM.g((a) list2.get(Integer.valueOf(valueOf9).intValue()));
        }
        for (int valueOf10 : gVar.fH) {
            jM.bB(fVar.fi[Integer.valueOf(valueOf10).intValue()].fY);
        }
        return jM.jX();
    }

    private static <T> T a(T[] tArr, int i, String str) throws g {
        if (i < 0 || i >= tArr.length) {
            bw("Index out of bounds detected: " + i + " in " + str);
        }
        return tArr[i];
    }

    public static c b(com.google.android.gms.internal.c.f fVar) throws g {
        com.google.android.gms.internal.d.a[] aVarArr = new com.google.android.gms.internal.d.a[fVar.fi.length];
        for (int i = 0; i < fVar.fi.length; i++) {
            a(i, fVar, aVarArr, (Set<Integer>) new HashSet<Integer>(0));
        }
        d jI = c.jI();
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < fVar.fl.length; i2++) {
            arrayList.add(a(fVar.fl[i2], fVar, aVarArr, i2));
        }
        ArrayList arrayList2 = new ArrayList();
        for (int i3 = 0; i3 < fVar.fm.length; i3++) {
            arrayList2.add(a(fVar.fm[i3], fVar, aVarArr, i3));
        }
        ArrayList arrayList3 = new ArrayList();
        for (int i4 = 0; i4 < fVar.fk.length; i4++) {
            a a2 = a(fVar.fk[i4], fVar, aVarArr, i4);
            jI.a(a2);
            arrayList3.add(a2);
        }
        for (com.google.android.gms.internal.c.g a3 : fVar.fn) {
            jI.a(a(a3, arrayList, arrayList3, arrayList2, fVar));
        }
        jI.bx(fVar.fr);
        jI.bW(fVar.fw);
        return jI.jL();
    }

    public static void b(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    private static void bw(String str) throws g {
        bh.t(str);
        throw new g(str);
    }

    public static com.google.android.gms.internal.d.a g(com.google.android.gms.internal.d.a aVar) {
        com.google.android.gms.internal.d.a aVar2 = new com.google.android.gms.internal.d.a();
        aVar2.type = aVar.type;
        aVar2.gh = (int[]) aVar.gh.clone();
        if (aVar.gi) {
            aVar2.gi = aVar.gi;
        }
        return aVar2;
    }

    private static h h(com.google.android.gms.internal.d.a aVar) throws g {
        if (((h) aVar.a(h.fI)) == null) {
            bw("Expected a ServingValue and didn't get one. Value is: " + aVar);
        }
        return (h) aVar.a(h.fI);
    }
}
