package com.google.android.gms.tagmanager;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import com.google.android.gms.internal.b;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class i extends dg {
    private static final String ID = com.google.android.gms.internal.a.ARBITRARY_PIXEL.toString();
    private static final String TF = b.ADDITIONAL_PARAMS.toString();
    private static final String TG = b.UNREPEATABLE.toString();
    static final String TH = ("gtm_" + ID + "_unrepeatable");
    private static final Set<String> TI = new HashSet();
    private static final String URL = b.URL.toString();
    private final a TJ;
    private final Context mContext;

    public interface a {
        aq iz();
    }

    public i(final Context context) {
        this(context, new a() {
            public aq iz() {
                return y.F(context);
            }
        });
    }

    i(Context context, a aVar) {
        super(ID, URL);
        this.TJ = aVar;
        this.mContext = context;
    }

    private synchronized boolean aU(String str) {
        boolean z = true;
        synchronized (this) {
            if (!aW(str)) {
                if (aV(str)) {
                    TI.add(str);
                } else {
                    z = false;
                }
            }
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public boolean aV(String str) {
        return this.mContext.getSharedPreferences(TH, 0).contains(str);
    }

    /* access modifiers changed from: 0000 */
    public boolean aW(String str) {
        return TI.contains(str);
    }

    public void w(Map<String, com.google.android.gms.internal.d.a> map) {
        String str = map.get(TG) != null ? di.j((com.google.android.gms.internal.d.a) map.get(TG)) : null;
        if (str == null || !aU(str)) {
            Builder buildUpon = Uri.parse(di.j((com.google.android.gms.internal.d.a) map.get(URL))).buildUpon();
            com.google.android.gms.internal.d.a aVar = (com.google.android.gms.internal.d.a) map.get(TF);
            if (aVar != null) {
                Object o = di.o(aVar);
                if (!(o instanceof List)) {
                    bh.t("ArbitraryPixel: additional params not a list: not sending partial hit: " + buildUpon.build().toString());
                    return;
                }
                for (Object next : (List) o) {
                    if (!(next instanceof Map)) {
                        bh.t("ArbitraryPixel: additional params contains non-map: not sending partial hit: " + buildUpon.build().toString());
                        return;
                    }
                    for (Entry entry : ((Map) next).entrySet()) {
                        buildUpon.appendQueryParameter(entry.getKey().toString(), entry.getValue().toString());
                    }
                }
            }
            String uri = buildUpon.build().toString();
            this.TJ.iz().bk(uri);
            bh.v("ArbitraryPixel: url = " + uri);
            if (str != null) {
                synchronized (i.class) {
                    TI.add(str);
                    cz.a(this.mContext, TH, str, "true");
                }
            }
        }
    }
}
