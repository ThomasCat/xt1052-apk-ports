package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public class b implements Creator<FieldOnlyFilter> {
    static void a(FieldOnlyFilter fieldOnlyFilter, Parcel parcel, int i) {
        int p = com.google.android.gms.common.internal.safeparcel.b.p(parcel);
        com.google.android.gms.common.internal.safeparcel.b.c(parcel, 1000, fieldOnlyFilter.wj);
        com.google.android.gms.common.internal.safeparcel.b.a(parcel, 1, (Parcelable) fieldOnlyFilter.EP, i, false);
        com.google.android.gms.common.internal.safeparcel.b.D(parcel, p);
    }

    /* renamed from: aI */
    public FieldOnlyFilter[] newArray(int i) {
        return new FieldOnlyFilter[i];
    }

    /* renamed from: ad */
    public FieldOnlyFilter createFromParcel(Parcel parcel) {
        int o = a.o(parcel);
        int i = 0;
        MetadataBundle metadataBundle = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    metadataBundle = (MetadataBundle) a.a(parcel, n, MetadataBundle.CREATOR);
                    break;
                case 1000:
                    i = a.g(parcel, n);
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new FieldOnlyFilter(i, metadataBundle);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }
}
