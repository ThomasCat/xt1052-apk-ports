package com.motorola.sharing.provider.cloud;

import android.content.Context;
import com.motorola.gcsp.json.StatusCode;
import com.motorola.sharing.Authenticator;
import com.motorola.sharing.Authenticator.TokenInfo;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.ContentShareListener.ContentStopShareEvent.Substatus;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.instrumentation.ActionResult.Error;
import com.motorola.sharing.instrumentation.CheckinManager;
import com.motorola.sharing.util.Logger;
import java.net.UnknownHostException;
import org.json.JSONException;
import org.json.JSONObject;

class ContentStopShare {
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getCloudTag("ContentStopShare");
    private com.motorola.sharing.Authenticator.Callback mAuthCallback;
    private Callback mCallerCallback;
    /* access modifiers changed from: private */
    public ContentContainer mContainer;
    private Context mContext;
    private boolean mIsStarted;

    public interface Callback {
        void onComplete(ContentContainer contentContainer, Status status, Substatus substatus);
    }

    ContentStopShare(ContentContainer container) {
        this.mIsStarted = false;
        this.mContext = null;
        this.mContainer = null;
        this.mCallerCallback = null;
        this.mAuthCallback = new com.motorola.sharing.Authenticator.Callback() {
            public void onAuthComplete(Authenticator.Status status, TokenInfo tokenInfo, Object data) {
                ContentStopShare.this.mContainer.userCred.set(tokenInfo);
                if (!ContentStopShare.this.mContainer.userCred.isEmpty()) {
                    ContentStopShare.this.startTask();
                    return;
                }
                Logger.e(ContentStopShare.TAG, "authentication failed");
                ContentStopShare.this.mContainer.actionResult.setError(status == Authenticator.Status.ERROR_NETWORK ? Error.AUTH_NETWORK_ERROR : Error.NOT_AUTHORIZED);
                ContentStopShare.this.onComplete(Status.ERROR, Substatus.AUTH);
            }
        };
        this.mContext = GCSP.getContext();
        this.mContainer = container;
    }

    public void start(Callback callback) {
        synchronized (this) {
            if (this.mIsStarted) {
                throw new IllegalStateException("task already started");
            }
            this.mIsStarted = true;
        }
        this.mCallerCallback = callback;
        TokenInfo tokenInfo = GCSP.getAuthenticator().getTokenInfo();
        if (tokenInfo == null || !tokenInfo.isValid()) {
            Logger.d(TAG, "no valid local token, requesting a new one");
            GCSP.getAuthenticator().authenticate(this.mAuthCallback, null);
            return;
        }
        this.mContainer.userCred.set(tokenInfo);
        if (this.mContainer.userCred.isEmpty()) {
            this.mContainer.actionResult.setError(Error.NOT_AUTHORIZED);
            onComplete(Status.ERROR, Substatus.AUTH);
            return;
        }
        startTask();
    }

    /* access modifiers changed from: private */
    public void onComplete(Status status, Substatus substatus) {
        if (this.mCallerCallback != null) {
            this.mCallerCallback.onComplete(this.mContainer, status, substatus);
        }
        CheckinManager.getInstance().logZapCancelShare(this.mContainer);
    }

    /* access modifiers changed from: private */
    public void startTask() {
        new Thread() {
            public void run() {
                try {
                    ContentStopShare.this.sendStopShare();
                } catch (JSONException e) {
                    Logger.e(ContentStopShare.TAG, "response not parsable: " + e);
                    ContentStopShare.this.mContainer.actionResult.setError(Error.RESPONSE_NOT_PARSABLE);
                    ContentStopShare.this.onComplete(Status.ERROR, Substatus.GENERAL);
                } catch (ConnectionException e2) {
                    ContentStopShare.this.mContainer.actionResult.setError(Error.CONNECTION_EXCEPTION, null, e2.getStatusCode());
                    ContentStopShare.this.onComplete(Status.ERROR, Substatus.GENERAL);
                } catch (UnknownHostException e3) {
                    ContentStopShare.this.mContainer.actionResult.setError(Error.UNKNOWN_HOST_EXCEPTION);
                    ContentStopShare.this.onComplete(Status.ERROR, Substatus.NETWORK);
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public boolean sendStopShare() throws JSONException, ConnectionException, UnknownHostException {
        JSONObject bodyJson = new JSONObject();
        bodyJson.put("fromEmail", this.mContainer.userCred.account);
        bodyJson.put("shareId", this.mContainer.shareId);
        bodyJson.put("shareStatus", "cancelled");
        Logger.d(TAG, "Update Share Request:");
        Logger.dump(TAG, bodyJson);
        String response = HttpHelper.sendPostRequest(UrlHelper.getUpdateShare(), bodyJson.toString(), this.mContainer.userCred.token, this.mContainer.userCred.id, true);
        if (response == null) {
            Logger.e(TAG, "got empty server response");
            this.mContainer.actionResult.setError(Error.EMPTY_RESPONSE);
            onComplete(Status.ERROR, Substatus.GENERAL);
            return false;
        }
        JSONObject responseJson = new JSONObject(response);
        StatusCode status = StatusCode.codeOf(responseJson.getString("status"));
        Logger.d(TAG, "Stop Share Response:");
        Logger.dump(TAG, responseJson);
        if (status == StatusCode.OK) {
            this.mContainer.actionResult.setSuccess();
            onComplete(Status.SUCCESS, Substatus.NOT_SHARED);
            return true;
        }
        Logger.e(TAG, "invalid server response status: " + status);
        this.mContainer.actionResult.setError(Error.INVALID_STATUS, status.name(), 0);
        onComplete(Status.ERROR, Substatus.GENERAL);
        return false;
    }
}
