package com.google.sample.castcompanionlibrary.cast.callbacks;

import android.support.v7.media.MediaRouter.RouteInfo;
import com.google.android.gms.common.ConnectionResult;
import com.google.sample.castcompanionlibrary.cast.exceptions.OnFailedListener;

public interface IBaseCastConsumer extends OnFailedListener {
    void onCastAvailabilityChanged(boolean z);

    void onCastDeviceDetected(RouteInfo routeInfo);

    void onConnected();

    boolean onConnectionFailed(ConnectionResult connectionResult);

    void onConnectionSuspended(int i);

    void onConnectivityRecovered();

    void onDisconnected();
}
