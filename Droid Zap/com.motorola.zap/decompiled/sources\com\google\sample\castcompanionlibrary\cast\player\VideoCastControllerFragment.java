package com.google.sample.castcompanionlibrary.cast.player;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.SeekBar;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.sample.castcompanionlibrary.cast.VideoCastManager;
import com.google.sample.castcompanionlibrary.cast.callbacks.VideoCastConsumerImpl;
import com.google.sample.castcompanionlibrary.cast.exceptions.CastException;
import com.google.sample.castcompanionlibrary.cast.exceptions.NoConnectionException;
import com.google.sample.castcompanionlibrary.cast.exceptions.TransientNetworkDisconnectionException;
import com.google.sample.castcompanionlibrary.utils.LogUtils;
import com.google.sample.castcompanionlibrary.utils.Utils;
import com.motorola.zap.R;
import com.motorola.zap.ZapConstants;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONException;
import org.json.JSONObject;

public class VideoCastControllerFragment extends Fragment implements IMediaAuthListener, OnVideoCastControllerListener {
    /* access modifiers changed from: private */
    public static final String TAG = LogUtils.makeLogTag(VideoCastControllerFragment.class);
    /* access modifiers changed from: private */
    public static boolean sDialogCanceled = false;
    protected boolean mAuthSuccess = true;
    private Thread mAuthThread;
    private MyCastConsumer mCastConsumer;
    /* access modifiers changed from: private */
    public IVideoCastController mCastController;
    /* access modifiers changed from: private */
    public VideoCastManager mCastManager;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private AsyncTask<String, Void, Bitmap> mImageAsyncTask;
    private boolean mIsFresh = true;
    /* access modifiers changed from: private */
    public IMediaAuthService mMediaAuthService;
    private Timer mMediaAuthTimer;
    /* access modifiers changed from: private */
    public OverallState mOverallState = OverallState.UNKNOWN;
    /* access modifiers changed from: private */
    public int mPlaybackState;
    private Timer mSeekbarTimer;
    /* access modifiers changed from: private */
    public MediaInfo mSelectedMedia;
    /* access modifiers changed from: private */
    public UrlAndBitmap mUrlAndBitmap;

    public static class ErrorDialogFragment extends DialogFragment {
        /* access modifiers changed from: private */
        public IVideoCastController mController;

        public static ErrorDialogFragment newInstance(String message) {
            ErrorDialogFragment frag = new ErrorDialogFragment();
            Bundle args = new Bundle();
            args.putString("message", message);
            frag.setArguments(args);
            return frag;
        }

        public void onAttach(Activity activity) {
            this.mController = (IVideoCastController) activity;
            super.onAttach(activity);
            setCancelable(false);
        }

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return new Builder(getActivity()).setTitle(R.string.error).setMessage(getArguments().getString("message")).setPositiveButton(R.string.ok, new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    VideoCastControllerFragment.sDialogCanceled = true;
                    ErrorDialogFragment.this.mController.closeActivity();
                }
            }).create();
        }
    }

    class MediaAuthServiceTimerTask extends TimerTask {
        private final Thread mThread;

        public MediaAuthServiceTimerTask(Thread thread) {
            this.mThread = thread;
        }

        public void run() {
            if (this.mThread != null) {
                LogUtils.LOGD(VideoCastControllerFragment.TAG, "Timer is expired, going to interrupt the thread");
                this.mThread.interrupt();
                VideoCastControllerFragment.this.mHandler.post(new Runnable() {
                    public void run() {
                        VideoCastControllerFragment.this.mCastController.showLoading(false);
                        VideoCastControllerFragment.this.showErrorDialog(VideoCastControllerFragment.this.getString(R.string.failed_authorization_timeout));
                        VideoCastControllerFragment.this.mAuthSuccess = false;
                        if (VideoCastControllerFragment.this.mMediaAuthService != null && VideoCastControllerFragment.this.mMediaAuthService.getStatus() == MediaAuthStatus.PENDING) {
                            VideoCastControllerFragment.this.mMediaAuthService.abort(MediaAuthStatus.ABORT_TIMEOUT);
                        }
                    }
                });
            }
        }
    }

    private class MyCastConsumer extends VideoCastConsumerImpl {
        private MyCastConsumer() {
        }

        public void onDisconnected() {
            VideoCastControllerFragment.this.mCastController.closeActivity();
        }

        public void onApplicationDisconnected(int errorCode) {
            VideoCastControllerFragment.this.mCastController.closeActivity();
        }

        public void onRemoteMediaPlayerMetadataUpdated() {
            try {
                VideoCastControllerFragment.this.mSelectedMedia = VideoCastControllerFragment.this.mCastManager.getRemoteMediaInformation();
                VideoCastControllerFragment.this.updateMetadata();
            } catch (TransientNetworkDisconnectionException e) {
                LogUtils.LOGE(VideoCastControllerFragment.TAG, "Failed to update the metadata due to network issues", e);
            } catch (NoConnectionException e2) {
                LogUtils.LOGE(VideoCastControllerFragment.TAG, "Failed to update the metadata due to network issues", e2);
            }
        }

        public void onRemoteMediaPlayerStatusUpdated() {
            VideoCastControllerFragment.this.updatePlayerStatus();
        }

        public void onConnectionSuspended(int cause) {
            VideoCastControllerFragment.this.mCastController.updateControllersStatus(false);
        }

        public void onConnectivityRecovered() {
            VideoCastControllerFragment.this.mCastController.updateControllersStatus(true);
        }
    }

    private enum OverallState {
        AUTHORIZING,
        PLAYBACK,
        UNKNOWN
    }

    private class UpdateSeekbarTask extends TimerTask {
        private UpdateSeekbarTask() {
        }

        public void run() {
            VideoCastControllerFragment.this.mHandler.post(new Runnable() {
                public void run() {
                    if (VideoCastControllerFragment.this.mPlaybackState != 4 && VideoCastControllerFragment.this.mCastManager.isConnected()) {
                        try {
                            int duration = (int) VideoCastControllerFragment.this.mCastManager.getMediaDuration();
                            if (duration > 0) {
                                try {
                                    VideoCastControllerFragment.this.mCastController.updateSeekbar((int) VideoCastControllerFragment.this.mCastManager.getCurrentMediaPosition(), duration);
                                } catch (Exception e) {
                                    LogUtils.LOGE(VideoCastControllerFragment.TAG, "Failed to get current media position", e);
                                }
                            }
                        } catch (TransientNetworkDisconnectionException e2) {
                            LogUtils.LOGE(VideoCastControllerFragment.TAG, "Failed to update the progress bar due to network issues", e2);
                        } catch (NoConnectionException e3) {
                            LogUtils.LOGE(VideoCastControllerFragment.TAG, "Failed to update the progress bar due to network issues", e3);
                        }
                    }
                }
            });
        }
    }

    private class UrlAndBitmap {
        /* access modifiers changed from: private */
        public Bitmap mBitmap;
        /* access modifiers changed from: private */
        public String mUrl;

        private UrlAndBitmap() {
        }

        /* access modifiers changed from: private */
        public boolean isMatch(String url) {
            return (url == null || this.mBitmap == null || !url.equals(this.mUrl)) ? false : true;
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        sDialogCanceled = false;
        this.mCastController = (IVideoCastController) activity;
        this.mHandler = new Handler();
        try {
            this.mCastManager = VideoCastManager.getInstance(activity);
        } catch (CastException e) {
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mCastConsumer = new MyCastConsumer();
        Bundle bundle = getArguments();
        if (bundle != null) {
            Bundle extras = bundle.getBundle("extras");
            Bundle mediaWrapper = extras.getBundle(VideoCastManager.EXTRA_MEDIA);
            setRetainInstance(true);
            if (extras.getBoolean(VideoCastManager.EXTRA_HAS_AUTH)) {
                this.mOverallState = OverallState.AUTHORIZING;
                this.mMediaAuthService = this.mCastManager.getMediaAuthService();
                handleMediaAuthTask(this.mMediaAuthService);
                showImage(Utils.getImageUrl(this.mMediaAuthService.getMediaInfo(), 1));
            } else if (mediaWrapper != null) {
                this.mOverallState = OverallState.PLAYBACK;
                boolean shouldStartPlayback = extras.getBoolean(VideoCastManager.EXTRA_SHOULD_START);
                String customDataStr = extras.getString(VideoCastManager.EXTRA_CUSTOM_DATA);
                JSONObject customData = null;
                if (!TextUtils.isEmpty(customDataStr)) {
                    try {
                        customData = new JSONObject(customDataStr);
                    } catch (JSONException e) {
                        LogUtils.LOGE(TAG, "Failed to unmarshalize custom data string: customData=" + customDataStr, e);
                    }
                }
                onReady(Utils.toMediaInfo(mediaWrapper), shouldStartPlayback, extras.getInt(VideoCastManager.EXTRA_START_POINT, 0), customData);
            }
        }
    }

    private void handleMediaAuthTask(final IMediaAuthService authService) {
        this.mCastController.showLoading(true);
        this.mCastController.setLine2(authService.getPendingMessage() != null ? authService.getPendingMessage() : ZapConstants.GOOGLE_ACCOUNT_NONE);
        this.mAuthThread = new Thread(new Runnable() {
            public void run() {
                if (authService != null) {
                    try {
                        authService.setOnResult(VideoCastControllerFragment.this);
                        authService.start();
                    } catch (Exception e) {
                        LogUtils.LOGE(VideoCastControllerFragment.TAG, "mAuthService.start() encountered exception", e);
                        VideoCastControllerFragment.this.mAuthSuccess = false;
                    }
                }
            }
        });
        this.mAuthThread.start();
        this.mMediaAuthTimer = new Timer();
        this.mMediaAuthTimer.schedule(new MediaAuthServiceTimerTask(this.mAuthThread), authService.getTimeout());
    }

    /* access modifiers changed from: private */
    public void onReady(MediaInfo mediaInfo, boolean shouldStartPlayback, int startPoint, JSONObject customData) {
        this.mSelectedMedia = mediaInfo;
        try {
            this.mCastController.setStreamType(this.mSelectedMedia.getStreamType());
            if (shouldStartPlayback) {
                this.mPlaybackState = 4;
                this.mCastController.setPlaybackStatus(this.mPlaybackState);
                this.mCastManager.loadMedia(this.mSelectedMedia, true, startPoint, customData);
            } else {
                if (this.mCastManager.isRemoteMoviePlaying()) {
                    this.mPlaybackState = 2;
                } else {
                    this.mPlaybackState = 3;
                }
                this.mCastController.setPlaybackStatus(this.mPlaybackState);
            }
        } catch (Exception e) {
            LogUtils.LOGE(TAG, "Failed to get playback and media information", e);
            this.mCastController.closeActivity();
        }
        updateMetadata();
        restartTrickplayTimer();
    }

    private void stopTrickplayTimer() {
        LogUtils.LOGD(TAG, "Stopped TrickPlay Timer");
        if (this.mSeekbarTimer != null) {
            this.mSeekbarTimer.cancel();
        }
    }

    private void restartTrickplayTimer() {
        stopTrickplayTimer();
        this.mSeekbarTimer = new Timer();
        this.mSeekbarTimer.scheduleAtFixedRate(new UpdateSeekbarTask(), 100, 1000);
        LogUtils.LOGD(TAG, "Restarted TrickPlay Timer");
    }

    private void updateOverallState() {
        switch (this.mOverallState) {
            case AUTHORIZING:
                IMediaAuthService authService = this.mCastManager.getMediaAuthService();
                if (authService != null) {
                    this.mCastController.setLine2(authService.getPendingMessage() != null ? authService.getPendingMessage() : ZapConstants.GOOGLE_ACCOUNT_NONE);
                    this.mCastController.showLoading(true);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void updateMetadata() {
        boolean isLive = true;
        String imageUrl = null;
        if (this.mSelectedMedia != null) {
            imageUrl = Utils.getImageUrl(this.mSelectedMedia, 1);
        } else if (this.mMediaAuthService != null) {
            imageUrl = Utils.getImageUrl(this.mMediaAuthService.getMediaInfo(), 1);
        }
        showImage(imageUrl);
        if (this.mSelectedMedia != null) {
            MediaMetadata mm = this.mSelectedMedia.getMetadata();
            this.mCastController.setLine1(mm.getString(MediaMetadata.KEY_TITLE) != null ? mm.getString(MediaMetadata.KEY_TITLE) : ZapConstants.GOOGLE_ACCOUNT_NONE);
            if (this.mSelectedMedia.getStreamType() != 2) {
                isLive = false;
            }
            this.mCastController.adjustControllersForLiveStream(isLive);
        }
    }

    /* access modifiers changed from: private */
    public void updatePlayerStatus() {
        int mediaStatus = this.mCastManager.getPlaybackStatus();
        LogUtils.LOGD(TAG, "updatePlayerStatus(), state: " + mediaStatus);
        if (this.mSelectedMedia != null) {
            this.mCastController.setStreamType(this.mSelectedMedia.getStreamType());
            if (mediaStatus == 4) {
                this.mCastController.setLine2(getString(R.string.loading));
            } else {
                this.mCastController.setLine2(getString(R.string.casting_to_device, this.mCastManager.getDeviceName()));
            }
            switch (mediaStatus) {
                case 1:
                    switch (this.mCastManager.getIdleReason()) {
                        case 1:
                            if (!this.mIsFresh) {
                                this.mCastController.closeActivity();
                                return;
                            }
                            return;
                        case 2:
                            try {
                                if (this.mCastManager.isRemoteStreamLive() && this.mPlaybackState != 1) {
                                    this.mPlaybackState = 1;
                                    this.mCastController.setPlaybackStatus(this.mPlaybackState);
                                    return;
                                }
                                return;
                            } catch (TransientNetworkDisconnectionException e) {
                                LogUtils.LOGD(TAG, "Failed to determine if stream is live", e);
                                return;
                            } catch (NoConnectionException e2) {
                                LogUtils.LOGD(TAG, "Failed to determine if stream is live", e2);
                                return;
                            }
                        default:
                            return;
                    }
                case 2:
                    this.mIsFresh = false;
                    if (this.mPlaybackState != 2) {
                        this.mPlaybackState = 2;
                        this.mCastController.setPlaybackStatus(this.mPlaybackState);
                        return;
                    }
                    return;
                case 3:
                    this.mIsFresh = false;
                    if (this.mPlaybackState != 3) {
                        this.mPlaybackState = 3;
                        this.mCastController.setPlaybackStatus(this.mPlaybackState);
                        return;
                    }
                    return;
                case 4:
                    this.mIsFresh = false;
                    if (this.mPlaybackState != 4) {
                        this.mPlaybackState = 4;
                        this.mCastController.setPlaybackStatus(this.mPlaybackState);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void onDestroy() {
        LogUtils.LOGD(TAG, "onDestroy()");
        stopTrickplayTimer();
        cleanup();
        super.onDestroy();
    }

    public void onResume() {
        boolean shouldFinish = true;
        LogUtils.LOGD(TAG, "onResume() was called");
        try {
            this.mCastManager = VideoCastManager.getInstance(getActivity());
            if (this.mCastManager.isConnected() && !(this.mCastManager.getPlaybackStatus() == 1 && this.mCastManager.getIdleReason() == 1 && !this.mIsFresh)) {
                shouldFinish = false;
            }
            if (shouldFinish) {
                this.mCastController.closeActivity();
            }
            this.mCastManager.addVideoCastConsumer(this.mCastConsumer);
            this.mCastManager.incrementUiCounter();
            if (!this.mIsFresh) {
                updatePlayerStatus();
            }
        } catch (CastException e) {
        }
        super.onResume();
    }

    public void onPause() {
        this.mCastManager.removeVideoCastConsumer(this.mCastConsumer);
        this.mCastManager.decrementUiCounter();
        this.mIsFresh = false;
        super.onPause();
    }

    public static VideoCastControllerFragment newInstance(Bundle extras) {
        VideoCastControllerFragment f = new VideoCastControllerFragment();
        Bundle b = new Bundle();
        b.putBundle("extras", extras);
        f.setArguments(b);
        return f;
    }

    private void showImage(final String url) {
        if (this.mImageAsyncTask != null) {
            this.mImageAsyncTask.cancel(true);
        }
        if (url == null) {
            this.mCastController.setImage(BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.dummy_album_art_large));
        } else if (this.mUrlAndBitmap == null || !this.mUrlAndBitmap.isMatch(url)) {
            this.mUrlAndBitmap = null;
            this.mImageAsyncTask = new AsyncTask<String, Void, Bitmap>() {
                /* access modifiers changed from: protected */
                public Bitmap doInBackground(String... params) {
                    String uri = params[0];
                    try {
                        return BitmapFactory.decodeStream(new URL(uri).openStream());
                    } catch (Exception e) {
                        LogUtils.LOGE(VideoCastControllerFragment.TAG, "Failed to load the image with mUrl: " + uri, e);
                        return null;
                    }
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(Bitmap bitmap) {
                    if (bitmap != null) {
                        VideoCastControllerFragment.this.mUrlAndBitmap = new UrlAndBitmap();
                        VideoCastControllerFragment.this.mUrlAndBitmap.mBitmap = bitmap;
                        VideoCastControllerFragment.this.mUrlAndBitmap.mUrl = url;
                        VideoCastControllerFragment.this.mCastController.setImage(bitmap);
                    }
                }
            };
            this.mImageAsyncTask.execute(new String[]{url});
        } else {
            this.mCastController.setImage(this.mUrlAndBitmap.mBitmap);
        }
    }

    /* access modifiers changed from: private */
    public void showErrorDialog(String message) {
        ErrorDialogFragment.newInstance(message).show(getFragmentManager(), "dlg");
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
        try {
            if (this.mPlaybackState == 2) {
                this.mPlaybackState = 4;
                this.mCastController.setPlaybackStatus(this.mPlaybackState);
                this.mCastManager.play(seekBar.getProgress());
            } else if (this.mPlaybackState == 3) {
                this.mCastManager.seek(seekBar.getProgress());
            }
            restartTrickplayTimer();
        } catch (Exception e) {
            LogUtils.LOGE(TAG, "Failed to complete seek", e);
            this.mCastController.closeActivity();
        }
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
        stopTrickplayTimer();
    }

    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
    }

    public void onPlayPauseClicked(View v) throws CastException, TransientNetworkDisconnectionException, NoConnectionException {
        LogUtils.LOGD(TAG, "isConnected returning: " + this.mCastManager.isConnected());
        togglePlayback();
    }

    private void togglePlayback() throws CastException, TransientNetworkDisconnectionException, NoConnectionException {
        switch (this.mPlaybackState) {
            case 1:
                if (this.mSelectedMedia.getStreamType() == 2 && this.mCastManager.getIdleReason() == 2) {
                    this.mCastManager.play();
                } else {
                    this.mCastManager.loadMedia(this.mSelectedMedia, true, 0);
                }
                this.mPlaybackState = 4;
                restartTrickplayTimer();
                break;
            case 2:
                this.mCastManager.pause();
                this.mPlaybackState = 4;
                break;
            case 3:
                this.mCastManager.play();
                this.mPlaybackState = 4;
                restartTrickplayTimer();
                break;
        }
        this.mCastController.setPlaybackStatus(this.mPlaybackState);
    }

    public void onConfigurationChanged() {
        updateOverallState();
        if (this.mSelectedMedia != null) {
            updateMetadata();
            updatePlayerStatus();
            this.mCastController.updateControllersStatus(this.mCastManager.isConnected());
        } else if (this.mMediaAuthService != null) {
            showImage(Utils.getImageUrl(this.mMediaAuthService.getMediaInfo(), 1));
        }
    }

    public void onResult(MediaAuthStatus status, final MediaInfo info, final String message, final int startPoint, final JSONObject customData) {
        if (status != MediaAuthStatus.RESULT_AUTHORIZED || !this.mAuthSuccess) {
            if (this.mMediaAuthTimer != null) {
                this.mMediaAuthTimer.cancel();
            }
            this.mHandler.post(new Runnable() {
                public void run() {
                    VideoCastControllerFragment.this.mOverallState = OverallState.UNKNOWN;
                    VideoCastControllerFragment.this.showErrorDialog(message);
                }
            });
            return;
        }
        this.mMediaAuthService = null;
        if (this.mMediaAuthTimer != null) {
            this.mMediaAuthTimer.cancel();
        }
        this.mSelectedMedia = info;
        this.mHandler.post(new Runnable() {
            public void run() {
                VideoCastControllerFragment.this.mOverallState = OverallState.PLAYBACK;
                VideoCastControllerFragment.this.onReady(info, true, startPoint, customData);
            }
        });
    }

    public void onFailure(final String failureMessage) {
        if (this.mMediaAuthTimer != null) {
            this.mMediaAuthTimer.cancel();
        }
        this.mHandler.post(new Runnable() {
            public void run() {
                VideoCastControllerFragment.this.mOverallState = OverallState.UNKNOWN;
                VideoCastControllerFragment.this.showErrorDialog(failureMessage);
            }
        });
    }

    private void cleanup() {
        IMediaAuthService authService = this.mCastManager.getMediaAuthService();
        if (this.mMediaAuthTimer != null) {
            this.mMediaAuthTimer.cancel();
        }
        if (this.mAuthThread != null) {
            this.mAuthThread = null;
        }
        if (this.mCastManager.getMediaAuthService() != null) {
            authService.setOnResult(null);
            this.mCastManager.removeMediaAuthService();
        }
        if (this.mCastManager != null) {
            this.mCastManager.removeVideoCastConsumer(this.mCastConsumer);
        }
        if (this.mHandler != null) {
            this.mHandler.removeCallbacksAndMessages(null);
        }
        if (this.mUrlAndBitmap != null) {
            this.mUrlAndBitmap.mBitmap = null;
        }
        if (!sDialogCanceled && this.mMediaAuthService != null) {
            this.mMediaAuthService.abort(MediaAuthStatus.ABORT_USER_CANCELLED);
        }
        this.mCastManager.clearContext(getActivity());
    }
}
