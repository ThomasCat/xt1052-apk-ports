package com.motorola.sharing.provider.cloud;

public class UrlHelper {
    private static String getBaseUrl() {
        return "https" + "://" + ((String) CloudSettings.SERVER_HOST.get());
    }

    public static String getShares() {
        return getBaseUrl() + "/" + "getShares";
    }

    public static String startShare() {
        return getBaseUrl() + "/" + "startShare";
    }

    public static String getUpdateShare() {
        return getBaseUrl() + "/" + "updateShare";
    }

    public static String getReportAbuse() {
        return getBaseUrl() + "/" + "reportAbuse";
    }

    public static String getSettings() {
        return getBaseUrl() + "/" + "getLatestAppSettings";
    }

    public static String createTarget() {
        return getBaseUrl() + "/" + "createTarget";
    }

    public static String addTargetMember() {
        return getBaseUrl() + "/" + "addTargetMember";
    }

    public static String updateTargetMember() {
        return getBaseUrl() + "/" + "updateTargetMember";
    }

    public static String updateTarget() {
        return getBaseUrl() + "/" + "updateTarget";
    }

    public static String getTargetInfo() {
        return getBaseUrl() + "/" + "getTargetInfo";
    }

    public static String getUserProfile() {
        return getBaseUrl() + "/" + "getUserProfile";
    }

    public static String postUserProfile() {
        return getBaseUrl() + "/" + "postUserProfile";
    }

    public static String deleteUserProfile() {
        return getBaseUrl() + "/" + "deleteUserProfile";
    }
}
