package com.google.android.gms.tagmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.google.android.gms.internal.fl;
import com.google.android.gms.internal.fn;
import com.motorola.sharing.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import org.apache.http.impl.client.DefaultHttpClient;

class cb implements at {
    /* access modifiers changed from: private */
    public static final String ua = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL,'%s' INTEGER NOT NULL);", new Object[]{"gtm_hits", "hit_id", "hit_time", "hit_url", "hit_first_send_time"});
    /* access modifiers changed from: private */
    public fl Ty;
    private final b VL;
    private volatile ab VM;
    private final au VN;
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public final String ud;
    private long uf;
    private final int ug;

    class a implements com.google.android.gms.tagmanager.db.a {
        a() {
        }

        public void a(ap apVar) {
            cb.this.u(apVar.ci());
        }

        public void b(ap apVar) {
            cb.this.u(apVar.ci());
            bh.v("Permanent failure dispatching hitId: " + apVar.ci());
        }

        public void c(ap apVar) {
            long je = apVar.je();
            if (je == 0) {
                cb.this.c(apVar.ci(), cb.this.Ty.currentTimeMillis());
            } else if (je + 14400000 < cb.this.Ty.currentTimeMillis()) {
                cb.this.u(apVar.ci());
                bh.v("Giving up on failed hitId: " + apVar.ci());
            }
        }
    }

    class b extends SQLiteOpenHelper {
        private boolean ui;
        private long uj = 0;

        b(Context context, String str) {
            super(context, str, null, 1);
        }

        /* JADX INFO: finally extract failed */
        private void a(SQLiteDatabase sQLiteDatabase) {
            Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT * FROM gtm_hits WHERE 0", null);
            HashSet hashSet = new HashSet();
            try {
                String[] columnNames = rawQuery.getColumnNames();
                for (String add : columnNames) {
                    hashSet.add(add);
                }
                rawQuery.close();
                if (!hashSet.remove("hit_id") || !hashSet.remove("hit_url") || !hashSet.remove("hit_time") || !hashSet.remove("hit_first_send_time")) {
                    throw new SQLiteException("Database column missing");
                } else if (!hashSet.isEmpty()) {
                    throw new SQLiteException("Database has extra columns");
                }
            } catch (Throwable th) {
                rawQuery.close();
                throw th;
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0048  */
        private boolean a(String str, SQLiteDatabase sQLiteDatabase) {
            Cursor cursor;
            Cursor cursor2 = null;
            try {
                SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
                Cursor query = sQLiteDatabase2.query("SQLITE_MASTER", new String[]{"name"}, "name=?", new String[]{str}, null, null, null);
                try {
                    boolean moveToFirst = query.moveToFirst();
                    if (query == null) {
                        return moveToFirst;
                    }
                    query.close();
                    return moveToFirst;
                } catch (SQLiteException e) {
                    cursor = query;
                } catch (Throwable th) {
                    th = th;
                    cursor2 = query;
                    if (cursor2 != null) {
                    }
                    throw th;
                }
            } catch (SQLiteException e2) {
                cursor = null;
            } catch (Throwable th2) {
                th = th2;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
            try {
                bh.w("Error querying for table " + str);
                if (cursor != null) {
                    cursor.close();
                }
                return false;
            } catch (Throwable th3) {
                cursor2 = cursor;
                th = th3;
                if (cursor2 != null) {
                }
                throw th;
            }
        }

        public SQLiteDatabase getWritableDatabase() {
            if (!this.ui || this.uj + 3600000 <= cb.this.Ty.currentTimeMillis()) {
                SQLiteDatabase sQLiteDatabase = null;
                this.ui = true;
                this.uj = cb.this.Ty.currentTimeMillis();
                try {
                    sQLiteDatabase = super.getWritableDatabase();
                } catch (SQLiteException e) {
                    cb.this.mContext.getDatabasePath(cb.this.ud).delete();
                }
                if (sQLiteDatabase == null) {
                    sQLiteDatabase = super.getWritableDatabase();
                }
                this.ui = false;
                return sQLiteDatabase;
            }
            throw new SQLiteException("Database creation failed");
        }

        public void onCreate(SQLiteDatabase db) {
            ak.B(db.getPath());
        }

        public void onOpen(SQLiteDatabase db) {
            if (VERSION.SDK_INT < 15) {
                Cursor rawQuery = db.rawQuery("PRAGMA journal_mode=memory", null);
                try {
                    rawQuery.moveToFirst();
                } finally {
                    rawQuery.close();
                }
            }
            if (!a("gtm_hits", db)) {
                db.execSQL(cb.ua);
            } else {
                a(db);
            }
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    cb(au auVar, Context context) {
        this(auVar, context, "gtm_urls.db", 2000);
    }

    cb(au auVar, Context context, String str, int i) {
        this.mContext = context.getApplicationContext();
        this.ud = str;
        this.VN = auVar;
        this.Ty = fn.eI();
        this.VL = new b(this.mContext, this.ud);
        this.VM = new db(new DefaultHttpClient(), this.mContext, new a());
        this.uf = 0;
        this.ug = i;
    }

    private SQLiteDatabase G(String str) {
        try {
            return this.VL.getWritableDatabase();
        } catch (SQLiteException e) {
            bh.w(str);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void c(long j, long j2) {
        SQLiteDatabase G = G("Error opening database for getNumStoredHits.");
        if (G != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("hit_first_send_time", Long.valueOf(j2));
            try {
                G.update("gtm_hits", contentValues, "hit_id=?", new String[]{String.valueOf(j)});
            } catch (SQLiteException e) {
                bh.w("Error setting HIT_FIRST_DISPATCH_TIME for hitId: " + j);
                u(j);
            }
        }
    }

    private void co() {
        int cq = (cq() - this.ug) + 1;
        if (cq > 0) {
            List s = s(cq);
            bh.v("Store full, deleting " + s.size() + " hits to make room.");
            a((String[]) s.toArray(new String[0]));
        }
    }

    private void f(long j, String str) {
        SQLiteDatabase G = G("Error opening database for putHit");
        if (G != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("hit_time", Long.valueOf(j));
            contentValues.put("hit_url", str);
            contentValues.put("hit_first_send_time", Integer.valueOf(0));
            try {
                G.insert("gtm_hits", null, contentValues);
                this.VN.p(false);
            } catch (SQLiteException e) {
                bh.w("Error storing hit");
            }
        }
    }

    /* access modifiers changed from: private */
    public void u(long j) {
        a(new String[]{String.valueOf(j)});
    }

    /* access modifiers changed from: 0000 */
    public void a(String[] strArr) {
        boolean z = true;
        if (strArr != null && strArr.length != 0) {
            SQLiteDatabase G = G("Error opening database for deleteHits.");
            if (G != null) {
                try {
                    G.delete("gtm_hits", String.format("HIT_ID in (%s)", new Object[]{TextUtils.join(",", Collections.nCopies(strArr.length, "?"))}), strArr);
                    au auVar = this.VN;
                    if (cq() != 0) {
                        z = false;
                    }
                    auVar.p(z);
                } catch (SQLiteException e) {
                    bh.w("Error deleting hits");
                }
            }
        }
    }

    public void bp() {
        bh.v("GTM Dispatch running...");
        if (this.VM.bA()) {
            List t = t(40);
            if (t.isEmpty()) {
                bh.v("...nothing to dispatch");
                this.VN.p(true);
                return;
            }
            this.VM.e(t);
            if (js() > 0) {
                cy.kh().bp();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public int cp() {
        boolean z = true;
        long currentTimeMillis = this.Ty.currentTimeMillis();
        if (currentTimeMillis <= this.uf + Constants.DAY) {
            return 0;
        }
        this.uf = currentTimeMillis;
        SQLiteDatabase G = G("Error opening database for deleteStaleHits.");
        if (G == null) {
            return 0;
        }
        int delete = G.delete("gtm_hits", "HIT_TIME < ?", new String[]{Long.toString(this.Ty.currentTimeMillis() - 2592000000L)});
        au auVar = this.VN;
        if (cq() != 0) {
            z = false;
        }
        auVar.p(z);
        return delete;
    }

    /* access modifiers changed from: 0000 */
    public int cq() {
        Cursor cursor = null;
        int i = 0;
        SQLiteDatabase G = G("Error opening database for getNumStoredHits.");
        if (G != null) {
            try {
                Cursor rawQuery = G.rawQuery("SELECT COUNT(*) from gtm_hits", null);
                if (rawQuery.moveToFirst()) {
                    i = (int) rawQuery.getLong(0);
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } catch (SQLiteException e) {
                bh.w("Error getting numStoredHits");
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
        return i;
    }

    public void e(long j, String str) {
        cp();
        co();
        f(j, str);
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0040  */
    public int js() {
        Cursor cursor;
        int i;
        Cursor cursor2 = null;
        SQLiteDatabase G = G("Error opening database for getNumStoredHits.");
        if (G == null) {
            return 0;
        }
        try {
            Cursor query = G.query("gtm_hits", new String[]{"hit_id", "hit_first_send_time"}, "hit_first_send_time=0", null, null, null, null);
            try {
                i = query.getCount();
                if (query != null) {
                    query.close();
                }
            } catch (SQLiteException e) {
                cursor = query;
            } catch (Throwable th) {
                th = th;
                cursor2 = query;
                if (cursor2 != null) {
                }
                throw th;
            }
        } catch (SQLiteException e2) {
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            if (cursor2 != null) {
                cursor2.close();
            }
            throw th;
        }
        return i;
        try {
            bh.w("Error getting num untried hits");
            if (cursor != null) {
                cursor.close();
                i = 0;
            } else {
                i = 0;
            }
            return i;
        } catch (Throwable th3) {
            cursor2 = cursor;
            th = th3;
            if (cursor2 != null) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0082  */
    public List<String> s(int i) {
        Cursor cursor;
        ArrayList arrayList = new ArrayList();
        if (i <= 0) {
            bh.w("Invalid maxHits specified. Skipping");
            return arrayList;
        }
        SQLiteDatabase G = G("Error opening database for peekHitIds.");
        if (G == null) {
            return arrayList;
        }
        try {
            cursor = G.query("gtm_hits", new String[]{"hit_id"}, null, null, null, null, String.format("%s ASC", new Object[]{"hit_id"}), Integer.toString(i));
            try {
                if (cursor.moveToFirst()) {
                    do {
                        arrayList.add(String.valueOf(cursor.getLong(0)));
                    } while (cursor.moveToNext());
                }
                if (cursor != null) {
                    cursor.close();
                }
            } catch (SQLiteException e) {
                e = e;
                try {
                    bh.w("Error in peekHits fetching hitIds: " + e.getMessage());
                    if (cursor != null) {
                        cursor.close();
                    }
                    return arrayList;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e2) {
            e = e2;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00e8, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00f0, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0169, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x016a, code lost:
        r11 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x016f, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0170, code lost:
        r2 = r1;
        r3 = r12;
        r1 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        return r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0169 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:6:0x003e] */
    /* JADX WARNING: Removed duplicated region for block: B:80:? A[RETURN, SYNTHETIC] */
    public List<ap> t(int i) {
        Cursor cursor;
        SQLiteException sQLiteException;
        ArrayList arrayList;
        Cursor query;
        ArrayList<ap> arrayList2;
        ArrayList arrayList3 = new ArrayList();
        SQLiteDatabase G = G("Error opening database for peekHits");
        if (G == null) {
            return arrayList3;
        }
        Cursor cursor2 = null;
        try {
            query = G.query("gtm_hits", new String[]{"hit_id", "hit_time", "hit_first_send_time"}, null, null, null, null, String.format("%s ASC", new Object[]{"hit_id"}), Integer.toString(i));
            try {
                arrayList2 = new ArrayList<>();
                if (query.moveToFirst()) {
                    do {
                        arrayList2.add(new ap(query.getLong(0), query.getLong(1), query.getLong(2)));
                    } while (query.moveToNext());
                }
                if (query != null) {
                    query.close();
                }
                try {
                    Cursor query2 = G.query("gtm_hits", new String[]{"hit_id", "hit_url"}, null, null, null, null, String.format("%s ASC", new Object[]{"hit_id"}), Integer.toString(i));
                    try {
                        if (query2.moveToFirst()) {
                            int i2 = 0;
                            while (true) {
                                if (((SQLiteCursor) query2).getWindow().getNumRows() > 0) {
                                    ((ap) arrayList2.get(i2)).F(query2.getString(1));
                                } else {
                                    bh.w(String.format("HitString for hitId %d too large.  Hit will be deleted.", new Object[]{Long.valueOf(((ap) arrayList2.get(i2)).ci())}));
                                }
                                int i3 = i2 + 1;
                                if (!query2.moveToNext()) {
                                    break;
                                }
                                i2 = i3;
                            }
                        }
                        if (query2 != null) {
                            query2.close();
                        }
                        return arrayList2;
                    } catch (SQLiteException e) {
                        e = e;
                        query = query2;
                    } catch (Throwable th) {
                        th = th;
                        query = query2;
                        if (query != null) {
                            query.close();
                        }
                        throw th;
                    }
                } catch (SQLiteException e2) {
                    e = e2;
                }
            } catch (SQLiteException e3) {
                sQLiteException = e3;
                cursor = query;
                arrayList = arrayList2;
                try {
                    bh.w("Error in peekHits fetching hitIds: " + sQLiteException.getMessage());
                    if (cursor == null) {
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor2 = cursor;
                    if (cursor2 != null) {
                    }
                    throw th;
                }
            } catch (Throwable th3) {
            }
        } catch (SQLiteException e4) {
            sQLiteException = e4;
            cursor = null;
            arrayList = arrayList3;
        } catch (Throwable th4) {
            th = th4;
            if (cursor2 != null) {
            }
            throw th;
        }
        try {
            bh.w("Error in peekHits fetching hit url: " + e.getMessage());
            ArrayList arrayList4 = new ArrayList();
            boolean z = false;
            for (ap apVar : arrayList2) {
                if (TextUtils.isEmpty(apVar.jf())) {
                    if (z) {
                        break;
                    }
                    z = true;
                }
                arrayList4.add(apVar);
            }
            if (query != null) {
                query.close();
            }
            return arrayList4;
        } catch (Throwable th5) {
            th = th5;
        }
    }
}
