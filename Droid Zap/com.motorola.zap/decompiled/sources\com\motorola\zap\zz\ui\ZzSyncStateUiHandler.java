package com.motorola.zap.zz.ui;

import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener.GroupEvent;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.ui.ZapUi;

public class ZzSyncStateUiHandler extends BaseZzUiHandler {
    /* access modifiers changed from: protected */
    public void handleOk(GroupEvent ge) {
        updateZzActiveNotification();
        if (GCSP.getGroupManager().isAnyGroupActive()) {
            ZapUi ui = ZapUi.getInstance();
            if (!((Boolean) Z.FEATURE_ENABLED.read()).booleanValue()) {
                ui.showToastMessage(getString(R.string.toast_error_app_disabled), ZapUi.getErrorToastIconId(), true, true);
                GCSP.getGroupManager().leave(null);
                return;
            }
            ui.showToastMessage(getString(R.string.toast_msg_zz_restarted), ZapUi.getZzToastIconId(), false, true);
        }
    }

    /* access modifiers changed from: protected */
    public void handleError(GroupEvent ge) {
        if (!handleCommonError(ge)) {
            ZapUi.getInstance().showToastMessage(getString(R.string.toast_error_zz_failed), ZapUi.getErrorToastIconId(), true, true);
        }
    }
}
