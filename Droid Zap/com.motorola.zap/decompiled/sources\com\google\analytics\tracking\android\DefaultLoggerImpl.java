package com.google.analytics.tracking.android;

import android.util.Log;
import com.google.analytics.tracking.android.Logger.LogLevel;

class DefaultLoggerImpl implements Logger {
    private LogLevel mLogLevel = LogLevel.INFO;

    DefaultLoggerImpl() {
    }

    public void verbose(String msg) {
        if (this.mLogLevel.ordinal() <= LogLevel.VERBOSE.ordinal()) {
            Log.v("GAV3", formatMessage(msg));
        }
    }

    public void info(String msg) {
        if (this.mLogLevel.ordinal() <= LogLevel.INFO.ordinal()) {
            Log.i("GAV3", formatMessage(msg));
        }
    }

    public void warn(String msg) {
        if (this.mLogLevel.ordinal() <= LogLevel.WARNING.ordinal()) {
            Log.w("GAV3", formatMessage(msg));
        }
    }

    public void error(String msg) {
        if (this.mLogLevel.ordinal() <= LogLevel.ERROR.ordinal()) {
            Log.e("GAV3", formatMessage(msg));
        }
    }

    public void error(Exception exception) {
        if (this.mLogLevel.ordinal() <= LogLevel.ERROR.ordinal()) {
            Log.e("GAV3", null, exception);
        }
    }

    public void setLogLevel(LogLevel level) {
        this.mLogLevel = level;
    }

    public LogLevel getLogLevel() {
        return this.mLogLevel;
    }

    private String formatMessage(String msg) {
        return Thread.currentThread().toString() + ": " + msg;
    }
}
