package com.motorola.zap.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupManager.GroupInfo;
import com.motorola.sharing.Locality;
import com.motorola.sharing.Profile;
import com.motorola.sharing.util.FileUtils;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.app.App;
import com.motorola.zap.app.GroupInvite;
import com.motorola.zap.identity.ZapProfileSharedPerfs;
import com.motorola.zap.provider.ZapContract.History;
import java.io.File;
import java.util.concurrent.TimeUnit;

public class HistoryHandler {
    public static final String ACTION_DEMO_CARD = "DC";
    public static final String ACTION_MEDIA_RECEIVE = "R";
    public static final String ACTION_MEDIA_RESHARE = "RS";
    public static final String ACTION_MEDIA_SEND = "S";
    public static final String ACTION_ZZ_INVITE = "ZI";
    public static final String ACTION_ZZ_SESSION = "Z";
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    public static final String HISTORY_TABLE_NAME = "receive_history";
    public static final String HIST_ACCOUNT_COLUMN = "rcv_account";
    public static final String HIST_ACTION = "rcv_action";
    public static final String HIST_CITY = "rcv_city";
    public static final String HIST_COUNTRY = "rcv_country";
    public static final String HIST_FILEPATH = "rcv_file_path";
    public static final String HIST_FROM = "rcv_from";
    public static final String HIST_ID = "_id";
    public static final String HIST_IS_ABUSE_REPORTED = "rcv_abuse_reported";
    public static final String HIST_IS_DELETED = "rcv_deleted";
    public static final String HIST_IS_LEFT_ZZ = "rcv_is_left_zz";
    public static final String HIST_IS_PIN_LOCKED = "rcv_pin_locked";
    public static final String HIST_IS_TRANSFER_ACKED = "rcv_download_acked";
    public static final String HIST_IS_TRANSFER_IN_PROGRESS = "rcv_transfer_in_progress";
    public static final String HIST_IS_ZZ_ACTIVE = "rcv_zz_active";
    public static final String HIST_LATITUDE = "rcv_latitude";
    public static final String HIST_LONGITUDE = "rcv_longitude";
    public static final String HIST_MEDIA_EXPIRATION_TIMESTAMP = "rcv_expiration_timestamp";
    public static final String HIST_MEDIA_TYPE = "rcv_media_type";
    public static final String HIST_PIN = "rcv_pin";
    public static final String HIST_PROF_ANONYMOUS = "rcv_prof_anonymous";
    public static final String HIST_PROF_NAME = "rcv_prof_name";
    public static final String HIST_PROF_PIC = "rcv_prof_pic";
    public static final String HIST_PROF_USERID = "rcv_prof_userid";
    public static final String HIST_RESHARE_UID = "rcv_reshare_id";
    public static final String HIST_SHAREID = "rcv_shareid";
    public static final String HIST_STATE = "rcv_state";
    public static final String HIST_THUMBNAIL = "rcv_thumbnail";
    public static final String HIST_TIMESTAMP = "rcv_timestamp";
    public static final String HIST_TRANSFER_COMPLETE_STATUS = "rcv_transfer_complete_status";
    public static final String HIST_UID = "rcv_uid";
    public static final String HIST_ZZ_ID = "rcv_zz_id";
    public static final String HIST_ZZ_NAME = "rcv_zz_name";
    public static final String HIST_ZZ_OWNER = "rcv_zz_owner";
    public static final int MEDIA_PICTURE = 1;
    public static final int MEDIA_UNKNOWN = 0;
    public static final int MEDIA_VIDEO = 2;
    public static final int STATUS_CANCELED = 2;
    public static final int STATUS_ERROR = 1;
    public static final int STATUS_NO_STATUS = -1;
    public static final int STATUS_OK = 0;

    /* access modifiers changed from: private */
    public SQLiteDatabase getDb() {
        return DbOpenHelper.getInstance().getDb();
    }

    /* access modifiers changed from: protected */
    public void init() {
        deleteMarkedForDeletion();
        deletePendingDownloads();
        resetInProgressTransfers();
        updateMediaTypes();
    }

    private void updateMediaTypes() {
        if (DatabaseUtils.queryNumEntries(getDb(), HISTORY_TABLE_NAME, String.format("%s IS NULL AND %s IS NOT NULL", new Object[]{HIST_MEDIA_TYPE, HIST_FILEPATH}), null) > 0) {
            updateMediaTypesInBackGround();
        }
    }

    private void updateMediaTypesInBackGround() {
        new Thread() {
            /* JADX INFO: finally extract failed */
            /* JADX WARNING: Removed duplicated region for block: B:12:0x005a  */
            /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
            public void run() {
                Cursor cursor = null;
                try {
                    cursor = HistoryHandler.this.getDb().query(HistoryHandler.HISTORY_TABLE_NAME, new String[]{HistoryHandler.HIST_ID, HistoryHandler.HIST_FILEPATH}, String.format("%s IS NULL AND %s IS NOT NULL", new Object[]{HistoryHandler.HIST_MEDIA_TYPE, HistoryHandler.HIST_FILEPATH}), null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        ContentValues contentValues = new ContentValues();
                        String sel = String.format("%s = ?", new Object[]{HistoryHandler.HIST_ID});
                        do {
                            String fileName = TCH.getFilePath(cursor);
                            if (fileName != null) {
                                int mediaType = HistoryHandler.getMediaType(fileName);
                                contentValues.clear();
                                contentValues.put(HistoryHandler.HIST_MEDIA_TYPE, Integer.valueOf(mediaType));
                                HistoryHandler.this.getDb().update(HistoryHandler.HISTORY_TABLE_NAME, contentValues, sel, new String[]{String.valueOf(TCH.getId(cursor))});
                            }
                        } while (cursor.moveToNext());
                        if (cursor == null) {
                        }
                    } else if (cursor == null) {
                        cursor.close();
                    }
                } catch (Throwable th) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        }.start();
    }

    private void deleteMarkedForDeletion() {
        getDb().delete(HISTORY_TABLE_NAME, String.format("%s = ?", new Object[]{HIST_IS_DELETED}), new String[]{"1"});
    }

    private void deletePendingDownloads() {
        getDb().delete(HISTORY_TABLE_NAME, String.format("%s = ? AND %s IS NULL", new Object[]{HIST_ACTION, HIST_FILEPATH}), new String[]{ACTION_MEDIA_RECEIVE});
    }

    private void resetInProgressTransfers() {
        String selection = String.format("%s = ?", new Object[]{HIST_IS_TRANSFER_IN_PROGRESS});
        String[] selectionArgs = {"1"};
        ContentValues values = new ContentValues();
        values.put(HIST_IS_TRANSFER_IN_PROGRESS, Boolean.valueOf(false));
        getDb().update(HISTORY_TABLE_NAME, values, selection, selectionArgs);
    }

    public int update(String selection, ContentValues values, String[] selectionArgs) {
        return getDb().update(HISTORY_TABLE_NAME, values, selection, selectionArgs);
    }

    private static String getCreateTableVersion5(String table) {
        return "CREATE TABLE " + table + " (" + HIST_ID + " INTEGER PRIMARY KEY," + HIST_FILEPATH + " TEXT," + HIST_SHAREID + " TEXT," + HIST_FROM + " TEXT," + HIST_ACCOUNT_COLUMN + " TEXT," + HIST_TIMESTAMP + " NUMERIC," + HIST_LATITUDE + " NUMERIC," + HIST_LONGITUDE + " NUMERIC," + HIST_CITY + " TEXT," + HIST_STATE + " TEXT," + HIST_COUNTRY + " TEXT," + HIST_THUMBNAIL + " BLOB," + HIST_IS_ABUSE_REPORTED + " NUMERIC," + HIST_ZZ_NAME + " TEXT," + HIST_ACTION + " TEXT," + HIST_ZZ_OWNER + " TEXT," + HIST_ZZ_ID + " TEXT" + ")";
    }

    /* access modifiers changed from: protected */
    public void onCreate(SQLiteDatabase database, int currentVersion) {
        if (DBG) {
            Log.d("Zap-HistoryHandler", "onCreate invoked");
        }
        database.execSQL(getCreateTableVersion5(HISTORY_TABLE_NAME));
        handleUpgradePostVersion5(database, 0, currentVersion);
    }

    /* access modifiers changed from: protected */
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        if (DBG) {
            Log.d("Zap-HistoryHandler", "onUpgrade invoked - oldVersion: " + oldVersion + " - newVersion: " + newVersion);
        }
        if (oldVersion < 4) {
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s TEXT", new Object[]{HISTORY_TABLE_NAME, HIST_ZZ_NAME}));
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s TEXT", new Object[]{HISTORY_TABLE_NAME, HIST_ACTION}));
        }
        if (oldVersion < 5) {
            String TMP_TABLE = "tmp";
            database.execSQL(getCreateTableVersion5(TMP_TABLE));
            database.execSQL("INSERT INTO " + TMP_TABLE + " (" + HIST_FILEPATH + "," + HIST_SHAREID + "," + HIST_FROM + "," + HIST_ACCOUNT_COLUMN + "," + HIST_TIMESTAMP + "," + HIST_LATITUDE + "," + HIST_LONGITUDE + "," + HIST_CITY + "," + HIST_STATE + "," + HIST_COUNTRY + "," + HIST_THUMBNAIL + "," + HIST_IS_ABUSE_REPORTED + "," + HIST_ZZ_NAME + "," + HIST_ACTION + ")" + " SELECT " + HIST_FILEPATH + "," + HIST_SHAREID + "," + HIST_FROM + "," + HIST_ACCOUNT_COLUMN + "," + HIST_TIMESTAMP + "," + HIST_LATITUDE + "," + HIST_LONGITUDE + "," + HIST_CITY + "," + HIST_STATE + "," + HIST_COUNTRY + "," + HIST_THUMBNAIL + "," + HIST_IS_ABUSE_REPORTED + "," + HIST_ZZ_NAME + "," + HIST_ACTION + " FROM " + HISTORY_TABLE_NAME);
            database.execSQL(String.format("DROP TABLE %s", new Object[]{HISTORY_TABLE_NAME}));
            database.execSQL(String.format("ALTER TABLE %s RENAME TO %s", new Object[]{TMP_TABLE, HISTORY_TABLE_NAME}));
        }
        handleUpgradePostVersion5(database, oldVersion, newVersion);
    }

    public void handleUpgradePostVersion5(SQLiteDatabase database, int oldVersion, int newVersion) {
        if (oldVersion < 6) {
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s INTEGER", new Object[]{HISTORY_TABLE_NAME, HIST_IS_ZZ_ACTIVE}));
        }
        if (oldVersion < 7) {
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s INTEGER", new Object[]{HISTORY_TABLE_NAME, HIST_IS_DELETED}));
        }
        if (oldVersion < 8) {
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s INTEGER", new Object[]{HISTORY_TABLE_NAME, HIST_IS_TRANSFER_ACKED}));
        }
        if (oldVersion < 9) {
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s INTEGER", new Object[]{HISTORY_TABLE_NAME, HIST_UID}));
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s INTEGER", new Object[]{HISTORY_TABLE_NAME, HIST_IS_PIN_LOCKED}));
        }
        if (oldVersion < 10) {
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s INTEGER", new Object[]{HISTORY_TABLE_NAME, HIST_IS_TRANSFER_IN_PROGRESS}));
        }
        if (oldVersion < 11) {
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s INTEGER", new Object[]{HISTORY_TABLE_NAME, HIST_RESHARE_UID}));
        }
        if (oldVersion < 12) {
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s TEXT", new Object[]{HISTORY_TABLE_NAME, HIST_PIN}));
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s INTEGER", new Object[]{HISTORY_TABLE_NAME, HIST_MEDIA_EXPIRATION_TIMESTAMP}));
        }
        if (oldVersion < 13) {
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s INTEGER", new Object[]{HISTORY_TABLE_NAME, HIST_TRANSFER_COMPLETE_STATUS}));
        }
        if (oldVersion < 14) {
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s INTEGER", new Object[]{HISTORY_TABLE_NAME, HIST_MEDIA_TYPE}));
        }
        if (oldVersion < 15) {
            database.execSQL(String.format("CREATE INDEX %s ON %s(%s DESC)", new Object[]{"ts_desc", HISTORY_TABLE_NAME, HIST_TIMESTAMP}));
            database.execSQL(String.format("CREATE INDEX %s ON %s(%s, %s)", new Object[]{"zz_owner_zz_id", HISTORY_TABLE_NAME, HIST_ZZ_OWNER, HIST_ZZ_ID}));
            database.execSQL(String.format("CREATE INDEX %s ON %s(%s)", new Object[]{"zz_file_path", HISTORY_TABLE_NAME, HIST_FILEPATH}));
            database.execSQL(String.format("CREATE INDEX %s ON %s(%s)", new Object[]{"zz_action", HISTORY_TABLE_NAME, HIST_ACTION}));
        }
        if (oldVersion < 16) {
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s INTEGER", new Object[]{HISTORY_TABLE_NAME, HIST_IS_LEFT_ZZ}));
        }
        if (oldVersion < 17) {
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s TEXT", new Object[]{HISTORY_TABLE_NAME, HIST_PROF_PIC}));
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s TEXT", new Object[]{HISTORY_TABLE_NAME, HIST_PROF_NAME}));
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s TEXT", new Object[]{HISTORY_TABLE_NAME, HIST_PROF_USERID}));
            database.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s BOOLEAN DEFAULT 'FALSE'", new Object[]{HISTORY_TABLE_NAME, HIST_PROF_ANONYMOUS}));
        }
    }

    private void notifyChange() {
        App.getAppContext().getContentResolver().notifyChange(History.CONTENT_URI, null);
    }

    /* JADX INFO: finally extract failed */
    public void addUpload(ContentContainer trans) {
        String selection;
        String[] selectionArgs;
        boolean z;
        boolean isReshare = true;
        getDb().beginTransaction();
        try {
            GroupInfo groupInfo = trans.getGroupInfo();
            if (groupInfo != null) {
                selection = String.format("%s = ? AND (%s = ? OR %s = ? OR %s = ?) AND (%s = ? AND %s = ?)", new Object[]{HIST_FILEPATH, HIST_ACTION, HIST_ACTION, HIST_ACTION, HIST_ZZ_OWNER, HIST_ZZ_ID});
                selectionArgs = new String[]{trans.getPath(), ACTION_MEDIA_RECEIVE, ACTION_MEDIA_SEND, ACTION_DEMO_CARD, groupInfo.owner, groupInfo.id};
            } else {
                selection = String.format("%s = ? AND (%s = ? OR %s = ? OR %s = ?) AND (%s IS NULL AND %s IS NULL)", new Object[]{HIST_FILEPATH, HIST_ACTION, HIST_ACTION, HIST_ACTION, HIST_ZZ_OWNER, HIST_ZZ_ID});
                selectionArgs = new String[]{trans.getPath(), ACTION_MEDIA_RECEIVE, ACTION_MEDIA_SEND, ACTION_DEMO_CARD};
            }
            ContentValues values = new ContentValues();
            values.put(HIST_IS_TRANSFER_IN_PROGRESS, Boolean.valueOf(true));
            values.put(HIST_RESHARE_UID, Long.valueOf(trans.getUid()));
            String str = HIST_IS_PIN_LOCKED;
            if (!TextUtils.isEmpty(trans.getPin())) {
                z = true;
            } else {
                z = false;
            }
            values.put(str, Boolean.valueOf(z));
            values.put(HIST_PIN, trans.getPin());
            Profile lProfile = ZapProfileSharedPerfs.getProfile();
            if (lProfile != null) {
                values.put(HIST_PROF_PIC, lProfile.pictureUrl);
                values.put(HIST_PROF_NAME, lProfile.nickname);
                values.put(HIST_PROF_USERID, lProfile.userId);
                values.put(HIST_PROF_ANONYMOUS, Boolean.valueOf(lProfile.anonymous));
            }
            if (getDb().update(HISTORY_TABLE_NAME, values, selection, selectionArgs) == 0) {
                isReshare = false;
            }
            values.remove(HIST_RESHARE_UID);
            if (isReshare) {
                values.put(HIST_ACTION, ACTION_MEDIA_RESHARE);
            } else {
                values.put(HIST_ACTION, ACTION_MEDIA_SEND);
            }
            int mediaType = getMediaType(trans.getPath());
            values.put(HIST_ACCOUNT_COLUMN, trans.getUserAccount());
            values.put(HIST_FROM, trans.getOrigin());
            values.put(HIST_SHAREID, trans.getShareId());
            values.put(HIST_FILEPATH, trans.getPath());
            values.put(HIST_IS_ZZ_ACTIVE, Boolean.valueOf(isZzActive(trans)));
            if (groupInfo != null) {
                values.put(HIST_ZZ_OWNER, groupInfo.owner);
                values.put(HIST_ZZ_ID, groupInfo.id);
                values.put(HIST_ZZ_NAME, groupInfo.name);
            }
            Locality locality = trans.getLocality();
            if (locality != null) {
                values.put(HIST_CITY, locality.city);
                values.put(HIST_STATE, locality.state);
                values.put(HIST_COUNTRY, locality.country);
            }
            values.put(HIST_UID, Long.valueOf(trans.getUid()));
            values.put(HIST_TIMESTAMP, Long.valueOf(System.currentTimeMillis()));
            values.put(HIST_MEDIA_TYPE, Integer.valueOf(mediaType));
            if (lProfile != null) {
                values.put(HIST_PROF_PIC, lProfile.pictureUrl);
                values.put(HIST_PROF_NAME, lProfile.nickname);
                values.put(HIST_PROF_USERID, lProfile.userId);
                values.put(HIST_PROF_ANONYMOUS, Boolean.valueOf(lProfile.anonymous));
            }
            getDb().insert(HISTORY_TABLE_NAME, null, values);
            if (groupInfo != null && !isReshare) {
                updateZzFilePath(trans);
            }
            updateZzSessionIsTransferInProgress(trans.getUid());
            getDb().setTransactionSuccessful();
            getDb().endTransaction();
            notifyChange();
        } catch (Throwable th) {
            getDb().endTransaction();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public void setUploadComplete(ContentContainer trans, int transferCompleteStatus) {
        getDb().beginTransaction();
        try {
            updateUploadStatus(trans.getUid(), transferCompleteStatus, getMetadataAvailabilityTime(trans));
            updateZzSessionIsTransferInProgress(trans.getUid());
            if (transferCompleteStatus == 2 || transferCompleteStatus == 1) {
                getDb().delete(HISTORY_TABLE_NAME, String.format("%s = ?", new Object[]{HIST_UID}), new String[]{String.valueOf(trans.getUid())});
            }
            getDb().setTransactionSuccessful();
            getDb().endTransaction();
            notifyChange();
        } catch (Throwable th) {
            getDb().endTransaction();
            throw th;
        }
    }

    private long getMetadataAvailabilityTime(ContentContainer trans) {
        return System.currentTimeMillis() + trans.getTtl();
    }

    private void updateUploadStatus(long uid, int transferCompleteStatus, long mediaExpireTime) {
        String selection = String.format("%s = ? OR %s = ?", new Object[]{HIST_UID, HIST_RESHARE_UID});
        String[] selectionArgs = {String.valueOf(uid), String.valueOf(uid)};
        ContentValues values = new ContentValues();
        values.put(HIST_IS_TRANSFER_IN_PROGRESS, Boolean.valueOf(false));
        values.put(HIST_MEDIA_EXPIRATION_TIMESTAMP, Long.valueOf(mediaExpireTime));
        values.put(HIST_TRANSFER_COMPLETE_STATUS, Integer.valueOf(transferCompleteStatus));
        getDb().update(HISTORY_TABLE_NAME, values, selection, selectionArgs);
    }

    public void setFromShareId(long uid, String from, String shareId) {
        ContentValues values = new ContentValues();
        values.put(HIST_FROM, from);
        values.put(HIST_SHAREID, shareId);
        getDb().update(HISTORY_TABLE_NAME, values, String.format("%s = ?", new Object[]{HIST_UID}), new String[]{String.valueOf(uid)});
    }

    /* JADX INFO: finally extract failed */
    public void addDownload(ContentContainer trans, String thumbnail) {
        int mediaType;
        boolean z = true;
        getDb().beginTransaction();
        try {
            long now = System.currentTimeMillis();
            if (FileUtils.isVideo(trans.getMimeType())) {
                mediaType = 2;
            } else {
                mediaType = 1;
            }
            ContentValues values = new ContentValues();
            values.put(HIST_ACTION, ACTION_MEDIA_RECEIVE);
            values.put(HIST_ACCOUNT_COLUMN, trans.getUserAccount());
            values.put(HIST_FILEPATH, trans.getPath());
            values.put(HIST_SHAREID, trans.getShareId());
            values.put(HIST_FROM, trans.getOrigin());
            values.put(HIST_IS_ZZ_ACTIVE, Boolean.valueOf(isZzActive(trans)));
            GroupInfo groupInfo = trans.getGroupInfo();
            if (groupInfo != null) {
                values.put(HIST_ZZ_OWNER, groupInfo.owner);
                values.put(HIST_ZZ_ID, groupInfo.id);
                values.put(HIST_ZZ_NAME, groupInfo.name);
            }
            Locality locality = trans.getLocality();
            if (locality != null) {
                values.put(HIST_LATITUDE, Double.valueOf(locality.latitude));
                values.put(HIST_LONGITUDE, Double.valueOf(locality.longitude));
                values.put(HIST_CITY, locality.city);
                values.put(HIST_STATE, locality.state);
                values.put(HIST_COUNTRY, locality.country);
            }
            values.put(HIST_THUMBNAIL, thumbnail);
            values.put(HIST_IS_TRANSFER_IN_PROGRESS, Boolean.valueOf(true));
            values.put(HIST_UID, Long.valueOf(trans.getUid()));
            String str = HIST_IS_PIN_LOCKED;
            if (TextUtils.isEmpty(trans.getPin())) {
                z = false;
            }
            values.put(str, Boolean.valueOf(z));
            values.put(HIST_PIN, trans.getPin());
            values.put(HIST_TIMESTAMP, Long.valueOf(now));
            values.put(HIST_MEDIA_TYPE, Integer.valueOf(mediaType));
            getDb().insert(HISTORY_TABLE_NAME, null, values);
            updateZzSessionIsTransferInProgress(trans.getUid());
            getDb().setTransactionSuccessful();
            getDb().endTransaction();
            notifyChange();
        } catch (Throwable th) {
            getDb().endTransaction();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public void setDownloadComplete(ContentContainer trans, int transferCompleteStatus) {
        getDb().beginTransaction();
        try {
            if (trans.isGroupRelated()) {
                updateZzFilePath(trans);
            }
            String selection = String.format("%s = ?", new Object[]{HIST_UID});
            String[] selectionArgs = {String.valueOf(trans.getUid())};
            ContentValues values = new ContentValues();
            values.put(HIST_FILEPATH, trans.getPath());
            values.put(HIST_IS_TRANSFER_IN_PROGRESS, Boolean.valueOf(false));
            values.put(HIST_TRANSFER_COMPLETE_STATUS, Integer.valueOf(transferCompleteStatus));
            values.put(HIST_PROF_PIC, trans.profile.pictureUrl);
            values.put(HIST_PROF_NAME, trans.profile.nickname);
            values.put(HIST_PROF_USERID, trans.profile.userId);
            values.put(HIST_PROF_ANONYMOUS, Boolean.valueOf(trans.profile.anonymous));
            getDb().update(HISTORY_TABLE_NAME, values, selection, selectionArgs);
            if (transferCompleteStatus != 0) {
                ContentValues values2 = new ContentValues();
                values2.put(HIST_IS_DELETED, Boolean.valueOf(true));
                getDb().update(HISTORY_TABLE_NAME, values2, selection, selectionArgs);
            }
            updateZzSessionIsTransferInProgress(trans.getUid());
            getDb().setTransactionSuccessful();
            getDb().endTransaction();
            notifyChange();
        } catch (Throwable th) {
            getDb().endTransaction();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public void updateProfileForZappedCards(Profile aProfile) {
        getDb().beginTransaction();
        try {
            String selection = String.format("%s = ?", new Object[]{HIST_PROF_USERID});
            String[] selectionArgs = {String.valueOf(aProfile.userId)};
            ContentValues values = new ContentValues();
            values.put(HIST_PROF_PIC, aProfile.pictureUrl);
            values.put(HIST_PROF_NAME, aProfile.nickname);
            values.put(HIST_PROF_ANONYMOUS, Boolean.valueOf(aProfile.anonymous));
            getDb().update(HISTORY_TABLE_NAME, values, selection, selectionArgs);
            getDb().setTransactionSuccessful();
            getDb().endTransaction();
            notifyChange();
        } catch (Throwable th) {
            getDb().endTransaction();
            throw th;
        }
    }

    private void updateZzFilePath(ContentContainer trans) {
        if (trans.getPath() != null) {
            ContentValues val = new ContentValues();
            val.put(HIST_FILEPATH, trans.getPath());
            String selection = String.format("%s = ? AND %s IS NULL AND %s = ? AND %s = ?", new Object[]{HIST_ACTION, HIST_FILEPATH, HIST_ZZ_OWNER, HIST_ZZ_ID});
            GroupInfo groupInfo = trans.getGroupInfo();
            getDb().update(HISTORY_TABLE_NAME, val, selection, new String[]{ACTION_ZZ_SESSION, groupInfo.owner, groupInfo.id});
        }
    }

    /* JADX INFO: finally extract failed */
    private void updateZzSessionIsTransferInProgress(long uid) {
        Cursor cursor = null;
        try {
            Cursor cursor2 = getDb().query(HISTORY_TABLE_NAME, new String[]{HIST_ZZ_OWNER, HIST_ZZ_ID}, String.format("(%s = ? OR %s = ?) AND (%s = ? OR %s = ?) AND %s IS NOT NULL AND %s IS NOT NULL", new Object[]{HIST_ACTION, HIST_ACTION, HIST_UID, HIST_RESHARE_UID, HIST_ZZ_OWNER, HIST_ZZ_ID}), new String[]{ACTION_MEDIA_RECEIVE, ACTION_MEDIA_SEND, String.valueOf(uid), String.valueOf(uid)}, null, null, null, null);
            while (cursor2.moveToNext()) {
                updateZzSessionIsTransferInProgress(TCH.getZzOwner(cursor2), TCH.getZzId(cursor2));
            }
            if (cursor2 != null) {
                cursor2.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    private void updateZzSessionIsTransferInProgress(String zzOwner, String zzId) {
        boolean isTransferInProgress;
        if (getTransfersInProgressCount(zzOwner, zzId) != 0) {
            isTransferInProgress = true;
        } else {
            isTransferInProgress = false;
        }
        ContentValues val = new ContentValues();
        val.put(HIST_IS_TRANSFER_IN_PROGRESS, Boolean.valueOf(isTransferInProgress));
        getDb().update(HISTORY_TABLE_NAME, val, String.format("%s = ? AND %s = ? AND %s = ?", new Object[]{HIST_ACTION, HIST_ZZ_OWNER, HIST_ZZ_ID}), new String[]{ACTION_ZZ_SESSION, zzOwner, zzId});
    }

    private long getTransfersInProgressCount(String zzOwner, String zzId) {
        return DatabaseUtils.queryNumEntries(getDb(), HISTORY_TABLE_NAME, String.format("%s = ? AND %s = ? AND (%s = ? OR %s = ?) AND %s = ?", new Object[]{HIST_ZZ_OWNER, HIST_ZZ_ID, HIST_ACTION, HIST_ACTION, HIST_IS_TRANSFER_IN_PROGRESS}), new String[]{zzOwner, zzId, ACTION_MEDIA_RECEIVE, ACTION_MEDIA_SEND, "1"});
    }

    private boolean isZzActive(ContentContainer trans) {
        GroupInfo currentGroupInfo = GCSP.getGroupManager().getGroupInfo(null);
        GroupInfo transGroupInfo = trans.getGroupInfo();
        return transGroupInfo != null && TextUtils.equals(transGroupInfo.owner, currentGroupInfo.owner) && TextUtils.equals(transGroupInfo.id, currentGroupInfo.id);
    }

    public void insertActiveZzSession(String zzOwner, String zzId, String zzName, String thumbnail, String pin) {
        getDb().beginTransaction();
        try {
            ContentValues val = new ContentValues();
            val.put(HIST_IS_ZZ_ACTIVE, Boolean.valueOf(true));
            int updates = getDb().update(HISTORY_TABLE_NAME, val, String.format("%s = ? AND %s = ?", new Object[]{HIST_ZZ_OWNER, HIST_ZZ_ID}), new String[]{zzOwner, zzId});
            if (insertZzSession(zzOwner, zzId, zzName, thumbnail, pin)) {
                updates++;
            }
            getDb().setTransactionSuccessful();
            if (updates != 0) {
                notifyChange();
            }
        } finally {
            getDb().endTransaction();
        }
    }

    private boolean insertZzSession(String zzOwner, String zzId, String zzName, String thumbnail, String pin) {
        if (DatabaseUtils.queryNumEntries(getDb(), HISTORY_TABLE_NAME, String.format("%s = ? AND %s = ? AND %s = ?", new Object[]{HIST_ACTION, HIST_ZZ_OWNER, HIST_ZZ_ID}), new String[]{ACTION_ZZ_SESSION, zzOwner, zzId}) != 0) {
            return false;
        }
        ContentValues values = new ContentValues();
        values.put(HIST_ACTION, ACTION_ZZ_SESSION);
        values.put(HIST_ACCOUNT_COLUMN, GCSP.getAuthenticator().getAccount());
        values.put(HIST_IS_ZZ_ACTIVE, Boolean.valueOf(true));
        values.put(HIST_ZZ_OWNER, zzOwner);
        values.put(HIST_ZZ_ID, zzId);
        values.put(HIST_ZZ_NAME, zzName);
        values.put(HIST_THUMBNAIL, thumbnail);
        values.put(HIST_PIN, pin);
        values.put(HIST_TIMESTAMP, Long.valueOf(System.currentTimeMillis()));
        Profile currentUserProfile = ZapProfileSharedPerfs.getProfile();
        if (currentUserProfile == null || !GCSP.getAuthenticator().getAccount().equals(zzOwner)) {
            Profile invitedUserProfile = getZzInviteProfile(zzOwner, zzId);
            if (invitedUserProfile != null) {
                values.put(HIST_PROF_PIC, invitedUserProfile.pictureUrl);
                values.put(HIST_PROF_NAME, invitedUserProfile.nickname);
                values.put(HIST_PROF_USERID, invitedUserProfile.userId);
                values.put(HIST_PROF_ANONYMOUS, Boolean.valueOf(invitedUserProfile.anonymous));
            }
        } else {
            values.put(HIST_PROF_PIC, currentUserProfile.pictureUrl);
            values.put(HIST_PROF_NAME, currentUserProfile.nickname);
            values.put(HIST_PROF_USERID, currentUserProfile.userId);
            values.put(HIST_PROF_ANONYMOUS, Boolean.valueOf(currentUserProfile.anonymous));
        }
        getDb().insert(HISTORY_TABLE_NAME, null, values);
        return true;
    }

    public void setZzInactive() {
        ContentValues val = new ContentValues();
        val.put(HIST_IS_ZZ_ACTIVE, Boolean.valueOf(false));
        getDb().update(HISTORY_TABLE_NAME, val, String.format("%s = ?", new Object[]{HIST_IS_ZZ_ACTIVE}), new String[]{"1"});
        notifyChange();
    }

    public Profile getProfileForActiveZone(String zzOwner, String zzId) {
        Cursor c = null;
        try {
            c = getDb().query(HISTORY_TABLE_NAME, new String[]{HIST_PROF_NAME, HIST_PROF_PIC, HIST_PROF_USERID, HIST_PROF_ANONYMOUS}, String.format("%s = ? AND %s = ? AND %s = ? AND (%s IS NULL OR %s != ?)", new Object[]{HIST_ACTION, HIST_ZZ_OWNER, HIST_ZZ_ID, HIST_IS_DELETED, HIST_IS_DELETED}), new String[]{ACTION_ZZ_SESSION, zzOwner, zzId, "1"}, null, null, null);
            if (c.getCount() == 0) {
                c.close();
                return null;
            }
            c.moveToFirst();
            Profile p = new Profile();
            try {
                p.pictureUrl = TCH.getProfilePic(c);
                p.nickname = TCH.getProfileName(c);
                p.userId = TCH.getProfileUserId(c);
                p.anonymous = TCH.getProfileVisiblity(c).booleanValue();
                c.close();
                Profile profile = p;
                return p;
            } catch (Throwable th) {
                th = th;
                Profile profile2 = p;
                c.close();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
        }
    }

    /* JADX INFO: finally extract failed */
    public void insertZzInvite(GroupInvite zzInvite, Profile profile) {
        getDb().beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(HIST_ACTION, ACTION_ZZ_INVITE);
            values.put(HIST_ACCOUNT_COLUMN, GCSP.getAuthenticator().getAccount());
            values.put(HIST_ZZ_OWNER, zzInvite.owner);
            values.put(HIST_ZZ_ID, zzInvite.id);
            values.put(HIST_ZZ_NAME, zzInvite.name);
            values.put(HIST_THUMBNAIL, zzInvite.thumbnail);
            values.put(HIST_IS_PIN_LOCKED, Boolean.valueOf(zzInvite.locked));
            values.put(HIST_UID, Integer.valueOf(zzInvite.inviteUid));
            values.put(HIST_TIMESTAMP, Long.valueOf(System.currentTimeMillis()));
            if (profile != null) {
                values.put(HIST_PROF_PIC, profile.pictureUrl);
                values.put(HIST_PROF_NAME, profile.nickname);
                values.put(HIST_PROF_USERID, profile.userId);
                values.put(HIST_PROF_ANONYMOUS, Boolean.valueOf(profile.anonymous));
            }
            if (getDb().update(HISTORY_TABLE_NAME, values, String.format("%s = ? AND %s = ?", new Object[]{HIST_ACTION, HIST_UID}), new String[]{ACTION_ZZ_INVITE, String.valueOf(zzInvite.inviteUid)}) == 0) {
                getDb().insert(HISTORY_TABLE_NAME, null, values);
            }
            getDb().setTransactionSuccessful();
            getDb().endTransaction();
            notifyChange();
        } catch (Throwable th) {
            getDb().endTransaction();
            throw th;
        }
    }

    public Profile getZzInviteProfile(String zzOwner, String zzId) {
        Cursor c = null;
        try {
            c = getDb().query(HISTORY_TABLE_NAME, new String[]{HIST_PROF_NAME, HIST_PROF_PIC, HIST_PROF_USERID, HIST_PROF_ANONYMOUS}, String.format("%s = ? AND %s = ? AND %s = ? AND (%s IS NULL OR %s != ?)", new Object[]{HIST_ACTION, HIST_ZZ_OWNER, HIST_ZZ_ID, HIST_IS_DELETED, HIST_IS_DELETED}), new String[]{ACTION_ZZ_INVITE, zzOwner, zzId, "1"}, null, null, null);
            if (c.getCount() == 0) {
                c.close();
                return null;
            }
            c.moveToFirst();
            Profile p = new Profile();
            try {
                p.pictureUrl = TCH.getProfilePic(c);
                p.nickname = TCH.getProfileName(c);
                p.userId = TCH.getProfileUserId(c);
                p.anonymous = TCH.getProfileVisiblity(c).booleanValue();
                c.close();
                Profile profile = p;
                return p;
            } catch (Throwable th) {
                th = th;
                Profile profile2 = p;
                c.close();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
        }
    }

    public GroupInvite getZzInvite(int uid) {
        Cursor c = null;
        try {
            c = getDb().query(HISTORY_TABLE_NAME, new String[]{HIST_ZZ_OWNER, HIST_ZZ_ID, HIST_ZZ_NAME, HIST_THUMBNAIL, HIST_IS_PIN_LOCKED, HIST_UID}, String.format("%s = ? AND %s = ? AND (%s IS NULL OR %s != ?)", new Object[]{HIST_ACTION, HIST_UID, HIST_IS_DELETED, HIST_IS_DELETED}), new String[]{ACTION_ZZ_INVITE, String.valueOf(uid), "1"}, null, null, null);
            if (c.getCount() == 0) {
                return null;
            }
            c.moveToFirst();
            GroupInvite groupInvite = new GroupInvite(TCH.getZzOwner(c), TCH.getZzId(c), TCH.getZzName(c), TCH.getThumbnail(c), TCH.isPinLocked(c), TCH.getUid(c));
            if (c == null) {
                return groupInvite;
            }
            c.close();
            return groupInvite;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public GroupInvite getZzInvite(String zzOwner, String zzId) {
        Cursor c = null;
        try {
            c = getDb().query(HISTORY_TABLE_NAME, new String[]{HIST_ZZ_OWNER, HIST_ZZ_ID, HIST_ZZ_NAME, HIST_THUMBNAIL, HIST_IS_PIN_LOCKED, HIST_UID}, String.format("%s = ? AND %s = ? AND %s = ? AND (%s IS NULL OR %s != ?)", new Object[]{HIST_ACTION, HIST_ZZ_OWNER, HIST_ZZ_ID, HIST_IS_DELETED, HIST_IS_DELETED}), new String[]{ACTION_ZZ_INVITE, zzOwner, zzId, "1"}, null, null, null);
            if (c.getCount() == 0) {
                return null;
            }
            c.moveToFirst();
            GroupInvite groupInvite = new GroupInvite(TCH.getZzOwner(c), TCH.getZzId(c), TCH.getZzName(c), TCH.getThumbnail(c), TCH.isPinLocked(c), TCH.getUid(c));
            if (c == null) {
                return groupInvite;
            }
            c.close();
            return groupInvite;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public void deleteZzInvite(int uid) {
        getDb().beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(HIST_IS_DELETED, Boolean.valueOf(true));
            int deleted = getDb().update(HISTORY_TABLE_NAME, values, String.format("%s = ? AND %s = ? AND (%s IS NULL OR %s != ?)", new Object[]{HIST_ACTION, HIST_UID, HIST_IS_DELETED, HIST_IS_DELETED}), new String[]{ACTION_ZZ_INVITE, String.valueOf(uid), "1"});
            getDb().setTransactionSuccessful();
            if (deleted != 0) {
                notifyChange();
            }
        } finally {
            getDb().endTransaction();
        }
    }

    public boolean hasZzInvite(String zzOwner, String zzId) {
        if (DatabaseUtils.queryNumEntries(getDb(), HISTORY_TABLE_NAME, String.format("%s = ? AND %s = ? AND %s = ? AND (%s IS NULL OR %s != ?)", new Object[]{HIST_ACTION, HIST_ZZ_OWNER, HIST_ZZ_ID, HIST_IS_DELETED, HIST_IS_DELETED}), new String[]{ACTION_ZZ_INVITE, zzOwner, zzId, "1"}) != 0) {
            return true;
        }
        return false;
    }

    public void setAbuseReported(Context context, String filePath, boolean value) {
        ContentValues values = new ContentValues();
        values.put(HIST_IS_ABUSE_REPORTED, Boolean.valueOf(value));
        if (getDb().update(HISTORY_TABLE_NAME, values, String.format("%s = ?", new Object[]{HIST_FILEPATH}), new String[]{filePath}) != 0) {
            notifyChange();
        }
    }

    public boolean isAbuseReported(String filePath) {
        if (DatabaseUtils.queryNumEntries(getDb(), HISTORY_TABLE_NAME, String.format("%s = ? AND %s = ?", new Object[]{HIST_FILEPATH, HIST_IS_ABUSE_REPORTED}), new String[]{filePath, "1"}) != 0) {
            return true;
        }
        return false;
    }

    public void deleteMedia(String filePath) {
        if (DBG) {
            Log.d("Zap-HistoryHandler", "Deleting Zap Hitory Info - file: " + filePath);
        }
        Cursor cursor = null;
        try {
            getDb().beginTransaction();
            int changed = getDb().delete(HISTORY_TABLE_NAME, String.format("(%s = ? OR %s = ? OR %s = ? OR %s = ?) AND %s = ?", new Object[]{HIST_ACTION, HIST_ACTION, HIST_ACTION, HIST_ACTION, HIST_FILEPATH}), new String[]{ACTION_MEDIA_RECEIVE, ACTION_MEDIA_SEND, ACTION_MEDIA_RESHARE, ACTION_DEMO_CARD, filePath});
            if (changed != 0) {
                cursor = getZzSessionByCover(filePath);
                setCover(cursor);
                getDb().setTransactionSuccessful();
                if (cursor != null) {
                    cursor.close();
                }
                getDb().endTransaction();
                if (changed != 0) {
                    notifyChange();
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            getDb().endTransaction();
        }
    }

    private Cursor getZzSessionByCover(String filePath) {
        return getDb().query(HISTORY_TABLE_NAME, new String[]{HIST_ID, HIST_ZZ_OWNER, HIST_ZZ_ID}, String.format("%s = ? AND %s = ?", new Object[]{HIST_ACTION, HIST_FILEPATH}), new String[]{ACTION_ZZ_SESSION, filePath}, null, null, null);
    }

    private void setCover(Cursor cursor) {
        while (cursor.moveToNext()) {
            long id = (long) TCH.getId(cursor);
            String filePath = getOldestMediaPath(TCH.getZzOwner(cursor), TCH.getZzId(cursor));
            ContentValues val = new ContentValues();
            val.put(HIST_FILEPATH, filePath);
            getDb().update(HISTORY_TABLE_NAME, val, String.format("%s = ?", new Object[]{HIST_ID}), new String[]{String.valueOf(id)});
        }
    }

    public String getOldestMediaPath(String zzOwner, String zzId) {
        Cursor cursor = null;
        try {
            String[] projection = {HIST_FILEPATH};
            String selection = String.format("(%s = ? OR %s = ? OR %s = ?) AND %s = ? AND %s = ?", new Object[]{HIST_ACTION, HIST_ACTION, HIST_ACTION, HIST_ZZ_OWNER, HIST_ZZ_ID});
            String[] selectionArgs = {ACTION_MEDIA_RECEIVE, ACTION_MEDIA_SEND, ACTION_MEDIA_RESHARE, zzOwner, zzId};
            Object[] objArr = {HIST_TIMESTAMP};
            cursor = getDb().query(HISTORY_TABLE_NAME, projection, selection, selectionArgs, null, null, String.format("%s ASC", objArr), "1");
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            String filePath = TCH.getFilePath(cursor);
            if (cursor == null) {
                return filePath;
            }
            cursor.close();
            return filePath;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void deleteZzSession(String zzOwner, String zzId) {
        if (getDb().delete(HISTORY_TABLE_NAME, String.format("%s = ? AND %s = ?", new Object[]{HIST_ZZ_OWNER, HIST_ZZ_ID}), new String[]{zzOwner, zzId}) != 0) {
            notifyChange();
        }
    }

    public boolean hasRecentCompletedRecord(String from, String shareId, String action) {
        return DatabaseUtils.queryNumEntries(getDb(), HISTORY_TABLE_NAME, String.format("%s = ? AND %s = ? AND %s = ? AND %s IS NOT NULL AND (%s = ? OR %s = ?) AND %s > ?", new Object[]{HIST_FROM, HIST_SHAREID, HIST_ACTION, HIST_TRANSFER_COMPLETE_STATUS, HIST_TRANSFER_COMPLETE_STATUS, HIST_TRANSFER_COMPLETE_STATUS, HIST_TIMESTAMP}), new String[]{from, shareId, action, String.valueOf(0), String.valueOf(2), String.valueOf(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1))}) > 0;
    }

    public void setDeleted(long id) {
        ContentValues val = new ContentValues();
        val.put(HIST_IS_DELETED, Boolean.valueOf(true));
        if (getDb().update(HISTORY_TABLE_NAME, val, String.format("%s = ?", new Object[]{HIST_ID}), new String[]{String.valueOf(id)}) != 0) {
            notifyChange();
        }
    }

    public Cursor getUnackedZzTransfers(String zzOwner, String zzId) {
        return getDb().query(HISTORY_TABLE_NAME, new String[]{HIST_FROM, HIST_SHAREID}, String.format("(%s = ? OR %s = ? OR %s = ?) AND %s = ? AND %s = ? AND (%s != ? OR %s IS NULL) AND %s IS NOT NULL AND (%s = ? OR %s = ?)", new Object[]{HIST_ACTION, HIST_ACTION, HIST_ACTION, HIST_ZZ_OWNER, HIST_ZZ_ID, HIST_IS_TRANSFER_ACKED, HIST_IS_TRANSFER_ACKED, HIST_TRANSFER_COMPLETE_STATUS, HIST_TRANSFER_COMPLETE_STATUS, HIST_TRANSFER_COMPLETE_STATUS}), new String[]{ACTION_MEDIA_RECEIVE, ACTION_MEDIA_SEND, ACTION_MEDIA_RESHARE, zzOwner, zzId, "1", String.valueOf(0), String.valueOf(2)}, null, null, null);
    }

    public Cursor getCompletedTransfers(long since) {
        return getDb().query(HISTORY_TABLE_NAME, new String[]{HIST_FROM, HIST_SHAREID}, String.format("%s IS NOT NULL AND (%s = ? OR %s = ? OR %s = ?) AND %s IS NOT NULL AND (%s = ? OR %s = ?) AND %s > ?", new Object[]{HIST_SHAREID, HIST_ACTION, HIST_ACTION, HIST_ACTION, HIST_TRANSFER_COMPLETE_STATUS, HIST_TRANSFER_COMPLETE_STATUS, HIST_TRANSFER_COMPLETE_STATUS, HIST_TIMESTAMP}), new String[]{ACTION_MEDIA_RECEIVE, ACTION_MEDIA_SEND, ACTION_MEDIA_RESHARE, String.valueOf(0), String.valueOf(2), String.valueOf(since)}, null, null, null);
    }

    public void setAckedZzTransfers(String from, String shareId) {
        ContentValues val = new ContentValues();
        val.put(HIST_IS_TRANSFER_ACKED, Boolean.valueOf(true));
        getDb().update(HISTORY_TABLE_NAME, val, String.format("%s = ? AND %s = ?", new Object[]{HIST_FROM, HIST_SHAREID}), new String[]{from, shareId});
    }

    public Cursor getAllFiles(String zzOwner, String zzId) {
        String[] projection = {HIST_FILEPATH};
        String selection = String.format("%s = ? AND %s = ? AND (%s = ? OR %s = ?)", new Object[]{HIST_ZZ_OWNER, HIST_ZZ_ID, HIST_ACTION, HIST_ACTION});
        String[] selectionArgs = {zzOwner, zzId, ACTION_MEDIA_SEND, ACTION_MEDIA_RECEIVE};
        return getDb().query(true, HISTORY_TABLE_NAME, projection, selection, selectionArgs, HIST_FILEPATH, null, null, "100");
    }

    public String getShareId(long uid) {
        Cursor cursor = null;
        try {
            cursor = getDb().query(HISTORY_TABLE_NAME, new String[]{HIST_SHAREID}, String.format("%s = ?", new Object[]{HIST_UID}), new String[]{String.valueOf(uid)}, null, null, null, "1");
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            String shareId = TCH.getShareId(cursor);
            if (cursor == null) {
                return shareId;
            }
            cursor.close();
            return shareId;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public void cancelShare(String from, String shareId) {
        getDb().beginTransaction();
        try {
            long uid = getUid(from, shareId);
            updateUploadStatus(uid, 2, -1);
            updateZzSessionIsTransferInProgress(uid);
            getDb().setTransactionSuccessful();
            getDb().endTransaction();
            notifyChange();
        } catch (Throwable th) {
            getDb().endTransaction();
            throw th;
        }
    }

    private long getUid(String from, String shareId) {
        long uid;
        Cursor cursor = null;
        try {
            cursor = getDb().query(HISTORY_TABLE_NAME, new String[]{HIST_UID}, String.format("%s = ? AND %s = ?", new Object[]{HIST_FROM, HIST_SHAREID}), new String[]{from, shareId}, null, null, null, "1");
            if (cursor.getCount() == 0) {
                uid = -1;
            } else {
                cursor.moveToFirst();
                uid = (long) TCH.getUid(cursor);
                if (cursor != null) {
                    cursor.close();
                }
            }
            return uid;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static int getMediaType(String fileName) {
        return FileUtils.isVideo(FileUtils.getMimeTypeFromFileName(fileName)) ? 2 : 1;
    }

    /* JADX INFO: finally extract failed */
    public void addDemoCard(File f) {
        getDb().beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(HIST_ACTION, ACTION_DEMO_CARD);
            values.put(HIST_ACCOUNT_COLUMN, GCSP.getAuthenticator().getAccount());
            values.put(HIST_FILEPATH, f.getAbsolutePath());
            values.put(HIST_CITY, App.getAppContext().getString(R.string.sample_zap));
            values.put(HIST_TIMESTAMP, Long.valueOf(System.currentTimeMillis()));
            values.put(HIST_MEDIA_TYPE, Integer.valueOf(1));
            getDb().insert(HISTORY_TABLE_NAME, null, values);
            getDb().setTransactionSuccessful();
            getDb().endTransaction();
            notifyChange();
        } catch (Throwable th) {
            getDb().endTransaction();
            throw th;
        }
    }

    public long getMediaCount(String zzOwner, String zzId) {
        return DatabaseUtils.queryNumEntries(getDb(), HISTORY_TABLE_NAME, String.format("%s = ? AND %s = ? AND (%s = ? OR %s = ?)", new Object[]{HIST_ZZ_OWNER, HIST_ZZ_ID, HIST_ACTION, HIST_ACTION}), new String[]{zzOwner, zzId, ACTION_MEDIA_SEND, ACTION_MEDIA_RECEIVE});
    }

    public boolean hasTransfersInProgress() {
        if (DatabaseUtils.queryNumEntries(getDb(), HISTORY_TABLE_NAME, String.format("%s = ?", new Object[]{HIST_IS_TRANSFER_IN_PROGRESS}), new String[]{"1"}) != 0) {
            return true;
        }
        return false;
    }

    public void setLeftZz(String zzOwner, String zzId, boolean isLeftZz) {
        ContentValues val = new ContentValues();
        val.put(HIST_IS_LEFT_ZZ, Boolean.valueOf(isLeftZz));
        if (getDb().update(HISTORY_TABLE_NAME, val, String.format("%s = ? AND %s = ? AND %s = ?", new Object[]{HIST_ACTION, HIST_ZZ_OWNER, HIST_ZZ_ID}), new String[]{ACTION_ZZ_SESSION, zzOwner, zzId}) > 0) {
            notifyChange();
        }
    }

    public Cursor getZzCursor(String zzOwner, String zzId) {
        return getDb().query(HISTORY_TABLE_NAME, new String[]{HIST_ZZ_NAME, HIST_THUMBNAIL, HIST_PIN}, String.format("%s = ? AND %s = ? AND %s = ?", new Object[]{HIST_ACTION, HIST_ZZ_OWNER, HIST_ZZ_ID}), new String[]{ACTION_ZZ_SESSION, zzOwner, zzId}, null, null, null, null);
    }

    public void setZzEmpty(String zzOwner, String zzId) {
        ContentValues val = new ContentValues();
        val.put(HIST_FILEPATH, null);
        if (getDb().update(HISTORY_TABLE_NAME, val, String.format("%s = ? AND %s = ? AND %s = ?", new Object[]{HIST_ACTION, HIST_ZZ_OWNER, HIST_ZZ_ID}), new String[]{ACTION_ZZ_SESSION, zzOwner, zzId}) > 0) {
            notifyChange();
        }
    }

    public static String e(String str) {
        return DatabaseUtils.sqlEscapeString(str);
    }
}
