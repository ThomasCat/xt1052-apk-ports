package com.motorola.sharing.provider.cloud;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.motorola.sharing.GroupManager.CreateParams;
import com.motorola.sharing.GroupManager.JoinParams;
import com.motorola.sharing.GroupManager.ShareInviteParams;
import com.motorola.sharing.provider.cloud.group.GroupHandler;
import com.motorola.sharing.util.Logger;

public class GroupReceiver extends BroadcastReceiver {
    public static final String ACTION_GROUP_CREATE = "com.motorola.sharing.provider.cloud.ACTION_GROUP_CREATE";
    public static final String ACTION_GROUP_JOIN = "com.motorola.sharing.provider.cloud.ACTION_GROUP_JOIN";
    public static final String ACTION_GROUP_LEAVE = "com.motorola.sharing.provider.cloud.ACTION_GROUP_LEAVE";
    public static final String ACTION_GROUP_SHARE_INVITE = "com.motorola.sharing.provider.cloud.ACTION_GROUP_SHARE_INVITE";
    public static final String ACTION_GROUP_SYNC_STATE = "com.motorola.sharing.provider.cloud.ACTION_GROUP_SYNC_STATE";
    public static final String EXTRA_GROUP_PARAMS = "com.motorola.sharing.provider.cloud.EXTRA_GROUP_PARAMS";
    private static final String TAG = Logger.getCloudTag("GroupReceiver");

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Logger.d(TAG, "got " + action);
        if (action.equals(ACTION_GROUP_SYNC_STATE)) {
            GroupHandler.getInstance().syncGroupState();
        } else if (action.equals(ACTION_GROUP_CREATE)) {
            GroupHandler.getInstance().createGroup((CreateParams) intent.getSerializableExtra(EXTRA_GROUP_PARAMS));
        } else if (action.equals(ACTION_GROUP_LEAVE)) {
            GroupHandler.getInstance().leaveGroup(false);
        } else if (action.equals(ACTION_GROUP_SHARE_INVITE)) {
            GroupHandler.getInstance().shareGroupInvite((ShareInviteParams) intent.getSerializableExtra(EXTRA_GROUP_PARAMS));
        } else if (action.equals(ACTION_GROUP_JOIN)) {
            GroupHandler.getInstance().joinGroup((JoinParams) intent.getSerializableExtra(EXTRA_GROUP_PARAMS));
        }
    }
}
