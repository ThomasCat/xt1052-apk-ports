package com.motorola.sharing;

import android.os.Environment;
import com.motorola.sharing.util.Logger.LogLevel;

public class Defaults {
    public static final String BASE_FILE_NAME = "content";
    public static final String CHROMECAST_DOWNLOADS_DIRECTORY = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS + "/chromecastempfiles").toString();
    public static final String DOWNLOADS_DIRECTORY = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
    public static final boolean GOOGLE_ANALYTICS_ENABLED = false;
    public static final long GROUP_INVITE_TTL = 120000;
    public static final int JPEG_REDUCTION_MAX_SIDE_SIZE = 1400;
    public static final int JPEG_REDUCTION_MIN_TARGET_SIZE = 512000;
    public static final float JPEG_REDUCTION_SIZE_FACTOR = 0.75f;
    public static final LogLevel LOG_LEVEL = LogLevel.INFO;
    public static final int MAX_DISCOVERY_RESULTS_PER_REQUEST = 20;
    public static final boolean MOTO_CHECKIN_ENABLED = false;
    public static final int SHARING_DISTANCE = 300;
    public static final long SHARING_TTL = 120000;
}
