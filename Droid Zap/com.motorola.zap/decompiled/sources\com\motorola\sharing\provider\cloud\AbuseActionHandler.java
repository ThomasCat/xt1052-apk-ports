package com.motorola.sharing.provider.cloud;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.motorola.sharing.Abuse.AbuseAction;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.util.SharedPreference;

public class AbuseActionHandler {
    private static AbuseActionHandler sInstance;

    public static synchronized AbuseActionHandler getInstance() {
        AbuseActionHandler abuseActionHandler;
        synchronized (AbuseActionHandler.class) {
            if (sInstance == null) {
                sInstance = new AbuseActionHandler();
            }
            abuseActionHandler = sInstance;
        }
        return abuseActionHandler;
    }

    private AbuseActionHandler() {
    }

    public synchronized boolean handleBlocking(ContentContainer container, AbuseAction abuseAction, boolean isSending) {
        boolean notificationRequired;
        boolean result;
        boolean z;
        switch (abuseAction) {
            case WARNING:
                notificationRequired = isFirstWarning(container.userCred.account);
                result = false;
                break;
            case SENDING_BLOCKED:
                notificationRequired = true;
                result = isSending;
                break;
            case BLOCKED:
                notificationRequired = true;
                result = true;
                break;
            default:
                z = false;
                break;
        }
        if (notificationRequired) {
            container.onAbuseEvent(abuseAction, CloudProvider.getInstance().getEndpoint());
        }
        z = result;
        return z;
    }

    private synchronized boolean isFirstWarning(String userAccount) {
        boolean z = false;
        synchronized (this) {
            SharedPreferences preferences = GCSP.getContext().getSharedPreferences(SharedPreference.PREFERENCES_FILE_NAME, 0);
            if (!preferences.contains("cloud_abusedUser:" + userAccount)) {
                Editor editor = preferences.edit();
                editor.putString("cloud_abusedUser:" + userAccount, "firstWarningReceived");
                editor.apply();
                z = true;
            }
        }
        return z;
    }
}
