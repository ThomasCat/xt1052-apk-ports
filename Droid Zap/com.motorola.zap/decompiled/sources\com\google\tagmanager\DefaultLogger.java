package com.google.tagmanager;

import android.util.Log;
import com.google.tagmanager.Logger.LogLevel;

class DefaultLogger implements Logger {
    private LogLevel mLogLevel = LogLevel.WARNING;

    DefaultLogger() {
    }

    public void e(String message) {
        if (this.mLogLevel.ordinal() <= LogLevel.ERROR.ordinal()) {
            Log.e("GoogleTagManager", message);
        }
    }

    public void e(String message, Throwable t) {
        if (this.mLogLevel.ordinal() <= LogLevel.ERROR.ordinal()) {
            Log.e("GoogleTagManager", message, t);
        }
    }

    public void w(String message) {
        if (this.mLogLevel.ordinal() <= LogLevel.WARNING.ordinal()) {
            Log.w("GoogleTagManager", message);
        }
    }

    public void w(String message, Throwable t) {
        if (this.mLogLevel.ordinal() <= LogLevel.WARNING.ordinal()) {
            Log.w("GoogleTagManager", message, t);
        }
    }

    public void i(String message) {
        if (this.mLogLevel.ordinal() <= LogLevel.INFO.ordinal()) {
            Log.i("GoogleTagManager", message);
        }
    }

    public void i(String message, Throwable t) {
        if (this.mLogLevel.ordinal() <= LogLevel.INFO.ordinal()) {
            Log.i("GoogleTagManager", message, t);
        }
    }

    public void d(String message) {
        if (this.mLogLevel.ordinal() <= LogLevel.DEBUG.ordinal()) {
            Log.d("GoogleTagManager", message);
        }
    }

    public void d(String message, Throwable t) {
        if (this.mLogLevel.ordinal() <= LogLevel.DEBUG.ordinal()) {
            Log.d("GoogleTagManager", message, t);
        }
    }

    public void v(String message) {
        if (this.mLogLevel.ordinal() <= LogLevel.VERBOSE.ordinal()) {
            Log.v("GoogleTagManager", message);
        }
    }

    public void v(String message, Throwable t) {
        if (this.mLogLevel.ordinal() <= LogLevel.VERBOSE.ordinal()) {
            Log.v("GoogleTagManager", message, t);
        }
    }

    public LogLevel getLogLevel() {
        return this.mLogLevel;
    }

    public void setLogLevel(LogLevel logLevel) {
        this.mLogLevel = logLevel;
    }
}
