package com.motorola.sharing.instrumentation;

import android.text.TextUtils;
import com.motorola.zap.ZapConstants;

public class ActionResult {
    private Error mError = null;
    private String mErrorExtra = null;
    private int mHttpStatusCode = 0;
    private Status mStatus = Status.UNKNOWN;

    public enum Error {
        EMPTY_RESPONSE("Empty response", ErrorType.SERVER),
        RESPONSE_NOT_PARSABLE("Response not parsable", ErrorType.SERVER),
        INVALID_RESPONSE("Invalid response", ErrorType.SERVER),
        INVALID_STATUS("Invalid status", ErrorType.SERVER),
        INVALID_PIN("Invalid PIN", ErrorType.SERVER),
        SHARE_CANCELED("Share canceled", ErrorType.SERVER),
        NO_METADATA("No metadata", ErrorType.SERVER),
        NO_USER_ACCOUNT("No user account", ErrorType.CLIENT),
        NOT_AUTHORIZED("Not authorized", ErrorType.CLIENT),
        AUTH_NETWORK_ERROR("Auth network error", ErrorType.CLIENT),
        INVALID_PATH("Invalid file path", ErrorType.CLIENT),
        MIME_TYPE_UNSUPPORTED("Mime-type not supported", ErrorType.CLIENT),
        LOW_STORAGE_MEMORY("Low storage memory", ErrorType.CLIENT),
        FILE_TOO_LARGE("File too large", ErrorType.CLIENT),
        DRM_MEDIA_SHARE_PROHIBITED("DRM media share prohibited", ErrorType.CLIENT),
        MEDIA_SHARE_PROHIBITED("Media share prohibited", ErrorType.CLIENT),
        NO_DATA_CONNECTION("No data connection", ErrorType.CLIENT),
        WIFI_ONLY_ALLOWED("WiFi Only allowed", ErrorType.CLIENT),
        LOCATION_NOT_ACCURATE("Location not accurate", ErrorType.CLIENT),
        LOCATION_DISABLED("Location disabled", ErrorType.CLIENT),
        FILE_IO_ERROR("File IO error", ErrorType.CLIENT),
        REQUEST_ERROR("Request not generated", ErrorType.CLIENT),
        DOWNLOAD_FAILED("Download failed", ErrorType.CLIENT),
        UPLOAD_FAILED("Upload failed", ErrorType.CLIENT),
        UNKNOWN("Unknown", ErrorType.CLIENT),
        EXCEEDED_MAX_RETRIES("Exceeded max retries", ErrorType.TRANSPORT),
        CONTENT_AVAILABILITY_TIMEOUT("Media availability timeout", ErrorType.TRANSPORT),
        TRANSFER_TIMEOUT("Transfer timeout", ErrorType.TRANSPORT),
        SSL_NO_SUCH_ALG_EXCEPTION("NoSuchAlgorithmException thrown", ErrorType.TRANSPORT),
        SSL_KEY_MANAGEMENT_EXCEPTION("KeyManagementException thrown", ErrorType.TRANSPORT),
        UNKNOWN_HOST_EXCEPTION("UnknownHostException thrown", ErrorType.TRANSPORT),
        CONNECTION_EXCEPTION("ConnectionException thrown", ErrorType.TRANSPORT);
        
        private String humanReadable;
        private ErrorType type;

        private Error(String humanReadable2, ErrorType type2) {
            this.humanReadable = humanReadable2;
            this.type = type2;
        }

        public String humanReadable() {
            return this.humanReadable;
        }

        public ErrorType type() {
            return this.type;
        }
    }

    public enum ErrorType {
        CLIENT("Client"),
        SERVER("Server"),
        TRANSPORT("Transport");
        
        private String humanReadable;

        private ErrorType(String humanReadable2) {
            this.humanReadable = humanReadable2;
        }

        public String humanReadable() {
            return this.humanReadable;
        }
    }

    public enum Status {
        UNKNOWN("Unknown"),
        SUCCESS("Success"),
        FAIL("Fail"),
        CANCELED("Canceled"),
        NO_CONTENT("No Content");
        
        private String humanReadable;

        private Status(String humanReadable2) {
            this.humanReadable = humanReadable2;
        }

        public String humanReadable() {
            return this.humanReadable;
        }
    }

    public void setSuccess() {
        setSuccess(Status.SUCCESS);
    }

    public void setSuccess(Status status) {
        switch (status) {
            case SUCCESS:
            case CANCELED:
            case NO_CONTENT:
                this.mStatus = status;
                this.mError = null;
                this.mErrorExtra = null;
                this.mHttpStatusCode = 0;
                return;
            default:
                throw new IllegalArgumentException("setting " + status + " is not allowed");
        }
    }

    public void setError(Error error) {
        setError(error, null, 0);
    }

    public void setError(Error error, String errorExtra, int httpStatusCode) {
        this.mStatus = Status.FAIL;
        this.mError = error;
        this.mErrorExtra = errorExtra;
        this.mHttpStatusCode = httpStatusCode;
    }

    public Status getStatus() {
        return this.mStatus;
    }

    public Error getError() {
        return this.mError;
    }

    public int getHttpStatusCode() {
        return this.mHttpStatusCode;
    }

    public ErrorType getErrorType() {
        if (this.mError != null) {
            return this.mError.type();
        }
        return null;
    }

    public String getErrorText() {
        if (this.mError == null) {
            return ZapConstants.GOOGLE_ACCOUNT_NONE;
        }
        return TextUtils.isEmpty(this.mErrorExtra) ? this.mError.humanReadable() : this.mError.humanReadable() + ": " + this.mErrorExtra;
    }
}
