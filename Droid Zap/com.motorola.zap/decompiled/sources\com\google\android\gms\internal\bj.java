package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;

public final class bj<NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> implements MediationBannerListener, MediationInterstitialListener {
    /* access modifiers changed from: private */
    public final bh mT;

    public bj(bh bhVar) {
        this.mT = bhVar;
    }

    public void onClick(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        da.s("Adapter called onClick.");
        if (!cz.aX()) {
            da.w("onClick must be called on the main UI thread.");
            cz.pT.post(new Runnable() {
                public void run() {
                    try {
                        bj.this.mT.O();
                    } catch (RemoteException e) {
                        da.b("Could not call onAdClicked.", e);
                    }
                }
            });
            return;
        }
        try {
            this.mT.O();
        } catch (RemoteException e) {
            da.b("Could not call onAdClicked.", e);
        }
    }

    public void onDismissScreen(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        da.s("Adapter called onDismissScreen.");
        if (!cz.aX()) {
            da.w("onDismissScreen must be called on the main UI thread.");
            cz.pT.post(new Runnable() {
                public void run() {
                    try {
                        bj.this.mT.onAdClosed();
                    } catch (RemoteException e) {
                        da.b("Could not call onAdClosed.", e);
                    }
                }
            });
            return;
        }
        try {
            this.mT.onAdClosed();
        } catch (RemoteException e) {
            da.b("Could not call onAdClosed.", e);
        }
    }

    public void onDismissScreen(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        da.s("Adapter called onDismissScreen.");
        if (!cz.aX()) {
            da.w("onDismissScreen must be called on the main UI thread.");
            cz.pT.post(new Runnable() {
                public void run() {
                    try {
                        bj.this.mT.onAdClosed();
                    } catch (RemoteException e) {
                        da.b("Could not call onAdClosed.", e);
                    }
                }
            });
            return;
        }
        try {
            this.mT.onAdClosed();
        } catch (RemoteException e) {
            da.b("Could not call onAdClosed.", e);
        }
    }

    public void onFailedToReceiveAd(MediationBannerAdapter<?, ?> mediationBannerAdapter, final ErrorCode errorCode) {
        da.s("Adapter called onFailedToReceiveAd with error. " + errorCode);
        if (!cz.aX()) {
            da.w("onFailedToReceiveAd must be called on the main UI thread.");
            cz.pT.post(new Runnable() {
                public void run() {
                    try {
                        bj.this.mT.onAdFailedToLoad(bk.a(errorCode));
                    } catch (RemoteException e) {
                        da.b("Could not call onAdFailedToLoad.", e);
                    }
                }
            });
            return;
        }
        try {
            this.mT.onAdFailedToLoad(bk.a(errorCode));
        } catch (RemoteException e) {
            da.b("Could not call onAdFailedToLoad.", e);
        }
    }

    public void onFailedToReceiveAd(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter, final ErrorCode errorCode) {
        da.s("Adapter called onFailedToReceiveAd with error " + errorCode + ".");
        if (!cz.aX()) {
            da.w("onFailedToReceiveAd must be called on the main UI thread.");
            cz.pT.post(new Runnable() {
                public void run() {
                    try {
                        bj.this.mT.onAdFailedToLoad(bk.a(errorCode));
                    } catch (RemoteException e) {
                        da.b("Could not call onAdFailedToLoad.", e);
                    }
                }
            });
            return;
        }
        try {
            this.mT.onAdFailedToLoad(bk.a(errorCode));
        } catch (RemoteException e) {
            da.b("Could not call onAdFailedToLoad.", e);
        }
    }

    public void onLeaveApplication(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        da.s("Adapter called onLeaveApplication.");
        if (!cz.aX()) {
            da.w("onLeaveApplication must be called on the main UI thread.");
            cz.pT.post(new Runnable() {
                public void run() {
                    try {
                        bj.this.mT.onAdLeftApplication();
                    } catch (RemoteException e) {
                        da.b("Could not call onAdLeftApplication.", e);
                    }
                }
            });
            return;
        }
        try {
            this.mT.onAdLeftApplication();
        } catch (RemoteException e) {
            da.b("Could not call onAdLeftApplication.", e);
        }
    }

    public void onLeaveApplication(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        da.s("Adapter called onLeaveApplication.");
        if (!cz.aX()) {
            da.w("onLeaveApplication must be called on the main UI thread.");
            cz.pT.post(new Runnable() {
                public void run() {
                    try {
                        bj.this.mT.onAdLeftApplication();
                    } catch (RemoteException e) {
                        da.b("Could not call onAdLeftApplication.", e);
                    }
                }
            });
            return;
        }
        try {
            this.mT.onAdLeftApplication();
        } catch (RemoteException e) {
            da.b("Could not call onAdLeftApplication.", e);
        }
    }

    public void onPresentScreen(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        da.s("Adapter called onPresentScreen.");
        if (!cz.aX()) {
            da.w("onPresentScreen must be called on the main UI thread.");
            cz.pT.post(new Runnable() {
                public void run() {
                    try {
                        bj.this.mT.onAdOpened();
                    } catch (RemoteException e) {
                        da.b("Could not call onAdOpened.", e);
                    }
                }
            });
            return;
        }
        try {
            this.mT.onAdOpened();
        } catch (RemoteException e) {
            da.b("Could not call onAdOpened.", e);
        }
    }

    public void onPresentScreen(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        da.s("Adapter called onPresentScreen.");
        if (!cz.aX()) {
            da.w("onPresentScreen must be called on the main UI thread.");
            cz.pT.post(new Runnable() {
                public void run() {
                    try {
                        bj.this.mT.onAdOpened();
                    } catch (RemoteException e) {
                        da.b("Could not call onAdOpened.", e);
                    }
                }
            });
            return;
        }
        try {
            this.mT.onAdOpened();
        } catch (RemoteException e) {
            da.b("Could not call onAdOpened.", e);
        }
    }

    public void onReceivedAd(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        da.s("Adapter called onReceivedAd.");
        if (!cz.aX()) {
            da.w("onReceivedAd must be called on the main UI thread.");
            cz.pT.post(new Runnable() {
                public void run() {
                    try {
                        bj.this.mT.onAdLoaded();
                    } catch (RemoteException e) {
                        da.b("Could not call onAdLoaded.", e);
                    }
                }
            });
            return;
        }
        try {
            this.mT.onAdLoaded();
        } catch (RemoteException e) {
            da.b("Could not call onAdLoaded.", e);
        }
    }

    public void onReceivedAd(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        da.s("Adapter called onReceivedAd.");
        if (!cz.aX()) {
            da.w("onReceivedAd must be called on the main UI thread.");
            cz.pT.post(new Runnable() {
                public void run() {
                    try {
                        bj.this.mT.onAdLoaded();
                    } catch (RemoteException e) {
                        da.b("Could not call onAdLoaded.", e);
                    }
                }
            });
            return;
        }
        try {
            this.mT.onAdLoaded();
        } catch (RemoteException e) {
            da.b("Could not call onAdLoaded.", e);
        }
    }
}
