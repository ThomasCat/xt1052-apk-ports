package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;

public abstract class cb extends ct {
    private final cd mf;
    private final com.google.android.gms.internal.ca.a nY;

    public static final class a extends cb {
        private final Context mContext;

        public a(Context context, cd cdVar, com.google.android.gms.internal.ca.a aVar) {
            super(cdVar, aVar);
            this.mContext = context;
        }

        public void aD() {
        }

        public ch aE() {
            return ci.a(this.mContext, new av());
        }
    }

    public static final class b extends cb implements ConnectionCallbacks, OnConnectionFailedListener {
        private final Object mg = new Object();
        private final com.google.android.gms.internal.ca.a nY;
        private final cc nZ;

        public b(Context context, cd cdVar, com.google.android.gms.internal.ca.a aVar) {
            super(cdVar, aVar);
            this.nY = aVar;
            this.nZ = new cc(context, this, this, cdVar.kN.pW);
            this.nZ.connect();
        }

        public void aD() {
            synchronized (this.mg) {
                if (this.nZ.isConnected() || this.nZ.isConnecting()) {
                    this.nZ.disconnect();
                }
            }
        }

        public ch aE() {
            ch chVar;
            synchronized (this.mg) {
                try {
                    chVar = this.nZ.aH();
                } catch (IllegalStateException e) {
                    chVar = null;
                }
            }
            return chVar;
        }

        public void onConnected(Bundle connectionHint) {
            start();
        }

        public void onConnectionFailed(ConnectionResult result) {
            this.nY.a(new cf(0));
        }

        public void onDisconnected() {
            da.s("Disconnected from remote ad request service.");
        }
    }

    public cb(cd cdVar, com.google.android.gms.internal.ca.a aVar) {
        this.mf = cdVar;
        this.nY = aVar;
    }

    private static cf a(ch chVar, cd cdVar) {
        try {
            return chVar.b(cdVar);
        } catch (RemoteException e) {
            da.b("Could not fetch ad response from ad request service.", e);
            return null;
        }
    }

    /* JADX INFO: finally extract failed */
    public final void aB() {
        cf a2;
        try {
            ch aE = aE();
            if (aE == null) {
                a2 = new cf(0);
            } else {
                a2 = a(aE, this.mf);
                if (a2 == null) {
                    a2 = new cf(0);
                }
            }
            aD();
            this.nY.a(a2);
        } catch (Throwable th) {
            aD();
            throw th;
        }
    }

    public abstract void aD();

    public abstract ch aE();

    public final void onStop() {
        aD();
    }
}
