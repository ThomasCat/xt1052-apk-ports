package com.motorola.gcsp.json;

import java.util.List;

public class StartShareList implements InputSet {
    public String appId;
    public AppSetting appSetting;
    public String appType;
    public Integer appVersion;
    public long clientTime;
    public List<ContentMetaData> contentMetaData;
    public String contentUploadRule;
    public DeviceInfo deviceInfo;
    public String fromDeviceId;
    public String fromEmail;
    public String fromUserId;
    public P2PCred p2pCred;
    public Integer settingsVersion;
    public Integer shareCount;
    public ShareFilter shareFilter;
    public ShareMeta shareMeta;
    public Target target;
    public String targetType;
}
