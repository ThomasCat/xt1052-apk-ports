package com.motorola.zap.ui;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.widget.ListView;
import com.motorola.zap.LogUtils;
import com.motorola.zap.ui.ZapGestureTouchListener.AnimationCallback;
import com.motorola.zap.ui.ZapGestureTouchListener.ZapGestureListener;

public class CustomViewPager extends ViewPager {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private static final String TAG = CustomViewPager.class.getSimpleName();
    float mLastTouchX = -1.0f;
    float mLastTouchY = -1.0f;
    ListView mListView = null;
    private int mMaximumFlingVelocity;
    private int mMinimumFlingVelocity;
    private VelocityTracker mVelocityTracker;
    ZapGestureTouchListener mZapGestureTouchListener = null;

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomViewPager(Context context) {
        super(context);
        init();
    }

    private void init() {
        ViewConfiguration configuration = ViewConfiguration.get(getContext());
        this.mMinimumFlingVelocity = 5000;
        this.mMaximumFlingVelocity = configuration.getScaledMaximumFlingVelocity();
    }

    public void setAnimators(ListView listview, AnimationCallback dragZapAnimator, ZapGestureListener gestureDetector) {
        this.mListView = listview;
        this.mZapGestureTouchListener = new ViewGroupZapGestureTouchListener(getContext());
        this.mZapGestureTouchListener.registerAnimationCallback(dragZapAnimator);
        this.mZapGestureTouchListener.registerZapGestureListener(gestureDetector);
        setOnTouchListener(this.mZapGestureTouchListener);
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        MotionEventCompat.getActionMasked(event);
        return true;
    }

    public boolean onTouchEvent(MotionEvent event) {
        try {
            int action = MotionEventCompat.getActionMasked(event);
            if (event.getPointerCount() >= 2) {
                if (this.mListView != null) {
                    this.mListView.requestDisallowInterceptTouchEvent(true);
                }
                return true;
            }
            PagerAdapter adapter = getAdapter();
            if (adapter == null || adapter.getCount() == 0) {
                return false;
            }
            if (action == 0) {
                if (this.mVelocityTracker == null) {
                    this.mVelocityTracker = VelocityTracker.obtain();
                } else {
                    this.mVelocityTracker.clear();
                }
                this.mVelocityTracker.addMovement(event);
            }
            if (action == 2) {
                this.mVelocityTracker.addMovement(event);
            }
            if (action == 1) {
                if (this.mListView != null) {
                    this.mListView.requestDisallowInterceptTouchEvent(false);
                }
                if (Math.abs(event.getX() - this.mLastTouchX) >= 5.0f || Math.abs(event.getY() - this.mLastTouchY) >= 5.0f) {
                    VelocityTracker velocityTracker = this.mVelocityTracker;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.mMaximumFlingVelocity);
                    int initialVelocityX = (int) velocityTracker.getXVelocity();
                    if (Math.abs(initialVelocityX) > this.mMinimumFlingVelocity) {
                        int totalCount = getAdapter().getCount();
                        int currentItem = getCurrentItem();
                        if (initialVelocityX < 0) {
                            int cardsToFling = (int) (2.0f + ((((float) (Math.abs(initialVelocityX) * (((totalCount - currentItem) - 1) - 2))) * 1.0f) / ((float) this.mMaximumFlingVelocity)));
                            if (DBG) {
                                Log.i(TAG, "currentItem =" + currentItem + "/" + totalCount + "CardToFling =" + cardsToFling + " MaxVel =" + this.mMaximumFlingVelocity + "MinVel =" + this.mMinimumFlingVelocity + " currentVel =" + initialVelocityX);
                            }
                            if (cardsToFling < totalCount) {
                                setCurrentItem(currentItem + cardsToFling, true);
                            } else {
                                setCurrentItem(totalCount - 1, true);
                            }
                        } else {
                            int cardsToFling2 = (int) (((float) currentItem) - ((((float) (Math.abs(initialVelocityX) * currentItem)) * 1.0f) / ((float) this.mMaximumFlingVelocity)));
                            if (DBG) {
                                Log.i(TAG, "currentItem =" + currentItem + "/" + totalCount + "CardToFling =" + cardsToFling2 + " MaxVel =" + this.mMaximumFlingVelocity + "MinVel =" + this.mMinimumFlingVelocity + " currentVel =" + initialVelocityX);
                            }
                            if (cardsToFling2 >= 0) {
                                setCurrentItem(cardsToFling2, true);
                            } else {
                                setCurrentItem(0, true);
                            }
                        }
                        this.mVelocityTracker.recycle();
                        this.mVelocityTracker = null;
                        return true;
                    }
                } else {
                    if (DBG) {
                        Log.i(TAG, "Invoke Perform click");
                    }
                    performClick();
                    return true;
                }
            }
            if (action == 3) {
                this.mVelocityTracker.recycle();
                this.mVelocityTracker = null;
            }
            return super.onTouchEvent(event);
        } catch (Exception e) {
            return true;
        }
    }

    public boolean dispatchTouchEvent(MotionEvent event) {
        int action = MotionEventCompat.getActionMasked(event);
        if (action == 0) {
            this.mLastTouchX = event.getX();
            this.mLastTouchY = event.getY();
        }
        if (event.getPointerCount() <= 1 && action == 2) {
            Math.abs(event.getX() - this.mLastTouchX);
            float xDeviation = Math.abs(event.getX() - this.mLastTouchX);
            if (Math.abs(event.getY() - this.mLastTouchY) > 5.0f && xDeviation < 10.0f) {
                if (this.mListView != null) {
                    this.mListView.requestDisallowInterceptTouchEvent(false);
                }
                if (this.mZapGestureTouchListener != null) {
                    this.mZapGestureTouchListener.onTouch(this, event);
                }
            } else if (this.mListView != null) {
                this.mListView.requestDisallowInterceptTouchEvent(true);
            }
        } else if (this.mZapGestureTouchListener != null) {
            this.mZapGestureTouchListener.onTouch(this, event);
        }
        return onTouchEvent(event);
    }
}
