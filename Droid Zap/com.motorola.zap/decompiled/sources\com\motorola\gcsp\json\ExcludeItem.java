package com.motorola.gcsp.json;

public class ExcludeItem implements InputSet {
    public final boolean isDownload;
    public final String shareId;

    public ExcludeItem(String shareId2, boolean isDownload2) {
        this.shareId = shareId2;
        this.isDownload = isDownload2;
    }
}
