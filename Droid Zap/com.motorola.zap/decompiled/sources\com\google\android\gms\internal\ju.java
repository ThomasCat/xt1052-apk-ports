package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;

public class ju implements Creator<jt> {
    static void a(jt jtVar, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.c(parcel, 1, jtVar.getVersionCode());
        b.a(parcel, 2, jtVar.ZN, false);
        b.a(parcel, 3, jtVar.description, false);
        b.D(parcel, p);
    }

    /* renamed from: bm */
    public jt createFromParcel(Parcel parcel) {
        String str = null;
        int o = a.o(parcel);
        int i = 0;
        String str2 = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    i = a.g(parcel, n);
                    break;
                case 2:
                    str2 = a.m(parcel, n);
                    break;
                case 3:
                    str = a.m(parcel, n);
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new jt(i, str2, str);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }

    /* renamed from: cs */
    public jt[] newArray(int i) {
        return new jt[i];
    }
}
