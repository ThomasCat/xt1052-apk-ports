package com.motorola.sharing;

import com.motorola.sharing.ContentListener.Event;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.util.Logger;

public interface ContentDownloadListener extends ContentListener {

    public static class ContentDownloadEvent extends Event {
        protected static final String TAG = Logger.getGcspTag("ContDownEvent");
        public ContentContainer container;
        public Endpoint endpoint;
        public Substatus substatus;

        public enum Substatus {
            PROGRESS,
            NOT_READY_FOR_TOO_LONG,
            ESTIMATED_TRANSFER_TIME_TOO_LONG,
            TRANSFER_TAKING_TOO_LONG,
            AVAILABILITY_TIMEOUT,
            TRANSFER_TIMEOUT,
            STORAGE_FULL,
            SENDER_CANCELED,
            AUTH,
            GENERAL,
            NETWORK,
            CANCELED,
            DOWNLOADED
        }

        public ContentDownloadEvent(Status status, Substatus substatus2, ContentContainer container2, Endpoint endpoint2) {
            this.status = status;
            this.substatus = substatus2;
            this.container = container2;
            this.endpoint = endpoint2;
        }
    }

    void onContentDownloadEvent(ContentDownloadEvent contentDownloadEvent);
}
