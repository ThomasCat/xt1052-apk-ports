package com.motorola.gcsp.json;

import java.util.List;
import java.util.Map;

public class GetTargetInfoResponseList implements InputSet {
    public String memberType;
    public String message;
    public ShareFilter shareFilter;
    public String status;
    public String targetId;
    public String targetMemberStatus;
    public List<Map<String, String>> targetMembers;
    public TargetMeta targetMeta;
    public String targetType;
}
