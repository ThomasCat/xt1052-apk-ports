package com.motorola.sharing.provider.cloud;

import android.text.TextUtils;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentContainer.DiscoveryParams;
import com.motorola.sharing.ContentContainer.UploadRule;
import com.motorola.sharing.ContentDiscoveryListener;
import com.motorola.sharing.ContentDiscoveryListener.ContentDiscoveryEvent.Substatus;
import com.motorola.sharing.ContentDownloadListener;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.ContentShareListener;
import com.motorola.sharing.ContentShareListener.ContentShareEvent;
import com.motorola.sharing.Endpoint;
import com.motorola.sharing.Endpoint.AttrValue;
import com.motorola.sharing.Endpoint.Attributes;
import com.motorola.sharing.EndpointDiscoveryListener;
import com.motorola.sharing.EndpointDiscoveryListener.EndpointDiscoveryEvent;
import com.motorola.sharing.ProviderApi;
import com.motorola.sharing.ReportAbuseListener;
import com.motorola.sharing.provider.cloud.api.CloudEndpoint;
import com.motorola.sharing.util.Logger;

public class CloudProvider implements ProviderApi {
    private static final String TAG = Logger.getCloudTag("Provider");
    private static CloudProvider sProvider = null;
    private CloudEndpoint mEndpoint = null;

    public static synchronized CloudProvider getInstance() {
        CloudProvider cloudProvider;
        synchronized (CloudProvider.class) {
            if (sProvider == null) {
                sProvider = new CloudProvider();
            }
            cloudProvider = sProvider;
        }
        return cloudProvider;
    }

    private CloudProvider() {
    }

    public void discoverContent(DiscoveryParams params, ContentDiscoveryListener listener, Endpoint[] endpoints) {
        Logger.d(TAG, "discoverContent(): " + params.getPrintableSignature());
        new ContentDiscovery(params).start();
    }

    public void stopDiscovery(DiscoveryParams params) {
        Logger.d(TAG, "stopDiscovery(): " + params.getPrintableSignature());
        if (!CloudData.isOperationCanceled(params)) {
            CloudData.getExtras(params).isOperationCanceled = true;
            params.onContentDiscoveryEvent(Status.SUCCESS, Substatus.CANCELLED, getEndpoint());
        }
    }

    public void downloadContent(ContentContainer container, ContentDownloadListener listener) {
        Logger.d(TAG, "downloadContent(): " + container.getPrintableSignature());
        if (TextUtils.isEmpty(container.origin) || TextUtils.isEmpty(container.shareId) || TextUtils.isEmpty(container.url)) {
            throw new IllegalArgumentException("container doesn't have required params");
        }
        ContentDownloadManager.getInstance().download(container);
    }

    public void cancelDownload(ContentContainer container) {
        Logger.d(TAG, "cancelDownload(): " + container.getPrintableSignature());
        if (!CloudData.isOperationCanceled(container)) {
            ContentDownloadManager.getInstance().cancelDownload(container);
        }
    }

    public void reportAbuse(ContentContainer container, ReportAbuseListener listener) {
        Logger.d(TAG, "reportAbuse(): " + container.getPrintableSignature());
        if (TextUtils.isEmpty(container.origin) || TextUtils.isEmpty(container.shareId)) {
            throw new IllegalArgumentException("container doesn't have required params");
        }
        new AbuseReport(container).send();
    }

    public void shareContent(ContentContainer container, ContentShareListener listener, Endpoint[] endpoints) {
        Logger.d(TAG, "shareContent(): " + container.getPrintableSignature());
        if (!TextUtils.isEmpty(container.shareId)) {
            throw new IllegalArgumentException("container has share id which is not allowed");
        } else if (container.uploadRule == null) {
            throw new IllegalArgumentException("upload rule is not specified");
        } else {
            ContentShareManager.getInstance().startSharing(container);
        }
    }

    public void uploadContent(ContentContainer container, ContentShareListener listener) {
        Logger.d(TAG, "uploadContent(): " + container.getPrintableSignature());
        if (TextUtils.isEmpty(container.shareId) || TextUtils.isEmpty(container.url)) {
            throw new IllegalStateException("container doesn't have required params");
        } else if (container.uploadRule == UploadRule.ALWAYS || container.uploadRule == UploadRule.ON_DEMAND) {
            ContentShareManager.getInstance().uploadContent(container);
        } else {
            throw new IllegalArgumentException("upload rule is " + container.uploadRule);
        }
    }

    public void stopSharing(ContentContainer container) {
        Logger.d(TAG, "stopSharing(): " + container.getPrintableSignature());
        ContentShareManager.getInstance().stopSharing(container);
    }

    public void stopSharing(String shareId, ContentShareListener listener) {
        Logger.d(TAG, "stopSharing(): sid" + shareId);
        throw new UnsupportedOperationException("stopShare with container must be called");
    }

    public void cancelSharing(ContentContainer container) {
        Logger.d(TAG, "cancelSharing(): " + container.getPrintableSignature());
    }

    public void resumeSharing(ContentContainer container, ContentShareListener listener) {
        Logger.d(TAG, "resumeSharing(): " + container.getPrintableSignature());
        container.onContentShareEvent(Status.SUCCESS, ContentShareEvent.Substatus.RESUMED, getEndpoint());
    }

    public void discoverEndpoints(Attributes attrs, EndpointDiscoveryListener listener) {
        Logger.d(TAG, "discoverEndpoints()");
        if (attrs.contains(AttrValue.CLOUD)) {
            listener.sendEvent(new EndpointDiscoveryEvent(EndpointDiscoveryEvent.Status.INFO, EndpointDiscoveryEvent.Substatus.DISCOVERED, this, new CloudEndpoint()));
        }
        listener.sendEvent(new EndpointDiscoveryEvent(EndpointDiscoveryEvent.Status.SUCCESS, EndpointDiscoveryEvent.Substatus.FINISHED, this));
    }

    public void stopEndpointDiscovery(EndpointDiscoveryListener listener) {
        Logger.d(TAG, "stopEndpointDiscovery()");
        listener.sendEvent(new EndpointDiscoveryEvent(EndpointDiscoveryEvent.Status.SUCCESS, EndpointDiscoveryEvent.Substatus.STOPPED, this));
    }

    public synchronized Endpoint getEndpoint() {
        if (this.mEndpoint == null) {
            this.mEndpoint = new CloudEndpoint();
        }
        return this.mEndpoint;
    }

    public Endpoint[] getStaticEndpoints() {
        return new Endpoint[]{getEndpoint()};
    }
}
