package com.motorola.sharing;

import com.motorola.sharing.ContentListener.Event;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.util.Logger;

public interface ReportAbuseListener extends ContentListener {

    public static class ReportAbuseEvent extends Event {
        protected static final String TAG = Logger.getGcspTag("RptAbuseEvent");
        public ContentContainer container;
        public Endpoint endpoint;
        public Substatus substatus;

        public enum Substatus {
            AUTH,
            GENERAL,
            NETWORK,
            SENT
        }

        ReportAbuseEvent(Status status, Substatus substatus2, ContentContainer container2, Endpoint endpoint2) {
            this.status = status;
            this.substatus = substatus2;
            this.container = container2;
            this.endpoint = endpoint2;
        }
    }

    void onReportAbuseEvent(ReportAbuseEvent reportAbuseEvent);
}
