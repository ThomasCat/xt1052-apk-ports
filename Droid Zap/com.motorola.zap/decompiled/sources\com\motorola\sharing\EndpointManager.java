package com.motorola.sharing;

import com.motorola.sharing.Endpoint.Attributes;
import com.motorola.sharing.EndpointDiscoveryListener.EndpointDiscoveryEvent;
import com.motorola.sharing.EndpointDiscoveryListener.EndpointDiscoveryEvent.Status;
import com.motorola.sharing.EndpointDiscoveryListener.EndpointDiscoveryEvent.Substatus;
import com.motorola.sharing.util.Logger;
import com.motorola.sharing.util.PackageUtils;
import java.util.HashSet;
import java.util.Set;

public class EndpointManager {
    protected static final String TAG = Logger.getGcspTag("EndpointManager");
    /* access modifiers changed from: private */
    public EndpointDiscoveryListener mClientListener = null;
    /* access modifiers changed from: private */
    public Set<Endpoint> mDiscoveredEndpoints = new HashSet();
    /* access modifiers changed from: private */
    public boolean mDiscoveryInProgress = false;
    private EndpointDiscoveryListener mManagerListener = new EndpointDiscoveryListener() {
        public void onEndpointDiscoveryEvent(EndpointDiscoveryEvent event) {
            switch (AnonymousClass2.$SwitchMap$com$motorola$sharing$EndpointDiscoveryListener$EndpointDiscoveryEvent$Status[event.status.ordinal()]) {
                case 1:
                    switch (AnonymousClass2.$SwitchMap$com$motorola$sharing$EndpointDiscoveryListener$EndpointDiscoveryEvent$Substatus[event.substatus.ordinal()]) {
                        case 1:
                        case 2:
                            EndpointManager.this.mProvidersInProgress.remove(event.provider);
                            if (EndpointManager.this.mProvidersInProgress.size() == 0) {
                                EndpointManager.this.mClientListener.sendEvent(event);
                                EndpointManager.this.mDiscoveryInProgress = false;
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                case 2:
                    switch (AnonymousClass2.$SwitchMap$com$motorola$sharing$EndpointDiscoveryListener$EndpointDiscoveryEvent$Substatus[event.substatus.ordinal()]) {
                        case 3:
                            EndpointManager.this.mDiscoveredEndpoints.add(event.endpoint);
                            EndpointManager.this.mClientListener.sendEvent(event);
                            return;
                        default:
                            return;
                    }
                case 3:
                    switch (AnonymousClass2.$SwitchMap$com$motorola$sharing$EndpointDiscoveryListener$EndpointDiscoveryEvent$Substatus[event.substatus.ordinal()]) {
                        case 4:
                        case 5:
                            EndpointManager.this.mClientListener.sendEvent(event);
                            return;
                        default:
                            return;
                    }
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Set<ProviderApi> mProvidersInProgress = new HashSet();

    /* renamed from: com.motorola.sharing.EndpointManager$2 reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$com$motorola$sharing$EndpointDiscoveryListener$EndpointDiscoveryEvent$Status = new int[Status.values().length];
        static final /* synthetic */ int[] $SwitchMap$com$motorola$sharing$EndpointDiscoveryListener$EndpointDiscoveryEvent$Substatus = new int[Substatus.values().length];

        static {
            try {
                $SwitchMap$com$motorola$sharing$EndpointDiscoveryListener$EndpointDiscoveryEvent$Status[Status.SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$motorola$sharing$EndpointDiscoveryListener$EndpointDiscoveryEvent$Status[Status.INFO.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$motorola$sharing$EndpointDiscoveryListener$EndpointDiscoveryEvent$Status[Status.ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$motorola$sharing$EndpointDiscoveryListener$EndpointDiscoveryEvent$Substatus[Substatus.STOPPED.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$motorola$sharing$EndpointDiscoveryListener$EndpointDiscoveryEvent$Substatus[Substatus.FINISHED.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$motorola$sharing$EndpointDiscoveryListener$EndpointDiscoveryEvent$Substatus[Substatus.DISCOVERED.ordinal()] = 3;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$motorola$sharing$EndpointDiscoveryListener$EndpointDiscoveryEvent$Substatus[Substatus.GENERAL.ordinal()] = 4;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$motorola$sharing$EndpointDiscoveryListener$EndpointDiscoveryEvent$Substatus[Substatus.NETWORK.ordinal()] = 5;
            } catch (NoSuchFieldError e8) {
            }
        }
    }

    EndpointManager() {
    }

    public boolean isDiscoveryInProgress() {
        return this.mDiscoveryInProgress;
    }

    public void discoverEndpoints(Attributes attrs, EndpointDiscoveryListener listener) {
        ProviderApi[] arr$;
        if (this.mDiscoveryInProgress) {
            throw new IllegalStateException("Discovery is in progress");
        }
        this.mDiscoveredEndpoints.clear();
        this.mClientListener = listener;
        this.mDiscoveryInProgress = true;
        for (ProviderApi provider : PackageUtils.getProviders()) {
            this.mProvidersInProgress.add(provider);
            provider.discoverEndpoints(attrs, this.mManagerListener);
        }
    }

    public void stopDiscovery() {
        for (ProviderApi provider : this.mProvidersInProgress) {
            provider.stopEndpointDiscovery(this.mManagerListener);
        }
    }

    public Endpoint[] getEndpoints() {
        return (Endpoint[]) this.mDiscoveredEndpoints.toArray(new Endpoint[this.mDiscoveredEndpoints.size()]);
    }
}
