package com.motorola.zap.provider;

import android.net.Uri;

public class ZapContract {
    public static final String AUTHORITY = "com.motorola.zap";
    public static final Uri CONTENT_URI = Uri.parse("content://com.motorola.zap");

    public static final class History {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(ZapContract.CONTENT_URI, URI_PATH);
        public static final String INVERSE_CHRONOLOGICAL_ORDER = "rcv_timestamp DESC";
        public static final String MIME_TYPE = "vnd.android.cursor.dir/vnd.com.motorola.zap.provider.history";
        public static final String URI_PATH = "history";

        public static final class Columns {
            public static final String[] DEFAULT_PROJECTION = {HistoryHandler.HIST_ID, HistoryHandler.HIST_FILEPATH, HistoryHandler.HIST_SHAREID, HistoryHandler.HIST_FROM, HistoryHandler.HIST_ACCOUNT_COLUMN, HistoryHandler.HIST_TIMESTAMP, HistoryHandler.HIST_LATITUDE, HistoryHandler.HIST_LONGITUDE, HistoryHandler.HIST_CITY, HistoryHandler.HIST_STATE, HistoryHandler.HIST_COUNTRY, HistoryHandler.HIST_THUMBNAIL, HistoryHandler.HIST_ZZ_NAME, HistoryHandler.HIST_ACTION, HistoryHandler.HIST_UID, HistoryHandler.HIST_IS_TRANSFER_IN_PROGRESS, HistoryHandler.HIST_RESHARE_UID, HistoryHandler.HIST_ZZ_OWNER, HistoryHandler.HIST_ZZ_ID, HistoryHandler.HIST_IS_ZZ_ACTIVE, HistoryHandler.HIST_IS_ABUSE_REPORTED, HistoryHandler.HIST_IS_DELETED, HistoryHandler.HIST_IS_TRANSFER_ACKED, HistoryHandler.HIST_IS_PIN_LOCKED, HistoryHandler.HIST_PIN, HistoryHandler.HIST_MEDIA_EXPIRATION_TIMESTAMP, HistoryHandler.HIST_TRANSFER_COMPLETE_STATUS, HistoryHandler.HIST_MEDIA_TYPE, HistoryHandler.HIST_IS_LEFT_ZZ, HistoryHandler.HIST_PROF_PIC, HistoryHandler.HIST_PROF_NAME, HistoryHandler.HIST_PROF_USERID, HistoryHandler.HIST_PROF_ANONYMOUS};
        }

        public static final class Timeline {
            public static final String FULL_SELECTION = String.format("(%s) OR (%s = '%s')", new Object[]{SELECTION_WITH_ZZ_INVITE, HistoryHandler.HIST_ACTION, HistoryHandler.ACTION_DEMO_CARD});
            public static final String SELECTION = String.format("(%s = '%s' AND %s IS NOT NULL AND %s != '%s') OR ((%s = '%s' OR %s = '%s') AND ((%s IS NULL AND %s IS NULL) OR %s = '%s' OR %s = '%s'))", new Object[]{HistoryHandler.HIST_ACTION, HistoryHandler.ACTION_ZZ_SESSION, HistoryHandler.HIST_FILEPATH, HistoryHandler.HIST_IS_ZZ_ACTIVE, "1", HistoryHandler.HIST_ACTION, HistoryHandler.ACTION_MEDIA_RECEIVE, HistoryHandler.HIST_ACTION, HistoryHandler.ACTION_MEDIA_SEND, HistoryHandler.HIST_ZZ_OWNER, HistoryHandler.HIST_ZZ_ID, HistoryHandler.HIST_IS_ZZ_ACTIVE, "1", HistoryHandler.HIST_IS_TRANSFER_IN_PROGRESS, "1"});
            public static final String SELECTION_ONLY_IMAGES = String.format("(%s) AND (%s = '%s')", new Object[]{FULL_SELECTION, HistoryHandler.HIST_MEDIA_TYPE, Integer.valueOf(1)});
            public static final String SELECTION_ONLY_VIDEOS = String.format("(%s) AND (%s = '%s')", new Object[]{FULL_SELECTION, HistoryHandler.HIST_MEDIA_TYPE, Integer.valueOf(2)});
            public static final String SELECTION_WITH_ZZ_INVITE = String.format("(%s) OR (%s = '%s')", new Object[]{SELECTION, HistoryHandler.HIST_ACTION, HistoryHandler.ACTION_ZZ_INVITE});
        }

        public static final class ZapZoneDetail {
            public static final String SELECTION = String.format("(%s = '%s' OR %s = '%s') AND %s = ? AND %s = ?", new Object[]{HistoryHandler.HIST_ACTION, HistoryHandler.ACTION_MEDIA_RECEIVE, HistoryHandler.HIST_ACTION, HistoryHandler.ACTION_MEDIA_SEND, HistoryHandler.HIST_ZZ_OWNER, HistoryHandler.HIST_ZZ_ID});
        }

        public static final class ZapZones {
            public static final String SELECTION = String.format("(%s = '%s' AND %s IS NOT NULL AND %s != '%s') OR ((%s = '%s' OR %s = '%s') AND %s = '%s')", new Object[]{HistoryHandler.HIST_ACTION, HistoryHandler.ACTION_ZZ_SESSION, HistoryHandler.HIST_FILEPATH, HistoryHandler.HIST_IS_ZZ_ACTIVE, "1", HistoryHandler.HIST_ACTION, HistoryHandler.ACTION_MEDIA_RECEIVE, HistoryHandler.HIST_ACTION, HistoryHandler.ACTION_MEDIA_SEND, HistoryHandler.HIST_IS_ZZ_ACTIVE, "1"});
        }
    }
}
