package com.google.android.gms.cast;

import com.google.android.gms.cast.Cast.MessageReceivedCallback;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.dq;
import com.google.android.gms.internal.dv;
import com.google.android.gms.internal.dw;
import com.google.android.gms.internal.dx;
import java.io.IOException;
import org.json.JSONObject;

public class RemoteMediaPlayer implements MessageReceivedCallback {
    public static final int RESUME_STATE_PAUSE = 2;
    public static final int RESUME_STATE_PLAY = 1;
    public static final int RESUME_STATE_UNCHANGED = 0;
    public static final int STATUS_CANCELED = 2;
    public static final int STATUS_FAILED = 1;
    public static final int STATUS_REPLACED = 4;
    public static final int STATUS_SUCCEEDED = 0;
    public static final int STATUS_TIMED_OUT = 3;
    /* access modifiers changed from: private */
    public final Object mg = new Object();
    /* access modifiers changed from: private */
    public final dv xg = new dv() {
        /* access modifiers changed from: protected */
        public void onMetadataUpdated() {
            RemoteMediaPlayer.this.onMetadataUpdated();
        }

        /* access modifiers changed from: protected */
        public void onStatusUpdated() {
            RemoteMediaPlayer.this.onStatusUpdated();
        }
    };
    /* access modifiers changed from: private */
    public final a xh = new a();
    private OnMetadataUpdatedListener xi;
    private OnStatusUpdatedListener xj;

    public interface MediaChannelResult extends Result {
    }

    public interface OnMetadataUpdatedListener {
        void onMetadataUpdated();
    }

    public interface OnStatusUpdatedListener {
        void onStatusUpdated();
    }

    private class a implements dw {
        private GoogleApiClient xu;
        private long xv = 0;

        /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer$a$a reason: collision with other inner class name */
        private final class C0002a implements ResultCallback<Status> {
            private final long xw;

            C0002a(long j) {
                this.xw = j;
            }

            /* renamed from: i */
            public void onResult(Status status) {
                if (!status.isSuccess()) {
                    RemoteMediaPlayer.this.xg.a(this.xw, status.getStatusCode());
                }
            }
        }

        public a() {
        }

        public void a(String str, String str2, long j, String str3) throws IOException {
            if (this.xu == null) {
                throw new IOException("No GoogleApiClient available");
            }
            Cast.CastApi.sendMessage(this.xu, str, str2).setResultCallback(new C0002a(j));
        }

        public void b(GoogleApiClient googleApiClient) {
            this.xu = googleApiClient;
        }

        public long cV() {
            long j = this.xv + 1;
            this.xv = j;
            return j;
        }
    }

    private static abstract class b extends a<MediaChannelResult> {
        dx xy = new dx() {
            public void a(long j, int i, JSONObject jSONObject) {
                b.this.b(new c(new Status(i), jSONObject));
            }

            public void k(long j) {
                b.this.b(b.this.d(new Status(4)));
            }
        };

        b() {
        }

        /* renamed from: j */
        public MediaChannelResult d(final Status status) {
            return new MediaChannelResult() {
                public Status getStatus() {
                    return status;
                }
            };
        }
    }

    private static final class c implements MediaChannelResult {
        private final Status vl;
        private final JSONObject wP;

        c(Status status, JSONObject jSONObject) {
            this.vl = status;
            this.wP = jSONObject;
        }

        public Status getStatus() {
            return this.vl;
        }
    }

    public RemoteMediaPlayer() {
        this.xg.a(this.xh);
    }

    /* access modifiers changed from: private */
    public void onMetadataUpdated() {
        if (this.xi != null) {
            this.xi.onMetadataUpdated();
        }
    }

    /* access modifiers changed from: private */
    public void onStatusUpdated() {
        if (this.xj != null) {
            this.xj.onStatusUpdated();
        }
    }

    public long getApproximateStreamPosition() {
        long approximateStreamPosition;
        synchronized (this.mg) {
            approximateStreamPosition = this.xg.getApproximateStreamPosition();
        }
        return approximateStreamPosition;
    }

    public MediaInfo getMediaInfo() {
        MediaInfo mediaInfo;
        synchronized (this.mg) {
            mediaInfo = this.xg.getMediaInfo();
        }
        return mediaInfo;
    }

    public MediaStatus getMediaStatus() {
        MediaStatus mediaStatus;
        synchronized (this.mg) {
            mediaStatus = this.xg.getMediaStatus();
        }
        return mediaStatus;
    }

    public String getNamespace() {
        return this.xg.getNamespace();
    }

    public long getStreamDuration() {
        long streamDuration;
        synchronized (this.mg) {
            streamDuration = this.xg.getStreamDuration();
        }
        return streamDuration;
    }

    public PendingResult<MediaChannelResult> load(GoogleApiClient apiClient, MediaInfo mediaInfo) {
        return load(apiClient, mediaInfo, true, 0, null);
    }

    public PendingResult<MediaChannelResult> load(GoogleApiClient apiClient, MediaInfo mediaInfo, boolean autoplay) {
        return load(apiClient, mediaInfo, autoplay, 0, null);
    }

    public PendingResult<MediaChannelResult> load(GoogleApiClient apiClient, MediaInfo mediaInfo, boolean autoplay, long playPosition) {
        return load(apiClient, mediaInfo, autoplay, playPosition, null);
    }

    public PendingResult<MediaChannelResult> load(GoogleApiClient apiClient, MediaInfo mediaInfo, boolean autoplay, long playPosition, JSONObject customData) {
        final GoogleApiClient googleApiClient = apiClient;
        final MediaInfo mediaInfo2 = mediaInfo;
        final boolean z = autoplay;
        final long j = playPosition;
        final JSONObject jSONObject = customData;
        return apiClient.b(new b() {
            /* access modifiers changed from: protected */
            public void a(dq dqVar) {
                synchronized (RemoteMediaPlayer.this.mg) {
                    RemoteMediaPlayer.this.xh.b(googleApiClient);
                    try {
                        RemoteMediaPlayer.this.xg.a(this.xy, mediaInfo2, z, j, jSONObject);
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (IOException e) {
                        b(d(new Status(1)));
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (Throwable th) {
                        RemoteMediaPlayer.this.xh.b(null);
                        throw th;
                    }
                }
            }
        });
    }

    public void onMessageReceived(CastDevice castDevice, String namespace, String message) {
        this.xg.P(message);
    }

    public PendingResult<MediaChannelResult> pause(GoogleApiClient apiClient) {
        return pause(apiClient, null);
    }

    public PendingResult<MediaChannelResult> pause(final GoogleApiClient apiClient, final JSONObject customData) {
        return apiClient.b(new b() {
            /* access modifiers changed from: protected */
            public void a(dq dqVar) {
                synchronized (RemoteMediaPlayer.this.mg) {
                    RemoteMediaPlayer.this.xh.b(apiClient);
                    try {
                        RemoteMediaPlayer.this.xg.a(this.xy, customData);
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (IOException e) {
                        b(d(new Status(1)));
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (Throwable th) {
                        RemoteMediaPlayer.this.xh.b(null);
                        throw th;
                    }
                }
            }
        });
    }

    public PendingResult<MediaChannelResult> play(GoogleApiClient apiClient) {
        return play(apiClient, null);
    }

    public PendingResult<MediaChannelResult> play(final GoogleApiClient apiClient, final JSONObject customData) {
        return apiClient.b(new b() {
            /* access modifiers changed from: protected */
            public void a(dq dqVar) {
                synchronized (RemoteMediaPlayer.this.mg) {
                    RemoteMediaPlayer.this.xh.b(apiClient);
                    try {
                        RemoteMediaPlayer.this.xg.c(this.xy, customData);
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (IOException e) {
                        b(d(new Status(1)));
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (Throwable th) {
                        RemoteMediaPlayer.this.xh.b(null);
                        throw th;
                    }
                }
            }
        });
    }

    public PendingResult<MediaChannelResult> requestStatus(final GoogleApiClient apiClient) {
        return apiClient.b(new b() {
            /* access modifiers changed from: protected */
            public void a(dq dqVar) {
                synchronized (RemoteMediaPlayer.this.mg) {
                    RemoteMediaPlayer.this.xh.b(apiClient);
                    try {
                        RemoteMediaPlayer.this.xg.a(this.xy);
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (IOException e) {
                        b(d(new Status(1)));
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (Throwable th) {
                        RemoteMediaPlayer.this.xh.b(null);
                        throw th;
                    }
                }
            }
        });
    }

    public PendingResult<MediaChannelResult> seek(GoogleApiClient apiClient, long position) {
        return seek(apiClient, position, 0, null);
    }

    public PendingResult<MediaChannelResult> seek(GoogleApiClient apiClient, long position, int resumeState) {
        return seek(apiClient, position, resumeState, null);
    }

    public PendingResult<MediaChannelResult> seek(GoogleApiClient apiClient, long position, int resumeState, JSONObject customData) {
        final GoogleApiClient googleApiClient = apiClient;
        final long j = position;
        final int i = resumeState;
        final JSONObject jSONObject = customData;
        return apiClient.b(new b() {
            /* access modifiers changed from: protected */
            public void a(dq dqVar) {
                synchronized (RemoteMediaPlayer.this.mg) {
                    RemoteMediaPlayer.this.xh.b(googleApiClient);
                    try {
                        RemoteMediaPlayer.this.xg.a(this.xy, j, i, jSONObject);
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (IOException e) {
                        b(d(new Status(1)));
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (Throwable th) {
                        RemoteMediaPlayer.this.xh.b(null);
                        throw th;
                    }
                }
            }
        });
    }

    public void setOnMetadataUpdatedListener(OnMetadataUpdatedListener listener) {
        this.xi = listener;
    }

    public void setOnStatusUpdatedListener(OnStatusUpdatedListener listener) {
        this.xj = listener;
    }

    public PendingResult<MediaChannelResult> setStreamMute(GoogleApiClient apiClient, boolean muteState) {
        return setStreamMute(apiClient, muteState, null);
    }

    public PendingResult<MediaChannelResult> setStreamMute(final GoogleApiClient apiClient, final boolean muteState, final JSONObject customData) {
        return apiClient.b(new b() {
            /* access modifiers changed from: protected */
            public void a(dq dqVar) {
                synchronized (RemoteMediaPlayer.this.mg) {
                    RemoteMediaPlayer.this.xh.b(apiClient);
                    try {
                        RemoteMediaPlayer.this.xg.a(this.xy, muteState, customData);
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (IllegalStateException e) {
                        b(d(new Status(1)));
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (IOException e2) {
                        b(d(new Status(1)));
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (Throwable th) {
                        RemoteMediaPlayer.this.xh.b(null);
                        throw th;
                    }
                }
            }
        });
    }

    public PendingResult<MediaChannelResult> setStreamVolume(GoogleApiClient apiClient, double volume) throws IllegalArgumentException {
        return setStreamVolume(apiClient, volume, null);
    }

    public PendingResult<MediaChannelResult> setStreamVolume(GoogleApiClient apiClient, double volume, JSONObject customData) throws IllegalArgumentException {
        if (Double.isInfinite(volume) || Double.isNaN(volume)) {
            throw new IllegalArgumentException("Volume cannot be " + volume);
        }
        final GoogleApiClient googleApiClient = apiClient;
        final double d = volume;
        final JSONObject jSONObject = customData;
        return apiClient.b(new b() {
            /* access modifiers changed from: protected */
            public void a(dq dqVar) {
                synchronized (RemoteMediaPlayer.this.mg) {
                    RemoteMediaPlayer.this.xh.b(googleApiClient);
                    try {
                        RemoteMediaPlayer.this.xg.a(this.xy, d, jSONObject);
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (IllegalStateException e) {
                        b(d(new Status(1)));
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (IllegalArgumentException e2) {
                        b(d(new Status(1)));
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (IOException e3) {
                        b(d(new Status(1)));
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (Throwable th) {
                        RemoteMediaPlayer.this.xh.b(null);
                        throw th;
                    }
                }
                return;
            }
        });
    }

    public PendingResult<MediaChannelResult> stop(GoogleApiClient apiClient) {
        return stop(apiClient, null);
    }

    public PendingResult<MediaChannelResult> stop(final GoogleApiClient apiClient, final JSONObject customData) {
        return apiClient.b(new b() {
            /* access modifiers changed from: protected */
            public void a(dq dqVar) {
                synchronized (RemoteMediaPlayer.this.mg) {
                    RemoteMediaPlayer.this.xh.b(apiClient);
                    try {
                        RemoteMediaPlayer.this.xg.b(this.xy, customData);
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (IOException e) {
                        b(d(new Status(1)));
                        RemoteMediaPlayer.this.xh.b(null);
                    } catch (Throwable th) {
                        RemoteMediaPlayer.this.xh.b(null);
                        throw th;
                    }
                }
            }
        });
    }
}
