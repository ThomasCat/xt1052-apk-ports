package com.motorola.zap.zz.ui;

import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener.GroupEvent;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.ui.ZapUi;

public class ZzCreateUiHandler extends BaseZzUiHandler {
    /* access modifiers changed from: protected */
    public void handleInfo(GroupEvent ge) {
        ZapUi.vibrate(300);
        ZapUi.playSendingSound();
    }

    /* access modifiers changed from: protected */
    public void handleOk(GroupEvent ge) {
        if (!((Boolean) Z.FEATURE_ENABLED.read()).booleanValue()) {
            ZapUi.getInstance().showToastMessage(getString(R.string.toast_error_app_disabled), ZapUi.getErrorToastIconId(), true, true);
            GCSP.getGroupManager().leave(null);
            return;
        }
        showZzActiveNotification();
        ZapUi.getInstance().showDataWarningDialog();
    }

    /* access modifiers changed from: protected */
    public void handleError(GroupEvent ge) {
        if (!handleCommonError(ge)) {
            ZapUi.getInstance().showToastMessage(getString(R.string.toast_error_zz_creating), ZapUi.getErrorToastIconId(), true, true);
        }
    }
}
