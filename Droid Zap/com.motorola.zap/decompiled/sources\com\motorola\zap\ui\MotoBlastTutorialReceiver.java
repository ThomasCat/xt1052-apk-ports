package com.motorola.zap.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.drive.DriveFile;
import com.motorola.zap.ZapConfig;
import com.motorola.zap.app.App;
import com.motorola.zap.ui.tutorial.TutorialActivity;

public class MotoBlastTutorialReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (ZapConfig.supportsFullFeature()) {
            Intent intent2 = new Intent(App.getAppContext(), TutorialActivity.class);
            intent2.putExtra(TutorialActivity.EXTRA_DB_MEDIA_SHARED, intent.getStringExtra(TutorialActivity.EXTRA_DB_MEDIA_SHARED));
            intent2.setFlags(DriveFile.MODE_READ_ONLY);
            context.startActivity(intent2);
        }
    }
}
