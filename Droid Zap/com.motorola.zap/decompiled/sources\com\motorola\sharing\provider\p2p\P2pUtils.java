package com.motorola.sharing.provider.p2p;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.motorola.sharing.ContentContainer;
import java.io.File;

public class P2pUtils {
    private static ConnectivityManager getConnManager(Context context) {
        return (ConnectivityManager) context.getSystemService("connectivity");
    }

    public static boolean isWifiConnected(Context context) {
        NetworkInfo wifi = getConnManager(context).getNetworkInfo(1);
        if (wifi == null || !wifi.isConnected()) {
            return false;
        }
        return true;
    }

    public static boolean isConnected(Context context) {
        NetworkInfo info = getConnManager(context).getActiveNetworkInfo();
        return info != null && info.isConnectedOrConnecting();
    }

    public static long contentSize(ContentContainer container) {
        if (container.path == null) {
            throw new IllegalArgumentException("No file is specified");
        } else if (container.size != 0) {
            return (long) container.size;
        } else {
            return new File(container.path).length();
        }
    }
}
