package com.google.sample.castcompanionlibrary.cast.dialog.video;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.MediaRouteControllerDialogFragment;

public class VideoMediaRouteControllerDialogFragment extends MediaRouteControllerDialogFragment {
    private VideoMediaRouteControllerDialog mCustomControllerDialog;

    public VideoMediaRouteControllerDialog onCreateControllerDialog(Context context, Bundle savedInstanceState) {
        this.mCustomControllerDialog = new VideoMediaRouteControllerDialog(context);
        this.mCustomControllerDialog.setVolumeControlEnabled(false);
        return this.mCustomControllerDialog;
    }
}
