package com.motorola.zap.ui;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.widget.AbsListView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.motorola.zap.LogUtils;
import java.util.ArrayList;
import java.util.List;

public class ZapGestureTouchListener implements OnTouchListener {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private static final double SWIPE_ANGLE_LIMIT_RADIANS = Math.toRadians(15.0d);
    private final List<AnimationCallback> mAnimationCallbackList;
    Direction mDirection;
    private PointF mDownPoint1;
    private PointF mDownPoint2;
    private int mDownPointerId1;
    private int mDownPointerId2;
    private int mDownPosition;
    private boolean mIsValidZap;
    private AbsListView mListView;
    private boolean mPaused;
    private int mPointerCount = 0;
    private int mScreenWidth = 0;
    private final int mSlop;
    private boolean mSwiping;
    private VelocityTracker mVelocityTracker;
    private final List<ZapGestureListener> mZapGestureListenerList;
    private View mZapListItem;
    private View mZapView;

    public interface AnimationCallback {
        void animateOnZapGestureCancel(View view, View view2);

        void animateOnZapGestureComplete(View view, View view2, boolean z, float f, OnAnimationEndListener onAnimationEndListener);

        void animateOnZapGestureInit(View view, View view2, PointF pointF);

        void animateOnZapGestureMove(View view, View view2, boolean z, PointF pointF, PointF pointF2);

        void animateOnZapGestureStart(View view, View view2, PointF pointF, PointF pointF2);
    }

    enum Direction {
        NONE,
        UP,
        DOWN
    }

    public interface OnAnimationEndListener {
        void onAnimationEnd();
    }

    public interface ZapGestureListener {
        void onHorizontalZap(boolean z);

        void onVerticalZap(boolean z, View view, int i);
    }

    public ZapGestureTouchListener(Context context) {
        this.mSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        context.getResources().getInteger(17694720);
        this.mScreenWidth = context.getResources().getDisplayMetrics().widthPixels;
        this.mAnimationCallbackList = new ArrayList();
        this.mZapGestureListenerList = new ArrayList();
    }

    public void setListView(AbsListView listView) {
        this.mListView = listView;
    }

    public void registerZapGestureListener(ZapGestureListener listener) {
        this.mZapGestureListenerList.add(listener);
    }

    public void unregisterZapGestureListener(ZapGestureListener listener) {
        this.mZapGestureListenerList.remove(listener);
    }

    public void registerAnimationCallback(AnimationCallback callback) {
        this.mAnimationCallbackList.add(callback);
    }

    public void unregisterAnimationCallback(AnimationCallback callback) {
        this.mAnimationCallbackList.remove(callback);
    }

    public void setEnabled(boolean enabled) {
        this.mPaused = !enabled;
    }

    private boolean initZapGestureDetection(MotionEvent event) {
        int index = MotionEventCompat.getActionIndex(event);
        this.mDownPointerId1 = MotionEventCompat.getPointerId(event, index);
        this.mDownPoint1 = new PointF(event.getX(index), event.getY(index));
        this.mDirection = Direction.NONE;
        if (DBG) {
            Log.d("Zap-TouchListener", "initZapGestureDetection - point1(" + this.mDownPoint1.x + "," + this.mDownPoint1.y + ")");
        }
        if (this.mListView != null) {
            Rect rect = new Rect();
            int childCount = this.mListView.getChildCount();
            int[] listViewCoords = new int[2];
            int x = ((int) this.mDownPoint1.x) - listViewCoords[0];
            int y = ((int) this.mDownPoint1.y) - listViewCoords[1];
            int i = 0;
            while (true) {
                if (i >= childCount) {
                    break;
                }
                View child = this.mListView.getChildAt(i);
                child.getHitRect(rect);
                if (rect.contains(x, y)) {
                    this.mZapListItem = child;
                    break;
                }
                i++;
            }
        }
        animateOnZapGestureInit(this.mZapView, this.mZapListItem, this.mDownPoint1);
        return false;
    }

    private boolean startZapGestureTracking(MotionEvent event) {
        if (this.mDownPoint1 == null) {
            return false;
        }
        int index = MotionEventCompat.getActionIndex(event);
        this.mDownPointerId2 = MotionEventCompat.getPointerId(event, index);
        this.mDownPoint2 = new PointF(event.getX(index), event.getY(index));
        this.mIsValidZap = true;
        this.mVelocityTracker = VelocityTracker.obtain();
        this.mVelocityTracker.addMovement(event);
        if (DBG) {
            Log.d("Zap-TouchListener", "startZapGestureTracking - point1(" + this.mDownPoint1.x + "," + this.mDownPoint1.y + "), point2(" + this.mDownPoint2.x + "," + this.mDownPoint2.y + ")");
        }
        boolean doAnimate = true;
        if (!(this.mListView == null || this.mZapListItem == null)) {
            Rect rect = new Rect();
            int[] listViewCoords = new int[2];
            int x = ((int) this.mDownPoint2.x) - listViewCoords[0];
            int y = ((int) this.mDownPoint2.y) - listViewCoords[1];
            this.mZapListItem.getHitRect(rect);
            if (rect.contains(x, y)) {
                try {
                    this.mDownPosition = this.mListView.getPositionForView(this.mZapListItem);
                } catch (NullPointerException e) {
                    Log.w("Zap-TouchListener", "A race condition with parent for mZapListItem becoming null, hence ignore the touch");
                    int childCount = this.mListView.getChildCount();
                    for (int i = 0; i < childCount; i++) {
                        if (this.mListView.getChildAt(i) == this.mZapListItem) {
                            Log.w("Zap-TouchListener", "Card location = " + childCount);
                        }
                    }
                }
                if (DBG) {
                    Log.d("Zap-TouchListener", "startZapGestureTracking - list pos=" + this.mDownPosition);
                }
            } else {
                doAnimate = false;
                resetZapGestureDetection();
            }
        }
        if (doAnimate) {
            animateOnZapGestureStart(this.mZapView, this.mZapListItem, new PointF(this.mDownPoint1.x, this.mDownPoint1.y), new PointF(this.mDownPoint2.x, this.mDownPoint2.y));
        }
        return true;
    }

    private boolean processZapGestureTracking(MotionEvent event) {
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.addMovement(event);
        }
        if (!(event.getPointerCount() != 2 || this.mDownPoint1 == null || this.mDownPoint2 == null)) {
            int p1Index = event.findPointerIndex(this.mDownPointerId1);
            float deltaX1 = this.mDownPoint1.x - event.getX(p1Index);
            float deltaY1 = this.mDownPoint1.y - event.getY(p1Index);
            int p2Index = event.findPointerIndex(this.mDownPointerId2);
            float deltaX2 = this.mDownPoint2.x - event.getX(p2Index);
            float deltaY2 = this.mDownPoint2.y - event.getY(p2Index);
            if (Math.abs(deltaY1) > ((float) this.mSlop) && Math.abs(deltaY1) > Math.abs(deltaX1) && Math.abs(deltaY2) > ((float) this.mSlop) && Math.abs(deltaY2) > Math.abs(deltaX2)) {
                this.mSwiping = true;
                if (this.mDirection == Direction.NONE) {
                    if (deltaY1 > BitmapDescriptorFactory.HUE_RED && deltaY2 > BitmapDescriptorFactory.HUE_RED) {
                        this.mDirection = Direction.UP;
                    } else if (deltaY1 < BitmapDescriptorFactory.HUE_RED && deltaY2 < BitmapDescriptorFactory.HUE_RED) {
                        this.mDirection = Direction.DOWN;
                    }
                } else if ((this.mDirection == Direction.UP && (deltaY1 < BitmapDescriptorFactory.HUE_RED || deltaY2 < BitmapDescriptorFactory.HUE_RED)) || (this.mDirection == Direction.DOWN && (deltaY1 > BitmapDescriptorFactory.HUE_RED || deltaY2 > BitmapDescriptorFactory.HUE_RED))) {
                    this.mIsValidZap = false;
                }
                if (this.mListView != null) {
                    this.mListView.requestDisallowInterceptTouchEvent(true);
                    MotionEvent cancelEvent = MotionEvent.obtain(event);
                    cancelEvent.setAction((event.getActionIndex() << 8) | 3);
                    this.mListView.onTouchEvent(cancelEvent);
                }
            }
            if (this.mSwiping) {
                if (DBG) {
                    Log.d("Zap-TouchListener", "processZapGestureTracking - point1(" + event.getX(p1Index) + "," + event.getY(p1Index) + "), point2(" + event.getX(p2Index) + "," + event.getY(p2Index) + ")");
                }
                if (this.mDirection != Direction.NONE) {
                    animateOnZapGestureMove(this.mZapView, this.mZapListItem, this.mDirection == Direction.UP, new PointF(event.getX(p1Index), event.getY(p1Index)), new PointF(event.getX(p2Index), event.getY(p2Index)));
                }
                return true;
            }
        }
        return false;
    }

    private boolean finishZapGestureTracking(MotionEvent event) {
        boolean handled = false;
        if (this.mDownPoint1 == null) {
            return false;
        }
        int p1Index = event.findPointerIndex(this.mDownPointerId1);
        float upX1 = event.getX(p1Index);
        float upY1 = event.getY(p1Index);
        float deltaX1 = Math.abs(this.mDownPoint1.x - upX1);
        float deltaY1 = Math.abs(this.mDownPoint1.y - upY1);
        if (this.mIsValidZap && this.mVelocityTracker != null) {
            boolean zap = false;
            boolean zapSend = false;
            this.mVelocityTracker.addMovement(event);
            this.mVelocityTracker.computeCurrentVelocity(1000);
            int p2Index = event.findPointerIndex(this.mDownPointerId2);
            float upX2 = event.getX(p2Index);
            float upY2 = event.getY(p2Index);
            float deltaX2 = Math.abs(this.mDownPoint2.x - upX2);
            float deltaY2 = Math.abs(this.mDownPoint2.y - upY2);
            double deltaAngle1 = Math.abs(Math.atan2((double) deltaX1, (double) deltaY1));
            double deltaAngle2 = Math.abs(Math.atan2((double) deltaX2, (double) deltaY2));
            float velocityY = Math.abs(this.mVelocityTracker.getYVelocity());
            float avgDeltaY = (deltaY1 + deltaY2) / 2.0f;
            if (DBG) {
                Log.d("Zap-TouchListener", "finishZapGestureTracking - deltaX1=" + deltaX1 + " deltaY1=" + deltaY1 + " deltaX2=" + deltaX2 + " deltaY2=" + deltaY2 + " deltaAngle1=" + Math.toDegrees(deltaAngle1) + " deltaAngle2=" + Math.toDegrees(deltaAngle2) + " velocityY=" + velocityY);
            }
            if (deltaY1 > 100.0f && deltaY2 > 100.0f && deltaAngle1 <= SWIPE_ANGLE_LIMIT_RADIANS && deltaAngle2 <= SWIPE_ANGLE_LIMIT_RADIANS && ((avgDeltaY > 180.0f && velocityY > 1000.0f) || (avgDeltaY > 300.0f && velocityY > 300.0f))) {
                zap = true;
                zapSend = this.mDirection == Direction.NONE ? this.mVelocityTracker.getYVelocity() < BitmapDescriptorFactory.HUE_RED : this.mDirection == Direction.UP;
            }
            if (zap) {
                if (DBG) {
                    Log.d("Zap-TouchListener", "Zap gesture complete");
                }
                final boolean isUp = zapSend;
                final int i = this.mDownPosition;
                animateOnZapGestureComplete(this.mZapView, this.mZapListItem, isUp, velocityY, new OnAnimationEndListener() {
                    public void onAnimationEnd() {
                        ZapGestureTouchListener.this.notifyOnVerticalZap(isUp, i);
                    }
                });
            } else {
                animateCancelZapGestureTracking(this.mZapView, this.mZapListItem);
            }
            handled = true;
        } else if (!this.mIsValidZap && this.mVelocityTracker != null) {
            animateCancelZapGestureTracking(this.mZapView, this.mZapListItem);
            handled = true;
        } else if (deltaX1 > deltaY1 && deltaX1 > ((float) (this.mScreenWidth / 2))) {
            handled = true;
            notifyOnHorizontalZap(this.mDownPoint1.x < upX1);
        }
        resetZapGestureDetection();
        return handled;
    }

    private void notifyOnHorizontalZap(boolean isLeftToRight) {
        if (DBG) {
            Log.d("Zap-TouchListener", "notifyOnHorizontalZap - isLeftToRight: " + isLeftToRight);
        }
        for (ZapGestureListener listener : this.mZapGestureListenerList) {
            listener.onHorizontalZap(isLeftToRight);
        }
    }

    /* access modifiers changed from: private */
    public void notifyOnVerticalZap(boolean isUp, int downPosition) {
        if (DBG) {
            Log.d("Zap-TouchListener", "notifyOnVerticalZap - isUp: " + isUp);
        }
        for (ZapGestureListener listener : this.mZapGestureListenerList) {
            listener.onVerticalZap(isUp, this.mZapView, downPosition);
        }
    }

    private void animateOnZapGestureInit(View view, View listItem, PointF p1) {
        for (AnimationCallback callback : this.mAnimationCallbackList) {
            callback.animateOnZapGestureInit(view, listItem, p1);
        }
    }

    private void animateOnZapGestureStart(View view, View listItem, PointF p1, PointF p2) {
        for (AnimationCallback callback : this.mAnimationCallbackList) {
            callback.animateOnZapGestureStart(view, listItem, p1, p2);
        }
    }

    private void animateOnZapGestureMove(View view, View listItem, boolean isUp, PointF p1, PointF p2) {
        for (AnimationCallback callback : this.mAnimationCallbackList) {
            callback.animateOnZapGestureMove(view, listItem, isUp, p1, p2);
        }
    }

    private void animateOnZapGestureComplete(View view, View listItem, boolean isUp, float velocity, OnAnimationEndListener animateEndCallback) {
        for (AnimationCallback callback : this.mAnimationCallbackList) {
            callback.animateOnZapGestureComplete(view, listItem, isUp, velocity, animateEndCallback);
        }
        if (this.mAnimationCallbackList.size() == 0 && animateEndCallback != null) {
            animateEndCallback.onAnimationEnd();
        }
    }

    private void animateCancelZapGestureTracking(View view, View listItem) {
        for (AnimationCallback callback : this.mAnimationCallbackList) {
            callback.animateOnZapGestureCancel(view, listItem);
        }
    }

    private void resetZapGestureDetection() {
        if (DBG) {
            Log.d("Zap-TouchListener", "resetZapGestureDetection");
        }
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.clear();
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
        this.mDownPoint1 = null;
        this.mDownPoint2 = null;
        this.mZapView = null;
        this.mDownPosition = -1;
        this.mSwiping = false;
        this.mIsValidZap = false;
        this.mDirection = Direction.NONE;
    }

    public boolean onTouch(View view, MotionEvent event) {
        this.mPointerCount = event.getPointerCount();
        this.mZapView = view;
        switch (MotionEventCompat.getActionMasked(event)) {
            case 0:
                if (!this.mPaused && initZapGestureDetection(event)) {
                    view.onTouchEvent(event);
                    return true;
                }
            case 1:
            case 6:
                return finishZapGestureTracking(event);
            case 2:
                if (!this.mPaused && processZapGestureTracking(event)) {
                    return true;
                }
            case 3:
                animateCancelZapGestureTracking(this.mZapView, this.mZapListItem);
                resetZapGestureDetection();
                break;
            case 5:
                if (!this.mPaused) {
                    if (event.getPointerCount() != 2 || !startZapGestureTracking(event)) {
                        if (event.getPointerCount() > 3) {
                            animateCancelZapGestureTracking(this.mZapView, this.mZapListItem);
                            resetZapGestureDetection();
                            break;
                        }
                    } else {
                        view.onTouchEvent(event);
                        return true;
                    }
                }
                break;
        }
        return false;
    }

    public int getPointerCount() {
        return this.mPointerCount;
    }
}
