package com.motorola.zap.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.motorola.sharing.util.FileUtils;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.app.ActionReceiver;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ShareSupportActivity extends Activity implements MediaScannerConnectionClient, OnClickListener {
    /* access modifiers changed from: private */
    public static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    protected static final int FAIL_TO_DOWNLOAD = 0;
    protected static final int LOCAL_COPY_AVAILABLE = 1;
    /* access modifiers changed from: private */
    public static final String TAG = ShareSupportActivity.class.getSimpleName();
    Context mContext = null;
    ExecutorService mExecutor = null;
    Handler mHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    ShareSupportActivity.this.mScanner.disconnect();
                    ShareSupportActivity.this.finish();
                    return;
                case 1:
                    ShareSupportActivity.this.mScanner.disconnect();
                    ShareSupportActivity.this.updateScreen();
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean mIsAlive = false;
    Uri mMediaUri = null;
    /* access modifiers changed from: private */
    public String mPathWaitingForScan = null;
    MediaScannerConnection mScanner = null;
    ProgressDialog mWaitDialog = null;
    Runnable sendRemoteZap = new Runnable() {
        public void run() {
            if (ShareSupportActivity.DBG) {
                Log.d(ShareSupportActivity.TAG, "Received Media URI to zap = " + ShareSupportActivity.this.mMediaUri.toString());
            }
            String filePath = FileUtils.getMediaPathFromUri(ShareSupportActivity.this.mContext, ShareSupportActivity.this.mMediaUri);
            if (ShareSupportActivity.DBG) {
                Log.d(ShareSupportActivity.TAG, "filePath for URI = " + filePath);
            }
            String uri = ShareSupportActivity.this.mMediaUri.toString();
            if (ShareSupportActivity.this.mIsAlive) {
                if (filePath == null || ShareSupportActivity.this.mMediaUri.toString().startsWith("content://")) {
                    try {
                        InputStream is = ShareSupportActivity.this.getContentResolver().openInputStream(ShareSupportActivity.this.mMediaUri);
                        if (ShareSupportActivity.this.mIsAlive) {
                            String fileExtension = FileUtils.getFileExtensionFromMimeType(ShareSupportActivity.this.getContentResolver().getType(ShareSupportActivity.this.mMediaUri));
                            if (is != null) {
                                String targetFileLocation = ZapUtils.getZapDirectoryPath();
                                File directory = new File(targetFileLocation);
                                if (directory.exists() || directory.mkdirs()) {
                                    File targetFile = new File(targetFileLocation + "/media_" + System.currentTimeMillis() + "." + fileExtension);
                                    try {
                                        FileOutputStream outputStream = new FileOutputStream(targetFile);
                                        if (outputStream != null) {
                                            byte[] bytes = new byte[1024];
                                            while (true) {
                                                try {
                                                    int read = is.read(bytes);
                                                    if (read != -1) {
                                                        outputStream.write(bytes, 0, read);
                                                    } else {
                                                        try {
                                                            break;
                                                        } catch (IOException e) {
                                                            Log.e(ShareSupportActivity.TAG, "Unable to close the output stream : " + e);
                                                            ShareSupportActivity.this.mHandler.sendEmptyMessage(0);
                                                            return;
                                                        }
                                                    }
                                                } catch (IOException e2) {
                                                    Log.e(ShareSupportActivity.TAG, "Unable to write to the file : " + e2);
                                                    ShareSupportActivity.this.mHandler.sendEmptyMessage(0);
                                                    return;
                                                }
                                            }
                                            outputStream.close();
                                        }
                                        String finalPath = targetFile.getAbsolutePath();
                                        if (ShareSupportActivity.DBG) {
                                            Log.d(ShareSupportActivity.TAG, "Created local copy at " + finalPath);
                                        }
                                        if (!ShareSupportActivity.this.mIsAlive) {
                                            return;
                                        }
                                        if (ShareSupportActivity.this.mScanner.isConnected()) {
                                            ShareSupportActivity.this.mPathWaitingForScan = null;
                                            ShareSupportActivity.this.mScanner.scanFile(finalPath, FileUtils.getMimeTypeFromFileName(finalPath));
                                            return;
                                        }
                                        ShareSupportActivity.this.mPathWaitingForScan = finalPath;
                                    } catch (FileNotFoundException e3) {
                                        Log.e(ShareSupportActivity.TAG, "Target file not created : " + e3);
                                        ShareSupportActivity.this.mHandler.sendEmptyMessage(0);
                                    }
                                } else {
                                    try {
                                        is.close();
                                    } catch (IOException e4) {
                                        Log.e(ShareSupportActivity.TAG, "Exception during closing of the stream : " + e4);
                                    }
                                    ShareSupportActivity.this.mHandler.sendEmptyMessage(0);
                                }
                            } else {
                                ShareSupportActivity.this.mHandler.sendEmptyMessage(0);
                            }
                        }
                    } catch (FileNotFoundException e5) {
                        Log.e(ShareSupportActivity.TAG, "File Not Found : " + e5);
                        ShareSupportActivity.this.mHandler.sendEmptyMessage(0);
                    }
                } else {
                    ShareSupportActivity.this.mHandler.sendEmptyMessage(1);
                }
            }
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.mMediaUri = (Uri) extras.getParcelable("android.intent.extra.STREAM");
            if (this.mMediaUri != null) {
                this.mExecutor = Executors.newSingleThreadExecutor();
                this.mScanner = new MediaScannerConnection(this, this);
                this.mScanner.connect();
                this.mContext = this;
                this.mIsAlive = true;
                this.mExecutor.execute(this.sendRemoteZap);
                this.mWaitDialog = ProgressDialog.show(this, null, getString(R.string.wait_message), true, true, new OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        ShareSupportActivity.this.mWaitDialog.dismiss();
                        ShareSupportActivity.this.mWaitDialog = null;
                        ShareSupportActivity.this.finish();
                    }
                });
                this.mWaitDialog.setCanceledOnTouchOutside(false);
                return;
            }
            Log.e(TAG, "Media Uri received is empty");
        } else {
            Uri data = getIntent().getData();
            this.mMediaUri = data;
            if (data != null) {
                updateScreen();
                return;
            }
            Log.e(TAG, "extras is null for Intent.ACTION_SEND");
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.mWaitDialog != null) {
            this.mWaitDialog.dismiss();
            this.mWaitDialog = null;
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.mScanner != null && this.mScanner.isConnected()) {
            this.mScanner.disconnect();
        }
        this.mIsAlive = false;
        super.onDestroy();
    }

    public void onMediaScannerConnected() {
        if (this.mPathWaitingForScan != null) {
            this.mScanner.scanFile(this.mPathWaitingForScan, FileUtils.getMimeTypeFromFileName(this.mPathWaitingForScan));
        }
    }

    public void onScanCompleted(String path, Uri uri) {
        if (DBG) {
            Log.d(TAG, "After scan new uri = " + uri);
        }
        this.mMediaUri = uri;
        this.mHandler.sendEmptyMessage(1);
        this.mScanner.disconnect();
    }

    /* access modifiers changed from: 0000 */
    public void updateScreen() {
        if (this.mWaitDialog != null) {
            this.mWaitDialog.dismiss();
            this.mWaitDialog = null;
        }
        setContentView(R.layout.share_layout);
        loadImage((ImageView) findViewById(R.id.share_image));
        ImageView zapCancel = (ImageView) findViewById(R.id.review_cancel);
        ((ImageView) findViewById(R.id.review_approve)).setOnClickListener(this);
        zapCancel.setOnClickListener(this);
    }

    /* access modifiers changed from: 0000 */
    public void sendZap() {
        Intent intent = new Intent(ZapConstants.ACTION_GS_GALLERY_PHOTO_SHARED);
        intent.setClass(getApplicationContext(), ActionReceiver.class);
        intent.putExtra(ZapConstants.EXTRA_GS_GALLERY_PHOTO_SHARED, this.mMediaUri.toString());
        intent.putExtra(ZapConstants.EXTRA_EVENT_ORIGIN_ZAP_APP, true);
        intent.putExtra(ZapConstants.EXTRA_FROM_SHARE_INTENT, true);
        sendBroadcast(intent);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.review_cancel /*2131296445*/:
                finish();
                return;
            case R.id.review_approve /*2131296446*/:
                sendZap();
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: 0000 */
    public void loadImage(ImageView v) {
        setImage(v, this.mMediaUri);
        if (FileUtils.isVideo(FileUtils.getMimeTypeFromFileName(FileUtils.getMediaPathFromUri(this, this.mMediaUri)))) {
            ((ImageView) findViewById(R.id.share_video_icon)).setVisibility(0);
        }
    }

    private void setImage(final ImageView image, final Uri uri) {
        new Thread(new Runnable() {
            public void run() {
                int targetW = image.getWidth();
                int targetH = image.getHeight();
                if (targetH == 0 || targetW == 0) {
                    DisplayMetrics d = ShareSupportActivity.this.getResources().getDisplayMetrics();
                    targetH = d.heightPixels;
                    targetW = d.widthPixels;
                }
                Bitmap bitmap = FileUtils.getImage(FileUtils.getMediaPathFromUri(image.getContext(), uri), targetW, targetH);
                if (bitmap != null) {
                    final Bitmap b = bitmap;
                    ShareSupportActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            image.setScaleType(ScaleType.CENTER_INSIDE);
                            image.setImageBitmap(b);
                        }
                    });
                    return;
                }
                if (Log.isLoggable(ShareSupportActivity.TAG, 6)) {
                    Log.e(ShareSupportActivity.TAG, " Failed to decode the Image");
                }
                ShareSupportActivity.this.finish();
            }
        }).start();
    }
}
