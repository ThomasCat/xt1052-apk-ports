package com.motorola.sharing.provider.cloud.api;

import com.motorola.sharing.Endpoint;
import com.motorola.sharing.Endpoint.AttrValue;
import com.motorola.sharing.Endpoint.Attributes;

public class CloudEndpoint extends Endpoint {
    public CloudEndpoint() {
        super(new Attributes(AttrValue.STATIC, AttrValue.CLOUD, AttrValue.SHARE_AND_RECEIVE));
    }

    public String getName() {
        return CloudEndpoint.class.getCanonicalName();
    }
}
