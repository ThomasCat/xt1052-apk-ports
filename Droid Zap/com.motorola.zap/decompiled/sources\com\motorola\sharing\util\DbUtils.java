package com.motorola.sharing.util;

import android.database.Cursor;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class DbUtils {
    private static final String TAG = Logger.getGcspTag("DbUtils");

    public static long getLong(Cursor c, String column, long defaultValue) {
        int columnIndex = c.getColumnIndex(column);
        return c.isNull(columnIndex) ? defaultValue : c.getLong(columnIndex);
    }

    public static int getInt(Cursor c, String column, int defaultValue) {
        int columnIndex = c.getColumnIndex(column);
        return c.isNull(columnIndex) ? defaultValue : c.getInt(columnIndex);
    }

    public static double getDouble(Cursor c, String column, double defaultValue) {
        int columnIndex = c.getColumnIndex(column);
        return c.isNull(columnIndex) ? defaultValue : c.getDouble(columnIndex);
    }

    public static String getString(Cursor c, String column) {
        int columnIndex = c.getColumnIndex(column);
        if (c.isNull(columnIndex)) {
            return null;
        }
        return c.getString(columnIndex);
    }

    public static byte[] getBlob(Cursor c, String column) {
        int columnIndex = c.getColumnIndex(column);
        if (c.isNull(columnIndex)) {
            return null;
        }
        return c.getBlob(columnIndex);
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0048 A[SYNTHETIC, Splitter:B:25:0x0048] */
    public static byte[] serialize(Object object) {
        byte[] bArr = null;
        if (object != null) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = null;
            try {
                ObjectOutput out2 = new ObjectOutputStream(bos);
                try {
                    out2.writeObject(object);
                    bArr = bos.toByteArray();
                    if (out2 != null) {
                        try {
                            out2.close();
                        } catch (IOException e) {
                        }
                    }
                    try {
                        bos.close();
                    } catch (IOException e2) {
                    }
                } catch (Exception e3) {
                    e = e3;
                    out = out2;
                } catch (Throwable th) {
                    th = th;
                    out = out2;
                    if (out != null) {
                    }
                    try {
                        bos.close();
                    } catch (IOException e4) {
                    }
                    throw th;
                }
            } catch (Exception e5) {
                e = e5;
                try {
                    Logger.e(TAG, "got exception: " + e);
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e6) {
                        }
                    }
                    try {
                        bos.close();
                    } catch (IOException e7) {
                    }
                    return bArr;
                } catch (Throwable th2) {
                    th = th2;
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e8) {
                        }
                    }
                    bos.close();
                    throw th;
                }
            }
        }
        return bArr;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x004b A[SYNTHETIC, Splitter:B:29:0x004b] */
    public static Object deserialize(byte[] data) {
        Object obj = null;
        if (data != null && data.length > 0) {
            ByteArrayInputStream bis = new ByteArrayInputStream(data);
            ObjectInput in = null;
            try {
                ObjectInput in2 = new ObjectInputStream(bis);
                try {
                    obj = in2.readObject();
                    try {
                        bis.close();
                    } catch (IOException e) {
                    }
                    if (in2 != null) {
                        try {
                            in2.close();
                        } catch (IOException e2) {
                        }
                    }
                } catch (Exception e3) {
                    e = e3;
                    in = in2;
                } catch (Throwable th) {
                    th = th;
                    in = in2;
                    try {
                        bis.close();
                    } catch (IOException e4) {
                    }
                    if (in != null) {
                    }
                    throw th;
                }
            } catch (Exception e5) {
                e = e5;
                try {
                    Logger.e(TAG, "got exception: " + e);
                    try {
                        bis.close();
                    } catch (IOException e6) {
                    }
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException e7) {
                        }
                    }
                    return obj;
                } catch (Throwable th2) {
                    th = th2;
                    bis.close();
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException e8) {
                        }
                    }
                    throw th;
                }
            }
        }
        return obj;
    }
}
