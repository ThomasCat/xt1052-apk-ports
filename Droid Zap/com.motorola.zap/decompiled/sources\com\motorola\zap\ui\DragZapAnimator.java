package com.motorola.zap.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.database.Cursor;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.motorola.sharing.GCSP;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.app.ZapTracker;
import com.motorola.zap.ui.ZapGestureTouchListener.AnimationCallback;
import com.motorola.zap.ui.ZapGestureTouchListener.OnAnimationEndListener;

public class DragZapAnimator implements AnimationCallback {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    boolean isAnimating = false;
    private ConcentricCircles mConcentricCircles = null;
    boolean mHideAnimInProgress = false;
    boolean mIsAnimatingUp = true;
    /* access modifiers changed from: private */
    public ListView mListView = null;
    float mScale = 1.0f;
    private float mStartY = -1.0f;
    Handler mTutorialHanlder = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            boolean isSwipeDownTutorialShown = ((Boolean) Z.SWIPE_DOWN_TUTORIAL_SHOWN.read()).booleanValue();
            boolean lastSwipeDownSuccess = ((Boolean) Z.LAST_SWIPE_DOWN_SUCCESS.read()).booleanValue();
            boolean lastSwipeUpSuccess = ((Boolean) Z.LAST_SWIPE_UP_SUCCESS.read()).booleanValue();
            boolean lastSwipeIsDown = ((Boolean) Z.LAST_SWIPE_DOWN_EVENT_FAILED.read()).booleanValue();
            switch (msg.what) {
                case 0:
                    if (!ZapTracker.getInstance().getLockedContenAvailable() && isSwipeDownTutorialShown && !GCSP.getGroupManager().isAnyGroupActive() && !DragZapAnimator.this.isAnimating) {
                        if (!lastSwipeDownSuccess || !lastSwipeUpSuccess) {
                            if (DragZapAnimator.this.mListView != null) {
                                int firstVisiblePostion = DragZapAnimator.this.mListView.getFirstVisiblePosition();
                                int lastVisiblePosition = DragZapAnimator.this.mListView.getLastVisiblePosition();
                                for (int i = 0; i <= lastVisiblePosition - firstVisiblePostion; i++) {
                                    View view = DragZapAnimator.this.mListView.getChildAt(i);
                                    if (view != null) {
                                        View progress = view.findViewById(R.id.progress);
                                        if (progress == null || progress.getVisibility() != 0) {
                                            View timer = view.findViewById(R.id.timer);
                                            if (timer != null && timer.getVisibility() == 0) {
                                                return;
                                            }
                                        } else {
                                            return;
                                        }
                                    }
                                }
                            }
                            if (lastSwipeIsDown) {
                                DragZapAnimator.this.mTutorialView.setVisibility(0);
                                DragZapAnimator.this.mTutorialView.setY(DragZapAnimator.this.mScale * -150.0f);
                                DragZapAnimator.this.mTutorialView.animate().translationY(-25.0f * DragZapAnimator.this.mScale).setDuration(1000).setListener(new AnimatorListenerAdapter() {
                                    public void onAnimationEnd(Animator animation) {
                                    }
                                });
                                return;
                            }
                            DragZapAnimator.this.setTutorialTextOnCards(0);
                            return;
                        }
                        return;
                    }
                    return;
                case 1:
                    if (!DragZapAnimator.this.mHideAnimInProgress && isSwipeDownTutorialShown) {
                        if (!lastSwipeUpSuccess) {
                            DragZapAnimator.this.setTutorialTextOnCards(4);
                        }
                        if (!lastSwipeDownSuccess) {
                            DragZapAnimator.this.mHideAnimInProgress = true;
                            DragZapAnimator.this.mTutorialView.animate().translationY(DragZapAnimator.this.mScale * -150.0f).setDuration(500).setListener(new AnimatorListenerAdapter() {
                                public void onAnimationEnd(Animator animation) {
                                    DragZapAnimator.this.mTutorialView.setVisibility(4);
                                    DragZapAnimator.this.mHideAnimInProgress = false;
                                }
                            });
                            return;
                        }
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public View mTutorialView = null;
    private int mTutorialVisibilityStatus = 8;

    public void cleanUpAllHandlerMessages() {
        this.mTutorialView.setVisibility(4);
        setTutorialTextOnCards(4);
        this.mTutorialHanlder.removeCallbacksAndMessages(null);
    }

    /* access modifiers changed from: private */
    public void setTutorialTextOnCards(int visiblilty) {
        if (this.mListView != null) {
            int firstVisiblePostion = this.mListView.getFirstVisiblePosition();
            int lastVisiblePosition = this.mListView.getLastVisiblePosition();
            for (int i = 0; i <= lastVisiblePosition - firstVisiblePostion; i++) {
                View view = this.mListView.getChildAt(i);
                if (view != null) {
                    View tv = view.findViewById(R.id.swipe_up_tutorial);
                    if (tv != null) {
                        if (visiblilty == 0) {
                            Cursor cursor = (Cursor) this.mListView.getAdapter().getItem(firstVisiblePostion + i);
                            if (cursor != null) {
                                if (!cursor.isClosed()) {
                                    if (view.getTop() < 0) {
                                    }
                                }
                            }
                        }
                        tv.setVisibility(visiblilty);
                    }
                }
            }
        }
    }

    public void resetInActivityTimer() {
        resetInActivityTimer(5000);
    }

    public void resetInActivityTimer(long delay) {
        this.mTutorialHanlder.removeMessages(1);
        this.mTutorialHanlder.sendEmptyMessageDelayed(1, 10);
        this.mTutorialHanlder.removeMessages(0);
        this.mTutorialHanlder.sendEmptyMessageDelayed(0, delay);
    }

    DragZapAnimator(ConcentricCircles cc, ConcentricCircles staticCircle, View tutorialView, ListView listview, float scale) {
        this.mConcentricCircles = cc;
        this.mConcentricCircles.setVisibility(4);
        this.mTutorialView = tutorialView;
        this.mListView = listview;
        this.mScale = scale;
    }

    private void hideDragIcons() {
        this.mConcentricCircles.setVisibility(8);
    }

    private void moveIntoDragDownMode() {
        this.mConcentricCircles.setVisibility(0);
        this.mConcentricCircles.setY(BitmapDescriptorFactory.HUE_RED);
        this.mConcentricCircles.setVisibleHeight(this.mConcentricCircles.getCircleWidth());
        this.mConcentricCircles.setDirection(false);
        this.mConcentricCircles.invalidate();
    }

    private void setDragDownYPoint(float y, int height) {
        this.mConcentricCircles.setVisibility(0);
        this.mConcentricCircles.setY(y - ((float) this.mConcentricCircles.getCircleWidth()));
        this.mConcentricCircles.setVisibleHeight(height);
        this.mConcentricCircles.setDirection(false);
        this.mConcentricCircles.invalidate();
    }

    public void animateOnZapGestureStart(View view, View listItem, PointF p1, PointF p2) {
        this.isAnimating = true;
        int start = 0;
        if (view != null) {
            int[] location = new int[2];
            view.getLocationInWindow(location);
            start = location[1];
        }
        this.mStartY = (p1.y < p2.y ? p1.y : p2.y) + (Math.abs(p1.y - p2.y) / 2.0f);
        this.mStartY += (float) start;
        if (DBG) {
            Log.d("DragZapAnimator", "animateOnZapGestureStart,  mStartY -" + this.mStartY);
        }
        onGestureStart();
    }

    public void animateOnZapGestureMove(View view, View listItem, boolean isUp, PointF p1, PointF p2) {
        this.mIsAnimatingUp = isUp;
        int start = 0;
        if (view != null) {
            int[] location = new int[2];
            view.getLocationInWindow(location);
            start = location[1];
        }
        float y = (p1.y < p2.y ? p1.y : p2.y) + (Math.abs(p1.y - p2.y) / 2.0f);
        if (DBG) {
            Log.d("DragZapAnimator", "animateOnZapGestureMove, Y : " + y);
        }
        if (this.mStartY - (((float) start) + y) > BitmapDescriptorFactory.HUE_RED || isUp) {
            Z.LAST_SWIPE_UP_SUCCESS.write(Boolean.valueOf(false));
            Z.LAST_SWIPE_DOWN_EVENT_FAILED.write(Boolean.valueOf(false));
            hideDragIcons();
            return;
        }
        setDragDownYPoint(this.mStartY, (int) Math.abs(this.mStartY - (((float) start) + y)));
    }

    public void animateOnZapGestureComplete(View view, View listItem, boolean isUp, float velocity, OnAnimationEndListener animateEndCallback) {
        if (DBG) {
            Log.i("DragZapAnimator", "DragZapAnimator::animateOnZapGestureComplete");
        }
        moveIntoDragDownMode();
        hideDragIcons();
        if (animateEndCallback != null && !isUp) {
            if (DBG) {
                Log.e("DragZapAnimator", "DragZapAnimator just completed my animation");
            }
            animateEndCallback.onAnimationEnd();
            Z.LAST_SWIPE_DOWN_SUCCESS.write(Boolean.valueOf(true));
            Z.LAST_SWIPE_DOWN_EVENT_FAILED.write(Boolean.valueOf(false));
        }
        this.mIsAnimatingUp = true;
        onGestureComplete(true);
    }

    public void animateOnZapGestureCancel(View view, View listItem) {
        moveIntoDragDownMode();
        onGestureComplete(false);
        if (!this.mIsAnimatingUp) {
            Z.LAST_SWIPE_DOWN_SUCCESS.write(Boolean.valueOf(false));
            Z.LAST_SWIPE_DOWN_EVENT_FAILED.write(Boolean.valueOf(true));
        }
        this.mIsAnimatingUp = true;
    }

    private void onGestureStart() {
        cleanUpAllHandlerMessages();
        if (this.mTutorialView != null) {
            this.mTutorialVisibilityStatus = this.mTutorialView.getVisibility();
            this.mTutorialView.setVisibility(8);
        }
    }

    private void onGestureComplete(boolean isSuccess) {
        this.isAnimating = false;
        this.mConcentricCircles.setVisibility(8);
        if (isSuccess) {
            resetInActivityTimer(ZapConstants.LOCATION_RETRY_MAX_TIME_SETTINGS_DISABLED);
        } else {
            resetInActivityTimer(50);
        }
        if (this.mTutorialView != null && !isSuccess) {
            this.mTutorialView.setVisibility(this.mTutorialVisibilityStatus);
        }
    }

    public void animateOnZapGestureInit(View view, View listItem, PointF p1) {
    }
}
