package com.google.android.gms.drive.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi.ContentsResult;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFile.DownloadProgressListener;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataChangeSet;

public class o extends r implements DriveFile {

    private abstract class a extends m<Status> {
        private a() {
        }

        /* renamed from: f */
        public Status d(Status status) {
            return status;
        }
    }

    private abstract class b extends m<Status> {
        private b() {
        }

        /* renamed from: f */
        public Status d(Status status) {
            return status;
        }
    }

    private static class c extends c {
        private final DownloadProgressListener DL;
        private final com.google.android.gms.common.api.a.c<ContentsResult> vj;

        public c(com.google.android.gms.common.api.a.c<ContentsResult> cVar, DownloadProgressListener downloadProgressListener) {
            this.vj = cVar;
            this.DL = downloadProgressListener;
        }

        public void a(OnContentsResponse onContentsResponse) throws RemoteException {
            this.vj.b(new a(Status.zQ, onContentsResponse.eX()));
        }

        public void a(OnDownloadProgressResponse onDownloadProgressResponse) throws RemoteException {
            if (this.DL != null) {
                this.DL.onProgress(onDownloadProgressResponse.eY(), onDownloadProgressResponse.eZ());
            }
        }

        public void l(Status status) throws RemoteException {
            this.vj.b(new a(status, null));
        }
    }

    private abstract class d extends m<ContentsResult> {
        private d() {
        }

        /* renamed from: n */
        public ContentsResult d(Status status) {
            return new a(status, null);
        }
    }

    public o(DriveId driveId) {
        super(driveId);
    }

    public PendingResult<Status> commitAndCloseContents(GoogleApiClient apiClient, final Contents contents) {
        if (contents != null) {
            return apiClient.b(new b() {
                /* access modifiers changed from: protected */
                public void a(n nVar) throws RemoteException {
                    contents.close();
                    nVar.eT().a(new CloseContentsRequest(contents, true), (v) new ak(this));
                }
            });
        }
        throw new IllegalArgumentException("Contents must be provided.");
    }

    public PendingResult<Status> commitAndCloseContents(GoogleApiClient apiClient, final Contents contents, final MetadataChangeSet changeSet) {
        if (contents != null) {
            return apiClient.b(new a() {
                /* access modifiers changed from: protected */
                public void a(n nVar) throws RemoteException {
                    contents.close();
                    nVar.eT().a(new CloseContentsAndUpdateMetadataRequest(o.this.CS, changeSet.eS(), contents), (v) new ak(this));
                }
            });
        }
        throw new IllegalArgumentException("Contents must be provided.");
    }

    public PendingResult<Status> discardContents(GoogleApiClient apiClient, Contents contents) {
        return Drive.DriveApi.discardContents(apiClient, contents);
    }

    public PendingResult<ContentsResult> openContents(GoogleApiClient apiClient, final int mode, final DownloadProgressListener listener) {
        if (mode == 268435456 || mode == 536870912 || mode == 805306368) {
            return apiClient.a(new d() {
                /* access modifiers changed from: protected */
                public void a(n nVar) throws RemoteException {
                    nVar.eT().a(new OpenContentsRequest(o.this.getDriveId(), mode), (v) new c(this, listener));
                }
            });
        }
        throw new IllegalArgumentException("Invalid mode provided.");
    }
}
