package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;

public class hu implements Creator<ht> {
    static void a(ht htVar, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.a(parcel, 1, htVar.Oc, false);
        b.c(parcel, 1000, htVar.wj);
        b.D(parcel, p);
    }

    /* renamed from: az */
    public ht createFromParcel(Parcel parcel) {
        int o = a.o(parcel);
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    str = a.m(parcel, n);
                    break;
                case 1000:
                    i = a.g(parcel, n);
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new ht(i, str);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }

    /* renamed from: bt */
    public ht[] newArray(int i) {
        return new ht[i];
    }
}
