package com.google.android.gms.analytics;

import com.motorola.zap.ZapConstants;
import java.util.HashMap;
import java.util.Map;

class ab {
    private final Map<String, Integer> tW = new HashMap();
    private final Map<String, String> tX = new HashMap();
    private final boolean tY;
    private final String tZ;

    ab(String str, boolean z) {
        this.tY = z;
        this.tZ = str;
    }

    /* access modifiers changed from: 0000 */
    public void c(String str, int i) {
        if (this.tY) {
            Integer num = (Integer) this.tW.get(str);
            if (num == null) {
                num = Integer.valueOf(0);
            }
            this.tW.put(str, Integer.valueOf(num.intValue() + i));
        }
    }

    /* access modifiers changed from: 0000 */
    public String cn() {
        if (!this.tY) {
            return ZapConstants.GOOGLE_ACCOUNT_NONE;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.tZ);
        for (String str : this.tW.keySet()) {
            sb.append("&").append(str).append("=").append(this.tW.get(str));
        }
        for (String str2 : this.tX.keySet()) {
            sb.append("&").append(str2).append("=").append((String) this.tX.get(str2));
        }
        return sb.toString();
    }
}
