package com.google.android.gms.internal;

public final class fn implements fl {
    private static fn CN;

    public static synchronized fl eI() {
        fn fnVar;
        synchronized (fn.class) {
            if (CN == null) {
                CN = new fn();
            }
            fnVar = CN;
        }
        return fnVar;
    }

    public long currentTimeMillis() {
        return System.currentTimeMillis();
    }
}
