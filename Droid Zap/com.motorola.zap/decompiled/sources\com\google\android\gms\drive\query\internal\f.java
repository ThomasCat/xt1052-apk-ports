package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;
import java.util.List;

public class f implements Creator<LogicalFilter> {
    static void a(LogicalFilter logicalFilter, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.c(parcel, 1000, logicalFilter.wj);
        b.a(parcel, 1, (Parcelable) logicalFilter.EO, i, false);
        b.b(parcel, 2, logicalFilter.EY, false);
        b.D(parcel, p);
    }

    /* renamed from: aL */
    public LogicalFilter[] newArray(int i) {
        return new LogicalFilter[i];
    }

    /* renamed from: ag */
    public LogicalFilter createFromParcel(Parcel parcel) {
        List c;
        Operator operator;
        int i;
        List list = null;
        int o = a.o(parcel);
        int i2 = 0;
        Operator operator2 = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    i = i2;
                    Operator operator3 = (Operator) a.a(parcel, n, Operator.CREATOR);
                    c = list;
                    operator = operator3;
                    break;
                case 2:
                    c = a.c(parcel, n, FilterHolder.CREATOR);
                    operator = operator2;
                    i = i2;
                    break;
                case 1000:
                    List list2 = list;
                    operator = operator2;
                    i = a.g(parcel, n);
                    c = list2;
                    break;
                default:
                    a.b(parcel, n);
                    c = list;
                    operator = operator2;
                    i = i2;
                    break;
            }
            i2 = i;
            operator2 = operator;
            list = c;
        }
        if (parcel.dataPosition() == o) {
            return new LogicalFilter(i2, operator2, list);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }
}
