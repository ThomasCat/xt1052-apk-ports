package com.google.android.gms.tagmanager;

import android.os.Build.VERSION;

class l<K, V> {
    final a<K, V> TK = new a<K, V>() {
        public int sizeOf(K k, V v) {
            return 1;
        }
    };

    public interface a<K, V> {
        int sizeOf(K k, V v);
    }

    public k<K, V> a(int i, a<K, V> aVar) {
        if (i > 0) {
            return iA() < 12 ? new da(i, aVar) : new bb(i, aVar);
        }
        throw new IllegalArgumentException("maxSize <= 0");
    }

    /* access modifiers changed from: 0000 */
    public int iA() {
        return VERSION.SDK_INT;
    }
}
