package com.google.android.gms.drive;

import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public final class b extends Metadata {
    private final MetadataBundle CZ;

    public b(MetadataBundle metadataBundle) {
        this.CZ = metadataBundle;
    }

    /* access modifiers changed from: protected */
    public <T> T a(MetadataField<T> metadataField) {
        return this.CZ.a(metadataField);
    }

    /* renamed from: eQ */
    public Metadata freeze() {
        return new b(MetadataBundle.a(this.CZ));
    }

    public boolean isDataValid() {
        return this.CZ != null;
    }

    public String toString() {
        return "Metadata [mImpl=" + this.CZ + "]";
    }
}
