package com.google.android.gms.internal;

import android.content.Context;
import android.os.SystemClock;
import android.text.TextUtils;
import com.google.android.gms.internal.ch.a;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

public final class ci extends a {
    private static final Object op = new Object();
    private static ci oq;
    private final Context mContext;
    private final au or;

    private ci(Context context, au auVar) {
        this.mContext = context;
        this.or = auVar;
    }

    private static cf a(final Context context, au auVar, final cd cdVar) {
        da.s("Starting ad request from service.");
        auVar.init();
        cm cmVar = new cm(context);
        if (cmVar.oX == -1) {
            da.s("Device is offline.");
            return new cf(2);
        }
        final ck ckVar = new ck(cdVar.applicationInfo.packageName);
        if (cdVar.oc.extras != null) {
            String string = cdVar.oc.extras.getString("_ad");
            if (string != null) {
                return cj.a(context, cdVar, string);
            }
        }
        final String a = cj.a(cdVar, cmVar, auVar.a(250));
        if (a == null) {
            return new cf(0);
        }
        cz.pT.post(new Runnable() {
            public void run() {
                dd a = dd.a(context, new ab(), false, false, null, cdVar.kN);
                a.setWillNotDraw(true);
                ckVar.b(a);
                de bb = a.bb();
                bb.a("/invalidRequest", ckVar.oz);
                bb.a("/loadAdURL", ckVar.oA);
                bb.a("/log", aq.mb);
                da.s("Getting the ad request URL.");
                a.loadDataWithBaseURL("http://googleads.g.doubleclick.net", "<!DOCTYPE html><html><head><script src=\"http://googleads.g.doubleclick.net/mads/static/sdk/native/sdk-core-v40.js\"></script><script>AFMA_buildAdURL(" + a + ");</script></head><body></body></html>", "text/html", "UTF-8", null);
            }
        });
        String aI = ckVar.aI();
        return TextUtils.isEmpty(aI) ? new cf(ckVar.getErrorCode()) : a(context, cdVar.kN.pU, aI);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        com.google.android.gms.internal.da.w("Received error HTTP response code: " + r6);
        r1 = new com.google.android.gms.internal.cf(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        r0.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        return r1;
     */
    public static cf a(Context context, String str, String str2) {
        HttpURLConnection httpURLConnection;
        try {
            cl clVar = new cl();
            URL url = new URL(str2);
            long elapsedRealtime = SystemClock.elapsedRealtime();
            URL url2 = url;
            int i = 0;
            while (true) {
                httpURLConnection = (HttpURLConnection) url2.openConnection();
                cv.a(context, str, false, httpURLConnection);
                int responseCode = httpURLConnection.getResponseCode();
                Map headerFields = httpURLConnection.getHeaderFields();
                if (responseCode < 200 || responseCode >= 300) {
                    a(url2.toString(), headerFields, null, responseCode);
                    if (responseCode < 300 || responseCode >= 400) {
                        break;
                    }
                    String headerField = httpURLConnection.getHeaderField("Location");
                    if (TextUtils.isEmpty(headerField)) {
                        da.w("No location header to follow redirect.");
                        cf cfVar = new cf(0);
                        httpURLConnection.disconnect();
                        return cfVar;
                    }
                    url2 = new URL(headerField);
                    i++;
                    if (i > 5) {
                        da.w("Too many redirects.");
                        cf cfVar2 = new cf(0);
                        httpURLConnection.disconnect();
                        return cfVar2;
                    }
                    clVar.d(headerFields);
                    httpURLConnection.disconnect();
                } else {
                    String url3 = url2.toString();
                    String a = cv.a((Readable) new InputStreamReader(httpURLConnection.getInputStream()));
                    a(url3, headerFields, a, responseCode);
                    clVar.a(url3, headerFields, a);
                    cf f = clVar.f(elapsedRealtime);
                    httpURLConnection.disconnect();
                    return f;
                }
            }
        } catch (IOException e) {
            da.w("Error while connecting to ad server: " + e.getMessage());
            return new cf(2);
        } catch (Throwable th) {
            httpURLConnection.disconnect();
            throw th;
        }
    }

    public static ci a(Context context, au auVar) {
        ci ciVar;
        synchronized (op) {
            if (oq == null) {
                oq = new ci(context.getApplicationContext(), auVar);
            }
            ciVar = oq;
        }
        return ciVar;
    }

    private static void a(String str, Map<String, List<String>> map, String str2, int i) {
        if (da.n(2)) {
            da.v("Http Response: {\n  URL:\n    " + str + "\n  Headers:");
            if (map != null) {
                for (String str3 : map.keySet()) {
                    da.v("    " + str3 + ":");
                    for (String str4 : (List) map.get(str3)) {
                        da.v("      " + str4);
                    }
                }
            }
            da.v("  Body:");
            if (str2 != null) {
                for (int i2 = 0; i2 < Math.min(str2.length(), 100000); i2 += 1000) {
                    da.v(str2.substring(i2, Math.min(str2.length(), i2 + 1000)));
                }
            } else {
                da.v("    null");
            }
            da.v("  Response Code:\n    " + i + "\n}");
        }
    }

    public cf b(cd cdVar) {
        return a(this.mContext, this.or, cdVar);
    }
}
