package com.google.sample.castcompanionlibrary.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.app.TaskStackBuilder;
import android.widget.RemoteViews;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.common.images.WebImage;
import com.google.sample.castcompanionlibrary.cast.BaseCastManager;
import com.google.sample.castcompanionlibrary.cast.VideoCastManager;
import com.google.sample.castcompanionlibrary.cast.callbacks.VideoCastConsumerImpl;
import com.google.sample.castcompanionlibrary.cast.exceptions.CastException;
import com.google.sample.castcompanionlibrary.cast.exceptions.NoConnectionException;
import com.google.sample.castcompanionlibrary.cast.exceptions.TransientNetworkDisconnectionException;
import com.google.sample.castcompanionlibrary.cast.player.VideoCastControllerActivity;
import com.google.sample.castcompanionlibrary.utils.LogUtils;
import com.google.sample.castcompanionlibrary.utils.Utils;
import com.motorola.zap.R;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class VideoCastNotificationService extends Service {
    public static final String ACTION_STOP = "com.google.sample.castcompanionlibrary.action.stop";
    public static final String ACTION_TOGGLE_PLAYBACK = "com.google.sample.castcompanionlibrary.action.toggleplayback";
    public static final String ACTION_VISIBILITY = "com.google.sample.castcompanionlibrary.action.notificationvisibility";
    /* access modifiers changed from: private */
    public static int NOTIFICATION_ID = 1;
    /* access modifiers changed from: private */
    public static final String TAG = LogUtils.makeLogTag(VideoCastNotificationService.class);
    private String mApplicationId;
    private DecodeVideoArtBitmapTask mBitmapDecoderTask;
    private BroadcastReceiver mBroadcastReceiver;
    /* access modifiers changed from: private */
    public VideoCastManager mCastManager;
    private VideoCastConsumerImpl mConsumer;
    private String mDataNamespace;
    boolean mIsIcsOrAbove;
    /* access modifiers changed from: private */
    public boolean mIsPlaying;
    /* access modifiers changed from: private */
    public Notification mNotification;
    private int mStatus;
    /* access modifiers changed from: private */
    public Class<?> mTargetActivity;
    /* access modifiers changed from: private */
    public Bitmap mVideoArtBitmap;
    /* access modifiers changed from: private */
    public Uri mVideoArtUri;
    /* access modifiers changed from: private */
    public boolean mVisible;

    private class DecodeVideoArtBitmapTask extends AsyncTask<MediaInfo, Void, Void> {
        private MediaInfo mInfo;

        private DecodeVideoArtBitmapTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(MediaInfo... info) {
            this.mInfo = info[0];
            if (this.mInfo.getMetadata().hasImages()) {
                Uri imgUri = ((WebImage) this.mInfo.getMetadata().getImages().get(0)).getUrl();
                if (!imgUri.equals(VideoCastNotificationService.this.mVideoArtUri)) {
                    URL imgUrl = null;
                    try {
                        URL imgUrl2 = new URL(imgUri.toString());
                        try {
                            VideoCastNotificationService.this.mVideoArtBitmap = BitmapFactory.decodeStream(imgUrl2.openStream());
                            VideoCastNotificationService.this.mVideoArtUri = imgUri;
                            URL url = imgUrl2;
                        } catch (MalformedURLException e) {
                            e = e;
                            imgUrl = imgUrl2;
                            LogUtils.LOGE(VideoCastNotificationService.TAG, "setIcon(): Failed to load the image with url: " + imgUrl + ", using the default one", e);
                            return null;
                        } catch (IOException e2) {
                            e = e2;
                            imgUrl = imgUrl2;
                            LogUtils.LOGE(VideoCastNotificationService.TAG, "setIcon(): Failed to load the image with url: " + imgUrl + ", using the default one", e);
                            return null;
                        }
                    } catch (MalformedURLException e3) {
                        e = e3;
                        LogUtils.LOGE(VideoCastNotificationService.TAG, "setIcon(): Failed to load the image with url: " + imgUrl + ", using the default one", e);
                        return null;
                    } catch (IOException e4) {
                        e = e4;
                        LogUtils.LOGE(VideoCastNotificationService.TAG, "setIcon(): Failed to load the image with url: " + imgUrl + ", using the default one", e);
                        return null;
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void v) {
            try {
                if (!this.mInfo.getMetadata().hasImages()) {
                    VideoCastNotificationService.this.build(this.mInfo, null, VideoCastNotificationService.this.mIsPlaying, VideoCastNotificationService.this.mTargetActivity);
                } else {
                    VideoCastNotificationService.this.build(this.mInfo, VideoCastNotificationService.this.mVideoArtBitmap, VideoCastNotificationService.this.mIsPlaying, VideoCastNotificationService.this.mTargetActivity);
                }
            } catch (CastException e) {
                LogUtils.LOGE(VideoCastNotificationService.TAG, "Failed to set notification for " + this.mInfo.toString(), e);
            } catch (TransientNetworkDisconnectionException e2) {
                LogUtils.LOGE(VideoCastNotificationService.TAG, "Failed to set notification for " + this.mInfo.toString(), e2);
            } catch (NoConnectionException e3) {
                LogUtils.LOGE(VideoCastNotificationService.TAG, "Failed to set notification for " + this.mInfo.toString(), e3);
            }
            if (VideoCastNotificationService.this.mVisible) {
                VideoCastNotificationService.this.startForeground(VideoCastNotificationService.NOTIFICATION_ID, VideoCastNotificationService.this.mNotification);
            }
        }
    }

    public VideoCastNotificationService() {
        boolean z;
        if (VERSION.SDK_INT >= 14) {
            z = true;
        } else {
            z = false;
        }
        this.mIsIcsOrAbove = z;
    }

    public void onCreate() {
        super.onCreate();
        LogUtils.LOGD(TAG, "onCreate()");
        IntentFilter filter = new IntentFilter("android.intent.action.SCREEN_ON");
        filter.addAction("android.intent.action.SCREEN_OFF");
        this.mBroadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                LogUtils.LOGD(VideoCastNotificationService.TAG, "onReceive(): " + intent.getAction());
            }
        };
        registerReceiver(this.mBroadcastReceiver, filter);
        readPersistedData();
        this.mCastManager = VideoCastManager.initialize(this, this.mApplicationId, this.mTargetActivity, this.mDataNamespace);
        if (!this.mCastManager.isConnected()) {
            this.mCastManager.reconnectSessionIfPossible(this, false);
        }
        this.mConsumer = new VideoCastConsumerImpl() {
            public void onApplicationDisconnected(int errorCode) {
                LogUtils.LOGD(VideoCastNotificationService.TAG, "onApplicationDisconnected() was reached");
                VideoCastNotificationService.this.stopSelf();
            }

            public void onRemoteMediaPlayerStatusUpdated() {
                VideoCastNotificationService.this.onRemoteMediaPlayerStatusUpdated(VideoCastNotificationService.this.mCastManager.getPlaybackStatus());
            }
        };
        this.mCastManager.addVideoCastConsumer(this.mConsumer);
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtils.LOGD(TAG, "onStartCommand");
        if (intent != null) {
            String action = intent.getAction();
            if (ACTION_TOGGLE_PLAYBACK.equals(action) && this.mIsIcsOrAbove) {
                LogUtils.LOGD(TAG, "onStartCommand(): Action: ACTION_TOGGLE_PLAYBACK");
                togglePlayback();
            } else if (ACTION_STOP.equals(action) && this.mIsIcsOrAbove) {
                LogUtils.LOGD(TAG, "onStartCommand(): Action: ACTION_STOP");
                stopApplication();
            } else if (ACTION_VISIBILITY.equals(action)) {
                this.mVisible = intent.getBooleanExtra("visible", false);
                LogUtils.LOGD(TAG, "onStartCommand(): Action: ACTION_VISIBILITY " + this.mVisible);
                if (!this.mVisible || this.mNotification == null) {
                    stopForeground(true);
                } else {
                    startForeground(NOTIFICATION_ID, this.mNotification);
                    this.mCastManager.setContext(this);
                }
            } else {
                LogUtils.LOGD(TAG, "onStartCommand(): Action: none");
            }
        } else {
            LogUtils.LOGD(TAG, "onStartCommand(): Intent was null");
        }
        return 3;
    }

    private void setupNotification(MediaInfo info) throws TransientNetworkDisconnectionException, NoConnectionException {
        if (info != null) {
            if (this.mBitmapDecoderTask != null) {
                this.mBitmapDecoderTask.cancel(false);
            }
            this.mBitmapDecoderTask = new DecodeVideoArtBitmapTask();
            this.mBitmapDecoderTask.execute(new MediaInfo[]{info});
        }
    }

    private void removeNotification() {
        ((NotificationManager) getSystemService("notification")).cancel(NOTIFICATION_ID);
    }

    /* access modifiers changed from: private */
    public void onRemoteMediaPlayerStatusUpdated(int mediaStatus) {
        this.mStatus = mediaStatus;
        LogUtils.LOGD(TAG, "onRemoteMediaPlayerMetadataUpdated() reached with status: " + this.mStatus);
        switch (mediaStatus) {
            case 0:
                this.mIsPlaying = false;
                stopForeground(true);
                return;
            case 1:
                this.mIsPlaying = false;
                if (!this.mCastManager.shouldRemoteUiBeVisible(mediaStatus, this.mCastManager.getIdleReason())) {
                    stopForeground(true);
                    return;
                } else {
                    setupNotification(this.mCastManager.getRemoteMediaInformation());
                    return;
                }
            case 2:
                this.mIsPlaying = true;
                setupNotification(this.mCastManager.getRemoteMediaInformation());
                return;
            case 3:
                this.mIsPlaying = false;
                setupNotification(this.mCastManager.getRemoteMediaInformation());
                return;
            case 4:
                try {
                    this.mIsPlaying = false;
                    setupNotification(this.mCastManager.getRemoteMediaInformation());
                    return;
                } catch (TransientNetworkDisconnectionException e) {
                    LogUtils.LOGE(TAG, "Failed to update the playback status due to network issues", e);
                    return;
                } catch (NoConnectionException e2) {
                    LogUtils.LOGE(TAG, "Failed to update the playback status due to network issues", e2);
                    return;
                }
            default:
                return;
        }
    }

    public void onDestroy() {
        if (this.mBitmapDecoderTask != null) {
            this.mBitmapDecoderTask.cancel(false);
        }
        LogUtils.LOGD(TAG, "onDestroy was called");
        removeNotification();
        if (this.mBroadcastReceiver != null) {
            unregisterReceiver(this.mBroadcastReceiver);
        }
        if (this.mCastManager != null && this.mConsumer != null) {
            this.mCastManager.removeVideoCastConsumer(this.mConsumer);
            this.mCastManager = null;
        }
    }

    /* access modifiers changed from: private */
    public RemoteViews build(MediaInfo mediaInfo, Bitmap bitmap, boolean z, Class<?> cls) throws CastException, TransientNetworkDisconnectionException, NoConnectionException {
        Bundle fromMediaInfo = Utils.fromMediaInfo(this.mCastManager.getRemoteMediaInformation());
        if (this.mTargetActivity == null) {
            this.mTargetActivity = VideoCastControllerActivity.class;
        }
        Intent intent = new Intent(this, this.mTargetActivity);
        intent.putExtra(VideoCastManager.EXTRA_MEDIA, fromMediaInfo);
        TaskStackBuilder create = TaskStackBuilder.create(this);
        create.addParentStack(this.mTargetActivity);
        create.addNextIntent(intent);
        if (create.getIntentCount() > 1) {
            create.editIntentAt(1).putExtra(VideoCastManager.EXTRA_MEDIA, fromMediaInfo);
        }
        PendingIntent pendingIntent = create.getPendingIntent(NOTIFICATION_ID, 134217728);
        MediaMetadata metadata = mediaInfo.getMetadata();
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.custom_notification);
        if (this.mIsIcsOrAbove) {
            addPendingIntents(remoteViews, z, mediaInfo);
        }
        if (bitmap != null) {
            remoteViews.setImageViewBitmap(R.id.iconView, bitmap);
        }
        remoteViews.setTextViewText(R.id.titleView, metadata.getString(MediaMetadata.KEY_TITLE));
        remoteViews.setTextViewText(R.id.subTitleView, getResources().getString(R.string.casting_to_device, new Object[]{this.mCastManager.getDeviceName()}));
        this.mNotification = new Builder(this).setSmallIcon(R.drawable.ic_stat_action_notification).setContentIntent(pendingIntent).setContent(remoteViews).setAutoCancel(false).setOngoing(true).build();
        this.mNotification.contentView = remoteViews;
        return remoteViews;
    }

    private void addPendingIntents(RemoteViews rv, boolean isPlaying, MediaInfo info) {
        Intent playbackIntent = new Intent(ACTION_TOGGLE_PLAYBACK);
        playbackIntent.setPackage(getPackageName());
        PendingIntent playbackPendingIntent = PendingIntent.getBroadcast(this, 0, playbackIntent, 0);
        Intent stopIntent = new Intent(ACTION_STOP);
        stopIntent.setPackage(getPackageName());
        PendingIntent stopPendingIntent = PendingIntent.getBroadcast(this, 0, stopIntent, 0);
        rv.setOnClickPendingIntent(R.id.playPauseView, playbackPendingIntent);
        rv.setOnClickPendingIntent(R.id.removeView, stopPendingIntent);
        if (!isPlaying) {
            rv.setImageViewResource(R.id.playPauseView, R.drawable.ic_av_play_sm_dark);
        } else if (info.getStreamType() == 2) {
            rv.setImageViewResource(R.id.playPauseView, R.drawable.ic_av_stop_sm_dark);
        } else {
            rv.setImageViewResource(R.id.playPauseView, R.drawable.ic_av_pause_sm_dark);
        }
    }

    private void togglePlayback() {
        try {
            this.mCastManager.togglePlayback();
        } catch (Exception e) {
            LogUtils.LOGE(TAG, "Failed to toggle the playback", e);
        }
    }

    private void stopApplication() {
        try {
            LogUtils.LOGD(TAG, "Calling stopApplication");
            this.mCastManager.disconnect();
        } catch (Exception e) {
            LogUtils.LOGE(TAG, "Failed to disconnect application", e);
        }
        stopSelf();
    }

    private void readPersistedData() {
        this.mApplicationId = Utils.getStringFromPreference(this, BaseCastManager.PREFS_KEY_APPLICATION_ID);
        String stringFromPreference = Utils.getStringFromPreference(this, BaseCastManager.PREFS_KEY_CAST_ACTIVITY_NAME);
        this.mDataNamespace = Utils.getStringFromPreference(this, BaseCastManager.PREFS_KEY_CAST_CUSTOM_DATA_NAMESPACE);
        if (stringFromPreference != null) {
            try {
                this.mTargetActivity = Class.forName(stringFromPreference);
            } catch (ClassNotFoundException e) {
                LogUtils.LOGE(TAG, "Failed to find the targetActivity class", e);
            }
        } else {
            this.mTargetActivity = VideoCastControllerActivity.class;
        }
    }
}
