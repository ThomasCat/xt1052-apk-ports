package com.motorola.zap.app;

import android.os.Handler;
import android.os.Looper;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener;
import com.motorola.sharing.GroupManager.GroupInfo;
import com.motorola.zap.zz.ui.BaseZzUiHandler;

public class ZzUtil {
    public static void ignoreZoneInvite(GroupListener handler, final int zzInviteUid) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                ZzInviteStore.remove(zzInviteUid);
                BaseZzUiHandler.cancelZzInviteNotification(zzInviteUid);
            }
        });
    }

    public static boolean isUserOwner(GroupInfo groupInfo) {
        if (groupInfo.owner == null) {
            return false;
        }
        return groupInfo.owner.equals(GCSP.getAuthenticator().getAccount());
    }
}
