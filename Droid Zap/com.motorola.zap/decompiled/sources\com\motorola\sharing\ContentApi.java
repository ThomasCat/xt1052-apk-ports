package com.motorola.sharing;

import com.motorola.sharing.ContentContainer.DiscoveryParams;

public interface ContentApi {
    void cancelDownload(ContentContainer contentContainer);

    void cancelSharing(ContentContainer contentContainer);

    void discoverContent(DiscoveryParams discoveryParams, ContentDiscoveryListener contentDiscoveryListener, Endpoint[] endpointArr);

    void downloadContent(ContentContainer contentContainer, ContentDownloadListener contentDownloadListener);

    void reportAbuse(ContentContainer contentContainer, ReportAbuseListener reportAbuseListener);

    void resumeSharing(ContentContainer contentContainer, ContentShareListener contentShareListener);

    void shareContent(ContentContainer contentContainer, ContentShareListener contentShareListener, Endpoint[] endpointArr);

    void stopDiscovery(DiscoveryParams discoveryParams);

    void stopSharing(ContentContainer contentContainer);

    void stopSharing(String str, ContentShareListener contentShareListener);
}
