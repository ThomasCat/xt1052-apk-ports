package com.motorola.sharing.provider.cloud.group.request;

import android.content.Intent;
import android.text.TextUtils;
import com.motorola.gcsp.json.StatusCode;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener.GroupEvent.Substatus;
import com.motorola.sharing.GroupListener.GroupEvent.Type;
import com.motorola.sharing.GroupManager.CreateParams;
import com.motorola.sharing.Settings;
import com.motorola.sharing.provider.cloud.GroupReceiver;
import com.motorola.sharing.provider.cloud.HttpHelper;
import com.motorola.sharing.provider.cloud.UrlHelper;
import com.motorola.sharing.provider.cloud.group.GroupHandler;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker.GroupState;
import com.motorola.sharing.util.FileUtils;
import com.motorola.sharing.util.HistoryManager.Shares.C;
import com.motorola.sharing.util.JsonHelper;
import com.motorola.sharing.util.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class GroupCreateRequest extends GroupRequest {
    private static final String TAG = Logger.getCloudTag("GroupCreateReq");
    private CreateParams mParams;
    private String mThumbnail;

    private GroupCreateRequest(CreateParams params, boolean isRetry) {
        super(GCSP.getContext(), isRetry);
        this.mThumbnail = null;
        this.mParams = params;
    }

    private GroupCreateRequest(GroupCreateRequest request, boolean isRetry) {
        this(request.mParams, isRetry);
    }

    public GroupCreateRequest(CreateParams params) {
        this(params, false);
    }

    public Type getWhat() {
        return Type.GROUP_CREATE;
    }

    public Substatus checkPreconditions() {
        if (!isNetworkConnected()) {
            return Substatus.NETWORK_ERROR;
        }
        return super.checkPreconditions();
    }

    public Intent getRetryIntent() {
        Intent intent = new Intent(GroupReceiver.ACTION_GROUP_CREATE);
        intent.putExtra(GroupReceiver.EXTRA_GROUP_PARAMS, this.mParams);
        if (isRetry()) {
            return null;
        }
        return intent;
    }

    public void run() {
        Logger.d(TAG, String.format("Running request, retry: %b", new Object[]{Boolean.valueOf(isRetry())}));
        GroupState groupState = this.mGroupStateTracker.getState();
        if (groupState.isActive() && TextUtils.equals(groupState.getName(), this.mParams.name) && TextUtils.equals(groupState.getPin(), this.mParams.pin) && TextUtils.equals(groupState.getOwner(), GCSP.getAuthenticator().getAccount())) {
            Logger.d(TAG, "Group already created - gid: " + groupState.getId());
        } else if (!authenticate()) {
            Logger.e(TAG, "Authentication error");
            sendError(Substatus.AUTHENTICATION_ERROR);
        } else {
            String requestData = createRequestData();
            if (requestData == null) {
                Logger.e(TAG, "Failed to construct request");
                sendError(Substatus.CLIENT_ERROR);
                return;
            }
            String response = null;
            try {
                Logger.d(TAG, "Sending request to server...");
                response = HttpHelper.sendPostRequest(UrlHelper.createTarget(), requestData, this.mUserToken, this.mUserId, true);
            } catch (Exception e) {
                Logger.e(TAG, "Exception sending request to server: " + e);
            }
            if (response == null) {
                Logger.e(TAG, "Request to server failed");
                sendError(Substatus.NETWORK_ERROR);
                return;
            }
            handleServerResponse(response);
        }
    }

    private String createRequestData() {
        JSONObject jsonObj = new JSONObject();
        try {
            JsonHelper.addCommonRequestFields(jsonObj);
            JSONObject shareFilter = new JSONObject();
            jsonObj.put("shareFilter", shareFilter);
            if (this.mParams.locality != null) {
                JSONObject location = new JSONObject();
                shareFilter.put("location", location);
                location.put("lat", this.mParams.locality.latitude);
                location.put("lng", this.mParams.locality.longitude);
                location.put("distance", this.mParams.locality.distance);
            }
            if (!TextUtils.isEmpty(this.mParams.pin)) {
                JSONObject pin = new JSONObject();
                shareFilter.put(C.PIN, pin);
                pin.put(C.PIN, this.mParams.pin);
            }
            jsonObj.put("targetType", "group");
            JSONObject targetMeta = new JSONObject();
            jsonObj.put("targetMeta", targetMeta);
            targetMeta.put("targetName", this.mParams.name);
            targetMeta.put("targetTTL", Settings.GROUP_INVITE_TTL.get());
            if (this.mParams.imagePath != null) {
                this.mThumbnail = FileUtils.getBase64Thumbnail(this.mParams.imagePath, 180, 180);
                if (this.mThumbnail != null) {
                    targetMeta.put("targetThumbnail", this.mThumbnail);
                }
            }
            Logger.d(TAG, "Server Request:");
            Logger.dump(TAG, jsonObj);
            return jsonObj.toString();
        } catch (JSONException e) {
            return null;
        }
    }

    private void handleServerResponse(String response) {
        try {
            JSONObject responseJson = new JSONObject(response);
            Logger.d(TAG, "Server Response:");
            Logger.dump(TAG, responseJson);
            StatusCode status = StatusCode.codeOf(responseJson.getString("status"));
            switch (status) {
                case OK:
                    handleOk(responseJson);
                    return;
                case ERR_009:
                case ERR_040:
                    if (!isRetry()) {
                        handleAlreadyInZone(responseJson);
                        return;
                    }
                    break;
            }
            Logger.e(TAG, "Request failed with status: " + status);
            sendError(statusToError(status));
        } catch (Exception e) {
            Logger.e(TAG, "Exception parsing JSON response: " + e);
            sendError(Substatus.SERVER_ERROR);
        }
    }

    private void handleOk(JSONObject responseJson) throws JSONException {
        String owner = null;
        String id = null;
        if (responseJson.has("targetOwner")) {
            owner = responseJson.getString("targetOwner");
        }
        if (responseJson.has("targetId")) {
            id = responseJson.getString("targetId");
        }
        if (!GroupStateTracker.isValidGroup(owner, id)) {
            Logger.e(TAG, "Missing mandatory param: owner: " + Logger.obfuscate(owner) + "; gid: " + id);
            sendError(Substatus.SERVER_ERROR);
            return;
        }
        onGroupStarted(owner, id, this.mParams.name, this.mParams.pin, this.mThumbnail, this.mParams.locality);
        onGroupInviteSent();
        Logger.d(TAG, "Group created - gid: " + id);
        sendOk();
    }

    private void handleAlreadyInZone(JSONObject responseJson) throws JSONException {
        JSONObject target = responseJson.getJSONArray("activeTargets").getJSONObject(0);
        String id = target.getString("targetId");
        String owner = target.getJSONObject("targetMeta").getString("targetOwner");
        Logger.w(TAG, "Still in an active group, quitting it first");
        GroupHandler.getInstance().leaveGroup(owner, id);
        GroupHandler.getInstance().retry(new GroupCreateRequest(this, true));
    }
}
