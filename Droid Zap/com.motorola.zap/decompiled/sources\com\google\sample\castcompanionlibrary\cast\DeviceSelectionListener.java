package com.google.sample.castcompanionlibrary.cast;

import android.support.v7.media.MediaRouter.RouteInfo;
import com.google.android.gms.cast.CastDevice;

public interface DeviceSelectionListener {
    void onCastDeviceDetected(RouteInfo routeInfo);

    void onDeviceSelected(CastDevice castDevice);
}
