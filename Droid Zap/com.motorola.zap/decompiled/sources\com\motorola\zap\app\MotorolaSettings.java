package com.motorola.zap.app;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import com.motorola.zap.LogUtils;
import java.lang.reflect.Method;

public class MotorolaSettings {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private static MotorolaSettings sInstance;
    private final Context mContext;
    private Method mGetInt;
    private Method mGetIntNoDefault;
    private Method mGetUriFor;
    private Method mPutInt;

    public static synchronized MotorolaSettings getInstance() {
        MotorolaSettings motorolaSettings;
        synchronized (MotorolaSettings.class) {
            if (sInstance == null) {
                sInstance = new MotorolaSettings(App.getAppContext());
            }
            motorolaSettings = sInstance;
        }
        return motorolaSettings;
    }

    private MotorolaSettings(Context context) {
        this.mContext = context;
        try {
            Class cls = Class.forName("com.motorola.android.provider.MotorolaSettings");
            this.mGetInt = cls.getMethod("getInt", new Class[]{ContentResolver.class, String.class, Integer.TYPE});
            this.mPutInt = cls.getMethod("putInt", new Class[]{ContentResolver.class, String.class, Integer.TYPE});
            this.mGetIntNoDefault = cls.getMethod("getInt", new Class[]{ContentResolver.class, String.class});
            this.mGetUriFor = cls.getMethod("getUriFor", new Class[]{String.class});
        } catch (Throwable th) {
            if (Log.isLoggable("Zap-MotoSettings", 6)) {
                Log.e("Zap-MotoSettings", "MotorolaSettings class error: ", th);
            }
        }
    }

    public int isIntSettingAvailable(String name) {
        if (this.mGetIntNoDefault != null) {
            try {
                this.mGetIntNoDefault.invoke(null, new Object[]{this.mContext.getContentResolver(), name});
                return 1;
            } catch (Throwable th) {
                if (DBG) {
                    Log.d("Zap-MotoSettings", "MotorolaSettings - Setting not available for: " + name);
                }
            }
        }
        return 0;
    }

    public int getInt(String name, int defaultValue) {
        try {
            if (this.mGetInt == null) {
                return defaultValue;
            }
            return ((Integer) this.mGetInt.invoke(null, new Object[]{this.mContext.getContentResolver(), name, Integer.valueOf(defaultValue)})).intValue();
        } catch (Throwable ex) {
            if (!Log.isLoggable("Zap-MotoSettings", 6)) {
                return defaultValue;
            }
            Log.e("Zap-MotoSettings", "MotorolaSettings - getInt error: ", ex);
            return defaultValue;
        }
    }

    public boolean putInt(String name, int value) {
        try {
            if (this.mPutInt != null) {
                return ((Boolean) this.mPutInt.invoke(null, new Object[]{this.mContext.getContentResolver(), name, Integer.valueOf(value)})).booleanValue();
            }
        } catch (Throwable ex) {
            if (Log.isLoggable("Zap-MotoSettings", 6)) {
                Log.e("Zap-MotoSettings", "MotorolaSettings - putInt error: ", ex);
            }
        }
        return false;
    }

    public Uri getUriFor(String name) {
        try {
            if (this.mGetUriFor != null) {
                return (Uri) this.mGetUriFor.invoke(null, new Object[]{name});
            }
        } catch (Throwable ex) {
            if (Log.isLoggable("Zap-MotoSettings", 6)) {
                Log.e("Zap-MotoSettings", "MotorolaSettings - getUriFor error: ", ex);
            }
        }
        return null;
    }
}
