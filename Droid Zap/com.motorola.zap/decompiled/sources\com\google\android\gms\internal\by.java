package com.google.android.gms.internal;

import android.content.Context;

public final class by {

    public interface a {
        void a(cn cnVar);
    }

    public static ct a(Context context, com.google.android.gms.internal.cd.a aVar, l lVar, dd ddVar, bf bfVar, a aVar2) {
        bz bzVar = new bz(context, aVar, lVar, ddVar, bfVar, aVar2);
        bzVar.start();
        return bzVar;
    }
}
