package com.google.tagmanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import com.google.android.gms.common.util.VisibleForTesting;
import com.motorola.zap.ZapConstants;
import java.util.HashMap;
import java.util.Map;

class InstallReferrerUtil {
    @VisibleForTesting
    static Map<String, String> clickReferrers = new HashMap();
    private static String installReferrer;

    InstallReferrerUtil() {
    }

    static void cacheInstallReferrer(String referrer) {
        synchronized (InstallReferrerUtil.class) {
            installReferrer = referrer;
        }
    }

    static void saveInstallReferrer(Context context, String referrer) {
        SharedPreferencesUtil.saveAsync(context, "gtm_install_referrer", "referrer", referrer);
        addClickReferrer(context, referrer);
    }

    static String getInstallReferrer(Context context, String component) {
        if (installReferrer == null) {
            synchronized (InstallReferrerUtil.class) {
                if (installReferrer == null) {
                    SharedPreferences settings = context.getSharedPreferences("gtm_install_referrer", 0);
                    if (settings != null) {
                        installReferrer = settings.getString("referrer", ZapConstants.GOOGLE_ACCOUNT_NONE);
                    } else {
                        installReferrer = ZapConstants.GOOGLE_ACCOUNT_NONE;
                    }
                }
            }
        }
        return extractComponent(installReferrer, component);
    }

    static void addClickReferrer(Context context, String referrer) {
        String conversionId = extractComponent(referrer, "conv");
        if (conversionId != null && conversionId.length() > 0) {
            clickReferrers.put(conversionId, referrer);
            SharedPreferencesUtil.saveAsync(context, "gtm_click_referrers", conversionId, referrer);
        }
    }

    static String extractComponent(String referrer, String component) {
        if (component != null) {
            return Uri.parse("http://hostname/?" + referrer).getQueryParameter(component);
        }
        if (referrer.length() > 0) {
            return referrer;
        }
        return null;
    }

    static String getClickReferrer(Context context, String conversionId, String component) {
        String referrer = (String) clickReferrers.get(conversionId);
        if (referrer == null) {
            SharedPreferences settings = context.getSharedPreferences("gtm_click_referrers", 0);
            if (settings != null) {
                referrer = settings.getString(conversionId, ZapConstants.GOOGLE_ACCOUNT_NONE);
            } else {
                referrer = ZapConstants.GOOGLE_ACCOUNT_NONE;
            }
            clickReferrers.put(conversionId, referrer);
        }
        return extractComponent(referrer, component);
    }
}
