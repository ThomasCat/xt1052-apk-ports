package com.google.android.gms.cast;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.dr;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CastDevice implements SafeParcelable {
    public static final Creator<CastDevice> CREATOR = new b();
    private String wC;
    String wD;
    private Inet4Address wE;
    private String wF;
    private String wG;
    private String wH;
    private int wI;
    private List<WebImage> wJ;
    private final int wj;

    private CastDevice() {
        this(1, null, null, null, null, null, -1, new ArrayList());
    }

    CastDevice(int versionCode, String deviceId, String hostAddress, String friendlyName, String modelName, String deviceVersion, int servicePort, List<WebImage> icons) {
        this.wj = versionCode;
        this.wC = deviceId;
        this.wD = hostAddress;
        if (this.wD != null) {
            try {
                InetAddress byName = InetAddress.getByName(this.wD);
                if (byName instanceof Inet4Address) {
                    this.wE = (Inet4Address) byName;
                }
            } catch (UnknownHostException e) {
                this.wE = null;
            }
        }
        this.wF = friendlyName;
        this.wG = modelName;
        this.wH = deviceVersion;
        this.wI = servicePort;
        this.wJ = icons;
    }

    public static CastDevice getFromBundle(Bundle extras) {
        if (extras == null) {
            return null;
        }
        extras.setClassLoader(CastDevice.class.getClassLoader());
        return (CastDevice) extras.getParcelable("com.google.android.gms.cast.EXTRA_CAST_DEVICE");
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof CastDevice)) {
            return false;
        }
        CastDevice castDevice = (CastDevice) obj;
        return getDeviceId() == null ? castDevice.getDeviceId() == null : dr.a(this.wC, castDevice.wC) && dr.a(this.wE, castDevice.wE) && dr.a(this.wG, castDevice.wG) && dr.a(this.wF, castDevice.wF) && dr.a(this.wH, castDevice.wH) && this.wI == castDevice.wI && dr.a(this.wJ, castDevice.wJ);
    }

    public String getDeviceId() {
        return this.wC;
    }

    public String getDeviceVersion() {
        return this.wH;
    }

    public String getFriendlyName() {
        return this.wF;
    }

    public WebImage getIcon(int preferredWidth, int preferredHeight) {
        WebImage webImage;
        WebImage webImage2 = null;
        if (this.wJ.isEmpty()) {
            return null;
        }
        if (preferredWidth <= 0 || preferredHeight <= 0) {
            return (WebImage) this.wJ.get(0);
        }
        WebImage webImage3 = null;
        for (WebImage webImage4 : this.wJ) {
            int width = webImage4.getWidth();
            int height = webImage4.getHeight();
            if (width < preferredWidth || height < preferredHeight) {
                if (width < preferredWidth && height < preferredHeight && (webImage2 == null || (webImage2.getWidth() < width && webImage2.getHeight() < height))) {
                    webImage = webImage3;
                }
                webImage4 = webImage2;
                webImage = webImage3;
            } else {
                if (webImage3 == null || (webImage3.getWidth() > width && webImage3.getHeight() > height)) {
                    WebImage webImage5 = webImage2;
                    webImage = webImage4;
                    webImage4 = webImage5;
                }
                webImage4 = webImage2;
                webImage = webImage3;
            }
            webImage3 = webImage;
            webImage2 = webImage4;
        }
        if (webImage3 == null) {
            webImage3 = webImage2 != null ? webImage2 : (WebImage) this.wJ.get(0);
        }
        return webImage3;
    }

    public List<WebImage> getIcons() {
        return Collections.unmodifiableList(this.wJ);
    }

    public Inet4Address getIpAddress() {
        return this.wE;
    }

    public String getModelName() {
        return this.wG;
    }

    public int getServicePort() {
        return this.wI;
    }

    /* access modifiers changed from: 0000 */
    public int getVersionCode() {
        return this.wj;
    }

    public boolean hasIcons() {
        return !this.wJ.isEmpty();
    }

    public int hashCode() {
        if (this.wC == null) {
            return 0;
        }
        return this.wC.hashCode();
    }

    public boolean isSameDevice(CastDevice castDevice) {
        if (castDevice == null) {
            return false;
        }
        if (getDeviceId() == null) {
            return castDevice.getDeviceId() == null;
        }
        return dr.a(getDeviceId(), castDevice.getDeviceId());
    }

    public void putInBundle(Bundle bundle) {
        if (bundle != null) {
            bundle.putParcelable("com.google.android.gms.cast.EXTRA_CAST_DEVICE", this);
        }
    }

    public String toString() {
        return String.format("\"%s\" (%s)", new Object[]{this.wF, this.wC});
    }

    public void writeToParcel(Parcel out, int flags) {
        b.a(this, out, flags);
    }
}
