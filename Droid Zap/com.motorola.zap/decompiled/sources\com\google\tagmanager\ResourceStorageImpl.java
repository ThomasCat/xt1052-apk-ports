package com.google.tagmanager;

import android.content.Context;
import android.content.res.AssetManager;
import com.google.analytics.containertag.proto.Serving.Resource;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.tagmanager.LoadCallback.Failure;
import com.google.tagmanager.ResourceUtil.ExpandedResource;
import com.google.tagmanager.proto.Resource.ResourceWithMetadata;
import com.google.tagmanager.protobuf.nano.MessageNano;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONException;

class ResourceStorageImpl implements ResourceStorage {
    private LoadCallback<ResourceWithMetadata> mCallback;
    private final String mContainerId;
    private final Context mContext;
    private final ExecutorService mExecutor = Executors.newSingleThreadExecutor();

    ResourceStorageImpl(Context context, String containerId) {
        this.mContext = context;
        this.mContainerId = containerId;
    }

    public void setLoadCallback(LoadCallback<ResourceWithMetadata> callback) {
        this.mCallback = callback;
    }

    public void loadResourceFromDiskInBackground() {
        this.mExecutor.execute(new Runnable() {
            public void run() {
                ResourceStorageImpl.this.loadResourceFromDisk();
            }
        });
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void loadResourceFromDisk() {
        if (this.mCallback == null) {
            throw new IllegalStateException("callback must be set before execute");
        }
        this.mCallback.startLoad();
        Log.v("Start loading resource from disk ...");
        if ((PreviewManager.getInstance().getPreviewMode() == PreviewMode.CONTAINER || PreviewManager.getInstance().getPreviewMode() == PreviewMode.CONTAINER_DEBUG) && this.mContainerId.equals(PreviewManager.getInstance().getContainerId())) {
            this.mCallback.onFailure(Failure.NOT_AVAILABLE);
            return;
        }
        try {
            FileInputStream stream = new FileInputStream(getResourceFile());
            try {
                ByteArrayOutputStream input = new ByteArrayOutputStream();
                ResourceUtil.copyStream(stream, input);
                this.mCallback.onSuccess(ResourceWithMetadata.parseFrom(input.toByteArray()));
                try {
                } catch (IOException e) {
                    Log.w("error closing stream for reading resource from disk");
                }
            } catch (IOException e2) {
                Log.w("error reading resource from disk");
                this.mCallback.onFailure(Failure.IO_ERROR);
                try {
                } catch (IOException e3) {
                    Log.w("error closing stream for reading resource from disk");
                }
            } finally {
                try {
                    stream.close();
                } catch (IOException e4) {
                    Log.w("error closing stream for reading resource from disk");
                }
            }
            Log.v("Load resource from disk finished.");
        } catch (FileNotFoundException e5) {
            Log.d("resource not on disk");
            this.mCallback.onFailure(Failure.NOT_AVAILABLE);
        }
    }

    public void saveResourceToDiskInBackground(final ResourceWithMetadata resource) {
        this.mExecutor.execute(new Runnable() {
            public void run() {
                ResourceStorageImpl.this.saveResourceToDisk(resource);
            }
        });
    }

    public Resource loadResourceFromContainerAsset(String str) {
        Log.v("Loading default container from " + str);
        AssetManager assets = this.mContext.getAssets();
        if (assets == null) {
            Log.e("No assets found in package");
            return null;
        }
        try {
            InputStream open = assets.open(str);
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                ResourceUtil.copyStream(open, byteArrayOutputStream);
                Resource parseFrom = Resource.parseFrom(byteArrayOutputStream.toByteArray());
                Log.v("Parsed default container: " + parseFrom);
                return parseFrom;
            } catch (IOException e) {
                Log.w("Error when parsing: " + str);
                try {
                    return null;
                } catch (IOException e2) {
                    return null;
                }
            } finally {
                try {
                    open.close();
                } catch (IOException e3) {
                }
            }
        } catch (IOException e4) {
            Log.w("No asset file: " + str + " found.");
            return null;
        }
    }

    /* JADX WARNING: type inference failed for: r1v6, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r1v9 */
    /* JADX WARNING: type inference failed for: r2v1, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r1v11 */
    /* JADX WARNING: type inference failed for: r2v2 */
    /* JADX WARNING: type inference failed for: r1v16, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r1v19 */
    /* JADX WARNING: type inference failed for: r2v10 */
    /* JADX WARNING: type inference failed for: r1v24 */
    /* JADX WARNING: type inference failed for: r1v25 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0083 A[SYNTHETIC, Splitter:B:25:0x0083] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008f A[SYNTHETIC, Splitter:B:31:0x008f] */
    /* JADX WARNING: Unknown variable types count: 3 */
    public ExpandedResource loadExpandedResourceFromJsonAsset(String str) {
        ? r1;
        Throwable th;
        ? r2;
        ? r12;
        ExpandedResource expandedResource = 0;
        Log.v("loading default container from " + str);
        AssetManager assets = this.mContext.getAssets();
        if (assets == null) {
            Log.w("Looking for default JSON container in package, but no assets were found.");
            expandedResource = expandedResource;
        } else {
            try {
                InputStream open = assets.open(str);
                try {
                    expandedResource = JsonUtils.expandedResourceFromJsonString(stringFromInputStream(open));
                    if (open != 0) {
                        try {
                            open.close();
                        } catch (IOException e) {
                        }
                    }
                } catch (IOException e2) {
                    r12 = open;
                } catch (JSONException e3) {
                    Object obj = e3;
                    r2 = open;
                    e = obj;
                    try {
                        Log.w("Error parsing JSON file" + str + " : " + e);
                        if (r2 != 0) {
                            try {
                                r2.close();
                            } catch (IOException e4) {
                            }
                        }
                        return expandedResource;
                    } catch (Throwable th2) {
                        th = th2;
                        r1 = r2;
                        if (r1 != 0) {
                        }
                        throw th;
                    }
                }
            } catch (IOException e5) {
                r12 = expandedResource;
            } catch (JSONException e6) {
                e = e6;
                r2 = expandedResource;
                Log.w("Error parsing JSON file" + str + " : " + e);
                if (r2 != 0) {
                }
                return expandedResource;
            } catch (Throwable th3) {
                Throwable th4 = th3;
                r1 = expandedResource;
                th = th4;
                if (r1 != 0) {
                }
                throw th;
            }
        }
        return expandedResource;
        try {
            Log.w("No asset file: " + str + " found (or errors reading it).");
            if (r12 != 0) {
                try {
                    r12.close();
                } catch (IOException e7) {
                }
            }
            return expandedResource;
        } catch (Throwable th5) {
            th = th5;
            r1 = r12;
            if (r1 != 0) {
                try {
                    r1.close();
                } catch (IOException e8) {
                }
            }
            throw th;
        }
    }

    public synchronized void close() {
        this.mExecutor.shutdown();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean saveResourceToDisk(ResourceWithMetadata resourceWithMetadata) {
        boolean z = false;
        File resourceFile = getResourceFile();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(resourceFile);
            try {
                fileOutputStream.write(MessageNano.toByteArray(resourceWithMetadata));
                z = true;
                try {
                } catch (IOException e) {
                    Log.w("error closing stream for writing resource to disk");
                }
            } catch (IOException e2) {
                Log.w("Error writing resource to disk. Removing resource from disk.");
                resourceFile.delete();
                try {
                } catch (IOException e3) {
                    Log.w("error closing stream for writing resource to disk");
                }
            } finally {
                try {
                    fileOutputStream.close();
                } catch (IOException e4) {
                    Log.w("error closing stream for writing resource to disk");
                }
            }
        } catch (FileNotFoundException e5) {
            Log.e("Error opening resource file for writing");
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public File getResourceFile() {
        return new File(this.mContext.getDir("google_tagmanager", 0), "resource_" + this.mContainerId);
    }

    private String stringFromInputStream(InputStream inputStream) throws IOException {
        StringWriter stringWriter = new StringWriter();
        char[] cArr = new char[1024];
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        while (true) {
            int read = bufferedReader.read(cArr);
            if (read == -1) {
                return stringWriter.toString();
            }
            stringWriter.write(cArr, 0, read);
        }
    }
}
