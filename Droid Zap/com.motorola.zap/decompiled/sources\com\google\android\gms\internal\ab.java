package com.google.android.gms.internal;

import android.content.Context;
import android.os.Parcel;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.a;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class ab implements SafeParcelable {
    public static final ac CREATOR = new ac();
    public final int height;
    public final int heightPixels;
    public final String ln;
    public final boolean lo;
    public final ab[] lp;
    public final int versionCode;
    public final int width;
    public final int widthPixels;

    public ab() {
        this(2, "interstitial_mb", 0, 0, true, 0, 0, null);
    }

    ab(int i, String str, int i2, int i3, boolean z, int i4, int i5, ab[] abVarArr) {
        this.versionCode = i;
        this.ln = str;
        this.height = i2;
        this.heightPixels = i3;
        this.lo = z;
        this.width = i4;
        this.widthPixels = i5;
        this.lp = abVarArr;
    }

    public ab(Context context, AdSize adSize) {
        this(context, new AdSize[]{adSize});
    }

    public ab(Context context, AdSize[] adSizeArr) {
        int i;
        AdSize adSize = adSizeArr[0];
        this.versionCode = 2;
        this.lo = false;
        this.width = adSize.getWidth();
        this.height = adSize.getHeight();
        boolean z = this.width == -1;
        boolean z2 = this.height == -2;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        if (z) {
            this.widthPixels = a(displayMetrics);
            i = (int) (((float) this.widthPixels) / displayMetrics.density);
        } else {
            int i2 = this.width;
            this.widthPixels = cz.a(displayMetrics, this.width);
            i = i2;
        }
        int i3 = z2 ? c(displayMetrics) : this.height;
        this.heightPixels = cz.a(displayMetrics, i3);
        if (z || z2) {
            this.ln = i + "x" + i3 + "_as";
        } else {
            this.ln = adSize.toString();
        }
        if (adSizeArr.length > 1) {
            this.lp = new ab[adSizeArr.length];
            for (int i4 = 0; i4 < adSizeArr.length; i4++) {
                this.lp[i4] = new ab(context, adSizeArr[i4]);
            }
            return;
        }
        this.lp = null;
    }

    public ab(ab abVar, ab[] abVarArr) {
        this(2, abVar.ln, abVar.height, abVar.heightPixels, abVar.lo, abVar.width, abVar.widthPixels, abVarArr);
    }

    public static int a(DisplayMetrics displayMetrics) {
        return displayMetrics.widthPixels;
    }

    public static int b(DisplayMetrics displayMetrics) {
        return (int) (((float) c(displayMetrics)) * displayMetrics.density);
    }

    private static int c(DisplayMetrics displayMetrics) {
        int i = (int) (((float) displayMetrics.heightPixels) / displayMetrics.density);
        if (i <= 400) {
            return 32;
        }
        return i <= 720 ? 50 : 90;
    }

    public AdSize ai() {
        return a.a(this.width, this.height, this.ln);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        ac.a(this, out, flags);
    }
}
