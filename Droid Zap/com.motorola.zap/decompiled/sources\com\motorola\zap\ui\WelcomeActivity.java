package com.motorola.zap.ui;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.identity.ZapProfileSharedPerfs;

public class WelcomeActivity extends Activity {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;

    public static class WelcomeFragment extends Fragment {
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View welcome = inflater.inflate(R.layout.activity_welcome, container, false);
            ((Button) welcome.findViewById(R.id.start_tutorial_button)).setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    FragmentTransaction ft = WelcomeFragment.this.getFragmentManager().beginTransaction();
                    Fragment prev = WelcomeFragment.this.getFragmentManager().findFragmentByTag("welcome");
                    if (prev != null) {
                        ft.remove(prev);
                    }
                    ft.addToBackStack("welcome");
                    ft.replace(16908290, new WelcomePartOne(), "welcome").commit();
                }
            });
            return welcome;
        }
    }

    public static class WelcomePartOne extends Fragment {
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View welcomePartOne = inflater.inflate(R.layout.welcome_partone, container, false);
            ((Button) welcomePartOne.findViewById(R.id.welcome_next)).setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    FragmentTransaction ft = WelcomePartOne.this.getFragmentManager().beginTransaction();
                    Fragment prev = WelcomePartOne.this.getFragmentManager().findFragmentByTag("partone");
                    if (prev != null) {
                        ft.remove(prev);
                    }
                    ft.addToBackStack("partone");
                    ft.replace(16908290, new WelcomePartTwo(), "partone").commit();
                }
            });
            return welcomePartOne;
        }
    }

    public static class WelcomePartTwo extends Fragment {
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View welcomePartTwo = inflater.inflate(R.layout.activity_confirm_profile, container, false);
            Button notNow = (Button) welcomePartTwo.findViewById(R.id.zap_confirm_notNow);
            Button yesIn = (Button) welcomePartTwo.findViewById(R.id.zap_confirm_yesIn);
            TextView privacyPolicy = (TextView) welcomePartTwo.findViewById(R.id.accept_privacy_policy);
            privacyPolicy.setText(Html.fromHtml(getResources().getString(R.string.accept_privacy_title)));
            privacyPolicy.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    ZapUtils.showGlobalPrivacyPolicy(WelcomePartTwo.this.getActivity());
                }
            });
            notNow.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    WelcomePartTwo.this.getFragmentManager().popBackStack("welcome", 1);
                }
            });
            yesIn.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    ZapProfileSharedPerfs.ZAP_PROFILE_ACTIVATE.write(Boolean.valueOf(true));
                    WelcomePartTwo.this.getActivity().setResult(1);
                    WelcomePartTwo.this.getActivity().finish();
                }
            });
            return welcomePartTwo;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fm = getFragmentManager();
        WelcomeFragment welcomeFragment = new WelcomeFragment();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.add(16908290, welcomeFragment);
        transaction.commit();
    }
}
