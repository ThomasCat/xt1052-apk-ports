package com.google.sample.castcompanionlibrary.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.sample.castcompanionlibrary.cast.exceptions.CastException;
import com.google.sample.castcompanionlibrary.cast.exceptions.NoConnectionException;
import com.google.sample.castcompanionlibrary.cast.exceptions.OnFailedListener;
import com.google.sample.castcompanionlibrary.cast.exceptions.TransientNetworkDisconnectionException;
import com.google.sample.castcompanionlibrary.utils.LogUtils;
import com.motorola.zap.R;
import java.net.URL;

public class MiniController extends RelativeLayout implements IMiniController {
    public static final int IDLE = 3;
    public static final int PAUSE = 2;
    public static final int PLAYBACK = 1;
    private View mContainer;
    protected ImageView mIcon;
    /* access modifiers changed from: private */
    public Uri mIconUri;
    /* access modifiers changed from: private */
    public OnMiniControllerChangedListener mListener;
    protected ProgressBar mLoading;
    private Drawable mPauseDrawable;
    private Drawable mPlayDrawable;
    protected ImageView mPlayPause;
    private Drawable mStopDrawable;
    private int mStreamType = 1;
    protected TextView mSubTitle;
    protected TextView mTitle;

    public interface OnMiniControllerChangedListener extends OnFailedListener {
        void onPlayPauseClicked(View view) throws CastException, TransientNetworkDisconnectionException, NoConnectionException;

        void onTargetActivityInvoked(Context context) throws TransientNetworkDisconnectionException, NoConnectionException;
    }

    public MiniController(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.mini_controller, this);
        this.mPauseDrawable = getResources().getDrawable(R.drawable.ic_mini_controller_pause);
        this.mPlayDrawable = getResources().getDrawable(R.drawable.ic_mini_controller_play);
        this.mStopDrawable = getResources().getDrawable(R.drawable.ic_mini_controller_stop);
        loadViews();
        setupCallbacks();
    }

    public void setOnMiniControllerChangedListener(OnMiniControllerChangedListener listener) {
        if (listener != null) {
            this.mListener = listener;
        }
    }

    public void removeOnMiniControllerChangedListener(OnMiniControllerChangedListener listener) {
        if (listener != null && this.mListener == listener) {
            this.mListener = null;
        }
    }

    public void setStreamType(int streamType) {
        this.mStreamType = streamType;
    }

    private void setupCallbacks() {
        this.mPlayPause.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (MiniController.this.mListener != null) {
                    MiniController.this.setLoadingVisibility(true);
                    try {
                        MiniController.this.mListener.onPlayPauseClicked(v);
                    } catch (CastException e) {
                        MiniController.this.mListener.onFailed(R.string.failed_perform_action, -1);
                    } catch (TransientNetworkDisconnectionException e2) {
                        MiniController.this.mListener.onFailed(R.string.failed_no_connection_trans, -1);
                    } catch (NoConnectionException e3) {
                        MiniController.this.mListener.onFailed(R.string.failed_no_connection, -1);
                    }
                }
            }
        });
        this.mContainer.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (MiniController.this.mListener != null) {
                    MiniController.this.setLoadingVisibility(false);
                    try {
                        MiniController.this.mListener.onTargetActivityInvoked(MiniController.this.mIcon.getContext());
                    } catch (Exception e) {
                        MiniController.this.mListener.onFailed(R.string.failed_perform_action, -1);
                    }
                }
            }
        });
    }

    public MiniController(Context context) {
        super(context);
        loadViews();
    }

    public final void setIcon(Bitmap bm) {
        this.mIcon.setImageBitmap(bm);
    }

    public void setIcon(Uri uri) {
        if (this.mIconUri == null || !this.mIconUri.equals(uri)) {
            this.mIconUri = uri;
            new Thread(new Runnable() {
                Bitmap bm = null;

                public void run() {
                    try {
                        this.bm = BitmapFactory.decodeStream(new URL(MiniController.this.mIconUri.toString()).openStream());
                    } catch (Exception e) {
                        LogUtils.LOGE("MiniController", "setIcon(): Failed to load the image with url: " + MiniController.this.mIconUri + ", using the default one", e);
                        this.bm = BitmapFactory.decodeResource(MiniController.this.getResources(), R.drawable.dummy_album_art);
                    }
                    MiniController.this.mIcon.post(new Runnable() {
                        public void run() {
                            MiniController.this.setIcon(AnonymousClass3.this.bm);
                        }
                    });
                }
            }).start();
        }
    }

    public void setTitle(String title) {
        this.mTitle.setText(title);
    }

    public void setSubTitle(String subTitle) {
        this.mSubTitle.setText(subTitle);
    }

    public void setPlaybackStatus(int state, int idleReason) {
        switch (state) {
            case 1:
                switch (this.mStreamType) {
                    case 1:
                        this.mPlayPause.setVisibility(4);
                        setLoadingVisibility(false);
                        return;
                    case 2:
                        if (idleReason == 2) {
                            this.mPlayPause.setVisibility(0);
                            this.mPlayPause.setImageDrawable(this.mPlayDrawable);
                            setLoadingVisibility(false);
                            return;
                        }
                        this.mPlayPause.setVisibility(4);
                        setLoadingVisibility(false);
                        return;
                    default:
                        return;
                }
            case 2:
                this.mPlayPause.setVisibility(0);
                this.mPlayPause.setImageDrawable(getPauseStopButton());
                setLoadingVisibility(false);
                return;
            case 3:
                this.mPlayPause.setVisibility(0);
                this.mPlayPause.setImageDrawable(this.mPlayDrawable);
                setLoadingVisibility(false);
                return;
            case 4:
                this.mPlayPause.setVisibility(4);
                setLoadingVisibility(true);
                return;
            default:
                this.mPlayPause.setVisibility(4);
                setLoadingVisibility(false);
                return;
        }
    }

    public boolean isVisible() {
        return isShown();
    }

    private void loadViews() {
        this.mIcon = (ImageView) findViewById(R.id.iconView);
        this.mTitle = (TextView) findViewById(R.id.titleView);
        this.mSubTitle = (TextView) findViewById(R.id.subTitleView);
        this.mPlayPause = (ImageView) findViewById(R.id.playPauseView);
        this.mLoading = (ProgressBar) findViewById(R.id.loadingView);
        this.mContainer = findViewById(R.id.bigContainer);
    }

    /* access modifiers changed from: private */
    public void setLoadingVisibility(boolean show) {
        this.mLoading.setVisibility(show ? 0 : 8);
    }

    private Drawable getPauseStopButton() {
        switch (this.mStreamType) {
            case 1:
                return this.mPauseDrawable;
            case 2:
                return this.mStopDrawable;
            default:
                return this.mPauseDrawable;
        }
    }
}
