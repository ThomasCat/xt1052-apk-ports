package com.google.android.gms.drive.internal;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.a.C0003a;
import com.google.android.gms.drive.Drive;

abstract class m<R extends Result> extends C0003a<R, n> {
    public m() {
        super(Drive.va);
    }
}
