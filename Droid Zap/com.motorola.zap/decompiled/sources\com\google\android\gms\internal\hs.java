package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.location.LocationRequest;

public class hs implements Creator<hr> {
    static void a(hr hrVar, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.a(parcel, 1, (Parcelable) hrVar.gu(), i, false);
        b.c(parcel, 1000, hrVar.wj);
        b.a(parcel, 2, (Parcelable) hrVar.gv(), i, false);
        b.D(parcel, p);
    }

    /* renamed from: ay */
    public hr createFromParcel(Parcel parcel) {
        hn hnVar;
        LocationRequest locationRequest;
        int i;
        hn hnVar2 = null;
        int o = a.o(parcel);
        int i2 = 0;
        LocationRequest locationRequest2 = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    i = i2;
                    LocationRequest locationRequest3 = (LocationRequest) a.a(parcel, n, (Creator<T>) LocationRequest.CREATOR);
                    hnVar = hnVar2;
                    locationRequest = locationRequest3;
                    break;
                case 2:
                    hnVar = (hn) a.a(parcel, n, (Creator<T>) hn.CREATOR);
                    locationRequest = locationRequest2;
                    i = i2;
                    break;
                case 1000:
                    hn hnVar3 = hnVar2;
                    locationRequest = locationRequest2;
                    i = a.g(parcel, n);
                    hnVar = hnVar3;
                    break;
                default:
                    a.b(parcel, n);
                    hnVar = hnVar2;
                    locationRequest = locationRequest2;
                    i = i2;
                    break;
            }
            i2 = i;
            locationRequest2 = locationRequest;
            hnVar2 = hnVar;
        }
        if (parcel.dataPosition() == o) {
            return new hr(i2, locationRequest2, hnVar2);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }

    /* renamed from: bs */
    public hr[] newArray(int i) {
        return new hr[i];
    }
}
