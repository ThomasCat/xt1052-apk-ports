package com.google.android.gms.internal;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import java.util.ArrayList;

public class hy implements Creator<hx> {
    static void a(hx hxVar, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.a(parcel, 1, hxVar.getId(), false);
        b.a(parcel, 2, hxVar.gE(), false);
        b.a(parcel, 3, (Parcelable) hxVar.gF(), i, false);
        b.a(parcel, 4, (Parcelable) hxVar.gx(), i, false);
        b.a(parcel, 5, hxVar.gy());
        b.a(parcel, 6, (Parcelable) hxVar.gz(), i, false);
        b.a(parcel, 7, hxVar.gG(), false);
        b.a(parcel, 8, (Parcelable) hxVar.gA(), i, false);
        b.a(parcel, 9, hxVar.gB());
        b.a(parcel, 10, hxVar.getRating());
        b.c(parcel, 11, hxVar.gC());
        b.a(parcel, 12, hxVar.gD());
        b.b(parcel, 13, hxVar.gw(), false);
        b.c(parcel, 1000, hxVar.wj);
        b.D(parcel, p);
    }

    /* renamed from: aB */
    public hx createFromParcel(Parcel parcel) {
        int o = a.o(parcel);
        int i = 0;
        String str = null;
        ArrayList arrayList = null;
        Bundle bundle = null;
        hz hzVar = null;
        LatLng latLng = null;
        float f = BitmapDescriptorFactory.HUE_RED;
        LatLngBounds latLngBounds = null;
        String str2 = null;
        Uri uri = null;
        boolean z = false;
        float f2 = BitmapDescriptorFactory.HUE_RED;
        int i2 = 0;
        long j = 0;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    str = a.m(parcel, n);
                    break;
                case 2:
                    bundle = a.o(parcel, n);
                    break;
                case 3:
                    hzVar = (hz) a.a(parcel, n, (Creator<T>) hz.CREATOR);
                    break;
                case 4:
                    latLng = (LatLng) a.a(parcel, n, (Creator<T>) LatLng.CREATOR);
                    break;
                case 5:
                    f = a.j(parcel, n);
                    break;
                case 6:
                    latLngBounds = (LatLngBounds) a.a(parcel, n, (Creator<T>) LatLngBounds.CREATOR);
                    break;
                case 7:
                    str2 = a.m(parcel, n);
                    break;
                case 8:
                    uri = (Uri) a.a(parcel, n, Uri.CREATOR);
                    break;
                case 9:
                    z = a.c(parcel, n);
                    break;
                case 10:
                    f2 = a.j(parcel, n);
                    break;
                case 11:
                    i2 = a.g(parcel, n);
                    break;
                case 12:
                    j = a.h(parcel, n);
                    break;
                case 13:
                    arrayList = a.c(parcel, n, ht.CREATOR);
                    break;
                case 1000:
                    i = a.g(parcel, n);
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new hx(i, str, arrayList, bundle, hzVar, latLng, f, latLngBounds, str2, uri, z, f2, i2, j);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }

    /* renamed from: bw */
    public hx[] newArray(int i) {
        return new hx[i];
    }
}
