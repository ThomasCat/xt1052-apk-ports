package com.motorola.sharing.provider.tv.api;

import com.motorola.sharing.Endpoint;
import com.motorola.sharing.Endpoint.AttrValue;
import com.motorola.sharing.Endpoint.Attributes;

public class TVEndpoint extends Endpoint {
    public TVEndpoint() {
        super(new Attributes(AttrValue.STATIC, AttrValue.SHARE_ONLY));
    }

    public String getName() {
        return TVEndpoint.class.getCanonicalName();
    }
}
