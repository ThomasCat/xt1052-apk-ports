package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;

public class dc implements Creator<db> {
    static void a(db dbVar, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.c(parcel, 1, dbVar.versionCode);
        b.a(parcel, 2, dbVar.pU, false);
        b.c(parcel, 3, dbVar.pV);
        b.c(parcel, 4, dbVar.pW);
        b.a(parcel, 5, dbVar.pX);
        b.D(parcel, p);
    }

    /* renamed from: h */
    public db createFromParcel(Parcel parcel) {
        boolean z = false;
        int o = a.o(parcel);
        String str = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    i3 = a.g(parcel, n);
                    break;
                case 2:
                    str = a.m(parcel, n);
                    break;
                case 3:
                    i2 = a.g(parcel, n);
                    break;
                case 4:
                    i = a.g(parcel, n);
                    break;
                case 5:
                    z = a.c(parcel, n);
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new db(i3, str, i2, i, z);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }

    /* renamed from: o */
    public db[] newArray(int i) {
        return new db[i];
    }
}
