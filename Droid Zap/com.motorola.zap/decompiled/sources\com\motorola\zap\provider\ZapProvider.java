package com.motorola.zap.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import com.motorola.zap.provider.ZapContract.History;

public class ZapProvider extends ContentProvider {
    private static final UriMatcher sUriMatcher = new UriMatcher(-1);

    static {
        sUriMatcher.addURI(ZapContract.AUTHORITY, History.URI_PATH, 0);
    }

    private SQLiteDatabase getDb() {
        return DbOpenHelper.getInstance().getDb();
    }

    public boolean onCreate() {
        return true;
    }

    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case 0:
                return History.MIME_TYPE;
            default:
                return null;
        }
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        switch (sUriMatcher.match(uri)) {
            case 0:
                builder.setTables(HistoryHandler.HISTORY_TABLE_NAME);
                builder.appendWhere(String.format("%s != '%s' OR %s IS NULL", new Object[]{HistoryHandler.HIST_IS_DELETED, "1", HistoryHandler.HIST_IS_DELETED}));
                Cursor cursor = builder.query(getDb(), projection, selection, selectionArgs, null, null, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), History.CONTENT_URI);
                return cursor;
            default:
                return null;
        }
    }

    public Uri insert(Uri uri, ContentValues values) {
        throw new UnsupportedOperationException();
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }
}
