package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;
import java.util.HashSet;
import java.util.Set;

public class ip implements Creator<io> {
    static void a(io ioVar, Parcel parcel, int i) {
        int p = b.p(parcel);
        Set hB = ioVar.hB();
        if (hB.contains(Integer.valueOf(1))) {
            b.c(parcel, 1, ioVar.getVersionCode());
        }
        if (hB.contains(Integer.valueOf(2))) {
            b.a(parcel, 2, ioVar.getId(), true);
        }
        if (hB.contains(Integer.valueOf(4))) {
            b.a(parcel, 4, (Parcelable) ioVar.hS(), i, true);
        }
        if (hB.contains(Integer.valueOf(5))) {
            b.a(parcel, 5, ioVar.getStartDate(), true);
        }
        if (hB.contains(Integer.valueOf(6))) {
            b.a(parcel, 6, (Parcelable) ioVar.hT(), i, true);
        }
        if (hB.contains(Integer.valueOf(7))) {
            b.a(parcel, 7, ioVar.getType(), true);
        }
        b.D(parcel, p);
    }

    /* renamed from: aH */
    public io createFromParcel(Parcel parcel) {
        String str = null;
        int o = a.o(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        im imVar = null;
        String str2 = null;
        im imVar2 = null;
        String str3 = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    i = a.g(parcel, n);
                    hashSet.add(Integer.valueOf(1));
                    break;
                case 2:
                    str3 = a.m(parcel, n);
                    hashSet.add(Integer.valueOf(2));
                    break;
                case 4:
                    im imVar3 = (im) a.a(parcel, n, (Creator<T>) im.CREATOR);
                    hashSet.add(Integer.valueOf(4));
                    imVar2 = imVar3;
                    break;
                case 5:
                    str2 = a.m(parcel, n);
                    hashSet.add(Integer.valueOf(5));
                    break;
                case 6:
                    im imVar4 = (im) a.a(parcel, n, (Creator<T>) im.CREATOR);
                    hashSet.add(Integer.valueOf(6));
                    imVar = imVar4;
                    break;
                case 7:
                    str = a.m(parcel, n);
                    hashSet.add(Integer.valueOf(7));
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new io(hashSet, i, str3, imVar2, str2, imVar, str);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }

    /* renamed from: bE */
    public io[] newArray(int i) {
        return new io[i];
    }
}
