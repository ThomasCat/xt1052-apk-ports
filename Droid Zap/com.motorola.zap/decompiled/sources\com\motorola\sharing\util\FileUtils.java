package com.motorola.sharing.util;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.StatFs;
import android.provider.MediaStore.Images.Media;
import android.provider.MediaStore.Video;
import android.text.TextUtils;
import android.util.Base64;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.provider.HistoryHandler;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class FileUtils {
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getGcspTag("FileUtils");
    private static final Map<String, String> extToMimeTypeMap = new HashMap();
    private static final Map<String, String> mimeTypeToExtMap = new HashMap();

    private interface RefreshMediaFileListener {
        void onMediaFileRefreshed();
    }

    static {
        extToMimeTypeMap.put("jpg", "image/jpeg");
        extToMimeTypeMap.put("jpeg", "image/jpeg");
        extToMimeTypeMap.put("gif", "image/gif");
        extToMimeTypeMap.put("png", "image/png");
        extToMimeTypeMap.put("tiff", "image/tiff");
        extToMimeTypeMap.put("bmp", "image/bmp");
        extToMimeTypeMap.put("dxf", "image/vnd.dxf");
        extToMimeTypeMap.put("btif", "image/prs.btif");
        extToMimeTypeMap.put("ras", "image/x-cmu-raster");
        extToMimeTypeMap.put("cgm", "image/cgm");
        extToMimeTypeMap.put("cmx", "image/x-cmx");
        extToMimeTypeMap.put("uvi", "image/vnd.dece.graphic");
        extToMimeTypeMap.put("djvu", "image/vnd.djvu");
        extToMimeTypeMap.put("dwg", "image/vnd.dwg");
        extToMimeTypeMap.put("xif", "image/vnd.xiff");
        extToMimeTypeMap.put("fst", "image/vnd.fst");
        extToMimeTypeMap.put("fbs", "image/vnd.fastbidsheet");
        extToMimeTypeMap.put("fpx", "image/vnd.fpx");
        extToMimeTypeMap.put("npx", "image/vnd.net-fpx");
        extToMimeTypeMap.put("fh", "image/x-freehand");
        extToMimeTypeMap.put("g3", "image/g3fax");
        extToMimeTypeMap.put("ico", "image/x-icon");
        extToMimeTypeMap.put("ief", "image/ief");
        extToMimeTypeMap.put("mdi", "image/vnd.ms-modi");
        extToMimeTypeMap.put("ktx", "image/ktx");
        extToMimeTypeMap.put("pcx", "image/x-pcx");
        extToMimeTypeMap.put("psd", "image/vnd.adobe.photoshop");
        extToMimeTypeMap.put("pic", "image/x-pict");
        extToMimeTypeMap.put("pnm", "image/x-portable-anymap");
        extToMimeTypeMap.put("pbm", "image/x-portable-bitmap");
        extToMimeTypeMap.put("pgm", "image/x-portable-graymap");
        extToMimeTypeMap.put("ppm", "image/x-portable-pixmap");
        extToMimeTypeMap.put("svg", "image/svg+xml");
        extToMimeTypeMap.put("rgb", "image/x-rgb");
        extToMimeTypeMap.put("wbmp", "image/vnd.wap.wbmp");
        extToMimeTypeMap.put("webp", "image/webp");
        extToMimeTypeMap.put("xbm", "image/x-xbitmap");
        extToMimeTypeMap.put("xpm", "image/x-xpixmap");
        extToMimeTypeMap.put("xwd", "image/x-xwindowdump");
        extToMimeTypeMap.put("mp4", "video/mp4");
        extToMimeTypeMap.put("3gp", "video/3gpp");
        extToMimeTypeMap.put("3g2", "video/3gpp2");
        extToMimeTypeMap.put("avi", "video/x-msvideo");
        extToMimeTypeMap.put("uvh", "video/vnd.dece.hd");
        extToMimeTypeMap.put("uvm", "video/vnd.dece.mobile");
        extToMimeTypeMap.put("uvu", "video/vnd.uvvu.mp4");
        extToMimeTypeMap.put("uvp", "video/vnd.dece.pd");
        extToMimeTypeMap.put("uvs", "video/vnd.dece.sd");
        extToMimeTypeMap.put("uvv", "video/vnd.dece.video");
        extToMimeTypeMap.put("fvt", "video/vnd.fvt");
        extToMimeTypeMap.put("f4v", "video/x-f4v");
        extToMimeTypeMap.put("flv", "video/x-flv");
        extToMimeTypeMap.put("fli", "video/x-fli");
        extToMimeTypeMap.put("h261", "video/h261");
        extToMimeTypeMap.put("h263", "video/h263");
        extToMimeTypeMap.put("h264", "video/h264");
        extToMimeTypeMap.put("hpid", "video/hpid");
        extToMimeTypeMap.put("jpm", "video/jpm");
        extToMimeTypeMap.put("jpgv", "video/jpgv");
        extToMimeTypeMap.put("m4v", "video/m4v");
        extToMimeTypeMap.put("wmx", "video/video/x-ms-wmx");
        extToMimeTypeMap.put("wm", "video/x-ms-wm");
        extToMimeTypeMap.put("wmv", "video/x-ms-wmv");
        extToMimeTypeMap.put("wvx", "video/x-ms-wvx");
        extToMimeTypeMap.put("mj2", "video/mj2");
        extToMimeTypeMap.put("mxu", "video/vnd.mpegurl");
        extToMimeTypeMap.put("mpeg", "video/mpeg");
        extToMimeTypeMap.put("mp4", "video/mp4");
        extToMimeTypeMap.put("ogv", "video/ogg");
        extToMimeTypeMap.put("webm", "video/webm");
        extToMimeTypeMap.put("qt", "video/quicktime");
        extToMimeTypeMap.put("movie", "video/x-sgi-movie");
        extToMimeTypeMap.put("viv", "video/vnd.vivo");
        mimeTypeToExtMap.put("image/jpeg", "jpg");
        mimeTypeToExtMap.put("image/gif", "gif");
        mimeTypeToExtMap.put("image/png", "png");
        mimeTypeToExtMap.put("image/tiff", "tiff");
        mimeTypeToExtMap.put("image/bmp", "bmp");
        mimeTypeToExtMap.put("image/vnd.dxf", "dxf");
        mimeTypeToExtMap.put("image/prs.btif", "btif");
        mimeTypeToExtMap.put("image/x-cmu-raster", "ras");
        mimeTypeToExtMap.put("image/cgm", "cgm");
        mimeTypeToExtMap.put("image/x-cmx", "cmx");
        mimeTypeToExtMap.put("image/vnd.dece.graphic", "uvi");
        mimeTypeToExtMap.put("image/vnd.djvu", "djvu");
        mimeTypeToExtMap.put("image/vnd.dwg", "dwg");
        mimeTypeToExtMap.put("image/vnd.xiff", "xif");
        mimeTypeToExtMap.put("image/vnd.fst", "fst");
        mimeTypeToExtMap.put("image/vnd.fastbidsheet", "fbs");
        mimeTypeToExtMap.put("image/vnd.fpx", "fpx");
        mimeTypeToExtMap.put("image/vnd.net-fpx", "npx");
        mimeTypeToExtMap.put("image/x-freehand", "fh");
        mimeTypeToExtMap.put("image/g3fax", "g3");
        mimeTypeToExtMap.put("image/x-icon", "ico");
        mimeTypeToExtMap.put("image/ief", "ief");
        mimeTypeToExtMap.put("image/vnd.ms-modi", "mdi");
        mimeTypeToExtMap.put("image/ktx", "ktx");
        mimeTypeToExtMap.put("image/x-pcx", "pcx");
        mimeTypeToExtMap.put("image/vnd.adobe.photoshop", "psd");
        mimeTypeToExtMap.put("image/x-pict", "pic");
        mimeTypeToExtMap.put("image/x-portable-anymap", "pnm");
        mimeTypeToExtMap.put("image/x-portable-bitmap", "pbm");
        mimeTypeToExtMap.put("image/x-portable-graymap", "pgm");
        mimeTypeToExtMap.put("image/x-portable-pixmap", "ppm");
        mimeTypeToExtMap.put("image/svg+xml", "svg");
        mimeTypeToExtMap.put("image/x-rgb", "rgb");
        mimeTypeToExtMap.put("image/vnd.wap.wbmp", "wbmp");
        mimeTypeToExtMap.put("image/webp", "webp");
        mimeTypeToExtMap.put("image/x-xbitmap", "xbm");
        mimeTypeToExtMap.put("image/x-xpixmap", "xpm");
        mimeTypeToExtMap.put("image/x-xwindowdump", "xwd");
        mimeTypeToExtMap.put("video/mp4", "mp4");
        mimeTypeToExtMap.put("video/3gpp", "3gp");
        mimeTypeToExtMap.put("video/3gpp2", "3g2");
        mimeTypeToExtMap.put("video/x-msvideo", "avi");
        mimeTypeToExtMap.put("video/vnd.dece.hd", "uvh");
        mimeTypeToExtMap.put("video/vnd.dece.mobile", "uvm");
        mimeTypeToExtMap.put("video/vnd.uvvu.mp4", "uvu");
        mimeTypeToExtMap.put("video/vnd.dece.pd", "uvp");
        mimeTypeToExtMap.put("video/vnd.dece.sd", "uvs");
        mimeTypeToExtMap.put("video/vnd.dece.video", "uvv");
        mimeTypeToExtMap.put("video/vnd.fvt", "fvt");
        mimeTypeToExtMap.put("video/x-f4v", "f4v");
        mimeTypeToExtMap.put("video/x-flv", "flv");
        mimeTypeToExtMap.put("video/x-fli", "fli");
        mimeTypeToExtMap.put("video/h261", "h261");
        mimeTypeToExtMap.put("video/h263", "h263");
        mimeTypeToExtMap.put("video/h264", "h264");
        mimeTypeToExtMap.put("video/hpid", "hpid");
        mimeTypeToExtMap.put("video/jpm", "jpm");
        mimeTypeToExtMap.put("video/jpgv", "jpgv");
        mimeTypeToExtMap.put("video/m4v", "m4v");
        mimeTypeToExtMap.put("video/video/x-ms-wmx", "wmx");
        mimeTypeToExtMap.put("video/x-ms-wm", "wm");
        mimeTypeToExtMap.put("video/x-ms-wmv", "wmv");
        mimeTypeToExtMap.put("video/x-ms-wvx", "wvx");
        mimeTypeToExtMap.put("video/mj2", "mj2");
        mimeTypeToExtMap.put("video/vnd.mpegurl", "mxu");
        mimeTypeToExtMap.put("video/mpeg", "mpeg");
        mimeTypeToExtMap.put("video/mp4", "mp4");
        mimeTypeToExtMap.put("video/ogg", "ogv");
        mimeTypeToExtMap.put("video/webm", "webm");
        mimeTypeToExtMap.put("video/quicktime", "qt");
        mimeTypeToExtMap.put("video/x-sgi-movie", "movie");
        mimeTypeToExtMap.put("video/vnd.vivo", "viv");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x004d A[SYNTHETIC, Splitter:B:17:0x004d] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0052 A[SYNTHETIC, Splitter:B:20:0x0052] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00d3 A[SYNTHETIC, Splitter:B:41:0x00d3] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00d8 A[SYNTHETIC, Splitter:B:44:0x00d8] */
    /* JADX WARNING: Removed duplicated region for block: B:59:? A[RETURN, SYNTHETIC] */
    public static byte[] getBytesFromFile(String filePath) {
        byte[] buffer = new byte[102400];
        FileInputStream fis = null;
        ByteArrayOutputStream baos = null;
        try {
            FileInputStream fis2 = new FileInputStream(new File(filePath));
            try {
                ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
                while (true) {
                    try {
                        int bytesRead = fis2.read(buffer, 0, buffer.length);
                        if (bytesRead <= 0) {
                            break;
                        }
                        baos2.write(buffer, 0, bytesRead);
                    } catch (Exception e) {
                        e = e;
                        baos = baos2;
                        fis = fis2;
                    } catch (Throwable th) {
                        th = th;
                        baos = baos2;
                        fis = fis2;
                        if (fis != null) {
                        }
                        if (baos != null) {
                        }
                        throw th;
                    }
                }
                byte[] result = baos2.toByteArray();
                if (fis2 != null) {
                    try {
                        fis2.close();
                    } catch (IOException e2) {
                        Logger.e(TAG, "failed to close: " + e2);
                    }
                }
                if (baos2 != null) {
                    try {
                        baos2.close();
                    } catch (IOException e3) {
                        Logger.e(TAG, "failed to close: " + e3);
                    }
                }
                ByteArrayOutputStream byteArrayOutputStream = baos2;
                FileInputStream fileInputStream = fis2;
                return result;
            } catch (Exception e4) {
                e = e4;
                fis = fis2;
                try {
                    Logger.e(TAG, "failed to read file " + filePath + ": " + e);
                    if (fis != null) {
                        try {
                            fis.close();
                        } catch (IOException e5) {
                            Logger.e(TAG, "failed to close: " + e5);
                        }
                    }
                    if (baos != null) {
                        return null;
                    }
                    try {
                        baos.close();
                        return null;
                    } catch (IOException e6) {
                        Logger.e(TAG, "failed to close: " + e6);
                        return null;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (fis != null) {
                        try {
                            fis.close();
                        } catch (IOException e7) {
                            Logger.e(TAG, "failed to close: " + e7);
                        }
                    }
                    if (baos != null) {
                        try {
                            baos.close();
                        } catch (IOException e8) {
                            Logger.e(TAG, "failed to close: " + e8);
                        }
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                fis = fis2;
                if (fis != null) {
                }
                if (baos != null) {
                }
                throw th;
            }
        } catch (Exception e9) {
            e = e9;
            Logger.e(TAG, "failed to read file " + filePath + ": " + e);
            if (fis != null) {
            }
            if (baos != null) {
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:74:0x0194  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0199 A[SYNTHETIC, Splitter:B:76:0x0199] */
    public static String reduceJpegSize(String imagePath, String outputDirPath, int refMaxSideSize, float sizeFactor) {
        if (TextUtils.isEmpty(imagePath)) {
            throw new IllegalArgumentException("image path is null");
        } else if (TextUtils.isEmpty(outputDirPath)) {
            throw new IllegalArgumentException("output directory path is null");
        } else {
            File origFile = new File(imagePath);
            if (!origFile.exists()) {
                throw new IllegalArgumentException("image path is invalid");
            }
            long origFileSize = origFile.length();
            if (((float) origFileSize) * sizeFactor < 512000.0f) {
                return null;
            }
            Options o = new Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(imagePath, o);
            int scale = Math.round(((float) (o.outHeight > o.outWidth ? o.outHeight : o.outWidth)) / ((float) refMaxSideSize));
            if (scale == 0) {
                scale = 1;
            }
            Options loadOpt = new Options();
            loadOpt.inSampleSize = scale;
            Bitmap image = null;
            ByteArrayOutputStream imageOutputStream = null;
            try {
                image = BitmapFactory.decodeFile(imagePath, loadOpt);
                if (image == null) {
                    Logger.e(TAG, "could not decode image file " + imagePath);
                    if (image != null) {
                        image.recycle();
                    }
                    if (imageOutputStream == null) {
                        return null;
                    }
                    try {
                        imageOutputStream.close();
                        return null;
                    } catch (IOException e) {
                        Logger.e(TAG, "failed to close: " + e);
                        return null;
                    }
                } else {
                    ByteArrayOutputStream imageOutputStream2 = new ByteArrayOutputStream();
                    try {
                        image.compress(CompressFormat.JPEG, 100, imageOutputStream2);
                        image.recycle();
                        image = null;
                        Logger.d(TAG, "resized image size: " + imageOutputStream2.size());
                        if (imageOutputStream2.size() < 512000 || ((float) imageOutputStream2.size()) > ((float) origFileSize) * sizeFactor) {
                            if (image != null) {
                                image.recycle();
                            }
                            if (imageOutputStream2 == null) {
                                return null;
                            }
                            try {
                                imageOutputStream2.close();
                                return null;
                            } catch (IOException e2) {
                                Logger.e(TAG, "failed to close: " + e2);
                                return null;
                            }
                        } else {
                            String saveToFile = saveToFile(outputDirPath, generateTempFileName(".jpg"), imageOutputStream2);
                            if (image != null) {
                                image.recycle();
                            }
                            if (imageOutputStream2 == null) {
                                return saveToFile;
                            }
                            try {
                                imageOutputStream2.close();
                                return saveToFile;
                            } catch (IOException e3) {
                                Logger.e(TAG, "failed to close: " + e3);
                                return saveToFile;
                            }
                        }
                    } catch (Exception e4) {
                        e = e4;
                        imageOutputStream = imageOutputStream2;
                    } catch (Throwable th) {
                        th = th;
                        imageOutputStream = imageOutputStream2;
                        if (image != null) {
                        }
                        if (imageOutputStream != null) {
                        }
                        throw th;
                    }
                }
            } catch (Exception e5) {
                e = e5;
                try {
                    Logger.e(TAG, "failed to resize image: " + e);
                    if (image != null) {
                        image.recycle();
                    }
                    if (imageOutputStream != null) {
                        try {
                            imageOutputStream.close();
                        } catch (IOException e6) {
                            Logger.e(TAG, "failed to close: " + e6);
                        }
                    }
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    if (image != null) {
                        image.recycle();
                    }
                    if (imageOutputStream != null) {
                        try {
                            imageOutputStream.close();
                        } catch (IOException e7) {
                            Logger.e(TAG, "failed to close: " + e7);
                        }
                    }
                    throw th;
                }
            }
        }
    }

    public static String resize(String imagePath, String outputDirPath, int width, int height) {
        if (TextUtils.isEmpty(imagePath)) {
            throw new IllegalArgumentException("image path is null");
        } else if (TextUtils.isEmpty(outputDirPath)) {
            throw new IllegalArgumentException("output directory path is null");
        } else if (!new File(imagePath).exists()) {
            throw new IllegalArgumentException("image path is invalid");
        } else {
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            try {
                resize(imagePath, width, height, outStream);
                return saveToFile(outputDirPath, generateTempFileName(".jpg"), outStream);
            } finally {
                silentClose(outStream);
            }
        }
    }

    private static void resize(String imagePath, int width, int height, ByteArrayOutputStream outStream) {
        int[] scaledDimensions = getScaledDimensions(imagePath, width, height);
        Options options = new Options();
        options.inSampleSize = scaledDimensions[2];
        options.inDither = true;
        Bitmap inBitmap = null;
        Bitmap outBitmap = null;
        try {
            inBitmap = BitmapFactory.decodeFile(imagePath, options);
            if (inBitmap == null) {
                Logger.e(TAG, "could not decode image file " + imagePath);
                if (inBitmap != null) {
                    inBitmap.recycle();
                }
                if (outBitmap != null) {
                    outBitmap.recycle();
                    return;
                }
                return;
            }
            outBitmap = Bitmap.createBitmap(scaledDimensions[0], scaledDimensions[1], Config.ARGB_8888);
            Rect dest = new Rect();
            dest.top = 0;
            dest.left = 0;
            dest.right = scaledDimensions[0];
            dest.bottom = scaledDimensions[1];
            Canvas canvas = new Canvas(outBitmap);
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setFilterBitmap(true);
            paint.setDither(true);
            canvas.drawBitmap(inBitmap, null, dest, paint);
            outBitmap.compress(CompressFormat.JPEG, 100, outStream);
            if (inBitmap != null) {
                inBitmap.recycle();
            }
            if (outBitmap != null) {
                outBitmap.recycle();
            }
        } catch (Exception e) {
            Logger.e(TAG, "failed to resize image: " + e);
            if (inBitmap != null) {
                inBitmap.recycle();
            }
            if (outBitmap != null) {
                outBitmap.recycle();
            }
        } catch (Throwable th) {
            if (inBitmap != null) {
                inBitmap.recycle();
            }
            if (outBitmap != null) {
                outBitmap.recycle();
            }
            throw th;
        }
    }

    public static String resizeBase64(String imagePath, int width, int height) {
        if (TextUtils.isEmpty(imagePath)) {
            throw new IllegalArgumentException("image path is null");
        } else if (!new File(imagePath).exists()) {
            throw new IllegalArgumentException("image path is invalid");
        } else {
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            try {
                resize(imagePath, width, height, outStream);
                return Base64.encodeToString(outStream.toByteArray(), 0);
            } finally {
                silentClose(outStream);
            }
        }
    }

    private static int[] getScaledDimensions(String imagePath, int targetWidth, int targetHeight) {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, options);
        if (options.outWidth <= targetWidth && options.outHeight <= targetHeight) {
            return new int[]{options.outWidth, options.outHeight, 1};
        } else if (isLandscape(options)) {
            double factor = ((double) options.outWidth) / ((double) targetWidth);
            return new int[]{targetWidth, (int) (((double) options.outHeight) / factor), (int) factor};
        } else {
            double factor2 = ((double) options.outHeight) / ((double) targetHeight);
            return new int[]{(int) (((double) options.outWidth) / factor2), targetHeight, (int) factor2};
        }
    }

    private static boolean isLandscape(Options options) {
        return options.outWidth > options.outHeight;
    }

    private static void silentClose(OutputStream out) {
        if (out != null) {
            try {
                out.close();
            } catch (IOException e) {
                Logger.e(TAG, "failed to close: " + e);
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public static Uri getImageContentUri(Context context, String filePath) {
        Cursor cursor = null;
        try {
            Cursor cursor2 = context.getContentResolver().query(Media.EXTERNAL_CONTENT_URI, new String[]{HistoryHandler.HIST_ID}, "_data=?", new String[]{filePath}, null);
            if (cursor2 == null || !cursor2.moveToFirst()) {
                if (cursor2 != null) {
                    cursor2.close();
                }
                return null;
            }
            Uri withAppendedId = ContentUris.withAppendedId(Media.EXTERNAL_CONTENT_URI, (long) cursor2.getInt(cursor2.getColumnIndex(HistoryHandler.HIST_ID)));
            if (cursor2 == null) {
                return withAppendedId;
            }
            cursor2.close();
            return withAppendedId;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public static Uri getVideoContentUri(Context context, String filePath) {
        Cursor cursor = null;
        try {
            Cursor cursor2 = context.getContentResolver().query(Video.Media.EXTERNAL_CONTENT_URI, new String[]{HistoryHandler.HIST_ID}, "_data=?", new String[]{filePath}, null);
            if (cursor2 == null || !cursor2.moveToFirst()) {
                if (cursor2 != null) {
                    cursor2.close();
                }
                return null;
            }
            Uri withAppendedId = ContentUris.withAppendedId(Video.Media.EXTERNAL_CONTENT_URI, (long) cursor2.getInt(cursor2.getColumnIndex(HistoryHandler.HIST_ID)));
            if (cursor2 == null) {
                return withAppendedId;
            }
            cursor2.close();
            return withAppendedId;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public static String getMimeTypeFromFileName(String fileName) {
        String ext = getFileExtensionFromFileName(fileName);
        if (ext == null) {
            return null;
        }
        if (extToMimeTypeMap.containsKey(ext)) {
            return (String) extToMimeTypeMap.get(ext);
        }
        Logger.e(TAG, "file extension not supported: " + ext);
        return null;
    }

    /* JADX INFO: finally extract failed */
    public static String getMediaPathFromUri(Context context, Uri mediaUri) {
        if (mediaUri == null) {
            return null;
        }
        String scheme = mediaUri.getScheme();
        if (scheme == null) {
            return null;
        }
        if (scheme.equalsIgnoreCase("file")) {
            try {
                String string = mediaUri.toString();
                if (string != null) {
                    string = string.replace(" ", "%20");
                }
                return new File(new URI(string)).getAbsolutePath();
            } catch (Exception e) {
                return null;
            }
        } else {
            String path = null;
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(mediaUri, new String[]{"_data"}, ZapConstants.GOOGLE_ACCOUNT_NONE, null, ZapConstants.GOOGLE_ACCOUNT_NONE);
                if (cursor == null || cursor.getCount() == 0) {
                    Logger.w(TAG, "cursor is null or empty");
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                if (cursor.moveToFirst()) {
                    path = cursor.getString(cursor.getColumnIndex("_data"));
                    Logger.d(TAG, "path from URI: " + path);
                }
                if (cursor != null) {
                    cursor.close();
                }
                if (path != null) {
                    return path;
                }
                Logger.w(TAG, "path from URI: null");
                return path;
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
    }

    public static String getFileExtensionFromFileName(String fileName) {
        if (fileName == null || !fileName.contains(".")) {
            return null;
        }
        return fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase(Locale.getDefault());
    }

    public static String getFileExtensionFromMimeType(String mimeType) {
        if (mimeType != null && mimeTypeToExtMap.containsKey(mimeType)) {
            return (String) mimeTypeToExtMap.get(mimeType);
        }
        return null;
    }

    public static boolean deleteFile(String filePath, Context context) {
        if (filePath == null) {
            return false;
        }
        File f = new File(filePath);
        if (!f.exists()) {
            return true;
        }
        String mimeType = getMimeTypeFromFileName(filePath);
        Uri uri = null;
        if (isVideo(mimeType)) {
            uri = getVideoContentUri(context, filePath);
        } else if (isImage(mimeType)) {
            uri = getImageContentUri(context, filePath);
        }
        if (uri == null) {
            return f.delete();
        }
        try {
            if (context.getContentResolver().delete(uri, null, null) == 1) {
                return true;
            }
            return false;
        } catch (Exception e) {
            Logger.e(TAG, "could not delete file " + filePath + ": " + e);
            return false;
        }
    }

    public static Bitmap getThumbnail(String filePath, int width, int height) {
        if (filePath == null) {
            return null;
        }
        String mimeType = getMimeTypeFromFileName(filePath);
        if (mimeType == null) {
            return null;
        }
        if (mimeType.contains("video")) {
            return getVideoThumbnail(filePath, width, height);
        }
        return getPhotoThumbnail(filePath, width, height);
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0047 A[SYNTHETIC, Splitter:B:17:0x0047] */
    public static String getBase64Thumbnail(String filePath, int width, int height) {
        Bitmap thumbnail = getThumbnail(filePath, width, height);
        if (thumbnail == null) {
            return null;
        }
        ByteArrayOutputStream baos = null;
        try {
            ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
            try {
                thumbnail.compress(CompressFormat.JPEG, 30, baos2);
                String encodeToString = Base64.encodeToString(baos2.toByteArray(), 0);
                thumbnail.recycle();
                if (baos2 == null) {
                    return encodeToString;
                }
                try {
                    baos2.close();
                    return encodeToString;
                } catch (Exception e) {
                    Logger.e(TAG, "failed to close: " + e);
                    return encodeToString;
                }
            } catch (Throwable th) {
                th = th;
                baos = baos2;
                thumbnail.recycle();
                if (baos != null) {
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            thumbnail.recycle();
            if (baos != null) {
                try {
                    baos.close();
                } catch (Exception e2) {
                    Logger.e(TAG, "failed to close: " + e2);
                }
            }
            throw th;
        }
    }

    public static Bitmap base64ToBitmap(String base64) {
        if (base64 != null) {
            try {
                byte[] thumbBytes = Base64.decode(base64.getBytes(), 0);
                return BitmapFactory.decodeByteArray(thumbBytes, 0, thumbBytes.length);
            } catch (Exception e) {
                Logger.e(TAG, "could not decode an image: " + e);
            }
        }
        return null;
    }

    private static Bitmap getPhotoThumbnail(String filePath, int width, int height) {
        Options o = new Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);
        int scale = Math.round(((float) (o.outHeight < o.outWidth ? o.outHeight : o.outWidth)) / ((float) (width * 2)));
        if (scale == 0) {
            scale = 1;
        }
        Options loadOpt = new Options();
        loadOpt.inSampleSize = scale;
        Bitmap image = BitmapFactory.decodeFile(filePath, loadOpt);
        if (image == null) {
            return null;
        }
        Bitmap image2 = adjustRotation(image, filePath);
        Bitmap thumbnail = ThumbnailUtils.extractThumbnail(image2, width, height);
        if (thumbnail == image2) {
            return thumbnail;
        }
        image2.recycle();
        return thumbnail;
    }

    private static Bitmap adjustRotation(Bitmap inBitmap, String filePath) {
        int angle = getAngle(filePath);
        if (angle == 0) {
            return inBitmap;
        }
        Matrix matrix = new Matrix();
        matrix.postRotate((float) angle);
        Bitmap outBitmap = Bitmap.createBitmap(inBitmap, 0, 0, inBitmap.getWidth(), inBitmap.getHeight(), matrix, true);
        if (inBitmap != outBitmap) {
            inBitmap.recycle();
        }
        return outBitmap;
    }

    private static int getAngle(String filePath) {
        try {
            switch (new ExifInterface(filePath).getAttributeInt("Orientation", 0)) {
                case 3:
                    return 180;
                case 6:
                    return 90;
                case 8:
                    return 270;
                default:
                    return 0;
            }
        } catch (IOException e) {
            Logger.e(TAG, "could not read exif from " + filePath + ": " + e);
            return 0;
        }
    }

    private static Bitmap getVideoThumbnail(String filePath, int width, int height) {
        Bitmap image = ThumbnailUtils.createVideoThumbnail(filePath, 1);
        if (image == null) {
            return null;
        }
        Bitmap thumbnail = ThumbnailUtils.extractThumbnail(image, width, height);
        if (thumbnail == image) {
            return thumbnail;
        }
        image.recycle();
        return thumbnail;
    }

    public static Bitmap getImage(String filePath, int width, int height) {
        if (filePath == null) {
            return null;
        }
        String mimeType = getMimeTypeFromFileName(filePath);
        if (mimeType == null) {
            return null;
        }
        if (mimeType.contains("video")) {
            return ThumbnailUtils.createVideoThumbnail(filePath, 1);
        }
        Options o = new Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);
        int scale = Math.round(((float) (o.outHeight < o.outWidth ? o.outHeight : o.outWidth)) / ((float) width));
        if (scale == 0) {
            scale = 1;
        }
        Options loadOpt = new Options();
        loadOpt.inSampleSize = scale;
        loadOpt.inJustDecodeBounds = false;
        return adjustRotation(BitmapFactory.decodeFile(filePath, loadOpt), filePath);
    }

    public static void refreshMediaFile(String filePath, Context context) {
        refreshMediaFile(filePath, context, null);
    }

    private static void refreshMediaFile(String filePath, Context context, final RefreshMediaFileListener listener) {
        MediaScannerConnection.scanFile(context, new String[]{filePath}, null, new OnScanCompletedListener() {
            public void onScanCompleted(String path, Uri uri) {
                Logger.d(FileUtils.TAG, "scanned " + path);
                if (listener != null) {
                    listener.onMediaFileRefreshed();
                }
            }
        });
    }

    public static boolean isMedia(String mimeType) {
        return isImage(mimeType) || isVideo(mimeType);
    }

    public static boolean isVideo(String mimeType) {
        if (mimeType == null) {
            return false;
        }
        return mimeType.toLowerCase(Locale.ROOT).startsWith("video/");
    }

    public static boolean isImage(String mimeType) {
        if (mimeType == null) {
            return false;
        }
        return mimeType.toLowerCase(Locale.ROOT).startsWith("image/");
    }

    public static boolean isBigFile(long size) {
        return size > 10485760;
    }

    private static String generateTempFileName(String extention) {
        return "temp_" + System.currentTimeMillis() + extention;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b2 A[SYNTHETIC, Splitter:B:20:0x00b2] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00d5 A[SYNTHETIC, Splitter:B:26:0x00d5] */
    /* JADX WARNING: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
    private static String saveToFile(String directoryPath, String fileName, ByteArrayOutputStream stream) {
        File directory = new File(directoryPath);
        if (directory.exists() || directory.mkdirs()) {
            File targetFile = new File(directoryPath + "/" + fileName);
            Logger.d(TAG, "saving to: " + targetFile.toString());
            FileOutputStream outputStream = null;
            try {
                FileOutputStream outputStream2 = new FileOutputStream(targetFile);
                try {
                    outputStream2.write(stream.toByteArray());
                    outputStream2.flush();
                    if (outputStream2 != null) {
                        try {
                            outputStream2.close();
                        } catch (Exception e) {
                            Logger.e(TAG, "failed to close: " + e);
                        }
                    }
                    return targetFile.toString();
                } catch (Exception e2) {
                    e = e2;
                    outputStream = outputStream2;
                    try {
                        Logger.e(TAG, "could not save to file: " + e);
                        if (outputStream != null) {
                            return null;
                        }
                        try {
                            outputStream.close();
                            return null;
                        } catch (Exception e3) {
                            Logger.e(TAG, "failed to close: " + e3);
                            return null;
                        }
                    } catch (Throwable th) {
                        th = th;
                        if (outputStream != null) {
                            try {
                                outputStream.close();
                            } catch (Exception e4) {
                                Logger.e(TAG, "failed to close: " + e4);
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    outputStream = outputStream2;
                    if (outputStream != null) {
                    }
                    throw th;
                }
            } catch (Exception e5) {
                e = e5;
                Logger.e(TAG, "could not save to file: " + e);
                if (outputStream != null) {
                }
            }
        } else {
            Logger.e(TAG, "could not create directory for saving: " + directoryPath);
            return null;
        }
    }

    public static boolean isSdcardMounted() {
        String state = Environment.getExternalStorageState();
        return "mounted".equals(state) || "mounted_ro".equals(state);
    }

    public static String getSdcardPath() {
        return Environment.getExternalStorageDirectory().toString();
    }

    @SuppressLint({"NewApi"})
    public static Intent getGalleryIntent(Context context) {
        String packageName;
        if (context == null) {
            return null;
        }
        PackageManager pm = context.getPackageManager();
        if (VERSION.SDK_INT >= 15) {
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.APP_GALLERY");
            intent.addCategory("android.intent.category.LAUNCHER");
            if (!pm.queryIntentActivities(intent, 65536).isEmpty()) {
                intent.setFlags(268468224);
                return intent;
            }
        }
        Intent intent2 = new Intent();
        intent2.setType("image/*");
        intent2.setAction("android.intent.action.GET_CONTENT");
        intent2.addCategory("android.intent.category.OPENABLE");
        for (ResolveInfo info : pm.queryIntentActivities(intent2, 65536)) {
            if (info != null) {
                packageName = info.activityInfo.packageName;
            } else {
                packageName = null;
            }
            if (!TextUtils.isEmpty(packageName)) {
                Intent intent3 = new Intent("android.intent.action.MAIN");
                intent3.addCategory("android.intent.category.LAUNCHER");
                intent3.setPackage(packageName);
                if (!pm.queryIntentActivities(intent3, 65536).isEmpty()) {
                    intent3.setFlags(268468224);
                    return intent3;
                }
            }
        }
        return null;
    }

    public static int getFileSize(String filePath) {
        return (int) new File(filePath).length();
    }

    public static boolean isFileExists(String filePath) {
        return getFileSize(filePath) > 0;
    }

    public static boolean isLowStorage() {
        if (!isSdcardMounted()) {
            return false;
        }
        try {
            StatFs stat = new StatFs(Environment.getExternalStorageDirectory().toString());
            long availableStorage = ((long) stat.getAvailableBlocks()) * ((long) stat.getBlockSize());
            Logger.d(TAG, "available storage: " + availableStorage);
            if (availableStorage < 10485760) {
                return true;
            }
            return false;
        } catch (IllegalArgumentException e) {
            Logger.e(TAG, "could not stat external storage dir: " + e);
            return false;
        }
    }
}
