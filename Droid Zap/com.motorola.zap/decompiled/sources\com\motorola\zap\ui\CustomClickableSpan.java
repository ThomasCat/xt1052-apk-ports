package com.motorola.zap.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

public class CustomClickableSpan extends ClickableSpan {
    private String mAction;
    private Context mContext;
    private Intent mIntent;

    public CustomClickableSpan(Context context, String action) {
        this.mContext = context;
        this.mAction = action;
    }

    public CustomClickableSpan(Context context, Intent intent) {
        this.mContext = context;
        this.mIntent = intent;
    }

    public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setUnderlineText(false);
        ds.setTypeface(Typeface.create(ds.getTypeface(), 0));
    }

    public void onClick(View widget) {
        try {
            if (this.mAction != null) {
                this.mContext.startActivity(new Intent(this.mAction));
            } else {
                this.mContext.startActivity(this.mIntent);
            }
        } catch (Exception e) {
        }
    }
}
