package com.google.android.gms.tagmanager;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import com.google.android.gms.internal.c.f;
import com.google.android.gms.internal.jd.a;
import com.google.android.gms.internal.kd;
import com.google.android.gms.internal.ke;
import com.google.android.gms.tagmanager.cr.c;
import com.google.android.gms.tagmanager.cr.g;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONException;

class cq implements f {
    private final String TM;
    private bg<a> Wi;
    private final ExecutorService Wp = Executors.newSingleThreadExecutor();
    private final Context mContext;

    cq(Context context, String str) {
        this.mContext = context;
        this.TM = str;
    }

    private c a(ByteArrayOutputStream byteArrayOutputStream) {
        boolean z = false;
        try {
            return ba.br(byteArrayOutputStream.toString("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            bh.s("Tried to convert binary resource to string for JSON parsing; not UTF-8 format");
            return z;
        } catch (JSONException e2) {
            bh.w("Resource is a UTF-8 encoded string but doesn't contain a JSON container");
            return z;
        }
    }

    private c k(byte[] bArr) {
        boolean z = false;
        try {
            return cr.b(f.a(bArr));
        } catch (kd e) {
            bh.w("Resource doesn't contain a binary container");
            return z;
        } catch (g e2) {
            bh.w("Resource doesn't contain a binary container");
            return z;
        }
    }

    public void a(bg<a> bgVar) {
        this.Wi = bgVar;
    }

    public void b(final a aVar) {
        this.Wp.execute(new Runnable() {
            public void run() {
                cq.this.c(aVar);
            }
        });
    }

    public c bP(int i) {
        bh.v("Atttempting to load container from resource ID " + i);
        try {
            InputStream openRawResource = this.mContext.getResources().openRawResource(i);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            cr.b(openRawResource, byteArrayOutputStream);
            c a = a(byteArrayOutputStream);
            return a != null ? a : k(byteArrayOutputStream.toByteArray());
        } catch (IOException e) {
            bh.w("Error reading default container resource with ID " + i);
            return null;
        } catch (NotFoundException e2) {
            bh.w("No default container resource found.");
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean c(a aVar) {
        boolean z = false;
        File jD = jD();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(jD);
            try {
                fileOutputStream.write(ke.d(aVar));
                z = true;
                try {
                } catch (IOException e) {
                    bh.w("error closing stream for writing resource to disk");
                }
            } catch (IOException e2) {
                bh.w("Error writing resource to disk. Removing resource from disk.");
                jD.delete();
                try {
                } catch (IOException e3) {
                    bh.w("error closing stream for writing resource to disk");
                }
            } finally {
                try {
                    fileOutputStream.close();
                } catch (IOException e4) {
                    bh.w("error closing stream for writing resource to disk");
                }
            }
        } catch (FileNotFoundException e5) {
            bh.t("Error opening resource file for writing");
        }
        return z;
    }

    public void iN() {
        this.Wp.execute(new Runnable() {
            public void run() {
                cq.this.jC();
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void jC() {
        if (this.Wi == null) {
            throw new IllegalStateException("callback must be set before execute");
        }
        this.Wi.iM();
        bh.v("Start loading resource from disk ...");
        if ((ce.ju().jv() == a.CONTAINER || ce.ju().jv() == a.CONTAINER_DEBUG) && this.TM.equals(ce.ju().getContainerId())) {
            this.Wi.a(bg.a.NOT_AVAILABLE);
            return;
        }
        try {
            FileInputStream fileInputStream = new FileInputStream(jD());
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                cr.b(fileInputStream, byteArrayOutputStream);
                this.Wi.i(a.l(byteArrayOutputStream.toByteArray()));
                try {
                } catch (IOException e) {
                    bh.w("error closing stream for reading resource from disk");
                }
            } catch (IOException e2) {
                bh.w("error reading resource from disk");
                this.Wi.a(bg.a.IO_ERROR);
                try {
                } catch (IOException e3) {
                    bh.w("error closing stream for reading resource from disk");
                }
            } finally {
                try {
                    fileInputStream.close();
                } catch (IOException e4) {
                    bh.w("error closing stream for reading resource from disk");
                }
            }
            bh.v("Load resource from disk finished.");
        } catch (FileNotFoundException e5) {
            bh.s("resource not on disk");
            this.Wi.a(bg.a.NOT_AVAILABLE);
        }
    }

    /* access modifiers changed from: 0000 */
    public File jD() {
        return new File(this.mContext.getDir("google_tagmanager", 0), "resource_" + this.TM);
    }

    public synchronized void release() {
        this.Wp.shutdown();
    }
}
