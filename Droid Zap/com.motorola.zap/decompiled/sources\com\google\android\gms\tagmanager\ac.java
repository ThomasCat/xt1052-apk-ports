package com.google.android.gms.tagmanager;

import android.util.Base64;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.d;
import java.util.Map;

class ac extends aj {
    private static final String ID = a.ENCODE.toString();
    private static final String US = b.ARG0.toString();
    private static final String UT = b.NO_PADDING.toString();
    private static final String UU = b.INPUT_FORMAT.toString();
    private static final String UV = b.OUTPUT_FORMAT.toString();

    public ac() {
        super(ID, US);
    }

    public boolean iy() {
        return true;
    }

    public d.a u(Map<String, d.a> map) {
        byte[] decode;
        String encodeToString;
        d.a aVar = (d.a) map.get(US);
        if (aVar == null || aVar == di.ku()) {
            return di.ku();
        }
        String j = di.j(aVar);
        d.a aVar2 = (d.a) map.get(UU);
        String j2 = aVar2 == null ? "text" : di.j(aVar2);
        d.a aVar3 = (d.a) map.get(UV);
        String j3 = aVar3 == null ? "base16" : di.j(aVar3);
        d.a aVar4 = (d.a) map.get(UT);
        int i = (aVar4 == null || !di.n(aVar4).booleanValue()) ? 2 : 3;
        try {
            if ("text".equals(j2)) {
                decode = j.getBytes();
            } else if ("base16".equals(j2)) {
                decode = j.aX(j);
            } else if ("base64".equals(j2)) {
                decode = Base64.decode(j, i);
            } else if ("base64url".equals(j2)) {
                decode = Base64.decode(j, i | 8);
            } else {
                bh.t("Encode: unknown input format: " + j2);
                return di.ku();
            }
            if ("base16".equals(j3)) {
                encodeToString = j.d(decode);
            } else if ("base64".equals(j3)) {
                encodeToString = Base64.encodeToString(decode, i);
            } else if ("base64url".equals(j3)) {
                encodeToString = Base64.encodeToString(decode, i | 8);
            } else {
                bh.t("Encode: unknown output format: " + j3);
                return di.ku();
            }
            return di.r(encodeToString);
        } catch (IllegalArgumentException e) {
            bh.t("Encode: invalid input:");
            return di.ku();
        }
    }
}
