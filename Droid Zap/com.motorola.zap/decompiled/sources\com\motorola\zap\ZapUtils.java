package com.motorola.zap;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.KeyguardManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.PermissionInfo;
import android.content.pm.ResolveInfo;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.util.Log;
import android.util.TypedValue;
import android.widget.Toast;
import com.google.android.gms.drive.DriveFile;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.Settings;
import com.motorola.sharing.instrumentation.ActionResult.Error;
import com.motorola.sharing.instrumentation.CheckinInterface;
import com.motorola.sharing.util.FileUtils;
import com.motorola.sharing.util.NetworkUtils;
import com.motorola.zap.app.App;
import com.motorola.zap.app.GestureRules;
import com.motorola.zap.identity.ZapProfileSharedPerfs;
import com.motorola.zap.location.BlastLocationProvider;
import com.motorola.zap.ui.DroidBlastLauncher;
import com.motorola.zap.ui.PrivacyPolicyActivity;
import com.motorola.zap.ui.ReportAbuseActivity;
import com.motorola.zap.ui.ZapUi;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class ZapUtils {
    private static final String CAMERA_STORAGE_PATH = (FileUtils.getSdcardPath() + "/dcim");
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;

    public static boolean hasUserSignedOut() {
        return ((Boolean) Z.USER_SIGNEDOUT.read()).booleanValue() || GCSP.getAuthenticator().getAccount() == null;
    }

    public static void startSetupActivity(Intent originalIntent, boolean fromShareIntent) {
        boolean isRecoverUserAction = false;
        if (originalIntent != null) {
            isRecoverUserAction = originalIntent.getBooleanExtra(ZapConstants.EXTRA_RECOVER_USER_ACTION_INTENT, false);
        }
        if (new GestureRules().canHandleGesture(originalIntent, isRecoverUserAction, true, fromShareIntent)) {
            if (originalIntent != null) {
                originalIntent.putExtra(ZapConstants.EXTRA_RECOVER_USER_ACTION_INTENT, true);
            }
            ZapProfileSharedPerfs.resetProfile();
            ZapUi.getInstance().startSetupActivity(originalIntent);
        } else if (DBG) {
            Log.d("Zap-Utils", "Gesture is not allowed from this package for initial setup");
        }
    }

    public static void startZapApp() {
        App.getAppContext().startActivity(getStartZapIntent());
    }

    public static Intent getStartZapIntent() {
        Intent intent = new Intent(App.getAppContext(), DroidBlastLauncher.class);
        intent.setFlags(337641472);
        return intent;
    }

    public static boolean isZapAppOnTop() {
        List<RunningTaskInfo> runningTasks = ((ActivityManager) App.getAppContext().getSystemService("activity")).getRunningTasks(1);
        if (runningTasks == null || runningTasks.size() == 0) {
            return false;
        }
        return ((RunningTaskInfo) runningTasks.get(0)).baseActivity.getPackageName().equals(App.getAppContext().getPackageName());
    }

    public static boolean isZapLauncherOnTop() {
        List<RunningTaskInfo> runningTasks = ((ActivityManager) App.getAppContext().getSystemService("activity")).getRunningTasks(1);
        if (runningTasks == null || runningTasks.size() == 0) {
            return false;
        }
        return ((RunningTaskInfo) runningTasks.get(0)).topActivity.flattenToShortString().equals(App.getAppContext().getPackageName() + "/.ui.DroidBlastLauncher");
    }

    public static String getForegroundApp() {
        List<RunningTaskInfo> runningTasks = ((ActivityManager) App.getAppContext().getSystemService("activity")).getRunningTasks(1);
        if (runningTasks == null || runningTasks.size() <= 0) {
            return null;
        }
        return ((RunningTaskInfo) runningTasks.get(0)).topActivity.getPackageName().toString();
    }

    public static Intent getSystemGalleryPickerIntent(Context context, String type) {
        Intent in = new Intent("android.intent.action.GET_CONTENT");
        in.addCategory("android.intent.category.OPENABLE");
        in.addFlags(AccessibilityEventCompat.TYPE_GESTURE_DETECTION_END);
        in.putExtra("android.intent.extra.LOCAL_ONLY", true);
        in.setDataAndType(null, type);
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> activityInfo = pm.queryIntentActivities(in, 65536);
        if (activityInfo != null) {
            for (ResolveInfo r : activityInfo) {
                try {
                    if (!r.activityInfo.packageName.equalsIgnoreCase(ZapConstants.DOCUMENTS_UI_PACKAGE) && !r.activityInfo.packageName.equalsIgnoreCase(ZapConstants.VZW_VVM_PACKAGE)) {
                        ApplicationInfo appInfo = pm.getApplicationInfo(r.activityInfo.packageName, 0);
                        if (!(appInfo == null || (appInfo.flags & 1) == 0)) {
                            in.setPackage(r.activityInfo.packageName);
                            return in;
                        }
                    }
                } catch (NameNotFoundException e) {
                }
            }
        } else {
            in = null;
        }
        return in;
    }

    public static String getDefaultCameraPackage(Context context) {
        PackageManager pkgManager = context.getPackageManager();
        List<String> systemCameraPackages = new ArrayList<>();
        for (PackageInfo pkgInfo : pkgManager.getInstalledPackages(0)) {
            try {
                ApplicationInfo appInfo = pkgManager.getApplicationInfo(pkgInfo.packageName, 0);
                if (!(appInfo == null || (appInfo.flags & 1) == 0 || !pkgInfo.packageName.toLowerCase().endsWith(".camera"))) {
                    if (DBG) {
                        Log.d("Zap-Utils", "System camera package: " + pkgInfo.packageName);
                    }
                    systemCameraPackages.add(pkgInfo.packageName);
                }
            } catch (NameNotFoundException e) {
            }
        }
        if (systemCameraPackages.size() == 1) {
            return (String) systemCameraPackages.get(0);
        }
        if (DBG) {
            Log.d("Zap-Utils", "Default camera package not determined");
        }
        return null;
    }

    public static String getZapDirectoryPath() {
        return FileUtils.getSdcardPath() + "/" + ZapConstants.ZAP_DIRECTORY;
    }

    public static String getZapCameraFilePath(String fileName) {
        File zapCameraDir = getZapCameraDir();
        if (zapCameraDir == null) {
            return null;
        }
        zapCameraDir.getAbsolutePath();
        String filePath = zapCameraDir.getAbsolutePath() + fileName;
        File newFile = new File(filePath);
        try {
            if (DBG) {
                Log.d("Zap-Utils", "Creating File: " + filePath);
            }
            newFile.createNewFile();
            if (!newFile.exists()) {
                if (Log.isLoggable("Zap-Utils", 6)) {
                    Log.e("Zap-Utils", "ERROR - Could not create file");
                }
                return null;
            } else if (!DBG) {
                return filePath;
            } else {
                Log.d("Zap-Utils", "File successfully created");
                return filePath;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public static File getZapCameraDir() {
        File directory = new File((String) Settings.DOWNLOADS_DIRECTORY.get());
        if (directory.exists() || directory.mkdirs()) {
            return directory;
        }
        return null;
    }

    public static File getDemoCardFile() {
        String DEMO_CARD_IMAGE_FILE_NAME = "demo.jpg";
        Context c = App.getAppContext();
        File f = c.getFileStreamPath(DEMO_CARD_IMAGE_FILE_NAME);
        if (f.exists()) {
            return f;
        }
        InputStream in = null;
        OutputStream out = null;
        try {
            in = c.getResources().openRawResource(R.raw.demo);
            out = c.openFileOutput(DEMO_CARD_IMAGE_FILE_NAME, 0);
            byte[] buffer = new byte[1024];
            while (true) {
                int read = in.read(buffer);
                if (read != -1) {
                    out.write(buffer, 0, read);
                } else {
                    out.flush();
                    return f;
                }
            }
        } catch (IOException e) {
            if (Log.isLoggable("Zap-Utils", 6)) {
                Log.e("Zap-Utils", "Could not copy resource", e);
            }
            f.delete();
            return null;
        } finally {
            silentClose(in);
            silentClose(out);
        }
    }

    public static boolean isWifiOnlyEnabled() {
        return Integer.parseInt((String) Z.USE_CONNECTION_TYPE.read()) == 1;
    }

    public static boolean isPhoneLocked() {
        return ((KeyguardManager) App.getAppContext().getSystemService("keyguard")).inKeyguardRestrictedInputMode();
    }

    public static void setAutoProcessing(ContentContainer container, boolean value) {
        container.putCheckinData(CheckinInterface.KEY_AUTO_PROCESSING, Boolean.toString(value));
        container.getAppData().putBoolean(ZapConstants.KEY_AUTO_PROCESSING, value);
    }

    public static boolean isAutoProcessing(ContentContainer container) {
        return container.getAppData().getBoolean(ZapConstants.KEY_AUTO_PROCESSING, false);
    }

    /* JADX INFO: finally extract failed */
    public static boolean checkGeneralTransferPreConditions(Intent intent, ContentContainer container) {
        Error error = null;
        Context context = App.getAppContext();
        ZapUi ui = ZapUi.getInstance();
        try {
            boolean isAuto = isAutoProcessing(container);
            if (FileUtils.isLowStorage()) {
                if (!isAuto) {
                    ui.showDialogMessage(context.getString(R.string.dialog_title_generic), context.getString(R.string.dialog_low_memory_error), true);
                }
                Error error2 = Error.LOW_STORAGE_MEMORY;
                if (0 == 0) {
                    if (error2 == null) {
                        error2 = Error.UNKNOWN;
                    }
                    container.actionResult.setError(error2);
                    GCSP.getCheckinManager().logZapEvent(container);
                    if (Log.isLoggable("Zap-Utils", 6)) {
                        Log.e("Zap-Utils", "ERROR - " + container.actionResult.getErrorText());
                    }
                }
                return false;
            }
            NetworkInfo networkInfo = NetworkUtils.getActiveNetworkInfo(context);
            if (networkInfo == null) {
                if (!isAuto) {
                    ui.showConnectNetworkDialog(intent);
                }
                Error error3 = Error.NO_DATA_CONNECTION;
                if (0 == 0) {
                    if (error3 == null) {
                        error3 = Error.UNKNOWN;
                    }
                    container.actionResult.setError(error3);
                    GCSP.getCheckinManager().logZapEvent(container);
                    if (Log.isLoggable("Zap-Utils", 6)) {
                        Log.e("Zap-Utils", "ERROR - " + container.actionResult.getErrorText());
                    }
                }
                return false;
            } else if (isWifiOnlyEnabled() && !NetworkUtils.isWifiConnected(context, networkInfo)) {
                if (!isAuto) {
                    ui.showWifiOnlyNotConnectedDialog(intent);
                }
                Error error4 = Error.WIFI_ONLY_ALLOWED;
                if (0 == 0) {
                    if (error4 == null) {
                        error4 = Error.UNKNOWN;
                    }
                    container.actionResult.setError(error4);
                    GCSP.getCheckinManager().logZapEvent(container);
                    if (Log.isLoggable("Zap-Utils", 6)) {
                        Log.e("Zap-Utils", "ERROR - " + container.actionResult.getErrorText());
                    }
                }
                return false;
            } else if (BlastLocationProvider.getInstance().isLocationEnabled() || container.isGroupRelated()) {
                if (1 == 0) {
                    if (0 == 0) {
                        error = Error.UNKNOWN;
                    }
                    container.actionResult.setError(error);
                    GCSP.getCheckinManager().logZapEvent(container);
                    if (Log.isLoggable("Zap-Utils", 6)) {
                        Log.e("Zap-Utils", "ERROR - " + container.actionResult.getErrorText());
                    }
                }
                return true;
            } else {
                ui.showEnableLocationDialog(intent);
                Error error5 = Error.LOCATION_DISABLED;
                if (0 == 0) {
                    if (error5 == null) {
                        error5 = Error.UNKNOWN;
                    }
                    container.actionResult.setError(error5);
                    GCSP.getCheckinManager().logZapEvent(container);
                    if (Log.isLoggable("Zap-Utils", 6)) {
                        Log.e("Zap-Utils", "ERROR - " + container.actionResult.getErrorText());
                    }
                }
                return false;
            }
        } catch (Throwable th) {
            if (0 == 0) {
                if (0 == 0) {
                    error = Error.UNKNOWN;
                }
                container.actionResult.setError(error);
                GCSP.getCheckinManager().logZapEvent(container);
                if (Log.isLoggable("Zap-Utils", 6)) {
                    Log.e("Zap-Utils", "ERROR - " + container.actionResult.getErrorText());
                }
            }
            throw th;
        }
    }

    public static void viewImage(Context context, String filePath) {
        Uri content;
        String mimeType = FileUtils.getMimeTypeFromFileName(filePath);
        if (!new File(filePath).exists()) {
            Toast.makeText(context, R.string.media_has_been_deleted, 0).show();
            return;
        }
        if (FileUtils.isVideo(mimeType)) {
            content = FileUtils.getVideoContentUri(context, filePath);
        } else {
            content = FileUtils.getImageContentUri(context, filePath);
        }
        if (content == null) {
            if (Log.isLoggable("Zap-Utils", 5)) {
                Log.w("Zap-Utils", "Could not find Content URI for this file - using file path");
            }
            content = Uri.fromFile(new File(filePath));
        }
        try {
            Intent i = new Intent("android.intent.action.VIEW");
            i.setDataAndType(content, mimeType);
            context.startActivity(i);
        } catch (ActivityNotFoundException e) {
            ZapUi.getInstance().showDialogMessage(context.getString(R.string.dialog_no_activity_title), context.getString(R.string.dialog_no_activity_msg), false);
        }
    }

    public static void reportImage(Context context, String str, String str2, String str3) {
        Intent intent = new Intent(context, ReportAbuseActivity.class);
        intent.setFlags(DriveFile.MODE_READ_ONLY);
        intent.putExtra(ZapConstants.EXTRA_SERVER_SHARE_ID, str3);
        intent.putExtra(ZapConstants.EXTRA_FROM_ID, str);
        intent.putExtra(ZapConstants.EXTRA_REPORT_FILE_PATH, str2);
        intent.putExtra(ZapConstants.EXTRA_REPORT_MIMETYPE, FileUtils.getMimeTypeFromFileName(str2));
        intent.putExtra(ZapConstants.EXTRA_REPORT_TIMESTAMP, String.valueOf(System.currentTimeMillis()));
        context.startActivity(intent);
    }

    public static int dipToPix(Context context, int dp) {
        return (int) TypedValue.applyDimension(1, (float) dp, context.getResources().getDisplayMetrics());
    }

    private static void silentClose(InputStream in) {
        if (in != null) {
            try {
                in.close();
            } catch (IOException e) {
            }
        }
    }

    private static void silentClose(OutputStream out) {
        if (out != null) {
            try {
                out.close();
            } catch (IOException e) {
            }
        }
    }

    public static boolean isValidPermission(Context context, String permission) {
        PackageManager pm = context.getPackageManager();
        try {
            PermissionInfo pInfo = pm.getPermissionInfo(permission, 0);
            if (DBG) {
                Log.d("Zap-Utils", String.format("Checking permission to %s %s", new Object[]{pInfo.packageName, pInfo.name}));
            }
            if (pm.checkSignatures(context.getPackageName(), pInfo.packageName) == 0) {
                return true;
            }
            try {
                if ((pm.getApplicationInfo(pInfo.packageName, 0).flags & 1) == 0) {
                    return false;
                }
                return true;
            } catch (NameNotFoundException e) {
                return false;
            }
        } catch (NameNotFoundException e2) {
            return false;
        }
    }

    public static void showGlobalPrivacyPolicy(Context context) {
        if (ZapConfig.isMotoDevice()) {
            Intent intent = new Intent(ZapConstants.ACTION_MOTO_PRIVACY);
            intent.putExtra(ZapConstants.EXTRA_TOPIC_ID, "zap");
            intent.setFlags(DriveFile.MODE_READ_ONLY);
            context.startActivity(intent);
            return;
        }
        Intent intent2 = new Intent(context, PrivacyPolicyActivity.class);
        intent2.setFlags(DriveFile.MODE_READ_ONLY);
        context.startActivity(intent2);
    }
}
