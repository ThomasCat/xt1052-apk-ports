package com.motorola.sharing.provider.cloud;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.motorola.gcsp.json.DeviceInfo;
import com.motorola.gcsp.json.ExcludeItem;
import com.motorola.gcsp.json.GetSharesList;
import com.motorola.gcsp.json.ShareFilter;
import com.motorola.gcsp.json.ShareFilterLocation;
import com.motorola.gcsp.json.ShareFilterPin;
import com.motorola.gcsp.json.ShareFilterTarget;
import com.motorola.gcsp.json.StatusCode;
import com.motorola.sharing.Abuse.AbuseAction;
import com.motorola.sharing.Authenticator;
import com.motorola.sharing.Authenticator.Callback;
import com.motorola.sharing.Authenticator.TokenInfo;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentContainer.DiscoveryParams;
import com.motorola.sharing.ContentContainer.P2pParams;
import com.motorola.sharing.ContentDiscoveryListener.ContentDiscoveryEvent;
import com.motorola.sharing.ContentDiscoveryListener.ContentDiscoveryEvent.Substatus;
import com.motorola.sharing.ContentListener.Event;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener.GroupEvent.Type;
import com.motorola.sharing.GroupManager.GroupContract;
import com.motorola.sharing.GroupManager.GroupContract.Columns;
import com.motorola.sharing.GroupManager.GroupInfo;
import com.motorola.sharing.ProfileManager;
import com.motorola.sharing.instrumentation.ActionResult.Error;
import com.motorola.sharing.instrumentation.ActionResult.Status;
import com.motorola.sharing.instrumentation.CheckinManager;
import com.motorola.sharing.provider.cloud.group.GroupHandler;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker.GroupState;
import com.motorola.sharing.util.DeviceUtils;
import com.motorola.sharing.util.HistoryManager;
import com.motorola.sharing.util.HistoryManager.Shares.C;
import com.motorola.sharing.util.Logger;
import com.motorola.sharing.util.PackageUtils;
import com.motorola.zap.ZapConstants;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

class ContentDiscovery {
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getCloudTag("ContentDiscovery");
    private Callback mAuthCallback;
    private Context mContext;
    /* access modifiers changed from: private */
    public DiscoveryParams mDiscoveryParams;
    private boolean mIsRetrying;
    private boolean mIsStarted;

    private class ContentDiscoveryTask extends AsyncTask<Void, Void, Void> {
        private ContentDiscoveryTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            if (Logger.isDebuggable()) {
                Logger.d(ContentDiscovery.TAG, "---------------------");
                Logger.d(ContentDiscovery.TAG, "Content Discovery:");
                Logger.d(ContentDiscovery.TAG, "  - userAccount: " + Logger.obfuscate(ContentDiscovery.this.mDiscoveryParams.userCred.account));
                Logger.d(ContentDiscovery.TAG, "  - userToken length: " + (ContentDiscovery.this.mDiscoveryParams.userCred.token != null ? ContentDiscovery.this.mDiscoveryParams.userCred.token.length() : 0));
                Logger.d(ContentDiscovery.TAG, "  - userId length: " + (ContentDiscovery.this.mDiscoveryParams.userCred.id != null ? ContentDiscovery.this.mDiscoveryParams.userCred.id.length() : 0));
                Logger.d(ContentDiscovery.TAG, "  - pin length: " + (ContentDiscovery.this.mDiscoveryParams.pin != null ? ContentDiscovery.this.mDiscoveryParams.pin.length() : 0));
                if (ContentDiscovery.this.mDiscoveryParams.locality != null) {
                    Logger.d(ContentDiscovery.TAG, "  - latitude: " + ContentDiscovery.this.mDiscoveryParams.locality.latitude);
                    Logger.d(ContentDiscovery.TAG, "  - longitude: " + ContentDiscovery.this.mDiscoveryParams.locality.longitude);
                }
                if (ContentDiscovery.this.mDiscoveryParams.groupInfo != null) {
                    Logger.d(ContentDiscovery.TAG, "  - groupOwner: " + Logger.obfuscate(ContentDiscovery.this.mDiscoveryParams.groupInfo.owner));
                    Logger.d(ContentDiscovery.TAG, "  - groupId: " + ContentDiscovery.this.mDiscoveryParams.groupInfo.id);
                }
                Logger.d(ContentDiscovery.TAG, "  - deviceModel: " + DeviceUtils.getDeviceModelInfo());
                Logger.d(ContentDiscovery.TAG, "---------------------");
            }
            JSONArray availableItems = ContentDiscovery.this.getAvailableMetadata();
            if (CloudData.isOperationCanceled(ContentDiscovery.this.mDiscoveryParams)) {
                return null;
            }
            if (availableItems == null) {
                return null;
            }
            Logger.d(ContentDiscovery.TAG, "content discovered: " + availableItems.length());
            if (availableItems.length() == 0) {
                Logger.i(ContentDiscovery.TAG, "no shared content available in server");
                ContentDiscovery.this.mDiscoveryParams.actionResult.setSuccess(Status.NO_CONTENT);
                ContentDiscovery.this.mDiscoveryParams.onContentDiscoveryEvent(Event.Status.SUCCESS, Substatus.ENDED, CloudProvider.getInstance().getEndpoint());
                CheckinManager.getInstance().logZapEvent(ContentDiscovery.this.mDiscoveryParams);
                return null;
            }
            int i = 0;
            while (i < availableItems.length()) {
                if (CloudData.isOperationCanceled(ContentDiscovery.this.mDiscoveryParams)) {
                    return null;
                }
                ContentContainer container = ContentDiscovery.this.mDiscoveryParams.generateContainer();
                try {
                    JSONObject itemJson = availableItems.getJSONObject(i);
                    StatusCode itemStatus = StatusCode.codeOf(itemJson.getString("status"));
                    if (itemStatus == StatusCode.OK) {
                        String from = itemJson.getString("from");
                        String targetId = null;
                        if (itemJson.has("targetId")) {
                            targetId = itemJson.getString("targetId");
                        }
                        container.profile = ProfileManager.getProfile(itemJson);
                        if (targetId != null) {
                            String targetName = null;
                            if (itemJson.has("targetName")) {
                                targetName = itemJson.getString("targetName");
                            }
                            String targetThumbnail = null;
                            if (itemJson.has("thumbnail")) {
                                targetThumbnail = itemJson.getString("thumbnail");
                            }
                            boolean locked = false;
                            if (itemJson.has("locked")) {
                                locked = itemJson.getBoolean("locked");
                            }
                            GroupInfo groupInfo = new GroupInfo(false, from, targetId, targetName, targetThumbnail, locked, null, 0, 0);
                            container.groupInfo = groupInfo;
                            ContentDiscoveryEvent contentDiscoveryEvent = new ContentDiscoveryEvent(Event.Status.INFO, Substatus.GROUP_FOUND, container, CloudProvider.getInstance().getEndpoint());
                            contentDiscoveryEvent.groupInfo = groupInfo;
                            container.onContentDiscoveryEvent(contentDiscoveryEvent);
                            GroupHandler.getInstance().handleGroupInvite(groupInfo, container.profile);
                        } else {
                            container.origin = from;
                            container.shareId = itemJson.getString("shareId");
                            if (itemJson.has("mediaUrl")) {
                                container.url = itemJson.getString("mediaUrl");
                                if (container.url != null) {
                                    container.url = container.url.replaceAll("http://", "https://");
                                }
                            }
                            if (itemJson.has("name")) {
                                container.name = itemJson.getString("name");
                            }
                            if (itemJson.has(C.SIZE)) {
                                container.size = Integer.parseInt(itemJson.getString(C.SIZE));
                            }
                            if (itemJson.has(C.MIME_TYPE)) {
                                container.mimeType = itemJson.getString(C.MIME_TYPE);
                            }
                            if (itemJson.has("thumbnail")) {
                                container.thumbnailBase64 = itemJson.getString("thumbnail");
                            }
                            if (itemJson.has("p2pCred")) {
                                JSONObject customJson = itemJson.getJSONObject("p2pCred");
                                if (customJson.has(C.P2P_URL) && customJson.has(C.P2P_NAME) && customJson.has(C.P2P_PASSWORD)) {
                                    container.setP2pParams(new P2pParams(customJson.getString(C.P2P_URL), customJson.getString(C.P2P_NAME), customJson.getString(C.P2P_PASSWORD)));
                                }
                            }
                            container.onContentDiscoveryEvent(Event.Status.INFO, Substatus.CONTENT_FOUND, CloudProvider.getInstance().getEndpoint());
                        }
                    } else if (itemStatus == StatusCode.LOCKED) {
                        container.onContentDiscoveryEvent(Event.Status.INFO, Substatus.LOCKED_CONTENT_FOUND, CloudProvider.getInstance().getEndpoint());
                    } else if (itemStatus == StatusCode.NO_MATCH) {
                        container.actionResult.setError(Error.INVALID_PIN);
                        CheckinManager.getInstance().logZapEvent(container);
                    } else if (itemStatus == StatusCode.ERR_009 || itemStatus == StatusCode.ERR_040) {
                        JSONObject target = itemJson.getJSONArray("activeTargets").getJSONObject(0);
                        String targetId2 = target.getString("targetId");
                        String targetOwner = target.getJSONObject("targetMeta").getString("targetOwner");
                        if (GroupHandler.getInstance().isLeavingGroup()) {
                            GroupHandler.getInstance().leaveGroup(targetOwner, targetId2);
                        } else {
                            GroupHandler.getInstance().syncGroupState();
                        }
                    } else if (itemStatus == StatusCode.ERR_010) {
                        GroupHandler.getInstance().handleGroupExpired();
                        GroupHandler.getInstance().syncGroupState();
                    } else if (itemStatus == StatusCode.ERR_044 || itemStatus == StatusCode.INACTIVE) {
                        GroupHandler.getInstance().handleGroupExpired();
                    } else {
                        Logger.e(ContentDiscovery.TAG, "invalid server response status: " + itemStatus);
                        container.actionResult.setError(Error.INVALID_STATUS, itemStatus.name(), 0);
                        CheckinManager.getInstance().logZapEvent(container);
                    }
                    i++;
                } catch (JSONException e) {
                    Logger.e(ContentDiscovery.TAG, "response not parsable", e);
                    ContentDiscovery.this.mDiscoveryParams.actionResult.setError(Error.RESPONSE_NOT_PARSABLE);
                    ContentDiscovery.this.mDiscoveryParams.onContentDiscoveryEvent(Event.Status.ERROR, Substatus.GENERAL, CloudProvider.getInstance().getEndpoint());
                    CheckinManager.getInstance().logZapEvent(ContentDiscovery.this.mDiscoveryParams);
                    return null;
                }
            }
            ContentDiscovery.this.mDiscoveryParams.onContentDiscoveryEvent(Event.Status.SUCCESS, Substatus.ENDED, CloudProvider.getInstance().getEndpoint());
            return null;
        }
    }

    ContentDiscovery(DiscoveryParams discoveryParams) {
        this.mIsStarted = false;
        this.mIsRetrying = false;
        this.mContext = null;
        this.mDiscoveryParams = null;
        this.mAuthCallback = new Callback() {
            public void onAuthComplete(Authenticator.Status status, TokenInfo tokenInfo, Object data) {
                ContentDiscovery.this.mDiscoveryParams.userCred.set(tokenInfo);
                if (!ContentDiscovery.this.mDiscoveryParams.userCred.isEmpty()) {
                    ContentDiscovery.this.startTask();
                    return;
                }
                Logger.e(ContentDiscovery.TAG, "authentication failed");
                ContentDiscovery.this.mDiscoveryParams.actionResult.setError(status == Authenticator.Status.ERROR_NETWORK ? Error.AUTH_NETWORK_ERROR : Error.NOT_AUTHORIZED);
                ContentDiscovery.this.mDiscoveryParams.onContentDiscoveryEvent(Event.Status.ERROR, Substatus.AUTH, CloudProvider.getInstance().getEndpoint());
                CheckinManager.getInstance().logZapEvent(ContentDiscovery.this.mDiscoveryParams);
            }
        };
        this.mContext = GCSP.getContext();
        this.mDiscoveryParams = discoveryParams;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        if (r0.isValid() != false) goto L_0x0030;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001b, code lost:
        com.motorola.sharing.util.Logger.d(TAG, "no valid local token, requesting a new one");
        com.motorola.sharing.GCSP.getAuthenticator().authenticate(r5.mAuthCallback, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0030, code lost:
        r5.mDiscoveryParams.userCred.set(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003f, code lost:
        if (r5.mDiscoveryParams.userCred.isEmpty() == false) goto L_0x0065;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0041, code lost:
        r5.mDiscoveryParams.actionResult.setError(com.motorola.sharing.instrumentation.ActionResult.Error.NOT_AUTHORIZED);
        r5.mDiscoveryParams.onContentDiscoveryEvent(com.motorola.sharing.ContentListener.Event.Status.ERROR, com.motorola.sharing.ContentDiscoveryListener.ContentDiscoveryEvent.Substatus.AUTH, com.motorola.sharing.provider.cloud.CloudProvider.getInstance().getEndpoint());
        com.motorola.sharing.instrumentation.CheckinManager.getInstance().logZapEvent(r5.mDiscoveryParams);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0065, code lost:
        startTask();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000b, code lost:
        r0 = com.motorola.sharing.GCSP.getAuthenticator().getTokenInfo();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r0 == null) goto L_0x001b;
     */
    public void start() {
        synchronized (this) {
            if (!this.mIsStarted) {
                this.mIsStarted = true;
            }
        }
    }

    /* access modifiers changed from: private */
    public void startTask() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                new ContentDiscoveryTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
            }
        });
    }

    /* access modifiers changed from: private */
    public JSONArray getAvailableMetadata() {
        String imei = DeviceUtils.getImei(this.mContext);
        GetSharesList payload = new GetSharesList();
        payload.appId = "1234";
        payload.fromDeviceId = imei;
        payload.fromEmail = this.mDiscoveryParams.userCred.account;
        if (this.mDiscoveryParams.locality != null) {
            payload.shareFilter = new ShareFilter();
            payload.shareFilter.location = new ShareFilterLocation();
            payload.shareFilter.location.lat = Double.valueOf(this.mDiscoveryParams.locality.latitude);
            payload.shareFilter.location.lng = Double.valueOf(this.mDiscoveryParams.locality.longitude);
            payload.shareFilter.location.distance = this.mDiscoveryParams.locality.distance;
        }
        if (!TextUtils.isEmpty(this.mDiscoveryParams.pin)) {
            if (payload.shareFilter == null) {
                payload.shareFilter = new ShareFilter();
            }
            payload.shareFilter.pin = new ShareFilterPin();
            payload.shareFilter.pin.pin = this.mDiscoveryParams.pin;
        }
        if (this.mDiscoveryParams.groupInfo != null) {
            if (payload.shareFilter == null) {
                payload.shareFilter = new ShareFilter();
            }
            payload.shareFilter.target = new ShareFilterTarget();
            payload.shareFilter.target.targetId = this.mDiscoveryParams.groupInfo.id;
            String changeId = GroupStateTracker.getInstance().getState().getMemberListVersion();
            if (changeId == null) {
                changeId = "0";
            }
            payload.shareFilter.target.changeId = changeId;
        }
        ArrayList<ExcludeItem> excludeItems = putExcludeList(payload);
        payload.appVersion = Integer.valueOf(PackageUtils.getAppVersionCode(this.mContext));
        payload.settingsVersion = (Integer) CloudSettings.SETTINGS_VERSION.get();
        payload.deviceInfo = new DeviceInfo();
        payload.deviceInfo.imei = imei;
        payload.deviceInfo.model = DeviceUtils.getDeviceModelInfo();
        payload.deviceInfo.serial = Build.SERIAL;
        if (CloudData.isOperationCanceled(this.mDiscoveryParams)) {
            return null;
        }
        try {
            String jsonPayload = new Gson().toJson((Object) payload);
            Logger.d(TAG, "Get Shares Request:");
            Logger.dumpJson(TAG, jsonPayload);
            String response = HttpHelper.sendPostRequest(UrlHelper.getShares(), jsonPayload, this.mDiscoveryParams.userCred.token, this.mDiscoveryParams.userCred.id, true);
            if (response == null) {
                Logger.e(TAG, "got empty server response");
                this.mDiscoveryParams.actionResult.setError(Error.EMPTY_RESPONSE);
                this.mDiscoveryParams.onContentDiscoveryEvent(Event.Status.ERROR, Substatus.GENERAL, CloudProvider.getInstance().getEndpoint());
                CheckinManager.getInstance().logZapEvent(this.mDiscoveryParams);
                return null;
            } else if (CloudData.isOperationCanceled(this.mDiscoveryParams)) {
                return null;
            } else {
                JSONArray availableItems = null;
                JSONObject responseData = getResponseData(response);
                if (responseData != null) {
                    try {
                        if (responseData.has("authErr")) {
                            String authErr = responseData.getString("authErr");
                            Logger.w(TAG, "auth ERROR: " + authErr);
                            if (authErr.equals("fail")) {
                                this.mDiscoveryParams.actionResult.setError(Error.NOT_AUTHORIZED);
                                this.mDiscoveryParams.onContentDiscoveryEvent(Event.Status.ERROR, Substatus.AUTH, CloudProvider.getInstance().getEndpoint());
                                CheckinManager.getInstance().logZapEvent(this.mDiscoveryParams);
                            }
                            return null;
                        } else if (responseData.has("availableItems")) {
                            availableItems = responseData.getJSONArray("availableItems");
                        }
                    } catch (JSONException e) {
                        Logger.e(TAG, "response not parsable: " + e);
                        responseData = null;
                    }
                }
                if (responseData == null) {
                    this.mDiscoveryParams.actionResult.setError(Error.RESPONSE_NOT_PARSABLE);
                    this.mDiscoveryParams.onContentDiscoveryEvent(Event.Status.ERROR, Substatus.GENERAL, CloudProvider.getInstance().getEndpoint());
                    CheckinManager.getInstance().logZapEvent(this.mDiscoveryParams);
                    return availableItems;
                } else if (availableItems == null) {
                    this.mDiscoveryParams.actionResult.setError(Error.INVALID_RESPONSE);
                    this.mDiscoveryParams.onContentDiscoveryEvent(Event.Status.ERROR, Substatus.INVALID_METADATA, CloudProvider.getInstance().getEndpoint());
                    CheckinManager.getInstance().logZapEvent(this.mDiscoveryParams);
                    return availableItems;
                } else if (excludeItems == null || excludeItems.size() <= 0) {
                    return availableItems;
                } else {
                    List<String> downloadShareIdList = new ArrayList<>();
                    Iterator i$ = excludeItems.iterator();
                    while (i$.hasNext()) {
                        ExcludeItem item = (ExcludeItem) i$.next();
                        if (item.isDownload) {
                            downloadShareIdList.add(item.shareId);
                        } else {
                            ContentShare.removeActivePinShare(item.shareId);
                        }
                    }
                    String[] downloadShareIdArray = new String[downloadShareIdList.size()];
                    for (int i = 0; i < downloadShareIdList.size(); i++) {
                        downloadShareIdArray[i] = (String) downloadShareIdList.get(i);
                    }
                    HistoryManager.getInstance().removeDownloads(downloadShareIdArray);
                    return availableItems;
                }
            }
        } catch (UnknownHostException e2) {
            this.mDiscoveryParams.actionResult.setError(Error.UNKNOWN_HOST_EXCEPTION);
            this.mDiscoveryParams.onContentDiscoveryEvent(Event.Status.ERROR, Substatus.NETWORK, CloudProvider.getInstance().getEndpoint());
            CheckinManager.getInstance().logZapEvent(this.mDiscoveryParams);
            return null;
        } catch (ConnectionException e3) {
            this.mDiscoveryParams.actionResult.setError(Error.CONNECTION_EXCEPTION, null, e3.getStatusCode());
            this.mDiscoveryParams.onContentDiscoveryEvent(Event.Status.ERROR, Substatus.GENERAL, CloudProvider.getInstance().getEndpoint());
            CheckinManager.getInstance().logZapEvent(this.mDiscoveryParams);
            return null;
        }
    }

    private ArrayList putExcludeList(GetSharesList payload) {
        Cursor downloadsCursor = null;
        Cursor uploadsCursor = null;
        try {
            downloadsCursor = HistoryManager.getInstance().getDownloads();
            uploadsCursor = HistoryManager.getInstance().getPinProtectedShares();
            return putExcludeList(downloadsCursor, uploadsCursor, payload);
        } finally {
            if (downloadsCursor != null) {
                downloadsCursor.close();
            }
            if (uploadsCursor != null) {
                uploadsCursor.close();
            }
        }
    }

    private ArrayList putExcludeList(Cursor downloadsCursor, Cursor uploadsCursor, GetSharesList payload) {
        ArrayList<ExcludeItem> excludeList = new ArrayList<>();
        addItems(downloadsCursor, excludeList, true, "shareId");
        addItems(uploadsCursor, excludeList, false, "shareId");
        if (excludeList.size() == 0) {
            return null;
        }
        payload.excludeList = excludeList;
        return excludeList;
    }

    private void addItems(Cursor cursor, ArrayList<ExcludeItem> excludeList, boolean isDownload, String shareIdColumn) {
        Set<String> activeShares = new HashSet<>();
        if (!isDownload) {
            activeShares = ContentShare.getsActivePinSharesCopy();
        }
        for (String shareId : activeShares) {
            excludeList.add(new ExcludeItem(shareId, isDownload));
        }
        while (cursor.moveToNext()) {
            String shareId2 = cursor.getString(cursor.getColumnIndex(shareIdColumn));
            if (!activeShares.contains(shareId2)) {
                excludeList.add(new ExcludeItem(shareId2, isDownload));
            }
        }
    }

    private JSONObject getResponseData(String response) {
        try {
            Logger.d(TAG, "Get Shares Response:");
            Logger.dumpJson(TAG, response);
            JSONArray serverResponse = null;
            JSONTokener jSONTokener = new JSONTokener(response);
            Object json = jSONTokener.nextValue();
            if (json instanceof JSONObject) {
                serverResponse = new JSONArray();
                serverResponse.put(json);
            } else if (json instanceof JSONArray) {
                serverResponse = (JSONArray) json;
            } else if (json instanceof String) {
                JSONTokener jSONTokener2 = new JSONTokener((String) json);
                Object json2 = jSONTokener2.nextValue();
                if (json2 instanceof JSONObject) {
                    serverResponse = new JSONArray();
                    serverResponse.put(json2);
                } else {
                    serverResponse = (JSONArray) json2;
                }
            }
            int itemsCount = 0;
            JSONObject result = new JSONObject();
            result.put("availableItems", new JSONArray());
            String myUuid = getUuid(serverResponse);
            boolean settingsUpdateRequired = false;
            for (int i = 0; i < serverResponse.length(); i++) {
                JSONObject item = serverResponse.getJSONObject(i);
                if (item.has("abuseAction")) {
                    if (AbuseActionHandler.getInstance().handleBlocking(this.mDiscoveryParams, AbuseAction.values()[Integer.parseInt(item.getString("abuseAction"))], false)) {
                        Logger.w(TAG, "report abuse status - cannot receive");
                        return getEmptyArray();
                    }
                }
                if (!settingsUpdateRequired && item.has("settingsUpdateRequired")) {
                    settingsUpdateRequired = Boolean.parseBoolean(item.getString("settingsUpdateRequired"));
                    if (settingsUpdateRequired) {
                        new SyncableSettingsDownloader(this.mContext).start(this.mDiscoveryParams.userCred.account, this.mDiscoveryParams.userCred.token, this.mDiscoveryParams.userCred.id);
                    }
                }
                if (!item.has("status")) {
                    handleNonStatusItem(myUuid, item);
                } else {
                    StatusCode status = StatusCode.codeOf(item.getString("status"));
                    Logger.d(TAG, "status: " + status);
                    if (status == StatusCode.DISABLED) {
                        if (AbuseActionHandler.getInstance().handleBlocking(this.mDiscoveryParams, AbuseAction.BLOCKED, false)) {
                            Logger.w(TAG, "report abuse - status DISABLED - cannot receive");
                            return getEmptyArray();
                        }
                    } else if (status == StatusCode.ERR_001) {
                        Logger.w(TAG, "authentication error on server");
                        if (this.mIsRetrying) {
                            Logger.e(TAG, "it's already retrying - authentication failed on server");
                            result.put("authErr", "fail");
                            return result;
                        }
                        Logger.i(TAG, "starting client authentication process to refresh token");
                        this.mIsRetrying = true;
                        GCSP.getAuthenticator().authenticate(this.mAuthCallback, null, true);
                        result.put("authErr", "retrying");
                        return result;
                    } else if (status == StatusCode.LOCKED) {
                        result.getJSONArray("availableItems").put(item);
                    } else if (status == StatusCode.NO_MATCH) {
                        result.getJSONArray("availableItems").put(item);
                    } else if (status == StatusCode.ERR_009 || status == StatusCode.ERR_010 || status == StatusCode.ERR_040 || status == StatusCode.ERR_044 || status == StatusCode.INACTIVE) {
                        result.getJSONArray("availableItems").put(item);
                    } else if (status == StatusCode.OK) {
                        String shareId = null;
                        if (item.has("shareId")) {
                            shareId = item.getString("shareId");
                        }
                        String from = item.getString("from");
                        String targetId = null;
                        if (item.has("targetId")) {
                            targetId = item.getString("targetId");
                        }
                        String targetOwner = null;
                        if (item.has("targetOwner")) {
                            targetOwner = item.getString("targetOwner");
                        }
                        if (targetId == null || targetOwner == null) {
                            boolean isRecentlyDownloaded = isRecentlyDownloaded(from, shareId);
                            boolean isSelfShared = isRecentlyUploaded(from, shareId);
                            boolean isReceiveSelfSharedAllowed = ((Boolean) CloudSettings.DISCOVER_SELF_SHARED_CONTENT.get()).booleanValue();
                            Logger.d(TAG, "content discovered (sid: " + shareId + "): " + "recently downloaded: " + isRecentlyDownloaded + ",  " + "self shared: " + isSelfShared + ((!isSelfShared || !isReceiveSelfSharedAllowed) ? ZapConstants.GOOGLE_ACCOUNT_NONE : ", but asked to receive"));
                            if (!isRecentlyDownloaded && (!isSelfShared || (isSelfShared && isReceiveSelfSharedAllowed))) {
                                result.getJSONArray("availableItems").put(item);
                                itemsCount++;
                                if (itemsCount >= this.mDiscoveryParams.maxResultsCount) {
                                    Logger.i(TAG, "max number of received items per request reached: " + this.mDiscoveryParams.maxResultsCount);
                                    return result;
                                }
                            }
                        } else if (!GCSP.getGroupManager().isAnyGroupActive() && !from.equals(this.mDiscoveryParams.userCred.account)) {
                            result.getJSONArray("availableItems").put(item);
                            itemsCount++;
                            if (itemsCount >= this.mDiscoveryParams.maxResultsCount) {
                                Logger.i(TAG, "max number of received items per request reached: " + this.mDiscoveryParams.maxResultsCount);
                                return result;
                            }
                        }
                    } else {
                        continue;
                    }
                }
            }
            return result;
        } catch (Exception e) {
            Logger.e(TAG, "response not parsable", e);
            return null;
        }
    }

    private JSONObject getEmptyArray() throws JSONException {
        JSONObject result = new JSONObject();
        result.put("availableItems", new JSONArray());
        return result;
    }

    private String getUuid(JSONArray serverResponse) throws JSONException {
        for (int i = 0; i < serverResponse.length(); i++) {
            JSONObject item = serverResponse.getJSONObject(i);
            if (item.has("myProf")) {
                JSONObject myProf = item.getJSONObject("myProf");
                if (myProf.has("userUUID")) {
                    return myProf.getString("userUUID");
                }
            }
        }
        return null;
    }

    private void handleNonStatusItem(String myUuid, JSONObject item) throws JSONException {
        if (item.has("targetMemberCount")) {
            int memberCount = item.getInt("targetMemberCount");
            GroupState info = GroupStateTracker.getInstance().getState();
            GroupStateTracker.getInstance().setCurrentMembersCount(memberCount, info.getOwner(), info.getId());
            GCSP.getGroupManager().sendEvent(Type.GROUP_MODIFIED);
        }
        if (item.has("memberEvents")) {
            String changeId = item.getString("changeId");
            JSONArray profiles = item.getJSONArray("memberEvents");
            if (profiles.length() > 0) {
                try {
                    updateMemberList(myUuid, changeId, profiles);
                } catch (SQLiteException e) {
                    Logger.e(TAG, "Exception while trying to update member list", e);
                    GroupStateTracker.getInstance().setMemberListVersion(null);
                }
            }
        }
    }

    private void updateMemberList(String str, String str2, JSONArray jSONArray) throws JSONException {
        for (int i = 0; i < jSONArray.length(); i++) {
            String id = GroupStateTracker.getInstance().getState().getId();
            Uri membersContentUri = GroupContract.getMembersContentUri();
            Context context = GCSP.getContext();
            JSONObject jSONObject = jSONArray.getJSONObject(i);
            ContentValues contentValues = getContentValues(jSONObject);
            if (!TextUtils.equals(str, contentValues.getAsString(Columns.USER_ID))) {
                contentValues.put("groupId", id);
                String string = jSONObject.getString("event");
                if (string.equals("JOIN_TARGET")) {
                    context.getContentResolver().insert(membersContentUri, contentValues);
                } else if (string.equals("LEAVE_TARGET")) {
                    context.getContentResolver().delete(membersContentUri, GroupContract.SELECTION_MEMBER, new String[]{id, contentValues.getAsString(Columns.USER_ID)});
                } else if (string.equals("BLOCK_MEMBER")) {
                    contentValues.put(Columns.BLOCKED, Boolean.valueOf(true));
                    context.getContentResolver().update(membersContentUri, contentValues, GroupContract.SELECTION_MEMBER, new String[]{id, contentValues.getAsString(Columns.USER_ID)});
                } else if (string.equals("UPDATE_MEMBER")) {
                }
            }
        }
        GroupStateTracker.getInstance().setMemberListVersion(str2);
    }

    private ContentValues getContentValues(JSONObject jSONObject) throws JSONException {
        ContentValues contentValues = new ContentValues();
        if (jSONObject.has("userUUID")) {
            contentValues.put(Columns.USER_ID, jSONObject.getString("userUUID"));
        }
        if (jSONObject.has(Columns.PROFILE_VERSION)) {
            contentValues.put(Columns.PROFILE_VERSION, jSONObject.getString(Columns.PROFILE_VERSION));
        }
        if (jSONObject.has("nickName")) {
            contentValues.put(Columns.NICKNAME, jSONObject.getString("nickName"));
        }
        if (jSONObject.has("avatarUrl")) {
            contentValues.put(Columns.PICTURE_URL, jSONObject.getString("avatarUrl"));
        }
        return contentValues;
    }

    private boolean isRecentlyDownloaded(String from, String shareId) {
        return HistoryManager.getInstance().downloadExists(shareId);
    }

    private boolean isRecentlyUploaded(String from, String shareId) {
        return TextUtils.equals(from, GCSP.getAuthenticator().getAccount());
    }
}
