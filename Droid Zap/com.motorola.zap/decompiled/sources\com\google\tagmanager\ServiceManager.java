package com.google.tagmanager;

abstract class ServiceManager {
    /* access modifiers changed from: 0000 */
    public abstract void onRadioPowered();

    /* access modifiers changed from: 0000 */
    public abstract void updateConnectivityStatus(boolean z);

    ServiceManager() {
    }
}
