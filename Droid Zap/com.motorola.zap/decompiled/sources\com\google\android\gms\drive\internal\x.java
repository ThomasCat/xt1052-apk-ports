package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.drive.DriveId;

public class x implements Creator<ListParentsRequest> {
    static void a(ListParentsRequest listParentsRequest, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.c(parcel, 1, listParentsRequest.wj);
        b.a(parcel, 2, (Parcelable) listParentsRequest.DT, i, false);
        b.D(parcel, p);
    }

    /* renamed from: M */
    public ListParentsRequest createFromParcel(Parcel parcel) {
        int o = a.o(parcel);
        int i = 0;
        DriveId driveId = null;
        while (parcel.dataPosition() < o) {
            int n = a.n(parcel);
            switch (a.S(n)) {
                case 1:
                    i = a.g(parcel, n);
                    break;
                case 2:
                    driveId = (DriveId) a.a(parcel, n, DriveId.CREATOR);
                    break;
                default:
                    a.b(parcel, n);
                    break;
            }
        }
        if (parcel.dataPosition() == o) {
            return new ListParentsRequest(i, driveId);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }

    /* renamed from: ar */
    public ListParentsRequest[] newArray(int i) {
        return new ListParentsRequest[i];
    }
}
