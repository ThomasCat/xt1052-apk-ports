package com.google.sample.castcompanionlibrary.remotecontrol;

import android.media.AudioManager;
import com.google.sample.castcompanionlibrary.utils.LogUtils;
import java.lang.reflect.Method;

public class RemoteControlHelper {
    private static boolean sHasRemoteControlAPIs;
    private static Method sRegisterRemoteControlClientMethod;
    private static Method sUnregisterRemoteControlClientMethod;

    static {
        sHasRemoteControlAPIs = false;
        try {
            Class actualRemoteControlClientClass = RemoteControlClientCompat.getActualRemoteControlClientClass(RemoteControlHelper.class.getClassLoader());
            sRegisterRemoteControlClientMethod = AudioManager.class.getMethod("registerRemoteControlClient", new Class[]{actualRemoteControlClientClass});
            sUnregisterRemoteControlClientMethod = AudioManager.class.getMethod("unregisterRemoteControlClient", new Class[]{actualRemoteControlClientClass});
            sHasRemoteControlAPIs = true;
        } catch (ClassNotFoundException | IllegalArgumentException | NoSuchMethodException | SecurityException e) {
        }
    }

    public static void registerRemoteControlClient(AudioManager audioManager, RemoteControlClientCompat remoteControlClient) {
        if (sHasRemoteControlAPIs) {
            try {
                sRegisterRemoteControlClientMethod.invoke(audioManager, new Object[]{remoteControlClient.getActualRemoteControlClientObject()});
            } catch (Exception e) {
                LogUtils.LOGE("RemoteControlHelper", e.getMessage(), e);
            }
        }
    }

    public static void unregisterRemoteControlClient(AudioManager audioManager, RemoteControlClientCompat remoteControlClient) {
        if (sHasRemoteControlAPIs) {
            try {
                sUnregisterRemoteControlClientMethod.invoke(audioManager, new Object[]{remoteControlClient.getActualRemoteControlClientObject()});
            } catch (Exception e) {
                LogUtils.LOGE("RemoteControlHelper", e.getMessage(), e);
            }
        }
    }
}
