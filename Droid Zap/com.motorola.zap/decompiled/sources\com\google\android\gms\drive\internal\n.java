package com.google.android.gms.drive.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.DriveEvent;
import com.google.android.gms.drive.events.DriveEvent.Listener;
import com.google.android.gms.drive.events.c;
import com.google.android.gms.drive.internal.u.a;
import com.google.android.gms.internal.ee;
import com.google.android.gms.internal.eh;
import com.google.android.gms.internal.eh.e;
import com.google.android.gms.internal.en;
import com.google.android.gms.internal.er;
import java.util.HashMap;
import java.util.Map;

public class n extends eh<u> {
    private DriveId DA;
    final ConnectionCallbacks DB;
    Map<DriveId, Map<Listener<?>, s<?>>> DC = new HashMap();
    private DriveId Dz;
    private final String vi;

    public n(Context context, Looper looper, ee eeVar, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, String[] strArr) {
        super(context, looper, connectionCallbacks, onConnectionFailedListener, strArr);
        this.vi = (String) er.b(eeVar.dR(), (Object) "Must call Api.ClientBuilder.setAccountName()");
        this.DB = connectionCallbacks;
    }

    /* access modifiers changed from: protected */
    /* renamed from: C */
    public u p(IBinder iBinder) {
        return a.D(iBinder);
    }

    /* access modifiers changed from: 0000 */
    public <C extends DriveEvent> PendingResult<Status> a(GoogleApiClient googleApiClient, final DriveId driveId, final int i, Listener<C> listener) {
        PendingResult<Status> b;
        er.b(c.a(i, driveId), (Object) "id");
        er.b(listener, (Object) "listener");
        er.a(isConnected(), "Client must be connected");
        synchronized (this.DC) {
            Map map = (Map) this.DC.get(driveId);
            if (map == null) {
                map = new HashMap();
                this.DC.put(driveId, map);
            }
            if (map.containsKey(listener)) {
                b = new k<>(Status.zQ);
            } else {
                final s sVar = new s(getLooper(), i, listener);
                map.put(listener, sVar);
                b = googleApiClient.b(new j() {
                    /* access modifiers changed from: protected */
                    public void a(n nVar) throws RemoteException {
                        nVar.eT().a(new AddEventListenerRequest(driveId, i), (w) sVar, (String) null, (v) new ak(this));
                    }
                });
            }
        }
        return b;
    }

    /* access modifiers changed from: protected */
    public void a(int i, IBinder iBinder, Bundle bundle) {
        if (bundle != null) {
            bundle.setClassLoader(getClass().getClassLoader());
            this.Dz = (DriveId) bundle.getParcelable("com.google.android.gms.drive.root_id");
            this.DA = (DriveId) bundle.getParcelable("com.google.android.gms.drive.appdata_id");
        }
        super.a(i, iBinder, bundle);
    }

    /* access modifiers changed from: protected */
    public void a(en enVar, e eVar) throws RemoteException {
        String packageName = getContext().getPackageName();
        er.f(eVar);
        er.f(packageName);
        er.f(ea());
        enVar.a(eVar, GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, packageName, ea(), this.vi, new Bundle());
    }

    /* access modifiers changed from: protected */
    public String aF() {
        return "com.google.android.gms.drive.ApiService.START";
    }

    /* access modifiers changed from: protected */
    public String aG() {
        return "com.google.android.gms.drive.internal.IDriveService";
    }

    /* access modifiers changed from: 0000 */
    public PendingResult<Status> b(GoogleApiClient googleApiClient, final DriveId driveId, final int i, Listener<?> listener) {
        PendingResult<Status> b;
        er.b(c.a(i, driveId), (Object) "id");
        er.b(listener, (Object) "listener");
        er.a(isConnected(), "Client must be connected");
        synchronized (this.DC) {
            Map map = (Map) this.DC.get(driveId);
            if (map == null) {
                b = new k<>(Status.zQ);
            } else {
                final s sVar = (s) map.remove(listener);
                if (sVar == null) {
                    b = new k<>(Status.zQ);
                } else {
                    if (map.isEmpty()) {
                        this.DC.remove(driveId);
                    }
                    b = googleApiClient.b(new j() {
                        /* access modifiers changed from: protected */
                        public void a(n nVar) throws RemoteException {
                            nVar.eT().a(new RemoveEventListenerRequest(driveId, i), (w) sVar, (String) null, (v) new ak(this));
                        }
                    });
                }
            }
        }
        return b;
    }

    public void disconnect() {
        u uVar = (u) eb();
        if (uVar != null) {
            try {
                uVar.a(new DisconnectRequest());
            } catch (RemoteException e) {
            }
        }
        super.disconnect();
        this.DC.clear();
    }

    public u eT() {
        return (u) eb();
    }

    public DriveId eU() {
        return this.Dz;
    }

    public DriveId eV() {
        return this.DA;
    }
}
