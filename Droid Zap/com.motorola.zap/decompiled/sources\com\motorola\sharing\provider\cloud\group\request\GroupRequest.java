package com.motorola.sharing.provider.cloud.group.request;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.motorola.gcsp.json.StatusCode;
import com.motorola.sharing.Authenticator.TokenInfo;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener.GroupEvent;
import com.motorola.sharing.GroupListener.GroupEvent.Status;
import com.motorola.sharing.GroupListener.GroupEvent.Substatus;
import com.motorola.sharing.GroupListener.GroupEvent.Type;
import com.motorola.sharing.GroupManager.GroupContract;
import com.motorola.sharing.GroupManager.GroupInfo;
import com.motorola.sharing.Locality;
import com.motorola.sharing.provider.cloud.group.GroupHandler;
import com.motorola.sharing.provider.cloud.group.GroupStateTracker;
import com.motorola.sharing.util.Logger;
import com.motorola.sharing.util.NetworkUtils;
import com.motorola.sharing.util.auth.SyncAuthenticator;

public abstract class GroupRequest implements Runnable {
    private static final String TAG = Logger.getCloudTag("GroupReq");
    protected Context mContext = null;
    protected GroupStateTracker mGroupStateTracker = null;
    private boolean mIsRetry = false;
    protected String mUserId = null;
    protected String mUserToken = null;

    public abstract Type getWhat();

    public GroupRequest(Context context, boolean isRetry) {
        this.mContext = context;
        this.mGroupStateTracker = GroupStateTracker.getInstance();
        this.mIsRetry = isRetry;
    }

    public boolean isRetry() {
        return this.mIsRetry;
    }

    /* access modifiers changed from: protected */
    public boolean authenticate() {
        TokenInfo tokenInfo = SyncAuthenticator.auth();
        if (tokenInfo == null) {
            this.mUserToken = null;
            this.mUserId = null;
            return false;
        }
        this.mUserToken = tokenInfo.getToken();
        this.mUserId = tokenInfo.getUserId();
        return true;
    }

    public Bundle getBundle() {
        return null;
    }

    public void sendStart() {
        GCSP.getGroupManager().sendEvent(getWhat(), Status.INFO, Substatus.NO_ERROR, getBundle(), getGroupInfo());
    }

    /* access modifiers changed from: protected */
    public void sendOk() {
        GCSP.getGroupManager().sendEvent(getWhat(), Status.OK, Substatus.NO_ERROR, getBundle(), getGroupInfo());
    }

    /* access modifiers changed from: protected */
    public void sendError(Substatus error) {
        Bundle data = new Bundle();
        Intent intent = getRetryIntent();
        if (intent != null) {
            Logger.d(TAG, "Retry intent available for recovery: " + intent.getAction());
            data.putParcelable(GroupEvent.KEY_RETRY_INTENT, intent);
        }
        Bundle bundle = getBundle();
        if (bundle != null) {
            data.putAll(bundle);
        }
        GCSP.getGroupManager().sendEvent(getWhat(), Status.ERROR, error, data, getGroupInfo());
    }

    /* access modifiers changed from: protected */
    public Substatus statusToError(StatusCode status) {
        switch (status) {
            case OK:
                return Substatus.NO_ERROR;
            case ERR_001:
                return Substatus.AUTHENTICATION_ERROR;
            case ERR_009:
            case ERR_026:
            case ERR_028:
            case ERR_040:
                return Substatus.NOT_ALLOWED;
            case ERR_010:
            case INACTIVE:
                return Substatus.GROUP_INACTIVE;
            case ERR_015:
                return Substatus.WRONG_PIN;
            case ERR_016:
                return Substatus.CLIENT_ERROR;
            case BLOCKED:
                return Substatus.USER_BLOCKED;
            case DISABLED:
                return Substatus.USER_DISABLED;
            default:
                return Substatus.SERVER_ERROR;
        }
    }

    public Substatus checkPreconditions() {
        return Substatus.NO_ERROR;
    }

    public Intent getRetryIntent() {
        return null;
    }

    public void onEnqueue() {
        if (!this.mIsRetry) {
            sendStart();
        }
    }

    /* access modifiers changed from: protected */
    public boolean isNetworkConnected() {
        return NetworkUtils.hasDataConnection(this.mContext);
    }

    /* access modifiers changed from: protected */
    public void onGroupStarted(String owner, String id, String name, String pin, String thumbnail, Locality locality) {
        double latitude = 0.0d;
        double longitude = 0.0d;
        if (locality != null) {
            latitude = locality.latitude;
            longitude = locality.longitude;
        }
        this.mGroupStateTracker.setActive(owner, id, name, pin, thumbnail, latitude, longitude);
        setGroupActive(true);
    }

    /* access modifiers changed from: protected */
    public void onGroupLeaveRequested(String owner, String id) {
        this.mGroupStateTracker.setLeaving(owner, id);
        setGroupActive(false);
    }

    /* access modifiers changed from: protected */
    public void onGroupStopped() {
        this.mGroupStateTracker.setInactive();
        setGroupActive(false);
    }

    /* access modifiers changed from: protected */
    public void onGroupUpdate() {
        setGroupActive(this.mGroupStateTracker.isActive());
    }

    /* access modifiers changed from: protected */
    public void onGroupInviteSent() {
        this.mGroupStateTracker.setLastInviteSentTime(System.currentTimeMillis());
    }

    private void setGroupActive(boolean active) {
        if (!active) {
            GCSP.getContext().getContentResolver().delete(GroupContract.getMembersContentUri(), null, null);
        }
        notifyGroupStateChanged(active);
    }

    private void notifyGroupStateChanged(boolean active) {
        if (active) {
            GCSP.getGroupManager().sendEvent(Type.GROUP_ACTIVE);
        } else {
            GCSP.getGroupManager().sendEvent(Type.GROUP_INACTIVE);
        }
    }

    public GroupInfo getGroupInfo() {
        return GroupHandler.getInstance().getGroupInfo();
    }
}
