package com.motorola.zap.app;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.motorola.zap.ZapConfig;
import com.motorola.zap.ui.CircleNotifierReceiver.BlastStateReceiver;

public class TransferState {
    public static final String BLAST_STATE_CHANGED_ACTION = "com.motorola.blast.BLAST_STATE_CHANGED_ACTION";
    public static final int BLAST_TRANSFER_IDLE = 0;
    public static final int BLAST_TRANSFER_RECEIVING = 3;
    public static final int BLAST_TRANSFER_SENDING = 1;
    public static final int BLAST_TRANSFER_SENT = 2;
    public static final int BLAST_TRANSFER_ZZ = 4;
    public static final String EXTRA_BLAST_STATE = "blast_state";
    private static TransferState sTransferState;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private boolean mIsZz;
    private int mReceivingCount;
    private int mSendingCount;
    private Runnable mSentTimeoutHandler;

    public static synchronized TransferState getInstance() {
        TransferState transferState;
        synchronized (TransferState.class) {
            if (sTransferState == null) {
                sTransferState = new TransferState();
            }
            transferState = sTransferState;
        }
        return transferState;
    }

    private TransferState() {
    }

    public synchronized void setZz(Context context, boolean isZz) {
        int initialState = getBlastState();
        removeSent(context, false);
        this.mIsZz = isZz;
        updateBlastState(context, initialState);
    }

    public synchronized void addSending(Context context) {
        int initialState = getBlastState();
        removeSent(context, false);
        this.mSendingCount++;
        updateBlastState(context, initialState);
    }

    public synchronized void removeSending(final Context context, long serverAvailabityTimeout) {
        int initialState = getBlastState();
        this.mSendingCount--;
        if (this.mSendingCount == 0 && this.mReceivingCount == 0 && serverAvailabityTimeout != 0) {
            this.mSentTimeoutHandler = new Runnable() {
                public void run() {
                    TransferState.this.removeSent(context, true);
                }
            };
            this.mHandler.postDelayed(this.mSentTimeoutHandler, serverAvailabityTimeout);
        }
        updateBlastState(context, initialState);
    }

    public synchronized void removeSent(Context context) {
        removeSent(context, true);
    }

    /* access modifiers changed from: private */
    public synchronized void removeSent(Context context, boolean updateBlastState) {
        int initialState = getBlastState();
        if (this.mSentTimeoutHandler != null) {
            this.mHandler.removeCallbacks(this.mSentTimeoutHandler);
            this.mSentTimeoutHandler = null;
        }
        if (updateBlastState) {
            updateBlastState(context, initialState);
        }
    }

    public synchronized void addReceiving(Context context) {
        int initialState = getBlastState();
        removeSent(context, false);
        this.mReceivingCount++;
        updateBlastState(context, initialState);
    }

    public synchronized void removeReceiving(Context context) {
        int initialState = getBlastState();
        this.mReceivingCount--;
        updateBlastState(context, initialState);
    }

    private synchronized void updateBlastState(Context context, int i) {
        if (!ZapConfig.supportsFullFeature()) {
            throw new UnsupportedOperationException("Cirlces widget is only supported in the full version");
        }
        if (Log.isLoggable("Zap-TransferState", 4)) {
            Log.i("Zap-TransferState", "updateBlastState: sending: " + this.mSendingCount + ", receiving: " + this.mReceivingCount + ", sent: " + (this.mSentTimeoutHandler != null));
        }
        int blastState = getBlastState();
        if (blastState != i) {
            Intent intent = new Intent(BLAST_STATE_CHANGED_ACTION);
            intent.setClass(context.getApplicationContext(), BlastStateReceiver.class);
            intent.putExtra(EXTRA_BLAST_STATE, blastState);
            context.sendBroadcast(intent);
        }
    }

    public synchronized int getBlastState() {
        int i;
        if (this.mIsZz) {
            i = 4;
        } else if (this.mSendingCount != 0) {
            i = 1;
        } else if (this.mReceivingCount != 0) {
            i = 3;
        } else if (this.mSentTimeoutHandler != null) {
            i = 2;
        } else {
            i = 0;
        }
        return i;
    }
}
