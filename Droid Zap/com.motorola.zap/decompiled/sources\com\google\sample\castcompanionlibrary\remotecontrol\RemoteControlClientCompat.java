package com.google.sample.castcompanionlibrary.remotecontrol;

import android.app.PendingIntent;
import android.graphics.Bitmap;
import android.os.Looper;
import android.support.v7.media.MediaRouter;
import com.google.sample.castcompanionlibrary.utils.LogUtils;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class RemoteControlClientCompat {
    /* access modifiers changed from: private */
    public static boolean sHasRemoteControlAPIs;
    private static Method sRCCEditMetadataMethod;
    private static Method sRCCSetPlayStateMethod;
    private static Method sRCCSetTransportControlFlags;
    private static Class sRemoteControlClientClass;
    private Object mActualRemoteControlClient;

    public class MetadataEditorCompat {
        public static final int METADATA_KEY_ARTWORK = 100;
        private final Object mActualMetadataEditor;
        private Method mApplyMethod;
        private Method mClearMethod;
        private Method mPutBitmapMethod;
        private Method mPutLongMethod;
        private Method mPutStringMethod;

        private MetadataEditorCompat(Object obj) {
            if (!RemoteControlClientCompat.sHasRemoteControlAPIs || obj != null) {
                if (RemoteControlClientCompat.sHasRemoteControlAPIs) {
                    Class cls = obj.getClass();
                    try {
                        this.mPutStringMethod = cls.getMethod("putString", new Class[]{Integer.TYPE, String.class});
                        this.mPutBitmapMethod = cls.getMethod("putBitmap", new Class[]{Integer.TYPE, Bitmap.class});
                        this.mPutLongMethod = cls.getMethod("putLong", new Class[]{Integer.TYPE, Long.TYPE});
                        this.mClearMethod = cls.getMethod("clear", new Class[0]);
                        this.mApplyMethod = cls.getMethod("apply", new Class[0]);
                    } catch (Exception e) {
                        throw new RuntimeException(e.getMessage(), e);
                    }
                }
                this.mActualMetadataEditor = obj;
                return;
            }
            throw new IllegalArgumentException("Remote Control API's exist, should not be given a null MetadataEditor");
        }

        public MetadataEditorCompat putString(int key, String value) {
            if (RemoteControlClientCompat.sHasRemoteControlAPIs) {
                try {
                    this.mPutStringMethod.invoke(this.mActualMetadataEditor, new Object[]{Integer.valueOf(key), value});
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
            return this;
        }

        public MetadataEditorCompat putBitmap(int key, Bitmap bitmap) {
            if (RemoteControlClientCompat.sHasRemoteControlAPIs) {
                try {
                    this.mPutBitmapMethod.invoke(this.mActualMetadataEditor, new Object[]{Integer.valueOf(key), bitmap});
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
            return this;
        }

        public MetadataEditorCompat putLong(int key, long value) {
            if (RemoteControlClientCompat.sHasRemoteControlAPIs) {
                try {
                    this.mPutLongMethod.invoke(this.mActualMetadataEditor, new Object[]{Integer.valueOf(key), Long.valueOf(value)});
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
            return this;
        }

        public void clear() {
            if (RemoteControlClientCompat.sHasRemoteControlAPIs) {
                try {
                    this.mClearMethod.invoke(this.mActualMetadataEditor, null);
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        }

        public void apply() {
            if (RemoteControlClientCompat.sHasRemoteControlAPIs) {
                try {
                    this.mApplyMethod.invoke(this.mActualMetadataEditor, null);
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        android.util.Log.w("RemoteControlCompat", "Could not get real field: " + r4.getName());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0052, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0053, code lost:
        android.util.Log.w("RemoteControlCompat", "Error trying to pull field value for: " + r4.getName() + " " + r0.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0080, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0081, code lost:
        android.util.Log.w("RemoteControlCompat", "Error trying to pull field value for: " + r4.getName() + " " + r0.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0050 A[Catch:{ ClassNotFoundException | SecurityException -> 0x0050, NoSuchMethodException -> 0x007e, IllegalArgumentException -> 0x00ac }, ExcHandler: ClassNotFoundException | SecurityException (e java.lang.Throwable), Splitter:B:1:0x0005] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x007e A[Catch:{ ClassNotFoundException | SecurityException -> 0x0050, NoSuchMethodException -> 0x007e, IllegalArgumentException -> 0x00ac }, ExcHandler: NoSuchMethodException (e java.lang.NoSuchMethodException), Splitter:B:1:0x0005] */
    static {
        sHasRemoteControlAPIs = false;
        try {
            sRemoteControlClientClass = getActualRemoteControlClientClass(RemoteControlClientCompat.class.getClassLoader());
            Field[] fields = RemoteControlClientCompat.class.getFields();
            int length = fields.length;
            for (int i = 0; i < length; i++) {
                Field field = fields[i];
                field.set(null, sRemoteControlClientClass.getField(field.getName()).get(null));
            }
            sRCCEditMetadataMethod = sRemoteControlClientClass.getMethod("editMetadata", new Class[]{Boolean.TYPE});
            sRCCSetPlayStateMethod = sRemoteControlClientClass.getMethod("setPlaybackState", new Class[]{Integer.TYPE});
            sRCCSetTransportControlFlags = sRemoteControlClientClass.getMethod("setTransportControlFlags", new Class[]{Integer.TYPE});
            sHasRemoteControlAPIs = true;
        } catch (ClassNotFoundException | SecurityException e) {
        } catch (NoSuchMethodException e2) {
        } catch (IllegalArgumentException e3) {
        }
    }

    public static Class getActualRemoteControlClientClass(ClassLoader classLoader) throws ClassNotFoundException {
        return classLoader.loadClass("android.media.RemoteControlClient");
    }

    public RemoteControlClientCompat(PendingIntent pendingIntent) {
        if (sHasRemoteControlAPIs) {
            try {
                this.mActualRemoteControlClient = sRemoteControlClientClass.getConstructor(new Class[]{PendingIntent.class}).newInstance(new Object[]{pendingIntent});
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public RemoteControlClientCompat(PendingIntent pendingIntent, Looper looper) {
        if (sHasRemoteControlAPIs) {
            try {
                this.mActualRemoteControlClient = sRemoteControlClientClass.getConstructor(new Class[]{PendingIntent.class, Looper.class}).newInstance(new Object[]{pendingIntent, looper});
            } catch (Exception e) {
                LogUtils.LOGE("RemoteControlCompat", "Error creating new instance of " + sRemoteControlClientClass.getName(), e);
            }
        }
    }

    public MetadataEditorCompat editMetadata(boolean startEmpty) {
        Object obj;
        if (sHasRemoteControlAPIs) {
            try {
                obj = sRCCEditMetadataMethod.invoke(this.mActualRemoteControlClient, new Object[]{Boolean.valueOf(startEmpty)});
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            obj = null;
        }
        return new MetadataEditorCompat(obj);
    }

    public void setPlaybackState(int state) {
        if (sHasRemoteControlAPIs) {
            try {
                sRCCSetPlayStateMethod.invoke(this.mActualRemoteControlClient, new Object[]{Integer.valueOf(state)});
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void setTransportControlFlags(int transportControlFlags) {
        if (sHasRemoteControlAPIs) {
            try {
                sRCCSetTransportControlFlags.invoke(this.mActualRemoteControlClient, new Object[]{Integer.valueOf(transportControlFlags)});
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public final Object getActualRemoteControlClientObject() {
        return this.mActualRemoteControlClient;
    }

    public void addToMediaRouter(MediaRouter router) {
        if (this.mActualRemoteControlClient != null) {
            router.addRemoteControlClient(this.mActualRemoteControlClient);
        }
    }

    public void removeFromMediaRouter(MediaRouter router) {
        if (this.mActualRemoteControlClient != null) {
            router.removeRemoteControlClient(this.mActualRemoteControlClient);
        }
    }
}
