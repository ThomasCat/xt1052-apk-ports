package com.google.android.gms.tagmanager;

final class bh {
    static bi VE = new x();
    static int VF;

    public static void b(String str, Throwable th) {
        VE.b(str, th);
    }

    public static void c(String str, Throwable th) {
        VE.c(str, th);
    }

    public static int getLogLevel() {
        return VF;
    }

    public static void s(String str) {
        VE.s(str);
    }

    public static void setLogLevel(int logLevel) {
        VE.setLogLevel(logLevel);
    }

    public static void t(String str) {
        VE.t(str);
    }

    public static void u(String str) {
        VE.u(str);
    }

    public static void v(String str) {
        VE.v(str);
    }

    public static void w(String str) {
        VE.w(str);
    }
}
