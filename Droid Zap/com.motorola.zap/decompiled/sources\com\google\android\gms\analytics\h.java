package com.google.android.gms.analytics;

import android.content.Context;
import com.google.analytics.tracking.android.Fields;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

class h implements m {
    private static final Object qI = new Object();
    private static h qW;
    private final Context mContext;
    /* access modifiers changed from: private */
    public String qX;
    /* access modifiers changed from: private */
    public boolean qY = false;
    /* access modifiers changed from: private */
    public final Object qZ = new Object();

    protected h(Context context) {
        this.mContext = context;
        bx();
    }

    public static h bu() {
        h hVar;
        synchronized (qI) {
            hVar = qW;
        }
        return hVar;
    }

    private String bv() {
        if (!this.qY) {
            synchronized (this.qZ) {
                if (!this.qY) {
                    aa.v("Waiting for clientId to load");
                    do {
                        try {
                            this.qZ.wait();
                        } catch (InterruptedException e) {
                            aa.t("Exception while waiting for clientId: " + e);
                        }
                    } while (!this.qY);
                }
            }
        }
        aa.v("Loaded clientId");
        return this.qX;
    }

    private void bx() {
        new Thread("client_id_fetcher") {
            public void run() {
                synchronized (h.this.qZ) {
                    h.this.qX = h.this.by();
                    h.this.qY = true;
                    h.this.qZ.notifyAll();
                }
            }
        }.start();
    }

    public static void n(Context context) {
        synchronized (qI) {
            if (qW == null) {
                qW = new h(context);
            }
        }
    }

    private boolean y(String str) {
        try {
            aa.v("Storing clientId.");
            FileOutputStream openFileOutput = this.mContext.openFileOutput("gaClientId", 0);
            openFileOutput.write(str.getBytes());
            openFileOutput.close();
            return true;
        } catch (FileNotFoundException e) {
            aa.t("Error creating clientId file.");
            return false;
        } catch (IOException e2) {
            aa.t("Error writing to clientId file.");
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public String bw() {
        String lowerCase = UUID.randomUUID().toString().toLowerCase();
        try {
            return !y(lowerCase) ? "0" : lowerCase;
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public String by() {
        String str = null;
        try {
            FileInputStream openFileInput = this.mContext.openFileInput("gaClientId");
            byte[] bArr = new byte[128];
            int read = openFileInput.read(bArr, 0, 128);
            if (openFileInput.available() > 0) {
                aa.t("clientId file seems corrupted, deleting it.");
                openFileInput.close();
                this.mContext.deleteFile("gaClientId");
            } else if (read <= 0) {
                aa.t("clientId file seems empty, deleting it.");
                openFileInput.close();
                this.mContext.deleteFile("gaClientId");
            } else {
                String str2 = new String(bArr, 0, read);
                try {
                    openFileInput.close();
                    str = str2;
                } catch (FileNotFoundException e) {
                    str = str2;
                } catch (IOException e2) {
                    str = str2;
                    aa.t("Error reading clientId file, deleting it.");
                    this.mContext.deleteFile("gaClientId");
                }
            }
        } catch (FileNotFoundException e3) {
        } catch (IOException e4) {
            aa.t("Error reading clientId file, deleting it.");
            this.mContext.deleteFile("gaClientId");
        }
        return str == null ? bw() : str;
    }

    public String getValue(String field) {
        if (Fields.CLIENT_ID.equals(field)) {
            return bv();
        }
        return null;
    }

    public boolean x(String str) {
        return Fields.CLIENT_ID.equals(str);
    }
}
