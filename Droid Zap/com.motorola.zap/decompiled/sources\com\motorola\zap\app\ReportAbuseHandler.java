package com.motorola.zap.app;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.motorola.sharing.Abuse.AbuseType;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentContainer.AbuseReportBuilder;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.Locality;
import com.motorola.sharing.instrumentation.ActionResult.Error;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.location.BlastLocationProvider.LocationResult;
import com.motorola.zap.ui.ZapUi;

public class ReportAbuseHandler {
    /* access modifiers changed from: private */
    public static final boolean DBG = LogUtils.IS_DEBUGGABLE;

    public void handleActionReportAbuse(Intent intent) {
        asyncReportAbuse(new AbuseReportBuilder().setAbuseType(AbuseType.values()[intent.getIntExtra(ZapConstants.EXTRA_ABUSE_TYPE, AbuseType.UNKNOWN.ordinal())]).setShareId(intent.getStringExtra(ZapConstants.EXTRA_SERVER_SHARE_ID)).setOrigin(intent.getStringExtra(ZapConstants.EXTRA_FROM_ID)).setPath(intent.getStringExtra(ZapConstants.EXTRA_REPORT_FILE_PATH)).setMimeType(intent.getStringExtra(ZapConstants.EXTRA_REPORT_MIMETYPE)).build());
    }

    private static void asyncReportAbuse(final ContentContainer container) {
        final Context context = App.getAppContext();
        final ZapUi ui = ZapUi.getInstance();
        final LocationHandler locationHandler = LocationHandler.getInstance();
        new Thread(new Runnable() {
            public void run() {
                if (ReportAbuseHandler.DBG) {
                    Log.d("Zap-ReportAbuseHandler", "Reporting abuse - shareId: " + container.getShareId() + " abuseType: " + container.getAbuseType());
                }
                LocationResult result = locationHandler.getLocationForCurrentModality(true);
                if (result == null || result.location == null) {
                    if (Log.isLoggable("Zap-ReportAbuseHandler", 6)) {
                        Log.e("Zap-ReportAbuseHandler", "ERROR - Could not acquire accurate location to report abuse");
                    }
                    ui.showDialogMessage(context.getString(R.string.dialog_title_location_not_determined), context.getString(R.string.dialog_msg_no_accurate_location), true);
                    container.actionResult.setError(Error.LOCATION_NOT_ACCURATE);
                    GCSP.getCheckinManager().logZapAbuse(container);
                    return;
                }
                Locality locality = new Locality();
                locality.latitude = result.location.getLatitude();
                locality.longitude = result.location.getLongitude();
                container.setLocality(locality);
                GCSP.getContentDispatcher().reportAbuse(container, ui.getReportAbuseListener());
            }
        }).start();
    }
}
