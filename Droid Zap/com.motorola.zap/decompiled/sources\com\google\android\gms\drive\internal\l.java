package com.google.android.gms.drive.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.CreateFileActivityBuilder;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveApi.ContentsResult;
import com.google.android.gms.drive.DriveApi.DriveIdResult;
import com.google.android.gms.drive.DriveApi.MetadataBufferResult;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.OpenFileActivityBuilder;
import com.google.android.gms.drive.query.Query;

public class l implements DriveApi {

    static class a implements ContentsResult {
        private final Contents CW;
        private final Status vl;

        public a(Status status, Contents contents) {
            this.vl = status;
            this.CW = contents;
        }

        public Contents getContents() {
            return this.CW;
        }

        public Status getStatus() {
            return this.vl;
        }
    }

    private static class b extends c {
        private final com.google.android.gms.common.api.a.c<DriveIdResult> vj;

        public b(com.google.android.gms.common.api.a.c<DriveIdResult> cVar) {
            this.vj = cVar;
        }

        public void a(OnMetadataResponse onMetadataResponse) throws RemoteException {
            this.vj.b(new c(Status.zQ, new j(onMetadataResponse.fe()).getDriveId()));
        }

        public void l(Status status) throws RemoteException {
            this.vj.b(new c(status, null));
        }
    }

    static class c implements DriveIdResult {
        private final DriveId CS;
        private final Status vl;

        public c(Status status, DriveId driveId) {
            this.vl = status;
            this.CS = driveId;
        }

        public DriveId getDriveId() {
            return this.CS;
        }

        public Status getStatus() {
            return this.vl;
        }
    }

    abstract class d extends m<DriveIdResult> {
        d() {
        }

        /* renamed from: m */
        public DriveIdResult d(Status status) {
            return new c(status, null);
        }
    }

    static class e implements MetadataBufferResult {
        private final MetadataBuffer Dy;
        private final Status vl;

        public e(Status status, MetadataBuffer metadataBuffer) {
            this.vl = status;
            this.Dy = metadataBuffer;
        }

        public MetadataBuffer getMetadataBuffer() {
            return this.Dy;
        }

        public Status getStatus() {
            return this.vl;
        }
    }

    private static class f extends c {
        private final com.google.android.gms.common.api.a.c<ContentsResult> vj;

        public f(com.google.android.gms.common.api.a.c<ContentsResult> cVar) {
            this.vj = cVar;
        }

        public void a(OnContentsResponse onContentsResponse) throws RemoteException {
            this.vj.b(new a(Status.zQ, onContentsResponse.eX()));
        }

        public void l(Status status) throws RemoteException {
            this.vj.b(new a(status, null));
        }
    }

    abstract class g extends m<ContentsResult> {
        g() {
        }

        /* renamed from: n */
        public ContentsResult d(Status status) {
            return new a(status, null);
        }
    }

    static class h extends c {
        private final com.google.android.gms.common.api.a.c<MetadataBufferResult> vj;

        public h(com.google.android.gms.common.api.a.c<MetadataBufferResult> cVar) {
            this.vj = cVar;
        }

        public void a(OnListEntriesResponse onListEntriesResponse) throws RemoteException {
            this.vj.b(new e(Status.zQ, new MetadataBuffer(onListEntriesResponse.fc(), null)));
        }

        public void l(Status status) throws RemoteException {
            this.vj.b(new e(status, null));
        }
    }

    abstract class i extends m<MetadataBufferResult> {
        i() {
        }

        /* renamed from: o */
        public MetadataBufferResult d(Status status) {
            return new e(status, null);
        }
    }

    static abstract class j extends m<Status> {
        j() {
        }

        /* renamed from: f */
        public Status d(Status status) {
            return status;
        }
    }

    static class k extends j {
        k(Status status) {
            b(status);
        }

        /* access modifiers changed from: protected */
        public void a(n nVar) {
        }
    }

    /* renamed from: com.google.android.gms.drive.internal.l$l reason: collision with other inner class name */
    abstract class C0007l extends m<Status> {
        C0007l() {
        }

        /* renamed from: f */
        public Status d(Status status) {
            return status;
        }
    }

    public PendingResult<Status> discardContents(GoogleApiClient apiClient, final Contents contents) {
        return apiClient.b(new j() {
            /* access modifiers changed from: protected */
            public void a(n nVar) throws RemoteException {
                nVar.eT().a(new CloseContentsRequest(contents, false), (v) new ak(this));
            }
        });
    }

    public PendingResult<DriveIdResult> fetchDriveId(GoogleApiClient apiClient, final String resourceId) {
        return apiClient.a(new d() {
            /* access modifiers changed from: protected */
            public void a(n nVar) throws RemoteException {
                nVar.eT().a(new GetMetadataRequest(DriveId.aq(resourceId)), (v) new b(this));
            }
        });
    }

    public DriveFolder getAppFolder(GoogleApiClient apiClient) {
        if (!apiClient.isConnected()) {
            throw new IllegalStateException("Client must be connected");
        }
        DriveId eV = ((n) apiClient.a(Drive.va)).eV();
        if (eV != null) {
            return new q(eV);
        }
        return null;
    }

    public DriveFile getFile(GoogleApiClient apiClient, DriveId id) {
        if (id == null) {
            throw new IllegalArgumentException("Id must be provided.");
        } else if (apiClient.isConnected()) {
            return new o(id);
        } else {
            throw new IllegalStateException("Client must be connected");
        }
    }

    public DriveFolder getFolder(GoogleApiClient apiClient, DriveId id) {
        if (id == null) {
            throw new IllegalArgumentException("Id must be provided.");
        } else if (apiClient.isConnected()) {
            return new q(id);
        } else {
            throw new IllegalStateException("Client must be connected");
        }
    }

    public DriveFolder getRootFolder(GoogleApiClient apiClient) {
        if (apiClient.isConnected()) {
            return new q(((n) apiClient.a(Drive.va)).eU());
        }
        throw new IllegalStateException("Client must be connected");
    }

    public PendingResult<ContentsResult> newContents(GoogleApiClient apiClient) {
        return apiClient.a(new g() {
            /* access modifiers changed from: protected */
            public void a(n nVar) throws RemoteException {
                nVar.eT().a(new CreateContentsRequest(), (v) new f(this));
            }
        });
    }

    public CreateFileActivityBuilder newCreateFileActivityBuilder() {
        return new CreateFileActivityBuilder();
    }

    public OpenFileActivityBuilder newOpenFileActivityBuilder() {
        return new OpenFileActivityBuilder();
    }

    public PendingResult<MetadataBufferResult> query(GoogleApiClient apiClient, final Query query) {
        if (query != null) {
            return apiClient.a(new i() {
                /* access modifiers changed from: protected */
                public void a(n nVar) throws RemoteException {
                    nVar.eT().a(new QueryRequest(query), (v) new h(this));
                }
            });
        }
        throw new IllegalArgumentException("Query must be provided.");
    }

    public PendingResult<Status> requestSync(GoogleApiClient client) {
        return client.b(new C0007l() {
            /* access modifiers changed from: protected */
            public void a(n nVar) throws RemoteException {
                nVar.eT().a((v) new ak(this));
            }
        });
    }
}
