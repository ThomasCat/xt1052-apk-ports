package com.google.android.gms.analytics;

import com.google.analytics.tracking.android.Fields;
import com.motorola.zap.ZapConstants;

final class o {
    private static String b(String str, int i) {
        if (i >= 1) {
            return str + i;
        }
        aa.t("index out of range for " + str + " (" + i + ")");
        return ZapConstants.GOOGLE_ACCOUNT_NONE;
    }

    static String q(int i) {
        return b("&cd", i);
    }

    static String r(int i) {
        return b(Fields.CAMPAIGN_MEDIUM, i);
    }
}
