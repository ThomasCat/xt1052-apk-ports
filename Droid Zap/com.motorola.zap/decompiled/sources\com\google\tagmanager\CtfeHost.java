package com.google.tagmanager;

class CtfeHost {
    private String mCtfeServerAddress = "https://www.googletagmanager.com";

    public void setCtfeServerAddress(String newCtfeAddress) {
        this.mCtfeServerAddress = newCtfeAddress;
        Log.i("The Ctfe server endpoint was changed to: " + newCtfeAddress);
    }

    public String getCtfeServerAddress() {
        return this.mCtfeServerAddress;
    }
}
