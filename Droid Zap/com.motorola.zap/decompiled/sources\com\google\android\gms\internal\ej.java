package com.google.android.gms.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.IBinder;
import android.os.Message;
import com.google.android.gms.common.GooglePlayServicesUtil;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public final class ej implements Callback {
    private static final Object BQ = new Object();
    private static ej BR;
    /* access modifiers changed from: private */
    public final HashMap<String, a> BS = new HashMap<>();
    private final Handler mHandler;
    private final Context qe;

    final class a {
        private final String BT;
        private final C0029a BU = new C0029a();
        /* access modifiers changed from: private */
        public final HashSet<f> BV = new HashSet<>();
        private boolean BW;
        /* access modifiers changed from: private */
        public IBinder BX;
        /* access modifiers changed from: private */
        public ComponentName BY;
        /* access modifiers changed from: private */
        public int mState = 0;

        /* renamed from: com.google.android.gms.internal.ej$a$a reason: collision with other inner class name */
        public class C0029a implements ServiceConnection {
            public C0029a() {
            }

            public void onServiceConnected(ComponentName component, IBinder binder) {
                synchronized (ej.this.BS) {
                    a.this.BX = binder;
                    a.this.BY = component;
                    Iterator it = a.this.BV.iterator();
                    while (it.hasNext()) {
                        ((f) it.next()).onServiceConnected(component, binder);
                    }
                    a.this.mState = 1;
                }
            }

            public void onServiceDisconnected(ComponentName component) {
                synchronized (ej.this.BS) {
                    a.this.BX = null;
                    a.this.BY = component;
                    Iterator it = a.this.BV.iterator();
                    while (it.hasNext()) {
                        ((f) it.next()).onServiceDisconnected(component);
                    }
                    a.this.mState = 2;
                }
            }
        }

        public a(String str) {
            this.BT = str;
        }

        public void a(f fVar) {
            this.BV.add(fVar);
        }

        public void b(f fVar) {
            this.BV.remove(fVar);
        }

        public boolean c(f fVar) {
            return this.BV.contains(fVar);
        }

        public C0029a ee() {
            return this.BU;
        }

        public String ef() {
            return this.BT;
        }

        public boolean eg() {
            return this.BV.isEmpty();
        }

        public IBinder getBinder() {
            return this.BX;
        }

        public ComponentName getComponentName() {
            return this.BY;
        }

        public int getState() {
            return this.mState;
        }

        public boolean isBound() {
            return this.BW;
        }

        public void w(boolean z) {
            this.BW = z;
        }
    }

    private ej(Context context) {
        this.mHandler = new Handler(context.getMainLooper(), this);
        this.qe = context.getApplicationContext();
    }

    public static ej y(Context context) {
        synchronized (BQ) {
            if (BR == null) {
                BR = new ej(context.getApplicationContext());
            }
        }
        return BR;
    }

    public boolean a(String str, f fVar) {
        boolean isBound;
        synchronized (this.BS) {
            a aVar = (a) this.BS.get(str);
            if (aVar != null) {
                this.mHandler.removeMessages(0, aVar);
                if (!aVar.c(fVar)) {
                    aVar.a(fVar);
                    switch (aVar.getState()) {
                        case 1:
                            fVar.onServiceConnected(aVar.getComponentName(), aVar.getBinder());
                            break;
                        case 2:
                            aVar.w(this.qe.bindService(new Intent(str).setPackage(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE), aVar.ee(), 129));
                            break;
                    }
                } else {
                    throw new IllegalStateException("Trying to bind a GmsServiceConnection that was already connected before.  startServiceAction=" + str);
                }
            } else {
                aVar = new a(str);
                aVar.a(fVar);
                aVar.w(this.qe.bindService(new Intent(str).setPackage(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE), aVar.ee(), 129));
                this.BS.put(str, aVar);
            }
            isBound = aVar.isBound();
        }
        return isBound;
    }

    public void b(String str, f fVar) {
        synchronized (this.BS) {
            a aVar = (a) this.BS.get(str);
            if (aVar == null) {
                throw new IllegalStateException("Nonexistent connection status for service action: " + str);
            } else if (!aVar.c(fVar)) {
                throw new IllegalStateException("Trying to unbind a GmsServiceConnection  that was not bound before.  startServiceAction=" + str);
            } else {
                aVar.b(fVar);
                if (aVar.eg()) {
                    this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(0, aVar), 5000);
                }
            }
        }
    }

    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case 0:
                a aVar = (a) msg.obj;
                synchronized (this.BS) {
                    if (aVar.eg()) {
                        this.qe.unbindService(aVar.ee());
                        this.BS.remove(aVar.ef());
                    }
                }
                return true;
            default:
                return false;
        }
    }
}
