package com.google.sample.castcompanionlibrary.utils;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.images.WebImage;
import com.motorola.zap.R;
import com.motorola.zap.ZapConstants;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public class Utils {
    private static final String TAG = LogUtils.makeLogTag(Utils.class);

    public static String formatMillis(int millis) {
        String result = ZapConstants.GOOGLE_ACCOUNT_NONE;
        int hr = millis / 3600000;
        int millis2 = millis % 3600000;
        int min = millis2 / 60000;
        int sec = (millis2 % 60000) / 1000;
        if (hr > 0) {
            result = result + hr + ":";
        }
        if (min >= 0) {
            if (min > 9) {
                result = result + min + ":";
            } else {
                result = result + "0" + min + ":";
            }
        }
        if (sec > 9) {
            return result + sec;
        }
        return result + "0" + sec;
    }

    public static final void showErrorDialog(Context context, int resourceId) {
        showErrorDialog(context, context.getString(resourceId));
    }

    public static final void showErrorDialog(Context context, String message) {
        new Builder(context).setTitle(R.string.error).setMessage(message).setPositiveButton(R.string.ok, new OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        }).create().show();
    }

    public static String getImageUrl(MediaInfo info, int level) {
        MediaMetadata mm = info.getMetadata();
        if (mm == null || mm.getImages().size() <= level) {
            return null;
        }
        return ((WebImage) mm.getImages().get(level)).getUrl().toString();
    }

    public static void saveStringToPreference(Context context, String key, String value) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        if (value == null) {
            pref.edit().remove(key).apply();
        } else {
            pref.edit().putString(key, value).apply();
        }
    }

    public static void saveFloatToPreference(Context context, String key, float value) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        if (Float.MIN_VALUE == value) {
            pref.edit().remove(key).apply();
        } else {
            pref.edit().putFloat(key, value).apply();
        }
    }

    public static String getStringFromPreference(Context context, String key) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(key, null);
    }

    public static float getFloatFromPreference(Context context, String key) {
        return PreferenceManager.getDefaultSharedPreferences(context).getFloat(key, Float.MIN_VALUE);
    }

    public static boolean getBooleanFromPreference(Context context, String key, boolean defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, defaultValue);
    }

    public static boolean checkGooglePlayServices(final Activity activity) {
        int googlePlayServicesCheck = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
        switch (googlePlayServicesCheck) {
            case 0:
                return true;
            default:
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(googlePlayServicesCheck, activity, 0);
                dialog.setOnCancelListener(new OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                        activity.finish();
                    }
                });
                dialog.show();
                return false;
        }
    }

    public static boolean checkGooglePlaySevices(Activity activity) {
        return checkGooglePlayServices(activity);
    }

    public static Bundle fromMediaInfo(MediaInfo info) {
        if (info == null) {
            return null;
        }
        MediaMetadata md = info.getMetadata();
        Bundle wrapper = new Bundle();
        wrapper.putString(MediaMetadata.KEY_TITLE, md.getString(MediaMetadata.KEY_TITLE));
        wrapper.putString(MediaMetadata.KEY_SUBTITLE, md.getString(MediaMetadata.KEY_SUBTITLE));
        wrapper.putString("movie-urls", info.getContentId());
        wrapper.putString(MediaMetadata.KEY_STUDIO, md.getString(MediaMetadata.KEY_STUDIO));
        wrapper.putString("content-type", info.getContentType());
        wrapper.putInt("stream-type", info.getStreamType());
        if (!md.getImages().isEmpty()) {
            ArrayList<String> urls = new ArrayList<>();
            for (WebImage img : md.getImages()) {
                urls.add(img.getUrl().toString());
            }
            wrapper.putStringArrayList("images", urls);
        }
        JSONObject customData = info.getCustomData();
        if (customData == null) {
            return wrapper;
        }
        wrapper.putString("custom-data", customData.toString());
        return wrapper;
    }

    public static MediaInfo toMediaInfo(Bundle wrapper) {
        if (wrapper == null) {
            return null;
        }
        MediaMetadata metaData = new MediaMetadata(1);
        metaData.putString(MediaMetadata.KEY_SUBTITLE, wrapper.getString(MediaMetadata.KEY_SUBTITLE));
        metaData.putString(MediaMetadata.KEY_TITLE, wrapper.getString(MediaMetadata.KEY_TITLE));
        metaData.putString(MediaMetadata.KEY_STUDIO, wrapper.getString(MediaMetadata.KEY_STUDIO));
        ArrayList<String> images = wrapper.getStringArrayList("images");
        if (images != null && !images.isEmpty()) {
            Iterator i$ = images.iterator();
            while (i$.hasNext()) {
                metaData.addImage(new WebImage(Uri.parse((String) i$.next())));
            }
        }
        String customDataStr = wrapper.getString("custom-data");
        JSONObject customData = null;
        if (!TextUtils.isEmpty(customDataStr)) {
            try {
                customData = new JSONObject(customDataStr);
            } catch (JSONException e) {
                LogUtils.LOGE(TAG, "Failed to deserialize the custom data string: custom data= " + customDataStr);
            }
        }
        return new MediaInfo.Builder(wrapper.getString("movie-urls")).setStreamType(wrapper.getInt("stream-type")).setContentType(wrapper.getString("content-type")).setMetadata(metaData).setCustomData(customData).build();
    }
}
