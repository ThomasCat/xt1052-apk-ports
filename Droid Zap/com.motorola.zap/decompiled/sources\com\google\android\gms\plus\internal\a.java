package com.google.android.gms.plus.internal;

import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.internal.fh;
import com.google.android.gms.internal.ir;

public abstract class a extends com.google.android.gms.plus.internal.b.a {
    public void L(Status status) {
    }

    public void a(int i, Bundle bundle, Bundle bundle2) throws RemoteException {
    }

    public void a(int i, Bundle bundle, ParcelFileDescriptor parcelFileDescriptor) throws RemoteException {
    }

    public final void a(int i, Bundle bundle, fh fhVar) {
    }

    public void a(int i, ir irVar) {
    }

    public void a(DataHolder dataHolder, String str) {
    }

    public void a(DataHolder dataHolder, String str, String str2) {
    }

    public void aP(String str) throws RemoteException {
    }

    public void aQ(String str) {
    }

    public void d(int i, Bundle bundle) {
    }
}
