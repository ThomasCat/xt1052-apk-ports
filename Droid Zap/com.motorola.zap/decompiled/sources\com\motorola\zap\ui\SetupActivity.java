package com.motorola.zap.ui;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.motorola.sharing.GCSP;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConfig;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.identity.ZapProfileSharedPerfs;
import com.motorola.zap.location.BlastLocationProvider;

public class SetupActivity extends Activity {
    /* access modifiers changed from: private */
    public static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    public static final int RESULT_CANCELED = 4;
    /* access modifiers changed from: private */
    public Dialog mGpsDialog;
    private boolean mIsAccountSetup;
    private boolean mIsActivityStarted;
    private boolean mIsLocationSetup;
    private BlastLocationProvider mLoc;
    /* access modifiers changed from: private */
    public Dialog mLocationDialog;
    private long mOnCreateTime;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mOnCreateTime = System.currentTimeMillis();
        this.mLoc = BlastLocationProvider.getInstance();
        setContentView(R.layout.setup);
        ((Button) findViewById(R.id.setup_account_picker)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                SetupActivity.this.startAccountPickerActivity();
            }
        });
        findViewById(R.id.setup_ok).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ZapConfig.savePrivacyAgreed(true);
                GCSP.getCheckinManager().logZapFeatureOptIn(true);
                SetupActivity.this.handleSetup();
            }
        });
        if (bundle != null && bundle.getBoolean("gps")) {
            showGpsDialog();
        }
        if (bundle != null && bundle.getBoolean("loc")) {
            showLocationDialog();
        }
        if (!((Boolean) ZapProfileSharedPerfs.ZAP_PROFILE_ACTIVATE.read()).booleanValue()) {
            handleWelcomeScreenSqueuence();
        }
    }

    private void handleWelcomeScreenSqueuence() {
        Intent intent = new Intent(this, WelcomeActivity.class);
        intent.setFlags(67108864);
        synchronized (this) {
            startActivityForResult(intent, 3);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Button picker = (Button) findViewById(R.id.setup_account_picker);
        String account = GCSP.getAuthenticator().getAccount();
        if (account == null) {
            picker.setText(R.string.setup_picker_empty_text);
        } else {
            picker.setText(account);
        }
        if (this.mIsActivityStarted) {
            this.mIsActivityStarted = false;
            if (isStateChanged()) {
                handleSetup();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (this.mLocationDialog != null) {
            outState.putBoolean("loc", true);
        }
        if (this.mGpsDialog != null) {
            outState.putBoolean("gps", true);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.mLocationDialog != null) {
            this.mLocationDialog.dismiss();
            this.mLocationDialog = null;
        }
        if (this.mGpsDialog != null) {
            this.mGpsDialog.dismiss();
            this.mGpsDialog = null;
        }
        super.onStop();
    }

    /* access modifiers changed from: private */
    public void startAccountPickerActivity() {
        String action;
        if (ZapConfig.supportsFullFeature()) {
            if (GCSP.getAuthenticator().getAccount() == null) {
                action = "com.motorola.ccc.sso.action.ADD_ACCOUNT";
            } else {
                action = "com.motorola.ccc.sso.action.SETTINGS";
            }
            startActivity(new Intent(action));
            return;
        }
        String accountStr = GCSP.getAuthenticator().getAccount();
        Account account = null;
        if (accountStr != null) {
            account = new Account(accountStr, GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
        }
        startActivityForResult(AccountManager.newChooseAccountIntent(account, null, new String[]{GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE}, true, null, null, null, null), 0);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (DBG) {
            Log.d("Zap-SetupActivity", "onActivityResult: " + requestCode);
        }
        if (resultCode == -1) {
            String userAccount = data.getStringExtra("authAccount");
            if (userAccount != null) {
                if (DBG) {
                    Log.d("Zap-SetupActivity", "Saving account");
                }
                Z.GOOGLE_ACCOUNT.write(userAccount);
            }
        }
        if (requestCode == 3 && resultCode == 0) {
            setResult(4);
            finish();
        }
    }

    /* access modifiers changed from: private */
    public void handleSetup() {
        if (!isAccountSetup()) {
            this.mIsActivityStarted = true;
            saveState();
            startAccountPickerActivity();
        } else if (!isLocationSetup()) {
            this.mIsActivityStarted = true;
            saveState();
            showLocationDialog();
        } else {
            continueSwipe();
        }
    }

    private void continueSwipe() {
        Z.USER_SIGNEDOUT.write(Boolean.valueOf(false));
        setResult(-1);
        finish();
        if (getIntent().getExtras() != null) {
            Intent originalIntent = (Intent) getIntent().getExtras().get(ZapConstants.EXTRA_ORIGINAL_INTENT);
            if (isExpiredReceive(originalIntent)) {
                ZapUi.getInstance().showToastMessage(getString(R.string.toast_msg_no_content_found), ZapUi.getReceiveToastIconId(), true, true);
                return;
            }
            Intent i = originalIntent.cloneFilter();
            i.putExtras(originalIntent);
            sendBroadcast(i);
        }
    }

    private boolean isExpiredReceive(Intent intent) {
        String action = intent.getAction();
        return action != null && action.equals(ZapConstants.ACTION_GS_SWIPE_DOWN) && System.currentTimeMillis() - this.mOnCreateTime > 120000;
    }

    private boolean isAccountSetup() {
        return GCSP.getAuthenticator().getAccount() != null;
    }

    private boolean isLocationSetup() {
        return this.mLoc.isGpsLocationEnabled() && this.mLoc.isNetworkLocationEnabled();
    }

    private void saveState() {
        this.mIsAccountSetup = isAccountSetup();
        this.mIsLocationSetup = isLocationSetup();
    }

    private void showLocationDialog() {
        Builder alert = new Builder(this);
        alert.setTitle(R.string.setup_enable_location_title);
        alert.setMessage(R.string.setup_enable_location_message);
        alert.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (ZapConfig.supportsFullFeature()) {
                    SetupActivity.this.showGpsDialog();
                    return;
                }
                SetupActivity.this.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
            }
        });
        alert.setNegativeButton(17039360, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        this.mLocationDialog = alert.show();
        this.mLocationDialog.setOnDismissListener(new OnDismissListener() {
            public void onDismiss(DialogInterface arg0) {
                if (SetupActivity.DBG) {
                    Log.d("Zap-SetupActivity", "location dialog onDismiss");
                }
                SetupActivity.this.mLocationDialog = null;
            }
        });
    }

    /* access modifiers changed from: private */
    public void showGpsDialog() {
        if (this.mLoc.isGpsLocationEnabled()) {
            setupNetworkLocation();
            return;
        }
        Builder alert = new Builder(this);
        alert.setIcon(17301659);
        alert.setTitle(R.string.setup_enable_gps_location_title);
        alert.setMessage(R.string.setup_enable_gps_location_message);
        alert.setPositiveButton(R.string.setup_agree, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Secure.setLocationProviderEnabled(SetupActivity.this.getContentResolver(), "gps", true);
                SetupActivity.this.setupNetworkLocation();
            }
        });
        alert.setNegativeButton(R.string.setup_disagree, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        this.mGpsDialog = alert.show();
        this.mGpsDialog.setOnDismissListener(new OnDismissListener() {
            public void onDismiss(DialogInterface arg0) {
                if (SetupActivity.DBG) {
                    Log.d("Zap-SetupActivity", "gps dialog onDismiss");
                }
                SetupActivity.this.mGpsDialog = null;
            }
        });
    }

    /* access modifiers changed from: private */
    public void setupNetworkLocation() {
        if (this.mLoc.isNetworkLocationEnabled()) {
            continueSwipe();
        } else {
            Secure.setLocationProviderEnabled(getContentResolver(), "network", true);
        }
    }

    private boolean isStateChanged() {
        if (this.mIsAccountSetup == isAccountSetup() && this.mIsLocationSetup == isLocationSetup()) {
            return false;
        }
        return true;
    }
}
