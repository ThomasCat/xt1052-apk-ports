package com.motorola.sharing.util;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.motorola.sharing.Settings;
import com.motorola.zap.ZapConstants;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Logger {
    private static final Set<String> sObfuscateList = new HashSet(Arrays.asList(new String[]{"from", "fromEmail", "fromDeviceId", "imei", "serial", "owner", "targetOwner", "googleId", "email"}));

    public enum LogLevel {
        DISABLE(0),
        ERROR(1),
        WARNING(2),
        INFO(3),
        DEBUG(4),
        VERBOSE(5);
        
        protected int value;

        private LogLevel(int value2) {
            this.value = value2;
        }
    }

    public static String getGcspTag(String name) {
        return "GCSP-" + name;
    }

    public static String getCloudTag(String name) {
        return "Cloud-" + name;
    }

    public static String getP2pTag(String name) {
        return "P2P-" + name;
    }

    public static String getTvTag(String name) {
        return "TV-" + name;
    }

    public static void v(String tag, String string) {
        if (isLoggable(LogLevel.VERBOSE)) {
            Log.v(tag, string);
        }
    }

    public static void i(String tag, String string) {
        if (isLoggable(LogLevel.INFO) && Log.isLoggable(tag, 4)) {
            Log.i(tag, string);
        }
    }

    public static void d(String tag, String string) {
        if (isLoggable(LogLevel.DEBUG)) {
            Log.d(tag, string);
        }
    }

    public static void w(String tag, String string) {
        if (isLoggable(LogLevel.WARNING) && Log.isLoggable(tag, 5)) {
            Log.w(tag, string);
        }
    }

    public static void e(String tag, String string) {
        if (isLoggable(LogLevel.ERROR) && Log.isLoggable(tag, 6)) {
            Log.e(tag, string);
        }
    }

    public static void e(String tag, String string, Exception e) {
        if (isLoggable(LogLevel.ERROR) && Log.isLoggable(tag, 6)) {
            Log.e(tag, string, e);
        }
    }

    public static boolean isLoggable(LogLevel logLevel) {
        return ((LogLevel) Settings.LOG_LEVEL.get()).value >= logLevel.value;
    }

    public static boolean isDebuggable() {
        return isLoggable(LogLevel.DEBUG);
    }

    public static String obfuscate(String string) {
        int length = TextUtils.isEmpty(string) ? 0 : string.length();
        if (length == 0) {
            return string;
        }
        if (length < 5) {
            return "*****";
        }
        return string.charAt(0) + "*****" + string.charAt(length - 1);
    }

    public static void dumpJson(String tag, String json) {
        if (isDebuggable() && !TextUtils.isEmpty(json)) {
            try {
                Object object = new JSONTokener(json).nextValue();
                if (object == null) {
                    return;
                }
                if (object instanceof JSONObject) {
                    dump(tag, (JSONObject) object);
                } else if (object instanceof JSONArray) {
                    dump(tag, (JSONArray) object);
                }
            } catch (Exception e) {
            }
        }
    }

    public static void dump(String tag, JSONObject object) {
        if (isDebuggable()) {
            if (TextUtils.isEmpty(tag)) {
                tag = "GCSP-Logger";
            }
            d(tag, "======================");
            dump(tag, object, 0);
            d(tag, "======================");
        }
    }

    public static void dump(String tag, JSONArray array) {
        if (isDebuggable()) {
            if (TextUtils.isEmpty(tag)) {
                tag = "GCSP-Logger";
            }
            d(tag, "======================");
            dump(tag, array, 0);
            d(tag, "======================");
        }
    }

    private static void dump(String tag, JSONArray array, int level) {
        if (array != null) {
            int i = 0;
            while (i < array.length()) {
                try {
                    dump(tag, array.getJSONObject(i), level);
                    if (i < array.length() - 1) {
                        d(tag, "----------------------");
                    }
                    i++;
                } catch (Exception e) {
                    d("GCSP-Logger", "Failed to parse: " + e);
                    return;
                }
            }
        }
    }

    private static void dump(String tag, JSONObject object, int level) {
        if (object != null) {
            try {
                Iterator<String> it = object.keys();
                while (it.hasNext()) {
                    StringBuffer line = new StringBuffer();
                    for (int i = 0; i < level; i++) {
                        line.append("  ");
                    }
                    String key = (String) it.next();
                    Object o = object.get(key);
                    if (o != null) {
                        if (o instanceof JSONObject) {
                            line.append("- " + key + ":");
                            d(tag, line.toString());
                            dump(tag, (JSONObject) o, level + 1);
                        } else if (o instanceof JSONArray) {
                            line.append("- " + key + ":");
                            d(tag, line.toString());
                            dump(tag, (JSONArray) o, level + 1);
                        }
                    }
                    String value = o != null ? o.toString() : ZapConstants.GOOGLE_ACCOUNT_NONE;
                    if (value.length() > 60) {
                        value = value.substring(0, 60) + " ...";
                    }
                    if (sObfuscateList.contains(key)) {
                        value = obfuscate(value);
                    }
                    line.append("- " + key + ": " + value);
                    d(tag, line.toString());
                }
            } catch (Exception e) {
                d("GCSP-Logger", "Failed to parse: " + e);
            }
        }
    }

    public static void dump(String tag, Intent intent) {
        if (isDebuggable()) {
            if (TextUtils.isEmpty(tag)) {
                tag = "GCSP-Logger";
            }
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Set<String> keys = bundle.keySet();
                d(tag, ZapConstants.GOOGLE_ACCOUNT_NONE + intent);
                if (keys.size() > 0) {
                    d(tag, "======================");
                }
                for (String key : keys) {
                    Log.e(tag, key + ": " + bundle.get(key));
                }
                if (keys.size() > 0) {
                    d(tag, "======================");
                }
            }
        }
    }
}
