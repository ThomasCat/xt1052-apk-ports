package com.motorola.sharing.provider.cloud;

import android.text.TextUtils;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentContainer.Type;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.ContentShareListener.ContentShareEvent.Substatus;
import com.motorola.sharing.ContentShareListener.ContentStopShareEvent;
import com.motorola.sharing.provider.cloud.ContentShare.Callback;
import com.motorola.sharing.util.Logger;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

class ContentShareManager {
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getCloudTag("ContentShareMgr");
    private static ContentShareManager sInstance;
    private Callback mContentShareCallback = new Callback() {
        /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
            return;
         */
        public void onComplete(ContentContainer container, Status status, Substatus substatus) {
            synchronized (ContentShareManager.this) {
                CloudData containerExtras = CloudData.getExtras(container);
                if (!containerExtras.isOperationCanceled) {
                    container.onContentShareEvent(status, substatus, CloudProvider.getInstance().getEndpoint());
                    ContentShareManager.this.removeSharingInProgress(container);
                } else if (containerExtras.isPendingStopShare) {
                    Logger.d(ContentShareManager.TAG, "resume stop sharing: " + container.getPrintableSignature());
                    containerExtras.isPendingStopShare = false;
                    if (!TextUtils.isEmpty(container.shareId)) {
                        ContentShareManager.this.stopSharing(container);
                    } else {
                        container.onContentShareEvent(Status.SUCCESS, Substatus.INTERRUPTED, CloudProvider.getInstance().getEndpoint());
                        container.onContentStopShareEvent(Status.SUCCESS, ContentStopShareEvent.Substatus.NOT_SHARED, CloudProvider.getInstance().getEndpoint());
                        ContentShareManager.this.removeSharingInProgress(container);
                    }
                }
            }
        }
    };
    private ContentStopShare.Callback mContentStopShareCallback = new ContentStopShare.Callback() {
        public void onComplete(ContentContainer container, Status status, ContentStopShareEvent.Substatus substatus) {
            synchronized (ContentShareManager.this) {
                CloudData containerExtras = CloudData.getExtras(container);
                if (containerExtras.originalContainer != null) {
                    containerExtras.originalContainer.onContentStopShareEvent(status, substatus, CloudProvider.getInstance().getEndpoint());
                } else {
                    container.onContentStopShareEvent(status, substatus, CloudProvider.getInstance().getEndpoint());
                }
            }
        }
    };
    private ContentUpload.Callback mContentUploadCallback = new ContentUpload.Callback() {
        public void onProgress(ContentContainer container, Substatus substatus) {
            synchronized (ContentShareManager.this) {
                if (!CloudData.isOperationCanceled(container)) {
                    container.onContentShareEvent(Status.INFO, substatus, CloudProvider.getInstance().getEndpoint());
                }
            }
        }

        public void onComplete(ContentContainer container, Status status, Substatus substatus) {
            synchronized (ContentShareManager.this) {
                if (!CloudData.isOperationCanceled(container)) {
                    container.onContentShareEvent(status, substatus, CloudProvider.getInstance().getEndpoint());
                    ContentShareManager.this.removeSharingInProgress(container);
                }
            }
        }
    };
    private final Map<Long, ContentContainer> mSharingInProgress = new Hashtable();

    ContentShareManager() {
    }

    static synchronized ContentShareManager getInstance() {
        ContentShareManager contentShareManager;
        synchronized (ContentShareManager.class) {
            if (sInstance == null) {
                sInstance = new ContentShareManager();
            }
            contentShareManager = sInstance;
        }
        return contentShareManager;
    }

    public synchronized void startSharing(ContentContainer container) {
        if (isSharingInProgress(container)) {
            throw new IllegalStateException("sharing of this content is already in progress");
        }
        addSharingInProgress(container);
        new ContentShare(container).start(this.mContentShareCallback);
    }

    public synchronized void uploadContent(ContentContainer container) {
        if (isSharingInProgress(container)) {
            throw new IllegalStateException("sharing of this content is already in progress");
        }
        addSharingInProgress(container);
        new ContentUpload(container).start(this.mContentUploadCallback);
    }

    public synchronized void stopSharing(ContentContainer container) {
        ContentContainer containerInProgress;
        ContentContainer stopShareContainer;
        if (container.type == Type.SHARE) {
            containerInProgress = getSharingInProgress(Long.valueOf(container.uid));
        } else if (container.type == Type.CANCEL_SHARE) {
            containerInProgress = getSharingInProgress(container.shareId);
        } else {
            throw new IllegalArgumentException("wrong container type: " + container.type);
        }
        if (containerInProgress != null) {
            CloudData extras = CloudData.getExtras(containerInProgress);
            extras.isOperationCanceled = true;
            if (containerInProgress.equals(container)) {
                extras.isPendingStopShare = TextUtils.isEmpty(containerInProgress.shareId);
                if (extras.isPendingStopShare) {
                    Logger.d(TAG, "stop sharing is pending: " + containerInProgress.getPrintableSignature());
                }
            }
            removeSharingInProgress(containerInProgress);
            containerInProgress.onContentShareEvent(Status.SUCCESS, Substatus.INTERRUPTED, CloudProvider.getInstance().getEndpoint());
        }
        if (TextUtils.isEmpty(container.shareId)) {
            container.onContentStopShareEvent(Status.SUCCESS, ContentStopShareEvent.Substatus.NOT_SHARED, CloudProvider.getInstance().getEndpoint());
        } else {
            if (container.type == Type.CANCEL_SHARE) {
                stopShareContainer = container;
            } else {
                stopShareContainer = buildStopShareContainer(container);
            }
            new ContentStopShare(stopShareContainer).start(this.mContentStopShareCallback);
        }
    }

    private ContentContainer buildStopShareContainer(ContentContainer originalContainer) {
        ContentContainer container = ContentContainer.generateCancelShareContainer(originalContainer.shareId);
        container.setContentShareListener(originalContainer.getContentShareListener());
        CloudData.getExtras(container).originalContainer = originalContainer;
        return container;
    }

    private synchronized ContentContainer getSharingInProgress(String shareId) {
        ContentContainer container;
        if (!TextUtils.isEmpty(shareId)) {
            Iterator i$ = this.mSharingInProgress.keySet().iterator();
            while (true) {
                if (!i$.hasNext()) {
                    container = null;
                    break;
                }
                container = (ContentContainer) this.mSharingInProgress.get((Long) i$.next());
                if (TextUtils.equals(container.shareId, shareId)) {
                    break;
                }
            }
        } else {
            container = null;
        }
        return container;
    }

    private synchronized ContentContainer getSharingInProgress(Long uid) {
        return (ContentContainer) this.mSharingInProgress.get(uid);
    }

    private synchronized boolean isSharingInProgress(ContentContainer container) {
        return this.mSharingInProgress.containsKey(Long.valueOf(container.uid));
    }

    private synchronized void addSharingInProgress(ContentContainer container) {
        if (!this.mSharingInProgress.containsKey(Long.valueOf(container.uid))) {
            Logger.d(TAG, "add sharing in progress: " + container.getPrintableSignature());
            this.mSharingInProgress.put(Long.valueOf(container.uid), container);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void removeSharingInProgress(ContentContainer container) {
        if (this.mSharingInProgress.containsKey(Long.valueOf(container.uid))) {
            Logger.d(TAG, "remove sharing in progress: " + container.getPrintableSignature());
            this.mSharingInProgress.remove(Long.valueOf(container.uid));
        }
    }
}
