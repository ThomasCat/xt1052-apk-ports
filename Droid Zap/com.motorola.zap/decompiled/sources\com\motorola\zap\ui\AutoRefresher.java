package com.motorola.zap.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.motorola.sharing.GCSP;
import com.motorola.zap.LogUtils;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.app.ActionReceiver;
import com.motorola.zap.app.App;
import com.motorola.zap.app.ZapTracker;

public class AutoRefresher {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    private static final long[] REFRSH_TIMEOUT = {3000, 30000, 60000, 120000, ZapConstants.LOCATION_MAX_AGE_NO_MODALITY_SUPPORT};
    private static int currentRefreshTimeoutIndex = 0;
    private static AutoRefresher sInstance = null;
    private Context mContext = null;
    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    AutoRefresher.this.queueNextRefresh();
                    return;
                default:
                    return;
            }
        }
    };
    private boolean refreshCancelled = false;

    public static synchronized AutoRefresher getInstance() {
        AutoRefresher autoRefresher;
        synchronized (AutoRefresher.class) {
            if (sInstance == null) {
                sInstance = new AutoRefresher(App.getAppContext());
            }
            autoRefresher = sInstance;
        }
        return autoRefresher;
    }

    private AutoRefresher(Context context) {
        this.mContext = context;
    }

    /* access modifiers changed from: private */
    public void queueNextRefresh() {
        if (this.refreshCancelled || !ZapUtils.isZapLauncherOnTop() || GCSP.getGroupManager().isAnyGroupActive()) {
            stop();
            return;
        }
        Intent intent = new Intent(ZapConstants.ACTION_AUTO_POLL);
        intent.setClass(App.getAppContext(), ActionReceiver.class);
        App.getAppContext().sendBroadcast(intent);
        ZapTracker.getInstance().onRefresh();
        currentRefreshTimeoutIndex++;
        this.mHandler.sendEmptyMessageDelayed(0, getRefreshInterval());
    }

    private long getRefreshInterval() {
        if (currentRefreshTimeoutIndex >= REFRSH_TIMEOUT.length) {
            currentRefreshTimeoutIndex = 1;
        }
        return REFRSH_TIMEOUT[currentRefreshTimeoutIndex];
    }

    public synchronized boolean isAutoRefreshSettingEnabled() {
        return ((Boolean) Z.AUTO_REFRESH_ENABLED.read()).booleanValue();
    }

    public synchronized void setAutoRefreshSettingEnabled(boolean enabled) {
        Z.AUTO_REFRESH_ENABLED.write(Boolean.valueOf(enabled));
        start();
    }

    public synchronized void start() {
        if (isAutoRefreshSettingEnabled() && !this.mHandler.hasMessages(0) && !ZapUtils.hasUserSignedOut()) {
            currentRefreshTimeoutIndex = 0;
            this.refreshCancelled = false;
            this.mHandler.sendEmptyMessageDelayed(0, getRefreshInterval());
        }
    }

    public synchronized void stop() {
        this.refreshCancelled = true;
        this.mHandler.removeMessages(0);
    }
}
