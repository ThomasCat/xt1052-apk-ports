package com.google.sample.castcompanionlibrary.cast.callbacks;

import android.support.v7.media.MediaRouter.RouteInfo;
import com.google.android.gms.common.ConnectionResult;

public class BaseCastConsumerImpl implements IBaseCastConsumer {
    public void onConnected() {
    }

    public void onDisconnected() {
    }

    public boolean onConnectionFailed(ConnectionResult result) {
        return true;
    }

    public void onCastDeviceDetected(RouteInfo info) {
    }

    public void onCastAvailabilityChanged(boolean castPresent) {
    }

    public void onConnectionSuspended(int cause) {
    }

    public void onConnectivityRecovered() {
    }

    public void onFailed(int resourceId, int statusCode) {
    }
}
