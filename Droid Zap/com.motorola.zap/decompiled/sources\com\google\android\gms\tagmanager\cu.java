package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.d;
import java.util.Map;

class cu extends aj {
    private static final String ID = a.RUNTIME_VERSION.toString();

    public cu() {
        super(ID, new String[0]);
    }

    public boolean iy() {
        return true;
    }

    public d.a u(Map<String, d.a> map) {
        return di.r(Long.valueOf(59512756));
    }
}
