package com.motorola.sharing;

import android.text.TextUtils;
import com.motorola.sharing.provider.cloud.CloudSettings;
import com.motorola.sharing.util.Logger;
import com.motorola.sharing.util.Logger.LogLevel;
import com.motorola.sharing.util.SharedPreference;
import java.util.Map;

public class Config {
    public static final String CHROMECAST_DOWNLOADS_DIRECTORY = "g_chromecastdownloadsDirectory";
    public static final String DISCOVER_SELF_SHARED_CONTENT = "cloud_discoverSelfSharedContent";
    public static final String DOWNLOADS_DIRECTORY = "g_downloadsDirectory";
    public static final String GOOGLE_ANALYTICS_ENABLED = "g_googleAnalyticsEnabled";
    public static final String GOOGLE_ANALYTICS_ID = "g_googleAnalyticsId";
    public static final String GROUP_INVITE_TTL = "g_groupInviteTtl";
    public static final String JPEG_REDUCTION_MAX_SIDE_SIZE = "g_jpegReductionMaxSideSize";
    public static final String JPEG_REDUCTION_SIZE_FACTOR = "g_jpegReductionSizeFactor";
    public static final String LOG_LEVEL = "g_logLevel";
    public static final String MOTO_CHECKIN_ENABLED = "g_motoCheckinEnabled";
    public static final String MOTO_CHECKIN_ID = "g_motoCheckinId";
    public static final String SERVER_HOST = "cloud_serverHost";
    private static final String TAG = Logger.getGcspTag("Config");
    public static final String WIFI_ONLY_CONTENT_TRANSFER = "cloud_wifiOnlyContentTransfer";
    private static Config sInstance = null;

    public static synchronized Config getInstance() {
        Config config;
        synchronized (Config.class) {
            if (sInstance == null) {
                sInstance = new Config();
            }
            config = sInstance;
        }
        return config;
    }

    private Config() {
        loadSettings();
    }

    private void loadSettings() {
        ClassLoader classLoader = GCSP.getContext().getClassLoader();
        try {
            Class.forName(Settings.class.getName(), true, classLoader);
            Class.forName(CloudSettings.class.getName(), true, classLoader);
        } catch (ClassNotFoundException e) {
            Logger.e(TAG, "got exception: " + e);
        }
    }

    public Integer getInt(String str) {
        return (Integer) get(str, Integer.class);
    }

    public Long getLong(String str) {
        return (Long) get(str, Long.class);
    }

    public Float getFloat(String str) {
        return (Float) get(str, Float.class);
    }

    public Boolean getBoolean(String str) {
        return (Boolean) get(str, Boolean.class);
    }

    public String getString(String str) {
        return (String) get(str, String.class);
    }

    public LogLevel getLogLevel() {
        return (LogLevel) get(LOG_LEVEL, LogLevel.class);
    }

    public Config setInt(String name, Integer value) {
        set(name, value);
        return this;
    }

    public Config setLong(String name, Long value) {
        set(name, value);
        return this;
    }

    public Config setFloat(String name, Float value) {
        set(name, value);
        return this;
    }

    public Config setBoolean(String name, Boolean value) {
        set(name, value);
        return this;
    }

    public Config setString(String name, String value) {
        set(name, value);
        return this;
    }

    public Config setLogLevel(LogLevel value) {
        set(LOG_LEVEL, value);
        return this;
    }

    public Config set(Map<String, Object> values) {
        if (values == null) {
            throw new IllegalArgumentException("values must not be null");
        }
        for (String name : values.keySet()) {
            set(name, values.get(name));
        }
        return this;
    }

    private Object get(String name, Class type) {
        if (TextUtils.isEmpty(name)) {
            throw new IllegalArgumentException("name must not be empty");
        }
        SharedPreference s = SharedPreference.forName(name);
        if (s == null) {
            throw new IllegalArgumentException("'" + name + "' is not supported");
        } else if (type == s.getType()) {
            return s.get();
        } else {
            throw new IllegalArgumentException("value of '" + name + "' is instance of " + s.getType().getSimpleName());
        }
    }

    private void set(String name, Object value) {
        if (TextUtils.isEmpty(name)) {
            throw new IllegalArgumentException("name must not be empty");
        }
        SharedPreference s = SharedPreference.forName(name);
        if (s == null) {
            throw new IllegalArgumentException("'" + name + "' is not supported");
        } else if (value != null && value.getClass() != s.getType()) {
            throw new IllegalArgumentException("value of '" + name + "' must be instance of " + s.getType().getSimpleName());
        } else if (value == null) {
            s.set(null);
        } else if (value instanceof Integer) {
            s.set((Integer) value);
        } else if (value instanceof Long) {
            s.set((Long) value);
        } else if (value instanceof Float) {
            s.set((Float) value);
        } else if (value instanceof Boolean) {
            s.set((Boolean) value);
        } else if (value instanceof String) {
            s.set((String) value);
        } else if (value instanceof LogLevel) {
            s.set((LogLevel) value);
        } else {
            throw new IllegalStateException(value.getClass().getSimpleName() + " is not supported");
        }
    }
}
