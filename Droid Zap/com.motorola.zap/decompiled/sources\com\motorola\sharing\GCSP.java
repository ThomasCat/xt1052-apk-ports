package com.motorola.sharing;

import android.content.Context;
import com.motorola.sharing.instrumentation.CheckinManager;

public class GCSP {
    private static Authenticator sAuthenticator = null;
    private static ContentDispatcher sContentDispatcher = null;
    private static Context sContext = null;
    private static EndpointManager sEndpointManager = null;
    private static GroupManager sGroupManager = null;
    private static SyncableSettingsListener sSyncableSettingsListener = null;

    public static synchronized void init(Context context, Authenticator authenticator) {
        synchronized (GCSP.class) {
            setContext(context);
            setAuthenticator(authenticator);
        }
    }

    private static synchronized void setContext(Context context) {
        synchronized (GCSP.class) {
            if (context == null) {
                throw new IllegalArgumentException("context must not be null");
            }
            sContext = context.getApplicationContext();
        }
    }

    public static synchronized Context getContext() {
        Context context;
        synchronized (GCSP.class) {
            context = sContext;
        }
        return context;
    }

    private static synchronized void setAuthenticator(Authenticator authenticator) {
        synchronized (GCSP.class) {
            if (authenticator == null) {
                throw new IllegalArgumentException("authenticator must not be null");
            }
            sAuthenticator = authenticator;
        }
    }

    public static synchronized Authenticator getAuthenticator() {
        Authenticator authenticator;
        synchronized (GCSP.class) {
            authenticator = sAuthenticator;
        }
        return authenticator;
    }

    public static synchronized Config getConfig() {
        Config instance;
        synchronized (GCSP.class) {
            instance = Config.getInstance();
        }
        return instance;
    }

    public static synchronized CheckinManager getCheckinManager() {
        CheckinManager instance;
        synchronized (GCSP.class) {
            instance = CheckinManager.getInstance();
        }
        return instance;
    }

    public static synchronized EndpointManager getEndpointManager() {
        EndpointManager endpointManager;
        synchronized (GCSP.class) {
            if (sEndpointManager == null) {
                sEndpointManager = new EndpointManager();
            }
            endpointManager = sEndpointManager;
        }
        return endpointManager;
    }

    public static synchronized ContentDispatcher getContentDispatcher() {
        ContentDispatcher contentDispatcher;
        synchronized (GCSP.class) {
            if (sContentDispatcher == null) {
                sContentDispatcher = new ContentDispatcher();
            }
            contentDispatcher = sContentDispatcher;
        }
        return contentDispatcher;
    }

    public static synchronized GroupManager getGroupManager() {
        GroupManager groupManager;
        synchronized (GCSP.class) {
            if (sGroupManager == null) {
                sGroupManager = new GroupManager();
            }
            groupManager = sGroupManager;
        }
        return groupManager;
    }

    public static synchronized void setSyncableSettingsListener(SyncableSettingsListener listener) {
        synchronized (GCSP.class) {
            sSyncableSettingsListener = listener;
        }
    }

    public static synchronized SyncableSettingsListener getSyncableSettingsListener() {
        SyncableSettingsListener syncableSettingsListener;
        synchronized (GCSP.class) {
            syncableSettingsListener = sSyncableSettingsListener;
        }
        return syncableSettingsListener;
    }
}
