package com.google.sample.castcompanionlibrary.cast;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.MediaRouteActionProvider;
import android.support.v7.app.MediaRouteButton;
import android.support.v7.app.MediaRouteDialogFactory;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.media.MediaRouter.RouteInfo;
import android.view.Menu;
import android.view.MenuItem;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.Cast.ApplicationConnectionResult;
import com.google.android.gms.cast.Cast.CastOptions.Builder;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.CastMediaControlIntent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.sample.castcompanionlibrary.cast.callbacks.IBaseCastConsumer;
import com.google.sample.castcompanionlibrary.cast.exceptions.CastException;
import com.google.sample.castcompanionlibrary.cast.exceptions.NoConnectionException;
import com.google.sample.castcompanionlibrary.cast.exceptions.OnFailedListener;
import com.google.sample.castcompanionlibrary.cast.exceptions.TransientNetworkDisconnectionException;
import com.google.sample.castcompanionlibrary.utils.LogUtils;
import com.google.sample.castcompanionlibrary.utils.Utils;
import com.motorola.zap.R;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public abstract class BaseCastManager implements ConnectionCallbacks, OnConnectionFailedListener, DeviceSelectionListener, OnFailedListener {
    private static String CCL_VERSION = null;
    public static final int FEATURE_DEBUGGING = 1;
    public static final int FEATURE_LOCKSCREEN = 2;
    public static final int FEATURE_NOTIFICATION = 4;
    public static final int NO_STATUS_CODE = -1;
    public static final String PREFS_KEY_APPLICATION_ID = "application-id";
    public static final String PREFS_KEY_CAST_ACTIVITY_NAME = "cast-activity-name";
    public static final String PREFS_KEY_CAST_CUSTOM_DATA_NAMESPACE = "cast-custom-data-namespace";
    public static final String PREFS_KEY_ROUTE_ID = "route-id";
    public static final String PREFS_KEY_SESSION_ID = "session-id";
    public static final String PREFS_KEY_VOLUME_INCREMENT = "volume-increment";
    /* access modifiers changed from: private */
    public static final String TAG = LogUtils.makeLogTag(BaseCastManager.class);
    protected static BaseCastManager mCastManager;
    protected GoogleApiClient mApiClient;
    protected String mApplicationId;
    private final Set<IBaseCastConsumer> mBaseCastConsumers = Collections.synchronizedSet(new HashSet());
    protected int mCapabilities;
    protected boolean mConnectionSuspened;
    protected Context mContext;
    private boolean mDestroyOnDisconnect = false;
    protected String mDeviceName;
    protected Handler mHandler;
    protected MediaRouteSelector mMediaRouteSelector;
    protected MediaRouter mMediaRouter;
    protected CastMediaRouterCallback mMediaRouterCallback;
    protected ReconnectionStatus mReconnectionStatus = ReconnectionStatus.INACTIVE;
    protected AsyncTask<Void, Integer, Integer> mReconnectionTask;
    protected CastDevice mSelectedCastDevice;
    protected String mSessionId;
    protected boolean mUiVisible;
    protected int mVisibilityCounter;
    private boolean mWifiConnectivity = true;

    /* renamed from: com.google.sample.castcompanionlibrary.cast.BaseCastManager$6 reason: invalid class name */
    static /* synthetic */ class AnonymousClass6 {
        static final /* synthetic */ int[] $SwitchMap$com$google$sample$castcompanionlibrary$cast$BaseCastManager$ReconnectionStatus = new int[ReconnectionStatus.values().length];

        static {
            try {
                $SwitchMap$com$google$sample$castcompanionlibrary$cast$BaseCastManager$ReconnectionStatus[ReconnectionStatus.STARTED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$google$sample$castcompanionlibrary$cast$BaseCastManager$ReconnectionStatus[ReconnectionStatus.IN_PROGRESS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$google$sample$castcompanionlibrary$cast$BaseCastManager$ReconnectionStatus[ReconnectionStatus.FINALIZE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    public enum ReconnectionStatus {
        STARTED,
        IN_PROGRESS,
        FINALIZE,
        INACTIVE
    }

    /* access modifiers changed from: 0000 */
    public abstract Builder getCastOptionBuilder(CastDevice castDevice);

    /* access modifiers changed from: 0000 */
    public abstract MediaRouteDialogFactory getMediaRouteDialogFactory();

    /* access modifiers changed from: 0000 */
    public abstract void onApplicationConnected(ApplicationMetadata applicationMetadata, String str, String str2, boolean z);

    /* access modifiers changed from: 0000 */
    public abstract void onApplicationConnectionFailed(int i);

    /* access modifiers changed from: 0000 */
    public abstract void onApplicationStopFailed(int i);

    /* access modifiers changed from: 0000 */
    public abstract void onDeviceUnselected();

    protected BaseCastManager(Context context, String applicationId) {
        CCL_VERSION = context.getString(R.string.ccl_version);
        LogUtils.LOGD(TAG, "BaseCastManager is instantiated");
        this.mContext = context;
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mApplicationId = applicationId;
        Utils.saveStringToPreference(this.mContext, PREFS_KEY_APPLICATION_ID, applicationId);
        LogUtils.LOGD(TAG, "Application ID is: " + this.mApplicationId);
        this.mMediaRouter = MediaRouter.getInstance(context);
        this.mMediaRouteSelector = new MediaRouteSelector.Builder().addControlCategory(CastMediaControlIntent.categoryForCast(this.mApplicationId)).build();
        this.mMediaRouterCallback = new CastMediaRouterCallback(this, context);
        this.mMediaRouter.addCallback(this.mMediaRouteSelector, this.mMediaRouterCallback, 1);
    }

    public void onWifiConnectivityChanged(boolean connected) {
        LogUtils.LOGD(TAG, "WIFI connectivity changed to " + (connected ? "enabled" : "disabled"));
        if (!connected || this.mWifiConnectivity) {
            this.mWifiConnectivity = connected;
            return;
        }
        this.mWifiConnectivity = true;
        this.mHandler.postDelayed(new Runnable() {
            public void run() {
                BaseCastManager.this.reconnectSessionIfPossible(BaseCastManager.this.mContext, false, 10);
            }
        }, 1000);
    }

    public static BaseCastManager getCastManager() {
        return mCastManager;
    }

    public void setContext(Context context) {
        this.mContext = context;
    }

    public void onDeviceSelected(CastDevice device) {
        setDevice(device, this.mDestroyOnDisconnect);
    }

    public void onCastAvailabilityChanged(boolean castPresent) {
        if (this.mBaseCastConsumers != null) {
            synchronized (this.mBaseCastConsumers) {
                for (IBaseCastConsumer consumer : this.mBaseCastConsumers) {
                    try {
                        consumer.onCastAvailabilityChanged(castPresent);
                    } catch (Exception e) {
                        LogUtils.LOGE(TAG, "onCastAvailabilityChanged(): Failed to inform " + consumer, e);
                    }
                }
            }
        }
    }

    public void setDevice(CastDevice device, boolean stopAppOnExit) {
        this.mSelectedCastDevice = device;
        this.mDeviceName = this.mSelectedCastDevice != null ? this.mSelectedCastDevice.getFriendlyName() : null;
        if (this.mSelectedCastDevice == null) {
            if (!this.mConnectionSuspened) {
                Utils.saveStringToPreference(this.mContext, PREFS_KEY_SESSION_ID, null);
                Utils.saveStringToPreference(this.mContext, PREFS_KEY_ROUTE_ID, null);
            }
            this.mConnectionSuspened = false;
            try {
                if (isConnected() && stopAppOnExit) {
                    LogUtils.LOGD(TAG, "Calling stopApplication");
                    stopApplication();
                }
            } catch (IllegalStateException e) {
                LogUtils.LOGE(TAG, "Failed to stop the application after disconnecting route", e);
            } catch (IOException e2) {
                LogUtils.LOGE(TAG, "Failed to stop the application after disconnecting route", e2);
            } catch (TransientNetworkDisconnectionException e3) {
                LogUtils.LOGE(TAG, "Failed to stop the application after disconnecting route", e3);
            } catch (NoConnectionException e4) {
                LogUtils.LOGE(TAG, "Failed to stop the application after disconnecting route", e4);
            }
            onDisconnected();
            onDeviceUnselected();
            if (this.mApiClient != null) {
                LogUtils.LOGD(TAG, "Trying to disconnect");
                this.mApiClient.disconnect();
                if (this.mMediaRouter != null) {
                    this.mMediaRouter.selectRoute(this.mMediaRouter.getDefaultRoute());
                }
                this.mApiClient = null;
            }
            this.mSessionId = null;
        } else if (this.mApiClient == null) {
            LogUtils.LOGD(TAG, "acquiring a connection to Google Play services for " + this.mSelectedCastDevice);
            this.mApiClient = new GoogleApiClient.Builder(this.mContext).addApi(Cast.API, getCastOptionBuilder(this.mSelectedCastDevice).build()).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
            this.mApiClient.connect();
        } else if (!this.mApiClient.isConnected()) {
            this.mApiClient.connect();
        }
    }

    public void onCastDeviceDetected(RouteInfo info) {
        if (this.mBaseCastConsumers != null) {
            synchronized (this.mBaseCastConsumers) {
                for (IBaseCastConsumer consumer : this.mBaseCastConsumers) {
                    try {
                        consumer.onCastDeviceDetected(info);
                    } catch (Exception e) {
                        LogUtils.LOGE(TAG, "onCastDeviceDetected(): Failed to inform " + consumer, e);
                    }
                }
            }
        }
    }

    public MenuItem addMediaRouterButton(Menu menu, int menuResourceId) {
        MenuItem mediaRouteMenuItem = menu.findItem(menuResourceId);
        MediaRouteActionProvider mediaRouteActionProvider = (MediaRouteActionProvider) MenuItemCompat.getActionProvider(mediaRouteMenuItem);
        mediaRouteActionProvider.setRouteSelector(this.mMediaRouteSelector);
        if (getMediaRouteDialogFactory() != null) {
            mediaRouteActionProvider.setDialogFactory(getMediaRouteDialogFactory());
        }
        return mediaRouteMenuItem;
    }

    public MediaRouteButton addMediaRouterButton(MediaRouteButton button) {
        button.setRouteSelector(this.mMediaRouteSelector);
        if (getMediaRouteDialogFactory() != null) {
            button.setDialogFactory(getMediaRouteDialogFactory());
        }
        return button;
    }

    public synchronized void incrementUiCounter() {
        this.mVisibilityCounter++;
        if (!this.mUiVisible) {
            this.mUiVisible = true;
            onUiVisibilityChanged(true);
        }
        if (this.mVisibilityCounter == 0) {
            LogUtils.LOGD(TAG, "UI is no longer visible");
        } else {
            LogUtils.LOGD(TAG, "UI is visible");
        }
    }

    public synchronized void decrementUiCounter() {
        int i = this.mVisibilityCounter - 1;
        this.mVisibilityCounter = i;
        if (i == 0) {
            LogUtils.LOGD(TAG, "UI is no longer visible");
            if (this.mUiVisible) {
                this.mUiVisible = false;
                onUiVisibilityChanged(false);
            }
        } else {
            LogUtils.LOGD(TAG, "UI is visible");
        }
    }

    /* access modifiers changed from: protected */
    public void onUiVisibilityChanged(boolean visible) {
        if (visible) {
            if (this.mMediaRouter != null && this.mMediaRouterCallback != null) {
                LogUtils.LOGD(TAG, "onUiVisibilityChanged() addCallback called");
                this.mMediaRouter.addCallback(this.mMediaRouteSelector, this.mMediaRouterCallback, 1);
            }
        } else if (this.mMediaRouter != null) {
            LogUtils.LOGD(TAG, "onUiVisibilityChanged() removeCallback called");
            this.mMediaRouter.removeCallback(this.mMediaRouterCallback);
        }
    }

    public static boolean checkGooglePlayServices(Activity activity) {
        return Utils.checkGooglePlayServices(activity);
    }

    public static boolean checkGooglePlaySevices(Activity activity) {
        return checkGooglePlayServices(activity);
    }

    public boolean isConnected() {
        return this.mApiClient != null && this.mApiClient.isConnected();
    }

    public void disconnect() {
        if (isConnected()) {
            setDevice(null, false);
        }
    }

    public final String getDeviceName() {
        return this.mDeviceName;
    }

    public final void setStopOnDisconnect(boolean stopOnExit) {
        this.mDestroyOnDisconnect = stopOnExit;
    }

    public final MediaRouteSelector getMediaRouteSelector() {
        return this.mMediaRouteSelector;
    }

    public void enableFeatures(int capabilities) {
        this.mCapabilities = capabilities;
    }

    /* access modifiers changed from: protected */
    public boolean isFeatureEnabled(int feature) {
        return (this.mCapabilities & feature) > 0;
    }

    public void setDeviceVolume(double volume) throws CastException, TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        try {
            Cast.CastApi.setVolume(this.mApiClient, volume);
        } catch (Exception e) {
            LogUtils.LOGE(TAG, "Failed to set volume", e);
            throw new CastException("Failed to set volume");
        }
    }

    public final double getDeviceVolume() throws TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        return Cast.CastApi.getVolume(this.mApiClient);
    }

    public void incrementDeviceVolume(double delta) throws CastException, TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        double vol = getDeviceVolume();
        if (vol >= 0.0d) {
            setDeviceVolume(vol + delta);
        }
    }

    public final boolean isDeviceMute() throws TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        return Cast.CastApi.isMute(this.mApiClient);
    }

    public void setDeviceMute(boolean mute) throws CastException, TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        try {
            Cast.CastApi.setMute(this.mApiClient, mute);
        } catch (Exception e) {
            LogUtils.LOGE(TAG, "Failed to set mute to: " + mute, e);
            throw new CastException("Failed to mute");
        }
    }

    public void clearContext() {
        this.mContext = null;
    }

    public void clearContext(Context context) {
        if (this.mContext != null && this.mContext == context) {
            LogUtils.LOGD(TAG, "Cleared context: " + context);
            this.mContext = null;
        }
    }

    public ReconnectionStatus getReconnectionStatus() {
        return this.mReconnectionStatus;
    }

    public final void setReconnectionStatus(ReconnectionStatus status) {
        this.mReconnectionStatus = status;
    }

    public final boolean canConsiderSessionRecovery(Context context) {
        String sessionId = Utils.getStringFromPreference(context, PREFS_KEY_SESSION_ID);
        String routeId = Utils.getStringFromPreference(context, PREFS_KEY_ROUTE_ID);
        if (sessionId == null || routeId == null) {
            return false;
        }
        LogUtils.LOGD(TAG, "Found session info in the preferences, so proceed with an attempt to reconnect if possible");
        return true;
    }

    private void reconnectSessionIfPossibleInternal(RouteInfo theRoute) {
        if (!isConnected()) {
            String sessionId = Utils.getStringFromPreference(this.mContext, PREFS_KEY_SESSION_ID);
            String routeId = Utils.getStringFromPreference(this.mContext, PREFS_KEY_ROUTE_ID);
            LogUtils.LOGD(TAG, "reconnectSessionIfPossible() Retrieved from preferences: sessionId=" + sessionId + ", routeId=" + routeId);
            if (sessionId != null && routeId != null) {
                this.mReconnectionStatus = ReconnectionStatus.IN_PROGRESS;
                CastDevice device = CastDevice.getFromBundle(theRoute.getExtras());
                if (device != null) {
                    LogUtils.LOGD(TAG, "trying to acquire Cast Client for " + device);
                    onDeviceSelected(device);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void cancelReconnectionTask() {
        LogUtils.LOGD(TAG, "cancelling reconnection task");
        if (this.mReconnectionTask != null && !this.mReconnectionTask.isCancelled()) {
            this.mReconnectionTask.cancel(true);
        }
    }

    public void reconnectSessionIfPossible(final Context context, final boolean showDialog, final int timeoutInSeconds) {
        if (!isConnected()) {
            LogUtils.LOGD(TAG, "reconnectSessionIfPossible()");
            String routeId = Utils.getStringFromPreference(context, PREFS_KEY_ROUTE_ID);
            if (canConsiderSessionRecovery(context)) {
                List<RouteInfo> routes = this.mMediaRouter.getRoutes();
                RouteInfo theRoute = null;
                if (routes != null && !routes.isEmpty()) {
                    Iterator i$ = routes.iterator();
                    while (true) {
                        if (!i$.hasNext()) {
                            break;
                        }
                        RouteInfo route = (RouteInfo) i$.next();
                        if (route.getId().equals(routeId)) {
                            theRoute = route;
                            break;
                        }
                    }
                }
                if (theRoute != null) {
                    reconnectSessionIfPossibleInternal(theRoute);
                } else {
                    this.mReconnectionStatus = ReconnectionStatus.STARTED;
                }
                this.mReconnectionTask = new AsyncTask<Void, Integer, Integer>() {
                    private final int FAILED = 2;
                    private final int SUCCESS = 1;
                    /* access modifiers changed from: private */
                    public ProgressDialog dlg;

                    /* access modifiers changed from: protected */
                    public void onCancelled() {
                        if (this.dlg != null) {
                            this.dlg.dismiss();
                        }
                        super.onCancelled();
                    }

                    /* access modifiers changed from: protected */
                    public void onPreExecute() {
                        if (showDialog) {
                            this.dlg = new ProgressDialog(context);
                            this.dlg.setMessage(context.getString(R.string.session_reconnection_attempt));
                            this.dlg.setIndeterminate(true);
                            this.dlg.setCancelable(true);
                            this.dlg.setOnCancelListener(new OnCancelListener() {
                                public void onCancel(DialogInterface dialog) {
                                    switch (AnonymousClass6.$SwitchMap$com$google$sample$castcompanionlibrary$cast$BaseCastManager$ReconnectionStatus[BaseCastManager.this.mReconnectionStatus.ordinal()]) {
                                        case 1:
                                        case 2:
                                        case 3:
                                            BaseCastManager.this.mReconnectionStatus = ReconnectionStatus.INACTIVE;
                                            BaseCastManager.this.onDeviceSelected(null);
                                            break;
                                    }
                                    BaseCastManager.this.mReconnectionStatus = ReconnectionStatus.INACTIVE;
                                    if (AnonymousClass2.this.dlg != null) {
                                        AnonymousClass2.this.dlg.dismiss();
                                    }
                                    BaseCastManager.this.mReconnectionTask.cancel(true);
                                }
                            });
                            this.dlg.setButton(-2, "Cancel", new OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (AnonymousClass6.$SwitchMap$com$google$sample$castcompanionlibrary$cast$BaseCastManager$ReconnectionStatus[BaseCastManager.this.mReconnectionStatus.ordinal()]) {
                                        case 1:
                                        case 2:
                                        case 3:
                                            BaseCastManager.this.mReconnectionStatus = ReconnectionStatus.INACTIVE;
                                            BaseCastManager.this.onDeviceSelected(null);
                                            break;
                                    }
                                    BaseCastManager.this.mReconnectionStatus = ReconnectionStatus.INACTIVE;
                                    if (AnonymousClass2.this.dlg != null) {
                                        AnonymousClass2.this.dlg.cancel();
                                    }
                                    BaseCastManager.this.mReconnectionTask.cancel(true);
                                }
                            });
                            this.dlg.show();
                        }
                    }

                    /* access modifiers changed from: protected */
                    public Integer doInBackground(Void... params) {
                        for (int i = 0; i < timeoutInSeconds; i++) {
                            if (BaseCastManager.this.mReconnectionTask.isCancelled()) {
                                if (this.dlg != null) {
                                    this.dlg.dismiss();
                                }
                                return Integer.valueOf(1);
                            }
                            try {
                                if (BaseCastManager.this.isConnected()) {
                                    cancel(true);
                                }
                                Thread.sleep(1000);
                            } catch (Exception e) {
                            }
                        }
                        return Integer.valueOf(2);
                    }

                    /* access modifiers changed from: protected */
                    public void onPostExecute(Integer result) {
                        if (showDialog && this.dlg != null) {
                            this.dlg.dismiss();
                        }
                        if (result != null && result.intValue() == 2) {
                            BaseCastManager.this.mReconnectionStatus = ReconnectionStatus.INACTIVE;
                            BaseCastManager.this.onDeviceSelected(null);
                        }
                    }
                };
                this.mReconnectionTask.execute(new Void[0]);
            }
        }
    }

    public void reconnectSessionIfPossible(Context context, boolean showDialog) {
        LogUtils.LOGD(TAG, "Context for calling reconnectSessionIfPossible(): " + context);
        reconnectSessionIfPossible(context, showDialog, 5);
    }

    public void onConnectivityRecovered() {
        if (this.mBaseCastConsumers != null) {
            synchronized (this.mBaseCastConsumers) {
                for (IBaseCastConsumer consumer : this.mBaseCastConsumers) {
                    try {
                        consumer.onConnectivityRecovered();
                    } catch (Exception e) {
                        LogUtils.LOGE(TAG, "onConnectivityRecovered: Failed to inform " + consumer, e);
                    }
                }
            }
        }
    }

    public void onConnected(Bundle hint) {
        LogUtils.LOGD(TAG, "onConnected() reached with prior suspension: " + this.mConnectionSuspened);
        if (this.mConnectionSuspened) {
            this.mConnectionSuspened = false;
            if (hint == null || !hint.getBoolean(Cast.EXTRA_APP_NO_LONGER_RUNNING)) {
                onConnectivityRecovered();
                return;
            }
            LogUtils.LOGD(TAG, "onConnected(): App no longer running, so disconnecting");
            disconnect();
        } else if (isConnected()) {
            try {
                Cast.CastApi.requestStatus(this.mApiClient);
                launchApp();
                if (this.mBaseCastConsumers != null) {
                    synchronized (this.mBaseCastConsumers) {
                        for (IBaseCastConsumer consumer : this.mBaseCastConsumers) {
                            try {
                                consumer.onConnected();
                            } catch (Exception e) {
                                LogUtils.LOGE(TAG, "onConnected: Failed to inform " + consumer, e);
                            }
                        }
                    }
                }
            } catch (IOException e2) {
                LogUtils.LOGE(TAG, "error requesting status", e2);
            } catch (IllegalStateException e3) {
                LogUtils.LOGE(TAG, "error requesting status", e3);
            } catch (TransientNetworkDisconnectionException e4) {
                LogUtils.LOGE(TAG, "error requesting status due to network issues", e4);
            } catch (NoConnectionException e5) {
                LogUtils.LOGE(TAG, "error requesting status due to network issues", e5);
            }
        } else if (this.mReconnectionStatus == ReconnectionStatus.IN_PROGRESS) {
            this.mReconnectionStatus = ReconnectionStatus.INACTIVE;
        }
    }

    /* access modifiers changed from: protected */
    public void onDisconnected() {
        LogUtils.LOGD(TAG, "onDisconnected() reached");
        this.mDeviceName = null;
        if (this.mBaseCastConsumers != null) {
            synchronized (this.mBaseCastConsumers) {
                for (IBaseCastConsumer consumer : this.mBaseCastConsumers) {
                    try {
                        consumer.onDisconnected();
                    } catch (Exception e) {
                        LogUtils.LOGE(TAG, "onDisconnected(): Failed to inform " + consumer, e);
                    }
                }
            }
        }
    }

    public void onConnectionFailed(ConnectionResult result) {
        LogUtils.LOGD(TAG, "onConnectionFailed() reached, error code: " + result.getErrorCode() + ", reason: " + result.toString());
        this.mConnectionSuspened = false;
        setDevice(null, this.mDestroyOnDisconnect);
        if (this.mMediaRouter != null) {
            this.mMediaRouter.selectRoute(this.mMediaRouter.getDefaultRoute());
        }
        if (this.mBaseCastConsumers != null) {
            synchronized (this.mBaseCastConsumers) {
                for (IBaseCastConsumer consumer : this.mBaseCastConsumers) {
                    try {
                        consumer.onConnectionFailed(result);
                    } catch (Exception e) {
                        LogUtils.LOGE(TAG, "onConnectionFailed(): Failed to inform " + consumer, e);
                    }
                }
            }
        }
        if (0 != 0) {
            Utils.showErrorDialog(this.mContext, (int) R.string.failed_to_connect);
        }
    }

    public void onConnectionSuspended(int cause) {
        this.mConnectionSuspened = true;
        LogUtils.LOGD(TAG, "onConnectionSuspended() was called with cause: " + cause);
        if (this.mBaseCastConsumers != null) {
            synchronized (this.mBaseCastConsumers) {
                for (IBaseCastConsumer consumer : this.mBaseCastConsumers) {
                    try {
                        consumer.onConnectionSuspended(cause);
                    } catch (Exception e) {
                        LogUtils.LOGE(TAG, "onConnectionSuspended(): Failed to inform " + consumer, e);
                    }
                }
            }
        }
    }

    private void launchApp() throws TransientNetworkDisconnectionException, NoConnectionException {
        LogUtils.LOGD(TAG, "launchApp() is called");
        if (!isConnected()) {
            if (this.mReconnectionStatus == ReconnectionStatus.IN_PROGRESS) {
                this.mReconnectionStatus = ReconnectionStatus.INACTIVE;
                return;
            }
            checkConnectivity();
        }
        if (this.mReconnectionStatus == ReconnectionStatus.IN_PROGRESS) {
            LogUtils.LOGD(TAG, "Attempting to join a previously interrupted session...");
            String sessionId = Utils.getStringFromPreference(this.mContext, PREFS_KEY_SESSION_ID);
            LogUtils.LOGD(TAG, "joinApplication() -> start");
            Cast.CastApi.joinApplication(this.mApiClient, this.mApplicationId, sessionId).setResultCallback(new ResultCallback<ApplicationConnectionResult>() {
                public void onResult(ApplicationConnectionResult result) {
                    if (result.getStatus().isSuccess()) {
                        LogUtils.LOGD(BaseCastManager.TAG, "joinApplication() -> success");
                        BaseCastManager.this.onApplicationConnected(result.getApplicationMetadata(), result.getApplicationStatus(), result.getSessionId(), result.getWasLaunched());
                        return;
                    }
                    LogUtils.LOGD(BaseCastManager.TAG, "joinApplication() -> failure");
                    BaseCastManager.this.onApplicationConnectionFailed(result.getStatus().getStatusCode());
                }
            });
            return;
        }
        LogUtils.LOGD(TAG, "Launching app");
        Cast.CastApi.launchApplication(this.mApiClient, this.mApplicationId).setResultCallback(new ResultCallback<ApplicationConnectionResult>() {
            public void onResult(ApplicationConnectionResult result) {
                if (result.getStatus().isSuccess()) {
                    LogUtils.LOGD(BaseCastManager.TAG, "launchApplication() -> success result");
                    BaseCastManager.this.onApplicationConnected(result.getApplicationMetadata(), result.getApplicationStatus(), result.getSessionId(), result.getWasLaunched());
                    return;
                }
                LogUtils.LOGD(BaseCastManager.TAG, "launchApplication() -> failure result");
                BaseCastManager.this.onApplicationConnectionFailed(result.getStatus().getStatusCode());
            }
        });
    }

    public void stopApplication() throws IllegalStateException, IOException, TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        Cast.CastApi.stopApplication(this.mApiClient, this.mSessionId).setResultCallback(new ResultCallback<Status>() {
            public void onResult(Status result) {
                if (!result.isSuccess()) {
                    LogUtils.LOGD(BaseCastManager.TAG, "stopApplication -> onResult: stopping application failed");
                    BaseCastManager.this.onApplicationStopFailed(result.getStatusCode());
                    return;
                }
                LogUtils.LOGD(BaseCastManager.TAG, "stopApplication -> onResult Stopped application successfully");
            }
        });
    }

    public void addBaseCastConsumer(IBaseCastConsumer listener) {
        if (listener != null) {
            synchronized (this.mBaseCastConsumers) {
                if (this.mBaseCastConsumers.add(listener)) {
                    LogUtils.LOGD(TAG, "Successfully added the new BaseCastConsumer listener " + listener);
                }
            }
        }
    }

    public void removeBaseCastConsumer(IBaseCastConsumer listener) {
        if (listener != null) {
            synchronized (this.mBaseCastConsumers) {
                if (this.mBaseCastConsumers.remove(listener)) {
                    LogUtils.LOGD(TAG, "Successfully removed the existing BaseCastConsumer listener " + listener);
                }
            }
        }
    }

    public void checkConnectivity() throws TransientNetworkDisconnectionException, NoConnectionException {
        if (isConnected()) {
            return;
        }
        if (this.mConnectionSuspened) {
            throw new TransientNetworkDisconnectionException();
        }
        throw new NoConnectionException();
    }

    public void onFailed(int resourceId, int statusCode) {
        LogUtils.LOGD(TAG, "onFailed() was called with statusCode: " + statusCode);
        if (this.mBaseCastConsumers != null) {
            synchronized (this.mBaseCastConsumers) {
                for (IBaseCastConsumer consumer : this.mBaseCastConsumers) {
                    try {
                        consumer.onFailed(resourceId, statusCode);
                    } catch (Exception e) {
                        LogUtils.LOGE(TAG, "onFailed(): Failed to inform " + consumer, e);
                    }
                }
            }
        }
    }

    public static final String getCclVersion() {
        return CCL_VERSION;
    }
}
