package com.google.tagmanager;

interface RateLimiter {
    boolean tokenAvailable();
}
