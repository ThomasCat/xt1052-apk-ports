package com.motorola.sharing;

public class Abuse {

    public enum AbuseAction {
        NONE,
        WARNING,
        SENDING_BLOCKED,
        BLOCKED
    }

    public enum AbuseType {
        UNKNOWN("Unknown"),
        SPAM("Spam"),
        PORNOGRAPHY("Pornography"),
        VIOLENCE("Violence"),
        HARASSMENT("Harassment"),
        COPYRIGHTED("Copyrighted");
        
        private String humanReadable;

        private AbuseType(String humanReadable2) {
            this.humanReadable = humanReadable2;
        }

        public String humanReadable() {
            return this.humanReadable;
        }
    }
}
