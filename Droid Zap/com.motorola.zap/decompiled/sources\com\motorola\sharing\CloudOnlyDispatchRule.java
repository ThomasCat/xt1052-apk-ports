package com.motorola.sharing;

import android.text.TextUtils;
import com.motorola.sharing.ContentContainer.DiscoveryParams;
import com.motorola.sharing.ContentContainer.Type;
import com.motorola.sharing.ContentContainer.UploadRule;
import com.motorola.sharing.ContentShareListener.ContentCancelShareEvent;
import com.motorola.sharing.ContentShareListener.ContentShareEvent;
import com.motorola.sharing.ContentShareListener.ContentShareEvent.Substatus;
import com.motorola.sharing.ContentShareListener.ContentStopShareEvent;
import com.motorola.sharing.DispatchRule.Params;
import com.motorola.sharing.provider.cloud.CloudProvider;
import com.motorola.sharing.util.ContainerExpiryTracker.ExpiryTrigger;
import com.motorola.sharing.util.ContainerExpiryTracker.ExpiryTriggers;
import com.motorola.sharing.util.Logger;

public class CloudOnlyDispatchRule implements DispatchRule {
    private static final String TAG = Logger.getGcspTag("CloudOnlyDispatch");
    private static CloudOnlyDispatchRule sRuleInstance = null;
    /* access modifiers changed from: private */
    public CloudProvider mCloudProvider = CloudProvider.getInstance();

    private class ContentShareInterceptListener implements ContentShareListener {
        private ContentShareListener mOriginalListener = null;

        ContentShareInterceptListener(ContentShareListener listener) {
            this.mOriginalListener = listener;
        }

        public void onContentShareEvent(ContentShareEvent event) {
            ExpiryTriggers expiryTriggers;
            switch (event.status) {
                case INFO:
                    if (event.substatus == Substatus.META_SHARED) {
                        CloudOnlyDispatchRule.this.mCloudProvider.uploadContent(event.container, null);
                        break;
                    }
                    break;
                case ERROR:
                    if (!TextUtils.isEmpty(event.container.shareId)) {
                        GCSP.getContentDispatcher().stopSharing(event.container.shareId, null);
                        break;
                    }
                    break;
                case SUCCESS:
                    if (event.substatus == Substatus.SHARED) {
                        ContentContainer contentContainer = event.container;
                        if (event.container.isGroupRelated()) {
                            expiryTriggers = new ExpiryTriggers(ExpiryTrigger.BY_GROUP_TERMINATION);
                        } else {
                            expiryTriggers = new ExpiryTriggers(ExpiryTrigger.BY_TIME);
                        }
                        contentContainer.expiryTriggers = expiryTriggers;
                        break;
                    }
                    break;
            }
            if (this.mOriginalListener != null) {
                this.mOriginalListener.onContentShareEvent(event);
            }
        }

        public void onContentStopShareEvent(ContentStopShareEvent event) {
            if (this.mOriginalListener != null) {
                if (event.container.type == Type.CANCEL_SHARE) {
                    event.container = null;
                }
                this.mOriginalListener.onContentStopShareEvent(event);
            }
        }

        public void onContentCancelShareEvent(ContentCancelShareEvent event) {
            if (this.mOriginalListener != null) {
                this.mOriginalListener.onContentCancelShareEvent(event);
            }
        }
    }

    static synchronized DispatchRule getInstance(Params params) {
        CloudOnlyDispatchRule cloudOnlyDispatchRule;
        synchronized (CloudOnlyDispatchRule.class) {
            if (sRuleInstance == null) {
                sRuleInstance = new CloudOnlyDispatchRule();
            }
            cloudOnlyDispatchRule = sRuleInstance;
        }
        return cloudOnlyDispatchRule;
    }

    private CloudOnlyDispatchRule() {
    }

    public Params getParams() {
        Params params = new Params();
        params.put(DispatchRule.KEY_CLASS_NAME, getClass().getName());
        return params;
    }

    public void discoverContent(DiscoveryParams params, ContentDiscoveryListener listener, Endpoint[] endpoints) {
        Logger.d(TAG, "discoverContent(): " + params.getPrintableSignature());
        params.setContentDiscoveryListener(listener);
        this.mCloudProvider.discoverContent(params, null, endpoints);
    }

    public void stopDiscovery(DiscoveryParams params) {
        Logger.d(TAG, "stopDiscovery(): " + params.getPrintableSignature());
        this.mCloudProvider.stopDiscovery(params);
    }

    public void downloadContent(ContentContainer container, ContentDownloadListener listener) {
        Logger.d(TAG, "downloadContent(): " + container.getPrintableSignature());
        container.setContentDownloadListener(listener);
        this.mCloudProvider.downloadContent(container, null);
    }

    public void cancelDownload(ContentContainer container) {
        Logger.d(TAG, "cancelDownload(): " + container.getPrintableSignature());
        this.mCloudProvider.cancelDownload(container);
    }

    public void reportAbuse(ContentContainer container, ReportAbuseListener listener) {
        Logger.d(TAG, "reportAbuse(): " + container.getPrintableSignature());
        container.setReportAbuseListener(listener);
        this.mCloudProvider.reportAbuse(container, null);
    }

    public void shareContent(ContentContainer container, ContentShareListener listener, Endpoint[] endpoints) {
        Logger.d(TAG, "shareContent(): " + container.getPrintableSignature());
        container.uploadRule = UploadRule.ALWAYS;
        setContentShareInterceptListener(container, listener);
        this.mCloudProvider.shareContent(container, null, endpoints);
    }

    public void stopSharing(ContentContainer container) {
        Logger.d(TAG, "stopSharing(): " + container.getPrintableSignature());
        this.mCloudProvider.stopSharing(container);
    }

    public void stopSharing(String shareId, ContentShareListener listener) {
        Logger.d(TAG, "stopSharing(): sid=" + shareId);
        ContentContainer container = ContentContainer.generateCancelShareContainer(shareId);
        setContentShareInterceptListener(container, listener);
        this.mCloudProvider.stopSharing(container);
    }

    public void cancelSharing(ContentContainer container) {
        Logger.d(TAG, "cancelSharing(): " + container.getPrintableSignature());
        this.mCloudProvider.cancelSharing(container);
    }

    public void resumeSharing(ContentContainer container, ContentShareListener listener) {
        Logger.d(TAG, "resumeSharing(): " + container.getPrintableSignature());
        container.setContentShareListener(listener);
        this.mCloudProvider.resumeSharing(container, null);
    }

    private synchronized void setContentShareInterceptListener(ContentContainer container, ContentShareListener clientListener) {
        if (clientListener instanceof ContentShareInterceptListener) {
            container.setContentShareListener(clientListener);
        } else {
            Logger.v(TAG, "intercepting share events for: " + container.getPrintableSignature());
            container.setContentShareListener(new ContentShareInterceptListener(clientListener));
        }
    }

    public Endpoint[] getStaticEndpoints() {
        return CloudProvider.getInstance().getStaticEndpoints();
    }
}
