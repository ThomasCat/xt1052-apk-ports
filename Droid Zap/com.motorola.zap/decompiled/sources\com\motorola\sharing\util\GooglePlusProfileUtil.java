package com.motorola.sharing.util;

import android.os.Handler;
import android.os.Looper;
import com.motorola.sharing.Authenticator.TokenInfo;
import com.motorola.sharing.SharedExecutors;
import com.motorola.sharing.provider.cloud.ConnectionException;
import com.motorola.sharing.provider.cloud.HttpHelper;
import com.motorola.sharing.util.GooglePlusProfileListener.Status;
import com.motorola.sharing.util.GooglePlusProfileListener.Substatus;
import com.motorola.sharing.util.auth.SyncAuthenticator;
import java.net.UnknownHostException;
import org.json.JSONException;
import org.json.JSONObject;

public class GooglePlusProfileUtil {
    private static final String TAG = Logger.getGcspTag("GPlus");
    private static GooglePlusProfileUtil sInstance;

    public static synchronized GooglePlusProfileUtil getsInstance() {
        GooglePlusProfileUtil googlePlusProfileUtil;
        synchronized (GooglePlusProfileUtil.class) {
            if (sInstance == null) {
                sInstance = new GooglePlusProfileUtil();
            }
            googlePlusProfileUtil = sInstance;
        }
        return googlePlusProfileUtil;
    }

    private GooglePlusProfileUtil() {
    }

    public void getMyProfile(final GooglePlusProfileListener listener) {
        final Handler handler = getHandler();
        SharedExecutors.threadPoolExecutor.execute(new Runnable() {
            public void run() {
                GooglePlusProfileUtil.this.doGetMyProfile(listener, handler);
            }
        });
    }

    /* access modifiers changed from: private */
    public void doGetMyProfile(GooglePlusProfileListener listener, Handler handler) {
        TokenInfo tokenInfo = SyncAuthenticator.auth();
        if (tokenInfo == null) {
            postEvent(null, Status.ERROR, Substatus.AUTHENTICATION_ERROR, listener, handler);
            return;
        }
        try {
            handleResp(HttpHelper.sendGetRequest("https://www.googleapis.com/plus/v1/people/me?fields=displayName%2Cimage&access_token=" + tokenInfo.getToken(), null, null, true), listener, handler);
        } catch (UnknownHostException eu) {
            Logger.e(TAG, "Network exception", eu);
            postEvent(null, Status.ERROR, Substatus.NETWORK_ERROR, listener, handler);
        } catch (ConnectionException ec) {
            Logger.e(TAG, "Network exception", ec);
            postEvent(null, Status.ERROR, Substatus.NETWORK_ERROR, listener, handler);
        }
    }

    private void handleResp(String respStr, GooglePlusProfileListener listener, Handler handler) {
        try {
            postEvent(new JSONObject(respStr), Status.SUCCESS, Substatus.NO_ERROR, listener, handler);
        } catch (JSONException e) {
            Logger.e(TAG, "Json exception", e);
            postEvent(null, Status.ERROR, Substatus.SERVER_ERROR, listener, handler);
        }
    }

    private Handler getHandler() {
        Looper looper = Looper.myLooper();
        if (looper == null) {
            Looper.getMainLooper();
        }
        return new Handler(looper);
    }

    public void postEvent(JSONObject profile, Status status, Substatus substatus, GooglePlusProfileListener listener, Handler handler) {
        if (listener != null) {
            final GooglePlusProfileListener googlePlusProfileListener = listener;
            final JSONObject jSONObject = profile;
            final Status status2 = status;
            final Substatus substatus2 = substatus;
            handler.post(new Runnable() {
                public void run() {
                    googlePlusProfileListener.onProfile(jSONObject, status2, substatus2);
                }
            });
        }
    }
}
