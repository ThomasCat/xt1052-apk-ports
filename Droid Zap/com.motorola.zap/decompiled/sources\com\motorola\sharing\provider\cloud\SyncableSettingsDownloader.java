package com.motorola.sharing.provider.cloud;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import com.motorola.gcsp.json.StatusCode;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.SyncableSettingsListener;
import com.motorola.sharing.instrumentation.ActionResult.Error;
import com.motorola.sharing.instrumentation.CheckinManager;
import com.motorola.sharing.instrumentation.Operation;
import com.motorola.sharing.instrumentation.Operation.Type;
import com.motorola.sharing.util.JsonHelper;
import com.motorola.sharing.util.Logger;
import com.motorola.sharing.util.PackageUtils;
import com.motorola.sharing.util.SharedPreference;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class SyncableSettingsDownloader {
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getCloudTag("SyncSettingsDown");
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public String mUserAccount;
    /* access modifiers changed from: private */
    public String mUserId;
    /* access modifiers changed from: private */
    public String mUserToken;

    private class AsyncSyncableSettingsDownloaderTask extends AsyncTask<String, Void, Void> {
        private AsyncSyncableSettingsDownloaderTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(String... params) {
            Logger.d(SyncableSettingsDownloader.TAG, "Executing Async Syncable Settings Downloader");
            if (params.length != 3) {
                throw new IllegalArgumentException("Invalid number of parameters passed!");
            }
            SyncableSettingsDownloader.this.mUserAccount = params[0];
            SyncableSettingsDownloader.this.mUserToken = params[1];
            SyncableSettingsDownloader.this.mUserId = params[2];
            Operation operation = new Operation(Type.SETTINGS_SYNC);
            if (SyncableSettingsDownloader.this.mUserAccount == null) {
                Logger.e(SyncableSettingsDownloader.TAG, "no user account");
                operation.actionResult.setError(Error.NO_USER_ACCOUNT);
                CheckinManager.getInstance().logSettingsSync(operation);
            } else {
                post(operation);
            }
            return null;
        }

        private void post(Operation operation) {
            int i;
            int i2 = 0;
            String url = UrlHelper.getSettings();
            int appVersion = PackageUtils.getAppVersionCode(SyncableSettingsDownloader.this.mContext);
            int settingsVersion = ((Integer) CloudSettings.SETTINGS_VERSION.get()).intValue();
            if (Logger.isDebuggable()) {
                Logger.d(SyncableSettingsDownloader.TAG, "---------------------");
                Logger.d(SyncableSettingsDownloader.TAG, "Syncable Settings Download:");
                Logger.d(SyncableSettingsDownloader.TAG, "  - UserAccount: " + Logger.obfuscate(SyncableSettingsDownloader.this.mUserAccount));
                String access$000 = SyncableSettingsDownloader.TAG;
                StringBuilder append = new StringBuilder().append("  - UserToken length: ");
                if (SyncableSettingsDownloader.this.mUserToken != null) {
                    i = SyncableSettingsDownloader.this.mUserToken.length();
                } else {
                    i = 0;
                }
                Logger.d(access$000, append.append(i).toString());
                String access$0002 = SyncableSettingsDownloader.TAG;
                StringBuilder append2 = new StringBuilder().append("  - UserId length: ");
                if (SyncableSettingsDownloader.this.mUserId != null) {
                    i2 = SyncableSettingsDownloader.this.mUserId.length();
                }
                Logger.d(access$0002, append2.append(i2).toString());
                Logger.d(SyncableSettingsDownloader.TAG, "  - App version: " + appVersion);
                Logger.d(SyncableSettingsDownloader.TAG, "  - Settings Version: " + settingsVersion);
                Logger.d(SyncableSettingsDownloader.TAG, "---------------------");
            }
            JSONObject payloadObj = new JSONObject();
            try {
                JsonHelper.addCommonRequestFields(payloadObj);
                try {
                    String response = HttpHelper.sendPostRequest(url, payloadObj.toString(), SyncableSettingsDownloader.this.mUserToken, SyncableSettingsDownloader.this.mUserId, true);
                    if (response == null) {
                        Logger.e(SyncableSettingsDownloader.TAG, "got empty server response");
                        operation.actionResult.setError(Error.EMPTY_RESPONSE);
                        CheckinManager.getInstance().logSettingsSync(operation);
                        return;
                    }
                    try {
                        JSONObject respObj = new JSONObject(response);
                        Logger.d(SyncableSettingsDownloader.TAG, "Server Response:");
                        Logger.dump(SyncableSettingsDownloader.TAG, respObj);
                        StatusCode serverStatus = StatusCode.codeOf(respObj.getString("status"));
                        if (serverStatus == StatusCode.OK) {
                            operation.actionResult.setSuccess();
                            CheckinManager.getInstance().logSettingsSync(operation);
                            updateSettings(respObj);
                            return;
                        }
                        Logger.e(SyncableSettingsDownloader.TAG, "invalid server response status: " + serverStatus);
                        if (respObj.has("message")) {
                            Logger.e(SyncableSettingsDownloader.TAG, "message from server: " + respObj.getString("message"));
                        }
                        operation.actionResult.setError(Error.INVALID_STATUS, serverStatus.name(), 0);
                        CheckinManager.getInstance().logSettingsSync(operation);
                    } catch (JSONException e) {
                        Logger.e(SyncableSettingsDownloader.TAG, "response not parsable: " + e);
                        operation.actionResult.setError(Error.RESPONSE_NOT_PARSABLE);
                        CheckinManager.getInstance().logSettingsSync(operation);
                    }
                } catch (UnknownHostException e2) {
                    Logger.e(SyncableSettingsDownloader.TAG, "got: " + e2);
                    operation.actionResult.setError(Error.UNKNOWN_HOST_EXCEPTION);
                    CheckinManager.getInstance().logSettingsSync(operation);
                } catch (ConnectionException e3) {
                    Logger.e(SyncableSettingsDownloader.TAG, "got (" + e3.getStatusCode() + "): " + e3);
                    operation.actionResult.setError(Error.CONNECTION_EXCEPTION, null, e3.getStatusCode());
                    CheckinManager.getInstance().logSettingsSync(operation);
                }
            } catch (JSONException e4) {
                Logger.e(SyncableSettingsDownloader.TAG, "got: " + e4);
                operation.actionResult.setError(Error.REQUEST_ERROR);
                CheckinManager.getInstance().logSettingsSync(operation);
            }
        }

        private boolean canUpdateSettings(int serverVersion) {
            return serverVersion > ((Integer) CloudSettings.SETTINGS_VERSION.get()).intValue();
        }

        private void updateSettings(JSONObject settingsJson) {
            Logger.i(SyncableSettingsDownloader.TAG, "updating settings received from server");
            try {
                if (settingsJson.has("settingsVersion")) {
                    int serverSettingsVersion = settingsJson.getInt("settingsVersion");
                    Logger.i(SyncableSettingsDownloader.TAG, "new settings version from server: " + serverSettingsVersion);
                    Logger.i(SyncableSettingsDownloader.TAG, "current settings version in client: " + CloudSettings.SETTINGS_VERSION.get());
                    if (canUpdateSettings(serverSettingsVersion)) {
                        CloudSettings.SETTINGS_VERSION.set(Integer.valueOf(serverSettingsVersion));
                        if (settingsJson.has("settings")) {
                            JSONObject settings = new JSONObject(settingsJson.getString("settings"));
                            if (settings.length() != 0) {
                                Map<String, String> appSettings = new HashMap<>();
                                Iterator<String> iterator = settings.keys();
                                while (iterator.hasNext()) {
                                    String name = (String) iterator.next();
                                    String value = settings.getString(name);
                                    Logger.d(SyncableSettingsDownloader.TAG, "updateSettings(): name=" + name + " value=" + value);
                                    SharedPreference s = SharedPreference.forName(name);
                                    if (s != null) {
                                        s.set(s.valueOf(value));
                                    } else {
                                        appSettings.put(name, value);
                                    }
                                }
                                SyncableSettingsListener listener = GCSP.getSyncableSettingsListener();
                                if (listener != null && !appSettings.isEmpty()) {
                                    SyncableSettingsDownloader.this.postSyncableSetting(listener, appSettings);
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                }
                Logger.w(SyncableSettingsDownloader.TAG, "settings version not found on server response - not updating");
            } catch (JSONException e) {
                Logger.e(SyncableSettingsDownloader.TAG, "could not parser settings coming from server - " + e.toString());
            }
        }
    }

    public SyncableSettingsDownloader(Context context) {
        this.mContext = context;
    }

    /* access modifiers changed from: private */
    public void postSyncableSetting(final SyncableSettingsListener listener, final Map<String, String> settings) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                listener.onSyncableSettings(settings);
            }
        });
    }

    public void start(final String userAccount, final String userToken, final String userId) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                new AsyncSyncableSettingsDownloaderTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{userAccount, userToken, userId});
            }
        });
    }
}
