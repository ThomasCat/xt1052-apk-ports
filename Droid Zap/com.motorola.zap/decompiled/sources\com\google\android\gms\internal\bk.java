package com.google.android.gms.internal;

import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdRequest.Gender;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.a;
import java.util.Date;
import java.util.HashSet;

public final class bk {
    public static int a(ErrorCode errorCode) {
        switch (errorCode) {
            case INVALID_REQUEST:
                return 1;
            case NETWORK_ERROR:
                return 2;
            case NO_FILL:
                return 3;
            default:
                return 0;
        }
    }

    public static int a(Gender gender) {
        switch (gender) {
            case FEMALE:
                return 2;
            case MALE:
                return 1;
            default:
                return 0;
        }
    }

    public static AdSize b(ab abVar) {
        return new AdSize(a.a(abVar.width, abVar.height, abVar.ln));
    }

    public static MediationAdRequest e(z zVar) {
        return new MediationAdRequest(new Date(zVar.le), g(zVar.lf), zVar.lg != null ? new HashSet(zVar.lg) : null, zVar.lh);
    }

    public static Gender g(int i) {
        switch (i) {
            case 1:
                return Gender.MALE;
            case 2:
                return Gender.FEMALE;
            default:
                return Gender.UNKNOWN;
        }
    }

    public static final ErrorCode h(int i) {
        switch (i) {
            case 1:
                return ErrorCode.INVALID_REQUEST;
            case 2:
                return ErrorCode.NETWORK_ERROR;
            case 3:
                return ErrorCode.NO_FILL;
            default:
                return ErrorCode.INTERNAL_ERROR;
        }
    }
}
