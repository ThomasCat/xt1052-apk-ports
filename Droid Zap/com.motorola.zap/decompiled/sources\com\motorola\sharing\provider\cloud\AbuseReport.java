package com.motorola.sharing.provider.cloud;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import com.motorola.gcsp.json.StatusCode;
import com.motorola.sharing.Authenticator;
import com.motorola.sharing.Authenticator.Callback;
import com.motorola.sharing.Authenticator.TokenInfo;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.ReportAbuseListener.ReportAbuseEvent.Substatus;
import com.motorola.sharing.instrumentation.ActionResult;
import com.motorola.sharing.instrumentation.ActionResult.Error;
import com.motorola.sharing.instrumentation.CheckinManager;
import com.motorola.sharing.util.DeviceUtils;
import com.motorola.sharing.util.HistoryManager.Shares.C;
import com.motorola.sharing.util.JsonHelper;
import com.motorola.sharing.util.Logger;
import java.net.UnknownHostException;
import org.json.JSONException;
import org.json.JSONObject;

public class AbuseReport {
    /* access modifiers changed from: private */
    public static final String TAG = Logger.getCloudTag("AbuseReport");
    private Callback mAuthCallback;
    /* access modifiers changed from: private */
    public ContentContainer mContainer;
    /* access modifiers changed from: private */
    public Context mContext;
    private boolean mIsStarted;

    private class ReportAbuseTask extends AsyncTask<Void, Void, Void> {
        private ReportAbuseTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            String imei = DeviceUtils.getImei(AbuseReport.this.mContext);
            String reqUrl = UrlHelper.getReportAbuse();
            JSONObject payload = new JSONObject();
            try {
                JsonHelper.addCommonRequestFields(payload);
                payload.put("shareId", AbuseReport.this.mContainer.shareId);
                payload.put("abuseCategory", AbuseReport.this.mContainer.abuseType.ordinal());
                payload.put(C.MIME_TYPE, AbuseReport.this.mContainer.mimeType);
                payload.put("reportedEmail", AbuseReport.this.mContainer.origin);
                Logger.d(AbuseReport.TAG, "Report Abuse Request:");
                Logger.dump(AbuseReport.TAG, payload);
                try {
                    String response = HttpHelper.sendPostRequest(reqUrl, payload.toString(), AbuseReport.this.mContainer.userCred.token, AbuseReport.this.mContainer.userCred.id, true);
                    if (response == null) {
                        Logger.e(AbuseReport.TAG, "got empty server response");
                        AbuseReport.this.mContainer.actionResult.setError(Error.EMPTY_RESPONSE);
                        AbuseReport.this.onComplete(Status.ERROR, Substatus.GENERAL);
                        return null;
                    }
                    try {
                        JSONObject responseJson = new JSONObject(response);
                        StatusCode status = StatusCode.codeOf(responseJson.getString("status"));
                        Logger.d(AbuseReport.TAG, "Report Abuse Response:");
                        Logger.dump(AbuseReport.TAG, responseJson);
                        if (status == StatusCode.OK || status == StatusCode.DUPLICATE) {
                            AbuseReport.this.mContainer.actionResult.setSuccess();
                            AbuseReport.this.onComplete(Status.SUCCESS, Substatus.SENT);
                            return null;
                        } else if (status == StatusCode.NOT_FOUND) {
                            try {
                                String uploadUrl = responseJson.getString("uploadURL");
                                if (uploadUrl != null) {
                                    uploadUrl = uploadUrl.replaceAll("http://", "https://");
                                }
                                AbuseReport.this.mContainer.url = uploadUrl;
                                if (HttpHelper.uploadContent(AbuseReport.this.mContainer, AbuseReport.this.mContext, 1000, null, true, true).success) {
                                    AbuseReport.this.mContainer.actionResult.setSuccess();
                                    AbuseReport.this.onComplete(Status.SUCCESS, Substatus.SENT);
                                    return null;
                                }
                                Logger.e(AbuseReport.TAG, "report abuse upload failed");
                                if (AbuseReport.this.mContainer.actionResult.getStatus() != ActionResult.Status.FAIL) {
                                    AbuseReport.this.mContainer.actionResult.setError(Error.UPLOAD_FAILED);
                                }
                                AbuseReport.this.onComplete(Status.ERROR, AbuseReport.this.mContainer.actionResult.getError() == Error.WIFI_ONLY_ALLOWED ? Substatus.NETWORK : Substatus.GENERAL);
                                return null;
                            } catch (JSONException e) {
                                Logger.e(AbuseReport.TAG, "response not parsable: " + e);
                                AbuseReport.this.mContainer.actionResult.setError(Error.RESPONSE_NOT_PARSABLE);
                                AbuseReport.this.onComplete(Status.ERROR, Substatus.GENERAL);
                                return null;
                            }
                        } else {
                            Logger.e(AbuseReport.TAG, "invalid server response status: " + status);
                            AbuseReport.this.mContainer.actionResult.setError(Error.INVALID_STATUS, status.name(), 0);
                            AbuseReport.this.onComplete(Status.ERROR, Substatus.GENERAL);
                            return null;
                        }
                    } catch (JSONException e2) {
                        Logger.e(AbuseReport.TAG, "response not parsable: " + e2);
                        AbuseReport.this.mContainer.actionResult.setError(Error.RESPONSE_NOT_PARSABLE);
                        AbuseReport.this.onComplete(Status.ERROR, Substatus.GENERAL);
                        return null;
                    }
                } catch (UnknownHostException e3) {
                    AbuseReport.this.mContainer.actionResult.setError(Error.UNKNOWN_HOST_EXCEPTION);
                    AbuseReport.this.onComplete(Status.ERROR, Substatus.NETWORK);
                    return null;
                } catch (ConnectionException e4) {
                    AbuseReport.this.mContainer.actionResult.setError(Error.CONNECTION_EXCEPTION, null, e4.getStatusCode());
                    AbuseReport.this.onComplete(Status.ERROR, Substatus.GENERAL);
                    return null;
                }
            } catch (JSONException e5) {
                Logger.e(AbuseReport.TAG, "got: " + e5);
                AbuseReport.this.mContainer.actionResult.setError(Error.REQUEST_ERROR);
                AbuseReport.this.onComplete(Status.ERROR, Substatus.GENERAL);
                return null;
            }
        }
    }

    public AbuseReport(ContentContainer container) {
        this.mIsStarted = false;
        this.mContext = null;
        this.mContainer = null;
        this.mAuthCallback = new Callback() {
            public void onAuthComplete(Authenticator.Status status, TokenInfo tokenInfo, Object data) {
                AbuseReport.this.mContainer.userCred.set(tokenInfo);
                if (!AbuseReport.this.mContainer.userCred.isEmpty()) {
                    AbuseReport.this.startTask();
                    return;
                }
                Logger.e(AbuseReport.TAG, "authentication failed");
                AbuseReport.this.mContainer.actionResult.setError(status == Authenticator.Status.ERROR_NETWORK ? Error.AUTH_NETWORK_ERROR : Error.NOT_AUTHORIZED);
                AbuseReport.this.onComplete(Status.ERROR, Substatus.AUTH);
            }
        };
        this.mContext = GCSP.getContext();
        this.mContainer = container;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        if (r0.isValid() != false) goto L_0x0030;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001b, code lost:
        com.motorola.sharing.util.Logger.d(TAG, "no valid local token, requesting a new one");
        com.motorola.sharing.GCSP.getAuthenticator().authenticate(r4.mAuthCallback, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0030, code lost:
        r4.mContainer.userCred.set(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003f, code lost:
        if (r4.mContainer.userCred.isEmpty() == false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0041, code lost:
        r4.mContainer.actionResult.setError(com.motorola.sharing.instrumentation.ActionResult.Error.NOT_AUTHORIZED);
        onComplete(com.motorola.sharing.ContentListener.Event.Status.ERROR, com.motorola.sharing.ReportAbuseListener.ReportAbuseEvent.Substatus.AUTH);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0052, code lost:
        startTask();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000b, code lost:
        r0 = com.motorola.sharing.GCSP.getAuthenticator().getTokenInfo();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r0 == null) goto L_0x001b;
     */
    public void send() {
        synchronized (this) {
            if (!this.mIsStarted) {
                this.mIsStarted = true;
            }
        }
    }

    /* access modifiers changed from: private */
    public void onComplete(Status status, Substatus substatus) {
        this.mContainer.onReportAbuseEvent(status, substatus, CloudProvider.getInstance().getEndpoint());
        CheckinManager.getInstance().logZapAbuse(this.mContainer);
    }

    /* access modifiers changed from: private */
    public void startTask() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                new ReportAbuseTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
            }
        });
    }
}
