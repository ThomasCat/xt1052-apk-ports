package com.motorola.zap.zz.ui;

import com.motorola.sharing.GroupListener.GroupEvent;
import com.motorola.sharing.GroupListener.GroupEvent.Substatus;
import com.motorola.zap.R;
import com.motorola.zap.provider.HistoryHandler;
import com.motorola.zap.ui.ZapUi;

public class ZzLeaveUiHandler extends BaseZzUiHandler {
    /* access modifiers changed from: protected */
    public void handleInfo(GroupEvent ge) {
        ZapUi.getInstance().showToastMessage(getString(R.string.toast_msg_zz_ended), ZapUi.getZzToastIconId(), true, true);
        cancelZzActiveNotification();
        new HistoryHandler().setLeftZz(ge.groupInfo.owner, ge.groupInfo.id, true);
    }

    /* access modifiers changed from: protected */
    public void handleError(GroupEvent ge) {
        if (ge.subStatus == Substatus.CANCELED) {
            updateZzActiveNotification();
        }
    }
}
