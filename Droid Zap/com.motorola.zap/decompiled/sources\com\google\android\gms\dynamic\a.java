package com.google.android.gms.dynamic;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.dynamic.LifecycleDelegate;
import java.util.Iterator;
import java.util.LinkedList;

public abstract class a<T extends LifecycleDelegate> {
    /* access modifiers changed from: private */
    public T Fp;
    /* access modifiers changed from: private */
    public Bundle Fq;
    /* access modifiers changed from: private */
    public LinkedList<C0011a> Fr;
    private final d<T> Fs = new d<T>() {
        public void a(T t) {
            a.this.Fp = t;
            Iterator it = a.this.Fr.iterator();
            while (it.hasNext()) {
                ((C0011a) it.next()).b(a.this.Fp);
            }
            a.this.Fr.clear();
            a.this.Fq = null;
        }
    };

    /* renamed from: com.google.android.gms.dynamic.a$a reason: collision with other inner class name */
    private interface C0011a {
        void b(LifecycleDelegate lifecycleDelegate);

        int getState();
    }

    private void a(Bundle bundle, C0011a aVar) {
        if (this.Fp != null) {
            aVar.b(this.Fp);
            return;
        }
        if (this.Fr == null) {
            this.Fr = new LinkedList<>();
        }
        this.Fr.add(aVar);
        if (bundle != null) {
            if (this.Fq == null) {
                this.Fq = (Bundle) bundle.clone();
            } else {
                this.Fq.putAll(bundle);
            }
        }
        a(this.Fs);
    }

    private void aO(int i) {
        while (!this.Fr.isEmpty() && ((C0011a) this.Fr.getLast()).getState() >= i) {
            this.Fr.removeLast();
        }
    }

    public static void b(FrameLayout frameLayout) {
        final Context context = frameLayout.getContext();
        final int isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        String b = GooglePlayServicesUtil.b(context, isGooglePlayServicesAvailable, -1);
        String b2 = GooglePlayServicesUtil.b(context, isGooglePlayServicesAvailable);
        LinearLayout linearLayout = new LinearLayout(frameLayout.getContext());
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new LayoutParams(-2, -2));
        frameLayout.addView(linearLayout);
        TextView textView = new TextView(frameLayout.getContext());
        textView.setLayoutParams(new LayoutParams(-2, -2));
        textView.setText(b);
        linearLayout.addView(textView);
        if (b2 != null) {
            Button button = new Button(context);
            button.setLayoutParams(new LayoutParams(-2, -2));
            button.setText(b2);
            linearLayout.addView(button);
            button.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    context.startActivity(GooglePlayServicesUtil.a(context, isGooglePlayServicesAvailable, -1));
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void a(FrameLayout frameLayout) {
        b(frameLayout);
    }

    /* access modifiers changed from: protected */
    public abstract void a(d<T> dVar);

    public T fj() {
        return this.Fp;
    }

    public void onCreate(final Bundle savedInstanceState) {
        a(savedInstanceState, (C0011a) new C0011a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                a.this.Fp.onCreate(savedInstanceState);
            }

            public int getState() {
                return 1;
            }
        });
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final FrameLayout frameLayout = new FrameLayout(inflater.getContext());
        final LayoutInflater layoutInflater = inflater;
        final ViewGroup viewGroup = container;
        final Bundle bundle = savedInstanceState;
        a(savedInstanceState, (C0011a) new C0011a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                frameLayout.removeAllViews();
                frameLayout.addView(a.this.Fp.onCreateView(layoutInflater, viewGroup, bundle));
            }

            public int getState() {
                return 2;
            }
        });
        if (this.Fp == null) {
            a(frameLayout);
        }
        return frameLayout;
    }

    public void onDestroy() {
        if (this.Fp != null) {
            this.Fp.onDestroy();
        } else {
            aO(1);
        }
    }

    public void onDestroyView() {
        if (this.Fp != null) {
            this.Fp.onDestroyView();
        } else {
            aO(2);
        }
    }

    public void onInflate(final Activity activity, final Bundle attrs, final Bundle savedInstanceState) {
        a(savedInstanceState, (C0011a) new C0011a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                a.this.Fp.onInflate(activity, attrs, savedInstanceState);
            }

            public int getState() {
                return 0;
            }
        });
    }

    public void onLowMemory() {
        if (this.Fp != null) {
            this.Fp.onLowMemory();
        }
    }

    public void onPause() {
        if (this.Fp != null) {
            this.Fp.onPause();
        } else {
            aO(5);
        }
    }

    public void onResume() {
        a((Bundle) null, (C0011a) new C0011a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                a.this.Fp.onResume();
            }

            public int getState() {
                return 5;
            }
        });
    }

    public void onSaveInstanceState(Bundle outState) {
        if (this.Fp != null) {
            this.Fp.onSaveInstanceState(outState);
        } else if (this.Fq != null) {
            outState.putAll(this.Fq);
        }
    }
}
