package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.ContentProviderClient;
import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.a.C0049a;
import java.util.HashMap;

public class hh {
    private final hl<hg> Lk;
    private ContentProviderClient Ll = null;
    private boolean Lm = false;
    private HashMap<LocationListener, b> Ln = new HashMap<>();
    private final Context mContext;

    private static class a extends Handler {
        private final LocationListener Lo;

        public a(LocationListener locationListener) {
            this.Lo = locationListener;
        }

        public a(LocationListener locationListener, Looper looper) {
            super(looper);
            this.Lo = locationListener;
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    this.Lo.onLocationChanged(new Location((Location) msg.obj));
                    return;
                default:
                    Log.e("LocationClientHelper", "unknown message in LocationHandler.handleMessage");
                    return;
            }
        }
    }

    private static class b extends C0049a {
        private Handler Lp;

        b(LocationListener locationListener, Looper looper) {
            this.Lp = looper == null ? new a(locationListener) : new a(locationListener, looper);
        }

        public void onLocationChanged(Location location) {
            if (this.Lp == null) {
                Log.e("LocationClientHelper", "Received a location in client after calling removeLocationUpdates.");
                return;
            }
            Message obtain = Message.obtain();
            obtain.what = 1;
            obtain.obj = location;
            this.Lp.sendMessage(obtain);
        }

        public void release() {
            this.Lp = null;
        }
    }

    public hh(Context context, hl<hg> hlVar) {
        this.mContext = context;
        this.Lk = hlVar;
    }

    public Location getLastLocation() {
        this.Lk.bm();
        try {
            return ((hg) this.Lk.eb()).aF(this.mContext.getPackageName());
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public void gl() {
        if (this.Lm) {
            setMockMode(false);
        }
    }

    public void removeAllListeners() {
        try {
            synchronized (this.Ln) {
                for (b bVar : this.Ln.values()) {
                    if (bVar != null) {
                        ((hg) this.Lk.eb()).a((com.google.android.gms.location.a) bVar);
                    }
                }
                this.Ln.clear();
            }
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public void removeLocationUpdates(PendingIntent callbackIntent) {
        this.Lk.bm();
        try {
            ((hg) this.Lk.eb()).a(callbackIntent);
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public void removeLocationUpdates(LocationListener listener) {
        this.Lk.bm();
        er.b(listener, (Object) "Invalid null listener");
        synchronized (this.Ln) {
            b bVar = (b) this.Ln.remove(listener);
            if (this.Ll != null && this.Ln.isEmpty()) {
                this.Ll.release();
                this.Ll = null;
            }
            if (bVar != null) {
                bVar.release();
                try {
                    ((hg) this.Lk.eb()).a((com.google.android.gms.location.a) bVar);
                } catch (RemoteException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }

    public void requestLocationUpdates(LocationRequest request, PendingIntent callbackIntent) {
        this.Lk.bm();
        try {
            ((hg) this.Lk.eb()).a(request, callbackIntent);
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public void requestLocationUpdates(LocationRequest request, LocationListener listener, Looper looper) {
        this.Lk.bm();
        if (looper == null) {
            er.b(Looper.myLooper(), (Object) "Can't create handler inside thread that has not called Looper.prepare()");
        }
        synchronized (this.Ln) {
            b bVar = (b) this.Ln.get(listener);
            b bVar2 = bVar == null ? new b(listener, looper) : bVar;
            this.Ln.put(listener, bVar2);
            try {
                ((hg) this.Lk.eb()).a(request, (com.google.android.gms.location.a) bVar2, this.mContext.getPackageName());
            } catch (RemoteException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    public void setMockLocation(Location mockLocation) {
        this.Lk.bm();
        try {
            ((hg) this.Lk.eb()).setMockLocation(mockLocation);
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public void setMockMode(boolean isMockMode) {
        this.Lk.bm();
        try {
            ((hg) this.Lk.eb()).setMockMode(isMockMode);
            this.Lm = isMockMode;
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }
}
