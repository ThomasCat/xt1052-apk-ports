package com.motorola.gcsp.json;

public class AvailableItem {
    public int abuseAction;
    public String from;
    public String mediaUrl;
    public String mimeType;
    public boolean settingsUpdateRequired;
    public String sharedId;
    public long size;
    public String status;
    public String thumbnail;
    public long ts;
}
