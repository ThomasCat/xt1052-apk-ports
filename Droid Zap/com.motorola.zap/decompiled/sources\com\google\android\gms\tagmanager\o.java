package com.google.android.gms.tagmanager;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.c.j;
import com.google.android.gms.internal.fl;
import com.google.android.gms.internal.fn;
import com.motorola.zap.ZapConstants;

class o extends ca<ContainerHolder> {
    private final String TM;
    /* access modifiers changed from: private */
    public long TR;
    private final TagManager TY;
    /* access modifiers changed from: private */
    public final fl Ty;
    private final d Ub;
    /* access modifiers changed from: private */
    public final cg Uc;
    private final int Ud;
    private f Ue;
    /* access modifiers changed from: private */
    public volatile n Uf;
    /* access modifiers changed from: private */
    public volatile boolean Ug;
    /* access modifiers changed from: private */
    public j Uh;
    private String Ui;
    private e Uj;
    private a Uk;
    private final Context mContext;
    private final Looper zs;

    interface a {
        boolean b(Container container);
    }

    private class b implements bg<com.google.android.gms.internal.jd.a> {
        private b() {
        }

        /* renamed from: a */
        public void i(com.google.android.gms.internal.jd.a aVar) {
            j jVar;
            if (aVar.Yc != null) {
                jVar = aVar.Yc;
            } else {
                com.google.android.gms.internal.c.f fVar = aVar.fV;
                jVar = new j();
                jVar.fV = fVar;
                jVar.fU = null;
                jVar.fW = fVar.fr;
            }
            o.this.a(jVar, aVar.Yb, true);
        }

        public void a(com.google.android.gms.tagmanager.bg.a aVar) {
            if (!o.this.Ug) {
                o.this.s(0);
            }
        }

        public void iM() {
        }
    }

    private class c implements bg<j> {
        private c() {
        }

        public void a(com.google.android.gms.tagmanager.bg.a aVar) {
            if (o.this.Uf != null) {
                o.this.a(o.this.Uf);
            } else {
                o.this.a(o.this.d(Status.zS));
            }
            o.this.s(3600000);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            return;
         */
        /* renamed from: b */
        public void i(j jVar) {
            synchronized (o.this) {
                if (jVar.fV == null) {
                    if (o.this.Uh.fV == null) {
                        bh.t("Current resource is null; network resource is also null");
                        o.this.s(3600000);
                        return;
                    }
                    jVar.fV = o.this.Uh.fV;
                }
                o.this.a(jVar, o.this.Ty.currentTimeMillis(), false);
                bh.v("setting refresh time to current time: " + o.this.TR);
                if (!o.this.iL()) {
                    o.this.a(jVar);
                }
            }
        }

        public void iM() {
        }
    }

    private class d implements com.google.android.gms.tagmanager.n.a {
        private d() {
        }

        public void bc(String str) {
            o.this.bc(str);
        }

        public String iF() {
            return o.this.iF();
        }

        public void iH() {
            if (o.this.Uc.cl()) {
                o.this.s(0);
            }
        }
    }

    interface e extends Releasable {
        void a(bg<j> bgVar);

        void bf(String str);

        void d(long j, String str);
    }

    interface f extends Releasable {
        void a(bg<com.google.android.gms.internal.jd.a> bgVar);

        void b(com.google.android.gms.internal.jd.a aVar);

        com.google.android.gms.tagmanager.cr.c bP(int i);

        void iN();
    }

    o(Context context, TagManager tagManager, Looper looper, String str, int i, f fVar, e eVar, fl flVar, cg cgVar) {
        super(looper == null ? Looper.getMainLooper() : looper);
        this.mContext = context;
        this.TY = tagManager;
        if (looper == null) {
            looper = Looper.getMainLooper();
        }
        this.zs = looper;
        this.TM = str;
        this.Ud = i;
        this.Ue = fVar;
        this.Uj = eVar;
        this.Ub = new d();
        this.Uh = new j();
        this.Ty = flVar;
        this.Uc = cgVar;
        if (iL()) {
            bc(ce.ju().jw());
        }
    }

    public o(Context context, TagManager tagManager, Looper looper, String str, int i, r rVar) {
        this(context, tagManager, looper, str, i, new cq(context, str), new cp(context, str, rVar), fn.eI(), new bf(30, 900000, 5000, "refreshing", fn.eI()));
    }

    /* access modifiers changed from: private */
    public synchronized void a(j jVar) {
        if (this.Ue != null) {
            com.google.android.gms.internal.jd.a aVar = new com.google.android.gms.internal.jd.a();
            aVar.Yb = this.TR;
            aVar.fV = new com.google.android.gms.internal.c.f();
            aVar.Yc = jVar;
            this.Ue.b(aVar);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0008, code lost:
        if (r8.Ug != false) goto L_0x000a;
     */
    public synchronized void a(j jVar, long j, boolean z) {
        if (z) {
        }
        if (!isReady() || this.Uf == null) {
        }
        this.Uh = jVar;
        this.TR = j;
        s(Math.max(0, Math.min(43200000, (this.TR + 43200000) - this.Ty.currentTimeMillis())));
        Container container = new Container(this.mContext, this.TY.getDataLayer(), this.TM, j, jVar);
        if (this.Uf == null) {
            this.Uf = new n(this.TY, this.zs, container, this.Ub);
        } else {
            this.Uf.a(container);
        }
        if (!isReady() && this.Uk.b(container)) {
            a(this.Uf);
        }
    }

    /* access modifiers changed from: private */
    public boolean iL() {
        ce ju = ce.ju();
        return (ju.jv() == a.CONTAINER || ju.jv() == a.CONTAINER_DEBUG) && this.TM.equals(ju.getContainerId());
    }

    /* access modifiers changed from: private */
    public synchronized void s(long j) {
        if (this.Uj == null) {
            bh.w("Refresh requested, but no network load scheduler.");
        } else {
            this.Uj.d(j, this.Uh.fW);
        }
    }

    private void z(final boolean z) {
        this.Ue.a(new b());
        this.Uj.a(new c());
        com.google.android.gms.tagmanager.cr.c bP = this.Ue.bP(this.Ud);
        if (bP != null) {
            this.Uf = new n(this.TY, this.zs, new Container(this.mContext, this.TY.getDataLayer(), this.TM, 0, bP), this.Ub);
        }
        this.Uk = new a() {
            public boolean b(Container container) {
                return z ? container.getLastRefreshTime() + 43200000 >= o.this.Ty.currentTimeMillis() : !container.isDefault();
            }
        };
        if (iL()) {
            this.Uj.d(0, ZapConstants.GOOGLE_ACCOUNT_NONE);
        } else {
            this.Ue.iN();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: O */
    public ContainerHolder d(Status status) {
        if (this.Uf != null) {
            return this.Uf;
        }
        if (status == Status.zS) {
            bh.t("timer expired: setting result to failure");
        }
        return new n(status);
    }

    /* access modifiers changed from: 0000 */
    public synchronized void bc(String str) {
        this.Ui = str;
        if (this.Uj != null) {
            this.Uj.bf(str);
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized String iF() {
        return this.Ui;
    }

    public void iI() {
        com.google.android.gms.tagmanager.cr.c bP = this.Ue.bP(this.Ud);
        if (bP != null) {
            a(new n(this.TY, this.zs, new Container(this.mContext, this.TY.getDataLayer(), this.TM, 0, bP), new com.google.android.gms.tagmanager.n.a() {
                public void bc(String str) {
                    o.this.bc(str);
                }

                public String iF() {
                    return o.this.iF();
                }

                public void iH() {
                    bh.w("Refresh ignored: container loaded as default only.");
                }
            }));
        } else {
            String str = "Default was requested, but no default container was found";
            bh.t(str);
            a(d(new Status(10, str, null)));
        }
        this.Uj = null;
        this.Ue = null;
    }

    public void iJ() {
        z(false);
    }

    public void iK() {
        z(true);
    }
}
