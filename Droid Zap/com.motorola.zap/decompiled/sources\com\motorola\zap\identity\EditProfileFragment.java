package com.motorola.zap.identity;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.drive.DriveFile;
import com.motorola.sharing.Profile;
import com.motorola.sharing.ProfileManager;
import com.motorola.sharing.util.FileUtils;
import com.motorola.zap.R;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.app.App;
import com.motorola.zap.cacheutils.ZapImageCache;
import com.motorola.zap.dialogs.DialogMessageActivity;
import com.motorola.zap.ui.DroidBlastLauncher;
import com.motorola.zap.ui.ZapUi;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class EditProfileFragment extends Fragment {
    /* access modifiers changed from: private */
    public static String mCurrentZapCameraFile;
    private static ImageView mProfileImage;
    private static String mUpdatedImageURL = null;
    private Button mBtnUpdatePic = null;
    private File mCacheDir;
    /* access modifiers changed from: private */
    public CheckBox mCheckAnonymous = null;
    private Profile mCurrentUserProfile = ZapProfileSharedPerfs.getProfile();
    /* access modifiers changed from: private */
    public boolean mIsPicUpdated = false;
    /* access modifiers changed from: private */
    public Button mSave = null;
    /* access modifiers changed from: private */
    public File mUpdatedFile;
    /* access modifiers changed from: private */
    public EditText mUserName = null;
    OnClickListener saveListener = new OnClickListener() {
        public void onClick(View view) {
            boolean access$400;
            if (EditProfileFragment.this.shouldUpdateProfile()) {
                DroidBlastLauncher activity = (DroidBlastLauncher) EditProfileFragment.this.getActivity();
                if (activity != null) {
                    activity.updateDrawer();
                }
                EditProfileFragment.this.getActivity().getFragmentManager().popBackStack();
                Profile lProfile = new Profile();
                lProfile.nickname = EditProfileFragment.this.mUserName.getText().toString();
                lProfile.anonymous = EditProfileFragment.this.mCheckAnonymous.isChecked();
                ZapUi ui = ZapUi.getInstance();
                if ((EditProfileFragment.this.mUpdatedFile != null) && EditProfileFragment.this.mIsPicUpdated) {
                    ui.showToastMessage(EditProfileFragment.this.getActivity().getString(R.string.profile_saving_toast), ZapUi.getZapToastIconId(), true, true);
                    ProfileManager.getInstance().updateMyProfile(lProfile, EditProfileFragment.this.mUpdatedFile, new UpdateZapProfile(EditProfileFragment.this.getActivity()));
                    return;
                }
                ui.showToastMessage(EditProfileFragment.this.getActivity().getString(R.string.profile_saving_toast), ZapUi.getZapToastIconId(), true, true);
                ProfileManager.getInstance().updateMyProfile(lProfile, new UpdateZapProfile(EditProfileFragment.this.getActivity()));
            }
        }
    };

    public static class OptionDialog extends DialogFragment {
        private static String mDialogTitle;

        public static OptionDialog newInstance(String title) {
            OptionDialog frag = new OptionDialog();
            mDialogTitle = title;
            return frag;
        }

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Builder builder = new Builder(getActivity());
            builder.setTitle(mDialogTitle).setItems(getResources().getStringArray(R.array.update_profile_options), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    switch (i) {
                        case 0:
                            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                            Uri access$600 = OptionDialog.this.getZapCameraFileUri(intent);
                            if (access$600 != null) {
                                try {
                                    intent.putExtra("output", access$600);
                                    OptionDialog.this.getTargetFragment().startActivityForResult(intent, 0);
                                    return;
                                } catch (ActivityNotFoundException e) {
                                    Intent intent2 = new Intent(OptionDialog.this.getActivity(), DialogMessageActivity.class);
                                    intent2.putExtra(ZapConstants.EXTRA_DIALOG_TITLE, OptionDialog.this.getActivity().getString(R.string.app_name));
                                    intent2.putExtra(ZapConstants.EXTRA_DIALOG_MESSAGE, OptionDialog.this.getActivity().getString(R.string.toast_msg_camera_err));
                                    intent2.setFlags(DriveFile.MODE_READ_ONLY);
                                    OptionDialog.this.startActivity(intent2);
                                    return;
                                }
                            } else if (Log.isLoggable("EditProfileFragment", 6)) {
                                Log.e("EditProfileFragment", "Could not get new file uri");
                                return;
                            } else {
                                return;
                            }
                        case 1:
                            Intent systemGalleryPickerIntent = ZapUtils.getSystemGalleryPickerIntent(App.getAppContext(), "image/*");
                            if (systemGalleryPickerIntent != null) {
                                OptionDialog.this.getTargetFragment().startActivityForResult(Intent.createChooser(systemGalleryPickerIntent, "Choose Picture"), 1);
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                }
            });
            return builder.create();
        }

        /* access modifiers changed from: private */
        public Uri getZapCameraFileUri(Intent camIntent) {
            String filePath = ZapUtils.getZapCameraFilePath("/" + ("ZAP_PROF_IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ROOT).format(new Date())) + ".jpg");
            if (filePath == null) {
                return null;
            }
            EditProfileFragment.mCurrentZapCameraFile = filePath;
            return Uri.fromFile(new File(filePath));
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mCurrentUserProfile = ZapProfileSharedPerfs.getProfile();
        if (this.mCurrentUserProfile == null) {
            this.mCurrentUserProfile = new Profile();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.cloneInContext(new ContextThemeWrapper(getActivity().getApplicationContext(), 16974105)).inflate(R.layout.edit_profile, container, false);
        this.mUserName = (EditText) rootView.findViewById(R.id.editText_profile_user_name);
        this.mCheckAnonymous = (CheckBox) rootView.findViewById(R.id.checkBox_remain_anonymous);
        this.mBtnUpdatePic = (Button) rootView.findViewById(R.id.button_update_pic);
        mProfileImage = (ImageView) rootView.findViewById(R.id.imageView_profile_image);
        this.mSave = (Button) rootView.findViewById(R.id.button_update_profile);
        ImageLoader imageLoader = ZapImageCache.getInstance().getImageLoader();
        if (this.mCurrentUserProfile != null) {
            this.mCheckAnonymous.setChecked(this.mCurrentUserProfile.anonymous);
            this.mUserName.setText(this.mCurrentUserProfile.nickname);
            if (this.mCurrentUserProfile.pictureUrl != null) {
                imageLoader.get(this.mCurrentUserProfile.pictureUrl, ImageLoader.getImageListener(mProfileImage, R.drawable.ic_zap_inviteaddphoto, R.drawable.ic_zap_inviteaddphoto));
            }
        }
        this.mBtnUpdatePic.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                OptionDialog dlg = OptionDialog.newInstance(EditProfileFragment.this.getString(R.string.update_photo_title));
                dlg.setTargetFragment(EditProfileFragment.this, 1);
                dlg.show(EditProfileFragment.this.getFragmentManager(), "EditProfileFragment");
            }
        });
        this.mCheckAnonymous.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                EditProfileFragment.this.mSave.setEnabled(EditProfileFragment.this.shouldUpdateProfile());
            }
        });
        this.mUserName.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void afterTextChanged(Editable editable) {
                EditProfileFragment.this.mSave.setEnabled(EditProfileFragment.this.shouldUpdateProfile());
            }
        });
        this.mSave.setOnClickListener(this.saveListener);
        this.mCacheDir = getActivity().getCacheDir();
        return rootView;
    }

    /* access modifiers changed from: private */
    public boolean shouldUpdateProfile() {
        if (this.mUserName.getText().length() == 0) {
            return false;
        }
        if (this.mIsPicUpdated || this.mCurrentUserProfile.anonymous != this.mCheckAnonymous.isChecked() || !TextUtils.equals(this.mUserName.getText(), this.mCurrentUserProfile.nickname)) {
            return true;
        }
        return false;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 0:
                if (resultCode == -1) {
                    Bitmap resizeBitmap = FileUtils.getThumbnail(mCurrentZapCameraFile, 135, 135);
                    saveResizedImageToTempFile(resizeBitmap);
                    mProfileImage.setImageBitmap(resizeBitmap);
                } else if (mCurrentZapCameraFile != null && new File(mCurrentZapCameraFile).exists()) {
                    FileUtils.deleteFile(mCurrentZapCameraFile, App.getAppContext());
                }
                this.mIsPicUpdated = true;
                this.mSave.setEnabled(shouldUpdateProfile());
                break;
            case 1:
                if (data != null) {
                    mUpdatedImageURL = FileUtils.getMediaPathFromUri(getActivity().getApplicationContext(), data.getData());
                    Bitmap resizeBitmap2 = FileUtils.getThumbnail(mUpdatedImageURL, 135, 135);
                    saveResizedImageToTempFile(resizeBitmap2);
                    mProfileImage.setImageBitmap(resizeBitmap2);
                    this.mIsPicUpdated = true;
                    this.mSave.setEnabled(shouldUpdateProfile());
                    break;
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void saveResizedImageToTempFile(Bitmap bitmap) {
        this.mUpdatedFile = new File(this.mCacheDir, "profile_pic.jpg");
        try {
            FileOutputStream out = new FileOutputStream(this.mUpdatedFile);
            bitmap.compress(CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }
}
