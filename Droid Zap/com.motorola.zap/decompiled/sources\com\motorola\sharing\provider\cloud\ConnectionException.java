package com.motorola.sharing.provider.cloud;

public class ConnectionException extends Exception {
    private static final long serialVersionUID = 1;
    private int mStatusCode;

    public ConnectionException(String reason, int statusCode) {
        super(reason);
        this.mStatusCode = statusCode;
    }

    public int getStatusCode() {
        return this.mStatusCode;
    }
}
