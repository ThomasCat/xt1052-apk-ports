package com.motorola.sharing.provider.tv;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.support.v4.view.MotionEventCompat;
import android.text.TextUtils;
import com.google.android.gms.plus.PlusShare;
import com.motorola.sharing.CastManager;
import com.motorola.sharing.ContentContainer;
import com.motorola.sharing.ContentContainer.DiscoveryParams;
import com.motorola.sharing.ContentDiscoveryListener;
import com.motorola.sharing.ContentDownloadListener;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.ContentShareListener;
import com.motorola.sharing.ContentShareListener.ContentShareEvent.Substatus;
import com.motorola.sharing.Endpoint;
import com.motorola.sharing.Endpoint.Attributes;
import com.motorola.sharing.EndpointDiscoveryListener;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.Locality;
import com.motorola.sharing.ProviderApi;
import com.motorola.sharing.ReportAbuseListener;
import com.motorola.sharing.Settings;
import com.motorola.sharing.chromecast.NanoHttpDServer;
import com.motorola.sharing.instrumentation.ActionResult.Error;
import com.motorola.sharing.instrumentation.CheckinInterface;
import com.motorola.sharing.instrumentation.CheckinManager;
import com.motorola.sharing.provider.tv.api.TVEndpoint;
import com.motorola.sharing.util.FileUtils;
import com.motorola.sharing.util.Logger;
import com.motorola.zap.ui.TimeLineFragment;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class TVProvider implements ProviderApi {
    private static final String TAG = Logger.getTvTag("TVProvider");
    private static Map<String, String> mMediaListUris = new HashMap();
    private static TVProvider sProvider = null;
    private CastManager mCastManager = null;
    private TVEndpoint mEndpoint = null;
    private String mIPAddress = null;
    private int mListeningPort = -1;
    private NanoHttpDServer mServer;

    public static synchronized TVProvider getInstance() {
        TVProvider tVProvider;
        synchronized (TVProvider.class) {
            if (sProvider == null) {
                sProvider = new TVProvider();
            }
            tVProvider = sProvider;
        }
        return tVProvider;
    }

    private String generateHashString() {
        return "/" + Long.toHexString(new Date().getTime());
    }

    public synchronized void startHttpServer() {
        if (this.mServer == null || !this.mServer.isAlive()) {
            Context context = GCSP.getContext();
            GCSP.getContext();
            int ipAddress = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getIpAddress();
            String formatedIpAddress = String.format("%d.%d.%d.%d", new Object[]{Integer.valueOf(ipAddress & MotionEventCompat.ACTION_MASK), Integer.valueOf((ipAddress >> 8) & MotionEventCompat.ACTION_MASK), Integer.valueOf((ipAddress >> 16) & MotionEventCompat.ACTION_MASK), Integer.valueOf((ipAddress >> 24) & MotionEventCompat.ACTION_MASK)});
            this.mIPAddress = formatedIpAddress;
            try {
                this.mServer = new NanoHttpDServer(formatedIpAddress, -1);
                this.mServer.start();
                this.mListeningPort = this.mServer.getListeningPort();
            } catch (IOException e) {
                Logger.e(TAG, "startHttpServer()", e);
            }
        }
        return;
    }

    public synchronized void stopHttpServer() {
        if (this.mServer != null && this.mServer.isAlive()) {
            this.mServer.stop();
            this.mServer = null;
            mMediaListUris.clear();
        }
    }

    public Boolean hasMediaUri(String aKey) {
        return Boolean.valueOf(mMediaListUris.containsKey(aKey));
    }

    public String getMediaUri(String aKey) {
        return (String) mMediaListUris.get(aKey);
    }

    private TVProvider() {
    }

    public void discoverContent(DiscoveryParams params, ContentDiscoveryListener listener, Endpoint[] endpoints) {
        Logger.d(TAG, "discoverContent(): " + params.getPrintableSignature());
        throw new UnsupportedOperationException("ChromeCast discovery is not supported");
    }

    public void stopDiscovery(DiscoveryParams params) {
        Logger.d(TAG, "stopDiscovery(): " + params.getPrintableSignature());
        throw new UnsupportedOperationException("ChromeCast discovery is not supported");
    }

    public void downloadContent(ContentContainer container, ContentDownloadListener listener) {
        Logger.d(TAG, "downloadContent(): " + container.getPrintableSignature());
        throw new UnsupportedOperationException("downloadContent is not supported for ChromeCast");
    }

    public void cancelDownload(ContentContainer container) {
        Logger.d(TAG, "cancelDownload(): " + container.getPrintableSignature());
        throw new UnsupportedOperationException("cancelDownload is not supported for ChromeCast");
    }

    public void reportAbuse(ContentContainer container, ReportAbuseListener listener) {
        Logger.d(TAG, "reportAbuse(): " + container.getPrintableSignature());
        throw new UnsupportedOperationException("Reporting abuse is not supported for ChromeCast");
    }

    public void shareContent(ContentContainer container, ContentShareListener listener, Endpoint[] endpoints) {
        Logger.d(TAG, "shareContent(): " + container.getPrintableSignature());
        startHttpServer();
        if (verifyChromecastDirectory()) {
            container.tmpPath = FileUtils.resize(container.path, (String) Settings.CHROMECAST_DOWNLOADS_DIRECTORY.get(), 1280, 720);
        } else {
            Logger.e(TAG, "Could not create download path");
        }
        this.mCastManager = CastManager.getInstance();
        String pathHash = generateHashString();
        if (TextUtils.isEmpty(container.tmpPath)) {
            mMediaListUris.put(pathHash, container.path);
        } else {
            mMediaListUris.put(pathHash, container.tmpPath);
        }
        container.putCheckinData(CheckinInterface.KEY_DEST, CheckinInterface.VAL_DEST_CHROMECAST);
        container.shareId = pathHash;
        try {
            String webUri = "http://" + this.mIPAddress + ":" + this.mListeningPort + pathHash;
            JSONObject jObj = new JSONObject();
            jObj.put(TimeLineFragment.TYPE, "share");
            jObj.put(PlusShare.KEY_CALL_TO_ACTION_URL, webUri);
            jObj.put("shareid", container.shareId);
            if (container.mimeType.contains("video")) {
                jObj.put("mimetype", "video");
            } else {
                jObj.put("mimetype", "image");
            }
            Locality loc = container.getLocality();
            if (!(loc == null || loc.city == null || loc.city.length() <= 0)) {
                jObj.put("location", loc.city);
            }
            if (this.mCastManager != null) {
                this.mCastManager.sendDataMessage(jObj);
            }
            container.actionResult.setSuccess();
            CheckinManager.getInstance().logZapEvent(container);
        } catch (JSONException e) {
            container.onContentShareEvent(Status.ERROR, Substatus.TRANSFER_FAILED, getInstance().getEndpoint());
            Logger.d(TAG, "shareContent(): " + e.getMessage());
            container.actionResult.setError(Error.UNKNOWN);
            CheckinManager.getInstance().logZapEvent(container);
        }
    }

    private static boolean verifyChromecastDirectory() {
        File directory = new File((String) Settings.CHROMECAST_DOWNLOADS_DIRECTORY.get());
        if (directory.exists() || directory.mkdirs()) {
            return true;
        }
        return false;
    }

    public void uploadContent(ContentContainer container, ContentShareListener listener) {
        Logger.d(TAG, "uploadContent(): " + container.getPrintableSignature());
        throw new UnsupportedOperationException("uploadContent is not supported for ChromeCast");
    }

    public void stopSharing(ContentContainer container) {
        Logger.d(TAG, "stopSharing(): " + container.getPrintableSignature());
        mMediaListUris.values().remove(container.path);
    }

    public void stopSharing(String shareId, ContentShareListener listener) {
        Logger.d(TAG, "stopSharing(): sid" + shareId);
        throw new UnsupportedOperationException("stopShare with container must be called");
    }

    public void cancelSharing(ContentContainer container) {
        throw new UnsupportedOperationException("cancelSharing is not supported for ChromeCast");
    }

    public void resumeSharing(ContentContainer container, ContentShareListener listener) {
        throw new UnsupportedOperationException("resumeSharing is not supported for ChromeCast");
    }

    public void discoverEndpoints(Attributes attrs, EndpointDiscoveryListener listener) {
        throw new UnsupportedOperationException("discoverEndpoints is not supported for ChromeCast");
    }

    public void stopEndpointDiscovery(EndpointDiscoveryListener listener) {
        throw new UnsupportedOperationException("stopEndpointDiscovery is not supported for ChromeCast");
    }

    public synchronized Endpoint getEndpoint() {
        if (this.mEndpoint == null) {
            this.mEndpoint = new TVEndpoint();
        }
        return this.mEndpoint;
    }

    public Endpoint[] getStaticEndpoints() {
        return new Endpoint[]{getEndpoint()};
    }
}
