package com.google.android.gms.internal;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaStatus;
import com.google.sample.castcompanionlibrary.cast.VideoCastManager;
import com.motorola.zap.ui.TimeLineFragment;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class dv extends dp {
    private static final long yi = TimeUnit.HOURS.toMillis(24);
    private static final long yj = TimeUnit.HOURS.toMillis(24);
    private static final long yk = TimeUnit.HOURS.toMillis(24);
    private static final long yl = TimeUnit.SECONDS.toMillis(1);
    private final Handler mHandler = new Handler(Looper.getMainLooper());
    private long ym;
    private MediaStatus yn;
    /* access modifiers changed from: private */
    public final dy yo = new dy(yj);
    /* access modifiers changed from: private */
    public final dy yp = new dy(yi);
    /* access modifiers changed from: private */
    public final dy yq = new dy(yi);
    /* access modifiers changed from: private */
    public final dy yr = new dy(yi);
    /* access modifiers changed from: private */
    public final dy ys = new dy(yk);
    /* access modifiers changed from: private */
    public final dy yt = new dy(yi);
    /* access modifiers changed from: private */
    public final dy yu = new dy(yi);
    /* access modifiers changed from: private */
    public final dy yv = new dy(yi);
    private final Runnable yw = new a();
    /* access modifiers changed from: private */
    public boolean yx;

    private class a implements Runnable {
        private a() {
        }

        public void run() {
            boolean z = false;
            dv.this.yx = false;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            dv.this.yo.d(elapsedRealtime, 3);
            dv.this.yp.d(elapsedRealtime, 3);
            dv.this.yq.d(elapsedRealtime, 3);
            dv.this.yr.d(elapsedRealtime, 3);
            dv.this.ys.d(elapsedRealtime, 3);
            dv.this.yt.d(elapsedRealtime, 3);
            dv.this.yu.d(elapsedRealtime, 3);
            dv.this.yv.d(elapsedRealtime, 3);
            synchronized (dy.yD) {
                if (dv.this.yo.dl() || dv.this.ys.dl() || dv.this.yt.dl() || dv.this.yu.dl() || dv.this.yv.dl()) {
                    z = true;
                }
            }
            dv.this.u(z);
        }
    }

    public dv() {
        super("urn:x-cast:com.google.cast.media", "MediaControlChannel");
        dj();
    }

    private void a(long j, JSONObject jSONObject) throws JSONException {
        int i;
        boolean z = true;
        boolean m = this.yo.m(j);
        boolean z2 = this.ys.dl() && !this.ys.m(j);
        if ((!this.yt.dl() || this.yt.m(j)) && (!this.yu.dl() || this.yu.m(j))) {
            z = false;
        }
        int i2 = z2 ? 2 : 0;
        if (z) {
            i2 |= 1;
        }
        if (m || this.yn == null) {
            this.yn = new MediaStatus(jSONObject);
            this.ym = SystemClock.elapsedRealtime();
            i = 7;
        } else {
            i = this.yn.a(jSONObject, i2);
        }
        if ((i & 1) != 0) {
            this.ym = SystemClock.elapsedRealtime();
            onStatusUpdated();
        }
        if ((i & 2) != 0) {
            this.ym = SystemClock.elapsedRealtime();
            onStatusUpdated();
        }
        if ((i & 4) != 0) {
            onMetadataUpdated();
        }
        this.yo.c(j, 0);
        this.yp.c(j, 0);
        this.yq.c(j, 0);
        this.yr.c(j, 0);
        this.ys.c(j, 0);
        this.yt.c(j, 0);
        this.yu.c(j, 0);
        this.yv.c(j, 0);
    }

    private void dj() {
        u(false);
        this.ym = 0;
        this.yn = null;
        this.yo.clear();
        this.ys.clear();
        this.yt.clear();
    }

    /* access modifiers changed from: private */
    public void u(boolean z) {
        if (this.yx != z) {
            this.yx = z;
            if (z) {
                this.mHandler.postDelayed(this.yw, yl);
            } else {
                this.mHandler.removeCallbacks(this.yw);
            }
        }
    }

    public final void P(String str) {
        this.xB.b("message received: %s", str);
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString(TimeLineFragment.TYPE);
            long optLong = jSONObject.optLong("requestId", -1);
            if (string.equals("MEDIA_STATUS")) {
                JSONArray jSONArray = jSONObject.getJSONArray("status");
                if (jSONArray.length() > 0) {
                    a(optLong, jSONArray.getJSONObject(0));
                    return;
                }
                this.yn = null;
                onStatusUpdated();
                onMetadataUpdated();
                this.yv.c(optLong, 0);
            } else if (string.equals("INVALID_PLAYER_STATE")) {
                this.xB.d("received unexpected error: Invalid Player State.", new Object[0]);
                JSONObject optJSONObject = jSONObject.optJSONObject(VideoCastManager.EXTRA_CUSTOM_DATA);
                this.yo.b(optLong, 1, optJSONObject);
                this.yp.b(optLong, 1, optJSONObject);
                this.yq.b(optLong, 1, optJSONObject);
                this.yr.b(optLong, 1, optJSONObject);
                this.ys.b(optLong, 1, optJSONObject);
                this.yt.b(optLong, 1, optJSONObject);
                this.yu.b(optLong, 1, optJSONObject);
                this.yv.b(optLong, 1, optJSONObject);
            } else if (string.equals("LOAD_FAILED")) {
                this.yo.b(optLong, 1, jSONObject.optJSONObject(VideoCastManager.EXTRA_CUSTOM_DATA));
            } else if (string.equals("LOAD_CANCELLED")) {
                this.yo.b(optLong, 2, jSONObject.optJSONObject(VideoCastManager.EXTRA_CUSTOM_DATA));
            } else if (string.equals("INVALID_REQUEST")) {
                this.xB.d("received unexpected error: Invalid Request.", new Object[0]);
                JSONObject optJSONObject2 = jSONObject.optJSONObject(VideoCastManager.EXTRA_CUSTOM_DATA);
                this.yo.b(optLong, 1, optJSONObject2);
                this.yp.b(optLong, 1, optJSONObject2);
                this.yq.b(optLong, 1, optJSONObject2);
                this.yr.b(optLong, 1, optJSONObject2);
                this.ys.b(optLong, 1, optJSONObject2);
                this.yt.b(optLong, 1, optJSONObject2);
                this.yu.b(optLong, 1, optJSONObject2);
                this.yv.b(optLong, 1, optJSONObject2);
            }
        } catch (JSONException e) {
            this.xB.d("Message is malformed (%s); ignoring: %s", e.getMessage(), str);
        }
    }

    public long a(dx dxVar) throws IOException {
        JSONObject jSONObject = new JSONObject();
        long cW = cW();
        this.yv.a(cW, dxVar);
        u(true);
        try {
            jSONObject.put("requestId", cW);
            jSONObject.put(TimeLineFragment.TYPE, "GET_STATUS");
            if (this.yn != null) {
                jSONObject.put("mediaSessionId", this.yn.cU());
            }
        } catch (JSONException e) {
        }
        a(jSONObject.toString(), cW, null);
        return cW;
    }

    public long a(dx dxVar, double d, JSONObject jSONObject) throws IOException, IllegalStateException, IllegalArgumentException {
        if (Double.isInfinite(d) || Double.isNaN(d)) {
            throw new IllegalArgumentException("Volume cannot be " + d);
        }
        JSONObject jSONObject2 = new JSONObject();
        long cW = cW();
        this.yt.a(cW, dxVar);
        u(true);
        try {
            jSONObject2.put("requestId", cW);
            jSONObject2.put(TimeLineFragment.TYPE, "SET_VOLUME");
            jSONObject2.put("mediaSessionId", cU());
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("level", d);
            jSONObject2.put("volume", jSONObject3);
            if (jSONObject != null) {
                jSONObject2.put(VideoCastManager.EXTRA_CUSTOM_DATA, jSONObject);
            }
        } catch (JSONException e) {
        }
        a(jSONObject2.toString(), cW, null);
        return cW;
    }

    public long a(dx dxVar, long j, int i, JSONObject jSONObject) throws IOException, IllegalStateException {
        JSONObject jSONObject2 = new JSONObject();
        long cW = cW();
        this.ys.a(cW, dxVar);
        u(true);
        try {
            jSONObject2.put("requestId", cW);
            jSONObject2.put(TimeLineFragment.TYPE, "SEEK");
            jSONObject2.put("mediaSessionId", cU());
            jSONObject2.put("currentTime", dr.l(j));
            if (i == 1) {
                jSONObject2.put("resumeState", "PLAYBACK_START");
            } else if (i == 2) {
                jSONObject2.put("resumeState", "PLAYBACK_PAUSE");
            }
            if (jSONObject != null) {
                jSONObject2.put(VideoCastManager.EXTRA_CUSTOM_DATA, jSONObject);
            }
        } catch (JSONException e) {
        }
        a(jSONObject2.toString(), cW, null);
        return cW;
    }

    public long a(dx dxVar, MediaInfo mediaInfo, boolean z, long j, JSONObject jSONObject) throws IOException {
        JSONObject jSONObject2 = new JSONObject();
        long cW = cW();
        this.yo.a(cW, dxVar);
        u(true);
        try {
            jSONObject2.put("requestId", cW);
            jSONObject2.put(TimeLineFragment.TYPE, "LOAD");
            jSONObject2.put(VideoCastManager.EXTRA_MEDIA, mediaInfo.cT());
            jSONObject2.put("autoplay", z);
            jSONObject2.put("currentTime", dr.l(j));
            if (jSONObject != null) {
                jSONObject2.put(VideoCastManager.EXTRA_CUSTOM_DATA, jSONObject);
            }
        } catch (JSONException e) {
        }
        a(jSONObject2.toString(), cW, null);
        return cW;
    }

    public long a(dx dxVar, JSONObject jSONObject) throws IOException {
        JSONObject jSONObject2 = new JSONObject();
        long cW = cW();
        this.yp.a(cW, dxVar);
        u(true);
        try {
            jSONObject2.put("requestId", cW);
            jSONObject2.put(TimeLineFragment.TYPE, "PAUSE");
            jSONObject2.put("mediaSessionId", cU());
            if (jSONObject != null) {
                jSONObject2.put(VideoCastManager.EXTRA_CUSTOM_DATA, jSONObject);
            }
        } catch (JSONException e) {
        }
        a(jSONObject2.toString(), cW, null);
        return cW;
    }

    public long a(dx dxVar, boolean z, JSONObject jSONObject) throws IOException, IllegalStateException {
        JSONObject jSONObject2 = new JSONObject();
        long cW = cW();
        this.yu.a(cW, dxVar);
        u(true);
        try {
            jSONObject2.put("requestId", cW);
            jSONObject2.put(TimeLineFragment.TYPE, "SET_VOLUME");
            jSONObject2.put("mediaSessionId", cU());
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("muted", z);
            jSONObject2.put("volume", jSONObject3);
            if (jSONObject != null) {
                jSONObject2.put(VideoCastManager.EXTRA_CUSTOM_DATA, jSONObject);
            }
        } catch (JSONException e) {
        }
        a(jSONObject2.toString(), cW, null);
        return cW;
    }

    public void a(long j, int i) {
        this.yo.c(j, i);
        this.yp.c(j, i);
        this.yq.c(j, i);
        this.yr.c(j, i);
        this.ys.c(j, i);
        this.yt.c(j, i);
        this.yu.c(j, i);
        this.yv.c(j, i);
    }

    public long b(dx dxVar, JSONObject jSONObject) throws IOException {
        JSONObject jSONObject2 = new JSONObject();
        long cW = cW();
        this.yr.a(cW, dxVar);
        u(true);
        try {
            jSONObject2.put("requestId", cW);
            jSONObject2.put(TimeLineFragment.TYPE, "STOP");
            jSONObject2.put("mediaSessionId", cU());
            if (jSONObject != null) {
                jSONObject2.put(VideoCastManager.EXTRA_CUSTOM_DATA, jSONObject);
            }
        } catch (JSONException e) {
        }
        a(jSONObject2.toString(), cW, null);
        return cW;
    }

    public long c(dx dxVar, JSONObject jSONObject) throws IOException, IllegalStateException {
        JSONObject jSONObject2 = new JSONObject();
        long cW = cW();
        this.yq.a(cW, dxVar);
        u(true);
        try {
            jSONObject2.put("requestId", cW);
            jSONObject2.put(TimeLineFragment.TYPE, "PLAY");
            jSONObject2.put("mediaSessionId", cU());
            if (jSONObject != null) {
                jSONObject2.put(VideoCastManager.EXTRA_CUSTOM_DATA, jSONObject);
            }
        } catch (JSONException e) {
        }
        a(jSONObject2.toString(), cW, null);
        return cW;
    }

    public long cU() throws IllegalStateException {
        if (this.yn != null) {
            return this.yn.cU();
        }
        throw new IllegalStateException("No current media session");
    }

    public void cX() {
        dj();
    }

    public long getApproximateStreamPosition() {
        MediaInfo mediaInfo = getMediaInfo();
        if (mediaInfo == null || this.ym == 0) {
            return 0;
        }
        double playbackRate = this.yn.getPlaybackRate();
        long streamPosition = this.yn.getStreamPosition();
        int playerState = this.yn.getPlayerState();
        if (playbackRate == 0.0d || playerState != 2) {
            return streamPosition;
        }
        long elapsedRealtime = SystemClock.elapsedRealtime() - this.ym;
        long j = elapsedRealtime < 0 ? 0 : elapsedRealtime;
        if (j == 0) {
            return streamPosition;
        }
        long streamDuration = mediaInfo.getStreamDuration();
        long j2 = streamPosition + ((long) (((double) j) * playbackRate));
        if (j2 <= streamDuration) {
            streamDuration = j2 < 0 ? 0 : j2;
        }
        return streamDuration;
    }

    public MediaInfo getMediaInfo() {
        if (this.yn == null) {
            return null;
        }
        return this.yn.getMediaInfo();
    }

    public MediaStatus getMediaStatus() {
        return this.yn;
    }

    public long getStreamDuration() {
        MediaInfo mediaInfo = getMediaInfo();
        if (mediaInfo != null) {
            return mediaInfo.getStreamDuration();
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onMetadataUpdated() {
    }

    /* access modifiers changed from: protected */
    public void onStatusUpdated() {
    }
}
