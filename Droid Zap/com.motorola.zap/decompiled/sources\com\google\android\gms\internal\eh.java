package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.data.DataHolder;
import java.util.ArrayList;

public abstract class eh<T extends IInterface> implements GooglePlayServicesClient, com.google.android.gms.common.api.Api.a, com.google.android.gms.internal.ei.b {
    public static final String[] BB = {"service_esmobile", "service_googleme"};
    boolean BA;
    /* access modifiers changed from: private */
    public T Bv;
    /* access modifiers changed from: private */
    public final ArrayList<b<?>> Bw;
    /* access modifiers changed from: private */
    public f Bx;
    /* access modifiers changed from: private */
    public volatile int By;
    private final String[] Bz;
    /* access modifiers changed from: private */
    public final Context mContext;
    final Handler mHandler;
    private final Looper zs;
    /* access modifiers changed from: private */
    public final ei zx;

    final class a extends Handler {
        public a(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            if (msg.what == 1 && !eh.this.isConnecting()) {
                b bVar = (b) msg.obj;
                bVar.cP();
                bVar.unregister();
            } else if (msg.what == 3) {
                eh.this.zx.a(new ConnectionResult(((Integer) msg.obj).intValue(), null));
            } else if (msg.what == 4) {
                eh.this.By = 1;
                eh.this.Bv = null;
                eh.this.zx.P(((Integer) msg.obj).intValue());
            } else if (msg.what == 2 && !eh.this.isConnected()) {
                b bVar2 = (b) msg.obj;
                bVar2.cP();
                bVar2.unregister();
            } else if (msg.what == 2 || msg.what == 1) {
                ((b) msg.obj).ec();
            } else {
                Log.wtf("GmsClient", "Don't know how to handle this message.");
            }
        }
    }

    protected abstract class b<TListener> {
        private boolean BD = false;
        private TListener mListener;

        public b(TListener tlistener) {
            this.mListener = tlistener;
        }

        /* access modifiers changed from: protected */
        public abstract void a(TListener tlistener);

        /* access modifiers changed from: protected */
        public abstract void cP();

        public void ec() {
            TListener tlistener;
            synchronized (this) {
                tlistener = this.mListener;
                if (this.BD) {
                    Log.w("GmsClient", "Callback proxy " + this + " being reused. This is not safe.");
                }
            }
            if (tlistener != null) {
                try {
                    a(tlistener);
                } catch (RuntimeException e) {
                    cP();
                    throw e;
                }
            } else {
                cP();
            }
            synchronized (this) {
                this.BD = true;
            }
            unregister();
        }

        public void ed() {
            synchronized (this) {
                this.mListener = null;
            }
        }

        public void unregister() {
            ed();
            synchronized (eh.this.Bw) {
                eh.this.Bw.remove(this);
            }
        }
    }

    public static final class c implements ConnectionCallbacks {
        private final GooglePlayServicesClient.ConnectionCallbacks BE;

        public c(GooglePlayServicesClient.ConnectionCallbacks connectionCallbacks) {
            this.BE = connectionCallbacks;
        }

        public boolean equals(Object other) {
            return other instanceof c ? this.BE.equals(((c) other).BE) : this.BE.equals(other);
        }

        public void onConnected(Bundle connectionHint) {
            this.BE.onConnected(connectionHint);
        }

        public void onConnectionSuspended(int cause) {
            this.BE.onDisconnected();
        }
    }

    public abstract class d<TListener> extends b<TListener> {
        private final DataHolder zU;

        public d(TListener tlistener, DataHolder dataHolder) {
            super(tlistener);
            this.zU = dataHolder;
        }

        /* access modifiers changed from: protected */
        public final void a(TListener tlistener) {
            a(tlistener, this.zU);
        }

        /* access modifiers changed from: protected */
        public abstract void a(TListener tlistener, DataHolder dataHolder);

        /* access modifiers changed from: protected */
        public void cP() {
            if (this.zU != null) {
                this.zU.close();
            }
        }

        public /* bridge */ /* synthetic */ void ec() {
            super.ec();
        }

        public /* bridge */ /* synthetic */ void ed() {
            super.ed();
        }

        public /* bridge */ /* synthetic */ void unregister() {
            super.unregister();
        }
    }

    public static final class e extends com.google.android.gms.internal.em.a {
        private eh BF;

        public e(eh ehVar) {
            this.BF = ehVar;
        }

        public void b(int i, IBinder iBinder, Bundle bundle) {
            er.b("onPostInitComplete can be called only once per call to getServiceFromBroker", (Object) this.BF);
            this.BF.a(i, iBinder, bundle);
            this.BF = null;
        }
    }

    final class f implements ServiceConnection {
        f() {
        }

        public void onServiceConnected(ComponentName component, IBinder binder) {
            eh.this.x(binder);
        }

        public void onServiceDisconnected(ComponentName component) {
            eh.this.mHandler.sendMessage(eh.this.mHandler.obtainMessage(4, Integer.valueOf(1)));
        }
    }

    public static final class g implements OnConnectionFailedListener {
        private final GooglePlayServicesClient.OnConnectionFailedListener BG;

        public g(GooglePlayServicesClient.OnConnectionFailedListener onConnectionFailedListener) {
            this.BG = onConnectionFailedListener;
        }

        public boolean equals(Object other) {
            return other instanceof g ? this.BG.equals(((g) other).BG) : this.BG.equals(other);
        }

        public void onConnectionFailed(ConnectionResult result) {
            this.BG.onConnectionFailed(result);
        }
    }

    protected final class h extends b<Boolean> {
        public final Bundle BH;
        public final IBinder BI;
        public final int statusCode;

        public h(int i, IBinder iBinder, Bundle bundle) {
            super(Boolean.valueOf(true));
            this.statusCode = i;
            this.BI = iBinder;
            this.BH = bundle;
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void a(Boolean bool) {
            if (bool == null) {
                eh.this.By = 1;
                return;
            }
            switch (this.statusCode) {
                case 0:
                    try {
                        if (eh.this.aG().equals(this.BI.getInterfaceDescriptor())) {
                            eh.this.Bv = eh.this.p(this.BI);
                            if (eh.this.Bv != null) {
                                eh.this.By = 3;
                                eh.this.zx.bo();
                                return;
                            }
                        }
                    } catch (RemoteException e) {
                    }
                    ej.y(eh.this.mContext).b(eh.this.aF(), eh.this.Bx);
                    eh.this.Bx = null;
                    eh.this.By = 1;
                    eh.this.Bv = null;
                    eh.this.zx.a(new ConnectionResult(8, null));
                    return;
                case 10:
                    eh.this.By = 1;
                    throw new IllegalStateException("A fatal developer error has occurred. Check the logs for further information.");
                default:
                    PendingIntent pendingIntent = this.BH != null ? (PendingIntent) this.BH.getParcelable("pendingIntent") : null;
                    if (eh.this.Bx != null) {
                        ej.y(eh.this.mContext).b(eh.this.aF(), eh.this.Bx);
                        eh.this.Bx = null;
                    }
                    eh.this.By = 1;
                    eh.this.Bv = null;
                    eh.this.zx.a(new ConnectionResult(this.statusCode, pendingIntent));
                    return;
            }
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }
    }

    protected eh(Context context, Looper looper, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, String... strArr) {
        this.Bw = new ArrayList<>();
        this.By = 1;
        this.BA = false;
        this.mContext = (Context) er.f(context);
        this.zs = (Looper) er.b(looper, (Object) "Looper must not be null");
        this.zx = new ei(context, looper, this);
        this.mHandler = new a(looper);
        b(strArr);
        this.Bz = strArr;
        registerConnectionCallbacks((ConnectionCallbacks) er.f(connectionCallbacks));
        registerConnectionFailedListener((OnConnectionFailedListener) er.f(onConnectionFailedListener));
    }

    protected eh(Context context, GooglePlayServicesClient.ConnectionCallbacks connectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener onConnectionFailedListener, String... strArr) {
        this(context, context.getMainLooper(), new c(connectionCallbacks), new g(onConnectionFailedListener), strArr);
    }

    public void O(int i) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(4, Integer.valueOf(i)));
    }

    /* access modifiers changed from: protected */
    public void a(int i, IBinder iBinder, Bundle bundle) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(1, new h(i, iBinder, bundle)));
    }

    public final void a(b<?> bVar) {
        synchronized (this.Bw) {
            this.Bw.add(bVar);
        }
        this.mHandler.sendMessage(this.mHandler.obtainMessage(2, bVar));
    }

    /* access modifiers changed from: protected */
    public abstract void a(en enVar, e eVar) throws RemoteException;

    /* access modifiers changed from: protected */
    public abstract String aF();

    /* access modifiers changed from: protected */
    public abstract String aG();

    /* access modifiers changed from: protected */
    public void b(String... strArr) {
    }

    /* access modifiers changed from: protected */
    public final void bm() {
        if (!isConnected()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    public Bundle cY() {
        return null;
    }

    public void connect() {
        this.BA = true;
        this.By = 2;
        int isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this.mContext);
        if (isGooglePlayServicesAvailable != 0) {
            this.By = 1;
            this.mHandler.sendMessage(this.mHandler.obtainMessage(3, Integer.valueOf(isGooglePlayServicesAvailable)));
            return;
        }
        if (this.Bx != null) {
            Log.e("GmsClient", "Calling connect() while still connected, missing disconnect().");
            this.Bv = null;
            ej.y(this.mContext).b(aF(), this.Bx);
        }
        this.Bx = new f<>();
        if (!ej.y(this.mContext).a(aF(), this.Bx)) {
            Log.e("GmsClient", "unable to connect to service: " + aF());
            this.mHandler.sendMessage(this.mHandler.obtainMessage(3, Integer.valueOf(9)));
        }
    }

    public boolean dC() {
        return this.BA;
    }

    public void disconnect() {
        this.BA = false;
        synchronized (this.Bw) {
            int size = this.Bw.size();
            for (int i = 0; i < size; i++) {
                ((b) this.Bw.get(i)).ed();
            }
            this.Bw.clear();
        }
        this.By = 1;
        this.Bv = null;
        if (this.Bx != null) {
            ej.y(this.mContext).b(aF(), this.Bx);
            this.Bx = null;
        }
    }

    public final String[] ea() {
        return this.Bz;
    }

    /* access modifiers changed from: protected */
    public final T eb() {
        bm();
        return this.Bv;
    }

    public final Context getContext() {
        return this.mContext;
    }

    public final Looper getLooper() {
        return this.zs;
    }

    public boolean isConnected() {
        return this.By == 3;
    }

    public boolean isConnecting() {
        return this.By == 2;
    }

    public boolean isConnectionCallbacksRegistered(GooglePlayServicesClient.ConnectionCallbacks listener) {
        return this.zx.isConnectionCallbacksRegistered(new c(listener));
    }

    public boolean isConnectionFailedListenerRegistered(GooglePlayServicesClient.OnConnectionFailedListener listener) {
        return this.zx.isConnectionFailedListenerRegistered(listener);
    }

    /* access modifiers changed from: protected */
    public abstract T p(IBinder iBinder);

    public void registerConnectionCallbacks(GooglePlayServicesClient.ConnectionCallbacks listener) {
        this.zx.registerConnectionCallbacks(new c(listener));
    }

    public void registerConnectionCallbacks(ConnectionCallbacks listener) {
        this.zx.registerConnectionCallbacks(listener);
    }

    public void registerConnectionFailedListener(GooglePlayServicesClient.OnConnectionFailedListener listener) {
        this.zx.registerConnectionFailedListener(listener);
    }

    public void registerConnectionFailedListener(OnConnectionFailedListener listener) {
        this.zx.registerConnectionFailedListener(listener);
    }

    public void unregisterConnectionCallbacks(GooglePlayServicesClient.ConnectionCallbacks listener) {
        this.zx.unregisterConnectionCallbacks(new c(listener));
    }

    public void unregisterConnectionFailedListener(GooglePlayServicesClient.OnConnectionFailedListener listener) {
        this.zx.unregisterConnectionFailedListener(listener);
    }

    /* access modifiers changed from: protected */
    public final void x(IBinder iBinder) {
        try {
            a(com.google.android.gms.internal.en.a.z(iBinder), new e(this));
        } catch (RemoteException e2) {
            Log.w("GmsClient", "service died");
        }
    }
}
