package com.google.sample.castcompanionlibrary.cast.player;

public enum MediaAuthStatus {
    NOT_STARTED,
    PENDING,
    FINISHED,
    RESULT_AUTHORIZED,
    RESULT_NOT_AUTHORIZED,
    ABORT_TIMEOUT,
    ABORT_USER_CANCELLED,
    ABORT_UNKNOWN
}
