package com.motorola.zap;

import android.content.SharedPreferences.Editor;
import com.motorola.sharing.GCSP;
import java.util.HashMap;
import java.util.Map;

public class ZapSharedPreference<T> {
    public static final SettingHandler<Boolean> booleanHandler = new SettingHandler<Boolean>() {
        public synchronized Boolean read(ZapSharedPreference<Boolean> s) {
            return Boolean.valueOf(GCSP.getContext().getSharedPreferences(ZapConstants.PREFERENCES_FILE_NAME, 0).getBoolean(s.name, ((Boolean) s.defaultValue).booleanValue()));
        }

        public synchronized void write(ZapSharedPreference<Boolean> s, Boolean value) {
            Editor editor = GCSP.getContext().getSharedPreferences(ZapConstants.PREFERENCES_FILE_NAME, 0).edit();
            editor.putBoolean(s.name, value.booleanValue());
            editor.apply();
        }

        public Boolean valueOf(String value) {
            return Boolean.valueOf(value);
        }
    };
    public static final SettingHandler<Float> floatHandler = new SettingHandler<Float>() {
        public synchronized Float read(ZapSharedPreference<Float> s) {
            return Float.valueOf(GCSP.getContext().getSharedPreferences(ZapConstants.PREFERENCES_FILE_NAME, 0).getFloat(s.name, ((Float) s.defaultValue).floatValue()));
        }

        public synchronized void write(ZapSharedPreference<Float> s, Float value) {
            Editor editor = GCSP.getContext().getSharedPreferences(ZapConstants.PREFERENCES_FILE_NAME, 0).edit();
            editor.putFloat(s.name, value.floatValue());
            editor.apply();
        }

        public Float valueOf(String value) {
            return Float.valueOf(value);
        }
    };
    public static final SettingHandler<Integer> integerHandler = new SettingHandler<Integer>() {
        public synchronized Integer read(ZapSharedPreference<Integer> s) {
            return Integer.valueOf(GCSP.getContext().getSharedPreferences(ZapConstants.PREFERENCES_FILE_NAME, 0).getInt(s.name, ((Integer) s.defaultValue).intValue()));
        }

        public synchronized void write(ZapSharedPreference<Integer> s, Integer value) {
            Editor editor = GCSP.getContext().getSharedPreferences(ZapConstants.PREFERENCES_FILE_NAME, 0).edit();
            editor.putInt(s.name, value.intValue());
            editor.apply();
        }

        public Integer valueOf(String value) {
            return Integer.valueOf(value);
        }
    };
    public static final SettingHandler<Long> longHandler = new SettingHandler<Long>() {
        public synchronized Long read(ZapSharedPreference<Long> s) {
            return Long.valueOf(GCSP.getContext().getSharedPreferences(ZapConstants.PREFERENCES_FILE_NAME, 0).getLong(s.name, ((Long) s.defaultValue).longValue()));
        }

        public synchronized void write(ZapSharedPreference<Long> s, Long value) {
            Editor editor = GCSP.getContext().getSharedPreferences(ZapConstants.PREFERENCES_FILE_NAME, 0).edit();
            editor.putLong(s.name, value.longValue());
            editor.apply();
        }

        public Long valueOf(String value) {
            return Long.valueOf(value);
        }
    };
    private static Map<String, ZapSharedPreference<? extends Object>> sNameMap = new HashMap();
    public static final SettingHandler<String> stringHandler = new SettingHandler<String>() {
        public synchronized String read(ZapSharedPreference<String> s) {
            return GCSP.getContext().getSharedPreferences(ZapConstants.PREFERENCES_FILE_NAME, 0).getString(s.name, (String) s.defaultValue);
        }

        public synchronized void write(ZapSharedPreference<String> s, String value) {
            Editor editor = GCSP.getContext().getSharedPreferences(ZapConstants.PREFERENCES_FILE_NAME, 0).edit();
            editor.putString(s.name, value);
            editor.apply();
        }

        public String valueOf(String value) {
            return value;
        }
    };
    public T defaultValue = null;
    public SettingHandler<T> handler = null;
    public String name = null;

    private interface SettingHandler<T> {
        T read(ZapSharedPreference<T> zapSharedPreference);

        T valueOf(String str);

        void write(ZapSharedPreference<T> zapSharedPreference, T t);
    }

    public ZapSharedPreference(String name2, T defaultValue2, SettingHandler<T> handler2) {
        if (name2 != null) {
            this.name = name2;
            this.defaultValue = defaultValue2;
            this.handler = handler2;
            sNameMap.put(name2.toLowerCase(), this);
        }
    }

    public T read() {
        return this.handler.read(this);
    }

    public void write(T value) {
        this.handler.write(this, value);
    }

    public T valueOf(String value) {
        return this.handler.valueOf(value);
    }

    public static ZapSharedPreference<? extends Object> forName(String name2) {
        if (name2 == null) {
            return null;
        }
        return (ZapSharedPreference) sNameMap.get(name2.toLowerCase());
    }
}
