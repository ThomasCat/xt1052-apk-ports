package com.motorola.sharing;

import com.motorola.sharing.Abuse.AbuseAction;
import com.motorola.sharing.ContentListener.Event;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.GroupManager.GroupInfo;
import com.motorola.sharing.util.Logger;

public interface ContentDiscoveryListener extends ContentListener {

    public static class ContentDiscoveryEvent extends Event {
        protected static final String TAG = Logger.getGcspTag("ContDiscEvent");
        public AbuseAction abuseAction = AbuseAction.NONE;
        public ContentContainer container;
        public Endpoint endpoint;
        public GroupInfo groupInfo;
        public Substatus substatus;

        public enum Substatus {
            CONTENT_FOUND,
            LOCKED_CONTENT_FOUND,
            GROUP_FOUND,
            ABUSE,
            AUTH,
            GENERAL,
            NETWORK,
            INVALID_METADATA,
            CANCELLED,
            ENDED
        }

        public ContentDiscoveryEvent(Status status, Substatus substatus2, ContentContainer container2, Endpoint endpoint2) {
            this.status = status;
            this.substatus = substatus2;
            this.container = container2;
            this.endpoint = endpoint2;
        }
    }

    void onContentDiscoveryEvent(ContentDiscoveryEvent contentDiscoveryEvent);
}
