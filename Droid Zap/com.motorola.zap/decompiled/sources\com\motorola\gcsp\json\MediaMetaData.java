package com.motorola.gcsp.json;

public class MediaMetaData implements InputSet {
    public String mimeType;
    public int size;
    public String thumbnail;
}
