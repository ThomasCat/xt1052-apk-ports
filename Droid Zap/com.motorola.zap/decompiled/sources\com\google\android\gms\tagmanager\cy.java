package com.google.android.gms.tagmanager;

import android.content.Context;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;

class cy extends cx {
    private static cy Xp;
    /* access modifiers changed from: private */
    public static final Object ri = new Object();
    private Context Xf;
    /* access modifiers changed from: private */
    public at Xg;
    private volatile ar Xh;
    /* access modifiers changed from: private */
    public int Xi = 1800000;
    private boolean Xj = true;
    private boolean Xk = false;
    private boolean Xl = true;
    private au Xm = new au() {
        public void p(boolean z) {
            cy.this.a(z, cy.this.connected);
        }
    };
    private bn Xn;
    /* access modifiers changed from: private */
    public boolean Xo = false;
    /* access modifiers changed from: private */
    public boolean connected = true;
    /* access modifiers changed from: private */
    public Handler handler;

    private cy() {
    }

    private void bC() {
        this.Xn = new bn(this);
        this.Xn.o(this.Xf);
    }

    private void bD() {
        this.handler = new Handler(this.Xf.getMainLooper(), new Callback() {
            public boolean handleMessage(Message msg) {
                if (1 == msg.what && cy.ri.equals(msg.obj)) {
                    cy.this.bp();
                    if (cy.this.Xi > 0 && !cy.this.Xo) {
                        cy.this.handler.sendMessageDelayed(cy.this.handler.obtainMessage(1, cy.ri), (long) cy.this.Xi);
                    }
                }
                return true;
            }
        });
        if (this.Xi > 0) {
            this.handler.sendMessageDelayed(this.handler.obtainMessage(1, ri), (long) this.Xi);
        }
    }

    public static cy kh() {
        if (Xp == null) {
            Xp = new cy();
        }
        return Xp;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(Context context, ar arVar) {
        if (this.Xf == null) {
            this.Xf = context.getApplicationContext();
            if (this.Xh == null) {
                this.Xh = arVar;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(boolean z, boolean z2) {
        if (!(this.Xo == z && this.connected == z2)) {
            if (z || !z2) {
                if (this.Xi > 0) {
                    this.handler.removeMessages(1, ri);
                }
            }
            if (!z && z2 && this.Xi > 0) {
                this.handler.sendMessageDelayed(this.handler.obtainMessage(1, ri), (long) this.Xi);
            }
            bh.v("PowerSaveMode " + ((z || !z2) ? "initiated." : "terminated."));
            this.Xo = z;
            this.connected = z2;
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void bF() {
        if (!this.Xo && this.connected && this.Xi > 0) {
            this.handler.removeMessages(1, ri);
            this.handler.sendMessage(this.handler.obtainMessage(1, ri));
        }
    }

    public synchronized void bp() {
        if (!this.Xk) {
            bh.v("Dispatch call queued. Dispatch will run once initialization is complete.");
            this.Xj = true;
        } else {
            this.Xh.a(new Runnable() {
                public void run() {
                    cy.this.Xg.bp();
                }
            });
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized at ki() {
        if (this.Xg == null) {
            if (this.Xf == null) {
                throw new IllegalStateException("Cant get a store unless we have a context");
            }
            this.Xg = new cb(this.Xm, this.Xf);
        }
        if (this.handler == null) {
            bD();
        }
        this.Xk = true;
        if (this.Xj) {
            bp();
            this.Xj = false;
        }
        if (this.Xn == null && this.Xl) {
            bC();
        }
        return this.Xg;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void q(boolean z) {
        a(this.Xo, z);
    }
}
