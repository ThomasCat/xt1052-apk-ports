package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.d;
import java.util.Map;

class cc extends aj {
    private static final String ID = a.PLATFORM.toString();
    private static final d.a VP = di.r("Android");

    public cc() {
        super(ID, new String[0]);
    }

    public boolean iy() {
        return true;
    }

    public d.a u(Map<String, d.a> map) {
        return VP;
    }
}
