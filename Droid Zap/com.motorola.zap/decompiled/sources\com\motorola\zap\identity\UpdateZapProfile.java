package com.motorola.zap.identity;

import android.content.Context;
import com.motorola.sharing.ProfileManagerListener;
import com.motorola.sharing.ProfileManagerListener.ProfileEvent;
import com.motorola.sharing.ProfileManagerListener.ProfileEvent.Status;
import com.motorola.zap.R;
import com.motorola.zap.cacheutils.ZapImageCache;
import com.motorola.zap.provider.HistoryHandler;
import com.motorola.zap.ui.DroidBlastLauncher;
import com.motorola.zap.ui.ZapUi;

public class UpdateZapProfile implements ProfileManagerListener {
    Context mContext;
    private HistoryHandler mHistoryHandler = new HistoryHandler();

    public UpdateZapProfile(Context aContext) {
        this.mContext = aContext;
    }

    public void onProfileEvent(ProfileEvent event) {
        if (event.status == Status.SUCCESS) {
            ZapProfileSharedPerfs.ZAP_PROFILE_NAME.write(event.profile.nickname);
            ZapProfileSharedPerfs.ZAP_PROFILE_ANONYMOUS.write(Boolean.valueOf(event.profile.anonymous));
            ZapProfileSharedPerfs.ZAP_PROFILE_PIC_URL.write(event.profile.pictureUrl);
            ZapImageCache.getInstance().clearCache();
            DroidBlastLauncher activity = (DroidBlastLauncher) this.mContext;
            this.mHistoryHandler.updateProfileForZappedCards(event.profile);
            if (activity != null) {
                activity.updateDrawer();
            }
            ZapUi.getInstance().showToastMessage(this.mContext.getString(R.string.profile_updated_toast), ZapUi.getZapToastIconId(), true, true);
        }
    }
}
