package com.motorola.zap.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ActionBar;
import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.motorola.sharing.util.FileUtils;
import com.motorola.zap.R;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.ZapUtils;
import com.motorola.zap.app.ActionReceiver;
import com.motorola.zap.provider.HistoryHandler;
import com.motorola.zap.provider.TCH;
import com.motorola.zap.provider.ZapContract.History;
import com.motorola.zap.provider.ZapContract.History.Columns;
import com.motorola.zap.provider.ZapContract.History.ZapZoneDetail;
import com.motorola.zap.ui.ZapGestureTouchListener.AnimationCallback;
import com.motorola.zap.ui.ZapGestureTouchListener.OnAnimationEndListener;
import com.motorola.zap.ui.ZapGestureTouchListener.ZapGestureListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ImageViewActivity extends Activity implements LoaderCallbacks<Cursor> {
    public static final String IS_ZZ_FLODER_VIEW = "zz_view";
    protected static final String TAG = ImageViewActivity.class.getSimpleName();
    public static final String ZZ_ID = "zz_id";
    public static final String ZZ_ITEM_POSITION = "zz_item_position";
    public static final String ZZ_OWNER_NAME = "zz_owner";
    boolean isAnimating = false;
    boolean isVideo = false;
    boolean isZzFolderview = false;
    View mAnimateView = null;
    AnimationCallback mAnimatorCallBack = new AnimationCallback() {
        public void animateOnZapGestureInit(View view, View listItem, PointF p1) {
        }

        public void animateOnZapGestureStart(View view, View listItem, PointF p1, PointF p2) {
            ImageViewActivity.this.isAnimating = true;
            ImageViewActivity.this.mStartY = ((Math.abs(p1.y - p2.y) / 2.0f) + (p1.y < p2.y ? p1.y : p2.y)) - ((float) (ImageViewActivity.this.getActionBar() == null ? 0 : ImageViewActivity.this.getActionBar().getHeight()));
            ImageViewActivity.this.mLastY = ImageViewActivity.this.mStartY;
            ImageViewActivity.this.mConcentricCircle.setVisibility(0);
            ImageViewActivity.this.setDragDownYPoint(ImageViewActivity.this.mStartY - ((float) ImageViewActivity.this.mConcentricCircle.getCircleWidth()), 0);
            ImageViewActivity.this.mPlayIcon.setVisibility(4);
        }

        public void animateOnZapGestureMove(View view, View listItem, boolean isUp, PointF p1, PointF p2) {
            float y = ((Math.abs(p1.y - p2.y) / 2.0f) + (p1.y < p2.y ? p1.y : p2.y)) - ((float) (ImageViewActivity.this.getActionBar() == null ? 0 : ImageViewActivity.this.getActionBar().getHeight()));
            if (!isUp || ImageViewActivity.this.mLastY >= y) {
                ImageViewActivity.this.mLastY = y;
                ImageViewActivity.this.mConcentricCircle.setVisibility(0);
                if (ImageViewActivity.this.mStartY - y > BitmapDescriptorFactory.HUE_RED && isUp) {
                    ImageViewActivity.this.mAnimateView.setTranslationY(-(ImageViewActivity.this.mStartY - y));
                    ImageViewActivity.this.setDragUpYPoint(ImageViewActivity.this.mStartY, ((int) Math.abs(ImageViewActivity.this.mStartY - y)) + ImageViewActivity.this.mConcentricCircle.getCircleWidth());
                } else if (ImageViewActivity.this.mStartY - y < BitmapDescriptorFactory.HUE_RED && !isUp) {
                    ImageViewActivity.this.setDragDownYPoint(ImageViewActivity.this.mStartY, (int) Math.abs(ImageViewActivity.this.mStartY - y));
                }
            }
        }

        public void animateOnZapGestureComplete(View view, View listItem, boolean isUp, float velocity, final OnAnimationEndListener animateEndCallback) {
            ImageViewActivity.this.isAnimating = false;
            ImageViewActivity.this.mConcentricCircle.setVisibility(4);
            if (isUp) {
                ImageViewActivity.this.mConcentricCircle.setVisibility(4);
                ActionBar ab = ImageViewActivity.this.getActionBar();
                if (ab != null) {
                    ab.hide();
                }
                ImageViewActivity.this.mAnimateView.animate().translationY(-1.5f * ((float) ImageViewActivity.this.mAnimateView.getHeight())).scaleX(0.9f).setDuration(1000).setListener(new AnimatorListenerAdapter() {
                    public void onAnimationEnd(Animator animation) {
                        if (animateEndCallback != null) {
                            animateEndCallback.onAnimationEnd();
                        }
                    }
                });
                return;
            }
            ImageViewActivity.this.mAnimateView.setVisibility(0);
            if (ImageViewActivity.this.isVideo) {
                ImageViewActivity.this.mPlayIcon.setVisibility(0);
            }
            ImageViewActivity.this.setDragUpYPoint(BitmapDescriptorFactory.HUE_RED, ImageViewActivity.this.mConcentricCircle.getCircleWidth());
            ImageViewActivity.this.mConcentricCircle.setY((float) ImageViewActivity.this.mDragUpPosition);
            ImageViewActivity.this.mAnimateView.setTranslationY(BitmapDescriptorFactory.HUE_RED);
            if (animateEndCallback != null) {
                animateEndCallback.onAnimationEnd();
            }
        }

        public void animateOnZapGestureCancel(View view, View listItem) {
            ImageViewActivity.this.isAnimating = false;
            ImageViewActivity.this.mAnimateView.setVisibility(0);
            if (ImageViewActivity.this.isVideo) {
                ImageViewActivity.this.mPlayIcon.setVisibility(0);
            }
            ImageViewActivity.this.setDragUpYPoint(BitmapDescriptorFactory.HUE_RED, ImageViewActivity.this.mConcentricCircle.getCircleWidth());
            ImageViewActivity.this.mConcentricCircle.setY((float) ImageViewActivity.this.mDragUpPosition);
            ImageViewActivity.this.mAnimateView.setTranslationY(BitmapDescriptorFactory.HUE_RED);
            ImageViewActivity.this.mConcentricCircle.setVisibility(4);
        }
    };
    ConcentricCircles mConcentricCircle = null;
    Cursor mCursor = null;
    int mDragUpPosition = 0;
    String[] mFiles = null;
    Handler mHandler = new Handler();
    boolean mHideAnimInProgress = false;
    ImageView mImageView = null;
    Integer[] mIndexes = null;
    /* access modifiers changed from: private */
    public float mLastY = BitmapDescriptorFactory.HUE_RED;
    ImageView mPlayIcon = null;
    int mPosition = -1;
    protected float mStartY = -1.0f;
    Handler mTutorialHanlder = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    if (!ImageViewActivity.this.isAnimating) {
                        ImageViewActivity.this.mTutorialView.setVisibility(0);
                        int height = ImageViewActivity.this.mAnimateView.getHeight();
                        int tutorialHeight = ImageViewActivity.this.mTutorialView.getHeight();
                        ImageViewActivity.this.mTutorialView.setY((float) height);
                        ImageViewActivity.this.mTutorialView.animate().translationY((float) ((height - tutorialHeight) - 100)).setDuration(1000).setListener(new AnimatorListenerAdapter() {
                            public void onAnimationEnd(Animator animation) {
                            }
                        });
                        return;
                    }
                    return;
                case 1:
                    ImageViewActivity.this.mTutorialView.setVisibility(4);
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public View mTutorialView = null;
    Uri mUri = null;
    CustomViewPager mViewPager = null;
    ZapGestureListener mZapGestureListener = new ZapGestureListener() {
        public void onVerticalZap(boolean z, View view, int i) {
            Uri uri;
            if (z) {
                Intent intent = new Intent(ZapConstants.ACTION_GS_GALLERY_PHOTO_SHARED);
                intent.setClass(ImageViewActivity.this.getApplicationContext(), ActionReceiver.class);
                if (ImageViewActivity.this.isZzFolderview) {
                    uri = FileUtils.getImageContentUri(ImageViewActivity.this, ImageViewActivity.this.mFiles[ImageViewActivity.this.mViewPager.getCurrentItem()]);
                } else {
                    uri = ImageViewActivity.this.mUri;
                }
                intent.putExtra(ZapConstants.EXTRA_GS_GALLERY_PHOTO_SHARED, uri.toString());
                intent.putExtra(ZapConstants.EXTRA_EVENT_ORIGIN_ZAP_APP, true);
                ImageViewActivity.this.sendBroadcast(intent);
                ImageViewActivity.this.finish();
                return;
            }
            Intent intent2 = new Intent(ZapConstants.ACTION_GS_SWIPE_DOWN);
            intent2.setClass(ImageViewActivity.this.getApplicationContext(), ActionReceiver.class);
            intent2.putExtra(ZapConstants.EXTRA_EVENT_ORIGIN_ZAP_APP, true);
            ImageViewActivity.this.sendBroadcast(intent2);
        }

        public void onHorizontalZap(boolean isLeft) {
        }
    };
    private ZapGestureTouchListener mZapGestureTouchListener;

    class ZzDetailedViewAdapter extends PagerAdapter {
        private String[] mFiles = null;

        public ZzDetailedViewAdapter(String[] files) {
            this.mFiles = files;
        }

        public Object instantiateItem(View collection, int position) {
            ImageView view = (ImageView) LayoutInflater.from(ImageViewActivity.this).inflate(R.layout.imageview, null);
            ImageViewActivity.this.loadImage(view, FileUtils.getImageContentUri(ImageViewActivity.this, this.mFiles[position]));
            ((ViewPager) collection).addView(view, 0);
            return view;
        }

        public int getCount() {
            return this.mFiles.length;
        }

        public void destroyItem(View collection, int position, Object view) {
            ((ViewPager) collection).removeView((ImageView) view);
        }

        public boolean isViewFromObject(View view, Object object) {
            return view == ((ImageView) object);
        }
    }

    public void cleanUpAllHandlerMessages() {
        this.mTutorialView.setVisibility(4);
        this.mTutorialHanlder.removeCallbacksAndMessages(null);
    }

    public void resetInActivityTimer() {
        this.mTutorialHanlder.removeMessages(1);
        this.mTutorialHanlder.sendEmptyMessageDelayed(1, 10);
        this.mTutorialHanlder.removeMessages(0);
        this.mTutorialHanlder.sendEmptyMessageDelayed(0, 3000);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(8);
        setContentView(R.layout.activity_send_image);
        this.mPlayIcon = (ImageView) findViewById(R.id.video_icon);
        this.mImageView = (ImageView) findViewById(R.id.image_view);
        this.mViewPager = (CustomViewPager) findViewById(R.id.pager_view);
        this.mConcentricCircle = (ConcentricCircles) findViewById(R.id.concentric_circles);
        this.mConcentricCircle.setVisibility(4);
        this.mConcentricCircle.setDirection(true);
        ActionBar actionbar = getActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setDisplayShowTitleEnabled(true);
        }
        this.mTutorialView = findViewById(R.id.swipe_up_tutorial);
        Intent intent = getIntent();
        this.isZzFolderview = intent.getBooleanExtra(IS_ZZ_FLODER_VIEW, false);
        if (this.isZzFolderview) {
            this.mAnimateView = this.mViewPager;
            this.mViewPager.setVisibility(0);
            this.mImageView.setVisibility(8);
            String zzOwner = intent.getStringExtra(ZZ_OWNER_NAME);
            String zzId = intent.getStringExtra(ZZ_ID);
            this.mPosition = intent.getIntExtra(ZZ_ITEM_POSITION, 0);
            Bundle bundle = new Bundle();
            bundle.putString(ZZ_ID, zzId);
            bundle.putString(ZZ_OWNER_NAME, zzOwner);
            getLoaderManager().initLoader(0, bundle, this);
            this.mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
                public void onPageSelected(int arg0) {
                    ImageViewActivity.this.invalidateOptionsMenu();
                }

                public void onPageScrolled(int arg0, float arg1, int arg2) {
                }

                public void onPageScrollStateChanged(int arg0) {
                }
            });
        } else {
            this.mAnimateView = this.mImageView;
            this.mViewPager.setVisibility(8);
            this.mImageView.setVisibility(0);
            loadImage(this.mImageView, getIntent().getData());
        }
        this.mConcentricCircle.setX(BitmapDescriptorFactory.HUE_RED);
        this.mConcentricCircle.makeCirclesToStrech(true);
        this.mDragUpPosition = this.mConcentricCircle.getCircleWidth() * -2;
        this.mConcentricCircle.setY((float) this.mDragUpPosition);
        this.mConcentricCircle.invalidate();
        this.mZapGestureTouchListener = new ZapGestureTouchListener(this);
        this.mZapGestureTouchListener.registerZapGestureListener(this.mZapGestureListener);
        this.mZapGestureTouchListener.registerAnimationCallback(this.mAnimatorCallBack);
        if (this.isZzFolderview) {
            this.mViewPager.setAnimators(null, this.mAnimatorCallBack, this.mZapGestureListener);
        }
    }

    /* access modifiers changed from: 0000 */
    public void loadImage(ImageView imageView, Uri uri) {
        this.mUri = uri;
        setImage(imageView, this.mUri);
        this.isVideo = FileUtils.isVideo(FileUtils.getMimeTypeFromFileName(FileUtils.getMediaPathFromUri(this, this.mUri)));
        if (this.isVideo) {
            this.mPlayIcon.setVisibility(0);
        }
    }

    public void onResume() {
        super.onResume();
        resetInActivityTimer();
    }

    public void onPause() {
        super.onPause();
        cleanUpAllHandlerMessages();
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        super.dispatchTouchEvent(ev);
        resetInActivityTimer();
        return false;
    }

    /* access modifiers changed from: private */
    public void setDragUpYPoint(float y, int height) {
        this.mConcentricCircle.setDirection(true);
        this.mConcentricCircle.setY(y - ((float) this.mConcentricCircle.getHeight()));
        this.mConcentricCircle.setVisibleHeight(height);
        this.mConcentricCircle.invalidate();
    }

    /* access modifiers changed from: private */
    public void setDragDownYPoint(float y, int height) {
        this.mConcentricCircle.setDirection(false);
        this.mConcentricCircle.setY(y);
        this.mConcentricCircle.setVisibleHeight(height);
        this.mConcentricCircle.invalidate();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                finish();
                break;
            case R.id.menu_action_report /*2131296571*/:
                int currentItem = this.mViewPager.getCurrentItem();
                if (this.mCursor != null && this.mCursor.moveToPosition(currentItem)) {
                    ZapUtils.reportImage(this, TCH.getFrom(this.mCursor), TCH.getFilePath(this.mCursor), TCH.getShareId(this.mCursor));
                    break;
                }
            case R.id.menu_action_delete /*2131296572*/:
                int currentItem2 = this.mViewPager.getCurrentItem();
                if (this.mCursor != null && this.mCursor.moveToPosition(currentItem2)) {
                    String filePath = TCH.getFilePath(this.mCursor);
                    HistoryHandler histHdlr = new HistoryHandler();
                    if (currentItem2 == this.mViewPager.getAdapter().getCount() - 1) {
                        this.mPosition--;
                    } else {
                        this.mPosition++;
                    }
                    histHdlr.deleteMedia(filePath);
                    break;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    private void setImage(final ImageView image, final Uri uri) {
        new Thread(new Runnable() {
            public void run() {
                int targetW = image.getWidth();
                int targetH = image.getHeight();
                if (targetH == 0 || targetW == 0) {
                    DisplayMetrics d = ImageViewActivity.this.getResources().getDisplayMetrics();
                    targetH = d.heightPixels;
                    targetW = d.widthPixels;
                }
                Bitmap bitmap = FileUtils.getImage(FileUtils.getMediaPathFromUri(image.getContext(), uri), targetW, targetH);
                if (bitmap != null) {
                    final Bitmap b = bitmap;
                    ImageViewActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            image.setScaleType(ScaleType.CENTER_INSIDE);
                            image.setImageBitmap(b);
                        }
                    });
                    return;
                }
                if (Log.isLoggable(ImageViewActivity.TAG, 6)) {
                    Log.e(ImageViewActivity.TAG, " Failed to decode the Image, uri:" + uri);
                }
                ImageViewActivity.this.finish();
            }
        }).start();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (!this.isZzFolderview) {
            return false;
        }
        getMenuInflater().inflate(R.menu.options_menu_imageview, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!this.isZzFolderview) {
            return false;
        }
        menu.findItem(R.id.menu_action_report).setVisible(false);
        menu.findItem(R.id.menu_action_delete).setVisible(true);
        if (this.mCursor != null) {
            if (this.mCursor.moveToPosition(this.mIndexes[this.mViewPager.getCurrentItem()].intValue())) {
                boolean isAbusedReported = TCH.getAbuseReported(this.mCursor);
                String fileName = TCH.getFilePath(this.mCursor);
                boolean isSendCard = TCH.isSendCard(this.mCursor);
                boolean isDemoCard = TCH.isDemoCard(this.mCursor);
                if (isAbusedReported || !FileUtils.isFileExists(fileName) || isSendCard || isDemoCard) {
                    menu.findItem(R.id.menu_action_report).setVisible(false);
                } else {
                    menu.findItem(R.id.menu_action_report).setVisible(true);
                }
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.mZapGestureTouchListener.onTouch(getWindow().getDecorView(), event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, History.CONTENT_URI, Columns.DEFAULT_PROJECTION, ZapZoneDetail.SELECTION, new String[]{args.getString(ZZ_OWNER_NAME), args.getString(ZZ_ID)}, History.INVERSE_CHRONOLOGICAL_ORDER);
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor == null || cursor.getCount() == 0) {
            finish();
            return;
        }
        this.mCursor = cursor;
        int count = cursor.getCount();
        List<String> filePaths = new ArrayList<>(count);
        List<Integer> indexes = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            cursor.moveToPosition(i);
            String path = TCH.getFilePath(cursor);
            File demoCard = ZapUtils.getDemoCardFile();
            if (path != null && FileUtils.isFileExists(path) && !demoCard.getAbsolutePath().equals(path)) {
                filePaths.add(path);
                indexes.add(Integer.valueOf(i));
            }
        }
        if (filePaths.size() == 0) {
            finish();
            return;
        }
        this.mFiles = (String[]) filePaths.toArray(new String[filePaths.size()]);
        this.mIndexes = (Integer[]) indexes.toArray(new Integer[filePaths.size()]);
        if (this.mPosition < 0 && this.mPosition >= this.mFiles.length) {
            this.mPosition = 0;
        }
        this.mViewPager.setAdapter(new ZzDetailedViewAdapter(this.mFiles));
        this.mViewPager.setCurrentItem(this.mPosition);
    }

    public void onLoaderReset(Loader<Cursor> loader) {
    }
}
