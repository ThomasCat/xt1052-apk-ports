package com.motorola.sharing;

import com.motorola.sharing.Endpoint.Attributes;

public interface ProviderApi extends ContentApi {
    void discoverEndpoints(Attributes attributes, EndpointDiscoveryListener endpointDiscoveryListener);

    Endpoint[] getStaticEndpoints();

    void stopEndpointDiscovery(EndpointDiscoveryListener endpointDiscoveryListener);
}
