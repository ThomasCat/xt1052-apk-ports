package com.motorola.sharing;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.motorola.sharing.GroupListener.GroupEvent;
import com.motorola.sharing.GroupListener.GroupEvent.Status;
import com.motorola.sharing.GroupListener.GroupEvent.Substatus;
import com.motorola.sharing.GroupListener.GroupEvent.Type;
import com.motorola.sharing.provider.cloud.group.GroupHandler;
import com.motorola.sharing.util.Logger;
import com.motorola.zap.provider.HistoryHandler;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class GroupManager {
    private static final String TAG = Logger.getGcspTag("GroupManager");
    private Set<ListenerInfo> mListenerList = new HashSet();

    public static class BanParams {
        public String groupId;
        public String userId;
    }

    public static class CreateParams implements Serializable {
        public String imagePath;
        public Locality locality;
        public String name;
        public String pin;
    }

    public static class GroupContract {
        public static final String MEMBERS_MIME_TYPE = "vnd.android.cursor.dir/vnd.com.motorola.sharing.group.members";
        public static final String MEMBERS_URI_PATH = "members";
        public static final String SELECTION_ALL_MEMBERS = String.format("%s = ? AND (%s IS NULL OR %s != 1)", new Object[]{"groupId", Columns.BLOCKED, Columns.BLOCKED});
        public static final String SELECTION_MEMBER = String.format("%s = ? AND %s = ?", new Object[]{"groupId", Columns.USER_ID});

        public static final class Columns {
            public static final String[] ALL_COLUMNS = {HistoryHandler.HIST_ID, "groupId", USER_ID, PROFILE_VERSION, NICKNAME, PICTURE_URL};
            public static final String BLOCKED = "blocked";
            public static final String GROUP_ID = "groupId";
            public static final String NICKNAME = "nickname";
            public static final String PICTURE_URL = "pictureUrl";
            public static final String PROFILE_VERSION = "profileVersion";
            public static final String USER_ID = "userId";
        }

        public static String getAuthority() {
            return GCSP.getContext().getPackageName() + ".group";
        }

        public static Uri getContentUri() {
            return Uri.parse("content://" + getAuthority());
        }

        public static Uri getMembersContentUri() {
            return Uri.parse("content://" + getAuthority() + "/" + MEMBERS_URI_PATH);
        }
    }

    public static class GroupInfo {
        public final String id;
        public final boolean isActive;
        public final long lastInviteTime;
        public final boolean locked;
        public final int memberCount;
        public final String name;
        public final String owner;
        public final String pin;
        public final String thumbnail;

        public GroupInfo(boolean isActive2, String owner2, String id2, String name2, String thumbnail2, boolean locked2, String pin2, int memberCount2, long lastInviteTime2) {
            this.isActive = isActive2;
            this.owner = owner2;
            this.id = id2;
            this.name = name2;
            this.thumbnail = thumbnail2;
            this.locked = locked2;
            this.pin = pin2;
            this.memberCount = memberCount2;
            this.lastInviteTime = lastInviteTime2;
        }
    }

    public static class JoinParams implements Serializable {
        public String id;
        public Locality locality;
        public String name;
        public String owner;
        public String pin;
        public boolean rejoin;
        public String thumbnail;
    }

    private class ListenerInfo {
        Handler handler;
        GroupListener listener;

        public ListenerInfo(GroupListener listener2) {
            this.listener = listener2;
            Looper looper = Looper.myLooper();
            if (looper == null) {
                looper = Looper.getMainLooper();
            }
            this.handler = new Handler(looper);
        }
    }

    public static class ShareInviteParams implements Serializable {
        public Locality locality;
    }

    public void create(CreateParams params) {
        GroupHandler.getInstance().createGroup(params);
    }

    public void shareInvite(ShareInviteParams params) {
        GroupHandler.getInstance().shareGroupInvite(params);
    }

    public void join(JoinParams params) {
        GroupHandler.getInstance().joinGroup(params);
    }

    public void leave(String id) {
        GroupHandler.getInstance().leaveGroup(false);
    }

    public void banUser(BanParams params) {
        GroupHandler.getInstance().banUser(params);
    }

    public boolean isAnyGroupActive() {
        return GroupHandler.getInstance().isGroupActive();
    }

    public GroupInfo getGroupInfo(String id) {
        return GroupHandler.getInstance().getGroupInfo();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r5.mListenerList.add(new com.motorola.sharing.GroupManager.ListenerInfo(r5, r6));
        com.motorola.sharing.util.Logger.d(TAG, "listener registered: " + r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0053, code lost:
        if (r5.mListenerList.size() != 1) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0055, code lost:
        com.motorola.sharing.provider.cloud.group.GroupHandler.getInstance().updateGroupState();
     */
    public synchronized void registerGroupListener(GroupListener listener) {
        if (listener != null) {
            Iterator i$ = this.mListenerList.iterator();
            while (true) {
                if (i$.hasNext()) {
                    if (((ListenerInfo) i$.next()).listener.equals(listener)) {
                        break;
                    }
                } else {
                    break;
                }
            }
        } else {
            throw new IllegalArgumentException("listener is null");
        }
    }

    public synchronized void unregisterGroupListener(GroupListener listener) {
        if (listener != null) {
            Iterator i$ = this.mListenerList.iterator();
            while (true) {
                if (!i$.hasNext()) {
                    break;
                }
                ListenerInfo info = (ListenerInfo) i$.next();
                if (info.listener.equals(listener)) {
                    this.mListenerList.remove(info);
                    Logger.d(TAG, "listener unregistered: " + listener);
                    break;
                }
            }
        } else {
            throw new IllegalArgumentException("listener is null");
        }
    }

    public void sendEvent(Type type) {
        sendEvent(type, Status.OK, Substatus.NO_ERROR, null, GroupHandler.getInstance().getGroupInfo());
    }

    public synchronized void sendEvent(Type type, Status status, Substatus error, Bundle bundle, GroupInfo groupInfo) {
        if (this.mListenerList.size() > 0) {
            final GroupEvent ge = new GroupEvent();
            ge.type = type;
            ge.status = status;
            ge.subStatus = error;
            ge.groupInfo = groupInfo;
            ge.bundle = new Bundle();
            if (bundle != null) {
                ge.bundle.putAll(bundle);
            }
            for (ListenerInfo info : this.mListenerList) {
                final GroupListener listener = info.listener;
                info.handler.post(new Runnable() {
                    public void run() {
                        listener.onGroupEvent(ge);
                    }
                });
            }
        }
    }
}
