package com.google.analytics.tracking.android;

import java.util.Map;

abstract class TrackerHandler {
    /* access modifiers changed from: 0000 */
    public abstract void sendHit(Map<String, String> map);

    TrackerHandler() {
    }
}
