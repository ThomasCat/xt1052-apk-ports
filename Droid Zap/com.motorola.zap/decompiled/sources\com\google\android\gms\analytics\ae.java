package com.google.android.gms.analytics;

import android.content.Context;
import android.util.DisplayMetrics;
import com.google.analytics.tracking.android.Fields;

class ae implements m {
    private static Object qI = new Object();
    private static ae uk;
    private final Context mContext;

    protected ae(Context context) {
        this.mContext = context;
    }

    public static ae cs() {
        ae aeVar;
        synchronized (qI) {
            aeVar = uk;
        }
        return aeVar;
    }

    public static void n(Context context) {
        synchronized (qI) {
            if (uk == null) {
                uk = new ae(context);
            }
        }
    }

    /* access modifiers changed from: protected */
    public String ct() {
        DisplayMetrics displayMetrics = this.mContext.getResources().getDisplayMetrics();
        return displayMetrics.widthPixels + "x" + displayMetrics.heightPixels;
    }

    public String getValue(String field) {
        if (field != null && field.equals(Fields.SCREEN_RESOLUTION)) {
            return ct();
        }
        return null;
    }

    public boolean x(String str) {
        return Fields.SCREEN_RESOLUTION.equals(str);
    }
}
