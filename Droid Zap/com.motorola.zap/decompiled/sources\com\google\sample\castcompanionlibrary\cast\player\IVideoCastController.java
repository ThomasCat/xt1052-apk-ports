package com.google.sample.castcompanionlibrary.cast.player;

import android.graphics.Bitmap;

public interface IVideoCastController {
    void adjustControllersForLiveStream(boolean z);

    void closeActivity();

    void setImage(Bitmap bitmap);

    void setLine1(String str);

    void setLine2(String str);

    void setOnVideoCastControllerChangedListener(OnVideoCastControllerListener onVideoCastControllerListener);

    void setPlaybackStatus(int i);

    void setStreamType(int i);

    void showLoading(boolean z);

    void updateControllersStatus(boolean z);

    void updateSeekbar(int i, int i2);
}
