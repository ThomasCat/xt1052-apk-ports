package com.google.sample.castcompanionlibrary.cast.callbacks;

import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.common.api.Status;

public class DataCastConsumerImpl extends BaseCastConsumerImpl implements IDataCastConsumer {
    public void onApplicationConnected(ApplicationMetadata appMetadata, String applicationStatus, String sessionId, boolean wasLaunched) {
    }

    public void onApplicationDisconnected(int errorCode) {
    }

    public void onApplicationStopFailed(int errorCode) {
    }

    public boolean onApplicationConnectionFailed(int errorCode) {
        return true;
    }

    public void onApplicationStatusChanged(String appStatus) {
    }

    public void onVolumeChanged(double value, boolean isMute) {
    }

    public void onMessageReceived(CastDevice castDevice, String namespace, String message) {
    }

    public void onMessageSendFailed(Status status) {
    }

    public void onRemoved(CastDevice castDevice, String namespace) {
    }
}
