package com.google.sample.castcompanionlibrary.cast.callbacks;

import com.google.android.gms.cast.ApplicationMetadata;

public interface IVideoCastConsumer extends IBaseCastConsumer {
    void onApplicationConnected(ApplicationMetadata applicationMetadata, String str, boolean z);

    boolean onApplicationConnectionFailed(int i);

    void onApplicationDisconnected(int i);

    void onApplicationStatusChanged(String str);

    void onApplicationStopFailed(int i);

    void onDataMessageReceived(String str);

    void onDataMessageSendFailed(int i);

    void onRemoteMediaPlayerMetadataUpdated();

    void onRemoteMediaPlayerStatusUpdated();

    void onRemovedNamespace();

    void onVolumeChanged(double d, boolean z);
}
