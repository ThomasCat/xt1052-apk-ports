package com.motorola.sharing.util;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import java.util.regex.Pattern;

public class DeviceUtils {
    private static final SharedPreference<String> IMEI = new SharedPreference<>("g_imei", String.class, null, SharedPreference.stringHandler);

    public static String getDeviceModelInfo() {
        return Build.MANUFACTURER.toUpperCase() + "/" + Build.PRODUCT + "/" + Build.MODEL;
    }

    public static String getImei(Context context) {
        String imei = (String) IMEI.get();
        if (!TextUtils.isEmpty(imei)) {
            return imei;
        }
        String str = "^[0-9a-fA-F]*";
        String str2 = "NA";
        String imei2 = "NA";
        TelephonyManager tm = (TelephonyManager) context.getSystemService("phone");
        if (tm != null) {
            imei2 = tm.getDeviceId();
            if (imei2 == null) {
                imei2 = "NA";
            } else if (imei2.length() < 14 || imei2.length() > 50 || !Pattern.matches("^[0-9a-fA-F]*", imei2)) {
                imei2 = "NA";
            }
        }
        IMEI.set(imei2);
        return imei2;
    }
}
