package com.motorola.sharing.provider.cloud.group.request;

import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener.GroupEvent.Type;
import com.motorola.sharing.util.Logger;

public class GroupExpiredRequest extends GroupRequest {
    private static final String TAG = Logger.getCloudTag("GroupExpiredReq");

    public GroupExpiredRequest() {
        super(GCSP.getContext(), false);
    }

    public Type getWhat() {
        return Type.GROUP_EXPIRE;
    }

    public void run() {
        Logger.d(TAG, String.format("Running request, retry: %b", new Object[]{Boolean.valueOf(isRetry())}));
        String id = Logger.isDebuggable() ? this.mGroupStateTracker.getState().getId() : null;
        onGroupStopped();
        Logger.d(TAG, "Group expired - gid: " + id);
        sendOk();
    }
}
