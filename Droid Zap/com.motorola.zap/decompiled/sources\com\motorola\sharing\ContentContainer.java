package com.motorola.sharing;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import com.motorola.sharing.Abuse.AbuseAction;
import com.motorola.sharing.Abuse.AbuseType;
import com.motorola.sharing.Authenticator.TokenInfo;
import com.motorola.sharing.ContentDiscoveryListener.ContentDiscoveryEvent;
import com.motorola.sharing.ContentDiscoveryListener.ContentDiscoveryEvent.Substatus;
import com.motorola.sharing.ContentDownloadListener.ContentDownloadEvent;
import com.motorola.sharing.ContentListener.Event.Status;
import com.motorola.sharing.ContentShareListener.ContentCancelShareEvent;
import com.motorola.sharing.ContentShareListener.ContentShareEvent;
import com.motorola.sharing.ContentShareListener.ContentStopShareEvent;
import com.motorola.sharing.DispatchRule.Params;
import com.motorola.sharing.GroupManager.GroupInfo;
import com.motorola.sharing.ReportAbuseListener.ReportAbuseEvent;
import com.motorola.sharing.instrumentation.ActionResult;
import com.motorola.sharing.util.ContainerExpiryTracker.ExpiryTriggers;
import com.motorola.sharing.util.DbUtils;
import com.motorola.sharing.util.FileUtils;
import com.motorola.sharing.util.HistoryManager.Shares.C;
import com.motorola.sharing.util.Logger;
import com.motorola.sharing.util.Uid;
import com.motorola.zap.ZapConstants;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ContentContainer {
    private static final String TAG = Logger.getGcspTag("ContentContainer");
    public AbuseType abuseType;
    public final ActionResult actionResult;
    protected final Bundle appData;
    public boolean canBeReused;
    protected final Map<String, String> checkinData;
    private Handler contentDiscoveryHandler;
    /* access modifiers changed from: private */
    public ContentDiscoveryListener contentDiscoveryListener;
    private Handler contentDownloadHandler;
    /* access modifiers changed from: private */
    public ContentDownloadListener contentDownloadListener;
    private Handler contentShareHandler;
    /* access modifiers changed from: private */
    public ContentShareListener contentShareListener;
    public final long creationTime;
    public DispatchRule dispatchRule;
    public ExpiryTriggers expiryTriggers;
    public GroupInfo groupInfo;
    public Locality locality;
    public String mimeType;
    public String name;
    public String origin;
    public P2pParams p2pParams;
    public String path;
    public String pin;
    public Profile profile;
    private final Map<Class<?>, Object> providerData;
    private Handler reportAbuseHandler;
    /* access modifiers changed from: private */
    public ReportAbuseListener reportAbuseListener;
    public String shareId;
    private long sharedElapsedTime;
    private long sharedTime;
    public int size;
    public String thumbnailBase64;
    public String tmpPath;
    public TransferStatus transferStatus;
    private long ttl;
    public final Type type;
    public final long uid;
    public UploadRule uploadRule;
    public String url;
    public boolean useJpegSizeReduction;
    public final UserCred userCred;

    public static class AbuseReportBuilder {
        private AbuseType abuseType = null;
        private String mimeType = null;
        private String origin = null;
        private String path = null;
        private String shareId = null;

        public AbuseReportBuilder setAbuseType(AbuseType abuseType2) {
            this.abuseType = abuseType2;
            return this;
        }

        public AbuseReportBuilder setShareId(String shareId2) {
            this.shareId = shareId2;
            return this;
        }

        public AbuseReportBuilder setOrigin(String origin2) {
            this.origin = origin2;
            return this;
        }

        public AbuseReportBuilder setPath(String path2) {
            this.path = path2;
            if (this.mimeType == null && path2 != null) {
                this.mimeType = FileUtils.getMimeTypeFromFileName(path2);
            }
            return this;
        }

        public AbuseReportBuilder setMimeType(String mimeType2) {
            this.mimeType = mimeType2;
            return this;
        }

        public ContentContainer build() {
            ContentContainer container = new ContentContainer(Type.REPORT_ABUSE);
            if (this.abuseType == null) {
                throw new IllegalStateException("abuse type is not set");
            }
            container.abuseType = this.abuseType;
            if (TextUtils.isEmpty(this.shareId)) {
                throw new IllegalStateException("share Id is not set");
            }
            container.shareId = this.shareId;
            if (TextUtils.isEmpty(this.origin)) {
                throw new IllegalStateException("origin is not set");
            }
            container.origin = this.origin;
            if (TextUtils.isEmpty(this.path)) {
                throw new IllegalStateException("file path is not set");
            }
            container.path = this.path;
            if (TextUtils.isEmpty(this.mimeType)) {
                throw new IllegalStateException("mime type is unknown");
            }
            container.mimeType = this.mimeType;
            return container;
        }
    }

    public static class Builder {
        private Locality locality = null;
        private String mimeType = null;
        private String path = null;
        private String pin = null;
        private long ttl = 120000;

        public Builder setPath(String path2) {
            this.path = path2;
            if (this.mimeType == null && path2 != null) {
                this.mimeType = FileUtils.getMimeTypeFromFileName(path2);
            }
            return this;
        }

        public Builder setMimeType(String mimeType2) {
            this.mimeType = mimeType2;
            return this;
        }

        public Builder setLocality(Locality locality2) {
            this.locality = locality2;
            return this;
        }

        public Builder setPin(String pin2) {
            this.pin = pin2;
            return this;
        }

        public Builder setTtl(long ttl2) {
            this.ttl = ttl2;
            return this;
        }

        public ContentContainer build() {
            ContentContainer container = new ContentContainer(Type.SHARE);
            if (TextUtils.isEmpty(this.path)) {
                throw new IllegalStateException("file path is not set");
            }
            container.path = this.path;
            File file = new File(this.path);
            int size = (int) file.length();
            if (size <= 0) {
                throw new IllegalStateException("file size is 0");
            }
            container.size = size;
            container.name = file.getName();
            if (TextUtils.isEmpty(this.mimeType)) {
                throw new IllegalStateException("mime type is unknown");
            }
            container.mimeType = this.mimeType;
            container.setLocality(this.locality);
            container.setPin(this.pin);
            container.setTtl(this.ttl);
            return container;
        }
    }

    public static class DiscoveryParams extends ContentContainer {
        public int maxResultsCount = 20;

        public DiscoveryParams() {
            super(Type.RECEIVE);
        }

        public ContentContainer generateContainer() {
            return new ContentContainer(this);
        }
    }

    public static class P2pParams {
        public final String name;
        public final String password;
        public final String url;

        public P2pParams(String url2, String name2, String password2) {
            this.url = url2;
            this.name = name2;
            this.password = password2;
        }

        public boolean isValid() {
            return !TextUtils.isEmpty(this.url) && !TextUtils.isEmpty(this.name) && !TextUtils.isEmpty(this.password);
        }
    }

    public static class TransferStatus {
        public long elapsedTime = 0;
        public long estimatedRemainingTime = -1;
        public long maxAllowedTime = -1;
        public final long startTime = System.currentTimeMillis();
        public long stopTime = 0;
        public final int totalBytes;
        public int transferredBytes = 0;

        public TransferStatus(int totalBytes2) {
            this.totalBytes = totalBytes2;
        }

        public int remainingBytes() {
            if (this.transferredBytes <= this.totalBytes) {
                return this.totalBytes - this.transferredBytes;
            }
            throw new IllegalStateException("totalBytes=" + this.totalBytes + " transferredBytes=" + this.transferredBytes);
        }

        public int progress() {
            if (this.totalBytes <= 0) {
                throw new IllegalStateException("totalBytes=" + this.totalBytes);
            } else if (this.transferredBytes <= this.totalBytes) {
                return (int) ((((float) this.transferredBytes) / ((float) this.totalBytes)) * 100.0f);
            } else {
                throw new IllegalStateException("totalBytes=" + this.totalBytes + " transferredBytes=" + this.transferredBytes);
            }
        }

        public boolean isCompleted() {
            return this.stopTime > 0;
        }

        public boolean isValid() {
            return this.totalBytes > 0 && this.transferredBytes >= 0 && this.transferredBytes <= this.totalBytes && this.elapsedTime >= 0 && this.stopTime >= 0;
        }
    }

    public enum Type {
        SHARE("Send"),
        RECEIVE("Receive"),
        CANCEL_SHARE("Cancel Share"),
        REPORT_ABUSE("Report Abuse");
        
        private String humanReadable;

        private Type(String humanReadable2) {
            this.humanReadable = humanReadable2;
        }

        public String humanReadable() {
            return this.humanReadable;
        }
    }

    public enum UploadRule {
        ALWAYS,
        NEVER,
        ON_DEMAND
    }

    public static class UserCred {
        public final String account;
        public String id;
        public String token;

        private UserCred(String account2) {
            this.id = null;
            this.token = null;
            if (TextUtils.isEmpty(account2)) {
                account2 = null;
            }
            this.account = account2;
        }

        private UserCred(UserCred other) {
            this.id = null;
            this.token = null;
            this.account = other.account;
            this.id = other.id;
            this.token = other.token;
        }

        public void set(TokenInfo info) {
            if (info == null || info.isEmpty() || !TextUtils.equals(info.getAccount(), this.account)) {
                this.id = null;
                this.token = null;
                return;
            }
            this.id = info.getUserId();
            this.token = info.getToken();
        }

        public boolean isEmpty() {
            return TextUtils.isEmpty(this.account) || TextUtils.isEmpty(this.id) || TextUtils.isEmpty(this.token);
        }
    }

    private ContentContainer(Type type2) {
        this(type2, System.currentTimeMillis(), (long) Uid.getNext());
    }

    private ContentContainer(Type type2, long creationTime2, long uid2) {
        this.abuseType = AbuseType.UNKNOWN;
        this.shareId = null;
        this.origin = null;
        this.url = null;
        this.path = null;
        this.tmpPath = null;
        this.name = null;
        this.size = 0;
        this.mimeType = null;
        this.thumbnailBase64 = null;
        this.locality = null;
        this.pin = null;
        this.p2pParams = null;
        this.transferStatus = null;
        this.actionResult = new ActionResult();
        this.canBeReused = true;
        this.useJpegSizeReduction = false;
        this.dispatchRule = null;
        this.profile = null;
        this.checkinData = new HashMap();
        this.appData = new Bundle();
        this.sharedTime = 0;
        this.sharedElapsedTime = 0;
        this.ttl = 0;
        this.providerData = new HashMap();
        this.creationTime = creationTime2;
        this.uid = uid2;
        this.type = type2;
        this.userCred = new UserCred(GCSP.getAuthenticator().getAccount());
        if (type2 == Type.SHARE || type2 == Type.CANCEL_SHARE) {
            this.origin = this.userCred.account;
        }
        if (type2 == Type.SHARE || type2 == Type.RECEIVE) {
            GroupInfo gi = GCSP.getGroupManager().getGroupInfo(null);
            if (gi == null || !gi.isActive) {
                gi = null;
            }
            this.groupInfo = gi;
            return;
        }
        this.groupInfo = null;
    }

    private ContentContainer(DiscoveryParams params) {
        this.abuseType = AbuseType.UNKNOWN;
        this.shareId = null;
        this.origin = null;
        this.url = null;
        this.path = null;
        this.tmpPath = null;
        this.name = null;
        this.size = 0;
        this.mimeType = null;
        this.thumbnailBase64 = null;
        this.locality = null;
        this.pin = null;
        this.p2pParams = null;
        this.transferStatus = null;
        this.actionResult = new ActionResult();
        this.canBeReused = true;
        this.useJpegSizeReduction = false;
        this.dispatchRule = null;
        this.profile = null;
        this.checkinData = new HashMap();
        this.appData = new Bundle();
        this.sharedTime = 0;
        this.sharedElapsedTime = 0;
        this.ttl = 0;
        this.providerData = new HashMap();
        this.creationTime = System.currentTimeMillis();
        this.uid = (long) Uid.getNext();
        this.type = Type.RECEIVE;
        this.userCred = new UserCred(params.userCred);
        this.groupInfo = params.groupInfo;
        this.locality = params.locality;
        this.pin = params.pin;
        this.checkinData.putAll(params.checkinData);
        this.appData.putAll(params.appData);
        this.contentDiscoveryListener = params.getContentDiscoveryListener();
        this.contentDiscoveryHandler = params.getContentDiscoveryHandler();
    }

    public long getUid() {
        return this.uid;
    }

    public boolean isReceiving() {
        return this.type == Type.RECEIVE;
    }

    public boolean isSharing() {
        return this.type == Type.SHARE;
    }

    public boolean isAbuseReporting() {
        return this.type == Type.REPORT_ABUSE;
    }

    public AbuseType getAbuseType() {
        return this.abuseType;
    }

    public String getShareId() {
        if (this.shareId == null || this.shareId.length() != 0) {
            return this.shareId;
        }
        throw new IllegalStateException("share id is empty");
    }

    public String getOrigin() {
        if (this.origin == null || this.origin.length() != 0) {
            return this.origin;
        }
        throw new IllegalStateException("origin is empty");
    }

    public String getUserAccount() {
        return this.userCred.account;
    }

    public String getPath() {
        if (this.path == null || this.path.length() != 0) {
            return this.path;
        }
        throw new IllegalStateException("path is empty");
    }

    public int getSize() {
        if (this.size >= 0) {
            return this.size;
        }
        throw new IllegalStateException("size is negative");
    }

    public String getMimeType() {
        if (this.mimeType == null || this.mimeType.length() != 0) {
            return this.mimeType;
        }
        throw new IllegalStateException("mime-type is empty");
    }

    public String getThumbnailBase64() {
        if (this.thumbnailBase64 == null || this.thumbnailBase64.length() != 0) {
            return this.thumbnailBase64;
        }
        throw new IllegalStateException("thumbnail is empty");
    }

    public GroupInfo getGroupInfo() {
        return this.groupInfo;
    }

    public boolean isGroupRelated() {
        return this.groupInfo != null;
    }

    public Locality getLocality() {
        return this.locality;
    }

    public void setLocality(Locality locality2) {
        if (locality2 == null || locality2.hasValidCoordinates()) {
            this.locality = locality2;
            return;
        }
        throw new IllegalArgumentException("latitude or longitude value is wrong");
    }

    public String getPin() {
        if (this.pin == null || this.pin.length() != 0) {
            return this.pin;
        }
        throw new IllegalStateException("pin is empty");
    }

    public void setPin(String pin2) {
        if (pin2 != null) {
            if (pin2.length() == 0) {
                throw new IllegalArgumentException("pin must not be empty");
            } else if (containsWhitespace(pin2)) {
                throw new IllegalArgumentException("pin must not contain whitespaces");
            }
        }
        this.pin = pin2;
    }

    public TransferStatus getTransferStatus() {
        return this.transferStatus;
    }

    public Bundle getAppData() {
        return this.appData;
    }

    public long getTtl() {
        return this.ttl;
    }

    public long getExpiryTime() {
        if (this.sharedTime <= 0 || this.ttl <= 0) {
            return 0;
        }
        return this.sharedTime + this.ttl;
    }

    public Profile getProfile() {
        return this.profile;
    }

    public long getExpiryElapsedTime() {
        long expiryElapsedTime;
        if (this.sharedTime <= 0 || this.ttl <= 0) {
            expiryElapsedTime = 0;
        } else {
            expiryElapsedTime = this.sharedElapsedTime + this.ttl;
        }
        if (expiryElapsedTime > 0) {
            return expiryElapsedTime;
        }
        return 0;
    }

    public void setTtl(long ttl2) {
        if (this.sharedTime > 0) {
            throw new IllegalStateException("change of ttl after container shared is not allowed");
        } else if (isGroupRelated()) {
            this.ttl = 0;
        } else if (ttl2 <= 0) {
            throw new IllegalArgumentException("ttl is " + ttl2);
        } else {
            this.ttl = ttl2;
        }
    }

    public void setP2pParams(P2pParams params) {
        if (params != null && !params.isValid()) {
            params = null;
        }
        this.p2pParams = params;
    }

    public P2pParams getP2pParams() {
        return this.p2pParams;
    }

    public void putCheckinData(String key, String value) {
        if (key == null || value == null) {
            throw new IllegalArgumentException("must not be null: key=" + key + ", value=" + value);
        }
        this.checkinData.put(key, value);
    }

    public String removeCheckinData(String key) {
        return (String) this.checkinData.remove(key);
    }

    public String getCheckinData(String key) {
        return (String) this.checkinData.get(key);
    }

    public void putProviderData(Object data) {
        if (data == null) {
            throw new IllegalArgumentException("must not be null");
        }
        this.providerData.put(data.getClass(), data);
    }

    public Object removeProviderData(Class<?> cls) {
        return this.providerData.remove(cls);
    }

    public Object getProviderData(Class<?> cls) {
        return this.providerData.get(cls);
    }

    public ContentValues getStateIfShared() {
        if (this.type != Type.SHARE || TextUtils.isEmpty(this.shareId) || this.sharedTime <= 0) {
            return null;
        }
        ContentValues stateValues = new ContentValues();
        stateValues.put(C.CREATED_TIME, Long.valueOf(this.creationTime));
        stateValues.put(C.UID, Long.valueOf(this.uid));
        stateValues.put("shareId", this.shareId);
        stateValues.put(C.SHARED_TIME, Long.valueOf(this.sharedTime));
        stateValues.put(C.TTL, Long.valueOf(this.ttl));
        if (this.groupInfo != null) {
            stateValues.put("groupId", this.groupInfo.id);
            stateValues.put(C.GROUP_OWNER, this.groupInfo.owner);
        }
        stateValues.put(C.ACCOUNT, this.userCred.account);
        stateValues.put(C.PATH, this.path);
        stateValues.put(C.SIZE, Integer.valueOf(this.size));
        stateValues.put(C.MIME_TYPE, this.mimeType);
        stateValues.put(C.PIN, this.pin);
        if (this.p2pParams != null) {
            stateValues.put(C.P2P_URL, this.p2pParams.url);
            stateValues.put(C.P2P_NAME, this.p2pParams.name);
            stateValues.put(C.P2P_PASSWORD, this.p2pParams.password);
        }
        if (this.uploadRule != null) {
            stateValues.put(C.UPLOAD_RULE, this.uploadRule.name());
        }
        stateValues.put(C.EXPIRY_TRIGGERS, DbUtils.serialize(this.expiryTriggers));
        stateValues.put(C.DISPATCH_RULE, DbUtils.serialize(this.dispatchRule.getParams()));
        return stateValues;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0082, code lost:
        if (android.text.TextUtils.equals(r3, r1) == false) goto L_0x0084;
     */
    static ContentContainer restoreSharedContainer(Cursor cursor) {
        String str;
        String str2;
        try {
            long j = DbUtils.getLong(cursor, C.CREATED_TIME, 0);
            long j2 = DbUtils.getLong(cursor, C.UID, -1);
            if (j <= 0 || j2 == -1) {
                return null;
            }
            ContentContainer contentContainer = new ContentContainer(Type.SHARE, j, j2);
            contentContainer.canBeReused = false;
            String string = DbUtils.getString(cursor, C.ACCOUNT);
            if (TextUtils.isEmpty(string) || !TextUtils.equals(string, contentContainer.getUserAccount())) {
                return null;
            }
            String string2 = DbUtils.getString(cursor, "groupId");
            String string3 = DbUtils.getString(cursor, C.GROUP_OWNER);
            if (!TextUtils.isEmpty(string2) || !TextUtils.isEmpty(string3)) {
                GroupInfo groupInfo2 = contentContainer.groupInfo;
                if (groupInfo2 != null) {
                    str = groupInfo2.id;
                } else {
                    str = null;
                }
                if (TextUtils.equals(string2, str)) {
                    if (groupInfo2 != null) {
                        str2 = groupInfo2.owner;
                    } else {
                        str2 = null;
                    }
                }
                return null;
            }
            contentContainer.groupInfo = null;
            contentContainer.shareId = DbUtils.getString(cursor, "shareId");
            if (TextUtils.isEmpty(contentContainer.shareId)) {
                return null;
            }
            contentContainer.sharedTime = DbUtils.getLong(cursor, C.SHARED_TIME, 0);
            long currentTimeMillis = System.currentTimeMillis() - contentContainer.sharedTime;
            if (contentContainer.sharedTime <= 0 || currentTimeMillis < 0) {
                return null;
            }
            contentContainer.sharedElapsedTime = SystemClock.elapsedRealtime() - currentTimeMillis;
            contentContainer.ttl = DbUtils.getLong(cursor, C.TTL, 0);
            if (contentContainer.ttl < 0) {
                return null;
            }
            contentContainer.path = DbUtils.getString(cursor, C.PATH);
            contentContainer.size = DbUtils.getInt(cursor, C.SIZE, -1);
            contentContainer.mimeType = DbUtils.getString(cursor, C.MIME_TYPE);
            File file = new File(contentContainer.path);
            int length = (int) file.length();
            if (length <= 0 || length != contentContainer.size || TextUtils.isEmpty(contentContainer.mimeType)) {
                return null;
            }
            contentContainer.name = file.getName();
            contentContainer.pin = DbUtils.getString(cursor, C.PIN);
            P2pParams p2pParams2 = new P2pParams(DbUtils.getString(cursor, C.P2P_URL), DbUtils.getString(cursor, C.P2P_NAME), DbUtils.getString(cursor, C.P2P_PASSWORD));
            if (p2pParams2.isValid()) {
                contentContainer.setP2pParams(p2pParams2);
            }
            String string4 = DbUtils.getString(cursor, C.UPLOAD_RULE);
            if (!TextUtils.isEmpty(string4)) {
                contentContainer.uploadRule = UploadRule.valueOf(string4);
            }
            contentContainer.expiryTriggers = (ExpiryTriggers) DbUtils.deserialize(DbUtils.getBlob(cursor, C.EXPIRY_TRIGGERS));
            if (contentContainer.expiryTriggers == null) {
                return null;
            }
            Params params = (Params) DbUtils.deserialize(DbUtils.getBlob(cursor, C.DISPATCH_RULE));
            contentContainer.dispatchRule = (DispatchRule) Class.forName((String) params.get(DispatchRule.KEY_CLASS_NAME)).getDeclaredMethod("getInstance", new Class[]{Params.class}).invoke(null, new Object[]{params});
            if (contentContainer.dispatchRule == null) {
                return null;
            }
            return contentContainer;
        } catch (Exception e) {
            Logger.e(TAG, "got exception: " + e);
            return null;
        }
    }

    public synchronized void setContentDiscoveryListener(ContentDiscoveryListener listener) {
        this.contentDiscoveryListener = listener;
        this.contentDiscoveryHandler = listener != null ? getHandler() : null;
    }

    public synchronized ContentDiscoveryListener getContentDiscoveryListener() {
        return this.contentDiscoveryListener;
    }

    /* access modifiers changed from: protected */
    public synchronized Handler getContentDiscoveryHandler() {
        return this.contentDiscoveryHandler;
    }

    public synchronized void onContentDiscoveryEvent(Status status, Substatus substatus, Endpoint endpoint) {
        onContentDiscoveryEvent(new ContentDiscoveryEvent(status, substatus, this, endpoint));
    }

    public synchronized void onContentDiscoveryEvent(final ContentDiscoveryEvent event) {
        Logger.d(ContentDiscoveryEvent.TAG, (this.contentDiscoveryListener != null ? ZapConstants.GOOGLE_ACCOUNT_NONE : "not ") + "sent:" + " status=" + event.status + " substatus=" + event.substatus + " " + getPrintableSignature());
        if (this.contentDiscoveryListener != null) {
            this.contentDiscoveryHandler.post(new Runnable() {
                public void run() {
                    ContentContainer.this.contentDiscoveryListener.onContentDiscoveryEvent(event);
                }
            });
        }
    }

    public synchronized void setContentDownloadListener(ContentDownloadListener listener) {
        this.contentDownloadListener = listener;
        this.contentDownloadHandler = listener != null ? getHandler() : null;
    }

    public synchronized ContentDownloadListener getContentDownloadListener() {
        return this.contentDownloadListener;
    }

    public synchronized void onContentDownloadEvent(Status status, ContentDownloadEvent.Substatus substatus, Endpoint endpoint) {
        onContentDownloadEvent(new ContentDownloadEvent(status, substatus, this, endpoint));
    }

    public synchronized void onContentDownloadEvent(final ContentDownloadEvent event) {
        Logger.d(ContentDownloadEvent.TAG, (this.contentDownloadListener != null ? ZapConstants.GOOGLE_ACCOUNT_NONE : "not ") + "sent:" + " status=" + event.status + " substatus=" + event.substatus + " " + getPrintableSignature());
        if (this.contentDownloadListener != null) {
            this.contentDownloadHandler.post(new Runnable() {
                public void run() {
                    ContentContainer.this.contentDownloadListener.onContentDownloadEvent(event);
                }
            });
        }
    }

    public synchronized void setContentShareListener(ContentShareListener listener) {
        this.contentShareListener = listener;
        this.contentShareHandler = listener != null ? getHandler() : null;
    }

    public synchronized ContentShareListener getContentShareListener() {
        return this.contentShareListener;
    }

    public synchronized void onContentShareEvent(Status status, ContentShareEvent.Substatus substatus, Endpoint endpoint) {
        onContentShareEvent(new ContentShareEvent(status, substatus, this, endpoint));
    }

    public synchronized void onContentShareEvent(final ContentShareEvent event) {
        Logger.d(ContentShareEvent.TAG, (this.contentShareListener != null ? ZapConstants.GOOGLE_ACCOUNT_NONE : "not ") + "sent:" + " status=" + event.status + " substatus=" + event.substatus + " " + getPrintableSignature());
        if (event.substatus == ContentShareEvent.Substatus.SHARED) {
            this.sharedTime = System.currentTimeMillis();
            this.sharedElapsedTime = SystemClock.elapsedRealtime();
        }
        if (this.contentShareListener != null) {
            this.contentShareHandler.post(new Runnable() {
                public void run() {
                    ContentContainer.this.contentShareListener.onContentShareEvent(event);
                }
            });
        }
    }

    public synchronized void onContentStopShareEvent(Status status, ContentStopShareEvent.Substatus substatus, Endpoint endpoint) {
        onContentStopShareEvent(new ContentStopShareEvent(status, substatus, this, endpoint));
    }

    public synchronized void onContentStopShareEvent(final ContentStopShareEvent event) {
        Logger.d(ContentStopShareEvent.TAG, (this.contentShareListener != null ? ZapConstants.GOOGLE_ACCOUNT_NONE : "not ") + "sent:" + " status=" + event.status + " substatus=" + event.substatus + " " + getPrintableSignature());
        if (this.contentShareListener != null) {
            this.contentShareHandler.post(new Runnable() {
                public void run() {
                    ContentContainer.this.contentShareListener.onContentStopShareEvent(event);
                }
            });
        }
    }

    public synchronized void onContentCancelShareEvent(Status status, ContentCancelShareEvent.Substatus substatus, Endpoint endpoint) {
        onContentCancelShareEvent(new ContentCancelShareEvent(status, substatus, this, endpoint));
    }

    public synchronized void onContentCancelShareEvent(final ContentCancelShareEvent event) {
        Logger.d(ContentCancelShareEvent.TAG, (this.contentShareListener != null ? ZapConstants.GOOGLE_ACCOUNT_NONE : "not ") + "sent:" + " status=" + event.status + " substatus=" + event.substatus + " " + getPrintableSignature());
        if (this.contentShareListener != null) {
            this.contentShareHandler.post(new Runnable() {
                public void run() {
                    ContentContainer.this.contentShareListener.onContentCancelShareEvent(event);
                }
            });
        }
    }

    public void onAbuseEvent(AbuseAction abuseAction, Endpoint endpoint) {
        switch (this.type) {
            case SHARE:
                ContentShareEvent shareEvent = new ContentShareEvent(Status.INFO, ContentShareEvent.Substatus.ABUSE, this, endpoint);
                shareEvent.abuseAction = abuseAction;
                onContentShareEvent(shareEvent);
                return;
            case RECEIVE:
                ContentDiscoveryEvent discoveryEvent = new ContentDiscoveryEvent(Status.INFO, Substatus.ABUSE, this, endpoint);
                discoveryEvent.abuseAction = abuseAction;
                onContentDiscoveryEvent(discoveryEvent);
                return;
            default:
                return;
        }
    }

    public synchronized void setReportAbuseListener(ReportAbuseListener listener) {
        this.reportAbuseListener = listener;
        this.reportAbuseHandler = listener != null ? getHandler() : null;
    }

    public synchronized ReportAbuseListener getReportAbuseListener() {
        return this.reportAbuseListener;
    }

    public synchronized void onReportAbuseEvent(Status status, ReportAbuseEvent.Substatus substatus, Endpoint endpoint) {
        onReportAbuseEvent(new ReportAbuseEvent(status, substatus, this, endpoint));
    }

    public synchronized void onReportAbuseEvent(final ReportAbuseEvent event) {
        Logger.d(ReportAbuseEvent.TAG, (this.reportAbuseListener != null ? ZapConstants.GOOGLE_ACCOUNT_NONE : "not ") + "sent:" + " status=" + event.status + " substatus=" + event.substatus + " " + getPrintableSignature());
        if (this.reportAbuseListener != null) {
            this.reportAbuseHandler.post(new Runnable() {
                public void run() {
                    ContentContainer.this.reportAbuseListener.onReportAbuseEvent(event);
                }
            });
        }
    }

    private Handler getHandler() {
        Looper looper = Looper.myLooper();
        if (looper == null) {
            looper = Looper.getMainLooper();
        }
        return new Handler(looper);
    }

    public String getPrintableSignature() {
        return "uid=" + this.uid + (this.shareId != null ? " sid=" + this.shareId : ZapConstants.GOOGLE_ACCOUNT_NONE);
    }

    public static ContentContainer generateCancelShareContainer(String shareId2) {
        if (TextUtils.isEmpty(shareId2)) {
            throw new IllegalArgumentException("share id is empty");
        }
        ContentContainer container = new ContentContainer(Type.CANCEL_SHARE);
        container.shareId = shareId2;
        return container;
    }

    public static String generateDownloadPath(ContentContainer contentContainer) throws IOException {
        String str;
        String str2;
        String format;
        String format2;
        File file = new File((String) Settings.DOWNLOADS_DIRECTORY.get());
        if (file.exists() || file.mkdirs()) {
            if (TextUtils.isEmpty(contentContainer.name)) {
                str2 = Defaults.BASE_FILE_NAME;
                str = FileUtils.getFileExtensionFromMimeType(contentContainer.mimeType);
            } else {
                String str3 = contentContainer.name;
                int lastIndexOf = str3.lastIndexOf(46);
                if (lastIndexOf > 0) {
                    str2 = str3.substring(0, lastIndexOf);
                    str = str3.substring(lastIndexOf + 1);
                } else {
                    str = null;
                    str2 = str3;
                }
            }
            String str4 = file.getAbsolutePath() + File.separator;
            if (TextUtils.isEmpty(str)) {
                format = String.format(str4 + "%s", new Object[]{str2});
            } else {
                format = String.format(str4 + "%s.%s", new Object[]{str2, str});
            }
            if (!FileUtils.isFileExists(format)) {
                return format;
            }
            long currentTimeMillis = System.currentTimeMillis();
            while (true) {
                String l = Long.toString(currentTimeMillis);
                if (TextUtils.isEmpty(str)) {
                    format2 = String.format(str4 + "%s_%s", new Object[]{str2, l});
                } else {
                    format2 = String.format(str4 + "%s_%s.%s", new Object[]{str2, l, str});
                }
                if (!FileUtils.isFileExists(format2)) {
                    return format2;
                }
                currentTimeMillis++;
            }
        } else {
            throw new IOException("could not create download directory");
        }
    }

    private static boolean containsWhitespace(String s) {
        if (s == null) {
            return false;
        }
        for (int i = 0; i < s.length(); i++) {
            if (Character.isWhitespace(s.charAt(i))) {
                return true;
            }
        }
        return false;
    }
}
