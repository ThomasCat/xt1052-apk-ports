package com.motorola.zap.app;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;
import com.motorola.zap.LogUtils;
import com.motorola.zap.provider.ZapContract;

public class DroidBlastLauncherReceiver extends BroadcastReceiver {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;

    public void onReceive(Context context, Intent intent) {
        if (DBG) {
            Log.d("Zap-BlastLaunchRcvr", "onReceive invoked for action : " + intent.getAction());
        }
        PackageManager pm = context.getPackageManager();
        if (pm.hasSystemFeature(ZapContract.AUTHORITY)) {
            boolean needLauncher = needLauncher(context, pm);
            if (DBG) {
                Log.d("Zap-BlastLaunchRcvr", (needLauncher ? "enable" : "disable") + " launcher and enable global settings");
            }
            pm.setComponentEnabledSetting(new ComponentName(ZapContract.AUTHORITY, "com.motorola.zap.ui.DroidBlastLauncher"), needLauncher ? 1 : 2, 1);
            pm.setComponentEnabledSetting(new ComponentName(ZapContract.AUTHORITY, "com.motorola.zap.AliasDroidBlastSettingsActivity"), 1, 1);
        }
    }

    private boolean needLauncher(Context context, PackageManager pm) {
        try {
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            if (pi != null && pi.versionCode >= 50) {
                return true;
            }
            return false;
        } catch (NameNotFoundException e) {
            return false;
        }
    }
}
