package com.motorola.zap.ui;

public class DrawerActionItem {
    private int mIconRes;
    private int mStringRes;
    private int mType;

    public DrawerActionItem(int type, int iconRes, int stringRes) {
        this.mType = type;
        this.mStringRes = stringRes;
        this.mIconRes = iconRes;
    }

    public static DrawerActionItem create(int type, int iconRes, int stringRes) {
        return new DrawerActionItem(type, iconRes, stringRes);
    }

    public int getStringRes() {
        return this.mStringRes;
    }

    public int getIconRes() {
        return this.mIconRes;
    }

    public int getType() {
        return this.mType;
    }
}
