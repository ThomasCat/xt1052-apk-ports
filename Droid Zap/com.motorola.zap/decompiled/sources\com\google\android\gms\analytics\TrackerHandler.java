package com.google.android.gms.analytics;

import java.util.Map;

abstract class TrackerHandler {
    TrackerHandler() {
    }

    /* access modifiers changed from: 0000 */
    public abstract void n(Map<String, String> map);
}
