package com.google.tagmanager;

import com.google.analytics.containertag.common.Key;
import com.google.analytics.containertag.proto.Serving.FunctionCall;
import com.google.analytics.containertag.proto.Serving.Property;
import com.google.analytics.containertag.proto.Serving.Resource;
import com.google.analytics.containertag.proto.Serving.Rule;
import com.google.analytics.containertag.proto.Serving.ServingValue;
import com.google.analytics.midtier.proto.containertag.TypeSystem.Value;
import com.motorola.zap.ZapConstants;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

class ResourceUtil {

    public static class ExpandedFunctionCall {
        private final Map<String, Value> mPropertiesMap;
        private final Value mPushAfterEvaluate;

        private ExpandedFunctionCall(Map<String, Value> propertiesMap, Value pushAfterEvaluate) {
            this.mPropertiesMap = propertiesMap;
            this.mPushAfterEvaluate = pushAfterEvaluate;
        }

        public static ExpandedFunctionCallBuilder newBuilder() {
            return new ExpandedFunctionCallBuilder();
        }

        public void updateCacheableProperty(String key, Value v) {
            this.mPropertiesMap.put(key, v);
        }

        public Map<String, Value> getProperties() {
            return Collections.unmodifiableMap(this.mPropertiesMap);
        }

        public Value getPushAfterEvaluate() {
            return this.mPushAfterEvaluate;
        }

        public String toString() {
            return "Properties: " + getProperties() + " pushAfterEvaluate: " + this.mPushAfterEvaluate;
        }
    }

    public static class ExpandedFunctionCallBuilder {
        private final Map<String, Value> mPropertiesMap;
        private Value mPushAfterEvaluate;

        private ExpandedFunctionCallBuilder() {
            this.mPropertiesMap = new HashMap();
        }

        public ExpandedFunctionCallBuilder addProperty(String key, Value value) {
            this.mPropertiesMap.put(key, value);
            return this;
        }

        public ExpandedFunctionCallBuilder setPushAfterEvaluate(Value value) {
            this.mPushAfterEvaluate = value;
            return this;
        }

        public ExpandedFunctionCall build() {
            return new ExpandedFunctionCall(this.mPropertiesMap, this.mPushAfterEvaluate);
        }
    }

    public static class ExpandedResource {
        private final Map<String, List<ExpandedFunctionCall>> mMacros;
        private final int mResourceFormatVersion;
        private final List<ExpandedRule> mRules;
        private final String mVersion;

        private ExpandedResource(List<ExpandedRule> rules, Map<String, List<ExpandedFunctionCall>> macros, String version, int resourceFormatVersion) {
            this.mRules = Collections.unmodifiableList(rules);
            this.mMacros = Collections.unmodifiableMap(macros);
            this.mVersion = version;
            this.mResourceFormatVersion = resourceFormatVersion;
        }

        public static ExpandedResourceBuilder newBuilder() {
            return new ExpandedResourceBuilder();
        }

        public List<ExpandedRule> getRules() {
            return this.mRules;
        }

        public String getVersion() {
            return this.mVersion;
        }

        public int getResourceFormatVersion() {
            return this.mResourceFormatVersion;
        }

        public List<ExpandedFunctionCall> getMacros(String name) {
            return (List) this.mMacros.get(name);
        }

        public Map<String, List<ExpandedFunctionCall>> getAllMacros() {
            return this.mMacros;
        }

        public String toString() {
            return "Rules: " + getRules() + "  Macros: " + this.mMacros;
        }
    }

    public static class ExpandedResourceBuilder {
        private final Map<String, List<ExpandedFunctionCall>> mMacros;
        private int mResourceFormatVersion;
        private final List<ExpandedRule> mRules;
        private String mVersion;

        private ExpandedResourceBuilder() {
            this.mRules = new ArrayList();
            this.mMacros = new HashMap();
            this.mVersion = ZapConstants.GOOGLE_ACCOUNT_NONE;
            this.mResourceFormatVersion = 0;
        }

        public ExpandedResourceBuilder addRule(ExpandedRule r) {
            this.mRules.add(r);
            return this;
        }

        public ExpandedResourceBuilder addMacro(ExpandedFunctionCall f) {
            String macroName = Types.valueToString((Value) f.getProperties().get(Key.INSTANCE_NAME.toString()));
            List<ExpandedFunctionCall> macroList = (List) this.mMacros.get(macroName);
            if (macroList == null) {
                macroList = new ArrayList<>();
                this.mMacros.put(macroName, macroList);
            }
            macroList.add(f);
            return this;
        }

        public ExpandedResourceBuilder setVersion(String version) {
            this.mVersion = version;
            return this;
        }

        public ExpandedResourceBuilder setResourceFormatVersion(int resourceFormatVersion) {
            this.mResourceFormatVersion = resourceFormatVersion;
            return this;
        }

        public ExpandedResource build() {
            return new ExpandedResource(this.mRules, this.mMacros, this.mVersion, this.mResourceFormatVersion);
        }
    }

    public static class ExpandedRule {
        private final List<String> mAddMacroRuleNames;
        private final List<ExpandedFunctionCall> mAddMacros;
        private final List<String> mAddTagRuleNames;
        private final List<ExpandedFunctionCall> mAddTags;
        private final List<ExpandedFunctionCall> mNegativePredicates;
        private final List<ExpandedFunctionCall> mPositivePredicates;
        private final List<String> mRemoveMacroRuleNames;
        private final List<ExpandedFunctionCall> mRemoveMacros;
        private final List<String> mRemoveTagRuleNames;
        private final List<ExpandedFunctionCall> mRemoveTags;

        private ExpandedRule(List<ExpandedFunctionCall> postivePredicates, List<ExpandedFunctionCall> negativePredicates, List<ExpandedFunctionCall> addTags, List<ExpandedFunctionCall> removeTags, List<ExpandedFunctionCall> addMacros, List<ExpandedFunctionCall> removeMacros, List<String> addMacroRuleNames, List<String> removeMacroRuleNames, List<String> addTagRuleNames, List<String> removeTagRuleNames) {
            this.mPositivePredicates = Collections.unmodifiableList(postivePredicates);
            this.mNegativePredicates = Collections.unmodifiableList(negativePredicates);
            this.mAddTags = Collections.unmodifiableList(addTags);
            this.mRemoveTags = Collections.unmodifiableList(removeTags);
            this.mAddMacros = Collections.unmodifiableList(addMacros);
            this.mRemoveMacros = Collections.unmodifiableList(removeMacros);
            this.mAddMacroRuleNames = Collections.unmodifiableList(addMacroRuleNames);
            this.mRemoveMacroRuleNames = Collections.unmodifiableList(removeMacroRuleNames);
            this.mAddTagRuleNames = Collections.unmodifiableList(addTagRuleNames);
            this.mRemoveTagRuleNames = Collections.unmodifiableList(removeTagRuleNames);
        }

        public static ExpandedRuleBuilder newBuilder() {
            return new ExpandedRuleBuilder();
        }

        public List<ExpandedFunctionCall> getPositivePredicates() {
            return this.mPositivePredicates;
        }

        public List<ExpandedFunctionCall> getNegativePredicates() {
            return this.mNegativePredicates;
        }

        public List<ExpandedFunctionCall> getAddTags() {
            return this.mAddTags;
        }

        public List<ExpandedFunctionCall> getRemoveTags() {
            return this.mRemoveTags;
        }

        public List<ExpandedFunctionCall> getAddMacros() {
            return this.mAddMacros;
        }

        public List<String> getAddMacroRuleNames() {
            return this.mAddMacroRuleNames;
        }

        public List<String> getRemoveMacroRuleNames() {
            return this.mRemoveMacroRuleNames;
        }

        public List<String> getAddTagRuleNames() {
            return this.mAddTagRuleNames;
        }

        public List<String> getRemoveTagRuleNames() {
            return this.mRemoveTagRuleNames;
        }

        public List<ExpandedFunctionCall> getRemoveMacros() {
            return this.mRemoveMacros;
        }

        public String toString() {
            return "Positive predicates: " + getPositivePredicates() + "  Negative predicates: " + getNegativePredicates() + "  Add tags: " + getAddTags() + "  Remove tags: " + getRemoveTags() + "  Add macros: " + getAddMacros() + "  Remove macros: " + getRemoveMacros();
        }
    }

    public static class ExpandedRuleBuilder {
        private final List<String> mAddMacroRuleNames;
        private final List<ExpandedFunctionCall> mAddMacros;
        private final List<String> mAddTagRuleNames;
        private final List<ExpandedFunctionCall> mAddTags;
        private final List<ExpandedFunctionCall> mNegativePredicates;
        private final List<ExpandedFunctionCall> mPositivePredicates;
        private final List<String> mRemoveMacroRuleNames;
        private final List<ExpandedFunctionCall> mRemoveMacros;
        private final List<String> mRemoveTagRuleNames;
        private final List<ExpandedFunctionCall> mRemoveTags;

        private ExpandedRuleBuilder() {
            this.mPositivePredicates = new ArrayList();
            this.mNegativePredicates = new ArrayList();
            this.mAddTags = new ArrayList();
            this.mRemoveTags = new ArrayList();
            this.mAddMacros = new ArrayList();
            this.mRemoveMacros = new ArrayList();
            this.mAddMacroRuleNames = new ArrayList();
            this.mRemoveMacroRuleNames = new ArrayList();
            this.mAddTagRuleNames = new ArrayList();
            this.mRemoveTagRuleNames = new ArrayList();
        }

        public ExpandedRuleBuilder addPositivePredicate(ExpandedFunctionCall f) {
            this.mPositivePredicates.add(f);
            return this;
        }

        public ExpandedRuleBuilder addNegativePredicate(ExpandedFunctionCall f) {
            this.mNegativePredicates.add(f);
            return this;
        }

        public ExpandedRuleBuilder addAddTag(ExpandedFunctionCall f) {
            this.mAddTags.add(f);
            return this;
        }

        public ExpandedRuleBuilder addAddTagRuleName(String ruleName) {
            this.mAddTagRuleNames.add(ruleName);
            return this;
        }

        public ExpandedRuleBuilder addRemoveTag(ExpandedFunctionCall f) {
            this.mRemoveTags.add(f);
            return this;
        }

        public ExpandedRuleBuilder addRemoveTagRuleName(String ruleName) {
            this.mRemoveTagRuleNames.add(ruleName);
            return this;
        }

        public ExpandedRuleBuilder addAddMacro(ExpandedFunctionCall f) {
            this.mAddMacros.add(f);
            return this;
        }

        public ExpandedRuleBuilder addAddMacroRuleName(String ruleName) {
            this.mAddMacroRuleNames.add(ruleName);
            return this;
        }

        public ExpandedRuleBuilder addRemoveMacro(ExpandedFunctionCall f) {
            this.mRemoveMacros.add(f);
            return this;
        }

        public ExpandedRuleBuilder addRemoveMacroRuleName(String ruleName) {
            this.mRemoveMacroRuleNames.add(ruleName);
            return this;
        }

        public ExpandedRule build() {
            return new ExpandedRule(this.mPositivePredicates, this.mNegativePredicates, this.mAddTags, this.mRemoveTags, this.mAddMacros, this.mRemoveMacros, this.mAddMacroRuleNames, this.mRemoveMacroRuleNames, this.mAddTagRuleNames, this.mRemoveTagRuleNames);
        }
    }

    public static class InvalidResourceException extends Exception {
        public InvalidResourceException(String s) {
            super(s);
        }
    }

    private ResourceUtil() {
    }

    public static ExpandedResource getExpandedResource(Resource resource) throws InvalidResourceException {
        Value[] expandedValues = new Value[resource.value.length];
        for (int i = 0; i < resource.value.length; i++) {
            expandValue(i, resource, expandedValues, new HashSet(0));
        }
        ExpandedResourceBuilder builder = ExpandedResource.newBuilder();
        List<ExpandedFunctionCall> tags = new ArrayList<>();
        for (int i2 = 0; i2 < resource.tag.length; i2++) {
            tags.add(expandFunctionCall(resource.tag[i2], resource, expandedValues, i2));
        }
        List<ExpandedFunctionCall> predicates = new ArrayList<>();
        for (int i3 = 0; i3 < resource.predicate.length; i3++) {
            predicates.add(expandFunctionCall(resource.predicate[i3], resource, expandedValues, i3));
        }
        List<ExpandedFunctionCall> macros = new ArrayList<>();
        for (int i4 = 0; i4 < resource.macro.length; i4++) {
            ExpandedFunctionCall thisMacro = expandFunctionCall(resource.macro[i4], resource, expandedValues, i4);
            builder.addMacro(thisMacro);
            macros.add(thisMacro);
        }
        for (Rule r : resource.rule) {
            builder.addRule(expandRule(r, tags, macros, predicates, resource));
        }
        builder.setVersion(resource.version);
        builder.setResourceFormatVersion(resource.resourceFormatVersion);
        return builder.build();
    }

    public static Value newValueBasedOnValue(Value v) {
        Value result = new Value();
        result.type = v.type;
        result.escaping = (int[]) v.escaping.clone();
        if (v.containsReferences) {
            result.containsReferences = v.containsReferences;
        }
        return result;
    }

    private static Value expandValue(int i, Resource resource, Value[] valueArr, Set<Integer> set) throws InvalidResourceException {
        int i2 = 0;
        if (set.contains(Integer.valueOf(i))) {
            logAndThrow("Value cycle detected.  Current value reference: " + i + "." + "  Previous value references: " + set + ".");
        }
        Value value = (Value) getWithBoundsCheck(resource.value, i, "values");
        if (valueArr[i] != null) {
            return valueArr[i];
        }
        Value value2 = null;
        set.add(Integer.valueOf(i));
        switch (value.type) {
            case 1:
            case 5:
            case 6:
            case 8:
                value2 = value;
                break;
            case 2:
                ServingValue servingValue = getServingValue(value);
                value2 = newValueBasedOnValue(value);
                value2.listItem = new Value[servingValue.listItem.length];
                int[] iArr = servingValue.listItem;
                int length = iArr.length;
                int i3 = 0;
                while (i2 < length) {
                    int i4 = i3 + 1;
                    value2.listItem[i3] = expandValue(iArr[i2], resource, valueArr, set);
                    i2++;
                    i3 = i4;
                }
                break;
            case 3:
                value2 = newValueBasedOnValue(value);
                ServingValue servingValue2 = getServingValue(value);
                if (servingValue2.mapKey.length != servingValue2.mapValue.length) {
                    logAndThrow("Uneven map keys (" + servingValue2.mapKey.length + ") and map values (" + servingValue2.mapValue.length + ")");
                }
                value2.mapKey = new Value[servingValue2.mapKey.length];
                value2.mapValue = new Value[servingValue2.mapKey.length];
                int[] iArr2 = servingValue2.mapKey;
                int length2 = iArr2.length;
                int i5 = 0;
                int i6 = 0;
                while (i5 < length2) {
                    int i7 = i6 + 1;
                    value2.mapKey[i6] = expandValue(iArr2[i5], resource, valueArr, set);
                    i5++;
                    i6 = i7;
                }
                int[] iArr3 = servingValue2.mapValue;
                int length3 = iArr3.length;
                int i8 = 0;
                while (i2 < length3) {
                    int i9 = i8 + 1;
                    value2.mapValue[i8] = expandValue(iArr3[i2], resource, valueArr, set);
                    i2++;
                    i8 = i9;
                }
                break;
            case 4:
                value2 = newValueBasedOnValue(value);
                value2.macroReference = Types.valueToString(expandValue(getServingValue(value).macroNameReference, resource, valueArr, set));
                break;
            case 7:
                value2 = newValueBasedOnValue(value);
                ServingValue servingValue3 = getServingValue(value);
                value2.templateToken = new Value[servingValue3.templateToken.length];
                int[] iArr4 = servingValue3.templateToken;
                int length4 = iArr4.length;
                int i10 = 0;
                while (i2 < length4) {
                    int i11 = i10 + 1;
                    value2.templateToken[i10] = expandValue(iArr4[i2], resource, valueArr, set);
                    i2++;
                    i10 = i11;
                }
                break;
        }
        if (value2 == null) {
            logAndThrow("Invalid value: " + value);
        }
        valueArr[i] = value2;
        set.remove(Integer.valueOf(i));
        return value2;
    }

    private static ServingValue getServingValue(Value value) throws InvalidResourceException {
        if (((ServingValue) value.getExtension(ServingValue.ext)) == null) {
            logAndThrow("Expected a ServingValue and didn't get one. Value is: " + value);
        }
        return (ServingValue) value.getExtension(ServingValue.ext);
    }

    private static void logAndThrow(String error) throws InvalidResourceException {
        Log.e(error);
        throw new InvalidResourceException(error);
    }

    private static <T> T getWithBoundsCheck(T[] tArr, int i, String str) throws InvalidResourceException {
        if (i < 0 || i >= tArr.length) {
            logAndThrow("Index out of bounds detected: " + i + " in " + str);
        }
        return tArr[i];
    }

    private static ExpandedFunctionCall expandFunctionCall(FunctionCall functionCall, Resource resource, Value[] valueArr, int i) throws InvalidResourceException {
        ExpandedFunctionCallBuilder newBuilder = ExpandedFunctionCall.newBuilder();
        for (int valueOf : functionCall.property) {
            Property property = (Property) getWithBoundsCheck(resource.property, Integer.valueOf(valueOf).intValue(), "properties");
            String str = (String) getWithBoundsCheck(resource.key, property.key, "keys");
            Value value = (Value) getWithBoundsCheck(valueArr, property.value, "values");
            if (Key.PUSH_AFTER_EVALUATE.toString().equals(str)) {
                newBuilder.setPushAfterEvaluate(value);
            } else {
                newBuilder.addProperty(str, value);
            }
        }
        return newBuilder.build();
    }

    private static ExpandedRule expandRule(Rule rule, List<ExpandedFunctionCall> tags, List<ExpandedFunctionCall> macros, List<ExpandedFunctionCall> predicates, Resource resource) {
        ExpandedRuleBuilder ruleBuilder = ExpandedRule.newBuilder();
        for (int valueOf : rule.positivePredicate) {
            ruleBuilder.addPositivePredicate((ExpandedFunctionCall) predicates.get(Integer.valueOf(valueOf).intValue()));
        }
        for (int valueOf2 : rule.negativePredicate) {
            ruleBuilder.addNegativePredicate((ExpandedFunctionCall) predicates.get(Integer.valueOf(valueOf2).intValue()));
        }
        for (int valueOf3 : rule.addTag) {
            ruleBuilder.addAddTag((ExpandedFunctionCall) tags.get(Integer.valueOf(valueOf3).intValue()));
        }
        for (int valueOf4 : rule.addTagRuleName) {
            ruleBuilder.addAddTagRuleName(resource.value[Integer.valueOf(valueOf4).intValue()].string);
        }
        for (int valueOf5 : rule.removeTag) {
            ruleBuilder.addRemoveTag((ExpandedFunctionCall) tags.get(Integer.valueOf(valueOf5).intValue()));
        }
        for (int valueOf6 : rule.removeTagRuleName) {
            ruleBuilder.addRemoveTagRuleName(resource.value[Integer.valueOf(valueOf6).intValue()].string);
        }
        for (int valueOf7 : rule.addMacro) {
            ruleBuilder.addAddMacro((ExpandedFunctionCall) macros.get(Integer.valueOf(valueOf7).intValue()));
        }
        for (int valueOf8 : rule.addMacroRuleName) {
            ruleBuilder.addAddMacroRuleName(resource.value[Integer.valueOf(valueOf8).intValue()].string);
        }
        for (int valueOf9 : rule.removeMacro) {
            ruleBuilder.addRemoveMacro((ExpandedFunctionCall) macros.get(Integer.valueOf(valueOf9).intValue()));
        }
        for (int valueOf10 : rule.removeMacroRuleName) {
            ruleBuilder.addRemoveMacroRuleName(resource.value[Integer.valueOf(valueOf10).intValue()].string);
        }
        return ruleBuilder.build();
    }

    public static void copyStream(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        while (true) {
            int numBytes = in.read(buffer);
            if (numBytes != -1) {
                out.write(buffer, 0, numBytes);
            } else {
                return;
            }
        }
    }
}
