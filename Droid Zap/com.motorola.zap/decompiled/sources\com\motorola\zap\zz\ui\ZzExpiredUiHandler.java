package com.motorola.zap.zz.ui;

import com.motorola.sharing.GroupListener.GroupEvent;
import com.motorola.zap.R;
import com.motorola.zap.ui.ZapUi;

public class ZzExpiredUiHandler extends BaseZzUiHandler {
    /* access modifiers changed from: protected */
    public void handleOk(GroupEvent ge) {
        ZapUi.getInstance().showToastMessage(getString(R.string.toast_msg_zz_ended), ZapUi.getZzToastIconId(), true, true);
        cancelZzActiveNotification();
    }
}
