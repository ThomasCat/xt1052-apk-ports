package com.google.android.gms.internal;

import com.google.android.gms.internal.ka;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class kb<M extends ka<M>, T> {
    protected final Class<T> aaf;
    protected final boolean aag;
    protected final int tag;
    protected final int type;

    private kb(int i, Class<T> cls, int i2, boolean z) {
        this.type = i;
        this.aaf = cls;
        this.tag = i2;
        this.aag = z;
    }

    public static <M extends ka<M>, T extends ke> kb<M, T> a(int i, Class<T> cls, int i2) {
        return new kb<>(i, cls, i2, false);
    }

    /* access modifiers changed from: protected */
    public void a(kg kgVar, List<Object> list) {
        list.add(o(jy.n(kgVar.aai)));
    }

    /* access modifiers changed from: protected */
    public boolean cI(int i) {
        return i == this.tag;
    }

    /* access modifiers changed from: 0000 */
    public final T g(List<kg> list) {
        if (list == null) {
            return null;
        }
        if (this.aag) {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < list.size(); i++) {
                kg kgVar = (kg) list.get(i);
                if (cI(kgVar.tag) && kgVar.aai.length != 0) {
                    a(kgVar, arrayList);
                }
            }
            int size = arrayList.size();
            if (size == 0) {
                return null;
            }
            T cast = this.aaf.cast(Array.newInstance(this.aaf.getComponentType(), size));
            for (int i2 = 0; i2 < size; i2++) {
                Array.set(cast, i2, arrayList.get(i2));
            }
            return cast;
        }
        int size2 = list.size() - 1;
        kg kgVar2 = null;
        while (kgVar2 == null && size2 >= 0) {
            kg kgVar3 = (kg) list.get(size2);
            if (!cI(kgVar3.tag) || kgVar3.aai.length == 0) {
                kgVar3 = kgVar2;
            }
            size2--;
            kgVar2 = kgVar3;
        }
        if (kgVar2 == null) {
            return null;
        }
        return this.aaf.cast(o(jy.n(kgVar2.aai)));
    }

    /* access modifiers changed from: protected */
    public Object o(jy jyVar) {
        Class<T> cls = this.aag ? this.aaf.getComponentType() : this.aaf;
        try {
            switch (this.type) {
                case 10:
                    ke keVar = (ke) cls.newInstance();
                    jyVar.a(keVar, kh.cK(this.tag));
                    return keVar;
                case 11:
                    ke keVar2 = (ke) cls.newInstance();
                    jyVar.a(keVar2);
                    return keVar2;
                default:
                    throw new IllegalArgumentException("Unknown type " + this.type);
            }
        } catch (InstantiationException e) {
            throw new IllegalArgumentException("Error creating instance of class " + cls, e);
        } catch (IllegalAccessException e2) {
            throw new IllegalArgumentException("Error creating instance of class " + cls, e2);
        } catch (IOException e3) {
            throw new IllegalArgumentException("Error reading extension field", e3);
        }
    }
}
