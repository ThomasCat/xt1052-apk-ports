package com.google.tagmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.tagmanager.DataLayer.PersistentStore.Callback;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

class DataLayerPersistentStoreImpl implements PersistentStore {
    /* access modifiers changed from: private */
    public static final String CREATE_MAPS_TABLE = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' STRING NOT NULL, '%s' BLOB NOT NULL, '%s' INTEGER NOT NULL);", new Object[]{"datalayer", "ID", "key", "value", "expires"});
    private Clock mClock;
    /* access modifiers changed from: private */
    public final Context mContext;
    private DatabaseHelper mDbHelper;
    private final Executor mExecutor;
    private int mMaxNumStoredItems;

    @VisibleForTesting
    class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context, String databaseName) {
            super(context, databaseName, null, 1);
        }

        private boolean tablePresent(String table, SQLiteDatabase db) {
            Cursor cursor = null;
            try {
                SQLiteDatabase sQLiteDatabase = db;
                cursor = sQLiteDatabase.query("SQLITE_MASTER", new String[]{"name"}, "name=?", new String[]{table}, null, null, null);
                boolean moveToFirst = cursor.moveToFirst();
                if (cursor == null) {
                    return moveToFirst;
                }
                cursor.close();
                return moveToFirst;
            } catch (SQLiteException e) {
                Log.w("Error querying for table " + table);
                if (cursor != null) {
                    cursor.close();
                }
                return false;
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }

        public SQLiteDatabase getWritableDatabase() {
            SQLiteDatabase db = null;
            try {
                db = super.getWritableDatabase();
            } catch (SQLiteException e) {
                DataLayerPersistentStoreImpl.this.mContext.getDatabasePath("google_tagmanager.db").delete();
            }
            if (db == null) {
                return super.getWritableDatabase();
            }
            return db;
        }

        public void onOpen(SQLiteDatabase db) {
            if (VERSION.SDK_INT < 15) {
                Cursor cursor = db.rawQuery("PRAGMA journal_mode=memory", null);
                try {
                    cursor.moveToFirst();
                } finally {
                    cursor.close();
                }
            }
            if (!tablePresent("datalayer", db)) {
                db.execSQL(DataLayerPersistentStoreImpl.CREATE_MAPS_TABLE);
            } else {
                validateColumnsPresent(db);
            }
        }

        /* JADX INFO: finally extract failed */
        private void validateColumnsPresent(SQLiteDatabase db) {
            Cursor c = db.rawQuery("SELECT * FROM datalayer WHERE 0", null);
            Set<String> columns = new HashSet<>();
            try {
                String[] columnNames = c.getColumnNames();
                for (String add : columnNames) {
                    columns.add(add);
                }
                c.close();
                if (!columns.remove("key") || !columns.remove("value") || !columns.remove("ID") || !columns.remove("expires")) {
                    throw new SQLiteException("Database column missing");
                } else if (!columns.isEmpty()) {
                    throw new SQLiteException("Database has extra columns");
                }
            } catch (Throwable th) {
                c.close();
                throw th;
            }
        }

        public void onCreate(SQLiteDatabase db) {
            FutureApis.setOwnerOnlyReadWrite(db.getPath());
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    private static class KeyAndSerialized {
        final String mKey;
        final byte[] mSerialized;

        KeyAndSerialized(String key, byte[] serialized) {
            this.mKey = key;
            this.mSerialized = serialized;
        }

        public String toString() {
            return "KeyAndSerialized: key = " + this.mKey + " serialized hash = " + Arrays.hashCode(this.mSerialized);
        }
    }

    public DataLayerPersistentStoreImpl(Context context) {
        this(context, new Clock() {
            public long currentTimeMillis() {
                return System.currentTimeMillis();
            }
        }, "google_tagmanager.db", 2000, Executors.newSingleThreadExecutor());
    }

    @VisibleForTesting
    DataLayerPersistentStoreImpl(Context context, Clock clock, String databaseName, int maxNumStoredItems, Executor executor) {
        this.mContext = context;
        this.mClock = clock;
        this.mMaxNumStoredItems = maxNumStoredItems;
        this.mExecutor = executor;
        this.mDbHelper = new DatabaseHelper(this.mContext, databaseName);
    }

    public void saveKeyValues(List<KeyValue> keysAndValues, final long lifetimeInMillis) {
        final List<KeyAndSerialized> serializedKeysAndValues = serializeValues(keysAndValues);
        this.mExecutor.execute(new Runnable() {
            public void run() {
                DataLayerPersistentStoreImpl.this.saveSingleThreaded(serializedKeysAndValues, lifetimeInMillis);
            }
        });
    }

    public void loadSaved(final Callback callback) {
        this.mExecutor.execute(new Runnable() {
            public void run() {
                callback.onKeyValuesLoaded(DataLayerPersistentStoreImpl.this.loadSingleThreaded());
            }
        });
    }

    public void clearKeysWithPrefix(final String keyPrefix) {
        this.mExecutor.execute(new Runnable() {
            public void run() {
                DataLayerPersistentStoreImpl.this.clearKeysWithPrefixSingleThreaded(keyPrefix);
            }
        });
    }

    /* access modifiers changed from: private */
    public List<KeyValue> loadSingleThreaded() {
        try {
            deleteEntriesOlderThan(this.mClock.currentTimeMillis());
            return unserializeValues(loadSerialized());
        } finally {
            closeDatabaseConnection();
        }
    }

    private List<KeyValue> unserializeValues(List<KeyAndSerialized> serialized) {
        List<KeyValue> result = new ArrayList<>();
        for (KeyAndSerialized keyAndSerialized : serialized) {
            result.add(new KeyValue(keyAndSerialized.mKey, unserialize(keyAndSerialized.mSerialized)));
        }
        return result;
    }

    private List<KeyAndSerialized> serializeValues(List<KeyValue> keysAndValues) {
        List<KeyAndSerialized> result = new ArrayList<>();
        for (KeyValue keyAndValue : keysAndValues) {
            result.add(new KeyAndSerialized(keyAndValue.mKey, serialize(keyAndValue.mValue)));
        }
        return result;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0029 A[SYNTHETIC, Splitter:B:19:0x0029] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0035 A[SYNTHETIC, Splitter:B:25:0x0035] */
    private Object unserialize(byte[] bytes) {
        Object obj = null;
        ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);
        ObjectInputStream objectInput = null;
        try {
            ObjectInputStream objectInput2 = new ObjectInputStream(byteStream);
            try {
                obj = objectInput2.readObject();
                if (objectInput2 != null) {
                    try {
                        objectInput2.close();
                    } catch (IOException e) {
                    }
                }
                byteStream.close();
                ObjectInputStream objectInputStream = objectInput2;
            } catch (IOException e2) {
                objectInput = objectInput2;
            } catch (ClassNotFoundException e3) {
                objectInput = objectInput2;
                if (objectInput != null) {
                }
                byteStream.close();
                return obj;
            } catch (Throwable th) {
                th = th;
                objectInput = objectInput2;
                if (objectInput != null) {
                }
                byteStream.close();
                throw th;
            }
        } catch (IOException e4) {
            if (objectInput != null) {
                try {
                    objectInput.close();
                } catch (IOException e5) {
                }
            }
            byteStream.close();
            return obj;
        } catch (ClassNotFoundException e6) {
            if (objectInput != null) {
                try {
                    objectInput.close();
                } catch (IOException e7) {
                }
            }
            byteStream.close();
            return obj;
        } catch (Throwable th2) {
            th = th2;
            if (objectInput != null) {
                try {
                    objectInput.close();
                } catch (IOException e8) {
                    throw th;
                }
            }
            byteStream.close();
            throw th;
        }
        return obj;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x002c A[SYNTHETIC, Splitter:B:20:0x002c] */
    private byte[] serialize(Object o) {
        byte[] bArr;
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutput = null;
        try {
            ObjectOutputStream objectOutput2 = new ObjectOutputStream(byteStream);
            try {
                objectOutput2.writeObject(o);
                bArr = byteStream.toByteArray();
                if (objectOutput2 != null) {
                    try {
                        objectOutput2.close();
                    } catch (IOException e) {
                    }
                }
                byteStream.close();
                ObjectOutputStream objectOutputStream = objectOutput2;
            } catch (IOException e2) {
                objectOutput = objectOutput2;
            } catch (Throwable th) {
                th = th;
                objectOutput = objectOutput2;
                if (objectOutput != null) {
                }
                byteStream.close();
                throw th;
            }
        } catch (IOException e3) {
            bArr = null;
            if (objectOutput != null) {
                try {
                    objectOutput.close();
                } catch (IOException e4) {
                }
            }
            byteStream.close();
            return bArr;
        } catch (Throwable th2) {
            th = th2;
            if (objectOutput != null) {
                try {
                    objectOutput.close();
                } catch (IOException e5) {
                    throw th;
                }
            }
            byteStream.close();
            throw th;
        }
        return bArr;
    }

    /* access modifiers changed from: private */
    public synchronized void saveSingleThreaded(List<KeyAndSerialized> keysAndValues, long lifetimeInMillis) {
        try {
            long now = this.mClock.currentTimeMillis();
            deleteEntriesOlderThan(now);
            makeRoomForEntries(keysAndValues.size());
            writeEntriesToDatabase(keysAndValues, now + lifetimeInMillis);
            closeDatabaseConnection();
        } catch (Throwable th) {
            closeDatabaseConnection();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    private List<KeyAndSerialized> loadSerialized() {
        SQLiteDatabase writableDatabase = getWritableDatabase("Error opening database for loadSerialized.");
        ArrayList arrayList = new ArrayList();
        if (writableDatabase == null) {
            return arrayList;
        }
        Cursor query = writableDatabase.query("datalayer", new String[]{"key", "value"}, null, null, null, null, "ID", null);
        while (query.moveToNext()) {
            try {
                arrayList.add(new KeyAndSerialized(query.getString(0), query.getBlob(1)));
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        query.close();
        return arrayList;
    }

    private void writeEntriesToDatabase(List<KeyAndSerialized> list, long j) {
        SQLiteDatabase writableDatabase = getWritableDatabase("Error opening database for writeEntryToDatabase.");
        if (writableDatabase != null) {
            for (KeyAndSerialized keyAndSerialized : list) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("expires", Long.valueOf(j));
                contentValues.put("key", keyAndSerialized.mKey);
                contentValues.put("value", keyAndSerialized.mSerialized);
                writableDatabase.insert("datalayer", null, contentValues);
            }
        }
    }

    private void makeRoomForEntries(int i) {
        int numStoredEntries = (getNumStoredEntries() - this.mMaxNumStoredItems) + i;
        if (numStoredEntries > 0) {
            List peekEntryIds = peekEntryIds(numStoredEntries);
            Log.i("DataLayer store full, deleting " + peekEntryIds.size() + " entries to make room.");
            deleteEntries((String[]) peekEntryIds.toArray(new String[0]));
        }
    }

    /* access modifiers changed from: private */
    public void clearKeysWithPrefixSingleThreaded(String str) {
        SQLiteDatabase writableDatabase = getWritableDatabase("Error opening database for clearKeysWithPrefix.");
        if (writableDatabase != null) {
            try {
                Log.v("Cleared " + writableDatabase.delete("datalayer", "key = ? OR key LIKE ?", new String[]{str, str + ".%"}) + " items");
            } catch (SQLiteException e) {
                Log.w("Error deleting entries with key prefix: " + str + " (" + e + ").");
            } finally {
                closeDatabaseConnection();
            }
        }
    }

    private void deleteEntriesOlderThan(long j) {
        SQLiteDatabase writableDatabase = getWritableDatabase("Error opening database for deleteOlderThan.");
        if (writableDatabase != null) {
            try {
                Log.v("Deleted " + writableDatabase.delete("datalayer", "expires <= ?", new String[]{Long.toString(j)}) + " expired items");
            } catch (SQLiteException e) {
                Log.w("Error deleting old entries.");
            }
        }
    }

    private void deleteEntries(String[] strArr) {
        if (strArr != null && strArr.length != 0) {
            SQLiteDatabase writableDatabase = getWritableDatabase("Error opening database for deleteEntries.");
            if (writableDatabase != null) {
                try {
                    writableDatabase.delete("datalayer", String.format("%s in (%s)", new Object[]{"ID", TextUtils.join(",", Collections.nCopies(strArr.length, "?"))}), strArr);
                } catch (SQLiteException e) {
                    Log.w("Error deleting entries " + Arrays.toString(strArr));
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0082  */
    private List<String> peekEntryIds(int i) {
        Cursor cursor;
        ArrayList arrayList = new ArrayList();
        if (i <= 0) {
            Log.w("Invalid maxEntries specified. Skipping.");
            return arrayList;
        }
        SQLiteDatabase writableDatabase = getWritableDatabase("Error opening database for peekEntryIds.");
        if (writableDatabase == null) {
            return arrayList;
        }
        try {
            cursor = writableDatabase.query("datalayer", new String[]{"ID"}, null, null, null, null, String.format("%s ASC", new Object[]{"ID"}), Integer.toString(i));
            try {
                if (cursor.moveToFirst()) {
                    do {
                        arrayList.add(String.valueOf(cursor.getLong(0)));
                    } while (cursor.moveToNext());
                }
                if (cursor != null) {
                    cursor.close();
                }
            } catch (SQLiteException e) {
                e = e;
                try {
                    Log.w("Error in peekEntries fetching entryIds: " + e.getMessage());
                    if (cursor != null) {
                        cursor.close();
                    }
                    return arrayList;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e2) {
            e = e2;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
        return arrayList;
    }

    private int getNumStoredEntries() {
        Cursor cursor = null;
        int i = 0;
        SQLiteDatabase writableDatabase = getWritableDatabase("Error opening database for getNumStoredEntries.");
        if (writableDatabase != null) {
            try {
                Cursor rawQuery = writableDatabase.rawQuery("SELECT COUNT(*) from datalayer", null);
                if (rawQuery.moveToFirst()) {
                    i = (int) rawQuery.getLong(0);
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } catch (SQLiteException e) {
                Log.w("Error getting numStoredEntries");
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
        return i;
    }

    private SQLiteDatabase getWritableDatabase(String errorMessage) {
        try {
            return this.mDbHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            Log.w(errorMessage);
            return null;
        }
    }

    private void closeDatabaseConnection() {
        try {
            this.mDbHelper.close();
        } catch (SQLiteException e) {
        }
    }
}
