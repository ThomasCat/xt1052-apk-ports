package com.google.android.gms.drive.query;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a.C0006a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.drive.query.internal.LogicalFilter;

public class a implements Creator<Query> {
    static void a(Query query, Parcel parcel, int i) {
        int p = b.p(parcel);
        b.c(parcel, 1000, query.wj);
        b.a(parcel, 1, (Parcelable) query.EL, i, false);
        b.a(parcel, 3, query.EM, false);
        b.D(parcel, p);
    }

    /* renamed from: aG */
    public Query[] newArray(int i) {
        return new Query[i];
    }

    /* renamed from: ab */
    public Query createFromParcel(Parcel parcel) {
        String m;
        LogicalFilter logicalFilter;
        int i;
        String str = null;
        int o = com.google.android.gms.common.internal.safeparcel.a.o(parcel);
        int i2 = 0;
        LogicalFilter logicalFilter2 = null;
        while (parcel.dataPosition() < o) {
            int n = com.google.android.gms.common.internal.safeparcel.a.n(parcel);
            switch (com.google.android.gms.common.internal.safeparcel.a.S(n)) {
                case 1:
                    i = i2;
                    LogicalFilter logicalFilter3 = (LogicalFilter) com.google.android.gms.common.internal.safeparcel.a.a(parcel, n, LogicalFilter.CREATOR);
                    m = str;
                    logicalFilter = logicalFilter3;
                    break;
                case 3:
                    m = com.google.android.gms.common.internal.safeparcel.a.m(parcel, n);
                    logicalFilter = logicalFilter2;
                    i = i2;
                    break;
                case 1000:
                    String str2 = str;
                    logicalFilter = logicalFilter2;
                    i = com.google.android.gms.common.internal.safeparcel.a.g(parcel, n);
                    m = str2;
                    break;
                default:
                    com.google.android.gms.common.internal.safeparcel.a.b(parcel, n);
                    m = str;
                    logicalFilter = logicalFilter2;
                    i = i2;
                    break;
            }
            i2 = i;
            logicalFilter2 = logicalFilter;
            str = m;
        }
        if (parcel.dataPosition() == o) {
            return new Query(i2, logicalFilter2, str);
        }
        throw new C0006a("Overread allowed size end=" + o, parcel);
    }
}
