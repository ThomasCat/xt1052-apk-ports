package com.google.sample.castcompanionlibrary.widgets;

import android.graphics.Bitmap;
import android.net.Uri;
import com.google.sample.castcompanionlibrary.widgets.MiniController.OnMiniControllerChangedListener;

public interface IMiniController {
    boolean isVisible();

    void setIcon(Bitmap bitmap);

    void setIcon(Uri uri);

    void setOnMiniControllerChangedListener(OnMiniControllerChangedListener onMiniControllerChangedListener);

    void setPlaybackStatus(int i, int i2);

    void setStreamType(int i);

    void setSubTitle(String str);

    void setTitle(String str);

    void setVisibility(int i);
}
