package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.d;
import java.util.Map;

class u extends aj {
    private static final String ID = a.CUSTOM_VAR.toString();
    private static final String NAME = b.NAME.toString();
    private static final String UC = b.DEFAULT_VALUE.toString();
    private final DataLayer TN;

    public u(DataLayer dataLayer) {
        super(ID, NAME);
        this.TN = dataLayer;
    }

    public boolean iy() {
        return false;
    }

    public d.a u(Map<String, d.a> map) {
        Object obj = this.TN.get(di.j((d.a) map.get(NAME)));
        if (obj != null) {
            return di.r(obj);
        }
        d.a aVar = (d.a) map.get(UC);
        return aVar != null ? aVar : di.ku();
    }
}
