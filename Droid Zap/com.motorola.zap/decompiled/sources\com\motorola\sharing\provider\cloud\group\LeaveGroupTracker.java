package com.motorola.sharing.provider.cloud.group;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.util.Logger;
import com.motorola.sharing.util.NetworkUtils;
import com.motorola.sharing.util.PackageUtils;
import com.motorola.sharing.util.SharedPreference;

public class LeaveGroupTracker extends BroadcastReceiver {
    public static final SharedPreference<Integer> LEAVE_GROUP_ATTEMPTS = new SharedPreference<>("cloud_leaveGroupAttempts", Integer.class, Integer.valueOf(0), SharedPreference.integerHandler);
    private static int MAX_LEAVE_GROUP_ATTEMPTS = 5;
    private static final String TAG = Logger.getCloudTag("LeaveGroupTracker");

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Logger.d(TAG, "got " + action);
        GroupHandler groupHandler = GroupHandler.getInstance();
        if (!groupHandler.isLeavingGroup()) {
            Logger.d(TAG, "group has been already left");
            setEnabled(context, false);
        } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action) && NetworkUtils.hasDataConnection(GCSP.getContext())) {
            int attempts = ((Integer) LEAVE_GROUP_ATTEMPTS.get()).intValue() + 1;
            if (attempts <= MAX_LEAVE_GROUP_ATTEMPTS) {
                groupHandler.leaveGroup(true);
                LEAVE_GROUP_ATTEMPTS.set(Integer.valueOf(attempts));
                return;
            }
            Logger.d(TAG, "max number of attempts exceeded");
            setEnabled(context, false);
        }
    }

    public static void setEnabled(boolean enabled) {
        Context context = GCSP.getContext();
        boolean wasEnabled = isEnabled(context);
        if ((enabled && !wasEnabled) || (!enabled && wasEnabled)) {
            setEnabled(context, enabled);
        }
    }

    private static void setEnabled(Context context, boolean z) {
        if (PackageUtils.setComponentEnabled(context, new ComponentName(context, LeaveGroupTracker.class), z)) {
            Logger.d(TAG, "leave group tracker " + (z ? "enabled" : "disabled"));
            LEAVE_GROUP_ATTEMPTS.set(Integer.valueOf(0));
        }
    }

    private static boolean isEnabled(Context context) {
        return PackageUtils.isComponentEnabled(context, new ComponentName(context, LeaveGroupTracker.class));
    }
}
