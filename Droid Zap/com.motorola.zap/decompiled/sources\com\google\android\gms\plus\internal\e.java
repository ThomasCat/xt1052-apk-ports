package com.google.android.gms.plus.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.internal.eh;
import com.google.android.gms.internal.en;
import com.google.android.gms.internal.fh;
import com.google.android.gms.internal.io;
import com.google.android.gms.internal.ir;
import com.google.android.gms.plus.Moments.LoadMomentsResult;
import com.google.android.gms.plus.People.LoadPeopleResult;
import com.google.android.gms.plus.model.moments.Moment;
import com.google.android.gms.plus.model.moments.MomentBuffer;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class e extends eh<d> implements GooglePlayServicesClient {
    private Person Rd;
    private final h Re;

    final class a extends a {
        private final com.google.android.gms.common.api.a.c<Status> QI;

        public a(com.google.android.gms.common.api.a.c<Status> cVar) {
            this.QI = cVar;
        }

        public void L(Status status) {
            e.this.a((b<?>) new d<Object>(this.QI, status));
        }
    }

    final class b extends a {
        private final com.google.android.gms.common.api.a.c<LoadMomentsResult> QI;

        public b(com.google.android.gms.common.api.a.c<LoadMomentsResult> cVar) {
            this.QI = cVar;
        }

        public void a(DataHolder dataHolder, String str, String str2) {
            DataHolder dataHolder2;
            Status status = new Status(dataHolder.getStatusCode(), null, dataHolder.getMetadata() != null ? (PendingIntent) dataHolder.getMetadata().getParcelable("pendingIntent") : null);
            if (status.isSuccess() || dataHolder == null) {
                dataHolder2 = dataHolder;
            } else {
                if (!dataHolder.isClosed()) {
                    dataHolder.close();
                }
                dataHolder2 = null;
            }
            e.this.a((b<?>) new c<Object>(this.QI, status, dataHolder2, str, str2));
        }
    }

    final class c extends com.google.android.gms.internal.eh.d<com.google.android.gms.common.api.a.c<LoadMomentsResult>> implements LoadMomentsResult {
        private final String Dh;
        private final String Rg;
        private MomentBuffer Rh;
        private final Status vl;

        public c(com.google.android.gms.common.api.a.c<LoadMomentsResult> cVar, Status status, DataHolder dataHolder, String str, String str2) {
            super(cVar, dataHolder);
            this.vl = status;
            this.Dh = str;
            this.Rg = str2;
        }

        /* access modifiers changed from: protected */
        public void a(com.google.android.gms.common.api.a.c<LoadMomentsResult> cVar, DataHolder dataHolder) {
            this.Rh = dataHolder != null ? new MomentBuffer(dataHolder) : null;
            cVar.b(this);
        }

        public MomentBuffer getMomentBuffer() {
            return this.Rh;
        }

        public String getNextPageToken() {
            return this.Dh;
        }

        public Status getStatus() {
            return this.vl;
        }

        public String getUpdated() {
            return this.Rg;
        }

        public void release() {
            if (this.Rh != null) {
                this.Rh.close();
            }
        }
    }

    final class d extends b<com.google.android.gms.common.api.a.c<Status>> {
        private final Status vl;

        public d(com.google.android.gms.common.api.a.c<Status> cVar, Status status) {
            super(cVar);
            this.vl = status;
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void a(com.google.android.gms.common.api.a.c<Status> cVar) {
            if (cVar != null) {
                cVar.b(this.vl);
            }
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }
    }

    /* renamed from: com.google.android.gms.plus.internal.e$e reason: collision with other inner class name */
    final class C0084e extends a {
        private final com.google.android.gms.common.api.a.c<LoadPeopleResult> QI;

        public C0084e(com.google.android.gms.common.api.a.c<LoadPeopleResult> cVar) {
            this.QI = cVar;
        }

        public void a(DataHolder dataHolder, String str) {
            DataHolder dataHolder2;
            Status status = new Status(dataHolder.getStatusCode(), null, dataHolder.getMetadata() != null ? (PendingIntent) dataHolder.getMetadata().getParcelable("pendingIntent") : null);
            if (status.isSuccess() || dataHolder == null) {
                dataHolder2 = dataHolder;
            } else {
                if (!dataHolder.isClosed()) {
                    dataHolder.close();
                }
                dataHolder2 = null;
            }
            e.this.a((b<?>) new f<Object>(this.QI, status, dataHolder2, str));
        }
    }

    final class f extends com.google.android.gms.internal.eh.d<com.google.android.gms.common.api.a.c<LoadPeopleResult>> implements LoadPeopleResult {
        private final String Dh;
        private PersonBuffer Ri;
        private final Status vl;

        public f(com.google.android.gms.common.api.a.c<LoadPeopleResult> cVar, Status status, DataHolder dataHolder, String str) {
            super(cVar, dataHolder);
            this.vl = status;
            this.Dh = str;
        }

        /* access modifiers changed from: protected */
        public void a(com.google.android.gms.common.api.a.c<LoadPeopleResult> cVar, DataHolder dataHolder) {
            this.Ri = dataHolder != null ? new PersonBuffer(dataHolder) : null;
            cVar.b(this);
        }

        public String getNextPageToken() {
            return this.Dh;
        }

        public PersonBuffer getPersonBuffer() {
            return this.Ri;
        }

        public Status getStatus() {
            return this.vl;
        }

        public void release() {
            if (this.Ri != null) {
                this.Ri.close();
            }
        }
    }

    final class g extends a {
        private final com.google.android.gms.common.api.a.c<Status> QI;

        public g(com.google.android.gms.common.api.a.c<Status> cVar) {
            this.QI = cVar;
        }

        public void d(int i, Bundle bundle) {
            e.this.a((b<?>) new h<Object>(this.QI, new Status(i, null, bundle != null ? (PendingIntent) bundle.getParcelable("pendingIntent") : null)));
        }
    }

    final class h extends b<com.google.android.gms.common.api.a.c<Status>> {
        private final Status vl;

        public h(com.google.android.gms.common.api.a.c<Status> cVar, Status status) {
            super(cVar);
            this.vl = status;
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void a(com.google.android.gms.common.api.a.c<Status> cVar) {
            e.this.disconnect();
            if (cVar != null) {
                cVar.b(this.vl);
            }
        }

        /* access modifiers changed from: protected */
        public void cP() {
        }
    }

    public e(Context context, Looper looper, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, h hVar) {
        super(context, looper, connectionCallbacks, onConnectionFailedListener, hVar.hq());
        this.Re = hVar;
    }

    @Deprecated
    public e(Context context, GooglePlayServicesClient.ConnectionCallbacks connectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener onConnectionFailedListener, h hVar) {
        this(context, context.getMainLooper(), new com.google.android.gms.internal.eh.c(connectionCallbacks), new com.google.android.gms.internal.eh.g(onConnectionFailedListener), hVar);
    }

    /* access modifiers changed from: protected */
    public void a(int i, IBinder iBinder, Bundle bundle) {
        if (i == 0 && bundle != null && bundle.containsKey("loaded_person")) {
            this.Rd = ir.i(bundle.getByteArray("loaded_person"));
        }
        super.a(i, iBinder, bundle);
    }

    public void a(com.google.android.gms.common.api.a.c<LoadPeopleResult> cVar, int i, String str) {
        bm();
        C0084e eVar = new C0084e(cVar);
        try {
            ((d) eb()).a(eVar, 1, i, -1, str);
        } catch (RemoteException e) {
            eVar.a(DataHolder.empty(8), null);
        }
    }

    public void a(com.google.android.gms.common.api.a.c<LoadMomentsResult> cVar, int i, String str, Uri uri, String str2, String str3) {
        bm();
        b bVar = cVar != null ? new b(cVar) : null;
        try {
            ((d) eb()).a(bVar, i, str, uri, str2, str3);
        } catch (RemoteException e) {
            bVar.a(DataHolder.empty(8), null, null);
        }
    }

    public void a(com.google.android.gms.common.api.a.c<Status> cVar, Moment moment) {
        bm();
        a aVar = cVar != null ? new a(cVar) : null;
        try {
            ((d) eb()).a((b) aVar, fh.a((io) moment));
        } catch (RemoteException e) {
            if (aVar == null) {
                throw new IllegalStateException(e);
            }
            aVar.L(new Status(8, null, null));
        }
    }

    public void a(com.google.android.gms.common.api.a.c<LoadPeopleResult> cVar, Collection<String> collection) {
        bm();
        C0084e eVar = new C0084e(cVar);
        try {
            ((d) eb()).a((b) eVar, (List<String>) new ArrayList<String>(collection));
        } catch (RemoteException e) {
            eVar.a(DataHolder.empty(8), null);
        }
    }

    /* access modifiers changed from: protected */
    public void a(en enVar, com.google.android.gms.internal.eh.e eVar) throws RemoteException {
        Bundle hy = this.Re.hy();
        hy.putStringArray("request_visible_actions", this.Re.hr());
        enVar.a(eVar, GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, this.Re.hu(), this.Re.ht(), ea(), this.Re.getAccountName(), hy);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aB */
    public d p(IBinder iBinder) {
        return com.google.android.gms.plus.internal.d.a.aA(iBinder);
    }

    /* access modifiers changed from: protected */
    public String aF() {
        return "com.google.android.gms.plus.service.START";
    }

    /* access modifiers changed from: protected */
    public String aG() {
        return "com.google.android.gms.plus.internal.IPlusService";
    }

    public boolean aR(String str) {
        return Arrays.asList(ea()).contains(str);
    }

    public void c(com.google.android.gms.common.api.a.c<LoadPeopleResult> cVar, String[] strArr) {
        a(cVar, (Collection<String>) Arrays.asList(strArr));
    }

    public void clearDefaultAccount() {
        bm();
        try {
            this.Rd = null;
            ((d) eb()).clearDefaultAccount();
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public String getAccountName() {
        bm();
        try {
            return ((d) eb()).getAccountName();
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public Person getCurrentPerson() {
        bm();
        return this.Rd;
    }

    public void i(com.google.android.gms.common.api.a.c<LoadMomentsResult> cVar) {
        a(cVar, 20, null, null, null, "me");
    }

    public void i(com.google.android.gms.common.api.a.c<LoadPeopleResult> cVar, String str) {
        a(cVar, 0, str);
    }

    public void j(com.google.android.gms.common.api.a.c<LoadPeopleResult> cVar) {
        bm();
        C0084e eVar = new C0084e(cVar);
        try {
            ((d) eb()).a(eVar, 2, 1, -1, null);
        } catch (RemoteException e) {
            eVar.a(DataHolder.empty(8), null);
        }
    }

    public void k(com.google.android.gms.common.api.a.c<Status> cVar) {
        bm();
        clearDefaultAccount();
        g gVar = new g(cVar);
        try {
            ((d) eb()).b(gVar);
        } catch (RemoteException e) {
            gVar.d(8, null);
        }
    }

    public void removeMoment(String momentId) {
        bm();
        try {
            ((d) eb()).removeMoment(momentId);
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }
}
