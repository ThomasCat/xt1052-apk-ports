package com.motorola.sharing;

import android.os.Handler;
import android.os.Looper;
import com.motorola.gcsp.json.StatusCode;
import com.motorola.sharing.Authenticator.TokenInfo;
import com.motorola.sharing.ContentContainer.Builder;
import com.motorola.sharing.GroupManager.GroupContract.Columns;
import com.motorola.sharing.ProfileManagerListener.ProfileEvent;
import com.motorola.sharing.ProfileManagerListener.ProfileEvent.Status;
import com.motorola.sharing.ProfileManagerListener.ProfileEvent.Substatus;
import com.motorola.sharing.ProfileManagerListener.ProfileEvent.Type;
import com.motorola.sharing.provider.cloud.ConnectionException;
import com.motorola.sharing.provider.cloud.HttpHelper;
import com.motorola.sharing.provider.cloud.HttpHelper.UploadResult;
import com.motorola.sharing.provider.cloud.UrlHelper;
import com.motorola.sharing.util.FileUtils;
import com.motorola.sharing.util.JsonHelper;
import com.motorola.sharing.util.Logger;
import com.motorola.sharing.util.auth.SyncAuthenticator;
import java.io.File;
import java.net.UnknownHostException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class ProfileManager {
    private static final String TAG = Logger.getGcspTag("ProfileManager");
    private static ProfileManager sInstance;

    /* renamed from: com.motorola.sharing.ProfileManager$6 reason: invalid class name */
    static /* synthetic */ class AnonymousClass6 {
        static final /* synthetic */ int[] $SwitchMap$com$motorola$sharing$ProfileManagerListener$ProfileEvent$Type = new int[Type.values().length];

        static {
            try {
                $SwitchMap$com$motorola$sharing$ProfileManagerListener$ProfileEvent$Type[Type.GET_MY_PROFILE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$motorola$sharing$ProfileManagerListener$ProfileEvent$Type[Type.UPDATE_MY_PROFILE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SwitchMap$com$motorola$gcsp$json$StatusCode = new int[StatusCode.values().length];
            try {
                $SwitchMap$com$motorola$gcsp$json$StatusCode[StatusCode.ERR_041.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$motorola$gcsp$json$StatusCode[StatusCode.OK.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    public static synchronized ProfileManager getInstance() {
        ProfileManager profileManager;
        synchronized (ProfileManager.class) {
            if (sInstance == null) {
                sInstance = new ProfileManager();
            }
            profileManager = sInstance;
        }
        return profileManager;
    }

    private ProfileManager() {
    }

    public void getMyProfile(final ProfileManagerListener listener) {
        Logger.d(TAG, "getMyProfile");
        final Handler handler = getHandler();
        SharedExecutors.threadPoolExecutor.execute(new Runnable() {
            public void run() {
                ProfileManager.this.doGetMyProfile(listener, handler);
            }
        });
    }

    /* access modifiers changed from: private */
    public void doGetMyProfile(ProfileManagerListener listener, Handler handler) {
        TokenInfo tokenInfo = SyncAuthenticator.auth();
        if (tokenInfo == null) {
            postErrorEvent(Type.GET_MY_PROFILE, null, Substatus.AUTHENTICATION_ERROR, listener, handler);
            return;
        }
        JSONObject req = new JSONObject();
        try {
            JsonHelper.addCommonRequestFields(req);
            try {
                handleGetMyProfileResponse(HttpHelper.sendPostRequest(UrlHelper.getUserProfile(), req.toString(), tokenInfo.getToken(), tokenInfo.getUserId(), true), listener, handler);
            } catch (UnknownHostException eu) {
                Logger.e(TAG, "Network exception.", eu);
                postErrorEvent(Type.GET_MY_PROFILE, null, Substatus.NETWORK_ERROR, listener, handler);
            } catch (ConnectionException ec) {
                Logger.e(TAG, "Network exception.", ec);
                postErrorEvent(Type.GET_MY_PROFILE, null, Substatus.NETWORK_ERROR, listener, handler);
            }
        } catch (JSONException e) {
            Logger.e(TAG, "Json exception.", e);
            postErrorEvent(Type.GET_MY_PROFILE, null, Substatus.CLIENT_ERROR, listener, handler);
        }
    }

    private void handleGetMyProfileResponse(String respStr, ProfileManagerListener listener, Handler handler) {
        JSONObject resp;
        try {
            Object obj = new JSONTokener(respStr).nextValue();
            if (obj instanceof JSONArray) {
                resp = ((JSONArray) obj).getJSONObject(0);
            } else {
                resp = (JSONObject) obj;
            }
            switch (StatusCode.codeOf(resp.getString("status"))) {
                case ERR_041:
                    postSuccessEvent(Type.GET_MY_PROFILE, null, listener, handler);
                    return;
                case OK:
                    postSuccessEvent(Type.GET_MY_PROFILE, getProfile(resp), listener, handler);
                    return;
                default:
                    postErrorEvent(Type.GET_MY_PROFILE, null, Substatus.CLIENT_ERROR, listener, handler);
                    return;
            }
        } catch (JSONException e) {
            Logger.e(TAG, "Json exception.", e);
            postErrorEvent(Type.GET_MY_PROFILE, null, Substatus.CLIENT_ERROR, listener, handler);
        }
        Logger.e(TAG, "Json exception.", e);
        postErrorEvent(Type.GET_MY_PROFILE, null, Substatus.CLIENT_ERROR, listener, handler);
    }

    public static Profile getProfile(JSONObject resp) throws JSONException {
        Profile profile = new Profile();
        if (resp.has("userUUID")) {
            profile.userId = resp.getString("userUUID");
        }
        if (resp.has(Columns.PROFILE_VERSION)) {
            profile.version = resp.getString(Columns.PROFILE_VERSION);
        }
        if (resp.has("nickName")) {
            profile.nickname = resp.getString("nickName");
        }
        if (resp.has("avatarUrl")) {
            profile.pictureUrl = resp.getString("avatarUrl");
        }
        if (resp.has("uploadUrl")) {
            profile.uploadPictureUrl = resp.getString("uploadUrl");
        }
        if (resp.has("isAnonymous")) {
            profile.anonymous = resp.getBoolean("isAnonymous");
        }
        return profile;
    }

    public void updateMyProfile(final Profile profile, final ProfileManagerListener listener) {
        Logger.d(TAG, "updateMyProfile");
        final Handler handler = getHandler();
        SharedExecutors.threadPoolExecutor.execute(new Runnable() {
            public void run() {
                ProfileManager.this.doUpdateMyProfile(profile, listener, handler);
            }
        });
    }

    /* access modifiers changed from: private */
    public void doUpdateMyProfile(Profile profile, ProfileManagerListener listener, Handler handler) {
        TokenInfo tokenInfo = SyncAuthenticator.auth();
        if (tokenInfo == null) {
            postErrorEvent(Type.UPDATE_MY_PROFILE, profile, Substatus.AUTHENTICATION_ERROR, listener, handler);
            return;
        }
        JSONObject req = new JSONObject();
        try {
            JsonHelper.addCommonRequestFields(req);
            JSONObject userProfile = new JSONObject();
            req.put("userProfile", userProfile);
            if (profile.nickname != null) {
                JSONObject jSONObject = userProfile;
                jSONObject.put("nickName", profile.nickname);
            }
            if (profile.pictureUrl != null) {
                JSONObject jSONObject2 = userProfile;
                jSONObject2.put("avatarUrl", profile.pictureUrl);
            }
            JSONObject jSONObject3 = userProfile;
            jSONObject3.put("isAnonymous", profile.anonymous);
            try {
                handleUpdateMyProfileResponse(HttpHelper.sendPostRequest(UrlHelper.postUserProfile(), req.toString(), tokenInfo.getToken(), tokenInfo.getUserId(), true), profile, listener, handler);
            } catch (UnknownHostException eu) {
                Logger.e(TAG, "Network exception.", eu);
                postErrorEvent(Type.UPDATE_MY_PROFILE, profile, Substatus.NETWORK_ERROR, listener, handler);
            } catch (ConnectionException ec) {
                Logger.e(TAG, "Network exception.", ec);
                postErrorEvent(Type.UPDATE_MY_PROFILE, profile, Substatus.NETWORK_ERROR, listener, handler);
            }
        } catch (JSONException e) {
            Logger.e(TAG, "Json exception.", e);
            postErrorEvent(Type.UPDATE_MY_PROFILE, profile, Substatus.CLIENT_ERROR, listener, handler);
        }
    }

    private void handleUpdateMyProfileResponse(String respStr, Profile profile, ProfileManagerListener listener, Handler handler) {
        try {
            JSONObject resp = new JSONObject(respStr);
            switch (StatusCode.codeOf(resp.getString("status"))) {
                case OK:
                    postSuccessEvent(Type.UPDATE_MY_PROFILE, getProfile(resp), listener, handler);
                    return;
                default:
                    postErrorEvent(Type.UPDATE_MY_PROFILE, profile, Substatus.SERVER_ERROR, listener, handler);
                    return;
            }
        } catch (JSONException e) {
            Logger.e(TAG, "Json exception.", e);
            postErrorEvent(Type.UPDATE_MY_PROFILE, profile, Substatus.CLIENT_ERROR, listener, handler);
        }
    }

    public void deleteMyProfile(final ProfileManagerListener listener) {
        Logger.d(TAG, "deleteMyProfile");
        final Handler handler = getHandler();
        SharedExecutors.threadPoolExecutor.execute(new Runnable() {
            public void run() {
                ProfileManager.this.doDeleteMyProfile(listener, handler);
            }
        });
    }

    /* access modifiers changed from: private */
    public void doDeleteMyProfile(ProfileManagerListener listener, Handler handler) {
        TokenInfo tokenInfo = SyncAuthenticator.auth();
        if (tokenInfo == null) {
            postErrorEvent(Type.DELETE_MY_PROFILE, null, Substatus.AUTHENTICATION_ERROR, listener, handler);
            return;
        }
        JSONObject req = new JSONObject();
        try {
            JsonHelper.addCommonRequestFields(req);
            try {
                handleDeleteMyProfileResponse(HttpHelper.sendPostRequest(UrlHelper.deleteUserProfile(), req.toString(), tokenInfo.getToken(), tokenInfo.getUserId(), true), listener, handler);
            } catch (UnknownHostException eu) {
                Logger.e(TAG, "Network exception.", eu);
                postErrorEvent(Type.DELETE_MY_PROFILE, null, Substatus.NETWORK_ERROR, listener, handler);
            } catch (ConnectionException ec) {
                Logger.e(TAG, "Network exception.", ec);
                postErrorEvent(Type.DELETE_MY_PROFILE, null, Substatus.NETWORK_ERROR, listener, handler);
            }
        } catch (JSONException e) {
            Logger.e(TAG, "Json exception.", e);
            postErrorEvent(Type.DELETE_MY_PROFILE, null, Substatus.CLIENT_ERROR, listener, handler);
        }
    }

    private void handleDeleteMyProfileResponse(String respStr, ProfileManagerListener listener, Handler handler) {
        try {
            switch (StatusCode.codeOf(new JSONObject(respStr).getString("status"))) {
                case OK:
                    postSuccessEvent(Type.GET_MY_PROFILE, null, listener, handler);
                    return;
                default:
                    postErrorEvent(Type.DELETE_MY_PROFILE, null, Substatus.CLIENT_ERROR, listener, handler);
                    return;
            }
        } catch (JSONException e) {
            Logger.e(TAG, "Json exception.", e);
            postErrorEvent(Type.DELETE_MY_PROFILE, null, Substatus.CLIENT_ERROR, listener, handler);
        }
    }

    public void updateMyProfile(Profile profile, File f, ProfileManagerListener listener) {
        final Handler handler = getHandler();
        final ProfileManagerListener profileManagerListener = listener;
        final File file = f;
        final Profile profile2 = profile;
        getMyProfile(new ProfileManagerListener() {
            public void onProfileEvent(ProfileEvent event) {
                if (event.status != Status.SUCCESS) {
                    ProfileManager.this.postErrorEvent(Type.UPDATE_MY_PROFILE_PICTURE, null, event.substatus, profileManagerListener, handler);
                    return;
                }
                switch (AnonymousClass6.$SwitchMap$com$motorola$sharing$ProfileManagerListener$ProfileEvent$Type[event.type.ordinal()]) {
                    case 1:
                        handleGetMyProfile(event);
                        return;
                    case 2:
                        ProfileManager.this.postSuccessEvent(Type.UPDATE_MY_PROFILE_PICTURE, event.profile, profileManagerListener, handler);
                        return;
                    default:
                        ProfileManager.this.postErrorEvent(Type.UPDATE_MY_PROFILE_PICTURE, event.profile, Substatus.CLIENT_ERROR, profileManagerListener, handler);
                        return;
                }
            }

            private void handleGetMyProfile(final ProfileEvent event) {
                SharedExecutors.threadPoolExecutor.execute(new Runnable() {
                    public void run() {
                        AnonymousClass4.this.doHandleGetMyProfile(event);
                    }
                });
            }

            /* access modifiers changed from: private */
            public void doHandleGetMyProfile(ProfileEvent event) {
                if (event.profile == null) {
                    ProfileManager.this.postErrorEvent(Type.UPDATE_MY_PROFILE_PICTURE, event.profile, Substatus.PROFILE_NOT_FOUND, profileManagerListener, handler);
                    return;
                }
                UploadResult result = ProfileManager.this.uploadFile(file, event.profile.uploadPictureUrl, event.profile, profileManagerListener, handler);
                String mediaLink = ProfileManager.this.getMediaLink(result.response);
                if (!result.success || mediaLink == null) {
                    ProfileManager.this.postErrorEvent(Type.UPDATE_MY_PROFILE_PICTURE, event.profile, Substatus.SERVER_ERROR, profileManagerListener, handler);
                    return;
                }
                Profile updateProfile = new Profile(profile2);
                updateProfile.pictureUrl = mediaLink;
                ProfileManager.this.updateMyProfile(updateProfile, this);
            }
        });
    }

    /* access modifiers changed from: private */
    public String getMediaLink(String resp) {
        String str = null;
        if (resp == null) {
            return str;
        }
        try {
            return new JSONObject(resp).getString("mediaLink");
        } catch (JSONException e) {
            Logger.e(TAG, "Cannot get mediaLink", e);
            return str;
        }
    }

    /* access modifiers changed from: private */
    public UploadResult uploadFile(File f, String url, Profile profile, ProfileManagerListener listener, Handler handler) {
        Builder builder = new Builder();
        builder.setPath(f.getAbsolutePath());
        builder.setMimeType(FileUtils.getMimeTypeFromFileName(f.getAbsolutePath()));
        ContentContainer container = builder.build();
        container.url = url;
        container.shareId = profile.userId;
        container.userCred.set(SyncAuthenticator.auth());
        return HttpHelper.uploadContent(container, GCSP.getContext(), Long.MAX_VALUE, null, false, true);
    }

    private Handler getHandler() {
        Looper looper = Looper.myLooper();
        if (looper == null) {
            looper = Looper.getMainLooper();
        }
        return new Handler(looper);
    }

    /* access modifiers changed from: private */
    public void postErrorEvent(Type type, Profile profile, Substatus substatus, ProfileManagerListener listener, Handler handler) {
        ProfileEvent ev = new ProfileEvent();
        ev.type = type;
        ev.status = Status.ERROR;
        ev.substatus = substatus;
        ev.profile = profile;
        postEvent(ev, listener, handler);
    }

    /* access modifiers changed from: private */
    public void postSuccessEvent(Type type, Profile profile, ProfileManagerListener listener, Handler handler) {
        ProfileEvent ev = new ProfileEvent();
        ev.type = type;
        ev.status = Status.SUCCESS;
        ev.substatus = Substatus.NO_ERROR;
        ev.profile = profile;
        postEvent(ev, listener, handler);
    }

    private void postEvent(final ProfileEvent event, final ProfileManagerListener listener, Handler handler) {
        if (listener != null) {
            handler.post(new Runnable() {
                public void run() {
                    listener.onProfileEvent(event);
                }
            });
        }
    }
}
