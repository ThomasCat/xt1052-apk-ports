package com.motorola.zap.zz.ui;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat.BigTextStyle;
import android.support.v4.app.NotificationCompat.Builder;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.drive.DriveFile;
import com.motorola.sharing.GCSP;
import com.motorola.sharing.GroupListener.GroupEvent;
import com.motorola.sharing.GroupManager.GroupInfo;
import com.motorola.sharing.util.FileUtils;
import com.motorola.sharing.util.NetworkUtils;
import com.motorola.zap.LogUtils;
import com.motorola.zap.R;
import com.motorola.zap.Z;
import com.motorola.zap.ZapConstants;
import com.motorola.zap.app.ActionReceiver;
import com.motorola.zap.app.App;
import com.motorola.zap.app.ZzInviteStore;
import com.motorola.zap.ui.ZapUi;

public abstract class BaseZzUiHandler {
    private static final boolean DBG = LogUtils.IS_DEBUGGABLE;
    protected static final String ZZ_NOTIF_TAG = "zz";

    public final void handleMessage(GroupEvent ge) {
        switch (ge.status) {
            case INFO:
                handleInfo(ge);
                return;
            case OK:
                handleOk(ge);
                return;
            case ERROR:
                handleError(ge);
                return;
            default:
                if (Log.isLoggable("Zap-BaseZzUiHandler", 6)) {
                    Log.e("Zap-BaseZzUiHandler", "Invalid arg1: " + ge.status);
                    return;
                }
                return;
        }
    }

    protected static Intent getRetryIntent(GroupEvent ge) {
        if (ge.bundle == null) {
            return null;
        }
        return (Intent) ge.bundle.getParcelable(GroupEvent.KEY_RETRY_INTENT);
    }

    /* access modifiers changed from: protected */
    public void handleInfo(GroupEvent ge) {
    }

    /* access modifiers changed from: protected */
    public void handleOk(GroupEvent ge) {
    }

    /* access modifiers changed from: protected */
    public void handleError(GroupEvent ge) {
    }

    /* access modifiers changed from: protected */
    public boolean handleCommonError(GroupEvent ge) {
        ZapUi ui = ZapUi.getInstance();
        switch (ge.subStatus) {
            case NO_ERROR:
            case AUTHENTICATION_ERROR:
                return true;
            case NETWORK_ERROR:
                return handleNetworkError(getRetryIntent(ge), true);
            case USER_DISABLED:
                ui.showToastMessage(getString(R.string.toast_error_zz_disabled), ZapUi.getErrorToastIconId(), true, true);
                return true;
            case USER_BLOCKED:
                ui.showToastMessage(getString(R.string.toast_error_zz_blocked), ZapUi.getErrorToastIconId(), true, true);
                return true;
            default:
                return false;
        }
    }

    private boolean handleNetworkError(Intent retryIntent, boolean checkConnection) {
        if (!checkConnection || !NetworkUtils.hasDataConnection(App.getAppContext())) {
            ZapUi ui = ZapUi.getInstance();
            if (retryIntent != null) {
                if (DBG) {
                    Log.d("Zap-BaseZzUiHandler", "No network connection, try to fix this");
                }
                ui.showConnectNetworkDialog(retryIntent);
            } else {
                if (DBG) {
                    Log.d("Zap-BaseZzUiHandler", "No network connection");
                }
                ui.showNetworkConnectionErrorDialog();
            }
            return true;
        }
        if (DBG) {
            Log.d("Zap-BaseZzUiHandler", "Unknown network error");
        }
        return false;
    }

    protected static NotificationManager getNotificationManager() {
        return (NotificationManager) App.getAppContext().getSystemService("notification");
    }

    public static void cancelNotification(int notifId) {
        getNotificationManager().cancel(ZZ_NOTIF_TAG, notifId);
    }

    protected static String getString(int id) {
        return App.getAppContext().getString(id);
    }

    private static AlarmManager getAlarmManager() {
        return (AlarmManager) App.getAppContext().getSystemService("alarm");
    }

    protected static PendingIntent getZzCancelInvitePendingIntent(int i) {
        Intent intent = new Intent(ZapConstants.ACTION_ZZ_CANCEL_INVITE);
        intent.setClass(App.getAppContext(), ActionReceiver.class);
        intent.putExtra(ZapConstants.EXTRA_ZZ_INVITE_UID, i);
        return PendingIntent.getBroadcast(App.getAppContext(), i, intent, 0);
    }

    /* access modifiers changed from: protected */
    public void scheduleZzInviteExpiration(int zzInviteUid) {
        getAlarmManager().set(3, SystemClock.elapsedRealtime() + 1800000, getZzCancelInvitePendingIntent(zzInviteUid));
    }

    protected static void cancelZzInviteExpiration(int zzInviteUid) {
        getAlarmManager().cancel(getZzCancelInvitePendingIntent(zzInviteUid));
    }

    public static void cancelZzInviteNotification(int zzInviteUid) {
        getNotificationManager().cancel(ZZ_NOTIF_TAG, zzInviteUid);
        cancelZzInviteExpiration(zzInviteUid);
        ZzInviteStore.remove(zzInviteUid);
    }

    protected static void showZzActiveNotification() {
        boolean inviteAvailable;
        long inviteTimeToExpire = getZzSharedInviteTimeToExpire();
        if (inviteTimeToExpire > 0) {
            inviteAvailable = true;
        } else {
            inviteAvailable = false;
        }
        scheduleZzStatusUpdate(inviteTimeToExpire);
        getNotificationManager().notify(ZZ_NOTIF_TAG, 0, buildZzActiveNotification(inviteAvailable));
    }

    public static void updateZzActiveNotification() {
        if (GCSP.getGroupManager().isAnyGroupActive()) {
            showZzActiveNotification();
        } else {
            cancelZzActiveNotification();
        }
    }

    public static void cancelZzActiveNotification() {
        getNotificationManager().cancel(ZZ_NOTIF_TAG, 0);
        cancelZzStatusUpdate();
    }

    private static Notification buildZzActiveNotification(boolean inviteAvailable) {
        String status;
        String status2;
        GroupInfo groupInfo = GCSP.getGroupManager().getGroupInfo(null);
        Builder b = new Builder(App.getAppContext()).setSmallIcon(R.drawable.ic_zapzone).setLargeIcon(FileUtils.base64ToBitmap(groupInfo.thumbnail)).setContentTitle(getString(R.string.notif_msg_zz)).setOngoing(true).setContentIntent(getShowAppPendingIntent());
        String zzName = groupInfo.name;
        if (inviteAvailable) {
            b.setTicker(getString(R.string.notif_msg_zz_invite_available_ticker));
            if (TextUtils.isEmpty(zzName)) {
                status2 = getString(R.string.notif_msg_zz_invite_available_text_no_name);
            } else {
                status2 = String.format(getString(R.string.notif_msg_zz_invite_available_text), new Object[]{zzName});
            }
            b.setContentText(status2);
            String zzPin = groupInfo.pin;
            if (!TextUtils.isEmpty(zzPin)) {
                b.setContentInfo(getString(R.string.notif_msg_zz_invite_locked_info));
                status2 = status2 + "\n" + String.format(getString(R.string.notif_msg_code), new Object[]{zzPin});
            }
        } else {
            if (TextUtils.isEmpty(zzName)) {
                status = getString(R.string.notif_msg_zz_active_text_no_name);
            } else {
                status = String.format(getString(R.string.notif_msg_zz_active_text), new Object[]{zzName});
            }
            b.setContentText(status2);
        }
        if (VERSION.SDK_INT < 16) {
            return b.build();
        }
        b.setPriority(2);
        b.addAction(R.drawable.content_remove, getString(R.string.notif_button_leave_zz), getZzLeavePendingIntent());
        return new BigTextStyle(b).bigText(status2).build();
    }

    private static void scheduleZzStatusUpdate(long updateInMs) {
        if (updateInMs > 0) {
            getAlarmManager().set(3, SystemClock.elapsedRealtime() + updateInMs + 100, getZzUpdateStatusPendingIntent(false));
        }
    }

    private static void cancelZzStatusUpdate() {
        PendingIntent pi = getZzUpdateStatusPendingIntent(true);
        if (pi != null) {
            getAlarmManager().cancel(pi);
        }
    }

    private static PendingIntent getZzUpdateStatusPendingIntent(boolean z) {
        Intent intent = new Intent(ZapConstants.ACTION_ZZ_NOTIFICATION_UPDATE);
        intent.setClass(App.getAppContext(), ActionReceiver.class);
        return PendingIntent.getBroadcast(App.getAppContext(), 0, intent, z ? DriveFile.MODE_WRITE_ONLY : 134217728);
    }

    private static PendingIntent getZzLeavePendingIntent() {
        Intent intent = new Intent(ZapConstants.ACTION_LEAVE_ZZ);
        intent.setClass(App.getAppContext(), ActionReceiver.class);
        return PendingIntent.getBroadcast(App.getAppContext(), 0, intent, 0);
    }

    private static PendingIntent getShowAppPendingIntent() {
        Intent i = ZapUi.getInstance().getStartMainActivityIntent();
        i.setFlags(337641472);
        return PendingIntent.getActivity(App.getAppContext(), 0, i, 0);
    }

    private static long getZzSharedInviteTimeToExpire() {
        GroupInfo groupInfo = GCSP.getGroupManager().getGroupInfo(null);
        if (groupInfo == null) {
            return 0;
        }
        long now = System.currentTimeMillis();
        long timeShared = groupInfo.lastInviteTime;
        long timeExpired = timeShared + ((Long) Z.ZZ_SHARED_INVITE_TIMEOUT.read()).longValue();
        if (now < timeShared || now > timeExpired) {
            return 0;
        }
        return timeExpired - now;
    }
}
