package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.Cast.ApplicationConnectionResult;
import com.google.android.gms.cast.Cast.Listener;
import com.google.android.gms.cast.Cast.MessageReceivedCallback;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a.c;
import com.google.android.gms.internal.eh.e;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public final class dq extends eh<ds> {
    /* access modifiers changed from: private */
    public static final du xE = new du("CastClientImpl");
    /* access modifiers changed from: private */
    public static final Object xU = new Object();
    /* access modifiers changed from: private */
    public static final Object xV = new Object();
    /* access modifiers changed from: private */
    public final Handler mHandler;
    /* access modifiers changed from: private */
    public final Listener wz;
    /* access modifiers changed from: private */
    public ApplicationMetadata xF = null;
    /* access modifiers changed from: private */
    public final CastDevice xG;
    private final dt xH = new com.google.android.gms.internal.dt.a() {
        private boolean D(int i) {
            synchronized (dq.xV) {
                if (dq.this.xT == null) {
                    return false;
                }
                dq.this.xT.b(new Status(i));
                dq.this.xT = null;
                return true;
            }
        }

        private void b(long j, int i) {
            c cVar;
            synchronized (dq.this.xR) {
                cVar = (c) dq.this.xR.remove(Long.valueOf(j));
            }
            if (cVar != null) {
                cVar.b(new Status(i));
            }
        }

        public void A(int i) {
            synchronized (dq.xU) {
                if (dq.this.xS != null) {
                    dq.this.xS.b(new a(new Status(i)));
                    dq.this.xS = null;
                }
            }
        }

        public void B(int i) {
            D(i);
        }

        public void C(int i) {
            D(i);
        }

        public void a(ApplicationMetadata applicationMetadata, String str, String str2, boolean z) {
            dq.this.xF = applicationMetadata;
            dq.this.xO = applicationMetadata.getApplicationId();
            dq.this.xP = str2;
            synchronized (dq.xU) {
                if (dq.this.xS != null) {
                    dq.this.xS.b(new a(new Status(0), applicationMetadata, str, str2, z));
                    dq.this.xS = null;
                }
            }
        }

        public void a(String str, long j) {
            b(j, 0);
        }

        public void a(String str, long j, int i) {
            b(j, i);
        }

        public void b(String str, double d, boolean z) {
            final String str2 = str;
            final double d2 = d;
            final boolean z2 = z;
            dq.this.mHandler.post(new Runnable() {
                public void run() {
                    dq.this.a(str2, d2, z2);
                }
            });
        }

        public void b(String str, byte[] bArr) {
            dq.xE.b("IGNORING: Receive (type=binary, ns=%s) <%d bytes>", str, Integer.valueOf(bArr.length));
        }

        public void d(final String str, final String str2) {
            dq.xE.b("Receive (type=text, ns=%s) %s", str, str2);
            dq.this.mHandler.post(new Runnable() {
                public void run() {
                    MessageReceivedCallback messageReceivedCallback;
                    synchronized (dq.this.xI) {
                        messageReceivedCallback = (MessageReceivedCallback) dq.this.xI.get(str);
                    }
                    if (messageReceivedCallback != null) {
                        messageReceivedCallback.onMessageReceived(dq.this.xG, str, str2);
                        return;
                    }
                    dq.xE.b("Discarded message for unknown namespace '%s'", str);
                }
            });
        }

        public void onApplicationDisconnected(final int statusCode) {
            dq.this.xO = null;
            dq.this.xP = null;
            if (!D(statusCode) && dq.this.wz != null) {
                dq.this.mHandler.post(new Runnable() {
                    public void run() {
                        if (dq.this.wz != null) {
                            dq.this.wz.onApplicationDisconnected(statusCode);
                        }
                    }
                });
            }
        }

        public void z(int i) {
            dq.xE.b("ICastDeviceControllerListener.onDisconnected: %d", Integer.valueOf(i));
            dq.this.xM = false;
            dq.this.xF = null;
            if (i != 0) {
                dq.this.O(2);
            }
        }
    };
    /* access modifiers changed from: private */
    public final Map<String, MessageReceivedCallback> xI = new HashMap();
    private final long xJ;
    private String xK = null;
    private boolean xL;
    /* access modifiers changed from: private */
    public boolean xM = false;
    private final AtomicLong xN = new AtomicLong(0);
    /* access modifiers changed from: private */
    public String xO;
    /* access modifiers changed from: private */
    public String xP;
    private Bundle xQ;
    /* access modifiers changed from: private */
    public Map<Long, c<Status>> xR = new HashMap();
    /* access modifiers changed from: private */
    public c<ApplicationConnectionResult> xS;
    /* access modifiers changed from: private */
    public c<Status> xT;
    private double xe = 0.0d;
    private boolean xf = false;

    private static final class a implements ApplicationConnectionResult {
        private final String pz;
        private final Status vl;
        private final ApplicationMetadata yb;
        private final String yc;
        private final boolean yd;

        public a(Status status) {
            this(status, null, null, null, false);
        }

        public a(Status status, ApplicationMetadata applicationMetadata, String str, String str2, boolean z) {
            this.vl = status;
            this.yb = applicationMetadata;
            this.yc = str;
            this.pz = str2;
            this.yd = z;
        }

        public ApplicationMetadata getApplicationMetadata() {
            return this.yb;
        }

        public String getApplicationStatus() {
            return this.yc;
        }

        public String getSessionId() {
            return this.pz;
        }

        public Status getStatus() {
            return this.vl;
        }

        public boolean getWasLaunched() {
            return this.yd;
        }
    }

    public dq(Context context, Looper looper, CastDevice castDevice, long j, Listener listener, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, connectionCallbacks, onConnectionFailedListener, null);
        this.xG = castDevice;
        this.wz = listener;
        this.xJ = j;
        this.mHandler = new Handler(looper);
    }

    /* access modifiers changed from: private */
    public void a(String str, double d, boolean z) {
        boolean z2;
        boolean z3;
        if (!dr.a(str, this.xK)) {
            this.xK = str;
            z2 = true;
        } else {
            z2 = false;
        }
        if (this.wz != null && (z2 || this.xL)) {
            this.wz.onApplicationStatusChanged();
        }
        if (d != this.xe) {
            this.xe = d;
            z3 = true;
        } else {
            z3 = false;
        }
        if (z != this.xf) {
            this.xf = z;
            z3 = true;
        }
        xE.b("hasChange=%b, mFirstStatusUpdate=%b", Boolean.valueOf(z3), Boolean.valueOf(this.xL));
        if (this.wz != null && (z3 || this.xL)) {
            this.wz.onVolumeChanged();
        }
        this.xL = false;
    }

    private void d(c<ApplicationConnectionResult> cVar) {
        synchronized (xU) {
            if (this.xS != null) {
                this.xS.b(new a(new Status(2002)));
            }
            this.xS = cVar;
        }
    }

    private void db() throws IllegalStateException {
        if (!this.xM) {
            throw new IllegalStateException("not connected to a device");
        }
    }

    private void f(c<Status> cVar) {
        synchronized (xV) {
            if (this.xT != null) {
                cVar.b(new Status(2001));
            } else {
                this.xT = cVar;
            }
        }
    }

    public void Q(String str) throws IllegalArgumentException, RemoteException {
        MessageReceivedCallback messageReceivedCallback;
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Channel namespace cannot be null or empty");
        }
        synchronized (this.xI) {
            messageReceivedCallback = (MessageReceivedCallback) this.xI.remove(str);
        }
        if (messageReceivedCallback != null) {
            try {
                ((ds) eb()).T(str);
            } catch (IllegalStateException e) {
                xE.a(e, "Error unregistering namespace (%s): %s", str, e.getMessage());
            }
        }
    }

    public void a(double d) throws IllegalArgumentException, IllegalStateException, RemoteException {
        if (Double.isInfinite(d) || Double.isNaN(d)) {
            throw new IllegalArgumentException("Volume cannot be " + d);
        }
        ((ds) eb()).a(d, this.xe, this.xf);
    }

    /* access modifiers changed from: protected */
    public void a(int i, IBinder iBinder, Bundle bundle) {
        if (i == 0 || i == 1001) {
            this.xM = true;
            this.xL = true;
        } else {
            this.xM = false;
        }
        if (i == 1001) {
            this.xQ = new Bundle();
            this.xQ.putBoolean(Cast.EXTRA_APP_NO_LONGER_RUNNING, true);
            i = 0;
        }
        super.a(i, iBinder, bundle);
    }

    /* access modifiers changed from: protected */
    public void a(en enVar, e eVar) throws RemoteException {
        Bundle bundle = new Bundle();
        xE.b("getServiceFromBroker(): mLastApplicationId=%s, mLastSessionId=%s", this.xO, this.xP);
        this.xG.putInBundle(bundle);
        bundle.putLong("com.google.android.gms.cast.EXTRA_CAST_FLAGS", this.xJ);
        if (this.xO != null) {
            bundle.putString("last_application_id", this.xO);
            if (this.xP != null) {
                bundle.putString("last_session_id", this.xP);
            }
        }
        enVar.a((em) eVar, (int) GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, getContext().getPackageName(), this.xH.asBinder(), bundle);
    }

    public void a(String str, MessageReceivedCallback messageReceivedCallback) throws IllegalArgumentException, IllegalStateException, RemoteException {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Channel namespace cannot be null or empty");
        }
        Q(str);
        if (messageReceivedCallback != null) {
            synchronized (this.xI) {
                this.xI.put(str, messageReceivedCallback);
            }
            ((ds) eb()).S(str);
        }
    }

    public void a(String str, c<Status> cVar) throws IllegalStateException, RemoteException {
        f(cVar);
        ((ds) eb()).R(str);
    }

    public void a(String str, String str2, c<Status> cVar) throws IllegalArgumentException, IllegalStateException, RemoteException {
        if (TextUtils.isEmpty(str2)) {
            throw new IllegalArgumentException("The message payload cannot be null or empty");
        } else if (str == null || str.length() > 128) {
            throw new IllegalArgumentException("Invalid namespace length");
        } else if (str2.length() > 65536) {
            throw new IllegalArgumentException("Message exceeds maximum size");
        } else {
            db();
            long incrementAndGet = this.xN.incrementAndGet();
            ((ds) eb()).a(str, str2, incrementAndGet);
            this.xR.put(Long.valueOf(incrementAndGet), cVar);
        }
    }

    public void a(String str, boolean z, c<ApplicationConnectionResult> cVar) throws IllegalStateException, RemoteException {
        d(cVar);
        ((ds) eb()).e(str, z);
    }

    /* access modifiers changed from: protected */
    public String aF() {
        return "com.google.android.gms.cast.service.BIND_CAST_DEVICE_CONTROLLER_SERVICE";
    }

    /* access modifiers changed from: protected */
    public String aG() {
        return "com.google.android.gms.cast.internal.ICastDeviceController";
    }

    public void b(String str, String str2, c<ApplicationConnectionResult> cVar) throws IllegalStateException, RemoteException {
        d(cVar);
        ((ds) eb()).e(str, str2);
    }

    public Bundle cY() {
        if (this.xQ == null) {
            return super.cY();
        }
        Bundle bundle = this.xQ;
        this.xQ = null;
        return bundle;
    }

    public void cZ() throws IllegalStateException, RemoteException {
        ((ds) eb()).cZ();
    }

    public double da() throws IllegalStateException {
        db();
        return this.xe;
    }

    public void disconnect() {
        try {
            if (isConnected()) {
                synchronized (this.xI) {
                    this.xI.clear();
                }
                ((ds) eb()).disconnect();
            }
        } catch (RemoteException e) {
            try {
                xE.b("Error while disconnecting the controller interface: %s", e.getMessage());
            } catch (Throwable th) {
                super.disconnect();
                throw th;
            }
        }
        super.disconnect();
    }

    public void e(c<Status> cVar) throws IllegalStateException, RemoteException {
        f(cVar);
        ((ds) eb()).df();
    }

    public ApplicationMetadata getApplicationMetadata() throws IllegalStateException {
        db();
        return this.xF;
    }

    public String getApplicationStatus() throws IllegalStateException {
        db();
        return this.xK;
    }

    public boolean isMute() throws IllegalStateException {
        db();
        return this.xf;
    }

    public void t(boolean z) throws IllegalStateException, RemoteException {
        ((ds) eb()).a(z, this.xe, this.xf);
    }

    /* access modifiers changed from: protected */
    /* renamed from: v */
    public ds p(IBinder iBinder) {
        return com.google.android.gms.internal.ds.a.w(iBinder);
    }
}
