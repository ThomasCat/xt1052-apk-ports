package com.google.sample.castcompanionlibrary.cast;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v7.app.MediaRouteDialogFactory;
import android.support.v7.media.MediaRouter.RouteInfo;
import android.text.TextUtils;
import android.view.View;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.Cast.CastOptions;
import com.google.android.gms.cast.Cast.CastOptions.Builder;
import com.google.android.gms.cast.Cast.Listener;
import com.google.android.gms.cast.Cast.MessageReceivedCallback;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.CastStatusCodes;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.RemoteMediaPlayer;
import com.google.android.gms.cast.RemoteMediaPlayer.MediaChannelResult;
import com.google.android.gms.cast.RemoteMediaPlayer.OnMetadataUpdatedListener;
import com.google.android.gms.cast.RemoteMediaPlayer.OnStatusUpdatedListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.images.WebImage;
import com.google.sample.castcompanionlibrary.cast.BaseCastManager.ReconnectionStatus;
import com.google.sample.castcompanionlibrary.cast.callbacks.IVideoCastConsumer;
import com.google.sample.castcompanionlibrary.cast.dialog.video.VideoMediaRouteDialogFactory;
import com.google.sample.castcompanionlibrary.cast.exceptions.CastException;
import com.google.sample.castcompanionlibrary.cast.exceptions.NoConnectionException;
import com.google.sample.castcompanionlibrary.cast.exceptions.OnFailedListener;
import com.google.sample.castcompanionlibrary.cast.exceptions.TransientNetworkDisconnectionException;
import com.google.sample.castcompanionlibrary.cast.player.IMediaAuthService;
import com.google.sample.castcompanionlibrary.cast.player.VideoCastControllerActivity;
import com.google.sample.castcompanionlibrary.notification.VideoCastNotificationService;
import com.google.sample.castcompanionlibrary.remotecontrol.RemoteControlClientCompat;
import com.google.sample.castcompanionlibrary.remotecontrol.RemoteControlHelper;
import com.google.sample.castcompanionlibrary.remotecontrol.VideoIntentReceiver;
import com.google.sample.castcompanionlibrary.utils.LogUtils;
import com.google.sample.castcompanionlibrary.utils.Utils;
import com.google.sample.castcompanionlibrary.widgets.IMiniController;
import com.google.sample.castcompanionlibrary.widgets.MiniController.OnMiniControllerChangedListener;
import com.motorola.zap.R;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.json.JSONObject;

public class VideoCastManager extends BaseCastManager implements OnFailedListener, OnMiniControllerChangedListener {
    public static final String EXTRA_CUSTOM_DATA = "customData";
    public static final String EXTRA_HAS_AUTH = "hasAuth";
    public static final String EXTRA_MEDIA = "media";
    public static final String EXTRA_SHOULD_START = "shouldStart";
    public static final String EXTRA_START_POINT = "startPoint";
    /* access modifiers changed from: private */
    public static final String TAG = LogUtils.makeLogTag(VideoCastManager.class);
    private static VideoCastManager sInstance;
    private final AudioManager mAudioManager;
    private IMediaAuthService mAuthService;
    private MessageReceivedCallback mDataChannel;
    private final String mDataNamespace;
    private Handler mHandler;
    private int mIdleReason;
    private final ComponentName mMediaButtonReceiverComponent;
    private final Set<IMiniController> mMiniControllers;
    /* access modifiers changed from: private */
    public RemoteControlClientCompat mRemoteControlClientCompat;
    private RemoteMediaPlayer mRemoteMediaPlayer;
    private int mState = 1;
    private final Class<?> mTargetActivity;
    private Bitmap mVideoArtBitmap;
    /* access modifiers changed from: private */
    public Set<IVideoCastConsumer> mVideoConsumers;
    private VolumeType mVolumeType = VolumeType.DEVICE;

    class CastListener extends Listener {
        CastListener() {
        }

        public void onApplicationDisconnected(int statusCode) {
            VideoCastManager.this.onApplicationDisconnected(statusCode);
        }

        public void onApplicationStatusChanged() {
            VideoCastManager.this.onApplicationStatusChanged();
        }

        public void onVolumeChanged() {
            VideoCastManager.this.onVolumeChanged();
        }
    }

    private class UpdateNotificationHandlerCallback implements Callback {
        private UpdateNotificationHandlerCallback() {
        }

        public boolean handleMessage(Message msg) {
            boolean visibility = msg.what != 1;
            if (VideoCastManager.this.isFeatureEnabled(4)) {
                Intent intent = new Intent(VideoCastNotificationService.ACTION_VISIBILITY);
                intent.setPackage(VideoCastManager.this.mContext.getPackageName());
                intent.putExtra("visible", visibility);
                VideoCastManager.this.mContext.startService(intent);
            }
            return true;
        }
    }

    public enum VolumeType {
        STREAM,
        DEVICE
    }

    public static synchronized VideoCastManager initialize(Context context, String applicationId, Class<?> targetActivity, String dataNamespace) {
        VideoCastManager videoCastManager;
        synchronized (VideoCastManager.class) {
            if (sInstance == null) {
                LogUtils.LOGD(TAG, "New instance of VideoCastManager is created");
                if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(context) != 0) {
                    LogUtils.LOGE(TAG, "Couldn't find the appropriate version of Goolge Play Services");
                }
                sInstance = new VideoCastManager(context, applicationId, targetActivity, dataNamespace);
                mCastManager = sInstance;
            }
            videoCastManager = sInstance;
        }
        return videoCastManager;
    }

    public static VideoCastManager getInstance() throws CastException {
        if (sInstance != null) {
            return sInstance;
        }
        LogUtils.LOGE(TAG, "No VideoCastManager instance was built, you need to build one first");
        throw new CastException();
    }

    public static VideoCastManager getInstance(Context context) throws CastException {
        if (sInstance == null) {
            LogUtils.LOGE(TAG, "No VideoCastManager instance was built, you need to build one first (called from Context: " + context + ")");
            throw new CastException();
        }
        LogUtils.LOGD(TAG, "Updated context to: " + context);
        sInstance.mContext = context;
        return sInstance;
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.lang.Class<?>, code=java.lang.Class, for r6v0, types: [java.lang.Class<?>, java.lang.Class] */
    private VideoCastManager(Context context, String str, Class cls, String str2) {
        super(context, str);
        LogUtils.LOGD(TAG, "VideoCastManager is instantiated");
        this.mVideoConsumers = Collections.synchronizedSet(new HashSet());
        this.mDataNamespace = str2;
        if (cls == null) {
            cls = VideoCastControllerActivity.class;
        }
        this.mTargetActivity = cls;
        Utils.saveStringToPreference(this.mContext, BaseCastManager.PREFS_KEY_CAST_ACTIVITY_NAME, this.mTargetActivity.getName());
        if (this.mDataNamespace != null) {
            Utils.saveStringToPreference(this.mContext, BaseCastManager.PREFS_KEY_CAST_CUSTOM_DATA_NAMESPACE, str2);
        }
        this.mMiniControllers = Collections.synchronizedSet(new HashSet());
        this.mAudioManager = (AudioManager) context.getSystemService("audio");
        this.mMediaButtonReceiverComponent = new ComponentName(context, VideoIntentReceiver.class);
        this.mHandler = new Handler(new UpdateNotificationHandlerCallback());
    }

    private void updateMiniController(IMiniController controller) throws TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        checkRemoteMediaPlayerAvailable();
        if (this.mRemoteMediaPlayer.getStreamDuration() > 0 || isRemoteStreamLive()) {
            MediaInfo mediaInfo = getRemoteMediaInformation();
            MediaMetadata mm = mediaInfo.getMetadata();
            controller.setStreamType(mediaInfo.getStreamType());
            controller.setPlaybackStatus(this.mState, this.mIdleReason);
            controller.setSubTitle(this.mContext.getResources().getString(R.string.casting_to_device, new Object[]{this.mDeviceName}));
            controller.setTitle(mm.getString(MediaMetadata.KEY_TITLE));
            if (!mm.getImages().isEmpty()) {
                controller.setIcon(((WebImage) mm.getImages().get(0)).getUrl());
            }
        }
    }

    private void updateMiniControllers() {
        if (this.mMiniControllers != null && !this.mMiniControllers.isEmpty()) {
            synchronized (this.mMiniControllers) {
                for (IMiniController controller : this.mMiniControllers) {
                    try {
                        updateMiniController(controller);
                    } catch (Exception e) {
                    }
                }
            }
        }
    }

    public void onPlayPauseClicked(View v) throws CastException, TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        if (this.mState == 2) {
            pause();
            return;
        }
        boolean isLive = isRemoteStreamLive();
        if ((this.mState == 3 && !isLive) || (this.mState == 1 && isLive)) {
            play();
        }
    }

    public void onTargetActivityInvoked(Context ctx) throws TransientNetworkDisconnectionException, NoConnectionException {
        Intent intent = new Intent(ctx, this.mTargetActivity);
        intent.putExtra(EXTRA_MEDIA, Utils.fromMediaInfo(getRemoteMediaInformation()));
        ctx.startActivity(intent);
    }

    public void updateMiniControllersVisibility(boolean visible) {
        LogUtils.LOGD(TAG, "updateMiniControllersVisibility() reached with visibility: " + visible);
        if (this.mMiniControllers != null) {
            synchronized (this.mMiniControllers) {
                for (IMiniController controller : this.mMiniControllers) {
                    controller.setVisibility(visible ? 0 : 8);
                }
            }
        }
    }

    public void startCastControllerActivity(Context context, Bundle bundle, int i, boolean z, JSONObject jSONObject) {
        Intent intent = new Intent(context, VideoCastControllerActivity.class);
        intent.putExtra(EXTRA_MEDIA, bundle);
        intent.putExtra(EXTRA_START_POINT, i);
        intent.putExtra(EXTRA_SHOULD_START, z);
        if (jSONObject != null) {
            intent.putExtra(EXTRA_CUSTOM_DATA, jSONObject.toString());
        }
        context.startActivity(intent);
    }

    public void startCastControllerActivity(Context context, Bundle bundle, int i, boolean z) {
        Intent intent = new Intent(context, VideoCastControllerActivity.class);
        intent.putExtra(EXTRA_MEDIA, bundle);
        intent.putExtra(EXTRA_START_POINT, i);
        intent.putExtra(EXTRA_SHOULD_START, z);
        context.startActivity(intent);
    }

    public void startCastControllerActivity(Context context, IMediaAuthService iMediaAuthService) {
        if (iMediaAuthService != null) {
            this.mAuthService = iMediaAuthService;
            Intent intent = new Intent(context, VideoCastControllerActivity.class);
            intent.putExtra(EXTRA_HAS_AUTH, true);
            context.startActivity(intent);
        }
    }

    public void startCastControllerActivity(Context ctx, MediaInfo mediaInfo, int position, boolean shouldStart) {
        startCastControllerActivity(ctx, Utils.fromMediaInfo(mediaInfo), position, shouldStart);
    }

    public IMediaAuthService getMediaAuthService() {
        return this.mAuthService;
    }

    public void removeMediaAuthService() {
        this.mAuthService = null;
    }

    public final RemoteMediaPlayer getRemoteMediaPlayer() {
        return this.mRemoteMediaPlayer;
    }

    public final boolean isRemoteStreamLive() throws TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        MediaInfo info = getRemoteMediaInformation();
        return info != null && info.getStreamType() == 2;
    }

    public boolean shouldRemoteUiBeVisible(int state, int idleReason) throws TransientNetworkDisconnectionException, NoConnectionException {
        switch (state) {
            case 1:
                if (!isRemoteStreamLive()) {
                    return false;
                }
                return idleReason == 2;
            case 2:
            case 3:
            case 4:
                return true;
            default:
                return false;
        }
    }

    private void checkRemoteMediaPlayerAvailable() throws NoConnectionException {
        if (this.mRemoteMediaPlayer == null) {
            throw new NoConnectionException();
        }
    }

    public Bitmap getAlbumArt() {
        return this.mVideoArtBitmap;
    }

    public final void setVolumeType(VolumeType vType) {
        this.mVolumeType = vType;
    }

    public String getRemoteMovieUrl() throws TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        if (this.mRemoteMediaPlayer == null || this.mRemoteMediaPlayer.getMediaInfo() == null) {
            throw new NoConnectionException();
        }
        MediaInfo info = this.mRemoteMediaPlayer.getMediaInfo();
        this.mRemoteMediaPlayer.getMediaStatus().getPlayerState();
        return info.getContentId();
    }

    public boolean isRemoteMoviePlaying() throws TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        return this.mState == 4 || this.mState == 2;
    }

    public boolean isRemoteMoviePaused() throws TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        return this.mState == 3;
    }

    public boolean isRemoteMediaLoaded() throws TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        return isRemoteMoviePaused() || isRemoteMoviePlaying();
    }

    public MediaInfo getRemoteMediaInformation() throws TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        checkRemoteMediaPlayerAvailable();
        return this.mRemoteMediaPlayer.getMediaInfo();
    }

    public double getVolume() throws TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        if (this.mVolumeType != VolumeType.STREAM) {
            return Cast.CastApi.getVolume(this.mApiClient);
        }
        checkRemoteMediaPlayerAvailable();
        return this.mRemoteMediaPlayer.getMediaStatus().getStreamVolume();
    }

    public void setVolume(double volume) throws CastException, TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        if (volume > 1.0d) {
            volume = 1.0d;
        } else if (volume < 0.0d) {
            volume = 0.0d;
        }
        if (this.mVolumeType == VolumeType.STREAM) {
            checkRemoteMediaPlayerAvailable();
            this.mRemoteMediaPlayer.setStreamVolume(this.mApiClient, volume).setResultCallback(new ResultCallback<MediaChannelResult>() {
                public void onResult(MediaChannelResult result) {
                    if (!result.getStatus().isSuccess()) {
                        VideoCastManager.this.onFailed(R.string.failed_setting_volume, result.getStatus().getStatusCode());
                    }
                }
            });
            return;
        }
        try {
            Cast.CastApi.setVolume(this.mApiClient, volume);
        } catch (IOException e) {
            throw new CastException((Throwable) e);
        } catch (IllegalStateException e2) {
            throw new CastException((Throwable) e2);
        } catch (IllegalArgumentException e3) {
            throw new CastException((Throwable) e3);
        }
    }

    public void incrementVolume(double delta) throws CastException, TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        double vol = getVolume() + delta;
        if (vol > 1.0d) {
            vol = 1.0d;
        } else if (vol < 0.0d) {
            vol = 0.0d;
        }
        setVolume(vol);
    }

    public void updateVolume(int delta) {
        if (this.mMediaRouter.getSelectedRoute() != null) {
            this.mMediaRouter.getSelectedRoute().requestUpdateVolume(delta);
        }
    }

    public boolean isMute() throws TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        if (this.mVolumeType != VolumeType.STREAM) {
            return Cast.CastApi.isMute(this.mApiClient);
        }
        checkRemoteMediaPlayerAvailable();
        return this.mRemoteMediaPlayer.getMediaStatus().isMute();
    }

    public void setMute(boolean mute) throws CastException, TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        if (this.mVolumeType == VolumeType.STREAM) {
            checkRemoteMediaPlayerAvailable();
            this.mRemoteMediaPlayer.setStreamMute(this.mApiClient, mute);
            return;
        }
        try {
            Cast.CastApi.setMute(this.mApiClient, mute);
        } catch (Exception e) {
            LogUtils.LOGE(TAG, "Failed to set volume", e);
            throw new CastException("Failed to set volume", (Throwable) e);
        }
    }

    public long getMediaDuration() throws TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        checkRemoteMediaPlayerAvailable();
        return this.mRemoteMediaPlayer.getStreamDuration();
    }

    public long getCurrentMediaPosition() throws TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        checkRemoteMediaPlayerAvailable();
        return this.mRemoteMediaPlayer.getApproximateStreamPosition();
    }

    public Class<?> getTargetActivity() {
        return this.mTargetActivity;
    }

    /* access modifiers changed from: protected */
    public void onUiVisibilityChanged(boolean visible) {
        int i = 0;
        if (visible) {
            this.mHandler.removeMessages(0);
        }
        Handler handler = this.mHandler;
        if (visible) {
            i = 1;
        }
        handler.sendEmptyMessageDelayed(i, 300);
        super.onUiVisibilityChanged(visible);
    }

    private boolean startNotificationService() {
        if (!isFeatureEnabled(4)) {
            return true;
        }
        LogUtils.LOGD(TAG, "startNotificationService() ");
        Intent intent = new Intent(this.mContext, VideoCastNotificationService.class);
        intent.setPackage(this.mContext.getPackageName());
        if (this.mContext.startService(intent) == null) {
            return false;
        }
        return true;
    }

    private void stopNotificationService() {
        if (isFeatureEnabled(4) && this.mContext != null) {
            this.mContext.stopService(new Intent(this.mContext, VideoCastNotificationService.class));
        }
    }

    /* access modifiers changed from: private */
    public void onApplicationDisconnected(int errorCode) {
        LogUtils.LOGD(TAG, "onApplicationDisconnected() reached with error code: " + errorCode);
        updateRemoteControl(false);
        if (this.mRemoteControlClientCompat != null && isFeatureEnabled(2)) {
            this.mRemoteControlClientCompat.removeFromMediaRouter(this.mMediaRouter);
        }
        synchronized (this.mVideoConsumers) {
            for (IVideoCastConsumer consumer : this.mVideoConsumers) {
                try {
                    consumer.onApplicationDisconnected(errorCode);
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, "onApplicationDisconnected(): Failed to inform " + consumer, e);
                }
            }
        }
        if (this.mMediaRouter != null) {
            this.mMediaRouter.selectRoute(this.mMediaRouter.getDefaultRoute());
        }
        onDeviceSelected(null);
        updateMiniControllersVisibility(false);
        stopNotificationService();
    }

    /* access modifiers changed from: private */
    public void onApplicationStatusChanged() {
        if (isConnected()) {
            try {
                String appStatus = Cast.CastApi.getApplicationStatus(this.mApiClient);
                LogUtils.LOGD(TAG, "onApplicationStatusChanged() reached: " + Cast.CastApi.getApplicationStatus(this.mApiClient));
                synchronized (this.mVideoConsumers) {
                    for (IVideoCastConsumer consumer : this.mVideoConsumers) {
                        try {
                            consumer.onApplicationStatusChanged(appStatus);
                        } catch (Exception e) {
                            LogUtils.LOGE(TAG, "onApplicationStatusChanged(): Failed to inform " + consumer, e);
                        }
                    }
                }
            } catch (IllegalStateException e2) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void onVolumeChanged() {
        LogUtils.LOGD(TAG, "onVolumeChanged() reached");
        try {
            double volume = getVolume();
            boolean isMute = isMute();
            synchronized (this.mVideoConsumers) {
                for (IVideoCastConsumer consumer : this.mVideoConsumers) {
                    try {
                        consumer.onVolumeChanged(volume, isMute);
                    } catch (Exception e) {
                        LogUtils.LOGE(TAG, "onVolumeChanged(): Failed to inform " + consumer, e);
                    }
                }
            }
        } catch (Exception e1) {
            LogUtils.LOGE(TAG, "Failed to get volume", e1);
        }
    }

    /* access modifiers changed from: 0000 */
    public void onApplicationConnected(ApplicationMetadata appMetadata, String applicationStatus, String sessionId, boolean wasLaunched) {
        LogUtils.LOGD(TAG, "onApplicationConnected() reached with sessionId: " + sessionId + ", and mReconnectionStatus=" + this.mReconnectionStatus);
        if (this.mReconnectionStatus == ReconnectionStatus.IN_PROGRESS) {
            List<RouteInfo> routes = this.mMediaRouter.getRoutes();
            if (routes != null) {
                String routeId = Utils.getStringFromPreference(this.mContext, BaseCastManager.PREFS_KEY_ROUTE_ID);
                Iterator i$ = routes.iterator();
                while (true) {
                    if (!i$.hasNext()) {
                        break;
                    }
                    RouteInfo routeInfo = (RouteInfo) i$.next();
                    if (routeId.equals(routeInfo.getId())) {
                        LogUtils.LOGD(TAG, "Found the correct route during reconnection attempt");
                        this.mReconnectionStatus = ReconnectionStatus.FINALIZE;
                        this.mMediaRouter.selectRoute(routeInfo);
                        break;
                    }
                }
            }
        }
        startNotificationService();
        try {
            attachDataChannel();
            attachMediaChannel();
            this.mSessionId = sessionId;
            Utils.saveStringToPreference(this.mContext, BaseCastManager.PREFS_KEY_SESSION_ID, this.mSessionId);
            this.mRemoteMediaPlayer.requestStatus(this.mApiClient).setResultCallback(new ResultCallback<MediaChannelResult>() {
                public void onResult(MediaChannelResult result) {
                    if (!result.getStatus().isSuccess()) {
                        VideoCastManager.this.onFailed(R.string.failed_status_request, result.getStatus().getStatusCode());
                    }
                }
            });
            synchronized (this.mVideoConsumers) {
                for (IVideoCastConsumer consumer : this.mVideoConsumers) {
                    try {
                        consumer.onApplicationConnected(appMetadata, this.mSessionId, wasLaunched);
                    } catch (Exception e) {
                        LogUtils.LOGE(TAG, "onApplicationConnected(): Failed to inform " + consumer, e);
                    }
                }
            }
        } catch (TransientNetworkDisconnectionException e2) {
            LogUtils.LOGE(TAG, "Failed to attach media/data channel due to network issues", e2);
            onFailed(R.string.failed_no_connection_trans, -1);
        } catch (NoConnectionException e3) {
            LogUtils.LOGE(TAG, "Failed to attach media/data channel due to network issues", e3);
            onFailed(R.string.failed_no_connection, -1);
        }
    }

    public void onConnectivityRecovered() {
        reattachMediaChannel();
        reattachDataChannel();
        super.onConnectivityRecovered();
    }

    public void onApplicationStopFailed(int errorCode) {
        synchronized (this.mVideoConsumers) {
            for (IVideoCastConsumer consumer : this.mVideoConsumers) {
                try {
                    consumer.onApplicationStopFailed(errorCode);
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, "onApplicationLaunched(): Failed to inform " + consumer, e);
                }
            }
        }
    }

    public void onApplicationConnectionFailed(int errorCode) {
        LogUtils.LOGD(TAG, "onApplicationConnectionFailed() reached with errorCode: " + errorCode);
        if (this.mReconnectionStatus != ReconnectionStatus.IN_PROGRESS) {
            boolean showError = false;
            synchronized (this.mVideoConsumers) {
                for (IVideoCastConsumer consumer : this.mVideoConsumers) {
                    if (!showError) {
                        try {
                            if (!consumer.onApplicationConnectionFailed(errorCode)) {
                                showError = false;
                            }
                        } catch (Exception e) {
                            LogUtils.LOGE(TAG, "onApplicationLaunchFailed(): Failed to inform " + consumer, e);
                        }
                    }
                    showError = true;
                }
            }
            if (showError) {
                switch (errorCode) {
                    case 15:
                        LogUtils.LOGD(TAG, "onApplicationConnectionFailed(): failed due to: ERROR_TIMEOUT");
                        Utils.showErrorDialog(this.mContext, (int) R.string.failed_app_launch_timeout);
                        break;
                    case CastStatusCodes.APPLICATION_NOT_FOUND /*2004*/:
                        LogUtils.LOGD(TAG, "onApplicationConnectionFailed(): failed due to: ERROR_APPLICATION_NOT_FOUND");
                        Utils.showErrorDialog(this.mContext, (int) R.string.failed_to_find_app);
                        break;
                    default:
                        LogUtils.LOGD(TAG, "onApplicationConnectionFailed(): failed due to: errorcode=" + errorCode);
                        Utils.showErrorDialog(this.mContext, (int) R.string.failed_to_launch_app);
                        break;
                }
            }
            onDeviceSelected(null);
            if (this.mMediaRouter != null) {
                this.mMediaRouter.selectRoute(this.mMediaRouter.getDefaultRoute());
            }
        } else if (errorCode == 2005) {
            this.mReconnectionStatus = ReconnectionStatus.INACTIVE;
            onDeviceSelected(null);
        }
    }

    public void loadMedia(MediaInfo media, boolean autoPlay, int position) throws TransientNetworkDisconnectionException, NoConnectionException {
        loadMedia(media, autoPlay, position, null);
    }

    public void loadMedia(MediaInfo media, boolean autoPlay, int position, JSONObject customData) throws TransientNetworkDisconnectionException, NoConnectionException {
        LogUtils.LOGD(TAG, "loadMedia: " + media);
        checkConnectivity();
        if (media != null) {
            if (this.mRemoteMediaPlayer == null) {
                LogUtils.LOGE(TAG, "Trying to load a video with no active media session");
                throw new NoConnectionException();
            }
            this.mRemoteMediaPlayer.load(this.mApiClient, media, autoPlay, (long) position, customData).setResultCallback(new ResultCallback<MediaChannelResult>() {
                public void onResult(MediaChannelResult result) {
                    if (!result.getStatus().isSuccess()) {
                        VideoCastManager.this.onFailed(R.string.failed_load, result.getStatus().getStatusCode());
                    }
                }
            });
        }
    }

    public void play(int position) throws TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        LogUtils.LOGD(TAG, "attempting to play media at position " + position + " seconds");
        if (this.mRemoteMediaPlayer == null) {
            LogUtils.LOGE(TAG, "Trying to play a video with no active media session");
            throw new NoConnectionException();
        } else {
            seekAndPlay(position);
        }
    }

    public void play(JSONObject customData) throws TransientNetworkDisconnectionException, NoConnectionException {
        LogUtils.LOGD(TAG, "play(customData)");
        checkConnectivity();
        if (this.mRemoteMediaPlayer == null) {
            LogUtils.LOGE(TAG, "Trying to play a video with no active media session");
            throw new NoConnectionException();
        } else {
            this.mRemoteMediaPlayer.play(this.mApiClient, customData).setResultCallback(new ResultCallback<MediaChannelResult>() {
                public void onResult(MediaChannelResult result) {
                    if (!result.getStatus().isSuccess()) {
                        VideoCastManager.this.onFailed(R.string.failed_to_play, result.getStatus().getStatusCode());
                    }
                }
            });
        }
    }

    public void play() throws CastException, TransientNetworkDisconnectionException, NoConnectionException {
        play((JSONObject) null);
    }

    public void stop(JSONObject customData) throws TransientNetworkDisconnectionException, NoConnectionException {
        LogUtils.LOGD(TAG, "stop()");
        checkConnectivity();
        if (this.mRemoteMediaPlayer == null) {
            LogUtils.LOGE(TAG, "Trying to stop a stream with no active media session");
            throw new NoConnectionException();
        } else {
            this.mRemoteMediaPlayer.stop(this.mApiClient, customData).setResultCallback(new ResultCallback<MediaChannelResult>() {
                public void onResult(MediaChannelResult result) {
                    if (!result.getStatus().isSuccess()) {
                        VideoCastManager.this.onFailed(R.string.failed_to_stop, result.getStatus().getStatusCode());
                    }
                }
            });
        }
    }

    public void stop() throws CastException, TransientNetworkDisconnectionException, NoConnectionException {
        stop(null);
    }

    public void pause() throws CastException, TransientNetworkDisconnectionException, NoConnectionException {
        pause(null);
    }

    public void pause(JSONObject customData) throws TransientNetworkDisconnectionException, NoConnectionException {
        LogUtils.LOGD(TAG, "attempting to pause media");
        checkConnectivity();
        if (this.mRemoteMediaPlayer == null) {
            LogUtils.LOGE(TAG, "Trying to pause a video with no active media session");
            throw new NoConnectionException();
        } else {
            this.mRemoteMediaPlayer.pause(this.mApiClient, customData).setResultCallback(new ResultCallback<MediaChannelResult>() {
                public void onResult(MediaChannelResult result) {
                    if (!result.getStatus().isSuccess()) {
                        VideoCastManager.this.onFailed(R.string.failed_to_pause, result.getStatus().getStatusCode());
                    }
                }
            });
        }
    }

    public void seek(int i) throws TransientNetworkDisconnectionException, NoConnectionException {
        LogUtils.LOGD(TAG, "attempting to seek media");
        checkConnectivity();
        if (this.mRemoteMediaPlayer == null) {
            LogUtils.LOGE(TAG, "Trying to seek a video with no active media session");
            throw new NoConnectionException();
        } else {
            this.mRemoteMediaPlayer.seek(this.mApiClient, (long) i, 0).setResultCallback(new ResultCallback<MediaChannelResult>() {
                public void onResult(MediaChannelResult result) {
                    if (!result.getStatus().isSuccess()) {
                        VideoCastManager.this.onFailed(R.string.failed_seek, result.getStatus().getStatusCode());
                    }
                }
            });
        }
    }

    public void seekAndPlay(int i) throws TransientNetworkDisconnectionException, NoConnectionException {
        LogUtils.LOGD(TAG, "attempting to seek media");
        checkConnectivity();
        if (this.mRemoteMediaPlayer == null) {
            LogUtils.LOGE(TAG, "Trying to seekAndPlay a video with no active media session");
            throw new NoConnectionException();
        }
        this.mRemoteMediaPlayer.seek(this.mApiClient, (long) i, 1).setResultCallback(new ResultCallback<MediaChannelResult>() {
            public void onResult(MediaChannelResult result) {
                if (!result.getStatus().isSuccess()) {
                    VideoCastManager.this.onFailed(R.string.failed_seek, result.getStatus().getStatusCode());
                }
            }
        });
    }

    public void togglePlayback() throws CastException, TransientNetworkDisconnectionException, NoConnectionException {
        checkConnectivity();
        if (isRemoteMoviePlaying()) {
            pause();
        } else if (this.mState == 1 && this.mIdleReason == 1) {
            loadMedia(getRemoteMediaInformation(), true, 0);
        } else {
            play();
        }
    }

    private void attachMediaChannel() throws TransientNetworkDisconnectionException, NoConnectionException {
        LogUtils.LOGD(TAG, "attachMedia()");
        checkConnectivity();
        if (this.mRemoteMediaPlayer == null) {
            this.mRemoteMediaPlayer = new RemoteMediaPlayer();
            this.mRemoteMediaPlayer.setOnStatusUpdatedListener(new OnStatusUpdatedListener() {
                public void onStatusUpdated() {
                    LogUtils.LOGD(VideoCastManager.TAG, "RemoteMediaPlayer::onStatusUpdated() is reached");
                    VideoCastManager.this.onRemoteMediaPlayerStatusUpdated();
                }
            });
            this.mRemoteMediaPlayer.setOnMetadataUpdatedListener(new OnMetadataUpdatedListener() {
                public void onMetadataUpdated() {
                    LogUtils.LOGD(VideoCastManager.TAG, "RemoteMediaPlayer::onMetadataUpdated() is reached");
                    VideoCastManager.this.onRemoteMediaPlayerMetadataUpdated();
                }
            });
        }
        try {
            LogUtils.LOGD(TAG, "Registering MediaChannel namespace");
            Cast.CastApi.setMessageReceivedCallbacks(this.mApiClient, this.mRemoteMediaPlayer.getNamespace(), this.mRemoteMediaPlayer);
        } catch (Exception e) {
            LogUtils.LOGE(TAG, "Failed to set up media channel", e);
        }
    }

    private void reattachMediaChannel() {
        if (this.mRemoteMediaPlayer != null && this.mApiClient != null) {
            try {
                LogUtils.LOGD(TAG, "Registering MediaChannel namespace");
                Cast.CastApi.setMessageReceivedCallbacks(this.mApiClient, this.mRemoteMediaPlayer.getNamespace(), this.mRemoteMediaPlayer);
            } catch (IOException e) {
                LogUtils.LOGE(TAG, "Failed to setup media channel", e);
            } catch (IllegalStateException e2) {
                LogUtils.LOGE(TAG, "Failed to setup media channel", e2);
            }
        }
    }

    private void detachMediaChannel() {
        LogUtils.LOGD(TAG, "trying to detach media channel");
        if (this.mRemoteMediaPlayer != null) {
            if (!(this.mRemoteMediaPlayer == null || Cast.CastApi == null)) {
                try {
                    Cast.CastApi.removeMessageReceivedCallbacks(this.mApiClient, this.mRemoteMediaPlayer.getNamespace());
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, "Failed to detach media channel", e);
                }
            }
            this.mRemoteMediaPlayer = null;
        }
    }

    public int getPlaybackStatus() {
        return this.mState;
    }

    public int getIdleReason() {
        return this.mIdleReason;
    }

    private void attachDataChannel() throws TransientNetworkDisconnectionException, NoConnectionException {
        if (!TextUtils.isEmpty(this.mDataNamespace) && this.mDataChannel == null) {
            checkConnectivity();
            this.mDataChannel = new MessageReceivedCallback() {
                public void onMessageReceived(CastDevice castDevice, String namespace, String message) {
                    synchronized (VideoCastManager.this.mVideoConsumers) {
                        for (IVideoCastConsumer consumer : VideoCastManager.this.mVideoConsumers) {
                            try {
                                consumer.onDataMessageReceived(message);
                            } catch (Exception e) {
                                LogUtils.LOGE(VideoCastManager.TAG, "onMessageReceived(): Failed to inform " + consumer, e);
                            }
                        }
                    }
                }
            };
            try {
                Cast.CastApi.setMessageReceivedCallbacks(this.mApiClient, this.mDataNamespace, this.mDataChannel);
            } catch (IOException e) {
                LogUtils.LOGE(TAG, "Failed to setup data channel", e);
            } catch (IllegalStateException e2) {
                LogUtils.LOGE(TAG, "Failed to setup data channel", e2);
            }
        }
    }

    private void reattachDataChannel() {
        if (!TextUtils.isEmpty(this.mDataNamespace) && this.mDataChannel != null && this.mApiClient != null) {
            try {
                Cast.CastApi.setMessageReceivedCallbacks(this.mApiClient, this.mDataNamespace, this.mDataChannel);
            } catch (IOException e) {
                LogUtils.LOGE(TAG, "Failed to setup data channel", e);
            } catch (IllegalStateException e2) {
                LogUtils.LOGE(TAG, "Failed to setup data channel", e2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void onMessageSendFailed(int i) {
        synchronized (this.mVideoConsumers) {
            for (IVideoCastConsumer iVideoCastConsumer : this.mVideoConsumers) {
                try {
                    iVideoCastConsumer.onDataMessageSendFailed(i);
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, "onMessageSendFailed(): Failed to inform " + iVideoCastConsumer, e);
                }
            }
        }
    }

    public void sendDataMessage(String str) throws TransientNetworkDisconnectionException, NoConnectionException {
        if (TextUtils.isEmpty(this.mDataNamespace)) {
            throw new IllegalStateException("No Data Namespace is configured");
        }
        checkConnectivity();
        Cast.CastApi.sendMessage(this.mApiClient, this.mDataNamespace, str).setResultCallback(new ResultCallback<Status>() {
            public void onResult(Status result) {
                if (!result.isSuccess()) {
                    VideoCastManager.this.onMessageSendFailed(result.getStatusCode());
                }
            }
        });
    }

    public boolean removeDataChannel() {
        if (TextUtils.isEmpty(this.mDataNamespace)) {
            return false;
        }
        try {
            if (!(Cast.CastApi == null || this.mApiClient == null)) {
                Cast.CastApi.removeMessageReceivedCallbacks(this.mApiClient, this.mDataNamespace);
            }
            this.mDataChannel = null;
            Utils.saveStringToPreference(this.mContext, BaseCastManager.PREFS_KEY_CAST_CUSTOM_DATA_NAMESPACE, null);
            return true;
        } catch (Exception e) {
            LogUtils.LOGE(TAG, "Failed to remove namespace: " + this.mDataNamespace, e);
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void onRemoteMediaPlayerStatusUpdated() {
        boolean z;
        boolean z2 = true;
        LogUtils.LOGD(TAG, "onRemoteMediaPlayerStatusUpdated() reached");
        if (this.mApiClient == null || this.mRemoteMediaPlayer == null || this.mRemoteMediaPlayer.getMediaStatus() == null) {
            LogUtils.LOGD(TAG, "mApiClient or mRemoteMediaPlayer is null, so will not proceed");
            return;
        }
        this.mState = this.mRemoteMediaPlayer.getMediaStatus().getPlayerState();
        this.mIdleReason = this.mRemoteMediaPlayer.getMediaStatus().getIdleReason();
        try {
            double volume = getVolume();
            boolean isMute = isMute();
            if (this.mState == 2) {
                LogUtils.LOGD(TAG, "onRemoteMediaPlayerStatusUpdated(): Player status = playing");
                updateRemoteControl(true);
                z = false;
            } else if (this.mState == 3) {
                LogUtils.LOGD(TAG, "onRemoteMediaPlayerStatusUpdated(): Player status = paused");
                updateRemoteControl(false);
                z = false;
            } else if (this.mState == 1) {
                LogUtils.LOGD(TAG, "onRemoteMediaPlayerStatusUpdated(): Player status = idle");
                updateRemoteControl(false);
                if (this.mIdleReason == 1) {
                    removeRemoteControlClient();
                    z = true;
                } else if (this.mIdleReason == 4) {
                    LogUtils.LOGD(TAG, "onRemoteMediaPlayerStatusUpdated(): IDLE reason = ERROR");
                    removeRemoteControlClient();
                    onFailed(R.string.failed_receiver_player_error, -1);
                    z = true;
                } else if (this.mIdleReason == 2) {
                    LogUtils.LOGD(TAG, "onRemoteMediaPlayerStatusUpdated(): IDLE reason = CANCELLED");
                    z = !isRemoteStreamLive();
                } else {
                    z = false;
                }
            } else if (this.mState == 4) {
                LogUtils.LOGD(TAG, "onRemoteMediaPlayerStatusUpdated(): Player status = buffering");
                z = false;
            } else {
                LogUtils.LOGD(TAG, "onRemoteMediaPlayerStatusUpdated(): Player status = unknown");
                z = true;
            }
            if (z) {
                stopNotificationService();
            }
            if (z) {
                z2 = false;
            }
            updateMiniControllersVisibility(z2);
            updateMiniControllers();
            synchronized (this.mVideoConsumers) {
                for (IVideoCastConsumer iVideoCastConsumer : this.mVideoConsumers) {
                    try {
                        iVideoCastConsumer.onRemoteMediaPlayerStatusUpdated();
                        iVideoCastConsumer.onVolumeChanged(volume, isMute);
                    } catch (Exception e) {
                        LogUtils.LOGE(TAG, "onRemoteMediaPlayerStatusUpdated(): Failed to inform " + iVideoCastConsumer, e);
                    }
                }
            }
        } catch (TransientNetworkDisconnectionException e2) {
            LogUtils.LOGE(TAG, "Failed to get volume state due to network issues", e2);
        } catch (NoConnectionException e3) {
            LogUtils.LOGE(TAG, "Failed to get volume state due to network issues", e3);
        }
    }

    public void onRemoteMediaPlayerMetadataUpdated() {
        LogUtils.LOGD(TAG, "onRemoteMediaPlayerMetadataUpdated() reached");
        updateLockScreenMetadata();
        synchronized (this.mVideoConsumers) {
            for (IVideoCastConsumer iVideoCastConsumer : this.mVideoConsumers) {
                try {
                    iVideoCastConsumer.onRemoteMediaPlayerMetadataUpdated();
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, "onRemoteMediaPlayerMetadataUpdated(): Failed to inform " + iVideoCastConsumer, e);
                }
            }
        }
        updateLockScreenMetadata();
        try {
            updateLockScreenImage(getRemoteMediaInformation());
        } catch (TransientNetworkDisconnectionException e2) {
            LogUtils.LOGE(TAG, "Failed to update lock screen metadaa due to a network issue", e2);
        } catch (NoConnectionException e3) {
            LogUtils.LOGE(TAG, "Failed to update lock screen metadaa due to a network issue", e3);
        }
    }

    @SuppressLint({"InlinedApi"})
    private void setUpRemoteControl(MediaInfo mediaInfo) {
        if (isFeatureEnabled(2)) {
            LogUtils.LOGD(TAG, "setupRemoteControl() was called");
            this.mAudioManager.requestAudioFocus(null, 3, 3);
            this.mAudioManager.registerMediaButtonEventReceiver(new ComponentName(this.mContext, VideoIntentReceiver.class.getName()));
            if (this.mRemoteControlClientCompat == null) {
                Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
                intent.setComponent(this.mMediaButtonReceiverComponent);
                this.mRemoteControlClientCompat = new RemoteControlClientCompat(PendingIntent.getBroadcast(this.mContext, 0, intent, 0));
                RemoteControlHelper.registerRemoteControlClient(this.mAudioManager, this.mRemoteControlClientCompat);
            }
            this.mRemoteControlClientCompat.addToMediaRouter(this.mMediaRouter);
            this.mRemoteControlClientCompat.setTransportControlFlags(8);
            if (mediaInfo == null) {
                this.mRemoteControlClientCompat.setPlaybackState(2);
                return;
            }
            this.mRemoteControlClientCompat.setPlaybackState(3);
            updateLockScreenImage(mediaInfo);
            updateLockScreenMetadata();
        }
    }

    private void updateLockScreenImage(final MediaInfo info) {
        if (info != null) {
            new Thread(new Runnable() {
                public void run() {
                    if (VideoCastManager.this.mRemoteControlClientCompat != null) {
                        try {
                            Bitmap bm = VideoCastManager.this.getBitmapForLockScreen(info);
                            if (bm != null) {
                                VideoCastManager.this.mRemoteControlClientCompat.editMetadata(false).putBitmap(100, bm).apply();
                            }
                        } catch (Exception e) {
                            LogUtils.LOGD(VideoCastManager.TAG, "Failed to update lock screen image", e);
                        }
                    }
                }
            }).start();
        }
    }

    /* access modifiers changed from: private */
    public Bitmap getBitmapForLockScreen(MediaInfo mediaInfo) {
        Bitmap bitmap;
        URL url;
        URL url2 = null;
        if (mediaInfo == null) {
            return null;
        }
        List images = mediaInfo.getMetadata().getImages();
        try {
            if (VERSION.SDK_INT > 18) {
                if (images.size() > 1) {
                    URL url3 = new URL(((WebImage) images.get(1)).getUrl().toString());
                    bitmap = null;
                    url2 = url3;
                } else if (images.size() == 1) {
                    URL url4 = new URL(((WebImage) images.get(0)).getUrl().toString());
                    bitmap = null;
                    url2 = url4;
                } else if (this.mContext != null) {
                    bitmap = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.dummy_album_art_large);
                } else {
                    bitmap = null;
                }
            } else if (!images.isEmpty()) {
                URL url5 = new URL(((WebImage) images.get(0)).getUrl().toString());
                bitmap = null;
                url2 = url5;
            } else {
                bitmap = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.dummy_album_art_small);
            }
            url = url2;
        } catch (MalformedURLException e) {
            LogUtils.LOGE(TAG, "Failed to get the url for images", e);
            bitmap = null;
            url = null;
        }
        if (url != null) {
            try {
                bitmap = BitmapFactory.decodeStream(url.openStream());
            } catch (IOException e2) {
                LogUtils.LOGE(TAG, "Failed to decoded a bitmap for url: " + url, e2);
            }
        }
        return bitmap;
    }

    @SuppressLint({"InlinedApi"})
    private void updateRemoteControl(boolean z) {
        if (isFeatureEnabled(2)) {
            if (!isConnected()) {
                removeRemoteControlClient();
                return;
            }
            try {
                if (this.mRemoteControlClientCompat == null && z) {
                    setUpRemoteControl(getRemoteMediaInformation());
                }
                if (this.mRemoteControlClientCompat != null) {
                    int i = isRemoteStreamLive() ? 8 : 3;
                    RemoteControlClientCompat remoteControlClientCompat = this.mRemoteControlClientCompat;
                    if (!z) {
                        i = 2;
                    }
                    remoteControlClientCompat.setPlaybackState(i);
                }
            } catch (TransientNetworkDisconnectionException e) {
                LogUtils.LOGE(TAG, "Failed to setup RCC due to network issues", e);
            } catch (NoConnectionException e2) {
                LogUtils.LOGE(TAG, "Failed to setup RCC due to network issues", e2);
            }
        }
    }

    private void updateLockScreenMetadata() {
        if (this.mRemoteControlClientCompat != null && isFeatureEnabled(2)) {
            try {
                MediaInfo remoteMediaInformation = getRemoteMediaInformation();
                if (remoteMediaInformation != null) {
                    this.mRemoteControlClientCompat.editMetadata(false).putString(7, remoteMediaInformation.getMetadata().getString(MediaMetadata.KEY_TITLE)).putString(13, this.mContext.getResources().getString(R.string.casting_to_device, new Object[]{getDeviceName()})).putLong(9, remoteMediaInformation.getStreamDuration()).apply();
                }
            } catch (NotFoundException e) {
                LogUtils.LOGE(TAG, "Failed to update RCC due to resource not found", e);
            } catch (TransientNetworkDisconnectionException e2) {
                LogUtils.LOGE(TAG, "Failed to update RCC due to network issues", e2);
            } catch (NoConnectionException e3) {
                LogUtils.LOGE(TAG, "Failed to update RCC due to network issues", e3);
            }
        }
    }

    private void removeRemoteControlClient() {
        if (isFeatureEnabled(2)) {
            this.mAudioManager.abandonAudioFocus(null);
            if (this.mRemoteControlClientCompat != null) {
                RemoteControlHelper.unregisterRemoteControlClient(this.mAudioManager, this.mRemoteControlClientCompat);
                this.mRemoteControlClientCompat = null;
            }
        }
    }

    public synchronized void addVideoCastConsumer(IVideoCastConsumer iVideoCastConsumer) {
        if (iVideoCastConsumer != null) {
            super.addBaseCastConsumer(iVideoCastConsumer);
            synchronized (this.mVideoConsumers) {
                this.mVideoConsumers.add(iVideoCastConsumer);
            }
            LogUtils.LOGD(TAG, "Successfully added the new CastConsumer listener " + iVideoCastConsumer);
        }
    }

    public synchronized void removeVideoCastConsumer(IVideoCastConsumer listener) {
        if (listener != null) {
            super.removeBaseCastConsumer(listener);
            synchronized (this.mVideoConsumers) {
                this.mVideoConsumers.remove(listener);
            }
        }
    }

    public void addMiniController(IMiniController iMiniController, OnMiniControllerChangedListener onMiniControllerChangedListener) {
        boolean add;
        if (iMiniController != null) {
            synchronized (this.mMiniControllers) {
                add = this.mMiniControllers.add(iMiniController);
            }
            if (add) {
                if (onMiniControllerChangedListener == 0) {
                    onMiniControllerChangedListener = this;
                }
                iMiniController.setOnMiniControllerChangedListener(onMiniControllerChangedListener);
                try {
                    if (isConnected() && isRemoteMediaLoaded()) {
                        updateMiniController(iMiniController);
                        iMiniController.setVisibility(0);
                    }
                } catch (TransientNetworkDisconnectionException e) {
                    LogUtils.LOGE(TAG, "Failed to get the status of media playback on receiver", e);
                } catch (NoConnectionException e2) {
                    LogUtils.LOGE(TAG, "Failed to get the status of media playback on receiver", e2);
                }
                LogUtils.LOGD(TAG, "Successfully added the new MiniController " + iMiniController);
                return;
            }
            LogUtils.LOGD(TAG, "Attempting to adding " + iMiniController + " but it was already " + "registered, skipping this step");
        }
    }

    public void addMiniController(IMiniController miniController) {
        addMiniController(miniController, null);
    }

    public void removeMiniController(IMiniController listener) {
        if (listener != null) {
            synchronized (this.mMiniControllers) {
                this.mMiniControllers.remove(listener);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void onDeviceUnselected() {
        stopNotificationService();
        detachMediaChannel();
        removeDataChannel();
        this.mState = 1;
    }

    /* access modifiers changed from: 0000 */
    public Builder getCastOptionBuilder(CastDevice device) {
        Builder builder = CastOptions.builder(this.mSelectedCastDevice, new CastListener());
        if (isFeatureEnabled(1)) {
            builder.setVerboseLoggingEnabled(true);
        }
        return builder;
    }

    public void onConnectionFailed(ConnectionResult result) {
        super.onConnectionFailed(result);
        updateRemoteControl(false);
        stopNotificationService();
    }

    public void onDisconnected() {
        super.onDisconnected();
        updateMiniControllersVisibility(false);
        stopNotificationService();
        removeRemoteControlClient();
        this.mState = 1;
    }

    /* access modifiers changed from: 0000 */
    public MediaRouteDialogFactory getMediaRouteDialogFactory() {
        return new VideoMediaRouteDialogFactory();
    }

    public void onFailed(int i, int i2) {
        LogUtils.LOGD(TAG, "onFailed: " + this.mContext.getString(i) + ", code: " + i2);
        super.onFailed(i, i2);
    }
}
