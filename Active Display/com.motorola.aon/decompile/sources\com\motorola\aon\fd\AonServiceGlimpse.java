package com.motorola.aon.fd;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;
import com.motorola.aon.AonService;
import com.motorola.aon.Constants;
import com.motorola.aon.Constants.FdServiceMessage;
import com.motorola.aon.R;
import com.motorola.aon.SnAnalyticsEventService;
import com.motorola.aon.aidl.NotificationEntry;
import com.motorola.aon.env.android.app.StatusBarManager;
import com.motorola.aon.env.android.content.Context;
import com.motorola.aon.env.android.os.ServiceManager;
import com.motorola.aon.env.com.android.internal.statusbar.IStatusBarService.Stub;
import com.motorola.aon.env.com.motorola.slpc.ModalityManager;
import com.motorola.aon.env.com.motorola.slpc.ModalityManager.MovementListener;
import com.motorola.aon.env.com.motorola.slpc.ModalityManager.OrientationListener;
import com.motorola.aon.env.com.motorola.slpc.ModalityManager.StowedListener;
import com.motorola.aon.env.com.motorola.slpc.Transition;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class AonServiceGlimpse extends AonService {
    public static final String ACTION_BIND_AONSERVICEGLIMPSE = "com.motorola.aon.AonServiceGlimpse";
    public static final String ACTION_LAUNCH_NOTIFICATION = "com.motorola.aon.LAUNCH_NOTIFICATION";
    public static final String ACTION_UNLOCK = "com.motorola.aon.UNLOCK";
    private static final long AOD_RAMP_UP_DURATION = 1500;
    protected static final boolean CURRENT_DRAIN_DEBUG = Constants.CURRENT_DRAIN_DEBUG;
    private static final int DEFAULT_BRIGHTNESS = 14;
    private static final long ORIENTATION_DELAY = 100;
    private static final String TAG = "AonServiceGlimpse";
    private static final String TAG_BRIGHTNESS = "aod_brightness";
    private static AonViewManagerGlimpse mAonViewManager = null;
    /* access modifiers changed from: private */
    public boolean mActivityDestroyed = true;
    /* access modifiers changed from: private */
    public Messenger mActivityMessenger = null;
    /* access modifiers changed from: private */
    public long mAodStartTime = 0;
    private Sensor mBrightnessSensor;
    /* access modifiers changed from: private */
    public int mCurrentBrightness = 14;
    private OrientationListener mFacedownListener = new OrientationListener() {
        public void onOrientationChange(Transition transition) {
            if (transition.getOldState() == ModalityManager.ORIENTATION_FACE_DOWN) {
                boolean isStowed = true;
                if (AonServiceGlimpse.CURRENT_DRAIN_DEBUG) {
                    Log.d(AonServiceGlimpse.TAG, "Phone is NOT in facedown");
                }
                if (AonService.DEBUG) {
                    Log.d(AonServiceGlimpse.TAG, "facedown false");
                }
                if (AonServiceGlimpse.this.mModalityManager != null) {
                    Transition tran = AonServiceGlimpse.this.mModalityManager.getStowed();
                    if (tran != null) {
                        isStowed = tran.getNewState() == ModalityManager.STOWED_TRUE;
                    }
                }
                if (!isStowed) {
                    if (!AonServiceGlimpse.this.mGlimpseAlive) {
                        SnAnalyticsEventService.recordFacedownExit();
                    }
                    AonServiceGlimpse.this.mHandler.sendMessage(Message.obtain(null, 0));
                }
            }
        }
    };
    private OrientationListener mFlatUnknownListener = new OrientationListener() {
        public void onOrientationChange(Transition transition) {
            int newState = transition.getNewState();
            int oldState = transition.getOldState();
            if (newState != ModalityManager.ORIENTATION_UNKNOWN) {
                return;
            }
            if (oldState == ModalityManager.ORIENTATION_FACE_DOWN || oldState == ModalityManager.ORIENTATION_FACE_UP) {
                boolean isStowed = true;
                if (AonServiceGlimpse.CURRENT_DRAIN_DEBUG) {
                    Log.d(AonServiceGlimpse.TAG, "Phone is not face up/down");
                }
                if (AonService.DEBUG) {
                    Log.d(AonServiceGlimpse.TAG, "phone exit face up/down");
                }
                if (AonServiceGlimpse.this.mModalityManager != null) {
                    Transition tran = AonServiceGlimpse.this.mModalityManager.getStowed();
                    if (tran != null) {
                        isStowed = tran.getNewState() == ModalityManager.STOWED_TRUE;
                    }
                }
                if (!isStowed) {
                    if (!AonServiceGlimpse.this.mGlimpseAlive) {
                        if (oldState == ModalityManager.ORIENTATION_FACE_DOWN) {
                            SnAnalyticsEventService.recordFacedownExit();
                        } else if (oldState == ModalityManager.ORIENTATION_FACE_UP) {
                            SnAnalyticsEventService.recordFlatupExit();
                        }
                    }
                    AonServiceGlimpse.this.mHandler.sendMessage(Message.obtain(null, 0));
                }
            }
        }
    };
    private long mFlatupCoolDown;
    private boolean mFlatupEnable = false;
    /* access modifiers changed from: private */
    public boolean mGlimpseAlive = false;
    private GlimpseServer mGlimpseServer = new GlimpseServer();
    /* access modifiers changed from: private */
    public boolean mGlimpseSleepPhone = false;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (AonServiceGlimpse.this.mStoping) {
                Log.i(AonServiceGlimpse.TAG, "service is killing itself, ignore coming messages.");
                return;
            }
            switch (msg.what) {
                case 0:
                    if (!AonServiceGlimpse.this.mGlimpseAlive && AonServiceGlimpse.this.isGlimpseEnabled()) {
                        AonServiceGlimpse.this.updateBreathingView();
                        AonServiceGlimpse.this.startGlimpseActivity();
                        return;
                    } else if (!AonServiceGlimpse.this.mGlimpseAlive || AonServiceGlimpse.this.mGlimpseSleepPhone) {
                        if (AonServiceGlimpse.this.mGlimpseSleepPhone) {
                            AonServiceGlimpse.this.mPendingAODLaunch = true;
                            return;
                        }
                        return;
                    } else if (AonServiceGlimpse.this.mActivityMessenger != null) {
                        try {
                            AonServiceGlimpse.this.mActivityMessenger.send(Message.obtain(null, 11));
                            return;
                        } catch (RemoteException e) {
                            Log.e(AonServiceGlimpse.TAG, "The AOD activity has died.");
                            return;
                        }
                    } else {
                        return;
                    }
                case 1:
                    if (!AonServiceGlimpse.this.mGlimpseAlive && AonServiceGlimpse.this.isGlimpseEnabled()) {
                        SnAnalyticsEventService.recordIncomingAls();
                        AonServiceGlimpse.this.startGlimpseActivity();
                        return;
                    } else if (!AonServiceGlimpse.this.mGlimpseAlive || AonServiceGlimpse.this.mGlimpseSleepPhone) {
                        if (AonServiceGlimpse.this.mGlimpseSleepPhone) {
                            AonServiceGlimpse.this.mPendingAODLaunch = true;
                            return;
                        }
                        return;
                    } else if (AonServiceGlimpse.this.mActivityMessenger != null) {
                        try {
                            AonServiceGlimpse.this.mActivityMessenger.send(Message.obtain(null, 14));
                            return;
                        } catch (RemoteException e2) {
                            Log.e(AonServiceGlimpse.TAG, "Update glimpse failed.");
                            return;
                        }
                    } else {
                        return;
                    }
                case 3:
                    if (AonService.DEBUG) {
                        Log.d(AonServiceGlimpse.TAG, "Get glimpse alive message");
                    }
                    try {
                        AonServiceGlimpse.this.mAonInterface.disableTSB(StatusBarManager.DISABLE_BACK | StatusBarManager.DISABLE_HOME | StatusBarManager.DISABLE_RECENT | StatusBarManager.DISABLE_SEARCH);
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        Log.e(AonServiceGlimpse.TAG, "Failed to call disableTSB");
                    }
                    synchronized (AonServiceGlimpse.this.mSleepLock) {
                        AonServiceGlimpse.this.mGlimpseAlive = true;
                    }
                    return;
                case 4:
                    if (AonService.DEBUG) {
                        Log.d(AonServiceGlimpse.TAG, "Get glimpse exit message");
                    }
                    synchronized (AonServiceGlimpse.this.mSleepLock) {
                        AonServiceGlimpse.this.mActivityMessenger = null;
                        AonServiceGlimpse.this.mGlimpseAlive = false;
                        AonServiceGlimpse.this.mAodStartTime = 0;
                    }
                    return;
                case 5:
                    if (AonService.DEBUG) {
                        Log.d(AonServiceGlimpse.TAG, "Get GLIMPSE_SLEEP_PHONE message");
                    }
                    synchronized (AonServiceGlimpse.this.mSleepLock) {
                        if (AonService.DEBUG) {
                            Log.d(AonServiceGlimpse.TAG, "sleep phone get lock");
                        }
                        if (!AonServiceGlimpse.this.mPowerKeyDismissGlimpse) {
                            try {
                                AonServiceGlimpse.this.mAonInterface.goToSleep(SystemClock.uptimeMillis());
                            } catch (Exception e4) {
                                e4.printStackTrace();
                                Log.e(AonServiceGlimpse.TAG, "Failed to call goToSleep");
                            }
                            AonServiceGlimpse.this.mGlimpseSleepPhone = true;
                            AonServiceGlimpse.this.mGlimpseAlive = false;
                            AonServiceGlimpse.this.mAodStartTime = 0;
                        }
                    }
                    return;
                case 6:
                    ((AonNotificationManagerGlimpse) AonServiceGlimpse.this.mAonNotificationManager).clearNotifications();
                    try {
                        Stub.asInterface(ServiceManager.getService(Context.STATUS_BAR_SERVICE)).onPanelRevealed();
                        return;
                    } catch (RemoteException e5) {
                        Log.e(AonServiceGlimpse.TAG, "IStatusBarService error");
                        return;
                    }
                case 7:
                    AonServiceGlimpse.this.mStoping = true;
                    AonServiceGlimpse.this.stopSelf();
                    return;
                case 9:
                    if (AonServiceGlimpse.this.mActivityMessenger != null) {
                        try {
                            Message message = Message.obtain(null, 8);
                            message.arg1 = msg.arg1;
                            AonServiceGlimpse.this.mActivityMessenger.send(message);
                            return;
                        } catch (RemoteException e6) {
                            AonServiceGlimpse.this.mActivityMessenger = null;
                            AonServiceGlimpse.this.mGlimpseAlive = false;
                            AonServiceGlimpse.this.mAodStartTime = 0;
                            Log.e(AonServiceGlimpse.TAG, "The AOD activity has died.");
                            return;
                        }
                    } else {
                        return;
                    }
                case 10:
                    AonServiceGlimpse.this.stopGlimpseActivity();
                    return;
                case FdServiceMessage.GLIMPSE_MESSENGER /*12*/:
                    synchronized (AonServiceGlimpse.this.mSleepLock) {
                        if (AonService.DEBUG) {
                            Log.d(AonServiceGlimpse.TAG, "get activity messenger");
                        }
                        if (msg.replyTo != null) {
                            AonServiceGlimpse.this.mActivityMessenger = msg.replyTo;
                        }
                        if (AonServiceGlimpse.this.mPowerKeyDismissGlimpse) {
                            AonServiceGlimpse.this.mHandler.sendEmptyMessage(10);
                        } else {
                            AonServiceGlimpse.this.handleBrightnessChanged(AonServiceGlimpse.this.mCurrentBrightness);
                        }
                    }
                    return;
                case FdServiceMessage.GLIMPSE_DESTROYED /*13*/:
                    if (AonService.DEBUG) {
                        Log.d(AonServiceGlimpse.TAG, "Get glimpse destory message");
                    }
                    try {
                        AonServiceGlimpse.this.mAonInterface.disableTSB(StatusBarManager.DISABLE_NONE);
                    } catch (Exception e7) {
                        e7.printStackTrace();
                        Log.e(AonServiceGlimpse.TAG, "Failed to call disableTSB");
                    }
                    synchronized (AonServiceGlimpse.this.mSleepLock) {
                        AonServiceGlimpse.this.mActivityMessenger = null;
                        AonServiceGlimpse.this.mGlimpseAlive = false;
                        AonServiceGlimpse.this.mAodStartTime = 0;
                        AonServiceGlimpse.this.mActivityDestroyed = true;
                    }
                    if (AonServiceGlimpse.this.mPendingAODLaunch && AonServiceGlimpse.this.mScreenOff) {
                        AonServiceGlimpse.this.mPendingAODLaunch = false;
                        AonServiceGlimpse.this.mHandler.sendEmptyMessage(0);
                        return;
                    }
                    return;
                default:
                    Log.e(AonServiceGlimpse.TAG, "handleMessage: unknown message encounted, " + msg.what);
                    return;
            }
        }
    };
    private Messenger mMessenger = new Messenger(this.mHandler);
    /* access modifiers changed from: private */
    public ModalityManager mModalityManager;
    private MovementListener mMovementListener = new MovementListener() {
        public void onStartMovement() {
            boolean isFaceDown = true;
            boolean isStowed = true;
            if (AonServiceGlimpse.this.mModalityManager != null) {
                Transition tran = AonServiceGlimpse.this.mModalityManager.getOrientation();
                if (tran != null) {
                    if (tran.getNewState() == ModalityManager.ORIENTATION_FACE_DOWN) {
                        isFaceDown = true;
                    } else {
                        isFaceDown = false;
                    }
                }
                Transition tran2 = AonServiceGlimpse.this.mModalityManager.getStowed();
                if (tran2 != null) {
                    isStowed = tran2.getNewState() == ModalityManager.STOWED_TRUE;
                }
            }
            if (!isStowed && !isFaceDown) {
                if (AonService.DEBUG) {
                    Log.d(AonServiceGlimpse.TAG, "nudge event and Glimpse will be launched");
                }
                if (AonServiceGlimpse.CURRENT_DRAIN_DEBUG) {
                    Log.d(AonServiceGlimpse.TAG, "A movement is detected");
                }
                if (!AonServiceGlimpse.this.mGlimpseAlive) {
                    SnAnalyticsEventService.recordMMDetect();
                }
                AonServiceGlimpse.this.mHandler.sendMessage(Message.obtain(null, 0));
            } else if (!AonServiceGlimpse.this.mGlimpseAlive) {
                SnAnalyticsEventService.recordMMInvalid();
            }
        }

        public void onEndMovement() {
            if (AonService.DEBUG) {
                Log.d(AonServiceGlimpse.TAG, "onEndMovement");
            }
        }
    };
    private long mMovementTime;
    private long mNoMovementTime;
    /* access modifiers changed from: private */
    public boolean mPendingAODLaunch = false;
    /* access modifiers changed from: private */
    public boolean mPowerKeyDismissGlimpse = false;
    private PowerManager mPowerManager = null;
    private boolean mRegisterToSensorhub = false;
    /* access modifiers changed from: private */
    public boolean mScreenOff;
    SensorEventListener mSensorListener = new SensorEventListener() {
        public void onAccuracyChanged(Sensor arg0, int arg1) {
        }

        public void onSensorChanged(SensorEvent event) {
            int sensorType = event.sensor.getType();
            if (sensorType == com.motorola.aon.env.android.hardware.Sensor.TYPE_DISPLAY_BRIGHTNESS) {
                int bn = (int) event.values[0];
                if (AonService.DEBUG) {
                    Log.d("aod_brightness", "brightness = " + Float.toString(event.values[0]));
                }
                AonServiceGlimpse.this.handleBrightnessChanged(bn);
                return;
            }
            Log.e(AonServiceGlimpse.TAG, "Fail to handle the sensor type " + String.valueOf(sensorType));
        }
    };
    private SensorManager mSensorManager;
    private Object mSensorRegisterLock = new Object();
    /* access modifiers changed from: private */
    public Object mSleepLock = new Object();
    /* access modifiers changed from: private */
    public boolean mStoping = false;
    private StowedListener mStowedListener = new StowedListener() {
        public void onStowedChange(Transition transition) {
            if (transition.getNewState() == ModalityManager.STOWED_FALSE) {
                boolean isFaceDown = true;
                if (AonService.DEBUG) {
                    Log.d(AonServiceGlimpse.TAG, "unstow event");
                }
                if (AonServiceGlimpse.this.mModalityManager != null) {
                    Transition tran = AonServiceGlimpse.this.mModalityManager.getOrientation();
                    if (tran != null) {
                        isFaceDown = tran.getNewState() == ModalityManager.ORIENTATION_FACE_DOWN;
                    }
                }
                if (!isFaceDown) {
                    if (AonServiceGlimpse.CURRENT_DRAIN_DEBUG) {
                        Log.d(AonServiceGlimpse.TAG, "Phone is in UNstowed");
                    }
                    if (!AonServiceGlimpse.this.mGlimpseAlive) {
                        SnAnalyticsEventService.recordStowedExit();
                    }
                    AonServiceGlimpse.this.mHandler.sendMessage(Message.obtain(null, 0));
                }
            }
        }
    };
    private long mUnstowedEnableDelay = 0;

    class GlimpseServer extends Binder {
        GlimpseServer() {
        }

        /* access modifiers changed from: protected */
        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            boolean down;
            int i = 0;
            boolean handled = false;
            if (data == null || reply == null) {
                Log.e(AonServiceGlimpse.TAG, "null data or reply is encountered!");
                return super.onTransact(code, data, reply, flags);
            }
            switch (code) {
                case 1:
                    if (data.readByte() == 0) {
                        down = false;
                    } else {
                        down = true;
                    }
                    if (AonService.DEBUG) {
                        Log.d(AonServiceGlimpse.TAG, "OnTransact() powerkey down = " + down);
                    }
                    if (down) {
                        AonServiceGlimpse.this.mPendingAODLaunch = false;
                        synchronized (AonServiceGlimpse.this.mSleepLock) {
                            if (AonService.DEBUG) {
                                Log.d(AonServiceGlimpse.TAG, "power down get lock");
                            }
                            if (AonServiceGlimpse.this.mGlimpseAlive) {
                                AonServiceGlimpse.this.mPowerKeyDismissGlimpse = true;
                                SnAnalyticsEventService.mPowerKeyCount++;
                                if (AonService.DEBUG) {
                                    Log.d(AonServiceGlimpse.TAG, "Power Key Count " + SnAnalyticsEventService.mPowerKeyCount);
                                }
                            } else {
                                AonServiceGlimpse.this.mPowerKeyDismissGlimpse = false;
                            }
                        }
                        if (AonServiceGlimpse.this.mPowerKeyDismissGlimpse) {
                            AonServiceGlimpse.this.mAonNotificationManager.enableTracking(false);
                            AonServiceGlimpse.this.mHandler.sendEmptyMessage(10);
                        }
                        if (AonServiceGlimpse.this.mScreenOff) {
                            AonServiceGlimpse.this.mAonNotificationManager.enableTracking(false);
                            if (AonServiceGlimpse.CURRENT_DRAIN_DEBUG) {
                                Log.d(AonServiceGlimpse.TAG, "White list notification is cleared by power key");
                            }
                        }
                    }
                    if (AonServiceGlimpse.this.mPowerKeyDismissGlimpse) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    handled = true;
                    break;
                case 2:
                    AonServiceGlimpse.this.mScreenOff = false;
                    AonServiceGlimpse.this.mPendingAODLaunch = false;
                    if (AonServiceGlimpse.this.mGlimpseAlive) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    synchronized (AonServiceGlimpse.this.mSleepLock) {
                        AonServiceGlimpse.this.mGlimpseSleepPhone = false;
                    }
                    handled = true;
                    break;
                case 3:
                    AonServiceGlimpse.this.mAonNotificationManager.enableTracking(true);
                    if (AonServiceGlimpse.this.mGlimpseSleepPhone) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    handled = true;
                    break;
                case 4:
                    if (AonService.DEBUG) {
                        Log.d(AonServiceGlimpse.TAG, "mScreenOff = " + AonServiceGlimpse.this.mScreenOff + " mPowerKeyDismissGlimpse = " + AonServiceGlimpse.this.mPowerKeyDismissGlimpse);
                    }
                    if (!AonServiceGlimpse.this.mScreenOff || AonServiceGlimpse.this.mPowerKeyDismissGlimpse) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    handled = true;
                    break;
                case 5:
                    switch (data.readInt()) {
                        case 0:
                            AonServiceGlimpse.this.mHandler.sendEmptyMessage(3);
                            break;
                        case 1:
                            AonServiceGlimpse.this.mHandler.sendEmptyMessage(4);
                            break;
                        case 2:
                            AonServiceGlimpse.this.mHandler.sendEmptyMessage(13);
                            break;
                        default:
                            Log.e(AonServiceGlimpse.TAG, "receive incorrect activity state!");
                            break;
                    }
                    handled = true;
                    break;
                default:
                    Log.e(AonServiceGlimpse.TAG, "Unknown method code found! " + code);
                    break;
            }
            if (!handled) {
                return super.onTransact(code, data, reply, flags);
            }
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void handleBrightnessChanged(int brightness) {
        long elapsed = System.currentTimeMillis() - this.mAodStartTime;
        this.mCurrentBrightness = brightness;
        if (elapsed < AOD_RAMP_UP_DURATION && this.mHandler != null) {
            Message msg = Message.obtain(null, 9);
            msg.arg1 = this.mCurrentBrightness;
            this.mHandler.sendMessage(msg);
        }
    }

    /* access modifiers changed from: private */
    public void startGlimpseActivity() {
        if (DEBUG) {
            Log.d(TAG, "startGlimpseActivity()");
        }
        mAonViewManager.showGlimpse();
        this.mAodStartTime = System.currentTimeMillis();
        onAodActivated();
        if (SnAnalyticsEventService.isIncomingRefreshed() && SnAnalyticsEventService.getIncomingTime() != 0) {
            SnAnalyticsEventService.recordIncomingTime(System.currentTimeMillis() - SnAnalyticsEventService.getIncomingTime());
            SnAnalyticsEventService.setIncomingTime(0);
            SnAnalyticsEventService.setIncomingRefresh(false);
        }
        synchronized (this.mSleepLock) {
            this.mPowerKeyDismissGlimpse = false;
            this.mGlimpseAlive = true;
            this.mGlimpseSleepPhone = false;
            this.mActivityDestroyed = false;
        }
        if (DEBUG) {
            Log.d("aod_brightness", "mCurrentBrightness = " + this.mCurrentBrightness);
        }
        Intent intent = new Intent(getApplicationContext(), AonGlimpseActivity.class);
        intent.putExtra("PrivacySetting", this.mSnPrivacy);
        intent.putExtra("TutorialState", this.mTutorialState);
        intent.putExtra("BrightNess", this.mCurrentBrightness);
        intent.putExtra("HapticsEnabled", this.mbHapticsEnabled);
        intent.addFlags(276889600);
        startActivity(intent);
        SnAnalyticsEventService.recordTotalBreaths();
        SnAnalyticsEventService.recordTotalBreathsAls();
    }

    /* access modifiers changed from: protected */
    public boolean onReceiveBroadcast(android.content.Context context, Intent intent) {
        if (intent == null) {
            return false;
        }
        String action = intent.getAction();
        if (DEBUG) {
            Log.d(TAG, "receive intent " + (action != null ? action : "null"));
        }
        if ("com.motorola.internal.policy.statusbar.NOTIFICATION_VIEW_FULLY_VISIBLE".equals(action)) {
            this.mAonNotificationManager.enableTracking(false);
            return true;
        } else if ("android.intent.action.SCREEN_OFF".equals(action)) {
            this.mScreenOff = true;
            this.mAonNotificationManager.enableTracking(true);
            if (this.mHandler != null) {
                this.mHandler.sendMessage(Message.obtain(null, 4));
            }
            if (this.mPendingAODLaunch && this.mActivityDestroyed) {
                this.mPendingAODLaunch = false;
                this.mHandler.sendEmptyMessage(0);
            }
            return true;
        } else if (!"android.intent.action.SCREEN_ON".equals(action)) {
            return false;
        } else {
            this.mScreenOff = false;
            return true;
        }
    }

    public void onCreate() {
        readConfigParameters();
        super.onCreate();
        this.mAonNotificationManager.enableTracking(true);
        mAonViewManager = new AonViewManagerGlimpse(getApplicationContext(), (AonNotificationManagerGlimpse) this.mAonNotificationManager, null);
        updateBreathingView();
        this.mPowerManager = (PowerManager) getSystemService("power");
        if (this.mPowerManager != null) {
            this.mScreenOff = !this.mPowerManager.isScreenOn();
        }
        if (this.mSnAon != 0) {
            registerToSensorhub();
        }
    }

    /* access modifiers changed from: protected */
    public void onAonInterfaceConntected() {
        super.onAonInterfaceConntected();
        try {
            this.mAonInterface.registerFullDisplay(this.mGlimpseServer);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "failed to register FullDisplayService");
        }
    }

    /* access modifiers changed from: protected */
    public void registerBroadcast(IntentFilter ifilter) {
        ifilter.addAction("com.motorola.internal.policy.statusbar.NOTIFICATION_VIEW_FULLY_VISIBLE");
        ifilter.addAction("android.intent.action.SCREEN_OFF");
        ifilter.addAction("android.intent.action.SCREEN_ON");
    }

    private void readConfigParameters() {
        Resources res = getApplicationContext().getResources();
        this.mMovementTime = (long) (res.getInteger(R.integer.movement_time) * 1000);
        this.mNoMovementTime = (long) (res.getInteger(R.integer.no_movement_time) * 1000);
        this.mUnstowedEnableDelay = (long) (res.getInteger(R.integer.stowed_time) * 1000);
        this.mFlatupEnable = res.getBoolean(R.bool.flatup_enable);
        this.mFlatupCoolDown = (long) (res.getInteger(R.integer.flatup_exit_cooldown) * 1000);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0011, code lost:
        android.util.Log.d(TAG, "registerToSensorhub()");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0018, code lost:
        r7 = getSystemService(com.motorola.aon.env.android.content.Context.MODALITY_SERVICE);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        if (r7 == null) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0020, code lost:
        r8.mModalityManager = com.motorola.aon.env.com.motorola.slpc.ModalityManager.from(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0028, code lost:
        if (r8.mModalityManager == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0038, code lost:
        if (r8.mModalityManager.addStowedListener(r8.mStowedListener, com.motorola.aon.env.com.motorola.slpc.ModalityManager.STOWED_FALSE, 0, r8.mUnstowedEnableDelay) == false) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004d, code lost:
        if (r8.mModalityManager.addStowedListener(r8.mStowedListener, com.motorola.aon.env.com.motorola.slpc.ModalityManager.STOWED_ALL & (com.motorola.aon.env.com.motorola.slpc.ModalityManager.STOWED_FALSE ^ -1), com.motorola.aon.env.com.motorola.slpc.ModalityManager.DURATION_IGNORE, com.motorola.aon.env.com.motorola.slpc.ModalityManager.DURATION_IGNORE) != false) goto L_0x0065;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004f, code lost:
        android.util.Log.e(TAG, "onCreate addStowedListener failed!");
        r8.mHandler.sendMessage(android.os.Message.obtain(null, 7));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0067, code lost:
        if (DEBUG == false) goto L_0x0070;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0069, code lost:
        android.util.Log.d(TAG, "onCreate addStowedListener success!");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0072, code lost:
        if (r8.mFlatupEnable == false) goto L_0x00da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0082, code lost:
        if (r8.mModalityManager.addOrientationListener(r8.mFlatUnknownListener, com.motorola.aon.env.com.motorola.slpc.ModalityManager.ORIENTATION_UNKNOWN, ORIENTATION_DELAY, r8.mFlatupCoolDown) == false) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0097, code lost:
        if (r8.mModalityManager.addOrientationListener(r8.mFlatUnknownListener, com.motorola.aon.env.com.motorola.slpc.ModalityManager.ORIENTATION_ALL & (com.motorola.aon.env.com.motorola.slpc.ModalityManager.ORIENTATION_UNKNOWN ^ -1), com.motorola.aon.env.com.motorola.slpc.ModalityManager.DURATION_IGNORE, com.motorola.aon.env.com.motorola.slpc.ModalityManager.DURATION_IGNORE) != false) goto L_0x00ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0099, code lost:
        android.util.Log.e(TAG, "onCreate addOrientationListener failed!");
        r8.mHandler.sendMessage(android.os.Message.obtain(null, 7));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00af, code lost:
        if (DEBUG == false) goto L_0x00b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b1, code lost:
        android.util.Log.d(TAG, "onCreate addOrientationListener success!");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00c4, code lost:
        if (r8.mModalityManager.addMovementListener(r8.mMovementListener, 0, r8.mNoMovementTime) != false) goto L_0x010a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00c6, code lost:
        android.util.Log.e(TAG, "onCreate addMovementListener failed!");
        r8.mHandler.sendMessage(android.os.Message.obtain(null, 7));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00e8, code lost:
        if (r8.mModalityManager.addOrientationListener(r8.mFacedownListener, com.motorola.aon.env.com.motorola.slpc.ModalityManager.ORIENTATION_FACE_DOWN, ORIENTATION_DELAY, ORIENTATION_DELAY) != false) goto L_0x00fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00ea, code lost:
        android.util.Log.e(TAG, "onCreate addOrientationListener failed for face down!");
        r8.mHandler.sendMessage(android.os.Message.obtain(null, 7));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0100, code lost:
        if (DEBUG == false) goto L_0x00b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0102, code lost:
        android.util.Log.d(TAG, "onCreate addOrientationListener success for face down!");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x010c, code lost:
        if (DEBUG == false) goto L_0x0115;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x010e, code lost:
        android.util.Log.d(TAG, "register to Modality APIs successful");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0117, code lost:
        if (r8.mSensorManager != null) goto L_0x0123;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0119, code lost:
        r8.mSensorManager = (android.hardware.SensorManager) getSystemService("sensor");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0123, code lost:
        r8.mBrightnessSensor = r8.mSensorManager.getDefaultSensor(com.motorola.aon.env.android.hardware.Sensor.TYPE_DISPLAY_BRIGHTNESS);
        r8.mSensorManager.registerListener(r8.mSensorListener, r8.mBrightnessSensor, 3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x000f, code lost:
        if (DEBUG == false) goto L_0x0018;
     */
    private void registerToSensorhub() {
        synchronized (this.mSensorRegisterLock) {
            if (!this.mRegisterToSensorhub) {
                this.mRegisterToSensorhub = true;
            }
        }
    }

    private void unregisterToSensorhub() {
        if (DEBUG) {
            Log.d(TAG, "unregisterToSensorhub()");
        }
        if (this.mModalityManager != null) {
            this.mModalityManager.removeMovementListener(this.mMovementListener);
            if (this.mFlatupEnable) {
                this.mModalityManager.removeOrientationListener(this.mFlatUnknownListener);
            } else {
                this.mModalityManager.removeOrientationListener(this.mFacedownListener);
            }
            this.mModalityManager.removeStowedListener(this.mStowedListener);
            this.mModalityManager = null;
            if (this.mSensorManager != null) {
                this.mSensorManager.unregisterListener(this.mSensorListener);
                this.mSensorManager = null;
            }
            synchronized (this.mSensorRegisterLock) {
                this.mRegisterToSensorhub = false;
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        unregisterToSensorhub();
        this.mPowerManager = null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return super.onStartCommand(intent, flags, startId);
        }
        String action = intent.getAction();
        if (DEBUG) {
            Log.e(TAG, "onStartCommand " + action);
        }
        if (ACTION_LAUNCH_NOTIFICATION.equals(action)) {
            mAonViewManager.launchNotification();
            this.mAonNotificationManager.enableTracking(false);
        } else if (ACTION_UNLOCK.equals(action)) {
            this.mAonNotificationManager.enableTracking(false);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    public AonNotificationManagerGlimpse getNotificationManager(android.content.Context context) {
        return new AonNotificationManagerGlimpse(context);
    }

    /* access modifiers changed from: private */
    public void updateBreathingView() {
        int viewType;
        ArrayList<NotificationEntry> breathingList = this.mAonNotificationManager.getNotificationListForBreathing();
        if (this.mTutorialState == 2) {
            viewType = 2;
        } else if (this.mSnPrivacy != 0) {
            viewType = 1;
        } else {
            viewType = 0;
        }
        mAonViewManager.requestUpdateBreathingView(viewType, breathingList, this.mBatteryStatus);
    }

    public void updateBreathing(boolean incoming, boolean force) {
        Calendar cal = Calendar.getInstance();
        if (CURRENT_DRAIN_DEBUG) {
            Log.d(TAG, "White list notification comes");
        }
        if (!((AonNotificationManagerGlimpse) this.mAonNotificationManager).getUpdated()) {
            return;
        }
        if (!this.mGlimpseAlive || !mAonViewManager.isPeeking()) {
            boolean isFaceDown = true;
            boolean isStowed = true;
            ((AonNotificationManagerGlimpse) this.mAonNotificationManager).setUpdated(false);
            updateBreathingView();
            if (this.mModalityManager != null) {
                Transition tran = this.mModalityManager.getOrientation();
                if (tran != null) {
                    if (tran.getNewState() == ModalityManager.ORIENTATION_FACE_DOWN) {
                        isFaceDown = true;
                    } else {
                        isFaceDown = false;
                    }
                }
                Transition tran2 = this.mModalityManager.getStowed();
                if (tran2 != null) {
                    isStowed = tran2.getNewState() == ModalityManager.STOWED_TRUE;
                }
            }
            if (isFaceDown || isStowed || isNightOff(cal)) {
                SnAnalyticsEventService.recordIncomingNoBreath();
                return;
            }
            if (DEBUG) {
                Log.d(TAG, "Incoming message will launch glimpse");
            }
            onIncomingNotification();
            this.mHandler.sendMessage(Message.obtain(null, 1));
        }
    }

    /* access modifiers changed from: protected */
    public void onAonSettingChange() {
        if (this.mSnAon == 0) {
            unregisterToSensorhub();
        } else {
            registerToSensorhub();
        }
    }

    /* access modifiers changed from: private */
    public void stopGlimpseActivity() {
        if (DEBUG) {
            Log.d(TAG, "stopGlimpseActivity");
        }
        this.mPendingAODLaunch = false;
        if (this.mActivityMessenger != null) {
            try {
                this.mActivityMessenger.send(Message.obtain(null, 2));
            } catch (RemoteException e) {
                Log.e(TAG, "The AOD activity has died.");
            } finally {
                this.mGlimpseAlive = false;
                this.mAodStartTime = 0;
                this.mActivityMessenger = null;
            }
        } else {
            this.mGlimpseAlive = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onHdmiPlugged() {
        this.mHandler.sendEmptyMessage(10);
    }

    /* access modifiers changed from: protected */
    public void onWirelessDisplayMirroring() {
        this.mHandler.sendEmptyMessage(10);
    }

    /* access modifiers changed from: protected */
    public void onHapticsSettingsChanged() {
        if (DEBUG) {
            Log.d(TAG, "Haptics settings changed: New Value : " + this.mbHapticsEnabled);
        }
    }

    /* access modifiers changed from: protected */
    public void onNotificationReadByAOV(long cleanTime, HashMap<String, ArrayList<String>> hashMap) {
        if (DEBUG) {
            Log.d(TAG, "Notification has been read out by AOV");
        }
    }

    /* access modifiers changed from: private */
    public boolean isGlimpseEnabled() {
        if (!isAODEnabled() || !this.mScreenOff) {
            return false;
        }
        return true;
    }

    public boolean isAODEnabled() {
        if (this.mSnAon == 0 || this.mTutorialState == 0) {
            if (DEBUG) {
                if (this.mSnAon == 0) {
                    Log.d(TAG, "AON is off");
                } else {
                    Log.d(TAG, "Device not provisioned");
                }
            }
            return false;
        } else if (this.mTelephonyManager != null && this.mTelephonyManager.getCallState() != 0) {
            if (DEBUG) {
                Log.d(TAG, "in calling");
            }
            return false;
        } else if (this.mCoolDownMode == 1) {
            if (DEBUG) {
                Log.d(TAG, "in cool down");
            }
            return false;
        } else if (this.mHdmiPlugged) {
            if (DEBUG) {
                Log.d(TAG, "HDMI plugged");
            }
            return false;
        } else if (!com.motorola.aon.env.android.os.PowerManager.proximityTargetDetected(this.mPowerManager)) {
            return true;
        } else {
            if (DEBUG) {
                Log.d(TAG, "proximityTargetDetected() returns TRUE, suppress breathing");
            }
            return false;
        }
    }

    public IBinder onBind(Intent intent) {
        if (ACTION_BIND_AONSERVICEGLIMPSE.equals(intent.getAction())) {
            return this.mMessenger.getBinder();
        }
        return super.onBind(intent);
    }

    protected static AonViewManagerGlimpse getViewManager() {
        return mAonViewManager;
    }
}
