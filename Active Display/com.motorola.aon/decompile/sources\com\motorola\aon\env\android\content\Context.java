package com.motorola.aon.env.android.content;

import android.util.Log;

public class Context {
    private static final String CLASS_CONTEXT = "android.content.Context";
    private static final String FIELD_MODALITY_SERVICE = "MODALITY_SERVICE";
    private static final String FIELD_STATUS_BAR_SERVICE = "STATUS_BAR_SERVICE";
    public static String MODALITY_SERVICE = null;
    public static String STATUS_BAR_SERVICE = null;
    private static final String TAG = "Aon_Context";
    public static boolean sInitialized;

    static {
        MODALITY_SERVICE = null;
        STATUS_BAR_SERVICE = null;
        sInitialized = false;
        try {
            Class<?> clazz = Class.forName(CLASS_CONTEXT);
            STATUS_BAR_SERVICE = (String) clazz.getDeclaredField(FIELD_STATUS_BAR_SERVICE).get(null);
            MODALITY_SERVICE = (String) clazz.getDeclaredField(FIELD_MODALITY_SERVICE).get(null);
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize class");
        }
    }
}
