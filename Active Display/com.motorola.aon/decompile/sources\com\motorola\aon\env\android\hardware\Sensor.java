package com.motorola.aon.env.android.hardware;

import android.util.Log;

public class Sensor {
    private static final String CLASS_SENSOR = "android.hardware.Sensor";
    private static final String FIELD_TYPE_DISPLAY_BRIGHTNESS = "TYPE_DISPLAY_BRIGHTNESS";
    private static final String TAG = "Aon_Sensor";
    public static int TYPE_DISPLAY_BRIGHTNESS;
    public static boolean sInitialized;

    static {
        TYPE_DISPLAY_BRIGHTNESS = 0;
        sInitialized = false;
        try {
            TYPE_DISPLAY_BRIGHTNESS = Class.forName(CLASS_SENSOR).getDeclaredField(FIELD_TYPE_DISPLAY_BRIGHTNESS).getInt(null);
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize class");
        }
    }
}
