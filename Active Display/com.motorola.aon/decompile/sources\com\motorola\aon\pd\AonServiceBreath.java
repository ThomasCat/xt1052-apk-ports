package com.motorola.aon.pd;

import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.telephony.PhoneStateListener;
import android.text.format.DateFormat;
import android.util.Log;
import com.motorola.aon.AonService;
import com.motorola.aon.SnAnalyticsEventService;
import com.motorola.aon.aidl.NotificationEntry;
import com.motorola.aon.env.aidl.IPartialDisplayService.Stub;
import com.motorola.aon.env.aidl.IRemoteCallback;
import com.motorola.aon.env.android.os.PowerManager;
import com.motorola.aon.env.com.android.internal.widget.LockPatternUtils;
import com.motorola.aon.jni.PartialDisplay;
import com.motorola.aon.jni.PartialDisplay.ControlRegister;
import com.motorola.aon.jni.PartialDisplay.InstrumentationRegister;
import com.motorola.aon.jni.PartialDisplay.StatusRegister;
import com.motorola.aon.jni.PartialDisplay.TouchRegister;
import com.motorola.aon.pd.AonViewManagerBreath.ICallback;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;

public class AonServiceBreath extends AonService {
    /* access modifiers changed from: private */
    public static int DRAW_BREATHING_TIMEDOUT_RETRY_DELAY = 6000;
    /* access modifiers changed from: private */
    public static int DRAW_BREATHING_TIMEDOUT_RETRY_TIMES = 10;
    /* access modifiers changed from: private */
    public static int MAX_POLL_PROX_SCREEN_OFF = 60;
    /* access modifiers changed from: private */
    public static int POLL_PROX_SCREEN_OFF_DELAY = 1000;
    private static final String TAG = "AonServiceBreath";
    /* access modifiers changed from: private */
    public AonViewManagerBreath mAonViewManager;
    DevicePolicyManager mDPM;
    /* access modifiers changed from: private */
    public ControlRegister mDefaultControlReg;
    /* access modifiers changed from: private */
    public boolean mInPartialMode = false;
    /* access modifiers changed from: private */
    public int mLastWakeupReason = 0;
    LockPatternUtils mLockPatternUtils;
    /* access modifiers changed from: private */
    public PartialDisplayServer mPartialDisplayServer = new PartialDisplayServer();
    /* access modifiers changed from: private */
    public final PhoneStateListener mPhonteStateListener = new PhoneStateListener() {
        public void onCallStateChanged(int state, String incomingNumber) {
            if (state == 0 && AonServiceBreath.this.mPartialDisplayServer.mState == 1) {
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, "call state to idle when screen is off");
                }
                AonServiceBreath.this.mTelephonyManager.listen(AonServiceBreath.this.mPhonteStateListener, 0);
                if (PowerManager.proximityTargetDetected(AonServiceBreath.this.mPowerManager)) {
                    AonServiceBreath.this.mPollProxCount = 0;
                    AonServiceBreath.this.mHandler.removeCallbacks(AonServiceBreath.this.mPollProxWhenScreenOff);
                    AonServiceBreath.this.mHandler.postDelayed(AonServiceBreath.this.mPollProxWhenScreenOff, (long) AonServiceBreath.POLL_PROX_SCREEN_OFF_DELAY);
                    return;
                }
                AonServiceBreath.this.doTryUpdateBreathing(true);
            }
        }
    };
    /* access modifiers changed from: private */
    public int mPollProxCount;
    Runnable mPollProxWhenScreenOff = new Runnable() {
        public void run() {
            if (AonServiceBreath.this.mPartialDisplayServer.mState == 1) {
                AonServiceBreath.this.mPollProxCount = AonServiceBreath.this.mPollProxCount + 1;
                if (AonServiceBreath.this.mPollProxCount > AonServiceBreath.MAX_POLL_PROX_SCREEN_OFF) {
                    Log.w(AonServiceBreath.TAG, "poll prox when screen off time out");
                } else if (PowerManager.proximityTargetDetected(AonServiceBreath.this.mPowerManager)) {
                    if (AonService.DEBUG) {
                        Log.d(AonServiceBreath.TAG, "poll prox when screen off " + AonServiceBreath.this.mPollProxCount);
                    }
                    AonServiceBreath.this.mHandler.removeCallbacks(AonServiceBreath.this.mPollProxWhenScreenOff);
                    AonServiceBreath.this.mHandler.postDelayed(AonServiceBreath.this.mPollProxWhenScreenOff, (long) AonServiceBreath.POLL_PROX_SCREEN_OFF_DELAY);
                } else {
                    AonServiceBreath.this.doTryUpdateBreathing(true);
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public int mTimedoutRetry;

    class PartialDisplayServer extends Stub {
        static final int STATE_RESUMED = 2;
        static final int STATE_SUSPENDED = 1;
        static final String SYS_GPIO_161 = "/sys/class/gpio/gpio161/value";
        static final int WAIT_GPIO_161_INTERVAL = 20;
        static final int WAIT_GPIO_161_STEP = 50;
        static final int WAIT_GPIO_161_TIMEDOUT = 1000;
        private Runnable mDrawTimedOutRetryRunnable = new Runnable() {
            public void run() {
                AonServiceBreath.this.doTryUpdateBreathing(false);
            }
        };
        File mFileGpio161 = new File(SYS_GPIO_161);
        String mLastCriticalError = "none";
        String mLastNotBreathing = "none";
        boolean mLastWaitDrawnTimeOut = false;
        int mState = 2;
        Object mSyncObj = new Object();

        public void forcePartialMode() {
            if (AonService.DEBUG) {
                Log.d(AonServiceBreath.TAG, "force into partial mode");
            }
            this.mState = 1;
            AonServiceBreath.this.mInPartialMode = true;
        }

        public PartialDisplayServer() {
            PartialDisplay.nativeInit();
            if (AonService.DEBUG) {
                Log.d(AonServiceBreath.TAG, "PartialDisplayServer() : loadFirmwareVersion");
            }
            PartialDisplay.loadFirmwareVersion();
        }

        /* JADX WARNING: Removed duplicated region for block: B:15:0x0026  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0035  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x004f  */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x007c A[SYNTHETIC, Splitter:B:48:0x007c] */
        /* JADX WARNING: Removed duplicated region for block: B:53:0x0085 A[SYNTHETIC, Splitter:B:53:0x0085] */
        /* JADX WARNING: Removed duplicated region for block: B:56:0x0089  */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x0024 A[EDGE_INSN: B:68:0x0024->B:14:0x0024 ?: BREAK  , SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:71:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
        private boolean waitDisplaySuspended() {
            if (AonService.DEBUG) {
                Log.d(AonServiceBreath.TAG, "Enter wait for display suspended");
            }
            int b = -1;
            int count = 0;
            while (true) {
                FileInputStream mGpio161 = null;
                try {
                    FileInputStream mGpio1612 = new FileInputStream(this.mFileGpio161);
                    try {
                        b = mGpio1612.read();
                        if (b != 49) {
                            try {
                                Thread.sleep(20);
                            } catch (InterruptedException e) {
                            }
                            if (mGpio1612 != null) {
                                try {
                                    mGpio1612.close();
                                    FileInputStream fileInputStream = mGpio1612;
                                } catch (IOException e2) {
                                    FileInputStream fileInputStream2 = mGpio1612;
                                }
                            }
                            if (b != 49) {
                                break;
                            }
                            int count2 = count + 1;
                            if (count >= WAIT_GPIO_161_STEP) {
                                int i = count2;
                                break;
                            }
                            count = count2;
                        } else if (mGpio1612 != null) {
                            try {
                                mGpio1612.close();
                                FileInputStream fileInputStream3 = mGpio1612;
                            } catch (IOException e3) {
                                FileInputStream fileInputStream4 = mGpio1612;
                            }
                        }
                    } catch (FileNotFoundException e4) {
                        mGpio161 = mGpio1612;
                    } catch (IOException e5) {
                        mGpio161 = mGpio1612;
                        this.mLastCriticalError = "can not read gpio 161";
                        Log.e(AonServiceBreath.TAG, this.mLastCriticalError);
                        if (mGpio161 != null) {
                            try {
                                mGpio161.close();
                            } catch (IOException e6) {
                            }
                        }
                        if (b != 49) {
                        }
                        if (b != 49) {
                        }
                        if (AonService.DEBUG) {
                        }
                        if (b != 1) {
                        }
                    } catch (Throwable th) {
                        th = th;
                        mGpio161 = mGpio1612;
                        if (mGpio161 != null) {
                        }
                        throw th;
                    }
                } catch (FileNotFoundException e7) {
                    try {
                        this.mLastCriticalError = "can not open gpio 161";
                        Log.e(AonServiceBreath.TAG, this.mLastCriticalError);
                        if (mGpio161 != null) {
                            try {
                                mGpio161.close();
                            } catch (IOException e8) {
                            }
                        }
                        if (b != 49) {
                        }
                        if (b != 49) {
                        }
                        if (AonService.DEBUG) {
                        }
                        if (b != 1) {
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        if (mGpio161 != null) {
                            try {
                                mGpio161.close();
                            } catch (IOException e9) {
                            }
                        }
                        throw th;
                    }
                } catch (IOException e10) {
                    this.mLastCriticalError = "can not read gpio 161";
                    Log.e(AonServiceBreath.TAG, this.mLastCriticalError);
                    if (mGpio161 != null) {
                    }
                    if (b != 49) {
                    }
                    if (b != 49) {
                    }
                    if (AonService.DEBUG) {
                    }
                    if (b != 1) {
                    }
                }
            }
            if (b != 49) {
                this.mLastCriticalError = "timedout to wait gpio 161";
                Log.e(AonServiceBreath.TAG, this.mLastCriticalError);
            }
            if (AonService.DEBUG) {
                Log.d(AonServiceBreath.TAG, "Leave wait for display suspended");
            }
            if (b != 1) {
                return true;
            }
            return false;
        }

        public void fbWillBeResumed(IRemoteCallback listener, int reason) throws RemoteException {
            if (AonService.DEBUG) {
                Log.d(AonServiceBreath.TAG, "framebuffer will be resumed " + this.mState);
            }
            synchronized (this.mSyncObj) {
            }
            if (AonServiceBreath.this.mInPartialMode) {
                InstrumentationRegister ir = new InstrumentationRegister();
                PartialDisplay.nativeReadInstrumentationRegister(ir);
                SnAnalyticsEventService.recordStatusRegister(ir);
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, "AOD_STAT: " + ir.mStowedEnter + " " + ir.mStowedExit + " " + ir.mStowedTotal + " " + ir.mFacedownEnter + " " + ir.mFacedownExit + " " + ir.mUnstowNoAction + " " + ir.mMMAction + " " + ir.mMMNoAction + " " + ir.mIncomingNoBreath + " " + ir.mTotalBreathsStage1 + " " + ir.mTotalBreathsStage2 + " " + ir.mTotalBreathsStage3 + " " + ir.mTotalBreathsStage4 + " " + ir.mTouchValid + " " + ir.mTouchInvalid + " " + ir.mTouchInvalidRelease + " " + ir.mTotalBreathsAls1 + " " + ir.mTotalBreathsAls2 + " " + ir.mTotalBreathsAls3 + " " + ir.mTotalBreathsAls4 + " " + ir.mTotalBreathsAls5 + " " + ir.mTotalBreathsAls6 + " " + ir.mTotalBreathsAls7 + " " + ir.mTotalBreathsAls8 + " " + ir.mTotalBreathsAls9 + " " + ir.mTotalBreathsAls10 + " " + ir.mTotalTimePeeking + " " + ir.mLateTouchCount);
                }
                StatusRegister sr = new StatusRegister();
                PartialDisplay.nativeReadStatusRegister(sr);
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, "read status: mute " + sr.mMute + ", panel " + sr.mPanelState + ", wakeup reason " + sr.mWakeupReason + ", active region " + sr.mActiveRegion + ", brightness " + sr.mBrightness);
                }
                if (sr.mMute != 0) {
                    SnAnalyticsEventService.mMuteCount++;
                    AonServiceBreath.this.mAonNotificationManager.removeBreathedNotifications();
                }
                if (AonService.DEBUG) {
                    NotificationEntry ne = AonServiceBreath.this.mAonNotificationManager.getBreathingNotification();
                    if (ne != null) {
                        Log.d(AonServiceBreath.TAG, "Breathing ends: " + ne.minedData.pkg + ", " + ne.minedData.id + ", reason: " + sr.mWakeupReason);
                    } else {
                        Log.d(AonServiceBreath.TAG, "Breathing ends, reason: " + sr.mWakeupReason);
                    }
                }
                if (reason == PowerManager.WAKE_UP_PARTIAL_DISPLAY_UPDATE_BREATHING) {
                    AonServiceBreath.this.mLastWakeupReason = 4;
                    PartialDisplay.nativePrepareDisplayResume(true, sr.mPanelState, true);
                } else {
                    if (sr.mWakeupReason != 0) {
                        TouchRegister tr = new TouchRegister();
                        PartialDisplay.nativeReadTouchRegister(tr);
                        if (AonService.DEBUG) {
                            Log.d(AonServiceBreath.TAG, "read touch status: x " + tr.mX + ", y " + tr.mY + ", pressure " + tr.mPressure);
                        }
                        if (AonServiceBreath.this.mDefaultControlReg.mTouchRegionUpper.inRegion(tr.mX, tr.mY)) {
                            AonServiceBreath.this.mLastWakeupReason = 2;
                        } else if (AonServiceBreath.this.mDefaultControlReg.mTouchRegionDown.inRegion(tr.mX, tr.mY)) {
                            AonServiceBreath.this.mLastWakeupReason = 1;
                        }
                    }
                    PartialDisplay.nativePrepareDisplayResume(true, sr.mPanelState, false);
                }
            } else {
                if (reason == PowerManager.WAKE_UP_PARTIAL_DISPLAY_UPDATE_BREATHING) {
                    AonServiceBreath.this.mLastWakeupReason = 4;
                    PartialDisplay.nativePrepareDisplayResume(false, 0, true);
                }
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, "no breathing");
                }
            }
            SnAnalyticsEventService.recordHandback(System.currentTimeMillis());
            SnAnalyticsEventService.calculateTimeBlock(AonServiceBreath.this.getApplicationContext());
            if (listener != null) {
                try {
                    listener.sendResult(null);
                } catch (RemoteException e) {
                }
            }
            if (reason != PowerManager.GO_TO_SLEEP_REASON_UPDATE_BREATHING) {
                if (AonServiceBreath.this.mInPartialMode) {
                    AonServiceBreath.this.mAonViewManager.handleScreenOnAction(AonServiceBreath.this.mLastWakeupReason);
                }
                ((AonNotificationManagerBreath) AonServiceBreath.this.mAonNotificationManager).onScreenOn(AonServiceBreath.this.mLastWakeupReason);
            } else {
                ((AonNotificationManagerBreath) AonServiceBreath.this.mAonNotificationManager).releaseWakeLock();
            }
            if (AonServiceBreath.this.mLastWakeupReason == 3) {
                SnAnalyticsEventService.mPowerKeyCount++;
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, "Power Key Count " + SnAnalyticsEventService.mPowerKeyCount);
                }
            }
            if (AonServiceBreath.this.mLastWakeupReason == 1 || AonServiceBreath.this.mLastWakeupReason == 2 || AonServiceBreath.this.mLastWakeupReason == 3) {
                AonServiceBreath.this.mNotificationReadByAOV = false;
            }
            this.mState = 2;
            AonServiceBreath.this.mInPartialMode = false;
            PersistentManager.mInst.setPartialMode(AonServiceBreath.this.getApplicationContext(), 0);
        }

        public void fbWillBeSuspended(IRemoteCallback listener, int reason) throws RemoteException {
            int viewType;
            if (AonService.DEBUG) {
                Log.d(AonServiceBreath.TAG, "fb will be suspended " + this.mState + ", reason " + reason);
            }
            boolean bUpdateBreathing = reason == PowerManager.GO_TO_SLEEP_REASON_UPDATE_BREATHING;
            AonServiceBreath.this.mLastWakeupReason = 0;
            AonServiceBreath.this.mInPartialMode = true;
            boolean bNotificationDisabled = false;
            boolean bBreathingClock = true;
            boolean bGestureBased = false;
            Calendar cal = Calendar.getInstance();
            boolean nightOff = AonServiceBreath.this.isNightOff(cal);
            if (AonService.DEBUG && nightOff) {
                Log.d(AonServiceBreath.TAG, "night off");
            }
            ArrayList<NotificationEntry> breathingList = AonServiceBreath.this.mAonNotificationManager.getNotificationListForBreathing();
            if (AonServiceBreath.this.mSnAon == 0 || AonServiceBreath.this.mTutorialState == 0) {
                this.mLastNotBreathing = AonServiceBreath.this.mSnAon == 0 ? "AON is off" : "Device not provisioned";
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, this.mLastNotBreathing);
                }
                bNotificationDisabled = true;
                bBreathingClock = false;
            } else if (AonServiceBreath.this.mTelephonyManager.getCallState() != 0) {
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, "in calling");
                }
                this.mLastNotBreathing = "in calling";
                AonServiceBreath.this.mInPartialMode = false;
                AonServiceBreath.this.mTelephonyManager.listen(AonServiceBreath.this.mPhonteStateListener, 32);
            } else if (PowerManager.proximityTargetDetected(AonServiceBreath.this.mPowerManager)) {
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, "proximityTargetDetected() returns TRUE, suppress breathing");
                }
                AonServiceBreath.this.mInPartialMode = false;
                this.mLastNotBreathing = "proximityTargetDetected";
            } else if (AonServiceBreath.this.mHdmiPlugged) {
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, "HDMI connected");
                }
                AonServiceBreath.this.mInPartialMode = false;
                this.mLastNotBreathing = "HDMI connected";
            } else if (AonServiceBreath.this.mCoolDownMode == 1) {
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, "in cool down");
                }
                AonServiceBreath.this.mInPartialMode = false;
                this.mLastNotBreathing = "in cool down";
            } else if (AonServiceBreath.this.mTutorialState == 2) {
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, "show tutorial breathing");
                }
                bNotificationDisabled = false;
            } else {
                bNotificationDisabled = breathingList == null || breathingList.size() <= 0;
                bGestureBased = nightOff || bNotificationDisabled || AonServiceBreath.this.mNotificationReadByAOV;
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, "Notification has been read out by AOV : " + AonServiceBreath.this.mNotificationReadByAOV);
                }
            }
            if (AonServiceBreath.this.mInPartialMode) {
                if (bGestureBased) {
                    AonServiceBreath.this.onAodActivated();
                } else if (!bNotificationDisabled) {
                    AonServiceBreath.this.onAodActivated();
                    if (bUpdateBreathing) {
                        AonServiceBreath.this.onIncomingNotification();
                    }
                }
                if (AonService.DEBUG) {
                    if (bGestureBased) {
                        Log.d(AonServiceBreath.TAG, "going to partial display with gesture mode");
                        if (bNotificationDisabled) {
                            Log.d(AonServiceBreath.TAG, "clock only, no notification");
                        } else {
                            Log.d(AonServiceBreath.TAG, "clock and notification");
                        }
                    } else if (!bNotificationDisabled) {
                        Log.d(AonServiceBreath.TAG, "going to partial display with breathing");
                        NotificationEntry ne = null;
                        if (breathingList != null && breathingList.size() > 0) {
                            ne = (NotificationEntry) breathingList.get(0);
                        }
                        if (bUpdateBreathing) {
                            if (ne != null) {
                                Log.d(AonServiceBreath.TAG, "The Incoming Notification Screen appeared: " + ne.minedData.pkg + ", " + ne.minedData.id);
                            } else {
                                Log.d(AonServiceBreath.TAG, "The Incoming Notification Screen appeared.");
                            }
                        } else if (ne != null) {
                            Log.d(AonServiceBreath.TAG, "Breathing starts for: " + ne.minedData.pkg + ", " + ne.minedData.id);
                        } else {
                            Log.d(AonServiceBreath.TAG, "Breathing starts.");
                        }
                    } else {
                        Log.d(AonServiceBreath.TAG, "going to partial display without showing anything");
                    }
                }
                int privacy = isSecureBreathing(reason) ? 1 : 0;
                PartialDisplay.nativePrepareDisplaySuspend(true, !bUpdateBreathing);
                if (AonServiceBreath.this.mTutorialState == 2) {
                    viewType = 2;
                } else if (privacy != 0) {
                    viewType = 1;
                } else {
                    viewType = 0;
                }
                final int batteryStatus = AonServiceBreath.this.mBatteryStatus;
                Object waitDrawn = new Object();
                synchronized (waitDrawn) {
                    if (AonService.DEBUG) {
                        Log.d(AonServiceBreath.TAG, "begin post breathing view");
                    }
                    this.mLastWaitDrawnTimeOut = true;
                    long start = System.currentTimeMillis();
                    AonViewManagerBreath access$1800 = AonServiceBreath.this.mAonViewManager;
                    final Object obj = waitDrawn;
                    AnonymousClass2 r0 = new ICallback() {
                        public void sendResult(Bundle data) {
                            boolean z = true;
                            synchronized (obj) {
                                if (data != null) {
                                    int[] offset = new int[2];
                                    AonServiceBreath.this.mAonViewManager.getBreathingOffset(data.getInt("pos", -1), offset);
                                    ControlRegister access$1700 = AonServiceBreath.this.mDefaultControlReg;
                                    int i = offset[0];
                                    int i2 = offset[1];
                                    if (batteryStatus == 0) {
                                        z = false;
                                    }
                                    access$1700.setBreathingViewPostion(i, i2, z);
                                } else {
                                    Log.w(AonServiceBreath.TAG, "null data from UI thread after post view");
                                }
                                PartialDisplayServer.this.mLastWaitDrawnTimeOut = false;
                                obj.notifyAll();
                            }
                        }
                    };
                    access$1800.requestUpdateBreathingView(viewType, breathingList, batteryStatus, r0);
                    try {
                        waitDrawn.wait(500);
                    } catch (InterruptedException e) {
                        Log.w(AonServiceBreath.TAG, "InterruptedException in waiting drawing brathing view");
                    }
                    if (this.mLastWaitDrawnTimeOut) {
                        this.mLastCriticalError = "timed out drawing breathing view";
                        Log.w(AonServiceBreath.TAG, this.mLastCriticalError);
                        AonServiceBreath.access$4004(AonServiceBreath.this);
                        if (AonServiceBreath.this.mTimedoutRetry < AonServiceBreath.DRAW_BREATHING_TIMEDOUT_RETRY_TIMES) {
                            AonServiceBreath.this.mHandler.removeCallbacks(this.mDrawTimedOutRetryRunnable);
                            AonServiceBreath.this.mHandler.postDelayed(this.mDrawTimedOutRetryRunnable, (long) AonServiceBreath.DRAW_BREATHING_TIMEDOUT_RETRY_DELAY);
                        }
                    } else {
                        AonServiceBreath.this.mTimedoutRetry = 0;
                        AonServiceBreath.this.mHandler.removeCallbacks(this.mDrawTimedOutRetryRunnable);
                    }
                    SnAnalyticsEventService.recordDrawTime(System.currentTimeMillis() - start);
                }
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, "end post breathing view");
                }
                boolean is24 = DateFormat.is24HourFormat(AonServiceBreath.this);
                AonServiceBreath.this.mDefaultControlReg.setNotificationDisable(bNotificationDisabled);
                AonServiceBreath.this.mDefaultControlReg.setGestureBased(bGestureBased);
                AonServiceBreath.this.mDefaultControlReg.setClockEnabled(bBreathingClock);
                AonServiceBreath.this.mDefaultControlReg.setClock24(is24);
                AonServiceBreath.this.mDefaultControlReg.setBrightness(AonServiceBreath.this.mBrightness);
                AonServiceBreath.this.mDefaultControlReg.setIsIncoming(bUpdateBreathing);
                AonServiceBreath.this.mDefaultControlReg.disableHaptics(!AonServiceBreath.this.mbHapticsEnabled);
                if (bUpdateBreathing) {
                    SnAnalyticsEventService.mIncomingALS++;
                }
                long tzOffset = 0;
                if (cal != null) {
                    TimeZone tz = cal.getTimeZone();
                    if (tz != null) {
                        tzOffset = (long) tz.getOffset(System.currentTimeMillis());
                    }
                }
                SnAnalyticsEventService.recordHandover(System.currentTimeMillis());
                SnAnalyticsEventService.isClockOnlyBreathing(bNotificationDisabled);
                PartialDisplay.setCurrentTime(tzOffset);
                int res = PartialDisplay.nativeSetControlRegister(AonServiceBreath.this.mDefaultControlReg);
                if (res < 0) {
                    this.mLastCriticalError = "Error set control register " + res;
                    Log.e(AonServiceBreath.TAG, this.mLastCriticalError);
                    AonServiceBreath.this.mInPartialMode = false;
                } else {
                    PersistentManager.mInst.setPartialMode(AonServiceBreath.this.getApplicationContext(), 1);
                }
                synchronized (this.mSyncObj) {
                    if (listener != null) {
                        try {
                            listener.sendResult(null);
                        } catch (RemoteException e2) {
                        }
                    }
                    waitDisplaySuspended();
                    if (SnAnalyticsEventService.isIncomingRefreshed() && SnAnalyticsEventService.getIncomingTime() != 0) {
                        SnAnalyticsEventService.recordIncomingTime(System.currentTimeMillis() - SnAnalyticsEventService.getIncomingTime());
                        SnAnalyticsEventService.setIncomingTime(0);
                        SnAnalyticsEventService.setIncomingRefresh(false);
                    }
                    AonServiceBreath.this.mAonViewManager.requestHideBreathingView(null);
                }
            } else {
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, "no need to activate partial display");
                }
                if (listener != null) {
                    try {
                        listener.sendResult(null);
                    } catch (RemoteException e3) {
                    }
                }
            }
            ((AonNotificationManagerBreath) AonServiceBreath.this.mAonNotificationManager).onScreenOff(AonServiceBreath.this.mInPartialMode);
            this.mState = 1;
            if (AonServiceBreath.this.mInPartialMode) {
                if (breathingList != null) {
                    Iterator i$ = breathingList.iterator();
                    while (i$.hasNext()) {
                        ((NotificationEntry) i$.next()).breathing = true;
                    }
                }
                ArrayList<NotificationEntry> newBreathingList = AonServiceBreath.this.mAonNotificationManager.getNotificationListForBreathing();
                if (newBreathingList != null) {
                    Iterator i$2 = newBreathingList.iterator();
                    while (i$2.hasNext()) {
                        if (!((NotificationEntry) i$2.next()).breathing) {
                            if (AonService.DEBUG) {
                                Log.d(AonServiceBreath.TAG, "got new notification to breathe during display suspension");
                            }
                            Handler access$5000 = AonServiceBreath.this.mHandler;
                            AnonymousClass3 r02 = new Runnable() {
                                public void run() {
                                    AonServiceBreath.this.doTryUpdateBreathing(false);
                                }
                            };
                            access$5000.post(r02);
                            return;
                        }
                    }
                }
            }
        }

        public boolean isPartialDisplayActivated() throws RemoteException {
            return AonServiceBreath.this.mInPartialMode;
        }

        public void notifyPowerKeyWakeup() {
            if (AonService.DEBUG) {
                Log.d(AonServiceBreath.TAG, "notify power key wake up");
            }
            AonServiceBreath.this.mLastWakeupReason = 3;
        }

        private boolean isSecureBreathing(int reason) {
            boolean res = false;
            if (AonServiceBreath.this.mSnPrivacy == 0 || !AonServiceBreath.this.mKeyguardManager.isKeyguardSecure()) {
                return false;
            }
            if (AonServiceBreath.this.mKeyguardManager.isKeyguardLocked()) {
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, "secure breathing, secure key guard already active");
                }
                res = true;
            }
            if (!res && reason == PowerManager.GO_TO_SLEEP_REASON_DEVICE_ADMIN) {
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, "secure breathing, off by device admin");
                }
                res = true;
            }
            if (!res && reason == PowerManager.GO_TO_SLEEP_REASON_USER && AonServiceBreath.this.mLockPatternUtils.getPowerButtonInstantlyLocks()) {
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, "secure breathing, power instant lock");
                }
                res = true;
            }
            if (!res && AonServiceBreath.this.mLockScreenLockAfterTimeOut == 0) {
                if (AonService.DEBUG) {
                    Log.d(AonServiceBreath.TAG, "secure breathing, timeout 0 to lock");
                }
                res = true;
            }
            if (res) {
                return res;
            }
            long policyTimeout = AonServiceBreath.this.mDPM.getMaximumTimeToLock(null);
            if (policyTimeout <= 0 || policyTimeout - AonServiceBreath.this.mDisplayTimeOut > 0) {
                return res;
            }
            if (AonService.DEBUG) {
                Log.d(AonServiceBreath.TAG, "secure breathing, policy " + policyTimeout + ", display " + AonServiceBreath.this.mDisplayTimeOut);
            }
            return true;
        }

        /* access modifiers changed from: protected */
        public void dump(FileDescriptor fd, PrintWriter fout, String[] args) {
            if (AonServiceBreath.this.checkCallingPermission("android.permission.DUMP") == 0) {
                fout.println("Last not breathing reason: " + this.mLastNotBreathing);
                fout.println("Last critical error: " + this.mLastCriticalError);
                ArrayList<NotificationEntry> breathingList = AonServiceBreath.this.mAonNotificationManager.getNotificationListForBreathing();
                fout.println("Breathing notification list");
                Iterator i$ = breathingList.iterator();
                while (i$.hasNext()) {
                    fout.println(" " + ((NotificationEntry) i$.next()).minedData);
                }
            }
        }

        public void onESD() throws RemoteException {
            AonServiceBreath.this.doTryUpdateBreathing(true);
        }

        public void onTouch() throws RemoteException {
            throw new RemoteException("should not call onTouch from AonServiceBreath");
        }
    }

    interface WakeupReason {
        public static final int OTHER = 0;
        public static final int POWER_BUTTON = 3;
        public static final int SWIPE_NOTIFICATION = 2;
        public static final int SWIPE_UNLOCK = 1;
        public static final int UPDATE_BREATHING = 4;
    }

    static /* synthetic */ int access$4004(AonServiceBreath x0) {
        int i = x0.mTimedoutRetry + 1;
        x0.mTimedoutRetry = i;
        return i;
    }

    /* access modifiers changed from: private */
    public void doTryUpdateBreathing(boolean force) {
        boolean isBreathing = false;
        if (!force) {
            try {
                isBreathing = this.mPartialDisplayServer.isPartialDisplayActivated();
            } catch (RemoteException e) {
            }
        }
        if ((force || isBreathing) && this.mAonNotificationManager != null) {
            ((AonNotificationManagerBreath) this.mAonNotificationManager).notifyForceRefreshBreathingView();
        }
    }

    /* access modifiers changed from: protected */
    public boolean onReceiveBroadcast(Context context, Intent intent) {
        boolean z;
        if (intent != null) {
            String action = intent.getAction();
            if (DEBUG) {
                Log.d(TAG, "receive intent " + (action != null ? action : "null"));
            }
            if ("android.intent.action.TIMEZONE_CHANGED".equals(action) || "android.intent.action.TIME_SET".equals(action) || "android.intent.action.DATE_CHANGED".equals(action)) {
                if (this.mSnAon != 0) {
                    z = true;
                } else {
                    z = false;
                }
                scheduleNextCheckNightOffAlarm(z, true);
                doTryUpdateBreathing(false);
                return true;
            } else if ("com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD".equals(action)) {
                if (this.mSnPrivacy == 0) {
                    return true;
                }
                doTryUpdateBreathing(false);
                return true;
            } else if ("com.motorola.aon.CHECK_NIGHT_OFF".equals(action)) {
                scheduleNextCheckNightOffAlarm(true, true);
                doTryUpdateBreathing(true);
                return true;
            } else if ("com.motorola.internal.policy.statusbar.NOTIFICATION_VIEW_FULLY_VISIBLE".equals(action)) {
                this.mAonNotificationManager.enableTracking(false);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onMidNightAlarm() {
        this.mAonViewManager.nextBreathingViewPos();
        doTryUpdateBreathing(false);
    }

    /* access modifiers changed from: protected */
    public void onAonInterfaceConntected() {
        super.onAonInterfaceConntected();
        try {
            this.mAonInterface.registerPartialDisplay(this.mPartialDisplayServer);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "failed to register PartailDisplayService");
        }
    }

    /* access modifiers changed from: protected */
    public void onAonInterfaceDisconntected() {
        this.mAonViewManager.requestInvalidateSurface();
    }

    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "on create");
        if (this.mDefaultControlReg == null) {
            this.mDefaultControlReg = PartialDisplay.loadDefaultControlReg(getApplicationContext());
        }
        android.os.PowerManager pm = (android.os.PowerManager) getSystemService("power");
        this.mDPM = (DevicePolicyManager) getSystemService("device_policy");
        this.mLockPatternUtils = new LockPatternUtils(this);
        onEnableAon();
        boolean inPartialMode = PersistentManager.mInst.getPartialMode(getApplicationContext()) != 0;
        boolean isScreenOn = pm.isScreenOn();
        if (inPartialMode) {
            if (DEBUG) {
                Log.d(TAG, "AON service got killed in partial mode");
            }
            if (!isScreenOn) {
                this.mPartialDisplayServer.forcePartialMode();
                doTryUpdateBreathing(true);
            } else if (DEBUG) {
                Log.d(TAG, "Screen on");
            }
        } else if (!isScreenOn) {
            this.mPartialDisplayServer.mState = 1;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.mAonViewManager.deInitBreathingThread();
    }

    /* access modifiers changed from: protected */
    public AonNotificationManagerBreath getNotificationManager(Context context) {
        return new AonNotificationManagerBreath(context);
    }

    private void onEnableAon() {
        if (DEBUG) {
            Log.d(TAG, "on Enable AON");
        }
        this.mAonViewManager = new AonViewManagerBreath(getApplicationContext(), (AonNotificationManagerBreath) this.mAonNotificationManager, this.mIGetAonInterface);
        this.mAonViewManager.initBreathingThread();
    }

    /* access modifiers changed from: protected */
    public void registerBroadcast(IntentFilter ifilter) {
        ifilter.addAction("com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD");
        ifilter.addAction("com.motorola.aon.CHECK_NIGHT_OFF");
        ifilter.addAction("com.motorola.internal.policy.statusbar.NOTIFICATION_VIEW_FULLY_VISIBLE");
    }

    /* access modifiers changed from: protected */
    public void updateBreathing(boolean incoming, boolean force) {
        if (this.mPartialDisplayServer.mState == 1 && !this.mHdmiPlugged && this.mTelephonyManager.getCallState() == 0) {
            if (this.mAonInterface != null) {
                try {
                    this.mAonInterface.requestUpdateAod();
                } catch (RemoteException e) {
                    Log.w(TAG, "remote exception for requestUpdateAod");
                    requestRebind();
                }
            }
        } else if (DEBUG) {
            Log.d(TAG, "ignore update breathing under state " + this.mPartialDisplayServer.mState);
        }
    }

    /* access modifiers changed from: protected */
    public void onHdmiPlugged() {
        doTryUpdateBreathing(true);
    }

    /* access modifiers changed from: protected */
    public void onWirelessDisplayMirroring() {
        doTryUpdateBreathing(true);
    }

    /* access modifiers changed from: protected */
    public void onPrivacyModeChange() {
        doTryUpdateBreathing(false);
    }

    /* access modifiers changed from: protected */
    public void onNotificationReadByAOV(long timeCompleted, HashMap<String, ArrayList<String>> hashMap) {
        if (DEBUG) {
            Log.d(TAG, "Notification has been read out by AOV");
        }
        doTryUpdateBreathing(true);
    }

    /* access modifiers changed from: protected */
    public void onBatteryChange() {
        doTryUpdateBreathing(false);
    }

    /* access modifiers changed from: protected */
    public void onHapticsSettingsChanged() {
        if (DEBUG) {
            Log.d(TAG, "Haptics settings changed: New Value : " + this.mbHapticsEnabled);
        }
    }
}
