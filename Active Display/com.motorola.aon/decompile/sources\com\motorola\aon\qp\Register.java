package com.motorola.aon.qp;

import android.os.RemoteException;
import android.util.Log;
import com.motorola.slpc.ISensorhubService;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public abstract class Register {
    protected static final int CMD_DATA_SIZE = 4;
    private static final boolean DEBUG = true;
    private static final String TAG = "Aon_Register";
    protected int mRegisterID;
    protected int mRegisterSize;

    /* access modifiers changed from: 0000 */
    public abstract byte[] toByteArray();

    /* access modifiers changed from: protected */
    public int write(ISensorhubService service) {
        int result;
        Log.d(TAG, "write: ISensorhubService[" + service + "]");
        if (service == null) {
            return -1;
        }
        byte[] writeData = toByteArray();
        if (writeData.length != this.mRegisterSize) {
            Log.e(TAG, "write: The data size of register[" + this.mRegisterID + "]:" + writeData.length + " expecting: " + this.mRegisterSize);
            return -1;
        }
        ByteArrayOutputStream cmdDataByteArrayOut = new ByteArrayOutputStream(4);
        DataOutputStream cmdDataOut = new DataOutputStream(cmdDataByteArrayOut);
        try {
            cmdDataOut.writeShort(this.mRegisterID);
            cmdDataOut.writeShort(this.mRegisterSize);
        } catch (IOException e) {
            Log.e(TAG, "Write cmdData excetpion:", e);
        }
        Log.d(TAG, "write_register_cmd_data[" + cmdDataByteArrayOut.size() + "]");
        StringBuilder sb = new StringBuilder();
        for (byte b : cmdDataByteArrayOut.toByteArray()) {
            sb.append(String.format("%02X ", new Object[]{Byte.valueOf(b)}));
        }
        Log.d(TAG, "write: Command data dump:" + sb);
        try {
            result = service.write(cmdDataByteArrayOut.toByteArray(), writeData);
            if (result < 0) {
                Log.e(TAG, "Failed to write to ISensorhubService");
            }
            Log.d(TAG, "control register written to SH");
        } catch (RemoteException e2) {
            Log.e(TAG, "write: Connection with ISensorhubService borken: ", e2);
            result = -1;
        }
        return result;
    }

    public boolean read(ISensorhubService service) {
        return DEBUG;
    }
}
