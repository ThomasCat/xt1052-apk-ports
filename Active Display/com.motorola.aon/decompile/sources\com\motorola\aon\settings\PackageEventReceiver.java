package com.motorola.aon.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.motorola.aon.Constants;

public class PackageEventReceiver extends BroadcastReceiver {
    private static final boolean DEBUG = AonSettingsConst.DEBUG;
    static final String TAG = "Aon_PkgEventReceiver";

    public void onReceive(Context context, Intent intent) {
        if (intent != null && context != null) {
            String action = intent.getAction();
            if (action != null) {
                if (DEBUG) {
                    Log.d(TAG, "receive: " + action);
                }
                Intent serviceIntent = new Intent(context, PackageEventService.class);
                if ("android.intent.action.BOOT_COMPLETED".equals(action)) {
                    if (DEBUG) {
                        Log.d(TAG, "About to checking in settings data");
                    }
                    serviceIntent.setAction(Constants.SN_ANALYTICS_SETTINGS);
                } else {
                    serviceIntent.setAction(action);
                    serviceIntent.setData(intent.getData());
                }
                context.startService(serviceIntent);
            }
        }
    }
}
