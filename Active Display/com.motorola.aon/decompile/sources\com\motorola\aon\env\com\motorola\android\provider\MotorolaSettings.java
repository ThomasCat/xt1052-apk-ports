package com.motorola.aon.env.com.motorola.android.provider;

import android.content.ContentResolver;
import android.net.Uri;
import android.util.Log;
import java.lang.reflect.Method;

public class MotorolaSettings {
    private static final String CLASS_MOTO_SETTINGS = "com.motorola.android.provider.MotorolaSettings";
    private static final String FIELD_PRIVACY_SMART_NOTIFICATIONS_CHROME = "PRIVACY_SMART_NOTIFICATIONS_CHROME";
    private static final String FIELD_SN_AON = "SN_AON";
    private static final String FIELD_SN_AUTO_SELECT_INSTALLED = "SN_AUTO_SELECT_INSTALLED";
    private static final String FIELD_SN_NIGHT_BEGIN = "SN_NIGHT_BEGIN";
    private static final String FIELD_SN_NIGHT_END = "SN_NIGHT_END";
    private static final String FIELD_SN_NIGHT_OFF = "SN_NIGHT_OFF";
    private static final String FIELD_SN_PRIVACY = "SN_PRIVACY";
    private static final String FIELD_SN_WHITE_LIST = "SN_WHITE_LIST";
    private static final String METHOD_GET_INT = "getInt";
    private static final String METHOD_GET_STRING = "getString";
    private static final String METHOD_GET_URI_FOR = "getUriFor";
    private static final String METHOD_PUT_INT = "putInt";
    private static final String METHOD_PUT_STRING = "putString";
    public static String PRIVACY_SMART_NOTIFICATIONS_CHROME = null;
    public static String SN_AON = null;
    public static String SN_AUTO_SELECT_INSTALLED = null;
    public static String SN_NIGHT_BEGIN = null;
    public static String SN_NIGHT_END = null;
    public static String SN_NIGHT_OFF = null;
    public static String SN_PRIVACY = null;
    public static String SN_WHITE_LIST = null;
    private static final String TAG = "Aon_MotorolaSettings";
    public static boolean sInitialized;
    private static Method sMethodGetInt;
    private static Method sMethodGetString;
    private static Method sMethodGetUriFor;
    private static Method sMethodPutInt;
    private static Method sMethodPutString;

    static {
        SN_WHITE_LIST = null;
        SN_AON = null;
        SN_PRIVACY = null;
        SN_NIGHT_OFF = null;
        SN_NIGHT_BEGIN = null;
        SN_NIGHT_END = null;
        SN_AUTO_SELECT_INSTALLED = null;
        PRIVACY_SMART_NOTIFICATIONS_CHROME = null;
        sInitialized = false;
        try {
            Class<?> clazz = Class.forName(CLASS_MOTO_SETTINGS);
            SN_WHITE_LIST = (String) clazz.getField(FIELD_SN_WHITE_LIST).get(null);
            SN_AON = (String) clazz.getField(FIELD_SN_AON).get(null);
            SN_PRIVACY = (String) clazz.getField(FIELD_SN_PRIVACY).get(null);
            SN_NIGHT_OFF = (String) clazz.getField(FIELD_SN_NIGHT_OFF).get(null);
            SN_NIGHT_BEGIN = (String) clazz.getField(FIELD_SN_NIGHT_BEGIN).get(null);
            SN_NIGHT_END = (String) clazz.getField(FIELD_SN_NIGHT_END).get(null);
            SN_AUTO_SELECT_INSTALLED = (String) clazz.getField(FIELD_SN_AUTO_SELECT_INSTALLED).get(null);
            PRIVACY_SMART_NOTIFICATIONS_CHROME = (String) clazz.getField(FIELD_PRIVACY_SMART_NOTIFICATIONS_CHROME).get(null);
            sMethodGetString = clazz.getDeclaredMethod(METHOD_GET_STRING, new Class[]{ContentResolver.class, String.class});
            sMethodGetUriFor = clazz.getDeclaredMethod(METHOD_GET_URI_FOR, new Class[]{String.class});
            sMethodGetInt = clazz.getDeclaredMethod(METHOD_GET_INT, new Class[]{ContentResolver.class, String.class, Integer.TYPE});
            sMethodPutInt = clazz.getDeclaredMethod(METHOD_PUT_INT, new Class[]{ContentResolver.class, String.class, Integer.TYPE});
            sMethodPutString = clazz.getDeclaredMethod(METHOD_PUT_STRING, new Class[]{ContentResolver.class, String.class, String.class});
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize class");
        }
    }

    public static String getString(ContentResolver cr, String name) {
        if (sMethodGetString != null) {
            try {
                return (String) sMethodGetString.invoke(null, new Object[]{cr, name});
            } catch (Throwable th) {
                Log.w(TAG, "unable to invoke getString");
            }
        }
        return null;
    }

    public static Uri getUriFor(String name) {
        if (sMethodGetUriFor != null) {
            try {
                return (Uri) sMethodGetUriFor.invoke(null, new Object[]{name});
            } catch (Throwable th) {
                Log.w(TAG, "unable to invoke getUriFor");
            }
        }
        return null;
    }

    public static int getInt(ContentResolver cr, String name, int def) {
        int i = 0;
        if (sMethodGetInt == null) {
            return i;
        }
        try {
            return ((Integer) sMethodGetInt.invoke(null, new Object[]{cr, name, Integer.valueOf(def)})).intValue();
        } catch (Throwable th) {
            Log.w(TAG, "unable to invoke getInt");
            return i;
        }
    }

    public static void putInt(ContentResolver cr, String name, int val) {
        if (sMethodPutInt != null) {
            try {
                sMethodPutInt.invoke(null, new Object[]{cr, name, Integer.valueOf(val)});
            } catch (Throwable th) {
                Log.w(TAG, "unable to invoke putInt");
            }
        }
    }

    public static void putString(ContentResolver cr, String name, String val) {
        if (sMethodPutString != null) {
            try {
                sMethodPutString.invoke(null, new Object[]{cr, name, val});
            } catch (Throwable th) {
                Log.w(TAG, "unable to invoke putString");
            }
        }
    }
}
