package com.motorola.aon.env;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.motorola.aon.Constants;
import com.motorola.aon.env.android.app.ActivityManagerNative;
import com.motorola.aon.env.android.app.StatusBarManager;
import com.motorola.aon.env.android.app.admin.DevicePolicyManager;
import com.motorola.aon.env.android.content.Intent;
import com.motorola.aon.env.android.os.IRemoteCallback.Stub;
import com.motorola.aon.env.android.os.PowerManager;
import com.motorola.aon.env.android.os.ServiceManager;
import com.motorola.aon.env.android.os.SystemProperties;
import com.motorola.aon.env.android.provider.Settings.Secure;
import com.motorola.aon.env.android.view.Display;
import com.motorola.aon.env.android.view.DisplayInfo;
import com.motorola.aon.env.android.view.IWindowManager;
import com.motorola.aon.env.android.view.WindowManagerPolicy;
import com.motorola.aon.env.com.android.internal.widget.LockPatternUtils;
import com.motorola.aon.env.com.motorola.android.provider.MotorolaSettings;
import com.motorola.aon.env.com.motorola.blur.util.CheckinEventWrapper;
import com.motorola.aon.env.com.motorola.blur.util.CheckinSegmentWrapper;
import com.motorola.aon.env.com.motorola.slpc.ModalityManager;
import com.motorola.aon.env.com.motorola.slpc.Transition;
import com.motorola.aon.env.com.motorola.wirelessdisplay.service.WirelessDisplayManager;

public class EnvChecker {
    private static final boolean DEBUG = Constants.DEBUG;
    private static final String PKG_SETTINGS = "com.android.settings";
    private static final String TAG = "Aon_EnvChecker";
    private static Drawable mActionBarIcon;
    private static Drawable mQuickSettingIcon;

    public static Drawable getActionBarIcon() {
        return mActionBarIcon;
    }

    public static Drawable getQuickSettingIcon() {
        return mQuickSettingIcon;
    }

    private static boolean initResources(Context context) {
        try {
            Resources res = context.createPackageContext(PKG_SETTINGS, 2).getResources();
            int id = res.getIdentifier("ic_bt_config", "drawable", PKG_SETTINGS);
            if (id != 0) {
                mQuickSettingIcon = res.getDrawable(id);
            }
            int id2 = res.getIdentifier("ic_launcher_settings", "mipmap", PKG_SETTINGS);
            if (id2 != 0) {
                mActionBarIcon = res.getDrawable(id2);
            }
        } catch (NameNotFoundException e) {
            Log.w(TAG, "unable to initialize resources");
        }
        return (mActionBarIcon == null || mQuickSettingIcon == null) ? false : true;
    }

    public static boolean isEnvOK(Context context) {
        if (DEBUG) {
            Log.i(TAG, "StatusBarManager.sInitialized       " + StatusBarManager.sInitialized);
            Log.i(TAG, "DevicePolicyManager.sInitialized    " + DevicePolicyManager.sInitialized);
            Log.i(TAG, "Context.sInitialized                " + com.motorola.aon.env.android.content.Context.sInitialized);
            Log.i(TAG, "Intent.sInitialized                 " + Intent.sInitialized);
            Log.i(TAG, "IRemoteCallback.Stub.sInitialized   " + Stub.sInitialized);
            Log.i(TAG, "PowerManager.sInitialized           " + PowerManager.sInitialized);
            Log.i(TAG, "ServiceManager.sInitialized         " + ServiceManager.sInitialized);
            Log.i(TAG, "SystemProperties.sInitialized       " + SystemProperties.sInitialized);
            Log.i(TAG, "Settings.Secure.sInitialized        " + Secure.sInitialized);
            Log.i(TAG, "Display.sInitialized                " + Display.sInitialized);
            Log.i(TAG, "DisplayInfo.sInitialized            " + DisplayInfo.sInitialized);
            Log.i(TAG, "IWindowManager.Stub.sInitialized    " + IWindowManager.Stub.sInitialized);
            Log.i(TAG, "WindowManagerPolicy.sInitialized    " + WindowManagerPolicy.sInitialized);
            Log.i(TAG, "MotorolaSettings.sInitialized       " + MotorolaSettings.sInitialized);
            Log.i(TAG, "CheckinEventWrapper.sInitialized    " + CheckinEventWrapper.sInitialized);
            Log.i(TAG, "CheckinSegmentWrapper.sInitialized  " + CheckinSegmentWrapper.sInitialized);
            Log.i(TAG, "WirelessDisplayManager.sInitialized " + WirelessDisplayManager.sInitialized);
            Log.i(TAG, "ModalityManager.sInitialized        " + ModalityManager.sInitialized);
            Log.i(TAG, "Transition.sInitialized             " + Transition.sInitialized);
            Log.i(TAG, "LockPatternUtils.sInitialized       " + LockPatternUtils.sInitialized);
            Log.i(TAG, "ActivityManagerNative.sInitialized  " + ActivityManagerNative.sInitialized);
        }
        return initResources(context) && StatusBarManager.sInitialized && DevicePolicyManager.sInitialized && com.motorola.aon.env.android.content.Context.sInitialized && Intent.sInitialized && Stub.sInitialized && PowerManager.sInitialized && ServiceManager.sInitialized && SystemProperties.sInitialized && Secure.sInitialized && Display.sInitialized && DisplayInfo.sInitialized && IWindowManager.Stub.sInitialized && WindowManagerPolicy.sInitialized && MotorolaSettings.sInitialized && CheckinEventWrapper.sInitialized && CheckinSegmentWrapper.sInitialized && WirelessDisplayManager.sInitialized && LockPatternUtils.sInitialized && ActivityManagerNative.sInitialized;
    }
}
