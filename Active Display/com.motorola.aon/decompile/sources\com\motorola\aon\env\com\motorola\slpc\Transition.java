package com.motorola.aon.env.com.motorola.slpc;

import android.util.Log;
import java.lang.reflect.Method;

public class Transition {
    private static final String CLASS_TRANSITION = "com.motorola.slpc.Transition";
    private static final String METHOD_GET_NEW_STATE = "getNewState";
    private static final String METHOD_GET_OLD_STATE = "getOldState";
    private static final String TAG = "Aon_Transition";
    public static boolean sInitialized;
    private static Method sMethodGetNewState;
    private static Method sMethodGetOldState;
    private final Object mObjTransition;

    private Transition(Object obj) {
        this.mObjTransition = obj;
    }

    public int getNewState() {
        int i = 0;
        if (sMethodGetNewState == null) {
            return i;
        }
        try {
            return ((Integer) sMethodGetNewState.invoke(this.mObjTransition, new Object[0])).intValue();
        } catch (Throwable th) {
            Log.w(TAG, "unable to invoke getNewState");
            return i;
        }
    }

    public int getOldState() {
        int i = 0;
        if (sMethodGetOldState == null) {
            return i;
        }
        try {
            return ((Integer) sMethodGetOldState.invoke(this.mObjTransition, new Object[0])).intValue();
        } catch (Throwable th) {
            Log.w(TAG, "unable to invoke getOldState");
            return i;
        }
    }

    public static Transition from(Object object) {
        if (object != null) {
            return new Transition(object);
        }
        return null;
    }

    static {
        sInitialized = false;
        try {
            Class<?> clazz = Class.forName(CLASS_TRANSITION);
            sMethodGetNewState = clazz.getDeclaredMethod(METHOD_GET_NEW_STATE, new Class[0]);
            sMethodGetOldState = clazz.getDeclaredMethod(METHOD_GET_OLD_STATE, new Class[0]);
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize class");
        }
    }
}
