package com.motorola.aon.env.com.motorola.blur.util;

import android.util.Log;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class CheckinSegmentWrapper {
    private static final String CLASS_SEGMENT = "com.motorola.data.event.api.Segment";
    private static final String METHOD_GET_SEGMENT_NAME = "getSegmentName";
    private static final String METHOD_SET_VALUE = "setValue";
    private static final String TAG = CheckinSegmentWrapper.class.getSimpleName();
    private static Constructor<?> sConstructorSegment;
    public static boolean sInitialized;
    private static Method sMethodGetSegmentName;
    private static Method sMethodSetNameValueBoolean;
    private static Method sMethodSetNameValueDouble;
    private static Method sMethodSetNameValueInt;
    private static Method sMethodSetNameValueLong;
    private static Method sMethodSetNameValueString;
    private final Object mReflectedSegment;

    static {
        sInitialized = false;
        try {
            Class<?> clsSegment = Class.forName(CLASS_SEGMENT);
            sConstructorSegment = clsSegment.getDeclaredConstructor(new Class[]{String.class});
            sMethodGetSegmentName = clsSegment.getDeclaredMethod(METHOD_GET_SEGMENT_NAME, new Class[0]);
            sMethodSetNameValueBoolean = clsSegment.getDeclaredMethod(METHOD_SET_VALUE, new Class[]{String.class, Boolean.TYPE});
            sMethodSetNameValueDouble = clsSegment.getDeclaredMethod(METHOD_SET_VALUE, new Class[]{String.class, Double.TYPE});
            sMethodSetNameValueInt = clsSegment.getDeclaredMethod(METHOD_SET_VALUE, new Class[]{String.class, Integer.TYPE});
            sMethodSetNameValueLong = clsSegment.getDeclaredMethod(METHOD_SET_VALUE, new Class[]{String.class, Long.TYPE});
            sMethodSetNameValueString = clsSegment.getDeclaredMethod(METHOD_SET_VALUE, new Class[]{String.class, String.class});
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "Unable to get segment class.");
        }
    }

    public CheckinSegmentWrapper(String segmentName) {
        Object segment = null;
        if (sInitialized && segmentName != null && !segmentName.isEmpty()) {
            try {
                segment = sConstructorSegment.newInstance(new Object[]{segmentName});
            } catch (Throwable th) {
                Log.w(TAG, "Unable to instantiate Segment.");
            }
        }
        this.mReflectedSegment = segment;
    }

    public static boolean isInitialized() {
        return sInitialized;
    }

    /* access modifiers changed from: 0000 */
    public Object getSegment() {
        return this.mReflectedSegment;
    }

    public String getSegmentName() {
        String segmentName = null;
        if (!sInitialized || this.mReflectedSegment == null) {
            return segmentName;
        }
        try {
            return String.valueOf(sMethodGetSegmentName.invoke(this.mReflectedSegment, new Object[0]));
        } catch (Throwable th) {
            Log.w(TAG, "Unable to get Segment name.");
            return segmentName;
        }
    }

    public void setValue(String name, boolean value) {
        if (sInitialized && this.mReflectedSegment != null) {
            try {
                sMethodSetNameValueBoolean.invoke(this.mReflectedSegment, new Object[]{name, Boolean.valueOf(value)});
            } catch (Throwable th) {
                Log.w(TAG, "Unable to set value.");
            }
        }
    }

    public void setValue(String name, double value) {
        if (sInitialized && this.mReflectedSegment != null) {
            try {
                sMethodSetNameValueDouble.invoke(this.mReflectedSegment, new Object[]{name, Double.valueOf(value)});
            } catch (Throwable th) {
                Log.w(TAG, "Unable to set value.");
            }
        }
    }

    public void setValue(String name, int value) {
        if (sInitialized && this.mReflectedSegment != null) {
            try {
                sMethodSetNameValueInt.invoke(this.mReflectedSegment, new Object[]{name, Integer.valueOf(value)});
            } catch (Throwable th) {
                Log.w(TAG, "Unable to set value.");
            }
        }
    }

    public void setValue(String name, long value) {
        if (sInitialized && this.mReflectedSegment != null) {
            try {
                sMethodSetNameValueLong.invoke(this.mReflectedSegment, new Object[]{name, Long.valueOf(value)});
            } catch (Throwable th) {
                Log.w(TAG, "Unable to set value.");
            }
        }
    }

    public void setValue(String name, String value) {
        if (sInitialized && this.mReflectedSegment != null) {
            if (value == null) {
                value = "null";
            }
            try {
                sMethodSetNameValueString.invoke(this.mReflectedSegment, new Object[]{name, value});
            } catch (Throwable th) {
                Log.w(TAG, "Unable to set value.");
            }
        }
    }
}
