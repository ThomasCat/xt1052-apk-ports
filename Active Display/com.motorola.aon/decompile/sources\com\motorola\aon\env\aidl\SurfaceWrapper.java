package com.motorola.aon.env.aidl;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.view.Surface;

public class SurfaceWrapper implements Parcelable {
    public static final Creator<SurfaceWrapper> CREATOR = new Creator<SurfaceWrapper>() {
        public SurfaceWrapper createFromParcel(Parcel parcel) {
            SurfaceWrapper sw = new SurfaceWrapper();
            sw.readFromParcel(parcel);
            return sw;
        }

        public SurfaceWrapper[] newArray(int size) {
            return new SurfaceWrapper[size];
        }
    };
    private Surface mSurface;

    public SurfaceWrapper() {
    }

    public SurfaceWrapper(Surface s) {
        this.mSurface = s;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int flags) {
        if (this.mSurface != null) {
            parcel.writeInt(1);
            this.mSurface.writeToParcel(parcel, flags);
            return;
        }
        parcel.writeInt(0);
    }

    public void readFromParcel(Parcel source) {
        if (source.readInt() != 0) {
            if (this.mSurface == null) {
                this.mSurface = com.motorola.aon.env.android.view.Surface.create();
            }
            this.mSurface.readFromParcel(source);
        }
    }

    public void copyFrom(Surface other) {
    }
}
