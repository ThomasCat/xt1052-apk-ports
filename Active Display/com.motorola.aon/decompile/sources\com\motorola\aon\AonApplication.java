package com.motorola.aon;

import android.app.Application;
import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.util.Log;
import com.motorola.aon.env.EnvChecker;
import com.motorola.aon.fd.AonServiceGlimpse;
import com.motorola.aon.pd.AonServiceBreath;
import com.motorola.aon.qp.AonServiceQuick;

public class AonApplication extends Application {
    private static final String TAG = "AonApplication";

    public void onCreate() {
        if (!EnvChecker.isEnvOK(getApplicationContext())) {
            Log.w(TAG, "environment check failed");
        } else {
            Log.d(TAG, "environment check Successful");
        }
        PackageManager pm = getPackageManager();
        if (Constants.isPartialDisplay) {
            Log.d(TAG, "partialDisplay=true disable AonServiceGlimpse.class)");
            pm.setComponentEnabledSetting(new ComponentName(this, AonServiceGlimpse.class), 2, 1);
            if (!pm.hasSystemFeature(Constants.QUICK_PEEK_FEATURE)) {
                Log.d(TAG, "No QP feature enable AonServiceBreath.class");
                pm.setComponentEnabledSetting(new ComponentName(this, AonServiceBreath.class), 1, 1);
                Log.d(TAG, "No QP feature disable AonServiceQuick.class");
                pm.setComponentEnabledSetting(new ComponentName(this, AonServiceQuick.class), 2, 1);
                return;
            }
            Log.d(TAG, " with QP enable AonServiceQuick.class)");
            pm.setComponentEnabledSetting(new ComponentName(this, AonServiceQuick.class), 1, 1);
            Log.d(TAG, "with QP  disable AonServiceBreath.class)");
            pm.setComponentEnabledSetting(new ComponentName(this, AonServiceBreath.class), 2, 1);
            return;
        }
        Log.d(TAG, "partialDisplay=false disable AonServiceQuick.class & AonServiceBreath.class)");
        pm.setComponentEnabledSetting(new ComponentName(this, AonServiceBreath.class), 2, 1);
        pm.setComponentEnabledSetting(new ComponentName(this, AonServiceQuick.class), 2, 1);
    }
}
