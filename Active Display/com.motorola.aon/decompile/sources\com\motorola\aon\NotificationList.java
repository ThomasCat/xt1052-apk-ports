package com.motorola.aon;

import com.motorola.aon.aidl.NotificationEntry;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.ListIterator;

public class NotificationList extends ArrayList<NotificationEntry> {
    private static final long serialVersionUID = 1;
    private final Comparator<NotificationEntry> mEntryCmp = new Comparator<NotificationEntry>() {
        public int compare(NotificationEntry a, NotificationEntry b) {
            MinedData na = a.minedData;
            MinedData nb = b.minedData;
            int d = na.score - nb.score;
            return d != 0 ? d : (int) (na.timeStamp - nb.timeStamp);
        }
    };

    public boolean add(NotificationEntry e) {
        throw new RuntimeException("should not call");
    }

    public boolean addInOrder(NotificationEntry e) {
        if (e != null) {
            ListIterator<NotificationEntry> it = listIterator();
            while (true) {
                if (!it.hasNext()) {
                    super.add(e);
                    break;
                } else if (this.mEntryCmp.compare(e, it.next()) > 0) {
                    if (it.hasPrevious()) {
                        it.previous();
                    }
                    it.add(e);
                }
            }
        }
        return true;
    }
}
