package com.motorola.aon.qp;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.os.SystemClock;
import android.telephony.PhoneStateListener;
import android.text.format.DateFormat;
import android.util.Log;
import com.motorola.aon.AonService;
import com.motorola.aon.SnAnalyticsEventService;
import com.motorola.aon.aidl.NotificationEntry;
import com.motorola.aon.env.aidl.IPartialDisplayService;
import com.motorola.aon.env.aidl.IRemoteCallback;
import com.motorola.aon.env.android.os.PowerManager;
import com.motorola.aon.env.com.android.internal.widget.LockPatternUtils;
import com.motorola.aon.jni.PartialDisplay;
import com.motorola.aon.jni.PartialDisplay.InstrumentationRegister;
import com.motorola.aon.jni.PartialDisplay.StatusRegister;
import com.motorola.aon.jni.PartialDisplay.TouchRegister;
import com.motorola.aon.qp.AonViewManagerQuick.ICallback;
import com.motorola.slpc.ISensorhubListener;
import com.motorola.slpc.ISensorhubService;
import com.motorola.slpc.ISensorhubService.Stub;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;

public class AonServiceQuick extends AonService {
    private static int DRAW_BREATHING_TIMEDOUT_RETRY_DELAY = 6000;
    private static int DRAW_BREATHING_TIMEDOUT_RETRY_TIMES = 10;
    /* access modifiers changed from: private */
    public static int MAX_POLL_PROX_SCREEN_OFF = 60;
    private static final int PEEK_WAIT_MAX_RETRY = 10;
    private static final int PEEK_WAIT_RETRY_INTERVAL = 500;
    /* access modifiers changed from: private */
    public static int POLL_PROX_SCREEN_OFF_DELAY = 1000;
    private static final String TAG = "AonServiceQuick";
    /* access modifiers changed from: private */
    public AonViewManagerQuick mAonViewManager;
    /* access modifiers changed from: private */
    public ControlRegister mControlRegister;
    private DevicePolicyManager mDPM;
    /* access modifiers changed from: private */
    public boolean mInPartialMode = false;
    /* access modifiers changed from: private */
    public String mLastCriticalError = "none";
    /* access modifiers changed from: private */
    public String mLastNotBreathing = "none";
    /* access modifiers changed from: private */
    public int mLastWakeupReason = 0;
    private LockPatternUtils mLockPatternUtils;
    private PartialDisplayServer mPartialDisplayServer = new PartialDisplayServer();
    /* access modifiers changed from: private */
    public final PhoneStateListener mPhonteStateListener = new PhoneStateListener() {
        public void onCallStateChanged(int state, String incomingNumber) {
            if (state == 0 && !AonServiceQuick.this.mScreenOn) {
                if (AonService.DEBUG) {
                    Log.d(AonServiceQuick.TAG, "call state to idle when screen is off");
                }
                AonServiceQuick.this.mTelephonyManager.listen(AonServiceQuick.this.mPhonteStateListener, 0);
                if (PowerManager.proximityTargetDetected(AonServiceQuick.this.mPowerManager)) {
                    AonServiceQuick.this.mPollProxCount = 0;
                    AonServiceQuick.this.mHandler.removeCallbacks(AonServiceQuick.this.mPollProxWhenScreenOff);
                    AonServiceQuick.this.mHandler.postDelayed(AonServiceQuick.this.mPollProxWhenScreenOff, (long) AonServiceQuick.POLL_PROX_SCREEN_OFF_DELAY);
                    return;
                }
                AonServiceQuick.this.updateBreathing(false, true);
            }
        }
    };
    /* access modifiers changed from: private */
    public int mPollProxCount;
    Runnable mPollProxWhenScreenOff = new Runnable() {
        public void run() {
            if (!AonServiceQuick.this.mScreenOn) {
                AonServiceQuick.this.mPollProxCount = AonServiceQuick.this.mPollProxCount + 1;
                if (AonServiceQuick.this.mPollProxCount > AonServiceQuick.MAX_POLL_PROX_SCREEN_OFF) {
                    Log.w(AonServiceQuick.TAG, "poll prox when screen off time out");
                } else if (PowerManager.proximityTargetDetected(AonServiceQuick.this.mPowerManager)) {
                    if (AonService.DEBUG) {
                        Log.d(AonServiceQuick.TAG, "poll prox when screen off " + AonServiceQuick.this.mPollProxCount);
                    }
                    AonServiceQuick.this.mHandler.removeCallbacks(AonServiceQuick.this.mPollProxWhenScreenOff);
                    AonServiceQuick.this.mHandler.postDelayed(AonServiceQuick.this.mPollProxWhenScreenOff, (long) AonServiceQuick.POLL_PROX_SCREEN_OFF_DELAY);
                } else {
                    AonServiceQuick.this.updateBreathing(false, true);
                }
            }
        }
    };
    private final int mRetry = 0;
    /* access modifiers changed from: private */
    public boolean mScreenOn;
    /* access modifiers changed from: private */
    public final ServiceConnection mSensorHubConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            AonServiceQuick.this.mSensorHubService = Stub.asInterface(service);
            Log.d(AonServiceQuick.TAG, "bound to remote ISensorhubService...");
            try {
                AonServiceQuick.this.mSensorHubService.addSensorhubListener(AonServiceQuick.this.mSensorHubListener);
            } catch (RemoteException e) {
                Log.e(AonServiceQuick.TAG, "ERROR adding SensorHubListener: ", e);
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            Log.w(AonServiceQuick.TAG, "Service disconnected. ISensorhubService...");
            AonServiceQuick.this.mSensorHubService = null;
            AonServiceQuick.this.connectToSHService();
        }
    };
    /* access modifiers changed from: private */
    public SensorhubListener mSensorHubListener = new SensorhubListener();
    /* access modifiers changed from: private */
    public ISensorhubService mSensorHubService = null;
    /* access modifiers changed from: private */
    public WakeLock mWakeLock;

    class PartialDisplayServer extends IPartialDisplayService.Stub {
        public PartialDisplayServer() {
            PartialDisplay.nativeInit();
            if (AonService.DEBUG) {
                Log.d(AonServiceQuick.TAG, "PartialDisplayServer() : loadFirmwareVersion");
            }
            PartialDisplay.loadFirmwareVersion();
        }

        public boolean isPartialDisplayActivated() throws RemoteException {
            return AonServiceQuick.this.mInPartialMode;
        }

        public void notifyPowerKeyWakeup() {
            if (AonService.DEBUG) {
                Log.d(AonServiceQuick.TAG, "notify power key wake up");
            }
            AonServiceQuick.this.mLastWakeupReason = 3;
        }

        public void fbWillBeResumed(IRemoteCallback listener, int reason) throws RemoteException {
            throw new RemoteException("should not call fbWillBeResumed from AonServiceQuick");
        }

        public void fbWillBeSuspended(IRemoteCallback listener, int reason) throws RemoteException {
            throw new RemoteException("should not call fbWillBeSuspended from AonServiceQuick");
        }

        /* access modifiers changed from: protected */
        public void dump(FileDescriptor fd, PrintWriter fout, String[] args) {
            if (AonServiceQuick.this.checkCallingPermission("android.permission.DUMP") == 0) {
                fout.println("Last not breathing reason: " + AonServiceQuick.this.mLastNotBreathing);
                fout.println("Last critical error: " + AonServiceQuick.this.mLastCriticalError);
                fout.println("mAonInterface= " + AonServiceQuick.this.mAonInterface);
                fout.println("mIAonNL= " + AonServiceQuick.this.mIAonNL);
                fout.println("mSensorHubService= " + AonServiceQuick.this.mSensorHubService);
                fout.println("mSensorHubConnection= " + AonServiceQuick.this.mSensorHubConnection);
                ArrayList<NotificationEntry> breathingList = AonServiceQuick.this.mAonNotificationManager.getNotificationListForBreathing();
                fout.println("Breathing notification list");
                Iterator i$ = breathingList.iterator();
                while (i$.hasNext()) {
                    fout.println(" " + ((NotificationEntry) i$.next()).minedData);
                }
            }
        }

        public void onESD() throws RemoteException {
            throw new RemoteException("should not call onESD from AonServiceQuick");
        }

        public void onTouch() throws RemoteException {
            if (AonService.DEBUG) {
                Log.d(AonServiceQuick.TAG, "onTouch");
            }
            if (AonServiceQuick.this.mWakeLock != null) {
                if (AonService.DEBUG) {
                    Log.d(AonServiceQuick.TAG, "onTouch acquire wakelock");
                }
                AonServiceQuick.this.mWakeLock.acquire();
            } else {
                Log.w(AonServiceQuick.TAG, "onTouch null==mWakeLock This should never happen");
            }
            StatusRegister sr = new StatusRegister();
            PartialDisplay.nativeReadStatusRegister(sr);
            if (sr.mWakeupReason != 0) {
                TouchRegister tr = new TouchRegister();
                PartialDisplay.nativeReadTouchRegister(tr);
                if (AonService.DEBUG) {
                    Log.d(AonServiceQuick.TAG, "read touch status: x " + tr.mX + ", y " + tr.mY + ", pressure " + tr.mPressure);
                }
                if (AonServiceQuick.this.mControlRegister.mTouchRegionUpper.inRegion(tr.mX, tr.mY)) {
                    AonServiceQuick.this.mLastWakeupReason = 2;
                } else if (AonServiceQuick.this.mControlRegister.mTouchRegionDown.inRegion(tr.mX, tr.mY)) {
                    AonServiceQuick.this.mLastWakeupReason = 1;
                }
                AonServiceQuick.this.mAonInterface.wakeUp(SystemClock.uptimeMillis());
                AonServiceQuick.this.mAonViewManager.handleOnTouch(AonServiceQuick.this.mLastWakeupReason);
            }
            if (AonServiceQuick.this.mWakeLock == null) {
                Log.w(AonServiceQuick.TAG, "onTouch Tried to release wakeLock but null==mWakeLock This should never happen");
            } else if (AonServiceQuick.this.mWakeLock.isHeld()) {
                AonServiceQuick.this.mWakeLock.release();
                if (AonService.DEBUG) {
                    Log.d(AonServiceQuick.TAG, "onTouch release wakelock");
                }
            }
        }
    }

    private final class SensorhubListener extends ISensorhubListener.Stub {
        private SensorhubListener() {
        }

        public void onSensorhubReset() throws RemoteException {
            if (AonService.DEBUG) {
                Log.d(AonServiceQuick.TAG, "onSensorhubReset: restoring control register for SH...");
            }
            if (AonServiceQuick.this.mControlRegister == null) {
                Log.e(AonServiceQuick.TAG, "onSensorhubRest: ERROR restoring control register - mControlRegister is null");
                return;
            }
            PartialDisplay.nativeEnableBreathing(false);
            AonServiceQuick.this.writeRegister(AonServiceQuick.this.mControlRegister);
            PartialDisplay.nativeEnableBreathing(true);
        }
    }

    interface WakeupReason {
        public static final int OTHER = 0;
        public static final int POWER_BUTTON = 3;
        public static final int SWIPE_NOTIFICATION = 2;
        public static final int SWIPE_UNLOCK = 1;
    }

    /* access modifiers changed from: protected */
    public void updateBreathing(boolean incoming, boolean force) {
        int i;
        if (DEBUG) {
            Log.d(TAG, "updateBreathing - incoming[" + incoming + "] force[" + force + "] inPartialMode[" + this.mInPartialMode + "]");
        }
        verfiyPartialMode();
        if (force || this.mInPartialMode) {
            this.mWakeLock.acquire();
            Handler handler = this.mHandler;
            if (incoming) {
                i = 1;
            } else {
                i = 0;
            }
            handler.obtainMessage(5, i, 0).sendToTarget();
        }
    }

    /* access modifiers changed from: protected */
    public boolean onReceiveBroadcast(Context context, Intent intent) {
        boolean z;
        if (intent != null) {
            String action = intent.getAction();
            if (DEBUG) {
                Log.d(TAG, "receive intent " + (action != null ? action : "null"));
            }
            if ("android.intent.action.TIMEZONE_CHANGED".equals(action) || "android.intent.action.TIME_SET".equals(action) || "android.intent.action.DATE_CHANGED".equals(action)) {
                if (this.mSnAon != 0) {
                    z = true;
                } else {
                    z = false;
                }
                scheduleNextCheckNightOffAlarm(z, true);
                updateBreathing(false, false);
                return true;
            } else if ("com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD".equals(action)) {
                if (this.mSnPrivacy == 0) {
                    return true;
                }
                updateBreathing(false, false);
                return true;
            } else if ("com.motorola.aon.CHECK_NIGHT_OFF".equals(action)) {
                scheduleNextCheckNightOffAlarm(true, true);
                updateBreathing(false, true);
                return true;
            } else if ("com.motorola.internal.policy.statusbar.NOTIFICATION_VIEW_FULLY_VISIBLE".equals(action)) {
                this.mAonNotificationManager.enableTracking(false);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onMidNightAlarm() {
        this.mAonViewManager.nextBreathingViewPos();
        updateBreathing(false, false);
    }

    /* access modifiers changed from: protected */
    public void onAonInterfaceConntected() {
        super.onAonInterfaceConntected();
        try {
            this.mAonInterface.registerPartialDisplay(this.mPartialDisplayServer);
            Log.d(TAG, "onAonInterfaceConnected registering PartailDisplayService=" + this.mPartialDisplayServer + " with mAonInterface=" + this.mAonInterface);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "failed to register PartailDisplayService");
        }
    }

    public void onCreate() {
        super.onCreate();
        if (DEBUG) {
            Log.d(TAG, "on create");
        }
        this.mControlRegister = new ControlRegister();
        this.mControlRegister.loadDefault(getApplicationContext());
        android.os.PowerManager powerManager = (android.os.PowerManager) getSystemService("power");
        this.mDPM = (DevicePolicyManager) getSystemService("device_policy");
        this.mLockPatternUtils = new LockPatternUtils(this);
        this.mAonViewManager = new AonViewManagerQuick(getApplicationContext(), this.mAonNotificationManager, this.mIGetAonInterface);
        this.mAonViewManager.initBreathingThread();
        this.mInPartialMode = PersistentManager.mInst.getPartialMode(getApplicationContext()) != 0;
        this.mWakeLock = this.mPowerManager.newWakeLock(1, "aon_updating");
        this.mScreenOn = this.mPowerManager.isScreenOn();
        connectToSHService();
        if (this.mInPartialMode) {
            if (DEBUG) {
                Log.d(TAG, "onCreate: AON service got killed in partial mode");
            }
            if (this.mScreenOn) {
                if (DEBUG) {
                    Log.d(TAG, "onCreate: screen is on, set in partial mode to false");
                }
                this.mInPartialMode = false;
                PersistentManager.mInst.setPartialMode(getApplicationContext(), 0);
            } else {
                updateBreathing(false, true);
            }
        }
        if (!this.mScreenOn) {
            handleScreenOff();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.mAonViewManager.deInitBreathingThread();
        disconnectFromSHService();
    }

    /* access modifiers changed from: protected */
    public AonNotificationManagerQuick getNotificationManager(Context context) {
        return new AonNotificationManagerQuick(context);
    }

    /* access modifiers changed from: protected */
    public void registerBroadcast(IntentFilter ifilter) {
        ifilter.addAction("com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD");
        ifilter.addAction("com.motorola.aon.CHECK_NIGHT_OFF");
        ifilter.addAction("com.motorola.internal.policy.statusbar.NOTIFICATION_VIEW_FULLY_VISIBLE");
    }

    /* access modifiers changed from: protected */
    public void onHdmiPlugged() {
        updateBreathing(false, true);
    }

    /* access modifiers changed from: protected */
    public void onWirelessDisplayMirroring() {
        updateBreathing(false, true);
    }

    /* access modifiers changed from: protected */
    public void onPrivacyModeChange() {
        updateBreathing(false, false);
    }

    /* access modifiers changed from: protected */
    public void onBatteryChange() {
        updateBreathing(false, false);
    }

    /* access modifiers changed from: protected */
    public void onSensorHubPackageAdded() {
        Log.d(TAG, "onSensorHubPackageAdded");
        connectToSHService();
    }

    /* access modifiers changed from: protected */
    public void handleScreenOn() {
        if (DEBUG) {
            Log.d(TAG, "handleScreenOn");
        }
        this.mScreenOn = true;
        if (this.mInPartialMode) {
            InstrumentationRegister ir = new InstrumentationRegister();
            PartialDisplay.nativeReadInstrumentationRegister(ir);
            SnAnalyticsEventService.recordStatusRegister(ir);
            if (DEBUG) {
                Log.d(TAG, "AOD_STAT: " + ir.mStowedEnter + " " + ir.mStowedExit + " " + ir.mStowedTotal + " " + ir.mFacedownEnter + " " + ir.mFacedownExit + " " + ir.mUnstowNoAction + " " + ir.mMMAction + " " + ir.mMMNoAction + " " + ir.mIncomingNoBreath + " " + ir.mTotalBreathsStage1 + " " + ir.mTotalBreathsStage2 + " " + ir.mTotalBreathsStage3 + " " + ir.mTotalBreathsStage4 + " " + ir.mTouchValid + " " + ir.mTouchInvalid + " " + ir.mTouchInvalidRelease + " " + ir.mTotalBreathsAls1 + " " + ir.mTotalBreathsAls2 + " " + ir.mTotalBreathsAls3 + " " + ir.mTotalBreathsAls4 + " " + ir.mTotalBreathsAls5 + " " + ir.mTotalBreathsAls6 + " " + ir.mTotalBreathsAls7 + " " + ir.mTotalBreathsAls8 + " " + ir.mTotalBreathsAls9 + " " + ir.mTotalBreathsAls10 + " " + ir.mTotalTimePeeking + " " + ir.mLateTouchCount + " " + ir.mBreathMovementNormal + " " + ir.mBreathMovementShake + " " + ir.mBreathStowedExit + " " + ir.mBreathFlatDownExit + " " + ir.mBreathFlatUpExit + " " + ir.mBreathMagicSwipe + " " + ir.mBreathNoGesture + " " + ir.mBreathTouchMovementNormal + " " + ir.mBreathTouchMovementShake + " " + ir.mBreathTouchStowedExit + " " + ir.mBreathTouchFlatDownExit + " " + ir.mBreathTouchFlatUpExit + " " + ir.mBreathTouchMagicSwipe + " " + ir.mBreathTouchNoGesture);
            }
            StatusRegister sr = new StatusRegister();
            PartialDisplay.nativeReadStatusRegister(sr);
            if (DEBUG) {
                Log.d(TAG, "read status: mute " + sr.mMute + ", panel " + sr.mPanelState + ", wakeup reason " + sr.mWakeupReason + ", active region " + sr.mActiveRegion + ", brightness " + sr.mBrightness);
            }
            if (sr.mMute != 0) {
                SnAnalyticsEventService.mMuteCount++;
                this.mAonNotificationManager.removeBreathedNotifications();
            }
            if (DEBUG) {
                NotificationEntry ne = this.mAonNotificationManager.getBreathingNotification();
                if (ne != null) {
                    Log.d(TAG, "Breathing ends: " + ne.minedData.pkg + ", " + ne.minedData.id + ", reason: " + sr.mWakeupReason);
                } else {
                    Log.d(TAG, "Breathing ends, reason: " + sr.mWakeupReason);
                }
            }
        }
        SnAnalyticsEventService.recordHandback(System.currentTimeMillis());
        SnAnalyticsEventService.calculateTimeBlock(getApplicationContext());
        if (this.mLastWakeupReason == 2 || this.mLastWakeupReason == 1 || this.mLastWakeupReason == 3) {
            this.mAonNotificationManager.enableTracking(false);
            this.mNotificationReadByAOV = false;
        }
        if (this.mLastWakeupReason == 3) {
            SnAnalyticsEventService.mPowerKeyCount++;
            if (DEBUG) {
                Log.d(TAG, "Power Key Count " + SnAnalyticsEventService.mPowerKeyCount);
            }
        }
        this.mAonViewManager.handleScreenOnAction(this.mLastWakeupReason);
        this.mInPartialMode = false;
        PersistentManager.mInst.setPartialMode(getApplicationContext(), 0);
    }

    /* access modifiers changed from: protected */
    public void handleScreenOff() {
        int i = 1;
        if (DEBUG) {
            Log.d(TAG, "handleScreenOff");
        }
        this.mScreenOn = false;
        this.mAonNotificationManager.enableTracking(true);
        verfiyPartialMode();
        if (this.mInPartialMode) {
            updateBreathing(false, false);
        } else {
            Log.d(TAG, "no in partial mode because " + this.mLastNotBreathing);
        }
        this.mLastWakeupReason = 0;
        PersistentManager persistentManager = PersistentManager.mInst;
        Context applicationContext = getApplicationContext();
        if (!this.mInPartialMode) {
            i = 0;
        }
        persistentManager.setPartialMode(applicationContext, i);
    }

    private boolean isSecureBreathing() {
        if (this.mSnPrivacy == 0 || !this.mKeyguardManager.isKeyguardSecure() || !this.mKeyguardManager.isKeyguardLocked()) {
            return false;
        }
        if (DEBUG) {
            Log.d(TAG, "secure breathing, secure key guard already active");
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void handleUpdateBreathing(boolean incoming) {
        boolean isPeeking;
        int i = 1;
        if (DEBUG) {
            Log.d(TAG, "handleUpdateBreathing: incoming[" + incoming + "]" + "inPartialMode[" + this.mInPartialMode + "]");
        }
        if (!this.mScreenOn) {
            StatusRegister sr = new StatusRegister();
            PartialDisplay.nativeReadStatusRegister(sr);
            if (DEBUG) {
                Log.d(TAG, "Peek status " + sr.mPeek);
            }
            if (sr.mPeek == 1) {
                isPeeking = true;
            } else {
                isPeeking = false;
            }
            if (isPeeking) {
                Handler handler = this.mHandler;
                if (!incoming) {
                    i = 0;
                }
                this.mHandler.sendMessageDelayed(handler.obtainMessage(5, i, 0), 500);
                return;
            }
            doUpdateBreathing(incoming);
        }
        if (this.mWakeLock.isHeld()) {
            this.mWakeLock.release();
        }
    }

    /* access modifiers changed from: private */
    public void connectToSHService() {
        Log.d(TAG, "connectToSHService");
        if (this.mSensorHubService == null) {
            bindService(new Intent("com.motorola.slpc.SensorhubService.Connect"), this.mSensorHubConnection, 1);
        } else if (DEBUG) {
            Log.d(TAG, "Service already bound mSerivce[" + this.mSensorHubService + "]");
        }
    }

    private void disconnectFromSHService() {
        Log.d(TAG, "disconnectFromSHService");
        if (this.mSensorHubService != null) {
            try {
                this.mSensorHubService.removeSensorhubListener(this.mSensorHubListener);
            } catch (RemoteException e) {
                Log.e(TAG, "ERROR removng SH listener - ", e);
            }
            unbindService(this.mSensorHubConnection);
            this.mSensorHubService = null;
        } else if (DEBUG) {
            Log.d(TAG, "Already disconnected from ISensorhubService");
        }
    }

    /* access modifiers changed from: private */
    public void writeRegister(Register register) {
        if (DEBUG) {
            Log.d(TAG, "writeRegister");
        }
        if (this.mSensorHubService == null) {
            Log.d(TAG, "writeRegister - mSensorHubService is NULL.");
        }
        int res = register.write(this.mSensorHubService);
        if (res < 0) {
            this.mLastCriticalError = "Error setting register[" + register.mRegisterID + "]:" + res;
            Log.e(TAG, this.mLastCriticalError);
            this.mInPartialMode = false;
            PersistentManager.mInst.setPartialMode(getApplicationContext(), 0);
        }
    }

    /* access modifiers changed from: protected */
    public void doUpdateBreathing(boolean incoming) {
        boolean bNotificationDisabled;
        int viewType;
        boolean bBreathingClock = true;
        boolean bGestureBased = false;
        Log.d(TAG, "doUpdateBreathing: incoming[" + incoming + "]");
        PartialDisplay.nativeEnableBreathing(false);
        if (this.mInPartialMode) {
            StatusRegister sr = new StatusRegister();
            PartialDisplay.nativeReadStatusRegister(sr);
            if (DEBUG) {
                Log.d(TAG, "read status: mute " + sr.mMute + ", panel " + sr.mPanelState + ", wakeup reason " + sr.mWakeupReason + ", active region " + sr.mActiveRegion + ", brightness " + sr.mBrightness);
            }
            if (sr.mMute != 0) {
                SnAnalyticsEventService.mMuteCount++;
                this.mAonNotificationManager.removeBreathedNotifications();
            }
        }
        ArrayList<NotificationEntry> breathingList = this.mAonNotificationManager.getNotificationListForBreathing();
        if (!this.mScreenOn) {
            Calendar cal = Calendar.getInstance();
            boolean nightOff = isNightOff(cal);
            if (DEBUG && nightOff) {
                Log.d(TAG, "night off");
            }
            if (this.mSnAon == 0 || this.mTutorialState == 0) {
                this.mLastNotBreathing = this.mSnAon == 0 ? "AON is off" : "Device not provisioned";
                if (DEBUG) {
                    Log.d(TAG, this.mLastNotBreathing);
                }
                bNotificationDisabled = true;
                bBreathingClock = false;
            } else if (this.mTutorialState == 2) {
                if (DEBUG) {
                    Log.d(TAG, "show tutorial breathing");
                }
                bNotificationDisabled = false;
            } else {
                bNotificationDisabled = breathingList == null || breathingList.size() <= 0;
                bGestureBased = nightOff || bNotificationDisabled || this.mNotificationReadByAOV;
                if (DEBUG) {
                    Log.d(TAG, "Notification has been read out by AOV : " + this.mNotificationReadByAOV);
                }
            }
            if (bGestureBased) {
                onAodActivated();
            } else if (!bNotificationDisabled) {
                onAodActivated();
                if (incoming) {
                    onIncomingNotification();
                }
            }
            if (DEBUG) {
                if (bGestureBased) {
                    Log.d(TAG, "going to partial display with gesture mode");
                    if (bNotificationDisabled) {
                        Log.d(TAG, "clock only, no notification");
                    } else {
                        Log.d(TAG, "clock and notification");
                    }
                } else if (!bNotificationDisabled) {
                    Log.d(TAG, "going to partial display with breathing");
                    NotificationEntry ne = null;
                    if (breathingList != null && breathingList.size() > 0) {
                        ne = (NotificationEntry) breathingList.get(0);
                    }
                    if (incoming) {
                        if (ne != null) {
                            Log.d(TAG, "The Incoming Notification Screen appeared: " + ne.minedData.pkg + ", " + ne.minedData.id);
                        } else {
                            Log.d(TAG, "The Incoming Notification Screen appeared.");
                        }
                    } else if (ne != null) {
                        Log.d(TAG, "Breathing starts for: " + ne.minedData.pkg + ", " + ne.minedData.id);
                    } else {
                        Log.d(TAG, "Breathing starts.");
                    }
                } else {
                    Log.d(TAG, "going to partial display without showing anything");
                }
            }
            int privacy = isSecureBreathing() ? 1 : 0;
            if (this.mTutorialState == 2) {
                viewType = 2;
            } else if (privacy != 0) {
                viewType = 1;
            } else {
                viewType = 0;
            }
            Object waitDrawn = new Object();
            synchronized (waitDrawn) {
                if (DEBUG) {
                    Log.d(TAG, "begin post breathing view");
                }
                long start = System.currentTimeMillis();
                AonViewManagerQuick aonViewManagerQuick = this.mAonViewManager;
                int i = this.mBatteryStatus;
                final Object obj = waitDrawn;
                AnonymousClass4 r0 = new ICallback() {
                    public void sendResult(Bundle data) {
                        boolean z = true;
                        synchronized (obj) {
                            if (data != null) {
                                int[] offset = new int[2];
                                AonServiceQuick.this.mAonViewManager.getBreathingOffset(data.getInt("pos", -1), offset);
                                ControlRegister access$1600 = AonServiceQuick.this.mControlRegister;
                                int i = offset[0];
                                int i2 = offset[1];
                                if (AonServiceQuick.this.mBatteryStatus == 0) {
                                    z = false;
                                }
                                access$1600.setBreathingViewPostion(i, i2, z);
                            } else {
                                Log.w(AonServiceQuick.TAG, "null data from UI thread after post view");
                            }
                            obj.notifyAll();
                        }
                    }
                };
                aonViewManagerQuick.requestUpdateBreathingView(viewType, breathingList, i, r0);
                try {
                    waitDrawn.wait(500);
                } catch (InterruptedException e) {
                    Log.w(TAG, "InterruptedException in waiting drawing brathing view");
                }
                SnAnalyticsEventService.recordDrawTime(System.currentTimeMillis() - start);
            }
            if (DEBUG) {
                Log.d(TAG, "end post breathing view");
            }
            boolean is24 = DateFormat.is24HourFormat(this);
            this.mControlRegister.setNotificationDisable(bNotificationDisabled);
            this.mControlRegister.setGestureOnlyMode(bGestureBased);
            this.mControlRegister.setClockEnable(bBreathingClock);
            this.mControlRegister.setClockFormat(is24 ? 1 : 0);
            this.mControlRegister.setBrightness(this.mBrightness);
            this.mControlRegister.setIncomingPlateauTime(incoming);
            this.mControlRegister.disableHaptics(!this.mbHapticsEnabled);
            this.mControlRegister.loadPanelInfo();
            if (DEBUG) {
                Log.d(TAG, "SetControlReg: notiDisabled=" + bNotificationDisabled + " geasture=" + bGestureBased + " clockBreathing=" + bBreathingClock + " clock24=" + is24 + " incoming=" + incoming + " disableHaptics=" + (!this.mbHapticsEnabled));
            }
            long tzOffset = 0;
            if (cal != null) {
                TimeZone tz = cal.getTimeZone();
                if (tz != null) {
                    tzOffset = (long) tz.getOffset(System.currentTimeMillis());
                }
            }
            SnAnalyticsEventService.recordHandover(System.currentTimeMillis());
            SnAnalyticsEventService.isClockOnlyBreathing(bNotificationDisabled);
            PartialDisplay.setCurrentTime(tzOffset);
            writeRegister(this.mControlRegister);
            PartialDisplay.nativeEnableBreathing(true);
            if (SnAnalyticsEventService.isIncomingRefreshed() && SnAnalyticsEventService.getIncomingTime() != 0) {
                SnAnalyticsEventService.recordIncomingTime(System.currentTimeMillis() - SnAnalyticsEventService.getIncomingTime());
                SnAnalyticsEventService.setIncomingTime(0);
                SnAnalyticsEventService.setIncomingRefresh(false);
            }
        } else if (DEBUG) {
            Log.d(TAG, "no need to activate partial display");
        }
        if (this.mInPartialMode && breathingList != null) {
            Iterator i$ = breathingList.iterator();
            while (i$.hasNext()) {
                ((NotificationEntry) i$.next()).breathing = true;
            }
            ArrayList<NotificationEntry> newBreathingList = this.mAonNotificationManager.getNotificationListForBreathing();
            if (newBreathingList != null) {
                Iterator i$2 = newBreathingList.iterator();
                while (i$2.hasNext()) {
                    if (!((NotificationEntry) i$2.next()).breathing) {
                        if (DEBUG) {
                            Log.d(TAG, "got new notification to breathe during display suspension");
                        }
                        Handler handler = this.mHandler;
                        AnonymousClass5 r02 = new Runnable() {
                            public void run() {
                                AonServiceQuick.this.updateBreathing(false, false);
                            }
                        };
                        handler.post(r02);
                        return;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onHapticsSettingsChanged() {
        if (DEBUG) {
            Log.d(TAG, "Haptics settings changed: New Value : " + this.mbHapticsEnabled);
        }
        if (this.mControlRegister != null) {
            this.mControlRegister.disableHaptics(!this.mbHapticsEnabled);
        }
    }

    /* access modifiers changed from: protected */
    public void onNotificationReadByAOV(long timeCompleted, HashMap<String, ArrayList<String>> hashMap) {
        if (DEBUG) {
            Log.d(TAG, "Notification has been read out by AOV");
        }
        updateBreathing(false, true);
    }

    private void verfiyPartialMode() {
        if (DEBUG) {
            Log.d(TAG, "verfiyPartialMode()");
        }
        this.mInPartialMode = true;
        if (this.mAonInterface == null || this.mIAonNL == null || this.mSensorHubService == null) {
            Log.w(TAG, "verfiyPartialMode mAonInterface=" + this.mAonInterface + " mIAonNL=" + this.mIAonNL + " mSensorHubService=" + this.mSensorHubService);
            this.mInPartialMode = false;
        } else if (this.mTelephonyManager.getCallState() != 0) {
            if (DEBUG) {
                Log.d(TAG, "verfiyPartialMode in calling");
            }
            this.mLastNotBreathing = "in calling";
            this.mInPartialMode = false;
            this.mTelephonyManager.listen(this.mPhonteStateListener, 32);
        } else if (PowerManager.proximityTargetDetected(this.mPowerManager)) {
            if (DEBUG) {
                Log.d(TAG, "verfiyPartialMode proximityTargetDetected() returns TRUE, suppress breathing");
            }
            this.mInPartialMode = false;
            this.mLastNotBreathing = "proximityTargetDetected";
        } else if (this.mHdmiPlugged) {
            if (DEBUG) {
                Log.d(TAG, "verfiyPartialMode HDMI connected");
            }
            this.mInPartialMode = false;
            this.mLastNotBreathing = "HDMI connected";
        } else if (this.mCoolDownMode == 1) {
            if (DEBUG) {
                Log.d(TAG, "verfiyPartialMode in cool down");
            }
            this.mInPartialMode = false;
            this.mLastNotBreathing = "in cool down";
        }
        if (DEBUG) {
            Log.d(TAG, "verfiyPartialMode mInPartialMode : " + this.mInPartialMode + ", mLastNotBreathing : " + this.mLastNotBreathing);
        }
    }
}
