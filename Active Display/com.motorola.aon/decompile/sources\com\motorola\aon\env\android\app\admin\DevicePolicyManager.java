package com.motorola.aon.env.android.app.admin;

import android.util.Log;

public class DevicePolicyManager {
    public static String ACTION_DEVICE_POLICY_MANAGER_STATE_CHANGED = null;
    private static final String CLASS_DEVICE_POLICY_MANAGER = "android.app.admin.DevicePolicyManager";
    private static final String FIELD_STATE_CHANGED = "ACTION_DEVICE_POLICY_MANAGER_STATE_CHANGED";
    private static final String TAG = "Aon_DevicePolicyManager";
    public static boolean sInitialized;

    static {
        ACTION_DEVICE_POLICY_MANAGER_STATE_CHANGED = null;
        sInitialized = false;
        try {
            ACTION_DEVICE_POLICY_MANAGER_STATE_CHANGED = (String) Class.forName(CLASS_DEVICE_POLICY_MANAGER).getDeclaredField(FIELD_STATE_CHANGED).get(null);
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize class DevicePolicyManager");
        }
    }
}
