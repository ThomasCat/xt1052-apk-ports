package com.motorola.aon.settings;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.motorola.aon.Constants;
import com.motorola.aon.R;
import com.motorola.aon.env.EnvChecker;

public class BreathingDetailsActivity extends Activity {
    private static final String COLOR_BLUE = "#33B5E5";
    /* access modifiers changed from: private */
    public static final boolean DEBUG = Constants.DEBUG;
    private static final String TAG = "Aon_BreathingDetailsAct";

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        if (!Constants.isPartialDisplay) {
            setContentView(R.layout.breathing_details_fd);
        } else {
            setContentView(R.layout.breathing_details);
        }
        ActionBar ab = getActionBar();
        if (ab != null) {
            ab.setIcon(EnvChecker.getActionBarIcon());
            ab.setTitle(R.string.learn_more_aon_title);
            ab.setDisplayHomeAsUpEnabled(true);
        }
        setSelectableText();
    }

    public boolean onNavigateUp() {
        finish();
        return true;
    }

    private void setSelectableText() {
        if (DEBUG) {
            Log.d(TAG, "Updating tutorial text for selectable settings.");
        }
        TextView tvSettings = (TextView) findViewById(R.id.whiteListSetting);
        if (tvSettings == null) {
            Log.e(TAG, "Failed to get text view!");
            return;
        }
        Resources res = getResources();
        if (res == null) {
            Log.e(TAG, "Failed to get Resources object!");
            return;
        }
        String settings = res.getString(R.string.settings);
        String detail = res.getString(R.string.aod_t5_summary_selectable, new Object[]{settings});
        if (detail == null || settings == null) {
            Log.e(TAG, "Failed to get selectable tutorial string!");
            return;
        }
        int start = detail.indexOf(settings);
        if (start == -1) {
            Log.e(TAG, "Failed to get spannable index!");
            return;
        }
        int end = start + settings.length();
        if (DEBUG) {
            Log.d(TAG, "Creating selectable text from index " + start + " to " + end);
        }
        SpannableString spannable = new SpannableString(detail);
        spannable.setSpan(new ClickableSpan() {
            public void onClick(View textView) {
                if (BreathingDetailsActivity.DEBUG) {
                    Log.d(BreathingDetailsActivity.TAG, "Settings clicked from learn more tutorial");
                }
                Intent i = new Intent(BreathingDetailsActivity.this, WhiteListActivity.class);
                i.putExtra("from_tutorial", 1);
                BreathingDetailsActivity.this.startActivity(i);
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(Color.parseColor(BreathingDetailsActivity.COLOR_BLUE));
            }
        }, start, end, 33);
        tvSettings.setText(spannable);
        tvSettings.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
