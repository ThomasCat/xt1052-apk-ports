package com.motorola.aon.env.com.android.internal;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.util.Log;

public class R {
    private static final String PKG_ANDROID = "android";
    private static final String TAG = "Aon_R";
    private static R sInstance;
    public ID id;
    public Integer integer;
    public Layout layout;

    public static class ID {
        public final int big_picture;
        public final int big_text;
        public final int inbox_more;
        public final int inbox_text0;
        public final int inbox_text1;
        public final int inbox_text2;
        public final int inbox_text3;
        public final int inbox_text4;
        public final int inbox_text5;
        public final int inbox_text6;
        public final int info;
        public final int text;
        public final int text2;
        public final int time;
        public final int title;

        ID(Resources res) {
            this.title = res.getIdentifier("title", "id", R.PKG_ANDROID);
            this.text = res.getIdentifier("text", "id", R.PKG_ANDROID);
            this.text2 = res.getIdentifier("text2", "id", R.PKG_ANDROID);
            this.time = res.getIdentifier("time", "id", R.PKG_ANDROID);
            this.info = res.getIdentifier("info", "id", R.PKG_ANDROID);
            this.big_text = res.getIdentifier("big_text", "id", R.PKG_ANDROID);
            this.big_picture = res.getIdentifier("big_picture", "id", R.PKG_ANDROID);
            this.inbox_text0 = res.getIdentifier("inbox_text0", "id", R.PKG_ANDROID);
            this.inbox_text1 = res.getIdentifier("inbox_text1", "id", R.PKG_ANDROID);
            this.inbox_text2 = res.getIdentifier("inbox_text2", "id", R.PKG_ANDROID);
            this.inbox_text3 = res.getIdentifier("inbox_text3", "id", R.PKG_ANDROID);
            this.inbox_text4 = res.getIdentifier("inbox_text4", "id", R.PKG_ANDROID);
            this.inbox_text5 = res.getIdentifier("inbox_text5", "id", R.PKG_ANDROID);
            this.inbox_text6 = res.getIdentifier("inbox_text6", "id", R.PKG_ANDROID);
            this.inbox_more = res.getIdentifier("inbox_more", "id", R.PKG_ANDROID);
        }
    }

    public static class Integer {
        public int config_criticalBatteryWarningLevel;
        public int config_lowBatteryWarningLevel;

        Integer(Resources res) {
            this.config_criticalBatteryWarningLevel = res.getIdentifier("config_criticalBatteryWarningLevel", "integer", R.PKG_ANDROID);
            this.config_lowBatteryWarningLevel = res.getIdentifier("config_lowBatteryWarningLevel", "integer", R.PKG_ANDROID);
        }
    }

    public static class Layout {
        public final int notification_template_base;
        public final int notification_template_big_base;
        public final int notification_template_big_picture;
        public final int notification_template_big_text;
        public final int notification_template_inbox;

        Layout(Resources res) {
            this.notification_template_base = res.getIdentifier("notification_template_base", "layout", R.PKG_ANDROID);
            this.notification_template_big_text = res.getIdentifier("notification_template_big_text", "layout", R.PKG_ANDROID);
            this.notification_template_big_picture = res.getIdentifier("notification_template_big_picture", "layout", R.PKG_ANDROID);
            this.notification_template_inbox = res.getIdentifier("notification_template_inbox", "layout", R.PKG_ANDROID);
            this.notification_template_big_base = res.getIdentifier("notification_template_big_base", "layout", R.PKG_ANDROID);
        }
    }

    public static R getInstance(Context c) {
        if (sInstance == null) {
            sInstance = new R(c);
        }
        return sInstance;
    }

    private R(Context c) {
        Resources res = null;
        try {
            Context context = c.createPackageContext(PKG_ANDROID, 0);
            if (context != null) {
                res = context.getResources();
            } else {
                Log.w(TAG, "can not create context for package android");
            }
        } catch (NameNotFoundException e) {
            Log.w(TAG, "can not find resource " + e.getMessage());
        }
        if (res != null) {
            this.layout = new Layout(res);
            this.id = new ID(res);
            this.integer = new Integer(res);
        }
    }
}
