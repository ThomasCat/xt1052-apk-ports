package com.motorola.aon.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IAon extends IInterface {

    public static abstract class Stub extends Binder implements IAon {
        private static final String DESCRIPTOR = "com.motorola.aon.aidl.IAon";
        static final int TRANSACTION_getNotificationsByPriority = 1;
        static final int TRANSACTION_getNotificationsByTime = 2;

        private static class Proxy implements IAon {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return "com.motorola.aon.aidl.IAon";
            }

            public void getNotificationsByPriority(int maxEntries, List<ParsedInfo> notifications) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.motorola.aon.aidl.IAon");
                    _data.writeInt(maxEntries);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                    _reply.readTypedList(notifications, ParsedInfo.CREATOR);
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void getNotificationsByTime(int maxEntries, List<ParsedInfo> notifications) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.motorola.aon.aidl.IAon");
                    _data.writeInt(maxEntries);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                    _reply.readTypedList(notifications, ParsedInfo.CREATOR);
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.motorola.aon.aidl.IAon");
        }

        public static IAon asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("com.motorola.aon.aidl.IAon");
            if (iin == null || !(iin instanceof IAon)) {
                return new Proxy(obj);
            }
            return (IAon) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface("com.motorola.aon.aidl.IAon");
                    int _arg0 = data.readInt();
                    List<ParsedInfo> _arg1 = new ArrayList<>();
                    getNotificationsByPriority(_arg0, _arg1);
                    reply.writeNoException();
                    reply.writeTypedList(_arg1);
                    return true;
                case 2:
                    data.enforceInterface("com.motorola.aon.aidl.IAon");
                    int _arg02 = data.readInt();
                    List<ParsedInfo> _arg12 = new ArrayList<>();
                    getNotificationsByTime(_arg02, _arg12);
                    reply.writeNoException();
                    reply.writeTypedList(_arg12);
                    return true;
                case 1598968902:
                    reply.writeString("com.motorola.aon.aidl.IAon");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void getNotificationsByPriority(int i, List<ParsedInfo> list) throws RemoteException;

    void getNotificationsByTime(int i, List<ParsedInfo> list) throws RemoteException;
}
