package com.motorola.aon.qp;

import android.content.Context;
import android.content.Intent;
import com.motorola.aon.AonNotificationManager;
import com.motorola.aon.AonService;
import com.motorola.aon.SnAnalyticsEventService;

public class AonNotificationManagerQuick extends AonNotificationManager {
    protected static final boolean DEBUG = AonService.DEBUG;
    static final String TAG = "AonNotificationManagerQuick";

    public AonNotificationManagerQuick(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void notifyRefreshBreathingViewOnNotification() {
        SnAnalyticsEventService.mIncomingALS++;
        Intent intent = new Intent(this.mContext, AonServiceQuick.class);
        intent.setAction(AonService.ACTION_REFRESH_BREATHING);
        intent.putExtra("incoming", true);
        this.mContext.startService(intent);
    }

    /* access modifiers changed from: protected */
    public void notifyLaunchTutorialOnNotification() {
        Intent intent = new Intent(this.mContext, AonServiceQuick.class);
        intent.setAction(AonService.ACTION_REFRESH_BREATHING);
        intent.putExtra("incoming", false);
        this.mContext.startService(intent);
    }
}
