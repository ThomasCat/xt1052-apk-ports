package com.motorola.aon.env.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPartialDisplayService extends IInterface {

    public static abstract class Stub extends Binder implements IPartialDisplayService {
        private static final String DESCRIPTOR = "com.motorola.aon.env.aidl.IPartialDisplayService";
        static final int TRANSACTION_fbWillBeResumed = 1;
        static final int TRANSACTION_fbWillBeSuspended = 2;
        static final int TRANSACTION_isPartialDisplayActivated = 3;
        static final int TRANSACTION_notifyPowerKeyWakeup = 4;
        static final int TRANSACTION_onESD = 5;
        static final int TRANSACTION_onTouch = 6;

        private static class Proxy implements IPartialDisplayService {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public void fbWillBeResumed(IRemoteCallback listener, int reason) throws RemoteException {
                IBinder iBinder = null;
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (listener != null) {
                        iBinder = listener.asBinder();
                    }
                    _data.writeStrongBinder(iBinder);
                    _data.writeInt(reason);
                    this.mRemote.transact(1, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void fbWillBeSuspended(IRemoteCallback listener, int reason) throws RemoteException {
                IBinder iBinder = null;
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (listener != null) {
                        iBinder = listener.asBinder();
                    }
                    _data.writeStrongBinder(iBinder);
                    _data.writeInt(reason);
                    this.mRemote.transact(2, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public boolean isPartialDisplayActivated() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void notifyPowerKeyWakeup() throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(4, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void onESD() throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(5, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void onTouch() throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(6, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IPartialDisplayService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof IPartialDisplayService)) {
                return new Proxy(obj);
            }
            return (IPartialDisplayService) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface(DESCRIPTOR);
                    fbWillBeResumed(com.motorola.aon.env.aidl.IRemoteCallback.Stub.asInterface(data.readStrongBinder()), data.readInt());
                    return true;
                case 2:
                    data.enforceInterface(DESCRIPTOR);
                    fbWillBeSuspended(com.motorola.aon.env.aidl.IRemoteCallback.Stub.asInterface(data.readStrongBinder()), data.readInt());
                    return true;
                case 3:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result = isPartialDisplayActivated();
                    reply.writeNoException();
                    reply.writeInt(_result ? 1 : 0);
                    return true;
                case 4:
                    data.enforceInterface(DESCRIPTOR);
                    notifyPowerKeyWakeup();
                    return true;
                case 5:
                    data.enforceInterface(DESCRIPTOR);
                    onESD();
                    return true;
                case 6:
                    data.enforceInterface(DESCRIPTOR);
                    onTouch();
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void fbWillBeResumed(IRemoteCallback iRemoteCallback, int i) throws RemoteException;

    void fbWillBeSuspended(IRemoteCallback iRemoteCallback, int i) throws RemoteException;

    boolean isPartialDisplayActivated() throws RemoteException;

    void notifyPowerKeyWakeup() throws RemoteException;

    void onESD() throws RemoteException;

    void onTouch() throws RemoteException;
}
