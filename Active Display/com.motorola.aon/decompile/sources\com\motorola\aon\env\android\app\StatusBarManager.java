package com.motorola.aon.env.android.app;

import android.util.Log;

public class StatusBarManager {
    static final String CLASS_STATUS_BAR_MANAGER = "android.app.StatusBarManager";
    public static int DISABLE_BACK = 0;
    public static int DISABLE_HOME = 0;
    public static int DISABLE_NONE = 0;
    public static int DISABLE_RECENT = 0;
    public static int DISABLE_SEARCH = 0;
    static final String FIELD_DISABLE_HOME = "DISABLE_HOME";
    static final String FIELD_DISABLE_NONE = "DISABLE_NONE";
    static final String FIELD_DISABLE_RECENT = "DISABLE_RECENT";
    static final String FIELD_DISABLE_SEARCH = "DISABLE_SEARCH";
    static final String FIELD_DISALBE_BACK = "DISABLE_BACK";
    private static final String TAG = "Aon_StatusBarManager";
    public static boolean sInitialized;

    static {
        DISABLE_BACK = 0;
        DISABLE_HOME = 0;
        DISABLE_RECENT = 0;
        DISABLE_SEARCH = 0;
        DISABLE_NONE = 0;
        sInitialized = false;
        try {
            Class<?> clazz = Class.forName(CLASS_STATUS_BAR_MANAGER);
            DISABLE_BACK = clazz.getDeclaredField(FIELD_DISALBE_BACK).getInt(null);
            DISABLE_HOME = clazz.getDeclaredField(FIELD_DISABLE_HOME).getInt(null);
            DISABLE_RECENT = clazz.getDeclaredField(FIELD_DISABLE_RECENT).getInt(null);
            DISABLE_SEARCH = clazz.getDeclaredField(FIELD_DISABLE_SEARCH).getInt(null);
            DISABLE_NONE = clazz.getDeclaredField(FIELD_DISABLE_NONE).getInt(null);
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize class");
        }
    }
}
