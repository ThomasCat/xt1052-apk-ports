package com.motorola.aon.settings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.motorola.aon.Constants;
import com.motorola.aon.R;

public class AodTutorialActivity extends Activity implements OnClickListener, OnPageChangeListener {
    private static final boolean DEBUG = true;
    private static final int DIALOG_OK_CANCEL_MESSAGE = 1;
    private static final String PKG_DEFAULT_WHITE_LIST = "com.motorola.android.providers.settings";
    private static final String RES_NAME_DEFAULT_WHITE_LIST = "sn_white_list";
    private static final String RES_TYPE_DEFAULT_WHITE_LIST = "array";
    private static final String TAG = "AodTutorialActivity";
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    int[] mViewPages = new int[5];

    private class HelpPagerAdapter extends PagerAdapter {
        private HelpPagerAdapter() {
        }

        public int getCount() {
            return AodTutorialActivity.this.mViewPages.length;
        }

        public boolean isViewFromObject(View v, Object o) {
            if (v == ((View) o)) {
                return AodTutorialActivity.DEBUG;
            }
            return false;
        }

        public Object instantiateItem(ViewGroup container, int pos) {
            if (pos < 0 || pos >= AodTutorialActivity.this.mViewPages.length) {
                return null;
            }
            View page = View.inflate(AodTutorialActivity.this.getApplicationContext(), AodTutorialActivity.this.mViewPages[pos], null);
            container.addView(page);
            return page;
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        if (!Constants.isPartialDisplay) {
            this.mViewPages[0] = R.layout.aod_fd_tutorial1;
        } else {
            this.mViewPages[0] = R.layout.aod_tutorial1;
        }
        this.mViewPages[1] = R.layout.aod_tutorial2;
        this.mViewPages[2] = R.layout.aod_tutorial3;
        this.mViewPages[3] = R.layout.aod_tutorial4;
        this.mViewPages[4] = R.layout.aod_tutorial5;
        setContentView(R.layout.aod_tutorial);
        this.mPager = (ViewPager) findViewById(R.id.view_pager);
        this.mPagerAdapter = new HelpPagerAdapter();
        this.mPager.setAdapter(this.mPagerAdapter);
        this.mPager.setOnPageChangeListener(this);
        TextView settings = (TextView) findViewById(R.id.go_white_list);
        if (icicle != null) {
            int pos = icicle.getInt("pos", 0);
            this.mPager.setCurrentItem(pos);
            setNextButtonText((TextView) findViewById(R.id.textNext), pos);
            setSettingsButtonVisibility(settings, pos);
        } else {
            setSettingsButtonVisibility(settings, 0);
        }
        settings.setOnClickListener(this);
        findViewById(R.id.textNext).setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("pos", this.mPager.getCurrentItem());
    }

    public void onBackPressed() {
        if (this.mPager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            this.mPager.setCurrentItem(this.mPager.getCurrentItem() - 1);
        }
    }

    public void onClick(View v) {
        if (v != null) {
            switch (v.getId()) {
                case R.id.go_white_list /*2131623939*/:
                    Intent i = new Intent(this, WhiteListActivity.class);
                    i.putExtra("from_tutorial", 1);
                    startActivity(i);
                    return;
                case R.id.textNext /*2131623940*/:
                    int pos = this.mPager.getCurrentItem();
                    if (pos < this.mViewPages.length - 1) {
                        this.mPager.setCurrentItem(pos + 1);
                        setNextButtonText((TextView) v, pos + 1);
                        return;
                    }
                    finish();
                    return;
                default:
                    return;
            }
        }
    }

    private void setNextButtonText(TextView tv, int pos) {
        if (tv == null) {
            return;
        }
        if (pos < this.mViewPages.length - 1) {
            tv.setText(R.string.next);
        } else {
            tv.setText(R.string.done);
        }
    }

    private void setSettingsButtonVisibility(TextView tv, int pos) {
        if (tv == null) {
            return;
        }
        if (pos == this.mViewPages.length - 1) {
            tv.setVisibility(0);
        } else {
            tv.setVisibility(4);
        }
    }

    public void onPageScrollStateChanged(int pos) {
    }

    public void onPageScrolled(int arg0, float arg1, int arg2) {
    }

    public void onPageSelected(int pos) {
        setNextButtonText((TextView) findViewById(R.id.textNext), pos);
        setSettingsButtonVisibility((TextView) findViewById(R.id.go_white_list), pos);
    }
}
