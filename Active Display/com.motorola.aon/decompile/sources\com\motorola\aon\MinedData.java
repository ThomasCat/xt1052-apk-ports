package com.motorola.aon;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.service.notification.StatusBarNotification;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.motorola.aon.env.com.android.internal.R;
import java.util.ArrayList;
import java.util.Date;

public class MinedData {
    private static final boolean DEBUG = Constants.DEBUG;
    static final int PHONE_VOICEMAIL_NOTIFICATION = 5;
    private static final String TAG = "Aon_MinedData";
    private static R internalR;
    private static Handler mMainHandler;
    public int bigStyle;
    public PendingIntent contentIntent;
    public int flags;
    private boolean hasBigContent;
    public int icon;
    public Bitmap iconBitmap;
    public int id;
    public Bitmap largeIcon;
    public Bundle mExtra = new Bundle();
    public String pkg;
    public int score;
    public int smallStyle;
    public String tag;
    public CharSequence tickerText;
    public long timeStamp;
    public String when;

    static class MineCustomRunnable implements Runnable {
        final Context mContext;
        final Bundle mExtra;
        final Notification mNotification;
        final Object mSync;

        MineCustomRunnable(Context context, Bundle extra, Notification notification) {
            this.mContext = context;
            this.mExtra = extra;
            this.mNotification = notification;
            this.mSync = null;
        }

        MineCustomRunnable(Context context, Bundle extra, Notification notification, Object sync) {
            this.mContext = context;
            this.mExtra = extra;
            this.mNotification = notification;
            this.mSync = sync;
        }

        public void run() {
            View view = null;
            if (this.mNotification.bigContentView != null) {
                view = this.mNotification.bigContentView.apply(this.mContext, null);
            } else if (this.mNotification.contentView != null) {
                view = this.mNotification.contentView.apply(this.mContext, null);
            }
            if (view != null && (view instanceof ViewGroup)) {
                ArrayList<CharSequence> lstText = new ArrayList<>();
                traverseView((ViewGroup) view, lstText);
                int size = lstText.size();
                if (size > 0) {
                    this.mExtra.putCharSequence("android.title", (CharSequence) lstText.get(0));
                }
                if (size > 1) {
                    this.mExtra.putCharSequence("android.text", (CharSequence) lstText.get(1));
                }
                if (size > 2) {
                    this.mExtra.putCharSequence("android.subText", (CharSequence) lstText.get(2));
                }
            }
            if (this.mSync != null) {
                synchronized (this.mSync) {
                    this.mSync.notify();
                }
            }
        }

        private void traverseView(ViewGroup vg, ArrayList<CharSequence> lstText) {
            int size = vg.getChildCount();
            for (int i = 0; i < size; i++) {
                View v = vg.getChildAt(i);
                if (v.getVisibility() == 0) {
                    if (v instanceof ViewGroup) {
                        traverseView((ViewGroup) v, lstText);
                    } else if (v instanceof TextView) {
                        lstText.add(((TextView) v).getText());
                        if (lstText.size() > 3) {
                            return;
                        }
                    } else {
                        continue;
                    }
                }
            }
        }
    }

    private MinedData() {
    }

    public static MinedData fromStatusBarNotification(StatusBarNotification sbn, Context context) {
        MinedData minedData = null;
        if (!(sbn == null || sbn.getNotification() == null)) {
            Notification notification = sbn.getNotification();
            if ((((notification.flags & 2) == 0 && (notification.flags & 32) == 0) || (("com.android.phone".equals(sbn.getPackageName()) || "com.android.dialer".equals(sbn.getPackageName())) && sbn.getId() == 5)) && notification.priority >= 0) {
                SnAnalyticsEventService.recordMinedNotification();
                minedData = new MinedData();
                if (notification.bigContentView != null) {
                    minedData.bigStyle = notification.bigContentView.getLayoutId();
                    minedData.hasBigContent = true;
                } else {
                    minedData.hasBigContent = false;
                }
                if (notification.contentView != null) {
                    minedData.smallStyle = notification.contentView.getLayoutId();
                }
                minedData.pkg = sbn.getPackageName();
                minedData.tag = sbn.getTag();
                minedData.id = sbn.getId();
                minedData.timeStamp = sbn.getPostTime();
                minedData.score = notification.priority;
                minedData.contentIntent = notification.contentIntent;
                minedData.when = timeToString(context, notification.when);
                minedData.icon = notification.icon;
                minedData.iconBitmap = fillNotificationIcon(context, minedData.pkg, notification.icon, notification.iconLevel);
                minedData.tickerText = notification.tickerText;
                minedData.largeIcon = notification.largeIcon;
                boolean customNotification = false;
                internalR = R.getInstance(context);
                if (notification.bigContentView != null) {
                    int bigLayoutId = notification.bigContentView.getLayoutId();
                    if (!(bigLayoutId == internalR.layout.notification_template_big_base || bigLayoutId == internalR.layout.notification_template_big_text || bigLayoutId == internalR.layout.notification_template_big_picture || bigLayoutId == internalR.layout.notification_template_inbox)) {
                        customNotification = true;
                        if (DEBUG) {
                            Log.d(TAG, "notification with custom big style");
                        }
                    }
                } else if (!(notification.contentView == null || notification.contentView.getLayoutId() == internalR.layout.notification_template_base)) {
                    customNotification = true;
                    if (DEBUG) {
                        Log.d(TAG, "notification with custom small style");
                    }
                }
                if (customNotification || notification.extras == null || notification.extras.isEmpty()) {
                    SnAnalyticsEventService.recordCustomNotifications();
                    getTextFromCustomNotification(context, minedData.mExtra, notification);
                } else {
                    minedData.mExtra.putAll(notification.extras);
                }
            }
        }
        return minedData;
    }

    private static void getTextFromCustomNotification(Context context, Bundle extra, Notification notification) {
        if (Looper.myLooper() != null) {
            if (DEBUG) {
                Log.d(TAG, "mine in current thread");
            }
            if (DEBUG) {
                Log.d(TAG, "before mine custom notification");
            }
            new MineCustomRunnable(context, extra, notification).run();
            if (DEBUG) {
                Log.d(TAG, "After mine custom notification");
            }
        } else if (mMainHandler != null) {
            Object sync = new Object();
            mMainHandler.post(new MineCustomRunnable(context, extra, notification, sync));
            synchronized (sync) {
                try {
                    if (DEBUG) {
                        Log.d(TAG, "before mine custom notification");
                    }
                    sync.wait(1000);
                    if (DEBUG) {
                        Log.d(TAG, "After mine custom notification");
                    }
                } catch (InterruptedException e) {
                }
            }
        }
    }

    private static String timeToString(Context context, long when2) {
        if (when2 <= 0) {
            return null;
        }
        Time t = new Time();
        t.set(when2);
        t.second = 0;
        return DateFormat.getTimeFormat(context).format(new Date(t.year - 1900, t.month, t.monthDay, t.hour, t.minute, 0));
    }

    private static Bitmap fillNotificationIcon(Context context, String pkg2, int icon2, int iconLevel) {
        Resources r;
        if (icon2 == 0) {
            return null;
        }
        if (pkg2 != null) {
            try {
                r = context.getPackageManager().getResourcesForApplication(pkg2);
            } catch (NameNotFoundException e) {
                return null;
            }
        } else {
            r = context.getResources();
        }
        try {
            Drawable d = r.getDrawable(icon2);
            if (d instanceof BitmapDrawable) {
                return ((BitmapDrawable) d).getBitmap();
            }
            if (d instanceof LevelListDrawable) {
                return drawableToBitmap(d, iconLevel);
            }
            return null;
        } catch (RuntimeException e2) {
            Log.d(TAG, "Icon not found in " + (pkg2 != null ? Integer.valueOf(icon2) : "<system>") + ": " + Integer.toHexString(icon2));
            return null;
        }
    }

    private static Bitmap drawableToBitmap(Drawable d, int iconLevel) {
        if (d.getIntrinsicWidth() <= 0 || d.getIntrinsicHeight() <= 0) {
            return null;
        }
        if (iconLevel > 0) {
            d.setLevel(iconLevel);
        }
        Bitmap bitmap = Bitmap.createBitmap(d.getIntrinsicWidth(), d.getIntrinsicHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        d.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        d.draw(canvas);
        return bitmap;
    }

    public boolean hasBigContent() {
        return this.hasBigContent;
    }

    public static void setMainHandler(Handler handler) {
        mMainHandler = handler;
    }

    public String toString() {
        return this.pkg + "/" + this.id;
    }
}
