package com.motorola.aon.settings;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy.Builder;
import android.os.StrictMode.VmPolicy;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TimePicker;
import com.motorola.aon.AonNotificationListenerService;
import com.motorola.aon.Constants;
import com.motorola.aon.R;
import com.motorola.aon.env.EnvChecker;
import com.motorola.aon.env.com.motorola.android.provider.MotorolaSettings;

public class AonSettingsActivity extends PreferenceActivity {
    static final String AON_KEY = "aon";
    private static final boolean DEBUG = false;
    private static final int DIALOG_BEGIN_TIMEPICKER = 1;
    private static final int DIALOG_END_TIMEPICKER = 2;
    private static final int DIALOG_NIGHT_SETTING = 0;
    static final String NIGHT_KEY = "night";
    private static final int POLICY_PRIVACY_NONE = 0;
    private static final int POLICY_PRIVACY_STRONG = 1;
    static final String PRIVACY_KEY = "privacy";
    private static final String TAG = "AonSettingsActivity";
    CheckBoxPreference mAon;
    ContentResolver mContentResolver;
    int mEndTime = 360;
    /* access modifiers changed from: private */
    public int mLastSnPrivacyByUI = -1;
    CheckBoxAndSettingsPreference mNightOff;
    CheckBoxPreference mPrivacy;
    private ContentObserver mSettingsObserver = new ContentObserver(new Handler()) {
        public void onChange(boolean selfChange, Uri uri) {
            if (uri != null && uri.equals(MotorolaSettings.getUriFor(MotorolaSettings.SN_PRIVACY))) {
                int newPrivacy = MotorolaSettings.getInt(AonSettingsActivity.this.mContentResolver, MotorolaSettings.SN_PRIVACY, -1);
                if (AonSettingsActivity.this.mLastSnPrivacyByUI != newPrivacy) {
                    AonSettingsActivity.this.updatePrivacyPreference(newPrivacy);
                    AonSettingsActivity.this.mLastSnPrivacyByUI = newPrivacy;
                }
            }
        }
    };
    int mStartTime = 1380;

    private class TimeListener implements OnTimeSetListener {
        final int mDlgId;

        TimeListener(int id) {
            this.mDlgId = id;
        }

        public void onTimeSet(TimePicker timePicker, int hour, int min) {
            if (this.mDlgId == 1) {
                AonSettingsActivity.this.mStartTime = (hour * 60) + min;
                AonSettingsActivity.this.removeDialog(1);
            } else if (this.mDlgId == 2) {
                AonSettingsActivity.this.mEndTime = (hour * 60) + min;
                AonSettingsActivity.this.removeDialog(2);
            } else {
                Log.e(AonSettingsActivity.TAG, "unexpected dialg id");
            }
            AonSettingsActivity.this.showDialog(0);
        }
    }

    public void onCreate(Bundle icicle) {
        boolean z;
        boolean z2 = true;
        if (AonSettingsConst.DEVELOPER_MODE) {
            StrictMode.setThreadPolicy(new Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
            StrictMode.setVmPolicy(new VmPolicy.Builder().detectLeakedSqlLiteObjects().detectLeakedClosableObjects().penaltyLog().penaltyDeath().build());
        }
        super.onCreate(icicle);
        if (!Constants.isPartialDisplay) {
            addPreferencesFromResource(R.xml.sn_setup_fd);
        } else {
            addPreferencesFromResource(R.xml.sn_setup);
        }
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        this.mContentResolver = getContentResolver();
        if (actionBar != null) {
            actionBar.setIcon(EnvChecker.getActionBarIcon());
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.sn_setting_title);
        }
        this.mAon = (CheckBoxPreference) findPreference(AON_KEY);
        this.mPrivacy = (CheckBoxPreference) findPreference(PRIVACY_KEY);
        this.mNightOff = (CheckBoxAndSettingsPreference) findPreference(NIGHT_KEY);
        this.mNightOff.setParentCheckPreference(this.mAon);
        this.mNightOff.setSettingClickListener(new OnClickListener() {
            public void onClick(View view) {
                AonSettingsActivity.this.showDialog(0);
            }
        });
        this.mNightOff.setCheckBoxClickListener(new OnClickListener() {
            public void onClick(View view) {
                MotorolaSettings.putInt(AonSettingsActivity.this.mContentResolver, MotorolaSettings.SN_NIGHT_OFF, AonSettingsActivity.this.mNightOff.isChecked() ? 1 : 0);
            }
        });
        if (icicle != null) {
            this.mStartTime = icicle.getInt(MotorolaSettings.SN_NIGHT_BEGIN);
            this.mEndTime = icicle.getInt(MotorolaSettings.SN_NIGHT_END);
        } else {
            loadTimes();
        }
        int aon = MotorolaSettings.getInt(this.mContentResolver, MotorolaSettings.SN_AON, 0);
        int nightOff = MotorolaSettings.getInt(this.mContentResolver, MotorolaSettings.SN_NIGHT_OFF, 0);
        CheckBoxAndSettingsPreference checkBoxAndSettingsPreference = this.mNightOff;
        if (nightOff != 0) {
            z = true;
        } else {
            z = false;
        }
        checkBoxAndSettingsPreference.setChecked(z);
        CheckBoxPreference checkBoxPreference = this.mAon;
        if (aon == 0) {
            z2 = false;
        }
        checkBoxPreference.setChecked(z2);
        try {
            ((PreferenceScreen) findPreference(getString(R.string.aod_version_key))).setSummary(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (Exception e) {
            Log.e(TAG, "Failed to get package info for AOD", e);
        }
    }

    public void onResume() {
        super.onResume();
        this.mContentResolver.registerContentObserver(MotorolaSettings.getUriFor(MotorolaSettings.SN_PRIVACY), true, this.mSettingsObserver);
        this.mLastSnPrivacyByUI = MotorolaSettings.getInt(this.mContentResolver, MotorolaSettings.SN_PRIVACY, -1);
        updatePrivacyPreference(this.mLastSnPrivacyByUI);
        updateNightSummary();
    }

    public void onPause() {
        this.mContentResolver.unregisterContentObserver(this.mSettingsObserver);
        super.onPause();
    }

    public void onDestroy() {
        removeDialog(1);
        removeDialog(2);
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt(MotorolaSettings.SN_NIGHT_BEGIN, this.mStartTime);
        bundle.putInt(MotorolaSettings.SN_NIGHT_END, this.mEndTime);
        super.onSaveInstanceState(bundle);
    }

    public void onBtnLearnMoreClicked(View v) {
        if (v != null && v.getId() == R.id.btn_learn_more) {
            startActivity(new Intent(this, BreathingDetailsActivity.class));
        }
    }

    /* access modifiers changed from: private */
    public void updatePrivacyPreference(int privacy) {
        int aon = MotorolaSettings.getInt(this.mContentResolver, MotorolaSettings.SN_AON, 0);
        int policyData = getPolicyDataFromPrivacySetting(privacy);
        this.mPrivacy.setChecked(privacy != 0);
        if (policyData == 1) {
            this.mPrivacy.setEnabled(false);
            this.mPrivacy.setSummaryOn(getResources().getString(R.string.sn_setting_privacy_summary) + " " + getResources().getString(R.string.sn_setting_privacy_disable_widgets_policy));
            return;
        }
        if (aon != 0) {
            this.mPrivacy.setEnabled(true);
        }
        this.mPrivacy.setSummaryOn(R.string.sn_setting_privacy_summary);
    }

    public boolean onNavigateUp() {
        finish();
        return true;
    }

    private String getTimeString(int time, boolean is24Hour) {
        if (is24Hour) {
            return getResources().getString(R.string.time_24, new Object[]{Integer.valueOf(time / 60), Integer.valueOf(time % 60)});
        }
        int pmStart = time / 60;
        if (pmStart < 12) {
            if (pmStart == 0) {
                pmStart = 12;
            }
            return getResources().getString(R.string.time_am, new Object[]{Integer.valueOf(pmStart), Integer.valueOf(time % 60)});
        }
        int pmStart2 = pmStart - 12;
        if (pmStart2 == 0) {
            pmStart2 = 12;
        }
        return getResources().getString(R.string.time_pm, new Object[]{Integer.valueOf(pmStart2), Integer.valueOf(time % 60)});
    }

    private void loadTimes() {
        this.mStartTime = MotorolaSettings.getInt(getContentResolver(), MotorolaSettings.SN_NIGHT_BEGIN, 0);
        this.mEndTime = MotorolaSettings.getInt(getContentResolver(), MotorolaSettings.SN_NIGHT_END, 0);
    }

    /* access modifiers changed from: private */
    public void updateNightSummary() {
        boolean is24Hour = DateFormat.is24HourFormat(this);
        String strStart = getTimeString(this.mStartTime, is24Hour);
        String strEnd = getTimeString(this.mEndTime, is24Hour);
        this.mNightOff.setSummary(getResources().getString(R.string.sn_setting_night_summary, new Object[]{strStart, strEnd}));
    }

    public Dialog onCreateDialog(int id) {
        boolean is24Hour = DateFormat.is24HourFormat(this);
        switch (id) {
            case 0:
                View settingView = LayoutInflater.from(this).inflate(R.layout.night_settings, null);
                Button end = (Button) settingView.findViewById(R.id.night_end);
                ((Button) settingView.findViewById(R.id.night_begin)).setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        AonSettingsActivity.this.showDialog(1);
                    }
                });
                end.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        AonSettingsActivity.this.showDialog(2);
                    }
                });
                return new AlertDialog.Builder(this).setTitle(R.string.night_settings_title).setView(settingView).setOnCancelListener(new OnCancelListener() {
                    public void onCancel(DialogInterface arg0) {
                        MotorolaSettings.putInt(AonSettingsActivity.this.getContentResolver(), MotorolaSettings.SN_NIGHT_BEGIN, AonSettingsActivity.this.mStartTime);
                        MotorolaSettings.putInt(AonSettingsActivity.this.getContentResolver(), MotorolaSettings.SN_NIGHT_END, AonSettingsActivity.this.mEndTime);
                        AonSettingsActivity.this.updateNightSummary();
                    }
                }).setPositiveButton(R.string.done, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MotorolaSettings.putInt(AonSettingsActivity.this.getContentResolver(), MotorolaSettings.SN_NIGHT_BEGIN, AonSettingsActivity.this.mStartTime);
                        MotorolaSettings.putInt(AonSettingsActivity.this.getContentResolver(), MotorolaSettings.SN_NIGHT_END, AonSettingsActivity.this.mEndTime);
                        AonSettingsActivity.this.updateNightSummary();
                    }
                }).create();
            case 1:
                Dialog d = new TimePickerDialog(this, new TimeListener(id), this.mStartTime / 60, this.mStartTime % 60, is24Hour);
                d.setCanceledOnTouchOutside(true);
                d.setTitle(R.string.night_settings_night_start_picker_title);
                return d;
            case 2:
                Dialog d2 = new TimePickerDialog(this, new TimeListener(id), this.mEndTime / 60, this.mEndTime % 60, is24Hour);
                d2.setCanceledOnTouchOutside(true);
                d2.setTitle(R.string.night_settings_night_end_picker_title);
                return d2;
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        if (id == 0 && dialog != null) {
            boolean is24Hour = DateFormat.is24HourFormat(this);
            String strStart = getTimeString(this.mStartTime, is24Hour);
            String strEnd = getTimeString(this.mEndTime, is24Hour);
            Button end = (Button) dialog.findViewById(R.id.night_end);
            ((Button) dialog.findViewById(R.id.night_begin)).setText(strStart);
            end.setText(strEnd);
        } else if (id == 1 && dialog != null) {
            ((TimePickerDialog) dialog).updateTime(this.mStartTime / 60, this.mStartTime % 60);
        } else if (id == 2 && dialog != null) {
            ((TimePickerDialog) dialog).updateTime(this.mEndTime / 60, this.mEndTime % 60);
        }
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        int i = 1;
        if (this.mAon == preference) {
            ContentResolver contentResolver = this.mContentResolver;
            String str = MotorolaSettings.SN_AON;
            if (!this.mAon.isChecked()) {
                i = 0;
            }
            MotorolaSettings.putInt(contentResolver, str, i);
            sendBroadcast(new Intent(Constants.SN_ANALYTICS_SETTINGS));
            Intent iReg = new Intent(this.mAon.isChecked() ? Constants.ACTION_REGISTER_NOTIFICATION_LISTENER : Constants.ACTION_UNREGISTER_NOTIFICATION_LISTENER);
            iReg.putExtra(Constants.EXTRA_PACKAGE_NAME, getPackageName());
            iReg.putExtra(Constants.EXTRA_SERVICE_NAME, AonNotificationListenerService.class.getName());
            sendBroadcast(iReg);
        } else if (this.mPrivacy == preference) {
            if (!this.mPrivacy.isChecked()) {
                i = 0;
            }
            this.mLastSnPrivacyByUI = i;
            MotorolaSettings.putInt(this.mContentResolver, MotorolaSettings.SN_PRIVACY, this.mLastSnPrivacyByUI);
        }
        return false;
    }

    private int getPolicyDataFromPrivacySetting(int snPrivacy) {
        return (-65536 & snPrivacy) >> 16;
    }

    private int getUserChoiceFromPrivacySetting(int snPrivacy) {
        return 65535 & snPrivacy;
    }
}
