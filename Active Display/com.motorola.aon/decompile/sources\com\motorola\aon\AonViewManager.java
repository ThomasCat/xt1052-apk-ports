package com.motorola.aon;

import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.os.SystemClock;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.motorola.aon.AonService.IGetAonInterface;
import com.motorola.aon.aidl.NotificationEntry;
import com.motorola.aon.aidl.ParsedInfo;
import com.motorola.aon.env.android.app.ActivityManagerNative;
import com.motorola.aon.env.android.os.IRemoteCallback.Stub;
import com.motorola.aon.env.android.os.ServiceManager;
import com.motorola.aon.env.android.view.IWindowManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class AonViewManager {
    public static final int AOD_VIEW_NONE = -1;
    public static final int AOD_VIEW_NOTIFICATION_NORMAL = 0;
    public static final int AOD_VIEW_NOTIFICATION_SECURE = 1;
    public static final int AOD_VIEW_TUTORIAL = 2;
    /* access modifiers changed from: private */
    public static final boolean CURRENT_DRAIN_DEBUG = Constants.CURRENT_DRAIN_DEBUG;
    protected static final boolean DEBUG = AonService.DEBUG;
    public static final float GREY_LEVEL_FACTOR_BLUE = 0.07f;
    public static final float GREY_LEVEL_FACTOR_GREEN = 0.71f;
    public static final float GREY_LEVEL_FACTOR_RED = 0.21f;
    private static final int MAX_ONE_LINE_TEXT_LENGTH = 100;
    private static final int MAX_THREE_LINES_TEXT_LENGTH = 300;
    public static final int MIN_PHOTO_HEIGHT = 128;
    public static final int MIN_PHOTO_WIDTH = 128;
    private static final int OVERRIDE_DISPLAY_TURNING_ON = -1000001;
    static final String TAG = "AonViewManager";
    private final int DARK_GREY = 4473924;
    private float DARK_THRESHOLD = 100.0f;
    private int NUM_PIXELS_TO_SKIP_FOR_AVERAGE = 3;
    private int NUM_ROWS_TO_SKIP_FOR_AVERAGE_AT_BOUNDARY = 5;
    protected NotificationEntry mBreathingNotificationEntry;
    protected View mCachedBreathingView;
    private int mColorBlueText;
    private int mColorText;
    protected final Context mContext;
    private Bitmap mDefPhoto;
    protected final Display mDisplay;
    protected View mDot01;
    protected View mDot02;
    protected View mDot03;
    private Drawable[] mDrawableDigits = new Drawable[10];
    private View mEnhancedViewPaddingL;
    private View mEnhancedViewPaddingR;
    private View mEnhancedViewPaddingShift;
    private ColorMatrixColorFilter mGreyFilter;
    protected Handler mHandler = new Handler();
    protected final IGetAonInterface mIGetAonInterface;
    private ImageView mIvBattery;
    protected View mIvBreathingContent;
    private ImageView mIvHour0;
    private ImageView mIvHour1;
    private ImageView mIvMin0;
    private ImageView mIvMin1;
    protected ImageView mIvNoti0;
    protected ImageView mIvNoti0Circle;
    protected ImageView mIvNoti1;
    protected ImageView mIvNoti2;
    protected ImageView mIvNoti3;
    protected ImageView mIvNotiMore;
    protected ImageView mIvPeekNoti0;
    protected ImageView mIvPeekNoti0Circle;
    protected ImageView mIvPicture;
    private View mIvPictureFrame;
    private final KeyguardManager mKeyguardManager;
    private int mLastBreathingViewType = -1;
    private ColorMatrixColorFilter mLightGreyFilter;
    protected View mPeekSmallNotiContainer;
    protected View mPeekTop;
    protected View mPeekTopContent;
    protected View mPriPeekSmallNotiContainer;
    protected MyRemoteCallback mRemoteCallback = new MyRemoteCallback();
    private int mSmallIconHeight;
    private TextView mTvInbox0;
    private TextView mTvInbox1;
    private TextView mTvInbox2;
    protected TextView mTvPeekNoti1;
    protected TextView mTvPeekNoti2;
    protected TextView mTvPeekNoti3;
    private TextView mTvText;
    private TextView mTvText2;
    private TextView mTvTextTitle;
    private TextView mTvWhen;
    protected IWindowManager mWindowManager;

    protected class MyRemoteCallback extends Stub {
        static final int ACTION_SELECT = 1;
        static final int ACTION_TUTORIAL = 2;
        private int mAction;
        private MinedData mMinedData;

        protected MyRemoteCallback() {
        }

        public void setAction(int action, MinedData minedData) {
            this.mAction = action;
            this.mMinedData = minedData;
        }

        public void sendResult(Bundle args) throws RemoteException {
            switch (this.mAction) {
                case 1:
                    if (AonViewManager.DEBUG) {
                        Log.d(AonViewManager.TAG, "sendResult - ACTION_SELECT ");
                    }
                    if (this.mMinedData != null) {
                        if (AonViewManager.CURRENT_DRAIN_DEBUG) {
                            Log.d(AonViewManager.TAG, "White list notification is cleared by swipe up");
                        }
                        ActivityManagerNative.getDefault().resumeAppSwitches();
                        if (AonViewManager.this.canDismissKeyguard()) {
                            AonViewManager.this.requestToDismissKeyguard();
                        }
                        PendingIntent intent = this.mMinedData.contentIntent;
                        if (intent != null) {
                            try {
                                AonViewManager.this.mWindowManager.overridePendingAppTransition(this.mMinedData.pkg, AonViewManager.OVERRIDE_DISPLAY_TURNING_ON, AonViewManager.OVERRIDE_DISPLAY_TURNING_ON, null);
                                intent.send();
                                AonViewManager.this.mWindowManager.overridePendingAppTransition(this.mMinedData.pkg, 0, 0, null);
                            } catch (CanceledException e) {
                                Log.w(AonViewManager.TAG, "Sending contentIntent failed: " + e);
                            }
                        } else if (AonViewManager.DEBUG) {
                            Log.d(AonViewManager.TAG, "pending intent null in notification");
                        }
                        if ((this.mMinedData.flags & 16) != 0) {
                            Intent iService = new Intent(AonViewManager.this.mContext, AonViewManager.this.getServiceClass());
                            iService.setAction(Constants.ACTION_CANCEL_NOTIFICATION);
                            iService.putExtra(Constants.EXTRA_PACKAGE_NAME, this.mMinedData.pkg);
                            iService.putExtra(Constants.EXTRA_TAG, this.mMinedData.tag);
                            iService.putExtra(Constants.EXTRA_ID, this.mMinedData.id);
                            AonViewManager.this.mContext.startService(iService);
                            return;
                        }
                        return;
                    }
                    return;
                case 2:
                    if (AonViewManager.DEBUG) {
                        Log.d(AonViewManager.TAG, "sendResult - ACTION_TUTORIAL ");
                    }
                    Intent i = new Intent("com.motorola.aon.ACTION_TUTORIAL");
                    i.setPackage(BuildConfig.PACKAGE_NAME);
                    i.addFlags(268435456);
                    AonViewManager.this.mContext.startActivity(i);
                    AonViewManager.this.onTutorialLaunched();
                    return;
                default:
                    return;
            }
        }
    }

    public AonViewManager(Context context, AonNotificationManager aonNotificationManager, IGetAonInterface getAonInterface) {
        if (DEBUG) {
            Log.d(TAG, "AonViewManager ");
        }
        if (context == null || aonNotificationManager == null) {
            throw new IllegalArgumentException("Invalid parameters");
        }
        this.mIGetAonInterface = getAonInterface;
        this.mContext = context;
        this.mDisplay = ((WindowManager) this.mContext.getSystemService("window")).getDefaultDisplay();
        this.mKeyguardManager = (KeyguardManager) this.mContext.getSystemService("keyguard");
        this.mWindowManager = IWindowManager.Stub.asInterface(ServiceManager.getService("window"));
        Resources res = this.mContext.getResources();
        this.mColorText = res.getColor(R.color.text);
        this.mColorBlueText = res.getColor(R.color.blue);
        this.mDrawableDigits[0] = res.getDrawable(R.drawable.ic_clock_0);
        this.mDrawableDigits[1] = res.getDrawable(R.drawable.ic_clock_1);
        this.mDrawableDigits[2] = res.getDrawable(R.drawable.ic_clock_2);
        this.mDrawableDigits[3] = res.getDrawable(R.drawable.ic_clock_3);
        this.mDrawableDigits[4] = res.getDrawable(R.drawable.ic_clock_4);
        this.mDrawableDigits[5] = res.getDrawable(R.drawable.ic_clock_5);
        this.mDrawableDigits[6] = res.getDrawable(R.drawable.ic_clock_6);
        this.mDrawableDigits[7] = res.getDrawable(R.drawable.ic_clock_7);
        this.mDrawableDigits[8] = res.getDrawable(R.drawable.ic_clock_8);
        this.mDrawableDigits[9] = res.getDrawable(R.drawable.ic_clock_9);
        this.mLightGreyFilter = new ColorMatrixColorFilter(new float[]{0.21f, 0.71f, 0.07f, 0.0f, 100.0f, 0.21f, 0.71f, 0.07f, 0.0f, 100.0f, 0.21f, 0.71f, 0.07f, 0.0f, 100.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f});
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0.0f);
        this.mGreyFilter = new ColorMatrixColorFilter(matrix);
        this.mDefPhoto = ((BitmapDrawable) res.getDrawable(R.drawable.ic_contact_picture)).getBitmap();
        this.mSmallIconHeight = res.getDimensionPixelSize(R.dimen.status_bar_image_size);
    }

    /* access modifiers changed from: protected */
    public void initRes() {
        if (DEBUG) {
            Log.d(TAG, "initRes ");
        }
        if (this.mCachedBreathingView == null) {
            this.mCachedBreathingView = LayoutInflater.from(this.mContext).inflate(R.layout.breathing, null);
            this.mCachedBreathingView.findViewById(R.id.clock_layout).setVisibility(4);
            this.mIvPicture = (ImageView) this.mCachedBreathingView.findViewById(R.id.picture);
            this.mIvPictureFrame = this.mCachedBreathingView.findViewById(R.id.picture_frame);
            this.mTvTextTitle = (TextView) this.mCachedBreathingView.findViewById(R.id.title);
            this.mTvText2 = (TextView) this.mCachedBreathingView.findViewById(R.id.text2);
            this.mTvText = (TextView) this.mCachedBreathingView.findViewById(R.id.text);
            this.mTvWhen = (TextView) this.mCachedBreathingView.findViewById(R.id.when);
            this.mIvPeekNoti0 = (ImageView) this.mCachedBreathingView.findViewById(R.id.peek_noti0);
            this.mIvBreathingContent = this.mCachedBreathingView.findViewById(R.id.breathing_content);
            this.mPeekTopContent = this.mCachedBreathingView.findViewById(R.id.peek_top_content);
            this.mPeekTop = this.mCachedBreathingView.findViewById(R.id.peek_top);
            this.mDot01 = this.mCachedBreathingView.findViewById(R.id.dot01);
            this.mDot02 = this.mCachedBreathingView.findViewById(R.id.dot02);
            this.mDot03 = this.mCachedBreathingView.findViewById(R.id.dot03);
            this.mIvHour0 = (ImageView) this.mCachedBreathingView.findViewById(R.id.hour0);
            this.mIvHour1 = (ImageView) this.mCachedBreathingView.findViewById(R.id.hour1);
            this.mIvMin0 = (ImageView) this.mCachedBreathingView.findViewById(R.id.min0);
            this.mIvMin1 = (ImageView) this.mCachedBreathingView.findViewById(R.id.min1);
            this.mIvBattery = (ImageView) this.mCachedBreathingView.findViewById(R.id.img_battery);
            this.mEnhancedViewPaddingL = this.mCachedBreathingView.findViewById(R.id.enhanced_view_padding_l);
            this.mEnhancedViewPaddingR = this.mCachedBreathingView.findViewById(R.id.enhanced_view_padding_r);
            this.mEnhancedViewPaddingShift = this.mCachedBreathingView.findViewById(R.id.enhanced_view_shift_padding);
            this.mIvNoti0 = (ImageView) this.mCachedBreathingView.findViewById(R.id.noti0);
            this.mIvNoti1 = (ImageView) this.mCachedBreathingView.findViewById(R.id.noti1);
            this.mIvNoti2 = (ImageView) this.mCachedBreathingView.findViewById(R.id.noti2);
            this.mIvNoti3 = (ImageView) this.mCachedBreathingView.findViewById(R.id.noti3);
            this.mIvNotiMore = (ImageView) this.mCachedBreathingView.findViewById(R.id.noti_more);
            this.mTvPeekNoti1 = (TextView) this.mCachedBreathingView.findViewById(R.id.peek_noti1);
            this.mTvPeekNoti2 = (TextView) this.mCachedBreathingView.findViewById(R.id.peek_noti2);
            this.mTvPeekNoti3 = (TextView) this.mCachedBreathingView.findViewById(R.id.peek_noti3);
            this.mPeekSmallNotiContainer = this.mCachedBreathingView.findViewById(R.id.peek_noti_container);
            this.mIvNoti0Circle = (ImageView) this.mCachedBreathingView.findViewById(R.id.circle_center);
            this.mIvPeekNoti0Circle = (ImageView) this.mCachedBreathingView.findViewById(R.id.circle_top);
            this.mTvInbox0 = (TextView) this.mCachedBreathingView.findViewById(R.id.inbox0);
            this.mTvInbox1 = (TextView) this.mCachedBreathingView.findViewById(R.id.inbox1);
            this.mTvInbox2 = (TextView) this.mCachedBreathingView.findViewById(R.id.inbox2);
        }
    }

    private void drawClockBattery(int batteryStatus) {
        boolean is3digit;
        if (DEBUG) {
            Log.d(TAG, "drawClockBattery ");
        }
        int[] digits = new int[4];
        if (getClockTime(digits) || (digits[0] > 0 && digits[0] <= 9)) {
            this.mIvHour0.setVisibility(0);
            this.mIvHour0.setImageDrawable(this.mDrawableDigits[digits[0]]);
            is3digit = false;
        } else {
            this.mIvHour0.setVisibility(8);
            is3digit = true;
        }
        if (batteryStatus == 0) {
            this.mEnhancedViewPaddingL.setVisibility(8);
            this.mEnhancedViewPaddingR.setVisibility(8);
            this.mEnhancedViewPaddingShift.setVisibility(8);
        } else {
            this.mEnhancedViewPaddingShift.setVisibility(0);
            if (is3digit) {
                this.mEnhancedViewPaddingL.setVisibility(0);
                this.mEnhancedViewPaddingR.setVisibility(0);
            } else {
                this.mEnhancedViewPaddingL.setVisibility(8);
                this.mEnhancedViewPaddingR.setVisibility(8);
            }
        }
        if (digits[1] >= 0 && digits[1] <= 9) {
            this.mIvHour1.setImageDrawable(this.mDrawableDigits[digits[1]]);
        }
        if (digits[2] >= 0 && digits[2] <= 9) {
            this.mIvMin0.setImageDrawable(this.mDrawableDigits[digits[2]]);
        }
        if (digits[3] >= 0 && digits[3] <= 9) {
            this.mIvMin1.setImageDrawable(this.mDrawableDigits[digits[3]]);
        }
        if (batteryStatus == 0) {
            this.mIvBattery.setVisibility(8);
            return;
        }
        this.mIvBattery.setVisibility(0);
        if (batteryStatus == 2) {
            this.mIvBattery.setImageResource(R.drawable.ic_battery5);
        } else if (batteryStatus == 1) {
            this.mIvBattery.setImageResource(R.drawable.ic_battery15);
        } else {
            this.mIvBattery.setVisibility(8);
        }
    }

    private boolean getClockTime(int[] digits) {
        if (DEBUG) {
            Log.d(TAG, "getClockTime ");
        }
        boolean is24 = DateFormat.is24HourFormat(this.mContext);
        Date dt = Calendar.getInstance().getTime();
        int hour = dt.getHours();
        int min = dt.getMinutes();
        if (!is24) {
            if (hour == 0) {
                hour = 12;
            } else if (hour > 12) {
                hour -= 12;
            }
        }
        digits[0] = hour / 10;
        digits[1] = hour % 10;
        digits[2] = min / 10;
        digits[3] = min % 10;
        return is24;
    }

    public void fillBreathingView(View views, ArrayList<NotificationEntry> allBreathings, int batteryStatus, int viewType) {
        int privacy = 1;
        if (DEBUG) {
            Log.d(TAG, "last view " + this.mLastBreathingViewType + ", current view " + viewType);
        }
        if (viewType == 0 || viewType == 1) {
            if (viewType == 0) {
                privacy = 0;
            }
            if (this.mLastBreathingViewType == 2 || this.mLastBreathingViewType == -1) {
                this.mTvText.setTextColor(this.mColorText);
                this.mCachedBreathingView.findViewById(R.id.peek_tutorial).setVisibility(8);
                this.mCachedBreathingView.findViewById(R.id.breathe_tutorial).setVisibility(8);
                this.mCachedBreathingView.findViewById(R.id.tutorial_touch).setVisibility(8);
                this.mCachedBreathingView.findViewById(R.id.frame_center).setVisibility(0);
                this.mCachedBreathingView.findViewById(R.id.more_icons).setVisibility(0);
                this.mCachedBreathingView.findViewById(R.id.frame_top).setVisibility(0);
            }
            fillNotificationView(views, allBreathings, privacy);
            drawClockBattery(batteryStatus);
        } else if (viewType == 2) {
            TextView tutTouchMe = (TextView) this.mCachedBreathingView.findViewById(R.id.tutorial_touch);
            if (this.mLastBreathingViewType != 2) {
                this.mPeekTop.setVisibility(0);
                this.mPeekTopContent.setVisibility(0);
                this.mTvTextTitle.setVisibility(0);
                this.mTvText2.setVisibility(0);
                this.mTvText.setVisibility(0);
                this.mCachedBreathingView.findViewById(R.id.peek_tutorial).setVisibility(0);
                this.mCachedBreathingView.findViewById(R.id.breathe_tutorial).setVisibility(0);
                tutTouchMe.setText(R.string.tutorial_aod_info_touch);
                tutTouchMe.setVisibility(0);
                this.mDot01.setVisibility(0);
                this.mDot02.setVisibility(0);
                this.mDot03.setVisibility(0);
                this.mTvInbox0.setVisibility(8);
                this.mTvInbox1.setVisibility(8);
                this.mTvInbox2.setVisibility(8);
                this.mCachedBreathingView.findViewById(R.id.frame_top).setVisibility(8);
                this.mCachedBreathingView.findViewById(R.id.frame_center).setVisibility(8);
                this.mCachedBreathingView.findViewById(R.id.more_icons).setVisibility(8);
                this.mIvPictureFrame.setVisibility(8);
                this.mTvWhen.setVisibility(8);
                this.mPeekSmallNotiContainer.setVisibility(8);
            }
            this.mTvTextTitle.setText(R.string.tutorial_aod_title);
            this.mTvText2.setText(R.string.tutorial_aod_description);
            this.mTvText.setText(R.string.tutorial_aod_info_drag);
            this.mTvText.setTextColor(this.mColorBlueText);
            tutTouchMe.setText(R.string.tutorial_aod_info_touch);
            drawClockBattery(batteryStatus);
        } else {
            Log.w(TAG, "not supported view type");
        }
        this.mLastBreathingViewType = viewType;
    }

    private void setTruncatedText(TextView tv, CharSequence cs, int len) {
        if (cs == null || cs.length() <= len) {
            tv.setText(cs);
        } else {
            tv.setText(cs.subSequence(0, len));
        }
    }

    private void setTruncatedText(TextView tv, String str, int len) {
        if (str == null || str.length() <= len) {
            tv.setText(str);
        } else {
            tv.setText(str.substring(0, len));
        }
    }

    private boolean optimizeBrightness(Drawable img) {
        if (img == null) {
            Log.e(TAG, "optimizeBrightness input img is null!");
            return false;
        }
        Bitmap bitmap = ((BitmapDrawable) img).getBitmap();
        if (bitmap == null) {
            Log.e(TAG, "optimizeBrightness bitmap is null!");
            return false;
        }
        int endX = bitmap.getWidth();
        int endY = bitmap.getHeight();
        int startX = 0;
        int startY = 0;
        int numPixels = 0;
        long rSum = 0;
        long gSum = 0;
        long bSum = 0;
        if (endX > this.NUM_ROWS_TO_SKIP_FOR_AVERAGE_AT_BOUNDARY * 3) {
            startX = this.NUM_ROWS_TO_SKIP_FOR_AVERAGE_AT_BOUNDARY;
            endX -= this.NUM_ROWS_TO_SKIP_FOR_AVERAGE_AT_BOUNDARY;
        }
        if (endY > this.NUM_ROWS_TO_SKIP_FOR_AVERAGE_AT_BOUNDARY * 3) {
            startY = this.NUM_ROWS_TO_SKIP_FOR_AVERAGE_AT_BOUNDARY;
            endY -= this.NUM_ROWS_TO_SKIP_FOR_AVERAGE_AT_BOUNDARY;
        }
        int y = startY;
        while (y < endY) {
            int x = startX;
            while (x < endX) {
                int pixel = bitmap.getPixel(x, y);
                if ((16777215 & pixel) > 4473924) {
                    rSum += (long) Color.red(pixel);
                    gSum += (long) Color.green(pixel);
                    bSum += (long) Color.blue(pixel);
                    numPixels++;
                }
                x += this.NUM_PIXELS_TO_SKIP_FOR_AVERAGE;
            }
            y += this.NUM_PIXELS_TO_SKIP_FOR_AVERAGE;
        }
        if (numPixels > 0) {
            float avGrey = (((float) (rSum / ((long) numPixels))) * 0.21f) + (((float) (gSum / ((long) numPixels))) * 0.71f) + (((float) (bSum / ((long) numPixels))) * 0.07f);
            if (avGrey < this.DARK_THRESHOLD) {
                Log.d(TAG, "optimizeBrightness avGrey = " + avGrey + " result:dark");
                return true;
            }
            Log.d(TAG, "optimizeBrightness avGrey = " + avGrey + " result:light");
            return false;
        }
        if (DEBUG) {
            Log.d(TAG, "optimizeBrightness num pixels found = 0");
        }
        return false;
    }

    private void setColorFilter(Drawable d) {
        long begin = 0;
        if (DEBUG) {
            begin = SystemClock.elapsedRealtime();
        }
        if (optimizeBrightness(d)) {
            d.setColorFilter(this.mLightGreyFilter);
            if (DEBUG) {
                Log.d(TAG, "It took [" + (SystemClock.elapsedRealtime() - begin) + "] ms to set light grey filter");
                return;
            }
            return;
        }
        d.setColorFilter(this.mGreyFilter);
        if (DEBUG) {
            Log.d(TAG, "It took [" + (SystemClock.elapsedRealtime() - begin) + "] ms to set normal grey filter");
        }
    }

    private void fillNotificationView(View views, ArrayList<NotificationEntry> allBreathings, int privacy) {
        boolean hasText2;
        boolean hasText;
        if (DEBUG) {
            Log.d(TAG, "fillNotificationView ");
        }
        if (views != null) {
            int N = 0;
            if (allBreathings != null) {
                N = allBreathings.size();
                if (DEBUG) {
                    Log.d(TAG, "number of notifications for breathing view " + N);
                }
            }
            this.mIvNotiMore.setVisibility(N > 4 ? 0 : 8);
            int noti3Visible = N > 3 ? 0 : 8;
            this.mIvNoti3.setVisibility(noti3Visible);
            this.mTvPeekNoti3.setVisibility(noti3Visible);
            if (N > 3) {
                NotificationEntry ne3 = (NotificationEntry) allBreathings.get(3);
                if (ne3 == null || ne3.minedData == null || ne3.minedData.iconBitmap == null) {
                    Log.w(TAG, "got a null notification");
                } else {
                    Drawable dNoti3 = new BitmapDrawable(this.mContext.getResources(), ne3.minedData.iconBitmap);
                    setColorFilter(dNoti3);
                    this.mIvNoti3.setImageDrawable(dNoti3);
                    if (privacy == 0) {
                        Drawable d3 = dNoti3.getConstantState().newDrawable();
                        d3.setBounds(0, 0, this.mSmallIconHeight, this.mSmallIconHeight);
                        this.mTvPeekNoti3.setCompoundDrawables(d3, null, null, null);
                        setTruncatedText(this.mTvPeekNoti3, AonNotificationManager.getNotificationText(ne3.minedData), (int) MAX_ONE_LINE_TEXT_LENGTH);
                    }
                }
            }
            int noti2Visible = N > 2 ? 0 : 8;
            this.mIvNoti2.setVisibility(noti2Visible);
            this.mTvPeekNoti2.setVisibility(noti2Visible);
            if (N > 2) {
                NotificationEntry ne2 = (NotificationEntry) allBreathings.get(2);
                if (ne2 == null || ne2.minedData == null || ne2.minedData.iconBitmap == null) {
                    Log.w(TAG, "got a null notification");
                } else {
                    Drawable dNoti2 = new BitmapDrawable(this.mContext.getResources(), ne2.minedData.iconBitmap);
                    setColorFilter(dNoti2);
                    this.mIvNoti2.setImageDrawable(dNoti2);
                    if (privacy == 0) {
                        Drawable d2 = dNoti2.getConstantState().newDrawable();
                        d2.setBounds(0, 0, this.mSmallIconHeight, this.mSmallIconHeight);
                        this.mTvPeekNoti2.setCompoundDrawables(d2, null, null, null);
                        setTruncatedText(this.mTvPeekNoti2, AonNotificationManager.getNotificationText(ne2.minedData), (int) MAX_ONE_LINE_TEXT_LENGTH);
                    }
                }
            }
            int noti1Visible = N > 1 ? 0 : 8;
            this.mIvNoti1.setVisibility(noti1Visible);
            this.mTvPeekNoti1.setVisibility(noti1Visible);
            if (N > 1) {
                NotificationEntry ne1 = (NotificationEntry) allBreathings.get(1);
                if (ne1 == null || ne1.minedData == null || ne1.minedData.iconBitmap == null) {
                    Log.w(TAG, "got a null notification");
                } else {
                    Drawable dNoti1 = new BitmapDrawable(this.mContext.getResources(), ne1.minedData.iconBitmap);
                    setColorFilter(dNoti1);
                    this.mIvNoti1.setImageDrawable(dNoti1);
                    if (privacy == 0) {
                        Drawable d1 = dNoti1.getConstantState().newDrawable();
                        d1.setBounds(0, 0, this.mSmallIconHeight, this.mSmallIconHeight);
                        this.mTvPeekNoti1.setCompoundDrawables(d1, null, null, null);
                        setTruncatedText(this.mTvPeekNoti1, AonNotificationManager.getNotificationText(ne1.minedData), (int) MAX_ONE_LINE_TEXT_LENGTH);
                    }
                }
            }
            NotificationEntry ne = null;
            if (N > 0) {
                ne = (NotificationEntry) allBreathings.get(0);
            }
            if (!(ne == null || ne.minedData == null)) {
                MinedData md = ne.minedData;
                if (md.iconBitmap != null) {
                    Drawable dNoti0 = new BitmapDrawable(this.mContext.getResources(), md.iconBitmap);
                    setColorFilter(dNoti0);
                    this.mIvNoti0Circle.clearColorFilter();
                    this.mIvPeekNoti0Circle.clearColorFilter();
                    this.mIvNoti0.setImageDrawable(dNoti0);
                    this.mIvPeekNoti0.setImageDrawable(dNoti0);
                } else {
                    this.mIvNoti0.setImageDrawable(null);
                    this.mIvPeekNoti0.setImageDrawable(null);
                }
                if (privacy == 0) {
                    ParsedInfo info = new ParsedInfo();
                    AonNotificationManager.parseNotification(md, true, info);
                    this.mPeekTopContent.setVisibility(0);
                    setTruncatedText(this.mTvTextTitle, info.title, (int) MAX_ONE_LINE_TEXT_LENGTH);
                    if (info.style == 2) {
                        this.mIvPictureFrame.setVisibility(8);
                        this.mIvPicture.setImageDrawable(null);
                        this.mTvText2.setVisibility(8);
                        this.mTvText.setVisibility(8);
                        if (info.inbox == null || info.inbox.size() <= 2) {
                            this.mTvInbox2.setVisibility(8);
                        } else {
                            this.mTvInbox2.setText(R.string.inbox_more);
                            this.mTvInbox2.setVisibility(0);
                        }
                        if (info.inbox == null || info.inbox.size() <= 1) {
                            this.mTvInbox1.setVisibility(8);
                        } else {
                            this.mTvInbox1.setVisibility(0);
                            setTruncatedText(this.mTvInbox1, (CharSequence) info.inbox.get(1), (int) MAX_ONE_LINE_TEXT_LENGTH);
                        }
                        if (info.inbox == null || info.inbox.size() <= 0) {
                            this.mTvInbox0.setVisibility(8);
                        } else {
                            this.mTvInbox0.setVisibility(0);
                            setTruncatedText(this.mTvInbox0, (CharSequence) info.inbox.get(0), (int) MAX_ONE_LINE_TEXT_LENGTH);
                        }
                    } else {
                        Bitmap photo = null;
                        if (info.bigPicture != null) {
                            photo = info.bigPicture;
                        } else if (md.largeIcon != null) {
                            photo = md.largeIcon;
                            if (photo != null && this.mDefPhoto.sameAs(photo)) {
                                photo = null;
                            }
                        }
                        boolean showPhoto = false;
                        if (photo != null) {
                            int h = photo.getHeight();
                            int w = photo.getWidth();
                            if (DEBUG) {
                                Log.d(TAG, "photo size (w,h) " + w + "," + h);
                            }
                            if (h >= 128 && w >= 128) {
                                showPhoto = true;
                                this.mIvPictureFrame.setVisibility(0);
                                this.mIvPicture.setImageBitmap(photo);
                            }
                        }
                        if (!showPhoto) {
                            this.mIvPictureFrame.setVisibility(8);
                            this.mIvPicture.setImageDrawable(null);
                        }
                        this.mTvInbox0.setVisibility(8);
                        this.mTvInbox1.setVisibility(8);
                        this.mTvInbox2.setVisibility(8);
                        if (!TextUtils.isEmpty(info.text2)) {
                            hasText2 = true;
                            this.mTvText2.setVisibility(0);
                            setTruncatedText(this.mTvText2, info.text2.toString(), (int) MAX_THREE_LINES_TEXT_LENGTH);
                        } else if (!TextUtils.isEmpty(info.bigText)) {
                            hasText2 = true;
                            this.mTvText2.setVisibility(0);
                            setTruncatedText(this.mTvText2, info.bigText.toString(), (int) MAX_THREE_LINES_TEXT_LENGTH);
                        } else {
                            hasText2 = false;
                            this.mTvText2.setText(BuildConfig.FLAVOR);
                            this.mTvText2.setVisibility(8);
                        }
                        if (!TextUtils.isEmpty(info.text)) {
                            hasText = true;
                            this.mTvText.setVisibility(0);
                            setTruncatedText(this.mTvText, info.text.toString(), (int) MAX_THREE_LINES_TEXT_LENGTH);
                        } else {
                            hasText = false;
                            this.mTvText.setText(BuildConfig.FLAVOR);
                            this.mTvText.setVisibility(8);
                        }
                        if (hasText2 && hasText) {
                            this.mTvText2.setMaxLines(2);
                            this.mTvText.setMaxLines(1);
                        } else if (hasText2 && !hasText) {
                            this.mTvText2.setMaxLines(3);
                        } else if (!hasText2 && hasText) {
                            this.mTvText.setMaxLines(3);
                        }
                    }
                    if (!TextUtils.isEmpty(info.when)) {
                        this.mTvWhen.setVisibility(0);
                        setTruncatedText(this.mTvWhen, info.when, (int) MAX_ONE_LINE_TEXT_LENGTH);
                    } else {
                        this.mTvWhen.setText(BuildConfig.FLAVOR);
                        this.mTvWhen.setVisibility(8);
                    }
                } else {
                    this.mPeekTopContent.setVisibility(4);
                }
            }
            if (N <= 1 || privacy != 0) {
                this.mPeekSmallNotiContainer.setVisibility(8);
            } else {
                this.mPeekSmallNotiContainer.setVisibility(0);
            }
            if (N <= 0) {
                this.mPeekTop.setVisibility(4);
                this.mDot01.setVisibility(4);
                this.mDot02.setVisibility(4);
                this.mDot03.setVisibility(4);
                this.mIvNoti0.setImageResource(R.drawable.ic_lock);
                this.mIvNoti0Circle.setVisibility(4);
                return;
            }
            this.mPeekTop.setVisibility(0);
            this.mDot01.setVisibility(0);
            this.mDot02.setVisibility(0);
            this.mDot03.setVisibility(0);
            this.mIvNoti0Circle.setVisibility(0);
        } else if (DEBUG) {
            Log.d(TAG, "null breathing view");
        }
    }

    public Bitmap getNotificationBitmap(MinedData md, boolean big) {
        if (DEBUG) {
            Log.d(TAG, "getNotificationBitmap ");
        }
        return big ? md.largeIcon : md.iconBitmap;
    }

    /* access modifiers changed from: protected */
    public void onTutorialLaunched() {
    }

    public void unlock() {
        if (DEBUG) {
            Log.d(TAG, "unlock");
        }
        if (!this.mKeyguardManager.isKeyguardSecure() || !this.mKeyguardManager.isKeyguardLocked()) {
            requestToDismissKeyguard();
            return;
        }
        Intent intent = new Intent("com.motorola.internal.policy.impl.REQUEST_UNLOCK");
        intent.putExtra("dismiss", true);
        this.mContext.sendBroadcast(intent);
    }

    public void launchNotification() {
        if (DEBUG) {
            Log.d(TAG, "launchNotification");
        }
        if (this.mLastBreathingViewType != 2 && this.mBreathingNotificationEntry != null) {
            this.mRemoteCallback.setAction(1, this.mBreathingNotificationEntry.minedData);
        } else if (this.mLastBreathingViewType == 2) {
            this.mRemoteCallback.setAction(2, null);
        } else {
            Log.w(TAG, "error view type " + this.mLastBreathingViewType);
            return;
        }
        if (!this.mKeyguardManager.isKeyguardSecure() || !this.mKeyguardManager.isKeyguardLocked()) {
            requestToDismissKeyguard();
            try {
                this.mRemoteCallback.sendResult(null);
            } catch (RemoteException e) {
            }
        } else {
            Intent intent = new Intent("com.motorola.internal.policy.impl.REQUEST_UNLOCK");
            com.motorola.aon.env.android.content.Intent.putExtra(intent, "callback", this.mRemoteCallback.asBinder());
            intent.putExtra("dismiss", true);
            this.mContext.sendBroadcast(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void requestToDismissKeyguard() {
    }

    /* access modifiers changed from: protected */
    public boolean canDismissKeyguard() {
        return false;
    }

    /* access modifiers changed from: protected */
    public Class getServiceClass() throws RuntimeException {
        throw new RuntimeException("Need to override this in the subclass");
    }
}
