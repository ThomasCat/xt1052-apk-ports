package com.motorola.aon.env.android.app;

import android.os.IBinder;
import android.util.Log;
import java.lang.reflect.Method;

public class ActivityManagerNative {
    private static String CLASS_NAME = "android.app.ActivityManagerNative";
    private static final String METHOD_AS_INTERFACE = "asInterface";
    private static final String METHOD_GET_DEFAULT = "getDefault";
    private static final String TAG = "Aon_ActivityManagerNative";
    public static boolean sInitialized;
    private static Method sMethodAsInterface;
    private static Method sMethodGetDefault;

    static {
        sInitialized = false;
        try {
            Class<?> clazz = Class.forName(CLASS_NAME);
            sMethodAsInterface = clazz.getDeclaredMethod(METHOD_AS_INTERFACE, new Class[]{IBinder.class});
            sMethodGetDefault = clazz.getDeclaredMethod(METHOD_GET_DEFAULT, new Class[0]);
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize class");
        }
    }

    public static IActivityManager asInterface(IBinder service) {
        if (sInitialized) {
            try {
                return new IActivityManager(sMethodAsInterface.invoke(null, new Object[]{service}));
            } catch (Throwable th) {
                Log.w(TAG, "unable to inovke asInterface");
            }
        }
        return null;
    }

    public static IActivityManager getDefault() {
        if (sInitialized && sMethodGetDefault != null) {
            try {
                return new IActivityManager(sMethodGetDefault.invoke(null, new Object[0]));
            } catch (Throwable th) {
                Log.w(TAG, "unable to inovke getDefault");
            }
        }
        return null;
    }
}
