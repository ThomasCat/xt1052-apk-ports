package com.motorola.aon.env.com.android.internal.statusbar;

import android.os.IBinder;
import android.util.Log;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class CommandQueue {
    public static String CLASS_COMMAND_QUEUE = "com.android.internal.statusbar.CommandQueue";
    public static String CLASS_COMMAND_QUEUE_CALL_BACK = "com.android.internal.statusbar.CommandQueue$Callbacks";
    public static String CLASS_IEND_POINT = "com.android.internal.statusbar.IEndPoint";
    /* access modifiers changed from: private */
    public static String METHOD_ADD_NOTIFICATION = "addNotification";
    /* access modifiers changed from: private */
    public static String METHOD_REMOVE_NOTIFICATION = "removeNotification";
    /* access modifiers changed from: private */
    public static String METHOD_UPDATE_NOTIFICATION = "updateNotification";
    private static final String TAG = "Aon_CommandQueue";
    /* access modifiers changed from: private */
    public final Callbacks mCallbacks;
    Object mObjCommandQueue;
    Object mObjCommandQueueCallbacks;

    public interface Callbacks {
        void addNotification(IBinder iBinder, MinedData minedData);

        void removeNotification(IBinder iBinder);

        void updateNotification(IBinder iBinder, MinedData minedData);
    }

    public Object getCommandQueueObject() {
        return this.mObjCommandQueue;
    }

    public CommandQueue(Callbacks cb) {
        this.mCallbacks = cb;
        try {
            Class<?> clsCommandQueue = Class.forName(CLASS_COMMAND_QUEUE);
            Class<?> clsCommandQueueCallbacks = Class.forName(CLASS_COMMAND_QUEUE_CALL_BACK);
            Constructor<?> commandQueueConstructor = clsCommandQueue.getDeclaredConstructor(new Class[]{clsCommandQueueCallbacks});
            this.mObjCommandQueueCallbacks = Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{clsCommandQueueCallbacks}, new InvocationHandler() {
                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                    int argLen;
                    String methodName = method.getName();
                    if (args != null) {
                        argLen = args.length;
                    } else {
                        argLen = 0;
                    }
                    if (CommandQueue.METHOD_UPDATE_NOTIFICATION.equals(methodName) && argLen == 2) {
                        CommandQueue.this.mCallbacks.updateNotification(args[0], MinedData.from(args[1]));
                    } else if (CommandQueue.METHOD_REMOVE_NOTIFICATION.equals(methodName) && argLen == 1) {
                        CommandQueue.this.mCallbacks.removeNotification(args[0]);
                    } else if (CommandQueue.METHOD_ADD_NOTIFICATION.equals(methodName) && argLen == 2) {
                        CommandQueue.this.mCallbacks.addNotification(args[0], MinedData.from(args[1]));
                    }
                    return null;
                }
            });
            try {
                this.mObjCommandQueue = commandQueueConstructor.newInstance(new Object[]{this.mObjCommandQueueCallbacks});
            } catch (Throwable th) {
                Log.w(TAG, "unable to create command queue");
            }
        } catch (Throwable th2) {
            Log.w(TAG, "unable to get command queue class");
        }
    }
}
