package com.motorola.aon.env.android.view;

import android.util.Log;

public class WindowManagerPolicy {
    public static String ACTION_HDMI_PLUGGED = null;
    private static final String CLASS_WINDOW_MANAGER_POLICY = "android.view.WindowManagerPolicy";
    public static String EXTRA_HDMI_PLUGGED_STATE = null;
    private static final String FIELD_ACTION_HDMI_PLUGGED = "ACTION_HDMI_PLUGGED";
    private static final String FIELD_EXTRA_HDMI_PLUGGED_STATE = "EXTRA_HDMI_PLUGGED_STATE";
    private static final String TAG = "Aon_WindowManagerPolicy";
    public static boolean sInitialized;

    static {
        ACTION_HDMI_PLUGGED = null;
        EXTRA_HDMI_PLUGGED_STATE = null;
        sInitialized = false;
        try {
            Class<?> clazz = Class.forName(CLASS_WINDOW_MANAGER_POLICY);
            ACTION_HDMI_PLUGGED = (String) clazz.getDeclaredField(FIELD_ACTION_HDMI_PLUGGED).get(null);
            EXTRA_HDMI_PLUGGED_STATE = (String) clazz.getDeclaredField(FIELD_EXTRA_HDMI_PLUGGED_STATE).get(null);
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to intialize class");
        }
    }
}
