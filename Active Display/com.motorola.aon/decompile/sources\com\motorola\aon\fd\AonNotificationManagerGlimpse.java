package com.motorola.aon.fd;

import android.content.Context;
import android.content.Intent;
import com.motorola.aon.AonNotificationManager;
import com.motorola.aon.AonService;
import com.motorola.aon.SnAnalyticsEventService;

public class AonNotificationManagerGlimpse extends AonNotificationManager {
    static final String TAG = "AonNotificationManagerGlimpse";

    public AonNotificationManagerGlimpse(Context context) {
        super(context);
    }

    public boolean getUpdated() {
        return this.mUpdated;
    }

    public void setUpdated(boolean updated) {
        this.mUpdated = updated;
    }

    public void clearNotifications() {
        this.mNotificationEntriesBreathing.clear();
    }

    /* access modifiers changed from: protected */
    public void notifyRefreshBreathingViewOnNotification() {
        SnAnalyticsEventService.setIncomingRefresh(true);
        Intent intent = new Intent(this.mContext, AonServiceGlimpse.class);
        intent.setAction(AonService.ACTION_REFRESH_BREATHING);
        this.mContext.startService(intent);
        SnAnalyticsEventService.mIncomingALS++;
    }

    /* access modifiers changed from: protected */
    public void notifyLaunchTutorialOnNotification() {
        notifyRefreshBreathingViewOnNotification();
    }
}
