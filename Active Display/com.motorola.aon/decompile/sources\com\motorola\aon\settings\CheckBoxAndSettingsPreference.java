package com.motorola.aon.settings;

import android.content.Context;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.motorola.aon.R;
import com.motorola.aon.env.EnvChecker;

public class CheckBoxAndSettingsPreference extends CheckBoxPreference {
    private OnClickListener mCheckBoxClickListener;
    private Context mContext;
    private int mDeviceCount;
    private String mEmailConfigured;
    private boolean mIsUserSetupDone;
    private CheckBoxPreference mParent;
    private Preference mPrivacyPref;
    private OnClickListener mSettingClickListener;
    private ImageView mSettingsButton;
    private boolean mShowLoadingDeviceCount = false;
    private boolean mShowSigningIn = false;
    private TextView mSummaryText;
    private TextView mTitleText;

    public CheckBoxAndSettingsPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        if (this.mContext.getString(R.string.chrome_key).equals(getKey())) {
            setLayoutResource(R.layout.chrome_setting);
            setWidgetLayoutResource(R.layout.preference_checkbox_setting_widget);
            return;
        }
        setLayoutResource(R.layout.preference_checkbox_setting);
        setWidgetLayoutResource(R.layout.preference_checkbox_setting_widget);
    }

    public void setParentCheckPreference(CheckBoxPreference parent) {
        this.mParent = parent;
    }

    public void setSettingClickListener(OnClickListener listener) {
        this.mSettingClickListener = listener;
    }

    public void setCheckBoxClickListener(OnClickListener listener) {
        this.mCheckBoxClickListener = listener;
    }

    /* access modifiers changed from: protected */
    public void onBindView(View view) {
        super.onBindView(view);
        setupCheckBoxPreference(view);
    }

    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        enableSettingsButton(enabled);
    }

    /* access modifiers changed from: protected */
    public void onCheckBoxClicked(View view) {
        if (isChecked()) {
            setChecked(false);
        } else {
            setChecked(true);
        }
        if (this.mCheckBoxClickListener != null) {
            this.mCheckBoxClickListener.onClick(view);
        }
    }

    /* access modifiers changed from: protected */
    public void onSettingsButtonClicked() {
        if (this.mSettingClickListener != null) {
            this.mSettingClickListener.onClick(this.mSettingsButton);
        }
    }

    private void enableSettingsButton(boolean enabled) {
        boolean z;
        boolean z2;
        boolean z3 = true;
        if (this.mSettingsButton != null) {
            boolean checked = isChecked();
            ImageView imageView = this.mSettingsButton;
            if (!checked || !enabled) {
                z = false;
            } else {
                z = true;
            }
            imageView.setEnabled(z);
            ImageView imageView2 = this.mSettingsButton;
            if (!checked || !enabled) {
                z2 = false;
            } else {
                z2 = true;
            }
            imageView2.setClickable(z2);
            ImageView imageView3 = this.mSettingsButton;
            if (!checked || !enabled) {
                z3 = false;
            }
            imageView3.setFocusable(z3);
            if (!checked || !enabled) {
                this.mSettingsButton.setAlpha(0.4f);
            }
        }
        if (this.mTitleText != null) {
            this.mTitleText.setEnabled(enabled);
        }
        if (this.mSummaryText != null) {
            this.mSummaryText.setEnabled(enabled);
        }
    }

    private void setupCheckBoxPreference(View view) {
        view.findViewById(R.id.checkbox_pref).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                CheckBoxAndSettingsPreference.this.onCheckBoxClicked(view);
            }
        });
        this.mSettingsButton = (ImageView) view.findViewById(R.id.settings);
        this.mSettingsButton.setImageDrawable(EnvChecker.getQuickSettingIcon());
        this.mTitleText = (TextView) view.findViewById(16908310);
        this.mSummaryText = (TextView) view.findViewById(16908304);
        this.mSettingsButton.setOnClickListener(new OnClickListener() {
            public void onClick(View clickedView) {
                CheckBoxAndSettingsPreference.this.onSettingsButtonClicked();
            }
        });
        enableSettingsButton(this.mParent != null ? this.mParent.isChecked() : true);
    }
}
