package com.motorola.aon;

import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;
import com.motorola.aon.aidl.IAonNL.Stub;
import com.motorola.aon.aidl.INLCallback;

public class AonNotificationListenerService extends NotificationListenerService {
    /* access modifiers changed from: private */
    public static final boolean DEBUG = Constants.DEBUG;
    protected static final int MAX_RETRY_LOAD_INITIAL_NOTIFICATION = 5;
    protected static final int RETRY_LOAD_INITIAL_NOTIFICATION_INTERVAL = 500;
    private static final String TAG = "AonNotifListenerService";
    private final IBinder mBinder = new Stub() {
        public void registerCallback(INLCallback nlCallback) throws RemoteException {
            AonNotificationListenerService.this.mINLCallback = nlCallback;
            if (AonNotificationListenerService.this.mBoundByNMS) {
                AonNotificationListenerService.this.requestLoadInitialNotifications();
            }
        }

        public void cancelNotification(String pkg, String tag, long id) throws RemoteException {
            cancelNotification(pkg, tag, id);
        }
    };
    /* access modifiers changed from: private */
    public boolean mBoundByNMS;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public INLCallback mINLCallback;
    /* access modifiers changed from: private */
    public int mLoadInitialNotificationCount;
    Runnable mLoadInitialNotificationRunnable = new Runnable() {
        public void run() {
            if (AonNotificationListenerService.DEBUG) {
                Log.d(AonNotificationListenerService.TAG, "mLoadInitialNotificationRunnable- run ");
            }
            StatusBarNotification[] sbns = null;
            try {
                sbns = AonNotificationListenerService.this.getActiveNotifications();
            } catch (Exception e) {
                Log.w(AonNotificationListenerService.TAG, e.getMessage());
                if (AonNotificationListenerService.this.mLoadInitialNotificationCount < 5) {
                    if (AonNotificationListenerService.DEBUG) {
                        Log.d(AonNotificationListenerService.TAG, "retry loading init notifications " + AonNotificationListenerService.this.mLoadInitialNotificationCount);
                    }
                    AonNotificationListenerService.this.mLoadInitialNotificationCount = AonNotificationListenerService.this.mLoadInitialNotificationCount + 1;
                    AonNotificationListenerService.this.mHandler.removeCallbacks(AonNotificationListenerService.this.mLoadInitialNotificationRunnable);
                    AonNotificationListenerService.this.mHandler.postDelayed(AonNotificationListenerService.this.mLoadInitialNotificationRunnable, 500);
                } else if (AonNotificationListenerService.DEBUG) {
                    Log.d(AonNotificationListenerService.TAG, "abort loading init notifications");
                }
            }
            if (sbns != null) {
                for (StatusBarNotification sbn : sbns) {
                    AonNotificationListenerService.this.onNotificationPosted(sbn);
                }
            }
        }
    };

    public void onNotificationPosted(StatusBarNotification noti) {
        if (this.mINLCallback != null) {
            try {
                this.mINLCallback.onNotificationPosted(noti);
            } catch (RemoteException e) {
            }
        }
    }

    public void onNotificationRemoved(StatusBarNotification noti) {
        if (this.mINLCallback != null) {
            if (DEBUG) {
                Log.d(TAG, "onNotificationRemoved : " + noti);
            }
            try {
                this.mINLCallback.onNotificationRemoved(noti);
            } catch (RemoteException e) {
            }
        }
    }

    public IBinder onBind(Intent intent) {
        if (intent == null || !"android.service.notification.NotificationListenerService".equals(intent.getAction())) {
            if (DEBUG) {
                Log.d(TAG, "Bind from AonService");
            }
            return this.mBinder;
        }
        if (DEBUG) {
            Log.d(TAG, "Bind from NMS");
        }
        if (this.mINLCallback != null) {
            requestLoadInitialNotifications();
        }
        this.mBoundByNMS = true;
        return super.onBind(intent);
    }

    /* access modifiers changed from: private */
    public void requestLoadInitialNotifications() {
        if (DEBUG) {
            Log.d(TAG, "requestLoadInitialNotifications ");
        }
        this.mLoadInitialNotificationCount = 0;
        this.mHandler.removeCallbacks(this.mLoadInitialNotificationRunnable);
        this.mHandler.postDelayed(this.mLoadInitialNotificationRunnable, 500);
    }
}
