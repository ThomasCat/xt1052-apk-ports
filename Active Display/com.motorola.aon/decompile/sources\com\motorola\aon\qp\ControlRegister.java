package com.motorola.aon.qp;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import com.motorola.aon.Constants;
import com.motorola.aon.R;
import com.motorola.aon.jni.PartialDisplay;
import com.motorola.aon.jni.PartialDisplay.PanelInfo;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class ControlRegister extends Register {
    private static final int BREATH_STATE_TIMES_COUNT = 8;
    private static final int BYTE_COUNT_TOUCH_COORDINATES = 6;
    /* access modifiers changed from: private */
    public static final boolean DEBUG = Constants.DEBUG;
    private static final int MAX_PARTIAL_REGIONS = 8;
    private static final int MAX_RAMP_STEPS_COUNT = 31;
    private static final String TAG = "Aon_ControlRegister";
    private int mAoDInterfaceVersion;
    private int[] mBreathingTimes;
    private int mBrightness;
    private int mByteIndex158;
    private int mByteIndex159;
    private int mByteIndex160;
    private int mByteIndex161;
    private int mByteIndex162;
    private int mByteIndex163;
    private int mByteIndex164;
    private int mByteIndex165;
    private int mByteIndex166;
    private int mByteIndex167;
    private int mByteIndex168;
    private int mByteIndex169;
    private int mByteIndex2;
    private int mByteIndex3;
    private int mByteIndex4;
    private int mCenterIconX;
    private int mCenterIconY;
    private int[] mClockOnlyPartialRegions;
    private int mClockX;
    private int mClockY;
    private int mDefaultIncomingPlateau;
    private int mEnhancedViewXOffset;
    private int mFlatExitCooldown;
    private int mFlatExitHysteresis;
    private int mHshakeCooldown;
    private int mHshakeCountIncr;
    private int mHshakeCountThreshold;
    private int mHshakeThresholdX;
    private int mHshakeThresholdY;
    private int mHshakeThresholdZ;
    private int mLateTouchTimerForPeek;
    private int mLazyTouchTimer;
    private int mMovementThreshold;
    private int mMovementTime;
    private int mNoMovementTime;
    private int mNotificationIconsX;
    private int mNotificationIconsY;
    private int mOrigCenterIconX;
    private int mOrigCenterIconY;
    private int mOrigClockX;
    private int mOrigClockY;
    private TouchRegion mOrigTouchRegionMiddle;
    private int mPanelControllerDriverVersion;
    private int mPanelControllerVersion;
    private int[] mPartialRegions;
    private int[] mRampDownSteps;
    private int mRampStepsCount;
    private int[] mRampUpSteps;
    private int[] mReserve;
    private int mStowedTime;
    private int[] mTouchDownCoordinates;
    private int mTouchFadex3;
    private int mTouchFadex4;
    private int mTouchMutex1;
    private int mTouchMutex2;
    private int mTouchMutex5;
    private int mTouchMutex6;
    private int mTouchMutey1;
    private int mTouchMutey2;
    public TouchRegion mTouchRegionDown;
    public TouchRegion mTouchRegionMiddle;
    public TouchRegion mTouchRegionUpper;
    private int[] mTouchReleaseDownCoordinates;
    private int[] mTouchReleaseTopCoordinates;
    private int mTransitionDelay;

    public static class TouchRegion {
        int mLeftTopX;
        int mLeftTopY;
        int mRightBottomX;
        int mRightBottomY;

        public TouchRegion(TouchRegion that) {
            this.mLeftTopX = that.mLeftTopX;
            this.mLeftTopY = that.mLeftTopY;
            this.mRightBottomX = that.mRightBottomX;
            this.mRightBottomY = that.mRightBottomY;
        }

        public void offset(TouchRegion that, int x, int y) {
            this.mLeftTopX = that.mLeftTopX + x;
            this.mLeftTopY = that.mLeftTopY + y;
            this.mRightBottomX = that.mRightBottomX + x;
            this.mRightBottomY = that.mRightBottomY + y;
        }

        public TouchRegion() {
        }

        public void dump() {
            if (ControlRegister.DEBUG) {
                Log.d(ControlRegister.TAG, "region (" + this.mLeftTopX + "," + this.mRightBottomX + "," + this.mLeftTopY + "," + this.mRightBottomY + ")");
            }
        }

        public boolean inRegion(int x, int y) {
            if (ControlRegister.DEBUG) {
                Log.i(ControlRegister.TAG, "(" + x + "," + y + ") (" + this.mLeftTopX + "," + this.mRightBottomX + "," + this.mLeftTopY + "," + this.mRightBottomY + ")");
            }
            return x >= this.mLeftTopX && x <= this.mRightBottomX && y >= this.mLeftTopY && y <= this.mRightBottomY;
        }
    }

    ControlRegister() {
        this.mRegisterSize = 201;
        this.mRegisterID = 13;
        this.mPanelControllerVersion = 0;
        this.mPanelControllerDriverVersion = 0;
    }

    public void setBrightness(int brightness) {
        this.mBrightness = brightness;
    }

    public void setTransitionDealy(int delay) {
        this.mTransitionDelay = delay;
    }

    public void setClockEnable(boolean enable) {
        this.mByteIndex2 &= -129;
        this.mByteIndex2 = ((enable ? 1 : 0) << 7) | this.mByteIndex2;
    }

    public void setEnhancedUI(boolean enhanced) {
        this.mByteIndex2 &= -65;
        this.mByteIndex2 = ((enhanced ? 1 : 0) << 6) | this.mByteIndex2;
    }

    public void setFlatupEnable(boolean enable) {
        this.mByteIndex2 &= -33;
        this.mByteIndex2 = ((enable ? 1 : 0) << 5) | this.mByteIndex2;
    }

    public void setReserve(int value) {
        this.mByteIndex2 &= -17;
        this.mByteIndex2 |= (value & 1) << 4;
    }

    public void setGuesturePlateauTime(int value) {
        this.mByteIndex2 &= -16;
        this.mByteIndex2 |= value & 15;
    }

    public void disableHaptics(boolean disable) {
        this.mByteIndex3 &= -129;
        this.mByteIndex3 = ((disable ? 1 : 0) << 7) | this.mByteIndex3;
    }

    public void setGestureOnlyMode(boolean guestureOnly) {
        this.mByteIndex3 &= -65;
        this.mByteIndex3 = ((guestureOnly ? 1 : 0) << 6) | this.mByteIndex3;
    }

    public void setNotificationDisable(boolean disable) {
        this.mByteIndex3 &= -33;
        this.mByteIndex3 = ((disable ? 1 : 0) << 5) | this.mByteIndex3;
    }

    public void setClockFormat(int value) {
        this.mByteIndex3 &= -17;
        this.mByteIndex3 |= (value & 1) << 4;
    }

    public void setPartialRegionsCount(int value) {
        if (value > 8) {
            value = 8;
        }
        this.mByteIndex3 &= -16;
        this.mByteIndex3 |= value & 15;
    }

    public void setIncomingPlateauTime(int value) {
        this.mByteIndex4 &= -241;
        this.mByteIndex4 |= (value & 15) << 4;
    }

    public void setIncomingPlateauTime(boolean isIncoming) {
        setIncomingPlateauTime(isIncoming ? this.mDefaultIncomingPlateau : 0);
    }

    public void setActivePlateauTime(int value) {
        this.mByteIndex4 &= -16;
        this.mByteIndex4 |= value & 15;
    }

    public void setRampSteps(int value) {
        if (value > MAX_RAMP_STEPS_COUNT) {
            value = MAX_RAMP_STEPS_COUNT;
        }
        this.mRampStepsCount = value;
    }

    public void setTouchMuteX1(int value) {
        this.mByteIndex158 = value >> 4;
        this.mByteIndex160 &= -241;
        this.mByteIndex160 |= (value & 15) << 4;
    }

    public void setTouchMuteX2(int value) {
        this.mByteIndex159 = value >> 4;
        this.mByteIndex160 &= -16;
        this.mByteIndex160 |= value & 15;
    }

    public void setTouchFadeX3(int value) {
        this.mByteIndex161 = value >> 4;
        this.mByteIndex163 &= -241;
        this.mByteIndex163 |= (value & 15) << 4;
    }

    public void setTouchFadeX4(int value) {
        this.mByteIndex162 = value >> 4;
        this.mByteIndex163 &= -16;
        this.mByteIndex163 |= value & 15;
    }

    public void setTouchMuteX5(int value) {
        this.mByteIndex164 = value >> 4;
        this.mByteIndex166 &= -241;
        this.mByteIndex166 |= (value & 15) << 4;
    }

    public void setTouchMuteX6(int value) {
        this.mByteIndex165 = value >> 4;
        this.mByteIndex166 &= -16;
        this.mByteIndex166 |= value & 15;
    }

    public void setTouchMuteY1(int value) {
        this.mByteIndex167 = value >> 4;
        this.mByteIndex169 &= -241;
        this.mByteIndex169 |= (value & 15) << 4;
    }

    public void setTouchMuteY2(int value) {
        this.mByteIndex168 = value >> 4;
        this.mByteIndex169 &= -16;
        this.mByteIndex169 |= value & 15;
    }

    public void setTouchDownCoordinates(TouchRegion tg) {
        this.mTouchDownCoordinates = new int[6];
        this.mTouchDownCoordinates[0] = tg.mLeftTopX >> 4;
        this.mTouchDownCoordinates[1] = tg.mLeftTopY >> 4;
        this.mTouchDownCoordinates[2] = ((tg.mLeftTopX & 15) << 4) | (tg.mLeftTopY & 15);
        this.mTouchDownCoordinates[3] = tg.mRightBottomX >> 4;
        this.mTouchDownCoordinates[4] = tg.mRightBottomY >> 4;
        this.mTouchDownCoordinates[5] = ((tg.mRightBottomX & 15) << 4) | (tg.mRightBottomY & 15);
    }

    public void setTouchReleasTopCoordinates(TouchRegion tg) {
        this.mTouchReleaseTopCoordinates = new int[6];
        this.mTouchReleaseTopCoordinates[0] = tg.mLeftTopX >> 4;
        this.mTouchReleaseTopCoordinates[1] = tg.mLeftTopY >> 4;
        this.mTouchReleaseTopCoordinates[2] = ((tg.mLeftTopX & 15) << 4) | (tg.mLeftTopY & 15);
        this.mTouchReleaseTopCoordinates[3] = tg.mRightBottomX >> 4;
        this.mTouchReleaseTopCoordinates[4] = tg.mRightBottomY >> 4;
        this.mTouchReleaseTopCoordinates[5] = ((tg.mRightBottomX & 15) << 4) | (tg.mRightBottomY & 15);
    }

    public void setTouchReleasDownCoordinates(TouchRegion tg) {
        this.mTouchReleaseDownCoordinates = new int[6];
        this.mTouchReleaseDownCoordinates[0] = tg.mLeftTopX >> 4;
        this.mTouchReleaseDownCoordinates[1] = tg.mLeftTopY >> 4;
        this.mTouchReleaseDownCoordinates[2] = ((tg.mLeftTopX & 15) << 4) | (tg.mLeftTopY & 15);
        this.mTouchReleaseDownCoordinates[3] = tg.mRightBottomX >> 4;
        this.mTouchReleaseDownCoordinates[4] = tg.mRightBottomY >> 4;
        this.mTouchReleaseDownCoordinates[5] = ((tg.mRightBottomX & 15) << 4) | (tg.mRightBottomY & 15);
    }

    public void setAoDInterfaceVersion(int version) {
        this.mAoDInterfaceVersion = version;
    }

    public void loadDefault(Context context) {
        Resources res = context.getResources();
        setTransitionDealy(res.getInteger(R.integer.transition_delay));
        setFlatupEnable(res.getBoolean(R.bool.flatup_enable));
        disableHaptics(res.getBoolean(R.bool.haptics_disable));
        setGuesturePlateauTime(res.getInteger(R.integer.gesture_plateau));
        this.mDefaultIncomingPlateau = res.getInteger(R.integer.incoming_plateau);
        setIncomingPlateauTime(this.mDefaultIncomingPlateau);
        setActivePlateauTime(res.getInteger(R.integer.plateau_time));
        this.mPartialRegions = new int[16];
        this.mPartialRegions[0] = (short) res.getDimensionPixelSize(R.dimen.region0_start);
        this.mPartialRegions[1] = res.getDimensionPixelSize(R.dimen.region0_end);
        this.mPartialRegions[2] = res.getDimensionPixelSize(R.dimen.region1_start);
        this.mPartialRegions[3] = res.getDimensionPixelSize(R.dimen.region1_end);
        this.mPartialRegions[4] = res.getDimensionPixelSize(R.dimen.region2_start);
        this.mPartialRegions[5] = res.getDimensionPixelSize(R.dimen.region2_end);
        this.mPartialRegions[6] = res.getDimensionPixelSize(R.dimen.region3_start);
        this.mPartialRegions[7] = res.getDimensionPixelSize(R.dimen.region3_end);
        this.mPartialRegions[8] = res.getDimensionPixelSize(R.dimen.region4_start);
        this.mPartialRegions[9] = res.getDimensionPixelSize(R.dimen.region4_end);
        this.mPartialRegions[10] = res.getDimensionPixelSize(R.dimen.region5_start);
        this.mPartialRegions[11] = res.getDimensionPixelSize(R.dimen.region5_end);
        this.mPartialRegions[12] = res.getDimensionPixelSize(R.dimen.region6_start);
        this.mPartialRegions[13] = res.getDimensionPixelSize(R.dimen.region6_end);
        this.mPartialRegions[14] = res.getDimensionPixelSize(R.dimen.region7_start);
        this.mPartialRegions[15] = res.getDimensionPixelSize(R.dimen.region7_end);
        for (int i = 0; i < this.mPartialRegions.length; i++) {
            Log.d(TAG, "region " + (i / 2) + " " + this.mPartialRegions[i]);
        }
        setPartialRegionsCount(this.mPartialRegions.length / 2);
        this.mRampUpSteps = res.getIntArray(R.array.ramp_up_stage);
        this.mRampDownSteps = res.getIntArray(R.array.ramp_down_stage);
        setRampSteps(this.mRampUpSteps.length);
        this.mOrigClockX = res.getDimensionPixelSize(R.dimen.clock_start_X);
        this.mOrigClockY = res.getDimensionPixelSize(R.dimen.clock_start_Y);
        this.mEnhancedViewXOffset = res.getDimensionPixelSize(R.dimen.enhanced_view_clock_left_offset);
        this.mClockX = this.mOrigClockX;
        this.mClockY = this.mOrigClockY;
        this.mClockOnlyPartialRegions = new int[16];
        this.mClockOnlyPartialRegions[0] = this.mPartialRegions[0];
        this.mClockOnlyPartialRegions[1] = this.mPartialRegions[1];
        this.mClockOnlyPartialRegions[2] = this.mPartialRegions[0];
        this.mClockOnlyPartialRegions[3] = this.mPartialRegions[3];
        this.mClockOnlyPartialRegions[4] = this.mPartialRegions[0];
        this.mClockOnlyPartialRegions[5] = this.mPartialRegions[5];
        this.mClockOnlyPartialRegions[6] = this.mPartialRegions[0];
        this.mClockOnlyPartialRegions[7] = this.mPartialRegions[7];
        this.mClockOnlyPartialRegions[8] = this.mPartialRegions[8];
        this.mClockOnlyPartialRegions[9] = res.getDimensionPixelSize(R.dimen.region_clock_only_end);
        this.mClockOnlyPartialRegions[10] = this.mPartialRegions[10];
        this.mClockOnlyPartialRegions[11] = res.getDimensionPixelSize(R.dimen.region_clock_only_end);
        this.mClockOnlyPartialRegions[12] = this.mPartialRegions[12];
        this.mClockOnlyPartialRegions[13] = res.getDimensionPixelSize(R.dimen.region_clock_only_end);
        this.mClockOnlyPartialRegions[14] = this.mPartialRegions[14];
        this.mClockOnlyPartialRegions[15] = res.getDimensionPixelSize(R.dimen.region_clock_only_end);
        this.mReserve = new int[2];
        this.mFlatExitHysteresis = res.getInteger(R.integer.flatup_exit_hysteresis);
        this.mFlatExitCooldown = res.getInteger(R.integer.flatup_exit_cooldown);
        this.mHshakeThresholdX = res.getInteger(R.integer.hard_shake_threshold_x);
        this.mHshakeThresholdY = res.getInteger(R.integer.hard_shake_threshold_y);
        this.mHshakeThresholdZ = res.getInteger(R.integer.hard_shake_threshold_z);
        this.mHshakeCountThreshold = res.getInteger(R.integer.hard_shake_count_threshold);
        this.mHshakeCountIncr = res.getInteger(R.integer.hard_shake_count_increment);
        this.mHshakeCooldown = res.getInteger(R.integer.hard_shake_cooldown);
        this.mMovementTime = res.getInteger(R.integer.movement_time);
        this.mNoMovementTime = res.getInteger(R.integer.no_movement_time);
        this.mMovementThreshold = res.getInteger(R.integer.movement_threshold);
        this.mStowedTime = res.getInteger(R.integer.stowed_time);
        this.mNotificationIconsX = res.getDimensionPixelSize(R.dimen.notification_icons_X);
        this.mNotificationIconsY = res.getDimensionPixelSize(R.dimen.notification_icons_Y);
        this.mOrigCenterIconX = res.getDimensionPixelSize(R.dimen.center_icon_X);
        this.mOrigCenterIconY = res.getDimensionPixelSize(R.dimen.center_icon_Y);
        this.mCenterIconX = this.mOrigCenterIconX;
        this.mCenterIconY = this.mOrigCenterIconY;
        this.mTouchMutex1 = res.getDimensionPixelSize(R.dimen.touch_region_mute_x1);
        setTouchMuteX1(this.mTouchMutex1);
        this.mTouchMutex2 = res.getDimensionPixelSize(R.dimen.touch_region_mute_x2);
        setTouchMuteX2(this.mTouchMutex2);
        this.mTouchFadex3 = res.getDimensionPixelSize(R.dimen.touch_region_fade_x3);
        setTouchFadeX3(this.mTouchFadex3);
        this.mTouchFadex4 = res.getDimensionPixelSize(R.dimen.touch_region_fade_x4);
        setTouchFadeX4(this.mTouchFadex4);
        this.mTouchMutex5 = res.getDimensionPixelSize(R.dimen.touch_region_mute_x5);
        setTouchMuteX5(this.mTouchMutex5);
        this.mTouchMutex6 = res.getDimensionPixelSize(R.dimen.touch_region_mute_x6);
        setTouchMuteX6(this.mTouchMutex6);
        this.mTouchMutey1 = res.getDimensionPixelSize(R.dimen.touch_region_mute_y1);
        setTouchMuteY1(this.mTouchMutey1);
        this.mTouchMutey2 = res.getDimensionPixelSize(R.dimen.touch_region_mute_y2);
        setTouchMuteY2(this.mTouchMutey2);
        this.mBreathingTimes = res.getIntArray(R.array.breathing_times);
        if (this.mBreathingTimes.length != 8) {
            Log.e(TAG, "number of breathing times[ " + this.mBreathingTimes.length + "]not correct, expecting" + 8);
            this.mBreathingTimes = null;
        }
        this.mTouchRegionUpper = new TouchRegion();
        this.mTouchRegionUpper.mLeftTopX = res.getDimensionPixelSize(R.dimen.touch_region_upper_x0);
        this.mTouchRegionUpper.mLeftTopY = res.getDimensionPixelSize(R.dimen.touch_region_upper_y0);
        this.mTouchRegionUpper.mRightBottomX = res.getDimensionPixelSize(R.dimen.touch_region_upper_x1);
        this.mTouchRegionUpper.mRightBottomY = res.getDimensionPixelSize(R.dimen.touch_region_upper_y1);
        setTouchReleasTopCoordinates(this.mTouchRegionUpper);
        this.mTouchRegionMiddle = new TouchRegion();
        this.mTouchRegionMiddle.mLeftTopX = res.getDimensionPixelSize(R.dimen.touch_region_middle_x0);
        this.mTouchRegionMiddle.mLeftTopY = res.getDimensionPixelSize(R.dimen.touch_region_middle_y0);
        this.mTouchRegionMiddle.mRightBottomX = res.getDimensionPixelSize(R.dimen.touch_region_middle_x1);
        this.mTouchRegionMiddle.mRightBottomY = res.getDimensionPixelSize(R.dimen.touch_region_middle_y1);
        this.mOrigTouchRegionMiddle = new TouchRegion(this.mTouchRegionMiddle);
        setTouchDownCoordinates(this.mTouchRegionMiddle);
        this.mTouchRegionDown = new TouchRegion();
        this.mTouchRegionDown.mLeftTopX = res.getDimensionPixelSize(R.dimen.touch_region_down_x0);
        this.mTouchRegionDown.mLeftTopY = res.getDimensionPixelSize(R.dimen.touch_region_down_y0);
        this.mTouchRegionDown.mRightBottomX = res.getDimensionPixelSize(R.dimen.touch_region_down_x1);
        this.mTouchRegionDown.mRightBottomY = res.getDimensionPixelSize(R.dimen.touch_region_down_y1);
        setTouchReleasDownCoordinates(this.mTouchRegionDown);
        this.mLazyTouchTimer = res.getInteger(R.integer.lazy_touch_timer);
        this.mLateTouchTimerForPeek = res.getInteger(R.integer.late_touch_timer);
        this.mAoDInterfaceVersion = res.getInteger(R.integer.aod_interface_version);
        setReserve(this.mAoDInterfaceVersion);
        setAoDInterfaceVersion(this.mAoDInterfaceVersion);
    }

    public void loadPanelInfo() {
        if (this.mPanelControllerVersion == 0 && this.mPanelControllerDriverVersion == 0) {
            if (DEBUG) {
                Log.d(TAG, "PanelControllerVersion & PanelControllerDriverVersion are 0");
            }
            PanelInfo pInfo = new PanelInfo();
            PartialDisplay.nativeGetPanelInfo(pInfo);
            this.mPanelControllerVersion = pInfo.mControllerVersion;
            this.mPanelControllerDriverVersion = pInfo.mControllerDriverVersion;
        }
        Log.d(TAG, "PanelControllerVersion[" + this.mPanelControllerVersion + "] PanelControllerDriverVersion[" + this.mPanelControllerDriverVersion + "]");
    }

    public void setBreathingViewPostion(int offX, int offY, boolean showBattery) {
        this.mClockX = this.mOrigClockX + offX;
        setEnhancedUI(showBattery);
        if (showBattery) {
            this.mClockX -= this.mEnhancedViewXOffset;
        }
        if (this.mClockX % 4 != 0) {
            Log.d(TAG, " mClockX=" + this.mClockX + " mOrigClockX=" + this.mOrigClockX + " mEnhancedViewXOffset=" + this.mEnhancedViewXOffset + " offX=" + offX + " showBattery=" + showBattery);
            this.mClockX += 4 - (this.mClockX % 4);
            if (DEBUG) {
                throw new RuntimeException("clock x not times of 4");
            }
        }
        this.mClockY = this.mOrigClockY + offY;
        this.mCenterIconX = this.mOrigCenterIconX + offX;
        this.mCenterIconY = this.mOrigCenterIconY + offY;
        this.mTouchRegionMiddle.offset(this.mOrigTouchRegionMiddle, offX, offY);
        if (DEBUG) {
            Log.d(TAG, "offset " + offX + "," + offY + " CLOCK:" + this.mClockX + "," + this.mClockY);
            Log.d(TAG, "Touch Region:");
            this.mTouchRegionMiddle.dump();
        }
        setTouchDownCoordinates(this.mTouchRegionMiddle);
    }

    /* access modifiers changed from: protected */
    public byte[] toByteArray() {
        ByteArrayOutputStream controlByteArrayOut = new ByteArrayOutputStream(this.mRegisterSize);
        DataOutputStream controlDataOut = new DataOutputStream(controlByteArrayOut);
        try {
            controlDataOut.writeByte(this.mBrightness);
            controlDataOut.writeByte(this.mTransitionDelay);
            controlDataOut.writeByte(this.mByteIndex2);
            controlDataOut.writeByte(this.mByteIndex3);
            controlDataOut.writeByte(this.mByteIndex4);
            controlDataOut.writeByte(this.mRampStepsCount);
            for (int s : this.mPartialRegions) {
                controlDataOut.writeShort(s);
            }
            for (int i = 0; i < 8 - this.mPartialRegions.length; i++) {
                controlDataOut.writeShort(0);
            }
            controlDataOut.writeShort(this.mClockY);
            controlDataOut.writeShort(this.mClockX);
            for (int i2 : this.mRampUpSteps) {
                controlDataOut.writeByte(i2);
            }
            for (int i3 = 0; i3 < 31 - this.mRampUpSteps.length; i3++) {
                controlDataOut.writeByte(0);
            }
            for (int i4 : this.mRampDownSteps) {
                controlDataOut.writeByte(i4);
            }
            for (int i5 = 0; i5 < 31 - this.mRampDownSteps.length; i5++) {
                controlDataOut.writeByte(0);
            }
            for (int s2 : this.mClockOnlyPartialRegions) {
                controlDataOut.writeShort(s2);
            }
            for (int i6 = 0; i6 < 8 - this.mClockOnlyPartialRegions.length; i6++) {
                controlDataOut.writeShort(0);
            }
            for (int s3 : this.mReserve) {
                controlDataOut.writeByte(s3);
            }
            controlDataOut.writeByte(this.mFlatExitHysteresis);
            controlDataOut.writeByte(this.mFlatExitCooldown);
            controlDataOut.writeByte(this.mHshakeThresholdX);
            controlDataOut.writeByte(this.mHshakeThresholdY);
            controlDataOut.writeByte(this.mHshakeThresholdZ);
            controlDataOut.writeByte(this.mHshakeCountThreshold);
            controlDataOut.writeByte(this.mHshakeCountIncr);
            controlDataOut.writeByte(this.mHshakeCooldown);
            controlDataOut.writeByte(this.mMovementTime);
            controlDataOut.writeByte(this.mNoMovementTime);
            controlDataOut.writeByte(this.mMovementThreshold);
            controlDataOut.writeByte(this.mStowedTime);
            controlDataOut.writeShort(this.mNotificationIconsY);
            controlDataOut.writeShort(this.mNotificationIconsX);
            controlDataOut.writeShort(this.mCenterIconY);
            controlDataOut.writeShort(this.mCenterIconX);
            controlDataOut.writeByte(this.mByteIndex158);
            controlDataOut.writeByte(this.mByteIndex159);
            controlDataOut.writeByte(this.mByteIndex160);
            controlDataOut.writeByte(this.mByteIndex161);
            controlDataOut.writeByte(this.mByteIndex162);
            controlDataOut.writeByte(this.mByteIndex163);
            controlDataOut.writeByte(this.mByteIndex164);
            controlDataOut.writeByte(this.mByteIndex165);
            controlDataOut.writeByte(this.mByteIndex166);
            controlDataOut.writeByte(this.mByteIndex167);
            controlDataOut.writeByte(this.mByteIndex168);
            controlDataOut.writeByte(this.mByteIndex169);
            if (this.mBreathingTimes != null) {
                for (int t : this.mBreathingTimes) {
                    controlDataOut.writeByte(t);
                }
            }
            for (int c : this.mTouchDownCoordinates) {
                controlDataOut.writeByte(c);
            }
            for (int c2 : this.mTouchReleaseTopCoordinates) {
                controlDataOut.writeByte(c2);
            }
            for (int c3 : this.mTouchReleaseDownCoordinates) {
                controlDataOut.writeByte(c3);
            }
            controlDataOut.writeByte(this.mLazyTouchTimer);
            controlDataOut.writeByte(this.mLateTouchTimerForPeek);
            controlDataOut.writeByte(this.mPanelControllerVersion);
            controlDataOut.writeByte(this.mPanelControllerDriverVersion);
            controlDataOut.writeByte(this.mAoDInterfaceVersion);
        } catch (IOException e) {
            Log.e(TAG, "Writing Exception", e);
        }
        Log.d(TAG, "control_regsiter_data[" + controlByteArrayOut.size() + "]");
        StringBuilder sb = new StringBuilder();
        for (byte b : controlByteArrayOut.toByteArray()) {
            sb.append(String.format("%02X ", new Object[]{Byte.valueOf(b)}));
        }
        Log.d(TAG, "Control Regiter dump:" + sb);
        return controlByteArrayOut.toByteArray();
    }
}
