package com.motorola.aon;

import android.os.Build;
import com.motorola.aon.env.android.os.SystemProperties;

public abstract class Constants {
    public static final String ACTION_CANCEL_NOTIFICATION = "com.motorola.aon.CANCEL_NOTIFICATION";
    public static final String ACTION_DISMISS_KEYGUARD = "com.motorola.aon.DISMISS_KEYGUARD";
    public static final String ACTION_EXIT_AONBLANK = "com.motorola.aon.EXIT_AONBLANK";
    public static final String ACTION_REGISTER_NOTIFICATION_LISTENER = "com.motorola.sn.REGISTER";
    public static final String ACTION_UNREGISTER_NOTIFICATION_LISTENER = "com.motorola.sn.UNREGISTER";
    public static final String AON_PREFS_NAME = "Aon_Prefs";
    public static final boolean CURRENT_DRAIN_DEBUG = SystemProperties.getBoolean("debug.aon.current_drain", false);
    public static final boolean DEBUG = (!PRODUCTION_MODE);
    public static final String EXTRA_ID = "EXTRA_ID";
    public static final String EXTRA_PACKAGE_NAME = "package";
    public static final String EXTRA_SERVICE_NAME = "service";
    public static final String EXTRA_TAG = "EXTRA_TAG";
    public static final String GLIMPSE_SERVICE = "com.motorola.aon.glimpse_service";
    public static final int INVALID_VERSION = -1;
    public static final int METHOD_IS_GLIMPSE_ACTIVATED = 1;
    public static final int METHOD_IS_GLIMPSE_OFF_SCREEN = 3;
    public static final int METHOD_IS_GLIMPSE_ON_SCREEN = 2;
    public static final int METHOD_IS_POWER_KEY_PRESSED = 4;
    public static final int METHOD_UPDATE_ACTIVITY_STATE = 5;
    public static final String PREF_KEY_WHITE_LIST_SETTING_LAUNCHED = "white_list_setting_launched";
    public static final boolean PRODUCTION_MODE = "user".equals(Build.TYPE);
    public static final String QUICK_PEEK_FEATURE = "com.motorola.aon.quickpeek";
    public static final String SDCARD_DUMP_DIR = "/sdcard/Aod";
    public static final String SN_ANALYTICS_SETTINGS = "com.motorola.aon.SN_ANALYTICS_SETTINGS";
    public static final String TAG_BRIGHTNESS = "aod_brightness";
    public static boolean isPartialDisplay = SystemProperties.getBoolean("ro.partial.display", false);

    public interface ActivityState {
        public static final int ALIVE = 0;
        public static final int DESTROYED = 2;
        public static final int FINISHING = 1;
    }

    public interface FdServiceMessage {
        public static final int END_GLIMPSE = 2;
        public static final int GESTURE_EXTEND_GLIMPSE = 11;
        public static final int GESTURE_START_GLIMPSE = 0;
        public static final int GLIMPSE_ALIVE = 3;
        public static final int GLIMPSE_DESTROYED = 13;
        public static final int GLIMPSE_EXIT = 4;
        public static final int GLIMPSE_MESSENGER = 12;
        public static final int GLIMPSE_MUTE_PEEK = 6;
        public static final int GLIMPSE_SLEEP_PHONE = 5;
        public static final int NOTIFICATION_START_GLIMPSE = 1;
        public static final int READ_BRIGHTNESS = 9;
        public static final int STOP_GLIMPSE = 10;
        public static final int STOP_SERVICE = 7;
        public static final int UPDATE_BRIGHTNESS = 8;
        public static final int UPDATE_NOTIFICATION = 14;
    }
}
