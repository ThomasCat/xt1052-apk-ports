package com.motorola.aon.fd;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.graphics.Rect;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import com.motorola.aon.AonService;
import com.motorola.aon.Constants;
import com.motorola.aon.Constants.FdServiceMessage;
import com.motorola.aon.R;
import com.motorola.aon.SnAnalyticsEventService;
import com.motorola.aon.env.android.os.ServiceManager;
import java.util.Timer;
import java.util.TimerTask;

public class AonGlimpseActivity extends Activity {
    private static final float BRIGHTNESS_LIMIT_FACTOR = 0.75f;
    private static final boolean CURRENT_DRAIN_DEBUG = Constants.CURRENT_DRAIN_DEBUG;
    /* access modifiers changed from: private */
    public static final boolean DEBUG = AonService.DEBUG;
    private static final long DIM_PHASE_LENGTH = 50;
    private static final long DIM_TOUCH_RELEASE_DELAY = 700;
    private static final String GLIMPSE_SIMULATE_SCREEN_ON_ACTION = "com.motorola.internal.policy.impl.GLIMPSE_SIMULATE_SCREEN_ON_ACTION";
    private static final int MAX_BRIGHTNESS = 140;
    private static final float MUTEREGION_BRIGHTNESS_FACTOR = 0.2f;
    private static final float NORMAL_BRIGHTNESS_FACTOR = 1.0f;
    private static final String PERMISSION_EXIT_AONBLANK = "com.motorola.aon.permission.EXIT_AONBLANK";
    private static final int RAMP_UP_TOTAL_STEPS = 8;
    private static final long STOP_GLIMPSE_DELAY = 100;
    private static final String TAG = "AonGlimpseActivity";
    private static final String TAG_BRIGHTNESS = "aod_brightness";
    private int[] BRIGHTNESS_TABLE = {9, 14, 18, 46, 67, 93, MAX_BRIGHTNESS};
    /* access modifiers changed from: private */
    public boolean mAODfade = false;
    /* access modifiers changed from: private */
    public long mAodBrightnessEndTime = 0;
    /* access modifiers changed from: private */
    public AonViewManagerGlimpse mAonViewManager;
    /* access modifiers changed from: private */
    public boolean mBound = false;
    private Object mBrightnessLock = new Object();
    /* access modifiers changed from: private */
    public int mCurrentBrightness = 0;
    /* access modifiers changed from: private */
    public float mCurrentDimPhase = 0.0f;
    /* access modifiers changed from: private */
    public Runnable mDimRunnable = new Runnable() {
        public void run() {
            AonGlimpseActivity.this.mRampingDown = true;
            AonGlimpseActivity.this.stopBrightnessRampUp();
            if (AonGlimpseActivity.this.mCurrentDimPhase < 0.0f || AonGlimpseActivity.this.mCurrentBrightness <= 0) {
                AonGlimpseActivity.this.mHandler.removeCallbacks(AonGlimpseActivity.this.mFinishRunnable);
                AonGlimpseActivity.this.mHandler.postDelayed(AonGlimpseActivity.this.mFinishRunnable, AonGlimpseActivity.this.mLateTouchTimer);
                return;
            }
            AonGlimpseActivity.this.setBrightness((AonGlimpseActivity.this.mCurrentDimPhase / ((float) AonGlimpseActivity.this.mTotalDimPhase)) * AonGlimpseActivity.this.mFactorSet);
            AonGlimpseActivity.this.mCurrentDimPhase = AonGlimpseActivity.this.mCurrentDimPhase - AonGlimpseActivity.NORMAL_BRIGHTNESS_FACTOR;
            AonGlimpseActivity.this.mHandler.removeCallbacks(AonGlimpseActivity.this.mDimRunnable);
            AonGlimpseActivity.this.mHandler.postDelayed(AonGlimpseActivity.this.mDimRunnable, AonGlimpseActivity.DIM_PHASE_LENGTH);
        }
    };
    /* access modifiers changed from: private */
    public float mFactorSet = NORMAL_BRIGHTNESS_FACTOR;
    /* access modifiers changed from: private */
    public Runnable mFinish = new Runnable() {
        public void run() {
            Intent intent = new Intent(AonGlimpseActivity.this, AonGlimpseBlank.class);
            intent.putExtra("AODfade", AonGlimpseActivity.this.mAODfade);
            AonGlimpseActivity.this.startActivity(intent);
            AonGlimpseActivity.this.finish();
            AonGlimpseActivity.this.overridePendingTransition(0, 0);
        }
    };
    /* access modifiers changed from: private */
    public Runnable mFinishRunnable = new Runnable() {
        public void run() {
            if (AonGlimpseActivity.DEBUG) {
                Log.d(AonGlimpseActivity.TAG, "finishRunnable run");
            }
            AonGlimpseActivity.this.mAODfade = true;
            AonGlimpseActivity.this.mHandler.removeCallbacks(AonGlimpseActivity.this.mSimpleFinishRunnable);
            AonGlimpseActivity.this.mHandler.post(AonGlimpseActivity.this.mSimpleFinishRunnable);
            AonGlimpseActivity.this.SendMessageToService(5);
        }
    };
    private DeathRecipient mGlimpseDeathHandler = new DeathRecipient() {
        public void binderDied() {
            synchronized (AonGlimpseActivity.this.mGlimpseLock) {
                AonGlimpseActivity.this.mGlimpseService = null;
            }
        }
    };
    /* access modifiers changed from: private */
    public final Object mGlimpseLock = new Object();
    /* access modifiers changed from: private */
    public IBinder mGlimpseService = null;
    /* access modifiers changed from: private */
    public View mGlimpseView;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 2:
                    if (!AonGlimpseActivity.this.mIsFinishing) {
                        AonGlimpseActivity.this.mIsFinishing = true;
                        AonGlimpseActivity.this.mHandler.post(AonGlimpseActivity.this.mFinish);
                        return;
                    } else if (AonGlimpseActivity.DEBUG) {
                        Log.d(AonGlimpseActivity.TAG, "finish() is in processing");
                        return;
                    } else {
                        return;
                    }
                case 8:
                    AonGlimpseActivity.this.updateBrightness(msg.arg1);
                    return;
                case FdServiceMessage.GESTURE_EXTEND_GLIMPSE /*11*/:
                    if (!AonGlimpseActivity.this.mValidPressDown && AonGlimpseActivity.this.mRampingDown) {
                        AonGlimpseActivity.this.schduleDimBrightness(AonGlimpseActivity.DIM_TOUCH_RELEASE_DELAY);
                        return;
                    }
                    return;
                case FdServiceMessage.UPDATE_NOTIFICATION /*14*/:
                    AonGlimpseActivity.this.UpdateGlimpseTime(true);
                    return;
                default:
                    Log.e(AonGlimpseActivity.TAG, "Unknown Message encounted: " + msg.what);
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean mIsFinishing = false;
    private KeyguardManager mKeyguardManager;
    /* access modifiers changed from: private */
    public long mLateTouchTimer;
    /* access modifiers changed from: private */
    public long mLazyTouchTimer;
    /* access modifiers changed from: private */
    public Messenger mMessenger = new Messenger(this.mHandler);
    private long mNotificationPlateau = 5000;
    /* access modifiers changed from: private */
    public int mOldTouchingArea = 7;
    private OnKeyListener mOnKeyListener = new OnKeyListener() {
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            switch (keyCode) {
                case 24:
                case 25:
                    AudioManager audioManager = (AudioManager) AonGlimpseActivity.this.getSystemService("audio");
                    if (!audioManager.isMusicActive()) {
                        return true;
                    }
                    if (AonGlimpseActivity.DEBUG) {
                        Log.d(AonGlimpseActivity.TAG, "isMusicActive");
                    }
                    com.motorola.aon.env.android.media.AudioManager.adjustLocalOrRemoteStreamVolume(audioManager, 3, keyCode == 24 ? 1 : -1);
                    return true;
                default:
                    return false;
            }
        }
    };
    private OnTouchListener mOnTouchListener = new OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            boolean handled = false;
            if (AonGlimpseActivity.this.mIsFinishing) {
                return true;
            }
            if (AonGlimpseActivity.this.mCurrentBrightness == 0) {
                long elapsed = System.currentTimeMillis() - AonGlimpseActivity.this.mAodBrightnessEndTime;
                if (AonGlimpseActivity.DEBUG) {
                    Log.d("aod_brightness", "elapse time = " + Float.toString((float) elapsed));
                }
                if (elapsed > AonGlimpseActivity.this.mLateTouchTimer) {
                    return true;
                }
            }
            float x = event.getX();
            AonGlimpseActivity.this.mTouchingArea = AonGlimpseActivity.this.findTouchingArea(x, event.getY());
            if ((event.getAction() == 1 || (event.getActionMasked() == 6 && event.getActionIndex() == 0)) && AonGlimpseActivity.this.mValidPressDown) {
                AonGlimpseActivity.this.mValidPressDown = false;
                AonGlimpseActivity.this.mHandler.removeCallbacks(AonGlimpseActivity.this.mPeekLazyRunnable);
                if (AonGlimpseActivity.DEBUG) {
                    Log.d(AonGlimpseActivity.TAG, "action_up");
                }
                if (AonGlimpseActivity.this.mPeekingStartTime > 0) {
                    SnAnalyticsEventService.recordPeekingTime(SystemClock.uptimeMillis() - AonGlimpseActivity.this.mPeekingStartTime);
                    AonGlimpseActivity.this.mPeekingStartTime = 0;
                }
                switch (AonGlimpseActivity.this.mTouchingArea) {
                    case 0:
                        if (AonGlimpseActivity.this.mAonViewManager.getBreathSize() > 0 || AonGlimpseActivity.this.mViewType == 2) {
                            AonGlimpseActivity.this.launchNotification();
                            AonGlimpseActivity.this.dismissAOD(0);
                            SnAnalyticsEventService.recordLaunchCount();
                        } else {
                            AonGlimpseActivity.this.reShowGlimpse();
                        }
                        handled = true;
                        break;
                    case 1:
                    case 4:
                    case 5:
                    case 7:
                        if (!AonGlimpseActivity.this.mPeekLazyRunning) {
                            AonGlimpseActivity.this.reShowGlimpse();
                            handled = true;
                        }
                        SnAnalyticsEventService.recordTouchInvalidRelease();
                        break;
                    case 2:
                        Intent intent = new Intent(AonGlimpseActivity.this, AonServiceGlimpse.class);
                        intent.setAction(AonServiceGlimpse.ACTION_UNLOCK);
                        AonGlimpseActivity.this.startService(intent);
                        AonGlimpseActivity.this.mAonViewManager.unlock();
                        AonGlimpseActivity.this.dismissAOD(AonGlimpseActivity.STOP_GLIMPSE_DELAY);
                        SnAnalyticsEventService.recordUnlockCount();
                        handled = true;
                        break;
                    case 3:
                    case 6:
                        if (AonGlimpseActivity.this.mAonViewManager.getBreathSize() > 0) {
                            AonGlimpseActivity.this.SendMessageToService(6);
                            AonGlimpseActivity.this.mHandler.removeCallbacks(AonGlimpseActivity.this.mFinishRunnable);
                            AonGlimpseActivity.this.mHandler.post(AonGlimpseActivity.this.mFinishRunnable);
                            SnAnalyticsEventService.recordMuteCount();
                        } else {
                            AonGlimpseActivity.this.reShowGlimpse();
                        }
                        handled = true;
                        break;
                    default:
                        Log.e(AonGlimpseActivity.TAG, "should not be here");
                        break;
                }
            } else if (event.getAction() == 2 && AonGlimpseActivity.this.mValidPressDown) {
                if (AonGlimpseActivity.this.mTouchingArea != AonGlimpseActivity.this.mOldTouchingArea) {
                    if (AonGlimpseActivity.this.mTouchingArea == 0 || AonGlimpseActivity.this.mTouchingArea == 2 || AonGlimpseActivity.this.mTouchingArea == 1 || AonGlimpseActivity.this.mTouchingArea == 7) {
                        AonGlimpseActivity.this.setBrightness(AonGlimpseActivity.NORMAL_BRIGHTNESS_FACTOR);
                    } else if (AonGlimpseActivity.this.mAonViewManager.getBreathSize() > 0 && (AonGlimpseActivity.this.mTouchingArea == 3 || AonGlimpseActivity.this.mTouchingArea == 6)) {
                        AonGlimpseActivity.this.setBrightness(AonGlimpseActivity.MUTEREGION_BRIGHTNESS_FACTOR);
                    }
                    if (AonGlimpseActivity.this.mAonViewManager.getBreathSize() > 0 || AonGlimpseActivity.this.mViewType == 2) {
                        if ((AonGlimpseActivity.this.mTouchingArea == 0 || AonGlimpseActivity.this.mTouchingArea == 2 || AonGlimpseActivity.this.mTouchingArea == 3 || AonGlimpseActivity.this.mTouchingArea == 6) && AonGlimpseActivity.this.mbHapticsEnabled) {
                            AonGlimpseActivity.this.mGlimpseView.performHapticFeedback(1);
                        }
                    } else if (AonGlimpseActivity.this.mTouchingArea == 2 && AonGlimpseActivity.this.mbHapticsEnabled) {
                        AonGlimpseActivity.this.mGlimpseView.performHapticFeedback(1);
                    }
                }
                if ((AonGlimpseActivity.this.mAonViewManager.getBreathSize() > 0 || AonGlimpseActivity.this.mViewType == 2) && (AonGlimpseActivity.this.mTouchingArea == 4 || AonGlimpseActivity.this.mTouchingArea == 5)) {
                    AonGlimpseActivity.this.fadeBrightness(x);
                }
                AonGlimpseActivity.this.mOldTouchingArea = AonGlimpseActivity.this.mTouchingArea;
                handled = true;
            } else if (event.getAction() == 0) {
                if (AonGlimpseActivity.this.mTouchingArea == 1) {
                    AonGlimpseActivity.this.mValidPressDown = true;
                    AonGlimpseActivity.this.mPeekLazyRunning = false;
                    AonGlimpseActivity.this.mHandler.removeCallbacks(AonGlimpseActivity.this.mPeekLazyRunnable);
                    AonGlimpseActivity.this.mHandler.postDelayed(AonGlimpseActivity.this.mPeekLazyRunnable, AonGlimpseActivity.this.mLazyTouchTimer);
                    AonGlimpseActivity.this.stopBrightnessRampUp();
                    AonGlimpseActivity.this.mHandler.removeCallbacks(AonGlimpseActivity.this.mDimRunnable);
                    AonGlimpseActivity.this.mRampingDown = false;
                    AonGlimpseActivity.this.setBrightness(AonGlimpseActivity.NORMAL_BRIGHTNESS_FACTOR);
                    AonGlimpseActivity.this.mHandler.removeCallbacks(AonGlimpseActivity.this.mSimpleFinishRunnable);
                    AonGlimpseActivity.this.mAonViewManager.showPeekDetail();
                    AonGlimpseActivity.this.mHandler.removeCallbacks(AonGlimpseActivity.this.mFinishRunnable);
                    if (AonGlimpseActivity.this.mbHapticsEnabled) {
                        AonGlimpseActivity.this.mGlimpseView.performHapticFeedback(1);
                    }
                    AonGlimpseActivity.this.mPeekingStartTime = SystemClock.uptimeMillis();
                    SnAnalyticsEventService.recordTouchValid();
                    handled = true;
                } else {
                    SnAnalyticsEventService.recordTouchInvalid();
                }
            }
            return handled;
        }
    };
    /* access modifiers changed from: private */
    public Runnable mPeekLazyRunnable = new Runnable() {
        public void run() {
            AonGlimpseActivity.this.mPeekLazyRunning = true;
            if (AonGlimpseActivity.this.mbHapticsEnabled) {
                AonGlimpseActivity.this.mGlimpseView.performHapticFeedback(1);
            }
            if (AonGlimpseActivity.this.mAonViewManager != null) {
                AonGlimpseActivity.this.mAonViewManager.showGlimpse();
            }
            AonGlimpseActivity.this.schduleDimBrightness(AonGlimpseActivity.DIM_TOUCH_RELEASE_DELAY);
        }
    };
    /* access modifiers changed from: private */
    public boolean mPeekLazyRunning = false;
    /* access modifiers changed from: private */
    public long mPeekingStartTime = 0;
    /* access modifiers changed from: private */
    public int mRampUpSteps = 0;
    private Timer mRampUpTimer = null;
    /* access modifiers changed from: private */
    public boolean mRampingDown = false;
    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if (AonGlimpseActivity.DEBUG) {
                    Log.d(AonGlimpseActivity.TAG, "receive intent " + (action != null ? action : "null"));
                }
                if ("android.intent.action.PHONE_STATE".equals(action)) {
                    if (TelephonyManager.EXTRA_STATE_RINGING.equals(intent.getStringExtra("state"))) {
                        AonGlimpseActivity.this.dismissAOD(0);
                    }
                } else if ("android.intent.action.ACTION_POWER_CONNECTED".equals(action) || "android.intent.action.ACTION_POWER_DISCONNECTED".equals(action)) {
                    AonGlimpseActivity.this.dismissAOD(0);
                }
            }
        }
    };
    private final Rect[] mRectArray = {this.mRectTouchUpper, this.mRectTouchMiddle, this.mRectTouchDown, this.mRectTouchMuteLeft, this.mRectTouchFadeLeft, this.mRectTouchFadeRight, this.mRectTouchMuteRight};
    private Rect mRectTouchDown = new Rect();
    private Rect mRectTouchFadeLeft = new Rect();
    private Rect mRectTouchFadeRight = new Rect();
    private Rect mRectTouchMiddle = new Rect();
    private Rect mRectTouchMuteLeft = new Rect();
    private Rect mRectTouchMuteRight = new Rect();
    private Rect mRectTouchUpper = new Rect();
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            AonGlimpseActivity.this.mBound = true;
            if (service != null) {
                AonGlimpseActivity.this.mServiceMessenger = new Messenger(service);
            }
            if (AonGlimpseActivity.this.mServiceMessenger != null) {
                try {
                    Message msg = Message.obtain(null, 12);
                    msg.replyTo = AonGlimpseActivity.this.mMessenger;
                    AonGlimpseActivity.this.mServiceMessenger.send(msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            AonGlimpseActivity.this.mBound = false;
            AonGlimpseActivity.this.mServiceMessenger = null;
        }
    };
    /* access modifiers changed from: private */
    public Messenger mServiceMessenger;
    private int mSetBrightness = 0;
    private long mShowTimePlateau = 3000;
    /* access modifiers changed from: private */
    public Runnable mSimpleFinishRunnable = new Runnable() {
        public void run() {
            if (AonGlimpseActivity.DEBUG) {
                Log.d(AonGlimpseActivity.TAG, "simplefinish run");
            }
            AonGlimpseActivity.this.mHandler.sendEmptyMessage(2);
        }
    };
    private int mSnPrivacy;
    private int mTargetBrightness = 0;
    /* access modifiers changed from: private */
    public int mTotalDimPhase = 20;
    /* access modifiers changed from: private */
    public int mTouchingArea = 7;
    /* access modifiers changed from: private */
    public boolean mValidPressDown = false;
    /* access modifiers changed from: private */
    public int mViewType;
    private Window mWin = null;
    WindowManager mWindowManager;
    /* access modifiers changed from: private */
    public boolean mbHapticsEnabled = false;
    Runnable rampUpRunnable = new Runnable() {
        public void run() {
            AonGlimpseActivity.this.setBrightnessBySteps(AonGlimpseActivity.this.mRampUpSteps);
            AonGlimpseActivity.this.mRampUpSteps = AonGlimpseActivity.this.mRampUpSteps + 1;
            if (AonGlimpseActivity.this.mRampUpSteps >= 8) {
                AonGlimpseActivity.this.stopBrightnessRampUp();
            }
        }
    };

    class RampUpTimerTask extends TimerTask {
        RampUpTimerTask() {
        }

        public void run() {
            AonGlimpseActivity.this.mHandler.post(AonGlimpseActivity.this.rampUpRunnable);
        }
    }

    private interface TouchingRegion {
        public static final int REGION_BREATH = 1;
        public static final int REGION_FADE_LEFT = 4;
        public static final int REGION_FADE_RIGHT = 5;
        public static final int REGION_HOME = 2;
        public static final int REGION_MUTE_LEFT = 3;
        public static final int REGION_MUTE_RIGHT = 6;
        public static final int REGION_NOTIFICATION = 0;
        public static final int REGION_OTHER = 7;
    }

    /* access modifiers changed from: private */
    public void schduleDimBrightness(long delayMillis) {
        this.mCurrentDimPhase = (float) this.mTotalDimPhase;
        setBrightness(BRIGHTNESS_LIMIT_FACTOR);
        stopTimers();
        this.mHandler.postDelayed(this.mDimRunnable, delayMillis);
    }

    /* access modifiers changed from: private */
    public void reShowGlimpse() {
        Intent updateIntent = new Intent(getApplicationContext(), AonServiceGlimpse.class);
        updateIntent.setAction(AonService.ACTION_REFRESH_BREATHING);
        startService(updateIntent);
        this.mAonViewManager.showGlimpse();
        UpdateGlimpseTime(false);
    }

    /* access modifiers changed from: private */
    public void SendMessageToService(int message) {
        if (this.mServiceMessenger != null) {
            try {
                this.mServiceMessenger.send(Message.obtain(null, message));
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public int findTouchingArea(float x, float y) {
        for (int i = 0; i < this.mRectArray.length; i++) {
            if (this.mRectArray[i].contains((int) x, (int) y)) {
                return i + 0;
            }
        }
        return 7;
    }

    /* access modifiers changed from: private */
    public void fadeBrightness(float x) {
        float xFromFade = 0.0f;
        int xFadeWidth = this.mRectTouchFadeLeft.right - this.mRectTouchFadeLeft.left;
        if (this.mTouchingArea == 4) {
            xFromFade = x - ((float) this.mRectTouchFadeLeft.left);
        } else if (this.mTouchingArea == 5) {
            xFromFade = ((float) this.mRectTouchFadeRight.right) - x;
        }
        setBrightness(((xFromFade / ((float) xFadeWidth)) * 0.8f) + MUTEREGION_BRIGHTNESS_FACTOR);
    }

    /* access modifiers changed from: private */
    public void UpdateGlimpseTime(boolean updated) {
        if (this.mAonViewManager.isPeeking()) {
            return;
        }
        if (!updated) {
            schduleDimBrightness(DIM_TOUCH_RELEASE_DELAY);
        } else if (this.mAonViewManager.getBreathSize() > 0) {
            schduleDimBrightness(this.mNotificationPlateau);
        } else {
            schduleDimBrightness(this.mShowTimePlateau);
        }
    }

    /* access modifiers changed from: private */
    public void dismissAOD(long delay) {
        this.mHandler.removeCallbacks(this.mSimpleFinishRunnable);
        this.mHandler.postDelayed(this.mSimpleFinishRunnable, delay);
        simulateScreenOn();
    }

    private void initTouchRegions() {
        Resources res = getResources();
        this.mRectTouchUpper.left = res.getDimensionPixelSize(R.dimen.touch_region_upper_x0);
        this.mRectTouchUpper.top = res.getDimensionPixelSize(R.dimen.touch_region_upper_y0);
        this.mRectTouchUpper.right = res.getDimensionPixelSize(R.dimen.touch_region_upper_x1);
        this.mRectTouchUpper.bottom = res.getDimensionPixelSize(R.dimen.touch_region_upper_y1);
        this.mRectTouchMiddle.left = res.getDimensionPixelSize(R.dimen.touch_region_middle_x0);
        this.mRectTouchMiddle.top = res.getDimensionPixelSize(R.dimen.touch_region_middle_y0);
        this.mRectTouchMiddle.right = res.getDimensionPixelSize(R.dimen.touch_region_middle_x1);
        this.mRectTouchMiddle.bottom = res.getDimensionPixelSize(R.dimen.touch_region_middle_y1);
        this.mRectTouchDown.left = res.getDimensionPixelSize(R.dimen.touch_region_down_x0);
        this.mRectTouchDown.top = res.getDimensionPixelSize(R.dimen.touch_region_down_y0);
        this.mRectTouchDown.right = res.getDimensionPixelSize(R.dimen.touch_region_down_x1);
        this.mRectTouchDown.bottom = res.getDimensionPixelSize(R.dimen.touch_region_down_y1);
        int dimensionPixelSize = res.getDimensionPixelSize(R.dimen.touch_region_mute_x1);
        int touchMutex2 = res.getDimensionPixelSize(R.dimen.touch_region_mute_x2) - res.getDimensionPixelSize(R.dimen.touch_region_mute_ultram_offset);
        int touchFadex3 = res.getDimensionPixelSize(R.dimen.touch_region_fade_x3);
        int touchFadex4 = res.getDimensionPixelSize(R.dimen.touch_region_fade_x4);
        int touchMutex5 = res.getDimensionPixelSize(R.dimen.touch_region_mute_x5) + res.getDimensionPixelSize(R.dimen.touch_region_mute_ultram_offset);
        int touchMutex6 = res.getDimensionPixelSize(R.dimen.touch_region_mute_x6);
        int touchMutey1 = res.getDimensionPixelSize(R.dimen.touch_region_mute_y1);
        int touchMutey2 = res.getDimensionPixelSize(R.dimen.touch_region_mute_y2);
        this.mRectTouchMuteLeft.left = 0;
        this.mRectTouchMuteLeft.top = touchMutey1;
        this.mRectTouchMuteLeft.right = touchMutex2;
        this.mRectTouchMuteLeft.bottom = touchMutey2;
        this.mRectTouchFadeLeft.left = touchMutex2;
        this.mRectTouchFadeLeft.top = touchMutey1;
        this.mRectTouchFadeLeft.right = touchFadex3;
        this.mRectTouchFadeLeft.bottom = touchMutey2;
        this.mRectTouchFadeRight.left = touchFadex4;
        this.mRectTouchFadeRight.top = touchMutey1;
        this.mRectTouchFadeRight.right = touchMutex5;
        this.mRectTouchFadeRight.bottom = touchMutey2;
        this.mRectTouchMuteRight.left = touchMutex5;
        this.mRectTouchMuteRight.top = touchMutey1;
        this.mRectTouchMuteRight.right = touchMutex6 + 2;
        this.mRectTouchMuteRight.bottom = touchMutey2;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = new Intent(this, AonServiceGlimpse.class);
        intent.setAction(AonServiceGlimpse.ACTION_BIND_AONSERVICEGLIMPSE);
        bindService(intent, this.mServiceConnection, 1);
        getWindow().requestFeature(1);
        overridePendingTransition(0, 0);
        this.mWin = getWindow();
        this.mSetBrightness = getIntent().getIntExtra("BrightNess", this.BRIGHTNESS_TABLE[0]);
        this.mTargetBrightness = this.mSetBrightness;
        for (int i = 0; i < this.BRIGHTNESS_TABLE.length - 1; i++) {
            if (this.BRIGHTNESS_TABLE[i] == this.mSetBrightness) {
                this.mTargetBrightness = this.BRIGHTNESS_TABLE[i + 1];
            }
        }
        LayoutParams param = this.mWin.getAttributes();
        param.flags |= 2622849;
        this.mWin.setAttributes(param);
        this.mKeyguardManager = (KeyguardManager) getSystemService("keyguard");
        onEnableAon_glimpse();
        this.mGlimpseView = this.mAonViewManager.getGlimpseView();
        this.mGlimpseView.setFocusable(true);
        this.mGlimpseView.setFocusableInTouchMode(true);
        this.mGlimpseView.requestFocus();
        this.mGlimpseView.setOnKeyListener(this.mOnKeyListener);
        this.mGlimpseView.setOnTouchListener(this.mOnTouchListener);
        initTouchRegions();
        Resources res = getResources();
        if (res != null) {
            this.mLazyTouchTimer = (long) (res.getInteger(R.integer.lazy_touch_timer) * 1000);
            this.mShowTimePlateau = (long) (res.getInteger(R.integer.gesture_plateau) * 1000);
            this.mNotificationPlateau = (long) (res.getInteger(R.integer.incoming_plateau) * 1000);
            this.mTotalDimPhase = res.getInteger(R.integer.dim_delay) / 50;
            this.mLateTouchTimer = (long) (res.getInteger(R.integer.late_touch_timer) * 125);
        }
        this.mCurrentDimPhase = (float) this.mTotalDimPhase;
        if (this.mAonViewManager.getBreathSize() != 0 || this.mViewType == 2) {
            this.mHandler.postDelayed(this.mDimRunnable, this.mNotificationPlateau);
            if (CURRENT_DRAIN_DEBUG) {
                Log.d(TAG, "Both time and notification are displayed");
            }
        } else {
            this.mHandler.postDelayed(this.mDimRunnable, this.mShowTimePlateau);
            if (CURRENT_DRAIN_DEBUG) {
                Log.d(TAG, "Only time is displayed");
            }
        }
        IntentFilter ifilter = new IntentFilter();
        ifilter.addAction("android.intent.action.PHONE_STATE");
        ifilter.addAction("android.intent.action.ACTION_POWER_CONNECTED");
        ifilter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED");
        registerReceiver(this.mReceiver, ifilter);
        if (DEBUG) {
            Log.d(TAG, "onCreate");
        }
        if (isPowerKeyPressed()) {
            updateActivityState(1);
            if (DEBUG) {
                Log.d(TAG, "Power key is pressed before AOD is shown.");
            }
            if (!this.mIsFinishing) {
                this.mIsFinishing = true;
                finish();
            }
            overridePendingTransition(0, 0);
            return;
        }
        updateActivityState(0);
        showView();
        setBrightness(BRIGHTNESS_LIMIT_FACTOR);
        if (DEBUG) {
            Log.d("aod_brightness", "Initial brightness is " + this.mSetBrightness);
        }
        startBrightnessRampUp();
    }

    private void showView() {
        this.mWindowManager = (WindowManager) getApplicationContext().getSystemService("window");
        LayoutParams param = this.mWin.getAttributes();
        param.type = com.motorola.aon.env.android.view.WindowManager.LayoutParams.TYPE_AOD;
        param.format = 1;
        if (this.mWindowManager != null) {
            this.mWindowManager.addView(this.mGlimpseView, param);
        }
    }

    private void stopTimers() {
        this.mHandler.removeCallbacks(this.mDimRunnable);
        this.mRampingDown = false;
        this.mHandler.removeCallbacks(this.mSimpleFinishRunnable);
        this.mHandler.removeCallbacks(this.mFinishRunnable);
        this.mHandler.removeCallbacks(this.mPeekLazyRunnable);
    }

    public void onResume() {
        super.onResume();
        sendBroadcast(new Intent(Constants.ACTION_EXIT_AONBLANK), PERMISSION_EXIT_AONBLANK);
    }

    public void onPause() {
        super.onPause();
        PowerManager pm = (PowerManager) getSystemService("power");
        if (DEBUG) {
            Log.d(TAG, "onPause finishing:" + this.mIsFinishing + " , screen on:" + pm.isScreenOn());
        }
        if (!this.mIsFinishing && pm.isScreenOn()) {
            stopTimers();
            dismissAOD(0);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.mWindowManager != null) {
            this.mWindowManager.removeViewImmediate(this.mGlimpseView);
        }
        unregisterReceiver(this.mReceiver);
        updateActivityState(2);
        this.mGlimpseService.unlinkToDeath(this.mGlimpseDeathHandler, 0);
        onDisableAon_glimpse();
        this.mWin = null;
        if (this.mBound) {
            unbindService(this.mServiceConnection);
        }
        if (DEBUG) {
            Log.d(TAG, "onDestroy");
        }
    }

    private void onEnableAon_glimpse() {
        boolean z;
        if (DEBUG) {
            Log.d(TAG, "on Enable AON Glimpse activity");
        }
        this.mAonViewManager = AonServiceGlimpse.getViewManager();
        Intent intent = getIntent();
        this.mbHapticsEnabled = intent.getBooleanExtra("HapticsEnabled", false);
        this.mSnPrivacy = intent.getIntExtra("PrivacySetting", 0);
        int tutorialState = intent.getIntExtra("TutorialState", 0);
        if (isSecureGlimpse()) {
            z = true;
        } else {
            z = false;
        }
        if (tutorialState == 2) {
            this.mViewType = 2;
        } else if (z) {
            this.mViewType = 1;
        } else {
            this.mViewType = 0;
        }
    }

    private void onDisableAon_glimpse() {
        if (DEBUG) {
            Log.d(TAG, "on disable AON Glimpse activity");
        }
        stopTimers();
        this.mAonViewManager = null;
    }

    private boolean isSecureGlimpse() {
        if (this.mSnPrivacy == 0 || !this.mKeyguardManager.isKeyguardSecure() || !this.mKeyguardManager.isKeyguardLocked()) {
            return false;
        }
        if (DEBUG) {
            Log.d(TAG, "secure glimpse, secure key guard already active");
        }
        return true;
    }

    private void simulateScreenOn() {
        sendBroadcast(new Intent(GLIMPSE_SIMULATE_SCREEN_ON_ACTION));
    }

    /* access modifiers changed from: private */
    public void launchNotification() {
        Intent intent = new Intent(this, AonServiceGlimpse.class);
        intent.setAction(AonServiceGlimpse.ACTION_LAUNCH_NOTIFICATION);
        startService(intent);
    }

    private IBinder getGlimpseService() {
        IBinder iBinder;
        synchronized (this.mGlimpseLock) {
            if (this.mGlimpseService == null) {
                this.mGlimpseService = ServiceManager.checkService(Constants.GLIMPSE_SERVICE);
                try {
                    if (this.mGlimpseService != null) {
                        this.mGlimpseService.linkToDeath(this.mGlimpseDeathHandler, 0);
                    }
                } catch (RemoteException e) {
                    this.mGlimpseService = null;
                    Log.e(TAG, "mGlimpseService.linkToDeath failed!");
                }
            }
            iBinder = this.mGlimpseService;
        }
        return iBinder;
    }

    private boolean isPowerKeyPressed() {
        boolean result = false;
        synchronized (this.mGlimpseLock) {
            IBinder glimpseService = getGlimpseService();
            if (glimpseService != null) {
                Parcel data = Parcel.obtain();
                Parcel reply = Parcel.obtain();
                try {
                    if (glimpseService.transact(4, data, reply, 0)) {
                        if (reply.readInt() == 1) {
                            result = true;
                        } else {
                            result = false;
                        }
                    }
                    reply.recycle();
                    data.recycle();
                } catch (RemoteException ex) {
                    Log.w(TAG, "GLIMPSE_SERVICE threw RemoteException", ex);
                    reply.recycle();
                    data.recycle();
                } catch (Throwable th) {
                    reply.recycle();
                    data.recycle();
                    throw th;
                }
            }
        }
        return result;
    }

    /* access modifiers changed from: private */
    public void updateBrightness(int brightness) {
        if (DEBUG) {
            Log.d("aod_brightness", "get a new brightness value: " + brightness);
        }
        synchronized (this.mBrightnessLock) {
            this.mTargetBrightness = brightness;
            if (this.mTargetBrightness > MAX_BRIGHTNESS) {
                this.mTargetBrightness = MAX_BRIGHTNESS;
            }
        }
    }

    /* access modifiers changed from: private */
    public void setBrightness(float factor) {
        if (this.mWin != null) {
            float brightnessFactor = (((float) this.mSetBrightness) / 255.0f) * factor;
            this.mCurrentBrightness = (int) (brightnessFactor * 255.0f);
            LayoutParams param = this.mWin.getAttributes();
            param.screenBrightness = brightnessFactor;
            com.motorola.aon.env.android.view.WindowManager.LayoutParams.setMinScreenBrightness(param, 0);
            this.mWin.setAttributes(param);
            this.mFactorSet = factor;
            if (this.mCurrentBrightness == 0) {
                this.mAodBrightnessEndTime = System.currentTimeMillis();
            }
        }
        ((PowerManager) getSystemService("power")).userActivity(SystemClock.uptimeMillis(), false);
    }

    /* access modifiers changed from: private */
    public void setBrightnessBySteps(int step) {
        int targetBn;
        synchronized (this.mBrightnessLock) {
            targetBn = this.mTargetBrightness;
        }
        if (targetBn <= this.mSetBrightness || step >= 8) {
            stopBrightnessRampUp();
            return;
        }
        this.mSetBrightness += (targetBn - this.mSetBrightness) / (8 - step);
        setBrightness(BRIGHTNESS_LIMIT_FACTOR);
        if (DEBUG) {
            Log.d("aod_brightness", "step:" + step + " brightness" + this.mSetBrightness);
        }
    }

    private void startBrightnessRampUp() {
        if (this.mRampUpTimer == null) {
            this.mRampUpSteps = 0;
            this.mRampUpTimer = new Timer();
            this.mRampUpTimer.schedule(new RampUpTimerTask(), 0, 80);
        }
    }

    /* access modifiers changed from: private */
    public void stopBrightnessRampUp() {
        if (this.mRampUpTimer != null) {
            this.mRampUpTimer.cancel();
            this.mRampUpTimer = null;
            this.mHandler.removeCallbacks(this.rampUpRunnable);
        }
    }

    private void updateActivityState(int state) {
        synchronized (this.mGlimpseLock) {
            IBinder glimpseService = getGlimpseService();
            if (glimpseService != null) {
                Parcel data = Parcel.obtain();
                data.writeInt(state);
                Parcel reply = Parcel.obtain();
                try {
                    if (glimpseService.transact(5, data, reply, 0)) {
                    }
                    reply.recycle();
                    data.recycle();
                } catch (RemoteException ex) {
                    Log.w(TAG, "GLIMPSE_SERVICE threw RemoteException", ex);
                    reply.recycle();
                    data.recycle();
                } catch (Throwable th) {
                    reply.recycle();
                    data.recycle();
                    throw th;
                }
            }
        }
    }
}
