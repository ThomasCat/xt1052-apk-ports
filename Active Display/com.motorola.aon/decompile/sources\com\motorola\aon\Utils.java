package com.motorola.aon;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;

public class Utils {
    public static final boolean DEBUG = Constants.DEBUG;
    private static final String TAG = "Aon_Utils";

    public static void dumpBitmaps(Bitmap bmp, String filename) {
        if (DEBUG) {
            Log.d(TAG, "dumpBitmaps bmp=" + bmp + " filename=" + filename);
            if (bmp != null && filename != null) {
                try {
                    File outBmpDir = new File(Constants.SDCARD_DUMP_DIR);
                    outBmpDir.mkdirs();
                    FileOutputStream fos = new FileOutputStream(new File(outBmpDir, filename));
                    bmp.compress(CompressFormat.PNG, 90, fos);
                    fos.close();
                } catch (Exception e) {
                    Log.e(TAG, "error writing to location ", e);
                }
            }
        }
    }
}
