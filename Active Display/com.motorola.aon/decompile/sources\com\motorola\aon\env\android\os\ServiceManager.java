package com.motorola.aon.env.android.os;

import android.os.IBinder;
import android.util.Log;
import java.lang.reflect.Method;

public class ServiceManager {
    private static String ClassManagerName = "android.os.ServiceManager";
    private static String MethodCheckService = "checkService";
    private static String MethodGetService = "getService";
    private static final String TAG = "Aon_ServiceManager";
    public static boolean sInitialized;
    private static Method sMethodCheckService;
    private static Method sMethodGetService;

    static {
        sInitialized = false;
        try {
            Class<?> serviceManagerClass = Class.forName(ClassManagerName);
            sMethodGetService = serviceManagerClass.getDeclaredMethod(MethodGetService, new Class[]{String.class});
            sMethodCheckService = serviceManagerClass.getDeclaredMethod(MethodCheckService, new Class[]{String.class});
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to get service manager class");
        }
    }

    public static IBinder getService(String service) {
        if (sInitialized) {
            try {
                return (IBinder) sMethodGetService.invoke(null, new Object[]{service});
            } catch (Throwable th) {
                Log.w(TAG, "unable to get service");
            }
        }
        return null;
    }

    public static IBinder checkService(String service) {
        if (sInitialized) {
            try {
                return (IBinder) sMethodCheckService.invoke(null, new Object[]{service});
            } catch (Throwable th) {
                Log.w(TAG, "unable to get service");
            }
        }
        return null;
    }
}
