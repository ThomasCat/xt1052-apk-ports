package com.motorola.aon.env.com.android.internal.statusbar;

import android.app.PendingIntent;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class MinedData implements Parcelable {
    private static final String CLASS_MINED_DATA = "com.android.internal.statusbar.MinedData";
    public static final Creator<MinedData> CREATOR = new Creator<MinedData>() {
        public MinedData createFromParcel(Parcel arg0) {
            return null;
        }

        public MinedData[] newArray(int arg0) {
            return null;
        }
    };
    private static final String FIELD_BIG_STYLE = "bigStyle";
    private static final String FIELD_BIG_VIEW_DATA_LIST = "bigViewDataList";
    private static final String FIELD_CONTENT_INTENT = "contentIntent";
    private static final String FIELD_FLAGS = "flags";
    private static final String FIELD_ICON = "icon";
    private static final String FIELD_ICON_BITMAP = "iconBitmap";
    private static final String FIELD_ID = "id";
    private static final String FIELD_LARGE_ICON = "largeIcon";
    private static final String FIELD_PKG = "pkg";
    private static final String FIELD_SCORE = "score";
    private static final String FIELD_SMALL_STYLE = "smallStyle";
    private static final String FIELD_SMALL_VIEW_DATA_LIST = "smallViewDataList";
    private static final String FIELD_TICKER_TEXT = "tickerText";
    private static final String FIELD_TIME_STAMP = "timeStamp";
    private static final String FIELD_WHEN = "when";
    private static final String TAG = "Aon_MinedData";
    static Field sFdBigStyle;
    static Field sFdBigViewDataList;
    static Field sFdContentIntent;
    static Field sFdFlags;
    static Field sFdIconBitmap;
    static Field sFdIcons;
    static Field sFdId;
    static Field sFdLargeIcon;
    static Field sFdPkg;
    static Field sFdScore;
    static Field sFdSmallStyle;
    static Field sFdSmallViewDataList;
    static Field sFdTickerText;
    static Field sFdTimeStamp;
    static Field sFdWhen;
    public static boolean sInitialized;
    public int bigStyle;
    public ArrayList<ContentViewData> bigViewDataList = new ArrayList<>();
    public PendingIntent contentIntent;
    public int flags;
    public int icon;
    public Bitmap iconBitmap;
    public int id;
    public Bitmap largeIcon;
    public String pkg;
    public int score;
    public int smallStyle;
    public ArrayList<ContentViewData> smallViewDataList = new ArrayList<>();
    public CharSequence tickerText;
    public long timeStamp;
    public long when;

    public static class ContentViewData {
        private static final String CLASS_CONTENT_VIEW_DATA = "com.android.internal.statusbar.MinedData$ContentViewData";
        private static final String FIELD_BITMAP = "bitmap";
        private static final String FIELD_ID = "id";
        private static final String FIELD_TEXT = "text";
        private static final String TAG = "ContentViewData";
        static Field sFdBitmap;
        static Field sFdId;
        static Field sFdText;
        private static boolean sInitialized;
        public Bitmap bitmap;
        public int id;
        public CharSequence text;

        static {
            sInitialized = false;
            try {
                Class<?> sClsContentViewData = Class.forName(CLASS_CONTENT_VIEW_DATA);
                sFdId = sClsContentViewData.getDeclaredField(FIELD_ID);
                sFdText = sClsContentViewData.getDeclaredField(FIELD_TEXT);
                sFdBitmap = sClsContentViewData.getDeclaredField(FIELD_BITMAP);
                sInitialized = true;
            } catch (Throwable th) {
                Log.w(TAG, "unable to initialize ContentViewData class");
            }
        }

        static ContentViewData from(Object obj) {
            if (sInitialized) {
                ContentViewData cvd = new ContentViewData();
                try {
                    cvd.id = sFdId.getInt(obj);
                    cvd.text = (CharSequence) sFdText.get(obj);
                    cvd.bitmap = (Bitmap) sFdBitmap.get(obj);
                    return cvd;
                } catch (Throwable th) {
                    Log.w(TAG, "unable to get field for ContentViewData");
                    return cvd;
                }
            } else {
                Log.w(TAG, "not initialized");
                return null;
            }
        }
    }

    static {
        sInitialized = false;
        try {
            Class<?> sClsMinedData = Class.forName(CLASS_MINED_DATA);
            sFdPkg = sClsMinedData.getDeclaredField(FIELD_PKG);
            sFdId = sClsMinedData.getDeclaredField(FIELD_ID);
            sFdScore = sClsMinedData.getDeclaredField(FIELD_SCORE);
            sFdBigStyle = sClsMinedData.getDeclaredField(FIELD_BIG_STYLE);
            sFdSmallStyle = sClsMinedData.getDeclaredField(FIELD_SMALL_STYLE);
            sFdTickerText = sClsMinedData.getDeclaredField(FIELD_TICKER_TEXT);
            sFdFlags = sClsMinedData.getDeclaredField(FIELD_FLAGS);
            sFdIcons = sClsMinedData.getDeclaredField(FIELD_ICON);
            sFdLargeIcon = sClsMinedData.getDeclaredField(FIELD_LARGE_ICON);
            sFdTimeStamp = sClsMinedData.getDeclaredField(FIELD_TIME_STAMP);
            sFdIconBitmap = sClsMinedData.getDeclaredField(FIELD_ICON_BITMAP);
            sFdWhen = sClsMinedData.getDeclaredField(FIELD_WHEN);
            sFdContentIntent = sClsMinedData.getDeclaredField(FIELD_CONTENT_INTENT);
            sFdSmallViewDataList = sClsMinedData.getDeclaredField(FIELD_SMALL_VIEW_DATA_LIST);
            sFdBigViewDataList = sClsMinedData.getDeclaredField(FIELD_BIG_VIEW_DATA_LIST);
            sInitialized = true;
        } catch (Throwable th) {
            sInitialized = false;
            Log.w(TAG, "unable to initialize Mined Data class");
        }
    }

    static void ViewDataListRemote2Local(ArrayList<?> remote, ArrayList<ContentViewData> local) {
        if (remote != null && local != null) {
            for (int i = 0; i < remote.size(); i++) {
                ContentViewData cvd = ContentViewData.from(remote.get(i));
                if (cvd != null) {
                    local.add(cvd);
                }
            }
        }
    }

    static MinedData from(Object obj) {
        if (sInitialized) {
            MinedData md = new MinedData();
            try {
                md.pkg = (String) sFdPkg.get(obj);
                md.id = sFdId.getInt(obj);
                md.score = sFdScore.getInt(obj);
                md.bigStyle = sFdBigStyle.getInt(obj);
                md.smallStyle = sFdSmallStyle.getInt(obj);
                md.tickerText = (CharSequence) sFdTickerText.get(obj);
                md.flags = sFdFlags.getInt(obj);
                md.icon = sFdIcons.getInt(obj);
                md.largeIcon = (Bitmap) sFdLargeIcon.get(obj);
                md.timeStamp = sFdTimeStamp.getLong(obj);
                md.iconBitmap = (Bitmap) sFdIconBitmap.get(obj);
                md.when = sFdWhen.getLong(obj);
                md.contentIntent = (PendingIntent) sFdContentIntent.get(obj);
                ViewDataListRemote2Local((ArrayList) sFdSmallViewDataList.get(obj), md.smallViewDataList);
                ViewDataListRemote2Local((ArrayList) sFdBigViewDataList.get(obj), md.bigViewDataList);
                return md;
            } catch (Throwable th) {
                Log.w(TAG, "unable to get data from MinedData");
                return null;
            }
        } else {
            Log.w(TAG, "not initialized");
            return null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel arg0, int arg1) {
    }
}
