package com.motorola.aon.pd;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;
import android.view.Surface;
import android.view.Surface.OutOfResourcesException;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import com.motorola.aon.AonService;
import com.motorola.aon.AonService.IGetAonInterface;
import com.motorola.aon.AonViewManager;
import com.motorola.aon.Constants;
import com.motorola.aon.R;
import com.motorola.aon.SnAnalyticsEventService;
import com.motorola.aon.aidl.NotificationEntry;
import com.motorola.aon.env.aidl.IAonInterface;
import com.motorola.aon.env.aidl.SurfaceWrapper;
import com.motorola.aon.env.android.os.SystemProperties;
import com.motorola.aon.env.android.view.Display;
import com.motorola.aon.env.android.view.DisplayInfo;
import com.motorola.aon.env.android.view.SurfaceControl;
import com.motorola.aon.env.com.android.internal.statusbar.MinedData;
import java.util.ArrayList;
import java.util.Random;

public class AonViewManagerBreath extends AonViewManager {
    static final int MSG_HIDE_SURFACE = 2;
    static final int MSG_INVALIDATE_SURFACE = 3;
    static final int MSG_UPDATE_BREATHING_VIEW = 1;
    static final String TAG = "AonViewManagerBreath";
    private static int[] mBreathingOffset = {0, 0, 8, 0, 8, -8, 0, -8, -8, -8, -8, 0, -8, 8, 0, 8, 8, 8};
    private View mBreathingContent = null;
    private int mBreathingContentLeft;
    private Handler mBreathingDrawerHandler;
    private Surface mBreathingSurface = com.motorola.aon.env.android.view.Surface.create();
    private int mBreathingViewPos;
    private BreathingViewUpdateThread mBreathingViewUpdateThread;
    private DisplayInfo mDisplayInfo;
    private int mRegion0X;

    class BreathingDrawerHandler extends Handler {
        BreathingDrawerHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    AonViewManagerBreath.this.handleUpdateBreathingView((BreathingViewUpdateReq) msg.obj);
                    return;
                case 2:
                    AonViewManagerBreath.this.handleHideSurface((ICallback) msg.obj);
                    return;
                case 3:
                    AonViewManagerBreath.this.handleInvalidateSurface();
                    return;
                default:
                    return;
            }
        }
    }

    static class BreathingViewUpdateReq {
        int mBatteryStatus;
        ArrayList<NotificationEntry> mBreathingList;
        ICallback mListener;
        int mViewType;

        public BreathingViewUpdateReq(int viewType, ArrayList<NotificationEntry> breathingList, int batteryStatus, ICallback listener) {
            this.mViewType = viewType;
            this.mBreathingList = breathingList;
            this.mBatteryStatus = batteryStatus;
            this.mListener = listener;
        }
    }

    class BreathingViewUpdateThread extends HandlerThread {
        public BreathingViewUpdateThread(String name) {
            super(name, -4);
        }

        /* access modifiers changed from: protected */
        public void onLooperPrepared() {
            AonViewManagerBreath.this.initRes();
        }
    }

    public interface ICallback {
        void sendResult(Bundle bundle);
    }

    public void getBreathingOffset(int pos, int[] offset) {
        if (pos < 0 || pos >= mBreathingOffset.length / 2 || offset == null || offset.length != 2) {
            throw new RuntimeException("unexpected input parameters " + pos);
        }
        offset[0] = mBreathingOffset[pos * 2];
        offset[1] = mBreathingOffset[(pos * 2) + 1];
    }

    public void nextBreathingViewPos() {
        if (this.mBreathingViewPos == (mBreathingOffset.length / 2) - 1) {
            this.mBreathingViewPos = 0;
        } else {
            this.mBreathingViewPos++;
        }
    }

    public AonViewManagerBreath(Context context, AonNotificationManagerBreath aonNotificationManager, IGetAonInterface getAonService) {
        super(context, aonNotificationManager, getAonService);
        if (PersistentManager.mInst.getPartialMode(context) != 0) {
            if (DEBUG) {
                Log.d(TAG, "restore auto rotation");
            }
            try {
                if (this.mIGetAonInterface != null) {
                    IAonInterface aonInterface = this.mIGetAonInterface.get();
                    if (aonInterface != null) {
                        aonInterface.resumeRotation();
                        PersistentManager.mInst.setPauseRotationMode(context, 0);
                    }
                }
            } catch (RemoteException e) {
                Log.w(TAG, "Unable to resume rotation.");
            }
        }
        this.mRegion0X = context.getResources().getDimensionPixelSize(R.dimen.region0_start);
        this.mBreathingContentLeft = context.getResources().getDimensionPixelSize(R.dimen.breathing_content_left_margin);
        this.mBreathingViewPos = new Random(SystemClock.uptimeMillis()).nextInt(mBreathingOffset.length / 2);
        if (DEBUG) {
            Log.d(TAG, "initial breathing position " + this.mBreathingViewPos + ",(" + mBreathingOffset[this.mBreathingViewPos * 2] + "," + mBreathingOffset[(this.mBreathingViewPos * 2) + 1] + ")");
        }
    }

    public void initBreathingThread() {
        this.mBreathingViewUpdateThread = new BreathingViewUpdateThread("breathing drawer");
        this.mBreathingViewUpdateThread.start();
        this.mBreathingDrawerHandler = new BreathingDrawerHandler(this.mBreathingViewUpdateThread.getLooper());
        if (DEBUG) {
            Log.d(TAG, "initBreathingThread");
        }
    }

    public void deInitBreathingThread() {
        if (this.mBreathingViewUpdateThread != null) {
            this.mBreathingViewUpdateThread.quit();
            this.mBreathingDrawerHandler = null;
            this.mBreathingViewUpdateThread = null;
        }
        if (DEBUG) {
            Log.d(TAG, "deInitBreathingThread");
        }
    }

    public Bitmap getNotificationBitmap(MinedData md, boolean big) {
        return big ? md.largeIcon : md.iconBitmap;
    }

    public void requestUpdateBreathingView(int viewType, ArrayList<NotificationEntry> breathingList, int batteryStatus, ICallback listener) {
        if (this.mBreathingDrawerHandler != null) {
            if (DEBUG) {
                Log.d(TAG, "request to update breathing view " + viewType);
            }
            this.mBreathingDrawerHandler.obtainMessage(1, new BreathingViewUpdateReq(viewType, breathingList, batteryStatus, listener)).sendToTarget();
            return;
        }
        Log.e(TAG, "UI thread not running");
    }

    public void requestHideBreathingView(ICallback listener) {
        if (this.mBreathingDrawerHandler != null) {
            if (DEBUG) {
                Log.d(TAG, "request to hide breathing view ");
            }
            this.mBreathingDrawerHandler.removeMessages(1);
            this.mBreathingDrawerHandler.obtainMessage(2, listener).sendToTarget();
            return;
        }
        Log.e(TAG, "UI thread not running");
    }

    /* access modifiers changed from: private */
    public void handleHideSurface(ICallback listener) {
        if ((!DEBUG || !SystemProperties.getBoolean("debug.aon.full_view", false)) && this.mBreathingSurface != null) {
            IAonInterface aonInterface = this.mIGetAonInterface.get();
            if (aonInterface != null) {
                try {
                    aonInterface.hideSurface();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
        if (DEBUG) {
            Log.d(TAG, "resume rotation");
        }
        try {
            if (this.mIGetAonInterface != null) {
                IAonInterface aonInterface2 = this.mIGetAonInterface.get();
                if (aonInterface2 != null) {
                    aonInterface2.resumeRotation();
                    PersistentManager.mInst.setPauseRotationMode(this.mContext, 0);
                }
            }
        } catch (RemoteException e2) {
            Log.w(TAG, "Unable to resume rotation.");
        }
        if (listener != null) {
            listener.sendResult(null);
        }
    }

    /* access modifiers changed from: private */
    public void handleUpdateBreathingView(BreathingViewUpdateReq req) {
        int width;
        int height;
        ICallback listener = req.mListener;
        int viewType = req.mViewType;
        ArrayList<NotificationEntry> breathingList = req.mBreathingList;
        if (viewType == 2) {
            this.mBreathingNotificationEntry = null;
        } else if (breathingList == null || breathingList.size() <= 0) {
            this.mBreathingNotificationEntry = null;
            if (DEBUG) {
                Log.d(TAG, "request update breathing view for clock only mode");
            }
        } else {
            this.mBreathingNotificationEntry = (NotificationEntry) breathingList.get(0);
        }
        if (this.mWindowManager != null) {
            try {
                if (DEBUG) {
                    Log.d(TAG, "pause rotation");
                }
                if (this.mIGetAonInterface != null) {
                    IAonInterface aonInterface = this.mIGetAonInterface.get();
                    if (aonInterface != null) {
                        PersistentManager.mInst.setPauseRotationMode(this.mContext, 1);
                        aonInterface.pauseRotation();
                    }
                }
            } catch (RemoteException e) {
                Log.w(TAG, "Unable to pause rotation.");
            }
        }
        this.mDisplayInfo = Display.getDisplayInfo(this.mDisplay);
        int rot = this.mDisplayInfo.rotation;
        if (rot == 1 || rot == 3) {
            width = this.mDisplayInfo.logicalHeight;
            height = this.mDisplayInfo.logicalWidth;
        } else {
            width = this.mDisplayInfo.logicalWidth;
            height = this.mDisplayInfo.logicalHeight;
        }
        fillBreathingView(this.mCachedBreathingView, breathingList, req.mBatteryStatus, viewType);
        if (this.mBreathingContent == null) {
            this.mBreathingContent = this.mCachedBreathingView.findViewById(R.id.breathing_content);
            if (DEBUG && this.mBreathingContent == null) {
                throw new RuntimeException("wrong breathing view");
            }
        }
        LayoutParams lp = this.mBreathingContent.getLayoutParams();
        if (!DEBUG || (lp instanceof FrameLayout.LayoutParams)) {
            FrameLayout.LayoutParams fllp = (FrameLayout.LayoutParams) lp;
            int brathingViewPos = this.mBreathingViewPos;
            fllp.leftMargin = mBreathingOffset[brathingViewPos * 2] + this.mBreathingContentLeft;
            fllp.topMargin = this.mRegion0X + mBreathingOffset[(brathingViewPos * 2) + 1];
            this.mBreathingContent.setLayoutParams(fllp);
            this.mCachedBreathingView.measure(MeasureSpec.makeMeasureSpec(width, 1073741824), MeasureSpec.makeMeasureSpec(height, 1073741824));
            this.mCachedBreathingView.layout(0, 0, width, height);
            drawView(this.mCachedBreathingView, width, height, rot);
            if (listener != null) {
                Bundle args = new Bundle();
                args.putInt("pos", brathingViewPos);
                listener.sendResult(args);
                return;
            }
            return;
        }
        throw new RuntimeException("wrong breathing view, not in framelayout");
    }

    /* access modifiers changed from: private */
    public void handleInvalidateSurface() {
        this.mBreathingSurface.release();
    }

    private void drawView(View view, int displayWidth, int displayHeight, int rotation) {
        IAonInterface aonInterface = this.mIGetAonInterface.get();
        if (!this.mBreathingSurface.isValid() && aonInterface != null) {
            try {
                aonInterface.createBreathingSurface("breathing", displayWidth, displayHeight, 4, SurfaceControl.HIDDEN, new SurfaceWrapper(this.mBreathingSurface));
            } catch (RemoteException e) {
                e.printStackTrace();
                return;
            }
        }
        if (aonInterface != null) {
            try {
                aonInterface.showSurface(rotation, displayWidth, displayHeight);
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
        try {
            Canvas c = this.mBreathingSurface.lockCanvas(null);
            c.drawColor(-16777216);
            view.draw(c);
            if (DEBUG) {
                Log.d(TAG, "before unlock and post");
            }
            this.mBreathingSurface.unlockCanvasAndPost(c);
            if (DEBUG) {
                Log.d(TAG, "after unlock and post");
            }
            Thread.sleep(70);
        } catch (IllegalArgumentException e3) {
            Log.e(TAG, "error drawing breathing view", e3);
        } catch (OutOfResourcesException e4) {
            Log.e(TAG, "out of memory during drawing breathing view", e4);
        } catch (InterruptedException e5) {
            Log.e(TAG, "error sleep");
        }
    }

    public void handleScreenOnAction(int reason) {
        if (DEBUG) {
            Log.i(TAG, "screen turning on " + reason);
        }
        if (this.mBreathingDrawerHandler != null) {
            this.mBreathingDrawerHandler.removeMessages(1);
        }
        if (reason == 1) {
            SnAnalyticsEventService.mUnlockCount++;
            unlock();
        } else if (reason == 2) {
            SnAnalyticsEventService.mLaunchCount++;
            launchNotification();
        }
    }

    /* access modifiers changed from: protected */
    public void onTutorialLaunched() {
        Intent i2 = new Intent(this.mContext, AonServiceBreath.class);
        i2.setAction(AonService.ACTION_TUTORIAL_DONE);
        this.mContext.startService(i2);
    }

    public void requestInvalidateSurface() {
        if (this.mBreathingDrawerHandler != null) {
            if (DEBUG) {
                Log.d(TAG, "request to invalidate breathing surface ");
            }
            this.mBreathingDrawerHandler.removeMessages(3);
            this.mBreathingDrawerHandler.obtainMessage(3).sendToTarget();
            return;
        }
        Log.e(TAG, "UI thread not running");
    }

    /* access modifiers changed from: protected */
    public void requestToDismissKeyguard() {
        Intent intent = new Intent(this.mContext, AonServiceBreath.class);
        intent.setAction(Constants.ACTION_DISMISS_KEYGUARD);
        this.mContext.startService(intent);
    }

    /* access modifiers changed from: protected */
    public Class getServiceClass() throws RuntimeException {
        return AonServiceBreath.class;
    }
}
