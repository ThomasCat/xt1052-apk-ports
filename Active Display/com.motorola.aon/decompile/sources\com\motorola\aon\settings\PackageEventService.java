package com.motorola.aon.settings;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import com.motorola.aon.AonService;
import com.motorola.aon.BuildConfig;
import com.motorola.aon.Constants;
import com.motorola.aon.env.com.motorola.android.provider.MotorolaSettings;
import com.motorola.aon.env.com.motorola.blur.util.CheckinEventWrapper;
import com.motorola.aon.fd.AonServiceGlimpse;
import com.motorola.aon.jni.PartialDisplay;
import com.motorola.aon.pd.AonServiceBreath;
import com.motorola.aon.qp.AonServiceQuick;
import com.motorola.aon.settings.WhiteListActivity.AppEntry;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class PackageEventService extends IntentService {
    private static final String CHECKIN_SETTINGS_VERSION = "1.2";
    private static final String CHECKIN_TAG_SN_AOD_STAT = "SN_AOD_STAT";
    private static final String CHECKIN_VERSION = "1.1";
    private static final boolean DEBUG = AonSettingsConst.DEBUG;
    private static final int DELAY_REMOVE_TIME = 5000;
    public static final String EVENT_SETTINGS = "SETTINGS";
    public static final String KEY_AOD_VERSION = "sh_aod_version";
    public static final String KEY_AUTO_WHITLELIST = "auto_whitelist";
    public static final String KEY_ENABLED = "enabled";
    public static final String KEY_NIGHT_ENABLED = "night_enabled";
    public static final String KEY_NIGHT_END = "night_end_time";
    public static final String KEY_NIGHT_START = "night_start_time";
    public static final String KEY_PRIVACY = "privacy_enabled";
    public static final String KEY_SENSOR_HUB_VERSION = "sh_version";
    public static final String KEY_VERSION_CODE = "version_code";
    public static final String KEY_WHITELIST_LEN = "whitelist_len";
    public static final String KEY_WHITELIST_SEL = "whitelist_sel";
    private static final String PREFS_ITEM_LAST_REMOVED_PACKGE = "last_removed_pkg";
    private static final String PREFS_ITEM_LAST_REMOVED_TIME = "last_removed_time";
    private static final String PREFS_NAME = "AonSettings_Prefs";
    private static final String TAG = "Aon_PackageEventService";
    private static int versionCode = 0;
    private Handler mHandler = new Handler();
    Runnable mRunnableDelayedRemove = new Runnable() {
        public void run() {
            PackageEventService.this.handleDelayedRemove();
        }
    };

    public PackageEventService() {
        super("PackageEventService");
    }

    /* access modifiers changed from: private */
    public synchronized void handleDelayedRemove() {
        if (DEBUG) {
            Log.d(TAG, "handle delayed REMOVE event");
        }
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, 4);
        String lastRemovedPkg = prefs.getString(PREFS_ITEM_LAST_REMOVED_PACKGE, null);
        if (!TextUtils.isEmpty(lastRemovedPkg)) {
            if (handleAddOrRemove("android.intent.action.PACKAGE_REMOVED", lastRemovedPkg)) {
                sendBroadcast(new Intent(AonSettingsConst.WHITE_LIST_CHANAGED));
            }
            setLastRemovedPkg(prefs, BuildConfig.FLAVOR, 0);
        } else if (DEBUG) {
            Log.d(TAG, "no delayed REMOVED event");
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void onHandleIntent(Intent intent) {
        if (DEBUG) {
            Log.d(TAG, "onHandleIntent ");
        }
        if (intent != null) {
            String action = intent.getAction();
            if (action == null || !action.equals(Constants.SN_ANALYTICS_SETTINGS)) {
                WakeLock wakeLock = ((PowerManager) getSystemService("power")).newWakeLock(1, "aon_package_event_handler");
                wakeLock.acquire();
                handlePackageEvents(intent);
                wakeLock.release();
            } else {
                handleSNAnalysticsEvents();
            }
        }
    }

    private void handleSNAnalysticsEvents() {
        ContentResolver contentResolver = getContentResolver();
        int whiteListLen = 0;
        int whiteListSel = 0;
        int aon = MotorolaSettings.getInt(contentResolver, MotorolaSettings.SN_AON, 1);
        int nightOff = MotorolaSettings.getInt(contentResolver, MotorolaSettings.SN_NIGHT_OFF, 1);
        int startTime = MotorolaSettings.getInt(contentResolver, MotorolaSettings.SN_NIGHT_BEGIN, 1380);
        int endTime = MotorolaSettings.getInt(contentResolver, MotorolaSettings.SN_NIGHT_END, 360);
        int privacy = MotorolaSettings.getInt(contentResolver, MotorolaSettings.SN_PRIVACY, 0);
        int autoSelectInstalled = MotorolaSettings.getInt(contentResolver, MotorolaSettings.SN_AUTO_SELECT_INSTALLED, 1);
        List<AppEntry> entries = WhiteListActivity.loadAppEntries(this);
        if (entries != null) {
            whiteListLen = entries.size();
        }
        String whiteList = MotorolaSettings.getString(contentResolver, MotorolaSettings.SN_WHITE_LIST);
        if (whiteList != null) {
            whiteListSel = (whiteList.length() - whiteList.replaceAll(",", BuildConfig.FLAVOR).length()) + 1;
        }
        if (versionCode == 0) {
            try {
                versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            } catch (NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        boolean enabled = aon == 1;
        boolean nightEnabled = nightOff == 1;
        String nightStart = String.format("%02d%02d", new Object[]{Integer.valueOf(startTime / 60), Integer.valueOf(startTime % 60)});
        String nightEnd = String.format("%02d%02d", new Object[]{Integer.valueOf(endTime / 60), Integer.valueOf(endTime % 60)});
        boolean privacyEnabled = privacy == 1;
        boolean autoWhiteList = autoSelectInstalled == 1;
        int sensonHubVersion = PartialDisplay.getSensorHubVersion();
        int aodVersion = PartialDisplay.getAODFeatureVersion();
        if (DEBUG) {
            Log.d(TAG, "AOD_STAT: Event: SETTINGS enabled: " + enabled + " night_enabled: " + nightEnabled + " night_start_time: " + nightStart + " night_end_time: " + nightEnd + " privacy_enabled: " + privacyEnabled + " whitelist_len: " + whiteListLen + " whitelist_sel: " + whiteListSel + " auto_whitelist: " + autoWhiteList + " version_code: " + versionCode + " sh_version: " + sensonHubVersion + " sh_aod_version: " + aodVersion);
        }
        CheckinEventWrapper event = new CheckinEventWrapper(CHECKIN_TAG_SN_AOD_STAT, EVENT_SETTINGS, CHECKIN_SETTINGS_VERSION);
        event.setValue(KEY_ENABLED, enabled);
        event.setValue(KEY_NIGHT_ENABLED, nightEnabled);
        event.setValue(KEY_NIGHT_START, nightStart);
        event.setValue(KEY_NIGHT_END, nightEnd);
        event.setValue(KEY_PRIVACY, privacyEnabled);
        event.setValue(KEY_WHITELIST_LEN, whiteListLen);
        event.setValue(KEY_WHITELIST_SEL, whiteListSel);
        event.setValue(KEY_AUTO_WHITLELIST, autoWhiteList);
        event.setValue(KEY_VERSION_CODE, versionCode);
        event.setValue(KEY_SENSOR_HUB_VERSION, sensonHubVersion);
        event.setValue(KEY_AOD_VERSION, aodVersion);
        event.publish(contentResolver);
    }

    private void setLastRemovedPkg(SharedPreferences prefs, String name, long when) {
        Editor editor = prefs.edit();
        editor.putString(PREFS_ITEM_LAST_REMOVED_PACKGE, name);
        editor.putLong(PREFS_ITEM_LAST_REMOVED_TIME, when);
        editor.commit();
    }

    private void handlePackageEvents(Intent intent) {
        Intent iService;
        String action = intent.getAction();
        if (action != null) {
            Uri data = intent.getData();
            String pkgName = data != null ? data.getEncodedSchemeSpecificPart() : null;
            if (DEBUG) {
                Log.d(TAG, "Pkg added / removed is: " + pkgName);
            }
            if (getPackageName().equals(pkgName)) {
                if (Constants.isPartialDisplay) {
                    if (!getPackageManager().hasSystemFeature(Constants.QUICK_PEEK_FEATURE)) {
                        if (DEBUG) {
                            Log.d(TAG, "send ACTION_REFRESH_BREATHING intent to AonServiceBreath");
                        }
                        iService = new Intent(this, AonServiceBreath.class);
                    } else {
                        if (DEBUG) {
                            Log.d(TAG, "send ACTION_REFRESH_BREATHING intent to AonServiceQuick");
                        }
                        iService = new Intent(this, AonServiceQuick.class);
                    }
                } else {
                    iService = new Intent(this, AonServiceGlimpse.class);
                }
                iService.setAction(AonService.ACTION_REFRESH_BREATHING);
                startService(iService);
                return;
            }
            if (AonService.SENSORHUB_PKG_NAME.equals(pkgName) && "android.intent.action.PACKAGE_ADDED".equals(action)) {
                Log.d(TAG, "SensorHub Pkg has been added");
                Intent intent2 = new Intent(this, AonServiceQuick.class);
                intent2.setAction(AonService.ACTION_SENSORHUB_PKG_ADDED);
                startService(intent2);
                if (DEBUG) {
                    Log.d(TAG, "start service with ACTION_SENSORHUB_PKG_ADDED");
                }
            }
            if (WhiteListActivity.isPackageVisible(this, pkgName)) {
                SharedPreferences prefs = getSharedPreferences(PREFS_NAME, 4);
                if ("android.intent.action.PACKAGE_REMOVED".equals(action)) {
                    if (DEBUG) {
                        Log.d(TAG, "check if previous REMOVE exists");
                    }
                    this.mHandler.removeCallbacks(this.mRunnableDelayedRemove);
                    this.mRunnableDelayedRemove.run();
                    setLastRemovedPkg(prefs, pkgName, SystemClock.elapsedRealtime());
                    this.mHandler.postDelayed(this.mRunnableDelayedRemove, 5000);
                } else if ("android.intent.action.PACKAGE_ADDED".equals(action)) {
                    boolean whiteListChanged = false;
                    String lastRemovedPkg = prefs.getString(PREFS_ITEM_LAST_REMOVED_PACKGE, null);
                    long lastRemovedTm = prefs.getLong(PREFS_ITEM_LAST_REMOVED_TIME, 0);
                    if (TextUtils.isEmpty(lastRemovedPkg)) {
                        whiteListChanged = handleAddOrRemove(action, pkgName);
                    } else {
                        if (pkgName == null || !pkgName.equals(lastRemovedPkg)) {
                            if (DEBUG) {
                                Log.d(TAG, "handle remove then add the different package " + lastRemovedPkg + "," + pkgName);
                            }
                            whiteListChanged = handleAddOrRemove("android.intent.action.PACKAGE_REMOVED", lastRemovedPkg) || handleAddOrRemove("android.intent.action.PACKAGE_ADDED", pkgName);
                        } else {
                            long diff = SystemClock.elapsedRealtime() - lastRemovedTm;
                            if (diff >= 5000 || diff <= 0) {
                                if (DEBUG) {
                                    Log.d(TAG, "handle remove 5 seconds then add the same package " + lastRemovedPkg);
                                }
                                whiteListChanged = handleAddOrRemove("android.intent.action.PACKAGE_REMOVED", lastRemovedPkg) != handleAddOrRemove("android.intent.action.PACKAGE_ADDED", pkgName);
                            } else if (DEBUG) {
                                Log.d(TAG, "found an UPDATE, ignore " + lastRemovedPkg);
                            }
                        }
                        setLastRemovedPkg(prefs, BuildConfig.FLAVOR, 0);
                        this.mHandler.removeCallbacks(this.mRunnableDelayedRemove);
                    }
                    if (whiteListChanged) {
                        if (DEBUG) {
                            Log.d(TAG, "white list changed");
                        }
                        sendBroadcast(new Intent(AonSettingsConst.WHITE_LIST_CHANAGED));
                    }
                }
            }
        } else if (DEBUG) {
            Log.d(TAG, "got a null action");
        }
    }

    private boolean handleAddOrRemove(String action, String pkgName) {
        boolean whiteListChanged = false;
        if ("android.intent.action.PACKAGE_ADDED".equals(action) || "android.intent.action.PACKAGE_REMOVED".equals(action)) {
            if (DEBUG) {
                Log.d(TAG, "PACKAGE EVENT " + action + " " + pkgName);
            }
            HashSet<String> setWhiteList = new HashSet<>();
            String whiteList = MotorolaSettings.getString(getContentResolver(), MotorolaSettings.SN_WHITE_LIST);
            setWhiteList.clear();
            if (whiteList != null) {
                for (String item : whiteList.split(",")) {
                    setWhiteList.add(item);
                }
            }
            if ("android.intent.action.PACKAGE_ADDED".equals(action)) {
                if (MotorolaSettings.getInt(getContentResolver(), MotorolaSettings.SN_AUTO_SELECT_INSTALLED, 1) != 0) {
                    if (DEBUG) {
                        Log.d(TAG, "add to white list: " + pkgName);
                    }
                    setWhiteList.add(pkgName);
                    whiteListChanged = true;
                }
            } else if ("android.intent.action.PACKAGE_REMOVED".equals(action) && setWhiteList.remove(pkgName)) {
                if (DEBUG) {
                    Log.d(TAG, "remove from white list: " + pkgName);
                }
                whiteListChanged = true;
            }
            if (whiteListChanged) {
                StringBuilder whiteListBuilder = new StringBuilder();
                int N = setWhiteList.size();
                int idx = 0;
                Iterator i$ = setWhiteList.iterator();
                while (i$.hasNext()) {
                    idx++;
                    whiteListBuilder.append((String) i$.next());
                    if (idx < N) {
                        whiteListBuilder.append(',');
                    }
                }
                MotorolaSettings.putString(getContentResolver(), MotorolaSettings.SN_WHITE_LIST, whiteListBuilder.toString());
            }
        }
        Intent refreshIntent = new Intent("com.motorola.aon.settings.REFRESH_LIST");
        refreshIntent.setPackage(getPackageName());
        sendBroadcast(refreshIntent);
        return whiteListChanged;
    }
}
