package com.motorola.aon.fd;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import com.motorola.aon.Constants;

public class AonGlimpseBlank extends Activity {
    public static final boolean DEBUG = Constants.DEBUG;
    private static final String TAG = "AonGlimpseBlank";
    private boolean mAODfade = false;
    /* access modifiers changed from: private */
    public Runnable mFinishRunnable = new Runnable() {
        public void run() {
            if (!AonGlimpseBlank.this.mFinishing) {
                AonGlimpseBlank.this.finish();
                AonGlimpseBlank.this.overridePendingTransition(0, 0);
                AonGlimpseBlank.this.mFinishing = true;
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean mFinishing = false;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public boolean mHasFocus = false;
    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if ("android.intent.action.USER_PRESENT".equals(action)) {
                    if (AonGlimpseBlank.DEBUG) {
                        Log.d(AonGlimpseBlank.TAG, "ACTION_USER_PRESENT received. blankfinish()");
                    }
                    AonGlimpseBlank.this.mHandler.post(AonGlimpseBlank.this.mFinishRunnable);
                } else if ("android.intent.action.SCREEN_ON".equals(action) && AonGlimpseBlank.this.mHasFocus) {
                    if (AonGlimpseBlank.DEBUG) {
                        Log.d(AonGlimpseBlank.TAG, "ACTION_SCREEN_ON received. blankfinish()");
                    }
                    AonGlimpseBlank.this.mHandler.post(AonGlimpseBlank.this.mFinishRunnable);
                } else if (Constants.ACTION_EXIT_AONBLANK.equals(action)) {
                    if (AonGlimpseBlank.DEBUG) {
                        Log.d(AonGlimpseBlank.TAG, "ACTION_EXIT_AONBLANK received. blankfinish()");
                    }
                    AonGlimpseBlank.this.mHandler.post(AonGlimpseBlank.this.mFinishRunnable);
                }
            }
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.mAODfade = getIntent().getBooleanExtra("AODfade", false);
        IntentFilter ifilter = new IntentFilter();
        ifilter.addAction("android.intent.action.USER_PRESENT");
        ifilter.addAction("android.intent.action.SCREEN_ON");
        ifilter.addAction(Constants.ACTION_EXIT_AONBLANK);
        registerReceiver(this.mReceiver, ifilter);
    }

    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.mReceiver);
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        boolean isScreenOn = false;
        this.mHasFocus = hasFocus;
        PowerManager pm = (PowerManager) getSystemService("power");
        if (pm != null) {
            isScreenOn = pm.isScreenOn();
        }
        if (hasFocus && !this.mAODfade && isScreenOn) {
            if (DEBUG) {
                Log.d(TAG, "got focus and mAODfade is false blankfinish()");
            }
            this.mHandler.post(this.mFinishRunnable);
        }
    }
}
