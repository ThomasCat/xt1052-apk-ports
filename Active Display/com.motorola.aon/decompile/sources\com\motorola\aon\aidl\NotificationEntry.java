package com.motorola.aon.aidl;

import com.motorola.aon.MinedData;

public class NotificationEntry {
    public boolean breathing = false;
    public MinedData minedData;

    public NotificationEntry(MinedData sbn) {
        this.minedData = sbn;
    }
}
