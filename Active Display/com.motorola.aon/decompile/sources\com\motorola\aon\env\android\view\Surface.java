package com.motorola.aon.env.android.view;

import android.util.Log;
import java.lang.reflect.Constructor;

public class Surface {
    private static String CLASS_SURFACE = "android.view.Surface";
    private static final String TAG = "Aon_Surface";
    private static Constructor<?> sConstructorDefault;
    public static boolean sInitialized;

    public static android.view.Surface create() {
        Object obj = null;
        if (sInitialized) {
            try {
                obj = sConstructorDefault.newInstance(new Object[0]);
            } catch (Throwable th) {
                Log.w(TAG, "unable to create surface");
            }
        }
        return (android.view.Surface) obj;
    }

    static {
        sInitialized = false;
        try {
            sConstructorDefault = Class.forName(CLASS_SURFACE).getDeclaredConstructor(new Class[0]);
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to intialize class");
        }
    }
}
