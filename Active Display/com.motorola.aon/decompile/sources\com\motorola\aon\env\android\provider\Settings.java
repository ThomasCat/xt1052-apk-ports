package com.motorola.aon.env.android.provider;

import android.util.Log;

public class Settings {

    public static class Secure {
        private static final String CLASS_SECURE = "android.provider.Settings$Secure";
        private static final String FIELD_LS_AFTER_TIMEOUT = "LOCK_SCREEN_LOCK_AFTER_TIMEOUT";
        public static String LOCK_SCREEN_LOCK_AFTER_TIMEOUT = null;
        private static final String TAG = "Aon_Settings.Secure";
        public static boolean sInitialized;

        static {
            LOCK_SCREEN_LOCK_AFTER_TIMEOUT = null;
            sInitialized = false;
            try {
                LOCK_SCREEN_LOCK_AFTER_TIMEOUT = (String) Class.forName(CLASS_SECURE).getDeclaredField(FIELD_LS_AFTER_TIMEOUT).get(null);
                sInitialized = true;
            } catch (Throwable th) {
                Log.w(TAG, "unable to initialize class");
            }
        }
    }
}
