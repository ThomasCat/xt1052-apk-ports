package com.motorola.aon.aidl;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import java.util.ArrayList;

public class ParsedInfo implements Parcelable {
    public static final Creator<ParsedInfo> CREATOR = new Creator<ParsedInfo>() {
        public ParsedInfo createFromParcel(Parcel source) {
            return new ParsedInfo(source);
        }

        public ParsedInfo[] newArray(int size) {
            return new ParsedInfo[size];
        }
    };
    public Bitmap bigPicture;
    public CharSequence bigText;
    public ArrayList<CharSequence> inbox;
    public CharSequence info;
    public String packageName;
    public int style;
    public CharSequence text;
    public CharSequence text2;
    public CharSequence title;
    public String when;

    public interface Style {
        public static final int BIG_INBOX = 2;
        public static final int BIG_PICTURE = 3;
        public static final int BIG_TEXT = 1;
        public static final int CUSTOMIZED = 4;
        public static final int DEFAULT = 0;
    }

    public ParsedInfo() {
    }

    private ParsedInfo(Parcel in) {
        if (in != null) {
            if (in.readInt() != 0) {
                this.title = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
            }
            if (in.readInt() != 0) {
                this.text = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
            }
            if (in.readInt() != 0) {
                this.text2 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
            }
            if (in.readInt() != 0) {
                this.info = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
            }
            if (in.readInt() != 0) {
                this.bigText = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
            }
            if (in.readInt() != 0) {
                this.bigPicture = (Bitmap) Bitmap.CREATOR.createFromParcel(in);
            }
            int length = in.readInt();
            if (length >= 0) {
                CharSequence[] cs = new CharSequence[length];
                for (int i = 0; i < length; i++) {
                    cs[i] = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
                }
                this.inbox = new ArrayList<>();
                for (CharSequence each : cs) {
                    this.inbox.add(each);
                }
            }
            this.style = in.readInt();
            this.when = in.readString();
            this.packageName = in.readString();
        }
    }

    public void writeToParcel(Parcel out, int flags) {
        int size;
        if (out != null) {
            if (this.title != null) {
                out.writeInt(1);
                TextUtils.writeToParcel(this.title, out, flags);
            } else {
                out.writeInt(0);
            }
            if (this.text != null) {
                out.writeInt(1);
                TextUtils.writeToParcel(this.text, out, flags);
            } else {
                out.writeInt(0);
            }
            if (this.text2 != null) {
                out.writeInt(1);
                TextUtils.writeToParcel(this.text2, out, flags);
            } else {
                out.writeInt(0);
            }
            if (this.info != null) {
                out.writeInt(1);
                TextUtils.writeToParcel(this.info, out, flags);
            } else {
                out.writeInt(0);
            }
            if (this.bigText != null) {
                out.writeInt(1);
                TextUtils.writeToParcel(this.bigText, out, flags);
            } else {
                out.writeInt(0);
            }
            if (this.bigPicture != null) {
                out.writeInt(1);
                this.bigPicture.writeToParcel(out, 0);
            } else {
                out.writeInt(0);
            }
            if (this.inbox != null) {
                size = this.inbox.size();
            } else {
                size = 0;
            }
            CharSequence[] cs = new CharSequence[size];
            if (this.inbox != null) {
                this.inbox.toArray(cs);
            }
            out.writeInt(N);
            for (CharSequence writeToParcel : cs) {
                TextUtils.writeToParcel(writeToParcel, out, 0);
            }
            out.writeInt(this.style);
            out.writeString(this.when);
            out.writeString(this.packageName);
        }
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("title=").append(this.title);
        sb.append(" text=").append(this.text);
        sb.append(" text2=").append(this.text2);
        sb.append(" info=").append(this.info);
        sb.append(" bigText=").append(this.bigText);
        sb.append(" bigPicture=").append(this.bigPicture);
        sb.append(" inbox=").append(this.inbox);
        sb.append(" style=").append(this.style);
        sb.append(" when=").append(this.when);
        sb.append(" packageName=").append(this.packageName);
        return sb.toString();
    }
}
