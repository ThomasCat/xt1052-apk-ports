package com.motorola.aon.env.android.view;

import android.os.IBinder;
import android.util.Log;
import java.lang.reflect.Method;

public class IWindowManager {
    private static final String METHOD_DISMISS_KEYGUARD = "dismissKeyguard";
    private static final String METHOD_OVERRIDE_PENDING_APP_TRANS = "overridePendingAppTransition";
    private static final String TAG = "Aon_IWindowManager";
    private static Method sMethodDismissKeyguard;
    private static Method sMethodOverridePendingAppTransition;
    final Object mIWindowManager;

    public static class Stub {
        private static String CLASS_STUB = "android.view.IWindowManager$Stub";
        private static String METHOD_AS_INTERFACE = "asInterface";
        private static final String TAG = "IWindowManager.Stub";
        public static boolean sInitialized;
        private static Method sMethodAsInterface;

        static {
            sInitialized = false;
            try {
                sMethodAsInterface = Class.forName(CLASS_STUB).getDeclaredMethod(METHOD_AS_INTERFACE, new Class[]{IBinder.class});
                sInitialized = true;
            } catch (Throwable th) {
                Log.w(TAG, "unable to initialize class");
            }
        }

        public static IWindowManager asInterface(IBinder service) {
            if (sInitialized) {
                try {
                    Object localProxy = sMethodAsInterface.invoke(null, new Object[]{service});
                    if (localProxy != null) {
                        return new IWindowManager(localProxy);
                    }
                    return null;
                } catch (Throwable th) {
                    Log.w(TAG, "unable to inovke asInterface");
                }
            }
            return null;
        }
    }

    private IWindowManager(Object localProxy) {
        this.mIWindowManager = localProxy;
        Class<?> clazz = this.mIWindowManager.getClass();
        try {
            sMethodDismissKeyguard = clazz.getDeclaredMethod(METHOD_DISMISS_KEYGUARD, new Class[0]);
            sMethodOverridePendingAppTransition = clazz.getDeclaredMethod(METHOD_OVERRIDE_PENDING_APP_TRANS, new Class[]{String.class, Integer.TYPE, Integer.TYPE, Class.forName("android.os.IRemoteCallback")});
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize class " + clazz);
        }
    }

    public void dismissKeyguard() {
        if (sMethodDismissKeyguard == null || this.mIWindowManager == null) {
            Log.w(TAG, "class not initialized or no service");
            return;
        }
        try {
            sMethodDismissKeyguard.invoke(this.mIWindowManager, new Object[0]);
        } catch (Throwable th) {
            Log.w(TAG, "unable to invoke dismiss keyguard");
        }
    }

    public void overridePendingAppTransition(String pkg, int i, int j, Object object) {
        if (sMethodOverridePendingAppTransition == null || this.mIWindowManager == null) {
            Log.w(TAG, "class not initialized or no service");
            return;
        }
        try {
            sMethodOverridePendingAppTransition.invoke(this.mIWindowManager, new Object[]{pkg, Integer.valueOf(i), Integer.valueOf(j), object});
        } catch (Throwable th) {
            Log.w(TAG, "unable to invoke dismiss keyguard");
        }
    }
}
