package com.motorola.aon;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import android.util.Log;
import com.motorola.aon.aidl.NotificationEntry;
import com.motorola.aon.aidl.ParsedInfo;
import com.motorola.aon.env.android.os.ServiceManager;
import com.motorola.aon.env.com.android.internal.R;
import com.motorola.aon.env.com.android.internal.statusbar.IStatusBarService.Stub;
import com.motorola.aon.env.com.motorola.android.provider.MotorolaSettings;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class AonNotificationManager {
    private static final String ACTION_AON_NOTIFICATION_UPDATE = "com.motorola.aon.NOTIFICATION_UPDATE";
    static final int AOV_NOTIFICATION = 1;
    protected static final boolean DEBUG = AonService.DEBUG;
    private static final int MAX_ICON_HEIGHT = 48;
    private static final int MAX_ICON_WIDTH = 48;
    private static final int MAX_PHOTO_HEIGHT = 256;
    private static final int MAX_PHOTO_WIDTH = 256;
    private static final String PERMISSION_NOTIFICATION_UPDATE = "com.motorola.aon.permission.NOTIFICATION_UPDATE";
    static final int PHONE_IN_CALL_NOTIFICATION = 2;
    static final int PHONE_VOICEMAIL_NOTIFICATION = 5;
    static final String TAG = "AonNotificationManager";
    private static R internalR;
    protected boolean mAonPrivacyOn;
    protected boolean mAonSettingOn;
    protected final Context mContext;
    protected Object mDataLock = new Object();
    protected boolean mDreamStarted = false;
    protected Handler mHandler = new Handler();
    protected final NotificationList mNotificationEntriesBreathing = new NotificationList();
    protected boolean mTracking = false;
    private int mTutorialState = 0;
    protected boolean mUpdated = false;
    private Set<String> mWhiteList = new HashSet();
    private Object mWhiteListLock = new Object();

    public AonNotificationManager(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Invalid parameter");
        }
        this.mContext = context;
        internalR = R.getInstance(context);
        loadWhiteList();
    }

    public ArrayList<NotificationEntry> getNotificationListForBreathing() {
        ArrayList<NotificationEntry> arrayList;
        synchronized (this.mDataLock) {
            arrayList = new ArrayList<>(this.mNotificationEntriesBreathing);
        }
        return arrayList;
    }

    public void enableTracking(boolean tracking) {
        synchronized (this.mDataLock) {
            if (this.mTracking != tracking) {
                if (DEBUG) {
                    Log.d(TAG, "set tracking " + (tracking ? "Yes" : "No"));
                }
                this.mTracking = tracking;
                this.mNotificationEntriesBreathing.clear();
            }
        }
    }

    private void addPrimaryInfo(Intent intent, MinedData md) {
        ParsedInfo info = new ParsedInfo();
        parseNotification(md, true, info);
        Intent intent2 = intent;
        intent2.putExtra("PendingIntent", md.contentIntent);
        if (info.title != null) {
            intent.putExtra("title", info.title.toString());
        }
        String inbox0 = BuildConfig.FLAVOR;
        String inbox1 = BuildConfig.FLAVOR;
        String inbox2 = BuildConfig.FLAVOR;
        String text2 = BuildConfig.FLAVOR;
        String text = BuildConfig.FLAVOR;
        String when = BuildConfig.FLAVOR;
        Bitmap photo = null;
        Resources res = this.mContext.getResources();
        Bitmap mDefPhoto = ((BitmapDrawable) res.getDrawable(R.drawable.ic_contact_picture)).getBitmap();
        if (info.style == 2) {
            if (info.inbox != null && info.inbox.size() > 2) {
                inbox2 = res.getString(R.string.inbox_more);
            }
            if (info.inbox != null && info.inbox.size() > 1) {
                inbox1 = ((CharSequence) info.inbox.get(1)).toString();
            }
            if (info.inbox != null && info.inbox.size() > 0) {
                inbox0 = ((CharSequence) info.inbox.get(0)).toString();
            }
        } else {
            if (info.bigPicture != null) {
                photo = info.bigPicture;
            } else if (md.largeIcon != null) {
                photo = md.largeIcon;
                if (photo != null && mDefPhoto.sameAs(photo)) {
                    photo = null;
                }
            }
            if (photo != null) {
                int h = photo.getHeight();
                int w = photo.getWidth();
                if (DEBUG) {
                    Log.d(TAG, "photo size (w,h) " + w + "," + h);
                }
                if (h >= 256 || w >= 256) {
                    photo = Bitmap.createScaledBitmap(photo, 256, 256, false);
                } else if (h < 128 || w < 128) {
                    photo = null;
                }
            }
            if (!TextUtils.isEmpty(info.text2)) {
                text2 = info.text2.toString();
            } else if (!TextUtils.isEmpty(info.bigText)) {
                text2 = info.bigText.toString();
            }
            if (!TextUtils.isEmpty(info.text)) {
                text = info.text.toString();
            }
        }
        if (!TextUtils.isEmpty(info.when)) {
            when = info.when;
        }
        intent.putExtra("inbox0", inbox0);
        intent.putExtra("inbox1", inbox1);
        intent.putExtra("inbox2", inbox2);
        intent.putExtra("text2", text2);
        intent.putExtra("text", text);
        intent.putExtra("when", when);
        intent.putExtra("photo", photo);
        Intent intent3 = intent;
        intent3.putExtra("privacy", this.mAonPrivacyOn);
    }

    public void notificationUpdated() {
        if (DEBUG) {
            Log.d(TAG, "AOD whitelist notification updated and send broadcast intent");
        }
        Intent intent = new Intent(ACTION_AON_NOTIFICATION_UPDATE);
        int size = this.mNotificationEntriesBreathing.size();
        if (size > 4) {
            size = 4;
        }
        intent.putExtra("size", size);
        for (int i = 0; i < size; i++) {
            MinedData md = ((NotificationEntry) this.mNotificationEntriesBreathing.get(i)).minedData;
            if (i == 0) {
                addPrimaryInfo(intent, md);
            }
            Bitmap icon = md.iconBitmap;
            if (icon != null) {
                int h = icon.getHeight();
                int w = icon.getWidth();
                if (DEBUG) {
                    Log.d(TAG, "icon size (w,h) " + w + "," + h);
                }
                if (h >= 48 || w >= 48) {
                    Bitmap icon2 = Bitmap.createScaledBitmap(icon, 48, 48, false);
                }
            }
            intent.putExtra("icon" + i, md.iconBitmap);
        }
        this.mContext.sendBroadcast(intent, PERMISSION_NOTIFICATION_UPDATE);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:?, code lost:
        return false;
     */
    public boolean onUpdateNotification(StatusBarNotification sbn) {
        if (sbn == null) {
            if (DEBUG) {
                Log.d(TAG, "on update notification, null data");
            }
            return false;
        }
        SnAnalyticsEventService.setIncomingTime(System.currentTimeMillis());
        SnAnalyticsEventService.recordNotification();
        if (sbn.isOngoing()) {
            SnAnalyticsEventService.recordOngoingNotification();
        }
        Notification n = sbn.getNotification();
        if (n != null && n.priority <= -1) {
            SnAnalyticsEventService.recordLowPriNotification();
        }
        if (DEBUG) {
            Log.d(TAG, "onUpdateNotification " + sbn.getPackageName() + ", " + sbn.getId());
        }
        synchronized (this.mDataLock) {
            if (this.mTracking) {
                boolean bAdded = false;
                MinedData md = null;
                if (n == null || !"com.android.calendar".equals(sbn.getPackageName()) || n.tickerText != null || n.sound != null) {
                    boolean bRemoved = removeFromListLocked(this.mNotificationEntriesBreathing, sbn.getPackageName(), sbn.getId());
                    if (isInWhiteList(sbn.getPackageName())) {
                        md = MinedData.fromStatusBarNotification(sbn, this.mContext);
                        if (md != null) {
                            this.mNotificationEntriesBreathing.addInOrder(new NotificationEntry(md));
                            bAdded = true;
                        }
                    }
                    if (this.mDreamStarted && (bRemoved || bAdded)) {
                        notificationUpdated();
                    }
                    this.mUpdated = true;
                    if (!notificationTurningOn(md) && (bRemoved || bAdded)) {
                        if (this.mTutorialState == 1 || this.mTutorialState == 3) {
                            notifyRefreshBreathingViewOnNotification();
                        } else if (this.mTutorialState == 2) {
                            notifyLaunchTutorialOnNotification();
                        }
                    }
                    int maxActive = this.mNotificationEntriesBreathing.size();
                    if (DEBUG) {
                        Log.d(TAG, "Number of Active Notifictions: " + maxActive);
                    }
                    SnAnalyticsEventService.recordMaxActiveNotifications(maxActive);
                    return bAdded;
                } else if (DEBUG) {
                    Log.d(TAG, "Calendar id=" + sbn.getId() + " ignored due to quiet refresh");
                }
            } else if (DEBUG) {
                Log.d(TAG, "Tracking off");
            }
        }
    }

    public void onRemoveNotification(StatusBarNotification sbn) {
        if (sbn != null) {
            SnAnalyticsEventService.setIncomingTime(System.currentTimeMillis());
            if (DEBUG) {
                Log.d(TAG, "onRemoveNotification " + sbn);
            }
            synchronized (this.mDataLock) {
                if (this.mTracking) {
                    boolean bFound = removeFromListLocked(this.mNotificationEntriesBreathing, sbn.getPackageName(), sbn.getId());
                    if (this.mDreamStarted) {
                        notificationUpdated();
                    }
                    this.mUpdated = true;
                    if (bFound) {
                        if (this.mTutorialState == 1 || this.mTutorialState == 3) {
                            notifyRefreshBreathingViewOnNotification();
                        } else if (this.mTutorialState == 2) {
                            notifyLaunchTutorialOnNotification();
                        }
                    }
                } else if (DEBUG) {
                    Log.d(TAG, "Tracking off");
                }
            }
        } else if (DEBUG) {
            Log.d(TAG, "on remove notification, null data");
        }
    }

    private boolean removeFromListLocked(ArrayList<NotificationEntry> list, String pkg, int id) {
        Iterator i$ = list.iterator();
        while (i$.hasNext()) {
            NotificationEntry each = (NotificationEntry) i$.next();
            if (pkg.equals(each.minedData.pkg) && id == each.minedData.id) {
                list.remove(each);
                return true;
            }
        }
        return false;
    }

    public boolean isInWhiteList(String pkg) {
        synchronized (this.mWhiteListLock) {
            if (!this.mWhiteList.contains(pkg)) {
                return false;
            }
            return true;
        }
    }

    private void loadWhiteList() {
        synchronized (this.mWhiteListLock) {
            this.mWhiteList.clear();
            String whiteList = MotorolaSettings.getString(this.mContext.getContentResolver(), MotorolaSettings.SN_WHITE_LIST);
            if (whiteList != null) {
                String[] whiteListArray = whiteList.split(",");
                if (whiteListArray != null && whiteListArray.length > 0) {
                    for (String item : whiteListArray) {
                        this.mWhiteList.add(item);
                    }
                }
            }
        }
    }

    public void refreshWhiteList() {
        loadWhiteList();
    }

    public List<String> getWhiteList() {
        synchronized (this.mWhiteListLock) {
            try {
                ArrayList arrayList = new ArrayList(this.mWhiteList);
                try {
                    return arrayList;
                } catch (Throwable th) {
                    th = th;
                    ArrayList arrayList2 = arrayList;
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
    }

    private boolean notificationTurningOn(MinedData sbn) {
        if (sbn == null) {
            return false;
        }
        String pkg = sbn.pkg;
        int id = sbn.id;
        if ((("com.android.phone".equals(pkg) || "com.android.dialer".equals(pkg)) && id == 2) || "com.android.deskclock".equals(pkg) || "com.google.android.deskclock".equals(pkg)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void notifyRefreshBreathingViewOnNotification() {
    }

    /* access modifiers changed from: protected */
    public void notifyLaunchTutorialOnNotification() {
    }

    public boolean hasBreathingNotification() {
        boolean z;
        synchronized (this.mDataLock) {
            z = this.mNotificationEntriesBreathing.size() > 0;
        }
        return z;
    }

    public NotificationEntry getBreathingNotification() {
        NotificationEntry notificationEntry;
        synchronized (this.mDataLock) {
            if (this.mNotificationEntriesBreathing.size() > 0) {
                notificationEntry = (NotificationEntry) this.mNotificationEntriesBreathing.get(0);
            } else {
                notificationEntry = null;
            }
        }
        return notificationEntry;
    }

    public void onSelectNotification(IBinder key) {
        try {
            Stub.asInterface(ServiceManager.getService(com.motorola.aon.env.android.content.Context.STATUS_BAR_SERVICE)).onNotificationClickEndPoint(key);
        } catch (RemoteException e) {
        }
    }

    public void onCancelNotification(IBinder key) {
        if (key != null) {
            try {
                Stub.asInterface(ServiceManager.getService(com.motorola.aon.env.android.content.Context.STATUS_BAR_SERVICE)).onNotificationClearEndPoint(key);
            } catch (RemoteException e) {
            }
        }
    }

    public int getBreathingSize() {
        int size;
        synchronized (this.mDataLock) {
            size = this.mNotificationEntriesBreathing.size();
        }
        return size;
    }

    public static void parseNotification(MinedData sbn, boolean bigSize, ParsedInfo resInfo) {
        if (resInfo == null) {
            throw new IllegalArgumentException("invalid parameter");
        }
        resInfo.packageName = sbn.pkg;
        if (bigSize && sbn.hasBigContent()) {
            if (sbn.bigStyle == internalR.layout.notification_template_big_text) {
                resInfo.style = 1;
            } else if (sbn.bigStyle == internalR.layout.notification_template_big_picture) {
                resInfo.style = 3;
            } else if (sbn.bigStyle == internalR.layout.notification_template_inbox) {
                resInfo.style = 2;
            } else if (sbn.bigStyle != internalR.layout.notification_template_big_base) {
                resInfo.style = 4;
            }
            if (resInfo.style != 4) {
                resInfo.title = sbn.mExtra.getCharSequence("android.title.big");
                if (resInfo.title == null) {
                    resInfo.title = sbn.mExtra.getCharSequence("android.title");
                }
                resInfo.text2 = sbn.mExtra.getCharSequence("android.text");
                resInfo.bigText = sbn.mExtra.getCharSequence("android.subText");
                resInfo.text = sbn.mExtra.getCharSequence("android.subText");
                if (sbn.mExtra.getBoolean("android.showWhen")) {
                    resInfo.when = sbn.when;
                }
                resInfo.info = sbn.mExtra.getCharSequence("android.infoText");
                resInfo.bigPicture = (Bitmap) sbn.mExtra.getParcelable("android.picture");
                CharSequence[] inbox = sbn.mExtra.getCharSequenceArray("android.textLines");
                if (inbox != null) {
                    resInfo.inbox = new ArrayList<>();
                    for (CharSequence each : inbox) {
                        resInfo.inbox.add(each);
                    }
                    return;
                }
                return;
            }
            resInfo.title = sbn.mExtra.getCharSequence("android.title");
            resInfo.text2 = sbn.mExtra.getCharSequence("android.text");
            resInfo.text = sbn.mExtra.getCharSequence("android.subText");
        } else if (sbn.smallStyle == internalR.layout.notification_template_base) {
            resInfo.title = sbn.mExtra.getCharSequence("android.title");
            resInfo.text2 = sbn.mExtra.getCharSequence("android.text");
            resInfo.text = sbn.mExtra.getCharSequence("android.subText");
            if (sbn.mExtra.getBoolean("android.showWhen")) {
                resInfo.when = sbn.when;
            }
            resInfo.info = sbn.mExtra.getCharSequence("android.infoText");
            resInfo.bigPicture = (Bitmap) sbn.mExtra.getParcelable("android.picture");
        } else {
            resInfo.title = sbn.mExtra.getCharSequence("android.title");
            resInfo.text2 = sbn.mExtra.getCharSequence("android.text");
            resInfo.text = sbn.mExtra.getCharSequence("android.subText");
        }
    }

    public static CharSequence getNotificationText(MinedData md) {
        if (md == null) {
            return null;
        }
        if (!TextUtils.isEmpty(md.tickerText)) {
            return md.tickerText;
        }
        CharSequence anyText = md.mExtra.getCharSequence("android.title");
        if (anyText != null) {
            return anyText;
        }
        CharSequence anyText2 = md.mExtra.getCharSequence("android.text");
        if (anyText2 == null) {
            return md.mExtra.getCharSequence("android.subText");
        }
        return anyText2;
    }

    public void setAonSettingOn(boolean on) {
        this.mAonSettingOn = on;
    }

    public void setAonPrivacyOn(boolean on) {
        this.mAonPrivacyOn = on;
    }

    public boolean getTracking() {
        return this.mTracking;
    }

    public void setDreamStarted(boolean Started) {
        this.mDreamStarted = Started;
    }

    public void removeBreathedNotifications() {
        synchronized (this.mDataLock) {
            for (int i = this.mNotificationEntriesBreathing.size() - 1; i >= 0; i--) {
                if (((NotificationEntry) this.mNotificationEntriesBreathing.get(i)).breathing) {
                    this.mNotificationEntriesBreathing.remove(i);
                }
            }
        }
    }

    public void setTutorialState(int state) {
        this.mTutorialState = state;
    }
}
