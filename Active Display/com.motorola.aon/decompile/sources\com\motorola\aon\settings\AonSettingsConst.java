package com.motorola.aon.settings;

import com.motorola.aon.AonService;

public class AonSettingsConst {
    public static final String ACCOUNT_TYPE_MOTO = "com.motorola.blur.service.bsutils.motorola";
    static final String ACTION_REFRESH_LIST = "com.motorola.aon.settings.REFRESH_LIST";
    public static final String ACTION_TARGETNOTIF_DEVICE_COUNT_UPDATED = "com.motorola.targetnotif.devicecount_update";
    public static final String ACTION_TARGETNOTIF_GET_STARTED = "com.motorola.targetnotif.optin";
    public static final String ACTION_TARGETNOTIF_LEARNMORE = "com.motorola.targetnotif.learnmore";
    public static final String ACTION_TARGETNOTIF_MAIN_SERVICE = "com.motorola.targetnotif.TargetNotifService";
    public static final String ACTION_TARGETNOTIF_OPTIN_SERVICE = "com.motorola.targetnotif.TargetNotifOptInSettingService";
    public static final String ACTION_TARGETNOTIF_SHUTDOWN = "com.motorola.targetnotif.shutdown";
    public static final String ACTION_TARGETNOTIF_STARTED = "com.motorola.targetnotif.started";
    public static boolean DEBUG = true;
    public static boolean DEVELOPER_MODE = false;
    static final float DISABLED_ALPHA = 0.4f;
    public static final String INTENT_ACTION_SSO_MOTO_ID_EDIT = "com.motorola.ccc.sso.action.SETTINGS";
    public static final String REQUEST_PRESENCE = "presence";
    public static final String TARGETNOTIF_PACKAGE = "com.motorola.targetnotif";
    public static String WHITE_LIST_CHANAGED = AonService.ACTION_WHITE_LIST_CHANGE;
}
