package com.motorola.aon;

public final class BuildConfig {
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final String PACKAGE_NAME = "com.motorola.aon";
    public static final int VERSION_CODE = 1020012;
}
