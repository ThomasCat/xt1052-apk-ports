package com.motorola.aon.pd;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PersistentManager {
    private static final String PREF_FILE_NAME = "aon_state";
    private static final String PREF_ITEM_PARTIAL_MODE = "aod_partial";
    private static final String PREF_ITEM_ROTATION_DISABLED_BY_AOD = "rot_disabled";
    private static final String TAG = "PersistentManager";
    public static PersistentManager mInst = new PersistentManager();
    private SharedPreferences mPreferences;

    protected PersistentManager() {
    }

    private void ensurePreference(Context context) {
        if (this.mPreferences == null) {
            this.mPreferences = context.getSharedPreferences(PREF_FILE_NAME, 0);
        }
    }

    public void setPartialMode(Context context, int val) {
        ensurePreference(context);
        Editor editor = this.mPreferences.edit();
        editor.putInt(PREF_ITEM_PARTIAL_MODE, val);
        editor.apply();
    }

    public int getPartialMode(Context context) {
        ensurePreference(context);
        return this.mPreferences.getInt(PREF_ITEM_PARTIAL_MODE, 0);
    }

    public void setPauseRotationMode(Context context, int val) {
        ensurePreference(context);
        Editor editor = this.mPreferences.edit();
        editor.putInt(PREF_ITEM_ROTATION_DISABLED_BY_AOD, val);
        editor.apply();
    }

    public int getPauseRotationMode(Context context) {
        ensurePreference(context);
        return this.mPreferences.getInt(PREF_ITEM_ROTATION_DISABLED_BY_AOD, 0);
    }
}
