package com.motorola.aon.env.android.view;

import android.util.Log;

public class SurfaceControl {
    private static String CLASS_SURFACECONTROL = "android.view.SurfaceControl";
    private static String FIELD_HIDDEN = "HIDDEN";
    public static int HIDDEN = 0;
    private static final String TAG = "Aon_SurfaceControl";
    public static boolean sInitialized;

    static {
        HIDDEN = 0;
        sInitialized = false;
        try {
            HIDDEN = Class.forName(CLASS_SURFACECONTROL).getDeclaredField(FIELD_HIDDEN).getInt(null);
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to intialize class");
        }
    }
}
