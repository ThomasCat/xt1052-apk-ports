package com.motorola.aon;

public final class R {

    public static final class animator {
        public static final int dotanim = 2130968576;
    }

    public static final class array {
        public static final int breathing_times = 2131099648;
        public static final int config_default_black_list = 2131099649;
        public static final int config_hidden_white_list_apps = 2131099650;
        public static final int config_white_list_excluded_apps = 2131099651;
        public static final int config_white_list_locked_apps = 2131099652;
        public static final int config_white_list_system_apps = 2131099653;
        public static final int ramp_down_stage = 2131099654;
        public static final int ramp_up_stage = 2131099655;
    }

    public static final class attr {
    }

    public static final class bool {
        public static final int flatup_enable = 2131165184;
        public static final int haptics_disable = 2131165185;
    }

    public static final class color {
        public static final int blue = 2131230720;
        public static final int text = 2131230721;
        public static final int text2 = 2131230722;
    }

    public static final class dimen {
        public static final int action_height = 2131296256;
        public static final int breathing_content_left_margin = 2131296257;
        public static final int center_icon_X = 2131296258;
        public static final int center_icon_Y = 2131296259;
        public static final int clock_font_size = 2131296260;
        public static final int clock_height = 2131296261;
        public static final int clock_start_X = 2131296262;
        public static final int clock_start_Y = 2131296263;
        public static final int clock_width = 2131296264;
        public static final int enhanced_view_clock_left_offset = 2131296265;
        public static final int notification_icons_X = 2131296266;
        public static final int notification_icons_Y = 2131296267;
        public static final int region0_end = 2131296268;
        public static final int region0_start = 2131296269;
        public static final int region1_end = 2131296270;
        public static final int region1_start = 2131296271;
        public static final int region2_end = 2131296272;
        public static final int region2_start = 2131296273;
        public static final int region3_end = 2131296274;
        public static final int region3_start = 2131296275;
        public static final int region4_end = 2131296276;
        public static final int region4_start = 2131296277;
        public static final int region5_end = 2131296278;
        public static final int region5_start = 2131296279;
        public static final int region6_end = 2131296280;
        public static final int region6_start = 2131296281;
        public static final int region7_end = 2131296282;
        public static final int region7_start = 2131296283;
        public static final int region_clock_only_end = 2131296284;
        public static final int status_bar_image_size = 2131296285;
        public static final int touch_region_down_x0 = 2131296286;
        public static final int touch_region_down_x1 = 2131296287;
        public static final int touch_region_down_y0 = 2131296288;
        public static final int touch_region_down_y1 = 2131296289;
        public static final int touch_region_fade_x3 = 2131296290;
        public static final int touch_region_fade_x4 = 2131296291;
        public static final int touch_region_middle_x0 = 2131296292;
        public static final int touch_region_middle_x1 = 2131296293;
        public static final int touch_region_middle_y0 = 2131296294;
        public static final int touch_region_middle_y1 = 2131296295;
        public static final int touch_region_mute_ultram_offset = 2131296296;
        public static final int touch_region_mute_x1 = 2131296297;
        public static final int touch_region_mute_x2 = 2131296298;
        public static final int touch_region_mute_x5 = 2131296299;
        public static final int touch_region_mute_x6 = 2131296300;
        public static final int touch_region_mute_y1 = 2131296301;
        public static final int touch_region_mute_y2 = 2131296302;
        public static final int touch_region_upper_x0 = 2131296303;
        public static final int touch_region_upper_x1 = 2131296304;
        public static final int touch_region_upper_y0 = 2131296305;
        public static final int touch_region_upper_y1 = 2131296306;
        public static final int white_list_left_right_padding = 2131296307;
    }

    public static final class drawable {
        public static final int black_with_gradient_alpha = 2130837504;
        public static final int blank_shape = 2130837505;
        public static final int dot = 2130837506;
        public static final int ic_ani_dot = 2130837507;
        public static final int ic_battery15 = 2130837508;
        public static final int ic_battery5 = 2130837509;
        public static final int ic_clock_0 = 2130837510;
        public static final int ic_clock_1 = 2130837511;
        public static final int ic_clock_2 = 2130837512;
        public static final int ic_clock_3 = 2130837513;
        public static final int ic_clock_4 = 2130837514;
        public static final int ic_clock_5 = 2130837515;
        public static final int ic_clock_6 = 2130837516;
        public static final int ic_clock_7 = 2130837517;
        public static final int ic_clock_8 = 2130837518;
        public static final int ic_clock_9 = 2130837519;
        public static final int ic_clock_colon = 2130837520;
        public static final int ic_contact_picture = 2130837521;
        public static final int ic_control_ellipsis = 2130837522;
        public static final int ic_launcher = 2130837523;
        public static final int ic_lock = 2130837524;
        public static final int ic_peek_notify_sneak = 2130837525;
        public static final int ic_settings_sn = 2130837526;
        public static final int ic_stat_ellipsis = 2130837527;
        public static final int ic_thb_circle_focused = 2130837528;
        public static final int ic_tutorial_target = 2130837529;
        public static final int ic_tutorial_target_breathe = 2130837530;
        public static final int ic_tutorial_target_peek = 2130837531;
        public static final int ic_unlock = 2130837532;
        public static final int learn_peek_all = 2130837533;
        public static final int learn_peek_new = 2130837534;
        public static final int learn_peek_stop = 2130837535;
        public static final int learn_white_list = 2130837536;
        public static final int nav_divider = 2130837537;
        public static final int peek_mask = 2130837538;
        public static final int spinner_background_holo_dark = 2130837539;
        public static final int spinner_default_holo_dark = 2130837540;
        public static final int spinner_disabled_holo_dark = 2130837541;
        public static final int spinner_focused_holo_dark = 2130837542;
        public static final int spinner_pressed_holo_dark = 2130837543;
        public static final int tutorial_peek = 2130837544;
        public static final int tutorial_peek_all = 2130837545;
        public static final int tutorial_peek_new = 2130837546;
        public static final int tutorial_peek_stop = 2130837547;
        public static final int tutorial_white_list = 2130837548;
    }

    public static final class id {
        public static final int breathe_tutorial = 2131623973;
        public static final int breathing_content = 2131623959;
        public static final int btn_learn_more = 2131624005;
        public static final int check = 2131624001;
        public static final int checkbox_pref = 2131623992;
        public static final int chrome_setting_status = 2131623995;
        public static final int chrome_setting_summary = 2131623994;
        public static final int chrome_setting_title = 2131623993;
        public static final int circle_center = 2131623971;
        public static final int circle_top = 2131623953;
        public static final int clock_layout = 2131623960;
        public static final int colon = 2131623964;
        public static final int divider = 2131623996;
        public static final int dot01 = 2131623956;
        public static final int dot02 = 2131623957;
        public static final int dot03 = 2131623958;
        public static final int dot11 = 2131623981;
        public static final int dot12 = 2131623982;
        public static final int dot13 = 2131623983;
        public static final int enhanced_view_padding_l = 2131623961;
        public static final int enhanced_view_padding_r = 2131623967;
        public static final int enhanced_view_shift_padding = 2131623968;
        public static final int frame_center = 2131623970;
        public static final int frame_top = 2131623952;
        public static final int go_white_list = 2131623939;
        public static final int hour0 = 2131623962;
        public static final int hour1 = 2131623963;
        public static final int icon = 2131623999;
        public static final int imageView1 = 2131623937;
        public static final int img_battery = 2131623969;
        public static final int inbox0 = 2131623944;
        public static final int inbox1 = 2131623945;
        public static final int inbox2 = 2131623946;
        public static final int min0 = 2131623965;
        public static final int min1 = 2131623966;
        public static final int more_icons = 2131623975;
        public static final int night_begin = 2131624003;
        public static final int night_end = 2131624004;
        public static final int noti0 = 2131623972;
        public static final int noti1 = 2131623977;
        public static final int noti2 = 2131623978;
        public static final int noti3 = 2131623979;
        public static final int noti_container = 2131623976;
        public static final int noti_more = 2131623980;
        public static final int peek_bottom = 2131623984;
        public static final int peek_noti0 = 2131623954;
        public static final int peek_noti1 = 2131623987;
        public static final int peek_noti2 = 2131623988;
        public static final int peek_noti3 = 2131623989;
        public static final int peek_noti_container = 2131623986;
        public static final int peek_noti_more = 2131623985;
        public static final int peek_top = 2131623941;
        public static final int peek_top_content = 2131623942;
        public static final int peek_tutorial = 2131623955;
        public static final int picture = 2131623951;
        public static final int picture_frame = 2131623950;
        public static final int scrollView1 = 2131623936;
        public static final int select_all = 2131624010;
        public static final int select_none = 2131624011;
        public static final int settings = 2131623997;
        public static final int subtext = 2131624000;
        public static final int svBreathingDetails = 2131623990;
        public static final int text = 2131623948;
        public static final int text2 = 2131623947;
        public static final int textDone = 2131624009;
        public static final int textNext = 2131623940;
        public static final int textSettings = 2131624008;
        public static final int textView1 = 2131624002;
        public static final int title = 2131623943;
        public static final int toggle_auto_select = 2131624007;
        public static final int tutorial_touch = 2131623974;
        public static final int view_pager = 2131623938;
        public static final int when = 2131623949;
        public static final int whiteListSetting = 2131623991;
        public static final int white_list_auto_select_installed = 2131624006;
        public static final int white_list_item = 2131623998;
    }

    public static final class integer {
        public static final int aod_interface_version = 2131361792;
        public static final int breathing_delay = 2131361793;
        public static final int dim_delay = 2131361794;
        public static final int flatup_exit_cooldown = 2131361795;
        public static final int flatup_exit_hysteresis = 2131361796;
        public static final int gesture_plateau = 2131361797;
        public static final int hard_shake_cooldown = 2131361798;
        public static final int hard_shake_count_increment = 2131361799;
        public static final int hard_shake_count_threshold = 2131361800;
        public static final int hard_shake_threshold_x = 2131361801;
        public static final int hard_shake_threshold_y = 2131361802;
        public static final int hard_shake_threshold_z = 2131361803;
        public static final int incoming_plateau = 2131361804;
        public static final int late_touch_timer = 2131361805;
        public static final int lazy_touch_timer = 2131361806;
        public static final int movement_threshold = 2131361807;
        public static final int movement_time = 2131361808;
        public static final int mute_touch_timer = 2131361809;
        public static final int no_movement_time = 2131361810;
        public static final int plateau_time = 2131361811;
        public static final int ramp_down_stage_sum = 2131361812;
        public static final int ramp_up_stage_sum = 2131361813;
        public static final int stowed_time = 2131361814;
        public static final int transition_delay = 2131361815;
    }

    public static final class layout {
        public static final int aod_fd_tutorial1 = 2130903040;
        public static final int aod_tutorial = 2130903041;
        public static final int aod_tutorial1 = 2130903042;
        public static final int aod_tutorial2 = 2130903043;
        public static final int aod_tutorial3 = 2130903044;
        public static final int aod_tutorial4 = 2130903045;
        public static final int aod_tutorial5 = 2130903046;
        public static final int breathing = 2130903047;
        public static final int breathing_details = 2130903048;
        public static final int breathing_details_fd = 2130903049;
        public static final int chrome_setting = 2130903050;
        public static final int list_item_icon_text = 2130903051;
        public static final int night_settings = 2130903052;
        public static final int preference_checkbox_setting = 2130903053;
        public static final int preference_checkbox_setting_widget = 2130903054;
        public static final int sn_settings_learn_more_brief = 2130903055;
        public static final int white_list_auto_select_installed = 2130903056;
        public static final int white_list_tutorial = 2130903057;
    }

    public static final class menu {
        public static final int white_list = 2131558400;
    }

    public static final class string {
        public static final int alert_dialog_cancel = 2131427328;
        public static final int alert_dialog_ok = 2131427329;
        public static final int aod_default_version = 2131427330;
        public static final int aod_t1_description = 2131427331;
        public static final int aod_t1_description_fd = 2131427332;
        public static final int aod_t1_title = 2131427333;
        public static final int aod_t2_summary = 2131427334;
        public static final int aod_t2_title = 2131427335;
        public static final int aod_t3_summary = 2131427336;
        public static final int aod_t3_title = 2131427337;
        public static final int aod_t4_summary = 2131427338;
        public static final int aod_t4_title = 2131427339;
        public static final int aod_t5_auto_sel = 2131427340;
        public static final int aod_t5_dlg_message = 2131427341;
        public static final int aod_t5_dlg_title = 2131427342;
        public static final int aod_t5_summary = 2131427343;
        public static final int aod_t5_summary_selectable = 2131427344;
        public static final int aod_t5_title = 2131427345;
        public static final int aod_version = 2131427346;
        public static final int aod_version_key = 2131427347;
        public static final int aon_widget_name = 2131427424;
        public static final int app_name = 2131427348;
        public static final int battery_savings_fd_summary = 2131427349;
        public static final int battery_savings_summary = 2131427350;
        public static final int battery_savings_title = 2131427351;
        public static final int black_list = 2131427425;
        public static final int black_list_title = 2131427426;
        public static final int breathing_details_brief = 2131427352;
        public static final int breathing_details_summary1 = 2131427353;
        public static final int breathing_details_summary2 = 2131427354;
        public static final int breathing_details_title = 2131427355;
        public static final int chrome_key = 2131427356;
        public static final int computer_key = 2131427357;
        public static final int done = 2131427358;
        public static final int inbox_more = 2131427359;
        public static final int learn_more_aon_summary = 2131427360;
        public static final int learn_more_aon_title = 2131427361;
        public static final int learn_more_chrome_summary = 2131427362;
        public static final int learn_more_chrome_title = 2131427363;
        public static final int learn_more_setup_chrome = 2131427364;
        public static final int learn_more_view_chrome_details = 2131427365;
        public static final int loading_devcount = 2131427366;
        public static final int menu_select_all = 2131427367;
        public static final int menu_select_none = 2131427368;
        public static final int next = 2131427369;
        public static final int night_settings_message = 2131427370;
        public static final int night_settings_night_end_picker_title = 2131427371;
        public static final int night_settings_night_start_picker_title = 2131427372;
        public static final int night_settings_title = 2131427373;
        public static final int night_settings_to = 2131427374;
        public static final int no_connection = 2131427375;
        public static final int not_breathing_summary = 2131427376;
        public static final int not_breathing_title = 2131427377;
        public static final int not_signed_in = 2131427378;
        public static final int ok_done = 2131427379;
        public static final int peek_more_info = 2131427380;
        public static final int privacy_policy = 2131427381;
        public static final int privacy_policy_key = 2131427382;
        public static final int ready_to_send = 2131427383;
        public static final int settings = 2131427384;
        public static final int signing_in = 2131427385;
        public static final int signing_out = 2131427386;
        public static final int sn_setting_aon_summary = 2131427387;
        public static final int sn_setting_aon_title = 2131427388;
        public static final int sn_setting_category_other = 2131427389;
        public static final int sn_setting_category_phone = 2131427390;
        public static final int sn_setting_chrome_summary_1 = 2131427391;
        public static final int sn_setting_chrome_summary_2 = 2131427392;
        public static final int sn_setting_chrome_title = 2131427393;
        public static final int sn_setting_fd_aon_summary = 2131427394;
        public static final int sn_setting_full_title = 2131427395;
        public static final int sn_setting_learn_more_brief_summary = 2131427396;
        public static final int sn_setting_learn_more_button_title = 2131427397;
        public static final int sn_setting_manage_types_header = 2131427398;
        public static final int sn_setting_manage_types_summary = 2131427399;
        public static final int sn_setting_night_summary = 2131427400;
        public static final int sn_setting_night_title = 2131427401;
        public static final int sn_setting_privacy_disable_widgets_policy = 2131427402;
        public static final int sn_setting_privacy_summary = 2131427403;
        public static final int sn_setting_privacy_title = 2131427404;
        public static final int sn_setting_title = 2131427405;
        public static final int sn_view_details = 2131427406;
        public static final int stop_breathing_summary = 2131427407;
        public static final int stop_breathing_title = 2131427408;
        public static final int string_new = 2131427409;
        public static final int time_24 = 2131427410;
        public static final int time_am = 2131427411;
        public static final int time_pm = 2131427412;
        public static final int toast_msg = 2131427413;
        public static final int toast_msg_out = 2131427414;
        public static final int tutorial_aod_description = 2131427415;
        public static final int tutorial_aod_info_drag = 2131427416;
        public static final int tutorial_aod_info_touch = 2131427417;
        public static final int tutorial_aod_title = 2131427418;
        public static final int tutorial_whitelist_description = 2131427419;
        public static final int tutorial_whitelist_title = 2131427420;
        public static final int white_list_auto_select_installed = 2131427421;
        public static final int white_list_description_item = 2131427422;
        public static final int white_list_googe_search_title = 2131427423;
    }

    public static final class style {
        public static final int AppTheme = 2131492864;
        public static final int IncomingActivityTheme = 2131492865;
        public static final int dialog = 2131492866;
    }

    public static final class xml {
        public static final int aon_setting_dynamic_header = 2131034112;
        public static final int sn_setup = 2131034113;
        public static final int sn_setup_fd = 2131034114;
    }
}
