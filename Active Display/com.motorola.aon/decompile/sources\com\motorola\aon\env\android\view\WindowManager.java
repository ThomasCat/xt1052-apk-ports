package com.motorola.aon.env.android.view;

import android.util.Log;
import java.lang.reflect.Field;

public class WindowManager {

    public static class LayoutParams {
        private static final String CLASS_WINDOWMANAGER_LAYOUTPARAMS = "android.view.WindowManager$LayoutParams";
        private static final String FIELD_MIN_SCREEN_BRIGHTNESS = "minScreenBrightness";
        private static final String FIELD_TYPE_AOD = "TYPE_AOD";
        private static final String TAG = "Aon_LayoutParams";
        public static int TYPE_AOD;
        static Field sFdMinScreenBrightness;
        public static boolean sInitialized;

        public static void setMinScreenBrightness(Object lp, int brightness) {
            if (sInitialized && sFdMinScreenBrightness != null) {
                try {
                    sFdMinScreenBrightness.setInt(lp, brightness);
                } catch (Throwable th) {
                    Log.w(TAG, "unable to set the minScreenBrightness");
                }
            }
        }

        static {
            sInitialized = false;
            try {
                Class<?> clazz = Class.forName(CLASS_WINDOWMANAGER_LAYOUTPARAMS);
                sFdMinScreenBrightness = clazz.getDeclaredField(FIELD_MIN_SCREEN_BRIGHTNESS);
                TYPE_AOD = clazz.getDeclaredField(FIELD_TYPE_AOD).getInt(null);
                sInitialized = true;
            } catch (Throwable th) {
                Log.w(TAG, "unable to initialize class");
            }
        }
    }
}
