package com.motorola.aon.pd;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.util.Log;
import com.motorola.aon.AonNotificationManager;
import com.motorola.aon.AonService;
import com.motorola.aon.R;
import com.motorola.aon.SnAnalyticsEventService;
import com.motorola.aon.jni.PartialDisplay;
import com.motorola.aon.jni.PartialDisplay.StatusRegister;

public class AonNotificationManagerBreath extends AonNotificationManager {
    static final int PEAK_RETRY_DELAY = 500;
    static final String TAG = "AonNotifManagerBreath";
    static final int WAIT_FOR_UPDATE_BREATHING_SCREEN_ON = 3000;
    static final int WAIT_PEEK_TIMEOUT = 20000;
    protected final int mBreathingDelay;
    /* access modifiers changed from: private */
    public boolean mInPartialMode;
    Runnable mRefreshBreathingRunnable = new Runnable() {
        public void run() {
            if (AonNotificationManagerBreath.DEBUG) {
                Log.d(AonNotificationManagerBreath.TAG, "mRefreshBreathingRunnable");
            }
            boolean isPeeking = false;
            if (!AonNotificationManagerBreath.this.mScreenOn && AonNotificationManagerBreath.this.mInPartialMode) {
                StatusRegister sr = new StatusRegister();
                PartialDisplay.nativeReadStatusRegister(sr);
                if (AonNotificationManagerBreath.DEBUG) {
                    Log.d(AonNotificationManagerBreath.TAG, "Peek status " + sr.mPeek);
                }
                isPeeking = sr.mPeek == 1;
            }
            if (isPeeking) {
                long now = SystemClock.uptimeMillis();
                if (AonNotificationManagerBreath.this.mWaitPeekBegin > 0) {
                    long wait = now - AonNotificationManagerBreath.this.mWaitPeekBegin;
                    if (AonNotificationManagerBreath.DEBUG) {
                        Log.d(AonNotificationManagerBreath.TAG, "wait peek finish for " + wait);
                    }
                    if (wait > 20000) {
                        if (AonNotificationManagerBreath.DEBUG) {
                            Log.d(AonNotificationManagerBreath.TAG, "wait time out");
                        }
                        AonNotificationManagerBreath.this.mWaitPeekBegin = -1;
                        AonNotificationManagerBreath.this.mHandler.removeCallbacks(AonNotificationManagerBreath.this.mRefreshBreathingRunnable);
                        AonNotificationManagerBreath.this.doRequestUpdateBreathing();
                        return;
                    }
                    if (AonNotificationManagerBreath.DEBUG) {
                        Log.d(AonNotificationManagerBreath.TAG, "retry in 500 ms");
                    }
                    AonNotificationManagerBreath.this.mHandler.removeCallbacks(AonNotificationManagerBreath.this.mRefreshBreathingRunnable);
                    AonNotificationManagerBreath.this.mHandler.postDelayed(AonNotificationManagerBreath.this.mRefreshBreathingRunnable, 500);
                    return;
                }
                if (AonNotificationManagerBreath.DEBUG) {
                    Log.d(AonNotificationManagerBreath.TAG, "start wait for peek finished");
                }
                AonNotificationManagerBreath.this.mWaitPeekBegin = now;
                if (!AonNotificationManagerBreath.this.mWakeLock.isHeld()) {
                    if (AonNotificationManagerBreath.DEBUG) {
                        Log.d(AonNotificationManagerBreath.TAG, "request wake lock to wait for peek finished");
                    }
                    AonNotificationManagerBreath.this.mWakeLock.acquire();
                }
                AonNotificationManagerBreath.this.mHandler.removeCallbacks(AonNotificationManagerBreath.this.mRefreshBreathingRunnable);
                AonNotificationManagerBreath.this.mHandler.postDelayed(AonNotificationManagerBreath.this.mRefreshBreathingRunnable, 500);
                return;
            }
            AonNotificationManagerBreath.this.mHandler.removeCallbacks(AonNotificationManagerBreath.this.mRefreshBreathingRunnable);
            AonNotificationManagerBreath.this.doRequestUpdateBreathing();
        }
    };
    /* access modifiers changed from: private */
    public boolean mScreenOn;
    long mWaitPeekBegin = -1;
    /* access modifiers changed from: private */
    public WakeLock mWakeLock;
    private Runnable mWakeLockTimeOut = new Runnable() {
        public void run() {
            if (AonNotificationManagerBreath.this.mWakeLock.isHeld()) {
                Log.w(AonNotificationManagerBreath.TAG, "wake lock timeout");
                AonNotificationManagerBreath.this.mWakeLock.release();
            }
        }
    };

    public AonNotificationManagerBreath(Context context) {
        super(context);
        this.mBreathingDelay = context.getResources().getInteger(R.integer.breathing_delay);
        PowerManager pm = (PowerManager) this.mContext.getSystemService("power");
        this.mWakeLock = pm.newWakeLock(1, "aon_wake_lock");
        this.mScreenOn = pm.isScreenOn();
        if (!this.mScreenOn) {
            enableTracking(true);
        }
    }

    /* access modifiers changed from: protected */
    public void notifyRefreshBreathingViewOnNotification() {
        if (this.mAonSettingOn && !this.mScreenOn) {
            SnAnalyticsEventService.setIncomingRefresh(true);
            if (!this.mWakeLock.isHeld()) {
                if (DEBUG) {
                    Log.d(TAG, "request wake lock for update breathing view due to notification change");
                }
                this.mWakeLock.acquire();
            }
            this.mHandler.removeCallbacks(this.mRefreshBreathingRunnable);
            this.mHandler.postDelayed(this.mRefreshBreathingRunnable, (long) this.mBreathingDelay);
        }
    }

    public void notifyForceRefreshBreathingView() {
        if (this.mAonSettingOn && !this.mScreenOn) {
            if (!this.mWakeLock.isHeld()) {
                if (DEBUG) {
                    Log.d(TAG, "request wake lock for update breathing view due to context change");
                }
                this.mWakeLock.acquire();
            }
            this.mHandler.removeCallbacks(this.mRefreshBreathingRunnable);
            this.mHandler.postDelayed(this.mRefreshBreathingRunnable, (long) this.mBreathingDelay);
        }
    }

    /* access modifiers changed from: private */
    public void doRequestUpdateBreathing() {
        this.mHandler.removeCallbacks(this.mWakeLockTimeOut);
        this.mHandler.postDelayed(this.mWakeLockTimeOut, 3000);
        if (!this.mScreenOn) {
            Intent intent = new Intent(this.mContext, AonServiceBreath.class);
            intent.setAction(AonService.ACTION_REFRESH_BREATHING);
            this.mContext.startService(intent);
        }
    }

    public void releaseWakeLock() {
        if (this.mWakeLock.isHeld()) {
            if (DEBUG) {
                Log.d(TAG, "release wake lock");
            }
            this.mWakeLock.release();
        }
    }

    public void onScreenOn(int reason) {
        if (DEBUG) {
            Log.d(TAG, "onScreenOn " + reason);
        }
        this.mHandler.removeCallbacks(this.mRefreshBreathingRunnable);
        this.mHandler.removeCallbacks(this.mWakeLockTimeOut);
        if (reason == 2 || reason == 1 || reason == 3) {
            enableTracking(false);
        }
        releaseWakeLock();
        this.mScreenOn = true;
    }

    public void onScreenOff(boolean inPartialMode) {
        if (DEBUG) {
            Log.d(TAG, "onScreenOff");
        }
        this.mInPartialMode = inPartialMode;
        enableTracking(true);
        this.mScreenOn = false;
        this.mWaitPeekBegin = -1;
        if (this.mWakeLock.isHeld()) {
            Log.e(TAG, "ERROR: release wake lock");
            this.mWakeLock.release();
        }
    }
}
