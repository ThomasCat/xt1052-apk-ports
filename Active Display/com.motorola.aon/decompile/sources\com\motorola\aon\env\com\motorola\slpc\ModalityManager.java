package com.motorola.aon.env.com.motorola.slpc;

import android.util.Log;
import android.util.Pair;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Iterator;

public class ModalityManager {
    private static final String CLASS_MODALITY_MANAGER = "com.motorola.slpc.ModalityManager";
    public static long DURATION_IGNORE = 0;
    private static final String FIELD_DURATION_IGNORE = "DURATION_IGNORE";
    private static final String FIELD_ORIENTATION_ALL = "ORIENTATION_ALL";
    private static final String FIELD_ORIENTATION_FACE_DOWN = "ORIENTATION_FACE_DOWN";
    private static final String FIELD_ORIENTATION_FACE_UP = "ORIENTATION_FACE_UP";
    private static final String FIELD_ORIENTATION_UNKNOWN = "ORIENTATION_UNKNOWN";
    private static final String FIELD_STOWED_ALL = "STOWED_ALL";
    private static final String FIELD_STOWED_FALSE = "STOWED_FALSE";
    private static final String FIELD_STOWED_TRUE = "STOWED_TRUE";
    private static final String INTERFACE_MOVEMENT_LISTENER = "com.motorola.slpc.ModalityManager$MovementListener";
    private static final String INTERFACE_ORIENTATION_LISTENER = "com.motorola.slpc.ModalityManager$OrientationListener";
    private static final String INTERFACE_STOWED_LISTENER = "com.motorola.slpc.ModalityManager$StowedListener";
    private static final String METHOD_ADD_MOVEMENT_LISTENER = "addMovementListener";
    private static final String METHOD_ADD_ORIENTATION_LISTENER = "addOrientationListener";
    private static final String METHOD_ADD_STOWED_LISTENER = "addStowedListener";
    private static final String METHOD_EQUALS = "equals";
    private static final String METHOD_GET_ORIENTATION = "getOrientation";
    private static final String METHOD_GET_STOWED = "getStowed";
    private static final String METHOD_HASH_CODE = "hashCode";
    private static final String METHOD_ON_END_MOVEMENT = "onEndMovement";
    private static final String METHOD_ON_ORIENTATION_CHANGE = "onOrientationChange";
    private static final String METHOD_ON_START_MOVEMENT = "onStartMovement";
    private static final String METHOD_ON_STOWED_CHANGE = "onStowedChange";
    private static final String METHOD_REMOVE_MOVEMENT_LISTENER = "removeMovementListener";
    private static final String METHOD_REMOVE_ORIENTATION_LISTENER = "removeOrientationListener";
    private static final String METHOD_REMOVE_STOWED_LISTENER = "removeStowedListener";
    public static int ORIENTATION_ALL = 0;
    public static int ORIENTATION_FACE_DOWN = 0;
    public static int ORIENTATION_FACE_UP = 0;
    public static int ORIENTATION_UNKNOWN = 0;
    public static int STOWED_ALL = 0;
    public static int STOWED_FALSE = 0;
    public static int STOWED_TRUE = 0;
    private static final String TAG = "Aon_ModalityManager";
    private static Class<?> sIfMovementListener;
    private static Class<?> sIfOrientationListener;
    private static Class<?> sIfStowedListener;
    public static boolean sInitialized;
    private static Method sMethodAddMovementListener;
    private static Method sMethodAddOrientationListener;
    private static Method sMethodAddStowedListener;
    private static Method sMethodGetOrientation;
    private static Method sMethodGetStowed;
    private static Method sMethodRemoveMovementListener;
    private static Method sMethodRemoveOrientationListener;
    private static Method sMethodRemoveStowedListener;
    /* access modifiers changed from: private */
    public ArrayList<Pair<MovementListener, Object>> mAlMovementListeners = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<Pair<OrientationListener, Object>> mAlOrientationListeners = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<Pair<StowedListener, Object>> mAlStowedListeners = new ArrayList<>();
    private final Object mObjModalityManager;

    public interface MovementListener {
        void onEndMovement();

        void onStartMovement();
    }

    public interface OrientationListener {
        void onOrientationChange(Transition transition);
    }

    public interface StowedListener {
        void onStowedChange(Transition transition);
    }

    static {
        STOWED_TRUE = 0;
        STOWED_FALSE = 0;
        STOWED_ALL = 0;
        DURATION_IGNORE = 0;
        ORIENTATION_FACE_DOWN = 0;
        ORIENTATION_FACE_UP = 0;
        ORIENTATION_UNKNOWN = 0;
        ORIENTATION_ALL = 0;
        sInitialized = false;
        try {
            Class<?> clazz = Class.forName(CLASS_MODALITY_MANAGER);
            sIfOrientationListener = Class.forName(INTERFACE_ORIENTATION_LISTENER);
            sIfMovementListener = Class.forName(INTERFACE_MOVEMENT_LISTENER);
            sIfStowedListener = Class.forName(INTERFACE_STOWED_LISTENER);
            sMethodAddStowedListener = clazz.getDeclaredMethod(METHOD_ADD_STOWED_LISTENER, new Class[]{sIfStowedListener, Integer.TYPE, Long.TYPE, Long.TYPE});
            sMethodAddMovementListener = clazz.getDeclaredMethod(METHOD_ADD_MOVEMENT_LISTENER, new Class[]{sIfMovementListener, Long.TYPE, Long.TYPE});
            sMethodAddOrientationListener = clazz.getDeclaredMethod(METHOD_ADD_ORIENTATION_LISTENER, new Class[]{sIfOrientationListener, Integer.TYPE, Long.TYPE, Long.TYPE});
            sMethodRemoveStowedListener = clazz.getDeclaredMethod(METHOD_REMOVE_STOWED_LISTENER, new Class[]{sIfStowedListener});
            sMethodRemoveOrientationListener = clazz.getDeclaredMethod(METHOD_REMOVE_ORIENTATION_LISTENER, new Class[]{sIfOrientationListener});
            sMethodRemoveMovementListener = clazz.getDeclaredMethod(METHOD_REMOVE_MOVEMENT_LISTENER, new Class[]{sIfMovementListener});
            sMethodGetOrientation = clazz.getDeclaredMethod(METHOD_GET_ORIENTATION, new Class[0]);
            sMethodGetStowed = clazz.getDeclaredMethod(METHOD_GET_STOWED, new Class[0]);
            ORIENTATION_FACE_DOWN = clazz.getDeclaredField(FIELD_ORIENTATION_FACE_DOWN).getInt(null);
            ORIENTATION_FACE_UP = clazz.getDeclaredField(FIELD_ORIENTATION_FACE_UP).getInt(null);
            ORIENTATION_UNKNOWN = clazz.getDeclaredField(FIELD_ORIENTATION_UNKNOWN).getInt(null);
            ORIENTATION_ALL = clazz.getDeclaredField(FIELD_ORIENTATION_ALL).getInt(null);
            STOWED_TRUE = clazz.getDeclaredField(FIELD_STOWED_TRUE).getInt(null);
            STOWED_FALSE = clazz.getDeclaredField(FIELD_STOWED_FALSE).getInt(null);
            STOWED_ALL = clazz.getDeclaredField(FIELD_STOWED_ALL).getInt(null);
            DURATION_IGNORE = clazz.getDeclaredField(FIELD_DURATION_IGNORE).getLong(null);
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize modality manager");
        }
    }

    private ModalityManager(Object objModalidyManager) {
        this.mObjModalityManager = objModalidyManager;
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [boolean] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r3v0, types: [boolean]
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY]]
  uses: [boolean, ?[int, short, byte, char]]
  mth insns count: 36
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    public boolean addStowedListener(final StowedListener listener, int stowedStates, long startDuration, long endDuration) {
        ? r3 = 0;
        if (!sInitialized || sMethodAddStowedListener == null) {
            return r3;
        }
        Object objStowedListener = Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{sIfStowedListener}, new InvocationHandler() {
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                int argLen;
                boolean z = false;
                String methodName = method.getName();
                if (args != null) {
                    argLen = args.length;
                } else {
                    argLen = 0;
                }
                if (ModalityManager.METHOD_ON_STOWED_CHANGE.equals(methodName) && argLen == 1) {
                    StowedListener stowedListener = null;
                    Iterator i$ = ModalityManager.this.mAlStowedListeners.iterator();
                    while (true) {
                        if (!i$.hasNext()) {
                            break;
                        }
                        Pair<StowedListener, Object> each = (Pair) i$.next();
                        if (each.second == proxy) {
                            stowedListener = (StowedListener) each.first;
                            break;
                        }
                    }
                    if (stowedListener != null) {
                        stowedListener.onStowedChange(Transition.from(args[0]));
                    }
                } else if (ModalityManager.METHOD_HASH_CODE.equals(methodName)) {
                    return Integer.valueOf(listener.hashCode());
                } else {
                    if (ModalityManager.METHOD_EQUALS.equals(methodName) && argLen == 1) {
                        if (listener.hashCode() == args[0].hashCode()) {
                            z = true;
                        }
                        return Boolean.valueOf(z);
                    }
                }
                return null;
            }
        });
        try {
            this.mAlStowedListeners.add(new Pair(listener, objStowedListener));
            return ((Boolean) sMethodAddStowedListener.invoke(this.mObjModalityManager, new Object[]{objStowedListener, Integer.valueOf(stowedStates), Long.valueOf(startDuration), Long.valueOf(endDuration)})).booleanValue();
        } catch (Throwable th) {
            Log.w(TAG, "unable to invoke addStowedListener");
            return r3;
        }
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [boolean] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r3v0, types: [boolean]
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY]]
  uses: [boolean, ?[int, short, byte, char]]
  mth insns count: 36
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    public boolean addOrientationListener(final OrientationListener listener, int orientations, long startDuration, long endDuration) {
        ? r3 = 0;
        if (!sInitialized || sMethodAddOrientationListener == null) {
            return r3;
        }
        Object objOrientationListener = Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{sIfOrientationListener}, new InvocationHandler() {
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                int argLen;
                boolean z = false;
                String methodName = method.getName();
                if (args != null) {
                    argLen = args.length;
                } else {
                    argLen = 0;
                }
                if (ModalityManager.METHOD_ON_ORIENTATION_CHANGE.equals(methodName) && argLen == 1) {
                    OrientationListener orientationListener = null;
                    Iterator i$ = ModalityManager.this.mAlOrientationListeners.iterator();
                    while (true) {
                        if (!i$.hasNext()) {
                            break;
                        }
                        Pair<OrientationListener, Object> each = (Pair) i$.next();
                        if (each.second == proxy) {
                            orientationListener = (OrientationListener) each.first;
                            break;
                        }
                    }
                    if (orientationListener != null) {
                        orientationListener.onOrientationChange(Transition.from(args[0]));
                    }
                } else if (ModalityManager.METHOD_HASH_CODE.equals(methodName)) {
                    return Integer.valueOf(listener.hashCode());
                } else {
                    if (ModalityManager.METHOD_EQUALS.equals(methodName) && argLen == 1) {
                        if (listener.hashCode() == args[0].hashCode()) {
                            z = true;
                        }
                        return Boolean.valueOf(z);
                    }
                }
                return null;
            }
        });
        try {
            this.mAlOrientationListeners.add(new Pair(listener, objOrientationListener));
            return ((Boolean) sMethodAddOrientationListener.invoke(this.mObjModalityManager, new Object[]{objOrientationListener, Integer.valueOf(orientations), Long.valueOf(startDuration), Long.valueOf(endDuration)})).booleanValue();
        } catch (Throwable th) {
            Log.w(TAG, "unable to invoke addOrientationListener");
            return r3;
        }
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [boolean] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r3v0, types: [boolean]
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY]]
  uses: [boolean, ?[int, short, byte, char]]
  mth insns count: 34
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    public boolean addMovementListener(final MovementListener listener, long startDuration, long endDuration) {
        ? r3 = 0;
        if (!sInitialized || sMethodAddMovementListener == null) {
            return r3;
        }
        Object objMovementListener = Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{sIfMovementListener}, new InvocationHandler() {
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                int argLen;
                boolean z = false;
                MovementListener movementListener = null;
                Iterator i$ = ModalityManager.this.mAlMovementListeners.iterator();
                while (true) {
                    if (!i$.hasNext()) {
                        break;
                    }
                    Pair<MovementListener, Object> each = (Pair) i$.next();
                    if (each.second == proxy) {
                        movementListener = (MovementListener) each.first;
                        break;
                    }
                }
                String methodName = method.getName();
                if (args != null) {
                    argLen = args.length;
                } else {
                    argLen = 0;
                }
                if (!ModalityManager.METHOD_ON_START_MOVEMENT.equals(methodName) || argLen != 0) {
                    if (!ModalityManager.METHOD_ON_END_MOVEMENT.equals(methodName) || argLen != 0) {
                        if (ModalityManager.METHOD_HASH_CODE.equals(methodName)) {
                            return Integer.valueOf(listener.hashCode());
                        }
                        if (ModalityManager.METHOD_EQUALS.equals(methodName) && argLen == 1) {
                            if (listener.hashCode() == args[0].hashCode()) {
                                z = true;
                            }
                            return Boolean.valueOf(z);
                        }
                    } else if (movementListener != null) {
                        movementListener.onEndMovement();
                    }
                } else if (movementListener != null) {
                    movementListener.onStartMovement();
                }
                return null;
            }
        });
        try {
            this.mAlMovementListeners.add(new Pair(listener, objMovementListener));
            return ((Boolean) sMethodAddMovementListener.invoke(this.mObjModalityManager, new Object[]{objMovementListener, Long.valueOf(startDuration), Long.valueOf(endDuration)})).booleanValue();
        } catch (Throwable th) {
            Log.w(TAG, "unable to invoke addMovementListener");
            return r3;
        }
    }

    public void removeMovementListener(MovementListener listener) {
        if (sInitialized && sMethodRemoveMovementListener != null) {
            Pair<MovementListener, Object> item = null;
            Iterator i$ = this.mAlMovementListeners.iterator();
            while (true) {
                if (!i$.hasNext()) {
                    break;
                }
                Pair<MovementListener, Object> each = (Pair) i$.next();
                if (each.first == listener) {
                    item = each;
                    break;
                }
            }
            if (item != null) {
                this.mAlMovementListeners.remove(item);
                try {
                    sMethodRemoveMovementListener.invoke(this.mObjModalityManager, new Object[]{item.second});
                } catch (Throwable th) {
                    Log.w(TAG, "unable to invoke removeMovementListener");
                }
            }
        }
    }

    public void removeOrientationListener(OrientationListener listener) {
        if (sInitialized && sMethodRemoveOrientationListener != null) {
            Pair<OrientationListener, Object> item = null;
            Iterator i$ = this.mAlOrientationListeners.iterator();
            while (true) {
                if (!i$.hasNext()) {
                    break;
                }
                Pair<OrientationListener, Object> each = (Pair) i$.next();
                if (each.first == listener) {
                    item = each;
                    break;
                }
            }
            if (item != null) {
                this.mAlOrientationListeners.remove(item);
                try {
                    sMethodRemoveOrientationListener.invoke(this.mObjModalityManager, new Object[]{item.second});
                } catch (Throwable th) {
                    Log.w(TAG, "unable to invoke removeOrientationListener");
                }
            }
        }
    }

    public void removeStowedListener(StowedListener listener) {
        if (sInitialized && sMethodRemoveStowedListener != null) {
            Pair<StowedListener, Object> item = null;
            Iterator i$ = this.mAlStowedListeners.iterator();
            while (true) {
                if (!i$.hasNext()) {
                    break;
                }
                Pair<StowedListener, Object> each = (Pair) i$.next();
                if (each.first == listener) {
                    item = each;
                    break;
                }
            }
            if (item != null) {
                this.mAlStowedListeners.remove(item);
                try {
                    sMethodRemoveStowedListener.invoke(this.mObjModalityManager, new Object[]{item.second});
                } catch (Throwable th) {
                    Log.w(TAG, "unable to invoke removeStowedListener");
                }
            }
        }
    }

    public static ModalityManager from(Object objModalidyManager) {
        if (objModalidyManager != null) {
            return new ModalityManager(objModalidyManager);
        }
        return null;
    }

    public Transition getOrientation() {
        Transition tr = null;
        if (!sInitialized || sMethodGetOrientation == null) {
            return tr;
        }
        try {
            return Transition.from(sMethodGetOrientation.invoke(this.mObjModalityManager, new Object[0]));
        } catch (Throwable th) {
            Log.w(TAG, "unable to invoke getOrientation");
            return tr;
        }
    }

    public Transition getStowed() {
        Transition tr = null;
        if (!sInitialized || sMethodGetStowed == null) {
            return tr;
        }
        try {
            return Transition.from(sMethodGetStowed.invoke(this.mObjModalityManager, new Object[0]));
        } catch (Throwable th) {
            Log.w(TAG, "unable to invoke getStowed");
            return tr;
        }
    }
}
