package com.motorola.aon.fd;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import com.motorola.aon.AonService;
import com.motorola.aon.AonService.IGetAonInterface;
import com.motorola.aon.AonViewManager;
import com.motorola.aon.Constants;
import com.motorola.aon.R;
import com.motorola.aon.aidl.NotificationEntry;
import com.motorola.aon.env.com.android.internal.statusbar.MinedData;
import java.util.ArrayList;

public class AonViewManagerGlimpse extends AonViewManager {
    static final String TAG = "AonViewManagerGlimpse";
    private int[] mDotRes = {R.id.dot03, R.id.dot11, R.id.dot02, R.id.dot12, R.id.dot01, R.id.dot13};
    /* access modifiers changed from: private */
    public boolean mGlimpseShown = false;
    /* access modifiers changed from: private */
    public int mNumNoti = 0;
    /* access modifiers changed from: private */
    public int[] mPeekDetailResArray = {R.id.picture, R.id.peek_top, R.id.noti_more, R.id.peek_bottom};
    Runnable mShowPeekDetailRunnable = new Runnable() {
        public void run() {
            if (!AonViewManagerGlimpse.this.mGlimpseShown) {
                AonViewManagerGlimpse.this.mCachedBreathingView.findViewById(R.id.breathing_content).setVisibility(4);
                if (AonViewManagerGlimpse.this.mNumNoti >= 1 || AonViewManagerGlimpse.this.mViewType == 2) {
                    for (int findViewById : AonViewManagerGlimpse.this.mPeekDetailResArray) {
                        AonViewManagerGlimpse.this.mCachedBreathingView.findViewById(findViewById).setVisibility(0);
                    }
                    AonViewManagerGlimpse.this.mPeekSmallNotiContainer.setVisibility(0);
                    return;
                }
                AonViewManagerGlimpse.this.mPeekSmallNotiContainer.setVisibility(8);
                AonViewManagerGlimpse.this.mCachedBreathingView.findViewById(R.id.peek_bottom).setVisibility(0);
            }
        }
    };
    /* access modifiers changed from: private */
    public int mViewType = 0;

    public AonViewManagerGlimpse(Context context, AonNotificationManagerGlimpse aonNotificationManager, IGetAonInterface getAonInterface) {
        super(context, aonNotificationManager, getAonInterface);
        initRes();
    }

    /* access modifiers changed from: protected */
    public void initRes() {
        super.initRes();
        this.mCachedBreathingView.findViewById(R.id.clock_layout).setVisibility(0);
    }

    public void showPeekDetail() {
        this.mGlimpseShown = false;
        for (int i = 0; i < this.mDotRes.length; i++) {
            if (this.mNumNoti >= 1 || this.mViewType == 2 || i % 2 != 0) {
                View view = this.mCachedBreathingView.findViewById(this.mDotRes[i]);
                AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(this.mContext, R.animator.dotanim);
                set.setStartDelay((long) ((i / 2) * 15));
                set.setTarget(view);
                set.start();
            }
        }
        this.mHandler.removeCallbacks(this.mShowPeekDetailRunnable);
        this.mHandler.postDelayed(this.mShowPeekDetailRunnable, 100);
    }

    public void showGlimpse() {
        this.mGlimpseShown = true;
        View view = this.mCachedBreathingView.findViewById(R.id.breathing_content);
        view.setAlpha(1.0f);
        view.setVisibility(0);
        for (int findViewById : this.mPeekDetailResArray) {
            this.mCachedBreathingView.findViewById(findViewById).setVisibility(4);
        }
        this.mPeekSmallNotiContainer.setVisibility(4);
    }

    public void fillBreathingView(View views, ArrayList<NotificationEntry> allBreathings, int batteryStatus, int viewType) {
        super.fillBreathingView(views, allBreathings, batteryStatus, viewType);
        this.mViewType = viewType;
        if (this.mCachedBreathingView.findViewById(R.id.breathing_content).getVisibility() == 0) {
            for (int findViewById : this.mPeekDetailResArray) {
                this.mCachedBreathingView.findViewById(findViewById).setVisibility(4);
            }
        }
        for (int findViewById2 : this.mDotRes) {
            this.mCachedBreathingView.findViewById(findViewById2).setAlpha(0.0f);
        }
        if (allBreathings != null) {
            this.mNumNoti = allBreathings.size();
        }
        if (this.mNumNoti > 0) {
            this.mBreathingNotificationEntry = (NotificationEntry) allBreathings.get(0);
        }
    }

    public View getGlimpseView() {
        return this.mCachedBreathingView;
    }

    public int getBreathSize() {
        return this.mNumNoti;
    }

    public Bitmap getNotificationBitmap(MinedData md, boolean big) {
        return big ? md.largeIcon : md.iconBitmap;
    }

    /* access modifiers changed from: protected */
    public void onTutorialLaunched() {
        Intent i2 = new Intent(this.mContext, AonServiceGlimpse.class);
        i2.setAction(AonService.ACTION_TUTORIAL_DONE);
        this.mContext.startService(i2);
    }

    public boolean isPeeking() {
        return this.mCachedBreathingView.findViewById(R.id.peek_bottom).getVisibility() == 0;
    }

    public void requestUpdateBreathingView(int viewType, ArrayList<NotificationEntry> breathingList, int batteryStatus) {
        fillBreathingView(this.mCachedBreathingView, breathingList, batteryStatus, viewType);
    }

    /* access modifiers changed from: protected */
    public void requestToDismissKeyguard() {
        Intent intent = new Intent(this.mContext, AonServiceGlimpse.class);
        intent.setAction(Constants.ACTION_DISMISS_KEYGUARD);
        this.mContext.startService(intent);
    }

    /* access modifiers changed from: protected */
    public Class getServiceClass() throws RuntimeException {
        return AonServiceGlimpse.class;
    }
}
