package com.motorola.aon.env.android.os;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;
import java.lang.reflect.Field;

public interface IRemoteCallback {

    public static abstract class Stub {
        private static final String CLASS_STUB = "android.os.IRemoteCallback$Stub";
        /* access modifiers changed from: private */
        public static String DESCRIPTOR = null;
        private static final String FIELD_DESC_NAME = "DESCRIPTOR";
        private static final String FIELD_TRANSACTION_sendResult = "TRANSACTION_sendResult";
        private static final String TAG = "Aon_IRemoteCallbk.Stub";
        /* access modifiers changed from: private */
        public static int TRANSACTION_sendResult;
        public static boolean sInitialized;
        private Binder mBinder = new Binder() {
            public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
                if (code == Stub.TRANSACTION_sendResult) {
                    data.enforceInterface(Stub.DESCRIPTOR);
                    Bundle args = null;
                    if (data.readInt() != 0) {
                        args = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    Stub.this.sendResult(args);
                    return true;
                } else if (code != 1598968902) {
                    return super.onTransact(code, data, reply, flags);
                } else {
                    reply.writeString(Stub.DESCRIPTOR);
                    return true;
                }
            }
        };

        public void sendResult(Bundle args) throws RemoteException {
        }

        public IBinder asBinder() {
            return this.mBinder;
        }

        static {
            DESCRIPTOR = null;
            TRANSACTION_sendResult = 0;
            sInitialized = false;
            try {
                Class<?> clazz = Class.forName(CLASS_STUB);
                Field f = clazz.getDeclaredField(FIELD_DESC_NAME);
                f.setAccessible(true);
                DESCRIPTOR = (String) f.get(null);
                Field f2 = clazz.getDeclaredField(FIELD_TRANSACTION_sendResult);
                f2.setAccessible(true);
                TRANSACTION_sendResult = f2.getInt(null);
                sInitialized = true;
            } catch (Throwable th) {
                Log.w(TAG, "unable to initialize class");
            }
        }
    }
}
