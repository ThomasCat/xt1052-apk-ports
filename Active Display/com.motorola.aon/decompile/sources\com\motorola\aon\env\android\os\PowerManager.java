package com.motorola.aon.env.android.os;

import android.util.Log;
import java.lang.reflect.Method;

public class PowerManager {
    private static final String CLASS_POWER_MANAGER = "android.os.PowerManager";
    private static final String FIELD_GO_TO_SLEEP_REASON_DEVICE_ADMIN = "GO_TO_SLEEP_REASON_DEVICE_ADMIN";
    private static final String FIELD_GO_TO_SLEEP_REASON_UPDATE_BREATHING = "GO_TO_SLEEP_REASON_UPDATE_BREATHING";
    private static final String FIELD_GO_TO_SLEEP_REASON_USER = "GO_TO_SLEEP_REASON_USER";
    private static final String FIELD_WAKE_UP_PARTIAL_DISPLAY = "WAKE_UP_PARTIAL_DISPLAY_UPDATE_BREATHING";
    public static int GO_TO_SLEEP_REASON_DEVICE_ADMIN = 0;
    public static int GO_TO_SLEEP_REASON_UPDATE_BREATHING = 0;
    public static int GO_TO_SLEEP_REASON_USER = 0;
    private static final String METHOD_PROXIMITY_TARGET_DETECTED = "proximityTargetDetected";
    private static final String TAG = "Aon_PowerManager";
    public static int WAKE_UP_PARTIAL_DISPLAY_UPDATE_BREATHING;
    public static boolean sInitialized;
    private static Method sMethodProximityTargetDetected;

    static {
        WAKE_UP_PARTIAL_DISPLAY_UPDATE_BREATHING = 0;
        GO_TO_SLEEP_REASON_UPDATE_BREATHING = 0;
        GO_TO_SLEEP_REASON_DEVICE_ADMIN = 0;
        GO_TO_SLEEP_REASON_USER = 0;
        sInitialized = false;
        try {
            Class<?> clazz = Class.forName(CLASS_POWER_MANAGER);
            WAKE_UP_PARTIAL_DISPLAY_UPDATE_BREATHING = clazz.getDeclaredField(FIELD_WAKE_UP_PARTIAL_DISPLAY).getInt(null);
            GO_TO_SLEEP_REASON_UPDATE_BREATHING = clazz.getDeclaredField(FIELD_GO_TO_SLEEP_REASON_UPDATE_BREATHING).getInt(null);
            GO_TO_SLEEP_REASON_DEVICE_ADMIN = clazz.getDeclaredField(FIELD_GO_TO_SLEEP_REASON_DEVICE_ADMIN).getInt(null);
            GO_TO_SLEEP_REASON_USER = clazz.getDeclaredField(FIELD_GO_TO_SLEEP_REASON_USER).getInt(null);
            sMethodProximityTargetDetected = clazz.getDeclaredMethod(METHOD_PROXIMITY_TARGET_DETECTED, new Class[0]);
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize class");
        }
    }

    public static boolean proximityTargetDetected(Object pm) {
        boolean z = false;
        if (!sInitialized || sMethodProximityTargetDetected == null) {
            return z;
        }
        try {
            return ((Boolean) sMethodProximityTargetDetected.invoke(pm, new Object[0])).booleanValue();
        } catch (Throwable th) {
            Log.w(TAG, "unable to invoke proximityTargetDetected");
            return z;
        }
    }
}
