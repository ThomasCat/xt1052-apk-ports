package com.motorola.aon;

import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.os.SystemClock;
import android.provider.Settings.Global;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.service.notification.StatusBarNotification;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.motorola.aon.aidl.IAon.Stub;
import com.motorola.aon.aidl.IAonNL;
import com.motorola.aon.aidl.INLCallback;
import com.motorola.aon.aidl.NotificationEntry;
import com.motorola.aon.aidl.ParsedInfo;
import com.motorola.aon.env.aidl.IAonInterface;
import com.motorola.aon.env.android.app.admin.DevicePolicyManager;
import com.motorola.aon.env.android.os.ServiceManager;
import com.motorola.aon.env.android.provider.Settings;
import com.motorola.aon.env.android.view.IWindowManager;
import com.motorola.aon.env.android.view.WindowManagerPolicy;
import com.motorola.aon.env.com.android.internal.R;
import com.motorola.aon.env.com.motorola.android.provider.MotorolaSettings;
import com.motorola.aon.env.com.motorola.wirelessdisplay.service.WirelessDisplayManager;
import com.motorola.aon.pd.PersistentManager;
import com.motorola.aon.settings.PackageEventService;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public abstract class AonService extends Service {
    public static final String ACTION_AON_AIDL = "com.motorola.aon.aidl.IAon";
    protected static final String ACTION_AON_CHECK_NIGHT_OFF = "com.motorola.aon.CHECK_NIGHT_OFF";
    protected static final String ACTION_AUDIOMONITOR_CLEAR_NOTIFICATION = "com.motorola.audiomonitor.clearnotifications";
    private static final String ACTION_CHECKIN_ALARM = "com.motorola.aon.ACTION_CHECKIN_ALARM";
    protected static final String ACTION_DELAYED_KEYGUARD = "com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD";
    private static final String ACTION_DREAM_STATUS = "com.motorola.motodock.deskdock.DREAM_STATUS";
    private static final String ACTION_LAUNCH_WHITE_LIST_TUTORIAL = "com.motorola.aon.LAUNCH_WHITE_LIST_TUTORIAL";
    protected static final String ACTION_MIDNIGHT_ALARM = "com.motorola.aon.MIDNIGHT_ALARM";
    protected static final String ACTION_NOTIFICATION_VIEW_FULLY_VISIBLE = "com.motorola.internal.policy.statusbar.NOTIFICATION_VIEW_FULLY_VISIBLE";
    public static final String ACTION_REFRESH_BREATHING = "com.motorola.aon.REFRESH_BREATHING";
    public static final String ACTION_SENSORHUB_PKG_ADDED = "com.motorola.aon.SENSORHUB_PKG_ADDED";
    public static final String ACTION_TUTORIAL_DONE = "com.motorola.aon.ACTION_TUTORIAL_DONE";
    public static final String ACTION_WHITE_LIST_CHANGE = "com.motorola.aon.settings.WHITE_LIST_CHANGED";
    private static final String ACTION_WHITE_LIST_TUTORIAL_CLEARED = "com.motorola.aon.WHITE_LIST_TUTORIAL_CLEARED";
    private static final String ACTION_WHITE_LIST_TUTORIAL_SELECTED = "com.motorola.aon.WHITE_LIST_TUTORIAL_SELECTED";
    public static final int BATTERY_CRITICAL = 2;
    public static final int BATTERY_LOW = 1;
    public static final int BATTERY_OK = 0;
    private static final long CHECKIN_INTERVAL = 14400000;
    private static final int CHECK_CANNED_NOTIFICATION = 1;
    public static final boolean DEBUG = Constants.DEBUG;
    private static final long DURATION_24_HOURS = 86400000;
    private static final long DURATION_7_DAYS = 604800000;
    protected static final String KEY_AUDIOMONITOR_CLEAR_TIME = "com.motorola.audiomonitor.starttime";
    protected static final String KEY_AUDIOMONITOR_NOTIFICATION_MAP = "com.motorola.audiomonitor.notificationmap";
    private static final String KEY_DEVICE_PROVISION_TIME = "device_provision_time";
    private static final String KEY_TUTORIAL_STATE = "aon_tutorial";
    protected static final int NOTIFICATION_ID_1_WEEK_WHITE_LIST = 1;
    private static final int ON_AOD_ACTIVATED = 4;
    private static final int ON_BOOTED = 2;
    private static final int ON_INCOMING_NOTIFICATION = 3;
    public static final int ON_UPDATE_BREATHING = 5;
    protected static final String PERMISSION_REGISTER_SMART_NOTIFICATION = "com.motorola.permission.REGISTER_SMART_NOTIFICATIONS";
    private static final String PKG_AON_INT = "com.motorola.aon.env";
    private static final int POLICY_PRIVACY_NONE = 0;
    private static final int POLICY_PRIVACY_STRONG = 1;
    private static final int SCREEN_STATE_FILTER_PRIORITY = 105;
    public static final String SENSORHUB_PKG_NAME = "com.motorola.slpc";
    private static final String TAG = "AonService";
    public static final int TUTORIAL_STATE_DONE = 3;
    public static final int TUTORIAL_STATE_NOT_PROVISIONED = 0;
    public static final int TUTORIAL_STATE_ON_GOING = 2;
    public static final int TUTORIAL_STATE_TRACK_TIME = 1;
    protected AlarmManager mAlarmManager;
    /* access modifiers changed from: protected */
    public IAonInterface mAonInterface;
    /* access modifiers changed from: protected */
    public AonNotificationManager mAonNotificationManager;
    /* access modifiers changed from: protected */
    public int mBatteryStatus = 0;
    private final Stub mBinder = new Stub() {
        public void getNotificationsByPriority(int maxEntries, List<ParsedInfo> notifications) throws RemoteException {
            if (AonService.DEBUG) {
                Log.d(AonService.TAG, "getNotificationsByPriority");
            }
            ArrayList<NotificationEntry> allBreathing = AonService.this.mAonNotificationManager.getNotificationListForBreathing();
            if (maxEntries > 0 && allBreathing != null) {
                int size = allBreathing.size();
                int i = 0;
                while (i < maxEntries && i < size) {
                    ParsedInfo pi = new ParsedInfo();
                    MinedData md = ((NotificationEntry) allBreathing.get(i)).minedData;
                    AonNotificationManager.parseNotification(md, true, pi);
                    if (pi.bigPicture == null && md.largeIcon != null) {
                        pi.bigPicture = md.largeIcon;
                    }
                    notifications.add(pi);
                    if (AonService.DEBUG) {
                        Log.d(AonService.TAG, "Add ParsedInfo: " + pi.toString());
                    }
                    i++;
                }
            }
        }

        public void getNotificationsByTime(int maxEntries, List<ParsedInfo> notifications) throws RemoteException {
            if (AonService.DEBUG) {
                Log.d(AonService.TAG, "getNotificationsByTime");
            }
            ArrayList<NotificationEntry> allBreathing = AonService.this.mAonNotificationManager.getNotificationListForBreathing();
            if (maxEntries > 0 && allBreathing != null) {
                int size = allBreathing.size();
                int i = 0;
                while (i < maxEntries && i < size) {
                    int index = 0;
                    long time = ((NotificationEntry) allBreathing.get(0)).minedData.timeStamp;
                    for (int m = 1; m < allBreathing.size(); m++) {
                        if (time < ((NotificationEntry) allBreathing.get(m)).minedData.timeStamp) {
                            index = m;
                            time = ((NotificationEntry) allBreathing.get(index)).minedData.timeStamp;
                        }
                    }
                    ParsedInfo pi = new ParsedInfo();
                    MinedData md = ((NotificationEntry) allBreathing.get(index)).minedData;
                    AonNotificationManager.parseNotification(md, true, pi);
                    if (pi.bigPicture == null && md.largeIcon != null) {
                        pi.bigPicture = md.largeIcon;
                    }
                    notifications.add(pi);
                    allBreathing.remove(index);
                    if (AonService.DEBUG) {
                        Log.d(AonService.TAG, "Add ParsedInfo: " + pi.toString());
                    }
                    i++;
                }
            }
        }
    };
    /* access modifiers changed from: protected */
    public int mBrightness;
    /* access modifiers changed from: private */
    public WakeLock mChargingWakeLock;
    ServiceConnection mConnNLService = new ServiceConnection() {
        public void onServiceConnected(ComponentName comp, IBinder binder) {
            if (AonService.DEBUG) {
                Log.d(AonService.TAG, "connected to NLS");
            }
            AonService.this.mIAonNL = IAonNL.Stub.asInterface(binder);
            try {
                AonService.this.mIAonNL.registerCallback(AonService.this.mINLCallback);
            } catch (RemoteException e) {
            }
        }

        public void onServiceDisconnected(ComponentName comp) {
            Log.e(AonService.TAG, "disconnected from NLS");
            AonService.this.mIAonNL = null;
        }
    };
    /* access modifiers changed from: private */
    public ContentResolver mContentResolver;
    /* access modifiers changed from: protected */
    public int mCoolDownMode;
    /* access modifiers changed from: private */
    public int mCriticalBatteryReminderLevels;
    DeathRecipient mDeathRecipient = new DeathRecipient() {
        public void binderDied() {
            AonService.this.requestRebind();
        }
    };
    private long mDeviceProvisionedTime = 0;
    /* access modifiers changed from: protected */
    public long mDisplayTimeOut;
    /* access modifiers changed from: protected */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            boolean z = true;
            switch (msg.what) {
                case 2:
                    PersistentManager.mInst.setPartialMode(AonService.this.getApplicationContext(), 0);
                    PersistentManager.mInst.setPauseRotationMode(AonService.this.getApplicationContext(), 0);
                    return;
                case 3:
                    AonService.this.handleOnIncomingNotification();
                    return;
                case 4:
                    AonService.this.handleOnAodActivated();
                    return;
                case 5:
                    AonService aonService = AonService.this;
                    if (msg.arg1 != 1) {
                        z = false;
                    }
                    aonService.handleUpdateBreathing(z);
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: protected */
    public boolean mHdmiPlugged = false;
    /* access modifiers changed from: protected */
    public IAonNL mIAonNL;
    protected IGetAonInterface mIGetAonInterface = new IGetAonInterface() {
        public IAonInterface get() {
            return AonService.this.mAonInterface;
        }
    };
    INLCallback mINLCallback = new INLCallback.Stub() {
        public void onNotificationPosted(StatusBarNotification sbn) throws RemoteException {
            if (AonService.this.mAonNotificationManager != null) {
                if (AonService.DEBUG) {
                    Log.d(AonService.TAG, "onNotificationPosted");
                }
                if (AonService.this.mAonNotificationManager.onUpdateNotification(sbn)) {
                    AonService.this.mNotificationReadByAOV = false;
                }
            }
        }

        public void onNotificationRemoved(StatusBarNotification sbn) throws RemoteException {
            if (AonService.this.mAonNotificationManager != null) {
                if (AonService.DEBUG) {
                    Log.d(AonService.TAG, "onNotificationRemoved");
                }
                AonService.this.mAonNotificationManager.onRemoveNotification(sbn);
            }
        }
    };
    private boolean mInitialized;
    private ServiceConnection mInterfaceServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName comp, IBinder binder) {
            Log.d(AonService.TAG, "connected to AonInt");
            AonService.this.mAonInterface = IAonInterface.Stub.asInterface(binder);
            try {
                binder.linkToDeath(AonService.this.mDeathRecipient, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            AonService.this.onAonInterfaceConntected();
        }

        public void onServiceDisconnected(ComponentName comp) {
            Log.d(AonService.TAG, "disconnect from AonInt");
            AonService.this.mAonInterface = null;
            AonService.this.onAonInterfaceDisconntected();
            AonService.this.requestRebind();
        }
    };
    protected boolean mIsWhiteListTutorialNotificationShowing = false;
    /* access modifiers changed from: protected */
    public KeyguardManager mKeyguardManager;
    /* access modifiers changed from: protected */
    public long mLockScreenLockAfterTimeOut;
    /* access modifiers changed from: private */
    public int mLowBatteryReminderLevels;
    /* access modifiers changed from: protected */
    public boolean mNotificationReadByAOV = false;
    BroadcastReceiver mNotificationReadByAOVReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if (AonService.DEBUG) {
                    Log.d(AonService.TAG, "mNotificationReadByAOVReceiver receive intent " + (action != null ? action : "null"));
                }
                if (AonService.ACTION_AUDIOMONITOR_CLEAR_NOTIFICATION.equals(action)) {
                    AonService.this.mNotificationReadByAOV = true;
                    AonService.this.onNotificationReadByAOV(intent.getLongExtra(AonService.KEY_AUDIOMONITOR_CLEAR_TIME, -1), (HashMap) intent.getSerializableExtra(AonService.KEY_AUDIOMONITOR_NOTIFICATION_MAP));
                }
            }
        }
    };
    private Runnable mOutOfBoxTutorialRunnable = new Runnable() {
        public void run() {
            if (AonService.DEBUG) {
                Log.d(AonService.TAG, "mOutOfBoxTutorialRunnable.run()");
            }
            AonService.this.updateTutorialState(2);
        }
    };
    boolean mPendingDismissKeyguard;
    /* access modifiers changed from: protected */
    public PowerManager mPowerManager;
    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if (AonService.DEBUG) {
                    Log.d(AonService.TAG, "receive intent " + (action != null ? action : "null"));
                }
                if (!AonService.this.onReceiveBroadcast(context, intent)) {
                    if (AonService.ACTION_WHITE_LIST_CHANGE.equals(action)) {
                        if (AonService.this.mAonNotificationManager != null) {
                            AonService.this.mAonNotificationManager.refreshWhiteList();
                        }
                    } else if (WindowManagerPolicy.ACTION_HDMI_PLUGGED.equals(action)) {
                        AonService.this.mHdmiPlugged = intent.getBooleanExtra(WindowManagerPolicy.EXTRA_HDMI_PLUGGED_STATE, false);
                        if (AonService.DEBUG) {
                            Log.d(AonService.TAG, "hdmi plugged " + (AonService.this.mHdmiPlugged ? "Yes" : "No"));
                        }
                        AonService.this.onHdmiPlugged();
                    } else if (WirelessDisplayManager.WIRELESS_DISPLAY_MIRRORING_STATE_CHANGED_ACTION.equals(action)) {
                        int mirror = intent.getIntExtra(WirelessDisplayManager.EXTRA_WIRELESS_DISPLAY_MIRRORING_STATE, WirelessDisplayManager.WIRELESS_DISPLAY_MIRRORING_OFF);
                        if (AonService.DEBUG) {
                            Log.d(AonService.TAG, "wifi mirror state " + mirror);
                        }
                        AonService.this.mHdmiPlugged = mirror == WirelessDisplayManager.WIRELESS_DISPLAY_MIRRORING_ON;
                        AonService.this.onWirelessDisplayMirroring();
                    } else if (AonService.ACTION_DREAM_STATUS.equals(action)) {
                        boolean started = intent.getBooleanExtra("started", false);
                        if (AonService.DEBUG) {
                            Log.d(AonService.TAG, "get the dream start intent with started = " + started);
                        }
                        if (started) {
                            AonService.this.mAonNotificationManager.setDreamStarted(true);
                            if (AonService.this.mAonNotificationManager.getTracking()) {
                                AonService.this.mAonNotificationManager.notificationUpdated();
                            }
                            AonService.this.mAonNotificationManager.enableTracking(true);
                        } else {
                            AonService.this.mAonNotificationManager.setDreamStarted(false);
                            if (intent.getBooleanExtra("user-intent", false)) {
                                AonService.this.mAonNotificationManager.enableTracking(false);
                            }
                        }
                    } else if ("android.intent.action.BATTERY_CHANGED".equals(action)) {
                        int batteryLevel = intent.getIntExtra("level", 0);
                        int batteryStatus = intent.getIntExtra("status", 1);
                        if (AonService.DEBUG) {
                            Log.d(AonService.TAG, "level " + batteryLevel + ", batteryStatus " + batteryStatus + ", scale " + intent.getIntExtra("scale", 0) + ", plug " + intent.getIntExtra("plugged", 0));
                        }
                        int newBatteryStatus = 0;
                        if (batteryStatus != 2) {
                            if (batteryLevel <= AonService.this.mCriticalBatteryReminderLevels) {
                                newBatteryStatus = 2;
                            } else if (batteryLevel <= AonService.this.mLowBatteryReminderLevels) {
                                newBatteryStatus = 1;
                            }
                        }
                        if (AonService.DEBUG) {
                            Log.d(AonService.TAG, "old status " + AonService.this.mBatteryStatus + ", new status " + newBatteryStatus);
                        }
                        if (newBatteryStatus != AonService.this.mBatteryStatus) {
                            AonService.this.mBatteryStatus = newBatteryStatus;
                            AonService.this.onBatteryChange();
                        }
                        if (batteryStatus == 2) {
                            if (!AonService.this.mChargingWakeLock.isHeld()) {
                                if (AonService.DEBUG) {
                                    Log.d(AonService.TAG, "acquiring charging lock");
                                }
                                AonService.this.mChargingWakeLock.acquire();
                            }
                        } else if (AonService.this.mChargingWakeLock.isHeld()) {
                            if (AonService.DEBUG) {
                                Log.d(AonService.TAG, "releasing charging lock");
                            }
                            AonService.this.mChargingWakeLock.release();
                        }
                    } else if (DevicePolicyManager.ACTION_DEVICE_POLICY_MANAGER_STATE_CHANGED.equals(action)) {
                        AonService.this.refreshPrivacy();
                    }
                }
                if ("android.intent.action.TIMEZONE_CHANGED".equals(action) || "android.intent.action.TIME_SET".equals(action) || "android.intent.action.DATE_CHANGED".equals(action)) {
                    AonService.this.scheduleNextMidNightAlarm();
                } else if (AonService.ACTION_MIDNIGHT_ALARM.equals(action)) {
                    AonService.this.scheduleNextMidNightAlarm();
                    AonService.this.onMidNightAlarm();
                } else if ("android.intent.action.SCREEN_ON".equals(action)) {
                    AonService.this.handleScreenOn();
                } else if ("android.intent.action.SCREEN_OFF".equals(action)) {
                    AonService.this.handleScreenOff();
                } else if (AonService.ACTION_WHITE_LIST_TUTORIAL_SELECTED.equals(action)) {
                    AonService.this.handleWhiteListNotificationSelected();
                } else if (AonService.ACTION_WHITE_LIST_TUTORIAL_CLEARED.equals(action)) {
                    AonService.this.handleWhiteListNotificationCleared();
                }
            }
        }
    };
    boolean mScreenOn;
    private ContentObserver mSettingsObserver = new ContentObserver(new Handler()) {
        /* JADX WARNING: Code restructure failed: missing block: B:102:0x02cf, code lost:
            if (r19.equals(com.motorola.aon.env.com.motorola.android.provider.MotorolaSettings.getUriFor(com.motorola.aon.env.com.motorola.android.provider.MotorolaSettings.SN_PRIVACY)) != false) goto L_0x02d1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x003f, code lost:
            if (r19.equals(android.provider.Settings.System.getUriFor("screen_brightness_mode")) != false) goto L_0x0041;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:123:0x0394, code lost:
            if (r19.equals(com.motorola.aon.env.com.motorola.android.provider.MotorolaSettings.getUriFor(com.motorola.aon.env.com.motorola.android.provider.MotorolaSettings.SN_NIGHT_OFF)) != false) goto L_0x0396;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:140:0x0401, code lost:
            if (r19.equals(com.motorola.aon.env.com.motorola.android.provider.MotorolaSettings.getUriFor(com.motorola.aon.env.com.motorola.android.provider.MotorolaSettings.SN_NIGHT_BEGIN)) != false) goto L_0x0403;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:154:0x0466, code lost:
            if (r19.equals(com.motorola.aon.env.com.motorola.android.provider.MotorolaSettings.getUriFor(com.motorola.aon.env.com.motorola.android.provider.MotorolaSettings.SN_NIGHT_END)) != false) goto L_0x0468;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x008f, code lost:
            if (r19.equals(android.provider.Settings.Secure.getUriFor(com.motorola.aon.env.android.provider.Settings.Secure.LOCK_SCREEN_LOCK_AFTER_TIMEOUT)) != false) goto L_0x0091;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x00dc, code lost:
            if (r19.equals(android.provider.Settings.System.getUriFor("screen_off_timeout")) != false) goto L_0x00de;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x012a, code lost:
            if (r19.equals(android.provider.Settings.Global.getUriFor("device_provisioned")) != false) goto L_0x012c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x018b, code lost:
            if (r19.equals(android.provider.Settings.System.getUriFor("haptic_feedback_enabled")) != false) goto L_0x018d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:70:0x01e2, code lost:
            if (r19.equals(android.provider.Settings.System.getUriFor("cooldown_mode_on")) != false) goto L_0x01e4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:80:0x0232, code lost:
            if (r19.equals(com.motorola.aon.env.com.motorola.android.provider.MotorolaSettings.getUriFor(com.motorola.aon.env.com.motorola.android.provider.MotorolaSettings.SN_AON)) != false) goto L_0x0234;
         */
        /* JADX WARNING: Removed duplicated region for block: B:100:0x02c1  */
        /* JADX WARNING: Removed duplicated region for block: B:105:0x02ee  */
        /* JADX WARNING: Removed duplicated region for block: B:108:0x02f7  */
        /* JADX WARNING: Removed duplicated region for block: B:119:0x0366  */
        /* JADX WARNING: Removed duplicated region for block: B:121:0x0386  */
        /* JADX WARNING: Removed duplicated region for block: B:126:0x03b3  */
        /* JADX WARNING: Removed duplicated region for block: B:132:0x03ca  */
        /* JADX WARNING: Removed duplicated region for block: B:136:0x03d3  */
        /* JADX WARNING: Removed duplicated region for block: B:138:0x03f3  */
        /* JADX WARNING: Removed duplicated region for block: B:143:0x0420  */
        /* JADX WARNING: Removed duplicated region for block: B:150:0x0438  */
        /* JADX WARNING: Removed duplicated region for block: B:152:0x0458  */
        /* JADX WARNING: Removed duplicated region for block: B:157:0x0485  */
        /* JADX WARNING: Removed duplicated region for block: B:164:0x049d  */
        /* JADX WARNING: Removed duplicated region for block: B:166:0x04bd  */
        /* JADX WARNING: Removed duplicated region for block: B:174:0x04f3  */
        /* JADX WARNING: Removed duplicated region for block: B:175:0x04f6  */
        /* JADX WARNING: Removed duplicated region for block: B:176:0x04f9  */
        /* JADX WARNING: Removed duplicated region for block: B:177:0x04fc  */
        /* JADX WARNING: Removed duplicated region for block: B:180:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00ce  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x00f7  */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x00fc  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x011c  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x0142  */
        /* JADX WARNING: Removed duplicated region for block: B:50:0x015e  */
        /* JADX WARNING: Removed duplicated region for block: B:53:0x0163  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x017d  */
        /* JADX WARNING: Removed duplicated region for block: B:60:0x01a3  */
        /* JADX WARNING: Removed duplicated region for block: B:63:0x01aa  */
        /* JADX WARNING: Removed duplicated region for block: B:65:0x01ca  */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x01d4  */
        /* JADX WARNING: Removed duplicated region for block: B:73:0x01ff  */
        /* JADX WARNING: Removed duplicated region for block: B:76:0x0204  */
        /* JADX WARNING: Removed duplicated region for block: B:78:0x0224  */
        /* JADX WARNING: Removed duplicated region for block: B:83:0x025d  */
        /* JADX WARNING: Removed duplicated region for block: B:86:0x0263  */
        /* JADX WARNING: Removed duplicated region for block: B:94:0x0282  */
        /* JADX WARNING: Removed duplicated region for block: B:98:0x02a1  */
        public void onChange(boolean selfChange, Uri uri) {
            int oldPrivacy;
            int provisioned;
            if (AonService.DEBUG) {
                Log.d(AonService.TAG, "settings changed , " + (uri != null ? uri.toString() : "null uri"));
            }
            boolean notNullChecked = false;
            if (uri != null) {
                if (!uri.equals(System.getUriFor("screen_brightness"))) {
                }
            }
            if (System.getInt(AonService.this.mContentResolver, "screen_brightness_mode", 0) == 1) {
                AonService.this.mBrightness = 0;
            } else {
                AonService.this.mBrightness = System.getInt(AonService.this.mContentResolver, "screen_brightness", 100);
            }
            if (uri != null) {
                notNullChecked = true;
            }
            if (AonService.DEBUG) {
                Log.d(AonService.TAG, "brightness " + AonService.this.mBrightness);
            }
            if (uri != null) {
                if (!notNullChecked) {
                }
                if (uri != null) {
                    if (!notNullChecked) {
                    }
                    if (uri != null) {
                        if (!notNullChecked) {
                        }
                        if (uri != null) {
                            if (!notNullChecked) {
                            }
                            if (uri != null) {
                                if (!notNullChecked) {
                                }
                                if (uri != null) {
                                    if (!notNullChecked) {
                                    }
                                    if (uri != null) {
                                        if (!notNullChecked) {
                                        }
                                        if (uri != null) {
                                            if (!notNullChecked) {
                                            }
                                            if (uri != null) {
                                                if (!notNullChecked) {
                                                }
                                                if (uri != null) {
                                                    if (!notNullChecked) {
                                                    }
                                                    if (uri == null) {
                                                        AonService.this.scheduleNextCheckNightOffAlarm((AonService.this.mSnAon == 0 || AonService.this.mSnNightOff == 0) ? false : true, false);
                                                        return;
                                                    }
                                                    return;
                                                }
                                                int oldSnNightEnd = AonService.this.mSnNightEnd;
                                                AonService.this.mSnNightEnd = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_END, 0);
                                                if (uri != null) {
                                                    notNullChecked = true;
                                                }
                                                if (notNullChecked && oldSnNightEnd != AonService.this.mSnNightEnd) {
                                                    AonService.this.scheduleNextCheckNightOffAlarm(true, false);
                                                }
                                                if (AonService.DEBUG) {
                                                    Log.d(AonService.TAG, "SN_NIGHT_END " + AonService.this.mSnNightEnd);
                                                }
                                                if (uri == null) {
                                                }
                                            }
                                            int oldNightBegin = AonService.this.mSnNightBegin;
                                            AonService.this.mSnNightBegin = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_BEGIN, 0);
                                            if (uri != null) {
                                                notNullChecked = true;
                                            }
                                            if (notNullChecked && oldNightBegin != AonService.this.mSnNightBegin) {
                                                AonService.this.scheduleNextCheckNightOffAlarm(true, false);
                                            }
                                            if (AonService.DEBUG) {
                                                Log.d(AonService.TAG, "SN_NIGHT_START " + AonService.this.mSnNightBegin);
                                            }
                                            if (uri != null) {
                                            }
                                            int oldSnNightEnd2 = AonService.this.mSnNightEnd;
                                            AonService.this.mSnNightEnd = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_END, 0);
                                            if (uri != null) {
                                            }
                                            AonService.this.scheduleNextCheckNightOffAlarm(true, false);
                                            if (AonService.DEBUG) {
                                            }
                                            if (uri == null) {
                                            }
                                        }
                                        int oldSnNightOff = AonService.this.mSnNightOff;
                                        AonService.this.mSnNightOff = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_OFF, 1);
                                        if (uri != null) {
                                            notNullChecked = true;
                                        }
                                        if (notNullChecked && oldSnNightOff != AonService.this.mSnNightOff) {
                                            AonService.this.scheduleNextCheckNightOffAlarm(AonService.this.mSnNightOff == 0, false);
                                        }
                                        if (AonService.DEBUG) {
                                            Log.d(AonService.TAG, "SN_NIGHT_OFF " + AonService.this.mSnNightOff);
                                        }
                                        if (uri != null) {
                                        }
                                        int oldNightBegin2 = AonService.this.mSnNightBegin;
                                        AonService.this.mSnNightBegin = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_BEGIN, 0);
                                        if (uri != null) {
                                        }
                                        AonService.this.scheduleNextCheckNightOffAlarm(true, false);
                                        if (AonService.DEBUG) {
                                        }
                                        if (uri != null) {
                                        }
                                        int oldSnNightEnd22 = AonService.this.mSnNightEnd;
                                        AonService.this.mSnNightEnd = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_END, 0);
                                        if (uri != null) {
                                        }
                                        AonService.this.scheduleNextCheckNightOffAlarm(true, false);
                                        if (AonService.DEBUG) {
                                        }
                                        if (uri == null) {
                                        }
                                    }
                                    oldPrivacy = AonService.this.mSnPrivacy;
                                    AonService.this.mSnPrivacy = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_PRIVACY, 0);
                                    if (uri != null) {
                                        notNullChecked = true;
                                    }
                                    if (oldPrivacy != AonService.this.mSnPrivacy) {
                                        if (AonService.DEBUG) {
                                            Log.d(AonService.TAG, "privacy changed by UI");
                                        }
                                        int oldPolicy = AonService.this.getPolicyDataFromPrivacySetting(oldPrivacy);
                                        int newPolicy = AonService.this.getPolicyDataFromPrivacySetting(AonService.this.mSnPrivacy);
                                        if (oldPolicy == 1 && newPolicy != 1) {
                                            Log.d(AonService.TAG, "UI is trying to remove strong privacy");
                                            AonService.this.mSnPrivacy = AonService.this.makePrivacySetting(1, AonService.this.getUserChoiceFromPrivacySetting(AonService.this.mSnPrivacy));
                                            MotorolaSettings.putInt(AonService.this.mContentResolver, MotorolaSettings.SN_PRIVACY, AonService.this.mSnPrivacy);
                                        }
                                        AonService.this.onPrivacyModeChange();
                                    }
                                    if (AonService.DEBUG) {
                                        Log.d(AonService.TAG, "SN_PRIVACY " + AonService.this.mSnPrivacy);
                                    }
                                    if (uri != null) {
                                    }
                                    int oldSnNightOff2 = AonService.this.mSnNightOff;
                                    AonService.this.mSnNightOff = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_OFF, 1);
                                    if (uri != null) {
                                    }
                                    AonService.this.scheduleNextCheckNightOffAlarm(AonService.this.mSnNightOff == 0, false);
                                    if (AonService.DEBUG) {
                                    }
                                    if (uri != null) {
                                    }
                                    int oldNightBegin22 = AonService.this.mSnNightBegin;
                                    AonService.this.mSnNightBegin = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_BEGIN, 0);
                                    if (uri != null) {
                                    }
                                    AonService.this.scheduleNextCheckNightOffAlarm(true, false);
                                    if (AonService.DEBUG) {
                                    }
                                    if (uri != null) {
                                    }
                                    int oldSnNightEnd222 = AonService.this.mSnNightEnd;
                                    AonService.this.mSnNightEnd = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_END, 0);
                                    if (uri != null) {
                                    }
                                    AonService.this.scheduleNextCheckNightOffAlarm(true, false);
                                    if (AonService.DEBUG) {
                                    }
                                    if (uri == null) {
                                    }
                                }
                                int oldSnAon = AonService.this.mSnAon;
                                AonService.this.mSnAon = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_AON, 0);
                                AonService.this.mAonNotificationManager.setAonSettingOn(AonService.this.mSnAon == 0);
                                if (uri != null) {
                                    notNullChecked = true;
                                }
                                if (!(!notNullChecked || oldSnAon == AonService.this.mSnAon || AonService.this.mSnNightOff == 0)) {
                                    AonService.this.scheduleNextCheckNightOffAlarm(AonService.this.mSnAon == 0, false);
                                }
                                AonService.this.scheduleNextMidNightAlarm();
                                AonService.this.onAonSettingChange();
                                AonService.this.scheduleCheckinAlarm(true);
                                if (AonService.DEBUG) {
                                    Log.d(AonService.TAG, "SN_AON " + AonService.this.mSnAon);
                                }
                                if (uri != null) {
                                }
                                oldPrivacy = AonService.this.mSnPrivacy;
                                AonService.this.mSnPrivacy = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_PRIVACY, 0);
                                if (uri != null) {
                                }
                                if (oldPrivacy != AonService.this.mSnPrivacy) {
                                }
                                if (AonService.DEBUG) {
                                }
                                if (uri != null) {
                                }
                                int oldSnNightOff22 = AonService.this.mSnNightOff;
                                AonService.this.mSnNightOff = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_OFF, 1);
                                if (uri != null) {
                                }
                                AonService.this.scheduleNextCheckNightOffAlarm(AonService.this.mSnNightOff == 0, false);
                                if (AonService.DEBUG) {
                                }
                                if (uri != null) {
                                }
                                int oldNightBegin222 = AonService.this.mSnNightBegin;
                                AonService.this.mSnNightBegin = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_BEGIN, 0);
                                if (uri != null) {
                                }
                                AonService.this.scheduleNextCheckNightOffAlarm(true, false);
                                if (AonService.DEBUG) {
                                }
                                if (uri != null) {
                                }
                                int oldSnNightEnd2222 = AonService.this.mSnNightEnd;
                                AonService.this.mSnNightEnd = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_END, 0);
                                if (uri != null) {
                                }
                                AonService.this.scheduleNextCheckNightOffAlarm(true, false);
                                if (AonService.DEBUG) {
                                }
                                if (uri == null) {
                                }
                            }
                            AonService.this.mCoolDownMode = System.getInt(AonService.this.getApplicationContext().getContentResolver(), "cooldown_mode_on", 0);
                            if (uri != null) {
                                notNullChecked = true;
                            }
                            if (AonService.DEBUG) {
                                Log.d(AonService.TAG, "cooldown: " + AonService.this.mCoolDownMode);
                            }
                            if (uri != null) {
                            }
                            int oldSnAon2 = AonService.this.mSnAon;
                            AonService.this.mSnAon = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_AON, 0);
                            AonService.this.mAonNotificationManager.setAonSettingOn(AonService.this.mSnAon == 0);
                            if (uri != null) {
                            }
                            AonService.this.scheduleNextCheckNightOffAlarm(AonService.this.mSnAon == 0, false);
                            AonService.this.scheduleNextMidNightAlarm();
                            AonService.this.onAonSettingChange();
                            AonService.this.scheduleCheckinAlarm(true);
                            if (AonService.DEBUG) {
                            }
                            if (uri != null) {
                            }
                            oldPrivacy = AonService.this.mSnPrivacy;
                            AonService.this.mSnPrivacy = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_PRIVACY, 0);
                            if (uri != null) {
                            }
                            if (oldPrivacy != AonService.this.mSnPrivacy) {
                            }
                            if (AonService.DEBUG) {
                            }
                            if (uri != null) {
                            }
                            int oldSnNightOff222 = AonService.this.mSnNightOff;
                            AonService.this.mSnNightOff = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_OFF, 1);
                            if (uri != null) {
                            }
                            AonService.this.scheduleNextCheckNightOffAlarm(AonService.this.mSnNightOff == 0, false);
                            if (AonService.DEBUG) {
                            }
                            if (uri != null) {
                            }
                            int oldNightBegin2222 = AonService.this.mSnNightBegin;
                            AonService.this.mSnNightBegin = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_BEGIN, 0);
                            if (uri != null) {
                            }
                            AonService.this.scheduleNextCheckNightOffAlarm(true, false);
                            if (AonService.DEBUG) {
                            }
                            if (uri != null) {
                            }
                            int oldSnNightEnd22222 = AonService.this.mSnNightEnd;
                            AonService.this.mSnNightEnd = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_END, 0);
                            if (uri != null) {
                            }
                            AonService.this.scheduleNextCheckNightOffAlarm(true, false);
                            if (AonService.DEBUG) {
                            }
                            if (uri == null) {
                            }
                        }
                        int hapticVal = System.getInt(AonService.this.getContentResolver(), "haptic_feedback_enabled", 1);
                        AonService.this.mbHapticsEnabled = hapticVal != 1;
                        if (AonService.DEBUG) {
                            Log.d(AonService.TAG, "New System haptics value: " + AonService.this.mbHapticsEnabled);
                        }
                        if (uri != null) {
                            notNullChecked = true;
                        }
                        AonService.this.onHapticsSettingsChanged();
                        if (uri != null) {
                        }
                        AonService.this.mCoolDownMode = System.getInt(AonService.this.getApplicationContext().getContentResolver(), "cooldown_mode_on", 0);
                        if (uri != null) {
                        }
                        if (AonService.DEBUG) {
                        }
                        if (uri != null) {
                        }
                        int oldSnAon22 = AonService.this.mSnAon;
                        AonService.this.mSnAon = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_AON, 0);
                        AonService.this.mAonNotificationManager.setAonSettingOn(AonService.this.mSnAon == 0);
                        if (uri != null) {
                        }
                        AonService.this.scheduleNextCheckNightOffAlarm(AonService.this.mSnAon == 0, false);
                        AonService.this.scheduleNextMidNightAlarm();
                        AonService.this.onAonSettingChange();
                        AonService.this.scheduleCheckinAlarm(true);
                        if (AonService.DEBUG) {
                        }
                        if (uri != null) {
                        }
                        oldPrivacy = AonService.this.mSnPrivacy;
                        AonService.this.mSnPrivacy = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_PRIVACY, 0);
                        if (uri != null) {
                        }
                        if (oldPrivacy != AonService.this.mSnPrivacy) {
                        }
                        if (AonService.DEBUG) {
                        }
                        if (uri != null) {
                        }
                        int oldSnNightOff2222 = AonService.this.mSnNightOff;
                        AonService.this.mSnNightOff = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_OFF, 1);
                        if (uri != null) {
                        }
                        AonService.this.scheduleNextCheckNightOffAlarm(AonService.this.mSnNightOff == 0, false);
                        if (AonService.DEBUG) {
                        }
                        if (uri != null) {
                        }
                        int oldNightBegin22222 = AonService.this.mSnNightBegin;
                        AonService.this.mSnNightBegin = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_BEGIN, 0);
                        if (uri != null) {
                        }
                        AonService.this.scheduleNextCheckNightOffAlarm(true, false);
                        if (AonService.DEBUG) {
                        }
                        if (uri != null) {
                        }
                        int oldSnNightEnd222222 = AonService.this.mSnNightEnd;
                        AonService.this.mSnNightEnd = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_END, 0);
                        if (uri != null) {
                        }
                        AonService.this.scheduleNextCheckNightOffAlarm(true, false);
                        if (AonService.DEBUG) {
                        }
                        if (uri == null) {
                        }
                    }
                    provisioned = Global.getInt(AonService.this.getApplicationContext().getContentResolver(), "device_provisioned", 0);
                    if (provisioned == 1) {
                        AonService.this.mHandler.sendEmptyMessage(1);
                        if (AonService.this.mTutorialState == 0) {
                            AonService.this.updateTutorialState(1);
                        }
                    }
                    if (uri != null) {
                        notNullChecked = true;
                    }
                    if (AonService.DEBUG) {
                        Log.d(AonService.TAG, "DEVICE_PROVISIONED " + provisioned);
                    }
                    if (uri != null) {
                    }
                    int hapticVal2 = System.getInt(AonService.this.getContentResolver(), "haptic_feedback_enabled", 1);
                    AonService.this.mbHapticsEnabled = hapticVal2 != 1;
                    if (AonService.DEBUG) {
                    }
                    if (uri != null) {
                    }
                    AonService.this.onHapticsSettingsChanged();
                    if (uri != null) {
                    }
                    AonService.this.mCoolDownMode = System.getInt(AonService.this.getApplicationContext().getContentResolver(), "cooldown_mode_on", 0);
                    if (uri != null) {
                    }
                    if (AonService.DEBUG) {
                    }
                    if (uri != null) {
                    }
                    int oldSnAon222 = AonService.this.mSnAon;
                    AonService.this.mSnAon = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_AON, 0);
                    AonService.this.mAonNotificationManager.setAonSettingOn(AonService.this.mSnAon == 0);
                    if (uri != null) {
                    }
                    AonService.this.scheduleNextCheckNightOffAlarm(AonService.this.mSnAon == 0, false);
                    AonService.this.scheduleNextMidNightAlarm();
                    AonService.this.onAonSettingChange();
                    AonService.this.scheduleCheckinAlarm(true);
                    if (AonService.DEBUG) {
                    }
                    if (uri != null) {
                    }
                    oldPrivacy = AonService.this.mSnPrivacy;
                    AonService.this.mSnPrivacy = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_PRIVACY, 0);
                    if (uri != null) {
                    }
                    if (oldPrivacy != AonService.this.mSnPrivacy) {
                    }
                    if (AonService.DEBUG) {
                    }
                    if (uri != null) {
                    }
                    int oldSnNightOff22222 = AonService.this.mSnNightOff;
                    AonService.this.mSnNightOff = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_OFF, 1);
                    if (uri != null) {
                    }
                    AonService.this.scheduleNextCheckNightOffAlarm(AonService.this.mSnNightOff == 0, false);
                    if (AonService.DEBUG) {
                    }
                    if (uri != null) {
                    }
                    int oldNightBegin222222 = AonService.this.mSnNightBegin;
                    AonService.this.mSnNightBegin = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_BEGIN, 0);
                    if (uri != null) {
                    }
                    AonService.this.scheduleNextCheckNightOffAlarm(true, false);
                    if (AonService.DEBUG) {
                    }
                    if (uri != null) {
                    }
                    int oldSnNightEnd2222222 = AonService.this.mSnNightEnd;
                    AonService.this.mSnNightEnd = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_END, 0);
                    if (uri != null) {
                    }
                    AonService.this.scheduleNextCheckNightOffAlarm(true, false);
                    if (AonService.DEBUG) {
                    }
                    if (uri == null) {
                    }
                }
                AonService.this.mDisplayTimeOut = (long) System.getInt(AonService.this.mContentResolver, "screen_off_timeout", 30000);
                if (uri != null) {
                    notNullChecked = true;
                }
                if (AonService.DEBUG) {
                    Log.d(AonService.TAG, "display timeout " + AonService.this.mDisplayTimeOut);
                }
                if (uri != null) {
                }
                provisioned = Global.getInt(AonService.this.getApplicationContext().getContentResolver(), "device_provisioned", 0);
                if (provisioned == 1) {
                }
                if (uri != null) {
                }
                if (AonService.DEBUG) {
                }
                if (uri != null) {
                }
                int hapticVal22 = System.getInt(AonService.this.getContentResolver(), "haptic_feedback_enabled", 1);
                AonService.this.mbHapticsEnabled = hapticVal22 != 1;
                if (AonService.DEBUG) {
                }
                if (uri != null) {
                }
                AonService.this.onHapticsSettingsChanged();
                if (uri != null) {
                }
                AonService.this.mCoolDownMode = System.getInt(AonService.this.getApplicationContext().getContentResolver(), "cooldown_mode_on", 0);
                if (uri != null) {
                }
                if (AonService.DEBUG) {
                }
                if (uri != null) {
                }
                int oldSnAon2222 = AonService.this.mSnAon;
                AonService.this.mSnAon = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_AON, 0);
                AonService.this.mAonNotificationManager.setAonSettingOn(AonService.this.mSnAon == 0);
                if (uri != null) {
                }
                AonService.this.scheduleNextCheckNightOffAlarm(AonService.this.mSnAon == 0, false);
                AonService.this.scheduleNextMidNightAlarm();
                AonService.this.onAonSettingChange();
                AonService.this.scheduleCheckinAlarm(true);
                if (AonService.DEBUG) {
                }
                if (uri != null) {
                }
                oldPrivacy = AonService.this.mSnPrivacy;
                AonService.this.mSnPrivacy = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_PRIVACY, 0);
                if (uri != null) {
                }
                if (oldPrivacy != AonService.this.mSnPrivacy) {
                }
                if (AonService.DEBUG) {
                }
                if (uri != null) {
                }
                int oldSnNightOff222222 = AonService.this.mSnNightOff;
                AonService.this.mSnNightOff = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_OFF, 1);
                if (uri != null) {
                }
                AonService.this.scheduleNextCheckNightOffAlarm(AonService.this.mSnNightOff == 0, false);
                if (AonService.DEBUG) {
                }
                if (uri != null) {
                }
                int oldNightBegin2222222 = AonService.this.mSnNightBegin;
                AonService.this.mSnNightBegin = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_BEGIN, 0);
                if (uri != null) {
                }
                AonService.this.scheduleNextCheckNightOffAlarm(true, false);
                if (AonService.DEBUG) {
                }
                if (uri != null) {
                }
                int oldSnNightEnd22222222 = AonService.this.mSnNightEnd;
                AonService.this.mSnNightEnd = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_END, 0);
                if (uri != null) {
                }
                AonService.this.scheduleNextCheckNightOffAlarm(true, false);
                if (AonService.DEBUG) {
                }
                if (uri == null) {
                }
            }
            AonService.this.mLockScreenLockAfterTimeOut = Secure.getLong(AonService.this.getContentResolver(), Settings.Secure.LOCK_SCREEN_LOCK_AFTER_TIMEOUT, 5000);
            if (uri != null) {
                notNullChecked = true;
            }
            if (AonService.DEBUG) {
                Log.d(AonService.TAG, "lock screen lock after time out " + AonService.this.mLockScreenLockAfterTimeOut);
            }
            if (uri != null) {
            }
            AonService.this.mDisplayTimeOut = (long) System.getInt(AonService.this.mContentResolver, "screen_off_timeout", 30000);
            if (uri != null) {
            }
            if (AonService.DEBUG) {
            }
            if (uri != null) {
            }
            provisioned = Global.getInt(AonService.this.getApplicationContext().getContentResolver(), "device_provisioned", 0);
            if (provisioned == 1) {
            }
            if (uri != null) {
            }
            if (AonService.DEBUG) {
            }
            if (uri != null) {
            }
            int hapticVal222 = System.getInt(AonService.this.getContentResolver(), "haptic_feedback_enabled", 1);
            AonService.this.mbHapticsEnabled = hapticVal222 != 1;
            if (AonService.DEBUG) {
            }
            if (uri != null) {
            }
            AonService.this.onHapticsSettingsChanged();
            if (uri != null) {
            }
            AonService.this.mCoolDownMode = System.getInt(AonService.this.getApplicationContext().getContentResolver(), "cooldown_mode_on", 0);
            if (uri != null) {
            }
            if (AonService.DEBUG) {
            }
            if (uri != null) {
            }
            int oldSnAon22222 = AonService.this.mSnAon;
            AonService.this.mSnAon = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_AON, 0);
            AonService.this.mAonNotificationManager.setAonSettingOn(AonService.this.mSnAon == 0);
            if (uri != null) {
            }
            AonService.this.scheduleNextCheckNightOffAlarm(AonService.this.mSnAon == 0, false);
            AonService.this.scheduleNextMidNightAlarm();
            AonService.this.onAonSettingChange();
            AonService.this.scheduleCheckinAlarm(true);
            if (AonService.DEBUG) {
            }
            if (uri != null) {
            }
            oldPrivacy = AonService.this.mSnPrivacy;
            AonService.this.mSnPrivacy = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_PRIVACY, 0);
            if (uri != null) {
            }
            if (oldPrivacy != AonService.this.mSnPrivacy) {
            }
            if (AonService.DEBUG) {
            }
            if (uri != null) {
            }
            int oldSnNightOff2222222 = AonService.this.mSnNightOff;
            AonService.this.mSnNightOff = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_OFF, 1);
            if (uri != null) {
            }
            AonService.this.scheduleNextCheckNightOffAlarm(AonService.this.mSnNightOff == 0, false);
            if (AonService.DEBUG) {
            }
            if (uri != null) {
            }
            int oldNightBegin22222222 = AonService.this.mSnNightBegin;
            AonService.this.mSnNightBegin = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_BEGIN, 0);
            if (uri != null) {
            }
            AonService.this.scheduleNextCheckNightOffAlarm(true, false);
            if (AonService.DEBUG) {
            }
            if (uri != null) {
            }
            int oldSnNightEnd222222222 = AonService.this.mSnNightEnd;
            AonService.this.mSnNightEnd = MotorolaSettings.getInt(AonService.this.mContentResolver, MotorolaSettings.SN_NIGHT_END, 0);
            if (uri != null) {
            }
            AonService.this.scheduleNextCheckNightOffAlarm(true, false);
            if (AonService.DEBUG) {
            }
            if (uri == null) {
            }
        }
    };
    protected OnSharedPreferenceChangeListener mSharedPreferenceListener = null;
    protected SharedPreferences mSharedPreferences;
    /* access modifiers changed from: protected */
    public int mSnAon;
    protected int mSnNightBegin;
    protected int mSnNightEnd;
    protected int mSnNightOff;
    /* access modifiers changed from: protected */
    public int mSnPrivacy;
    /* access modifiers changed from: protected */
    public TelephonyManager mTelephonyManager;
    /* access modifiers changed from: protected */
    public int mTutorialState = 0;
    /* access modifiers changed from: private */
    public boolean mWhiteListSettingLaunched = false;
    /* access modifiers changed from: private */
    public Runnable mWhiteListTutorialRunnable = new Runnable() {
        public void run() {
            if (AonService.DEBUG) {
                Log.d(AonService.TAG, "mWhiteListTutorialRunnable.run()");
            }
            SharedPreferences sharedPreferences = AonService.this.getSharedPreferences(Constants.AON_PREFS_NAME, 4);
            if (!AonService.this.mSharedPreferences.getBoolean(Constants.PREF_KEY_WHITE_LIST_SETTING_LAUNCHED, false)) {
                AonService.this.postWhiteListTutorialNotification();
            } else if (AonService.DEBUG) {
                Log.d(AonService.TAG, "White list already launched");
            }
        }
    };
    protected IWindowManager mWindowManager;
    /* access modifiers changed from: protected */
    public boolean mbHapticsEnabled = true;

    public interface IGetAonInterface {
        IAonInterface get();
    }

    private class MustOverrideException extends Exception {
        public MustOverrideException(String s) {
            super(s);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void onHapticsSettingsChanged();

    /* access modifiers changed from: protected */
    public abstract void onNotificationReadByAOV(long j, HashMap<String, ArrayList<String>> hashMap);

    /* access modifiers changed from: protected */
    public void registerBroadcast(IntentFilter ifilter) {
    }

    /* access modifiers changed from: protected */
    public boolean onReceiveBroadcast(Context context, Intent intent) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onAonSettingChange() {
    }

    /* access modifiers changed from: protected */
    public void onHdmiPlugged() {
    }

    /* access modifiers changed from: protected */
    public void onWirelessDisplayMirroring() {
    }

    /* access modifiers changed from: protected */
    public void onBatteryChange() {
    }

    /* access modifiers changed from: protected */
    public void onPrivacyModeChange() {
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    /* access modifiers changed from: protected */
    public void handleDismissKeyguard() {
        if (this.mScreenOn) {
            try {
                this.mWindowManager.dismissKeyguard();
            } catch (Exception e) {
                Log.w(TAG, "Failed to dismiss key guard.");
            }
        } else {
            this.mPendingDismissKeyguard = true;
        }
    }

    /* access modifiers changed from: protected */
    public void handleScreenOn() {
        this.mScreenOn = true;
        if (this.mPendingDismissKeyguard) {
            this.mPendingDismissKeyguard = false;
            try {
                this.mWindowManager.dismissKeyguard();
            } catch (Exception e) {
                Log.w(TAG, "Failed to dismiss key guard.");
            }
        }
    }

    /* access modifiers changed from: protected */
    public void handleScreenOff() {
        this.mScreenOn = false;
        this.mPendingDismissKeyguard = false;
    }

    /* access modifiers changed from: protected */
    public void handleWhiteListNotificationSelected() {
        if (DEBUG) {
            Log.d(TAG, "handleWhiteListNotificationSelected()");
        }
        this.mIsWhiteListTutorialNotificationShowing = false;
        Intent intent = new Intent(ACTION_LAUNCH_WHITE_LIST_TUTORIAL);
        intent.setPackage(BuildConfig.PACKAGE_NAME);
        intent.addFlags(268435456);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void handleWhiteListNotificationCleared() {
        if (DEBUG) {
            Log.d(TAG, "handleWhiteListNotificationCleared()");
        }
        this.mIsWhiteListTutorialNotificationShowing = false;
    }

    /* access modifiers changed from: protected */
    public synchronized void requestRebind() {
        if (DEBUG) {
            Log.d(TAG, "bind to AonInt");
        }
        Intent intentEnv = new Intent("com.motorola.aon.env.INTERFACE");
        intentEnv.setPackage(PKG_AON_INT);
        bindService(intentEnv, this.mInterfaceServiceConnection, 1);
    }

    /* access modifiers changed from: protected */
    public void onAonInterfaceConntected() {
    }

    /* access modifiers changed from: protected */
    public void onAonInterfaceDisconntected() {
    }

    public void onCreate() {
        super.onCreate();
        if (DEBUG) {
            Log.d(TAG, "onCreate");
        }
        MinedData.setMainHandler(this.mHandler);
        this.mTelephonyManager = (TelephonyManager) getSystemService("phone");
        this.mPowerManager = (PowerManager) getSystemService("power");
        this.mChargingWakeLock = this.mPowerManager.newWakeLock(1, "aon_charging");
        this.mContentResolver = getContentResolver();
        this.mAlarmManager = (AlarmManager) getSystemService("alarm");
        this.mKeyguardManager = (KeyguardManager) getSystemService("keyguard");
        this.mSharedPreferences = getSharedPreferences(Constants.AON_PREFS_NAME, 4);
        this.mTutorialState = this.mSharedPreferences.getInt(KEY_TUTORIAL_STATE, 0);
        this.mWhiteListSettingLaunched = this.mSharedPreferences.getBoolean(Constants.PREF_KEY_WHITE_LIST_SETTING_LAUNCHED, false);
        this.mDeviceProvisionedTime = this.mSharedPreferences.getLong(KEY_DEVICE_PROVISION_TIME, 0);
        R internalR = R.getInstance(this);
        this.mLowBatteryReminderLevels = getResources().getInteger(internalR.integer.config_lowBatteryWarningLevel);
        this.mCriticalBatteryReminderLevels = getResources().getInteger(internalR.integer.config_criticalBatteryWarningLevel);
        this.mScreenOn = this.mPowerManager.isScreenOn();
        this.mWindowManager = IWindowManager.Stub.asInterface(ServiceManager.getService("window"));
        refreshPrivacy();
        onEnableAon();
        scheduleNextMidNightAlarm();
        requestRebind();
        if (DEBUG) {
            Log.d(TAG, "mWhiteListSettingLaunched: " + this.mWhiteListSettingLaunched);
        }
        if (!this.mWhiteListSettingLaunched) {
            this.mSharedPreferenceListener = new OnSharedPreferenceChangeListener() {
                public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                    if (AonService.DEBUG) {
                        Log.d(AonService.TAG, "onSharedPreferenceChanged(" + key + "), mWhiteListSettingLaunched: " + AonService.this.mWhiteListSettingLaunched);
                    }
                    if (AonService.this.mWhiteListSettingLaunched) {
                        if (AonService.DEBUG) {
                            Log.d(AonService.TAG, "White list settings launched already, ignore.");
                        }
                    } else if (Constants.PREF_KEY_WHITE_LIST_SETTING_LAUNCHED.equals(key)) {
                        boolean launched = AonService.this.mSharedPreferences.getBoolean(Constants.PREF_KEY_WHITE_LIST_SETTING_LAUNCHED, false);
                        if (AonService.DEBUG) {
                            Log.d(AonService.TAG, "Shared preference changed: PREF_KEY_WHITE_LIST_SETTING_LAUNCHED : " + launched);
                        }
                        if (launched) {
                            if (AonService.DEBUG) {
                                Log.d(AonService.TAG, "User launched white list settings");
                            }
                            AonService.this.mWhiteListSettingLaunched = true;
                            AonService.this.mHandler.removeCallbacks(AonService.this.mWhiteListTutorialRunnable);
                            if (AonService.this.mIsWhiteListTutorialNotificationShowing) {
                                ((NotificationManager) AonService.this.getSystemService("notification")).cancel(1);
                                AonService.this.mIsWhiteListTutorialNotificationShowing = false;
                            }
                            AonService.this.mSharedPreferences.unregisterOnSharedPreferenceChangeListener(AonService.this.mSharedPreferenceListener);
                            AonService.this.mSharedPreferenceListener = null;
                        }
                    }
                }
            };
            this.mSharedPreferences.registerOnSharedPreferenceChangeListener(this.mSharedPreferenceListener);
        }
        if (this.mDeviceProvisionedTime == 0) {
            setDeviceProvisionTime();
        }
        checkTutorialState();
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.mInitialized) {
            onDisableAon();
        }
        if (this.mAonInterface != null) {
            this.mAonInterface.asBinder().unlinkToDeath(this.mDeathRecipient, 0);
            unbindService(this.mInterfaceServiceConnection);
            this.mAonInterface = null;
        }
        if (this.mIAonNL != null) {
            unbindService(this.mConnNLService);
            this.mIAonNL = null;
        }
        this.mTelephonyManager = null;
        this.mPowerManager = null;
        if (this.mSharedPreferenceListener != null) {
            this.mSharedPreferences.unregisterOnSharedPreferenceChangeListener(this.mSharedPreferenceListener);
        }
        this.mHandler.removeCallbacks(this.mOutOfBoxTutorialRunnable);
        this.mHandler.removeCallbacks(this.mWhiteListTutorialRunnable);
        if (this.mIsWhiteListTutorialNotificationShowing) {
            ((NotificationManager) getSystemService("notification")).cancel(1);
            this.mIsWhiteListTutorialNotificationShowing = false;
        }
        startService(new Intent(this, getClass()));
    }

    /* access modifiers changed from: protected */
    public AonNotificationManager getNotificationManager(Context context) throws MustOverrideException {
        throw new MustOverrideException("AonService.getNotificationManager mustbe overrided in subclass to return an instance of AonNotificationManager");
    }

    private void onEnableAon() {
        boolean z = true;
        if (!this.mInitialized) {
            this.mInitialized = true;
            if (DEBUG) {
                Log.d(TAG, "on Enable AON");
            }
            try {
                this.mAonNotificationManager = getNotificationManager(getApplicationContext());
                if (this.mAonNotificationManager != null) {
                    this.mAonNotificationManager.setTutorialState(this.mTutorialState);
                }
            } catch (MustOverrideException e) {
                e.printStackTrace();
            }
            this.mSettingsObserver.onChange(false, null);
            this.mContentResolver.registerContentObserver(System.getUriFor("screen_brightness"), true, this.mSettingsObserver);
            this.mContentResolver.registerContentObserver(System.getUriFor("screen_brightness_mode"), true, this.mSettingsObserver);
            this.mContentResolver.registerContentObserver(Secure.getUriFor(Settings.Secure.LOCK_SCREEN_LOCK_AFTER_TIMEOUT), true, this.mSettingsObserver);
            this.mContentResolver.registerContentObserver(System.getUriFor("screen_off_timeout"), true, this.mSettingsObserver);
            this.mContentResolver.registerContentObserver(Global.getUriFor("device_provisioned"), true, this.mSettingsObserver);
            this.mContentResolver.registerContentObserver(System.getUriFor("cooldown_mode_on"), true, this.mSettingsObserver);
            this.mContentResolver.registerContentObserver(System.getUriFor("haptic_feedback_enabled"), true, this.mSettingsObserver);
            this.mContentResolver.registerContentObserver(MotorolaSettings.getUriFor(MotorolaSettings.SN_AON), true, this.mSettingsObserver);
            this.mContentResolver.registerContentObserver(MotorolaSettings.getUriFor(MotorolaSettings.SN_PRIVACY), true, this.mSettingsObserver);
            this.mContentResolver.registerContentObserver(MotorolaSettings.getUriFor(MotorolaSettings.SN_NIGHT_OFF), true, this.mSettingsObserver);
            this.mContentResolver.registerContentObserver(MotorolaSettings.getUriFor(MotorolaSettings.SN_NIGHT_BEGIN), true, this.mSettingsObserver);
            this.mContentResolver.registerContentObserver(MotorolaSettings.getUriFor(MotorolaSettings.SN_NIGHT_END), true, this.mSettingsObserver);
            IntentFilter ifilter = new IntentFilter();
            ifilter.addAction("android.intent.action.SCREEN_ON");
            ifilter.addAction("android.intent.action.SCREEN_OFF");
            ifilter.addAction("android.intent.action.TIMEZONE_CHANGED");
            ifilter.addAction("android.intent.action.TIME_SET");
            ifilter.addAction("android.intent.action.DATE_CHANGED");
            ifilter.addAction(ACTION_MIDNIGHT_ALARM);
            ifilter.addAction(ACTION_WHITE_LIST_CHANGE);
            ifilter.addAction(WindowManagerPolicy.ACTION_HDMI_PLUGGED);
            ifilter.addAction(WirelessDisplayManager.WIRELESS_DISPLAY_MIRRORING_STATE_CHANGED_ACTION);
            ifilter.addAction(ACTION_DREAM_STATUS);
            ifilter.addAction("android.intent.action.BATTERY_CHANGED");
            ifilter.addAction(DevicePolicyManager.ACTION_DEVICE_POLICY_MANAGER_STATE_CHANGED);
            ifilter.addAction(ACTION_WHITE_LIST_TUTORIAL_SELECTED);
            ifilter.addAction(ACTION_WHITE_LIST_TUTORIAL_CLEARED);
            ifilter.setPriority(SCREEN_STATE_FILTER_PRIORITY);
            registerBroadcast(ifilter);
            registerReceiver(this.mReceiver, ifilter);
            IntentFilter ifilterAudioClear = new IntentFilter();
            ifilterAudioClear.addAction(ACTION_AUDIOMONITOR_CLEAR_NOTIFICATION);
            registerReceiver(this.mNotificationReadByAOVReceiver, ifilterAudioClear, PERMISSION_REGISTER_SMART_NOTIFICATION, null);
            this.mCoolDownMode = System.getInt(getApplicationContext().getContentResolver(), "cooldown_mode_on", 0);
            if (DEBUG) {
                Log.d(TAG, "bind to NLS");
            }
            Intent iReg = new Intent();
            iReg.setClass(getApplicationContext(), AonNotificationListenerService.class);
            bindService(iReg, this.mConnNLService, 1);
            if (this.mSnAon == 0) {
                z = false;
            }
            registerAsEndPoint(z);
        }
    }

    private void onDisableAon() {
        if (DEBUG) {
            Log.d(TAG, "on disable AON");
        }
        stopSelf();
        this.mContentResolver.unregisterContentObserver(this.mSettingsObserver);
        unregisterReceiver(this.mReceiver);
        this.mInitialized = false;
    }

    /* access modifiers changed from: protected */
    public void updateBreathing(boolean incoming, boolean force) {
    }

    /* access modifiers changed from: protected */
    public void updateTutorialState(int state) {
        if (DEBUG) {
            Log.d(TAG, "update tutorial state to " + state);
        }
        this.mTutorialState = state;
        Editor editor = this.mSharedPreferences.edit();
        editor.putInt(KEY_TUTORIAL_STATE, this.mTutorialState);
        editor.apply();
        this.mAonNotificationManager.setTutorialState(state);
    }

    /* access modifiers changed from: protected */
    public void onAodActivated() {
        if (DEBUG) {
            Log.d(TAG, "onAodActivated");
        }
        this.mHandler.sendEmptyMessage(4);
    }

    /* access modifiers changed from: protected */
    public void onIncomingNotification() {
        if (DEBUG) {
            Log.d(TAG, "onIncomingNotification");
        }
        this.mHandler.sendEmptyMessage(3);
    }

    /* access modifiers changed from: private */
    public void handleOnIncomingNotification() {
        if (DEBUG) {
            Log.d(TAG, "handleOnIncomingNotification");
        }
    }

    /* access modifiers changed from: private */
    public void handleOnAodActivated() {
        if (DEBUG) {
            Log.d(TAG, "handleOnAodActivated");
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return super.onStartCommand(intent, flags, startId);
        }
        if (intent.getIntExtra("BOOT", 0) == 1) {
            this.mHandler.sendEmptyMessage(2);
        }
        onEnableAon();
        String action = intent.getAction();
        if (DEBUG) {
            Log.d(TAG, "onStartCommand " + action);
        }
        if (ACTION_REFRESH_BREATHING.equals(action)) {
            updateBreathing(intent.getBooleanExtra("incoming", false), false);
        } else if (ACTION_TUTORIAL_DONE.equals(action)) {
            updateTutorialState(3);
        } else if (Constants.ACTION_CANCEL_NOTIFICATION.equals(action)) {
            String pkg = intent.getStringExtra(Constants.EXTRA_PACKAGE_NAME);
            String tag = intent.getStringExtra(Constants.EXTRA_TAG);
            int id = intent.getIntExtra(Constants.EXTRA_ID, -1);
            if (this.mIAonNL != null) {
                try {
                    this.mIAonNL.cancelNotification(pkg, tag, (long) id);
                } catch (RemoteException e) {
                }
            }
        } else if (ACTION_CHECKIN_ALARM.equals(action)) {
            if (DEBUG) {
                Log.d(TAG, "do checkin");
            }
            launchCheckin();
        } else if (Constants.ACTION_DISMISS_KEYGUARD.equals(action)) {
            handleDismissKeyguard();
        } else if (ACTION_SENSORHUB_PKG_ADDED.equals(action)) {
            if (DEBUG) {
                Log.d(TAG, "SensorHub Package added");
            }
            onSensorHubPackageAdded();
        }
        if (intent.getIntExtra("PERSIST", 0) == 1) {
            updateBreathing(false, false);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    /* access modifiers changed from: protected */
    public void onSensorHubPackageAdded() {
    }

    private void registerAsEndPoint(boolean bEnabled) {
        if (DEBUG) {
            Log.d(TAG, "registerAsEndPoint");
        }
        Intent iReg = new Intent(bEnabled ? Constants.ACTION_REGISTER_NOTIFICATION_LISTENER : Constants.ACTION_UNREGISTER_NOTIFICATION_LISTENER);
        iReg.putExtra(Constants.EXTRA_PACKAGE_NAME, getPackageName());
        iReg.putExtra(Constants.EXTRA_SERVICE_NAME, AonNotificationListenerService.class.getName());
        sendBroadcast(iReg);
    }

    /* access modifiers changed from: protected */
    public boolean isNightOff(Calendar cal) {
        boolean normalRegion;
        boolean overBegin;
        boolean underEnd;
        boolean res;
        if (DEBUG) {
            Log.d(TAG, "isNightOff");
        }
        if (this.mSnAon == 0 || this.mSnNightOff == 0 || this.mSnNightBegin == this.mSnNightEnd) {
            return false;
        }
        int nowMin = (cal.get(11) * 60) + cal.get(12);
        if (this.mSnNightBegin < this.mSnNightEnd) {
            normalRegion = true;
        } else {
            normalRegion = false;
        }
        if (nowMin >= this.mSnNightBegin) {
            overBegin = true;
        } else {
            overBegin = false;
        }
        if (nowMin < this.mSnNightEnd) {
            underEnd = true;
        } else {
            underEnd = false;
        }
        if ((!normalRegion || !overBegin || !underEnd) && (normalRegion || (!overBegin && !underEnd))) {
            res = false;
        } else {
            res = true;
        }
        if (!DEBUG) {
            return res;
        }
        Log.d(TAG, "Check now " + nowMin + " in night off (" + this.mSnNightBegin + "," + this.mSnNightEnd + ") " + (res ? "Yes" : "No"));
        return res;
    }

    /* access modifiers changed from: protected */
    public void scheduleNextCheckNightOffAlarm(boolean enableAlarm, boolean trigByPrevSchedule) {
        boolean beforeBegin;
        boolean beforeEnd;
        Intent intent = new Intent(ACTION_AON_CHECK_NIGHT_OFF);
        intent.setPackage(getPackageName());
        PendingIntent sender = PendingIntent.getBroadcast(this, 0, intent, 0);
        if (!enableAlarm || this.mSnAon == 0 || this.mSnNightOff == 0 || this.mSnNightBegin == this.mSnNightEnd) {
            this.mAlarmManager.cancel(sender);
            return;
        }
        Calendar calNow = Calendar.getInstance();
        long msNow = SystemClock.elapsedRealtime();
        Calendar calNextCheckPoint = (Calendar) calNow.clone();
        calNextCheckPoint.set(13, 0);
        int nowMin = (calNow.get(11) * 60) + calNow.get(12);
        if (trigByPrevSchedule) {
            beforeBegin = nowMin < this.mSnNightBegin;
            beforeEnd = nowMin < this.mSnNightEnd;
        } else {
            beforeBegin = nowMin <= this.mSnNightBegin;
            beforeEnd = nowMin <= this.mSnNightEnd;
        }
        if (this.mSnNightBegin < this.mSnNightEnd) {
            if (beforeBegin) {
                calNextCheckPoint.set(11, this.mSnNightBegin / 60);
                calNextCheckPoint.set(12, this.mSnNightBegin % 60);
            } else if (beforeEnd) {
                calNextCheckPoint.set(11, this.mSnNightEnd / 60);
                calNextCheckPoint.set(12, this.mSnNightEnd % 60);
            } else {
                calNextCheckPoint.set(11, this.mSnNightBegin / 60);
                calNextCheckPoint.set(12, this.mSnNightBegin % 60);
                calNextCheckPoint.add(6, 1);
            }
        } else if (beforeEnd) {
            calNextCheckPoint.set(11, this.mSnNightEnd / 60);
            calNextCheckPoint.set(12, this.mSnNightEnd % 60);
        } else if (beforeBegin) {
            calNextCheckPoint.set(11, this.mSnNightBegin / 60);
            calNextCheckPoint.set(12, this.mSnNightBegin % 60);
        } else {
            calNextCheckPoint.set(11, this.mSnNightEnd / 60);
            calNextCheckPoint.set(12, this.mSnNightEnd % 60);
            calNextCheckPoint.add(6, 1);
        }
        long timeout = calNextCheckPoint.getTimeInMillis() - calNow.getTimeInMillis();
        this.mAlarmManager.set(2, msNow + timeout, sender);
        if (DEBUG) {
            Log.d(TAG, "Schedule next check point after " + (timeout / 1000) + "seconds at " + calNextCheckPoint.toString());
        }
    }

    /* access modifiers changed from: private */
    public int getPolicyDataFromPrivacySetting(int snPrivacy) {
        return (-65536 & snPrivacy) >> 16;
    }

    /* access modifiers changed from: private */
    public int getUserChoiceFromPrivacySetting(int snPrivacy) {
        return 65535 & snPrivacy;
    }

    /* access modifiers changed from: private */
    public int makePrivacySetting(int policy, int user) {
        return (policy << 16) | (65535 & user);
    }

    /* access modifiers changed from: private */
    public void refreshPrivacy() {
        if (DEBUG) {
            Log.d(TAG, "refreshPrivacy");
        }
        int policyData = getNewPolicyData();
        int oldPolicyData = getPolicyDataFromPrivacySetting(this.mSnPrivacy);
        if (policyData != oldPolicyData) {
            if (DEBUG) {
                Log.d(TAG, "privacy policy changed from " + oldPolicyData + " to " + policyData);
            }
            this.mSnPrivacy = makePrivacySetting(policyData, getUserChoiceFromPrivacySetting(this.mSnPrivacy));
            MotorolaSettings.putInt(this.mContentResolver, MotorolaSettings.SN_PRIVACY, this.mSnPrivacy);
            onPrivacyModeChange();
        } else if (DEBUG) {
            Log.d(TAG, "privacy policy not changed " + oldPolicyData);
        }
    }

    private int getNewPolicyData() {
        if (DEBUG) {
            Log.d(TAG, "retrieve new policy data");
        }
        android.app.admin.DevicePolicyManager devicePolicyMgr = (android.app.admin.DevicePolicyManager) getSystemService("device_policy");
        if (keyguardDisableWidgetsSet(devicePolicyMgr, null)) {
            for (ComponentName admin : devicePolicyMgr.getActiveAdmins()) {
                if (keyguardDisableWidgetsSet(devicePolicyMgr, admin)) {
                    if (DEBUG) {
                        Log.d(TAG, "checking " + admin);
                    }
                    if ("com.google.android.apps.enterprise.dmagent".equals(admin.getPackageName())) {
                        if (!DEBUG) {
                            return 1;
                        }
                        Log.d(TAG, "google policy manager enforces the privacy");
                        return 1;
                    } else if (keyguardDisableWidgetsSet(devicePolicyMgr, admin)) {
                        int quality = devicePolicyMgr.getPasswordQuality(admin);
                        if (quality < 131072) {
                            if (DEBUG) {
                                Log.d(TAG, "none google policy manager requirs less security than numeric");
                            }
                        } else if (quality == 131072) {
                            if (devicePolicyMgr.getPasswordMinimumLength(admin) <= 4) {
                                if (DEBUG) {
                                    Log.d(TAG, "none google policy manager requirs <= 4 PIN");
                                }
                            } else if (!DEBUG) {
                                return 1;
                            } else {
                                Log.d(TAG, "none google policy manager requirs > 4 PIN");
                                return 1;
                            }
                        } else if (!DEBUG) {
                            return 1;
                        } else {
                            Log.d(TAG, "none google policy manager enforces the strong privacy");
                            return 1;
                        }
                    } else {
                        continue;
                    }
                }
            }
        }
        return 0;
    }

    private boolean keyguardDisableWidgetsSet(android.app.admin.DevicePolicyManager dpm, ComponentName admin) {
        if (DEBUG) {
            Log.d(TAG, "keyguardDisableWidgetsSet");
        }
        if ((dpm.getKeyguardDisabledFeatures(admin) & 1) == 1) {
            return true;
        }
        return false;
    }

    private void launchCheckin() {
        if (DEBUG) {
            Log.d(TAG, "launchCheckin mSnAon=" + this.mSnAon);
        }
        if (1 == this.mSnAon) {
            if (DEBUG) {
                Log.d(TAG, "launchCheckin reporting Sn Event Analytics. start SnAnalyticsEventService");
            }
            startService(new Intent(this, SnAnalyticsEventService.class));
        }
        if (DEBUG) {
            Log.d(TAG, "launchCheckin reporting Settings Analytics. start PackageEventService");
        }
        Intent serviceIntent = new Intent(this, PackageEventService.class);
        serviceIntent.setAction(Constants.SN_ANALYTICS_SETTINGS);
        startService(serviceIntent);
    }

    /* access modifiers changed from: private */
    public synchronized void scheduleNextMidNightAlarm() {
        boolean enabled = true;
        synchronized (this) {
            if (this.mSnAon == 0) {
                enabled = false;
            }
            Intent intent = new Intent(ACTION_MIDNIGHT_ALARM);
            intent.setPackage(getPackageName());
            PendingIntent sender = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
            if (!enabled) {
                if (DEBUG) {
                    Log.d(TAG, "cancel midnight alarm");
                }
                this.mAlarmManager.cancel(sender);
            } else {
                Calendar calNow = Calendar.getInstance();
                long msNow = SystemClock.elapsedRealtime();
                Calendar calNextMidNight = (Calendar) calNow.clone();
                calNextMidNight.add(6, 1);
                calNextMidNight.set(11, 0);
                calNextMidNight.set(12, 0);
                calNextMidNight.set(13, 0);
                calNextMidNight.set(14, 0);
                long timeout = calNextMidNight.getTimeInMillis() - calNow.getTimeInMillis();
                this.mAlarmManager.set(2, msNow + timeout, sender);
                if (DEBUG) {
                    Log.d(TAG, "Schedule next midnight alarm after " + (timeout / 1000) + "seconds at " + calNextMidNight.toString());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMidNightAlarm() {
    }

    /* access modifiers changed from: private */
    public void scheduleCheckinAlarm(boolean on) {
        Intent intent = new Intent(ACTION_CHECKIN_ALARM);
        intent.setClassName(getApplicationContext(), getClass().getName());
        PendingIntent sender = PendingIntent.getService(getApplicationContext(), 0, intent, 0);
        if (!on) {
            if (DEBUG) {
                Log.d(TAG, "cancel checkin alarm");
            }
            this.mAlarmManager.cancel(sender);
            return;
        }
        this.mAlarmManager.setRepeating(2, SystemClock.elapsedRealtime() + CHECKIN_INTERVAL, CHECKIN_INTERVAL, sender);
        if (DEBUG) {
            Log.d(TAG, "enable checkin alarm");
        }
    }

    /* access modifiers changed from: protected */
    public void handleUpdateBreathing(boolean incomingNotification) {
    }

    private void setDeviceProvisionTime() {
        if (this.mDeviceProvisionedTime == 0) {
            this.mDeviceProvisionedTime = System.currentTimeMillis();
            Editor editor = this.mSharedPreferences.edit();
            editor.putInt(KEY_TUTORIAL_STATE, this.mTutorialState);
            editor.putLong(KEY_DEVICE_PROVISION_TIME, this.mDeviceProvisionedTime);
            editor.apply();
        }
    }

    private void checkTutorialState() {
        if (this.mDeviceProvisionedTime != 0) {
            long elapsedTime = System.currentTimeMillis() - this.mDeviceProvisionedTime;
            if (DEBUG) {
                Log.d(TAG, "Elapsed time from provision : " + elapsedTime);
            }
            if (DEBUG) {
                Log.d(TAG, "Current tutorial state : " + this.mTutorialState);
            }
            if (this.mTutorialState == 1) {
                if (elapsedTime >= DURATION_24_HOURS) {
                    if (DEBUG) {
                        Log.d(TAG, "Already passed 24 hours, update tutorial state to on going");
                    }
                    updateTutorialState(2);
                } else {
                    long timeRemaining = DURATION_24_HOURS - elapsedTime;
                    if (DEBUG) {
                        Log.d(TAG, "Will update tutorial state to on going in " + timeRemaining + " ms");
                    }
                    this.mHandler.removeCallbacks(this.mOutOfBoxTutorialRunnable);
                    this.mHandler.postDelayed(this.mOutOfBoxTutorialRunnable, timeRemaining);
                }
            }
            if (DEBUG) {
                Log.d(TAG, "Has white list settings launched? " + this.mWhiteListSettingLaunched);
            }
            if (this.mWhiteListSettingLaunched) {
                return;
            }
            if (elapsedTime >= DURATION_7_DAYS) {
                if (DEBUG) {
                    Log.d(TAG, "Already passed 1 week, post white list tutorial notification");
                }
                postWhiteListTutorialNotification();
                return;
            }
            long timeRemaining2 = DURATION_7_DAYS - elapsedTime;
            if (DEBUG) {
                Log.d(TAG, "Will post white list tutorial notification in " + timeRemaining2 + " ms");
            }
            this.mHandler.removeCallbacks(this.mWhiteListTutorialRunnable);
            this.mHandler.postDelayed(this.mWhiteListTutorialRunnable, timeRemaining2);
        } else if (DEBUG) {
            Log.d(TAG, "Device not yet provisioned");
        }
    }

    /* access modifiers changed from: private */
    public void postWhiteListTutorialNotification() {
        if (DEBUG) {
            Log.d(TAG, "postWhiteListTutorialNotification()");
        }
        Context context = getApplicationContext();
        Intent iSelect = new Intent(ACTION_WHITE_LIST_TUTORIAL_SELECTED);
        iSelect.setPackage(getPackageName());
        iSelect.addFlags(268435456);
        PendingIntent piSelect = PendingIntent.getBroadcast(context, 0, iSelect, 0);
        Intent iClear = new Intent(ACTION_WHITE_LIST_TUTORIAL_CLEARED);
        iClear.setPackage(getPackageName());
        iClear.addFlags(268435456);
        ((NotificationManager) getSystemService("notification")).notify(1, new Builder(context).setContentTitle(getResources().getString(R.string.tutorial_whitelist_title)).setContentText(getResources().getString(R.string.tutorial_whitelist_description)).setContentIntent(piSelect).setDeleteIntent(PendingIntent.getBroadcast(context, 0, iClear, 0)).setSmallIcon(R.drawable.ic_tutorial_target).setAutoCancel(true).setShowWhen(false).build());
        this.mIsWhiteListTutorialNotificationShowing = true;
        if (DEBUG) {
            Log.d(TAG, "Posted 1 week white list notification");
        }
        updateWhiteListLaunchedSharedPreference();
    }

    private void updateWhiteListLaunchedSharedPreference() {
        if (DEBUG) {
            Log.d(TAG, "updateWhiteListLaunchedSharedPreference()");
        }
        this.mWhiteListSettingLaunched = true;
        this.mSharedPreferences.unregisterOnSharedPreferenceChangeListener(this.mSharedPreferenceListener);
        this.mSharedPreferenceListener = null;
        Editor editor = getSharedPreferences(Constants.AON_PREFS_NAME, 4).edit();
        editor.putBoolean(Constants.PREF_KEY_WHITE_LIST_SETTING_LAUNCHED, true);
        editor.apply();
        if (DEBUG) {
            Log.d(TAG, "white list launched preference updated to true.");
        }
    }
}
