package com.motorola.aon.settings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.motorola.aon.R;

public class WhiteListTutorialActivity extends Activity implements OnClickListener {
    private static final boolean DEBUG = true;
    private static final String TAG = "WhiteListTutorialActivity";

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        Log.d(TAG, "onCreate()");
        setContentView(R.layout.white_list_tutorial);
        View settings = findViewById(R.id.textSettings);
        if (settings != null) {
            settings.setOnClickListener(this);
        }
        TextView done = (TextView) findViewById(R.id.textDone);
        if (done != null) {
            done.setOnClickListener(this);
        }
    }

    public void onClick(View v) {
        Log.d(TAG, "onClick()");
        if (v != null) {
            switch (v.getId()) {
                case R.id.textSettings /*2131624008*/:
                    Log.d(TAG, "Launch white list Settings");
                    Intent i = new Intent(this, WhiteListActivity.class);
                    i.putExtra("from_tutorial", 1);
                    startActivity(i);
                    return;
                case R.id.textDone /*2131624009*/:
                    Log.d(TAG, "Done");
                    finish();
                    return;
                default:
                    return;
            }
        }
    }
}
