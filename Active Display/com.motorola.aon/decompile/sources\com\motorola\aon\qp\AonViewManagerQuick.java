package com.motorola.aon.qp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import com.motorola.aon.AonNotificationManager;
import com.motorola.aon.AonService;
import com.motorola.aon.AonService.IGetAonInterface;
import com.motorola.aon.AonViewManager;
import com.motorola.aon.R;
import com.motorola.aon.SnAnalyticsEventService;
import com.motorola.aon.Utils;
import com.motorola.aon.aidl.NotificationEntry;
import com.motorola.aon.env.android.view.Display;
import com.motorola.aon.env.android.view.DisplayInfo;
import com.motorola.aon.env.com.android.internal.statusbar.MinedData;
import com.motorola.aon.jni.QuickDraw;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;

public class AonViewManagerQuick extends AonViewManager {
    private static final String DUMP_BITMAP_FILE_PATH = "/data/aoddumpbitmap";
    static final int MSG_UPDATE_BREATHING_VIEW = 1;
    static final int MULTIPEEK_BUFFER_FULL_SCREEN = 0;
    static final int MULTIPEEK_BUFFER_PEEK_START = 1;
    static final String TAG = "AonViewManagerQuick";
    private static int[] mBreathingOffset = {0, 0, 8, 0, 8, -8, 0, -8, -8, -8, -8, 0, -8, 8, 0, 8, 8, 8};
    private View mBreathingContent = null;
    private final int mBreathingContentLeft;
    private Handler mBreathingDrawerHandler;
    private int mBreathingViewPos;
    private BreathingViewUpdateThread mBreathingViewUpdateThread;
    private DisplayInfo mDisplayInfo;
    private int mHeight;
    private int mMultiPeekBufferId;
    private final int mRegion0X;
    private int mWidth;

    class BreathingDrawerHandler extends Handler {
        BreathingDrawerHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    if (AonViewManagerQuick.DEBUG) {
                        Log.d(AonViewManagerQuick.TAG, "handleMessage- MSG_UPDATE_BREATHING_VIEW");
                    }
                    AonViewManagerQuick.this.handleUpdateBreathingView((BreathingViewUpdateReq) msg.obj);
                    return;
                default:
                    return;
            }
        }
    }

    static class BreathingViewUpdateReq {
        int mBatteryStatus;
        ArrayList<NotificationEntry> mBreathingList;
        ICallback mListener;
        int mViewType;

        public BreathingViewUpdateReq(int viewType, ArrayList<NotificationEntry> breathingList, int batteryStatus, ICallback listener) {
            this.mViewType = viewType;
            this.mBreathingList = breathingList;
            this.mBatteryStatus = batteryStatus;
            this.mListener = listener;
        }
    }

    class BreathingViewUpdateThread extends HandlerThread {
        public BreathingViewUpdateThread(String name) {
            super(name, -4);
        }

        /* access modifiers changed from: protected */
        public void onLooperPrepared() {
            AonViewManagerQuick.this.initRes();
        }
    }

    public interface ICallback {
        void sendResult(Bundle bundle);
    }

    public void getBreathingOffset(int pos, int[] offset) {
        if (DEBUG) {
            Log.d(TAG, "getBreathingOffset");
        }
        if (pos < 0 || pos >= mBreathingOffset.length / 2 || offset == null || offset.length != 2) {
            throw new RuntimeException("unexpected input parameters " + pos);
        }
        offset[0] = mBreathingOffset[pos * 2];
        offset[1] = mBreathingOffset[(pos * 2) + 1];
        if (DEBUG) {
            Log.d(TAG, "getBreathingOffset pos=" + pos + "offset[0]=" + offset[0] + " offset[1]=" + offset[1]);
        }
    }

    public void nextBreathingViewPos() {
        if (DEBUG) {
            Log.d(TAG, "nextBreathingViewPos");
        }
        if (this.mBreathingViewPos == (mBreathingOffset.length / 2) - 1) {
            this.mBreathingViewPos = 0;
        } else {
            this.mBreathingViewPos++;
        }
    }

    public AonViewManagerQuick(Context context, AonNotificationManager aonNotificationManager, IGetAonInterface getAonService) {
        super(context, aonNotificationManager, getAonService);
        if (DEBUG) {
            Log.d(TAG, TAG);
        }
        this.mRegion0X = context.getResources().getDimensionPixelSize(R.dimen.region0_start);
        this.mBreathingContentLeft = context.getResources().getDimensionPixelSize(R.dimen.breathing_content_left_margin);
        this.mBreathingViewPos = new Random(SystemClock.uptimeMillis()).nextInt(mBreathingOffset.length / 2);
        if (DEBUG) {
            Log.d(TAG, "initial breathing position " + this.mBreathingViewPos + ",(" + mBreathingOffset[this.mBreathingViewPos * 2] + "," + mBreathingOffset[(this.mBreathingViewPos * 2) + 1] + ")");
        }
        DisplayInfo info = Display.getDisplayInfo(this.mDisplay);
        if (info.rotation == 1 || info.rotation == 3) {
            this.mWidth = info.logicalHeight;
            this.mHeight = info.logicalWidth;
            return;
        }
        this.mWidth = info.logicalWidth;
        this.mHeight = info.logicalHeight;
    }

    public void initBreathingThread() {
        if (DEBUG) {
            Log.d(TAG, "initBreathingThread");
        }
        this.mMultiPeekBufferId = 1;
        this.mBreathingViewUpdateThread = new BreathingViewUpdateThread("breathing drawer");
        this.mBreathingViewUpdateThread.start();
        this.mBreathingDrawerHandler = new BreathingDrawerHandler(this.mBreathingViewUpdateThread.getLooper());
        QuickDraw.nativeInit();
    }

    public void deInitBreathingThread() {
        if (DEBUG) {
            Log.d(TAG, "deInitBreathingThread");
        }
        if (this.mBreathingViewUpdateThread != null) {
            this.mBreathingViewUpdateThread.quit();
            this.mBreathingDrawerHandler = null;
            this.mBreathingViewUpdateThread = null;
        }
        if (DEBUG) {
            Log.d(TAG, "deInitBreathingThread");
        }
    }

    public Bitmap getNotificationBitmap(MinedData md, boolean big) {
        return big ? md.largeIcon : md.iconBitmap;
    }

    public void requestUpdateBreathingView(int viewType, ArrayList<NotificationEntry> breathingList, int batteryStatus, ICallback listener) {
        if (DEBUG) {
            Log.d(TAG, "requestUpdateBreathingView");
        }
        if (this.mBreathingDrawerHandler != null) {
            if (DEBUG) {
                Log.d(TAG, "request to update breathing view " + viewType);
            }
            BreathingViewUpdateReq req = new BreathingViewUpdateReq(viewType, breathingList, batteryStatus, listener);
            if (DEBUG) {
                Log.d(TAG, "requestUpdateBreathingView: Send msg=MSG_UPDATE_BREATHING_VIEW to handlers");
            }
            this.mBreathingDrawerHandler.obtainMessage(1, req).sendToTarget();
            return;
        }
        Log.e(TAG, "UI thread not running");
    }

    public void requestHideBreathingView(ICallback listener) {
        if (DEBUG) {
            Log.d(TAG, "requestHideBreathingView");
        }
        if (this.mBreathingDrawerHandler != null) {
            if (DEBUG) {
                Log.d(TAG, "request to hide breathing view ");
            }
            this.mBreathingDrawerHandler.removeMessages(1);
            return;
        }
        Log.e(TAG, "UI thread not running");
    }

    /* access modifiers changed from: private */
    public void handleUpdateBreathingView(BreathingViewUpdateReq req) {
        if (DEBUG) {
            Log.d(TAG, "handleUpdateBreathingView");
        }
        ICallback listener = req.mListener;
        int viewType = req.mViewType;
        ArrayList<NotificationEntry> breathingList = req.mBreathingList;
        if (viewType == 2) {
            this.mBreathingNotificationEntry = null;
        } else if (breathingList == null || breathingList.size() <= 0) {
            this.mBreathingNotificationEntry = null;
            if (DEBUG) {
                Log.d(TAG, "request update breathing view for clock only mode");
            }
        } else {
            this.mBreathingNotificationEntry = (NotificationEntry) breathingList.get(0);
        }
        fillBreathingView(this.mCachedBreathingView, breathingList, req.mBatteryStatus, viewType);
        if (this.mBreathingContent == null) {
            this.mBreathingContent = this.mCachedBreathingView.findViewById(R.id.breathing_content);
            if (DEBUG && this.mBreathingContent == null) {
                throw new RuntimeException("wrong breathing view");
            }
        }
        LayoutParams lp = this.mBreathingContent.getLayoutParams();
        if (!DEBUG || (lp instanceof FrameLayout.LayoutParams)) {
            FrameLayout.LayoutParams fllp = (FrameLayout.LayoutParams) lp;
            int brathingViewPos = this.mBreathingViewPos;
            fllp.leftMargin = mBreathingOffset[brathingViewPos * 2] + this.mBreathingContentLeft;
            fllp.topMargin = this.mRegion0X + mBreathingOffset[(brathingViewPos * 2) + 1];
            this.mBreathingContent.setLayoutParams(fllp);
            this.mCachedBreathingView.measure(MeasureSpec.makeMeasureSpec(this.mWidth, 1073741824), MeasureSpec.makeMeasureSpec(this.mHeight, 1073741824));
            this.mCachedBreathingView.layout(0, 0, this.mWidth, this.mHeight);
            drawView(this.mCachedBreathingView);
            if (listener != null) {
                Bundle args = new Bundle();
                args.putInt("pos", brathingViewPos);
                if (DEBUG) {
                    Log.d(TAG, "drawView invoke callback.sendResult");
                }
                listener.sendResult(args);
                return;
            }
            return;
        }
        throw new RuntimeException("wrong breathing view, not in framelayout");
    }

    private void drawView(View view) {
        if (DEBUG) {
            Log.d(TAG, "drawView");
        }
        try {
            Bitmap bmp = Bitmap.createBitmap(this.mWidth, this.mHeight, Config.RGB_565);
            Canvas c = new Canvas(bmp);
            c.drawColor(-1);
            view.draw(c);
            if (DEBUG) {
                Log.d(TAG, "drawView passbuffer to kernel");
            }
            if (QuickDraw.nativePassBuffer(bmp, 0) != 0) {
                Log.e(TAG, "drawView: ERROR QuickDraw.nativePassBuffer failed");
            } else if (DEBUG && new File(DUMP_BITMAP_FILE_PATH).exists()) {
                Utils.dumpBitmaps(bmp, "peek0_" + System.currentTimeMillis() + ".png");
            }
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "error drawing breathing view", e);
        }
    }

    public void handleScreenOnAction(int reason) {
        if (DEBUG) {
            Log.i(TAG, "handleScreenOnAction reason=" + reason);
        }
        if (this.mBreathingDrawerHandler != null) {
            this.mBreathingDrawerHandler.removeMessages(1);
        }
        for (int i = 1; i < this.mMultiPeekBufferId; i++) {
            QuickDraw.nativeRemoveBuffer(i);
        }
        this.mMultiPeekBufferId = 1;
    }

    public void handleOnTouch(int reason) {
        if (DEBUG) {
            Log.d(TAG, "handleOnTouch reason=" + reason);
        }
        if (reason == 1) {
            SnAnalyticsEventService.mUnlockCount++;
            unlock();
        } else if (reason == 2) {
            SnAnalyticsEventService.mLaunchCount++;
            launchNotification();
        }
    }

    /* access modifiers changed from: protected */
    public void onTutorialLaunched() {
        Intent i2 = new Intent(this.mContext, AonServiceQuick.class);
        i2.setAction(AonService.ACTION_TUTORIAL_DONE);
        this.mContext.startService(i2);
    }

    /* access modifiers changed from: protected */
    public void requestToDismissKeyguard() {
        try {
            this.mWindowManager.dismissKeyguard();
        } catch (Exception e) {
            Log.w(TAG, "Failed to dismiss key guard.");
        }
    }

    /* access modifiers changed from: protected */
    public boolean canDismissKeyguard() {
        return true;
    }

    /* access modifiers changed from: protected */
    public Class getServiceClass() throws RuntimeException {
        return AonServiceQuick.class;
    }
}
