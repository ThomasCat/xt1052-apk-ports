package com.motorola.aon.jni;

import android.graphics.Bitmap;

public class QuickDraw {
    private static final boolean DEBUG = true;
    private static final String TAG = "Aon_QuickDraw";

    public static native int nativeInit();

    public static native int nativePassBuffer(Bitmap bitmap, int i);

    public static native void nativeRemoveBuffer(int i);

    static {
        System.loadLibrary("quickdraw");
    }
}
