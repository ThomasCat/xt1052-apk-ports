package com.motorola.aon.env.android.app;

import android.util.Log;
import java.lang.reflect.Method;

public class IActivityManager {
    private static final String METHOD_RESUME_APP_SWITCHES = "resumeAppSwitches";
    private static final String TAG = "Aon_IActivityManager";
    private final Object mIActivityManager;
    private Method mMethodResumeAppSwitches;

    public IActivityManager(Object localProxy) {
        this.mIActivityManager = localProxy;
        try {
            this.mMethodResumeAppSwitches = this.mIActivityManager.getClass().getDeclaredMethod(METHOD_RESUME_APP_SWITCHES, new Class[0]);
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize status bar service");
        }
    }

    public void resumeAppSwitches() {
        if (this.mIActivityManager != null && this.mMethodResumeAppSwitches != null) {
            try {
                this.mMethodResumeAppSwitches.invoke(this.mIActivityManager, new Object[0]);
            } catch (Throwable th) {
                Log.w(TAG, "unable to invoke resumeAppSwitches");
            }
        }
    }
}
