package com.motorola.aon.env.android.os;

import android.util.Log;
import java.lang.reflect.Method;

public class SystemProperties {
    private static final String CLASS_SYSTEM_PROPERTIES = "android.os.SystemProperties";
    private static final String METHOD_GET = "get";
    private static final String METHOD_GET_BOOLEAN = "getBoolean";
    private static final String TAG = "Aon_SystemProperties";
    public static boolean sInitialized;
    private static Method sMethodGet;
    private static Method sMethodGetBoolean;

    public static boolean getBoolean(String string, boolean b) {
        boolean z = false;
        if (sMethodGetBoolean == null) {
            return z;
        }
        try {
            return ((Boolean) sMethodGetBoolean.invoke(null, new Object[]{string, Boolean.valueOf(b)})).booleanValue();
        } catch (Throwable th) {
            Log.w(TAG, "unable to invoke getBoolean");
            return z;
        }
    }

    public static String get(String key, String def) {
        if (sMethodGet != null) {
            try {
                return (String) sMethodGet.invoke(null, new Object[]{key, def});
            } catch (Throwable th) {
                Log.w(TAG, "unable to invoke get");
            }
        }
        return def;
    }

    static {
        sInitialized = false;
        try {
            Class<?> clazz = Class.forName(CLASS_SYSTEM_PROPERTIES);
            sMethodGetBoolean = clazz.getDeclaredMethod(METHOD_GET_BOOLEAN, new Class[]{String.class, Boolean.TYPE});
            sMethodGet = clazz.getDeclaredMethod(METHOD_GET, new Class[]{String.class, String.class});
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize class");
        }
    }
}
