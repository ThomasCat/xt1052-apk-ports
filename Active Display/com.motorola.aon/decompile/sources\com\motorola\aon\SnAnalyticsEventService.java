package com.motorola.aon;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.IBinder;
import com.motorola.aon.env.com.motorola.blur.util.CheckinEventWrapper;
import com.motorola.aon.jni.PartialDisplay.InstrumentationRegister;

public class SnAnalyticsEventService extends Service {
    private static final String CHECKIN_ENDPOINT_NAME = "AOD";
    private static final String CHECKIN_GESTURE_ALS_VERSION = "1.2";
    private static final String CHECKIN_TAG_SN_AOD_STAT = "SN_AOD_STAT";
    private static final String CHECKIN_TAG_SN_STAT = "SN_STAT";
    private static final String CHECKIN_VERSION = "1.1";
    private static final boolean DEBUG = false;
    private static final String EVENT_ENDPOINT_ACTIONS = "ENDPOINT_ACTIONS";
    private static final String EVENT_FACE_DOWN_TOTALS = "FACE_DOWN_TOTALS";
    private static final String EVENT_GESTURE_ALS = "GESTURE_ALS";
    private static final String EVENT_INCOMING = "INCOMING";
    private static final String EVENT_INCOMING_ALS = "INCOMING_ALS";
    private static final String EVENT_MMOTION_TOTALS = "MMOTION_TOTALS";
    private static final String EVENT_NOTIFICATIONS = "SN_TOTALS";
    private static final String EVENT_STOWED_TOTALS = "STOWED_TOTALS";
    private static final String EVENT_TOTAL_BREATHS = "TOTAL_BREATHS";
    private static final String EVENT_TOTAL_BREATHS_ALS = "TOTAL_BREATHS_ALS";
    private static final String EVENT_TOUCH = "TOUCH";
    private static final String KEY_ACTION_COUNT = "action_count";
    private static final String KEY_ALS_0 = "als_0";
    private static final String KEY_ALS_1 = "als_1";
    private static final String KEY_ALS_2 = "als_2";
    private static final String KEY_ALS_3 = "als_3";
    private static final String KEY_ALS_4 = "als_4";
    private static final String KEY_ALS_5 = "als_5";
    private static final String KEY_ALS_6 = "als_6";
    private static final String KEY_BLOCKED_COUNT = "blocked_count";
    private static final String KEY_BREATH_FLAT_DOWN_EXIT = "bth_flat_down_exit";
    private static final String KEY_BREATH_FLAT_UP_EXIT = "bth_flat_up_exit";
    private static final String KEY_BREATH_INTERACTION_TOUCH_FLAT_DOWN_EXIT = "bit_flat_down_exit";
    private static final String KEY_BREATH_INTERACTION_TOUCH_FLAT_UP_EXIT = "bit_flat_up_exit";
    private static final String KEY_BREATH_INTERACTION_TOUCH_MAGIC_SWIPE = "bit_magic_swipe";
    private static final String KEY_BREATH_INTERACTION_TOUCH_MVMT_NORMAL = "bit_mvmt_normal";
    private static final String KEY_BREATH_INTERACTION_TOUCH_MVMT_SHAKE = "bit_mvmt_shake";
    private static final String KEY_BREATH_INTERACTION_TOUCH_NO_GESTURE = "bit_no_gesture";
    private static final String KEY_BREATH_INTERACTION_TOUCH_STOWED_EXIT = "bit_stowed_exit";
    private static final String KEY_BREATH_MAGIC_SWIPE = "bth_magic_swipe";
    private static final String KEY_BREATH_MVMT_NORMAL = "bth_mvmt_normal";
    private static final String KEY_BREATH_MVMT_SHAKE = "bth_mvmt_shake";
    private static final String KEY_BREATH_NO_GESTURE = "bth_no_gesture";
    private static final String KEY_BREATH_STOWED_EXIT = "bth_stowed_exit";
    private static final String KEY_CLEAR_COUNT = "clear_count";
    private static final String KEY_CONTACT_MATCH = "contact_match";
    private static final String KEY_CUSTOM_STYLE = "custom_style";
    private static final String KEY_DRAW_AVG_TIME = "draw_avg_time";
    private static final String KEY_DRAW_MAX_TIME = "draw_max_time";
    private static final String KEY_EXIT_COUNT = "exit_count";
    private static final String KEY_FU_EXIT = "flatup_exit";
    private static final String KEY_FU_EXIT_CD_BLOCKED = "fu_cd_blocked";
    private static final String KEY_HS = "hard_shake";
    private static final String KEY_HS_CD_BLOCKED = "hs_cd_blocked";
    private static final String KEY_INCOMING_AVG_TIME = "incoming_avg_time";
    private static final String KEY_INCOMING_MAX_TIME = "incoming_max_time";
    private static final String KEY_INCOMING_NO_BREATH = "incoming_no_breath";
    private static final String KEY_INTENT_LAUNCH = "intent_launch";
    private static final String KEY_INVALID_COUNT = "invalid_count";
    private static final String KEY_INVALID_RELEASE = "invalid_release";
    private static final String KEY_LATE_COUNT = "late_count";
    private static final String KEY_LAUNCH_COUNT = "launch_count";
    private static final String KEY_LOW_PRIORITY = "low_priority";
    private static final String KEY_MAX_MINED = "max_mined";
    private static final String KEY_MAX_MINED_PER_PKG = "max_mined_per_pkg";
    private static final String KEY_MINED = "mined";
    private static final String KEY_MM_DETECT = "mm_detect";
    private static final String KEY_MM_INVALID = "mm_invalid";
    private static final String KEY_MUTE_COUNT = "mute_count";
    private static final String KEY_NAME = "name";
    private static final String KEY_NOTIFICATIONS = "notifications";
    private static final String KEY_ONGOING = "ongoing";
    private static final String KEY_OTHER_LAUNCH = "other_launch";
    private static final String KEY_POWER_KEY = "power_key";
    private static final String KEY_SMS_COUNT = "sms_count";
    private static final String KEY_SN_NAME = "name";
    private static final String KEY_STAGE_0 = "stage_0";
    private static final String KEY_STAGE_1 = "stage_1";
    private static final String KEY_STAGE_2 = "stage_2";
    private static final String KEY_STAGE_3 = "stage_3";
    private static final String KEY_TOTAL_PEEK = "total_peek";
    private static final String KEY_TOTAL_SMS = "total_sms";
    private static final String KEY_TOTAL_TIME_ON = "total_time_on";
    private static final String KEY_VALID_COUNT = "valid_count";
    static final String TAG = "Aon_SnAnalyticsEventSvc";
    private static int mBlockBreaths = 0;
    private static boolean mClockOnlyBreathing = false;
    private static int mCurrentFaceDownExit;
    private static int mCurrentMMotion;
    private static int mCurrentStowedExit;
    private static long mCustomNotifications;
    private static long mDrawMaxTime = 0;
    private static long mDrawTotalCount = 0;
    private static long mDrawTotalTime = 0;
    private static long mHandbackTime = 0;
    private static long mHandoverTime = 0;
    public static int mIncomingALS = 0;
    private static int mIncomingBreathTime;
    private static long mIncomingMaxTime = 0;
    private static boolean mIncomingRefresh = false;
    private static long mIncomingTime = 0;
    private static long mIncomingTotalCount = 0;
    private static long mIncomingTotalTime = 0;
    private static InstrumentationRegister mInstrumetReg = new InstrumentationRegister();
    public static int mLaunchCount = 0;
    private static long mLowPriNotifications;
    private static long mMaxActiveNotifications;
    private static long mMinedNotifications;
    public static int mMuteCount = 0;
    private static int mNormalBreathTime;
    private static long mNotifications;
    private static long mOngoingNotifications;
    public static int mPowerKeyCount = 0;
    private static int mTotalCurrentBreathsStage1to4 = 0;
    public static int mUnlockCount = 0;

    public static void setIncomingTime(long time) {
        mIncomingTime = time;
    }

    public static long getIncomingTime() {
        return mIncomingTime;
    }

    public static void setIncomingRefresh(boolean refreshed) {
        mIncomingRefresh = refreshed;
    }

    public static boolean isIncomingRefreshed() {
        return mIncomingRefresh;
    }

    public static void recordStatusRegister(InstrumentationRegister ir) {
        InstrumentationRegister instrumentationRegister = mInstrumetReg;
        instrumentationRegister.mStowedEnter += ir.mStowedEnter;
        InstrumentationRegister instrumentationRegister2 = mInstrumetReg;
        instrumentationRegister2.mStowedExit += ir.mStowedExit;
        InstrumentationRegister instrumentationRegister3 = mInstrumetReg;
        instrumentationRegister3.mStowedTotal += ir.mStowedTotal;
        InstrumentationRegister instrumentationRegister4 = mInstrumetReg;
        instrumentationRegister4.mFacedownEnter += ir.mFacedownEnter;
        InstrumentationRegister instrumentationRegister5 = mInstrumetReg;
        instrumentationRegister5.mFacedownExit += ir.mFacedownExit;
        InstrumentationRegister instrumentationRegister6 = mInstrumetReg;
        instrumentationRegister6.mUnstowNoAction += ir.mUnstowNoAction;
        InstrumentationRegister instrumentationRegister7 = mInstrumetReg;
        instrumentationRegister7.mMMAction += ir.mMMAction;
        InstrumentationRegister instrumentationRegister8 = mInstrumetReg;
        instrumentationRegister8.mMMNoAction += ir.mMMNoAction;
        InstrumentationRegister instrumentationRegister9 = mInstrumetReg;
        instrumentationRegister9.mIncomingNoBreath += ir.mIncomingNoBreath;
        InstrumentationRegister instrumentationRegister10 = mInstrumetReg;
        instrumentationRegister10.mTotalBreathsStage1 += ir.mTotalBreathsStage1;
        InstrumentationRegister instrumentationRegister11 = mInstrumetReg;
        instrumentationRegister11.mTotalBreathsStage2 += ir.mTotalBreathsStage2;
        InstrumentationRegister instrumentationRegister12 = mInstrumetReg;
        instrumentationRegister12.mTotalBreathsStage3 += ir.mTotalBreathsStage3;
        InstrumentationRegister instrumentationRegister13 = mInstrumetReg;
        instrumentationRegister13.mTotalBreathsStage4 += ir.mTotalBreathsStage4;
        InstrumentationRegister instrumentationRegister14 = mInstrumetReg;
        instrumentationRegister14.mTouchValid += ir.mTouchValid;
        InstrumentationRegister instrumentationRegister15 = mInstrumetReg;
        instrumentationRegister15.mTouchInvalid += ir.mTouchInvalid;
        InstrumentationRegister instrumentationRegister16 = mInstrumetReg;
        instrumentationRegister16.mTouchInvalidRelease += ir.mTouchInvalidRelease;
        InstrumentationRegister instrumentationRegister17 = mInstrumetReg;
        instrumentationRegister17.mTotalBreathsAls1 += ir.mTotalBreathsAls1;
        InstrumentationRegister instrumentationRegister18 = mInstrumetReg;
        instrumentationRegister18.mTotalBreathsAls2 += ir.mTotalBreathsAls2;
        InstrumentationRegister instrumentationRegister19 = mInstrumetReg;
        instrumentationRegister19.mTotalBreathsAls3 += ir.mTotalBreathsAls3;
        InstrumentationRegister instrumentationRegister20 = mInstrumetReg;
        instrumentationRegister20.mTotalBreathsAls4 += ir.mTotalBreathsAls4;
        InstrumentationRegister instrumentationRegister21 = mInstrumetReg;
        instrumentationRegister21.mTotalBreathsAls5 += ir.mTotalBreathsAls5;
        InstrumentationRegister instrumentationRegister22 = mInstrumetReg;
        instrumentationRegister22.mTotalBreathsAls6 += ir.mTotalBreathsAls6;
        InstrumentationRegister instrumentationRegister23 = mInstrumetReg;
        instrumentationRegister23.mTotalBreathsAls7 += ir.mTotalBreathsAls7;
        InstrumentationRegister instrumentationRegister24 = mInstrumetReg;
        instrumentationRegister24.mTotalBreathsAls8 += ir.mTotalBreathsAls8;
        InstrumentationRegister instrumentationRegister25 = mInstrumetReg;
        instrumentationRegister25.mTotalBreathsAls9 += ir.mTotalBreathsAls9;
        InstrumentationRegister instrumentationRegister26 = mInstrumetReg;
        instrumentationRegister26.mTotalBreathsAls10 += ir.mTotalBreathsAls10;
        InstrumentationRegister instrumentationRegister27 = mInstrumetReg;
        instrumentationRegister27.mUnused1 += ir.mUnused1;
        InstrumentationRegister instrumentationRegister28 = mInstrumetReg;
        instrumentationRegister28.mHardShake += ir.mHardShake;
        InstrumentationRegister instrumentationRegister29 = mInstrumetReg;
        instrumentationRegister29.mHardShakeCooldownBlocked += ir.mHardShakeCooldownBlocked;
        InstrumentationRegister instrumentationRegister30 = mInstrumetReg;
        instrumentationRegister30.mFlatupExit += ir.mFlatupExit;
        InstrumentationRegister instrumentationRegister31 = mInstrumetReg;
        instrumentationRegister31.mFlatupExitCooldownBlocked += ir.mFlatupExitCooldownBlocked;
        InstrumentationRegister instrumentationRegister32 = mInstrumetReg;
        instrumentationRegister32.mTotalTimeOn += ir.mTotalTimeOn;
        InstrumentationRegister instrumentationRegister33 = mInstrumetReg;
        instrumentationRegister33.mUnused7 += ir.mUnused7;
        InstrumentationRegister instrumentationRegister34 = mInstrumetReg;
        instrumentationRegister34.mTotalTimePeeking += ir.mTotalTimePeeking;
        InstrumentationRegister instrumentationRegister35 = mInstrumetReg;
        instrumentationRegister35.mLateTouchCount += ir.mLateTouchCount;
        InstrumentationRegister instrumentationRegister36 = mInstrumetReg;
        instrumentationRegister36.mUnused11 += ir.mUnused11;
        InstrumentationRegister instrumentationRegister37 = mInstrumetReg;
        instrumentationRegister37.mUnused12 += ir.mUnused12;
        InstrumentationRegister instrumentationRegister38 = mInstrumetReg;
        instrumentationRegister38.mUnused13 += ir.mUnused13;
        InstrumentationRegister instrumentationRegister39 = mInstrumetReg;
        instrumentationRegister39.mUnused14 += ir.mUnused14;
        InstrumentationRegister instrumentationRegister40 = mInstrumetReg;
        instrumentationRegister40.mUnused15 += ir.mUnused15;
        InstrumentationRegister instrumentationRegister41 = mInstrumetReg;
        instrumentationRegister41.mUnused16 += ir.mUnused16;
        InstrumentationRegister instrumentationRegister42 = mInstrumetReg;
        instrumentationRegister42.mUnused17 += ir.mUnused17;
        InstrumentationRegister instrumentationRegister43 = mInstrumetReg;
        instrumentationRegister43.mBreathMovementNormal += ir.mBreathMovementNormal;
        InstrumentationRegister instrumentationRegister44 = mInstrumetReg;
        instrumentationRegister44.mBreathMovementShake += ir.mBreathMovementShake;
        InstrumentationRegister instrumentationRegister45 = mInstrumetReg;
        instrumentationRegister45.mBreathStowedExit += ir.mBreathStowedExit;
        InstrumentationRegister instrumentationRegister46 = mInstrumetReg;
        instrumentationRegister46.mBreathFlatDownExit += ir.mBreathFlatDownExit;
        InstrumentationRegister instrumentationRegister47 = mInstrumetReg;
        instrumentationRegister47.mBreathFlatUpExit += ir.mBreathFlatUpExit;
        InstrumentationRegister instrumentationRegister48 = mInstrumetReg;
        instrumentationRegister48.mBreathMagicSwipe += ir.mBreathMagicSwipe;
        InstrumentationRegister instrumentationRegister49 = mInstrumetReg;
        instrumentationRegister49.mBreathNoGesture += ir.mBreathNoGesture;
        InstrumentationRegister instrumentationRegister50 = mInstrumetReg;
        instrumentationRegister50.mBreathTouchMovementNormal += ir.mBreathTouchMovementNormal;
        InstrumentationRegister instrumentationRegister51 = mInstrumetReg;
        instrumentationRegister51.mBreathTouchMovementShake += ir.mBreathTouchMovementShake;
        InstrumentationRegister instrumentationRegister52 = mInstrumetReg;
        instrumentationRegister52.mBreathTouchStowedExit += ir.mBreathTouchStowedExit;
        InstrumentationRegister instrumentationRegister53 = mInstrumetReg;
        instrumentationRegister53.mBreathTouchFlatDownExit += ir.mBreathTouchFlatDownExit;
        InstrumentationRegister instrumentationRegister54 = mInstrumetReg;
        instrumentationRegister54.mBreathTouchFlatUpExit += ir.mBreathTouchFlatUpExit;
        InstrumentationRegister instrumentationRegister55 = mInstrumetReg;
        instrumentationRegister55.mBreathTouchMagicSwipe += ir.mBreathTouchMagicSwipe;
        InstrumentationRegister instrumentationRegister56 = mInstrumetReg;
        instrumentationRegister56.mBreathTouchNoGesture += ir.mBreathTouchNoGesture;
        mTotalCurrentBreathsStage1to4 = ir.mTotalBreathsStage1 + ir.mTotalBreathsStage2 + ir.mTotalBreathsStage3 + ir.mTotalBreathsStage4;
        mCurrentStowedExit = ir.mStowedExit;
        mCurrentFaceDownExit = ir.mFacedownExit;
        mCurrentMMotion = ir.mMMAction;
    }

    public static void recordDrawTime(long drawTime) {
        if (drawTime > mDrawMaxTime) {
            mDrawMaxTime = drawTime;
        }
        mDrawTotalTime += drawTime;
        mDrawTotalCount++;
    }

    public static void recordIncomingTime(long incomingTime) {
        if (incomingTime > mIncomingMaxTime) {
            mIncomingMaxTime = incomingTime;
        }
        mIncomingTotalTime += incomingTime;
        mIncomingTotalCount++;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        int ret = super.onStartCommand(intent, flags, startId);
        doCheckin_ENDPOINT_ACTIONS(getApplicationContext());
        doCheckin_STOWED_TOTALS(getApplicationContext());
        doCheckin_FACE_DOWN_TOTALS(getApplicationContext());
        doCheckin_MMOTION_TOTALS(getApplicationContext());
        doCheckin_INCOMING(getApplicationContext());
        doCheckin_TOTAL_BREATHS(getApplicationContext());
        doCheckin_TOTAL_BREATHS_ALS(getApplicationContext());
        doCheckin_GESTURE_ALS(getApplicationContext());
        doCheckin_INCOMING_ALS(getApplicationContext());
        doCheckin_TOUCH(getApplicationContext());
        doCheckin_NOTIFICATIONS(getApplicationContext());
        clearCheckInState();
        stopSelf();
        return ret;
    }

    private static void clearCheckInState() {
        mMuteCount = 0;
        mLaunchCount = 0;
        mUnlockCount = 0;
        mPowerKeyCount = 0;
        mIncomingALS = 0;
        mIncomingTime = 0;
        mIncomingRefresh = false;
        mHandoverTime = 0;
        mHandbackTime = 0;
        mBlockBreaths = 0;
        mIncomingMaxTime = 0;
        mIncomingTotalTime = 0;
        mIncomingTotalCount = 0;
        mDrawMaxTime = 0;
        mDrawTotalTime = 0;
        mDrawTotalCount = 0;
        mInstrumetReg = null;
        mInstrumetReg = new InstrumentationRegister();
        mNotifications = 0;
        mMinedNotifications = 0;
        mOngoingNotifications = 0;
        mLowPriNotifications = 0;
        mCustomNotifications = 0;
        mMaxActiveNotifications = 0;
    }

    private void doCheckin_ENDPOINT_ACTIONS(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        String name = CHECKIN_ENDPOINT_NAME;
        int launch_count = mLaunchCount;
        CheckinEventWrapper event = new CheckinEventWrapper(CHECKIN_TAG_SN_STAT, EVENT_ENDPOINT_ACTIONS, CHECKIN_VERSION);
        event.setValue("name", name);
        event.setValue(KEY_CLEAR_COUNT, 0);
        event.setValue(KEY_LAUNCH_COUNT, launch_count);
        event.setValue(KEY_ACTION_COUNT, 0);
        event.setValue(KEY_SMS_COUNT, 0);
        event.publish(contentResolver);
    }

    public static void recordHandover(long time) {
        mHandoverTime = time;
    }

    public static void recordHandback(long time) {
        mHandbackTime = time;
    }

    public static void isClockOnlyBreathing(boolean clockonly) {
        mClockOnlyBreathing = clockonly;
    }

    private static void readConfigParameter(Context context) {
        Resources res = context.getResources();
        mIncomingBreathTime = (((res.getInteger(R.integer.ramp_up_stage_sum) + res.getInteger(R.integer.ramp_down_stage_sum)) + (res.getInteger(R.integer.incoming_plateau) * 1000)) + (res.getInteger(R.integer.late_touch_timer) * 125)) / 1000;
        mNormalBreathTime = (((res.getInteger(R.integer.ramp_up_stage_sum) + res.getInteger(R.integer.ramp_down_stage_sum)) + (res.getInteger(R.integer.plateau_time) * 1000)) + (res.getInteger(R.integer.late_touch_timer) * 125)) / 1000;
    }

    public static void recordNotification() {
        mNotifications++;
    }

    public static void recordMinedNotification() {
        mMinedNotifications++;
    }

    public static void recordOngoingNotification() {
        mOngoingNotifications++;
    }

    public static void recordLowPriNotification() {
        mLowPriNotifications++;
    }

    public static void recordCustomNotifications() {
        mCustomNotifications++;
    }

    public static void recordMaxActiveNotifications(int maxActive) {
        if (mMaxActiveNotifications < ((long) maxActive)) {
            mMaxActiveNotifications = (long) maxActive;
        }
    }

    public static void calculateTimeBlock(Context context) {
        int tBreaths;
        if (mHandbackTime != 0 && mHandoverTime != 0) {
            if (mIncomingBreathTime == 0) {
                readConfigParameter(context);
            }
            int tMSP = (int) ((mHandbackTime - mHandoverTime) / 1000);
            int deviceFlipCount = mCurrentStowedExit > mCurrentFaceDownExit ? mCurrentStowedExit : mCurrentFaceDownExit;
            int deviceMovementCount = deviceFlipCount > mCurrentMMotion ? deviceFlipCount : mCurrentMMotion;
            int totalBreathCount = deviceMovementCount > mTotalCurrentBreathsStage1to4 ? deviceMovementCount : mTotalCurrentBreathsStage1to4;
            if (mClockOnlyBreathing) {
                tBreaths = totalBreathCount * mNormalBreathTime;
            } else {
                tBreaths = mIncomingBreathTime;
                if (totalBreathCount != 0) {
                    tBreaths += (totalBreathCount - 1) * mNormalBreathTime;
                }
            }
            mBlockBreaths += tMSP - tBreaths;
        }
    }

    private int getStowedBlocked() {
        if (mInstrumetReg.mStowedExit + mInstrumetReg.mFacedownExit == 0) {
            return 0;
        }
        return (mInstrumetReg.mStowedExit / (mInstrumetReg.mStowedExit + mInstrumetReg.mFacedownExit)) * mBlockBreaths;
    }

    private void doCheckin_STOWED_TOTALS(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        int exit_count = mInstrumetReg.mStowedExit;
        int blocked_count = getStowedBlocked();
        CheckinEventWrapper event = new CheckinEventWrapper(CHECKIN_TAG_SN_AOD_STAT, EVENT_STOWED_TOTALS, CHECKIN_VERSION);
        event.setValue(KEY_EXIT_COUNT, exit_count);
        event.setValue(KEY_BLOCKED_COUNT, blocked_count);
        event.publish(contentResolver);
    }

    private void doCheckin_FACE_DOWN_TOTALS(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        int exit_count = mInstrumetReg.mFacedownExit;
        int blocked_count = mBlockBreaths - getStowedBlocked();
        CheckinEventWrapper event = new CheckinEventWrapper(CHECKIN_TAG_SN_AOD_STAT, EVENT_FACE_DOWN_TOTALS, CHECKIN_VERSION);
        event.setValue(KEY_EXIT_COUNT, exit_count);
        event.setValue(KEY_BLOCKED_COUNT, blocked_count);
        event.publish(contentResolver);
    }

    private void doCheckin_MMOTION_TOTALS(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        int mm_detect = mInstrumetReg.mMMAction;
        int mm_invalid = mInstrumetReg.mMMNoAction;
        CheckinEventWrapper event = new CheckinEventWrapper(CHECKIN_TAG_SN_AOD_STAT, EVENT_MMOTION_TOTALS, CHECKIN_VERSION);
        event.setValue(KEY_MM_DETECT, mm_detect);
        event.setValue(KEY_MM_INVALID, mm_invalid);
        event.publish(contentResolver);
    }

    private void doCheckin_INCOMING(Context context) {
        int incoming_avg_time;
        int draw_avg_time;
        ContentResolver contentResolver = context.getContentResolver();
        int incoming_no_breath = mInstrumetReg.mIncomingNoBreath;
        if (mIncomingTotalCount != 0) {
            incoming_avg_time = (int) (mIncomingTotalTime / mIncomingTotalCount);
        } else {
            incoming_avg_time = 0;
        }
        int incoming_max_time = (int) mIncomingMaxTime;
        if (mDrawTotalCount != 0) {
            draw_avg_time = (int) (mDrawTotalTime / mDrawTotalCount);
        } else {
            draw_avg_time = 0;
        }
        int draw_max_time = (int) mDrawMaxTime;
        CheckinEventWrapper event = new CheckinEventWrapper(CHECKIN_TAG_SN_AOD_STAT, EVENT_INCOMING, CHECKIN_VERSION);
        event.setValue(KEY_INCOMING_NO_BREATH, incoming_no_breath);
        event.setValue(KEY_INCOMING_AVG_TIME, incoming_avg_time);
        event.setValue(KEY_INCOMING_MAX_TIME, incoming_max_time);
        event.setValue(KEY_DRAW_AVG_TIME, draw_avg_time);
        event.setValue(KEY_DRAW_MAX_TIME, draw_max_time);
        event.publish(contentResolver);
    }

    private void doCheckin_TOTAL_BREATHS(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        int stage_0 = mInstrumetReg.mTotalBreathsStage1;
        int stage_1 = mInstrumetReg.mTotalBreathsStage2;
        int stage_2 = mInstrumetReg.mTotalBreathsStage3;
        int stage_3 = mInstrumetReg.mTotalBreathsStage4;
        CheckinEventWrapper event = new CheckinEventWrapper(CHECKIN_TAG_SN_AOD_STAT, EVENT_TOTAL_BREATHS, CHECKIN_VERSION);
        event.setValue(KEY_STAGE_0, stage_0);
        event.setValue(KEY_STAGE_1, stage_1);
        event.setValue(KEY_STAGE_2, stage_2);
        event.setValue(KEY_STAGE_3, stage_3);
        event.publish(contentResolver);
    }

    private void doCheckin_TOTAL_BREATHS_ALS(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        int als_0 = mInstrumetReg.mTotalBreathsAls1;
        int als_1 = mInstrumetReg.mTotalBreathsAls2;
        int als_2 = mInstrumetReg.mTotalBreathsAls3;
        int als_3 = mInstrumetReg.mTotalBreathsAls4;
        int als_4 = mInstrumetReg.mTotalBreathsAls5;
        int als_5 = mInstrumetReg.mTotalBreathsAls6;
        int als_6 = mInstrumetReg.mTotalBreathsAls7;
        CheckinEventWrapper event = new CheckinEventWrapper(CHECKIN_TAG_SN_AOD_STAT, EVENT_TOTAL_BREATHS_ALS, CHECKIN_VERSION);
        event.setValue(KEY_ALS_0, als_0);
        event.setValue(KEY_ALS_1, als_1);
        event.setValue(KEY_ALS_2, als_2);
        event.setValue(KEY_ALS_3, als_3);
        event.setValue(KEY_ALS_4, als_4);
        event.setValue(KEY_ALS_5, als_5);
        event.setValue(KEY_ALS_6, als_6);
        event.publish(contentResolver);
    }

    private void doCheckin_GESTURE_ALS(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        int deviceFlipCount = mInstrumetReg.mStowedExit > mInstrumetReg.mFacedownExit ? mInstrumetReg.mStowedExit : mInstrumetReg.mFacedownExit;
        int als_0 = deviceFlipCount > mInstrumetReg.mMMAction ? deviceFlipCount : mInstrumetReg.mMMAction;
        int hard_shake = mInstrumetReg.mHardShake;
        int hard_shake_cd_blocked = mInstrumetReg.mHardShakeCooldownBlocked;
        int flatup_exit = mInstrumetReg.mFlatupExit;
        int flatup_exit_cd_blocked = mInstrumetReg.mFlatupExitCooldownBlocked;
        int total_time_on = mInstrumetReg.mTotalTimeOn;
        int als_6 = mInstrumetReg.mUnused7;
        int breath_mvmt_normal = mInstrumetReg.mBreathMovementNormal;
        int breath_mvmt_shake = mInstrumetReg.mBreathMovementShake;
        int breath_stowed_exit = mInstrumetReg.mBreathStowedExit;
        int breath_flat_down_exit = mInstrumetReg.mBreathFlatDownExit;
        int breath_flat_up_exit = mInstrumetReg.mBreathFlatUpExit;
        int breath_magic_swipe = mInstrumetReg.mBreathMagicSwipe;
        int breath_no_gesture = mInstrumetReg.mBreathNoGesture;
        int breath_touch_mvmt_normal = mInstrumetReg.mBreathTouchMovementNormal;
        int breath_touch_mvmt_shake = mInstrumetReg.mBreathTouchMovementShake;
        int breath_touch_stowed_exit = mInstrumetReg.mBreathTouchStowedExit;
        int breath_touch_flat_down_exit = mInstrumetReg.mBreathTouchFlatDownExit;
        int breath_touch_flat_up_exit = mInstrumetReg.mBreathTouchFlatUpExit;
        int breath_touch_magic_swipe = mInstrumetReg.mBreathTouchMagicSwipe;
        int breath_touch_no_gesture = mInstrumetReg.mBreathTouchNoGesture;
        CheckinEventWrapper checkinEventWrapper = new CheckinEventWrapper(CHECKIN_TAG_SN_AOD_STAT, EVENT_GESTURE_ALS, CHECKIN_GESTURE_ALS_VERSION);
        checkinEventWrapper.setValue(KEY_ALS_0, als_0);
        checkinEventWrapper.setValue(KEY_HS, hard_shake);
        checkinEventWrapper.setValue(KEY_HS_CD_BLOCKED, hard_shake_cd_blocked);
        checkinEventWrapper.setValue(KEY_FU_EXIT, flatup_exit);
        checkinEventWrapper.setValue(KEY_FU_EXIT_CD_BLOCKED, flatup_exit_cd_blocked);
        checkinEventWrapper.setValue(KEY_TOTAL_TIME_ON, total_time_on);
        checkinEventWrapper.setValue(KEY_ALS_6, als_6);
        checkinEventWrapper.setValue(KEY_BREATH_MVMT_NORMAL, breath_mvmt_normal);
        checkinEventWrapper.setValue(KEY_BREATH_MVMT_SHAKE, breath_mvmt_shake);
        checkinEventWrapper.setValue(KEY_BREATH_STOWED_EXIT, breath_stowed_exit);
        checkinEventWrapper.setValue(KEY_BREATH_FLAT_DOWN_EXIT, breath_flat_down_exit);
        checkinEventWrapper.setValue(KEY_BREATH_FLAT_UP_EXIT, breath_flat_up_exit);
        checkinEventWrapper.setValue(KEY_BREATH_MAGIC_SWIPE, breath_magic_swipe);
        checkinEventWrapper.setValue(KEY_BREATH_NO_GESTURE, breath_no_gesture);
        checkinEventWrapper.setValue(KEY_BREATH_INTERACTION_TOUCH_MVMT_NORMAL, breath_touch_mvmt_normal);
        checkinEventWrapper.setValue(KEY_BREATH_INTERACTION_TOUCH_MVMT_SHAKE, breath_touch_mvmt_shake);
        checkinEventWrapper.setValue(KEY_BREATH_INTERACTION_TOUCH_STOWED_EXIT, breath_touch_stowed_exit);
        checkinEventWrapper.setValue(KEY_BREATH_INTERACTION_TOUCH_FLAT_DOWN_EXIT, breath_touch_flat_down_exit);
        checkinEventWrapper.setValue(KEY_BREATH_INTERACTION_TOUCH_FLAT_UP_EXIT, breath_touch_flat_up_exit);
        checkinEventWrapper.setValue(KEY_BREATH_INTERACTION_TOUCH_MAGIC_SWIPE, breath_touch_magic_swipe);
        checkinEventWrapper.setValue(KEY_BREATH_INTERACTION_TOUCH_NO_GESTURE, breath_touch_no_gesture);
        checkinEventWrapper.publish(contentResolver);
    }

    private void doCheckin_INCOMING_ALS(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        int als_0 = mIncomingALS;
        int als_1 = mInstrumetReg.mUnused12;
        int als_2 = mInstrumetReg.mUnused13;
        int als_3 = mInstrumetReg.mUnused14;
        int als_4 = mInstrumetReg.mUnused15;
        int als_5 = mInstrumetReg.mUnused16;
        int als_6 = mInstrumetReg.mUnused17;
        CheckinEventWrapper event = new CheckinEventWrapper(CHECKIN_TAG_SN_AOD_STAT, EVENT_INCOMING_ALS, CHECKIN_VERSION);
        event.setValue(KEY_ALS_0, als_0);
        event.setValue(KEY_ALS_1, als_1);
        event.setValue(KEY_ALS_2, als_2);
        event.setValue(KEY_ALS_3, als_3);
        event.setValue(KEY_ALS_4, als_4);
        event.setValue(KEY_ALS_5, als_5);
        event.setValue(KEY_ALS_6, als_6);
        event.publish(contentResolver);
    }

    private void doCheckin_TOUCH(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        int total_peek = mInstrumetReg.mTotalTimePeeking;
        int late_count = mInstrumetReg.mLateTouchCount;
        int valid_count = mInstrumetReg.mTouchValid;
        int invalid_count = mInstrumetReg.mTouchInvalid;
        int invalid_release = mInstrumetReg.mTouchInvalidRelease;
        int mute_count = mMuteCount;
        int intent_launch = mLaunchCount;
        int other_launch = mUnlockCount;
        int power_key = mPowerKeyCount;
        CheckinEventWrapper event = new CheckinEventWrapper(CHECKIN_TAG_SN_AOD_STAT, EVENT_TOUCH, CHECKIN_VERSION);
        event.setValue(KEY_TOTAL_PEEK, total_peek);
        event.setValue(KEY_LATE_COUNT, late_count);
        event.setValue(KEY_VALID_COUNT, valid_count);
        event.setValue(KEY_INVALID_COUNT, invalid_count);
        event.setValue(KEY_INVALID_RELEASE, invalid_release);
        event.setValue(KEY_MUTE_COUNT, mute_count);
        event.setValue(KEY_INTENT_LAUNCH, intent_launch);
        event.setValue(KEY_OTHER_LAUNCH, other_launch);
        event.setValue(KEY_POWER_KEY, power_key);
        event.publish(contentResolver);
    }

    private void doCheckin_NOTIFICATIONS(Context context) {
        CheckinEventWrapper event = new CheckinEventWrapper(CHECKIN_TAG_SN_STAT, EVENT_NOTIFICATIONS, CHECKIN_VERSION);
        event.setValue("name", CHECKIN_ENDPOINT_NAME);
        event.setValue(KEY_NOTIFICATIONS, mNotifications);
        event.setValue(KEY_MINED, mMinedNotifications);
        event.setValue(KEY_ONGOING, mOngoingNotifications);
        event.setValue(KEY_LOW_PRIORITY, mLowPriNotifications);
        event.setValue(KEY_CONTACT_MATCH, 0);
        event.setValue(KEY_MAX_MINED, mMaxActiveNotifications);
        event.setValue(KEY_MAX_MINED_PER_PKG, 0);
        event.setValue(KEY_TOTAL_SMS, 0);
        event.setValue(KEY_CUSTOM_STYLE, mCustomNotifications);
        event.publish(context.getContentResolver());
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public static void recordStowedExit() {
        InstrumentationRegister instrumentationRegister = mInstrumetReg;
        instrumentationRegister.mStowedExit++;
    }

    public static void recordFacedownExit() {
        InstrumentationRegister instrumentationRegister = mInstrumetReg;
        instrumentationRegister.mFacedownExit++;
    }

    public static void recordFlatupExit() {
        InstrumentationRegister instrumentationRegister = mInstrumetReg;
        instrumentationRegister.mFlatupExit++;
    }

    public static void recordMMDetect() {
        InstrumentationRegister instrumentationRegister = mInstrumetReg;
        instrumentationRegister.mMMAction++;
    }

    public static void recordMMInvalid() {
        InstrumentationRegister instrumentationRegister = mInstrumetReg;
        instrumentationRegister.mMMNoAction++;
    }

    public static void recordIncomingNoBreath() {
        InstrumentationRegister instrumentationRegister = mInstrumetReg;
        instrumentationRegister.mIncomingNoBreath++;
    }

    public static void recordTotalBreaths() {
        InstrumentationRegister instrumentationRegister = mInstrumetReg;
        instrumentationRegister.mTotalBreathsStage1++;
    }

    public static void recordTotalBreathsAls() {
        InstrumentationRegister instrumentationRegister = mInstrumetReg;
        instrumentationRegister.mTotalBreathsAls1++;
    }

    public static void recordIncomingAls() {
        mIncomingALS++;
    }

    public static void recordPeekingTime(long peekingTime) {
        InstrumentationRegister instrumentationRegister = mInstrumetReg;
        instrumentationRegister.mTotalTimePeeking = (int) (((long) instrumentationRegister.mTotalTimePeeking) + peekingTime);
    }

    public static void recordTouchValid() {
        InstrumentationRegister instrumentationRegister = mInstrumetReg;
        instrumentationRegister.mTouchValid++;
    }

    public static void recordTouchInvalid() {
        InstrumentationRegister instrumentationRegister = mInstrumetReg;
        instrumentationRegister.mTouchInvalid++;
    }

    public static void recordLateTouchCount() {
        InstrumentationRegister instrumentationRegister = mInstrumetReg;
        instrumentationRegister.mLateTouchCount++;
    }

    public static void recordTouchInvalidRelease() {
        InstrumentationRegister instrumentationRegister = mInstrumetReg;
        instrumentationRegister.mTouchInvalidRelease++;
    }

    public static void recordMuteCount() {
        mMuteCount++;
    }

    public static void recordLaunchCount() {
        mLaunchCount++;
    }

    public static void recordUnlockCount() {
        mUnlockCount++;
    }

    public static void recordPowerKeyCount() {
        mPowerKeyCount++;
    }
}
