package com.motorola.aon.env.android.view;

import android.util.Log;
import java.lang.reflect.Method;

public class Display {
    private static final String CLASS_DISPLAY = "android.view.Display";
    private static final String METHOD_GET_DISPLAY_INFO = "getDisplayInfo";
    private static final String TAG = "Aon_Display";
    public static boolean sInitialized;
    private static Method sMethodGetDisplayInfo;

    public static DisplayInfo getDisplayInfo(Object objDisplay) {
        if (sMethodGetDisplayInfo != null) {
            Object objDisplayInfo = DisplayInfo.createDisplayInfo();
            if (objDisplayInfo != null) {
                try {
                    Boolean bool = (Boolean) sMethodGetDisplayInfo.invoke(objDisplay, new Object[]{objDisplayInfo});
                    return DisplayInfo.from(objDisplayInfo);
                } catch (Throwable th) {
                    Log.w(TAG, "unable to invoke get display info");
                }
            }
        }
        return null;
    }

    static {
        sInitialized = false;
        try {
            Class<?> clsDisplay = Class.forName(CLASS_DISPLAY);
            Class<?> clsDisplayInfo = Class.forName(DisplayInfo.CLASS_DISPLAY_INFO);
            sMethodGetDisplayInfo = clsDisplay.getDeclaredMethod(METHOD_GET_DISPLAY_INFO, new Class[]{clsDisplayInfo});
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize class");
        }
    }
}
