package com.motorola.aon.env.android.view;

import android.util.Log;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class DisplayInfo {
    public static final String CLASS_DISPLAY_INFO = "android.view.DisplayInfo";
    private static final String FIELD_LOGICAL_HEIGHT = "logicalHeight";
    private static final String FIELD_LOGICAL_WIDTH = "logicalWidth";
    private static final String FIELD_ROTATION = "rotation";
    private static final String TAG = "Aon_DisplayInfo";
    private static Constructor<?> sConstructor;
    private static Field sFldLogicalHeight;
    private static Field sFldLogicalWidth;
    private static Field sFldRotation;
    public static boolean sInitialized;
    public int logicalHeight;
    public int logicalWidth;
    public int rotation;

    private DisplayInfo() {
    }

    public static DisplayInfo from(Object objDisplayInfo) {
        if (sInitialized) {
            try {
                DisplayInfo di = new DisplayInfo();
                di.logicalHeight = sFldLogicalHeight.getInt(objDisplayInfo);
                di.logicalWidth = sFldLogicalWidth.getInt(objDisplayInfo);
                di.rotation = sFldRotation.getInt(objDisplayInfo);
                return di;
            } catch (Throwable th) {
                Log.w(TAG, "unable to retrieve fields from display info");
            }
        }
        return null;
    }

    static Object createDisplayInfo() {
        try {
            return sConstructor.newInstance(new Object[0]);
        } catch (Throwable th) {
            return null;
        }
    }

    static {
        sInitialized = false;
        try {
            Class<?> clazz = Class.forName(CLASS_DISPLAY_INFO);
            sFldLogicalHeight = clazz.getDeclaredField(FIELD_LOGICAL_HEIGHT);
            sFldLogicalWidth = clazz.getDeclaredField(FIELD_LOGICAL_WIDTH);
            sFldRotation = clazz.getDeclaredField(FIELD_ROTATION);
            sConstructor = clazz.getDeclaredConstructor(new Class[0]);
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize class");
        }
    }
}
