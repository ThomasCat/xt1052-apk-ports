package com.motorola.aon.settings;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.AsyncTaskLoader;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy.Builder;
import android.os.StrictMode.VmPolicy;
import android.support.v4.content.IntentCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.motorola.aon.Constants;
import com.motorola.aon.R;
import com.motorola.aon.env.EnvChecker;
import com.motorola.aon.env.com.motorola.android.provider.MotorolaSettings;
import java.io.File;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class WhiteListActivity extends Activity {
    public static final Comparator<AppEntry> ALPHA_COMPARATOR = new Comparator<AppEntry>() {
        private final Collator sCollator = Collator.getInstance();

        public int compare(AppEntry object1, AppEntry object2) {
            return this.sCollator.compare(object1.getLabel(), object2.getLabel());
        }
    };
    static final int APP_INDEX_BASE = 1;
    private static final boolean DEBUG = true;
    static final String TAG = "Aon_WhiteListActivity";
    static final HashSet<String> mDefaultWhiteListSystemApps = new HashSet<>();
    static final HashSet<String> mExplicitExcludedApps = new HashSet<>();
    static final HashSet<String> mHiddenWhiteListApps = new HashSet<>();
    static final ArrayList<String> mLockedApps = new ArrayList<>();
    static final HashSet<String> mLockedSystemApps = new HashSet<>();
    static final ArrayList<String> mPredefinedApps = new ArrayList<>();
    static boolean mPredefinedListLoaded;
    AppListFragment mAppListFragment;

    public static class AppEntry {
        private final File mApkFile;
        private final Context mContext;
        private Drawable mIcon;
        /* access modifiers changed from: private */
        public final ApplicationInfo mInfo;
        /* access modifiers changed from: private */
        public String mLabel;
        private boolean mLocked = false;
        private boolean mMounted;
        private final PackageManager mPm;
        private String mSubLabel;

        public AppEntry(Context context, PackageManager pm, ApplicationInfo info) {
            this.mContext = context;
            this.mPm = pm;
            this.mInfo = info;
            this.mApkFile = new File(info.sourceDir);
        }

        public ApplicationInfo getApplicationInfo() {
            return this.mInfo;
        }

        public String getSubLabel() {
            return this.mSubLabel;
        }

        public String getLabel() {
            return this.mLabel;
        }

        public Drawable getIcon() {
            if (this.mIcon == null) {
                if (this.mApkFile.exists()) {
                    this.mIcon = this.mInfo.loadIcon(this.mPm);
                    return this.mIcon;
                }
                this.mMounted = false;
            } else if (this.mMounted) {
                return this.mIcon;
            } else {
                if (this.mApkFile.exists()) {
                    this.mMounted = WhiteListActivity.DEBUG;
                    this.mIcon = this.mInfo.loadIcon(this.mPm);
                    return this.mIcon;
                }
            }
            return this.mContext.getResources().getDrawable(17301651);
        }

        public String toString() {
            return this.mLabel;
        }

        public void loadLabel(Context context) {
            if (this.mLabel != null && this.mMounted) {
                return;
            }
            if (!this.mApkFile.exists()) {
                this.mMounted = false;
                this.mLabel = this.mInfo.packageName;
                return;
            }
            this.mMounted = WhiteListActivity.DEBUG;
            if ("com.google.android.googlequicksearchbox".equals(this.mInfo.packageName)) {
                this.mLabel = context.getResources().getString(R.string.white_list_googe_search_title);
                return;
            }
            CharSequence label = this.mInfo.loadLabel(context.getPackageManager());
            this.mLabel = label != null ? label.toString() : null;
        }

        public void setLocked(boolean lock) {
            this.mLocked = lock;
        }

        public boolean isLocked() {
            return this.mLocked;
        }
    }

    public static class AppListAdapter extends ArrayAdapter<AppEntry> {
        final ContentResolver mContentResolver;
        private final LayoutInflater mInflater;
        int mIsAutoSelect = 1;
        HashSet<String> mWhiteList = new HashSet<>();

        public AppListAdapter(Context context) {
            super(context, 17367044);
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
            this.mContentResolver = context.getContentResolver();
        }

        public void loadAutoDownloadConfig() {
            this.mIsAutoSelect = MotorolaSettings.getInt(this.mContentResolver, MotorolaSettings.SN_AUTO_SELECT_INSTALLED, this.mIsAutoSelect);
            if (this.mIsAutoSelect != 0) {
                this.mIsAutoSelect = 1;
            }
        }

        /* access modifiers changed from: 0000 */
        public final void toggleAutoSelectDownloaded(CheckBox cb) {
            this.mIsAutoSelect = 1 - this.mIsAutoSelect;
            MotorolaSettings.putInt(this.mContentResolver, MotorolaSettings.SN_AUTO_SELECT_INSTALLED, this.mIsAutoSelect);
            cb.setChecked(this.mIsAutoSelect != 0 ? WhiteListActivity.DEBUG : false);
        }

        public void toggleSelectItem(String pkg) {
            Log.d(WhiteListActivity.TAG, "toggle " + pkg);
            if (this.mWhiteList.contains(pkg)) {
                this.mWhiteList.remove(pkg);
            } else {
                this.mWhiteList.add(pkg);
            }
            storeWhiteList();
            notifyDataSetChanged();
            notifyWhiteListChange();
        }

        public void whiteListSelectAll() {
            this.mWhiteList.clear();
            int N = getCount() - 1;
            for (int i = 0; i < N; i++) {
                this.mWhiteList.add(((AppEntry) getItem(i)).mInfo.packageName);
            }
            this.mWhiteList.addAll(WhiteListActivity.mHiddenWhiteListApps);
            storeWhiteList();
            notifyDataSetChanged();
            notifyWhiteListChange();
        }

        public void whiteListClear() {
            this.mWhiteList.clear();
            this.mWhiteList.addAll(WhiteListActivity.mLockedSystemApps);
            this.mWhiteList.addAll(WhiteListActivity.mHiddenWhiteListApps);
            storeWhiteList();
            notifyDataSetChanged();
            notifyWhiteListChange();
        }

        private void notifyWhiteListChange() {
            getContext().sendBroadcast(new Intent(AonSettingsConst.WHITE_LIST_CHANAGED));
        }

        private void storeWhiteList() {
            StringBuilder whiteList = new StringBuilder();
            int N = this.mWhiteList.size();
            int idx = 0;
            Iterator i$ = this.mWhiteList.iterator();
            while (i$.hasNext()) {
                idx++;
                whiteList.append((String) i$.next());
                if (idx < N) {
                    whiteList.append(',');
                }
            }
            MotorolaSettings.putString(this.mContentResolver, MotorolaSettings.SN_WHITE_LIST, whiteList.toString());
        }

        public void setData(List<AppEntry> data) {
            clear();
            if (data != null) {
                addAll(data);
            }
        }

        /* access modifiers changed from: 0000 */
        public final void reloadWhiteList() {
            this.mWhiteList.clear();
            String whiteList = MotorolaSettings.getString(getContext().getContentResolver(), MotorolaSettings.SN_WHITE_LIST);
            if (whiteList != null) {
                String[] whiteListArray = whiteList.split(",");
                if (whiteListArray != null && whiteListArray.length > 0) {
                    for (String item : whiteListArray) {
                        this.mWhiteList.add(item);
                    }
                }
            }
            this.mWhiteList.addAll(WhiteListActivity.mHiddenWhiteListApps);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (position == 0) {
                if (convertView == null || convertView.getId() != R.id.white_list_auto_select_installed) {
                    view = this.mInflater.inflate(R.layout.white_list_auto_select_installed, parent, false);
                } else {
                    view = convertView;
                }
                final CheckBox cb = (CheckBox) view.findViewById(R.id.check);
                cb.setChecked(this.mIsAutoSelect != 0 ? WhiteListActivity.DEBUG : false);
                view.findViewById(R.id.toggle_auto_select).setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        AppListAdapter.this.toggleAutoSelectDownloaded(cb);
                    }
                });
            } else if (convertView == null || convertView.getId() != R.id.white_list_item) {
                view = this.mInflater.inflate(R.layout.list_item_icon_text, parent, false);
            } else {
                view = convertView;
            }
            if (position >= 1) {
                AppEntry item = (AppEntry) getItem(position - 1);
                ((ImageView) view.findViewById(R.id.icon)).setImageDrawable(item.getIcon());
                ((TextView) view.findViewById(R.id.text)).setText(item.getLabel());
                TextView tvSubLabel = (TextView) view.findViewById(R.id.subtext);
                String subLabel = item.getSubLabel();
                if (!TextUtils.isEmpty(subLabel)) {
                    tvSubLabel.setText(subLabel);
                    tvSubLabel.setVisibility(0);
                } else {
                    tvSubLabel.setVisibility(8);
                }
                ((CheckBox) view.findViewById(R.id.check)).setChecked(this.mWhiteList != null ? this.mWhiteList.contains(item.mInfo.packageName) : false);
                view.setAlpha(item.isLocked() ? 0.4f : 1.0f);
                view.setFocusable(item.isLocked());
            }
            return view;
        }

        public int getCount() {
            return super.getCount() + 1;
        }
    }

    public static class AppListFragment extends ListFragment implements LoaderCallbacks<List<AppEntry>> {
        AppListAdapter mAdapter;

        public void onResume() {
            super.onResume();
            this.mAdapter.loadAutoDownloadConfig();
            getLoaderManager().initLoader(0, null, this);
        }

        public void onPause() {
            super.onPause();
        }

        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            setEmptyText("No applications");
            setHasOptionsMenu(WhiteListActivity.DEBUG);
            this.mAdapter = new AppListAdapter(getActivity());
            setListAdapter(this.mAdapter);
            setListShown(false);
            ListView listView = getListView();
            listView.setItemsCanFocus(false);
            listView.setChoiceMode(2);
            int pad = getResources().getDimensionPixelSize(R.dimen.white_list_left_right_padding);
            listView.setPadding(pad, 0, pad, 0);
        }

        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            inflater.inflate(R.menu.white_list, menu);
        }

        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.select_all /*2131624010*/:
                    doSelectAll();
                    return WhiteListActivity.DEBUG;
                case R.id.select_none /*2131624011*/:
                    doSelectNone();
                    return WhiteListActivity.DEBUG;
                default:
                    return false;
            }
        }

        private void doSelectNone() {
            this.mAdapter.whiteListClear();
        }

        private void doSelectAll() {
            this.mAdapter.whiteListSelectAll();
        }

        public void onListItemClick(ListView l, View v, int position, long id) {
            Log.i(WhiteListActivity.TAG, "Item clicked: " + id);
            if (position >= 1) {
                this.mAdapter.toggleSelectItem(((AppEntry) this.mAdapter.getItem(position - 1)).mInfo.packageName);
            }
        }

        public Loader<List<AppEntry>> onCreateLoader(int id, Bundle args) {
            return new AppListLoader(getActivity());
        }

        public void onLoadFinished(Loader<List<AppEntry>> loader, List<AppEntry> data) {
            this.mAdapter.reloadWhiteList();
            this.mAdapter.setData(data);
            if (isResumed()) {
                setListShown(WhiteListActivity.DEBUG);
            } else {
                setListShownNoAnimation(WhiteListActivity.DEBUG);
            }
        }

        public void onLoaderReset(Loader<List<AppEntry>> loader) {
            this.mAdapter.setData(null);
        }
    }

    public static class AppListLoader extends AsyncTaskLoader<List<AppEntry>> {
        private List<AppEntry> mApps;
        final HashMap<String, AppEntry> mCachedPredefinedAppEntry = new HashMap<>();
        final InterestingConfigChanges mLastConfig = new InterestingConfigChanges();
        private PackageIntentReceiver mPackageObserver;

        public AppListLoader(Context context) {
            super(context);
        }

        public List<AppEntry> loadInBackground() {
            List<AppEntry> entries = WhiteListActivity.loadAppEntries(getContext());
            if (entries != null) {
                Collections.sort(entries, WhiteListActivity.ALPHA_COMPARATOR);
            }
            return entries;
        }

        public void deliverResult(List<AppEntry> apps) {
            if (isReset() && apps != null) {
                onReleaseResources(apps);
            }
            List<AppEntry> oldApps = apps;
            this.mApps = apps;
            if (isStarted()) {
                super.deliverResult(apps);
            }
            if (oldApps != null) {
                onReleaseResources(oldApps);
            }
        }

        /* access modifiers changed from: protected */
        public void onStartLoading() {
            if (this.mApps != null) {
                deliverResult(this.mApps);
            }
            if (this.mPackageObserver == null) {
                this.mPackageObserver = new PackageIntentReceiver(this);
            }
            boolean configChange = this.mLastConfig.applyNewConfig(getContext().getResources());
            if (takeContentChanged() || this.mApps == null || configChange) {
                forceLoad();
            }
        }

        /* access modifiers changed from: protected */
        public void onStopLoading() {
            cancelLoad();
        }

        public void onCanceled(List<AppEntry> apps) {
            super.onCanceled(apps);
            onReleaseResources(apps);
        }

        /* access modifiers changed from: protected */
        public void onReset() {
            super.onReset();
            onStopLoading();
            if (this.mApps != null) {
                onReleaseResources(this.mApps);
                this.mApps = null;
            }
            if (this.mPackageObserver != null) {
                getContext().unregisterReceiver(this.mPackageObserver);
                this.mPackageObserver = null;
            }
        }

        /* access modifiers changed from: protected */
        public void onReleaseResources(List<AppEntry> list) {
        }
    }

    public static class InterestingConfigChanges {
        final Configuration mLastConfiguration = new Configuration();
        int mLastDensity;

        /* access modifiers changed from: 0000 */
        public boolean applyNewConfig(Resources res) {
            boolean densityChanged;
            int configChanges = this.mLastConfiguration.updateFrom(res.getConfiguration());
            if (this.mLastDensity != res.getDisplayMetrics().densityDpi) {
                densityChanged = true;
            } else {
                densityChanged = false;
            }
            if (!densityChanged && (configChanges & 772) == 0) {
                return false;
            }
            this.mLastDensity = res.getDisplayMetrics().densityDpi;
            return WhiteListActivity.DEBUG;
        }
    }

    public static class PackageIntentReceiver extends BroadcastReceiver {
        final AppListLoader mLoader;

        public PackageIntentReceiver(AppListLoader loader) {
            this.mLoader = loader;
            this.mLoader.getContext().registerReceiver(this, new IntentFilter("com.motorola.aon.settings.REFRESH_LIST"));
            IntentFilter sdFilter = new IntentFilter();
            sdFilter.addAction(IntentCompat.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE);
            sdFilter.addAction(IntentCompat.ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE);
            this.mLoader.getContext().registerReceiver(this, sdFilter);
        }

        public void onReceive(Context context, Intent intent) {
            this.mLoader.onContentChanged();
        }
    }

    public boolean onNavigateUp() {
        finish();
        return DEBUG;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        if (AonSettingsConst.DEVELOPER_MODE) {
            StrictMode.setThreadPolicy(new Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
            StrictMode.setVmPolicy(new VmPolicy.Builder().detectLeakedSqlLiteObjects().detectLeakedClosableObjects().penaltyLog().penaltyDeath().build());
        }
        super.onCreate(savedInstanceState);
        ActionBar ab = getActionBar();
        if (ab != null) {
            ab.setIcon(EnvChecker.getActionBarIcon());
            ab.setTitle(R.string.sn_setting_manage_types_header);
            ab.setDisplayHomeAsUpEnabled(DEBUG);
        }
        FragmentManager fm = getFragmentManager();
        if (fm.findFragmentById(16908290) == null) {
            this.mAppListFragment = new AppListFragment();
            fm.beginTransaction().add(16908290, this.mAppListFragment).commit();
        }
        updateWhiteListLaunchedSharedPreference();
    }

    public static boolean isPackageVisible(Context context, String pkgName) {
        if ("com.android.phone".equals(pkgName) || "com.android.dialer".equals(pkgName)) {
            return DEBUG;
        }
        PackageManager pm = context.getPackageManager();
        Intent mainIntent = new Intent("android.intent.action.MAIN", null);
        mainIntent.addCategory("android.intent.category.LAUNCHER");
        mainIntent.setPackage(pkgName);
        List<ResolveInfo> res = pm.queryIntentActivities(mainIntent, 0);
        if (res == null || res.size() <= 0) {
            return false;
        }
        return DEBUG;
    }

    public static void loadPredefinedList(Context context) {
        String[] arr$;
        String[] arr$2;
        String[] arr$3;
        String[] arr$4;
        if (!mPredefinedListLoaded) {
            mPredefinedListLoaded = DEBUG;
            Resources res = context.getResources();
            String[] hiddenWhiteListApps = res.getStringArray(R.array.config_hidden_white_list_apps);
            if (hiddenWhiteListApps != null && hiddenWhiteListApps.length > 0) {
                for (String each : hiddenWhiteListApps) {
                    mHiddenWhiteListApps.add(each);
                    Log.d(TAG, "hidden white list app " + each);
                }
            }
            String[] lockedApp = res.getStringArray(R.array.config_white_list_locked_apps);
            if (lockedApp != null && lockedApp.length > 0) {
                for (String each2 : lockedApp) {
                    mLockedSystemApps.add(each2);
                    if (!mHiddenWhiteListApps.contains(each2)) {
                        mLockedApps.add(each2);
                        Log.d(TAG, "locked white list app " + each2);
                    }
                }
            }
            String[] defaultWhiteList = res.getStringArray(R.array.config_white_list_system_apps);
            if (defaultWhiteList != null && defaultWhiteList.length > 0) {
                for (String each3 : defaultWhiteList) {
                    mDefaultWhiteListSystemApps.add(each3);
                    if (!mHiddenWhiteListApps.contains(each3)) {
                        mPredefinedApps.add(each3);
                        Log.d(TAG, "default white list app " + each3);
                    }
                }
            }
            String[] excludedApps = res.getStringArray(R.array.config_white_list_excluded_apps);
            if (excludedApps != null && excludedApps.length > 0) {
                for (String each4 : excludedApps) {
                    mExplicitExcludedApps.add(each4);
                    Log.d(TAG, "excluded from white list app " + each4);
                }
            }
        }
    }

    public static List<AppEntry> loadAppEntries(Context context) {
        PackageManager pm = context.getPackageManager();
        List<ApplicationInfo> apps = pm.getInstalledApplications(0);
        if (apps == null) {
            return null;
        }
        loadPredefinedList(context);
        List<AppEntry> entries = new ArrayList<>(apps.size());
        for (int i = 0; i < apps.size(); i++) {
            ApplicationInfo ai = (ApplicationInfo) apps.get(i);
            if (!mExplicitExcludedApps.contains(ai.packageName) && isPackageVisible(context, ai.packageName)) {
                AppEntry entry = new AppEntry(context, pm, ai);
                entry.loadLabel(context);
                if (entry.mLabel != null) {
                    if (mLockedSystemApps.contains(ai.packageName)) {
                        entry.setLocked(DEBUG);
                    }
                    if (!mHiddenWhiteListApps.contains(ai.packageName)) {
                        entries.add(entry);
                    }
                }
            }
        }
        return entries;
    }

    private void updateWhiteListLaunchedSharedPreference() {
        Log.d(TAG, "updateWhiteListLaunchedSharedPreference()");
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.AON_PREFS_NAME, 4);
        boolean launched = sharedPreferences.getBoolean(Constants.PREF_KEY_WHITE_LIST_SETTING_LAUNCHED, false);
        Log.d(TAG, "white list launched? " + launched);
        if (!launched) {
            Editor editor = sharedPreferences.edit();
            editor.putBoolean(Constants.PREF_KEY_WHITE_LIST_SETTING_LAUNCHED, DEBUG);
            editor.apply();
            Log.d(TAG, "white list launched preference updated.");
        }
    }
}
