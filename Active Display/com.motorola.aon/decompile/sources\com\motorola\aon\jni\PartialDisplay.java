package com.motorola.aon.jni;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import com.motorola.aon.Constants;
import com.motorola.aon.R;
import java.util.Calendar;

public class PartialDisplay {
    /* access modifiers changed from: private */
    public static final boolean DEBUG = Constants.DEBUG;
    private static final String TAG = "Aon_PartialDisplay";
    private static int sFeatureAODVersion = -1;
    private static int sMSP430FwVersion = -1;

    public static class ControlRegister {
        int[] mBreathingTimes;
        int mBrightness;
        int mCenterIconX;
        int mCenterIconY;
        boolean mClockEnabled;
        boolean mClockIs24;
        int mClockX;
        int mClockY;
        int mDefaultIncomingPlateau;
        boolean mEnhancedUIEnable;
        int mEnhancedViewXOffset;
        int mFlatExitCooldown;
        int mFlatExitHysteresis;
        boolean mFlatupEnable;
        boolean mGestureBasedOnly;
        int mGesturePlateau;
        boolean mHapticsDisable;
        int mHshakeCooldown;
        int mHshakeCountIncr;
        int mHshakeCountThreshold;
        int mHshakeThresholdX;
        int mHshakeThresholdY;
        int mHshakeThresholdZ;
        int mIncomingPlateau;
        int mLateTouchTimer;
        int mLazyTouchTimer;
        int mMovementThreshold;
        int mMovementTime;
        int mMuteTouchTimer;
        int mNoMovementTime;
        boolean mNotificationDisable;
        int mNotificationIconsX;
        int mNotificationIconsY;
        int mOriginCenterIconX;
        int mOriginCenterIconY;
        int mOriginClockX;
        int mOriginClockY;
        /* access modifiers changed from: private */
        public TouchRegion mOriginRegionMiddle;
        int mPlatTime;
        int[] mRampDownStages;
        int[] mRampUpStages;
        int[] mRegions;
        int[] mRegionsClockOnly;
        int mStowedTime;
        int mTouchFadex3;
        int mTouchFadex4;
        int mTouchMutex1;
        int mTouchMutex2;
        int mTouchMutex5;
        int mTouchMutex6;
        int mTouchMutey1;
        int mTouchMutey2;
        public TouchRegion mTouchRegionDown;
        public TouchRegion mTouchRegionMiddle;
        public TouchRegion mTouchRegionUpper;
        int mTransitionDelay;

        public void setClock24(boolean is24) {
            this.mClockIs24 = is24;
        }

        public void setBrightness(int brightness) {
            this.mBrightness = brightness;
        }

        public void setIsIncoming(boolean b) {
            this.mIncomingPlateau = b ? this.mDefaultIncomingPlateau : 0;
        }

        public void setNotificationDisable(boolean nd) {
            this.mNotificationDisable = nd;
        }

        public void setBreathingViewPostion(int offX, int offY, boolean showBattery) {
            this.mClockX = this.mOriginClockX + offX;
            this.mEnhancedUIEnable = showBattery;
            if (showBattery) {
                this.mClockX -= this.mEnhancedViewXOffset;
            }
            if (this.mClockX % 4 != 0) {
                Log.d(PartialDisplay.TAG, " mClockX=" + this.mClockX + " mOrigClockX=" + this.mOriginClockX + " mEnhancedViewXOffset=" + this.mEnhancedViewXOffset + " offX=" + offX + " showBattery=" + showBattery);
                this.mClockX += 4 - (this.mClockX % 4);
                if (PartialDisplay.DEBUG) {
                    throw new RuntimeException("clock x not times of 4");
                }
            }
            this.mClockY = this.mOriginClockY + offY;
            this.mCenterIconX = this.mOriginCenterIconX + offX;
            this.mCenterIconY = this.mOriginCenterIconY + offY;
            this.mTouchRegionMiddle.offset(this.mOriginRegionMiddle, offX, offY);
            if (PartialDisplay.DEBUG) {
                Log.d(PartialDisplay.TAG, "offset " + offX + "," + offY + " CLOCK:" + this.mClockX + "," + this.mClockY);
                Log.d(PartialDisplay.TAG, "Touch Region:");
                this.mTouchRegionMiddle.dump();
            }
        }

        public void setGestureBased(boolean b) {
            this.mGestureBasedOnly = b;
        }

        public void setClockEnabled(boolean b) {
            this.mClockEnabled = b;
        }

        public void disableHaptics(boolean disable) {
            if (PartialDisplay.DEBUG) {
                Log.d(PartialDisplay.TAG, "disableHaptics : " + disable);
            }
            this.mHapticsDisable = disable;
        }
    }

    public static class InstrumentationRegister {
        public int mBreathFlatDownExit;
        public int mBreathFlatUpExit;
        public int mBreathMagicSwipe;
        public int mBreathMovementNormal;
        public int mBreathMovementShake;
        public int mBreathNoGesture;
        public int mBreathStowedExit;
        public int mBreathTouchFlatDownExit;
        public int mBreathTouchFlatUpExit;
        public int mBreathTouchMagicSwipe;
        public int mBreathTouchMovementNormal;
        public int mBreathTouchMovementShake;
        public int mBreathTouchNoGesture;
        public int mBreathTouchStowedExit;
        public int mFacedownEnter;
        public int mFacedownExit;
        public int mFlatupExit;
        public int mFlatupExitCooldownBlocked;
        public int mHardShake;
        public int mHardShakeCooldownBlocked;
        public int mIncomingNoBreath;
        public int mLateTouchCount;
        public int mMMAction;
        public int mMMNoAction;
        public int mStowedEnter;
        public int mStowedExit;
        public int mStowedTotal;
        public int mTotalBreathsAls1;
        public int mTotalBreathsAls10;
        public int mTotalBreathsAls2;
        public int mTotalBreathsAls3;
        public int mTotalBreathsAls4;
        public int mTotalBreathsAls5;
        public int mTotalBreathsAls6;
        public int mTotalBreathsAls7;
        public int mTotalBreathsAls8;
        public int mTotalBreathsAls9;
        public int mTotalBreathsStage1;
        public int mTotalBreathsStage2;
        public int mTotalBreathsStage3;
        public int mTotalBreathsStage4;
        public int mTotalTimeOn;
        public int mTotalTimePeeking;
        public int mTouchInvalid;
        public int mTouchInvalidRelease;
        public int mTouchValid;
        public int mUnstowNoAction;
        public int mUnused1;
        public int mUnused11;
        public int mUnused12;
        public int mUnused13;
        public int mUnused14;
        public int mUnused15;
        public int mUnused16;
        public int mUnused17;
        public int mUnused7;
    }

    private static class MSP430FwVer {
        public int mAODVersion;
        public int mMSP430Ver;

        private MSP430FwVer() {
        }
    }

    public static class PanelInfo {
        public int mControllerDriverVersion;
        public int mControllerVersion;
    }

    public static class StatusRegister {
        public int mActiveRegion;
        public int mBrightness;
        public int mMute;
        public int mPanelState;
        public int mPeek;
        public int mWakeupReason;
    }

    public static class TouchRegion {
        int mLeftTopX;
        int mLeftTopY;
        int mRightBottomX;
        int mRightBottomY;

        public TouchRegion(TouchRegion that) {
            this.mLeftTopX = that.mLeftTopX;
            this.mLeftTopY = that.mLeftTopY;
            this.mRightBottomX = that.mRightBottomX;
            this.mRightBottomY = that.mRightBottomY;
        }

        public void offset(TouchRegion that, int x, int y) {
            this.mLeftTopX = that.mLeftTopX + x;
            this.mLeftTopY = that.mLeftTopY + y;
            this.mRightBottomX = that.mRightBottomX + x;
            this.mRightBottomY = that.mRightBottomY + y;
        }

        public TouchRegion() {
        }

        public void dump() {
            Log.d(PartialDisplay.TAG, "region (" + this.mLeftTopX + "," + this.mRightBottomX + "," + this.mLeftTopY + "," + this.mRightBottomY + ")");
        }

        public boolean inRegion(int x, int y) {
            if (PartialDisplay.DEBUG) {
                Log.i(PartialDisplay.TAG, "(" + x + "," + y + ") (" + this.mLeftTopX + "," + this.mRightBottomX + "," + this.mLeftTopY + "," + this.mRightBottomY + ")");
            }
            return x >= this.mLeftTopX && x <= this.mRightBottomX && y >= this.mLeftTopY && y <= this.mRightBottomY;
        }
    }

    public static class TouchRegister {
        public int mPressure;
        public int mX;
        public int mY;
    }

    public static native void nativeEnableBreathing(boolean z);

    private static native void nativeGetFirmwareVersion(MSP430FwVer mSP430FwVer);

    public static native void nativeGetPanelInfo(PanelInfo panelInfo);

    public static native int nativeInit();

    public static native void nativePrepareDisplayResume(boolean z, int i, boolean z2);

    public static native void nativePrepareDisplaySuspend(boolean z, boolean z2);

    public static native int nativeReadInstrumentationRegister(InstrumentationRegister instrumentationRegister);

    public static native void nativeReadStatusRegister(StatusRegister statusRegister);

    public static native void nativeReadTouchRegister(TouchRegister touchRegister);

    public static native int nativeSetControlRegister(ControlRegister controlRegister);

    private static native void nativeSetPosixTime(long j);

    public static native int nativeUnhideFramebuffer();

    static {
        System.loadLibrary("partial_display");
    }

    public static void setCurrentTime(long tzOffset) {
        long curTimeMillis = System.currentTimeMillis();
        nativeSetPosixTime((curTimeMillis + tzOffset) / 1000);
        if (DEBUG) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(curTimeMillis);
            int h = cal.get(11);
            int m = cal.get(12);
            Log.d(TAG, "pass time to sensor hub " + h + ":" + m + ":" + cal.get(13));
        }
    }

    public static ControlRegister loadDefaultControlReg(Context context) {
        ControlRegister reg = new ControlRegister();
        Resources res = context.getResources();
        int[] regions = {res.getDimensionPixelSize(R.dimen.region0_start), res.getDimensionPixelSize(R.dimen.region0_end), res.getDimensionPixelSize(R.dimen.region1_start), res.getDimensionPixelSize(R.dimen.region1_end), res.getDimensionPixelSize(R.dimen.region2_start), res.getDimensionPixelSize(R.dimen.region2_end), res.getDimensionPixelSize(R.dimen.region3_start), res.getDimensionPixelSize(R.dimen.region3_end), res.getDimensionPixelSize(R.dimen.region4_start), res.getDimensionPixelSize(R.dimen.region4_end), res.getDimensionPixelSize(R.dimen.region5_start), res.getDimensionPixelSize(R.dimen.region5_end), res.getDimensionPixelSize(R.dimen.region6_start), res.getDimensionPixelSize(R.dimen.region6_end), res.getDimensionPixelSize(R.dimen.region7_start), res.getDimensionPixelSize(R.dimen.region7_end)};
        reg.mRegions = regions;
        for (int i = 0; i < regions.length; i++) {
            Log.d(TAG, "region " + (i / 2) + " " + regions[i]);
        }
        int[] clockonlyregions = {regions[0], regions[1], regions[0], regions[3], regions[0], regions[5], regions[0], regions[7], regions[8], res.getDimensionPixelSize(R.dimen.region_clock_only_end), regions[10], res.getDimensionPixelSize(R.dimen.region_clock_only_end), regions[12], res.getDimensionPixelSize(R.dimen.region_clock_only_end), regions[14], res.getDimensionPixelSize(R.dimen.region_clock_only_end)};
        reg.mRegionsClockOnly = clockonlyregions;
        for (int i2 = 0; i2 < clockonlyregions.length; i2++) {
            Log.d(TAG, "clock only region " + (i2 / 2) + " " + clockonlyregions[i2]);
        }
        reg.mEnhancedViewXOffset = res.getDimensionPixelSize(R.dimen.enhanced_view_clock_left_offset);
        reg.mOriginClockX = res.getDimensionPixelSize(R.dimen.clock_start_X);
        reg.mOriginClockY = res.getDimensionPixelSize(R.dimen.clock_start_Y);
        reg.mClockX = reg.mOriginClockX;
        reg.mClockY = reg.mOriginClockY;
        reg.mCenterIconX = res.getDimensionPixelSize(R.dimen.center_icon_X);
        reg.mCenterIconY = res.getDimensionPixelSize(R.dimen.center_icon_Y);
        reg.mOriginCenterIconX = reg.mCenterIconX;
        reg.mOriginCenterIconY = reg.mCenterIconY;
        reg.mFlatupEnable = res.getBoolean(R.bool.flatup_enable);
        reg.mHapticsDisable = res.getBoolean(R.bool.haptics_disable);
        reg.mFlatExitHysteresis = res.getInteger(R.integer.flatup_exit_hysteresis);
        reg.mFlatExitCooldown = res.getInteger(R.integer.flatup_exit_cooldown);
        reg.mHshakeThresholdX = res.getInteger(R.integer.hard_shake_threshold_x);
        reg.mHshakeThresholdY = res.getInteger(R.integer.hard_shake_threshold_y);
        reg.mHshakeThresholdZ = res.getInteger(R.integer.hard_shake_threshold_z);
        reg.mHshakeCountThreshold = res.getInteger(R.integer.hard_shake_count_threshold);
        reg.mHshakeCountIncr = res.getInteger(R.integer.hard_shake_count_increment);
        reg.mHshakeCooldown = res.getInteger(R.integer.hard_shake_cooldown);
        reg.mMovementTime = res.getInteger(R.integer.movement_time);
        reg.mNoMovementTime = res.getInteger(R.integer.no_movement_time);
        reg.mMovementThreshold = res.getInteger(R.integer.movement_threshold);
        reg.mStowedTime = res.getInteger(R.integer.stowed_time);
        reg.mNotificationIconsX = res.getDimensionPixelSize(R.dimen.notification_icons_X);
        reg.mNotificationIconsY = res.getDimensionPixelSize(R.dimen.notification_icons_Y);
        reg.mRampUpStages = res.getIntArray(R.array.ramp_up_stage);
        reg.mRampDownStages = res.getIntArray(R.array.ramp_down_stage);
        reg.mPlatTime = res.getInteger(R.integer.plateau_time);
        reg.mDefaultIncomingPlateau = res.getInteger(R.integer.incoming_plateau);
        reg.mTransitionDelay = res.getInteger(R.integer.transition_delay);
        reg.mGesturePlateau = res.getInteger(R.integer.gesture_plateau);
        reg.mTouchRegionUpper = new TouchRegion();
        reg.mTouchRegionUpper.mLeftTopX = res.getDimensionPixelSize(R.dimen.touch_region_upper_x0);
        reg.mTouchRegionUpper.mLeftTopY = res.getDimensionPixelSize(R.dimen.touch_region_upper_y0);
        reg.mTouchRegionUpper.mRightBottomX = res.getDimensionPixelSize(R.dimen.touch_region_upper_x1);
        reg.mTouchRegionUpper.mRightBottomY = res.getDimensionPixelSize(R.dimen.touch_region_upper_y1);
        reg.mTouchRegionMiddle = new TouchRegion();
        reg.mTouchRegionMiddle.mLeftTopX = res.getDimensionPixelSize(R.dimen.touch_region_middle_x0);
        reg.mTouchRegionMiddle.mLeftTopY = res.getDimensionPixelSize(R.dimen.touch_region_middle_y0);
        reg.mTouchRegionMiddle.mRightBottomX = res.getDimensionPixelSize(R.dimen.touch_region_middle_x1);
        reg.mTouchRegionMiddle.mRightBottomY = res.getDimensionPixelSize(R.dimen.touch_region_middle_y1);
        reg.mOriginRegionMiddle = new TouchRegion(reg.mTouchRegionMiddle);
        reg.mTouchRegionDown = new TouchRegion();
        reg.mTouchRegionDown.mLeftTopX = res.getDimensionPixelSize(R.dimen.touch_region_down_x0);
        reg.mTouchRegionDown.mLeftTopY = res.getDimensionPixelSize(R.dimen.touch_region_down_y0);
        reg.mTouchRegionDown.mRightBottomX = res.getDimensionPixelSize(R.dimen.touch_region_down_x1);
        reg.mTouchRegionDown.mRightBottomY = res.getDimensionPixelSize(R.dimen.touch_region_down_y1);
        reg.mTouchMutex1 = res.getDimensionPixelSize(R.dimen.touch_region_mute_x1);
        reg.mTouchMutex2 = res.getDimensionPixelSize(R.dimen.touch_region_mute_x2);
        reg.mTouchFadex3 = res.getDimensionPixelSize(R.dimen.touch_region_fade_x3);
        reg.mTouchFadex4 = res.getDimensionPixelSize(R.dimen.touch_region_fade_x4);
        reg.mTouchMutex5 = res.getDimensionPixelSize(R.dimen.touch_region_mute_x5);
        reg.mTouchMutex6 = res.getDimensionPixelSize(R.dimen.touch_region_mute_x6);
        reg.mTouchMutey1 = res.getDimensionPixelSize(R.dimen.touch_region_mute_y1);
        reg.mTouchMutey2 = res.getDimensionPixelSize(R.dimen.touch_region_mute_y2);
        reg.mBreathingTimes = res.getIntArray(R.array.breathing_times);
        reg.mLazyTouchTimer = res.getInteger(R.integer.lazy_touch_timer);
        reg.mLateTouchTimer = res.getInteger(R.integer.late_touch_timer);
        reg.mMuteTouchTimer = res.getInteger(R.integer.mute_touch_timer);
        return reg;
    }

    public static void loadFirmwareVersion() {
        Log.i(TAG, "loadFirmwareVersion. sMSP430FwVersion=" + sMSP430FwVersion + " sFeatureAODVersion=" + sFeatureAODVersion);
        if (-1 == sMSP430FwVersion && -1 == sFeatureAODVersion) {
            MSP430FwVer fwVer = new MSP430FwVer();
            if (DEBUG) {
                Log.v(TAG, "loadFirmwareVersion. Making call to nativeGetFirmwareVersion");
            }
            nativeGetFirmwareVersion(fwVer);
            sMSP430FwVersion = fwVer.mMSP430Ver;
            sFeatureAODVersion = fwVer.mAODVersion;
            Log.i(TAG, "loadFirmwareVersion. After Native call.  sMSP430FwVersion=" + sMSP430FwVersion + " sFeatureAODVersion=" + sFeatureAODVersion);
        } else if (DEBUG) {
            Log.v(TAG, "loadFirmwareVersion. Not making native call.");
        }
    }

    public static int getSensorHubVersion() {
        if (DEBUG) {
            Log.v(TAG, "getSensorHubVersion sMSP430FwVersion=" + sMSP430FwVersion);
        }
        int shFwVersion = sMSP430FwVersion;
        Log.i(TAG, "getSensorHubVersion. returning value=" + shFwVersion);
        return shFwVersion;
    }

    public static int getAODFeatureVersion() {
        if (DEBUG) {
            Log.v(TAG, "getAODFeatureVersion sFeatureAODVersion=" + sFeatureAODVersion);
        }
        int aodVersion = sFeatureAODVersion;
        Log.i(TAG, "getAODFeatureVersion. returning value=" + aodVersion);
        return aodVersion;
    }
}
