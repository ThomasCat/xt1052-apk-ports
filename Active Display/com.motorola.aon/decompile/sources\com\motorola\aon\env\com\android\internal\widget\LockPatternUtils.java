package com.motorola.aon.env.com.android.internal.widget;

import android.content.Context;
import android.util.Log;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class LockPatternUtils {
    private static final String CLASS_LOCK_PATTERN_UTILS = "com.android.internal.widget.LockPatternUtils";
    private static final String METHOD_GET_POWER_BUTTON_INSTANTLY_LOCKS = "getPowerButtonInstantlyLocks";
    private static final String TAG = "Aon_LockPatternUtils";
    private static Constructor<?> sConstructorLockPatternUtils;
    public static boolean sInitialized;
    private static Method sMethodGetPowerButtonInstantlyLocks;
    private Object mObjLockPatternUtils;

    public boolean getPowerButtonInstantlyLocks() {
        boolean z = false;
        if (!sInitialized || sMethodGetPowerButtonInstantlyLocks == null) {
            return z;
        }
        try {
            return ((Boolean) sMethodGetPowerButtonInstantlyLocks.invoke(this.mObjLockPatternUtils, new Object[0])).booleanValue();
        } catch (Throwable th) {
            Log.w(TAG, "unable to invoke getPowerButtonInstantlyLocks");
            return z;
        }
    }

    public LockPatternUtils(Context context) {
        if (sInitialized && sConstructorLockPatternUtils != null) {
            try {
                this.mObjLockPatternUtils = sConstructorLockPatternUtils.newInstance(new Object[]{context});
            } catch (Throwable th) {
                Log.w(TAG, "unable to construct");
            }
        }
    }

    static {
        sInitialized = false;
        try {
            Class<?> clazz = Class.forName(CLASS_LOCK_PATTERN_UTILS);
            sConstructorLockPatternUtils = clazz.getConstructor(new Class[]{Context.class});
            sMethodGetPowerButtonInstantlyLocks = clazz.getDeclaredMethod(METHOD_GET_POWER_BUTTON_INSTANTLY_LOCKS, new Class[0]);
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to intialize class");
        }
    }
}
