package com.motorola.aon.env.com.android.internal.statusbar;

import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class IStatusBarService {
    private static final String METHOD_ON_CLEAR = "onNotificationClearEndPoint";
    private static final String METHOD_ON_CLICK = "onNotificationClickEndPoint";
    private static final String METHOD_ON_PANEL_REVEALED = "onPanelRevealed";
    private static final String METHOD_REGISTER_END_POINT_WITH_WHITE_LIST = "registerEndPointWithWhiteList";
    private static final String METHOD_UNREGISTER_END_POINT = "unRegisterEndPoint";
    private static final String METHOD_UPDATE_WHITE_LIST = "updateWhiteList";
    private static final String TAG = "Aon_IStatusBarService";
    private static Class<?> mClassIEndPoint;
    public static boolean sInitialized;
    private final Object mIStatusBarService;
    private Method mMehtodOnPanelRevealed;
    private Method mMethodOnNotificationClearEndPoint;
    private Method mMethodOnNotificationClickEndPoint;
    private Method mMethodRegisterEndPointWithWhiteList;
    private Method mMethodUnregisterEndPoint;
    private Method mMethodUpdateWhiteList;

    public static class Stub {
        private static String CLASS_STUB = "com.android.internal.statusbar.IStatusBarService$Stub";
        private static String METHOD_AS_INTERFACE = "asInterface";
        private static final String TAG = "IStatusBarService.Stub";
        public static boolean sInitialized;
        private static Method sMethodAsInterface;

        static {
            sInitialized = false;
            try {
                sMethodAsInterface = Class.forName(CLASS_STUB).getDeclaredMethod(METHOD_AS_INTERFACE, new Class[]{IBinder.class});
                sInitialized = true;
            } catch (Throwable th) {
                Log.w(TAG, "unable to initialize class");
            }
        }

        public static IStatusBarService asInterface(IBinder service) {
            if (sInitialized) {
                try {
                    return new IStatusBarService(sMethodAsInterface.invoke(null, new Object[]{service}));
                } catch (Throwable th) {
                    Log.w(TAG, "unable to inovke asInterface");
                }
            }
            return null;
        }
    }

    static {
        sInitialized = false;
        try {
            mClassIEndPoint = Class.forName(CommandQueue.CLASS_IEND_POINT);
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to get command queue class");
        }
    }

    private IStatusBarService(Object localProxy) {
        this.mIStatusBarService = localProxy;
        Class<?> classIStatusBarService = this.mIStatusBarService.getClass();
        try {
            this.mMethodOnNotificationClickEndPoint = classIStatusBarService.getDeclaredMethod(METHOD_ON_CLICK, new Class[]{IBinder.class});
            this.mMethodOnNotificationClearEndPoint = classIStatusBarService.getDeclaredMethod(METHOD_ON_CLEAR, new Class[]{IBinder.class});
            this.mMethodRegisterEndPointWithWhiteList = classIStatusBarService.getDeclaredMethod(METHOD_REGISTER_END_POINT_WITH_WHITE_LIST, new Class[]{mClassIEndPoint, List.class, List.class, List.class});
            this.mMethodUpdateWhiteList = classIStatusBarService.getDeclaredMethod(METHOD_UPDATE_WHITE_LIST, new Class[]{mClassIEndPoint, List.class});
            this.mMethodUnregisterEndPoint = classIStatusBarService.getDeclaredMethod(METHOD_UNREGISTER_END_POINT, new Class[]{mClassIEndPoint});
            this.mMehtodOnPanelRevealed = classIStatusBarService.getDeclaredMethod(METHOD_ON_PANEL_REVEALED, new Class[0]);
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize status bar service");
        }
    }

    public void onNotificationClickEndPoint(IBinder key) throws RemoteException {
        if (!sInitialized || this.mIStatusBarService == null) {
            Log.w(TAG, "not initialized or null service");
            return;
        }
        try {
            this.mMethodOnNotificationClickEndPoint.invoke(this.mIStatusBarService, new Object[]{key});
        } catch (Throwable t) {
            Log.w(TAG, "unable to invoke onNotificationClickEndPoint");
            Throwable t0 = t.getCause();
            if (t0 != null && (t0 instanceof RemoteException)) {
                throw new RemoteException(t0.getMessage());
            }
        }
    }

    public void onNotificationClearEndPoint(IBinder key) throws RemoteException {
        if (!sInitialized || this.mIStatusBarService == null) {
            Log.w(TAG, "not initialized or null service");
            return;
        }
        try {
            this.mMethodOnNotificationClearEndPoint.invoke(this.mIStatusBarService, new Object[]{key});
        } catch (Throwable t) {
            Throwable t0 = t.getCause();
            Log.w(TAG, "unable to invoke onNotificationClearEndPoint");
            if (t0 != null && (t0 instanceof RemoteException)) {
                throw new RemoteException(t0.getMessage());
            }
        }
    }

    public boolean updateWhiteList(CommandQueue mNotificationMonitor, List<String> whiteList) throws RemoteException {
        if (!sInitialized || this.mIStatusBarService == null) {
            Log.w(TAG, "not initialized or null service");
        } else {
            try {
                this.mMethodUpdateWhiteList.invoke(this.mIStatusBarService, new Object[]{mNotificationMonitor.getCommandQueueObject(), whiteList});
            } catch (Throwable t) {
                Log.w(TAG, "unable to invoke update white list");
                Throwable t0 = t.getCause();
                if (t0 != null && (t0 instanceof RemoteException)) {
                    throw new RemoteException(t0.getMessage());
                }
            }
        }
        return false;
    }

    public void unRegisterEndPoint(CommandQueue mNotificationMonitor) throws RemoteException {
        if (!sInitialized || this.mIStatusBarService == null) {
            Log.w(TAG, "not initialized or null service");
            return;
        }
        try {
            this.mMethodUnregisterEndPoint.invoke(this.mIStatusBarService, new Object[]{mNotificationMonitor.getCommandQueueObject()});
        } catch (Throwable t) {
            Log.w(TAG, "unable to invoke unregister end point");
            Throwable t0 = t.getCause();
            if (t0 != null && (t0 instanceof RemoteException)) {
                throw new RemoteException(t0.getMessage());
            }
        }
    }

    public void registerEndPointWithWhiteList(CommandQueue mNotificationMonitor, ArrayList<IBinder> notificationKeys, ArrayList<MinedData> notifications, List<String> whiteList) throws RemoteException {
        if (!sInitialized || this.mIStatusBarService == null) {
            Log.w(TAG, "not initialized or null service");
            return;
        }
        try {
            ArrayList<Object> remoteNotifications = new ArrayList<>();
            this.mMethodRegisterEndPointWithWhiteList.invoke(this.mIStatusBarService, new Object[]{mNotificationMonitor.getCommandQueueObject(), notificationKeys, remoteNotifications, whiteList});
            if (notificationKeys.size() > 0 && remoteNotifications.size() == notificationKeys.size()) {
                for (int i = 0; i < remoteNotifications.size(); i++) {
                    MinedData md = MinedData.from(remoteNotifications.get(i));
                    if (md != null) {
                        notifications.add(md);
                    }
                }
            }
        } catch (Throwable t) {
            Log.w(TAG, "unable to invoke register end point");
            Throwable t0 = t.getCause();
            if (t0 != null && (t0 instanceof RemoteException)) {
                throw new RemoteException(t0.getMessage());
            }
        }
    }

    public void onPanelRevealed() throws RemoteException {
        if (!sInitialized || this.mIStatusBarService == null) {
            Log.w(TAG, "not initialized or null service");
            return;
        }
        try {
            this.mMehtodOnPanelRevealed.invoke(this.mIStatusBarService, new Object[0]);
        } catch (Throwable t) {
            Log.w(TAG, "unable to invoke onPanelRevealed");
            Throwable t0 = t.getCause();
            if (t0 != null && (t0 instanceof RemoteException)) {
                throw new RemoteException(t0.getMessage());
            }
        }
    }
}
