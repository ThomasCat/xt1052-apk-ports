package com.motorola.aon.env.com.motorola.wirelessdisplay.service;

import android.util.Log;

public class WirelessDisplayManager {
    private static final String CLASS_WIRELESS_DISPLAY_MANAGER = "com.motorola.wirelessdisplay.service.WirelessDisplayManager";
    public static String EXTRA_WIRELESS_DISPLAY_MIRRORING_STATE = null;
    private static final String FIELD_EXTRA_WIRELESS_DISPLAY_MIRRORING_STATE = "EXTRA_WIRELESS_DISPLAY_MIRRORING_STATE";
    private static final String FIELD_WIRELESS_DISPLAY_MIRRORING_OFF = "WIRELESS_DISPLAY_MIRRORING_OFF";
    private static final String FIELD_WIRELESS_DISPLAY_MIRRORING_ON = "WIRELESS_DISPLAY_MIRRORING_ON";
    private static final String FIELD_WIRELESS_DISPLAY_MIRRORING_STATE_CHANGED_ACTION = "WIRELESS_DISPLAY_MIRRORING_STATE_CHANGED_ACTION";
    private static final String TAG = "Aon_WirelessDisplayManager";
    public static int WIRELESS_DISPLAY_MIRRORING_OFF;
    public static int WIRELESS_DISPLAY_MIRRORING_ON;
    public static String WIRELESS_DISPLAY_MIRRORING_STATE_CHANGED_ACTION;
    public static boolean sInitialized;

    static {
        WIRELESS_DISPLAY_MIRRORING_STATE_CHANGED_ACTION = "com.motorola.wirelessdisplay.service.WIRELESS_DISPLAY_MIRRORING_STATE_CHANGED";
        EXTRA_WIRELESS_DISPLAY_MIRRORING_STATE = "wireless_display_mirroring_state";
        WIRELESS_DISPLAY_MIRRORING_OFF = 0;
        WIRELESS_DISPLAY_MIRRORING_ON = 2;
        sInitialized = false;
        try {
            Class<?> clazz = Class.forName(CLASS_WIRELESS_DISPLAY_MANAGER);
            WIRELESS_DISPLAY_MIRRORING_STATE_CHANGED_ACTION = (String) clazz.getDeclaredField(FIELD_WIRELESS_DISPLAY_MIRRORING_STATE_CHANGED_ACTION).get(null);
            EXTRA_WIRELESS_DISPLAY_MIRRORING_STATE = (String) clazz.getDeclaredField(FIELD_EXTRA_WIRELESS_DISPLAY_MIRRORING_STATE).get(null);
            WIRELESS_DISPLAY_MIRRORING_OFF = clazz.getDeclaredField(FIELD_WIRELESS_DISPLAY_MIRRORING_OFF).getInt(null);
            WIRELESS_DISPLAY_MIRRORING_ON = clazz.getDeclaredField(FIELD_WIRELESS_DISPLAY_MIRRORING_ON).getInt(null);
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize class");
        }
    }
}
