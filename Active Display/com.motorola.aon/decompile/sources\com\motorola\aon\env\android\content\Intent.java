package com.motorola.aon.env.android.content;

import android.os.IBinder;
import android.util.Log;
import java.lang.reflect.Method;

public class Intent {
    private static final String CLASS_INTENT = "android.content.Intent";
    private static final String METHOD_PUT_EXTRA = "putExtra";
    private static final String TAG = "Aon_Intent";
    public static boolean sInitialized;
    private static Method sMethodPutExtra;

    public static void putExtra(android.content.Intent intent, String string, IBinder binder) {
        if (sMethodPutExtra != null) {
            try {
                sMethodPutExtra.invoke(intent, new Object[]{string, binder});
            } catch (Throwable th) {
                Log.w(TAG, "unable to invoke putExtra");
            }
        }
    }

    static {
        sInitialized = false;
        try {
            sMethodPutExtra = Class.forName(CLASS_INTENT).getDeclaredMethod(METHOD_PUT_EXTRA, new Class[]{String.class, IBinder.class});
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize class");
        }
    }
}
