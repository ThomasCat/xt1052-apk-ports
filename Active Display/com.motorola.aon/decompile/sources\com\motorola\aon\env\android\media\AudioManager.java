package com.motorola.aon.env.android.media;

import android.util.Log;
import java.lang.reflect.Method;

public class AudioManager {
    static final String CLASS_AUDIO_MANAGER = "android.media.AudioManager";
    static final String FIELD_STREAM_FM = "STREAM_FM";
    static final String METHOD_ADJUST_LOCAL_OR_REMOTE_STREAM_VOLUME = "adjustLocalOrRemoteStreamVolume";
    static final String METHOD_IS_FM_ACTIVE = "isFMActive";
    public static int STREAM_FM = 0;
    private static final String TAG = "Aon_AudioManager";
    public static boolean sInitialized;
    private static Method sMethodAdjustLocalOrRemoteStreamVolume;
    private static Method sMethodIsFMActive;

    static {
        STREAM_FM = 0;
        sInitialized = false;
        try {
            Class<?> clazz = Class.forName(CLASS_AUDIO_MANAGER);
            sMethodIsFMActive = clazz.getDeclaredMethod(METHOD_IS_FM_ACTIVE, new Class[0]);
            sMethodAdjustLocalOrRemoteStreamVolume = clazz.getDeclaredMethod(METHOD_ADJUST_LOCAL_OR_REMOTE_STREAM_VOLUME, new Class[]{Integer.TYPE, Integer.TYPE});
            STREAM_FM = clazz.getDeclaredField(FIELD_STREAM_FM).getInt(null);
            sInitialized = true;
        } catch (Throwable th) {
            Log.w(TAG, "unable to initialize class");
        }
    }

    public static boolean isFMActive(Object obj) {
        boolean result = false;
        if (!sInitialized || sMethodIsFMActive == null) {
            return result;
        }
        try {
            return ((Boolean) sMethodIsFMActive.invoke(obj, new Object[0])).booleanValue();
        } catch (Throwable th) {
            Log.w(TAG, "unable to invoke disable");
            return result;
        }
    }

    public static void adjustLocalOrRemoteStreamVolume(Object obj, int streamType, int direction) {
        if (sInitialized && sMethodAdjustLocalOrRemoteStreamVolume != null) {
            try {
                sMethodAdjustLocalOrRemoteStreamVolume.invoke(obj, new Object[]{Integer.valueOf(streamType), Integer.valueOf(direction)});
            } catch (Throwable th) {
                Log.w(TAG, "unable to invoke aAdjustLocalOrRemoteStreamVolume");
            }
        }
    }
}
